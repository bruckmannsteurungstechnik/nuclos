#!/bin/bash 
APP_DIR="/home/tpasch2/nuclos40/webapp/app"
SERVER="http://localhost:8080/nuclos-war"
JAVA_OPTIONS="-ea -ms256m -mx768m -Dserver=$SERVER -XX:PermSize=128M -XX:MaxPermSize=256M -XX:+UseThreadPriorities"

if [ -n "$JAVA_HOME" ]; then
	JAVA="$JAVA_HOME/bin/java"
elif [ -n "$JDK_HOME" ]; then
	JAVA="$JDK_HOME/bin/java"
elif [ -n "$JRE_HOME" ]; then
	JAVA="$JRE_HOME/bin/java"
else
	JAVA="java"
fi

unpack() {
	for i in *.jar.pack.gz; do
		if [ "$i" != "*.jar.pack.gz" ]; then 
			JAR=`basename "$i" ".pack.gz"`
			unpack200 "$i" "$JAR"
			if [ "$?" -ne 0 ]; then
				echo "Error unpack200: $i"
				exit -1
			fi
			rm "$i"
		fi
	done
}

cd "$APP_DIR"

# Add jnlp file (if not done before)
if [ ! -f "jnlp-1.0.jar" ]; then
	cp "$HOME/.m2/repository/de/novabit/jnlp/1.0/jnlp-1.0.jar" "."
fi

# Unpack pack200 stuff (if not done before)
unpack
if [ -d "extensions" ]; then
	pushd "extensions"
		for i in *; do
			if [ -d "$i" ]; then
				pushd "$i"
					unpack
				popd
			fi
		done
	popd
fi

# construct class path
CLASSPATH=`ls -1 *.jar extensions/*/*.jar | xargs echo | sed "s/ /:/g"`
if [ $? -ne 0 ]; then
	echo "Error construction CLASSPATH"
	exit -1
fi

# exec java
exec "$JAVA" $JAVA_OPTIONS -cp "$CLASSPATH" -Dserver="$SERVER" org.nuclos.client.main.Main
