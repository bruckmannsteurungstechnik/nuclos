//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.swing.JComponent;
import javax.swing.JOptionPane;

import org.apache.commons.io.FileUtils;
import org.nuclos.client.main.MainController;
import org.nuclos.client.ui.Errors;
import org.nuclos.common2.StringUtils;

public class DesktopUtils {
	
	//private static final Logger LOG = Logger.getLogger(DesktopUtils.class);
	
	public static void openURI(String sUri, JComponent parent) {
		if (org.nuclos.common2.StringUtils.isNullOrEmpty(sUri))
			return;
		String str = org.nuclos.common2.StringUtils.emptyIfNull(sUri).toLowerCase();
		boolean bHasProtocol = str.startsWith("http:") || str.startsWith("https:") || str.startsWith("file:") 
				|| str.startsWith("ftp:") || str.startsWith("mailto:");
	
		try {
			if (Desktop.isDesktopSupported()) {
				Desktop desktop = Desktop.getDesktop();
				URI uri;
				if (str.startsWith("file:")) {
					if (desktop.isSupported(Desktop.Action.OPEN)) {
						for (int i = 0; i <= 2; i++) {
						URL url = new URL(sUri);
						File file2 = FileUtils.toFile(url);
							if (file2.exists()) {
								Desktop.getDesktop().open(file2);
								return;
							} 
							if (i == 2)	{
								String text = "Error opening:" + file2.getAbsolutePath();
								JOptionPane.showMessageDialog(MainController.getMainFrame(), text);
							} else {
								sUri = "file:" + "/" + sUri.substring(5);
							}
						}
					}
					return;
				} else {
					if(bHasProtocol)	
						uri = new URI(sUri);
					else {
						if (sUri.indexOf("@") != -1) //@todo use a pattern here.
							 uri = new URI("mailto:" + sUri);
						else
							uri = new URI("http://" + sUri);
					}
				}
				if (sUri.startsWith("mailto:")) {
					if (desktop.isSupported(Desktop.Action.MAIL)) {
						Desktop.getDesktop().mail(uri);
						return;
					}
				}
				
				if (desktop.isSupported(Desktop.Action.BROWSE)) {
					Desktop.getDesktop().browse(uri);				
				}
			}
			
		} catch (IOException ex) {
			Errors.getInstance().showExceptionDialog(parent, "URIMouseAdapter.1", ex);
		} catch (URISyntaxException ex) {
			Errors.getInstance().showExceptionDialog(parent, "URIMouseAdapter.1", ex);
		} 
	}

	public static void openURL(String sUrl, JComponent parent) {
		try {
			if (!StringUtils.looksEmpty(sUrl)) {
				if (sUrl.indexOf("://") == -1) {
					sUrl = "http://"+sUrl;
				}
				Desktop.getDesktop().browse(URI.create(sUrl));
			}
		} catch (Exception ex) {
			Errors.getInstance().showExceptionDialog(parent, "URIMouseAdapter.1", ex);
		}
	}
	
	public static void newMail(String to, JComponent parent) {
		try {
			if (!StringUtils.looksEmpty(to)) {
				if (to.indexOf("://") == -1) {
					to = "mailto:"+to;
				}
				Desktop.getDesktop().mail(URI.create(to));
			}
		} catch (Exception ex) {
			Errors.getInstance().showExceptionDialog(parent, "URIMouseAdapter.1", ex);
		}
	}
}
