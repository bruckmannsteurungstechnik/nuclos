//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.prefs.Preferences;

import javax.swing.JTable;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.log4j.Logger;
import org.nuclos.client.autonumber.AutonumberUiUtils;
import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelProvider;
import org.nuclos.client.ui.collect.detail.FieldEvaluator;
import org.nuclos.client.ui.collect.model.SortableCollectableTableModel;
import org.nuclos.client.ui.collect.model.SortableCollectableTableModelImpl;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.client.ui.collect.subform.SubFormTableModel;
import org.nuclos.client.ui.collect.subform.ToolbarFunction;
import org.nuclos.client.ui.dnd.IReorderable;
import org.nuclos.client.ui.gc.ListenerUtil;
import org.nuclos.client.ui.table.TableUtils;
import org.nuclos.common.ProfileUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.StateClientDelegate;
import org.nuclos.common.UID;
import org.nuclos.common.WorkspaceDescription2.EntityPreferences;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableFactory;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProviderFactory;
import org.nuclos.common.collect.collectable.CollectableUtils;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.preferences.TablePreferencesManager;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.statemodel.valueobject.StateVO;

/**
 * Controller for collecting dependant data (in a one-to-many relationship) in a subform,
 * without the hassles concerning ValueObjectList or parent id.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:christoph.radig@novabit.de">christoph.radig</a>
 * @version 01.00.00
 */
public abstract class AbstractDetailsSubFormController<PK,Clct extends Collectable<PK>>
		extends SubFormController
		implements CollectableFactory<Clct> {

	private static final Logger LOG = Logger.getLogger(AbstractDetailsSubFormController.class);
	private FieldEvaluator<Boolean> isFieldEditableEvaluator;

	/**
	 * Successors must call postCreate.
	 * @param clctcompmodelproviderParent
	 * @param subform
	 * @param prefsUserParent
	 * @param clctfproviderfactory
	 * @see #postCreate()
	 */
	public AbstractDetailsSubFormController(CollectableEntity clcte, MainFrameTab tab, CollectableComponentModelProvider clctcompmodelproviderParent,
			UID parentEntityUID, SubForm subform, Preferences prefsUserParent, EntityPreferences entityPrefs, CollectableFieldsProviderFactory clctfproviderfactory) {

		super(clcte, tab, clctcompmodelproviderParent, parentEntityUID, subform, false, prefsUserParent, entityPrefs, clctfproviderfactory);

		getJTable().setModel(new DetailsSubFormTableModelImpl<>(this.newCollectableList(new ArrayList<>())));
		//
	}

	protected final void initModelAndKeys() {
		SubForm subform = getSubForm();

		// initialize table model:
		this.getCollectableTableModel().setColumns(getTableColumns());
		this.getCollectableTableModel().addSortingListener(newSubFormTablePreferencesUpdateListener());

		// Inititialize listeners for toolbar actions:
		ListenerUtil.registerSubFormToolListener(subform, this, (String actionCommand) -> {
			if (ToolbarFunction.fromCommandString(actionCommand) == ToolbarFunction.MULTIEDIT) {
				cmdMultiEdit();
			}
		});

		if (this.getCollectableTableModel().getColumnCount() > 0) {
			SortKey sortKey = null; //new SortKey(0, SortOrder.ASCENDING);
			final UID sInitialSortingColumn = subform.getInitialSortingColumn();
			if (sInitialSortingColumn != null) {
				final String sInitialSortingOrder = subform.getInitialSortingOrder();
				final int iColumn = this.getCollectableTableModel().findColumnByFieldUid(sInitialSortingColumn);
				if (iColumn >= 0) {
					sortKey = new SortKey(iColumn, sInitialSortingOrder.equals("ascending") ? SortOrder.ASCENDING : SortOrder.DESCENDING);
				}
			}
			// this.getCollectableTableModel().setSortKeys(Collections.singletonList(sortKey), true);
			final List<SortKey> sortKeys = new ArrayList<SortKey>();
			if (sortKey != null) {
				sortKeys.add(sortKey);
			}
			this.getCollectableTableModel().setSortKeys(sortKeys, true);
		}

		if (this.isColumnSelectionAllowed(getParentEntityUID())) {
			final List<UID> lstStoredFieldNames = getTablePreferencesManager().getSelectedColumns();
			if (!lstStoredFieldNames.isEmpty()) {
				removeColumnsFromTableColumnModel(subform.getJTable(), lstStoredFieldNames, true);
			}
			else {
				List<UID> lstSystemFields = new ArrayList<UID>();
				for (SF<?> stfield : SF.getAllFields()) {
					lstSystemFields.add(stfield.getUID(getCollectableEntity().getUID()));
				}
				removeColumnsFromTableColumnModel(subform.getJTable(), lstSystemFields, false);
			}
		}
	}
	
	/**
	 * set {@link FieldEvaluator} that is applied during field access
	 * 
	 * @param fev field evaluation visitor
	 */
	public synchronized void setIsFieldEditableEvaluator(final FieldEvaluator<Boolean> fev) {
		this.isFieldEditableEvaluator = fev;
	}

	/**
	 * @return a new <code>Collectable</code> for this subform.
	 */
	@Override
	public abstract Clct newCollectable();

	/**
	 * @param lstclct
	 * @return a new list of collectables for the given list. Successors may want to wrap the given list.
	 */
	protected abstract List<Clct> newCollectableList(List<Clct> lstclct);

	/**
	 * @return the table model used for the subform. May be used to add/remove/change rows in the subform.
	 */
    public final SortableCollectableTableModel<PK,Clct> getCollectableTableModel() {
		return (DetailsSubFormTableModel<PK,Clct>) getJTable().getModel();
	}

	/**
	 * @return the table model used for the subform. May be used to add/remove/change rows in the subform.
	 */
    @Override
	protected final SubFormTableModel getSubFormTableModel() {
		return (DetailsSubFormTableModel<PK,Clct>) getJTable().getModel();
	}

	/**
	 * @return an unmodifiable list containing the rows in the table model.
	 */
	public final List<Clct> getCollectables() {
		return this.getCollectableTableModel().getCollectables();
	}

	protected List<Clct> getModifiableListOfCollectables() {
		/** @todo elimminate this workaround - why not make getRows() public? */
		return ((DetailsSubFormTableModelImpl<PK,Clct>) this.getCollectableTableModel()).getRows();
	}

	/**
	 * stops editing and assigns the given list to this subform's table model.
	 * @param lstclct
	 */
	protected final void setCollectables(List<Clct> lstclct) {
		// Stop editing the current cell if any; else controls may be left over / UA
		this.stopEditing();
		isIgnorePreferencesUpdate = true;
		if (lstclct != null && Modules.getInstance().isModule(getCollectableEntity().getUID())) {
			for (Clct clct : lstclct) {
				setStateIconAttribute(clct, getCollectableEntity().getUID());
			}
		}

		if (getSubForm().isReadOnly()) {
			// NUCLOS-6048 There is no need to wrap the records with DefaultValueObjectList in readonly Subforms
			this.getCollectableTableModel().setCollectables(lstclct);
		} else {
			this.getCollectableTableModel().setCollectables(this.newCollectableList(lstclct));
		}

		isIgnorePreferencesUpdate = false;
	}

	private void setStateIconAttribute(Collectable<PK> clct, UID iModuleId) {
		final UID stateIconField = SF.STATEICON.getUID(iModuleId);
		final CollectableField clctf = clct.getField(stateIconField);
		if (clctf != null && clctf.getValue() == null) {
			// get state icon from client cache only ...
			try {
				final StateClientDelegate stateDelegate = SpringApplicationContextHolder.getBean(StateClientDelegate.class);
				if (stateDelegate != null) {
					final UID stateField = SF.STATE.getUID(iModuleId);
					final StateVO state = stateDelegate.getState(
							iModuleId, (UID) clct.getField(stateField).getValueId());
					CollectableField value = new CollectableValueIdField(clct.getField(stateField).getValueId(), state.getIcon());
					//Avoid that all sub-form data with state-model are dirty (FlagUpdate) from the beginning.
					if (clct instanceof CollectableEntityObject) {
						((CollectableEntityObject<?>)clct).setField(stateIconField, value, true);
					} else {
						clct.setField(stateIconField, value);						
					}					
				}
			} catch (Exception ex) {
				// ignore: server call
			}
		}
	}

	/**
	 * updates the table model with the given list of <code>Collectable</code>s.
	 * 
	 * §todo refactor: setCollectables(lstclct, bSortTable)
	 * §todo make this one public or fillSubForm()?
	 * 
	 * @param lstclct List&lt;Collectable&gt;
	 */
	public void updateTableModel(List<Clct> lstclct) {
		setCollectables(lstclct);
		isIgnorePreferencesUpdate = true;
		this.getCollectableTableModel().sort();
		isIgnorePreferencesUpdate = false;
	}

	@Override
	protected Clct insertNewRow() throws CommonBusinessException {
		Clct clct = this.newCollectable();
		this.getCollectableTableModel().add(clct);
		return clct;
	}
	
	@Override
	protected Clct insertNewRow(int idx) throws CommonBusinessException {
		Clct clct = this.newCollectable();
		this.getCollectableTableModel().setCollectable(idx, clct);
		return clct;
	}

	@Override
	public boolean isColumnEnabled(UID sColumnUid) {
		return this.isEnabled() && this.getSubForm().isColumnEnabled(sColumnUid);
	}

	/**
	 * lets the user add/remove multiple rows at once. This requires the subform to define a unique master data column.
	 */
	public void cmdMultiEdit() {
		UIUtils.runCommandForTabbedPane(this.getMainFrameTabbedPane(), new Runnable() {
			@Override
			public void run() {
				final SubForm subform = AbstractDetailsSubFormController.this.getSubForm();
				if (subform.getUniqueMasterColumnUid() == null || UID.UID_NULL.equals(subform.getUniqueMasterColumnUid()) ) {
					throw new IllegalStateException("No unique master column defined for subform " + subform.getEntityUID() + ".");
				}

				if (AbstractDetailsSubFormController.this.stopEditing()) {
					new SubFormMultiEditController<PK,Clct>(subform, AbstractDetailsSubFormController.this).run();
				}
			}
		});
	}

	/**
	 * @return Always returns true
	 */
	protected boolean isColumnSelectionAllowed(UID parentEntityUID) {
		return true;
	}

	/**
	 * must be called at the end of the ctor in each subclass.
	 */
	protected void postCreate() {
		final JTable tbl = this.getJTable();

		this.setupTableCellRenderers(this.getSubFormTableModel());
		this.setupRowHeight(this.getSubFormTableModel());
		this.setupStaticTableCellEditors(tbl);

		this.setColumnWidths();
		this.setColumnOrder();

		this.setupTableModelListener();
		this.setupColumnModelListener();

		TableUtils.addMouseListenerForSortingToTableHeader(tbl, this.getCollectableTableModel());
	}

	public void setupHistoryLookUp(ISubformHistoricalViewLookUp historyLookUp) {
		this.getSubForm().setupHistoryLookUp(historyLookUp);
	}
	
	@Override
	protected final PreferencesUpdateListener newSubFormTablePreferencesUpdateListener() {
		return new PreferencesUpdateListener();
	}

	protected class PreferencesUpdateListener extends SubFormController.PreferencesUpdateListener implements ChangeListener {
		@Override
		public void stateChanged(ChangeEvent ev) {
//			System.out.println("stateChanged " + ev);
			if (!isIgnorePreferencesUpdate) {
				storeColumnOrderToPreferences();
			}
		}
	}

	protected void storeColumnOrderToPreferences(){
		getTablePreferencesManager().setSortKeys(getCollectableTableModel().getSortKeys(),
				new TablePreferencesManager.IColumnFieldUidResolver() {
			@Override
			public UID getColumnFieldUid(int iColumn) {
				return getSubFormTableModel().getColumnFieldUid(iColumn);
			}
		});
	}

	/**
	 * Reads the user-preferences for the sorting order.
	 */
	protected List<SortKey> readColumnOrderFromPreferences() {
		return getTablePreferencesManager().getSortKeys(
				new TablePreferencesManager.IColumnIndexResolver() {
			@Override
			public int getColumnIndex(UID columnIdentifier) {
				return getCollectableTableModel().findColumnByFieldUid(columnIdentifier);
			}
		});
	}

	/**
	 * sorts the TableModel by a given column if declared in preferences.
	 */
	protected void setColumnOrder() {
		LOG.debug("setColumnOrder");

		isIgnorePreferencesUpdate = true;
		List<SortKey> sortKeys = readColumnOrderFromPreferences();
		if (this.getCollectableTableModel().getColumnCount() > 0) {
			//NUCLOS-1477
			// force ordering by autonumberfield
			int iColumnAutoNumber = AutonumberUiUtils.findColumnAutoNumber(getSubForm().getSubformTable());
			int iSortKeyAutoNumberIdx = 0;
			boolean bFoundSortKeyAutoNumber = false;
			
			if (iColumnAutoNumber > -1) {
				if (getSubForm().isAutonumberSorting()) {
					for (final SortKey sk : sortKeys) {
						if (sk.getColumn() == iColumnAutoNumber) {
							bFoundSortKeyAutoNumber = true;
							break;
						}
						++iSortKeyAutoNumberIdx;
					}
					if (bFoundSortKeyAutoNumber) {
						//Collections.rotate(sortKeys.subList(iSortKeyAutoNumberIdx, 1), -1);
						sortKeys.add(0, sortKeys.get(iSortKeyAutoNumberIdx));
						
					} else {
						sortKeys.add(0, new SortKey(iColumnAutoNumber, SortOrder.ASCENDING));
					}					
				} 
			}
			try {
				this.getCollectableTableModel().setSortKeys(sortKeys, true); // sortImmediately=true (NUCLOS-7029)
			} catch (IllegalArgumentException e) {
				LOG.warn("Sorting in subform \"" + this.getParentEntityUID() + "." + this.getSubForm().getEntityUID() +
					"\" could not be restored. Column count has changed.", e);
			}
		}
		isIgnorePreferencesUpdate = false;
	}

	abstract EntityCollectController<?,?> getCollectController();

	/**
	 * <code>TableModel</code> that can be used in a Details subform.
	 */
	protected interface DetailsSubFormTableModel<PK,Clct extends Collectable<PK>> extends SubFormTableModel, SortableCollectableTableModel<PK,Clct>, IReorderable {
	}
	
	abstract protected void valueSet();

	/**
	 * implementation of <code>DetailsSubFormTableModel</code>.
	 */
	protected class DetailsSubFormTableModelImpl<PK2,T extends Collectable<PK2>>
			extends SortableCollectableTableModelImpl<PK2,T>
			implements DetailsSubFormTableModel<PK2,T> {

		/**
		 * @param lstclct
		 */
		DetailsSubFormTableModelImpl(List<T> lstclct) {
			super(getEntityAndForeignKeyField().getEntity(), lstclct);
		}

		@Override
		protected List<T> getRows() {
			return super.getRows();
		}

		/**
		 * @param clctef
		 * @return a null value that can be set in a table cell for the given entity field.
		 */
		@Override
		public Object getNullValue(CollectableEntityField clctef) {
			return CollectableUtils.getNullField(clctef);
		}

		@Override
		public boolean isCellEditable(int iRow, int iColumn) {
			final CollectableEntityField collectableEntityField = this.getCollectableEntityField(iColumn);
			final CollectableEntityField clctef = collectableEntityField;
			final UID sColumnUid = collectableEntityField.getUID();
			if (isColumnEnabled(sColumnUid) 
				&& isRowEditable(iRow) 
				&& !AutonumberUiUtils.isAutonumber(clctef.getDefaultComponentType())) { // Nuclos-1477
				final Collectable<?> c = getRow(iRow);
				if (getSubForm().getEditEnabledScript() != null) {

					// apply evaluation hook setup by the parent collect controller
					if (null != isFieldEditableEvaluator) {
						synchronized(isFieldEditableEvaluator) {
							return isFieldEditableEvaluator.evaluate(getCollectController(), AbstractDetailsSubFormController.this, c, collectableEntityField);
						}
					}
				}
				return true;
			}
			return false;
		}

		/**
		 * @deprecated Strongly consider to use {@link #getCollectableEntityField(int)} instead.
		 */
		@Override
		public UID getColumnFieldUid(int iColumn) {
		
			return this.getCollectableEntityField(iColumn).getUID();
		}

		/*
		@Override
		public String getColumnFieldUid(int columnIndex) {
			return getCollectableEntityField(columnIndex).getName();
		}
		 */

		@Override
		public int getMinimumColumnWidth(int columnIndex) {
			return ProfileUtils.getMinimumColumnWidth(getCollectableEntityField(columnIndex).getJavaClass());
		}

		@Override
		public void setValueAt(Object oValue, int iRow, int iColumn) {		
			super.setValueAt(oValue, iRow, iColumn);
			valueSet();
		}

		@Override
		public void reorder(int fromModel, int toModel) {
			int from = fromModel;
			int to = toModel;
			
			if (from > to) {
				for (int i = from; i > to; i--) {
					Collections.swap(getRows(), i, i - 1);
					
				}
				from = toModel;
				to = fromModel;
			} else {
				for (int i = from; i < to; i++) {
					Collections.swap(getRows(), i, i + 1);
					
				}
			}
			fireTableRowsUpdated(from, to);
		}
		
		/**
		 * @param iColumn
		 * @return the name of column <code>iColumn</code>, as shown in the table header
		 */
		@Override
		public String getColumnName(int iColumn) {
			String sLabel = getSubForm().getColumnLabel(this.getCollectableEntityField(iColumn).getUID());
			if (sLabel != null) {
				return sLabel;
			}
			return super.getColumnName(iColumn);
		}
		
		
	}	// class DetailsSubFormTableModelImpl

}	// class AbstractDetailsSubFormController
