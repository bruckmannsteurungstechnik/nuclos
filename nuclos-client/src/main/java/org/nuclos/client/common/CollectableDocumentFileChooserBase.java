//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.common;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.dnd.InvalidDnDOperationException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.prefs.Preferences;

import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.nuclos.client.genericobject.CollectableGenericObjectAttributeField;
import org.nuclos.client.genericobject.CollectableGenericObjectFileChooser;
import org.nuclos.client.ui.Bubble;
import org.nuclos.client.ui.BubbleUtils;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.component.custom.AbstractCollectableFileChooser;
import org.nuclos.client.ui.popupmenu.DefaultJPopupMenuListener;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.PointerException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.attribute.DynamicAttributeVO;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.IOUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.SystemUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonIOException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.common.ejb3.EntityObjectFacadeRemote;
import org.nuclos.server.common.valueobject.DocumentFileBase;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;

/**
 * Base class for collectable file choosers.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * @author	<a href="mailto:uwe.allner@novabit.de">uwe.allner</a>
 * @version 01.00.00
 */
public abstract class CollectableDocumentFileChooserBase extends AbstractCollectableFileChooser implements
		MouseListener, NuclosDropTargetVisitor {

	private static final Logger LOG = Logger.getLogger(CollectableGenericObjectFileChooser.class);
	public static final String PREFS_KEY_LAST_DIRECTORY = "lastDirectory";
	public static final String PREFS_NODE_COLLECTABLEFILECHOOSER = "CollectableFileChooser";

	private static final ConcurrentMap<org.nuclos.common2.File, File> FILE_CACHE = new ConcurrentHashMap<>();

	private DragGestureListener dragGestureListener = null;

	public CollectableDocumentFileChooserBase(CollectableEntityField clctef, boolean bSearchable) {
		super(clctef, bSearchable);

		this.setupPopupMenu();
		//NUCLEUSINT-512
		getFileChooser().addMouseListener(this);
		getFileChooser().getFileNameComponent().addMouseListener(this);

		getFileChooser().getBrowseButton().addActionListener((ActionEvent e) -> cmdBrowse());
		setupDragDrop();
	}

	protected void setupPopupMenu() {
		JPopupMenu popupmenu = newJPopupMenu();
		this.getFileChooser().getFileNameComponent().addMouseListener(new DefaultJPopupMenuListener(popupmenu));
	}

	@Override
	public JPopupMenu newJPopupMenu() {
		final JPopupMenu popupmenu = new JPopupMenu();
		final JMenuItem miOpen = new JMenuItem(SpringLocaleDelegate.getInstance().getMessage(
				"CollectableDocumentFileChooserBase.1", "\u00d6ffnen"));
		popupmenu.add(miOpen);
		miOpen.addActionListener((ActionEvent ev) -> cmdOpenFile());

		final JMenuItem miSaveAs = new JMenuItem(SpringLocaleDelegate.getInstance().getMessage(
				"CollectableDocumentFileChooserBase.2", "Speichern unter..."));
		popupmenu.add(miSaveAs);
		miSaveAs.addActionListener((ActionEvent ev) -> cmdSaveAs());

		final JMenuItem miReset = new JMenuItem(SpringLocaleDelegate.getInstance().getMessage(
				"CollectableFileNameChooserBase.1", "Zur\u00fccksetzen"));
		popupmenu.add(miReset);
		miReset.addActionListener((ActionEvent ev) -> cmdReset());

		popupmenu.addPopupMenuListener(new PopupMenuListener() {
			@Override
			public void popupMenuWillBecomeVisible(PopupMenuEvent ev) {
				boolean bEnabled = false;
				try {
					final org.nuclos.common2.File file = (org.nuclos.common2.File) getField().getValue();
					bEnabled = (file != null);
				}
				catch (CollectableFieldFormatException ex) {
					// ignore
				}
				miOpen.setEnabled(bEnabled);
				miSaveAs.setEnabled(bEnabled);

				if (!getJComponent().isEnabled()) {
					miReset.setEnabled(false);
				}
				else {
					miReset.setEnabled(bEnabled);
				}
			}

			@Override
			public void popupMenuWillBecomeInvisible(PopupMenuEvent ev) {
			}

			@Override
			public void popupMenuCanceled(PopupMenuEvent ev) {
			}
		});

		return popupmenu;
	}

	private void cmdOpenFile() {
		UIUtils.runCommandLater(this.getJComponent(), new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				try {
					final org.nuclos.common2.File file1 = (org.nuclos.common2.File) getField().getValue();
					if (file1 != null) {
						File file = createTempFile(file1, true);
						if (file != null) {
							SystemUtils.open(file);
						}
					}
				}
				catch (IOException ex) {
					throw new CommonIOException(ex);
				}
			}
		});
	}

	private File createTempFile(org.nuclos.common2.File file1, boolean withExtension) throws IOException, NuclosBusinessException {
		byte[] abContents = file1.getContents();
		if (abContents == null) {
			throw new NuclosBusinessException(SpringLocaleDelegate.getInstance().getMessage(
					"CollectableDocumentFileChooserBase.3", "Die Datei ist leer."));
		}

		String parFileName = file1.getFilename();

		final String ext = FilenameUtils.getExtension(parFileName);
		String sFileName = FilenameUtils.getBaseName(parFileName);
		sFileName = sFileName.replaceAll("\\s", "");
		sFileName = sFileName.replaceAll("\\?", "");
		if (sFileName.length() < 3) {
			// see NUCLOS-5236
			sFileName = sFileName + "00";
		}

		try {
			byte[] content = getContent(abContents);
			File file;
			if (withExtension) {
				file = File.createTempFile(sFileName, "." + ext);
			} else {
			    String tDir = System.getProperty("java.io.tmpdir");
				file = new File(tDir, sFileName + "." + ext);
				if (file.exists()) {
					file.delete();
				}
				file.createNewFile();
			}
			LOG.debug("Schreibe Dokument " + parFileName + " in tempor\u00e4re Datei " + file.getAbsolutePath());
			IOUtils.writeToBinaryFile(file, content);
			LOG.debug("Schreiben der tempor\u00e4ren Datei war erfolgreich");

			file.setWritable(false);
			file.deleteOnExit();

			return file;
		} catch (CommonPermissionException e) {
			LOG.debug("tempor\u00e4re Datei wurde nicht erstellt. Grund: " + e.getMessage());
			LOG.trace(e);
			return null;
		}
	}

	private byte[] getContent(byte[] abContents) throws CommonPermissionException {
		if (abContents.length >= 20) {
			return abContents;
		}
		// NUCLOS-8027 If there is an ID instead of an Document, fetch the document directly
		try {
			long id = Long.parseLong(new String(abContents));
			EntityObjectFacadeRemote eoFacade = SpringApplicationContextHolder.getBean(EntityObjectFacadeRemote.class);
			UID entityUID = MetaProvider.getInstance().getEntityField(getFieldUID()).getEntity();
			EntityObjectVO<Long> eo = eoFacade.get(entityUID, id);
			return ((GenericObjectDocumentFile)eo.getFieldValue(getFieldUID())).getContents();
		} catch (NumberFormatException nfe) {
			// Really ignore
		}

		return abContents;
	}

	private void cmdSaveAs() {
		UIUtils.runCommandLater(this.getJComponent(), new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				final org.nuclos.common2.File file1 = (org.nuclos.common2.File) getField().getValue();
				if (file1 != null) {
					final byte[] abContents = file1.getContents();
					if (abContents == null) {
						throw new NuclosBusinessException(SpringLocaleDelegate.getInstance().getMessage(
								"CollectableDocumentFileChooserBase.3", "Die Datei ist leer."));
					}

					final Preferences prefs = getPreferences();
					final String sLastDir = (prefs == null) ? null : prefs.node(PREFS_NODE_COLLECTABLEFILECHOOSER).get(
							PREFS_KEY_LAST_DIRECTORY, null);
					final JFileChooser filechooser = new JFileChooser(sLastDir);
					filechooser.setSelectedFile(new java.io.File(sLastDir, file1.getFilename()));
					final int iBtn = filechooser.showSaveDialog(getJComponent());
					if (iBtn == JFileChooser.APPROVE_OPTION) {
						final java.io.File file = filechooser.getSelectedFile();
						if (file != null) {
							if (prefs != null) {
								prefs.node(PREFS_NODE_COLLECTABLEFILECHOOSER).put(PREFS_KEY_LAST_DIRECTORY,
										filechooser.getCurrentDirectory().getAbsolutePath());
							}
							try {
								byte[] content = getContent(abContents);
								LOG.debug("Schreibe Dokument in Datei " + file.getAbsolutePath() + ".");
								IOUtils.writeToBinaryFile(file, content);
								LOG.debug("Schreiben der Datei war erfolgreich.");
							} catch (CommonPermissionException | IOException e) {
								LOG.debug("Datei wurde nicht geschrieben. Grund: " + e.getMessage());
								Errors.getInstance().showExceptionDialog(getJComponent(), e);
							}
						}
					}
				}
			}
		});
	}

	private void cmdReset() {
		UIUtils.runCommandLater(this.getJComponent(), new CommonRunnable() {
			@Override
			public void run() {
				clear();
			}
		});
	}

	public final DragGestureListener getDragGestureListener() {
		return dragGestureListener;
	}

	protected void setupDragDrop() {
		if (dragGestureListener != null) {
			return;
		}

		final DragSourceListener dragSourceListener = new DragSourceListener() {

			@Override
			public void dragEnter(DragSourceDragEvent e) {
				e.getDragSourceContext().setCursor(DragSource.DefaultMoveDrop);
			}

			@Override
			public void dragExit(DragSourceEvent e) {
				e.getDragSourceContext().setCursor(DragSource.DefaultMoveNoDrop);
			}

			@Override
			public void dragOver(DragSourceDragEvent e) {
			}

			@Override
			public void dragDropEnd(DragSourceDropEvent e) {
				try {
					final org.nuclos.common2.File file1 = (org.nuclos.common2.File) getField().getValue();
					LOG.trace("DROPEND:" + file1);
					FILE_CACHE.remove(file1);
				} catch (CollectableFieldFormatException cfe) {
					LOG.warn(cfe.getMessage(), cfe);
				}
			}

			@Override
			public void dropActionChanged(DragSourceDragEvent e) {
			}
		};

		final Transferable transferable = new Transferable() {

			private final DataFlavor FLAVOR = DataFlavor.javaFileListFlavor;

			@Override
			public Object getTransferData(DataFlavor flavor) {
				try {
					final org.nuclos.common2.File file1 = (org.nuclos.common2.File) getField().getValue();
					if (file1 != null) {
						File file = FILE_CACHE.get(file1);
						if (file == null) {
							file = createTempFile(file1, false);
							FILE_CACHE.put(file1, file);
						}

						return Collections.singletonList(file);
					}
				}
				catch (Exception ex) {
					LOG.warn(ex);
				}
				return null;
			}

			@Override
			public DataFlavor[] getTransferDataFlavors() {
				DataFlavor[] f = new DataFlavor[1];
				f[0] = this.FLAVOR;
				return f;
			}

			@Override
			public boolean isDataFlavorSupported(DataFlavor flavor) {
				return this.FLAVOR.equals(flavor);
			}
		};
		dragGestureListener = new DragGestureListener() {
			@Override
			public void dragGestureRecognized(DragGestureEvent e) {
				try {
					e.startDrag(DragSource.DefaultCopyDrop, transferable, dragSourceListener);
				} catch (InvalidDnDOperationException ex) {
					// do nothing-
					LOG.warn(ex);
				}
			}
		};

		final DropTargetListener listener = new NuclosDropTargetListener(this);
		final DropTarget target1 = new DropTarget(getFileChooser(), DnDConstants.ACTION_COPY_OR_MOVE, listener, true);
		final DropTarget target2 = new DropTarget(getFileChooser().getFileNameComponent(),
				DnDConstants.ACTION_COPY_OR_MOVE, listener, true);
		final DragSource source = new DragSource();
		source.createDefaultDragGestureRecognizer(getFileChooser(), DnDConstants.ACTION_COPY_OR_MOVE,
				dragGestureListener);
		;
		source.createDefaultDragGestureRecognizer(getFileChooser().getFileNameComponent(),
				DnDConstants.ACTION_COPY_OR_MOVE, dragGestureListener);
		;

		/*
		new DropTarget(this.getFileChooser(), DnDConstants.ACTION_COPY_OR_MOVE, listener, true);
		new DragSource().createDefaultDragGestureRecognizer(this.getFileChooser(), DnDConstants.ACTION_COPY_OR_MOVE, dragGestureListener);
		new DropTarget(this.getFileChooser().getFileNameComponent(), DnDConstants.ACTION_COPY_OR_MOVE, listener, true);
		new DragSource().createDefaultDragGestureRecognizer(this.getFileChooser().getFileNameComponent(), DnDConstants.ACTION_COPY_OR_MOVE, dragGestureListener);
		 */
	}

	protected void cmdBrowse() {
		final Preferences prefs = this.getPreferences();
		final String sLastDir = (prefs == null) ? null : prefs.node(PREFS_NODE_COLLECTABLEFILECHOOSER).get(
				PREFS_KEY_LAST_DIRECTORY, null);
		final JFileChooser filechooser = new JFileChooser(sLastDir);

		// Customer's wish (B1149) / UA
		filechooser.setFileHidingEnabled(false);

		final int iBtn = filechooser.showOpenDialog(getJComponent());
		if (iBtn == JFileChooser.APPROVE_OPTION) {
			final java.io.File file = filechooser.getSelectedFile();
			if (file != null) {
				if (prefs != null) {
					prefs.node(PREFS_NODE_COLLECTABLEFILECHOOSER).put(PREFS_KEY_LAST_DIRECTORY,
							filechooser.getCurrentDirectory().getAbsolutePath());
				}
				loadFile(file);
			}
		}
	}

	@Override
	protected abstract org.nuclos.common2.File newFile(String sFileName, byte[] abContents);

	//NUCLEUSINT-512
	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON1) {
			if (e.getClickCount() == 2) {
				cmdOpenFile();
			}
		}
	}

	protected void loadFile(File file) {
		if (file != null) {
			final String sFileName = file.getName();
			try {
				UID documentFileUID = null;
				CollectableField field = getField();
				if (field != null && field.getValue() instanceof GenericObjectDocumentFile) {
					GenericObjectDocumentFile documentFile = (GenericObjectDocumentFile)field.getValue();
					if (documentFile != null) {
						documentFileUID = (UID) documentFile.getDocumentFilePk();
					}
				}
				if (documentFileUID == null || UID.UID_NULL.equals(documentFileUID)) {
					documentFileUID = DocumentFileBase.newFileUID();
				}
				final byte[] abContents = IOUtils.readFromBinaryFile(file);
				org.nuclos.common2.File newFile = newFile(sFileName, abContents);
				if (newFile instanceof GenericObjectDocumentFile) {
					((GenericObjectDocumentFile) newFile).setDocumentFilePk(documentFileUID);
					((GenericObjectDocumentFile) newFile).setDocumentFilePk(documentFileUID);
					DynamicAttributeVO daVO = new DynamicAttributeVO(null, null, documentFileUID, newFile);
					final CollectableGenericObjectAttributeField clctf = new CollectableGenericObjectAttributeField(daVO, CollectableField.TYPE_VALUEIDFIELD);
					this.setField(clctf);
				} else {
					final CollectableValueField clctf = new CollectableValueField(newFile);
					this.setField(clctf);
				}
			} catch (Exception ex) {
				Errors.getInstance().showExceptionDialog(this.getJComponent(), ex);
			}
		}
	}

	@Override
	public void visitDragEnter(DropTargetDragEvent dtde) {
	}

	@Override
	public void visitDragExit(DropTargetEvent dte) {
	}

	@Override
	public void visitDragOver(DropTargetDragEvent dtde) {
		if (dtde.getSourceActions() == DnDConstants.ACTION_COPY_OR_MOVE || !isEnabled()) {
			// shouldn't it be.. if ((dtde.getSourceActions() & DnDConstants.ACTION_COPY_OR_MOVE) != 0)  { 
			dtde.rejectDrag();
			return;
		}

		final Transferable trans = dtde.getTransferable();
		final DataFlavor flavors[] = trans.getTransferDataFlavors();

		try {
			for (int i = 0; i < flavors.length; i++) {
				final DataFlavor f = flavors[i];
				if (f.isFlavorJavaFileListType()) {
					final List<File> files = (List<File>) trans.getTransferData(f);
					if (files.size() != 1) {
						dtde.rejectDrag();
					} else {
						dtde.acceptDrag(dtde.getDropAction());
					}
					return;
				}
			}
		} catch (Exception e) {
			// do nothing here
			LOG.warn("visitDragOver fails: " + e);
		}

		for (int i = 0; i < flavors.length; i++) {
			final DataFlavor f = flavors[i];
			try {
				final Object obj = trans.getTransferData(f);
				if (obj instanceof String) {
					String strRow = (String) obj;
					if (strRow.indexOf("Betreff") != -1) {
						dtde.acceptDrag(dtde.getDropAction());
						return;
					}
				}
			} catch (Exception e) {
				// do nothing here
				LOG.warn("visitDragOver fails on Betreff to " + f + ": " + e);
			}
		}
		dtde.rejectDrag();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void visitDrop(DropTargetDropEvent dtde) {
		try {
			if (dtde.getSourceActions() == DnDConstants.ACTION_COPY_OR_MOVE) {
				// shouldn't it be.. if ((dtde.getSourceActions() & DnDConstants.ACTION_COPY_OR_MOVE) != 0)  {
				dtde.rejectDrop();
				return;
			}

			final Transferable trans = dtde.getTransferable();
			final DataFlavor flavors[] = trans.getTransferDataFlavors();

			for (int i = 0; i < flavors.length; i++) {
				final DataFlavor f = flavors[i];
				if (f.isFlavorJavaFileListType()) {
					dtde.acceptDrop(dtde.getDropAction());

					final List<File> files = (List<File>) trans.getTransferData(f);
					for (File file : files) {
						if (file != null) {
							loadFile(file);
							dtde.dropComplete(true);
							return;
						}
					}
				}
			}

			final List<File> lstFile = DragAndDropUtils.mailHandlingWithJacob();
			if (lstFile.size() == 1) {
				loadFile(lstFile.get(0));
			}
			else {
				throw new PointerException(SpringLocaleDelegate.getInstance().getMessage(
						"CollectableDocumentFileChooserBase.5",
						"Das Feld " + getField() + " kann nur eine EMail aufnehmen.", getFieldUID()));
			}
		} catch (PointerException e) {
			LOG.warn("visitDrop fails with PointerException: " + e);
			Bubble bubble = new Bubble(CollectableDocumentFileChooserBase.this.getControlComponent(),
					SpringLocaleDelegate.getInstance().getMessage(
							"details.subform.controller.2",
							"Diese Funktion wird nur unter Microsoft Windows unterstützt!"),
					5, BubbleUtils.Position.NE);
			bubble.setVisible(true);
		} catch (Exception e) {
			// ignore this
			LOG.warn("visitDrop fails: " + e);
		}
	}

	@Override
	public void visitDropActionChanged(DropTargetDragEvent dtde) {
	}

}
