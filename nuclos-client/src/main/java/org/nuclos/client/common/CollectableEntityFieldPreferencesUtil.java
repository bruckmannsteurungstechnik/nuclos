package org.nuclos.client.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;

import org.apache.log4j.Logger;
import org.nuclos.client.attribute.AttributeCache;
import org.nuclos.client.genericobject.CollectableGenericObjectEntity;
import org.nuclos.client.genericobject.GenericObjectMetaDataCache;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.common.CollectableEntityFieldWithEntity;
import org.nuclos.common.CollectableEntityFieldWithEntityForExternal;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableEntityFieldPref;
import org.nuclos.common.collect.collectable.CollectableEntityPref;
import org.nuclos.common.entityobject.CollectableEOEntity;
import org.nuclos.common.entityobject.CollectableEOEntityField;
import org.nuclos.common.genericobject.CollectableGenericObjectEntityField;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;
import org.nuclos.common.masterdata.CollectableMasterDataForeignKeyEntityField;
import org.nuclos.common.preferences.PreferencesUtils;
import org.nuclos.common.security.Permission;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.PreferencesException;

public class CollectableEntityFieldPreferencesUtil {
	
	private static final Logger LOG = Logger.getLogger(CollectableEntityFieldPreferencesUtil.class);
	
	private CollectableEntityFieldPreferencesUtil(IMetaProvider mdProv) {
		// Never invoked.
	}
	
	public static List<CollectableEntityField> readList(Preferences prefs, String node, boolean ignoreErrors) throws PreferencesException {
		final List<?> raw = PreferencesUtils.getSerializableListXML(prefs, node, ignoreErrors);
		final List<CollectableEntityField> result = new ArrayList<CollectableEntityField>(raw.size());
		// backward hack
		for (Object p: raw) {
			if (p instanceof CollectableEntityFieldPref) {
				final CollectableEntityFieldPref pref = (CollectableEntityFieldPref) p;
				try {
					result.add(fromPref(pref));
				}
				catch (PreferencesException e) {
					if (ignoreErrors) {
						LOG.warn("readList: fromPref fails on " + pref + ", " + e, e);
					}
					else {
						throw e;
					}
				}
				catch (CommonFatalException e) {
					if (ignoreErrors) {
						LOG.warn("readList: fromPref fails on " + pref + ", " + e, e);
					}
					else {
						throw new PreferencesException("readList: fromPref fails on " + pref + " reason: " + e);
					}
				}
			}
			// compatibility case
			else if (p instanceof CollectableEntityField) {
				result.add((CollectableEntityField) p);
			}
		}
		return result;
	}
	
	public static void writeList(Preferences prefs, String node, List<CollectableEntityField> fields) throws PreferencesException {
		final List<CollectableEntityFieldPref> converted = new ArrayList<CollectableEntityFieldPref>(fields.size());
		for (CollectableEntityField f: fields) {
			final CollectableEntityFieldPref conv = toPref(f);
			converted.add(conv);
		}
		PreferencesUtils.putSerializableListXML(prefs, node, converted);
	}
	
	public static CollectableEntityField fromPref(CollectableEntityFieldPref p) throws PreferencesException {
		final MetaProvider mdProv = MetaProvider.getInstance();
		final CollectableEntityField result;
		if (CollectableEOEntityField.class.getName().equals(p.getType())) {
			final FieldMeta<?> efMeta = mdProv.getEntityField(p.getField());
			result = new CollectableEOEntityField(efMeta);
		}
		else if (CollectableGenericObjectEntityField.class.getName().equals(p.getType())) {
			FieldMeta<?> fMeta = mdProv.getEntityField(p.getField());
			Map<UID, Permission> mpPermissions = null;
			if (fMeta.isCalcAttributeAllowCustomization()) {
				mpPermissions = AttributeCache.getInstance().getAttribute(LangUtils.defaultIfNull(fMeta.getCalcBaseFieldUID(), fMeta.getUID())).getPermissions();
			} else {
				mpPermissions = AttributeCache.getInstance().getAttribute(p.getField()).getPermissions();
			}
			result = new CollectableGenericObjectEntityField(mpPermissions, fMeta, p.getEntity());
		}
		else if (CollectableMasterDataForeignKeyEntityField.class.getName().equals(p.getType())) {
			result = new CollectableMasterDataForeignKeyEntityField(
					MasterDataDelegate.getInstance().getMetaData(p.getEntity()).getField(p.getField()));			
		}
		/*
		else if (DefaultCollectableEntityField.class.getName().equals(p.getType())) {
			result = new DefaultCollectableEntityField(p.getField(), clctef.getJavaClass(), clctef.getLabel(), clctef.getDescription(), clctef.getMaxLength(),
					clctef.getPrecision(), clctef.isNullable(), clctef.getFieldType(), clctef.getReferencedEntityName(),
					clctef.getDefault(), clctef.getFormatInput(), clctef.getFormatOutput(), p.getEntity());	
		}
		 */
		else if (CollectableEntityFieldWithEntityForExternal.class.getName().equals(p.getType())) {
			final CollectableEntityPref cep = p.getCollectableEntity();
			if (cep == null) {
				throw new PreferencesException("No CollectableEntityPref for CollectableEntityFieldWithEntityForExternal: " + p);
			}
			final CollectableEntity ce = mkCe(cep);
			result = new CollectableEntityFieldWithEntityForExternal(ce, p.getField(), p.getBelongsToSubEntity(), p.getBelongsToMainEntity());
		}
		// probably not needed (tp)
		else if (CollectableEntityFieldWithEntity.class.getName().equals(p.getType())) {
			final CollectableEntityPref cep = p.getCollectableEntity();
			if (cep == null) {
				throw new PreferencesException("No CollectableEntityPref for CollectableEntityFieldWithEntityForExternal: " + p);
			}
			final CollectableEntity ce = mkCe(cep);
			result = new CollectableEntityFieldWithEntity(ce, p.getField());
		}
		else {
			throw new PreferencesException("Unknown CollectableEntityField of type " + p.getType());
		}
		return result;
	}
	
	private static CollectableEntity mkCe(CollectableEntityPref cep) throws PreferencesException {
		final IMetaProvider mdProv = MetaProvider.getInstance();
		final CollectableEntity ce;
		if (CollectableEOEntity.class.getName().equals(cep.getType())) {
			ce = new CollectableEOEntity(mdProv.getEntity(cep.getEntity()));
		}
		else if (CollectableGenericObjectEntity.class.getName().equals(cep.getType())) {
			final EntityMeta<?> ceoe = mdProv.getEntity(cep.getEntity());
			
			final UID moduleUid = ceoe.getUID();
			final Modules modules = Modules.getInstance();
			final String sLabel = modules.getLabel(moduleUid);
			
			// Note that only attributes occuring in Details layouts are taken into account for building the entity:
			/* the collection from the AttributeCache must not be modified. A new one is created instead */
			final Collection<UID> collFields = new HashSet<UID>(
					GenericObjectMetaDataCache.getInstance().getAttributeUids(moduleUid, Boolean.FALSE));
			ce = new CollectableGenericObjectEntity(moduleUid, sLabel, collFields);
		}
		/*
		else if (CollectableGenericObjectEntityForAllAttributes.class.getName().equals(cep.getType())) {	
		}
		 */
		else if (CollectableMasterDataEntity.class.getName().equals(cep.getType())) {
			EntityMeta<?> metaData = MasterDataDelegate.getInstance().getMetaData(cep.getEntity());
			ce = new CollectableMasterDataEntity(metaData);			
		}
		else if (DoNotUseCollectableEntity.class.getName().equals(cep.getType())) {
			final EntityMeta<?> mdEntity = mdProv.getEntity(cep.getEntity());
			ce = new DoNotUseCollectableEntity(cep.getEntity(), SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(mdEntity));
		}
		else {
			throw new PreferencesException("Unknown CollectableEntity of type " + cep.getType());
		}
		return ce;
	}
	
	public static CollectableEntityFieldPref toPref(CollectableEntityField f) throws PreferencesException {
		final CollectableEntityFieldPref result;
		if (f instanceof CollectableEOEntityField) {
			result = toPref((CollectableEOEntityField) f);
		}
		else if (f instanceof CollectableGenericObjectEntityField) {
			result = toPref((CollectableGenericObjectEntityField) f);
		}
		else if (f instanceof CollectableMasterDataForeignKeyEntityField) {
			result = toPref((CollectableMasterDataForeignKeyEntityField) f);
		}
		else if (f instanceof CollectableEntityFieldWithEntityForExternal) {
			result = toPref((CollectableEntityFieldWithEntityForExternal) f);
		}
		// probably not needed (tp)
		else if (f instanceof CollectableEntityFieldWithEntity) {
			result = toPref((CollectableEntityFieldWithEntity) f);
		}
		else {
			throw new PreferencesException("Unknown CollectableEntityField of type " + f);
		}
		return result;
	}
	
	public static CollectableEntityFieldPref toPref(CollectableEOEntityField f) throws PreferencesException {
		final CollectableEntityFieldPref p = new CollectableEntityFieldPref(
				CollectableEOEntityField.class.getName(), f.getEntityUID(), f.getUID());
		return p;
	}
	
	public static CollectableEntityFieldPref toPref(CollectableGenericObjectEntityField f) throws PreferencesException {
		final CollectableEntityFieldPref p = new CollectableEntityFieldPref(
				CollectableGenericObjectEntityField.class.getName(), f.getEntityUID(), f.getUID());
		return p;
	}
	
	public static CollectableEntityFieldPref toPref(CollectableMasterDataForeignKeyEntityField f) throws PreferencesException {
		final CollectableEntityFieldPref p = new CollectableEntityFieldPref(
				CollectableMasterDataForeignKeyEntityField.class.getName(), f.getEntityUID(), f.getUID());
		return p;
	}
	
	/*
	public static CollectableEntityFieldPref toPref(DefaultCollectableEntityField f) throws PreferencesException {
		final CollectableEntityFieldPref p = new CollectableEntityFieldPref(
				DefaultCollectableEntityField.class.getName(), f.getEntityName(), f.getName(), null);
		return p;
	}
	 */
	
	public static CollectableEntityFieldPref toPref(CollectableEntityFieldWithEntityForExternal f) throws PreferencesException {
		final CollectableEntity ce = f.getCollectableEntity();
		final CollectableEntityPref cep = new CollectableEntityPref(ce.getClass().getName(), ce.getUID());
		final CollectableEntityFieldPref p = new CollectableEntityFieldPref(
				CollectableEntityFieldWithEntityForExternal.class.getName(), cep, f.getUID(), 
				f.fieldBelongsToSubEntity(), f.fieldBelongsToMainEntity());
		return p;
	}
	
	public static CollectableEntityFieldPref toPref(CollectableEntityFieldWithEntity f) throws PreferencesException {
		final CollectableEntity ce = f.getCollectableEntity();
		final CollectableEntityPref cep = new CollectableEntityPref(ce.getClass().getName(), ce.getUID());
		final CollectableEntityFieldPref p = new CollectableEntityFieldPref(
				CollectableEntityFieldWithEntity.class.getName(), cep, f.getUID(),
				// ???
				false, false);
		return p;
	}
	
}
