//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Created on 23.05.2005
 *
 */
package org.nuclos.client.common;

import java.util.Collection;
import java.util.Comparator;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nuclos.client.main.Main;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.masterdata.MetaDataDelegate;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.LockMode;
import org.nuclos.common.NuclosScript;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.UnlockMode;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

public class EntityUtils {	
	
	private static final Logger LOG = Logger.getLogger(EntityUtils.class);
	
	private static class UIDWrapper implements org.nuclos.api.UID, Comparable {
		private final UID uid;
		private final String label;
		
		public UIDWrapper(UID uid, String label) {
			this.uid = uid;
			this.label = label;
		}
		
		public String getString() {
			return uid.getString();
		}
		public String getStringifiedDefinition() {
			return uid.getStringifiedDefinition();
		}
		public String getStringifiedDefinitionWithEntity(EntityMeta<?> entity) {
			return uid.getStringifiedDefinitionWithEntity(entity);
		}
		public String getStringifiedDefinitionWithEntity(UID entityUID) {
			return uid.getStringifiedDefinitionWithEntity(entityUID);
		}
		
		public int hashCode() {
			return uid.hashCode();
		}
		public boolean equals(Object that) {
			try {
				return (uid == null) ? (that == null) : uid.equals(that);	
			} catch (IllegalArgumentException e) {
				return false;
			}
		}
		
		public String toString() {
			return label;
		}

		@Override
		public int compareTo(Object o) {
			if (o == null)
				return (uid == null) ? 0 : -1;
			if (!(o instanceof UIDWrapper))
				return -1;
			return StringUtils.compareIgnoreCase(label, ((UIDWrapper)o).label);
		}
		
	}
	
	private static class EntityMetaWrapper<PK> extends org.nuclos.common.EntityMeta<PK> {
		private final EntityMeta<PK> meta;
		
		public EntityMetaWrapper(EntityMeta<PK> meta) {
			this.meta = meta;
		}

		@Override
		public UID getUID() {
			return meta.getUID();
		}
		@Override
		public String getDbTable() {
			return meta.getDbTable();
		}
		@Override
		public String getEntityName() {
			return meta.getEntityName();
		}
		@Override
		public Class<PK> getPkClass() {
			return meta.getPkClass();
		}	
		
		@Override
		public UID getNuclet() {
			return meta.getNuclet();
		}
		@Override
		public boolean isUidEntity() {
			return meta.isUidEntity();
		}
		@Override
		public boolean isSearchable() {
			return meta.isSearchable();
		}
		@Override
		public boolean isEditable() {
			return meta.isEditable();
		}
		@Override
		public boolean isCacheable() {
			return meta.isCacheable();
		}
		@Override
		public boolean isImportExport() {
			return meta.isImportExport();
		}
		@Override
		public boolean isDynamic() {
			return meta.isDynamic();
	    }
		@Override
		public boolean isChart() {
			return meta.isChart();
	    }
		@Override
		public boolean isFieldValueEntity() {
			return meta.isFieldValueEntity();
		}
		@Override
		public boolean isLogBookTracking() {
			return meta.isLogBookTracking();
		}
		@Override
		public boolean isStateModel() {
			return meta.isStateModel();
		}
		@Override
		public boolean isTreeRelation() {
			return meta.isTreeRelation();
		}
		@Override
		public boolean isTreeGroup() {
			return meta.isTreeGroup();
		}
		@Override
		public boolean isTableMaster() {
			return meta.isTableMaster();
		}
		@Override
		public boolean isShowSearch() {
			return meta.isShowSearch();
		}
		@Override
		public boolean isThin() {
			return meta.isThin();
		}
		@Override
		public boolean isGeneric() {
			return meta.isGeneric();
		}
		@Override
		public String getNuclosResource() {
			return meta.getNuclosResource();
		}
		@Override
		public UID getResource() {
			return meta.getResource();
		}
		@Override
		public String getAccelerator() {
			return meta.getAccelerator();
		}
		@Override
		public Integer getAcceleratorModifier() {
			return meta.getAcceleratorModifier();
		}
		@Override
		public UID[] getFieldsForEquality() {
			return meta.getFieldsForEquality();
		}
		@Override
		public String getLocaleResourceIdForLabel() {
			return meta.getLocaleResourceIdForLabel();
		}
		@Override
		public String getLocaleResourceIdForMenuPath() {
			return meta.getLocaleResourceIdForMenuPath();
		}
		@Override
		public String getLocaleResourceIdForDescription() {
			return meta.getLocaleResourceIdForDescription();
		}
		@Override
		public String getLocaleResourceIdForTreeView() {
			return meta.getLocaleResourceIdForTreeView();
		}
		@Override
		public String getLocaleResourceIdForTreeViewDescription() {
			return meta.getLocaleResourceIdForTreeViewDescription();
		}
		@Override
		public String getSystemIdPrefix() {
			return meta.getSystemIdPrefix();
		}
		@Override
		public String getIdFactory() {
			return meta.getIdFactory();
		}
		@Override
		public UID[][] getUniqueFieldCombinations() {
			return meta.getUniqueFieldCombinations();
		}
		@Override
		public UID[][] getLogicalUniqueFieldCombinations() {
			return meta.getLogicalUniqueFieldCombinations();
		}
		@Override
		public UID[][] getIndexFieldCombinations() {
			return meta.getIndexFieldCombinations();
		}
		@Override
		public String getMenuShortcut() {
			return meta.getMenuShortcut();
		}
		@Override
		public String getReadDelegate() {
			return meta.getReadDelegate();
		}
		@Override
		public NuclosScript getRowColorScript() {
			return meta.getRowColorScript();
		}
		@Override
		public String getVirtualEntity() {
			return meta.getVirtualEntity();
		}
		@Override
		public UID getCloneGenerator() {
			return meta.getCloneGenerator();
		}
		@Override
		public String getDbSelect() {
			return meta.getDbSelect();
		}
		@Override
		public boolean checkEntityUID(UID entityUID) {
			return meta.checkEntityUID(entityUID);
		}
		@Override
		public Collection<FieldMeta<?>> getFields() {
			return meta.getFields();
		}		
		@Override
		public FieldMeta<?> getField(final UID fieldUid) {
			return meta.getField(fieldUid);
		}
		@Override
		public SF<PK> getPk() {
			return meta.getPk();
		}
		@Override
		public boolean isProxy() {
			return meta.isProxy();
		}
		@Override
		public UID getMandatorLevel() {
			return meta.getMandatorLevel();
		}
		@Override
		public boolean isMandatorUnique() {
			return meta.isMandatorUnique();
		}
		@Override
		public String getDataLangRefPath() {
			return meta.getDataLangRefPath();
		}
		
		@Override
		public boolean isResultdetailssplitview() {
			return meta.isResultdetailssplitview();
		}

		@Override
		public String getComment() {
			return meta.getComment();
		}
		
		@Override
		public LockMode getLockMode() {
			return meta.getLockMode();
		}
		@Override
		public String getOwnerForeignEntityField() {
			return meta.getOwnerForeignEntityField();
		}
		@Override
		public UnlockMode getUnlockMode() {
			return meta.getUnlockMode();
		}

		@Override
		public String toString() {
			return meta.getEntityName();
		}	
		
		@Override
		public int hashCode() {
			return meta.hashCode();
		}
		@Override
		public boolean equals(Object that) {
			try {
				return (meta == null) ? (that == null) : (that == null ? false : LangUtils.equal(meta, that));
			} catch (IllegalArgumentException e) {
				return false;
			}
		}
	}
	
	private static class FieldMetaWrapper<PK> extends org.nuclos.common.FieldMeta<PK> {
		private final FieldMeta<PK> meta;
		
		public FieldMetaWrapper(FieldMeta<PK> meta) {
			this.meta = meta;
		}

		@Override
		public UID getUID() {
			return meta.getUID();
		}
		@Override
		public UID getEntity() {
			return meta.getEntity();
		}
		@Override
		public String getFieldName() {
			return meta.getFieldName();
		}
		@Override
		public String getDbColumn() {
			return meta.getDbColumn();
		}
		@Override
		public Class<PK> getJavaClass() {
			return meta.getJavaClass();
		}
		
		@Override
		public String getDataType() {
			return meta.getDataType();
		}
		@Override
		public Integer getScale() {
			return meta.getScale();
		}
		@Override
		public Integer getPrecision() {
			return meta.getPrecision();
		}
		@Override
		public UID getForeignEntity() {
			return meta.getForeignEntity();
		}
		@Override
		public String getForeignEntityField() {
			return meta.getForeignEntityField();
		}
		@Override
		public UID getUnreferencedForeignEntity() {
			return meta.getUnreferencedForeignEntity();
		}
		@Override
		public String getUnreferencedForeignEntityField() {
			return meta.getUnreferencedForeignEntityField();
		}
		@Override
		public UID getLookupEntity() {
			return meta.getLookupEntity();
		}
		@Override
		public String getLookupEntityField() {
			return meta.getLookupEntityField();
		}
		@Override
		public UID getFieldGroup() {
			return meta.getFieldGroup();
		}
		@Override
		public String getFallbackLabel() {
			return meta.getFallbackLabel();
		}
		@Override
		public String getFormatInput() {
			return meta.getFormatInput();
		}
		@Override
		public String getFormatOutput() {
			return meta.getFormatOutput();
		}
		@Override
		public Long getDefaultForeignId() {
			return meta.getDefaultForeignId();
		}
		@Override
		public UID getDefaultForeignUid() {
			return meta.getDefaultForeignUid();
		}
		@Override
		public String getDefaultValue() {
			return meta.getDefaultValue();
		}
		@Override
		public String getLocaleResourceIdForDescription() {
			return meta.getLocaleResourceIdForDescription();
		}
		@Override
		public String getLocaleResourceIdForLabel() {
			return meta.getLocaleResourceIdForLabel();
		}
		@Override
		public String getCalcFunction() {
			return meta.getCalcFunction();
		}
		@Override
		public UID getCalcAttributeDS() {
			return meta.getCalcAttributeDS();
		}
		@Override
		public String getCalcAttributeParamValues() {
			return meta.getCalcAttributeParamValues();
		}
		@Override
		public boolean isCalcAttributeAllowCustomization() {
			return meta.isCalcAttributeAllowCustomization();
		}
		@Override
		public boolean isCalcOndemand() {
			return meta.isCalcOndemand();
		}
		@Override
		public NuclosScript getCalculationScript() {
			return meta.getCalculationScript();
		}
		@Override
		public String getSortorderASC() {
			return meta.getSortorderASC();
		}
		@Override
		public String getSortorderDESC() {
			return meta.getSortorderDESC();
		}
		@Override
		public String getDefaultMandatory() {
			return meta.getDefaultMandatory();
		}
		@Override
		public boolean isReadonly() {
			return meta.isReadonly();
		}
		@Override
		public boolean isUnique() {
			return meta.isUnique();
		}
		@Override
		public boolean isNullable() {
			return meta.isNullable();
		}
		@Override
		public boolean isSearchable() {
			return meta.isSearchable();
		}
		@Override
		public boolean isModifiable() {
			return meta.isModifiable();
		}
		@Override
		public boolean isInsertable() {
			return meta.isInsertable();
		}
		@Override
		public boolean isLogBookTracking() {
			return meta.isLogBookTracking();
		}
		@Override
		public boolean isShowMnemonic() {
			return meta.isShowMnemonic();
		}
		@Override
		public boolean isDynamic() {
			return meta.isDynamic();
		}
		@Override
		public boolean isIndexed() {
			return meta.isIndexed();
		}
		@Override
		public boolean isPermissionTransfer() {
			return meta.isPermissionTransfer();
		}
		@Override
		public boolean isOnDeleteCascade() {
			return meta.isOnDeleteCascade();
		}
		@Override
		public boolean isResourceField() {
			return meta.isResourceField();
		}
		@Override
		public boolean isInvariant() {
			return meta.isInvariant();
		}
		@Override
		public Integer getOrder() {
			return meta.getOrder();
		}
		@Override
		public String getDefaultComponentType() {
			return meta.getDefaultComponentType();
		}
		@Override
		public String getSearchField() {
			return meta.getSearchField();
		}
		@Override
		public NuclosScript getBackgroundColorScript() {
			return meta.getBackgroundColorScript();
		}
		@Override
		public UID getAutonumberEntity() {
			return meta.getAutonumberEntity();
		}
		@Override
		public Boolean isLocalized() {
			return meta.isLocalized();
		}

		@Override
		public String toString() {
			return meta.getFieldName();
		}
		
		@Override
		public boolean equals(Object that) {
			return (meta == null) ? (that == null) : (that == null ? false : LangUtils.equal(meta, that));
		}
		@Override
	    public int hashCode() {
			return meta.hashCode();
	    }
	}
	
	public static org.nuclos.api.UID wrapUID(UID uid, String label) {
		return new UIDWrapper(uid, label);
	}
	public static Comparator<org.nuclos.api.UID> getUIDComparator() {
		return new Comparator<org.nuclos.api.UID>() {
			@Override
			public int compare(org.nuclos.api.UID o1, org.nuclos.api.UID o2) {
				return LangUtils.compare(o1, o2);
			}
		};
	}
	
	public static <T> T wrapMetaData(T meta) {
		if (meta == null)
			throw new IllegalArgumentException("meta can not be null.");
		
		if (meta instanceof EntityMeta<?>)
			return (T)new EntityMetaWrapper((EntityMeta<?>)meta);
		if (meta instanceof FieldMeta<?>)
			return (T)new FieldMetaWrapper((FieldMeta<?>)meta);

		throw new IllegalArgumentException("no wrapper found for " + meta.getClass());
	}	
	public static <T> Comparator<T> getMetaComparator(Class<T> meta) {
		return new Comparator<T>() {
			@Override
			public int compare(T o1, T o2) {
				return LangUtils.compare(wrapMetaData(o1), wrapMetaData(o2));
			}
		};
	}
	
	public static boolean hasEntityDocumentType(UID entity) {
		final Map<UID, FieldMeta<?>> mpFields = MetaProvider.getInstance().getAllEntityFieldsByEntity(entity);
		for(UID sField : mpFields.keySet()) {
			if(GenericObjectDocumentFile.class.getName().equals(mpFields.get(sField).getDataType())) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean hasEntityGeneralDocumentSubform(UID entity) {
		final CollectableComparison compare = SearchConditionUtils.newComparison(E.LAYOUTUSAGE.getUID(), ComparisonOperator.EQUAL, entity);
		for(MasterDataVO<?> layout : MasterDataDelegate.getInstance().getMasterData(E.LAYOUTUSAGE.getUID(), compare)) {
			UID iLayoutId = layout.getFieldUid(E.LAYOUTUSAGE.layout);
			MasterDataVO<?> voLayout;
			try {
				voLayout = MasterDataDelegate.getInstance().get(E.LAYOUT.getUID(), iLayoutId);
				String sLayout = (String)voLayout.getFieldValue(E.LAYOUT.layoutML);
				if(sLayout.indexOf("entity=\"nuclos_generalsearchdocument\"") >= 0) 
					return true;
			}
			catch(CommonFinderException e) {
				LOG.warn("hasEntityGeneralDocumentSubform failed: " + e, e);
			}
			catch(CommonPermissionException e) {
				LOG.warn("hasEntityGeneralDocumentSubform failed: " + e, e);
			}				
		}
		return false;
	}
	
	public static boolean hasAnySubformDocumentType(UID entity) {
    	for(EntityMeta<?> voEntity : MetaProvider.getInstance().getAllEntities()) {
			if(voEntity.getUID().equals(entity))
				continue;
			
			for(FieldMeta<?> voField : MetaProvider.getInstance().getAllEntityFieldsByEntity(voEntity.getUID()).values()) {
				if(voField.getForeignEntity() != null && voField.getForeignEntity().equals(entity) && voField.getForeignEntityField() == null) { 
					return hasEntityDocumentType(voEntity.getUID());
				}
			}
		}
		return false;
	}
	
	/**
	 * Structural change in entity meta data -> clear all caches and 
	 * refresh menu. (tp)
	 * 
	 * @see http://support.nuclos.de/browse/NUCLOS-3544
	 */
	public static void clearAllEntityCaches() {
		MasterDataCache.getInstance().invalidate(E.LAYOUT.getUID());
		MasterDataCache.getInstance().invalidate(E.LAYOUTUSAGE.getUID());
		MasterDataDelegate.getInstance().invalidateCaches();
		MasterDataDelegate.getInstance().invalidateLayoutCache();

		MetaProvider.getInstance().revalidate();
		MetaDataDelegate.getInstance().invalidateServerMetadata();
		MasterDataCache.getInstance().invalidate(E.ENTITY.getUID());
		
		// NUCLOS-3544
		Main.getInstance().getMainController().setupMenuBar();
	}

}
