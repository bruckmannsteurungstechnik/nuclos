package org.nuclos.client.common;

import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;

public interface ICmdGenerateObject {

	void cmdGenerateObject(GeneratorActionVO generatoractionvo);

}
