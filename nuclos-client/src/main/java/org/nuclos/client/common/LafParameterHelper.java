//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.common;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import org.apache.log4j.Logger;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.toolbar.editor.ToolBarConfigurationEditor;
import org.nuclos.client.ui.util.ITableLayoutBuilder;
import org.nuclos.client.ui.util.TableLayoutBuilder;
import org.nuclos.common.LafParameter;
import org.nuclos.common.LafParameterStorage;
import org.nuclos.common.SplitViewSettings;
import org.nuclos.common.UID;
import org.nuclos.common.collect.NuclosDetailToolBarConfiguration;
import org.nuclos.common.collect.NuclosDetailsSubformToolBarConfiguration;
import org.nuclos.common.collect.NuclosResultDetailsSplitToolBarConfiguration;
import org.nuclos.common.collect.NuclosResultToolBarConfiguration;
import org.nuclos.common.collect.NuclosSearchSubformToolBarConfiguration;
import org.nuclos.common.collect.NuclosSearchToolBarConfiguration;
import org.nuclos.common.collect.ToolBarConfiguration;
import org.nuclos.common.collect.ToolBarConfiguration.Context;
import org.nuclos.common.collection.Pair;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;

import info.clearthought.layout.TableLayout;

public class LafParameterHelper<T,S> {
	
	private static final Logger LOG = Logger.getLogger(LafParameterHelper.class);
	
	private LafParameterHelper() {
		// Never invoked.
	}
	
	public static interface NotifyListener<T2,S2> {
		void valueChanged(LafParameter<T2,S2> parameter, LafParameterStorage storage, UID entityUid, T2 value);
	}
	
	public static <T2,S2> void installPopup(AbstractLafParameterEditor<T2, S2> editor) {
		installPopup(editor, null);
	}
	
	public static <T2,S2> void installPopup(AbstractLafParameterEditor<T2, S2> editor, NotifyListener<T2,S2> notifyListener) {
		final JComponent c = editor.getParent();
		final LafParameter<T2,S2> parameter = editor.getLafParameter();
		final UID entityUid = editor.getEntityUid();
		boolean changeAllowed = false;
		for (LafParameterStorage storage : LafParameterStorage.values()) {
			if (LafParameterProvider.getInstance().isStorageAllowed(parameter, storage, entityUid)) {
				changeAllowed = true;
			}
		}
		
		if (changeAllowed) {
			JPopupMenu pm = c.getComponentPopupMenu();
			if (pm == null) {
				pm = new JPopupMenu();
				c.setComponentPopupMenu(pm);
			}
			pm.add(new LafParameterAction<T2,S2>(editor, parameter, entityUid, c, notifyListener));
		}
	}
	
	@SuppressWarnings("serial")
	private static class LafParameterAction<T2,S2> extends AbstractAction {
		
		private final AbstractLafParameterEditor<T2, S2> editor;
		
		private final JComponent parent;
		
		private final LafParameter<T2,S2> parameter;
		
		private final UID entityUid;
		
		private final WeakReference<NotifyListener<T2,S2>> notifyListenerReference;
		
		public LafParameterAction(AbstractLafParameterEditor<T2, S2> editor, LafParameter<T2,S2> parameter, 
				UID entityUid, JComponent parent, NotifyListener<T2,S2> notifyListener) {
			
			super(String.format(SpringLocaleDelegate.getInstance().getResource("LafParameterAction.1", "Parameter \"%s\" ändern"), 
					SpringLocaleDelegate.getInstance().getResource(parameter.getName(), parameter.getName())));
			this.editor = editor;
			this.parameter = parameter;
			this.entityUid = entityUid;
			this.parent = parent;
			this.notifyListenerReference = new WeakReference<NotifyListener<T2,S2>>(notifyListener);
		}

		@Override
		public void actionPerformed(ActionEvent event) {
			editor.run();
			if (editor.okay()) {
				editor.changedValues.put(editor.storage, editor.getValueFromEditor());
				editor.changedValues.forEach((storage, sValue) -> {
					LafParameterProvider.getInstance().setValue(parameter, entityUid, storage, sValue);
					if (notifyListenerReference != null) {
						final NotifyListener<T2,S2> notifyListener = notifyListenerReference.get();
						if (notifyListener != null) {
							/*
							S2 v;
							try {
								v = parameter.convertToStore(sValue);
							} catch (ParseException e) {
								LOG.warn(String.format("Can't convert LafParameter %s (entityUid=%s) value %s: %s",
										vo.getParameter(), vo.getEntity(), vo.getValue(), e.toString()), e);
								v = parameter.getDefault();
							} catch (Exception e) {
								LOG.warn(String.format("Can't read LafParameter %s (entityUid=%s) value %s: %s",
										vo.getParameter(), vo.getEntity(), vo.getValue(), e.toString()), e);
								v = parameter.getDefault();
							}
							final T2 value = LafParameterProvider.getInstance().getValue(parameter, entityUid, storage, parameter.parse(sValue));
							 */
							final T2 t = LafParameterProvider.getInstance().getValue(parameter, entityUid, storage);
							notifyListener.valueChanged(parameter, storage, entityUid, t);
						}
					}
				});
			}
		}
		
	}
	
	static abstract class AbstractLafParameterEditor<T2,S2> implements ActionListener {
		
		protected final JComponent parent;
		
		protected final LafParameter<T2,S2> parameter;
		
		protected final UID entityUid;
		
		protected JPanel jpnMain;
		protected JPanel jpnStorage;
		
		private JLabel jlb;
		
		private ButtonGroup bgStorages;
		
		private final Map<LafParameterStorage,JRadioButton> storage2Button = new TreeMap<LafParameterStorage, JRadioButton>();
		private final Map<LafParameterStorage, S2> changedValues = new TreeMap<>();

		protected LafParameterStorage storage;
		private boolean okay = false;
		
		protected AbstractLafParameterEditor(LafParameter<T2,S2> parameter, UID entityUid, JComponent parent) {
			this.parameter = parameter;
			this.entityUid = entityUid;
			this.parent = parent;
			initialize();
		}
		
		private void initialize() {
			final SpringLocaleDelegate sld = SpringLocaleDelegate.getInstance();
			
			jpnMain = new JPanel();
			jpnMain.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
			jpnStorage = new JPanel();
			jpnStorage.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));
			jlb = new JLabel(sld.getResource(parameter.getName(), parameter.getName()) + ":");
			
			TableLayout tbllayMain = new TableLayout(
					new double[] {TableLayout.PREFERRED, 10, TableLayout.PREFERRED, TableLayout.FILL}, 
					new double[] {TableLayout.PREFERRED, TableLayout.FILL, 20, TableLayout.PREFERRED});
			jpnMain.setLayout(tbllayMain);
			ITableLayoutBuilder tbllayStorage = new TableLayoutBuilder(jpnStorage).columns(TableLayout.PREFERRED);
			tbllayStorage.newRow().addLabel(sld.getResource("LafParameterEditor.2", "Wo möchten Sie den Wert ändern?"));
			
			jpnMain.add(jpnStorage, "0,0,0,1,l,t");
			jpnMain.add(new JSeparator(JSeparator.VERTICAL), "1,0,1,1");
			jpnMain.add(jlb, "2,0,2,0,l,t");
			
			
			bgStorages = new ButtonGroup();			
			initStorage(LafParameterStorage.SYSTEMPARAMETER, sld.getResource("LafParameterEditor.4", "Systemparameter"), tbllayStorage);
			initStorage(LafParameterStorage.WORKSPACE, sld.getResource("LafParameterEditor.5", "Arbeitsumgebung"), tbllayStorage);
			initStorage(LafParameterStorage.ENTITY, sld.getResource("LafParameterEditor.6", "Entity"), tbllayStorage);
			init();
			
			jpnMain.add(new JLabel(sld.getResource("LafParameterEditor.3", 
					"Möglicherweise müssen Sie die Maske erneut öffnen oder den Client neu starten, damit die Einstellung wirksam wird!")), 
					"0,3,3,3,l,t");
		}
		
		abstract void init();
		
		public JComponent getParent() {
			return parent;
		}
		
		public LafParameter<T2,S2> getLafParameter() {
			return parameter;
		}
		
		public UID getEntityUid() {
			return entityUid;
		}
		
		public void run() {
			okay = JOptionPane.showConfirmDialog(UIUtils.getFrameForComponent(parent), jpnMain, 
					SpringLocaleDelegate.getInstance().getResource("LafParameterEditor.1", "L&F Parameter ändern"), 
					JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION;
		}
		
		private void initStorage(LafParameterStorage storage, String label, ITableLayoutBuilder tbllayStorage) {
			if (LafParameterProvider.getInstance().isStorageAllowed(parameter, storage, entityUid)) {
				JRadioButton jrb = new JRadioButton(label);
				jrb.setActionCommand(storage.name());
				jrb.addActionListener(this);
				bgStorages.add(jrb);
				tbllayStorage.newRow();
				tbllayStorage.add(jrb);
				storage2Button.put(storage, jrb);
				/*
				if (this.storage == null) {
					jrb.setSelected(true);
					setValueToEditor(storage);
				}
				 */
			}
		}
		
		private boolean okay() {
			return okay;
		}
		
		private LafParameterStorage getSelectedStorage() {
			return storage;
		}
		
		void setSelectedStorage(LafParameterStorage storage) {
			final JRadioButton button = storage2Button.get(storage);
			if (button != null) {
				button.setSelected(true);
				this.storage = storage;
				setValueToEditor(storage);
			}
		}
		
		abstract S2 getValueFromEditor();

		void setValueToEditor(LafParameterStorage storage) {
			setValueToEditor(storage, LafParameterProvider.getInstance().getValue(parameter, entityUid, storage));
		}

		abstract void setValueToEditor(LafParameterStorage storage, T2 value);

		@Override
		public void actionPerformed(ActionEvent e) {
			if (storage != null)
				changedValues.put(storage, getValueFromEditor());
			for (LafParameterStorage storage : LafParameterStorage.values()) {
				if (storage.name().equals(e.getActionCommand())) {
					try {
						if (changedValues.containsKey(storage))
							setValueToEditor(storage, parameter.convertFromStore(changedValues.get(storage)));
						else
							setValueToEditor(storage);
						assert this.storage == storage;
					} catch (ParseException ex) {
						changedValues.remove(storage);
						setValueToEditor(storage);
					}
				}
			}
		}
	}
		
	public static class BoolLafParameterEditor extends AbstractLafParameterEditor<Boolean,String> {
		
		private JComboBox jcbEditor;
		
		public BoolLafParameterEditor(LafParameter<Boolean,String> parameter, UID entityId, JComponent parent) {
			super(parameter, entityId, parent);
		}
		
		@Override
		void init() {
			final SpringLocaleDelegate sld = SpringLocaleDelegate.getInstance();
			if (parameter.getParameterClass() == boolean.class || parameter.getParameterClass() == Boolean.class) {
				jcbEditor = new JComboBox();
				jcbEditor.addItem("");
				jcbEditor.addItem(sld.getResource("LafParameterEditor.7", "Ja"));
				jcbEditor.addItem(sld.getResource("LafParameterEditor.8", "Nein"));
				jpnMain.add(jcbEditor, "2,1,2,1,l,t");
			} else {
				throw new IllegalStateException();
			}
		}
		
		@Override
		String getValueFromEditor() {
			if (parameter.getParameterClass() == boolean.class || parameter.getParameterClass() == Boolean.class) {
				switch (jcbEditor.getSelectedIndex()) {
				case 1:
					return Boolean.TRUE.toString();
				case 2:
					return Boolean.FALSE.toString();
				default:
					return null;
				}
			} else {
				throw new IllegalStateException();
			}
		}
		
		@Override
		void setValueToEditor(LafParameterStorage storage, Boolean value) {
			this.storage = storage;
			if (value == null) {
				jcbEditor.setSelectedIndex(0);
			} else if (value) {
				jcbEditor.setSelectedIndex(1);
			} else {
				jcbEditor.setSelectedIndex(2);
			}
		}

	}
	
	public static abstract class ToolBarLafParameterEditor<T extends ToolBarConfiguration> extends AbstractLafParameterEditor<T,String> {
		
		private ToolBarConfigurationEditor toolBarEditor;
		
		protected ToolBarLafParameterEditor(LafParameter<T,String> parameter, UID entityUid, JComponent parent) {
			super(parameter, entityUid, parent);
		}
		
		protected abstract ToolBarConfigurationEditor createDefaultToolBarConfigurationEditor();
		
		private Pair<T,LafParameterStorage> initFromStorage() {
			T config = null;
			LafParameterStorage storage = LafParameterStorage.SYSTEMPARAMETER;
			for (LafParameterStorage st: LafParameterStorage.values()) {
				if (LafParameterStorage.ENTITY.equals(st) && entityUid == null) {
					continue;
				}
				config = LafParameterProvider.getInstance().getValue(parameter, entityUid, st);
				if (config != null) {
					storage = st;
					break;
				}
			}
			if (config == null) {
				config = (T) getLafParameter().getDefault();
			}
			return new Pair<T,LafParameterStorage>(config, storage);
		}
		
		@Override
		void init() {
			final SpringLocaleDelegate sld = SpringLocaleDelegate.getInstance();
			if (ToolBarConfiguration.class.isAssignableFrom(parameter.getParameterClass())) {
				toolBarEditor = createDefaultToolBarConfigurationEditor();
				final Pair<T,LafParameterStorage> pair = initFromStorage();
				toolBarEditor.setConfiguration(pair.getX(), pair.getY().context(entityUid));
				setSelectedStorage(pair.getY());
				jpnMain.add(toolBarEditor.getComponent(), "2,1,2,1,l,t");
			} else {
				throw new IllegalStateException();
			}
		}
		
		@Override
		String getValueFromEditor() {
			if (ToolBarConfiguration.class.isAssignableFrom(parameter.getParameterClass())) {
				ToolBarConfiguration result = toolBarEditor.getConfiguration();
				if (result == null) {
					return null;
				}
				return result.toString();
			} else {
				throw new IllegalStateException();
			}
		}
		
		@Override
		void setValueToEditor(LafParameterStorage storage, T value) {
			this.storage = storage;
			if (ToolBarConfiguration.class.isAssignableFrom(parameter.getParameterClass())) {
				ToolBarConfiguration.Context context = null;
				switch (storage) {
					case SYSTEMPARAMETER:
					case WORKSPACE:
						context = Context.UNSPECIFIED;
						break;
					case ENTITY:
						context = MetaProvider.getInstance().getEntity(entityUid).isStateModel() ?
								Context.ENTITY_MODULE : Context.ENTITY_MASTERDATA;
				}
				toolBarEditor.setConfiguration(value == null ? parameter.getDefault() : value, context);
			} else {
				throw new IllegalStateException();
			}
		}

	}
	
	public static class NuclosDetailsSubformLafParameterEditor extends ToolBarLafParameterEditor<NuclosDetailsSubformToolBarConfiguration> {
		
		public NuclosDetailsSubformLafParameterEditor(LafParameter<NuclosDetailsSubformToolBarConfiguration,String> parameter, UID entityUid, JComponent parent) {
			super(parameter, entityUid, parent);
		}

		@Override
		protected ToolBarConfigurationEditor createDefaultToolBarConfigurationEditor() {
			return new ToolBarConfigurationEditor() {
				
				@Override
				protected ToolBarConfiguration createEmptyToolBarConfiguration() {
					return new NuclosDetailsSubformToolBarConfiguration();
				}
			};
		}

	}
	
	public static class NuclosDetailLafParameterEditor extends ToolBarLafParameterEditor<NuclosDetailToolBarConfiguration> {
		
		public NuclosDetailLafParameterEditor(LafParameter<NuclosDetailToolBarConfiguration,String> parameter, UID entityId, JComponent parent) {
			super(parameter, entityId, parent);
		}

		@Override
		protected ToolBarConfigurationEditor createDefaultToolBarConfigurationEditor() {
			return new ToolBarConfigurationEditor() {
				
				@Override
				protected ToolBarConfiguration createEmptyToolBarConfiguration() {
					return new NuclosDetailToolBarConfiguration();
				}
			};
		}

	}
	
	public static class NuclosResultDetailsSplitLafParameterEditor extends ToolBarLafParameterEditor<NuclosResultDetailsSplitToolBarConfiguration> {
		
		public NuclosResultDetailsSplitLafParameterEditor(LafParameter<NuclosResultDetailsSplitToolBarConfiguration,String> parameter, UID entityId, JComponent parent) {
			super(parameter, entityId, parent);
		}

		@Override
		protected ToolBarConfigurationEditor createDefaultToolBarConfigurationEditor() {
			return new ToolBarConfigurationEditor() {
				
				@Override
				protected ToolBarConfiguration createEmptyToolBarConfiguration() {
					return new NuclosResultDetailsSplitToolBarConfiguration();
				}
			};
		}

	}
	
	public static class NuclosResultLafParameterEditor extends ToolBarLafParameterEditor<NuclosResultToolBarConfiguration> {
		
		public NuclosResultLafParameterEditor(LafParameter<NuclosResultToolBarConfiguration,String> parameter, UID entityId, JComponent parent) {
			super(parameter, entityId, parent);
		}

		@Override
		protected ToolBarConfigurationEditor createDefaultToolBarConfigurationEditor() {
			return new ToolBarConfigurationEditor() {
				
				@Override
				protected ToolBarConfiguration createEmptyToolBarConfiguration() {
					return new NuclosResultToolBarConfiguration();
				}
			};
		}

	}
	
	public static class NuclosSearchSubformLafParameterEditor extends ToolBarLafParameterEditor<NuclosSearchSubformToolBarConfiguration> {
		
		public NuclosSearchSubformLafParameterEditor(LafParameter<NuclosSearchSubformToolBarConfiguration,String> parameter, UID entityId, JComponent parent) {
			super(parameter, entityId, parent);
		}

		@Override
		protected ToolBarConfigurationEditor createDefaultToolBarConfigurationEditor() {
			return new ToolBarConfigurationEditor() {
				
				@Override
				protected ToolBarConfiguration createEmptyToolBarConfiguration() {
					return new NuclosSearchSubformToolBarConfiguration();
				}
			};
		}

	}
	
	public static class NuclosSearchLafParameterEditor extends ToolBarLafParameterEditor<NuclosSearchToolBarConfiguration> {
		
		public NuclosSearchLafParameterEditor(LafParameter<NuclosSearchToolBarConfiguration,String> parameter, UID entityId, JComponent parent) {
			super(parameter, entityId, parent);
		}

		@Override
		protected ToolBarConfigurationEditor createDefaultToolBarConfigurationEditor() {
			return new ToolBarConfigurationEditor() {
				
				@Override
				protected ToolBarConfiguration createEmptyToolBarConfiguration() {
					return new NuclosSearchToolBarConfiguration();
				}
			};
		}

	}
	
	public static class StringLafParameterEditor extends AbstractLafParameterEditor<String,String> {
		
		private JTextField jtfEditor;
		
		public StringLafParameterEditor(LafParameter<String,String> parameter, UID entityId, JComponent parent) {
			super(parameter, entityId, parent);
		}
		
		@Override
		void init() {
			jtfEditor = new JTextField(40);
			jpnMain.add(jtfEditor, "2,1,2,1,l,t");
		}
		
		@Override
		String getValueFromEditor() {
			String result = jtfEditor.getText();
			return "".equals(result)? null: result;
		}
		
		@Override
		void setValueToEditor(LafParameterStorage storage, String value) {
			this.storage = storage;
			jtfEditor.setText(value == null ? "" : value);
		}

	}
	
	public static class GeneralLafParameterEditor<T> extends AbstractLafParameterEditor<T,String> {
		
		private JTextField jtfEditor;
		
		public GeneralLafParameterEditor(LafParameter<T,String> parameter, UID entityId, JComponent parent) {
			super(parameter, entityId, parent);
		}
		
		@Override
		void init() {
			jtfEditor = new JTextField(40);
			jpnMain.add(jtfEditor, "2,1,2,1,l,t");
		}
		
		@Override
		String getValueFromEditor() {
			String result = jtfEditor.getText();
			return "".equals(result)? null: result;
		}
		
		@Override
		void setValueToEditor(LafParameterStorage storage, T value) {
			this.storage = storage;
			jtfEditor.setText(value == null ? "" : parameter.convertToStore(value));
		}

	}

	public static class FixedStringLafParameterEditor<T> extends AbstractLafParameterEditor<T,String> {

		private JComboBox<String> jcbEditor;

		public FixedStringLafParameterEditor(LafParameter<T,String> parameter, UID entityId, JComponent parent) {
			super(parameter, entityId, parent);
		}
		
		@Override
		void init() {
			final SpringLocaleDelegate sld = SpringLocaleDelegate.getInstance();
			if (parameter.getFixedValueList() != null) {
				jcbEditor = new JComboBox<>();
				jcbEditor.addItem("");
				for (T t: parameter.getFixedValueList()) {
					jcbEditor.addItem(sld.getResource(t.toString(), t.toString()));
				}
				jpnMain.add(jcbEditor, "2,1,2,1,l,t");
			} else {
				throw new IllegalStateException();
			}
		}
		
		@Override
		String getValueFromEditor() {
			int index = jcbEditor.getSelectedIndex();
			if (index == 0) {
				return null;
			} else {
				return parameter.getFixedValueList()[index - 1].toString();
			}
		}
		
		@Override
		void setValueToEditor(LafParameterStorage storage, T value) {
			this.storage = storage;
			if (parameter.getFixedValueList() != null) {
				jcbEditor.setSelectedIndex(0);
				if (value != null) {
					for (int i = 0; i < parameter.getFixedValueList().length; i++) {
						if (value.equals(parameter.getFixedValueList()[i])) {
							jcbEditor.setSelectedIndex(i + 1);
						}
					}
				}
			} else {
				throw new IllegalStateException();
			}
		}

	}
	
	public static class SplitViewSettingsLafParameterEditor<T extends SplitViewSettings> extends AbstractLafParameterEditor<T,String> {
		
		private JComboBox jcbEditor;
		
		private T settings = (T) new SplitViewSettings();
		
		public SplitViewSettingsLafParameterEditor(LafParameter<T, String> parameter, UID entityUid, JComponent parent) {
			super(parameter, entityUid, parent);
		}

		@Override
		void init() {
			final SpringLocaleDelegate sld = SpringLocaleDelegate.getInstance();
			jcbEditor = new JComboBox();
			jcbEditor.addItem("");
			jcbEditor.addItem(sld.getText("fix first"));
			jcbEditor.addItem(sld.getText("fix second"));
			jpnMain.add(jcbEditor, "2,1,2,1,l,t");
		}

		@Override
		String getValueFromEditor() {
			int index = jcbEditor.getSelectedIndex();
			switch (index) {
			case 1:
				settings.setFixSize(1);
				settings.setFixFirstPane();
				break;
			case 2:
				settings.setFixSize(1);
				settings.setFixSecondPane();
				break;
			default:
				settings.setFixSize(SplitViewSettings.NO_FIX_SIZE);
			}
			return settings.toString();
		}

		@Override
		void setValueToEditor(LafParameterStorage storage, T value) {
			this.storage = storage;
			if (value != null) {
				settings = value;
			}
			if (!settings.isFixSize()) {
				jcbEditor.setSelectedIndex(0);
			} else if (settings.isFixFirstPane()) {
				jcbEditor.setSelectedIndex(1);
			} else if (settings.isFixSecondPane()) {
				jcbEditor.setSelectedIndex(2);
			}
		}
		
	}
}
