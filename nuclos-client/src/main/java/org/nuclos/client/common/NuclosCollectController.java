//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.common;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.prefs.Preferences;
import java.util.stream.Collectors;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.NullArgumentException;
import org.apache.log4j.Logger;
import org.nuclos.client.command.MultiThreadedRunnable;
import org.nuclos.client.common.controller.NuclosCollectControllerCommonState;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.datasource.NuclosSearchConditionUtils;
import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels.STATIC_BUTTON;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.MainController;
import org.nuclos.client.main.mainframe.IconResolver;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.main.mainframe.workspace.ITabStoreController;
import org.nuclos.client.main.mainframe.workspace.TabRestoreController;
import org.nuclos.client.masterdata.CollectableWithDependants;
import org.nuclos.client.rule.server.EventSupportDelegate;
import org.nuclos.client.searchfilter.EntitySearchFilter;
import org.nuclos.client.searchfilter.EntitySearchFilters;
import org.nuclos.client.searchfilter.SaveFilterController;
import org.nuclos.client.searchfilter.SearchFilter;
import org.nuclos.client.searchfilter.SearchFilterCache;
import org.nuclos.client.ui.CommonAbstractAction;
import org.nuclos.client.ui.DefaultSelectObjectsPanel;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.JInfoTabbedPane;
import org.nuclos.client.ui.SelectObjectsController;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.CollectActionAdapter;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.client.ui.collect.CollectPanel;
import org.nuclos.client.ui.collect.CollectState;
import org.nuclos.client.ui.collect.CollectStateAdapter;
import org.nuclos.client.ui.collect.CollectStateConstants;
import org.nuclos.client.ui.collect.CollectStateEvent;
import org.nuclos.client.ui.collect.RunCustomRuleSelectedCollectablesController;
import org.nuclos.client.ui.collect.detail.DetailsPanel;
import org.nuclos.client.ui.collect.result.NuclosResultController;
import org.nuclos.client.ui.collect.result.NuclosSearchResultStrategy;
import org.nuclos.client.ui.collect.result.ResultController;
import org.nuclos.client.ui.collect.search.ISearchStrategy;
import org.nuclos.client.ui.collect.toolbar.NuclosToolBarActions;
import org.nuclos.client.ui.collect.toolbar.ToolBarItemAction;
import org.nuclos.client.ui.layer.LayerLock;
import org.nuclos.client.ui.layer.LockingResultListener;
import org.nuclos.client.ui.layoutml.LayoutMLButtonActionListener;
import org.nuclos.client.ui.model.ChoiceList;
import org.nuclos.client.ui.profile.ProfileModel;
import org.nuclos.common.Actions;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.common.WorkspaceDescription2.EntityPreferences;
import org.nuclos.common.collect.ControllerPresentation;
import org.nuclos.common.collect.NuclosToolBarItems;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.SearchConditionUtils;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common.collect.exception.CollectableValidationException;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.preferences.PreferencesUtils;
import org.nuclos.common2.ClientPreferences;
import org.nuclos.common2.CloseableXStream;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.XStreamSupport;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.PreferencesException;
import org.nuclos.server.attribute.BadGenericObjectException;
import org.nuclos.server.common.NuclosUpdateException;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;

import com.thoughtworks.xstream.XStream;

/**
 * Base class for all <code>CollectController</code>s in Nucleus.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public abstract class NuclosCollectController<PK,Clct extends Collectable<PK>> extends CollectController<PK,Clct> {

	private static final Logger LOG = Logger.getLogger(NuclosCollectController.class);

	protected UID entityUid;
	protected static final String PREFS_KEY_OUTERSTATE = "outerState";
	protected static final String PREFS_KEY_ENTITY = "entity";
	protected static final String PREFS_KEY_COLLECTABLEID = "collectableId";
	protected static final String PREFS_NODE_SEARCHCONDITION = "searchCondition";
	
	protected String sEntity;
	private final List<EntitySearchFilter> filtersSearch = new ArrayList<EntitySearchFilter>();
	
	protected EntitySearchFilter selectedSearchFilter = null;
	protected final NuclosCollectControllerCommonState state;
	
	private boolean actSearchFilterSearchEnabled = true;
	protected final Action actSearchFilterSearch = NuclosToolBarActions.LOAD_FILTER.init(new AbstractAction() {
		
		private final AtomicBoolean running = new AtomicBoolean(false);

		@Override
		public void actionPerformed(ActionEvent e) {
			if (!running.compareAndSet(false, true)) {
				return;
			}
			try {
				try {
					selectedSearchFilter = filtersSearch.get(e.getID());
					state.setCurrentSearchFilter((EntitySearchFilter) selectedSearchFilter.clone());
				} catch (Exception ex) {
					selectedSearchFilter = null;
				}
				
				boolean bRegularFilterSelected = false;
				if (selectedSearchFilter != null) {
					bRegularFilterSelected = !selectedSearchFilter.isDefaultFilter();
				}
				// enable/disable remove filter action - the empty filter cannot be removed:
				actRemoveFilter.setEnabled(bRegularFilterSelected);
				
				if (actSearchFilterSearchEnabled) {
					cmdSetCollectableSearchConditionAccordingToFilter();
				}
			} finally {
				running.set(false);
			}
		}
	});
	protected List<EntitySearchFilter> getSearchFilterFromView() {
		return Collections.unmodifiableList(filtersSearch);
	}
	private final List<SearchFilter> filtersResult = new ArrayList<SearchFilter>();
	protected final Action actSearchFilterResult = NuclosToolBarActions.SET_FILTER.init(new AbstractAction() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			actSearchFilterSearch.putValue(ToolBarItemAction.ITEM_SELECTED_INDEX, filtersSearch.indexOf(filtersResult.get(e.getID())));
			getResultController().getSearchResultStrategy().cmdRefreshResult();
		}
	});

	protected final Action actSaveFilter = NuclosToolBarActions.SAVE_FILTER.init(new CommonAbstractAction() {
		@Override
	    public void actionPerformed(ActionEvent ev) {
			cmdSaveFilter();
		}
	});
	
	protected final Action actRemoveFilter = NuclosToolBarActions.DELETE_FILTER.init(new CommonAbstractAction() {
		@Override
	    public void actionPerformed(ActionEvent ev) {
			cmdRemoveFilter();
		}
	});

	private Action actFilter;

	/**
	 * <code>ActionListener</code> defining actions for buttons in the LayoutML definition.
	 */
	private LayoutMLButtonActionListener alLayoutMLButtons;

	/**
	 * Don't make this public!
	 *
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton}
	 * to get an instance.
	 *
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	@Deprecated
	protected NuclosCollectController(UID entityUid, MainFrameTab tabIfAny, NuclosCollectControllerCommonState state) {
		this(NuclosCollectableEntityProvider.getInstance().getCollectableEntity(entityUid), tabIfAny, state);
		this.entityUid = entityUid;
	}

	/**
	 * Don't make this public!
	 *
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	@Deprecated
	protected NuclosCollectController(CollectableEntity clcte, MainFrameTab tabIfAny, NuclosCollectControllerCommonState state) {
		super(clcte, tabIfAny, new NuclosResultController<PK,Clct>(clcte, new NuclosSearchResultStrategy<PK,Clct>(), state));
		this.entityUid = clcte.getUID();
		this.state = state;

		this.actFilter = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				getResultController().getSearchResultStrategy().cmdRefreshResult();
			}
		};
	}

	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton}
	 * to get an instance.
	 *
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	@Deprecated
	protected NuclosCollectController(UID entityUid, MainFrameTab tabIfAny, ResultController<PK,Clct> rc) {
		this(NuclosCollectableEntityProvider.getInstance().getCollectableEntity(entityUid), tabIfAny, rc);
		this.entityUid = entityUid;
	}

	protected NuclosCollectController(CollectableEntity clcte, MainFrameTab tabIfAny, ResultController<PK,Clct> rc) {
		super(clcte, tabIfAny, rc);
		this.entityUid = clcte.getUID();
		if (rc instanceof NuclosResultController) {
			this.state = ((NuclosResultController) rc).getState();
		} else {
			this.state = new NuclosCollectControllerCommonState();
		}

		this.actFilter = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				getResultController().getSearchResultStrategy().cmdRefreshResult();
			}
		};
	}

	@Override
	protected void initialize(CollectPanel<PK,Clct> pnlCollect) {
		super.initialize(pnlCollect);
		// NUCLOS-2182
		getCollectStateModel().addCollectStateListener(new CollectStateAdapter() {

			private final AtomicBoolean running = new AtomicBoolean(false);
			
			@Override
			public void resultModeEntered(CollectStateEvent ev) throws CommonBusinessException {
				final EntitySearchFilter currentSearchFilter = state != null ? state.getCurrentSearchFilter() : null;
				if (currentSearchFilter == null) {
					return;
				}
				if (!running.compareAndSet(false, true)) {
					return;
				}
				try {
					String mainProfile = getMainFilter() != null ? getMainFilter().getSearchFilterVO().getResultProfile() : null;
					if (currentSearchFilter != null && mainProfile == null) {
						final SearchFilterVO vo = currentSearchFilter.getSearchFilterVO();
						final String profile = vo.getResultProfile();
						if (profile != null) {
							((NuclosResultController<PK,Clct>) getResultController()).getProfilesController().switchToProfile(profile);
						}
					}
				} finally {
					running.set(false);
				}
			}

		});
	}
	
	/**
	 * @deprecated Consider using {@link #getEntityUid()}.
	 */
	@Deprecated
	public UID getEntityName() {
		return entityUid;
	}

	/**
	 * §todo this method is misused - it sets shortcuts for many things other than tabs...
	 */
	@Override
	protected void setupShortcutsForTabs(MainFrameTab frame) {
		final CollectPanel<PK,Clct> pnlCollect = this.getCollectPanel();
		final DetailsPanel pnlDetails = this.getDetailsPanel();

		final Action actSelectSearchTab = new AbstractAction() {
			@Override
            public void actionPerformed(ActionEvent ev) {
				if (pnlCollect.isTabbedPaneEnabledAt(CollectPanel.TAB_SEARCH)) {
					pnlCollect.setTabbedPaneSelectedComponent(getSearchPanel());
				}
			}
		};
		final Action actSelectSearchTabAndClear = new AbstractAction() {
			@Override
            public void actionPerformed(ActionEvent ev) {
				if (pnlCollect.isTabbedPaneEnabledAt(CollectPanel.TAB_SEARCH)) {
					pnlCollect.setTabbedPaneSelectedComponent(getSearchPanel());
					cmdClearSearchCondition();
				} else if (pnlCollect.getTabbedPaneSelectedIndex() == CollectPanel.TAB_SEARCH) {
					cmdClearSearchCondition();
				}
			}
		};
		
		
		//
		final Action actSelectTab = new AbstractAction() {
			@Override
            public void actionPerformed(ActionEvent ev) {
				
				Component pane=getTabbedPaneIfAvailable();
				if(pane!=null)
				{
					pane.requestFocus();
				}
			}
		};
		
		
		KeyBindingProvider.bindActionToComponent(KeyBindingProvider.NEXT_CHILD_TAB, actSelectTab, pnlCollect);

		//
		if (getViewMode() == ControllerPresentation.DEFAULT) {
			KeyBindingProvider.bindActionToComponent(KeyBindingProvider.ACTIVATE_SEARCH_PANEL_1, actSelectSearchTabAndClear, pnlCollect);
			KeyBindingProvider.bindActionToComponent(KeyBindingProvider.ACTIVATE_SEARCH_PANEL_2, actSelectSearchTab, pnlCollect);
		}

		//TODO This is a workaround. The detailpanel should keep the focus
		final Action actGrabFocus = new AbstractAction() {
			@Override
            public void actionPerformed(ActionEvent ev) {
				pnlDetails.requestFocus();
			}
		};

		/**
		 * A <code>ChainedAction</code> is an action composed of a primary and a secondary action.
		 * It behaves exactly like the primary action, except that additionally, the secondary action is performed
		 * after the primary action.
		 */
		class ChainedAction implements Action {
			private final Action actPrimary;
			private final Action actSecondary;

			public ChainedAction(Action actPrimary, Action actSecondary) {
				this.actPrimary = actPrimary;
				this.actSecondary = actSecondary;
			}

			@Override
            public void addPropertyChangeListener(PropertyChangeListener listener) {
				actPrimary.addPropertyChangeListener(listener);
			}

			@Override
            public Object getValue(String sKey) {
				return actPrimary.getValue(sKey);
			}

			@Override
            public boolean isEnabled() {
				return actPrimary.isEnabled();
			}

			@Override
            public void putValue(String sKey, Object oValue) {
				actPrimary.putValue(sKey, oValue);
			}

			@Override
            public void removePropertyChangeListener(PropertyChangeListener listener) {
				actPrimary.removePropertyChangeListener(listener);
			}

			@Override
            public void setEnabled(boolean bEnabled) {
				actPrimary.setEnabled(bEnabled);
			}

			@Override
            public void actionPerformed(ActionEvent ev) {
				actPrimary.actionPerformed(ev);
				actSecondary.actionPerformed(ev);
			}
		}

		//final Action actRefresh = new ChainedAction(this.getRefreshCurrentCollectableAction(), actGrabFocus);

		this.getCollectPanel().setTabbedPaneToolTipTextAt(CollectPanel.TAB_SEARCH, getSpringLocaleDelegate().getMessage(
				"NuclosCollectController.13","Suche (F7) (Strg+F)"));
		this.getCollectPanel().setTabbedPaneToolTipTextAt(CollectPanel.TAB_RESULT, getSpringLocaleDelegate().getMessage(
				"NuclosCollectController.7","Ergebnis (F8)"));
		this.getCollectPanel().setTabbedPaneToolTipTextAt(CollectPanel.TAB_DETAILS, getSpringLocaleDelegate().getMessage(
				"NuclosCollectController.3","Details (F2)"));

		// the search action
		if (getViewMode() == ControllerPresentation.DEFAULT) {
			KeyBindingProvider.bindActionToComponent(KeyBindingProvider.START_SEARCH, this.getSearchAction(), pnlCollect);
		}
		KeyBinding keybinding = KeyBindingProvider.REFRESH;

		// the refresh action
		KeyBindingProvider.removeActionFromComponent(keybinding, pnlDetails);
		pnlDetails.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(keybinding.getKeystroke(), keybinding.getKey());
		pnlDetails.getActionMap().put(keybinding.getKey(), this.getRefreshCurrentCollectableAction());
		KeyBindingProvider.removeActionFromComponent(keybinding, getResultPanel());
		getResultPanel().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(keybinding.getKeystroke(), keybinding.getKey());
		getResultPanel().getActionMap().put(keybinding.getKey(), getResultController().getRefreshAction());


		// the new action
		if (getViewMode() != ControllerPresentation.SPLIT_RESULT) {
			KeyBindingProvider.bindActionToComponent(KeyBindingProvider.NEW, this.getNewAction(), pnlCollect);

			// the new with search values action
			KeyBindingProvider.bindActionToComponent(KeyBindingProvider.NEW_SEARCHVALUE, this.getNewWithSearchValuesAction(), pnlCollect);
		}
		// the save action
		KeyBindingProvider.bindActionToComponent(KeyBindingProvider.SAVE_1, this.getSaveAction(), pnlDetails);
		KeyBindingProvider.bindActionToComponent(KeyBindingProvider.SAVE_2, this.getSaveAction(), pnlDetails);

		// the navigation actions
		KeyBindingProvider.bindActionToComponent(KeyBindingProvider.FIRST, this.getFirstAction(), pnlDetails);
		KeyBindingProvider.bindActionToComponent(KeyBindingProvider.LAST, this.getLastAction(), pnlDetails);
		KeyBindingProvider.bindActionToComponent(KeyBindingProvider.PREVIOUS_1, this.getPreviousAction(), pnlDetails);
		KeyBindingProvider.bindActionToComponent(KeyBindingProvider.PREVIOUS_2, this.getPreviousAction(), pnlDetails);
		KeyBindingProvider.bindActionToComponent(KeyBindingProvider.NEXT_1, this.getNextAction(), pnlDetails);
		KeyBindingProvider.bindActionToComponent(KeyBindingProvider.NEXT_2, this.getNextAction(), pnlDetails);

		Action actClose = new AbstractAction() {
			@Override
            public void actionPerformed(ActionEvent e) {
				getTab().close();
			}
		};
		KeyBindingProvider.bindActionToComponent(KeyBindingProvider.CLOSE_CHILD, actClose, pnlCollect);

		if (getResultPanel() != null && getResultTable() != null) {
			KeyBindingProvider.bindActionToComponent(KeyBindingProvider.EDIT_1, getResultController().getEditSelectedCollectablesAction(), getResultTable());
			if (getResultTable().getActionMap().get(KeyBindingProvider.EDIT_2.getKey()) == null)
					KeyBindingProvider.bindActionToComponent(KeyBindingProvider.EDIT_2, getResultController().getEditSelectedCollectablesAction(), getResultTable());
		}
	}

	protected Component getTabbedPaneIfAvailable() {
				
		Collection col=UIUtils.findAllInstancesOf(getCollectPanel(),JInfoTabbedPane.class);
		if(col.size()==0)
			return null;
		else
			return (Component) col.toArray()[0];
	}

	@Override
	protected Preferences getUserPreferencesRoot() {
		return ClientPreferences.getInstance().getUserPreferences();
	}

	/**
	 * @return false (default)
	 */
	@Override
	protected boolean isMultiEditAllowed() {
		return SecurityCache.getInstance().isActionAllowed(Actions.ACTION_BULKEDIT);
	}

	/**
	 * @deprecated Move to ResultController hierarchy and make protected again.
	 *   It would be far better, if the class (hierarchy) would known that search
	 *   should be single- or multi-threaded.
	 */
	@Deprecated
	@Override
	public boolean isMultiThreadingEnabled() {
		return true;
	}

	/**
	 * @return Is this <code>CollectController</code> restorable from preferences? Default: true.
	 */
	public boolean isRestorableFromPreferences() {
		return true;
	}

	public Action getFilterAction() {
		return actFilter;
	}

	protected static class RestorePreferences implements Serializable {
		private static final long serialVersionUID = 6637996725938917463L;

		UID entity;
		UID process;
		int  iCollectState;
		Object objectId;
		CollectableSearchCondition searchCondition;
		Map<String, String> inheritControllerPreferences = new HashMap<String, String>(1);
		String customUsage;

		public UID getProcess() {
			return process;
		}

		public void setProcess(UID process) {
			this.process = process;
		}
	}

	private static String toXML(RestorePreferences rp) {
		final XStreamSupport xs = XStreamSupport.getInstance();
		try (CloseableXStream closeable = xs.getCloseableStream()) {
			final XStream xstream = closeable.getStream();
			return xstream.toXML(rp);
		}
	}

	private static RestorePreferences fromXML(String xml) {
		final XStreamSupport xs = XStreamSupport.getInstance();
		try (CloseableXStream closeable = xs.getCloseableStream()) {
			final XStream xstream = closeable.getStream();
			return (RestorePreferences) xstream.fromXML(xml);
		}
	}

	protected void storeInstanceStateToPreferences(Map<String, String> inheritControllerPreferences) {}

	protected void restoreInstanceStateFromPreferences(Map<String, String> inheritControllerPreferences) {}

	public static class NuclosCollectTabStoreController<PK2> implements ITabStoreController {

		private final WeakReference<NuclosCollectController<PK2,?>> refCtl;

		public NuclosCollectTabStoreController(NuclosCollectController<PK2,?> ctl) {
			this.refCtl = new WeakReference<NuclosCollectController<PK2,?>>(ctl);
		}

		@Override
		public String getPreferencesXML() {
			final NuclosCollectController<PK2,?> ctl = refCtl.get();
			if (ctl == null) {
				throw new NullPointerException("Controller has already been gc'ed");
			}
			
			final RestorePreferences rp = new RestorePreferences();
			rp.entity = ctl.getEntityUid();
			rp.customUsage = ctl.getCustomUsage();
			try {
				if (ctl.getCollectStateModel().getOuterState() == CollectState.OUTERSTATE_DETAILS) {
					rp.iCollectState = CollectState.OUTERSTATE_DETAILS;
					ctl.writeCurrentCollectableToPreferences(rp);
				}
				ctl.writeStateToPreferences(rp);
			} catch(Exception e) {
				LOG.warn("Preferences not completely stored.", e);
			}

			ctl.storeInstanceStateToPreferences(rp.inheritControllerPreferences);
			return toXML(rp);
		}

		@Override
		public Class<?> getTabRestoreControllerClass() {
			return NuclosCollectTabRestoreController.class;
		}

	}

	public static class NuclosCollectTabRestoreController extends TabRestoreController {

		@Override
		public void restoreFromPreferences(String preferencesXML, MainFrameTab tab) throws Exception {
			RestorePreferences rp = fromXML(preferencesXML);

			NuclosCollectController<?,?> ctl = createFromPreferences(rp, tab);
			final MainController mc = Main.getInstance().getMainController();
			
			mc.initMainFrameTab(ctl, tab);
			// Main.getMainController().addMainFrameTab would be called from listener inside of initMainFrameTab, but only when tab added.
			// During restore the tabs are already added, so we need to do this manually.
			mc.addMainFrameTab(tab, ctl);

			ctl.restoreInstanceStateFromPreferences(rp.inheritControllerPreferences);
		}
		
		@Override
		public boolean validate(String preferencesXML) {
			return true;
		}

	}

	/**
	 * @return a new <code>NuclosCollectController</code> with the state read from the preferences.
	 */
	@Deprecated
	public static <PK> NuclosCollectController<PK,?> createFromPreferences(final Preferences prefs) 
			throws CommonBusinessException {
		
		final UID sEntity = new UID(prefs.get(PREFS_KEY_ENTITY, null));
		if (sEntity == null) {
			throw new PreferencesException(SpringLocaleDelegate.getInstance().getMessage(
					"NuclosCollectController.6","Entit\u00e4t {0} fehlt in den Benutzereinstellungen.", sEntity));
		}

		boolean allowed;
		allowed = SecurityCache.getInstance().isReadAllowedForModule(sEntity, null) 
				|| SecurityCache.getInstance().isReadAllowedForMasterData(sEntity);

		if(!allowed) {
			throw new PreferencesException(SpringLocaleDelegate.getInstance().getMessage(
					"NuclosCollectController.17", "Sie haben kein Recht die Entit\u00e4t ''{0}'' zu verwenden.", sEntity));
		}

		final NuclosCollectController<PK,Collectable<PK>> result = (NuclosCollectController<PK, Collectable<PK>>) 
				NuclosCollectControllerFactory.getInstance().newCollectController(
				sEntity, null, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));

		final int iCollectState = result.restoreStateFromPreferences(prefs);

		UIUtils.runCommandLaterForTabbedPane(MainFrame.getPredefinedEntityOpenLocation(sEntity), new CommonRunnable() {
			// This must be done later as reloading the layout in restoreStateFromPreferences is done later also:
			@Override
			public void run() throws CommonBusinessException {
				switch (iCollectState) {
				case CollectState.OUTERSTATE_SEARCH:
					result.runSearch(false);
					break;
				case CollectState.OUTERSTATE_RESULT:
					result.runViewAll(false);
					break;
				case CollectState.OUTERSTATE_DETAILS:
					final Collectable<PK> clct = (Collectable<PK>) result.getCurrentCollectableFromPreferences(prefs);
					if (clct == null) {
						result.runNew(false);
					}
					else {
						result.runViewSingleCollectable(clct, false);
					}
					break;
				default:
					throw new PreferencesException(SpringLocaleDelegate.getInstance().getMessage(
							"NuclosCollectController.14","Ung\u00fcltiger Erfassungsstatus in den Benutzereinstellungen: {0}", iCollectState));
				}
			}
		});
		return result;
	}

	/**
	 *
	 * @param rp
	 * @param tabIfAny
	 * @return
	 * @throws CommonBusinessException
	 */
	public static NuclosCollectController<?,?> createFromPreferences(final RestorePreferences rp, MainFrameTab tabIfAny) throws CommonBusinessException{
		boolean allowed;
		allowed = SecurityCache.getInstance().isReadAllowedForModule(rp.entity, null) 
				|| SecurityCache.getInstance().isReadAllowedForMasterData(rp.entity);

		if(!allowed) {
			throw new PreferencesException(SpringLocaleDelegate.getInstance().getMessage(
					"NuclosCollectController.17", "Sie haben kein Recht die Entit\u00e4t ''{0}'' zu verwenden.", rp.entity));
		}

		final NuclosCollectController<?,?> result = NuclosCollectControllerFactory.getInstance().newCollectController(rp.entity, rp.process, tabIfAny,
				rp.customUsage!=null? rp.customUsage: ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY), false);

		final int cs = result.restoreStateFromPreferences(rp.iCollectState, rp.searchCondition);

		UIUtils.runCommandLater(tabIfAny != null? tabIfAny : Main.getInstance().getMainFrame().getHomePane().getComponentPanel(), new CommonRunnable() {
			// This must be done later as reloading the layout in restoreStateFromPreferences is done later also:
			@Override
			public void run() throws CommonBusinessException {
				switch (cs) {
				case CollectState.OUTERSTATE_SEARCH:
					result.runSearch(false);
					break;
				case CollectState.OUTERSTATE_RESULT:
					result.runViewAll(false);
					break;
				case CollectState.OUTERSTATE_DETAILS:
					if (rp.objectId == null) {
						result.runNew(false);
					} else {
						((NuclosCollectController) result).runViewSingleCollectableWithId(rp.objectId, false);
					}
					break;
				default:
					throw new PreferencesException(SpringLocaleDelegate.getInstance().getMessage(
							"NuclosCollectController.14","Ung\u00fcltiger Erfassungsstatus in den Benutzereinstellungen: {0}", rp.iCollectState));
				}
			}
		});

		return result;
	}

	/**
	 * restores the state of this <code>CollectController</code> from the preferences.
	 * @param iCollectState
	 * @param cond
	 * @return
	 * @throws CommonBusinessException
	 */
	protected int restoreStateFromPreferences(Integer iCollectState, CollectableSearchCondition cond) throws CommonBusinessException {
		if (this.isSearchPanelAvailable()) {
			assert this.getCollectStateModel().getOuterState() == CollectState.OUTERSTATE_UNDEFINED;
			// Always set Search mode initially, to restore the search criteria.
			// Later, the previous state is restored.
			if (cond != null) {
				this.setCollectState(CollectState.OUTERSTATE_SEARCH, CollectState.SEARCHMODE_UNSYNCHED);
				this.restoreSearchCriteriaFromPreferences(cond);
			}
		}
		// restore collect state:
		return LangUtils.defaultIfNull(iCollectState, CollectState.OUTERSTATE_UNDEFINED);
	}

	/**
	 * restores the state of this <code>CollectController</code> from the preferences.
	 * @param prefs
	 * @return the stored collect state.
	 * @throws NuclosBusinessException
	 */
	@Deprecated
	protected int restoreStateFromPreferences(Preferences prefs) throws CommonBusinessException {
		if (this.isSearchPanelAvailable()) {
			assert this.getCollectStateModel().getOuterState() == CollectState.OUTERSTATE_UNDEFINED;
			final CollectableSearchCondition cond = SearchConditionUtils.getSearchCondition(
					prefs.node(PREFS_NODE_SEARCHCONDITION), getCollectableEntity().getUID());
			// Always set Search mode initially, to restore the search criteria.
			// Later, the previous state is restored.
			if (cond != null) {
				this.setCollectState(CollectState.OUTERSTATE_SEARCH, CollectState.SEARCHMODE_UNSYNCHED);
				this.restoreSearchCriteriaFromPreferences(prefs);
			}
		}
		// restore collect state:
		return prefs.getInt(PREFS_KEY_OUTERSTATE, CollectState.OUTERSTATE_UNDEFINED);
	}

	/**
	 * §precondition this.isSearchPanelVisible()
	 */
	protected void writeSearchCriteriaToPreferences(RestorePreferences rp) throws CommonBusinessException {
		// write current search criteria (which may differ from the current search filter's criteria):

		// We don't call makeConsistent (see below), but we want to get the last input of a table cell editor, if any:
		this.stopEditingInSearch();

		// Note that makeConsistent is not called here. If the search condition is incomplete, the differences in the view
		// to the model are ignored. This seems to be better than throwing an exception here and not storing the search condition at all:
		final CollectableSearchCondition cond = this.getCollectableSearchConditionFromSearchPanel(false);
		rp.searchCondition = cond;
		LOG.debug("Wrote searchcondition to prefs: " + LangUtils.toString(cond));
	}

	/**
	 * §precondition this.isSearchPanelVisible()
	 */
	protected void restoreSearchCriteriaFromPreferences(CollectableSearchCondition cond) throws CommonBusinessException {
		if (!this.isSearchPanelAvailable()) {
			throw new IllegalStateException("!isSearchPanelVisible()");
		}

		this.setCollectableSearchConditionInSearchPanel(NuclosSearchConditionUtils.restorePlainSubConditions(cond));

		try {
			LOG.debug("restored searchcondition from prefs: " + LangUtils.toString(getSearchStrategy().getCollectableSearchCondition()));
		}
		catch (CollectableFieldFormatException ex) {
			LOG.debug("Exception thrown in log statement", ex);
		}
	}

	/**
	 * §precondition this.isSearchPanelVisible()
	 */
	@Deprecated
	protected void restoreSearchCriteriaFromPreferences(Preferences prefs) throws CommonBusinessException {
		if (!this.isSearchPanelAvailable()) {
			throw new IllegalStateException("!isSearchPanelVisible()");
		}
		// restore search condition:
		final CollectableSearchCondition cond = SearchConditionUtils.getSearchCondition(
				prefs.node(PREFS_NODE_SEARCHCONDITION), getCollectableEntity().getUID());
		this.restoreSearchCriteriaFromPreferences(cond);
	}

	/**
	 * writes the state of this <code>CollectController</code> to the preferences.
	 */
	protected void writeStateToPreferences(RestorePreferences rp) throws CommonBusinessException {
		// write collect state:
		rp.iCollectState = this.getCollectStateModel().getOuterState();

		if (this.isSearchPanelAvailable()) {
			this.writeSearchCriteriaToPreferences(rp);
		}
	}

	/**
	 * writes the current collectable to the preferences.
	 */
	protected void writeCurrentCollectableToPreferences(RestorePreferences rp) {
		this.writeSerializableCurrentCollectableIdToPreferences(rp);
	}

	/**
	 * reads the current collectable from the preferences.
	 * @param prefs
	 * @return the current collectable, if any.
	 */
	protected Clct getCurrentCollectableFromPreferences(Preferences prefs) throws PreferencesException,
	CommonBusinessException {
		final PK oId = (PK) PreferencesUtils.getSerializable(prefs, PREFS_KEY_COLLECTABLEID);
		return (oId == null) ? null : findCollectableById(getEntityUid(), oId, -1);
	}

	/**
	 * makes the given Collectable complete, if it isn't already.
	 * 
	 * §postcondition isCollectableComplete(result)
	 * §postcondition isCollectableComplete(clct) --&gt; result == clct
	 * §todo pull down to CollectController?
	 */
	protected final Clct getCompleteCollectable(Clct clct) throws CommonBusinessException {
		final ISearchStrategy<PK,Clct> ss = getSearchStrategy();
		final Clct result = ss.isCollectableComplete(clct) ? clct : this.readCollectable(clct);
		assert ss.isCollectableComplete(result);
		assert !ss.isCollectableComplete(clct) || result == clct;
		return result;
	}
	
	/**
	 * writes the current collectable id to the preferences. The collectable id must be serializable.
	 */
	protected void writeSerializableCurrentCollectableIdToPreferences(RestorePreferences rp) {
		final Object oId = this.getSelectedCollectableId();
		LOG.debug("writeSerializableCurrentCollectableIdToPreferences: oId = " + oId);
		if ((oId != null) && !(oId instanceof Serializable)) {
			throw new NuclosFatalException("The CollectableId is not serializable");
		}
		rp.objectId = oId;
	}

	@Override
	protected void handleSaveException(CommonBusinessException ex, String sMessage1) throws CommonBusinessException {
		if (ex instanceof CollectableValidationException) {
			handleCollectableValidationException((CollectableValidationException) ex, sMessage1);
		} else {
			try {
				throw ex;
			}
			catch (BadGenericObjectException ex2) {
				Errors.getInstance().showExceptionDialog(this.getTab(), null, ex2);
			}
			catch (NuclosUpdateException ex2) {
				/** @todo this is a workaround. @see GenericObjectDelegate.update */
				Errors.getInstance().showExceptionDialog(this.getTab(), ex2);
			}
			catch (NuclosBusinessRuleException ex2) {
				Errors.getInstance().showExceptionDialog(this.getTab(),
						getSpringLocaleDelegate().getMessage("NuclosCollectController.1","{0}, da das Speichern zu einem Fehler innerhalb einer Gesch\u00e4ftsregel gef\u00fchrt hat.", sMessage1), ex2);
			}
		}
	}

	/**
	 * §todo refactor viewAll()!
	 *
	 * @deprecated Move to ResultController hierarchy.
	 */
	@Deprecated
	@Override
	protected void viewAll() throws CommonBusinessException {
		getResultController().getSearchResultStrategy().refreshResult();
	}

	/**
	 * translates Boolean fields by mapping <code>null</code> to <code>false</code> as there are no nullable
	 * booleans in Nucleus.
	 * 
	 * §precondition clctCurrent != null
	 * §precondition isCollectableComplete(clctCurrent)
	 * §precondition clcteCurrent != null
	 */
	@Override
	protected void prepareCollectableForSaving(Clct clctCurrent, CollectableEntity clcteCurrent) {
		if (clctCurrent == null) {
			throw new NullArgumentException("clctCurrent");
		}
		super.prepareCollectableForSaving(clctCurrent, clcteCurrent);

		Utils.mapNullBooleansToFalseInCollectable(clctCurrent, clcteCurrent);
	}
	
	private static class MyLayoutMLButtonActionListener<PK2,Clct2 extends Collectable<PK2>> extends LayoutMLButtonActionListener {
		
		private final WeakReference<CollectController<PK2,Clct2>> refCc;
		
		private MyLayoutMLButtonActionListener(CollectController<PK2,Clct2> cc) {
			this.refCc = new WeakReference<CollectController<PK2,Clct2>>(cc);
		}
		
		@Override
		public void actionPerformed(ActionEvent ev) {
			final CollectController<PK2,Clct2> cc = refCc.get();
			if (cc == null) {
				return;
			}
			
			final String sActionCommand = ev.getActionCommand();
			//NUCLOSINT-743 State and Rule Button Action
			String targetState = "_" + STATIC_BUTTON.STATE_CHANGE_ACTION_ARGUMENT + "=";
			String ruletoexecute = "_" + STATIC_BUTTON.EXECUTE_RULE_ACTION_ARGUMENT + "=";
			String generatortoexecute = "_" + STATIC_BUTTON.GENERATOR_ACTION_ARGUMENT + "=";
			String hyperlinkField = "_" + STATIC_BUTTON.HYPERLINK_ACTION_ARGUMENT + "=";
			try {
				if (sActionCommand.contains(STATIC_BUTTON.STATE_CHANGE_ACTION + targetState)) {
					Properties stateProperties = new Properties();
					String state = sActionCommand.substring(sActionCommand.indexOf(targetState) + targetState.length(), sActionCommand.length());
					stateProperties.put(STATIC_BUTTON.STATE_CHANGE_ACTION_ARGUMENT, state);
					((CollectActionAdapter<PK2,Clct2>) Class.forName(STATIC_BUTTON.STATE_CHANGE_ACTION).newInstance()).run((JButton)ev.getSource(), cc, stateProperties);
				} else if (sActionCommand.contains(STATIC_BUTTON.EXECUTE_RULE_ACTION + ruletoexecute)) {
					Properties ruleProperties = new Properties();
					String ruleId = sActionCommand.substring(sActionCommand.indexOf(ruletoexecute) + ruletoexecute.length(), sActionCommand.length());
					ruleProperties.put(STATIC_BUTTON.EXECUTE_RULE_ACTION_ARGUMENT, ruleId);
					((CollectActionAdapter<PK2,Clct2>) Class.forName(STATIC_BUTTON.EXECUTE_RULE_ACTION).newInstance()).run((JButton)ev.getSource(), cc, ruleProperties);
				} else if (sActionCommand.contains(STATIC_BUTTON.GENERATOR_ACTION + generatortoexecute)) {
					Properties generatorProperties = new Properties();
					String generatorId = sActionCommand.substring(sActionCommand.indexOf(generatortoexecute) + generatortoexecute.length(), sActionCommand.length());
					generatorProperties.put(STATIC_BUTTON.GENERATOR_ACTION_ARGUMENT, generatorId);
					((CollectActionAdapter<PK2,Clct2>) Class.forName(STATIC_BUTTON.GENERATOR_ACTION).newInstance()).run((JButton)ev.getSource(), cc, generatorProperties);
				} else if (sActionCommand.contains(STATIC_BUTTON.HYPERLINK_ACTION + hyperlinkField)) {
					Properties hyperlinkProperties = new Properties();
					String hyperlinkFieldId = sActionCommand.substring(sActionCommand.indexOf(hyperlinkField) + hyperlinkField.length(), sActionCommand.length());
					hyperlinkProperties.put(STATIC_BUTTON.HYPERLINK_ACTION_ARGUMENT, hyperlinkFieldId);
					((CollectActionAdapter<PK2,Clct2>) LangUtils.getClassLoaderThatWorksForWebStart().loadClass(STATIC_BUTTON.HYPERLINK_ACTION).newInstance()).run((JButton)ev.getSource(), cc, hyperlinkProperties);
				} else {
					((CollectActionAdapter<PK2,Clct2>) Class.forName(sActionCommand).newInstance()).run((JButton)ev.getSource(), cc, new Properties());
				}
			}
			catch (InstantiationException ex) {
				throw new CommonFatalException(ex);
			}
			catch (IllegalAccessException ex) {
				throw new CommonFatalException(ex);
			}
			catch (ClassNotFoundException ex) {
				throw new CommonFatalException(ex);
			}
		}
		
		@Override
		public boolean enableParentComponent(String sActionCommand) {
			if (sActionCommand == null)
				return false;
			
			final CollectController<PK2,Clct2> cc = refCc.get();
			if (cc == null) {
				return false;
			}
			
			boolean result = false;
			//NUCLOSINT-743 State and Rule Button Action
			String targetState = "_" + STATIC_BUTTON.STATE_CHANGE_ACTION_ARGUMENT + "=";
			String ruletoexecute = "_" + STATIC_BUTTON.EXECUTE_RULE_ACTION_ARGUMENT + "=";
			String generatortoexecute = "_" + STATIC_BUTTON.GENERATOR_ACTION_ARGUMENT + "=";
			String hyperlinkField = "_" + STATIC_BUTTON.HYPERLINK_ACTION_ARGUMENT + "=";
			try {
				if (sActionCommand.contains(STATIC_BUTTON.STATE_CHANGE_ACTION + targetState)) {
					Properties stateProperties = new Properties();
					String state = sActionCommand.substring(sActionCommand.indexOf(targetState) + targetState.length(), sActionCommand.length());
					stateProperties.put(STATIC_BUTTON.STATE_CHANGE_ACTION_ARGUMENT, state);
					result = ((CollectActionAdapter<PK2,Clct2>) Class.forName(STATIC_BUTTON.STATE_CHANGE_ACTION).newInstance()).isRunnable(cc, stateProperties);
				} else if (sActionCommand.contains(STATIC_BUTTON.EXECUTE_RULE_ACTION + ruletoexecute)) {
					Properties ruleProperties = new Properties();
					String ruleId = sActionCommand.substring(sActionCommand.indexOf(ruletoexecute) + ruletoexecute.length(), sActionCommand.length());
					ruleProperties.put(STATIC_BUTTON.EXECUTE_RULE_ACTION_ARGUMENT, ruleId);
					result = ((CollectActionAdapter<PK2,Clct2>) Class.forName(STATIC_BUTTON.EXECUTE_RULE_ACTION).newInstance()).isRunnable(cc, ruleProperties);
				} else if (sActionCommand.contains(STATIC_BUTTON.GENERATOR_ACTION + generatortoexecute)) {
					Properties generatorProperties = new Properties();
					String generatorId = sActionCommand.substring(sActionCommand.indexOf(generatortoexecute) + generatortoexecute.length(), sActionCommand.length());
					generatorProperties.put(STATIC_BUTTON.GENERATOR_ACTION_ARGUMENT, generatorId);
					result = ((CollectActionAdapter<PK2,Clct2>) LangUtils.getClassLoaderThatWorksForWebStart().loadClass(
							STATIC_BUTTON.GENERATOR_ACTION).newInstance()).isRunnable(cc, generatorProperties);
				} else if (sActionCommand.contains(STATIC_BUTTON.HYPERLINK_ACTION + hyperlinkField)) {
					Properties hyperlinkProperties = new Properties();
					String hyperlinkFieldId = sActionCommand.substring(sActionCommand.indexOf(hyperlinkField) + hyperlinkField.length(), sActionCommand.length());
					hyperlinkProperties.put(STATIC_BUTTON.HYPERLINK_ACTION_ARGUMENT, hyperlinkFieldId);
					result = ((CollectActionAdapter<PK2,Clct2>) LangUtils.getClassLoaderThatWorksForWebStart().loadClass(
							STATIC_BUTTON.HYPERLINK_ACTION).newInstance()).isRunnable(cc, hyperlinkProperties);
				} else {
					result = ((CollectActionAdapter<PK2,Clct2>) LangUtils.getClassLoaderThatWorksForWebStart().loadClass(
							sActionCommand).newInstance()).isRunnable(cc, new Properties());
				}
			}
			catch (InstantiationException ex) {
				throw new CommonFatalException(ex);
			}
			catch (IllegalAccessException ex) {
				throw new CommonFatalException(ex);
			}
			catch (ClassNotFoundException ex) {
				throw new CommonFatalException(ex);
			}			
			return result;
		}
	}

	/**
	 * @return <code>ActionListener</code> defining actions for buttons in the LayoutML definition. May be <code>null</code>.
	 */
	protected LayoutMLButtonActionListener getLayoutMLButtonsActionListener() {
		if (alLayoutMLButtons == null) {
			alLayoutMLButtons = new MyLayoutMLButtonActionListener<PK,Clct>(NuclosCollectController.this);
		}
		return alLayoutMLButtons;
	}
	
	protected void resetLayoutMLButtonsActionListener() {
		alLayoutMLButtons = null;
	}
	
	protected void setupDetailsToolBar() {
		LOG.trace("setup details toolbar");
	}
	
	protected void setupResultToolBar() {
		actSearchFilterResult.putValue(ToolBarItemAction.MENU_LABEL, getSpringLocaleDelegate().getMessage("CollectController.Search.Filter","Filter"));
		if (isSearchPanelAvailable()) {
			getResultPanel().registerToolBarAction(NuclosToolBarItems.SET_FILTER, actSearchFilterResult);
		}
	}
	
	/**
	 * @deprecated Nothing more than a SearchFilter (tp).
	 */
	@Deprecated
	private static class SearchFilterToolBarItem {
		
		private final SearchFilter searchFilter;
		
		public SearchFilterToolBarItem(SearchFilter vo) {
			this.searchFilter = vo;
		}
		
		@Override
		public String toString() {
			final SearchFilterVO vo = searchFilter.getSearchFilterVO();
			return SpringLocaleDelegate.getInstance().getTextFallback(vo.getLabelResourceId(), vo.getFilterName());
		}
		
		@Override
		public boolean equals(Object obj) {
			if (obj == this)
				return true;
			if (obj instanceof SearchFilterToolBarItem) {
				return LangUtils.equal(((SearchFilterToolBarItem) obj).searchFilter, searchFilter);
			}
			return super.equals(obj);
		}
		@Override
		public int hashCode() {
			return LangUtils.hashCode(searchFilter);
		}
	}

	protected void setupSearchToolBar() {
		actSearchFilterSearch.putValue(ToolBarItemAction.MENU_LABEL, getSpringLocaleDelegate().getMessage("CollectController.Search.Filter","Filter"));
		getSearchPanel().registerToolBarAction(NuclosToolBarItems.LOAD_FILTER, actSearchFilterSearch);

		getSearchPanel().registerToolBarAction(NuclosToolBarItems.SAVE_FILTER, actSaveFilter);
		getSearchPanel().registerToolBarAction(NuclosToolBarItems.DELETE_FILTER, actRemoveFilter);

		// disable the remove filter action initially:
		actRemoveFilter.setEnabled(false);
		
		boolean hasFastSelectSearchFilters = false;
		EntitySearchFilter defaultFilter = null;
		try {
			for (EntitySearchFilter sf : getSearchFilters().getAll()) {
				if (Boolean.TRUE.equals(sf.getSearchFilterVO().getFastSelectInResult())) {
					hasFastSelectSearchFilters = true;
				}
				if (Boolean.TRUE.equals(sf.getSearchFilterVO().getFastSelectDefault())) {
					defaultFilter = sf;
				}
			}
		} catch (PreferencesException e) {
			LOG.error(e.getMessage(), e);
		}
		if (!hasFastSelectSearchFilters && isSearchPanelAvailable() && defaultFilter != null) {
			selectedSearchFilter = defaultFilter;
			cmdSetCollectableSearchConditionAccordingToFilter();
		}
	}

	/**
	 * Command: save filter
	 */
	protected void cmdSaveFilter() {
		UIUtils.runCommand(this.getTab(), new CommonRunnable() {
			@Override
            public void run() throws CommonValidationException {
				if (!stopEditingInSearch()) {
					throw new CommonValidationException(getSpringLocaleDelegate().getMessage(
							"NuclosCollectController.4","Die eingegebene Suchbedingung ist ung\u00fcltig bzw. unvollst\u00e4ndig."));
				}
				try {
					EntitySearchFilter filterCurrent = retrieveCurrentSearchFilterFromSearchPanel();
					final ProfileModel pModel = ((NuclosResultController<PK,Clct>) getResultController()).getProfilesController().getModel();
					final SaveFilterController sfc = new SaveFilterController(
							getTab(), getSearchFilters(), filterCurrent.getSearchFilterVO(), pModel, NuclosCollectController.this);
					final SaveFilterController.Command cmd = sfc.runSave(
							getEntityUid(), getMainController().getUserName(), selectedSearchFilter, filterCurrent);
					switch (cmd) {
						case None:
							// do nothing
							break;

						case Overwrite:
							// selectDefaultFilter();
							filtersSearch.remove(selectedSearchFilter);
							// note that there is no "break" here!

						case New:
							filterCurrent = SearchFilterCache.getInstance().getSearchFilter(
									filterCurrent.getSearchFilterVO().getFilterName(), 
									filterCurrent.getSearchFilterVO().getOwner(), 
									filterCurrent.getSearchFilterVO().getEntity());
							
							// write change 
							filterCurrent.setSearchFilterVO(sfc.getSearchFilterVO());
							
							filtersSearch.add(filterCurrent);
							UIUtils.ensureMinimumSize(getTab());
							Integer selectedIndex = null;
							Object[] items = new Object[filtersSearch.size()];
							for (int i = 0; i < filtersSearch.size(); i++) {
								SearchFilter sf = filtersSearch.get(i);
								if (sf != null) {
									if (sf.equals(filterCurrent)) {
										selectedIndex = i;
									}
									items[i] = new SearchFilterToolBarItem(sf);
								}
							}
							actSearchFilterSearchEnabled = false;
							actSearchFilterSearch.putValue(ToolBarItemAction.ITEM_SELECTED_INDEX, selectedIndex);
							actSearchFilterSearch.putValue(ToolBarItemAction.ITEM_ARRAY, items);
							actSearchFilterSearchEnabled = true;
							cmdSetCollectableSearchConditionAccordingToFilter();
							break;

						default:
							assert false;
					}
				}
				catch (CommonBusinessException ex) {
					Errors.getInstance().showExceptionDialog(getTab(), ex);
				}
			}
		});
	}

	/**
	 * command: remove filter
	 */
	protected void cmdRemoveFilter() {
		if (selectedSearchFilter != null) {
			final SearchFilterVO vo = selectedSearchFilter.getSearchFilterVO();
			if (JOptionPane.showConfirmDialog(this.getTab(), getSpringLocaleDelegate().getMessage(
					"NuclosCollectController.16","Wollen Sie den Filter \"{0}\" wirklich l\u00f6schen?", 
					vo.getFilterName()), "Filter l\u00f6schen", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION) {
				UIUtils.runCommand(this.getTab(), new CommonRunnable() {
					
					@Override
                    public void run() throws NuclosBusinessException{
						getSearchFilters().remove(selectedSearchFilter);
						refreshSearchFilterView();
						refreshFastFilter();
					}
				});
			}
		}
	}



	/**
	 * @return empty search filter to be used as default
	 */
	private EntitySearchFilter newDefaultFilter() {
		final EntitySearchFilter result = getSearchFilters().newDefaultFilter();

		return result;
	}
	
	protected void refreshFastFilter() {
		getResultPanel().getSearchFilterBar().setSearchFilters(getSearchFilters());
	}

	protected void refreshResultFilterView() {
		// remember selected filter, if any:
		final List<? extends SearchFilter> lstFilters;
		try {
			lstFilters = getSearchFilters().getAll();
		}
		catch (PreferencesException ex) {
			throw new NuclosFatalException(ex);
		}

		// don't fire changed events here:
		// this.getResultFilterComboBox().removeActionListener(this.alResultFilter);
		
		filtersResult.clear();
		filtersResult.add(newDefaultFilter());
		for (SearchFilter filter : lstFilters) {
			filtersResult.add(filter);
		}
		Integer selectedIndex = null;
		Object[] items = new Object[filtersResult.size()];
		for (int i = 0; i < filtersResult.size(); i++) {
			SearchFilter sf = filtersResult.get(i);
			if (sf.equals(selectedSearchFilter)) {
				selectedIndex = i;
			}
			items[i] = new SearchFilterToolBarItem(sf);
		}
		
		actSearchFilterResult.putValue(ToolBarItemAction.ACTION_DISABLED, true);
		actSearchFilterResult.putValue(ToolBarItemAction.ITEM_SELECTED_INDEX, selectedIndex);
		actSearchFilterResult.putValue(ToolBarItemAction.ITEM_ARRAY, items);
		actSearchFilterResult.putValue(ToolBarItemAction.ACTION_DISABLED, false);
	}

	/**
	 * refreshes the combobox containing the filters
	 */
	protected void refreshSearchFilterView() {
		// remember selected filter, if any:
		final List<EntitySearchFilter> lstFilters;
		try {
			lstFilters = getSearchFilters().getAll();
		}
		catch (PreferencesException ex) {
			throw new NuclosFatalException(ex);
		}

		// don't fire changed events here:
		// getSearchFilterComboBox().removeActionListener(this.alSearchFilter);
		
		filtersSearch.clear();
		filtersSearch.add(newDefaultFilter());
		for (EntitySearchFilter filter : lstFilters) {
			filtersSearch.add(filter);
		}
		Integer selectedIndex = null;
		Object[] items = new Object[filtersSearch.size()];
		for (int i = 0; i < filtersSearch.size(); i++) {
			SearchFilter sf = filtersSearch.get(i);
			if (sf.equals(selectedSearchFilter)) {
				selectedIndex = i;
			}
			items[i] = new SearchFilterToolBarItem(sf);
		}
		
		actSearchFilterSearchEnabled = false;
		actSearchFilterSearch.putValue(ToolBarItemAction.ITEM_SELECTED_INDEX, selectedIndex);
		actSearchFilterSearch.putValue(ToolBarItemAction.ITEM_ARRAY, items);
		actSearchFilterSearchEnabled = true;
	}

	protected EntitySearchFilters getSearchFilters() {
		return EntitySearchFilters.forEntity(getEntityUid());
	}

	protected EntitySearchFilter retrieveCurrentSearchFilterFromSearchPanel() throws CommonBusinessException  {
		final EntitySearchFilter result = selectedSearchFilter == null ? 
				new EntitySearchFilter() : (EntitySearchFilter) selectedSearchFilter.clone();

		result.setSearchCondition(getCollectableSearchConditionFromSearchPanel(true));
		result.getSearchFilterVO().setEntity(getEntityName());
		// result.setResultProfile(filterSearchSelected.getResultProfile());

		state.setCurrentSearchFilter(result);
		
		/** @todo set sorting column names */
		return result;
	}

	/**
	 * selects the given search filter in the search filters combo box.
	 * 
	 * §postcondition LangUtils.equals(this.getSelectedSearchFilter(), filter)
	 */
	public final void setSelectedSearchFilter(SearchFilter filter) {
		int index = filtersSearch.indexOf(filter);
		if (index >= 0) {
			actSearchFilterSearch.putValue(ToolBarItemAction.ITEM_SELECTED_INDEX, index);
		}
	}

	/**
	 * selects the default filter (the first entry in the combobox).
	 */
	public void selectDefaultFilter() {
		if (filtersSearch.size() > 0) {
			actSearchFilterSearch.putValue(ToolBarItemAction.ITEM_SELECTED_INDEX, 0);
		}
	}
	
	private final CommonRunnable setCollectableSearchConditionAccordingToFilter = new CommonRunnable() {
		
		private final AtomicBoolean running = new AtomicBoolean(false);
		
		@Override
        public void run() {
			if (!running.compareAndSet(false, true)) {
				return;
			}
			try {
				// clearSearchFilter will change the selectedSearchFilter, hence we need to save it... (tp)
				final EntitySearchFilter filterToSet = selectedSearchFilter;
				final SearchFilterVO selectedVo = filterToSet != null ? filterToSet.getSearchFilterVO() : null;
				// filterSelected may be null here as an ActionEvent is triggered by Swing even after
				// DefaultComboBoxModel.removeAllElements() is called.
				clearSearchCondition();
				setCollectableSearchConditionInSearchPanel(filterToSet.getSearchCondition());
				if (filterToSet == null || filterToSet.isDefaultFilter()) {
					clearSearchCondition();
				}
				if (filterToSet != null && selectedVo.getSearchDeleted() != null) {
					setSearchDeleted(selectedVo.getSearchDeleted());
				}
				
				actSearchFilterSearchEnabled = false;
				actSearchFilterSearch.putValue(ToolBarItemAction.ITEM_SELECTED_INDEX, Math.max(0, filtersSearch.indexOf(filterToSet)));
				actSearchFilterSearchEnabled = true;					
			} catch (Exception ex) {
				Errors.getInstance().showExceptionDialog(getTab(), getSpringLocaleDelegate().getMessage(
						"NuclosCollectController.12","Suchbedingung kann nicht in der Suchmaske dargestellt werden."), ex);
			} finally {
				running.set(false);
			}
		}
	};

	protected void cmdSetCollectableSearchConditionAccordingToFilter() {
		if (!this.isSearchPanelAvailable()) {
			return;
		}

		UIUtils.runShortCommand(getTab(), setCollectableSearchConditionAccordingToFilter);
	}

	public void setSearchDeleted(Integer iSearchDeleted) {
		//override in GenericObjectCollectController
	}

	public void cmdExecuteRuleByUser(MainFrameTab iFrame, final UID entityUid, Clct clct) {
		if (this.getCollectState().getInnerState() != CollectStateConstants.DETAILSMODE_VIEW) {
			throw new IllegalStateException("view mode");
		}
		if (clct != null) {
			final SelectController controller = new SelectController(iFrame);
			final Collection<EventSupportSourceVO> collRules = getUserRules();
			final ChoiceList<EventSupportSourceVO> ro = new ChoiceList<EventSupportSourceVO>();
			ro.set(collRules,
					new Comparator<EventSupportSourceVO>() {
						@Override
						public int compare(EventSupportSourceVO o1, EventSupportSourceVO o2) {
							return o1.getClassname().compareToIgnoreCase(o2.getClassname());
						}
					}
			);
			controller.setModel(ro);
			if (controller.run(getSpringLocaleDelegate().getMessage("NuclosCollectController.11",
					"Regeln ausf\u00fchren"))) {
				//execute the selected Rules
				final List<EventSupportSourceVO> lstRuleToExecute = CollectionUtils.typecheck(
						controller.getSelectedObjects(), EventSupportSourceVO.class);
				if (lstRuleToExecute != null && !lstRuleToExecute.isEmpty()) {
					final Future<LayerLock> lock = lockFrame();
					final LockingResultListener<Exception> exResult = new LockingResultListener<Exception>() {
						
						@Override
						public void done(Exception result) {
							try {
								if (result == null) {
									//refresh the current object
									cmdRefreshCurrentCollectable();
								}

								if (result != null) {
									controller.setBlnExceptionOnWork(true);
									if (!handleSpecialException(result))
										Errors.getInstance().showExceptionDialog(controller.getParent(), result);
								}
							} finally {
								unLockFrame(getLayerLock());
							}
						}

						@Override
						public Future<LayerLock> getLayerLock() {
							return lock;
						}
					};

					executeBusinessRules(lstRuleToExecute, exResult);
				}
			}
		}
	}

	// TODO: The next methods concerning the execution of business rules is "migrated" from MasterDataCollectController
	// and GenericObjectCollectController. It is yet unclear, why some are doing the execution multi-thread and some
	// are not. Furthermore the parameters "applyMultiEditContext" and "bCollectiveProcessing" are anything but understandable.

	// TODO: Check for applyMultiEditContext because it is not used
	@Override
	public void executeBusinessRules(final List<EventSupportSourceVO> lstRuleVO, 
			final Clct clct, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		prepareCollectableForBusinessRuleExecution(clct);
		delegateBusinessRulesExecution(lstRuleVO, clct, getCustomUsage(), true);
	}

	@Override
	public void executeBusinessRules(final List<EventSupportSourceVO> lstRuleVO, final LockingResultListener<Exception> exResult) {
		if (getCollectState().isDetailsModeNew()) {
			try {
				if (!save()) {
					exResult.done(null);
					return;
				}
			} catch (final CommonBusinessException ex) {
				throw new NuclosFatalException("saving collectable failed", ex);
			}
		}

		if (getCollectState().isDetailsModeMultiViewOrEdit()) {
			try {
				RunCustomRuleSelectedCollectablesController selectedCollectablesController = new RunCustomRuleSelectedCollectablesController<>(this, lstRuleVO, exResult);
				final int iCount = getSelectedCollectables().size();
				selectedCollectablesController.run(getMultiActionProgressPanel(iCount));
			} catch (CommonBusinessException e) {
				//Fallback
				getSelectedCollectables().forEach(clct -> executeBusinessRules(lstRuleVO, exResult, clct, true));
			}
		} else {
			// get the edited state
			Clct clct = getCollectStateModel().getEditedCollectable();
			if (clct == null) {
				// .. or the selected state if the details have not been edited.
				clct = getSelectedCollectable() != null ? getSelectedCollectable() : newCollectableWithDefaultValues(false);
			}
			executeBusinessRules(lstRuleVO, exResult, clct, true);
		}
	}
	
	@Override
	public void executeBusinessRules(final List<EventSupportSourceVO> lstRuleVO, 
			final LockingResultListener<Exception> exResult, final Clct clct, final boolean inBackground) {
		boolean bCollectiveProcessing = false;
		//TODO decide if collective processing (this comment existed before)
		executeBusinessRulesMultiThreaded(lstRuleVO, exResult, clct, bCollectiveProcessing, inBackground, null);
	}

	// TODO: applyMultiEditContext is always null, but this is due to inconsistencies before
	private void executeBusinessRulesMultiThreaded(List<EventSupportSourceVO> lstRuleVO,
													 LockingResultListener<Exception> exResult, Clct clct,
													 boolean bCollectiveProcessing, boolean inBackground,
													 Map<String, Serializable> applyMultiEditContext) {
		MultiThreadedRunnable mtr = new MultiThreadedRunnable() {
			@Override
			public void run() throws CommonBusinessException {

				prepareCollectableForBusinessRuleExecution(clct);

				SwingWorker<Boolean, Object> worker = new SwingWorker<Boolean, Object>() {

					@Override
					protected Boolean doInBackground() {
						try {
							repeatAttributeContextSetting();
							delegateBusinessRulesExecution(lstRuleVO, clct, getCustomUsage(), bCollectiveProcessing);
							return true;
						} catch (Exception ex) {
							finishedWithError(ex);
							return false;
						}
					}

					@Override
					protected void done() {
						try {
							try {
								if (get()) {
									broadcastCollectableEvent(clct, MessageType.EDIT_DONE);
									executeBusinessRulesAfter();
									finished();
								}
							} catch (Exception e) {
								finishedWithError(e);
							}
						}
						finally {
							setDetailsChangedIgnored(false);
						}
					}
				};

				worker.execute();

				if (!inBackground) {
					try {
						worker.get();
					} catch (final InterruptedException | ExecutionException ex) {
						throw new NuclosFatalException("execution failed", ex);
					}
				}
			}
		};

		invokeForMultiThreaded(mtr, applyMultiEditContext, exResult);
	}

	private void prepareCollectableForBusinessRuleExecution(Clct clct) throws CommonBusinessException {

		setDetailsChangedIgnored(true);

		if (getCollectState().isDetailsMode()) {
			readValuesFromEditPanel(clct, false);
			prepareCollectableForSaving(clct, getCollectableEntityForDetails());
		}

		if (clct instanceof CollectableWithDependants) {
			if (getCollectState().isDetailsModeMultiViewOrEdit()) {
				DependantCollectableMasterDataMap map = getAdditionalDataForMultiUpdate(clct);
				if (map != null) {
					Utils.mapNullBooleansToFalseInDependents(map);
					((CollectableWithDependants) clct).setDependents(map.toDependentDataMap());
				}
			} else {
				((CollectableWithDependants) clct).setDependents(getAllSubFormData(clct.getId()).toDependentDataMap());
			}
		}

	}

	protected void delegateBusinessRulesExecution(List<EventSupportSourceVO> lstRuleVO, Clct clct, String custom,
												  boolean bCollectiveProcessing) throws CommonBusinessException {
		throw new NotImplementedException();
		// Overridden and implemented in MasterDataCollectController and GenericObjectCollectController
	};


	protected void executeBusinessRulesAfter() {
		// Override and or implement when needed
	}

	protected Map<UID, DetailsSubFormController<PK,CollectableEntityObject<PK>>> getMapOfSubFormControllersInDetails() {
		return Collections.emptyMap();
	}

	/**
	 * §postcondition result != null
	 */
	protected Collection<DetailsSubFormController<PK,CollectableEntityObject<PK>>> getSubFormControllersInDetails() {
		return CollectionUtils.valuesOrEmptySet(getMapOfSubFormControllersInDetails());
	}

	/**
	 * gathers the data from all enabled subforms. All rows are gathered, even
	 * the removed ones.
	 *
	 * @param oParentId
	 *            set as the parent id for each subform row.
	 * @return the data from all subforms
	 */
	protected final DependantCollectableMasterDataMap getAllSubFormData(PK oParentId) throws CommonValidationException {
		final DependantCollectableMasterDataMap result = new DependantCollectableMasterDataMap();

		final Collection<DetailsSubFormController<PK,CollectableEntityObject<PK>>> sfControllers =
				getSubFormControllersInDetails();

		for (DetailsSubFormController<PK,CollectableEntityObject<PK>> subformctl : sfControllers) {
			EntityMeta<?> eMeta = MetaProvider.getInstance().getEntity(subformctl.getCollectableEntity().getUID());
			// NUCLEUSINT-1119
			if (subformctl.getSubForm().getParentSubForm() == null) {
				boolean bSetParent = oParentId != null;
				List<CollectableEntityObject<PK>> sub
					= subformctl.getAllCollectables(oParentId, sfControllers, bSetParent, null);

				if (!eMeta.isEditable()) {
					sub = sub.stream().filter(CollectableEntityObject::hasDirtySubformData).collect(Collectors.toList());
				}

				IDependentKey dependentKey = DependentDataMap.createDependentKey(subformctl.getForeignKeyFieldUID());
				result.addValues(dependentKey, sub);
			}
		}

		return result;

	}

	/**
	 * is entity of this controller transferable, that means that collectables
	 * of this entity can be exported and imported.
	 *
	 * TODO: Make this protected again.
	 */
	@Override
	public boolean isTransferable() {
		boolean bTransferable = false;
		UID entityUid = getEntityUid();
		final MetaProvider mProv = MetaProvider.getInstance();
		
		if (SecurityCache.getInstance().isActionAllowed(Actions.ACTION_XML_EXPORT_IMPORT)) {
			EntityMeta<?> meta = mProv.getEntity(entityUid);
			bTransferable = meta.isImportExport();
			/*
			if (Modules.getInstance().isModule(entityUid)) {
				Object obj = Modules.getInstance().getModuleByEntityName(entityUid).getFieldValue("importexport");
				bTransferable = (obj != null) ? (Boolean)obj : false;
			}
			else {
				bTransferable = MasterDataDelegate.getInstance().getMetaData(entityUid).getIsImportExport();
			}
			 */
		}
		return bTransferable;
	}
	
	/**
	 *  helper class for cmdExecuteRulesByUser
	 */
	private static class SelectController extends SelectObjectsController<EventSupportSourceVO> {

		private boolean blnExceptionOnWork;

		public SelectController(Component parent) {
			super(parent, new DefaultSelectObjectsPanel<EventSupportSourceVO>());
			final DefaultSelectObjectsPanel<EventSupportSourceVO> panel = (DefaultSelectObjectsPanel<EventSupportSourceVO>) getPanel();
			panel.btnUp.setEnabled(true);
			panel.btnDown.setEnabled(true);
			panel.btnUp.setVisible(true);
			panel.btnDown.setVisible(true);
			blnExceptionOnWork = false;
		}

		public boolean isBlnExceptionOnWork() {
			return blnExceptionOnWork;
		}

		public void setBlnExceptionOnWork(boolean blnExceptionOnWork) {
			this.blnExceptionOnWork = blnExceptionOnWork;
		}


		DefaultSelectObjectsPanel<EventSupportSourceVO> getSelectObjectsPanel() {
			return (DefaultSelectObjectsPanel<EventSupportSourceVO>) getPanel();
		}

	}

	@Override
	protected boolean isDetailsModeViewLoadingWithoutDependants() {
		return false;
	}

	/**
	 * @return the user preferences node for this
	 */
	@Override
	public Preferences getPreferences() {
		return super.getPreferences();
	}

	@Override
	public EntityPreferences getEntityPreferences() {
		return super.getEntityPreferences();
	}

	@Override
	public Clct findCollectableById(UID entityUid, PK oId, final int dependantsDepth)
			throws CommonBusinessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Clct findCollectableById(UID entityUid, PK oId, Collection<FieldMeta<?>> fields, final int dependantsDepth) throws CommonBusinessException {
		return findCollectableById(entityUid, oId, dependantsDepth);
	}

	@Override
	protected Clct findCollectableByIdWithoutDependants(UID entityUid, PK oId) throws CommonBusinessException {
		// TODO Auto-generated method stub
		// return null;
		throw new UnsupportedOperationException();
	}

	@Override
	protected Clct updateCollectable(Clct clct, Object oAdditionalData, final Map<String, Serializable> applyMultiEditContext, boolean isCollectiveProcessing)
			throws CommonBusinessException {
		// TODO Auto-generated method stub
		// return null;
		throw new UnsupportedOperationException();
	}

	@Override
	protected Clct insertCollectable(Clct clctNew)
			throws CommonBusinessException {
		EventSupportDelegate.getInstance().invalidateCaches(E.NUCLET);
		// return null;
		throw new UnsupportedOperationException();
	}

	@Override
	protected void deleteCollectable(Clct clct, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		EventSupportDelegate.getInstance().invalidateCaches(E.NUCLET);
		throw new UnsupportedOperationException();
	}

	@Override
	protected String getEntityLabel() {
		// TODO Auto-generated method stub
		// return null;
		throw new UnsupportedOperationException();
	}

	@Override
	public Clct newCollectable() {
		// TODO Auto-generated method stub
		// return null;
		throw new UnsupportedOperationException();
	}

	public Collection<EventSupportSourceVO> getUserRules() {
		return Collections.emptyList();
	}

	@Override
	protected final void initTab() {
		super.initTab();
		getTab().setTabStoreController(new NuclosCollectController.NuclosCollectTabStoreController<PK>(this));
	}

	@Override
	public Pair<IconResolver, String> getIconAndResolver() {
		return Main.getInstance().getMainFrame().getEntityIconAndResolver(getEntityName());
	}
	
	@Override
	public String getCustomUsage() {
		return null;
	}

	@Override
	public void checkedRestoreCollectable(Clct clct, final Map<String, Serializable> applyMultiEditContext)
			throws CommonBusinessException {
		throw new IllegalStateException("restore not available");

	}
		
}	// class NuclosCollectController
