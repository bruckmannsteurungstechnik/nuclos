//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.common;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellRenderer;

import org.apache.log4j.Logger;
import org.nuclos.client.image.ImageScaler;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.collect.component.CollectableMediaComponent;
import org.nuclos.client.ui.labeled.LabeledImage;
import org.nuclos.client.ui.message.MessageExchange;
import org.nuclos.client.ui.message.MessageExchange.MessageExchangeListener;
import org.nuclos.common.INuclosImageProducer;
import org.nuclos.common.NuclosImage;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common2.IOUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.SystemUtils;

public class NuclosCollectableImage extends CollectableMediaComponent implements MessageExchangeListener {

	private static final Logger LOG = Logger.getLogger(NuclosCollectableImage.class);

	private NuclosImage nuclosImage;
	private boolean bScalable;
	private boolean keepAspectRatio;
	private int inputWidth = -1;
	private int inputHeight = -1;

	/**
	 * §postcondition this.isDetailsComponent()
	 */
	public NuclosCollectableImage(CollectableEntityField clctef) {
		this(clctef, false);
		assert this.isDetailsComponent();
	}

	public NuclosCollectableImage(CollectableEntityField clctef, boolean bSearchable) {
		super(clctef, new LabeledImage(), bSearchable);
		
		this.nuclosImage = new NuclosImage();
		
		MessageExchange.addListener(this);
		
		setupDragDrop();
		
		getLabeledComponent().addComponentListener(new ComponentAdapter() {
			
			@Override
			public void componentResized(ComponentEvent e) {
				try {
					// this is needed because labeled component can have size 0 on init.
					updateView(getModel().getField());
				} catch (Exception e2) {
					// ignore.
				}
			}
		});
	}

	protected void setupDragDrop() {
		DropTarget target = new DropTarget(this.getImageLabel(), new ImageLabelDragDropListener());
		target.setActive(true);
	}

	public JLabel getImageLabel() {
		return this.getMediaComponent();
	}

	@Override
	public CollectableField getFieldFromView() throws CollectableFieldFormatException {
		return new CollectableValueField(this.nuclosImage);
	}

	@Override
	protected void updateView(CollectableField clctfValue) {
		if (clctfValue.getValue() instanceof NuclosImage) {
			NuclosImage ni = (NuclosImage) clctfValue.getValue();
			if (ni == null) {
				clearField();
			} else {
				if (ni.getImage() != null) {
					ImageIcon ii;
					if (this.getLabeledComponent().getParent() == null) {
						
						if (ni.getThumbnail() == null) {
							ni.produceThumbnail();	
						}
						
						if (ni.getThumbnail() != null) {
							ii = new ImageIcon(ni.getThumbnail());
						} else {
							ii = new ImageIcon(ni.getImage());
						}
						
					} else {
						ii = new ImageIcon(ni.getImage());
					}

					int h = this.getLabeledComponent().getHeight();
					int w = this.getLabeledComponent().getWidth();

					this.getMediaComponent().setText(null);
					if (h == 0 || w == 0 || !bScalable) {
						this.getMediaComponent().setIcon(ii);
						this.getMediaComponent().setDisabledIcon(ii);
						this.getMediaComponent().setHorizontalAlignment(SwingConstants.CENTER);
						this.getMediaComponent().setVerticalAlignment(SwingConstants.CENTER);
					} else {
						if (keepAspectRatio) {
							double scalew = (double) w / ii.getIconWidth();
							double scaleh = (double) h / ii.getIconHeight();
							double scale = Math.min(scalew, scaleh);
							w = (int) (ii.getIconWidth() * scale);
							h = (int) (ii.getIconHeight() * scale);
						}
						// final Image imageScaled = ii.getImage().getScaledInstance(w, h, Image.SCALE_DEFAULT);
						final Image imageScaled = ImageScaler.scaleImage(ii.getImage(), w == 0 ? 1 : w, h == 0 ? 1 : h);
						ImageIcon icon = new ImageIcon(imageScaled);
						
						// set icon for both states, because if the component is disabled it should look the same way.
						this.getMediaComponent().setIcon(icon);
						this.getMediaComponent().setDisabledIcon(icon);
					}
					this.nuclosImage = ni;
					JComponent comp = NuclosCollectableImage.this.getJComponent();
					if (comp instanceof LabeledImage) {
						LabeledImage li = (LabeledImage) comp;
						li.setNuclosImage(ni);
					}
				} else {
					clearField();
				}
			}
		} else {
			clearField();
		}

		// Borders should be set from layout only...
		//getImageLabel().setBorder(new LineBorder(Color.BLACK));

		// ensure the start of the text is visible (instead of the end) when the
		// text is too long
		// to be fully displayed:

		this.adjustAppearance();
	}

	public void setInputWidth(int inputWidth) {
		this.inputWidth = inputWidth;
	}

	public void setInputHeight(int inputHeight) {
		this.inputHeight = inputHeight;
	}

	private void clearField() {
		nuclosImage = null;
		if (isEnabled()) {
			this.getMediaComponent().setText(SpringLocaleDelegate.getInstance().getMessage(
					"NuclosCollectableImage.1", "Bild hier fallen lassen"));
			this.getMediaComponent().setIcon(null);
			this.getMediaComponent().setDisabledIcon(null);
			this.getMediaComponent().setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
					"NuclosCollectableImage.1", "Bild hier fallen lassen"));
		} else {
			this.getMediaComponent().setToolTipText(null);
		}

		if (this.getJComponent() instanceof LabeledImage) {
			LabeledImage li = (LabeledImage) this.getJComponent();
			li.setNuclosImage(null);
			this.getMediaComponent().setIcon(null);
			this.getMediaComponent().setDisabledIcon(null);
		}
		adjustAppearance();
	}

	@SuppressWarnings("serial")
	@Override
	public JPopupMenu newJPopupMenu() {
		final JPopupMenu result = new JPopupMenu();
		result.add(new AbstractAction(SpringLocaleDelegate.getInstance().getMessage(
				"collectableimage.filechooser.1", "Bild \u00f6ffnen")) {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser chosser = new JFileChooser();
				chosser.setFileFilter(new ImageFileFilter());

				int choice = chosser.showOpenDialog(getLabeledComponent());
				if (choice == JFileChooser.CANCEL_OPTION)
					return;
				else {
					File file = chosser.getSelectedFile();
					try {
						loadImage(new FileInputStream(file));
					} catch (FileNotFoundException e1) {
						LOG.warn(e1);
					}
				}
			}

			@Override
			public boolean isEnabled() {
				try {return getControlComponent().isEnabled();} catch (Exception e) {return false;}				
			}
		});

		result.add(new AbstractAction(SpringLocaleDelegate.getInstance().getMessage(
				"collectableimage.filechooser.2", "Bild speichern")) {

			@Override
			public void actionPerformed(ActionEvent e) {
				JComponent comp = NuclosCollectableImage.this.getJComponent();
				if (comp instanceof LabeledImage) {
					LabeledImage li = (LabeledImage) comp;
					NuclosImage ni = li.getNuclosImage();
					String formatname = getFormatName(ni, "JPG");

					JFileChooser chosser = new JFileChooser();
					chosser.setFileSelectionMode(JFileChooser.FILES_ONLY);
					chosser.setFileFilter(new ImageFileFilter());
					chosser.setSelectedFile(new File("image." + formatname.toLowerCase()));
					int choice = chosser.showSaveDialog(getLabeledComponent());
					if (choice == JFileChooser.CANCEL_OPTION) {
						return;
					}

					File file = chosser.getSelectedFile();
					OutputStream fos = null;
					try {
						fos = new BufferedOutputStream(new FileOutputStream(file));
						fos.write(ni.getContent());
						fos.close();
					} 
					catch (FileNotFoundException ex) {
						Errors.getInstance().showExceptionDialog(getControlComponent(), ex);
					} 
					catch (IOException ex) {
						Errors.getInstance().showExceptionDialog(getControlComponent(), ex);
					}
					finally {
						if (fos != null) {
							try {
								fos.close();
							}
							catch (IOException e1) {
								// ignore
							}
						}
					}
				}
			}

			@Override
			public boolean isEnabled() {
				try {return !getField().equals(CollectableValueField.NULL);} catch (Exception e) {return false;}				
			}
		});

		result.add(new AbstractAction(SpringLocaleDelegate.getInstance().getMessage(
				"collectableimage.filechooser.3", "Bild anzeigen")) {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					JComponent comp = NuclosCollectableImage.this.getJComponent();
					if (comp instanceof LabeledImage) {
						LabeledImage li = (LabeledImage) comp;
						NuclosImage ni = li.getNuclosImage();
						String formatname = getFormatName(ni, "JPG");
						final java.io.File file = File.createTempFile("nuclos-image", "." + formatname.toLowerCase());
						IOUtils.writeToBinaryFile(file, ni.getContent());
						file.deleteOnExit();
						SystemUtils.open(file);
					}
				} catch (IOException ex) {
					Errors.getInstance().showExceptionDialog(getControlComponent(), ex);
				}
			}

			@Override
			public boolean isEnabled() {
				try {return !getField().equals(CollectableValueField.NULL);} catch (Exception e) {return false;}				
			}
		});

		result.add(new AbstractAction(SpringLocaleDelegate.getInstance().getMessage(
				"CollectableFileNameChooserBase.1", "zurücksetzen")) {

			@Override
			public void actionPerformed(ActionEvent e) {
				JComponent comp = NuclosCollectableImage.this.getJComponent();
				if (comp instanceof LabeledImage) {
					LabeledImage li = (LabeledImage) comp;
					li.setNuclosImage(null);
					updateView(new CollectableValueField(null));
					viewToModel(new CollectableValueField(null));
				}
			}

			@Override
			public boolean isEnabled() {
				try {return !getField().equals(CollectableValueField.NULL) && getControlComponent().isEnabled();} catch (Exception e) {return false;}				
			}

		});
		
		return result;
	}

	private String getFormatName(NuclosImage ni, String defaultformat) {
		return getFormatName(new ByteArrayInputStream(ni.getContent()), defaultformat);
	}

	private String getFormatName(InputStream is, String defaultformat) {
		try {
			ImageInputStream iis = ImageIO.createImageInputStream(is);
			Iterator<ImageReader> readers = ImageIO.getImageReaders(iis);
			if (readers.hasNext()) {
				return readers.next().getFormatName();
			}
		} catch (IOException e1) {
			LOG.warn("Error determining image format name.", e1);
		}
		return defaultformat;
	}

	public void loadImageFromIcon(ImageIcon icon) {
		BufferedImage buff = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
		buff.getGraphics().drawImage(icon.getImage(), 0, 0, null);
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			ImageIO.write(buff, "PNG", os);
			loadImage(new ByteArrayInputStream(os.toByteArray()));
		} catch (IOException e) {
			LOG.warn(e);
		}
	}
	
	private void loadImage(InputStream is) {
		this.getLabeledComponent().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		if (this.getLabeledComponent().getParent() != null) {
			this.getLabeledComponent().getParent().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		}
		
		try {
			String filename = "image";
			
			BufferedInputStream bis = new BufferedInputStream(is);
			byte b[] = new byte[bis.available()];
			int c = 0;
			int counter = 0;
			while ((c = bis.read()) != -1) {
				b[counter++] = (byte) c;
			}
			bis.close();
			
			Dimension d = null;
			if (inputWidth != -1 && inputHeight != -1) {
				d = new Dimension(inputWidth, inputHeight);
			}

			nuclosImage = new NuclosImage(filename, b, d);

			JComponent comp = NuclosCollectableImage.this.getJComponent();
			if (comp instanceof LabeledImage) {
				
				LabeledImage li = (LabeledImage) comp;
				ImageIcon ii = new ImageIcon(nuclosImage.getImage());
				if (li.getWidth() != 0 && li.getHeight() != 0) {
					final ImageIcon scaledImageForLabeled = new ImageIcon(ImageScaler.scaleImage(ii.getImage(), li.getWidth(), li.getHeight()));
					li.getJMediaComponent().setIcon(scaledImageForLabeled);
					li.getJMediaComponent().setDisabledIcon(scaledImageForLabeled);
					
				} else {
					li.getJMediaComponent().setIcon(ii);
					li.getJMediaComponent().setDisabledIcon(ii);
					
				}
				
				li.setNuclosImage(nuclosImage);				
				nuclosImage.produceThumbnail();
			}
			
			try {
				viewToModel();
				updateView(new CollectableValueField(nuclosImage));
				
			} catch (CollectableFieldFormatException e1) {
				Errors.getInstance().showExceptionDialog(this.getControlComponent(), e1);
				
			}
			
		} catch (FileNotFoundException e1) {
			Errors.getInstance().showExceptionDialog(this.getControlComponent(), e1);
		} catch (IOException e1) {
			Errors.getInstance().showExceptionDialog(this.getControlComponent(), e1);
		} catch (OutOfMemoryError e1) {
			LOG.warn("loadImage failed: " + e1, e1);
		} finally {
			this.getLabeledComponent().setCursor(Cursor.getDefaultCursor());
			if (this.getLabeledComponent().getParent() != null) {
				this.getLabeledComponent().getParent().setCursor(Cursor.getDefaultCursor());				
			}
		}
	}

	@Override
	public void setColumns(int iColumns) {

	}

	@Override
	protected void viewToModel() throws CollectableFieldFormatException {
		super.viewToModel();
	}

	@Override
	public void setComparisonOperator(ComparisonOperator compop) {
	}

	@Override
	public TableCellRenderer getTableCellRenderer(boolean subform) {
		final TableCellRenderer parentRenderer = super.getTableCellRenderer(subform);
		return new TableCellRenderer() {
			@Override
			public Component getTableCellRendererComponent(JTable tbl, Object oValue, boolean bSelected, boolean bHasFocus, int iRow, int iColumn) {
				Component comp = parentRenderer.getTableCellRendererComponent(tbl, oValue, bSelected, bHasFocus, iRow, iColumn);

				JLabel lbComp = new JLabel();
				if (oValue instanceof CollectableField) {
					CollectableField field = (CollectableField) oValue;
					if (field.getValue() instanceof NuclosImage) {
						final NuclosImage ni = (NuclosImage) field.getValue();
						if (ni.getThumbnail() == null)
							ni.produceThumbnail();
						
						if (ni.getThumbnail() != null) {
							ImageIcon ii = new ImageIcon(ni.getThumbnail());
							lbComp.setIcon(ii);
							lbComp.setDisabledIcon(ii);
							lbComp.setSize(ii.getIconWidth(), ii.getIconHeight());
							if (ii.getIconHeight() > INuclosImageProducer.DEFAULTTHUMBSIZE && tbl.getRowHeight() != ii.getIconHeight())
								tbl.setRowHeight(ii.getIconHeight());
						} else {
							lbComp.setText(ni.getName());
						}
					}
				}
				if (comp instanceof JLabel) {
					((JLabel) comp).setSize(lbComp.getSize());
					((JLabel) comp).setIcon(lbComp.getIcon());
					((JLabel) comp).setDisabledIcon(lbComp.getIcon());
					((JLabel) comp).setText(lbComp.getText());
					((JLabel) comp).setHorizontalAlignment(SwingConstants.CENTER);
					((JLabel) comp).setVerticalAlignment(SwingConstants.CENTER);
				}

				return comp;
			}

		};
	}

	@Override
	public void setScalable(boolean bln) {
		this.bScalable = bln;
	}

	@Override
	public void receive(Object id, ObjectType type, MessageType msg) {

	}

	@Override
	public void setKeepAspectRatio(boolean keepAspectRatio) {
		this.keepAspectRatio = keepAspectRatio;
	}

	class ImageFileFilter extends javax.swing.filechooser.FileFilter {
		@Override
		public boolean accept(File f) {
			final String sName = f.getName().toUpperCase();
			if (f.isDirectory() || sName.endsWith("JPG") || sName.endsWith("JPEG")
					|| sName.endsWith("BMP") || sName.endsWith("PNG") || sName.endsWith("GIF")
					|| sName.endsWith("SVG")) {
				return true;
			}
			return false;
		}

		@Override
		public String getDescription() {
			return SpringLocaleDelegate.getInstance().getMessage("filenameextensionfilter.1",
	    			"Bildformate") + " (*.bmp, *.gif, *.png, *.jpg, *.jpeg, *.svg)";
		}
	}

	class ImageLabelDragDropListener implements DropTargetListener {

		@Override
		public void dragEnter(DropTargetDragEvent dtde) {

		}

		@Override
		public void dragExit(DropTargetEvent dte) {
		}

		@Override
		public void dragOver(DropTargetDragEvent dtde) {
			if (!getControlComponent().isEnabled()) {
				dtde.rejectDrag();
				return;
			}
			Transferable trans = dtde.getTransferable();
			DataFlavor flavor[] = trans.getTransferDataFlavors();
			if (flavor != null && flavor.length > 0) {
				try {
					List<?> files = null;
					// search for file list flavor...
					for (int i = 0; i < flavor.length; i++) {
						if (flavor[i].isFlavorJavaFileListType()) {
							dtde.acceptDrag(dtde.getDropAction());
							return;
						}
					}
					if (trans.getTransferData(flavor[0]) instanceof ImageIcon) {
						dtde.acceptDrag(dtde.getDropAction());
						return;
					}
					dtde.rejectDrag();
				} catch (Exception e) {
					LOG.warn(e);
				}
			}
		}

		@Override
		public void drop(DropTargetDropEvent dtde) {
			try {
				dtde.acceptDrop(dtde.getDropAction());
				Transferable trans = dtde.getTransferable();

				DataFlavor flavor[] = trans.getTransferDataFlavors();

				for (int i = 0; i < flavor.length; i++) {
					Object obj = trans.getTransferData(flavor[i]);
					if (obj instanceof List) {
						List<?> files = (List<?>) trans.getTransferData(flavor[i]);
						if (files.size() == 1) {
							if (files.get(0) instanceof File) {
								File file = (File) files.get(0);
								String filename = file.getName().toUpperCase();
								if (filename.endsWith("JPG") || filename.endsWith("JPEG") || filename.endsWith("BMP") || filename.endsWith("PNG")
										|| filename.endsWith("GIF")) {
									loadImage(new FileInputStream(file));
								} else {
									dtde.rejectDrop();
								}
							}
						}
					} else if (obj instanceof ImageIcon) {
						ImageIcon icon = (ImageIcon) trans.getTransferData(flavor[i]);

						loadImageFromIcon(icon);
					}
				}
			} catch (Exception e) {
				LOG.warn(e);
			}
		}

		@Override
		public void dropActionChanged(DropTargetDragEvent dtde) { }
	}
} // class CollectableTextField
