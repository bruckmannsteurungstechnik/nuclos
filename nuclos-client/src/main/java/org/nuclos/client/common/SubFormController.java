//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.common;

import java.awt.AWTEvent;
import java.awt.EventQueue;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.EventListener;
import java.util.EventObject;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.prefs.Preferences;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import org.apache.log4j.Logger;
import org.nuclos.client.autonumber.AutonumberUiUtils;
import org.nuclos.client.common.LafParameterHelper.BoolLafParameterEditor;
import org.nuclos.client.common.LafParameterHelper.NotifyListener;
import org.nuclos.client.common.LafParameterHelper.StringLafParameterEditor;
import org.nuclos.client.common.LafParameterHelper.ToolBarLafParameterEditor;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.client.genericobject.ReportController;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.main.mainframe.MainFrameTabbedPane;
import org.nuclos.client.mandator.ClientMandatorContext;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.MainFrameTabController;
import org.nuclos.client.ui.SizeKnownEvent;
import org.nuclos.client.ui.SizeKnownListener;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.CollectableTableHelper;
import org.nuclos.client.ui.collect.component.CollectableComponentFactory;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModel;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelProvider;
import org.nuclos.client.ui.collect.model.CollectableEntityFieldBasedTableModel;
import org.nuclos.client.ui.collect.model.SortableCollectableTableModel;
import org.nuclos.client.ui.collect.subform.FixedColumnRowHeader.HeaderTable;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.client.ui.collect.subform.SubFormParameterProvider;
import org.nuclos.client.ui.collect.subform.SubFormTable;
import org.nuclos.client.ui.collect.subform.SubFormTableModel;
import org.nuclos.client.ui.collect.subform.SubformRowHeader;
import org.nuclos.client.ui.collect.subform.ToolbarFunction;
import org.nuclos.client.ui.collect.subform.ToolbarFunctionState;
import org.nuclos.client.ui.gc.IReferenceHolder;
import org.nuclos.client.ui.gc.ListenerUtil;
import org.nuclos.client.ui.table.TableCellEditorProvider;
import org.nuclos.client.ui.table.TableCellRendererProvider;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.FieldUtils;
import org.nuclos.common.LafParameter;
import org.nuclos.common.LafParameterStorage;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFieldNotInModelException;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.WorkspaceDescription2.EntityPreferences;
import org.nuclos.common.collect.ToolBarConfiguration;
import org.nuclos.common.collect.ToolBarItemGroup;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProviderFactory;
import org.nuclos.common.collect.collectable.CollectableUtils.GivenFieldOrderComparator;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.preferences.PreferencesProvider;
import org.nuclos.common.preferences.TablePreferencesManager;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;

/**
 * Controller for collecting or searching for dependant data (in a one-to-many relationship) in a subform.
 * 
 * §todo move to common?
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public abstract class SubFormController extends MainFrameTabController
		implements TableCellRendererProvider, TableCellEditorProvider, 
		SubFormParameterProvider, FocusActionListener, Closeable, IReferenceHolder {

	private static final Logger LOG = Logger.getLogger(SubFormController.class);

	/**
	 * this controller's subform
	 */
	private SubForm subform;

	/**
	 * the <code>CollectableEntity</code> of the subform.
	 */
	private final CollectableEntity clcte;
	
	/**
	 * Is this controller's subform searchable (to be used in a Search panel)?
	 */
	private final boolean bSearchable;
	private final CollectableComponentModelProvider clctcompmodelproviderParent;
	private final UID parentEntityUID;
	private final UID foreignKeyFieldUID;
	private final Preferences prefs;
	private TablePreferencesManager tablePrefsManager;
	private ListSelectionListener listselectionlistener;
	private final CollectableFieldsProviderFactory clctfproviderfactory;
	private ClientParameterProvider clientParameterProvider;
	protected boolean isIgnorePreferencesUpdate = true;
	
	/**
	 * The name of the entity of this subform as given in the preferences/workspace. (tp)
	 */
	protected final UID targetEntityFromPref;
	
	private boolean closed = false;
	
	private final List<Object> ref = new LinkedList<Object>();
	
	private UID mandatorUID;
	
	private Long intidForVLP;
	
	private final Map<UID, FieldMeta<?>> mpEntityFields;

	private ISubformHistoricalViewLookUp historyLookUp;
	
	private final NotifyListener<ToolBarConfiguration,String> toolBarChangeListener = new NotifyListener<ToolBarConfiguration,String>() {
		@Override
		public void valueChanged(LafParameter<ToolBarConfiguration,String> parameter, LafParameterStorage storage, UID entityUid, ToolBarConfiguration configuration) {
			subform.setToolBarItemGroup(configuration.getMainToolBar());
		}
	};
	
	/**
	 * §precondition prefsUserParent != null
	 * 
	 * @param tab
	 * @param clctcompmodelproviderParent provides the enclosing <code>CollectController</code>'s <code>CollectableComponentModel</code>s.
	 * This avoids handing the whole <code>CollectController</code> to the <code>SubFormController</code>.
	 * May be <code>null</code> if there are no dependencies from subform columns to fields of the main form.
	 * 
	 * @param subform
	 * @param bSearchable
	 * @param prefsUserParent the preferences of the parent (controller)
	 * @param clctfproviderfactory
	 */
	public SubFormController(CollectableEntity clcte, MainFrameTab tab, 
			CollectableComponentModelProvider clctcompmodelproviderParent, UID parentEntityUID, SubForm subform,
			boolean bSearchable, Preferences prefsUserParent, EntityPreferences entityPrefs, CollectableFieldsProviderFactory clctfproviderfactory) {

		super(tab);
		
		if (tab == null) {
			throw new IllegalArgumentException("tab must not be null");
		}
		
		mpEntityFields = MetaProvider.getInstance().getAllEntityFieldsByEntity(clcte.getUID());
		this.clcte = clcte;
		this.clctcompmodelproviderParent = clctcompmodelproviderParent;
		this.parentEntityUID = parentEntityUID;
		this.subform = subform;
		this.foreignKeyFieldUID = subform.getForeignKeyFieldName(parentEntityUID, clcte);
		this.bSearchable = bSearchable;
		final UID entityUid = subform.getEntityUID();
		this.prefs = prefsUserParent.node("subentity").node(entityUid.getString());
		this.targetEntityFromPref = entityPrefs.getEntity();
		this.tablePrefsManager = null;
		this.clctfproviderfactory = clctfproviderfactory;
		this.clientParameterProvider = ClientParameterProvider.getInstance();
		assert this.getCollectableEntity() != null;
		assert this.getCollectableEntity().getUID().equals(this.getSubForm().getEntityUID());

		// Inititialize listeners for toolbar actions:
		ListenerUtil.registerSubFormToolListener(subform, null, subformToolListener);

		this.setupListSelectionListener(subform);

		// set a provider for dynamic table cell editors and providers:
		subform.setTableCellEditorProvider(this);
		subform.setTableCellRendererProvider(this);

		// If this is a data subform indicate that the size is loading in the
		// corresponding tab.
		if (!bSearchable) {
			SizeKnownListener listener = subform.getSizeKnownListener();
			if (listener != null) {
				listener.actionPerformed(new SizeKnownEvent(subform, null));
			}
		}
		subform.addFocusActionListener(this);
		
		subform.addColumnModelListener(newSubFormTablePreferencesUpdateListener());
	}
	/*
	@Autowired
	public void setClientParameterProvider(final ClientParameterProvider clientParameterProvider) {
		this.clientParameterProvider = clientParameterProvider;
	}
	*/
		
	public boolean isClosed() {
		return closed;
	}

	@Override
	public void close() {
		if (!closed) {
			LOG.debug("close(): " + this);
			removeListSelectionListener(this.getJTable());
			subform.removeSubFormToolListener(subformToolListener);
			subform.setSubFormCtrlLateInit(null);
			
			subform.close();
			subform = null;
			
			ref.clear();
			closed = true;
		}
	}
	
	@Override
	public void addRef(EventListener o) {
		ref.add(o);
	}
	
	@Override
	public final UID getParentEntityUid() {
		return parentEntityUID;
	}
	
	public void setIgnorePreferencesUpdate(boolean ignore) {
		this.isIgnorePreferencesUpdate = ignore;
	}
	
	protected PreferencesUpdateListener newSubFormTablePreferencesUpdateListener() {
		return new PreferencesUpdateListener();
	}

	public boolean isIgnorePreferencesUpdate() {
		return isIgnorePreferencesUpdate;
	}

	protected class PreferencesUpdateListener extends TimedPreferencesUpdater implements TableColumnModelListener  {
		@Override
		public void columnSelectionChanged(ListSelectionEvent ev) {
			if (!isIgnorePreferencesUpdate) {
//				storeColumnOrderAndWidths(getJTable());
			}
		}
		
		@Override
		public void columnMoved(TableColumnModelEvent ev) {
			if (ev.getFromIndex() != ev.getToIndex() && !isIgnorePreferencesUpdate) {
				addFieldsAndWidthsWorker();
			}
		}
		
		@Override
		public void columnMarginChanged(ChangeEvent ev) {
			if (!isIgnorePreferencesUpdate) {
				addFieldsAndWidthsWorker();
			}
		}
		
		@Override
		public void columnAdded(TableColumnModelEvent ev) {
		}
		
		@Override
		public void columnRemoved(TableColumnModelEvent ev) {
		}

		private void addFieldsAndWidthsWorker() {
			super.registerWorker(new PreferencesStoreWorker(getTablePreferencesManager().getSelected().getUID()) {
				@Override
				public void storePreferences() {
					storeColumnOrderAndWidths(getJTable());
				}
			});
		}
	}

	private final SubForm.SubFormToolListener subformToolListener = new SubForm.SubFormToolListener() {
		@Override
		public void toolbarAction(String actionCommand) {
			ToolbarFunction cmd = ToolbarFunction.fromCommandString(actionCommand);
			if(cmd != null) {
				switch(cmd) {
					case NEW:
						cmdInsert(); break;
					case NEW_AT_POSITION:
						int idxInsert = getJTable().getSelectedRow();
						cmdInsertAtPosition(idxInsert);
						break;					
					case REMOVE:
						cmdRemove(); break;
					case PRINTREPORT:
						cmdPrint(); break;				
				}
			}
		}
	};
	
	public final SubForm getSubForm() {
		return subform;
	}

	public final JTable getJTable() {
		return subform.getJTable();
	}

	/**
	 * @return the table model that contains the data for the subform.
	 */
	protected abstract SubFormTableModel getSubFormTableModel();

	/**
	 * @return the <code>CollectableEntity</code> for the data that is to be collected or searched for via this subform.
	 */
	public final CollectableEntity getCollectableEntity() {
		return clcte;
	}

	/**
	 * @return the entity name and foreign key field name used to uniquely 
	 * 		identify this subform in its <code>CollectController</code>.
	 */
	public final EntityAndField getEntityAndForeignKeyField() {
		EntityAndField entityAndField = new EntityAndField(getCollectableEntity().getUID(), getForeignKeyFieldUID());
		entityAndField.setMapParams(subform.getMapParams());
		return entityAndField;
	}

	/**
	 * @return Is this controller's subform searchable (to be used in a Search panel)?
	 */
	protected boolean isSearchable() {
		return this.bSearchable;
	}

	public final Preferences getPrefs() {
		return this.prefs;
	}
	
	protected final TablePreferencesManager getTablePreferencesManager() {
		if (tablePrefsManager == null) {
			tablePrefsManager = PreferencesProvider.getInstance().getTablePreferencesManagerForSubformEntity(subform.getEntityUID(),
					subform.getLayoutUID(),
					Main.getInstance().getMainController().getUserName(), ClientMandatorContext.getMandatorUID())
					.validate(MetaProvider.getInstance());
		}
		return this.tablePrefsManager;
	}

	public final UID getParentEntityUID() {
		return this.parentEntityUID;
	}

	protected final MainFrameTabbedPane getMainFrameTabbedPane() {
		return MainFrame.getTabbedPane(getTab());
	}

	/**
	 * sets all column widths to user preferences; set optimal width if no preferences yet saved
	 */
	protected final void setColumnWidths() {
		LOG.trace("setColumnWidths");
		isIgnorePreferencesUpdate = true;
		getSubForm().setColumnWidths(this.getTableColumnWidthsFromPreferences());
		isIgnorePreferencesUpdate = false;
	}

	/**
	 * @return the table columns widths. If there are stored user preferences, the sizes will be restored.
	 * Size and order of list entries is determined by number and order of visible columns
	 */
	protected List<Integer> getTableColumnWidthsFromPreferences() {
		List<Integer> result = getTablePreferencesManager().getColumnWidthsWithoutFixed();

		if (LOG.isTraceEnabled()) {
			LOG.trace("getTableColumnWidthsFromPreferences for entity " + this.getSubForm().getEntityUID());
			for (Object o : result) {
				LOG.trace("getTableColumnWidthsFromPreferences: column width = " + o);
			}
		}

		return result;
	}

	/**
	 * stores the order of the columns in the table
	 */
	protected void storeColumnOrderAndWidths(JTable tbl) {
		if (!isSearchable()) {
			final List<UID> lstFields = CollectableTableHelper.getFieldUIDsFromColumns(tbl);
			final List<Integer> lstFieldWidths = CollectableTableHelper.getColumnWidths(tbl);
			getTablePreferencesManager().setColumnPreferences(lstFields, lstFieldWidths, null);
		}
	}
	
	public List<CollectableEntityField> getPublicTableColumns() {
		return getTableColumns();
	}

	/**
	 * @return the table columns. If there are stored user preferences, the column order will be restored.
	 * By default, the table column order is controlled by the record order of entities in table t_ad_masterdata_field
	 */
	protected List<CollectableEntityField> getTableColumns() {
		final List<CollectableEntityField> allFields = new ArrayList<CollectableEntityField>();
		
		final boolean bSuperUser = SecurityCache.getInstance().isSuperUser();

		final UID foreignKeyFieldUid = getForeignKeyFieldUID();
		for (UID fieldUid : getCollectableEntity().getFieldUIDs()) {
			if (getSubForm().isColumnVisible(fieldUid) && !fieldUid.equals(foreignKeyFieldUid)) {
				if (!FieldUtils.hideField(fieldUid, mpEntityFields, bSuperUser)) {
					allFields.add(this.getCollectableEntity().getEntityField(fieldUid));					
				}
			}
		}
		
		final List<UID> definitionOrder = new ArrayList<UID>(getSubForm().getColumnUIDs());
		final List<UID> storedFieldNames = getTablePreferencesManager().getSelectedWithoutFixedColumns();
		
		final List<CollectableEntityField> availableFields = new ArrayList<CollectableEntityField>(allFields); 
		final List<CollectableEntityField> result = new ArrayList<CollectableEntityField>();
		
		if (storedFieldNames.isEmpty()) {
			// try to order by meta data
			try {
				List<FieldMeta<?>> efsOrdered = CollectionUtils.sorted(
						MetaProvider.getInstance().getAllEntityFieldsByEntity(getCollectableEntity().getUID()).values(),
						new Comparator<FieldMeta<?>>() {
							@Override
							public int compare(FieldMeta<?> o1, FieldMeta<?> o2) {
								Integer order1 = (o1.getOrder()==null)?0:o1.getOrder();
								Integer order2 = (o2.getOrder()==null)?0:o2.getOrder();
								return order1.compareTo(order2);
							}
						}); 
				
				for (FieldMeta<?> efMeta : efsOrdered) {
					for (CollectableEntityField clctef : new ArrayList<CollectableEntityField>(availableFields)) {
						if (LangUtils.equal(efMeta.getUID(), clctef.getUID())) {
							boolean add = true;
							
							if (SF.isEOField(efMeta.getEntity(), efMeta.getUID())) {
								add = false;
							}
							if (efMeta.isCalculated()) {
								add = false;
							}
							// NUCLOS-1798
							if (String.class.getName().equals(efMeta.getDataType()) && 
									(efMeta.getScale() == null || efMeta.getScale() > 255)) {
								add = false;
							}
							
							if (add) {
								result.add(clctef);
								availableFields.remove(clctef);
								break;
							}
						}
					}
				}
			} catch (Exception ex) {
				LOG.warn("no column order from meta data", ex);
			}
			
		} else {
			// restore order from preferences
			for (UID storedFieldName : storedFieldNames) {
				for (CollectableEntityField clctef : new ArrayList<CollectableEntityField>(availableFields)) {
					if (LangUtils.equal(storedFieldName, clctef.getUID())) {
						result.add(clctef);
						availableFields.remove(clctef);
						break;
					}
				}
			}
		}
		
		Comparator<CollectableEntityField> comparator = new GivenFieldOrderComparator(definitionOrder);
		Collections.sort(availableFields, comparator);
		result.addAll(availableFields);

		return result;
	}
	
	/**
	 * makes sure the given list of selected fields is non-empty. If the list is empty, this method adds one field to it.
	 * This is to avoid a seemingly empty search result, which might be irritating to the user.
	 * <p>
	 * TODO: Make this protected again.
	 * 
	 * §precondition clcte != null
	 * §precondition lstclctefSelected != null
	 * §postcondition !lstclctefSelected.isEmpty()
	 * 
	 * @param clcte
	 * @param lstclctefSelected
	 */
	public void makeSureSelectedFieldsAreNonEmpty(CollectableEntity clcte, List<CollectableEntityField> lstclctefSelected) {
		if (lstclctefSelected.isEmpty()) {
			
			List<UID> fieldUids = new ArrayList<UID>();
			fieldUids.addAll(clcte.getFieldUIDs());
			
			CollectableEntityField sysStateIcon = null;
			CollectableEntityField sysStateNumber = null;
			CollectableEntityField sysStateName = null;
			
			Set<UID> clcteFieldUids = clcte.getFieldUIDs();
			for (UID fieldUid : fieldUids) {
				if (!clcteFieldUids.contains(fieldUid)) {
					LOG.warn("Field " + fieldUid + " in collectable entity " + clcte.getUID() + " does not exists");
					continue;
				}

				CollectableEntityField clctef = clcte.getEntityField(fieldUid);
				boolean select = true;
				
				if (SF.isEOField(clctef.getEntityUID(), fieldUid)) {
					select = false;
					if (fieldUid.equals(SF.STATEICON.getUID(clcte.getUID()))) {
						sysStateIcon = clctef;	
					} else if (fieldUid.equals(SF.STATENUMBER.getUID(clcte.getUID()))) {
						sysStateNumber = clctef;	
					} else if (fieldUid.equals(SF.STATE.getUID(clcte.getUID()))) {
						sysStateName = clctef;	
					}
				}
				if (select) {
					lstclctefSelected.add(clctef);
				}
			}
			
			if (sysStateIcon != null)
				lstclctefSelected.add(sysStateIcon);
			if (sysStateNumber != null)
				lstclctefSelected.add(sysStateNumber);
			if (sysStateName != null)
				lstclctefSelected.add(sysStateName);
			
			final UID sForeignKeyFieldUid = this.getForeignKeyFieldUID();
			for (CollectableEntityField clctef : new ArrayList<CollectableEntityField>(lstclctefSelected)) {
				final UID fieldUid = clctef.getUID();
				boolean remove = false;
				if (!getSubForm().isColumnVisible(fieldUid))
					remove = true;
				if (fieldUid.equals(sForeignKeyFieldUid))
					remove = true;
				
				if (remove)
					lstclctefSelected.remove(clctef);
			}
			
			if (lstclctefSelected.isEmpty()) {
				lstclctefSelected.add(clcte.getEntityField(SF.CHANGEDBY.getUID(clcte.getUID())));
			}
		}
	}

	/**
	 * removes the columns whose names are not contained in <code>lstStoredFieldNames</code> from the given table's column model.
	 */
	protected final void removeColumnsFromTableColumnModel(JTable tbl, List<UID> lstRemoveStoredFieldUids, boolean remove) {
		final TableColumnModel columnmodel = tbl.getColumnModel();

		final Set<TableColumn> stColumnsToRemove = new HashSet<TableColumn>();
		int iCountColumns = 0;
		for (Enumeration<TableColumn> enumcolumn = columnmodel.getColumns(); enumcolumn.hasMoreElements();) {
			final TableColumn column = enumcolumn.nextElement();
			iCountColumns++;
			final CollectableEntityField clctef = ((CollectableEntityFieldBasedTableModel) tbl.getModel()).getCollectableEntityField(column.getModelIndex());

			if (remove && !lstRemoveStoredFieldUids.contains(clctef.getUID())) {
				stColumnsToRemove.add(column);
			}
			if(!remove && lstRemoveStoredFieldUids.contains(clctef.getUID())){
				stColumnsToRemove.add(column);
			}

		}

		// check before the table has no more columns
		if (iCountColumns == stColumnsToRemove.size()) {
			// fallback: remove nothing!
			return;
		}

		for (TableColumn column : stColumnsToRemove) {
			columnmodel.removeColumn(column);
		}
	}

	/**
	 * §postcondition result != null
	 * §todo try to eliminate this on (or at least, make it optional). Why does a Collectable collected in a subform have
	 * to have a foreign key field to the "parent entity"? In the subform (view), the relation to the parent is clear.
	 * The "parent id" is not needed before we actually store the model in the database.
	 * Considering the usages of "getForeignKeyFieldName()", it's mostly used to <i>exclude</i> this field from the list of all fields!
	 * Historically, this method was necessary because the master data mechanism demands a foreign key field.
	 * 
	 * @return the name of the foreign key field referencing the parent entity
	 */
	public final UID getForeignKeyFieldUID() {
		return foreignKeyFieldUID;
	}

	public boolean isCellEditing() {
		return this.getJTable().isEditing();
	}

	public boolean isFixedCellEditing() {
		return this.getSubForm().getSubformRowHeader().getHeaderTable().isEditing();
	}

	public boolean stopEditing() {
		boolean result = true;

		if (this.isCellEditing()) {
			if (!this.getJTable().getCellEditor().stopCellEditing()) {
				result = false;
			}
		}
		else if (this.isFixedCellEditing()) {
			if (!this.getSubForm().getSubformRowHeader().getHeaderTable().getCellEditor().stopCellEditing()) {
				result = false;
			}
		}

		return result;
	}

	public void cancelEditing() {
		if (this.getJTable().isEditing()) {
			this.getJTable().getCellEditor().cancelCellEditing();
		}
	}

	protected final void setupTableCellRenderers(SubFormTableModel subformtblmdl) {
		this.getSubForm().setupTableCellRenderers(this.getCollectableEntity(), subformtblmdl, this.getCollectableFieldsProviderFactory(), this, this.isSearchable());
	}

	protected final void setupRowHeight(SubFormTableModel subformtablemodel) {
		if (this.getPrefs().getInt(TableRowIndicator.SUBFORM_ROW_HEIGHT, -20) == -20) {
			if (this.getSubForm().isDynamicRowHeightsDefault()) {
				this.getSubForm().setRowHeight(SubForm.DYNAMIC_ROW_HEIGHTS);
			} else {
				this.getSubForm().setRowHeight(this.getPreferredRowHeight(subformtablemodel));
			}
		}
	}

	private int getPreferredRowHeight(SubFormTableModel subformtablemodel) {
		final JTable tbl = this.getJTable();
		int result = getSubForm().getMinRowHeight();
		for (Enumeration<TableColumn> enumeration = tbl.getColumnModel().getColumns(); enumeration.hasMoreElements();) {
			final TableColumn column = enumeration.nextElement();
			final CollectableEntityField clctef = subformtablemodel.getCollectableEntityField(column.getModelIndex());
			result = Math.max(result, this.getSubForm().getTableCellRenderer(clctef).getTableCellRendererComponent(tbl, subformtablemodel.getNullValue(clctef),
					true, true, 0, 0).getPreferredSize().height);
		}
		return result;
	}

	/**
	 * Command: insert a new row.
	 */
	public void cmdInsert() {
		if (isEnabled()
				&& getSubForm().getToolbarButton("NEW").isEnabled()) {
			// TODO: check if this really must be encapsuled with runCommand
			try {
				if (stopEditing()) {
					JTable tbl = getJTable();
					Collectable<?> clct = insertNewRow();
					int rowIndex = indexOf(clct);
					if (rowIndex < 0) {
						rowIndex = tbl.getRowCount() - 1;
					}
					// Hotfix for 'Strukturdefinition' -> Subform Attribute -> add row (to empty subform) (tp)
					if (rowIndex >= 0) {
						tbl.addRowSelectionInterval(rowIndex, rowIndex);
					}
				}
			} catch (CommonBusinessException e) {
				Errors.getInstance().showExceptionDialog(getParent(), e);
			}
			
			if (!isSearchable()) {
				AutonumberUiUtils.fixSubFormOrdering(this.getSubForm().getSubformTable());
			}
		}
	}
	
	protected int indexOf(Collectable<?> clct) {
		JTable tbl = getJTable();
		TableModel model = tbl.getModel();
		if (model instanceof SortableCollectableTableModel) {
			SortableCollectableTableModel<?,?> sortableModel = (SortableCollectableTableModel<?,?>) model;
			for (int i=0; i<tbl.getRowCount(); i++) {
				if (sortableModel.getRow(i) == clct) {
					return i;
				}
			};
		}
		return -1;
	}
	
	public void cmdInsertAtPosition(int iIdxInsert) {
		if (isEnabled() 
				&& getSubForm().getToolbarButton("NEW_AT_POSITION").isEnabled()) {
			// TODO: check if this really must be encapsuled with runCommand
			try {
				if (stopEditing()) {
					int iIdx = iIdxInsert; 
					insertNewRow(iIdx);
					// select inserted row
					getSubForm().getJTable().getSelectionModel().setSelectionInterval(iIdx, iIdx);
					
				}
			} catch (CommonBusinessException e) {
				Errors.getInstance().showExceptionDialog(getParent(), e);
			}
		}
	}
	
	protected abstract Collectable insertNewRow() throws CommonBusinessException;

	/**
	 * insert new row at specified row index
	 * @param idx	row index
	 * @return inserted collectable
	 * @throws CommonBusinessException
	 */
	protected abstract Collectable insertNewRow(int idx) throws CommonBusinessException;

	/**
	 * Command: removes the selected row
	 */
	public void cmdRemove() {
		UIUtils.runCommandForTabbedPane(this.getMainFrameTabbedPane(), new Runnable() {
			@Override
            public void run() {
				if (stopEditing()) {
					final int iRowCount = getJTable().getSelectedRowCount();
					// NUCLOS-1480
					boolean bDelete = true;
					if (Boolean.TRUE.equals(Boolean.valueOf(clientParameterProvider.getValue("SUBFORM_CONFIRMATION_ON_DELETE")))) {
						final String sMessage = getSpringLocaleDelegate().getMessage("SubForm.12","Sollen die ausgew\u00e4hlten {0} Datens\u00e4tze wirklich gel\u00f6scht werden?", iRowCount);
						final int btn = JOptionPane.showConfirmDialog(getTab(), sMessage, null,JOptionPane.YES_NO_OPTION);
						if (btn != JOptionPane.YES_OPTION) {
							bDelete = false;
						} 
					} else {
					}
					if (bDelete) {
						removeSelectedRows();
					}
					
				}
			}
		});
	}

	public void cmdPrint() {
		UIUtils.runCommandForTabbedPane(this.getMainFrameTabbedPane(), new Runnable() {
			@Override
			public void run() {
				try {
					new ReportController(getTab()).export(getCollectableEntity().getUID(), null, getJTable(), subform.getSubformRowHeader().getHeaderTable(), null);
				} catch (NuclosBusinessException e) {
					LOG.warn("cmdPrint failed: " + e, e);
				}
			}
		});
	}
	
	protected void removeSelectedRows() {
		final JTable tbl = this.getJTable();
		int[] viewIndices;

		synchronized (getSubFormTableModel()) {
			viewIndices = tbl.getSelectedRows();
			int[] modelIndices = new int[viewIndices.length];
			for (int i = 0; i < viewIndices.length; i++) {
				modelIndices[i] = tbl.convertRowIndexToModel(viewIndices[i]);
			}

			this.getSubFormTableModel().remove(modelIndices);
		}

		// Select the nearest row if any (that may then be deleted next etc.)
		int rowCount = tbl.getRowCount();
		if (rowCount > 0 && viewIndices.length > 0) {
			if (tbl.getRowCount() > viewIndices[0]) {
				tbl.setRowSelectionInterval(viewIndices[0], viewIndices[0]);
			} else {
				tbl.setRowSelectionInterval(rowCount - 1, rowCount - 1);
			}
		}
	}

	protected final boolean isEnabled() {
		return !this.getSubForm().isReadOnly() && this.getSubForm().isEnabled();
	}

	public class FocusListSelectionListener implements ListSelectionListener {

		private final SubFormTable tbl;
		private final KeyStroke tabKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0);

		public FocusListSelectionListener(SubFormTable tbl) {
			this.tbl = tbl;
		}

		@Override
		public void valueChanged(final ListSelectionEvent e) {
			AWTEvent currentEvent = EventQueue.getCurrentEvent();
	        if(currentEvent instanceof KeyEvent){
	        	if (!getSubForm().getToolbarButton("NEW").isEnabled())
	            	return;
	            
	        	final KeyEvent ke = (KeyEvent)currentEvent;
	            if(!KeyStroke.getKeyStrokeForEvent(ke).equals(tabKeyStroke) || ke.isConsumed())
	            	return;
	            if(ke.isShiftDown() || ke.isControlDown())
	            	return;
	            int rowIndex = e.getFirstIndex();
	            int columnIndex = tbl.getSelectedColumn() == tbl.getColumnCount() -1 ? 0 : tbl.getSelectedColumn();
	            if(((rowIndex >= 0) || (rowIndex == 0 && columnIndex == 0)) && e.getLastIndex() > 0) {
		            ke.consume();
		            int colIndex = 0;
		            final UID identifier = (UID)tbl.getColumnModel().getColumn(tbl.getSelectedColumn()).getIdentifier();
					final UID sNextColumn = tbl.getSelectedColumn() == -1 ? null : 
						getSubForm().getColumnNextFocusField(identifier);
					if (sNextColumn != null) {
						colIndex = sNextColumn == null ? colIndex : tbl.getColumnModel().getColumnIndex(sNextColumn);
					}
		            int idxRow = e.getLastIndex(); 
		            if (e.getLastIndex() > 0) {
		            	SubFormController.this.cmdInsert();		            	
		            	idxRow = idxRow + 1;
		            	AutonumberUiUtils.fixSubFormOrdering(getSubForm().getSubformTable());
		            }
		            final int iRow = idxRow;
					final int cIndex = colIndex;
		            SwingUtilities.invokeLater(new Runnable() {

						@Override
						public void run() {
							SubformRowHeader rowHeader = tbl.getSubForm().getSubformRowHeader();
							boolean blnHasFixedRows = (rowHeader != null && rowHeader.getHeaderTable().getColumnCount() > 1);
							
							boolean bUseHeaderTable = false;
							try {
								if (sNextColumn != null)
									tbl.getColumn(sNextColumn);
							} catch (IllegalArgumentException e) {
								bUseHeaderTable = true;
							}
							if (iRow < tbl.getRowCount()) {
								if (!blnHasFixedRows || !bUseHeaderTable) 
										tbl.changeSelection(iRow, cIndex, false, false);
								else {
									if (!(rowHeader.getHeaderTable() instanceof HeaderTable))
										rowHeader.getHeaderTable().changeSelection(iRow, cIndex, false, false);
									else
										((HeaderTable)rowHeader.getHeaderTable()).changeSelection(iRow, cIndex, false, false, true);
								}
							}
						}
					});
	            }
	            // focus change with keyboard
	        }
		}
	}

	protected class SubformListSelectionListener implements ListSelectionListener {
		private final JTable tbl;

		public SubformListSelectionListener(JTable tbl) {
			this.tbl = tbl;
		}

		@Override
        public void valueChanged(ListSelectionEvent ev) {
			final ListSelectionModel lsm = (ListSelectionModel) ev.getSource();
			boolean bWriteEnabled = !lsm.isSelectionEmpty() && SubFormController.this.isEnabled();
			boolean bReadEnabled = ! lsm.isSelectionEmpty();
			for (int i : tbl.getSelectedRows()) {
				int index = i;
				if (!(i < 0 || i >= getSubForm().getSubformTable().getRowCount()))
					index = getSubForm().getSubformTable().convertRowIndexToModel(i);
				if (!isRowRemovable(index)) {
					bWriteEnabled = false;
					break;
				}
			}
			bWriteEnabled = bWriteEnabled && tbl.getSelectedRowCount() > 0;
			bReadEnabled = bReadEnabled && tbl.getSelectedRowCount() > 0;
			
			boolean bDetailZoomEnabled = bReadEnabled && tbl.getSelectedRowCount() == 1;

			// is entity at least readable ?
			bDetailZoomEnabled = bDetailZoomEnabled && SecurityCache.getInstance().isReadAllowedForEntity(clcte.getUID());
			// layout exist for entity ?
			bDetailZoomEnabled = bDetailZoomEnabled && MasterDataDelegate.getInstance().getLayoutUid(clcte.getUID(), false) != null; 	
				
			subform.setToolbarFunctionState(ToolbarFunction.REMOVE, bWriteEnabled ? ToolbarFunctionState.ACTIVE : ToolbarFunctionState.DISABLED);
			subform.setToolbarFunctionState(ToolbarFunction.LOCALIZATION, bReadEnabled ? ToolbarFunctionState.ACTIVE : ToolbarFunctionState.DISABLED);
			subform.setToolbarFunctionState(ToolbarFunction.CUT_ROW, bWriteEnabled ? ToolbarFunctionState.ACTIVE : ToolbarFunctionState.DISABLED);
			subform.setToolbarFunctionState(ToolbarFunction.COPY_ROW, bReadEnabled ? ToolbarFunctionState.ACTIVE : ToolbarFunctionState.DISABLED);
			subform.setToolbarFunctionState(ToolbarFunction.SUBFORM_DETAILS_ZOOM, bDetailZoomEnabled ? ToolbarFunctionState.ACTIVE : ToolbarFunctionState.DISABLED);
			
			// scroll table if necessary:
			if (tbl.getAutoscrolls()) {
				final int iRowIndex = lsm.getAnchorSelectionIndex();
				final int iColumnIndex = tbl.getSelectedColumn();
				final int iRowNumber = tbl.getRowCount();

				if (ev.getValueIsAdjusting() || iColumnIndex >= 0 || iRowIndex != iRowNumber -1) {
					return;
				}

				Collectable clc = getSelectedCollectable();
				if (clc == null || clc.getId() != null) {
					return;
				}

				final Rectangle cellRect = tbl.getCellRect(iRowIndex, iColumnIndex != -1 ? iColumnIndex : 0, true);
				if (cellRect != null && (iRowIndex < 0 || iColumnIndex < 0
						|| iColumnIndex >= tbl.getModel().getColumnCount() || iRowIndex >= tbl.getModel().getRowCount()
						|| tbl.convertColumnIndexToView(iColumnIndex) == -1 || tbl.convertRowIndexToView(iRowIndex) == -1)) {
					tbl.scrollRectToVisible(cellRect);
				}
			}
		}
	}

	protected abstract <PK> Collectable<PK> getSelectedCollectable();

	private void setupListSelectionListener(final SubForm subform) {
		final SubFormTable tbl = subform.getSubformTable();

		// initialize listener for row selection in the table:
		this.listselectionlistener = new SubformListSelectionListener(tbl);

		tbl.getSelectionModel().addListSelectionListener(listselectionlistener);

		tbl.setFocusListSelectionListener(new FocusListSelectionListener(tbl));
	}

	private void removeListSelectionListener(final JTable tbl) {
		tbl.getSelectionModel().removeListSelectionListener(this.listselectionlistener);
		this.listselectionlistener = null;
	}

	protected final void setupTableModelListener() {
		this.getSubForm().setupTableModelListener();
	}

	protected void removeTableModelListener() {
		if (getSubForm() != null) {
			getSubForm().removeTableModelListener();
		}
	}

	protected final void setupColumnModelListener() {
		this.getSubForm().setupColumnModelListener();
	}

	protected void removeColumnModelListener() {
		if (getSubForm() != null) {
			getSubForm().removeColumnModelListener();
		}
	}

	CollectableFieldsProviderFactory getCollectableFieldsProviderFactory() {
		return this.clctfproviderfactory;
	}

	/**
	 * Implementation of <code>TableCellRendererProvider</code>.
	 * Subclasses may do special rendering for specific fields here.
	 * @param clctefTarget
	 * @return this.getSubForm().getTableCellRenderer(clctefTarget);
	 */
	@Override
    public TableCellRenderer getTableCellRenderer(CollectableEntityField clctefTarget) {
		return this.getSubForm().getTableCellRenderer(clctefTarget);
	}

	/**
	 * Implementation of <code>TableCellEditorProvider</code>.
	 * @param tbl
	 * @param iRow row of the table (not the table model)
	 * @param clctefTarget collectable entity field (for the column)
	 * @return a <code>TableCellEditor</code> for columns that need a dynamic <code>TableCellEditor</code>.
	 * <code>null</code> for all other columns.
	 */
	@Override
    public TableCellEditor getTableCellEditor(JTable tbl, int iRow, CollectableEntityField clctefTarget) {
		subform.endEditing();

		return this.getSubForm().getTableCellEditor(tbl, iRow, this.getCollectableEntity(), clctefTarget,
				this.getSubFormTableModel(), this.isSearchable(), this.getPrefs(), this.getCollectableFieldsProviderFactory(), this, getMandatorUID(), getIntidForVLP());
	}
	
	@Override
	public TableCellEditor getTableCellEditorContinueEditing(JTable tbl, int iRow, CollectableEntityField clctefTarget) {

		return this.getSubForm().getTableCellEditor(tbl, iRow, this.getCollectableEntity(), clctefTarget,
				this.getSubFormTableModel(), this.isSearchable(), this.getPrefs(), this.getCollectableFieldsProviderFactory(), this, getMandatorUID(), getIntidForVLP());
	}

	@Override
    public final CollectableField getParameterForRefreshValueList(final SubFormTableModel subformtblmdl, int iRow, final UID parentComponentUid, final UID parentComponentEntityUid) {
		assert parentComponentUid != null;

		final CollectableField result;
		if (parentComponentEntityUid.equals(this.getCollectableEntity().getUID())) {
			if (iRow < 0) {
				return null;
			}
			final int iColumnParent = subformtblmdl.findColumnByFieldUid(parentComponentUid);
			final Object oValue = subformtblmdl.getValueAt(iRow, iColumnParent);

			// the parent component resides in the same subform as the target:
			if (SubFormController.this.isSearchable()) {
				final CollectableField clctf = getComparand((CollectableSearchCondition) oValue);
				result = (clctf == null) ? CollectableValueIdField.NULL : clctf;
			}
			else {
				result = (CollectableField) oValue;
			}
		}
		else if (parentComponentEntityUid.equals(this.getParentEntityUID())) {
			// the parent component resides in the form outside of the subform:
			final CollectableComponentModel clctcompmodel = clctcompmodelproviderParent.getCollectableComponentModelFor(parentComponentUid);
			UID sParentSubForm = subform.getParentSubForm();
			if(sParentSubForm != null)
				result = getFieldFromParentSubform(parentComponentUid);
			else
				result = clctcompmodel.getField();
		}
		else if (Modules.getInstance().isModule(parentComponentEntityUid)) {
			final CollectableComponentModel clctcompmodel = clctcompmodelproviderParent.getCollectableComponentModelFor(parentComponentUid);
			result = clctcompmodel.getField();
		}
		else if(MetaProvider.getInstance().getEntity(parentComponentEntityUid) != null) {
			final CollectableComponentModel clctcompmodel = clctcompmodelproviderParent.getCollectableComponentModelFor(parentComponentUid);
			result = clctcompmodel.getField();
		}
		else {
			throw new CommonFatalException(getSpringLocaleDelegate().getMessage("SubFormController.1",
				"Die Entit\u00e4t der Vaterkomponente ({0}) muss der Entit\u00e4t des Unterformulars ({1}) oder der Entit\u00e4t des \u00fcbergeordneten Formulars ({2}) entsprechen.",
				parentComponentUid, this.getCollectableEntity().getUID(), this.getParentEntityUID()));
		}

		if(result == null && isSearchable()) {
			throw new NuclosFieldNotInModelException();
		}
		else if(result == null) {
			throw new CommonFatalException(getSpringLocaleDelegate().getMessage(
					"SubFormController.2", "Das Feld ({0}) ist nicht in der Entität ({1}) vorhanden!", parentComponentUid, this.getParentEntityUID()));
		}

		return result;
	}

	protected abstract CollectableField getFieldFromParentSubform(UID fieldUid);

	/**
	 * @param cond May be <code>null</code>.
	 * @return the <code>CollectableField</code> contained in <code>cond</code>, if <code>cond</code> is a <code>CollectableComparison</code>.
	 * <code>null</code> otherwise.
	 */
	protected static CollectableField getComparand(CollectableSearchCondition cond) {
		final CollectableField result;
		if (cond != null && cond instanceof CollectableComparison) {
			result = ((CollectableComparison) cond).getComparand();
		}
		else {
			result = null;
		}
		return result;
	}

	/**
	 * §postcondition !this.isEnabled() --&gt; !result
	 */
	public abstract boolean isColumnEnabled(UID sColumnName);

	public abstract boolean isRowEditable(int row);

	public abstract boolean isRowRemovable(int row);

	public void setCollectableComponentFactory(CollectableComponentFactory collectableComponentFactory) {
		this.getSubForm().setCollectableComponentFactory(collectableComponentFactory);
		setupStaticTableCellEditors(this.getSubForm().getJTable());
	}

	protected final void setupStaticTableCellEditors(JTable tbl) {
		this.getSubForm().setupStaticTableCellEditors(tbl, this.isSearchable(), this.getPrefs(), this.getSubFormTableModel(),
				this.getCollectableFieldsProviderFactory(), this.getParentEntityUID(), this.getCollectableEntity());
	}

	@Override
	public void focusAction(EventObject eObject) {
		cmdInsert();
	}
	
	protected void init() {
		UID entityId = null;
		if (clcte != null) {
			if (!E.isNuclosEntity(clcte.getUID())) {
				try {
					EntityMeta<?> eMeta = MetaProvider.getInstance().getEntity(clcte.getUID());
					if (!eMeta.isDynamic()) {
						entityId = eMeta.getUID();
					}
				} catch (Exception ignore) {}
			}
		}
		
		final JComponent parent = subform.getToolbar();
		final ToolBarItemGroup iGroup;
		if (bSearchable) {
			iGroup = LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Tool_Bar_Search_Subform, entityId).getMainToolBar();
		} else {
			iGroup = LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Tool_Bar_Detail_Subform, entityId).getMainToolBar();			
		}
		subform.setToolBarItemGroup(iGroup);
		
		if (!subform.isLayout()) {
			final ToolBarLafParameterEditor editor1;
			if (bSearchable) {
				editor1 = new LafParameterHelper.NuclosSearchSubformLafParameterEditor(
						LafParameter.nuclos_LAF_Tool_Bar_Search_Subform, entityId, parent);
			} else {
				editor1 = new LafParameterHelper.NuclosDetailsSubformLafParameterEditor(
						LafParameter.nuclos_LAF_Tool_Bar_Detail_Subform, entityId, parent);				
			}
			final BoolLafParameterEditor editor2 = new BoolLafParameterEditor(LafParameter.nuclos_LAF_Single_Sort, entityId, parent);
			final BoolLafParameterEditor editor3 = new BoolLafParameterEditor(LafParameter.nuclos_LAF_Layout_Tab_Show_Row_Count, entityId, parent);
			final StringLafParameterEditor editor4 = new StringLafParameterEditor(LafParameter.nuclos_LAF_Webclient_Popup, entityId, parent);
			final BoolLafParameterEditor editor5 = new BoolLafParameterEditor(LafParameter.nuclos_LAF_Webclient_Subform_Show_Selection_Column, entityId, parent);

			LafParameterHelper.installPopup(editor1, toolBarChangeListener);
			LafParameterHelper.installPopup(editor2);
			LafParameterHelper.installPopup(editor3);
			LafParameterHelper.installPopup(editor4);
			LafParameterHelper.installPopup(editor5);
		}
	}
	
	public UID getMandatorUID() {
		return mandatorUID;
	}

	public void setMandatorUID(UID mandatorUID) {
		this.mandatorUID = mandatorUID;
	}
	
	public Long getIntidForVLP() {
		return intidForVLP;
	}

	public void setIntidForVLP(Long intidForVLP) {
		this.intidForVLP = intidForVLP;
	}

}	// class SubFormController
