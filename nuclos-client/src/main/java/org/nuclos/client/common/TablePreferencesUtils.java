//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.entityobject.CollectableEOEntityClientProvider;
import org.nuclos.client.genericobject.CollectableGenericObjectEntity;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.common.Actions;
import org.nuclos.common.CollectableEntityFieldWithEntity;
import org.nuclos.common.CollectableEntityFieldWithEntityForExternal;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.entityobject.CollectableEOEntityField;
import org.nuclos.common.entityobject.CollectableEOEntityProvider;
import org.nuclos.common.genericobject.CollectableGenericObjectEntityField;
import org.nuclos.common.genericobject.GenericObjectUtils;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;
import org.nuclos.common.masterdata.CollectableMasterDataForeignKeyEntityField;
import org.nuclos.common.preferences.ColumnPreferences;
import org.nuclos.common.preferences.TablePreferences;
import org.nuclos.common.preferences.TablePreferencesManager;
import org.nuclos.common2.StringUtils;

public class TablePreferencesUtils {

	private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(TablePreferencesUtils.class);

	public static List<? extends CollectableEntityField> getCollectableEntityFieldsForGenericObject(TablePreferencesManager tblprefManager) {
		final List<CollectableEntityField> result = new ArrayList<CollectableEntityField>();

		final CollectableEOEntityProvider ceeoProv = CollectableEOEntityClientProvider.getInstance();
		final MetaProvider metaProv = MetaProvider.getInstance();

		for (ColumnPreferences cp : tblprefManager.getSelectedColumnPreferences()) {
			try {
				switch (cp.getType()) {

					case ColumnPreferences.TYPE_EOEntityField:
						final FieldMeta<?> efMeta = metaProv.getEntityField(cp.getColumn());
						result.add(new CollectableEOEntityField(efMeta));
						break;

					case ColumnPreferences.TYPE_GenericObjectEntityField:
						result.add(CollectableGenericObjectEntity.getByModuleUid(
								metaProv.getEntity(cp.getEntity()).getUID()).getEntityField(cp.getColumn()));
						break;

					case ColumnPreferences.TYPE_MasterDataForeignKeyEntityField:
						CollectableMasterDataForeignKeyEntityField<?> clctef =
								new CollectableMasterDataForeignKeyEntityField(
										MasterDataDelegate.getInstance().getMetaData(cp.getEntity()).getField(cp.getColumn()));
						result.add(clctef);
						clctef.setCollectableEntity(new CollectableMasterDataEntity(MasterDataDelegate.getInstance().getMetaData(cp.getEntity())));
						break;

					case ColumnPreferences.TYPE_EntityFieldWithEntityForExternal:
						result.add(GenericObjectUtils.getCollectableEntityFieldForResult(
								ceeoProv.getCollectableEntity(cp.getEntity()==null?tblprefManager.getEntity():cp.getEntity()),
								cp.getColumn(),
								ceeoProv.getCollectableEntity(tblprefManager.getEntity())));
						break;

					case ColumnPreferences.TYPE_EntityFieldWithEntity:
						result.add(new CollectableEntityFieldWithEntity(
								ceeoProv.getCollectableEntity(cp.getEntity()),
								cp.getColumn()));
						break;

					default:
						result.add(GenericObjectUtils.getCollectableEntityFieldForResult(
								ceeoProv.getCollectableEntity(cp.getEntity()==null?tblprefManager.getEntity():cp.getEntity()),
								cp.getColumn(),
								ceeoProv.getCollectableEntity(tblprefManager.getEntity())));
				}
			} catch (Exception ex) {
				LOG.error("Column could not be restored " + cp, ex);
			}
		}

		// Our webclient does not handle hidden columns.
		// So we have to disable the 'addNewColumns' feature
//		if (SecurityCache.getInstance().isActionAllowed(Actions.ACTION_CUSTOMIZE_ENTITY_AND_SUBFORM_COLUMNS)) {
//
//			// do not add columns first time...
//			if (result.isEmpty() && tblprefManager.getSelected().getHiddenColumns().isEmpty()) {
//				return result;
//			}
//			// add new columns
//			try {
//				for (FieldMeta<?> efMeta : CollectionUtils.sorted( // order by intid
//						MetaProvider.getInstance().getAllEntityFieldsByEntity(tblprefManager.getEntity()).values(),
//						new Comparator<FieldMeta<?>>() {
//							@Override
//							public int compare(FieldMeta<?> o1, FieldMeta<?> o2) {
//								return o1.getUID().compareTo(o2.getUID());
//							}
//						})) {
//
//					if (SF.isEOField(efMeta.getEntity(), efMeta.getUID())) {
//						// do not add system fields
//						continue;
//					}
//
//					boolean alreadySelected = false;
//					for (CollectableEntityField clctef : result) {
//						if (LangUtils.equal(clctef.getUID(), efMeta.getUID())) {
//							// field already selected
//							alreadySelected = true;
//							break;
//						}
//					}
//					if (alreadySelected) {
//						continue;
//					}
//					if (efMeta.isCalculated()) {
//						// field is calculated
//						continue;
//					}
//					if (String.class.getName().equals(efMeta.getDataType()) &&
//							(efMeta.getScale() == null || efMeta.getScale() > 255)) {
//						// NUCLOS-1798: field is memo or clob
//						continue;
//					}
//					if (tblprefManager.getSelected().getHiddenColumns().contains(efMeta.getUID())) {
//						// field is hidden
//						continue;
//					}
//
//					// field is new
//					Map<UID, Permission> mpPermissions = null;
//					if (efMeta.isCalcAttributeAllowCustomization()) {
//						mpPermissions = AttributeCache.getInstance().getAttribute(LangUtils.defaultIfNull(efMeta.getCalcBaseFieldUID(), efMeta.getUID())).getPermissions();
//					} else {
//						mpPermissions = AttributeCache.getInstance().getAttribute(efMeta.getUID()).getPermissions();
//					}
//					result.add(new CollectableGenericObjectEntityField(mpPermissions, efMeta, tblprefManager.getEntity()));
//				}
//			} catch (Exception ex) {
//				LOG.error("New columns not added", ex);
//			}
//		}

		return result;
	}

	public static void setCollectableEntityFieldsForGenericObject(TablePreferencesManager tblprefManager,
																  List<? extends CollectableEntityField> selectedFields,
																  List<Integer> fieldWidths, List<UID> fixedFields) {

		if (SecurityCache.getInstance().isActionAllowed(Actions.ACTION_CUSTOMIZE_ENTITY_AND_SUBFORM_COLUMNS)) {

			final TablePreferences tp = tblprefManager.getSelected();

			final Map<UID, ColumnPreferences> mapColumns = new HashMap<>();
			for (ColumnPreferences cp : tp.getSelectedColumnPreferences()) {
				mapColumns.put(cp.getColumn(), cp);
			}
			tp.removeAllSelectedColumnPreferences();
			LOG.debug("setCollectableEntityFieldsForGenericObject for entity "+tblprefManager.getEntity());

			final List<CollectableEntityField> fixedEfs = new ArrayList<CollectableEntityField>();
			final List<CollectableEntityField> normalEfs = new ArrayList<CollectableEntityField>();
			// fixed before normal columns...
			for (CollectableEntityField clctef : selectedFields) {
				if (fixedFields != null && fixedFields.contains(clctef.getUID())) {
					fixedEfs.add(clctef);
				} else {
					normalEfs.add(clctef);
				}
			}

			final List<CollectableEntityField> efs = new ArrayList<CollectableEntityField>();
			efs.addAll(fixedEfs);
			efs.addAll(normalEfs);
			Set<UID> preventDuplicates = new HashSet<>();
			for (int i = 0; i < efs.size(); i++) {
				final CollectableEntityField clctef = efs.get(i);
				if (preventDuplicates.contains(clctef.getUID())) {
					continue;
				} else {
					preventDuplicates.add(clctef.getUID());
				}
				final ColumnPreferences cp;
				if (mapColumns.containsKey(clctef.getUID())) {
					cp = mapColumns.get(clctef.getUID());
				} else {
					cp = new ColumnPreferences();
					cp.setColumn(clctef.getUID());
					cp.setEntity(clctef.getEntityUID());
				}
				cp.setFixed(i < fixedEfs.size());

				if (clctef instanceof CollectableEOEntityField) {
					cp.setType(ColumnPreferences.TYPE_EOEntityField);
				} else if (clctef instanceof CollectableGenericObjectEntityField) {
					cp.setType(ColumnPreferences.TYPE_GenericObjectEntityField);
				} else if (clctef instanceof CollectableMasterDataForeignKeyEntityField) {
					cp.setType(ColumnPreferences.TYPE_MasterDataForeignKeyEntityField);
				} else if (clctef instanceof CollectableEntityFieldWithEntityForExternal) {
					cp.setType(ColumnPreferences.TYPE_EntityFieldWithEntityForExternal);
				} else if (clctef instanceof CollectableEntityFieldWithEntity) {
					cp.setType(ColumnPreferences.TYPE_EntityFieldWithEntity);
				}

				if (fieldWidths.size() > i) {
					cp.setWidth(fieldWidths.get(i));
				}

				tp.addSelectedColumnPreferences(cp);
				LOG.debug(StringUtils.logFormat("setCollectableEntityFieldsForGenericObject",cp.getColumn(),cp.getWidth()));
			}

			tblprefManager.update(tp);
		}
	}

}
