//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.client.common;

import org.apache.log4j.Logger;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.main.mainframe.workspace.RestoreUtils;
import org.nuclos.common.UID;
import org.nuclos.common.WorkspaceDescription2.Desktop;
import org.nuclos.common.WorkspaceDescription2.EntityPreferences;
import org.nuclos.common.WorkspaceDescription2.LayoutPreferences;
import org.nuclos.common.WorkspaceDescription2.MutableContent;
import org.nuclos.common.WorkspaceDescription2.NestedContent;
import org.nuclos.common.WorkspaceDescription2.Split;
import org.nuclos.common.WorkspaceDescription2.Tabbed;
import org.nuclos.common.WorkspaceVO;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.WorkspaceException;
import org.nuclos.server.common.ejb3.PreferencesFacadeRemote;

public class WorkspaceUtils {
	
	private static final Logger LOG = Logger.getLogger(WorkspaceUtils.class);
	
	private static WorkspaceUtils INSTANCE;
	
	// Spring injection
	
	private PreferencesFacadeRemote preferencesFacadeRemote;
	
	private MainFrame mainFrame;
	
	private RestoreUtils restoreUtils;
	
	// end of Spring injection

	WorkspaceUtils() {
		INSTANCE = this;
	}
	
	public static WorkspaceUtils getInstance() {
		return INSTANCE;
	}
	
	public final void setPreferencesFacadeRemote(PreferencesFacadeRemote preferencesFacadeRemote) {
		this.preferencesFacadeRemote = preferencesFacadeRemote;
	}
	
	public final void setMainFrame(MainFrame mainFrame) {
		this.mainFrame = mainFrame;
	}
	
	public final void setRestoreUtils(RestoreUtils restoreUtils) {
		this.restoreUtils = restoreUtils;
	}
	
	public WorkspaceVO getWorkspace() {
		return mainFrame.getWorkspace();
	}
	
	public MainFrame getMainFrame() {
		return mainFrame;
	}
	
	public WorkspaceVO getRestoreWorkspace() throws WorkspaceException {
		final WorkspaceVO currentWovo = mainFrame.getWorkspace();
		final UID assignedWorkspaceUid = mainFrame.getWorkspace().getAssignedWorkspace();
		
		if (assignedWorkspaceUid != null) {
			return preferencesFacadeRemote.getWorkspace(assignedWorkspaceUid);
		}
		return null;
	}
	
	public Desktop restoreDesktop(Desktop dsktp) throws WorkspaceException {
		final WorkspaceVO currentWovo = mainFrame.getWorkspace();
		restoreUtils.storeWorkspace(currentWovo, true);
		
		final UID assignedWorkspaceUid = mainFrame.getWorkspace().getAssignedWorkspace();
		final Desktop result;
		final WorkspaceVO assigned = getRestoreWorkspace();
		if (assigned != null) {
			// check structure
			if (preferencesFacadeRemote.isWorkspaceStructureChanged(assignedWorkspaceUid, mainFrame.getWorkspace().getPrimaryKey())) {
				throw new WorkspaceException(SpringLocaleDelegate.getInstance().getMessage(
						"Desktop.not.restoreable", "Desktop kann nicht zurückgesetzt werden. Die Struktur der Vorlage entspricht nicht der aktuellen Arbeitsumgebung."));
			}
			result = getDesktopFromTargetWorkspace(currentWovo.getWoDesc().getMainFrame().getContent(), 
					dsktp, assigned.getWoDesc().getMainFrame().getContent());
		} else {
			result = null;
		}
		return result;
	}
	
	private Desktop getDesktopFromTargetWorkspace(NestedContent ncSource, Desktop dsktpSource, NestedContent ncTarget) {
		try {
			if (ncSource instanceof MutableContent) {
				return getDesktopFromTargetWorkspace(((MutableContent) ncSource).getContent(), dsktpSource, ((MutableContent) ncTarget).getContent());
			} else if (ncSource instanceof Split) {
				Desktop splitResult = getDesktopFromTargetWorkspace(((Split) ncSource).getContentA(), dsktpSource, ((Split) ncTarget).getContentA());
				if (splitResult == null) 
					splitResult = getDesktopFromTargetWorkspace(((Split) ncSource).getContentB(), dsktpSource, ((Split) ncTarget).getContentB());
				return splitResult;
			} else if (ncSource instanceof Tabbed) {
				if (((Tabbed)ncSource).getDesktop() != null && 
						((Tabbed)ncSource).getDesktop() == dsktpSource) {
					return ((Tabbed)ncTarget).getDesktop();
				}
			}
		} catch (Exception ex) {
			LOG.error(ex);
			// structure change
		}
		
		return null;
	}
	
	public EntityPreferences getStoredEntityPreferences(final UID entityUID) throws WorkspaceException {
		final WorkspaceVO ws = getRestoreWorkspace();
		return getEntityPreferences(ws, entityUID);
	}
	
	public EntityPreferences getCurrentEntityPreferences(final UID entityUID) {
		return getEntityPreferences(getWorkspace(), entityUID);
	}
	
	public EntityPreferences getEntityPreferences(final WorkspaceVO ws, final UID entityUID) {
		final EntityPreferences result;
		if (ws != null) {
			if (ws.getWoDesc().containsEntityPreferences(entityUID)) {
				result = ws.getWoDesc().getEntityPreferences(entityUID);
			} else {
				result = null;
			}
		} else {
			result = null;
		}
		return result;
	}
	
	/**
	 * layout preferences
	 * 
	 * creates empty layout preferences if empty (null)
	 * 
	 * @param ws		{@link WorkspaceVO} workspace
	 * @param uidLayout	{@link UID} layout
	 * @return
	 */
	public LayoutPreferences getLayoutPreferences(final WorkspaceVO ws, final UID uidLayout) {
		
		LayoutPreferences result = null;
		if (null == uidLayout) {
			LOG.error("no uid set for layout");
			return result;
		}
		for (final LayoutPreferences preferences : ws.getWoDesc().getLayoutPreferences()) {
			if (uidLayout.equals(preferences.getLayout())) {
				result = preferences;
				break;
			}
		}
		if (null == result) {
			result = new LayoutPreferences();
			result.setLayout(uidLayout);
			ws.getWoDesc().addLayoutPreferences(result);
		}
		return result;
	}
	
	/**
	 * split pane preferences (position, ...)
	 * 
	 * creates empty split pane preferences if empty (null)
	 * 
	 * @param lp	{@link LayoutPreferences}
	 * @param name	split pane name
	 * @return
	 */
	public Split getSplitPanePreferences(final LayoutPreferences lp, final String name) {
		Split result = null;
		for (final Split sp : lp.getSplitPanePreferences()) {
			if (LangUtils.equal(name, sp.getName())) {
				result = sp;
				break;
			}
		}
		
		if (null == result) {
			result = new Split();
			result.setName(name);
			result.setPosition(-1);
			lp.addSplitPanePreferences(result);
		}
		return result;
	}
	
}
