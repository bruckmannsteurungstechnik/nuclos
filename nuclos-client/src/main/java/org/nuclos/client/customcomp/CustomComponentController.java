//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.client.customcomp;

import java.awt.Component;
import java.io.Closeable;
import java.io.Serializable;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.swing.JComponent;

import org.apache.log4j.Logger;
import org.nuclos.client.command.BackgroundTask;
import org.nuclos.client.command.CommandHandler;
import org.nuclos.client.command.CommonClientWorker;
import org.nuclos.client.command.ResultListener;
import org.nuclos.client.customcomp.resplan.ResPlanController;
import org.nuclos.client.main.ComponentHolder;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.mainframe.IconResolver;
import org.nuclos.client.main.mainframe.IconResolverConstants;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.main.mainframe.workspace.ITabStoreController;
import org.nuclos.client.main.mainframe.workspace.TabRestoreController;
import org.nuclos.client.ui.CommonMultiThreader;
import org.nuclos.client.ui.ComponentHolderController;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.MainFrameTabAdapter;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.layer.LayerLock;
import org.nuclos.client.ui.layer.LockingCommandHandler;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collection.Pair;
import org.nuclos.common2.ClientPreferences;
import org.nuclos.common2.CloseableXStream;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.XStreamSupport;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.PreferencesException;
import org.nuclos.server.customcomp.valueobject.CustomComponentVO;

import com.thoughtworks.xstream.XStream;

public abstract class CustomComponentController extends ComponentHolderController implements Closeable {
	protected final Logger log = Logger.getLogger(this.getClass());

	private final UID componentUID;
	private final String componentName;
	private String title;

	protected CustomComponentController(UID componentUID, String componentName, MainFrameTab tab) {
		super(tab);
		this.componentUID = componentUID;
		this.componentName = componentName;
	}
	
	protected void setTitle(String title) {
		this.title = title;
	}
	
	public String getTitle() {
		return title;
	}
	
	public final UID getCustomComponentUID() {
		return componentUID;
	}

	public final String getCustomComponentName() {
		return componentName;
	}
	
	/**
	 * This method is one of the entry points. The component is initialized for presentation
	 * and the corresponding internal frame is shown.
	 * Note: Controller subclasses may provide alternative entry points which allows a more
	 * specific initialization.
	 */
	public void run() {
		getTab().setVisible(true);
	}

	protected abstract JComponent getComponent();

	protected void storeSharedState() throws PreferencesException, CommonFinderException, CommonPermissionException {
	}

	protected void restoreSharedState() throws PreferencesException, BackingStoreException, CommonFinderException, CommonPermissionException {
	}

	/** Returns the client preferences node used for storing common/shared preferences. */
	protected Preferences getPreferences() {
		return ClientPreferences.getInstance().getUserPreferences().node("customcomponent").node(getCustomComponentName());
	}

	/** Runs the CommonRunnable and performs error handling.*/
	public <PK> void runCommandWithSpecialHandler(CommonRunnable runnable, Collectable<PK> clct) throws Exception {
		// This is similar to UIUtils.runCommand but handles
		try {
			final CommandHandler cmdHandler = new LockingCommandHandler();
			cmdHandler.commandStarted(getComponent());
			try {
				runnable.run();
			} finally {
				cmdHandler.commandFinished(getComponent());
			}
		} catch (Error error) {
			Errors.getInstance().getCriticalErrorHandler().handleCriticalError(getComponent(), error);
		}
	}

	/** Can be overridden by subclasses to handle some exceptions differently. */
	/* protected boolean handleSpecialException(Exception ex) {
		return false;
	} */

	/** Executes a background task in this controller. This method performs all relevant locking and error
	 * handling.
	 */
	protected void execute(final BackgroundTask task) {
		final ComponentHolder tab = this.getTab();
		CommonMultiThreader.getInstance().execute(new CommonClientWorker() {
			
			private LayerLock lock;
			
			@Override
			public void init() throws CommonBusinessException {
				if (tab != null) {
					lock = tab.lockLayer();
				}
				task.init();
			}
			@Override
			public void work() throws CommonBusinessException {
				task.doInBackground();
			}
			@Override
			public void paint() throws CommonBusinessException {
				if (tab != null) {
					tab.unlockLayer(lock);
					lock = null;
				}
				task.done();
			}
			@Override
			public JComponent getResultsComponent() {
				return (JComponent)tab;
			}
			@Override
			public void handleError(Exception ex) {
				if (tab != null) {
					tab.unlockLayer(lock);
					lock = null;
				}
				Errors.getInstance().showExceptionDialog(getResultsComponent(), ex);
			}
		});
	}

	/**
	 * Instantiates the given component controller.
	 * @param customComponent internal name of the component
	 */
	public static CustomComponentController newController(UID customComponent) {
		return newController(customComponent, CustomComponentController.class, null, true);
	}
	
	public static CustomComponentController newController(String customComponent, Class<?> controllerClazz, 
			ComponentHolder tabIfAny) {
		return newController(UID.parseUID(customComponent), controllerClazz, tabIfAny, true);
	}

	/**
	 * Instantiates the given component controller.  This method is identical to {@link #newController(String, Class, ComponentHolder)}
	 * but ensures that the created controller is of the specified class.
	 * 
	 * @param customComponent internal name of the component
	 * @param controllerClazz class of the controller
	 */
	public static CustomComponentController newController(UID customComponent, Class<?> controllerClazz, ComponentHolder tabIfAny, boolean editable) {
		CustomComponentVO componentVO = CustomComponentCache.getInstance().getByUID(customComponent);
				
		final CustomComponentController controller;
		if ("org.nuclos.resplan".equals(componentVO.getComponentType()) && controllerClazz.isAssignableFrom(ResPlanController.class)) {
			controller = new ResPlanController(componentVO, null, editable);
		} else {
			throw new NuclosFatalException("Component " + componentVO.getInternalName() + " has an unsupported or incompatible component type");
		}
		
		String title = SpringLocaleDelegate.getInstance().getTextFallback(
				componentVO.getLabelResourceId(), componentVO.getLabelResourceId());
		controller.setTitle(title);
		
		boolean newTab = false;
		final ComponentHolder mainFrameTab;
		if (tabIfAny == null) {
			newTab = true;
			mainFrameTab = Main.getInstance().getMainController().newMainFrameTab(controller, title);
		} else {
			mainFrameTab = tabIfAny;
			mainFrameTab.setTitle(title);
		}
		controller.setParent(mainFrameTab);

		mainFrameTab.setLayeredComponent(controller.getComponent());
		if (controller.isRestoreTab()) {
			mainFrameTab.setTabStoreController(new CustomComponentTabStoreController(controller));
		}
		mainFrameTab.addMainFrameTabListener(new MainFrameTabAdapter() {
			@Override
			public void tabClosing(MainFrameTab tab, ResultListener<Boolean> rl) {
				UIUtils.runShortCommand((Component)mainFrameTab, new CommonRunnable() {
					@Override
					public void run() throws CommonBusinessException {
//						try {
//							controller.getPreferences().removeNode();
//						} catch (BackingStoreException ex) {
//							throw new PreferencesException(ex);
//						}
						controller.storeSharedState();
					}
				});
				rl.done(true);
			}
			@Override
			public void tabClosed(MainFrameTab tab) {
				mainFrameTab.removeMainFrameTabListener(this);
			}
		});

		final boolean isMainFrameTab = mainFrameTab instanceof MainFrameTab;
		if (newTab && isMainFrameTab) {
			MainFrameTab mft = (MainFrameTab) mainFrameTab;
			MainFrame.getPredefinedEntityOpenLocation(customComponent).add(mft);		
		}
		
		UIUtils.runShortCommand((Component)mainFrameTab, () -> {
            try {
                controller.restoreSharedState();
            } catch (BackingStoreException e) {
                throw new NuclosBusinessException(e);
            }
        });
		return controller;
	}

	public abstract boolean isRestoreTab();

	/**
	 *
	 *
	 */
	private static class RestorePreferences implements Serializable {
		private static final long serialVersionUID = 6637996725938917463L;

		UID customComponentUID;
		String customComponentName;
		String customComponentClass;
		String instanceStateXML;
	}

	private static String toXML(RestorePreferences rp) {
		final XStreamSupport xs = XStreamSupport.getInstance();
		try (CloseableXStream closeable = xs.getCloseableStream()) {
			final XStream xstream = closeable.getStream();
			return xstream.toXML(rp);
		}
	}

	private static RestorePreferences fromXML(String xml) {
		final XStreamSupport xs = XStreamSupport.getInstance();
		try (CloseableXStream closeable = xs.getCloseableStream()) {
			final XStream xstream = closeable.getStream();
			return (RestorePreferences) xstream.fromXML(xml);
		}
	}

	protected abstract String storeInstanceStateToXML();

	protected abstract void restoreInstanceStateFromXML(String xml);

	/**
	 *
	 *
	 */
	public static class CustomComponentTabStoreController implements ITabStoreController {
		private final CustomComponentController ctl;

		public CustomComponentTabStoreController(CustomComponentController ctl) {
			super();
			this.ctl = ctl;
		}

		@Override
		public String getPreferencesXML() {
			RestorePreferences rp = new RestorePreferences();
			rp.customComponentUID = ctl.getCustomComponentUID();
			rp.customComponentName = ctl.getCustomComponentName();
			rp.customComponentClass = ctl.getClass().getName();
			rp.instanceStateXML = ctl.storeInstanceStateToXML();
			return toXML(rp);
		}

		@Override
		public Class<?> getTabRestoreControllerClass() {
			return CustomComponentTabRestoreController.class;
		}

	}

	public static class CustomComponentTabRestoreController extends TabRestoreController {
		
		public CustomComponentTabRestoreController() {
		}
		
		@Override
		public void restoreFromPreferences(String preferencesXML, MainFrameTab tab) throws Exception {
			RestorePreferences rp = fromXML(preferencesXML);
			Class<?> customComponentControllerClass = LangUtils.getClassLoaderThatWorksForWebStart().loadClass(rp.customComponentClass);
			CustomComponentController ctl = null;
			if (rp.customComponentUID == null) {
				for (CustomComponentVO ccvo : CustomComponentCache.getInstance().getAll()) {
					if (rp.customComponentName.equals(ccvo.getInternalName())) {
						ctl = CustomComponentController.newController(ccvo.getPrimaryKey(), customComponentControllerClass, tab, true);
					}
				}
			} else {
				ctl = CustomComponentController.newController(rp.customComponentUID, customComponentControllerClass, tab, true);
			}
			
			if (ctl != null) {
				Main.getInstance().getMainController().initMainFrameTab(ctl, tab);
				// Main.getMainController().addMainFrameTab would be called from listener inside of initMainFrameTab, but only when tab added.
				// During restore the tabs are already added, so we need to do this manually.
				Main.getInstance().getMainController().addMainFrameTab(tab, ctl);
	
				ctl.restoreInstanceStateFromXML(rp.instanceStateXML);
				ctl.run();
			}
		}
		
		@Override
		public boolean validate(String preferencesXML) {
			return true;
		}

	}

	@Override
	public Pair<IconResolver, String> getIconAndResolver() {
		return new Pair<IconResolver, String>(IconResolverConstants.NUCLOS_RESOURCE_ICON_RESOLVER, "org.nuclos.client.resource.icon.glyphish.83-calendar.png");
	}

}