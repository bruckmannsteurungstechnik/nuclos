//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.client.customcomp.resplan;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdesktop.swingx.painter.Painter;
import org.nuclos.client.scripting.InvocableMethod;
import org.nuclos.client.ui.resplan.JResPlanComponent.Area;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common2.DateUtils;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.interval.Interval;

public class BackgroundPainter<PK> implements Painter<Area<Collectable<PK>, Date>>, CalendarBackgroundProvider {

	public static Class<?>[] SCRIPTING_SIGNATURE = { Collectable.class, Interval.class, BackgroundPainter.class };

	private Date today = DateUtils.getPureDate(new Date());
	private GregorianCalendar gcToday = new GregorianCalendar();
	private GregorianCalendar gcStart = new GregorianCalendar();
	private GregorianCalendar gcEnd = new GregorianCalendar();

	public BackgroundPainter() {
		gcToday.setTime(today);
	}

	private Color color;
	private InvocableMethod groovyMethod;
	private Map<Date, List<String>> holidays = new HashMap<>();
	
	public Color getColor() {
		return color;
	}
	
	public void setColor(Object colorObject) {
		if (colorObject instanceof String) {
			this.color = Color.decode((String) colorObject);
		} else {
			this.color = (Color) colorObject;
		}
	}
	
	void setGroovyMethod(InvocableMethod groovyMethod) {
		this.groovyMethod = groovyMethod;
	}
	
	void setHolidays(Map<Date, List<String>> holidays) {
		this.holidays = holidays != null ? holidays : new HashMap<Date, List<String>>();
	}
	
	@Override
	public Color getBackgroundColorForInterval(Date start, Date end) {
		if (!today.equals(DateUtils.getPureDate(new Date()))) {
			today = DateUtils.getPureDate(new Date());
			gcToday.setTime(today);
		}
		color = null;
		Date pureDateStart = DateUtils.getPureDate(start);
		Date pureDateEnd = DateUtils.getPureDate(end);
		
		boolean dayView = pureDateStart.equals(pureDateEnd) || DateUtils.addDays(pureDateStart, 1).equals(pureDateEnd);
		boolean weekView = DateUtils.addDays(pureDateStart, 6).equals(pureDateEnd);
		
		if (dayView) {
			if (pureDateStart.equals(today)) {
				color = BACKGROUND_TODAY;
			} else {
				gcStart.setTime(pureDateStart);
				int dayOfWeek = gcStart.get(GregorianCalendar.DAY_OF_WEEK);
				boolean bHoliday = false;
				
				// TODO: Actually use this tooltip
				String sTooltip = "";
				if (holidays.get(gcStart.getTime()) != null) {
					bHoliday = true;
					for (String sTip : holidays.get(gcStart.getTime())) {
						sTooltip += holidays.get(sTip) + "\n";
					}
				}
				if (dayOfWeek == GregorianCalendar.SATURDAY || dayOfWeek == GregorianCalendar.SUNDAY || bHoliday) {
					color = BACKGROUND_HOLIDAY;	
				}
			}
		} else if (weekView) {
			if ((pureDateStart.equals(today) || pureDateStart.before(today)) &&
					(pureDateEnd.equals(today) || pureDateEnd.after(today)) ) {
				color = BACKGROUND_TODAY;
			}
		} else {
			gcStart.setTime(pureDateStart);
			gcEnd.setTime(pureDateEnd);
			
			if (LangUtils.equal(gcStart.get(Calendar.DAY_OF_MONTH), gcEnd.get(Calendar.DAY_OF_MONTH))) {
				// is MonthView...
				
				if (LangUtils.equal(gcStart.get(Calendar.MONTH), gcToday.get(Calendar.MONTH)) && 
					LangUtils.equal(gcStart.get(Calendar.YEAR), gcToday.get(Calendar.YEAR))) {
					color = BACKGROUND_TODAY;
				}
			}
		}
		return color;
	}
	
	@Override
	public void paint(Graphics2D g, Area<Collectable<PK>, Date> area, int width, int height) {
		getBackgroundColorForInterval(area.getInterval().getStart(), area.getInterval().getEnd());
		
		if (groovyMethod != null && !groovyMethod.hasErrors()) {
			groovyMethod.invoke(area.getResource(), area.getInterval(), this);
		}
		if (color != null) {
			g.setColor(color);
			g.fillRect(0, 0, width, height);
		}
	}
}
