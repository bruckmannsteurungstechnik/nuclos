//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.client.customcomp.resplan;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;
import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.common.SourceResultHelper;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.customcomp.resplan.AbstractResPlanConfigVO;
import org.nuclos.common.customcomp.resplan.PlanElement;
import org.nuclos.common.customcomp.resplan.ResPlanResourceVO;
import org.nuclos.common.customcomp.resplan.ResourceLocaleVO;
import org.nuclos.common2.LocaleInfo;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@XmlType
@XmlRootElement(name="resplan")
public class ClientResPlanConfigVO<PK,R,C extends Collectable<PK>> 
	extends AbstractResPlanConfigVO<PK,R,C,ClientPlanElement<PK,R,C>> implements HasCollHelper<PK,R,C> {
	
	private static final Logger LOG = Logger.getLogger(ClientResPlanConfigVO.class);
	
	//Transient stuff
	private transient CollectableHelper<PK,R,C> collHelper = null;

	public ClientResPlanConfigVO() {
	}
	
	@Override
	public CollectableHelper<PK,R,C> getCollHelper() {
		if (collHelper == null) {
			collHelper = (CollectableHelper<PK,R,C>) CollectableHelper.getForEntity(getResourceEntity());
		}
		return collHelper;
	}
	
	@Override
	public ClientPlanElement<PK,R,C> newPlanElementInstance() {
		return new ClientPlanElement<PK,R,C>();
	}
	
	public static <PK2,R2,C2 extends Collectable<PK2>> ClientPlanElement<PK2,R2,C2> newPlanElement() {
		return new ClientPlanElement<PK2,R2,C2>();
	}

	public static ClientResPlanConfigVO unsafeFromBytes(byte[] b, Jaxb2Marshaller jaxb2Marshaller) {
		return (ClientResPlanConfigVO) ((Object) ClientResPlanConfigVO.<Object,Object,Collectable<Object>>fromBytes(b, jaxb2Marshaller));
	}
	
	public static <PK2,R2,C2 extends Collectable<PK2>> ClientResPlanConfigVO<PK2,R2,C2> fromBytes(byte[] b, Jaxb2Marshaller marshaller) {
		final StreamSource in = SourceResultHelper.newSource(b);
		final ClientResPlanConfigVO<PK2,R2,C2> result;
		try {
			result = (ClientResPlanConfigVO<PK2,R2,C2>) marshaller.unmarshal(in);
		} catch (OutOfMemoryError e) {
			LOG.error("JAXB unmarshal failed: xml is:\n" + new String(b), e);
			throw e;
		}
		
		if (result.getPlanElements() != null && result.getPlanElements().size() > 0) {
			// transform PlanElement to ClientPlanElement. JAXB does not handle this...
			List<ClientPlanElement<PK2,R2,C2>> ples = new ArrayList<ClientPlanElement<PK2,R2,C2>>();
			for (int i = 0; i < result.getPlanElements().size(); i++) {
				PlanElement ple = result.getPlanElements().get(i);
				ples.add(new ClientPlanElement(ple));
			}
			result.setPlanElements(ples);
		}

		// transfer resources from template
		if (result.getResources() == null || result.getResources().size() == 0) {
			List<ResPlanResourceVO> resources = new ArrayList<ResPlanResourceVO>();
			Collection<LocaleInfo> locales = LocaleDelegate.getInstance().getAllLocales(false);
			for (LocaleInfo li : locales) {
				ResPlanResourceVO vo = new ResPlanResourceVO();
				vo.setLocaleId(li.getLocale());
				vo.setResourceLabel(result.getResourceLabelText());
				vo.setResourceTooltip(result.getResourceToolTipText());
				vo.setBookingLabel(result.getEntryLabelText());
				vo.setBookingTooltip(result.getEntryToolTipText());
				vo.setLegendLabel(result.getCornerLabelText());
				vo.setLegendTooltip(result.getCornerToolTipText());
				resources.add(vo);
			}
			result.setResources(resources);
		}
		return result;
	}

	public ResourceLocaleVO getResourceLocale(LocaleInfo li) {
		for (LocaleInfo locale : LocaleDelegate.getInstance().getParentChain()) {
			for (ResourceLocaleVO result : getOrMigrateResourceLocales()) {
				if (locale.getLocale().equals(result.getLocaleId())) {
					return result;
				}
			}
		}
		// if no resources can be determined, return an empty resource set
		return new ResourceLocaleVO();
	}
}

