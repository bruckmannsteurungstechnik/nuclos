package org.nuclos.client.customcomp.resplan;

import org.nuclos.common.collect.collectable.Collectable;

public interface HasCollHelper<PK,R,C extends Collectable<PK>> {
	
	CollectableHelper<PK,R,C> getCollHelper();
	
}
