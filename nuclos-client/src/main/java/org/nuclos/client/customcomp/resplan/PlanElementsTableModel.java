//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.customcomp.resplan;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.swing.table.AbstractTableModel;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.customcomp.resplan.PlanElement;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.SpringLocaleDelegate;

/**
 * 
 * @author Oliver Brausch
 *
 */
//Versoion
@SuppressWarnings("serial")
public class PlanElementsTableModel<PK,R,C extends Collectable<PK>> extends AbstractTableModel {

	protected static String[] columns = new String[7];
	private static String[] types = new String[3];
	
	static {
		SpringLocaleDelegate sld = SpringLocaleDelegate.getInstance();
		for (int i = 0; i < columns.length; i++) {
			columns[i] = sld.getMessage("wizard.step.planelemtablecol." + (i + 1), "?");
		}
		for (int i = 0; i < types.length; i++) {
			types[i] = sld.getMessage("wizard.step.planelemtabletype." + (i + 1), "?");
		}
	}
	
	static {
		SpringLocaleDelegate sld = SpringLocaleDelegate.getInstance();
		for (int i = 0; i < columns.length; i++) {
			columns[i] = sld.getMessage("wizard.step.planelemtablecol." + (i + 1), "?");
		}
		for (int i = 0; i < types.length; i++) {
			types[i] = sld.getMessage("wizard.step.planelemtabletype." + (i + 1), "?");
		}
	}
	
	protected List<PlanElement<R>> lstRows = new ArrayList<PlanElement<R>>();
	protected Map<Integer, LocaleInfo> localeLabels;

	@Override
	public int getColumnCount() {
		return columns.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex) {
			case 0:
				return rowIndex + 1;
			case 1:
				int type = lstRows.get(rowIndex).getType();
				if (type == 0 || type > types.length) return "Undef.";
				return types[type-1];
			case 2:
				return lstRows.get(rowIndex).getEntity() == null ? null
						: MetaProvider.getInstance().getEntity(lstRows.get(rowIndex).getEntity()).getEntityName();
			case 3:
				return lstRows.get(rowIndex).getPrimaryField() == null ? null
						: MetaProvider.getInstance().getEntityField(lstRows.get(rowIndex).getPrimaryField()).getFieldName();
			case 4:
				return lstRows.get(rowIndex).getSecondaryField() == null ? null
						: MetaProvider.getInstance().getEntityField(lstRows.get(rowIndex).getSecondaryField()).getFieldName();
			case 5:
				return lstRows.get(rowIndex).getDateFromField() == null ? null
						: MetaProvider.getInstance().getEntityField(lstRows.get(rowIndex).getDateFromField()).getFieldName();				
			case 6:
				return lstRows.get(rowIndex).getDateUntilField() == null ? null
						: MetaProvider.getInstance().getEntityField(lstRows.get(rowIndex).getDateUntilField()).getFieldName();				
			default:
				break;
		}
		return "";
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
	}
	
	public void setRows(List<PlanElement<R>> rows) {
		lstRows = rows;
		this.fireTableDataChanged();
	}

	public List<PlanElement<R>> getRows() {
		return lstRows;
	}

	@Override
	public int getRowCount() {
		return lstRows.size();
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}
	
	@Override
	public String getColumnName(int column) {
		return columns[column];
	}
	
	public static List<String> getTypes() {
		return Arrays.asList(types);
	}
}