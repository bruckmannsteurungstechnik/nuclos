//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.datasource.admin;

import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JOptionPane;

import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.datasource.querybuilder.QueryBuilderEditor;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.ui.Errors;
import org.nuclos.common.CollectableEntityFieldWithEntity;
import org.nuclos.common.E;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.database.query.definition.QueryTable;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;
import org.nuclos.common.report.ejb3.DatasourceFacadeRemote;
import org.nuclos.common.report.valueobject.CalcAttributeUtils;
import org.nuclos.common.report.valueobject.CalcAttributeVO;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common2.IOUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonValidationException;

/**
 * <code>CollectController</code> for entity "calcAttribute".
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Boris.Sander@novabit.de">Boris Sander</a>
 * @version 01.00.00
 */
public class CalcAttributeCollectController extends AbstractDatasourceCollectController<CalcAttributeVO> {

	// former Spring injection

	private DatasourceFacadeRemote datasourceFacadeRemote;
	
	// end of former Spring injection
	
	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton} 
	 * to get an instance.
	 * 
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	public CalcAttributeCollectController(MainFrameTab tabIfAny) {
		super(new CollectableMasterDataEntity(E.CALCATTRIBUTE), tabIfAny);
		setDatasourceFacadeRemote(SpringApplicationContextHolder.getBean(DatasourceFacadeRemote.class));
		
		CollectableMasterDataEntity clctEntity = new CollectableMasterDataEntity(E.CALCATTRIBUTE);
		initializeDatasourceCollectController(
			new DatasourceEditPanel(this, 
				new CollectableEntityFieldWithEntity(clctEntity, E.CALCATTRIBUTE.name.getUID()),
				null,
				new CollectableEntityFieldWithEntity(clctEntity, E.CALCATTRIBUTE.description.getUID()),
				null,
				false, null));
	}
	
	final void setDatasourceFacadeRemote(DatasourceFacadeRemote datasourceFacadeRemote) {
		this.datasourceFacadeRemote = datasourceFacadeRemote;
	}

	final DatasourceFacadeRemote getDatasourceFacadeRemote() {
		return datasourceFacadeRemote;
	}

	@Override
	public void detailsChanged(Component compSource) {
		super.detailsChanged(compSource);
	}

	@Override
	protected boolean isNewAllowed() {
		return isSaveAllowed(false);
	}

	@Override
	protected boolean isSaveAllowed(final boolean useCurrentCollectable) {
		return SecurityCache.getInstance().isWriteAllowedForMasterData(entityUid);
	}

	@Override
	protected boolean isDeleteSelectedCollectableAllowed(){
		return SecurityCache.getInstance().isDeleteAllowedForMasterData(entityUid);
	}

	@Override
	public CollectableDataSource<CalcAttributeVO> findCollectableById(UID sEntity, UID oId, final int dependantsDepth) throws CommonBusinessException {
		// TODO Multinuclet Refactoring method signature
		return new CollectableDataSource<CalcAttributeVO>(datasourcedelegate.getCalcAttribute((UID) oId));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CollectableDataSource<CalcAttributeVO> newCollectable() {
		return new CollectableDataSource<CalcAttributeVO>(new CalcAttributeVO(null, null, null, null, null));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean stopEditingInDetails() {
		return pnlEdit.stopEditing();
	}

	/**
	 * generate the sql statement from the current definition of this datasource
	 * @return
	 * @throws CommonBusinessException
	 */
	@Override
	public String generateSql() throws CommonBusinessException {
		return datasourcedelegate.createSQL(new CalcAttributeVO(null, null, pnlEdit.getQueryEditor().getXML(new DatasourceEntityOptions(false)), false, null));
	}

	/**
	 * @param sXML
	 */
	@Override
	protected void importXML(String sXML) throws NuclosBusinessException {
		final String sWarnings = QueryBuilderEditor.getSkippedElements(pnlEdit.getQueryEditor().setXML(sXML));
		if (sWarnings.length() > 0) {
			JOptionPane.showMessageDialog(getTab(), getSpringLocaleDelegate().getMessage(
					"DatasourceCollectController.12","Folgende Elemente existieren nicht mehr in dem aktuellen Datenbankschema\n und wurden daher entfernt:") + "\n" + sWarnings);
		}
		detailsChanged(pnlEdit.getQueryEditor());
	}

	/**
	 * @param file
	 * @throws IOException
	 * @throws NuclosBusinessException
	 */
	@Override
	protected void exportXML(File file) throws IOException, CommonBusinessException {
		final String sReportXML = pnlEdit.getQueryEditor().getXML(new DatasourceEntityOptions(false));
		IOUtils.writeToTextFile(file, sReportXML, "UTF-8");
	}

	@Override
	protected void validateSQL() {
		try {
			getDatasourceFacadeRemote().validateSqlFromXML(new CalcAttributeVO(null, null, pnlEdit.getQueryEditor().getXML(new DatasourceEntityOptions(false)), false, null));
			JOptionPane.showMessageDialog(getTab(), getSpringLocaleDelegate().getMessage(
					"DatasourceCollectController.10","Die SQL Abfrage ist syntaktisch korrekt."));
		}
		catch (Exception ex) {
			Errors.getInstance().showExceptionDialog(getTab(), ex);
		}
	}

	@Override
	public Set<String> getQueryTypes() {
		HashSet<String> result = new HashSet<String>();
		result.add(QueryTable.QUERY_TYPE_CALC_ATTRIBUTE);
		return result;
	}

	@Override
	public boolean isWithParameterEditor() {
		return true;
	}

	@Override
	public boolean isParameterEditorWithValuelistProviderColumn() {
		return true;
	}

	@Override
	public boolean isParameterEditorWithLabelColumn() {
		return true;
	}
	
	@Override
	protected CollectableDataSource<CalcAttributeVO> updateCurrentCollectable(
			CollectableDataSource<CalcAttributeVO> clctEdited)
			throws CommonBusinessException {
		validate(clctEdited.getDatasourceVO());
		return super.updateCurrentCollectable(clctEdited);
	}

	@Override
	protected CollectableDataSource<CalcAttributeVO> insertCollectable(
			CollectableDataSource<CalcAttributeVO> clctNew)
			throws CommonBusinessException {
		validate(clctNew.getDatasourceVO());
		return super.insertCollectable(clctNew);
	}
	
	private void validate(CalcAttributeVO datasourcevo) throws CommonBusinessException {
		boolean hasIntidParameter = false;
		for (DatasourceParameterVO paramvo : datasourceFacadeRemote.getParametersFromXML(datasourcevo.getSource())) {
			if ("intid".equals(paramvo.getParameter())) {
				hasIntidParameter = true;
			} else {
				if (!CalcAttributeUtils.checkParameterNameOrValue(paramvo.getParameter())) {
					throw new CommonValidationException("Do not use reserved expressions for parameter names: " + CalcAttributeUtils.getAllChecks());
				}
			}
		}
		if (!hasIntidParameter) {
			throw new NuclosBusinessException(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.34", "Die Datenquelle muss den Parameter intid definieren."));
		}
	}
}
