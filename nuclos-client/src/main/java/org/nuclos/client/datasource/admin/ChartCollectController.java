//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.datasource.admin;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JOptionPane;

import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.datasource.querybuilder.QueryBuilderConstants;
import org.nuclos.client.datasource.querybuilder.QueryBuilderEditor;
import org.nuclos.client.datasource.querybuilder.gui.ColumnEntry;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.toolbar.NuclosToolBarActions;
import org.nuclos.common.CollectableEntityFieldWithEntity;
import org.nuclos.common.E;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.exception.CollectableValidationException;
import org.nuclos.common.database.query.definition.QueryTable;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;
import org.nuclos.common.report.ejb3.DatasourceFacadeRemote;
import org.nuclos.common.report.valueobject.ChartVO;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common2.IOUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * <code>CollectController</code> for entity "datasource".
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Boris.Sander@novabit.de">Boris Sander</a>
 * @version 01.00.00
 */
public class ChartCollectController extends AbstractDatasourceCollectController<ChartVO>  {
	
	// former Spring injection
	
	private DatasourceFacadeRemote datasourceFacadeRemote;
	
	// end of former Spring injection

	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton} 
	 * to get an instance.
	 * 
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	public ChartCollectController(MainFrameTab tabIfAny) {
		super(new CollectableMasterDataEntity(E.CHART), tabIfAny);
		setDatasourceFacadeRemote(SpringApplicationContextHolder.getBean(DatasourceFacadeRemote.class));

		CollectableMasterDataEntity clctEntity = new CollectableMasterDataEntity(E.CHART);
		initializeDatasourceCollectController(
			new DatasourceEditPanel(this, 
				new CollectableEntityFieldWithEntity(clctEntity, E.CHART.name.getUID()),
				new CollectableEntityFieldWithEntity(clctEntity, E.CHART.detailsEntity.getUID()),
				new CollectableEntityFieldWithEntity(clctEntity, E.CHART.description.getUID()),
				new CollectableEntityFieldWithEntity(clctEntity, E.CHART.parentEntity.getUID()),
				false, null));
		
		_setupResultToolBar();
	}
	
	final void setDatasourceFacadeRemote(DatasourceFacadeRemote datasourceFacadeRemote) {
		this.datasourceFacadeRemote = datasourceFacadeRemote;
	}
	
	final DatasourceFacadeRemote getDatasourceFacadeRemote() {
		return datasourceFacadeRemote;
	}
	
	private void _setupResultToolBar() {
		this.getResultPanel().addToolBarComponent(new JButton(new AbstractAction("", Icons.getInstance().getIconTree16()) {
			@Override
			public void actionPerformed(ActionEvent e) {
				getMainController().getExplorerController().cmdShowDatasources(null);
			}
		}));
	}
	
	@Override
	protected List<JButton> getAdditionalToolbarButtons() {
		List<JButton> lstButtons = new ArrayList<JButton>();
		
		Action a = NuclosToolBarActions.SHOW_IN_EXPLORER.init(new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cmdJumpToTree();
			}
		});
		final JButton btnMakeTreeRoot = new JButton(a);
//		btnMakeTreeRoot.setIcon(Icons.getInstance().getIconMakeTreeRoot16());
//		btnMakeTreeRoot.setToolTipText(getSpringLocaleDelegate().getMessage("DatasourceCollectController.14","In Explorer anzeigen"));
//		btnMakeTreeRoot.addActionListener(new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent ev) {
//				cmdJumpToTree();
//			}
//		});
		lstButtons.add(btnMakeTreeRoot);
		
		return lstButtons;
	}

	/**
	 * show datasource in exlporer tree
	 *
	 */
	protected void cmdJumpToTree() {
		UIUtils.runCommand(this.getTab(), new Runnable() {
			@Override
			public void run() {
				final UID iDatasourceId = getSelectedCollectableId();
				getMainController().getExplorerController().cmdShowDatasources(iDatasourceId);
			}
		});
	}

	@Override
	public void detailsChanged(Component compSource) {
		super.detailsChanged(compSource);
	}

	@Override
	protected boolean isNewAllowed() {
		return isSaveAllowed(false);
	}

	@Override
	protected boolean isSaveAllowed(final boolean useCurrentCollectable) {
		return SecurityCache.getInstance().isWriteAllowedForMasterData(entityUid);
	}

	@Override
	protected boolean isDeleteSelectedCollectableAllowed(){
		return SecurityCache.getInstance().isDeleteAllowedForMasterData(entityUid);
	}

	@Override
	public CollectableDataSource<ChartVO> findCollectableById(UID sEntity, UID oId, final int dependantsDepth) throws CommonBusinessException {
		return new CollectableDataSource<ChartVO>(datasourcedelegate.getChart(oId));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CollectableDataSource<ChartVO> newCollectable() {
		return new CollectableDataSource<ChartVO>(new ChartVO(null, null, null, null, null));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean stopEditingInDetails() {
		return pnlEdit.stopEditing();
	}

	/**
	 * generate the sql statement from the current definition of this datasource
	 * @return
	 * @throws CommonBusinessException
	 */
	@Override
	public String generateSql() throws CommonBusinessException {
		return datasourcedelegate.createSQL(new ChartVO(null, null, pnlEdit.getQueryEditor().getXML(new DatasourceEntityOptions(true)), false, null));
	}

	/**
	 * @param sXML
	 */
	@Override
	protected void importXML(String sXML) throws NuclosBusinessException {
		final String sWarnings = QueryBuilderEditor.getSkippedElements(pnlEdit.getQueryEditor().setXML(sXML));
		if (sWarnings.length() > 0) {
			JOptionPane.showMessageDialog(getTab(), SpringLocaleDelegate.getInstance().getMessage(
					"DatasourceCollectController.12","Folgende Elemente existieren nicht mehr in dem aktuellen Datenbankschema\n und wurden daher entfernt:") + "\n" + sWarnings);
		}
		detailsChanged(pnlEdit.getQueryEditor());
	}

	/**
	 * @param file
	 * @throws IOException
	 * @throws NuclosBusinessException
	 */
	@Override
	protected void exportXML(File file) throws IOException, CommonBusinessException {
		final String sReportXML = pnlEdit.getQueryEditor().getXML(new DatasourceEntityOptions(true));
		IOUtils.writeToTextFile(file, sReportXML, "UTF-8");
	}

	@Override
	protected void validateSQL() {
		try {
			getDatasourceFacadeRemote().validateSqlFromXML(new ChartVO(null, null, pnlEdit.getQueryEditor().getXML(new DatasourceEntityOptions(true)), false, null));
			JOptionPane.showMessageDialog(getTab(), getSpringLocaleDelegate().getMessage(
					"DatasourceCollectController.10","Die SQL Abfrage ist syntaktisch korrekt."));
		}
		catch (Exception ex) {
			Errors.getInstance().showExceptionDialog(getTab(), ex);
		}
	}

	@Override
	protected void readValuesFromEditPanel(CollectableDataSource<ChartVO> clct,
		boolean bSearchTab) throws CollectableValidationException {
		// if we have a search tab here, this method must be extended.
		assert !bSearchTab;

		super.readValuesFromEditPanel(clct, bSearchTab);

		try {
			clct.getDatasourceVO().setSource(pnlEdit.getQueryEditor().getXML(new DatasourceEntityOptions(true)));
		}
		catch (CommonBusinessException ex) {
			throw new CollectableValidationException(this.getCollectableEntity().getEntityField(E.CHART.source.getUID()), ex);
		}
	}

	@Override
	protected List<ColumnEntry> getDefaultColumns() {
		List<ColumnEntry> result = new ArrayList<ColumnEntry>();
		
		final ColumnEntry INTID = new ColumnEntry();
		INTID.setAlias("INTID");
		INTID.setVisible(true);
		INTID.setDefaultWidth(120);
		result.add(INTID);
		
		final ColumnEntry INTID_T_UD_GENERICOBJECT = new ColumnEntry();
		INTID_T_UD_GENERICOBJECT.setAlias("INTID_T_UD_GENERICOBJECT");
		INTID_T_UD_GENERICOBJECT.setVisible(true);
		INTID_T_UD_GENERICOBJECT.setDefaultWidth(180);
		result.add(INTID_T_UD_GENERICOBJECT);
		
		return result;
	}
	
	@Override
	protected List<DatasourceParameterVO> getDefaultParameters() {
		List<DatasourceParameterVO> result = new ArrayList<DatasourceParameterVO>();
		
		final DatasourceParameterVO INTID_T_UD_GENERICOBJECT = new DatasourceParameterVO(
				null, "genericObject", QueryBuilderConstants.PARAMETER_TYPE_LONG, null, true);
		result.add(INTID_T_UD_GENERICOBJECT);
		
		return result;
	}

	@Override
	public Set<String> getQueryTypes() {
		HashSet<String> result = new HashSet<String>();
		result.add(QueryTable.QUERY_TYPE_CHART);
		return result;
	}

	@Override
	public boolean isWithParameterEditor() {
		return true;
	}

	@Override
	public boolean isParameterEditorWithValuelistProviderColumn() {
		return true;
	}

	@Override
	public boolean isParameterEditorWithLabelColumn() {
		return true;
	}
}
