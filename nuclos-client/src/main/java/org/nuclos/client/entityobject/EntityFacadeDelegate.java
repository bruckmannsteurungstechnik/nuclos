//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.entityobject;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.masterdata.ejb3.CollectableFieldsByNameParams;
import org.nuclos.server.masterdata.ejb3.EntityFacadeRemote;

// @Component
public class EntityFacadeDelegate implements EntityFacadeRemote {

	private static final Logger LOG = Logger.getLogger(EntityFacadeDelegate.class);

	private static EntityFacadeDelegate INSTANCE;

	private EntityFacadeRemote facade;

	EntityFacadeDelegate() {
		INSTANCE = this;
	}

	public static EntityFacadeDelegate getInstance() {
		if (INSTANCE == null) {
			// lazy support
			INSTANCE = SpringApplicationContextHolder.getBean(EntityFacadeDelegate.class);
		}
		return INSTANCE;
	}

	// @Autowired
	public final void setEntityFacadeRemote(EntityFacadeRemote entityFacadeRemote) {
		this.facade = entityFacadeRemote;
	}

	@Override
	public List<CollectableField> getCollectableFieldsByName(
			final CollectableFieldsByNameParams params
	) throws CommonBusinessException {
		return facade.getCollectableFieldsByName(
				params
		);
	}

	public Map<EntityAndField, UID> getSubFormEntityAndParentSubFormEntityNames(UID layoutUID) {
		return facade.getSubFormEntityAndParentSubFormEntityNames(layoutUID);
	}

	@Override
	public List<CollectableValueIdField> getQuickSearchResult(
			UID field,
			String search,
			UID vlpId,
			Map<String, Object> vlpParameter,
			Long iMaxRowCount,
			UID mandator
	) throws CommonBusinessException {
		return facade.getQuickSearchResult(field, search, vlpId, vlpParameter, iMaxRowCount, mandator);
	}

	@Override
	public List<CollectableValueIdField> getQuickSearchResult(
			FieldMeta<?> efMeta,
			String search,
			UID vlpId,
			Map<String, Object> vlpParameter,
			Long iMaxRowCount,
			UID mandator
	) throws CommonBusinessException {
		return facade.getQuickSearchResult(efMeta, search, vlpId, vlpParameter, iMaxRowCount, mandator);
	}

	@Override
	public UID getBaseEntity(UID dynamicentityname) {
		return facade.getBaseEntity(dynamicentityname);
	}

}
