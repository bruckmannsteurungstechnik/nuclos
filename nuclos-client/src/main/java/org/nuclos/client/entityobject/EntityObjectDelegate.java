package org.nuclos.client.entityobject;

import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.nuclos.common.CommonEntityObjectFacade;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.ejb3.EntityObjectFacadeRemote;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.genericobject.ProxyList;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;

public final class EntityObjectDelegate implements CommonEntityObjectFacade {

	private static final Logger LOG = Logger.getLogger(EntityObjectDelegate.class);

	private static EntityObjectDelegate INSTANCE;
	
	//

	private EntityObjectFacadeRemote entityObjectFacadeRemote;

	EntityObjectDelegate() {
		INSTANCE = this;
	}

	public static EntityObjectDelegate getInstance() {
		if (INSTANCE == null) {
			throw new IllegalStateException("too early");
		}
		return INSTANCE;
	}
	
	public final void setEntityObjectFacadeRemote(EntityObjectFacadeRemote entityObjectFacadeRemote) {
		this.entityObjectFacadeRemote = entityObjectFacadeRemote;
	}

	@Override
	public <PK> EntityObjectVO<PK> get(UID entityUid, PK id) throws CommonPermissionException {
		return entityObjectFacadeRemote.get(entityUid, id);
	}

	@Override
	public <PK> List<PK> getEntityObjectIds(UID entity, CollectableSearchExpression cse) throws CommonPermissionException {
		return entityObjectFacadeRemote.getEntityObjectIds(entity, cse);
	}

	@Override
	public <PK> ProxyList<PK,EntityObjectVO<PK>> getEntityObjectProxyList(UID entityUid, CollectableSearchExpression clctexpr,
			Collection<UID> fields, String customUsage) throws CommonPermissionException {
		return entityObjectFacadeRemote.getEntityObjectProxyList(entityUid, clctexpr, fields, customUsage);
	}

	@Override
	public <PK> EntityObjectVO<PK> getByIdWithDependents(UID entityUid, PK id, Collection<UID> fields, String customUsage)
		throws CommonPermissionException {
		return entityObjectFacadeRemote.getByIdWithDependents(entityUid, id, fields, customUsage);
	}
	
	@Override
	public <PK> Collection<EntityObjectVO<PK>> getEntityObjectsChunk(
			UID entityUid,
			CollectableSearchExpression clctexpr,
			ResultParams resultParams,
			String customUsage
	) throws CommonPermissionException {
		return entityObjectFacadeRemote.getEntityObjectsChunk(
				entityUid,
				clctexpr,
				resultParams,
				customUsage
		);
	}
	
	@Override
	public Long countEntityObjectRows(UID entityUid, CollectableSearchExpression clctexpr) throws CommonPermissionException {
		return entityObjectFacadeRemote.countEntityObjectRows(entityUid, clctexpr);
	}

	@Override
	public Long countEntityObjectRowsWithLimit(final UID entityUid, final CollectableSearchExpression clctexpr, final Long limit) throws CommonPermissionException {
		return entityObjectFacadeRemote.countEntityObjectRowsWithLimit(entityUid, clctexpr, limit);
	}

	@Override
	public <PK> Collection<EntityObjectVO<PK>> getDependentEntityObjects(UID sEntityName, UID sForeignKeyField,
			PK oRelatedId) throws CommonPermissionException {
		return entityObjectFacadeRemote.getDependentEntityObjects(sEntityName, sForeignKeyField, oRelatedId);
	}

	@Override
	public void removeEntity(UID nameUid, Object id) throws CommonPermissionException {
		entityObjectFacadeRemote.removeEntity(nameUid, id);
	}

	@Override
	public void remove(EntityObjectVO<?> entity) throws CommonPermissionException {
		entityObjectFacadeRemote.remove(entity);
	}

	@Override
	public <PK> EntityObjectVO<PK> getReferenced(UID referencingEntity, UID referencingEntityField, PK id) throws CommonBusinessException {
		return entityObjectFacadeRemote.getReferenced(referencingEntity, referencingEntityField, id);
	}

	@Override
	public <PK> void createOrUpdatePlain(EntityObjectVO<PK> entity) throws CommonPermissionException {
		entityObjectFacadeRemote.createOrUpdatePlain(entity);
	}

	@Override
	public Integer getVersion(UID entityUid, Object id) throws CommonPermissionException {
		return entityObjectFacadeRemote.getVersion(entityUid, id);
	}

	@Override
	public <PK> EntityObjectVO<PK> insert(EntityObjectVO<PK> entity)
			throws CommonPermissionException, CommonCreateException,
			CommonPermissionException, NuclosBusinessRuleException {
		return entityObjectFacadeRemote.insert(entity);
	}

	@Override
	public <PK> void delete(UID entity, PK pk, boolean logicalDeletion, boolean bRemoveDependents)
			throws CommonFinderException, CommonRemoveException,
			CommonStaleVersionException, NuclosBusinessException,
			CommonPermissionException,
			CommonCreateException {
		entityObjectFacadeRemote.delete(entity, pk, logicalDeletion, bRemoveDependents);
	}

	@Override
	public <PK> EntityObjectVO<PK> update(EntityObjectVO<PK> eoVO, boolean bCollectiveProcessing)
			throws CommonCreateException, CommonFinderException,
			CommonRemoveException, CommonStaleVersionException,
			CommonValidationException, CommonPermissionException,
			NuclosBusinessException, NoSuchElementException {
	
		return entityObjectFacadeRemote.update(eoVO, bCollectiveProcessing);
	}

	@Override
	public <PK> EntityObjectVO<PK> clone(final EntityObjectVO<PK> eoVO, final UID layout, final boolean fetchFromDb) throws CommonBusinessException {
		return entityObjectFacadeRemote.clone(eoVO, layout, fetchFromDb);
	}

	@Override
	public <PK> void setDefaultValues(EntityObjectVO<PK> eo) {
		entityObjectFacadeRemote.setDefaultValues(eo);
	}
	
	@Override
	public void cancelRunningStatements(UID entity) {
		entityObjectFacadeRemote.cancelRunningStatements(entity);
	}
	
	public <PK> void unlock(UID entityUID, PK pk) throws CommonPermissionException {
		entityObjectFacadeRemote.unlock(entityUID, pk);
	}

	@Override
	public <PK> EntityObjectVO<PK> executeBusinessRules(List<EventSupportSourceVO> lstRuleVO, EntityObjectVO<PK> eoVO, String customUsage, boolean isCollectiveProcessing)
			throws CommonBusinessException {
		return entityObjectFacadeRemote.executeBusinessRules(lstRuleVO, eoVO, customUsage, isCollectiveProcessing);
	}

	@Override
	public <PK> void executeBusinessRuleForPks(EventSupportSourceVO ruleVO, UID entity, Collection<PK> pks)
			throws CommonBusinessException {
		entityObjectFacadeRemote.executeBusinessRuleForPks(ruleVO, entity, pks);
	}

}
