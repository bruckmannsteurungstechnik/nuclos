//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.explorer.configuration;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.ToolTipManager;

import org.nuclos.client.main.mainframe.MainFrameTab;

/**
 * DefaultConfigurationExplorerView
 * 
 * @author Moritz Neuhäuser &lt;moritz.neuhaeuser@nuclos.de&gt;
 */
public class DefaultConfigurationExplorerView extends JPanel implements ConfigurationExplorerView<ConfigurationTreeNode<?>> {

	public static final int FADE = 2;
	private final JScrollPane scrlpn = new JScrollPane();
	private final JTree tree;
	private final JPanel content;

	public DefaultConfigurationExplorerView(final MainFrameTab parent, ConfigurationEntityTreeNode tn) {
		super(new BorderLayout());
		this.setOpaque(false);
		this.setBorder(BorderFactory.createEmptyBorder());

		this.scrlpn.setBorder(BorderFactory.createEmptyBorder());
		this.scrlpn.setOpaque(false);
		this.scrlpn.getViewport().setOpaque(false);

		this.content = new JPanel(new BorderLayout());
		/*
		this.content = new JPanel(new BorderLayout()) {

			@Override
			public void paint(Graphics g) {
				Graphics2D g2 = (Graphics2D) g;
				g2.setPaint(new GradientPaint(0, 0, NuclosThemeSettings.BACKGROUND_PANEL, 0, FADE, Color.WHITE));
				g2.fillRect(0, 0, getWidth(), getHeight());

				super.paint(g);
			}
		};
*/
		this.content.setOpaque(true);
		this.content.setBackground(new Color (0, 0, 0, 0));
		this.content.setBorder(BorderFactory.createEmptyBorder(FADE, 0, 0, 0));
		this.content.add(this.scrlpn, BorderLayout.CENTER);

		this.add(this.content, BorderLayout.CENTER);

		
		this.tree = new JTree(tn, true);

		this.tree.putClientProperty("JTree.lineStyle", "Angled");
		this.tree.setBorder(BorderFactory.createEmptyBorder(0, 5, 5, 5));
		this.tree.setBackground(Color.WHITE);
		this.tree.setRootVisible(true);
		this.tree.setShowsRootHandles(true);

		// enable drag:
		this.tree.setDragEnabled(false);
		//this.tree.setTransferHandler(new DefaultTransferHandler(Main.getInstance().getMainFrame()));

		// don't expand on double click:
		this.tree.setToggleClickCount(0);

		//this.tree.addKeyListener(new DefaultKeyListener(tree));
		this.tree.addMouseListener(new ConfigurationExplorerMouseListener(parent, this));

		// enable tool tips:
		ToolTipManager.sharedInstance().registerComponent(this.tree);

		this.tree.setCellRenderer(new ConfigurationExplorerNodeRenderer());
		//this.tree.addTreeWillExpandListener(new DefaultTreeWillExpandListener(tree));

		this.scrlpn.getViewport().add(tree, null);
	}
	
	@Override
	public JComponent getViewComponent() {
		return this;
	}

	@Override
	public JTree getJTree() {
		return tree;
	}

	@Override
	public ConfigurationTreeNode<?> getRootNode() {
		return (ConfigurationTreeNode<?>) this.getJTree().getModel().getRoot();
	}

}
