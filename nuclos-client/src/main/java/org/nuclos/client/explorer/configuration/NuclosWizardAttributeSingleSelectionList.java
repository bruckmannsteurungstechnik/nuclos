//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.explorer.configuration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.JList;
import javax.swing.ListSelectionModel;

import org.nuclos.client.wizard.model.Attribute;
import org.nuclos.client.wizard.steps.NuclosEntityNameStep;
import org.nuclos.client.wizard.util.NuclosWizardUtils;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;

/**
 * NuclosWizardAttributeSingleSelectionList
 * 
 * @author Moritz Neuhäuser &lt;moritz.neuhaeuser@nuclos.de&gt;
 */
public class NuclosWizardAttributeSingleSelectionList extends JList {

	// Comparator for list elements
	private final Comparator<Attribute> comparator = new Comparator<Attribute>() {

			@Override
         public int compare(Attribute o1,Attribute o2) {
				final String o1label = (null == o1.getLabel()) ? o1.getInternalName() : o1.getLabel(); 
				final String o2label = (null == o2.getLabel()) ? o2.getInternalName() : o2.getLabel(); 
				return o1label.compareToIgnoreCase(o2label);
         }
	};
	private List<Attribute> lstAttribute;
	
	public NuclosWizardAttributeSingleSelectionList() {
		this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
	}

	
	public void fill(final EntityMeta entityMetaVO, final Collection<FieldMeta<?>> lstFieldMeta ) {
		
		// transform to wizard attribute
		this.lstAttribute = CollectionUtils.transform(lstFieldMeta, new Transformer<FieldMeta<?>, Attribute>() {

			@Override
			public
			Attribute transform(final FieldMeta<?> vo) {
				try {
					return NuclosEntityNameStep.wrapEntityMetaFieldVO(vo, NuclosWizardUtils.isSystemField(entityMetaVO, vo));
				} catch (CommonFinderException e) {
					throw new IllegalStateException(e);
				} catch (CommonPermissionException e) {
					throw new IllegalStateException(e);
				}
			}
			
		});
		
		// sort by comparator
		Collections.sort(this.lstAttribute,comparator);
		
        
		// apply to model
    	this.setListData(this.lstAttribute.toArray());
    	
    	
	}
	
	public void clear() {
		this.setListData(new ArrayList<Attribute>().toArray());
	}
	
	public String getSelectedAttribute() {
		String selectedAttribute = null;
		Attribute selectedValue = (Attribute) getSelectedValue();
		
		if (null != selectedValue) {
			selectedAttribute = NuclosWizardUtils.getStapledString(selectedValue.getInternalName());
		}
		return selectedAttribute;
	}
}
