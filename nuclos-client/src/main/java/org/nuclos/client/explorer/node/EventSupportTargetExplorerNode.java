package org.nuclos.client.explorer.node;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JTree;

import org.apache.log4j.Logger;
import org.nuclos.client.explorer.node.eventsupport.EventSupportTargetTreeNode;
import org.nuclos.client.explorer.node.eventsupport.EventSupportTargetType;
import org.nuclos.client.explorer.node.eventsupport.EventSupportTreeNode;
import org.nuclos.client.rule.server.EventSupportActionHandler;
import org.nuclos.client.rule.server.EventSupportManagementController;
import org.nuclos.client.rule.server.EventSupportPreferenceHandler;
import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.client.ui.Icons;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.navigation.treenode.TreeNode;

public class EventSupportTargetExplorerNode extends AbstractEventSupportExplorerNode {
	
	private static final Logger LOG = Logger.getLogger(EventSupportTargetExplorerNode.class);
	
	public EventSupportTargetExplorerNode(TreeNode node)
	{
		super(node);
	}
		
	@Override
	public boolean isLeaf() {
		EventSupportTargetTreeNode node = (EventSupportTargetTreeNode) getTreeNode();
		return node.getSubNodes() == null || node.getSubNodes().isEmpty();
	}

	@Override
	public Icon getIcon() {

		Icon result = null;
		EventSupportTargetType treeNodeType = ((EventSupportTargetTreeNode) getUserObject()).getTreeNodeType();
		
		if (treeNodeType == null)
			 return null;
		
		switch (treeNodeType) 
		{
		case NUCLET:
			result = Icons.getInstance().getEventSupportNucletIcon();
			break;
		case EVENTSUPPORT:
			result = Icons.getInstance().getEventSupportRuleIcon();
			try {
				EventSupportSourceVO essVO = 
						EventSupportRepository.getInstance().
						getEventSupportByClassname(((EventSupportTreeNode) getUserObject()).getNodeName());
				if (!essVO.isActive()) {
					result = Icons.getInstance().getEventSupportInactiveRuleIcon();	
				}
			} catch (Exception e) {}
			
			break;
		case ENTITY_CATEGORIE:
		case ALL_ENTITIES_ENTITY:
			result = Icons.getInstance().getEventSupportEntityIcon();
			break;
		case STATEMODEL_CATEGORIE:
		case ALL_STATEMODELS_STATEMODEL:
			result = Icons.getInstance().getEventSupportTransitionIcon();
			break;
		case JOB_CATEGORIE:
		case ALL_JOBS_JOB:
			result = Icons.getInstance().getEventSupportJobIcon();
			break;
		case GENERATION_CATEGORIE:
		case ALL_GENERATIONS_GENERATION:
			result = Icons.getInstance().getEventSupportWorkstepIcon();
			break;
		case LAYOUT:
		case LAYOUT_BUTTONS:
			break;
		case LAYOUT_CATEGORIE:
			result = Icons.getInstance().getEventSupportLayoutIcon();
			break;
		default:
			break;
		}
		return result;
	}

	@Override
	public Action getTreeNodeActionOnMouseClick(JTree tree) {
		return new EventSupportTargetShowPropertyAction(tree);
	}
	
	public String getPreferenceNodeName () {
		return EventSupportPreferenceHandler.PREF_NODE_EVENTSUPPORTS_TARGETS;
	}
	
	/* (non-Javadoc)
	 * On the source side the default action on double-click is to op the eventsupport (if chosen and editable)
	 * in the editor
	 * @see org.nuclos.client.explorer.ExplorerNode#TreeNodeActionCommand(javax.swing.JTree)
	 */
	public String getDefaultTreeNodeActionCommand(JTree tree) {
		
		String retVal = null;
		
		final EventSupportTargetExplorerNode currentNode = (EventSupportTargetExplorerNode) tree.getSelectionPath().getLastPathComponent();
		final EventSupportTreeNode treeNode = currentNode.getTreeNode();
		
		if (EventSupportTargetType.EVENTSUPPORT.equals(treeNode.getTreeNodeType())) {
			retVal = EventSupportActionHandler.OPEN_EVENTSUPPORT;
		}
		
		return retVal;
	}
	
	private static class EventSupportTargetShowPropertyAction extends AbstractAction
	{
		private JTree tree;
		
		public EventSupportTargetShowPropertyAction(JTree tree) {
			this.tree = tree;
		}

		@Override
		public void actionPerformed(ActionEvent ev) {
			// selected tree element	
			
			final EventSupportTargetExplorerNode node= (EventSupportTargetExplorerNode) tree.getSelectionPath().getLastPathComponent();
			
			// show infos and properties for this node
			EventSupportManagementController controller = node.getTreeNode().getController();
			try {
				controller.showTargetSupportProperties(node.getTreeNode());
			} catch (CommonFinderException e) {
				LOG.warn("EventSupportTargetShowPropertyAction failed: " + e, e);
			} catch (CommonPermissionException e) {
				LOG.warn("EventSupportTargetShowPropertyAction failed: " + e, e);
			}					
		}
		
	}
	
}
