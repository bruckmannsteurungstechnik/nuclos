package org.nuclos.client.explorer.node.eventsupport;

import javax.swing.JTree;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreePath;

import org.apache.log4j.Logger;
import org.nuclos.client.explorer.node.AbstractEventSupportExplorerNode;
import org.nuclos.client.rule.server.EventSupportPreferenceHandler;
import org.nuclos.client.ui.UIUtils;

public class EventSupportTreeWillExpandListener implements
		TreeWillExpandListener {

private static final Logger LOG = Logger.getLogger(EventSupportTreeWillExpandListener.class);
	
	private final JTree tree;

	public EventSupportTreeWillExpandListener(JTree tree) {
		super();
		this.tree = tree;
	}
	
	@Override
	public void treeWillExpand(final TreeExpansionEvent event)
			throws ExpandVetoException {
		UIUtils.setWaitCursor(tree);
		UIUtils.runCommand(tree, new Runnable() {
			@Override
            public void run() {
				try {
					if (event.getPath().getLastPathComponent() != null && event.getPath().getLastPathComponent() instanceof AbstractEventSupportExplorerNode) {
						final AbstractEventSupportExplorerNode explorernode = (AbstractEventSupportExplorerNode) event.getPath().getLastPathComponent();
						final boolean bSubNodesHaventBeenLoaded = !(explorernode.getChildCount() > 0);
						if (bSubNodesHaventBeenLoaded) {
							explorernode.loadChildren(true);
						}
						
						// Store current Tree expand in Prefs
						if (event.getPath().getPath()[0] == null || event.getPath().getPath()[0].toString().trim().length() == 0) {
							Object[] path = event.getPath().getPath();
							path[0] = "Regelsourcen";
							EventSupportPreferenceHandler.getInstance().addTreePath(explorernode.getPreferenceNodeName(), new TreePath(path));					
							
						}
						else {
							EventSupportPreferenceHandler.getInstance().addTreePath(explorernode.getPreferenceNodeName(), event.getPath());						
						}
					}
					
				}
				catch (Exception e) {
					LOG.error("DefaultTreeWillExpandListener.treeWillExpand: " + e, e);
				}            		
			}
		});
		tree.setCursor(null);
	}

	@Override
	public void treeWillCollapse(TreeExpansionEvent event)
			throws ExpandVetoException {
		
		String nodeName = null;
		Object lastPathComponent = event.getPath().getLastPathComponent();
		if (lastPathComponent instanceof AbstractEventSupportExplorerNode)
			nodeName = ((AbstractEventSupportExplorerNode)lastPathComponent).getPreferenceNodeName();
		if (nodeName != null) {
			// Store current Tree expand in Prefs
			if (event.getPath().getPath()[0] == null || event.getPath().getPath()[0].toString().trim().length() == 0) {
				Object[] path = event.getPath().getPath();
				path[0] = "Regelsourcen";
				EventSupportPreferenceHandler.getInstance().removeTreePath(nodeName, new TreePath(path));					
				
			} else {
				EventSupportPreferenceHandler.getInstance().removeTreePath(nodeName, event.getPath());
			}
		}
	}

}
