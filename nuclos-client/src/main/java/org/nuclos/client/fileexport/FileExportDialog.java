package org.nuclos.client.fileexport;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.File;
import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.xml.stream.XMLStreamException;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.ui.renderer.EntityObjectValueListCellRenderer;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.fileimport.ejb3.XmlImportFacadeRemote;
import org.nuclos.server.genericobject.ProxyList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import info.clearthought.layout.TableLayout;

@Configurable
public class FileExportDialog extends JPanel {
	
	private static final Logger LOG = Logger.getLogger(FileExportDialog.class);
	
	private static final double TLS[][] = {
		// columns:
		{ 15, 80, 5, 80, 5, 80, 5, 120, 1 },
		// rows:
		{ 5, 20, 5, 20, 5, 20, 5, 20, 5, 20, 5 }
	};
	
	private final UID entityUid;
	
	// GUI
	
	private JComboBox xmlImportFileChooser;
	
	private JTextField outputFile;
	
	// end GUI
	
	// Spring injection
	
	@Autowired
	private XmlImportFacadeRemote xmlImportFacade;
	
	@Autowired
	private SpringLocaleDelegate localeDelegate;
	
	@Autowired
	private MetaProvider metaProvider;
	
	// end of Spring injection
	
	public FileExportDialog(UID entityUid) {
		this.entityUid = entityUid;
	}
	
	@PostConstruct
	void init() {
		final String entityName = metaProvider.getEntity(entityUid).getEntityName();
		final ProxyList<UID, EntityObjectVO<UID>> list = xmlImportFacade.getFileImportsForEntity(entityUid);
		final int size = list.size();
		setLayout(new TableLayout(TLS));
		if (size == 0) {
			final JLabel warn = new JLabel(
					localeDelegate.getMsg("xml.fileexport.no_xml_file_structure"));
			add(warn, "1, 1, 7, 1");
		} else {
			final JLabel ifl = new JLabel(
					localeDelegate.getMsg("xml.fileexport.xml_file_definition.label"));
			add(ifl, "1, 1, 7, 1");
			xmlImportFileChooser = new JComboBox();
			xmlImportFileChooser.setRenderer(new EntityObjectValueListCellRenderer<UID>(E.XMLIMPORTFILE.description));
			for (EntityObjectVO<UID> fileImport: list) {
				xmlImportFileChooser.addItem(fileImport);
			}
			add(xmlImportFileChooser, "1, 3, 7, 3");
			
			final JLabel fl = new JLabel(localeDelegate.getMsg("xml.fileexport.outputfile.name"));
			add(fl, "1, 5, 7, 5");
			outputFile = new JTextField(entityName + ".xml");
			add(outputFile, "1, 7, 5, 7");
			/*
			outputFile.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					LOG.info("file text field action event: " + e);
					file = new File(e.getActionCommand());
					outputFile.setText(file.getAbsolutePath());
				}
			});
			 */
			outputFile.addFocusListener(new FocusAdapter() {
				
				@Override
				public void focusLost(FocusEvent e) {
					LOG.info("file text field action event: " + e);
					final File file = new File(outputFile.getText());
					outputFile.setText(file.getAbsolutePath());
				}
				
			});
			final JButton sf = new JButton(localeDelegate.getMsg("xml.fileexport.outputfile.button"));
			add(sf, "7, 7");
			sf.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					final JFileChooser fc = new JFileChooser();
					final String f = outputFile.getText();
					if (!StringUtils.isNullOrEmpty(f)) {
						fc.setSelectedFile(new File(f));
					} else {
						fc.setSelectedFile(new File(entityName + ".xml"));
					}
					final int result = fc.showOpenDialog(FileExportDialog.this);
					if (result == JFileChooser.APPROVE_OPTION) {
						final File file = fc.getSelectedFile();
						outputFile.setText(file.getAbsolutePath());
					}
				}
			});
			final JTextField info = new JTextField();
			info.setEditable(false);
			info.setForeground(Color.RED);
			add(info, "1, 9, 7, 9");
		}
	}
	
	public File getSelectedFile() {
		return new File(outputFile.getText());
	}
	
	/**
	 * Could be null if there is not match XML file structure definition.
	 */
	public EntityObjectVO<UID> getSelectXmlImportFile() {
		if (xmlImportFileChooser ==  null) {
			return null;
		}
		return (EntityObjectVO<UID>) xmlImportFileChooser.getSelectedItem();
	}
	
	public byte[] export(CollectableSearchCondition cond) throws CommonPermissionException, IOException, XMLStreamException {
		final EntityObjectVO<UID> xmlImportFile = getSelectXmlImportFile();
		if (xmlImportFile != null) {
			return xmlImportFacade.exportFile("root", xmlImportFile.getPrimaryKey(), entityUid, cond);
		} else {
			return null;
		}
	}

}
