//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject;

import java.util.Map;

import org.apache.commons.lang.NullArgumentException;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.i18n.language.data.DataLanguageContext;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.attribute.DynamicAttributeVO;
import org.nuclos.common.collect.collectable.AbstractCollectable;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.IUncompletable;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Removable;
import org.nuclos.common.dal.vo.IDataLanguageMap;
import org.nuclos.common2.IdUtils;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.server.genericobject.valueobject.GenericObjectVO;
import org.nuclos.server.i18n.language.data.DataLanguageLocalizedEntityEntry;
import org.nuclos.server.i18n.language.data.DataLanguageUtils;

/**
 * Makes a <code>GenericObjectVO</code> <code>Collectable</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class CollectableGenericObject extends AbstractCollectable<Long> implements IUncompletable, Removable {

	public static final String ATTRIBUTENAME_NAME = GenericObjectVO.ATTRIBUTENAME_NAME;
	
	private final GenericObjectVO govo;

	private final Map<UID, CollectableField> mpFields = CollectionUtils.newHashMap();

	private UID userDataLangUID = DataLanguageContext.getDataUserLanguage();
	private UID datalangUID = DataLanguageContext.getDataSystemLanguage();
	
	/**
	 * §precondition govo != null
	 */
	public CollectableGenericObject(GenericObjectVO govo) {
		if (govo == null) {
			throw new NullArgumentException("govo");
		}
		this.govo = govo;
	}
	
	@Override
	public Long getId() {
		return getGenericObjectCVO().getId();
	}

	@Override
	public void removeId() {
		getGenericObjectCVO().setPrimaryKey(null);
	}

	/**
	 * §postcondition result != null
	 * 
	 * @return the encapsulated <code>GenericObjectVO</code>.
	 */
	public GenericObjectVO getGenericObjectCVO() {
		final GenericObjectVO result = this.govo;
		assert result != null;
		return result;
	}

	public CollectableEntity getCollectableEntity() {
		return CollectableGenericObjectEntity.getByModuleUid(govo.getModule());
	}

	@Override
	public int getVersion() {
		return this.govo.getVersion();
	}

	@Override
	public boolean isMarkedRemoved() {
		return this.getGenericObjectCVO().isRemoved();
	}

	@Override
	public void markRemoved() {
		this.getGenericObjectCVO().remove();
	}

	/**
	 * @param sName
	 * @return the attribute value with the given name, if any.
	 */
	private DynamicAttributeVO getAttributeValue(UID field) {
		return govo.getAttribute(field);
	}

	private static DynamicAttributeVO newGenericObjectAttributeVO(UID entityUid, UID fieldUID) {
		return newGenericObjectAttributeVO(entityUid, fieldUID, null, null, null);
	}

	private static DynamicAttributeVO newGenericObjectAttributeVO(UID entityUid, UID fieldUID, Long iValueId, UID uidValue, Object oValue) {
		return new DynamicAttributeVO(fieldUID, iValueId, uidValue, oValue);
	}

	@Override
	public boolean isComplete() {
		return this.getGenericObjectCVO().isComplete();
	}

	@Override
	public void markAsUncomplete() {
		getGenericObjectCVO().setComplete(false);
	}	

	public CollectableField getField(UID fieldUid, boolean skipLocalization) {
		CollectableField result;

		// mpFields is used as a cache:
		result = this.mpFields.get(fieldUid);

		if (result == null) {
			DynamicAttributeVO attrvo = this.getAttributeValue(fieldUid);
			if (attrvo == null || attrvo.isRemoved()) {
				attrvo = newGenericObjectAttributeVO(this.govo.getModule(), fieldUid);
			}
			final CollectableEntityField clctef = this.getCollectableEntity().getEntityField(fieldUid);
			if (clctef == null) {
				throw new NuclosFatalException(
						SpringLocaleDelegate.getInstance().getMessage("CollectableGenericObject.1", "Unbekanntes Attribut: {0}", fieldUid));
			}
			
			if (clctef.isLocalized() && !clctef.isCalculated() && !skipLocalization) {				
				result = new CollectableGenericObjectAttributeField(
						getLocalizedValue(attrvo.getAttributeUID(), this.govo, attrvo), clctef.getFieldType());
			} else {
				result = new CollectableGenericObjectAttributeField(attrvo, clctef.getFieldType());				
			}

			this.mpFields.put(fieldUid, result);
		}

		assert result != null;
		return result;
	}
	
	@Override
	public CollectableField getField(UID fieldUid) {
		return getField(fieldUid, false);
	}

	private DynamicAttributeVO getLocalizedValue(UID fieldUID, GenericObjectVO govo, DynamicAttributeVO source) {
		
		Object value = null;
		
		if (this.userDataLangUID == null || this.userDataLangUID.equals(this.datalangUID)) {
			value = govo.getAttribute(fieldUID) != null ? govo.getAttribute(fieldUID).getValue() : null;
		} else {
			if (govo.getDataLanguageMap() == null) {
				value = govo.getAttribute(fieldUID) != null ? govo.getAttribute(fieldUID).getValue() : null;
			} else {
				DataLanguageLocalizedEntityEntry fieldLanguageData = 
						govo.getDataLanguageMap().getDataLanguage(this.userDataLangUID);
				if (fieldLanguageData == null) {
					value = govo.getAttribute(fieldUID) != null ? govo.getAttribute(fieldUID).getValue() : null;			
				} else {
					value = fieldLanguageData.getFieldValue(
							DataLanguageUtils.extractFieldUID(fieldUID));
				}
			}
		}
		
		return new DynamicAttributeVO(source.getAttributeUID(), source.getValueId(), source.getValueUid(), value);
	}
	
	@Override
	public IDataLanguageMap getDataLanguageMap() {
		return this.govo.getDataLanguageMap();
	}
	
	/**
	 * §precondition clctfValue != null
	 * §precondition (clctfValue.getFieldType() == CollectableEntityField.TYPE_VALUEIDFIELD) -&gt; (clctfValue.getValueId() instanceof Integer)
	 */
	@Override
	public void setField(UID fieldUid, CollectableField clctfValue) {
		if (clctfValue == null) {
			throw new NullArgumentException("clctfValue");
		}

		boolean isLocalizedField = MetaProvider.getInstance().getEntityField(fieldUid).isLocalized();
		
		if (clctfValue.isNull()) {
			// remove from cvo:
			final DynamicAttributeVO attrvo = getAttributeValue(fieldUid);
			if (attrvo != null) {
				attrvo.remove();
			}
		}
		else {
			// 1. cvo \u00e4ndern:
			DynamicAttributeVO attrvo = getAttributeValue(fieldUid);
			if (attrvo == null) {
				attrvo = newGenericObjectAttributeVO(this.govo.getModule(), fieldUid);
			}
			else if (attrvo.isRemoved()) {
				attrvo.unremove();
			}
			assert !attrvo.isRemoved();

			attrvo.setValue(clctfValue.getValue());
			if (clctfValue.isIdField() && (this.getCollectableEntity().getEntityField(fieldUid).isReferencing() || this.getCollectableEntity().getEntityField(fieldUid).isIdField())) {
				if (clctfValue.getValueId() == null) {
					attrvo.setValueId(null);
					attrvo.setValueUid(null);
				} else {
					if (clctfValue.getValueId() instanceof Number) {
						attrvo.setValueId(IdUtils.toLongId(clctfValue.getValueId()));
					}
					if (clctfValue.getValueId() instanceof UID) {
						attrvo.setValueUid((UID)clctfValue.getValueId());
					}
				}
			}

			/** @todo OPTIMIZE: This method is very inefficient - check if this is an issue! */
			govo.setAttribute(attrvo);
			if (fieldUid.equals(SF.LOGICALDELETED.getUID(govo.getModule()))) {
				govo.setDeleted((Boolean)attrvo.getValue());
			}
		}

		// 2. Evtl. vorhandenen Cache-Eintrag l\u00f6schen:
		mpFields.remove(fieldUid);

		assert LangUtils.equal(this.getField(fieldUid, isLocalizedField).getValue(), clctfValue.getValue()) :
			SpringLocaleDelegate.getInstance().getMessage("CollectableGenericObject.2", 
				"Das Feld {0} in der Entit\u00e4t {1} enth\u00e4lt den Wert {2} (erwartet: {3})", 
				fieldUid, getCollectableEntity().getUID(), this.getField(fieldUid), clctfValue); 
	}

	@Override
	public UID getEntityUID() {
		return govo.getModule();
	}

	@Override
	public Long getPrimaryKey() {
		return govo.getPrimaryKey();
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("entity=").append(getCollectableEntity());
		result.append(",vo=").append(getGenericObjectCVO());
		result.append(",id=").append(getId());
//Note: Swing (since J1.7 more than before) calls "toString()" often for whatever reason. getIdentifierLabel() shouldn't
//be called therefore as it calls LocaleDelegate and thus uses lot of CPU Power.
//		result.append(",label=").append(getIdentifierLabel());
		result.append(",complete=").append(isComplete());
		result.append("]");
		return result.toString();
	}
	
	@Override
	public boolean isDirty() {
		//TODO: This is far to simple for GenericObject, but should be working in any case. GenericObject should anyway be converted
		//to EntityObject and thus solve the problem
		return true;
	}

	@Override
	public boolean canWrite() {
		return getGenericObjectCVO().canWrite();
	}

	@Override
	public boolean canDelete() {
		return getGenericObjectCVO().canDelete();
	}

	@Override
	public boolean canStateChange() {
		return getGenericObjectCVO().canStateChange();
	}

	@Override
	public String getLocalizedValue(UID field, UID language) {
		if (govo == null || govo.getDataLanguageMap() == null 
			|| govo.getDataLanguageMap().getLanguageMap() == null) return null;
		
		Map<UID, DataLanguageLocalizedEntityEntry> languageMap = 
				govo.getDataLanguageMap().getLanguageMap();
		
		if (languageMap.get(language) != null) {
			Object fieldValue = languageMap.get(language).getFieldValue(
					DataLanguageUtils.extractFieldUID(field));
			return fieldValue != null ? (String) fieldValue : null;
		}
		return null;
		
	}
}
