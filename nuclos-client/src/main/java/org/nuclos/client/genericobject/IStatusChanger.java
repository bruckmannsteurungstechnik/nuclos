package org.nuclos.client.genericobject;

import java.util.List;

import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.statemodel.StateWrapper;
import org.nuclos.common.UID;

/**
 * Created by Oliver Brausch on 2019-08-29.
 */
public interface IStatusChanger {

	void cmdChangeState(StateWrapper stateNew);

	void cmdChangeStates(MainFrameTab tab, StateWrapper stateSource, StateWrapper stateFinal, List<UID> statesNew);

	MainFrameTab getTab();

}
