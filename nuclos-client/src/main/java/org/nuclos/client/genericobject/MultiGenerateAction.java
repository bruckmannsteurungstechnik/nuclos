//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.i18n.language.data.DataLanguageContext;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.ui.collect.InvokeWithInputRequiredSupport;
import org.nuclos.client.ui.multiaction.MultiCollectablesActionController.Action;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.genericobject.GeneratorFailedException;
import org.nuclos.server.genericobject.context.GenerationContext;
import org.nuclos.server.genericobject.context.GenerationContextBuilder;
import org.nuclos.server.genericobject.ejb3.GenerationResult;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;

/**
 * Action used by the controller for creating multiple generic objects
 */
class MultiGenerateAction implements Action<Pair<Collection<EntityObjectVO<Long>>, Long>, GenerationResult> {

	private final MainFrameTab parent;
	private final GeneratorActionVO generatoractionvo;
	
	// former Spring injection
	
	private InvokeWithInputRequiredSupport invokeWithInputRequiredSupport;
	
	// end of former Spring injection

	MultiGenerateAction(MainFrameTab parent, GeneratorActionVO generatoractionvo) {
		this.parent = parent;
		this.generatoractionvo = generatoractionvo;
		
		setInvokeWithInputRequiredSupport(SpringApplicationContextHolder.getBean(InvokeWithInputRequiredSupport.class));
	}

	final void setInvokeWithInputRequiredSupport(InvokeWithInputRequiredSupport invokeWithInputRequiredSupport) {
		this.invokeWithInputRequiredSupport = invokeWithInputRequiredSupport;
	}

	final InvokeWithInputRequiredSupport getInvokeWithInputRequiredSupport() {
		return invokeWithInputRequiredSupport;
	}

	/**
	 * performs the action on the given object.
	 *
	 * @throws CommonBusinessException
	 */
	@Override
	public GenerationResult perform(final Pair<Collection<EntityObjectVO<Long>>, Long> sources, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		final HashMap<String, Serializable> context = new HashMap<String, Serializable>();
		final AtomicReference<GenerationResult> result = new AtomicReference<GenerationResult>();
		getInvokeWithInputRequiredSupport().invoke(new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				String customUsage = ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY);
				try {
					if (generatoractionvo.isGroupAttributes()) {
						GenerationContext<Long> context = new GenerationContextBuilder<>(sources.x, generatoractionvo)
								.parameterObjectId(sources.y)
								.customUsage(customUsage)
								.build();
						result.set(
								GeneratorDelegate.getInstance().generateGenericObject(context)
						);
					}
					else {
						if (sources.x.size() > 1) {
							throw new NuclosFatalException();
						}
						GenerationContext<Long> context = new GenerationContextBuilder<>(Collections.singleton(sources.x.iterator().next()), generatoractionvo)
								.parameterObjectId(sources.y)
								.customUsage(customUsage)
								.build();
						result.set(
								GeneratorDelegate.getInstance().generateGenericObject(context)
						);
					}
				}
				catch (GeneratorFailedException e) {
					result.set(e.getGenerationResult());
				}
			}
		}, context, applyMultiEditContext, parent);
		return result.get();
	}

	/**
	 * @return the text to display for the action on the given object.
	 */
	@Override
	public String getText(Pair<Collection<EntityObjectVO<Long>>, Long> sources) {
		if (sources.x.size() == 1) {
			return SpringLocaleDelegate.getInstance().getTreeViewLabel(
					sources.x.iterator().next(), MetaProvider.getInstance(), DataLanguageContext.getLanguageToUse());
		}
		else {
			return SpringLocaleDelegate.getInstance().getMessage(
					"generation.multiple", "Objektgenerierung f\u00fcr {0} Objekte ...", sources.x.size());
		}
	}

	/**
	 * @return the text to display after successful execution of the action for
	 *         the given object.
	 */
	@Override
	public String getSuccessfulMessage(Pair<Collection<EntityObjectVO<Long>>, Long> sources, GenerationResult rResult) {
		if (!StringUtils.isNullOrEmpty(rResult.getError())) {
			return SpringLocaleDelegate.getInstance().getMessage(
					"generation.unsaved",
					"Generated obect could not be saved: \\n{0} \\nPlease edit object in details view (see context menu).",
					SpringLocaleDelegate.getInstance().getMessageFromResource(rResult.getError()));
			
		}
		else {
			return SpringLocaleDelegate.getInstance().getMessage("R00022880",
					"Object \"{0}\" successfully generated.",
					SpringLocaleDelegate.getInstance().getTreeViewLabel(
							(EntityObjectVO)rResult.getGeneratedObject(), MetaProvider.getInstance(),
							DataLanguageContext.getLanguageToUse()));
		}
	}

	@Override
	public String getConfirmStopMessage() {
		return SpringLocaleDelegate.getInstance().getMessage(
				"R00022886", "Wollen Sie die Objektgenerierung an dieser Stelle beenden?\n(Die bisher generierten Objekte bleiben in jedem Fall erhalten.)");
	}

	@Override
	public String getExceptionMessage(Pair<Collection<EntityObjectVO<Long>>, Long> sources, Exception ex) {
		return SpringLocaleDelegate.getInstance().getMessage(
				"R00022883", "Objektgenerierung fehlgeschlagen. \\n{1}", ex.getMessage());
	}

	@Override
	public void executeFinalAction() throws CommonBusinessException {
		// do nothing
	}
}
