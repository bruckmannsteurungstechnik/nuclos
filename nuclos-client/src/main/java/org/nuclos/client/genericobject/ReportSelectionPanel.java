//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;

import javax.swing.JCheckBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.RowSorter;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXPanel;
import org.nuclos.client.ui.table.TableUtils;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.preferences.PreferencesUtils;
import org.nuclos.common.report.valueobject.DefaultReportOutputVO;
import org.nuclos.common.report.valueobject.DefaultReportVO;
import org.nuclos.common2.ClientPreferences;
import org.nuclos.common2.SpringLocaleDelegate;

/**
 * Panel for selection of possible output formats for a given report
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Boris.Sander@novabit.de">Boris Sander</a>
 * @version 01.00.00
 */
public class ReportSelectionPanel extends JXPanel {

	private static final Logger LOG = Logger.getLogger(ReportSelectionPanel.class);
	
	protected class ReportSelectionTableModel extends AbstractTableModel {

		protected final String[] captions = {
			SpringLocaleDelegate.getInstance().getMessage("ReportSelectionPanel.1", "Formular"),
			SpringLocaleDelegate.getInstance().getMessage("R00011618", "Vorlage"),
			SpringLocaleDelegate.getInstance().getMessage("ReportSelectionPanel.2", "Format"), 
			SpringLocaleDelegate.getInstance().getMessage("ReportSelectionPanel.3", "Ausgabemedium")};
		
		protected ArrayList<ReportEntry> lstReports = new ArrayList<ReportEntry>();

		@Override
		public int getColumnCount() {
			return captions.length;
		}

		@Override
		public int getRowCount() {
			return lstReports.size();
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			ReportEntry entry = lstReports.get(rowIndex);
			if (entry != null) {
				switch (columnIndex) {
					case 0:
						return entry.getReport().getName();
					case 1:
						return entry.getOutput() != null ? entry.getOutput().getDescription() : "";
					case 2:
						return entry.getOutput() != null ? entry.getOutput().getFormat() : "";
					case 3:
						String sResult = "";
						if (entry.getOutput() != null) {
							sResult += SpringLocaleDelegate.getInstance().getText(entry.getOutput().getDestination());
							if (entry.getOutput().getParameter() != null &&
									entry.getOutput().getParameter().length() > 0) {
								sResult += (" (" + entry.getOutput().getParameter() + ")");
							}
						}
						return sResult;
				}
			}
			return null;
		}

		@Override
		public String getColumnName(int column) {
			return captions[column];
		}

		public void add(ReportEntry entry) {
			lstReports.add(entry);
			fireTableDataChanged();
		}

		public ReportEntry getEntry(int iRow) {
			ReportEntry result = null;

			if (iRow < lstReports.size()) {
				result = lstReports.get(iRow);
			}
			return result;
		}
	}

	public class ReportEntry {
		protected DefaultReportVO report;
		protected DefaultReportOutputVO output;

		public ReportEntry(DefaultReportVO report, DefaultReportOutputVO output) {
			this.report = report;
			this.output = output;
		}

		public DefaultReportOutputVO getOutput() {
			return output;
		}

		public void setOutput(DefaultReportOutputVO output) {
			this.output = output;
		}

		public DefaultReportVO getReport() {
			return report;
		}

		public void setReport(DefaultReportVO report) {
			this.report = report;
		}
	}

	private final ReportSelectionTableModel model = new ReportSelectionTableModel();
	private final JTable tblReports = new JTable(model) {
		public boolean isCellEditable(int rowIndex, int columnIndex) {
		    return false;
		}
	};
	private final JScrollPane scrReports = new JScrollPane(tblReports);
	
	private final UID entityUid;

	private final JCheckBox cbAttachReport = new JCheckBox(
			SpringLocaleDelegate.getInstance().getMessage("ReportSelectionPanel.4", "Dokument anh\u00e4ngen"));
	
	public ReportSelectionPanel(UID entityUid) {
		this(entityUid, false);
	}

	public ReportSelectionPanel(UID entityUid, boolean bShowAttachReport) {
		super(new BorderLayout());
		
		this.entityUid = entityUid;

		init(bShowAttachReport);
	}

	/* (non-Javadoc)
	 * @see org.nuclos.client.genericobject.OutputFormatSelectionComponent#getPreferredSize()
	 */
	@Override
	public Dimension getPreferredSize() {
		Dimension result = super.getPreferredSize();
		Dimension dimTable = tblReports.getPreferredSize();
		result.width = Math.max(dimTable.width, result.width);
		result.width = Math.max(800, result.width);

		return result;
	}

	private void init(boolean bShowAttachReport) {
		scrReports.setPreferredSize(new Dimension(500, 150));
		this.setScrollableTracksViewportWidth(true);
		this.setScrollableTracksViewportHeight(true);
		add(scrReports, BorderLayout.CENTER);

		DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
		renderer.setHorizontalAlignment(SwingConstants.LEFT);
		tblReports.getColumnModel().getColumn(1).setCellRenderer(renderer);
		tblReports.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		cbAttachReport.setSelected(false);
		if (bShowAttachReport) {
			add(cbAttachReport, BorderLayout.SOUTH);
			cbAttachReport.setSelected(true); // as default
		}
		tblReports.setAutoCreateRowSorter(true);
		
		tblReports.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				final ReportEntry entry = getSelectedPrintProfiles();
				if (entry != null) {
					if (entry.getOutput() == null) 
						cbAttachReport.setSelected(entry.getReport().getAttachDocument());
					else
						cbAttachReport.setSelected(entry.getOutput().getAttachDocument());
				}
			}
		});
		
		tblReports.setFocusable(true);
		tblReports.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						tblReports.requestFocusInWindow();
					}
				});
				tblReports.removeComponentListener(this);
			}
		});
		tblReports.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, null);
		tblReports.setFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS, null);
	}
	
	
	public JTable getReportsTable() {
		return tblReports;
	}
	
	
	public void addDoubleClickListener(MouseListener l)	{
		tblReports.addMouseListener(l);
	}
	
	
	public void addReport(DefaultReportVO reportVO, DefaultReportOutputVO formatVO) {
		ReportEntry entry = new ReportEntry(reportVO, formatVO);
		if (entry != null) {
			model.add(entry);
		}
	}

	
	public ReportEntry getSelectedPrintProfiles() {
		ReportEntry result = null;
		if (tblReports.getSelectedRow() >= 0) {
			result = model.getEntry(tblReports.convertRowIndexToModel(tblReports.getSelectedRow()));
		}
		return result;
	}
	
	
	public void selectFirstReport() {
		tblReports.setRowSelectionInterval(0, 0);
	}
	
	
	public boolean getAttachReport() {
		return cbAttachReport.isSelected();
	}
	
	private class PreferencesUpdateListener extends MouseAdapter implements TableColumnModelListener  {
		@Override
		public void columnSelectionChanged(ListSelectionEvent ev) {
		}
		@Override
		public void columnMoved(TableColumnModelEvent ev) {
			if (ev.getFromIndex() != ev.getToIndex()) {
				storeColumnOrderAndWidths(tblReports);
			}
		}
		@Override
		public void columnMarginChanged(ChangeEvent ev) {
			storeColumnOrderAndWidths(tblReports);
		}
		@Override
		public void columnAdded(TableColumnModelEvent ev) {
		}
		@Override
		public void columnRemoved(TableColumnModelEvent ev) {
		}		
		@Override
		public void mouseClicked(MouseEvent e) {
			storeColumnOrderAndWidths(tblReports);
		}
	}

	private static String KEY_COLUMNIDENTIFIERS = "columnidendifiers";
	private static String KEY_COLUMNPREFERREDWIDTH = "columnpreferredwidth";
	private final PreferencesUpdateListener prefsUpdateListener = new PreferencesUpdateListener();
	
	protected void storeColumnOrderAndWidths(JTable tbl) {
		try {
			final Preferences prefs
				= ClientPreferences.getInstance().getUserPreferences()
					.node("collect").node("entity").node(entityUid.getStringifiedDefinitionWithEntity(E.ENTITY.getUID()));
			
			PreferencesUtils.writeSortKeysToPrefs(prefs, tbl.getRowSorter().getSortKeys());
			
			final List<Object> lstColumnIdentifiers = new ArrayList<Object>(); 
			final Map<Object, Integer> mpColumnPreferredWidth = new HashMap<Object, Integer>();
			for (int i = 0; i < tbl.getColumnCount(); i++) {
				final TableColumn column = tbl.getColumnModel().getColumn(i);
				lstColumnIdentifiers.add(column.getIdentifier());
				mpColumnPreferredWidth.put(column.getIdentifier(), column.getWidth());
			}
			PreferencesUtils.putSerializable(prefs, KEY_COLUMNIDENTIFIERS, lstColumnIdentifiers);
			PreferencesUtils.putSerializable(prefs, KEY_COLUMNPREFERREDWIDTH, mpColumnPreferredWidth);
		} catch (Exception e) {
			LOG.warn("error writing report selection settings from preferences. " + e.getMessage());
		}
	}
	protected void restoreColumnOrderAndWidths() {
		try {
			final Preferences prefs
				= ClientPreferences.getInstance().getUserPreferences()
					.node("collect").node("entity").node(entityUid.getStringifiedDefinitionWithEntity(E.ENTITY.getUID()));
				
			final List<SortKey> sortKeys = PreferencesUtils.readSortKeysFromPrefs(prefs);
			if (!sortKeys.isEmpty()) {
				tblReports.getRowSorter().setSortKeys(sortKeys);
			} else {
				tblReports.getRowSorter().setSortKeys(
						Collections.singletonList(new RowSorter.SortKey(1,SortOrder.ASCENDING)));
			}
			
			final List<Object> lstColumnIdentifier
				= (List<Object>)PreferencesUtils.getSerializable(prefs, KEY_COLUMNIDENTIFIERS);
			final Map<Object, Integer> mpColumnPreferredWidth
				= (Map<Object, Integer>)PreferencesUtils.getSerializable(prefs, KEY_COLUMNPREFERREDWIDTH);
			
			if (lstColumnIdentifier == null || mpColumnPreferredWidth == null) {
				TableUtils.setPreferredColumnWidth(tblReports, tblReports.getRowCount(), 10);
			} else {
				final Map<Object, TableColumn> mpColumns = new HashMap<Object, TableColumn>();
				for (int i = 0; i < tblReports.getColumnCount(); i++) {
					final TableColumn column = tblReports.getColumnModel().getColumn(i); 
					mpColumns.put(column.getIdentifier(), column);
				}				
				final TableColumnModel columnModel = new DefaultTableColumnModel();
				for (Object identifier : lstColumnIdentifier) {
					final TableColumn column = mpColumns.get(identifier);
					column.setPreferredWidth(mpColumnPreferredWidth.get(identifier));
					columnModel.addColumn(column);
				}
				tblReports.setColumnModel(columnModel);
			}
		} catch (Exception e) {
			LOG.warn("error writing report selection settings from preferences. " + e.getMessage() + " - setting defaults.");
			tblReports.getRowSorter().setSortKeys(
					Collections.singletonList(new RowSorter.SortKey(1,SortOrder.ASCENDING)));
			TableUtils.setPreferredColumnWidth(tblReports, tblReports.getRowCount(), 10);
		} finally {
			tblReports.getTableHeader().removeMouseListener(prefsUpdateListener);
			tblReports.getTableHeader().addMouseListener(prefsUpdateListener);
			tblReports.getColumnModel().removeColumnModelListener(prefsUpdateListener);
			tblReports.getColumnModel().addColumnModelListener(prefsUpdateListener);
		}
	}
}
