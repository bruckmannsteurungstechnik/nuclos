package org.nuclos.client.genericobject.controller;

import java.util.Collection;
import java.util.Collections;

import org.nuclos.api.rule.CustomRule;
import org.nuclos.client.rule.server.EventSupportDelegate;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.caching.NBCache;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.slf4j.LoggerFactory;

/**
 * Created by Oliver Brausch on 18.07.17.
 */
public class GetUserRulesProvider implements NBCache.LookupProvider<UsageCriteria, Collection<EventSupportSourceVO>> {

	@Override
	public Collection<EventSupportSourceVO> lookup(UsageCriteria uc) {
		try {
			final Collection<EventSupportSourceVO> collRules =
					EventSupportDelegate.getInstance().findEventSupportsByUsageAndEvent(CustomRule.class.getCanonicalName(), uc);
			// remove inactive rules
			CollectionUtils.removeAll(collRules, new Predicate<EventSupportSourceVO>() {
				@Override
				public boolean evaluate(EventSupportSourceVO rulevo) {
					return !rulevo.isActive();
				}
			});

			// add EventSupports if existing
			collRules.addAll(EventSupportDelegate.getInstance()
					.findEventSupportsByUsageAndEvent(CustomRule.class.getCanonicalName(), uc));

			return collRules;
		} catch (Exception e) {
			LoggerFactory.getLogger(GetUserRulesProvider.class).error(e.getMessage(), e);
			return Collections.EMPTY_LIST;
		}
	}
}
