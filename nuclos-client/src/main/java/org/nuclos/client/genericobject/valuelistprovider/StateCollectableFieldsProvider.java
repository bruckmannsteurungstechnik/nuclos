//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject.valuelistprovider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.statemodel.StateDelegate;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.statemodel.valueobject.StateVO;

/**
 * Value list provider to get all state numerals for a certain module.
 * This value list provider is used only in search masks.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:ramin.goettlich@novabit.de">Ramin Goettlich</a>
 * @version 00.01.000
 */
public class StateCollectableFieldsProvider implements CollectableFieldsProvider {
	
	private static final Logger LOG = Logger.getLogger(StateCollectableFieldsProvider.class);
	
	public static final String MODULE_UID = "module";
	public static final String USAGE_CRITERIA = "usagecriteria";

	private UsageCriteria usagecriteria;
	
	/**
	 * @deprecated
	 */
	public StateCollectableFieldsProvider() {
	}
	
	public StateCollectableFieldsProvider(UID entityWithStatemodelUid, UsageCriteria usageCriteria) {
		setParameter(MODULE_UID, entityWithStatemodelUid);
		setParameter(USAGE_CRITERIA, usageCriteria);
	}

	/**
	 * valid parameters:
	 * <ul>
	 *   <li>"service" = service id</li>
	 *   <li>"_searchmode" = collectable in search mask?
	 *       (now org.nuclos.common.NuclosConstants.VLP_SEARCHMODE_PARAMETER)
	 * </ul>
	 * @param sName parameter name
	 * @param oValue parameter value
	 * 
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String sName, Object oValue) {
		// LOG.debug("setParameter - sName = " + sName + " - oValue = " + oValue);
		
		if (sName.equals(MODULE_UID)) {
			try {
				UID iModuleId = null;
				if (oValue instanceof String && UID.isStringifiedUID((String) oValue)) {
					iModuleId = UID.parseUID((String) oValue);
				} else if (oValue instanceof UID) {
					iModuleId = oValue == null ? null : (UID) oValue;
				}
				if (iModuleId == null) {
					throw new IllegalArgumentException("oValue");
				}
				this.usagecriteria = new UsageCriteria(iModuleId, null, null, null);
			} catch (Exception ex) {
				throw new NuclosFatalException(SpringLocaleDelegate.getInstance().getMessage(
						"StateCollectableFieldsProvider.1", "Der Parameter \"module\" muss den Namen einer Modul-Entit\u00e4t enthalten.\n\"{0}\" ist keine g\u00fcltige Modul-Entit\u00e4t.", oValue), ex);
			}
		} else if (sName.equals(USAGE_CRITERIA)) {
			this.usagecriteria = oValue == null ? null : (UsageCriteria) oValue;
		} else {
			// ignore
			LOG.info("Unknown parameter " + sName + " with value " + oValue);
		}
	}

	@Override
	public List<CollectableField> getCollectableFields() {
		//LOG.debug("getCollectableFields - iModuleId/iProcessId = " + this.usagecriteria.getEntityUID() + "/" + this.usagecriteria.getProcessUID());
		
		Collection<StateVO> colSearchStates;
		if (usagecriteria.getProcessUID() == null) {
			colSearchStates = StateDelegate.getInstance().getStatesByModule(usagecriteria.getEntityUID());
		} else {
			colSearchStates = StateDelegate.getInstance().getStatesByModel(StateDelegate.getInstance().getStateModelId(usagecriteria));
		}	
		
		final List<CollectableField> result = new ArrayList<CollectableField>();
		final Map<Integer, StateVO> statesByNumeral = new HashMap<Integer, StateVO>();
		for (StateVO statevo : colSearchStates) {
			if (statesByNumeral.containsKey(statevo.getNumeral())) {
				StateVO statevoInResult = statesByNumeral.get(statevo.getNumeral());
				String name1 = statevo.getStatename(LocaleDelegate.getInstance().getLocale());
				String name2 = statevoInResult.getStatename(LocaleDelegate.getInstance().getLocale());
				if (!StringUtils.equals(name1, name2)) {
					statevoInResult.setStatename(LocaleDelegate.getInstance().getLocale(), "...");
				}
			} else {
				StateVO statevoCopy = new StateVO(statevo.getPrimaryKey(), statevo.getNumeral(),
						statevo.getStatename(Locale.GERMAN), statevo.getStatename(Locale.ENGLISH), 
						statevo.getDescription(Locale.GERMAN), statevo.getDescription(Locale.ENGLISH), null, null);

				statesByNumeral.put(statevoCopy.getNumeral(), statevoCopy);
				result.add(new CollectableValueField(statevoCopy));
			}
		}

		Collections.sort(result);
		return result;
	}

}	// class StateCollectableFieldsProvider
