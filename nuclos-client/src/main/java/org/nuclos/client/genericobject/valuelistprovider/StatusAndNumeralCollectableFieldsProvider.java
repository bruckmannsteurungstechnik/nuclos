//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject.valuelistprovider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.statemodel.StateDelegate;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.statemodel.valueobject.StateVO;

/**
 * Value list provider to get all numeral states for the given module entity (and process).
 *
 * TODO: Seems to be unused.
 * TODO: If not unused, it should extend {@link MultiStatusCollectableFieldsProvider}.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
public class StatusAndNumeralCollectableFieldsProvider implements CollectableFieldsProvider {
	
	private static final Logger LOG = Logger.getLogger(StatusAndNumeralCollectableFieldsProvider.class);
	
	public static final String MODULE_NAME = "module";
	public static final String MODULE_UID = "moduleId";
	public static final String PROCESS_UID = "process";
	public static final String PROVIDE_ID_FIELDS = "provideIdFields";
	/**
	 * @deprecated
	 */
	public static final String ENTITY_NAME = "entityName";
		
	//

	private UID iModuleId;
	private UID iProcessId;
	private boolean bProvideIdFields = false;
	
	public StatusAndNumeralCollectableFieldsProvider() {
	}
	
	public StatusAndNumeralCollectableFieldsProvider(UID entityWithStatemodelUid, UID processUid, boolean provideIdFields) {
		setParameter(MODULE_UID, entityWithStatemodelUid);
		setParameter(PROCESS_UID, processUid);
		setParameter(PROVIDE_ID_FIELDS, provideIdFields);
	}

	/**
	 * valid parameters:
	 * <ul>
	 *   <li>"module" = name of module entity</li>
	 *   <li>"process" = process id</li>
	 * </ul>
	 * @param sName parameter name
	 * @param oValue parameter value
	 * 
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String sName, Object oValue) {
		// LOG.debug("setParameter - sName = " + sName + " - oValue = " + oValue);
		
		if (sName.equals(MODULE_UID)) {
			iModuleId =  oValue == null ? null : (UID) oValue;
		} else if (sName.equals(MODULE_NAME)) {
			try {
				if (oValue instanceof String && UID.isStringifiedUID((String) oValue)) {
					iModuleId = UID.parseUID((String) oValue);
				} else if (oValue instanceof UID) {
					iModuleId = oValue == null ? null : (UID) oValue;
				}
				if (iModuleId == null) {
					throw new IllegalArgumentException("oValue");
				}
			} catch (Exception ex) {
				throw new NuclosFatalException(SpringLocaleDelegate.getInstance().getMessage(
						"StatusAndNumeralCollectableFieldsProvider.1", "Der Parameter \"module\" muss den Namen einer Modul-Entit\u00e4t enthalten.\n\"{0}\" ist keine g\u00fcltige Modul-Entit\u00e4t.", oValue), ex);
			}
		} else if (sName.equals(ENTITY_NAME)) {
			try {
				if (oValue instanceof String && UID.isStringifiedUID((String) oValue)) {
					iModuleId = UID.parseUID((String) oValue);
				} else if (oValue instanceof UID) {
					iModuleId = oValue == null ? null : (UID) oValue;
				}
				if (iModuleId == null) {
					throw new IllegalArgumentException("oValue");
				}
			} catch (Exception ex) {
				throw new NuclosFatalException(SpringLocaleDelegate.getInstance().getMessage(
						"StatusAndNumeralCollectableFieldsProvider.2", "Der Parameter \"entityName\" muss den Namen einer Entit\u00e4t enthalten.\n\"{0}\" ist keine g\u00fcltige Modul-Entit\u00e4t.", oValue), ex);
			}
		} else if (sName.equals(PROCESS_UID)) {
			if (oValue instanceof String && UID.isStringifiedUID((String) oValue)) {
				iProcessId = UID.parseUID((String) oValue);
			} else if (oValue instanceof UID) {
				iProcessId = oValue == null ? null : (UID) oValue;
			} else {
				iProcessId = null;
			}
		} else if (sName.equals(PROVIDE_ID_FIELDS)) {
			if (oValue instanceof String) {
				bProvideIdFields = Boolean.valueOf((String) oValue);
			} else if (oValue instanceof Boolean) {
				bProvideIdFields = oValue == null ? null : (Boolean) oValue;
			} else {
				iProcessId = null;
			}
		} else {
			// ignore
			LOG.info("Unknown parameter " + sName + " with value " + oValue);
		}
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonValidationException {
		//LOG.debug("getCollectableFields - iModuleId = " + iModuleId);
		//LOG.debug("getCollectableFields - iProcessId = " + iProcessId);
		
		if (iModuleId == null && iProcessId == null)
			return Collections.emptyList();
		
		final Map<UID, StateVO> mpState = new HashMap<UID, StateVO>();
		final List<CollectableField> result = new ArrayList<CollectableField>();
		
		try {
			final UID iStateModelId = StateDelegate.getInstance().getStateModelId(new UsageCriteria(iModuleId, iProcessId, null, null));
			for (StateVO statevo : StateDelegate.getInstance().getStatesByModel(iStateModelId)) {
				mpState.put(statevo.getId(), statevo);
			}
		} catch (RuntimeException e) {
			if (iProcessId != null) {
				// in some cases we want the states depending of processId==null.
				// for other we do not habe an depending statemodel. so we return all states per model.
				throw new CommonValidationException(e);
			}
			for (StateVO statevo : StateDelegate.getInstance().getStatesByModule(iModuleId)) {
				if (!mpState.containsKey(statevo.getId())) {
					// add states only once
					mpState.put(statevo.getId(), statevo);
				}
			}
		}
		
		for (StateVO statevo : mpState.values()) {
			if (bProvideIdFields)
				result.add(new CollectableValueIdField(statevo.getId(), statevo.getNumeral().toString() + " " + statevo.getStatename(LocaleDelegate.getInstance().getLocale())));
			else
				result.add(new CollectableValueField(statevo.getNumeral().toString() + " " + statevo.getStatename(LocaleDelegate.getInstance().getLocale())));
		}

		Collections.sort(result);
		return result;
	}

}	// class StatusAndNumeralCollectableFieldsProvider
