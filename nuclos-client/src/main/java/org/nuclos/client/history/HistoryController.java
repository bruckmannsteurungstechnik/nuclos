//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.history;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.prefs.Preferences;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;
import org.nuclos.client.command.ResultListener;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.EntityCollectController;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.NuclosCollectController;
import org.nuclos.client.common.NuclosCollectControllerFactory;
import org.nuclos.client.dal.DalSupportForGO;
import org.nuclos.client.entityobject.CollectableEOEntityClientProvider;
import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.genericobject.CollectableGenericObjectWithDependants;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.CollectableWithDependants;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.masterdata.MasterDataSubFormController;
import org.nuclos.client.ui.MainFrameTabAdapter;
import org.nuclos.client.ui.MainFrameTabController;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModel;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelProvider;
import org.nuclos.client.ui.collect.component.model.DefaultCollectableComponentModelProvider;
import org.nuclos.client.ui.collect.model.CollectableEntityFieldBasedTableModel;
import org.nuclos.client.ui.collect.model.CollectableTableModel;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.client.ui.collect.subform.SubFormFilter;
import org.nuclos.client.ui.collect.subform.SubFormTable;
import org.nuclos.client.ui.collect.subform.ToolbarFunction;
import org.nuclos.client.ui.event.IPopupListener;
import org.nuclos.client.ui.event.PopupMenuMouseAdapter;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.MasterDataToEntityObjectTransformer;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.dal.DalSupportForMD;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.entityobject.CollectableEOEntity;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.history.HistoryVO;

/**
 * Controller for viewing the logbook.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */

public class HistoryController<PK> extends MainFrameTabController {

	private static final Logger LOG = Logger.getLogger(HistoryController.class);

	private final String PREFS_KEY_HISTORY = "history";

	private MainFrameTab newTab;
	
	private final UID entityUID;
	private final PK lObjectId;
	private final String sTitle;
	
	private final Preferences prefs;

	private final CollectableEntity clcte;
	
	private final HistoryPanel pnlHistory;
	
	public HistoryController(MainFrameTab source, UID sEntity, PK iObjectId, Preferences prefs, String sIdentifier) {
		super(source);
		
		this.entityUID = sEntity;
		this.lObjectId = iObjectId;
		
		this.pnlHistory = new HistoryPanel(sEntity);
		
		this.clcte = CollectableEOEntityClientProvider.getInstance().getCollectableEntity(E.HISTORY.getUID());

		this.sTitle = getTitle(sIdentifier);
		this.newTab = Main.getInstance().getMainController().newMainFrameTab(null, sTitle);
		
		this.prefs = prefs.node(PREFS_KEY_HISTORY);
	}

	public void run() throws CommonFinderException, CommonPermissionException, NuclosBusinessException {
		final SubForm subform = pnlHistory.getSubform();
		final MasterDataSubFormController subFormController = new MasterDataSubFormController(clcte, newTab, getCollectableComponentModelProvider(), E.HISTORY.getUID(), subform, prefs, 
				Main.getInstance().getMainFrame().getWorkspaceDescription().getEntityPreferences(E.HISTORY.getUID()), 
				null, null, null) {
			@Override
			protected void postCreate() {
				super.postCreate();
				
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						final SubFormFilter sfFilter = subform.getSubFormFilter();
						if (!sfFilter.isFilteringActive())
							subform.getToolbarButton(ToolbarFunction.FILTER.name()).doClick();

						if (subform.getJTable().getRowCount() > 0) {
							subform.getJTable().clearSelection();
							subform.getJTable().setRowSelectionInterval(0,0);
						}
						
						sfFilter.setFilterByString(true);
						sfFilter.arrangeFilterComponents();
						sfFilter.initHistoryDate();
						sfFilter.createFilter(false);
					}
				});
			}
			
			@Override
			protected void setupSubFormTableContextMenue() {
				IPopupListener popupMenuMouseAdapter = new PopupMenuMouseAdapter() {
					
					@Override
					public boolean doPopup(MouseEvent e, JPopupMenu menu) {
						final JPopupMenu result = new JPopupMenu();

						JMenuItem miDetails = new JMenuItem(getSpringLocaleDelegate().getMessage(
								"AbstractCollectableComponent.7","Details anzeigen..."));

						miDetails.addActionListener(new ActionListener() {
							@Override
		                    public void actionPerformed(ActionEvent ev) {
								cmdShowCollectable((String)getSelectedCollectable().getValue(E.HISTORY.savepoint.getUID()), (Date)getSelectedCollectable().getValue(SF.CREATEDAT.getUID(E.HISTORY.getUID())));
							}
						});
						result.add(miDetails);

						result.show(getSubForm().getJTable(), e.getX(), e.getY());
						
						return true;
					}
				};

				this.getSubForm().setPopupMenuListener(popupMenuMouseAdapter);
			}
		};

		final Collection<EntityObjectVO<Long>> col = CollectionUtils.transform(
				HistoryDelegate.getInstance().getHistoryData(entityUID, lObjectId), new MasterDataToEntityObjectTransformer<Long>());

		subFormController.fillSubForm(null, col);
		
		final Set<TableColumn> setColumnsToRemove = new HashSet<TableColumn>();
		JTable subFormTable = subFormController.getSubForm().getJTable();
		TableColumnModel tableColumnModel = subFormTable.getColumnModel();
		for (Enumeration<TableColumn> enumcolumn = tableColumnModel.getColumns(); enumcolumn.hasMoreElements();) {
			final TableColumn column = enumcolumn.nextElement();
			final CollectableEntityField field = ((CollectableEntityFieldBasedTableModel) subFormTable.getModel()).getCollectableEntityField(column.getModelIndex());
			// remove some columns from view
			if (field.getUID().equals(E.HISTORY.value_refid.getUID()) ||
				field.getUID().equals(E.HISTORY.value_refuid.getUID()) ||
				field.getUID().equals(E.HISTORY.entityobjectid.getUID()) ||
				field.getUID().equals(E.HISTORY.entityobjectuid.getUID()) ||
				field.getUID().equals(E.HISTORY.value_byte.getUID())) {
				setColumnsToRemove.add(column);
			}
		}
		for (TableColumn column : setColumnsToRemove) {
			tableColumnModel.removeColumn(column);
		}
		subFormTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		newTab.setLayeredComponent(pnlHistory);

		getTab().add(newTab);
		
		this.setupTab();

		setupEscapeKey(newTab, pnlHistory);

		setupDoubleClickListener();

		newTab.setVisible(true);
	}

	public MainFrameTab getMainFrameTab() {
		return newTab;
	}
	
	private CollectableComponentModelProvider getCollectableComponentModelProvider() {
		HashMap<UID, CollectableComponentModel> map = new HashMap<UID, CollectableComponentModel>();
		for (UID s : clcte.getFieldUIDs()) {
			map.put(s, CollectableComponentModel.newCollectableComponentModel(clcte.getEntityField(s), false));
		}
		return new DefaultCollectableComponentModelProvider(map);
	}

	private String getTitle(String sGenericObjectIdentifier) {
		final StringBuffer sbTitle = new StringBuffer(getSpringLocaleDelegate().getMessage("LogbookController.12", "Logbuch f\u00fcr")+" ");
		if (sGenericObjectIdentifier == null) {
			sbTitle.append(getSpringLocaleDelegate().getMessage("LogbookController.13", "das Objekt mit der Id") + " ");
			sbTitle.append((lObjectId instanceof UID ? ((UID)lObjectId).getStringifiedDefinition() : String.valueOf(lObjectId)));
		} else {
			sbTitle.append(getSpringLocaleDelegate().getResource(MetaProvider.getInstance().getEntity(entityUID).getLocaleResourceIdForLabel(), null)).append(" ");
			sbTitle.append("\"").append(sGenericObjectIdentifier).append("\"");
		}

		return sbTitle.toString();
	}
	
	private void setupEscapeKey(final MainFrameTab ifrm, HistoryPanel pnlHistory) {
		// Escape key is to close the window:
		final Action actClose = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent ev) {
				ifrm.close();
			}
		};

		final String KEY_CLOSE = "Close";
		ifrm.getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), KEY_CLOSE);
		ifrm.getRootPane().getActionMap().put(KEY_CLOSE, actClose);

		if (pnlHistory != null) {
			pnlHistory.getSubform().getJTable().getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), KEY_CLOSE);
			pnlHistory.getSubform().getJTable().getActionMap().put(KEY_CLOSE, actClose);
		}
	}

	private void setupTab() {
		newTab.addMainFrameTabListener(new MainFrameTabAdapter() {
			@Override
			public void tabClosing(MainFrameTab tab, ResultListener<Boolean> rl) {
				rl.done(true);
			}
			@Override
			public void tabClosed(MainFrameTab tab) {
				tab.removeMainFrameTabListener(this);
			}
		});
	}

	private void setupDoubleClickListener() {
		pnlHistory.getSubform().getJTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent ev) {
				if (ev.getClickCount() == 2) {
					final SubFormTable tbl = pnlHistory.getSubform().getSubformTable();
					final int iRow = tbl.getSelectedRow();
					if (iRow >= 0) {
						final Collectable clct = ((CollectableTableModel)tbl.getSubFormModel()).getCollectable(iRow);
						cmdShowCollectable((String)clct.getValue(E.HISTORY.savepoint.getUID()), ((Date)clct.getValue(E.HISTORY.validuntil.getUID())));
					}
				}
			}
		});
	}

	private void cmdShowCollectable(final String sSavepoint, final Date dateHistorical) {
		UIUtils.runCommandForTabbedPane(MainFrame.getTabbedPane(getTab()), new CommonRunnable() {

			@Override
			public void run() throws CommonBusinessException {
				final MainFrameTab overlay = new MainFrameTab();

				final NuclosCollectController ctl = NuclosCollectControllerFactory.getInstance().newCollectController(entityUID, overlay, 
						ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
				Main.getInstance().getMainController().initMainFrameTab(ctl, overlay);
				getMainFrameTab().add(overlay);
				
				setupEscapeKey(overlay, pnlHistory);
				
				overlay.addMainFrameTabListener(new MainFrameTabAdapter() {
					@Override
					public void tabClosing(MainFrameTab tab, ResultListener<Boolean> rl) {
						super.tabClosing(tab, rl);
						overlay.removeMainFrameTabListener(this);
						if (!((EntityCollectController<?,Collectable<?>>)ctl).isHistoricalView()) {
							getMainFrameTab().close();
						} else {
							setupEscapeKey(getMainFrameTab(), pnlHistory);
						}
					}
				});
				CollectableWithDependants clct
					= (CollectableWithDependants)ctl.findCollectableById(entityUID, lObjectId, -1);
				
				// try to load subsubform.
				IDependentDataMap dependants = null;
				if (clct instanceof CollectableMasterDataWithDependants) {
					dependants = ((CollectableMasterDataWithDependants)clct).getMasterDataWithDependantsCVO().getDependents();
				} else if (clct instanceof CollectableGenericObjectWithDependants) {
					dependants = ((CollectableGenericObjectWithDependants)clct).getGenericObjectWithDependantsCVO().getDependents();
				}
				if (dependants != null) {
					List<EntityObjectVO<?>> allData = CollectionUtils.concatAll(dependants.getRoDataMap().values());
					for (EntityObjectVO<?> eoDependant : allData) {
						eoDependant.setDependents(MasterDataDelegate.getInstance().getWithDependants(
								eoDependant.getDalEntity(), eoDependant.getPrimaryKey(), -1).getDependents());
					}
				}
				
				replaceWithHistoricalValues(sSavepoint, lObjectId, dateHistorical, clct);

				((EntityCollectController<?,Collectable<?>>)ctl).runViewSingleHistoricalCollectable(clct, dateHistorical, true);
			}
		});
	}

	private void replaceWithHistoricalValues(final String sSavepoint, final Object iParentId, final Date dateHistorical, final CollectableWithDependants clct) throws CommonValidationException {
		final UID entityobjectFieldUid = (iParentId instanceof UID ?
					E.HISTORY.entityobjectuid.getUID() : E.HISTORY.entityobjectid.getUID());
		
		final List<CollectableEntityObject> clctHistoryEntries = getHistoryEntries(sSavepoint, iParentId, dateHistorical);
		
		// get all depending current entity
		for (CollectableEntityObject clctEntry : clctHistoryEntries) {	
			try {
				setCollectableFields(entityUID, clct.getId(), clctEntry.getEntityObjectVO(), clct);
			} catch (Exception e) {}
		}

		// remove dependant subform data with creation date older than given historical date
		final UID entityName;
		IDependentDataMap dependants = null;
		if (clct instanceof CollectableMasterDataWithDependants) {
			entityName = ((CollectableMasterDataWithDependants)clct).getCollectableEntity().getUID();
			dependants = ((CollectableMasterDataWithDependants)clct).getMasterDataWithDependantsCVO().getDependents();
		} else if (clct instanceof CollectableGenericObjectWithDependants) {
			entityName = ((CollectableGenericObjectWithDependants)clct).getCollectableEntity().getUID();
			dependants = ((CollectableGenericObjectWithDependants)clct).getGenericObjectWithDependantsCVO().getDependents();
		} else
			 entityName = entityUID;
	
		if (dependants != null) {
			List<EntityObjectVO<?>> allData = CollectionUtils.concatAll(dependants.getRoDataMap().values());
			for (EntityObjectVO<?> eo : CollectionUtils.select(allData, new Predicate<EntityObjectVO<?>>() {
				@Override
				public boolean evaluate(EntityObjectVO<?> t) {
					if (!MetaProvider.getInstance().getEntity(t.getDalEntity()).isLogBookTracking())
						return false;
					return t.getCreatedAt().equals(dateHistorical) || t.getCreatedAt().after(dateHistorical);
				}
			})) {
				eo.flagRemove();
			}
		}
		// find Date of first subform entry
		final EntityMeta<?> entityMeta = MetaProvider.getInstance().getEntity(entityName);
		final SubFormTable tbl = pnlHistory.getSubform().getSubformTable();
		Date firstSubformEntryDate = null;
		List<CollectableEntityObject> allHistoryEntries = (List<CollectableEntityObject>)((CollectableTableModel)tbl.getSubFormModel()).getCollectables();
		for (CollectableEntityObject ceo : allHistoryEntries) {
			if (!LangUtils.equal(ceo.getValueId(E.HISTORY.entity.getUID()), this.entityUID)) {
				firstSubformEntryDate = (Date)ceo.getValue(E.HISTORY.validuntil.getUID());
				break;
			}
		}
		boolean removeAllSubs = firstSubformEntryDate == null || dateHistorical.before(firstSubformEntryDate);
		
		// get all depending entity entries from current layout
		// find all removed dependants. remember to skip in next step.
		final Set<Object> lstRemovedEntries = new HashSet<Object>();
		for (CollectableEntityObject clctEntry : clctHistoryEntries) {
			Date dateValidUntil = (Date)clctEntry.getValue(E.HISTORY.validuntil.getUID());
			boolean isSubentity = !LangUtils.equal(clctEntry.getValueId(E.HISTORY.entity.getUID()), entityMeta.getUID());
			if (removeAllSubs || (isSubentity && !dateHistorical.equals(dateValidUntil) && dateHistorical.after(dateValidUntil))) {
				lstRemovedEntries.add(clctEntry.getField(entityobjectFieldUid).getValue());
			}
		}
		
		for (CollectableEntityObject clctEntry :
			CollectionUtils.select(clctHistoryEntries, new Predicate<Collectable>() {
				final EntityMeta<?> eMeta = MetaProvider.getInstance().getEntity(entityName);
				@Override
				public boolean evaluate(Collectable t) {
					return !LangUtils.equal(t.getValueId(E.HISTORY.entity.getUID()), eMeta.getUID())
							&& (dateHistorical.equals((Date)t.getValue(E.HISTORY.validuntil.getUID()))
									|| !dateHistorical.after((Date)t.getValue(E.HISTORY.validuntil.getUID())));
				}
			}))
		{
			final EntityObjectVO<?> eo = clctEntry.getEntityObjectVO();
			if (lstRemovedEntries.contains(eo.getFieldValue(entityobjectFieldUid)))
				continue; // skip removed entries.
			
			final EntityMeta<?> eMeta = MetaProvider.getInstance().getEntity(eo.getFieldUid(E.HISTORY.entity.getUID()));
			boolean skip = true;
			FieldMeta<?> refField = null;
			for (FieldMeta<?> efMeta : MetaProvider.getInstance().getAllEntityFieldsByEntity(eMeta.getUID()).values()) {
				if (LangUtils.equal(efMeta.getForeignEntity(), entityName)) {
					skip = false;
					refField = efMeta;
					break;
				}		
			}
			if (skip) // skip if we do not have referencing fields.
				continue;

			Collectable<?> clctDependant = null;
			for (Object oDependant : clct.getDependants(refField)) {
				final Collectable<?> dependant = (Collectable<?>)oDependant;
				if (LangUtils.equal(dependant.getId(),
						eo.getFieldValue(entityobjectFieldUid))) {
					clctDependant = dependant;
					break;
				}
			}
			boolean isNew = false;
			if (clctDependant == null) {
				isNew = true;
				clctDependant = new CollectableEntityObject(
						CollectableEOEntityClientProvider.getInstance().getCollectableEntity(eMeta.getUID()), EntityObjectVO.newObject(eMeta.getUID()));
				((CollectableEntityObject)clctDependant).getEntityObjectVO().setPrimaryKey(eo.getFieldValue(entityobjectFieldUid));
				if (clct instanceof CollectableMasterDataWithDependants)
					((CollectableMasterDataWithDependants)clct).getMasterDataWithDependantsCVO().getDependents()
							.addData(refField, ((CollectableEntityObject)clctDependant).getMasterDataCVO().getEntityObject());
				if (clct instanceof CollectableGenericObjectWithDependants)
					((CollectableGenericObjectWithDependants)clct).getGenericObjectWithDependantsCVO().getDependents()
							.addData(refField, ((CollectableEntityObject)clctDependant).getMasterDataCVO().getEntityObject());
			}
			
			isNew = isNew || ((CollectableEntityObject)clctDependant).getEntityObjectVO().isFlagNew();
			replaceWithHistoricalValues(null, dateHistorical, entityName, clct.getId(), (CollectableEntityObject)clctDependant);
			
			if (isNew) {
				((CollectableEntityObject)clctDependant).getEntityObjectVO().flagNew();
			}

			final CollectableEntity clcte = ((CollectableEntityObject)clctDependant).getCollectableEntity();
			final CollectableWithDependants clctWithDependants;
			if (!Modules.getInstance().isModule(clcte.getUID())) {
				clctWithDependants = CollectableMasterDataWithDependants.newInstance(clcte, 
						DalSupportForMD.wrapEntityObjectVO(((CollectableEntityObject)clctDependant).getEntityObjectVO()));
				((CollectableMasterDataWithDependants)clctWithDependants).getMasterDataWithDependantsCVO().setDependents(((CollectableEntityObject)clctDependant).getEntityObjectVO().getDependents());
			}
			else {
				clctWithDependants = CollectableGenericObjectWithDependants.newCollectableGenericObjectWithDependants(
						DalSupportForGO.getGenericObjectVO(((CollectableEntityObject)clctDependant).getEntityObjectVO(), (CollectableEOEntity)clcte));
				((CollectableGenericObjectWithDependants)clctWithDependants).getGenericObjectWithDependantsCVO().setDependents(((CollectableEntityObject)clctDependant).getEntityObjectVO().getDependents());
			}
			replaceWithHistoricalValues(sSavepoint, iParentId, dateHistorical, clctWithDependants);

			if (clctWithDependants instanceof CollectableMasterDataWithDependants)
				((CollectableEntityObject)clctDependant).getEntityObjectVO().setDependents(
						((CollectableMasterDataWithDependants)clctWithDependants).getMasterDataWithDependantsCVO().getDependents());
			if (clctWithDependants instanceof CollectableGenericObjectWithDependants)
				((CollectableEntityObject)clctDependant).getEntityObjectVO().setDependents(
						((CollectableGenericObjectWithDependants)clctWithDependants).getGenericObjectWithDependantsCVO().getDependents());
		}

		// remove id of dependant subform data with flag new
		if (clct instanceof CollectableMasterDataWithDependants)
			dependants = ((CollectableMasterDataWithDependants)clct).getMasterDataWithDependantsCVO().getDependents();
		if (clct instanceof CollectableGenericObjectWithDependants)
			dependants = ((CollectableGenericObjectWithDependants)clct).getGenericObjectWithDependantsCVO().getDependents();
		if (dependants != null) {
			List<EntityObjectVO<?>> allData = CollectionUtils.concatAll(dependants.getRoDataMap().values());
			for (EntityObjectVO<?> eo : CollectionUtils.select(allData, new Predicate<EntityObjectVO<?>>() {
				@Override
				public boolean evaluate(EntityObjectVO<?> t) {
					return t.isFlagNew();
				}
			})) {
				eo.setPrimaryKey(null);
			}
		}
	}

	private void replaceWithHistoricalValues(final String sSavepoint, final Date dateHistorical, final UID parentUid, final Object iParentId, final CollectableEntityObject clct) {
		
		List<CollectableEntityObject> clctHistoryEntries = getHistoryEntries(sSavepoint, iParentId, dateHistorical);
		// get all depending current entity
		for (CollectableEntityObject clctEntry :
			CollectionUtils.select(clctHistoryEntries, new Predicate<Collectable>() {
				final EntityMeta<?> eMeta = MetaProvider.getInstance().getEntity(clct.getEntityObjectVO().getDalEntity());
				@Override
				public boolean evaluate(Collectable t) {
					return LangUtils.equal(t.getValueId(E.HISTORY.entity.getUID()), eMeta.getUID())
							&& LangUtils.equal(t.getValue((clct.getId() instanceof UID
									? E.HISTORY.entityobjectuid.getUID() : E.HISTORY.entityobjectid.getUID())), clct.getId())
							&& (dateHistorical.equals(t.getValue(E.HISTORY.validuntil.getUID()))
									|| dateHistorical.before((Date)t.getValue(E.HISTORY.validuntil.getUID())));
				}
			}))
		{	
			try {
				setCollectableFields(parentUid, iParentId, clctEntry.getEntityObjectVO(), clct);
			} catch (Exception e) {
				//LOG.warn("unable to set field", e);
			}
		}
	}

	private void setCollectableFields(UID parentUid, Object iParentId, EntityObjectVO eo, Collectable clct)  throws Exception {
		final FieldMeta<?> efMeta = eo.getFieldUid(E.HISTORY.entityfield.getUID()) == null
				? SF.getByField((String)eo.getFieldValue(E.HISTORY.systemfield.getUID(), String.class)).getMetaData(eo.getFieldUid(E.HISTORY.entityfield.getUID())) 
						: MetaProvider.getInstance().getEntityField(eo.getFieldUid(E.HISTORY.entityfield.getUID()));
		
		final UID fieldname;
		if (eo.getFieldUid(E.HISTORY.systemfield.getUID()) == null)
			fieldname = efMeta.getUID();
		else {
			fieldname = eo.getFieldUid(E.HISTORY.systemfield.getUID());
		}
		
		CollectableField clctField = null;
		if (efMeta.getForeignEntity() != null) {
			final EntityMeta<?> refEntityMeta = MetaProvider.getInstance().getEntity(efMeta.getForeignEntity());
			// test if entry has changed parent reference.
			if (LangUtils.equal(efMeta.getForeignEntity(), parentUid)
					&& !LangUtils.equal((refEntityMeta.isUidEntity() ? (UID)eo.getFieldValue(E.HISTORY.value_refuid.getUID()) : (Long)eo.getFieldValue(E.HISTORY.value_refid.getUID())), iParentId)) {
				if (clct instanceof CollectableEntityObject)
					((CollectableEntityObject)clct).getEntityObjectVO().flagRemove();
			} else {
				clctField = new CollectableValueIdField(
						(refEntityMeta.isUidEntity() ? (UID)eo.getFieldValue(E.HISTORY.value_refuid.getUID()) : (Long)eo.getFieldValue(E.HISTORY.value_refid.getUID())),
							efMeta == null ? eo.getFieldValue(E.HISTORY.value_string.getUID()) : HistoryVO.getStringAsObjectValue(efMeta, (String)eo.getFieldValue(E.HISTORY.value_string.getUID())));
			}
		}
		else {
			if (eo.getFieldValue(E.HISTORY.value_byte) != null)
				clctField = new CollectableValueField(HistoryVO.getByteArrayAsObject((byte[])eo.getFieldValue(E.HISTORY.value_byte.getUID())));
			else
				clctField = new CollectableValueField(efMeta == null ? eo.getFieldValue(E.HISTORY.value_string.getUID()) : HistoryVO.getStringAsObjectValue(efMeta, (String)eo.getFieldValue(E.HISTORY.value_string.getUID())));				
		}
		
		if (clctField != null) {
			if (!clctField.equals(clct.getField(fieldname))) {
				clct.setField(fieldname, clctField);
			}
		}
	}
	
	private List<CollectableEntityObject> getHistoryEntries(final String sSavepoint, final Object iParentId, final Date dateHistorical) {
		final SubFormTable tbl = pnlHistory.getSubform().getSubformTable();
		List<CollectableEntityObject> result = CollectionUtils.select((List<CollectableEntityObject>)((CollectableTableModel)tbl.getSubFormModel()).getCollectables(), 
				new Predicate<Collectable>() {
					public boolean evaluate(Collectable t) {
						return sSavepoint == null || (LangUtils.equal(t.getValue(E.HISTORY.savepoint.getUID()), sSavepoint)
								|| ((dateHistorical.equals((Date) t.getValue(E.HISTORY.validuntil.getUID()))
								|| dateHistorical.before((Date) t.getValue(E.HISTORY.validuntil.getUID())))
								//&& !LangUtils.equals((iParentId instanceof UID ? UID.parseUID((String)t.getValue(E.HISTORY.entityobject.getUID())) : Long.valueOf((String)t.getValue(E.HISTORY.entityobject.getUID()))), iParentId)
						));
					};
				});
		
		Collections.sort(result, new Comparator<CollectableEntityObject>() {
			@Override
			public int compare(CollectableEntityObject o1,
					CollectableEntityObject o2) {
				int compare = LangUtils.compare(
						o1.getValue(E.HISTORY.validuntil.getUID()),
						o2.getValue(E.HISTORY.validuntil.getUID()));
				if (compare == 0)
					compare = LangUtils.compare(o1.getId(), o2.getId());
				return compare * -1;//desc
			}
		});
		return result;
	}
}