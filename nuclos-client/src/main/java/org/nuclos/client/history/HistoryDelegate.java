//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.history;

import java.util.Collection;

import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.history.ejb3.HistoryFacadeRemote;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Business Delegate for <code>HistoryFacadeBean</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author      <a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
// @Component
public class HistoryDelegate {

	private static HistoryDelegate INSTANCE;

	// 

	private HistoryFacadeRemote facade;


	/**
	 * Use getInstance() to create an (the) instance of this class
	 */
	HistoryDelegate() throws RuntimeException {
		INSTANCE = this;
	}

	public static HistoryDelegate getInstance() {
		if (INSTANCE == null) {
			// lazy support
			INSTANCE = SpringApplicationContextHolder.getBean(HistoryDelegate.class);
		}
		return INSTANCE;
	}
	
	// @Autowired
	public final void setHistoryFacadeRemote(HistoryFacadeRemote historyFacadeRemote) {
		this.facade = historyFacadeRemote;
	}
	
	public Collection<MasterDataVO<Long>> getHistoryData(UID sEntity, Object iObjectId) {
		 try {
			 final Collection<MasterDataVO<Long>> result = facade.getHistoryData(sEntity, iObjectId);
			 assert result != null;
			 return result;
		 }
		 catch (RuntimeException | CommonPermissionException ex) {
			 throw new CommonFatalException(ex);
		 }
	}
}
