//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.history;

import java.awt.BorderLayout;
import java.util.Collection;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import org.nuclos.client.masterdata.valuelistprovider.HistoryEntityFieldFieldsProvider;
import org.nuclos.client.masterdata.valuelistprovider.HistoryEntityFieldsProvider;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonFatalException;

/**
 * Panel for displaying the logbook.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */

public class HistoryPanel extends JPanel {

	private final SubForm subform;

	private UID entityUID;
	
	public HistoryPanel(final UID entityUID) {
		this.entityUID = entityUID;
		this.setLayout(new BorderLayout());
		this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

		this.subform = new SubForm(E.HISTORY.getUID(), false, -1, UID.UID_NULL) {
			@Override
			public CollectableFieldsProvider getValueListProvider(UID sColumnName) {
				
				if (sColumnName.equals(E.HISTORY.entity.getUID())) {
					return new HistoryEntityFieldsProvider(entityUID);
				}
				if (sColumnName.equals(E.HISTORY.entityfield.getUID())) {
					return new HistoryEntityFieldFieldsProvider(entityUID , false);
}
				if (sColumnName.equals(E.HISTORY.systemfield.getUID())) {
					return new HistoryEntityFieldFieldsProvider(entityUID , true);
				}
				return super.getValueListProvider(sColumnName);
			}
			
			public UID getForeignKeyFieldName(UID sParentEntityName, CollectableEntity clcte) {

				// Use the field referencing the parent entity from the subform, if any:
				UID result = this.getForeignKeyFieldToParent();

				if (result == null) {
					// Default: Find the field referencing the parent entity from the meta data.
					// If more than one field applies, throw an exception:
					for (UID sFieldName : clcte.getFieldUIDs()) {
						if (sParentEntityName.equals(clcte.getEntityField(sFieldName).getReferencedEntityUID())) {
							if (result == null) {
								// this is the foreign key field:
								result = sFieldName;
							}
							else {
								final String sMessage = SpringLocaleDelegate.getInstance().getMessage(
										"SubForm.4","Das Unterformular f\u00fcr die Entit\u00e4t \"{0}\" enth\u00e4lt mehr als ein Fremdschl\u00fcsselfeld, das die \u00fcbergeordnete Entit\u00e4t \"{1}\" referenziert:\n\t{2}\n\t{3}\nBitte geben Sie das Feld im Layout explizit an.", clcte.getLabel(), sParentEntityName, result, sFieldName);
								throw new CommonFatalException(sMessage);
							}
						}
					}
				}

				if (result == null) {
					result = UID.UID_NULL;
				}
				assert result != null;
				return result;
			}
		};
		subform.setEnabled(false);

		this.add(this.subform, BorderLayout.CENTER);
	}
	
	final SubForm getSubform() {
		return subform;
	}
}
