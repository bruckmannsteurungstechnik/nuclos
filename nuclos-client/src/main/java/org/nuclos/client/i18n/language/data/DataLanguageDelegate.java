package org.nuclos.client.i18n.language.data;

import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.client.remote.NoConnectionTimeoutRunner;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.server.i18n.language.data.DataLanguageFacadeRemote;
import org.nuclos.server.i18n.language.data.DataLanguageVO;

public class DataLanguageDelegate {

	private static DataLanguageDelegate INSTANCE;
	private static final Logger LOG = Logger.getLogger(DataLanguageDelegate.class);	
	
	private Collection<DataLanguageVO> allSelectedLanguages;
	
	private DataLanguageDelegate() {}

	public static DataLanguageDelegate getInstance()
	{
		if (INSTANCE == null) {
			INSTANCE = SpringApplicationContextHolder.getBean(DataLanguageDelegate.class);
		}
		return INSTANCE;
	}
	
	private DataLanguageFacadeRemote dataLanguageFacadeRemote;

	public void setDataLanguageFacadeRemote(DataLanguageFacadeRemote dataLanguageFacadeRemote) {
		this.dataLanguageFacadeRemote = dataLanguageFacadeRemote;
	}
	
	public Collection<DataLanguageVO> getSelectedLocales() {
		
		if (allSelectedLanguages == null) {
			allSelectedLanguages = this.dataLanguageFacadeRemote.getDataLanguages(); 
		}
		return allSelectedLanguages;
	}
	
	public List<UID> getSelectedLocaleUIDs() {
		
		if (allSelectedLanguages == null) {
			allSelectedLanguages = this.dataLanguageFacadeRemote.getDataLanguages(); 
		}
		
		List<UID> lstUIDs = CollectionUtils.transform(allSelectedLanguages, new Transformer<DataLanguageVO, UID>() {
			@Override
			public UID transform(DataLanguageVO i) {
				return i.getPrimaryKey();
			}
		});
		
		return lstUIDs;
	}

	public Collection<DataLanguageVO> getAllDataLanguages() {
		return this.dataLanguageFacadeRemote.getAllDataLanguages();
	}
		
	public UID getPrimaryDataLanguage() {
		return this.dataLanguageFacadeRemote.getPrimaryDataLanguageUID();
	}
	
	public void save(final List<DataLanguageVO> selectDataLanguages) throws Exception {
		NoConnectionTimeoutRunner.runSynchronized(() -> {
			dataLanguageFacadeRemote.save(selectDataLanguages);
			return null;
		});
	}
	
	public void invalidate() {
		this.allSelectedLanguages = null;
	}
	
}
