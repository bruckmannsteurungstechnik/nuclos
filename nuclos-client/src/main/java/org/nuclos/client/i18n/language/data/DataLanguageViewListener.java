package org.nuclos.client.i18n.language.data;

import java.awt.event.ActionListener;
import java.util.List;

import org.nuclos.server.i18n.language.data.DataLanguageVO;

public interface DataLanguageViewListener extends ActionListener{
	
	public List<DataLanguageVO> getAvailables();
	public List<DataLanguageVO> getSelected();
		
	public void setPrimaryDataLanguage(DataLanguageVO item);
	
	public void addDataLanguages(List<DataLanguageVO> elemnts);
	public void removeDataLanguages(List<DataLanguageVO> elemnts);
	public void moveUpDataLanguages(DataLanguageVO dlvo);
	public void moveDownDataLanguages(DataLanguageVO dlvo);
	
	public void save();
	public void refresh();
	public void switchSave();
}
