//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.layout.wysiwyg.component;

import java.awt.EventQueue;
import java.awt.KeyboardFocusManager;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.SwingUtilities;

import org.nuclos.client.genericobject.GenericObjectCollectController;
import org.nuclos.client.genericobject.controller.StateChangeAction;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels.CHANGESTATEACTIONLISTENER;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels.STATIC_BUTTON;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.collect.CollectActionAdapter;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.client.ui.collect.CollectController.CollectableEventListener;
import org.nuclos.client.ui.collect.CollectController.MessageType;
import org.nuclos.client.ui.collect.result.ResultActionCollection;
import org.nuclos.client.ui.collect.result.ResultActionCollection.ResultActionType;
import org.nuclos.client.ui.layoutml.LayoutMLParser;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common2.LangUtils;
import org.nuclos.server.statemodel.valueobject.StateVO;

/**
 * This is a Button Action for changing the State of a GO
 * 
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * NUCLEUSINT-1159
 * @author <a href="mailto:hartmut.beckschulze@novabit.de">hartmut.beckschulze</a>
 * @version 01.00.00
 */
public class ChangeStateButtonAction<PK,Clct extends Collectable<PK>> implements CollectActionAdapter<PK,Clct> {

	@Override
	public void run(final JButton btn, final CollectController<PK,Clct> controller, Properties probs) {
		try {
			final Object c = controller;
			if (c instanceof GenericObjectCollectController) {
				final GenericObjectCollectController gController = (GenericObjectCollectController) c;
				
				if (!controller.getDetailsPanel().isVisible()) {
					// is not in details view, is in search or elsewhere
					return;
				}
				
				final UID targetState = UID.parseUID(probs.getProperty(STATIC_BUTTON.STATE_CHANGE_ACTION_ARGUMENT));
				
				StateChangeAction sca = null;
				for (ResultActionCollection rac : gController.getResultActionsMultiThreaded(gController.getSelectedCollectables())) {
					if (sca == null && rac.getType().equals(ResultActionType.CHANGE_STATE)) {
						for (Action act : rac.getActions()) {
							if (act instanceof StateChangeAction) {
								if (targetState.equals(((StateChangeAction) act).getStateTarget().getId())) {
									sca = (StateChangeAction) act;
									break;
								}
							}
						}
					}
				}
				if (sca != null) {
					sca.actionPerformed(null);
				}

				//@todo refactor to LayoutMLButton.
				controller.addCollectableEventListener(new CollectableEventListener() {
					@Override
					public void handleCollectableEvent(
							Collectable collectable,
							final MessageType messageType) {
						if (messageType.equals(MessageType.STATECHANGE_DONE)
								|| messageType.equals(MessageType.STATECHANGE_UNDONE)) {
							SwingUtilities.invokeLater(new Runnable() {
								@Override
								public void run() {
									// Hier keinen Refresh! Wird bereits von GenericObjectCollectController.changeStatesForSingleObjectAndSave angestoßen.
//									try {
//										if (messageType.equals(MessageType.STATECHANGE_DONE))
//											controller.refreshCurrentCollectable(false);
//									} catch (CommonBusinessException e) {
//										Errors.getInstance().showExceptionDialog(controller.getCollectPanel(), e);
//									}
									
									// Must be invoked later, else focus is not set with compound components like LOVs
									EventQueue.invokeLater(new Runnable() {
										@Override
							            public void run() {
											if (btn.getClientProperty(LayoutMLParser.ATTRIBUTE_NEXTFOCUSONACTION) != null 
													&& btn.getClientProperty(LayoutMLParser.ATTRIBUTE_NEXTFOCUSONACTION).equals(Boolean.TRUE)) {
												KeyboardFocusManager.getCurrentKeyboardFocusManager().focusNextComponent(btn);
											}
										}
									});
								}
							});
						}
						controller.removeCollectableEventListener(this);
					}
				});
			}
			else {
				Errors.getInstance().showExceptionDialog(controller.getCollectPanel(),new NuclosBusinessException(CHANGESTATEACTIONLISTENER.NO_GENERICOBJECT));
			}
		}
		catch (Exception e) {
			Errors.getInstance().showExceptionDialog(controller.getCollectPanel(), e);
		}
	}

	@Override
	public boolean isRunnable(CollectController<PK,Clct> controller, Properties probs) {
		final Object c = controller;
		if(c instanceof GenericObjectCollectController) {
			final GenericObjectCollectController gController = (GenericObjectCollectController) c;
			
			if (!controller.getDetailsPanel().isVisible()) {
				// is not in details view, is in search or elsewhere
				return false;
			}
			
			if (controller.getCollectState().isDetailsModeNew()) {
				// only possible state is initial state
				return false;
			}
			
			final UID targetState = UID.parseUID(probs.getProperty(STATIC_BUTTON.STATE_CHANGE_ACTION_ARGUMENT));
			
			final List<StateVO> lstSubsequentStates = gController.getPossibleSubsequentStates();
			for (Iterator<StateVO> it = lstSubsequentStates.iterator(); it.hasNext();) {
				StateVO stateVO = it.next();
				if(LangUtils.equal(stateVO.getId(), targetState)) {
					return true;
				}
			}
		}
		return false;
	}
}
