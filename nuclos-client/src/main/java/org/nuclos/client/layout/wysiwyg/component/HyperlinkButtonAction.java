//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.layout.wysiwyg.component;

import java.awt.EventQueue;
import java.awt.KeyboardFocusManager;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.SwingUtilities;

import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels.STATIC_BUTTON;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.collect.CollectActionAdapter;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableHyperlink;
import org.nuclos.client.ui.layoutml.LayoutMLParser;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common2.StringUtils;

/**
 * This is a Button Action for open an hyperlink
 * 
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * NUCLEUSINT-1159
 * @author <a href="mailto:hartmut.beckschulze@novabit.de">hartmut.beckschulze</a>
 * @version 01.00.00
 */
public class HyperlinkButtonAction<PK,Clct extends Collectable<PK>> implements CollectActionAdapter<PK,Clct> {

	@Override
	public void run(final JButton btn, final CollectController<PK,Clct> controller, Properties probs) {
		try {
			if (!controller.getDetailsPanel().isVisible()) {
				// is not in details view, is in search or elsewhere
				return;
			}
			
			final UID hyperlinkField = UID.parseUID(probs.getProperty(STATIC_BUTTON.HYPERLINK_ACTION_ARGUMENT));
			
            for (CollectableComponent c : controller.getDetailCollectableComponentsFor(hyperlinkField)) {
            	if (c instanceof CollectableHyperlink) {
            		((CollectableHyperlink)c).getHyperlink().openLink();
            	}
            }
			
			//@todo refactor to LayoutMLButton.
			SwingUtilities.invokeLater(new Runnable() {
				
				@Override
				public void run() {
					// Must be invoked later, else focus is not set with compound components like LOVs
					EventQueue.invokeLater(new Runnable() {
						@Override
			            public void run() {
							if (btn.getClientProperty(LayoutMLParser.ATTRIBUTE_NEXTFOCUSONACTION) != null 
									&& btn.getClientProperty(LayoutMLParser.ATTRIBUTE_NEXTFOCUSONACTION).equals(Boolean.TRUE)) {
								KeyboardFocusManager.getCurrentKeyboardFocusManager().focusNextComponent(btn);
							}
						}
					});
				}
			});
		}
		catch (Exception e) {
			Errors.getInstance().showExceptionDialog(controller.getCollectPanel(), e);
		}
	}

	@Override
	public boolean isRunnable(CollectController<PK,Clct> controller, Properties probs) {			
		if (!controller.getDetailsPanel().isVisible()) {
			// is not in details view, is in search or elsewhere
			return false;
		}
		
		if (controller.getCollectState().isDetailsModeNew()) {
			// only possible state is initial state
			return false;
		}
		
		final UID hyperlinkField = UID.parseUID(probs.getProperty(STATIC_BUTTON.HYPERLINK_ACTION_ARGUMENT));
		
        for (CollectableComponent c : controller.getDetailCollectableComponentsFor(hyperlinkField)) {
        	if (c instanceof CollectableHyperlink) {
        		return !StringUtils.isNullOrEmpty(((CollectableHyperlink)c).getHyperlink().getText());
        	}
        }

        return false;
	}
}
