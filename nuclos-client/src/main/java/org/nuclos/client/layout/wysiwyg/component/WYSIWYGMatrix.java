package org.nuclos.client.layout.wysiwyg.component;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JLayeredPane;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.event.EventListenerList;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.NuclosCollectableEntityProvider;
import org.nuclos.client.layout.wysiwyg.WYSIWYGMetaInformation;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels.ERROR_MESSAGES;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels.PROPERTY_LABELS;
import org.nuclos.client.layout.wysiwyg.component.properties.ComponentProperties;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyUtils;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValue;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValueBoolean;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValueString;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.WYSIWYGLayoutEditorPanel;
import org.nuclos.client.layout.wysiwyg.editor.util.TableLayoutUtil;
import org.nuclos.client.layout.wysiwyg.editor.util.mouselistener.PropertiesDisplayMouseListener;
import org.nuclos.client.layout.wysiwyg.editor.util.popupmenu.ComponentPopUp;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.TableLayoutPanel;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.WYSIWYGValuelistProvider;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.layoutmlrules.LayoutMLRules;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModel;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelProvider;
import org.nuclos.client.ui.collect.component.model.DefaultCollectableComponentModelProvider;
import org.nuclos.client.ui.event.IPopupListener;
import org.nuclos.client.ui.event.PopupMenuMouseAdapter;
import org.nuclos.client.ui.matrix.JMatrixComponent;
import org.nuclos.client.ui.matrix.MatrixCollectable;
import org.nuclos.client.ui.matrix.MatrixTableModel;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosScript;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;

public class WYSIWYGMatrix extends JLayeredPane implements WYSIWYGComponent, MouseListener, WYSIWYGScriptComponent {
	
	private static final Logger LOG = Logger.getLogger(WYSIWYGMatrix.class);
	
	JMatrixComponent matrix;
	String sEntityFieldPreferences;
	String sEntityX;
	String sEntityY;
	String sEntityYParentField;
	String sEntityMatrix;
	String sEntityMatrixParentField;
	String sEntityMatrixXRefField;
	String sEntityMatrixValueField;
	
	String sEntityFieldCategorie;
	String sEntityFieldX;
	String sEntityFieldY;
	Integer sNumberState;
	String inputType;
	
	String entityMatrixReferenceField;
	
	String entityXVlpId;
	String entityXVlpIdFieldName;
	String entityXVlpFieldName;

	String entityXVlpReferenceParamName;
	
	String entityXHeader;
	String entityYHeader;
	
	String entityXSortFields;
	String entityYSortFields;
	
	private ComponentProperties properties;
	private boolean bSelected;
	
	private WYSIWYGMetaInformation meta;
	
	private LayoutMLRules matrixLayoutRules = new LayoutMLRules();
	
	private EventListenerList listenerList = new EventListenerList();
	
	private Rectangle lastViewPosition = null;
	private MatrixTableModel model;
	private Map<UID, WYSIWYGMatrixColumn> columns = new HashMap<UID, WYSIWYGMatrixColumn>();
	
	public static final String PROPERTY_NAME = PROPERTY_LABELS.NAME;
	public static final String PROPERTY_ENTITY_X = PROPERTY_LABELS.ENTITY_X;
	public static final String PROPERTY_ENTITY_FIELD_PREFERENCES = PROPERTY_LABELS.ENTITY_FIELD_PREFERENCES;
	public static final String PROPERTY_ENTITY_FIELD_CATEGORIE = PROPERTY_LABELS.ENTITY_FIELD_CATEGORIE;
	public static final String PROPERTY_ENTITY_FIELD_X = PROPERTY_LABELS.ENTITY_FIELD_X;
	public static final String PROPERTY_ENTITY_FIELD_Y = PROPERTY_LABELS.ENTITY_FIELD_Y;
	public static final String PROPERTY_ENTITY_Y = PROPERTY_LABELS.ENTITY_Y;
	public static final String PROPERTY_ENTITY_Y_PARENT_FIELD = PROPERTY_LABELS.ENTITY_Y_PARENT_FIELD;
	public static final String PROPERTY_ENTITY_MATRIX = PROPERTY_LABELS.ENTITY_MATRIX;
	public static final String PROPERTY_ENTITY_MATRIX_VALUE_FIELD = PROPERTY_LABELS.ENTITY_MATRIX_VALUE_FIELD;
	public static final String PROPERTY_ENTITY_MATRIX_PARENT_FIELD = PROPERTY_LABELS.ENTITY_MATRIX_PARENT_FIELD;
	public static final String PROPERTY_ENTITY_MATRIX_X_REF_FIELD = PROPERTY_LABELS.ENTITY_MATRIX_X_REF_FIELD;
	public static final String PROPERTY_ENABLED = PROPERTY_LABELS.ENABLED;
	public static final String PROPERTY_OPAQUE = PROPERTY_LABELS.OPAQUE;
	public static final String PROPERTY_ENTITY_MATRIX_NUMBER_STATE = PROPERTY_LABELS.ENTITY_MATRIX_NUMBER_STATE;
	
	public static final String PROPERTY_CELL_INPUT_TYPE = PROPERTY_LABELS.CELL_INPUT_TYPE;
	
	public static final String PROPERTY_ENTITY_MATRIX_REFERENCE_FIELD = PROPERTY_LABELS.ENTITY_MATRIX_REFERENCE_FIELD;

	public static final String PROPERTY_ENTITY_X_VLP_ID = PROPERTY_LABELS.ENTITY_X_VLP_ID;
	public static final String PROPERTY_ENTITY_X_VLP_IDFIELDNAME = PROPERTY_LABELS.ENTITY_X_VLP_IDFIELDNAME;
	public static final String PROPERTY_ENTITY_X_VLP_FIELDNAME = PROPERTY_LABELS.ENTITY_X_VLP_FIELDNAME;
	public static final String PROPERTY_ENTITY_X_VLP_REFERENCE_PARAM_NAME = PROPERTY_LABELS.ENTITY_X_VLP_REFERENCE_PARAM_NAME;

	public static final String PROPERTY_ENTITY_X_HEADER = PROPERTY_LABELS.ENTITY_X_HEADER;
	public static final String PROPERTY_ENTITY_Y_HEADER = PROPERTY_LABELS.ENTITY_Y_HEADER;
	
	public static final String PROPERTY_ENTITY_X_SORTING_FIELDS = PROPERTY_LABELS.ENTITY_X_SORTING_FIELDS;
	public static final String PROPERTY_ENTITY_Y_SORTING_FIELDS = PROPERTY_LABELS.ENTITY_Y_SORTING_FIELDS;
	
	public static final String PROPERTY_NEW_ENABLED = PROPERTY_LABELS.NEW_ENABLED;
	public static final String PROPERTY_EDIT_ENABLED = PROPERTY_LABELS.EDIT_ENABLED;
	public static final String PROPERTY_DELETE_ENABLED = PROPERTY_LABELS.DELETE_ENABLED;
	public static final String PROPERTY_CLONE_ENABLED = PROPERTY_LABELS.CLONE_ENABLED;

	public static final String PROPERTY_CONTROLLERTYPE = PROPERTY_LABELS.CONTROLLERTYPE;

	public static final String PROPERTY_DYNAMIC_CELL_HEIGHTS_DEFAULT = PROPERTY_LABELS.DYNAMIC_CELL_HEIGHTS_DEFAULT;
	
//	private static final String[] PROPERTY_NAMES = new String[]{PROPERTY_NAME, PROPERTY_ENTITY_FIELD_PREFERENCES,
//		PROPERTY_FONT, PROPERTY_ENTITY_MATRIX, PROPERTY_ENTITY_MATRIX_VALUE_FIELD, PROPERTY_ENTITY_MATRIX_PARENT_FIELD, PROPERTY_ENTITY_X, PROPERTY_ENTITY_FIELD_CATEGORIE, PROPERTY_ENTITY_FIELD_X, PROPERTY_ENTITY_Y, PROPERTY_ENTITY_Y_PARENT_FIELD, PROPERTY_ENTITY_MATRIX_X_REF_FIELD, /*PROPERTY_ENTITY_FIELD_Y,*/ PROPERTY_PREFFEREDSIZE, PROPERTY_ENABLED,
//		PROPERTY_NEW_ENABLED, PROPERTY_EDIT_ENABLED, PROPERTY_DELETE_ENABLED, PROPERTY_CLONE_ENABLED, PROPERTY_DYNAMIC_CELL_HEIGHTS_DEFAULT, PROPERTY_BACKGROUNDCOLOR, PROPERTY_BORDER, PROPERTY_CONTROLLERTYPE};
	
	private static final String[] PROPERTY_NAMES = new String[]{PROPERTY_NAME, PROPERTY_FONT, PROPERTY_ENTITY_MATRIX, 
		PROPERTY_ENTITY_MATRIX_VALUE_FIELD, PROPERTY_ENTITY_MATRIX_NUMBER_STATE, PROPERTY_CELL_INPUT_TYPE, PROPERTY_ENTITY_MATRIX_PARENT_FIELD, PROPERTY_ENTITY_MATRIX_X_REF_FIELD, PROPERTY_ENTITY_FIELD_PREFERENCES,
		PROPERTY_ENTITY_MATRIX_REFERENCE_FIELD,
		PROPERTY_ENTITY_X, PROPERTY_ENTITY_FIELD_CATEGORIE, PROPERTY_ENTITY_FIELD_X, PROPERTY_ENTITY_Y, PROPERTY_ENTITY_Y_PARENT_FIELD, PROPERTY_ENTITY_FIELD_Y,
		PROPERTY_ENTITY_X_VLP_ID, PROPERTY_ENTITY_X_VLP_IDFIELDNAME, PROPERTY_ENTITY_X_VLP_FIELDNAME, PROPERTY_ENTITY_X_VLP_REFERENCE_PARAM_NAME,
		PROPERTY_ENTITY_X_HEADER, PROPERTY_ENTITY_Y_HEADER,
		PROPERTY_ENTITY_X_SORTING_FIELDS, PROPERTY_ENTITY_Y_SORTING_FIELDS,
		PROPERTY_PREFFEREDSIZE, PROPERTY_ENABLED, 
		PROPERTY_NEW_ENABLED, PROPERTY_EDIT_ENABLED, PROPERTY_DELETE_ENABLED, PROPERTY_CLONE_ENABLED,
		PROPERTY_BACKGROUNDCOLOR, PROPERTY_BORDER};
	
	private static final PropertyClass[] PROPERTY_CLASSES = new PropertyClass[]{
		new PropertyClass(PROPERTY_NAME, String.class),
		new PropertyClass(PROPERTY_ENTITY_FIELD_PREFERENCES, String.class),
		new PropertyClass(PROPERTY_ENTITY_X, String.class),
		new PropertyClass(PROPERTY_ENTITY_Y, String.class),
		new PropertyClass(PROPERTY_ENTITY_MATRIX, String.class),
		new PropertyClass(PROPERTY_ENTITY_MATRIX_PARENT_FIELD, String.class),
		new PropertyClass(PROPERTY_ENTITY_MATRIX_X_REF_FIELD, String.class),
		new PropertyClass(PROPERTY_ENTITY_MATRIX_VALUE_FIELD, String.class),
		new PropertyClass(PROPERTY_ENTITY_MATRIX_NUMBER_STATE, Integer.class),
		new PropertyClass(PROPERTY_CELL_INPUT_TYPE, String.class),
		
		new PropertyClass(PROPERTY_ENTITY_MATRIX_REFERENCE_FIELD, String.class),
		
		new PropertyClass(PROPERTY_ENTITY_X_VLP_ID, String.class),
		new PropertyClass(PROPERTY_ENTITY_X_VLP_IDFIELDNAME, String.class),
		new PropertyClass(PROPERTY_ENTITY_X_VLP_FIELDNAME, String.class),
		new PropertyClass(PROPERTY_ENTITY_X_VLP_REFERENCE_PARAM_NAME, String.class),
		
		new PropertyClass(PROPERTY_ENTITY_X_HEADER, String.class),
		new PropertyClass(PROPERTY_ENTITY_Y_HEADER, String.class),

		new PropertyClass(PROPERTY_ENTITY_X_SORTING_FIELDS, String.class),
		new PropertyClass(PROPERTY_ENTITY_Y_SORTING_FIELDS, String.class),
		
		new PropertyClass(PROPERTY_ENTITY_FIELD_CATEGORIE, String.class),
		new PropertyClass(PROPERTY_ENTITY_FIELD_X, String.class),		
		new PropertyClass(PROPERTY_ENTITY_Y_PARENT_FIELD, String.class),
		new PropertyClass(PROPERTY_ENTITY_FIELD_Y, String.class),
		new PropertyClass(PROPERTY_PREFFEREDSIZE, Dimension.class),
		new PropertyClass(PROPERTY_ENABLED, boolean.class),
		new PropertyClass(PROPERTY_NEW_ENABLED, NuclosScript.class),
		new PropertyClass(PROPERTY_EDIT_ENABLED, NuclosScript.class),
		new PropertyClass(PROPERTY_DELETE_ENABLED, NuclosScript.class),
		new PropertyClass(PROPERTY_CLONE_ENABLED, NuclosScript.class),
		new PropertyClass(PROPERTY_BACKGROUNDCOLOR, Color.class),
		new PropertyClass(PROPERTY_BORDER, Border.class),
		new PropertyClass(PROPERTY_CONTROLLERTYPE, String.class),
		new PropertyClass(PROPERTY_DYNAMIC_CELL_HEIGHTS_DEFAULT, boolean.class),
		new PropertyClass(PROPERTY_FONT, Font.class)};
	
	private static final PropertySetMethod[] PROPERTY_SETMETHODS = new PropertySetMethod[]{
		new PropertySetMethod(PROPERTY_NAME, "setName"),
		new PropertySetMethod(PROPERTY_ENTITY_FIELD_PREFERENCES, "setPreferences"),
		new PropertySetMethod(PROPERTY_ENTITY_X, "setEntityX"),
		new PropertySetMethod(PROPERTY_ENTITY_Y, "setEntityY"),
		new PropertySetMethod(PROPERTY_ENTITY_MATRIX, "setEntityMatrix"),
		new PropertySetMethod(PROPERTY_ENTITY_MATRIX_PARENT_FIELD, "setEntityMatrixParentField"),
		new PropertySetMethod(PROPERTY_ENTITY_MATRIX_X_REF_FIELD, "setEntityMatrixXRefField"),
		new PropertySetMethod(PROPERTY_ENTITY_MATRIX_VALUE_FIELD, "setEntityMatrixValueField"),
		new PropertySetMethod(PROPERTY_ENTITY_MATRIX_NUMBER_STATE, "setNumberState"),
		new PropertySetMethod(PROPERTY_CELL_INPUT_TYPE, "setInputType"),
		
		new PropertySetMethod(PROPERTY_ENTITY_MATRIX_REFERENCE_FIELD, "setEntityMatrixReferenceField"),

		new PropertySetMethod(PROPERTY_ENTITY_X_VLP_ID, "setEntityXVlpId"),
		new PropertySetMethod(PROPERTY_ENTITY_X_VLP_IDFIELDNAME, "setEntityXVlpIdFieldName"),
		new PropertySetMethod(PROPERTY_ENTITY_X_VLP_FIELDNAME, "setEntityXVlpFieldName"),
		new PropertySetMethod(PROPERTY_ENTITY_X_VLP_REFERENCE_PARAM_NAME, "setEntityXVlpReferenceParamName"),
		
		new PropertySetMethod(PROPERTY_ENTITY_X_HEADER, "setEntityXHeader"),
		new PropertySetMethod(PROPERTY_ENTITY_Y_HEADER, "setEntityYHeader"),

		new PropertySetMethod(PROPERTY_ENTITY_X_SORTING_FIELDS, "setEntityXSortFields"),
		new PropertySetMethod(PROPERTY_ENTITY_Y_SORTING_FIELDS, "setEntityYSortFields"),
		
		new PropertySetMethod(PROPERTY_ENTITY_FIELD_CATEGORIE, "setEntityFieldCategorie"),
		new PropertySetMethod(PROPERTY_ENTITY_FIELD_X, "setEntityFieldX"),
		new PropertySetMethod(PROPERTY_ENTITY_Y_PARENT_FIELD, "setEntityYParentField"),
		new PropertySetMethod(PROPERTY_ENTITY_FIELD_Y, "setEntityFieldY"),
		new PropertySetMethod(PROPERTY_PREFFEREDSIZE, "setPreferredSize"),
		new PropertySetMethod(PROPERTY_ENABLED, "setEnabled"),
		new PropertySetMethod(PROPERTY_BACKGROUNDCOLOR, "setBackground"),
		new PropertySetMethod(PROPERTY_BORDER, "setBorder"),
		new PropertySetMethod(PROPERTY_FONT, "setFont"), 
		new PropertySetMethod(PROPERTY_DESCRIPTION, "setToolTipText")
		};
	
	private static PropertyFilter[] PROPERTY_FILTERS = new PropertyFilter[]{
		new PropertyFilter(PROPERTY_NAME, ENABLED),
		new PropertyFilter(PROPERTY_ENTITY_FIELD_PREFERENCES, ENABLED),
		new PropertyFilter(PROPERTY_ENTITY_X, ENABLED),
		new PropertyFilter(PROPERTY_ENTITY_Y, ENABLED),
		new PropertyFilter(PROPERTY_ENTITY_MATRIX, ENABLED),
		new PropertyFilter(PROPERTY_ENTITY_MATRIX_VALUE_FIELD, ENABLED),
		new PropertyFilter(PROPERTY_ENTITY_MATRIX_PARENT_FIELD, ENABLED),
		new PropertyFilter(PROPERTY_ENTITY_MATRIX_X_REF_FIELD, ENABLED),
		new PropertyFilter(PROPERTY_ENTITY_MATRIX_NUMBER_STATE, ENABLED),
		new PropertyFilter(PROPERTY_CELL_INPUT_TYPE, ENABLED),
		
		new PropertyFilter(PROPERTY_ENTITY_MATRIX_REFERENCE_FIELD, ENABLED),

		new PropertyFilter(PROPERTY_ENTITY_X_VLP_ID, ENABLED),
		new PropertyFilter(PROPERTY_ENTITY_X_VLP_IDFIELDNAME, ENABLED),
		new PropertyFilter(PROPERTY_ENTITY_X_VLP_FIELDNAME, ENABLED),
		new PropertyFilter(PROPERTY_ENTITY_X_VLP_REFERENCE_PARAM_NAME, ENABLED),

		new PropertyFilter(PROPERTY_ENTITY_X_HEADER, ENABLED),
		new PropertyFilter(PROPERTY_ENTITY_Y_HEADER, ENABLED),

		new PropertyFilter(PROPERTY_ENTITY_X_SORTING_FIELDS, ENABLED),
		new PropertyFilter(PROPERTY_ENTITY_Y_SORTING_FIELDS, ENABLED),
		
		new PropertyFilter(PROPERTY_ENTITY_FIELD_CATEGORIE, ENABLED),
		new PropertyFilter(PROPERTY_ENTITY_FIELD_X, ENABLED),
		new PropertyFilter(PROPERTY_ENTITY_Y_PARENT_FIELD, ENABLED),
		new PropertyFilter(PROPERTY_ENTITY_FIELD_Y, ENABLED),
		new PropertyFilter(PROPERTY_PREFFEREDSIZE, ENABLED),
		new PropertyFilter(PROPERTY_ENABLED, ENABLED),
		new PropertyFilter(PROPERTY_BACKGROUNDCOLOR, ENABLED),
		new PropertyFilter(PROPERTY_BORDER, ENABLED),
		new PropertyFilter(PROPERTY_CONTROLLERTYPE, ENABLED),
		new PropertyFilter(PROPERTY_FONT, ENABLED),
		new PropertyFilter(PROPERTY_NEW_ENABLED, ENABLED),
		new PropertyFilter(PROPERTY_CLONE_ENABLED, ENABLED),
		new PropertyFilter(PROPERTY_DELETE_ENABLED, ENABLED),
		new PropertyFilter(PROPERTY_EDIT_ENABLED, ENABLED),
		new PropertyFilter(PROPERTY_DYNAMIC_CELL_HEIGHTS_DEFAULT, ENABLED)
		};
	
	public static final String[][] PROPERTIES_TO_LAYOUTML_ATTRIBUTES = new String[][]{
		{PROPERTY_NAME, ATTRIBUTE_NAME}, 
		{PROPERTY_ENTITY_FIELD_PREFERENCES, ATTRIBUTE_ENTITY_FIELD_MATRIX_PREFERENCES}, 
		{PROPERTY_ENTITY_X, ATTRIBUTE_ENTITY_X}, {PROPERTY_ENTITY_Y, ATTRIBUTE_ENTITY_Y}, 
		{PROPERTY_ENTITY_MATRIX, ATTRIBUTE_ENTITY_MATRIX}, 
		{PROPERTY_ENTITY_MATRIX_PARENT_FIELD, ATTRIBUTE_MATRIX_PARENT_FIELD}, 
		{PROPERTY_ENTITY_MATRIX_VALUE_FIELD, ATTRIBUTE_MATRIX_VALUE_FIELD}, 
		{PROPERTY_ENTITY_MATRIX_X_REF_FIELD, ATTRIBUTE_MATRIX_X_REF_FIELD}, 
		{PROPERTY_ENTITY_FIELD_CATEGORIE, ATTRIBUTE_ENTITY_FIELD_CATEGORIE}, 
		{PROPERTY_ENTITY_FIELD_X, ATTRIBUTE_ENTITY_FIELD_X},
		{PROPERTY_ENTITY_Y_PARENT_FIELD, ATTRIBUTE_ENTITY_Y_PARENT_FIELD},
		{PROPERTY_ENTITY_FIELD_Y, ATTRIBUTE_ENTITY_FIELD_Y},
		{PROPERTY_ENABLED, ATTRIBUTE_ENABLED},
		{PROPERTY_CONTROLLERTYPE, ATTRIBUTE_CONTROLLERTYPE}, 
		{PROPERTY_DYNAMIC_CELL_HEIGHTS_DEFAULT, ATTRIBUTE_DYNAMIC_CELL_HEIGHTS_DEFAULT}, 
		{PROPERTY_ENTITY_MATRIX_NUMBER_STATE, ATTRIBUTE_MATRIX_NUMBER_STATE}, 
		{PROPERTY_CELL_INPUT_TYPE, ATTRIBUTE_CELL_INPUT_TYPE},
		{PROPERTY_ENTITY_MATRIX_REFERENCE_FIELD, ATTRIBUTE_ENTITY_MATRIX_REFERENCE_FIELD},
		{PROPERTY_ENTITY_X_VLP_ID, ATTRIBUTE_ENTITY_X_VLP_ID},
		{PROPERTY_ENTITY_X_VLP_IDFIELDNAME, ATTRIBUTE_ENTITY_X_VLP_IDFIELDNAME},
		{PROPERTY_ENTITY_X_VLP_FIELDNAME, ATTRIBUTE_ENTITY_X_VLP_FIELDNAME},
		{PROPERTY_ENTITY_X_VLP_REFERENCE_PARAM_NAME, ATTRIBUTE_ENTITY_X_VLP_REFERENCE_PARAM_NAME},
		{PROPERTY_ENTITY_X_HEADER, ATTRIBUTE_ENTITY_X_HEADER},
		{PROPERTY_ENTITY_Y_HEADER, ATTRIBUTE_ENTITY_Y_HEADER},
		{PROPERTY_ENTITY_X_SORTING_FIELDS, ATTRIBUTE_ENTITY_X_SORTING_FIELDS},
		{PROPERTY_ENTITY_Y_SORTING_FIELDS, ATTRIBUTE_ENTITY_Y_SORTING_FIELDS}
	};

	public static final String[][] PROPERTY_VALUES_FROM_METAINFORMATION = new String[][]{{PROPERTY_ENTITY_X, WYSIWYGMetaInformation.META_ENTITY_NAMES},
			{PROPERTY_ENTITY_Y, WYSIWYGMetaInformation.META_ENTITY_NAMES}, {PROPERTY_ENTITY_MATRIX, WYSIWYGMetaInformation.META_ENTITY_NAMES},
			{PROPERTY_ENTITY_FIELD_PREFERENCES, WYSIWYGMetaInformation.META_ENTITY_FIELD_NAMES_REFERENCING},
			{PROPERTY_ENTITY_MATRIX_PARENT_FIELD, WYSIWYGMetaInformation.META_ENTITY_FIELD_NAMES_REFERENCING},
			{PROPERTY_ENTITY_MATRIX_VALUE_FIELD, WYSIWYGMetaInformation.META_ENTITY_FIELD_NAMES_REFERENCING},
			{PROPERTY_ENTITY_MATRIX_X_REF_FIELD, WYSIWYGMetaInformation.META_ENTITY_FIELD_NAMES_REFERENCING},
			{PROPERTY_ENTITY_FIELD_CATEGORIE, WYSIWYGMetaInformation.META_ENTITY_FIELD_NAMES_REFERENCING},
			{PROPERTY_ENTITY_FIELD_X, WYSIWYGMetaInformation.META_ENTITY_FIELD_NAMES_REFERENCING},
			{PROPERTY_ENTITY_Y_PARENT_FIELD, WYSIWYGMetaInformation.META_ENTITY_FIELD_NAMES_REFERENCING},
			{PROPERTY_ENTITY_FIELD_Y, WYSIWYGMetaInformation.META_ENTITY_FIELD_NAMES_REFERENCING}};
	
	public static final String[][] PROPERTY_VALUES_STATIC = new String[][]{{PROPERTY_CELL_INPUT_TYPE, ATTRIBUTE_CELL_INPUT_TYPE_TEXTFIELD,
			ATTRIBUTE_CELL_INPUT_TYPE_CHECKICON, ATTRIBUTE_CELL_INPUT_TYPE_COMBOBOX}};
	
	public static final String[][] PROPERTIES_TO_SCRIPT_ELEMENTS = new String[][]{{PROPERTY_NEW_ENABLED, ELEMENT_NEW_ENABLED}, 
		{PROPERTY_EDIT_ENABLED, ELEMENT_EDIT_ENABLED}, 
		{PROPERTY_DELETE_ENABLED, ELEMENT_DELETE_ENABLED}, 
		{PROPERTY_CLONE_ENABLED, ELEMENT_CLONE_ENABLED} };
	
	public WYSIWYGMatrix(WYSIWYGMetaInformation meta) {
		this.meta = meta;
		this.setLayout(new BorderLayout());
		matrix = new JMatrixComponent(true);
		prepareComponent(matrix);
		this.add(matrix, BorderLayout.CENTER);
		
		addDisplayMouseListenerLater();
	
	}

	private IPopupListener createMatrixPopupMenuMouseAdapter() {
		final IPopupListener popupMenuMouseAdapter = new PopupMenuMouseAdapter() {

			@Override
			public boolean doPopup(MouseEvent e, JPopupMenu menu) {
				final TableLayoutUtil tableLayoutUtil = WYSIWYGMatrix.this.getParentEditor().getCurrentTableLayoutUtil();
				final ComponentPopUp popup = new ComponentPopUp(tableLayoutUtil, e.getComponent(), e.getX());

				Point loc = tableLayoutUtil.getContainer().getMousePosition() == null ?
						null : tableLayoutUtil.getContainer().getMousePosition().getLocation();
				if (loc == null) {
					loc = (Point)e.getLocationOnScreen().clone();
					SwingUtilities.convertPointFromScreen(loc, tableLayoutUtil.getContainer());
				}
				popup.showComponentPropertiesPopup(loc);

				return true;
			}
		};
		return popupMenuMouseAdapter;
	}

	private void addDisplayMouseListenerLater() {
		SwingUtilities.invokeLater(() -> {
			if (this.getParent() == null) {
				return;
			}
			PropertiesDisplayMouseListener ml = new PropertiesDisplayMouseListener(WYSIWYGMatrix.this, getParentEditor().getTableLayoutUtil());
			if (matrix != null) {
				matrix.addMouseListener(ml);
				matrix.getTable().addMouseListener(ml);
				matrix.getHeader().addMouseListener(ml);
				matrix.getToolbar().addMouseListener(ml);
				matrix.setPopupMenuListener(createMatrixPopupMenuMouseAdapter());
				matrix.setLayout(true);
			}
		});
	}
	
	private void prepareComponent(JMatrixComponent comp) {
		comp.getTable().setEnabled(false);
	}
	
	@Override
	public String[][] getPropertyScriptElementLink() {
		return PROPERTIES_TO_SCRIPT_ELEMENTS;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.awt.Component#addMouseListener(java.awt.event.MouseListener)
	 */
	@Override
	public synchronized void addMouseListener(MouseListener l) {
		listenerList.add(MouseListener.class, l);
	}

	/*
	 * (non-Javadoc)
	 * @see java.awt.Component#removeMouseListener(java.awt.event.MouseListener)
	 */
	@Override
	public synchronized void removeMouseListener(MouseListener l) {
		listenerList.remove(MouseListener.class, l);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		for (MouseListener l : listenerList.getListeners(MouseListener.class)) {
			if(e.getSource() instanceof JTableHeader) {
				WYSIWYGMatrixColumn column = getColumnAtX(e.getX());
				if(column != null) {
					MouseListener[] listenerlist = ((Component) column).getMouseListeners();
					for(MouseListener listener : listenerlist) {
						if(listener instanceof PropertiesDisplayMouseListener) {
							listener.mouseClicked(e);
						}
					}
	
				}
			}
			else {
				l.mouseClicked(e);		
			}
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		for (MouseListener l : listenerList.getListeners(MouseListener.class)) {
			l.mousePressed(e);
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		for (MouseListener l : listenerList.getListeners(MouseListener.class)) {
			l.mouseReleased(e);
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		for (MouseListener l : listenerList.getListeners(MouseListener.class)) {
			l.mouseEntered(e);
		}
	}

	@Override
	public void mouseExited(MouseEvent e) {
		for (MouseListener l : listenerList.getListeners(MouseListener.class)) {
			l.mouseExited(e);
		}
	}
	

	@Override
	public ComponentProperties getProperties() {
		return this.properties;
	}

	@Override
	public void setProperty(String property, PropertyValue<?> value, Class<?> valueClass) throws CommonBusinessException {
		properties.setProperty(property, value, valueClass);
	}

	@Override
	public void setProperties(ComponentProperties properties) {
		this.properties = properties;
		this.setMatrixFromProperties();
	}

	@Override
	public String[] getPropertyNames() {
		return PROPERTY_NAMES;
	}

	@Override
	public PropertySetMethod[] getPropertySetMethods() {
		return PROPERTY_SETMETHODS;
	}

	@Override
	public PropertyClass[] getPropertyClasses() {
		return PROPERTY_CLASSES;
	}

	@Override
	public WYSIWYGLayoutEditorPanel getParentEditor() {
		if (super.getParent() instanceof TableLayoutPanel) {
			return (WYSIWYGLayoutEditorPanel) super.getParent().getParent();
		}

		throw new CommonFatalException(ERROR_MESSAGES.PARENT_NO_WYSIWYG);
	}

	@Override
	public String[][] getPropertyAttributeLink() {
		return PROPERTIES_TO_LAYOUTML_ATTRIBUTES;
	}

	@Override
	public List<JMenuItem> getAdditionalContextMenuItems(int xClick) {
		List<JMenuItem> list = new ArrayList<JMenuItem>();

		// own implementation

		return list;
	}

	@Override
	public String[][] getPropertyValuesFromMetaInformation() {
		return PROPERTY_VALUES_FROM_METAINFORMATION;
	}

	@Override
	public String[][] getPropertyValuesStatic() {
		return PROPERTY_VALUES_STATIC;
	}

	@Override
	public PropertyFilter[] getPropertyFilters() {
		return PROPERTY_FILTERS;
	}

	@Override
	public boolean isSelected() {
		return this.bSelected;
	}

	@Override
	public void setSelected(boolean bSelected) {
		this.bSelected = bSelected;
	}

	@Override
	public void validateProperties(Map<String, PropertyValue<Object>> values) throws NuclosBusinessException {
		// nothing to do here
	}

	@Override
	public LayoutMLRules getLayoutMLRulesIfCapable() {
		return this.matrixLayoutRules;
	}
	
	// Property setter and getter
	
	public void setEntityX(String sEntity) {
		this.sEntityX = sEntity;
	}
	
	public UID getEntityX() {
		return UID.parseUID(this.sEntityX);
	}
	
	public void setEntityY(String sEntity) {
		this.sEntityY = sEntity;
		this.setMatrixFromProperties();
	}
	
	public void setEntityYParentField(String sField) {
		this.sEntityYParentField = sField;
		this.setMatrixFromProperties();
	}
	
	public UID getEntityYParentField() {
		return UID.parseUID(this.sEntityYParentField);
	}
	
	public UID getEntityY() {
		return UID.parseUID(this.sEntityY);
	}
	
	public void setEntityMatrix(String sEntity) {
		this.sEntityMatrix = sEntity;
	}
	
	public UID getEntityMatrix() {
		return UID.parseUID(this.sEntityMatrix);
	}
	
	public void setEntityFieldCategorie(String sField) {
		this.sEntityFieldCategorie = sField;
	}
	
	public void setEntityFieldX(String sField) {
		this.sEntityFieldX = sField;
	}
	
	public void setPreferences(String sPreferencesField) {
		this.sEntityFieldPreferences = sPreferencesField;
	}
	
	public UID getPreferences() {
		return UID.parseUID(this.sEntityFieldPreferences);
	}
	
	public void setEntityFieldY(String sField) {
		this.sEntityFieldY = sField;
	}
	
	public void setEntityMatrixParentField(String sField) {
		this.sEntityMatrixParentField = sField;
	}
	
	public UID getEntityMatrixParentField() {
		return UID.parseUID(this.sEntityMatrixParentField);
	}
	
	public void setEntityMatrixXRefField(String field) {
		this.sEntityMatrixXRefField = field;
	}
	
	public UID getEntityMatrixXRefField() {
		return UID.parseUID(this.sEntityMatrixXRefField);
	}
	
	public void setEntityMatrixValueField(String field) {
		this.sEntityMatrixValueField = field;
	}
	
	public UID getEntityMatrixValueField() {
		return UID.parseUID(this.sEntityMatrixValueField);
	}
	
	/**
	 * 
	 * @return {@link CollectableComponentModelProvider}
	 */
	private CollectableComponentModelProvider getCollectableComponentModelProvider() {
		HashMap<UID, CollectableComponentModel> map = new HashMap<UID, CollectableComponentModel>();

		for (UID uid : meta.getCollectableEntity().getFieldUIDs()) {
			map.put(uid, CollectableComponentModel.newCollectableComponentModel(meta.getCollectableEntity().getEntityField(uid), false));
		}
		return new DefaultCollectableComponentModelProvider(map);
	}
	
	public WYSIWYGMatrixColumn getColumnAtX(int x) {
		if (matrix != null) {
			//NUCLEUSINT-355
			int posx = 0;

			for (int i = 0; i < matrix.getHeaderFixed().getColumnModel().getColumnCount(); i++) {
				TableColumn tableColumn = matrix.getHeaderFixed().getColumnModel().getColumn(i);
				if (posx < x && x < posx + tableColumn.getWidth()) {
					for (WYSIWYGMatrixColumn column : getColumns()) {
						String sCol = ((String) tableColumn.getIdentifier()).trim();
						String sCol1 = column.getValue().trim();						
						if (sCol1.equals(sCol)) {
							return column;
						}
					}
				}
				posx = posx + tableColumn.getWidth();
			}
		}
		return null;
	}
	
	/**
	 * Setup of the {@link WYSIWYGSubForm} does a complete render and refresh 
	 */
	public void setMatrixFromProperties() {
		if (matrix != null) {
			lastViewPosition = matrix.getTable().getVisibleRect();
			matrix.getTable().getTableHeader().removeMouseListener(this);
		}
		UID entity = null;
		UID foreignkey = null;

		entity = getEntityY(); 
		if (entity != null) {		
			final EntityMeta<?> entityMeta = MetaProvider.getInstance().getEntity(entity);
			if(getProperties() != null) {
				entity = entityMeta.getUID();
				foreignkey = getEntityYParentField(); 
			}
		}
		try {
			if (entity != null && foreignkey != null) {
				CollectableComponentModelProvider clctmodelprovider = getCollectableComponentModelProvider();

				this.matrix = new JMatrixComponent(true, entity);
				
				this.add(this.matrix, BorderLayout.CENTER);
				
				addDisplayMouseListenerLater();

				boolean bEnabled = (Boolean) getProperties().getProperty(PROPERTY_ENABLED).getValue(boolean.class, this);
				this.matrix.setEnabled(bEnabled);
				
				if(getProperties().getProperty(PROPERTY_CONTROLLERTYPE) != null) {
					final String sControllerType = (String) getProperties().getProperty(PROPERTY_CONTROLLERTYPE).getValue();
					matrix.setControllerType(sControllerType);
				}

				final Color background = (Color) getProperties().getProperty(PROPERTY_BACKGROUNDCOLOR).getValue(Color.class, this);
				matrix.setBackground(background);

				final Border border = (Border) getProperties().getProperty(PROPERTY_BORDER).getValue(Border.class, this);
				matrix.setBorder(border);
				matrix.setPopupMenuListener(createMatrixPopupMenuMouseAdapter());
								
				if (matrix.getTableFixed().getModel() instanceof MatrixTableModel) {
					this.model = (MatrixTableModel) matrix.getTableFixed().getModel();					
				} else {
					throw new NuclosFatalException("Unexpected table model type.");
				}
				
				for (int i = 0; i < model.getColumnCount(); i++) {
					List<Collectable> lstHeader = model.getCategorieHeader();
					MatrixCollectable mc = (MatrixCollectable)lstHeader.get(i);
					UID uidField = mc.getField();				
					if(uidField.equals(UID.UID_NULL)) {
						continue;
					}
					final CollectableEntityField field = NuclosCollectableEntityProvider.getInstance().getCollectableEntity(matrix.getEntityY()).getEntityField(uidField);
					final Map<UID, FieldMeta<?>> fields = MetaProvider.getInstance().getAllEntityFieldsByEntity(field.getEntityUID());
					//NUCLEUSINT-401 there is no need to setup the columns every time... this does reset the properties and destroys changes...
					if (columns.get(field.getUID()) == null) {
						boolean ignore = false;
						final SF<?> sf = SF.getByField(fields.get(field.getUID()).getFieldName()); 
						if (sf != null) {
							if (sf.equals(SF.CHANGEDAT)
									|| sf.equals(SF.CHANGEDBY)
									|| sf.equals(SF.CREATEDAT)
									|| sf.equals(SF.CREATEDBY)
									|| sf.equals(SF.LOGICALDELETED)
									|| sf.equals(SF.ORIGIN)
									|| sf.equals(SF.SYSTEMIDENTIFIER)
									|| sf.equals(SF.PROCESS)
									|| sf.equals(SF.STATE)
									|| sf.equals(SF.STATEICON)
									|| sf.equals(SF.STATENUMBER)) {
								ignore = true;
							}
						}
						if (ignore) {
							continue;
						}
												
						WYSIWYGMatrixColumn column = new WYSIWYGMatrixColumn(this, field);
						column.setProperties(PropertyUtils.getEmptyProperties(column, this.meta));
						
						column.getProperties().setProperty(WYSIWYGCollectableComponent.PROPERTY_UID,
								new PropertyValueString(field.getUID().getStringifiedDefinitionWithEntity(E.ENTITYFIELD)), String.class);
						
						PropertyValueString value = new PropertyValueString(fields.get(field.getUID()).getFieldName());
						try {
							column.getProperties().setProperty(WYSIWYGMatrixColumn.PROPERTY_NAME, value, null);
							column.getProperties().setProperty(WYSIWYGMatrixColumn.PROPERTY_DEFAULTVALUES, new PropertyValueBoolean(true), null);
						} catch (CommonBusinessException e) {
							Errors.getInstance().showExceptionDialog(null, e);
						}
						columns.put(field.getUID(), column);
					}
				}
				SwingUtilities.invokeLater(() -> {
					if (this.getParent() == null) {
						return;
					}
					TableLayoutUtil layoutUtil = getParentEditor().getTableLayoutUtil();

					for (Map.Entry<UID, WYSIWYGMatrixColumn> e : columns.entrySet()) {
						final WYSIWYGMatrixColumn wc = e.getValue();
						String label = (String) wc.getProperties().getProperty(WYSIWYGMatrixColumn.PROPERTY_LABEL).getValue();
						Integer columnWidth = (Integer) wc.getProperties().getProperty(WYSIWYGMatrixColumn.PROPERTY_COLUMNWIDTH).getValue();
						UID sNextFocusComponent = UID.parseUID((String) wc.getProperties().getProperty(AbstractWYSIWYGTableColumn.PROPERTY_NEXTFOCUSCOMPONENT).getValue());
						wc.addMouseListener(new PropertiesDisplayMouseListener(wc, layoutUtil));
					}
				});

				final JTable table = matrix.getTableFixed();

				for (MouseListener ml : table.getTableHeader().getMouseListeners()) {
					table.getTableHeader().removeMouseListener(ml);
				}
				table.getTableHeader().addMouseListener(this);
				matrix.getHeaderFixed().addMouseListener(this);			
				
				for (int i = 0; i < matrix.getToolbar().getComponentCount(); i++) {
					matrix.getToolbar().getComponent(i).setEnabled(false);
				}
				
				//NUCLEUSINT-926
				if (lastViewPosition != null) {
					table.scrollRectToVisible(lastViewPosition);
				}				

			//} else {
			//	this.matrix = null; // NUCLOS-3567 this leads to not add mouselisteners later
			}
		} catch (Exception e) {
			LOG.warn("setMatrixFromProperties failed: " + e, e);
			this.matrix = null;
		}
	}
	
	public List<WYSIWYGMatrixColumn> getColumnsInOrder() {
		List<WYSIWYGMatrixColumn> orderedColumns = new ArrayList<>(getColumns());
		Collections.sort(orderedColumns, (WYSIWYGMatrixColumn c1, WYSIWYGMatrixColumn c2) -> (c1.getRelativeOrder() - c2.getRelativeOrder()));
		return orderedColumns;
	}
	
	/**
	 * 
	 * @return a {@link Collection} of {@link WYSIWYGMatrixColumn}
	 */
	public Collection<WYSIWYGMatrixColumn> getColumns() {
		return columns.values();
	}
	
	/**
	 * 
	 * @param uid the uid of the column field
	 * @param column the {@link WYSIWYGMatrixColumn} for the name
	 */
	public void addColumn(UID uid, WYSIWYGMatrixColumn column) {
		column.setRelativeOrder(this.columns.size());
		this.columns.put(uid, column);
		// setSubFormFromProperties()
		// ... seems unnecessary . It is called from LayoutMLContentHandler once per column,
		// but setSubFormFromProperties() is also called afterwards in finalizeInitialLoading().
		// It doesn't contribute to the LayoutMLContentHandler's subformColumnMissing catch-clause,
		// because setSubFormFromProperties() handles all exception on its own.
	}
	
	public List<FieldMeta<?>> getColumnsWithValueListProvider() {
		final List<FieldMeta<?>> result = new ArrayList<FieldMeta<?>>();
		for (WYSIWYGMatrixColumn column : columns.values()) {
			WYSIWYGValuelistProvider value = (WYSIWYGValuelistProvider) column.getProperties().getProperty(PROPERTY_VALUELISTPROVIDER).getValue();
			if (value != null && value.getType() != null)
					result.add(MetaProvider.getInstance().getEntityField(column.getCollectableEntityField().getUID()));
		}
		
		if (result.size() == 0)
			return null;
		
		return result;
	}
	
	@Override
	public void setEnabled(boolean enabled) {
		// do nothing here
	}
	
	public Integer getNumberState() {
		return sNumberState;
	}

	public void setNumberState(Integer sNumberState) {
//		if(sNumberState != null && (sNumberState < 2 || sNumberState > 3))
//			throw new NuclosFatalException("nur 2 oder 3 erlaubt!");
		this.sNumberState = sNumberState;
	}

	public void setInputType(String inputType) {
		this.inputType = inputType;
	}
	
	public String getEntityMatrixReferenceField() {
		return entityMatrixReferenceField;
	}

	public void setEntityMatrixReferenceField(String entityMatrixReferenceField) {
		this.entityMatrixReferenceField = entityMatrixReferenceField;
	}

	public String getEntityXVlpId() {
		return entityXVlpId;
	}

	public void setEntityXVlpId(String entityXVlpId) {
		this.entityXVlpId = entityXVlpId;
	}

	public String getEntityXVlpIdFieldName() {
		return entityXVlpIdFieldName;
	}

	public void setEntityXVlpIdFieldName(String entityXVlpIdFieldName) {
		this.entityXVlpIdFieldName = entityXVlpIdFieldName;
	}

	public String getEntityXVlpFieldName() {
		return entityXVlpFieldName;
	}

	public void setEntityXVlpFieldName(String entityXVlpFieldName) {
		this.entityXVlpFieldName = entityXVlpFieldName;
	}

	public String getEntityXVlpReferenceParamName() {
		return entityXVlpReferenceParamName;
	}

	public void setEntityXVlpReferenceParamName(String entityXVlpReferenceParamName) {
		this.entityXVlpReferenceParamName = entityXVlpReferenceParamName;
	}

	public String getEntityXHeader() {
		return entityXHeader;
	}

	public void setEntityXHeader(String entityXHeader) {
		this.entityXHeader = entityXHeader;
	}

	public String getEntityYHeader() {
		return entityYHeader;
	}

	public void setEntityYHeader(String entityYHeader) {
		this.entityYHeader = entityYHeader;
	}
	
	public String getEntityXSortFields() {
		return entityXSortFields;
	}
	
	public void setEntityXSortFields(String entityXSortFields) {
		this.entityXSortFields = entityXSortFields;
	}
	
	public String getEntityYSortFields() {
		return entityYSortFields;
	}
	
	public void setEntityYSortFields(String entityYSortFields) {
		this.entityYSortFields = entityYSortFields;
	}
	
}
