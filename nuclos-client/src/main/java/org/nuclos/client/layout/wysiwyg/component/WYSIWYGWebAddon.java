//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.layout.wysiwyg.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.Border;

import org.nuclos.api.Property;
import org.nuclos.api.context.LayoutComponentContext;
import org.nuclos.api.ui.Alignment;
import org.nuclos.api.ui.AlignmentH;
import org.nuclos.api.ui.AlignmentV;
import org.nuclos.client.entityobject.EntityObjectDelegate;
import org.nuclos.client.layout.wysiwyg.WYSIWYGMetaInformation;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels.ERROR_MESSAGES;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels.PROPERTY_LABELS;
import org.nuclos.client.layout.wysiwyg.component.properties.ComponentProperties;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValue;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.WYSIWYGLayoutEditorPanel;
import org.nuclos.client.layout.wysiwyg.editor.util.DnDUtil;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.TableLayoutPanel;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.layoutmlrules.LayoutMLRules;
import org.nuclos.client.layout.wysiwyg.palette.WebAddonPaletteItem;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.common.E;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonPermissionException;

/**
 * 
 * 
 * 
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * 
 * @author <a href="mailto:maik.stueker@nuclos.de">Maik Stueker</a>
 * @version 01.00.00
 */
public class WYSIWYGWebAddon extends JPanel implements WYSIWYGComponent {

	public static final Alignment DEFAULT_ALIGNMENT = new Alignment(AlignmentH.FULL, AlignmentV.CENTER);

	public static final String PROPERTY_NAME = PROPERTY_LABELS.NAME;

	public static final String[][] PROPERTIES_TO_LAYOUTML_ATTRIBUTES = new String[][]{
		{PROPERTY_NAME, ATTRIBUTE_NAME}
	};

	/**
	 * &lt;!ELEMENT layoutcomponent
	 * ((%layoutconstraints;)?,property*,(%borders;),(%sizes;),font?,description?)&gt;
	 * <p>
	 * &lt;!ATTLIST layoutcomponent class CDATA #IMPLIED enabled (%boolean;) #IMPLIED
	 * editable (%boolean;) #IMPLIED &gt;
	 */
	private ComponentProperties properties;

	private final EntityObjectVO<UID> eoWebAddon;
	private final List<EntityObjectVO<UID>> webAddonPropertyList;

	private final JLabel jLabel;

	private LayoutComponentContext context;

	public WYSIWYGWebAddon(EntityObjectVO<UID> eoWebAddon, WYSIWYGMetaInformation metaInf) {
		super(new FlowLayout());
		this.eoWebAddon = eoWebAddon;
		if (this.eoWebAddon != null) {
			try {
				this.webAddonPropertyList = new ArrayList<>(EntityObjectDelegate.getInstance().getDependentEntityObjects(E.WEBADDON_PROPERTY.getUID(), E.WEBADDON_PROPERTY.webAddon.getUID(), eoWebAddon.getPrimaryKey()));
			} catch (CommonPermissionException cpe) {
				// Should never happen as super-user
				throw new NuclosFatalException(cpe);
			}
			Collections.sort(webAddonPropertyList, new Comparator<EntityObjectVO<UID>>() {
				@Override
				public int compare(final EntityObjectVO<UID> o1, final EntityObjectVO<UID> o2) {
					return StringUtils.compareIgnoreCase(o1.getFieldValue(E.WEBADDON_PROPERTY.property), o2.getFieldValue(E.WEBADDON_PROPERTY.property));
				}
			});
		} else {
			this.webAddonPropertyList = new ArrayList<>();
		}

		this.setBackground(Color.WHITE);
		this.jLabel = new JLabel("");
		this.jLabel.setIcon(WebAddonPaletteItem.getWebAddonIcon());
		if (this.eoWebAddon != null) {
			this.jLabel.setText(eoWebAddon.getFieldValue(E.WEBADDON.name));
			this.jLabel.setToolTipText(eoWebAddon.getFieldValue(E.WEBADDON.nuclet.getUID(), String.class));
		} else {
			this.jLabel.setText("NOT FOUND");
		}

		super.add(this.jLabel);
		DnDUtil.addDragGestureListener(this);
	}

	public String getId() {
		return eoWebAddon != null ?
				eoWebAddon.getPrimaryKey().getStringifiedDefinitionWithEntity(E.WEBADDON)
				: null;
	}

	public Font getDefaultFont() {
		return jLabel.getFont();
	}

	@Override
	public void setComponentPopupMenu(JPopupMenu popup) {
		super.setComponentPopupMenu(popup);
		setComponentPopupMenu(popup, jLabel);
	}

	private void setComponentPopupMenu(JPopupMenu popup, Component c) {
		if (c instanceof JComponent) {
			((JComponent) c).setComponentPopupMenu(popup);
		}
		if (c instanceof Container) {
			for (Component child : ((Container)c).getComponents()) {
				setComponentPopupMenu(popup, child);
			}
		}
	}

	@Override
	public synchronized void addMouseListener(MouseListener l) {
		super.addMouseListener(l);
		addMouseListener(l, jLabel);
	}

	private void addMouseListener(MouseListener l, Component c) {
		if (c instanceof JComponent) {
			((JComponent) c).addMouseListener(l);
		}
		if (c instanceof Container) {
			for (Component child : ((Container)c).getComponents()) {
				addMouseListener(l, child);
			}
		}
	}

	@Override
	public List<JMenuItem> getAdditionalContextMenuItems(int click) {
		return null;
	}

	@Override
	public LayoutMLRules getLayoutMLRulesIfCapable() {
		return null;
	}

	@Override
	public WYSIWYGLayoutEditorPanel getParentEditor() {
		if (this.getParent() instanceof TableLayoutPanel) {
			return (WYSIWYGLayoutEditorPanel) this.getParent().getParent();
		}

		throw new CommonFatalException(ERROR_MESSAGES.PARENT_NO_WYSIWYG);
	}

	@Override
	public ComponentProperties getProperties() {
		return properties;
	}

	@Override
	public String[][] getPropertyAttributeLink() {
		return PROPERTIES_TO_LAYOUTML_ATTRIBUTES;
	}

	public Property[] getAdditionalProperties() {
		List<Property> result = new ArrayList<>();
		for (EntityObjectVO<UID> eoProperty : webAddonPropertyList) {
			try {
				Class cls = Class.forName(eoProperty.getFieldValue(E.WEBADDON_PROPERTY.type));
				result.add(new Property(eoProperty.getFieldValue(E.WEBADDON_PROPERTY.property), cls));
			} catch (ClassNotFoundException e) {
				//Errors.getInstance().showExceptionDialog(this, e);
				result.add(new Property(eoProperty.getFieldValue(E.WEBADDON_PROPERTY.property), String.class));
			}
		}
		return result.toArray(new Property[]{});
	}

	@Override
	public PropertyClass[] getPropertyClasses() {
		List<PropertyClass> result = new ArrayList<PropertyClass>();
		result.add(new PropertyClass(PROPERTY_NAME, String.class));
		result.add(new PropertyClass(PROPERTY_BORDER, Border.class));
		result.add(new PropertyClass(PROPERTY_PREFFEREDSIZE, Dimension.class));

		for (EntityObjectVO<UID> eoProperty : webAddonPropertyList) {
			try {
				Class cls = Class.forName(eoProperty.getFieldValue(E.WEBADDON_PROPERTY.type));
				result.add(new PropertyClass(eoProperty.getFieldValue(E.WEBADDON_PROPERTY.property), cls));
			} catch (ClassNotFoundException e) {
				//Errors.getInstance().showExceptionDialog(this, e);
				result.add(new PropertyClass(eoProperty.getFieldValue(E.WEBADDON_PROPERTY.property), String.class));
			}
		}
		return result.toArray(new PropertyClass[]{});
	}



	@Override
	public String[] getPropertyNames() {
		List<String> result = new ArrayList<String>();
		result.add(PROPERTY_NAME);
		result.add(PROPERTY_BORDER);
		result.add(PROPERTY_PREFFEREDSIZE);

		for (EntityObjectVO<UID> eoProperty : webAddonPropertyList) {
			result.add(eoProperty.getFieldValue(E.WEBADDON_PROPERTY.property));
		}
		return result.toArray(new String[]{});
	}

	@Override
	public PropertySetMethod[] getPropertySetMethods() {
		List<PropertySetMethod> result = new ArrayList<PropertySetMethod>();
		result.add(new PropertySetMethod(PROPERTY_NAME, "setName"));
		result.add(new PropertySetMethod(PROPERTY_BORDER, "setBorder"));
		result.add(new PropertySetMethod(PROPERTY_PREFFEREDSIZE, "setPreferredSize"));

		return result.toArray(new PropertySetMethod[]{});
	}

	@Override
	public String[][] getPropertyValuesFromMetaInformation() {
		return null;
	}

	@Override
	public String[][] getPropertyValuesStatic() {
		return null;
	}

	@Override
	public void setProperties(ComponentProperties properties) {
		this.properties = properties;
	}

	@Override
	public void setProperty(String property, PropertyValue<?> value, Class<?> valueClass) throws CommonBusinessException {
		properties.setProperty(property, value, valueClass);
	}

	@Override
	public void validateProperties(Map<String, PropertyValue<Object>> values) throws NuclosBusinessException {};

	@Override
	public PropertyFilter[] getPropertyFilters() {
		List<PropertyFilter> result = new ArrayList<PropertyFilter>();
		result.add(new PropertyFilter(PROPERTY_NAME, ENABLED));
		result.add(new PropertyFilter(PROPERTY_BORDER, ENABLED));
		result.add(new PropertyFilter(PROPERTY_PREFFEREDSIZE, ENABLED));
		for (EntityObjectVO<UID> eoProperty : webAddonPropertyList) {
			result.add(new PropertyFilter(eoProperty.getFieldValue(E.WEBADDON_PROPERTY.property), ENABLED));
		}

		return result.toArray(new PropertyFilter[]{});
	}

	@Override
	public void setPreferredSize(Dimension preferredSize) {
	}
	
	@Override
	public void setBorder(Border border) {
		super.setBorder(border);
	}

	private boolean bSelected;

	public boolean isSelected() {
		return bSelected;
	}
	public void setSelected(boolean bSelected) {
		this.bSelected = bSelected;
	}
	
	/**
	 * This Method draws a small red box on the {@link WYSIWYGComponent} to indicate existing {@link LayoutMLRules}
	 */
	@Override
	public void paint(Graphics g) {
		super.paint(g);

		//Note: this i a replace for false, as this block is essentially commented out
		if (getAlignmentX() == 4.711) {
			UIUtils.fillRectangleForWysiwyg(g, getSize());
		}	
	}
	
}
