//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.layout.wysiwyg.editor.ui.panels.layoutmlrule;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.layout.wysiwyg.WYSIWYGLayoutControllingPanel;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels.LAYOUTML_RULE_EDITOR;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableCheckBox;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableComboBox;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableListOfValues;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableOptionGroup;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableTextArea;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableTextfield;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGSubFormColumn;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.WYSIWYGLayoutEditorPanel;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.elementalcomponents.SaveAndCancelButtonPanel;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.elementalcomponents.SaveAndCancelButtonPanel.SaveAndCancelButtonPanelControllable;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.layoutmlrule.event.RuleAttachEvent;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.layoutmlrule.event.RuleListEvent;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.layoutmlrule.event.RuleRemoveEvent;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.layoutmlrule.event.listener.RuleListListener;
import org.nuclos.client.layout.wysiwyg.editor.util.InterfaceGuidelines;
import org.nuclos.client.layout.wysiwyg.editor.util.LayoutMLRuleController;
import org.nuclos.client.layout.wysiwyg.editor.util.LayoutMLRuleEditorController;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.layoutmlrules.LayoutMLRule;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.layoutmlrules.LayoutMLRuleAction;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.layoutmlrules.LayoutMLRuleEventType;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.layoutmlrules.LayoutMLRules;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.ui.Errors;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common2.layoutml.LayoutMLConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import info.clearthought.layout.TableLayout;
import info.clearthought.layout.TableLayoutConstraints;

/**
 * This is the Main Editor for {@link LayoutMLRules}.<br>
 * It contains:
 * <ul>
 *	<li> one or more SingleRulePanels
 * 		<ul>
 * 			<li> with one {@link LayoutMLRuleEventPanel}</li>
 * 			<li> with one or more {@link LayoutMLRuleActionPanel}</li>
 * 		</ul>
 * 	</li>
 * </ul>
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * 
 * @author <a href="mailto:hartmut.beckschulze@novabit.de">hartmut.beckschulze</a>
 * @version 01.00.00
 */
public class LayoutMLRuleEditorDialog extends JDialog implements SaveAndCancelButtonPanelControllable, RuleListListener{

	private static final Logger LOG = LoggerFactory.getLogger(LayoutMLRuleEditorDialog.class);

	private WYSIWYGComponent ruleSourceComponent = null;

	/** the scrollpane for scrolling the rules */
	private JScrollPane scrollpane = null;

	private final JPanel ruleContainer;

	/** every rule is stored here */
	private final Vector<LayoutMLRuleSingleRulePanel> rulePanels = new Vector<LayoutMLRuleSingleRulePanel>();

	private final WYSIWYGLayoutEditorPanel editorPanel;

	public static int EXIT_CANCEL = -1;
	public static  int EXIT_SAVE = 0;

	private int exitStatus = EXIT_SAVE;

	private String controlType = null;

	private final LayoutMLRuleEditorController ctrl;
	
	public LayoutMLRuleEditorDialog(WYSIWYGComponent ruleSourceComponent, WYSIWYGLayoutEditorPanel editorPanel) {
		this.ctrl = new LayoutMLRuleEditorController(this);
		getController().addRuleListListener(this);
		try {
		this.ruleSourceComponent = ruleSourceComponent;
		this.editorPanel = editorPanel;

		ImageIcon controllerIcon = getController(editorPanel).getLayoutCollectController().getTab().getTabIcon();
		if(controllerIcon != null){
			setIconImage(controllerIcon.getImage());
		}

		new LayoutMLRuleValidationLayer();

		/** 
		 * Find out what kind of Component is the Source of the Rule
		 */
		controlType = getControlTypeForComponent(ruleSourceComponent);

		/**
		 * Getting the Component Name to display
		 */
		String componentName = getComponentNameForDisplay(ruleSourceComponent);
		JLabel lblRuleSourceComponent = new JLabel(LAYOUTML_RULE_EDITOR.LABEL_RULE_SOURCECOMPONENT);

		JTextField txtRuleSourceComponent = new JTextField(componentName);
		txtRuleSourceComponent.setPreferredSize(new Dimension(300, 20));
		txtRuleSourceComponent.setEnabled(false);

		ruleContainer = new JPanel();

		double[][] containerLayout = {{TableLayout.FILL}, {TableLayout.PREFERRED}};
		ruleContainer.setLayout(new TableLayout(containerLayout));
		scrollpane = new JScrollPane(ruleContainer);
		scrollpane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollpane.getVerticalScrollBar().setUnitIncrement(20);


		/**
		 * Setting up the "Core Editor Window"
		 */
		double[][] mainWindowLayout = {{InterfaceGuidelines.MARGIN_LEFT, TableLayout.FILL, InterfaceGuidelines.MARGIN_RIGHT}, {TableLayout.PREFERRED, TableLayout.FILL, TableLayout.PREFERRED}};
		setLayout(new TableLayout(mainWindowLayout));

		JPanel jpnSourceComponent = new JPanel();
		double[][] titleLayout = {{TableLayout.PREFERRED, InterfaceGuidelines.MARGIN_BETWEEN, TableLayout.PREFERRED}, {InterfaceGuidelines.MARGIN_TOP, TableLayout.FILL, InterfaceGuidelines.MARGIN_BOTTOM}};
		jpnSourceComponent.setLayout(new TableLayout(titleLayout));

		jpnSourceComponent.add(lblRuleSourceComponent, "0,1");
		jpnSourceComponent.add(txtRuleSourceComponent, "2,1");

		add(jpnSourceComponent, "1,0");

		add(scrollpane, "1,1");

		/**
		 * Restoring Rules if there are any, otherwise start with empty
		 */
		restoreRules(controlType);

		/**
		 * Creating the default Buttons for Adding/ Saving and Removing all Rules
		 */
		JButton clearRulesForComponent = new JButton(LAYOUTML_RULE_EDITOR.BUTTON_REMOVE_ALL_RULES_FOR_COMPONENT);
		clearRulesForComponent.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clearAllRulesForThisComponent();
			}
		});

		ArrayList<AbstractButton> additionalButtons = new ArrayList<AbstractButton>(1);
		additionalButtons.add(clearRulesForComponent);
		add(new SaveAndCancelButtonPanel(this.getBackground(), this, ruleSourceComponent, additionalButtons), new TableLayoutConstraints(0,2,2,2));

		final MainFrameTab tab = getController(editorPanel).getLayoutCollectController().getTab();

		int width = (int)(tab.getWidth() * 0.5);
		int height = (int)(tab.getHeight() * 0.9);

		//@see NUCLOS-839
		setBounds(0, 0, width, height);

		try {
			setLocationRelativeTo(this.editorPanel.getController().getLayoutCollectController().getTab());
		} catch (NuclosBusinessException e) {
			// ignore.
		}

		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);		
		setTitle(LAYOUTML_RULE_EDITOR.TITLE_LAYOUTML_RULE_EDITOR);
		setModal(true);

		setVisible(true);
		
		} finally {
			getController().removeRuleListListener(this);
		}
	}

	protected String getControlTypeForSourceComponent() {
		return this.controlType;
	}

	/**
	 * 
	 * @return the {@link JPanel} with all the {@link LayoutMLRuleSingleRulePanel} contained
	 */
	protected JPanel getRuleContainer() {
		return this.ruleContainer;
	}

	protected List<LayoutMLRuleSingleRulePanel> getAttachedRules() {
		final List<LayoutMLRuleSingleRulePanel> rules = new ArrayList<LayoutMLRuleSingleRulePanel>();
		final Component[] arrayRules = getRuleContainer().getComponents();
		for (int i = 0; i < arrayRules.length; i++) {
			assert arrayRules[i] instanceof LayoutMLRuleSingleRulePanel;
			rules.add((LayoutMLRuleSingleRulePanel)arrayRules[i]);
		}
		return Collections.unmodifiableList(rules);
	}

	/**
	 * 
	 * @return the {@link WYSIWYGLayoutEditorPanel} used for finding Components of particular types
	 */
	protected WYSIWYGLayoutEditorPanel getWYSIWYGLayoutEditorPanel() {
		return this.editorPanel;
	}

	/**
	 * @return The {@link JScrollPane} for adjusting the Viewport
	 */
	protected JScrollPane getScrollPane() {
		return this.scrollpane;
	}

	/**
	 * This method does the setup of the {@link LayoutMLRuleEditorDialog}.
	 * It restores already existing rules or creates a empty panel if there are none
	 * @param controlType
	 */
	void restoreRules(String controlType) {
		if (ruleSourceComponent.getLayoutMLRulesIfCapable().getSize() > 0) {
			/** there are rules, starting reconstruction... */
			expandLayout(getRuleContainer().getLayout(), ruleSourceComponent.getLayoutMLRulesIfCapable().getSize(), false);
			//int row = 0;
			final List<LayoutMLRule> rules = ruleSourceComponent.getLayoutMLRulesIfCapable().getRules();
			LOG.debug("restore {} LayoutMLRules", rules.size());
			for (LayoutMLRule singleRule : rules) {
				LayoutMLRuleSingleRulePanel restoredRulePanel;
				try {
					restoredRulePanel = new LayoutMLRuleSingleRulePanel(this, (LayoutMLRule)singleRule.clone(), controlType);
					//refactored using attachNextRule(rule)
					//ruleContainer.add(restoredRulePanel, "0," + row);
					getController().fireRuleListEvent(new RuleAttachEvent(restoredRulePanel));

					LOG.trace("restored {}: {} ", singleRule.getRuleName(), singleRule.toString());
				} catch (CloneNotSupportedException e1) { }
				//row = row + 2;
			}
		} else {
			/** no rules defined for component, just starting with a new panel */
			LayoutMLRuleSingleRulePanel initialPanel = new LayoutMLRuleSingleRulePanel(this, controlType);
			initialPanel.addRemoveRowsFromPanel.setDeleteButtonEnabled(false);
			// refactored using attachNextRule(rule)
			//ruleContainer.add(initialPanel, "0,0");
			getController().fireRuleListEvent(new RuleAttachEvent(initialPanel));
		}
	}

	protected void attachNextRule(final LayoutMLRuleSingleRulePanel rule) {
		final JPanel rc = getRuleContainer();
		final Integer row = (rc.getComponentCount() == 1) ? 2 : rc.getComponentCount() * 2;

		final String position = "0," + row;
		
		expandLayout(rc.getLayout(),1, false);
		
		// update button actions for remaining rules
		for(final LayoutMLRuleSingleRulePanel ar : getAttachedRules()) {
			ar.addRemoveRowsFromPanel.setAddButtonEnabled(false);
			ar.addRemoveRowsFromPanel.setDeleteButtonEnabled(true);
		}
		rule.addRemoveRowsFromPanel.setAddButtonEnabled(true);
		
		// activate delete rule button if there were already rules assigned
		rule.addRemoveRowsFromPanel.setDeleteButtonEnabled(!getAttachedRules().isEmpty());
		// attach new rule
		rc.add(rule, position);
		
		// reload
		rc.revalidate();

		SwingUtilities.invokeLater(new Runnable(){
			@Override
			public void run() {
				try {
					rule.scrollRectToVisible(rule.getBounds());
				}
				catch (Exception e) {
					LOG.error("addAnotherRule failed: " + e, e);
				}
			}
		});
		
		LOG.trace("attached rule {} on position {}", rule, position);
	}

	protected void removeRule(final LayoutMLRuleSingleRulePanel rule) {
		final JPanel rc = getRuleContainer();
		if (getAttachedRules().size() > 1) {
			TableLayout tableLayout = (TableLayout) rc.getLayout();
			TableLayoutConstraints constraint = tableLayout.getConstraints(rule);
			int row1 = constraint.row1;
			if (row1 == 0)
				row1 = 1;
			rc.remove(rule);
			tableLayout.deleteRow(row1 - 1);
			tableLayout.deleteRow(row1 - 1);
			// why is it called a second time?
			//tableLayout.deleteRow(row1 - 1);

		} 

		if (rc.getComponentCount() == 1) {
			rule.eventPanel.setLayoutMLRuleEventType(new LayoutMLRuleEventType());
			//getRuleSourceComponent().getLayoutMLRulesIfCapable().clearRulesForComponent();
			//restoreRules(getControlTypeForComponent(getRuleSourceComponent()));

			((LayoutMLRuleSingleRulePanel) rc.getComponent(0)).addRemoveRowsFromPanel.setDeleteButtonEnabled(false);
			((LayoutMLRuleSingleRulePanel) rc.getComponent(0)).addRemoveRowsFromPanel.setAddButtonEnabled(true);
		}

		rc.updateUI();
		LOG.debug("removed rule {} from panel {}", this.toString(), rule.toString());
	}

	/**
	 * This Method gets the Controltype for the {@link WYSIWYGComponent}
	 * 
	 * Its needed for Selecting the Fitting Combinations of Rules...
	 * @param ruleSourceComponent
	 * @return The String representation for the Component, e.g. 
	 * 		LayoutMLConstants.ATTRIBUTEVALUE_TEXTFIELD or 
	 * 		LayoutMLConstants.ELEMENT_SUBFORMCOLUMS 
	 */
	String getControlTypeForComponent(WYSIWYGComponent ruleSourceComponent) {
		String controlType = null;
		if (ruleSourceComponent instanceof WYSIWYGCollectableTextfield)
			controlType = LayoutMLConstants.ATTRIBUTEVALUE_TEXTFIELD;
		else if (ruleSourceComponent instanceof WYSIWYGCollectableTextArea)
			controlType = LayoutMLConstants.ATTRIBUTEVALUE_TEXTAREA;
		else if (ruleSourceComponent instanceof WYSIWYGCollectableComboBox)
			controlType = LayoutMLConstants.ATTRIBUTEVALUE_COMBOBOX;
		else if (ruleSourceComponent instanceof WYSIWYGCollectableOptionGroup)
			controlType = LayoutMLConstants.ATTRIBUTEVALUE_OPTIONGROUP;
		else if (ruleSourceComponent instanceof WYSIWYGCollectableCheckBox)
			controlType = LayoutMLConstants.ATTRIBUTEVALUE_CHECKBOX;
		else if (ruleSourceComponent instanceof WYSIWYGSubFormColumn)
			controlType = LayoutMLConstants.ELEMENT_SUBFORMCOLUMN;
		//NUCLEUSINT-341
		else if (ruleSourceComponent instanceof WYSIWYGCollectableListOfValues)
			controlType = LayoutMLConstants.ATTRIBUTEVALUE_LISTOFVALUES;

		return controlType;
	}

	/**
	 * This Method returns the Name of the Component, if its a {@link WYSIWYGSubFormColumn} its Entityname.SubformColumn
	 * @param ruleSourceComponent
	 * @return
	 */
	private String getComponentNameForDisplay(WYSIWYGComponent ruleSourceComponent) {
		String componentName = null;

		if (!(ruleSourceComponent instanceof WYSIWYGSubFormColumn)){
			componentName = ((Component) ruleSourceComponent).getName();
		} else {
			final String subformEntity = ((WYSIWYGSubFormColumn)ruleSourceComponent).getSubForm().getName();
			final String subformColumn = ((WYSIWYGSubFormColumn)ruleSourceComponent).getName();

			componentName = subformEntity + "." + subformColumn;			
		}
		return componentName;
	}

	/**
	 * 
	 * @return the {@link Vector} containing all {@link LayoutMLRuleSingleRulePanel}
	 */
	protected Vector<LayoutMLRuleSingleRulePanel> getRulePanels() {
		return this.rulePanels;
	}

	/**
	 * NUCLEUSINT-341
	 * @return the {@link WYSIWYGComponent} the Rule(s) are attached to
	 */
	public WYSIWYGComponent getRuleSourceComponent() {
		return ruleSourceComponent;
	}

	private void expandLayout(LayoutManager layout, int rowsToAdd, boolean hasMarginBottom) {
		TableLayout tableLayout = (TableLayout) layout;
		double[] rows = tableLayout.getRow();
		double[] newRows = new double[rows.length + (rowsToAdd * 2)];

		for (int i = 0; i < rows.length; i++) {
			newRows[i] = rows[i];
		}

		int startingValue = rows.length;
		if (hasMarginBottom)
			startingValue--;

		for (int i = startingValue; i < newRows.length - 1; i = i + 2) {
			newRows[i] = InterfaceGuidelines.MARGIN_BETWEEN;
			newRows[i + 1] = TableLayout.PREFERRED;
		}

		if (hasMarginBottom)
			newRows[newRows.length - 1] = rows[rows.length - 1];

		tableLayout.setRow(newRows);
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.editor.ui.panels.elementalcomponents.SaveAndCancelButtonPanel.SaveAndCancelButtonPanelControllable#performCancelAction()
	 */
	@Override
	public void performCancelAction() {
		/** no action to perform, everything is irrelevant */
		dispose();
		this.exitStatus = EXIT_CANCEL;
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.editor.ui.panels.elementalcomponents.SaveAndCancelButtonPanel.SaveAndCancelButtonPanelControllable#performSaveAction()
	 */
	@Override
	public void performSaveAction() {
		
		try {
			
			final LayoutMLRules rulesToAdd = new LayoutMLRules();
			for (final LayoutMLRuleSingleRulePanel singleRule : getAttachedRules()) {
				
				// In case of localization source field and target field must both be equally (non) localized;
				// otherwise a clean transfer is not ensured
				validateLocalization(singleRule.getLayoutMLRule());
				
				rulesToAdd.addRule(singleRule.getLayoutMLRule());
			}

			LayoutMLRuleValidationLayer.validateLayoutMLRules(rulesToAdd);
			LayoutMLRuleController.attachRulesToComponent(ruleSourceComponent, rulesToAdd);
			dispose();
		}
		catch(NuclosBusinessException e) {
			Errors.getInstance().showExceptionDialog(this, e);
		}
	}

	private void validateLocalization(LayoutMLRule mlRule) throws NuclosBusinessException {
		 for (LayoutMLRuleAction action : mlRule.getLayoutMLRuleActions().getSingleActions()) {
			 if (LayoutMLConstants.ELEMENT_TRANSFERLOOKEDUPVALUE.equals(action.getRuleAction())) {
				 FieldMeta targetMeta = MetaProvider.getInstance().getEntityField(action.getTargetComponent());
				 FieldMeta sourceMeta = MetaProvider.getInstance().getEntityField(action.getSourceField());
				 if (!(targetMeta.isLocalized().equals(sourceMeta.isLocalized()))) {
					 throw new NuclosBusinessException("Cannot create Layout-Rule. Target and Source must both be equally (non) localized");
				 }
			 }
		 }
	}

	/**
	 * Removing all Rules for the {@link #ruleSourceComponent}
	 */
	protected void clearAllRulesForThisComponent() {
		int value = JOptionPane.showConfirmDialog(this, LAYOUTML_RULE_EDITOR.MESSAGE_DIALOG_SURE_TO_DELETE_ALL_RULES, LAYOUTML_RULE_EDITOR.TITLE_DIALOG_SURE_TO_DELETE_ALL_RULES, JOptionPane.YES_NO_OPTION);

		if (value == JOptionPane.YES_OPTION) {
			LayoutMLRuleController.clearLayoutMLRulesForComponent(ruleSourceComponent);
			dispose();
		}
	}

	/**
	 * @return the Status the Editor was closed with is either {@link LayoutMLRuleEditorDialog#EXIT_SAVE} (default) or {@link LayoutMLRuleEditorDialog#EXIT_CANCEL}
	 */
	public int getExitStatus() {
		return this.exitStatus;
	}

	private static WYSIWYGLayoutControllingPanel getController(WYSIWYGLayoutEditorPanel editorPanel) {
		try {
			return editorPanel.getController();
		}
		catch(NuclosBusinessException e) {
			throw new NuclosFatalException(e);
		}
	}

	@Override
	public void ruleListEvent(RuleListEvent event) {
			if (event instanceof RuleAttachEvent) {
			attachNextRule(((RuleAttachEvent) event).getRule());
		} else if (event instanceof RuleRemoveEvent) {
			removeRule(((RuleRemoveEvent) event).getRule());
		}
	}
	
	public LayoutMLRuleEditorController getController() {
		return this.ctrl;
	}

}
