package org.nuclos.client.layout.wysiwyg.editor.ui.panels.layoutmlrule.event.listener;

import java.util.EventListener;

import org.nuclos.client.layout.wysiwyg.editor.ui.panels.layoutmlrule.event.RuleListEvent;

/**
 * Rule List Listener
 * 
 *  listen to events for rule list
 * 
 * @author Moritz Neuhaeuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public interface RuleListListener extends EventListener {

	/**
	 * rule list event occured
	 * 
	 * @param event	{@link RuleListEvent} event
	 */
	public void ruleListEvent(final RuleListEvent event);
}
