package org.nuclos.client.main.mainframe.workspace;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;

import org.nuclos.client.ui.BackgroundPanel;

public class MaintenanceModePanel extends BackgroundPanel {

	private static final long serialVersionUID = 1L;
	private final JPanel pnlLogin = new JPanel();
	private final JProgressBar progressbar = new JProgressBar();
	private final JTextArea descriptionLabel = new JTextArea();
	private final JLabel userStatusLabel = new JLabel();
	private final JLabel userStatusValue = new JLabel();
	private final JLabel jobStatusLabel = new JLabel();
	private final JLabel jobStatusValue = new JLabel();

	public MaintenanceModePanel() {
		this.add(pnlLogin, BorderLayout.CENTER);

		pnlLogin.setLayout(new GridBagLayout());
		pnlLogin.setOpaque(false);
		pnlLogin.setBorder(BorderFactory.createEmptyBorder(1, 10, 1, 10));

		descriptionLabel.setText("Der Wartungsmodus ist aktiv, sobald alle laufenden Jobs beendet sind\n"
				+ "und keine weiteren Benutzer mehr eingeloggt sind. Sollte der Wechsel\n"
				+ "nicht stattfinden, brechen Sie ab und probieren Sie es nochmals!");
		descriptionLabel.setBackground(pnlLogin.getBackground());
		descriptionLabel.setEditable(false);

		pnlLogin.add(descriptionLabel, new GridBagConstraints(0, 1, 3, 1, 0.0,
				0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 10, 0), 0,
				0));

		pnlLogin.add(userStatusLabel, new GridBagConstraints(0, 2, 2, 1, 0.0,
				0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 10, 0), 0,
				0));

		pnlLogin.add(userStatusValue, new GridBagConstraints(2, 2, 2, 1, 0.0,
				0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 10, 0), 0,
				0));

		pnlLogin.add(jobStatusLabel, new GridBagConstraints(0, 3, 2, 1, 0.0,
				0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 10, 0), 0,
				0));

		pnlLogin.add(jobStatusValue, new GridBagConstraints(2, 3, 2, 1, 0.0,
				0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 10, 0), 0,
				0));

		pnlLogin.add(progressbar, new GridBagConstraints(0, 4, 3, 1, 0.0,
				0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 10, 0), 0,
				0));

		userStatusLabel.setText("Angemeldete Benutzer: ");

		jobStatusLabel.setText("Laufende Jobs: ");
	}

	public void setProgressbarValue(int value) {
		this.progressbar.setValue(value);
	}
	public void setUserStatusText(String value) {
		this.userStatusValue.setText(value);
	}
	public void setJobStatusText(String value) {
		this.jobStatusValue.setText(value);
	}

}
