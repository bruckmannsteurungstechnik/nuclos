//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata;

import java.text.ParseException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.nuclos.client.i18n.language.data.DataLanguageContext;
import org.nuclos.common.E;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.AbstractCollectableField;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldFormat;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.masterdata.CollectableMasterDataForeignKeyEntityField;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.i18n.language.data.DataLanguageLocalizedEntityEntry;
import org.nuclos.server.i18n.language.data.DataLanguageUtils;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;


/**
 * A <code>CollectableField</code> initialized with the contents of a <code>MasterDataVO</code> field.
 * This class is immutable.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */

public class CollectableMasterDataField extends AbstractCollectableField {
	
	private final Object oValue;
	private final Object oValueId;
	private final int iFieldType;
	
	private final CollectableMasterData<?> clctmd;
	private final UID fieldUid; 
	
	private UID userDataLangUID = DataLanguageContext.getDataUserLanguage();
	private UID datalangUID = DataLanguageContext.getDataSystemLanguage();
	
	/**
	 * §precondition clctmd != null
	 * §precondition sFieldName != null
	 */
	CollectableMasterDataField(CollectableMasterData<?> clctmd, UID fieldUid) {
		if (clctmd == null) {
			throw new NullArgumentException("clctmd");
		}
		if (fieldUid == null) {
			throw new NullArgumentException("fieldUid");
		}
		final MasterDataVO<?> mdvo = clctmd.getMasterDataCVO();

		if (fieldUid.equals(E.ENTITYFIELD.valuedefault.getUID()) && mdvo.getFieldValue(fieldUid) != null) {
			try {
				this.iFieldType = CollectableField.TYPE_VALUEFIELD;
				this.oValue = CollectableFieldFormat.getInstance(Class.forName(
						mdvo.getFieldValue(E.ENTITYFIELD.datatype).toString())).parse(null, mdvo.getFieldValue(fieldUid).toString());
				this.oValueId = null;
			}
			catch(CollectableFieldFormatException e) {
				throw new NuclosFatalException(
						SpringLocaleDelegate.getInstance().getMessage(
								"CollectableMasterDataField.1", 
								"Der Datentyp des Standardwerts der Entit\u00e4t {0} entspricht nicht dem Datentyp der Entit\u00e4t.", 
								clctmd),e);
			}
			catch(ClassNotFoundException e) {
				throw new NuclosFatalException(e);
			}
		}
		else {
			final CollectableEntityField clctef = clctmd.getCollectableEntity().getEntityField(fieldUid);
			this.iFieldType = clctef.getFieldType();
			this.oValue = getValue(fieldUid, mdvo, clctef);
			this.oValueId = clctef.isIdField() ? getValueId(fieldUid, mdvo) : null;
		}
		this.clctmd = clctmd;
		this.fieldUid = fieldUid;
	}

	public CollectableMasterData getCollectableMasterData() {
		return this.clctmd;
	}
	
	@Override
	public int getFieldType() {
		return this.iFieldType;
	}

	@Override
	public Object getValue() {
		return this.oValue;
	}

	@Override
	public Object getValueId() throws UnsupportedOperationException {
		if (!this.isIdField()) {
			throw new UnsupportedOperationException("getValueId");
		}
		return this.oValueId;
	}

	private Object getValue(UID fieldUID, MasterDataVO<?> mdvo, CollectableEntityField clctef) {
		Object result = null;
		// workaround: The server returns java.sql.Timestamp for Dates. This is problematic when
		// comparing dates. Timestamps can only be compared to timestamps.
		/** @todo introduce data type "java.sql.Timestamp" and do this conversion on the server side! */
		if (result != null && clctef.getJavaClass().equals(Date.class)) {
			try {
				if (result instanceof String) {
					result = SpringLocaleDelegate.getInstance().getDateFormat().parse((String)result);
				}
				else {
					Date date = (Date)result;
					result = new Date(date.getTime());
				}
			}
			catch(ParseException ex) {
				throw new CommonFatalException(ex);
			}
		} else {
			if (clctef.isLocalized() && !clctef.isCalculated()) {
				result = getLocalizedValue(fieldUID, mdvo);
			} else {
				if (clctef instanceof CollectableMasterDataForeignKeyEntityField) {
					CollectableMasterDataForeignKeyEntityField mdforKey = (CollectableMasterDataForeignKeyEntityField) clctef;
					UID refFieldUID = DataLanguageUtils.extractForeignEntityReference(mdforKey.getReferencedEntityUID());
					if (mdvo.getFieldValue(refFieldUID) != null) {
						result = mdvo.getFieldValue(refFieldUID);
					} else {
						result = mdvo.getFieldValue(fieldUID);								
					}
				} else {
					result = mdvo.getFieldValue(fieldUID);										
				}
			}
		}
		return result;
	}

	private Object getLocalizedValue(UID fieldUID, MasterDataVO<?> mdvo) {
		Object retVal = null;
		
		if (this.userDataLangUID == null || this.userDataLangUID.equals(this.datalangUID)) {
			retVal = mdvo.getFieldValue(fieldUID) != null ? mdvo.getFieldValue(fieldUID) : null;
		} else {
			if (mdvo.getDataLanguageMap() == null) {
				retVal = mdvo.getFieldValue(fieldUID) != null ? mdvo.getFieldValue(fieldUID) : null;	
			} else {
				DataLanguageLocalizedEntityEntry fieldLanguageData = 
						mdvo.getDataLanguageMap().getDataLanguage(this.userDataLangUID);
				if (fieldLanguageData == null) {
					retVal = mdvo.getFieldValue(fieldUID) != null ? mdvo.getFieldValue(fieldUID) : null;				
				} else {
					retVal = fieldLanguageData.getFieldValue(
							DataLanguageUtils.extractFieldUID(fieldUID));
				}
			}
		}
		
		return retVal;
	}

	private static Object getValueId(UID fieldUid, MasterDataVO<?> mdvo) {
		return mdvo.getFieldId(fieldUid) != null ? mdvo.getFieldId(fieldUid) : mdvo.getFieldUid(fieldUid);
	}
	
	@Override
	public String toString() {
		final Object oValue = this.getValue();
		final String sOutputFormat = clctmd.getCollectableEntity().getEntityField(fieldUid).getFormatOutput();
		return (oValue == null) ? "" : CollectableFieldFormat.getInstance(oValue.getClass()).format(sOutputFormat, oValue);		
	}

	@Override
	public String toDescription() {
		final ToStringBuilder b = new ToStringBuilder(this).append(iFieldType).append(fieldUid).append(oValueId).append(oValue);
		return b.toString();
	}

}	// class CollectableMasterDataField
