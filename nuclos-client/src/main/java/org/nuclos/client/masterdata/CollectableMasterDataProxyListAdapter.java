//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata;

import org.nuclos.client.ui.collect.CollectableProxyListAdapter;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;
import org.nuclos.server.genericobject.ProxyList;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Makes a <code>List&lt;MasterDataVO&gt;</code> look like a <code>List&lt;Collectable&gt;</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Uwe.Allner@novabit.de">Uwe.Allner</a>
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph Radig</a>
 * @version 01.00.00
 */
public class CollectableMasterDataProxyListAdapter<PK>
		extends CollectableProxyListAdapter<PK,MasterDataVO<PK>, CollectableMasterDataWithDependants<PK>> {

	/**
	 * the entity of the contained master data elements.
	 */
	private CollectableMasterDataEntity clcte;

	public CollectableMasterDataProxyListAdapter(ProxyList<PK,MasterDataVO<PK>> lstAdaptee, CollectableMasterDataEntity clcte) {
		super(lstAdaptee);
		this.clcte = clcte;
	}

	@Override
	protected ProxyList<PK,MasterDataVO<PK>> adaptee() {
		return (ProxyList<PK,MasterDataVO<PK>>) super.adaptee();
	}

	@Override
	protected CollectableMasterDataWithDependants<PK> makeCollectable(MasterDataVO<PK> mdvo) {
		return new CollectableMasterDataWithDependants<PK>(clcte, mdvo);
	}

	@Override
	protected MasterDataVO<PK> extractAdaptee(CollectableMasterDataWithDependants<PK> clctmd) {
		return clctmd.getMasterDataWithDependantsCVO();
	}

	@Override
	public int getLastIndexRead() {
		return this.adaptee().getLastIndexRead();
	}

	@Override
	public void fetchDataIfNecessary(int iIndex, ProxyList.IChangeListener changelistener) {
		this.adaptee().fetchDataIfNecessary(iIndex, changelistener);
	}

	@Override
	public boolean hasObjectBeenReadForIndex(int index) {
		return this.adaptee().hasObjectBeenReadForIndex(index);
	}

	@Override
	public int getIndexById(Object oId) {
		return this.adaptee().getIndexById((PK) oId);
	}
	
	@Override
	public boolean isOnlySequentialPaging() {
		return this.adaptee().isOnlySequentialPaging();
	}

}	// class CollectableMasterDataProxyListAdapter
