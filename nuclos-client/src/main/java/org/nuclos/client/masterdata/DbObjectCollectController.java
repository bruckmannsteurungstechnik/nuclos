//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JLabel;

import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.CollectStateEvent;
import org.nuclos.client.ui.collect.CollectStateListener;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModel;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelAdapter;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelEvent;
import org.nuclos.common.E;
import org.nuclos.common.NucletConstants;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.validation.FieldValidationError;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * <code>CollectController</code> for entity "Nuclet".
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Maik.Stueker@novabit.de">Maik Stueker</a>
 * @version 01.00.00
 */
public class DbObjectCollectController extends MasterDataCollectController<UID> {
		
	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton} 
	 * to get an instance.
	 * 
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	public DbObjectCollectController(MainFrameTab tabIfAny) {
		super(E.DBOBJECT.getUID(), tabIfAny, null);
		this.getCollectStateModel().addCollectStateListener(new CollectStateListener() {
			
			@Override
			public void searchModeLeft(CollectStateEvent ev)
			    throws CommonBusinessException {
			}
			
			@Override
			public void searchModeEntered(CollectStateEvent ev)
			    throws CommonBusinessException {
			}
			
			@Override
			public void resultModeLeft(CollectStateEvent ev)
			    throws CommonBusinessException {
			}
			
			@Override
			public void resultModeEntered(CollectStateEvent ev)
			    throws CommonBusinessException {				
			}
			
			@Override
			public void detailsModeLeft(CollectStateEvent ev)
			    throws CommonBusinessException {
			}
			
			@Override
			public void detailsModeEntered(CollectStateEvent ev)
			    throws CommonBusinessException {
				if (ev.getNewCollectState().isDetailsModeNew()) {
					setCollectableFieldEnable(true);
				} else {
					setCollectableFieldEnable(false);
				}
			}
		});
	}
	
	@Override
	public void init() {
		super.init();
		getDetailsComponentModel(E.DBOBJECT.dbobjecttype.getUID()).addCollectableComponentModelListener(null, new CollectableComponentModelAdapter() {
			@Override
			public void collectableFieldChangedInModel(CollectableComponentModelEvent ev) {
				CollectableComponentModel model = ev.getCollectableComponentModel();
				CollectableField field = model.getField();
				boolean disableOrder = "index".equals(field.getValue());
				for (CollectableComponent cc : getDetailCollectableComponentsFor(E.DBOBJECT.order.getUID())) {
					cc.setEnabled(!disableOrder);
					if (disableOrder) {
						cc.setField(new CollectableValueField(null));
					}
				}
			}
		});
	}
	
	private void setCollectableFieldEnable(boolean enable) {
		getDetailCollectableComponentsFor(E.DBOBJECT.name.getUID()).get(0).setEnabled(enable);
		getDetailCollectableComponentsFor(E.DBOBJECT.dbobjecttype.getUID()).get(0).setEnabled(enable);
	}
	
	/**
	 * build prefix for database object, DEF_ if no nuclet is assigned
	 * 
	 * example:
	 * PREF_
	 * 
	 * @param clct	{@link CollectableMasterDataWithDependants}
	 * @return
	 * @throws NuclosBusinessException
	 */
	private final String buildDatabaseObjectPrefix(CollectableMasterDataWithDependants<UID> clct) throws NuclosBusinessException {
		final CollectableField clctefNuclet = clct.getField(E.DBOBJECT.nuclet.getUID());
		final UID uidNuclet;
		if (clctefNuclet != null) {
			uidNuclet = (UID) clctefNuclet.getValueId();
		} else {
			uidNuclet = null;
		}
		String localIdentifier = NucletConstants.DEFAULT_LOCALIDENTIFIER;
		if (null != uidNuclet) {
			// nuclet set
			try {
				final MasterDataVO<UID> voNuclet = MasterDataDelegate.getInstance().get(E.NUCLET.getUID(), uidNuclet);
				localIdentifier = voNuclet.getFieldValue(E.NUCLET.localidentifier);
			} catch (CommonFinderException e) {
				throw new NuclosBusinessException(e.getMessage());
			} catch (CommonPermissionException e) {
				throw new NuclosBusinessException(e.getMessage());
			}
		}
		return localIdentifier + "_";
	}
	
	@Override
	protected void unsafeFillDetailsPanel(
			CollectableMasterDataWithDependants<UID> clct)
			throws NuclosBusinessException {
		super.unsafeFillDetailsPanel(clct);
		// find local identifier prefix
		final JComponent comp = UIUtils.findJComponent(getDetailsPanel(), "org.nuclos.nuclet.localidentifier");
		String prefix = buildDatabaseObjectPrefix(clct);
		if (null != comp && comp instanceof JLabel) {
			((JLabel)comp).setText(prefix);
		}
		
		final String dbObjName = (String) getDetailsPanel().getLayoutRoot().getCollectableComponentModelFor(E.DBOBJECT.name.getUID()).getField().getValue();
		if (null != dbObjName) {
			// find prefix in dbobj name
			if (dbObjName.startsWith(prefix)) {
				getDetailsPanel().getLayoutRoot().getCollectableComponentModelFor(E.DBOBJECT.name.getUID()).setField(new CollectableValueField(dbObjName.substring(prefix.length())));
				
			}
		}
		
	}

	@Override
	protected CollectableMasterDataWithDependants<UID> insertCollectable(
			CollectableMasterDataWithDependants<UID> clctNew)
					throws CommonBusinessException {

		updatePrefix(clctNew);

		return super.insertCollectable(clctNew);
	}

	@Override
	protected CollectableMasterDataWithDependants<UID> updateCollectable(CollectableMasterDataWithDependants<UID> clct,
			Object oAdditionalData, Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {

		updatePrefix(clct);

		return super.updateCollectable(clct, oAdditionalData, applyMultiEditContext);
	}

	private void updatePrefix(CollectableMasterDataWithDependants<UID> clctNew) throws CommonValidationException, NuclosBusinessException {
		final String dbObjName = (String) clctNew.getField(E.DBOBJECT.name.getUID()).getValue();
		// extend the name with the local identifier prefix ( MY_FUNC => DEF_MY_FUNC)
		String prefix = buildDatabaseObjectPrefix(clctNew);
		String newName = prefix + dbObjName;
		if (prefix.startsWith(NucletConstants.DEFAULT_LOCALIDENTIFIER)) {
			// Ist noch keinem Nuclet zugewiesen... (Localidentifier = DEF)
			// Dann darf die Laenge des Namens nicht 29 Zeichen ueberschreiten, da ein Nuclet 4 Zeichen als Localidentifier hat.
			if (newName.length() > 29) {
				String error = StringUtils.getParameterizedExceptionMessage("CollectableUtils.7", newName,
						E.DBOBJECT.name.getLocaleResourceIdForLabel());
				FieldValidationError validationError = new FieldValidationError(E.DBOBJECT.getUID(),
						E.DBOBJECT.name.getUID(), error, FieldValidationError.ValidationErrorType.FIELD_DIMENSION_ERROR);
				throw new CommonValidationException(new HashSet<String>(), Collections.singleton(validationError));
			}
		}
		clctNew.setField(E.DBOBJECT.name.getUID(), new CollectableValueField(newName));
	}
}
