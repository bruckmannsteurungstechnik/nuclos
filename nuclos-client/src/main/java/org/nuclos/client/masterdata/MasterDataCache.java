//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.log4j.Logger;
import org.nuclos.client.common.Utils;
import org.nuclos.client.entityobject.EntityFacadeDelegate;
import org.nuclos.client.i18n.language.data.DataLanguageContext;
import org.nuclos.client.jms.TopicNotificationReceiver;
import org.nuclos.client.startup.AbstractLocalUserCache;
import org.nuclos.common.E;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.DefaultCollectableEntityProvider;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.masterdata.ejb3.CollectableFieldsByNameParams;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

/**
 * Caches whole contents from master data entities. It is not used for data dependant on a foreign key.
 * Retrieves notifications about changes from the server (singleton).
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * §todo the caller has to decide whether an entity is cacheable or not. This is bad.
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
@ManagedResource(
		objectName="nuclos.client.cache:name=MasterDataCache",
		description="Nuclos Client MasterDataCache",
		currencyTimeLimit=15,
		log=false)
public class MasterDataCache extends AbstractLocalUserCache {
	
	private static final Logger LOG = Logger.getLogger(MasterDataCache.class);

	private static MasterDataCache INSTANCE;

	/**
	 * maps an entity name to the contents of the entity.
	 */
	private final Map<UID, List<MasterDataVO<?>>> mp 
		= new ConcurrentHashMap<UID, List<MasterDataVO<?>>>();

	private final Map<CollectableFieldsByNameKey, List<CollectableField>> mpCollectableFieldsByName 
		= new ConcurrentHashMap<MasterDataCache.CollectableFieldsByNameKey, List<CollectableField>>();
	
	/** 
	 * cache for "exist" queries
	 */
	private final Map<UID, ConcurrentMap<Object, Boolean>> mpPrimaryKey 
		= new ConcurrentHashMap<UID, ConcurrentMap<Object, Boolean>>();
	
	private transient TopicNotificationReceiver tnr;
	private transient MessageListener messageListener;
	
	private transient MasterDataDelegate masterDataDelegate;
	
	private transient EntityFacadeDelegate entityFacadeDelegate;


	public static MasterDataCache getInstance() {
		if (INSTANCE == null) {
			throw new IllegalStateException("too early");
		}
		return INSTANCE;
	}

	/**
	 * @deprecated Must be protected for Spring JMX - but never invoke this constructor.
	 */
	protected MasterDataCache() {
		INSTANCE = this;
	}
	
	public final void afterPropertiesSet() {
		// Constructor might not be called - as this instance might be deserialized (tp)
		if (INSTANCE == null) {
			INSTANCE = this;
		}
		if (!wasDeserialized() || !isValid())
			invalidate(null);
		messageListener = new MessageListener() {
			@Override
			public void onMessage(Message msg) {
				UID entityUid;
				if (msg instanceof ObjectMessage) {
					try {
						entityUid = (UID) ((ObjectMessage) msg).getObject();
						LOG.info("onMessage: JMS message is of type TextMessage, text is: " + entityUid);
					}
					catch (Exception ex) {
						LOG.warn("onMessage: Exception thrown in JMS message listener.", ex);
						entityUid = null;
					}
				}
				else {
					LOG.warn("onMessage: Message of type " + msg.getClass().getName() + " received, while a ObjectMessage was expected.");
					entityUid = null;
				}

				MasterDataCache.this.invalidate(entityUid);
			}
		};
		tnr.subscribe(getCachingTopic(), messageListener);
	}
	
	public final void setTopicNotificationReceiver(TopicNotificationReceiver tnr) {
		this.tnr = tnr;
	}
	
	public final void setMasterDataDelegate(MasterDataDelegate masterDataDelegate) {
		this.masterDataDelegate = masterDataDelegate;
	}
	
	public final void setEntityFacadeDelegate(EntityFacadeDelegate entityFacadeDelegate) {
		this.entityFacadeDelegate = entityFacadeDelegate;
	}
	
	@Override
	public String getCachingTopic() {
		return JMSConstants.TOPICNAME_MASTERDATACACHE;
	}

	// NUCLOS-6825 Data Entities will not be cached.
	private boolean checkAndWarnIfNonSystemEntity(UID entityUid) {
		if (!E.isNuclosEntity(entityUid)) {
			LOG.warn("It has been tried to fetch the data for non-system-entity (" + entityUid + ") via MasterDataCache!");
			return true;
		}
		return false;
	}
	/**
	 * fetches all data from the entity with the given name from the cache.
	 * If the entity is known not to be cacheable, ignores the cache.
	 * @param entityUid
	 * @return the current contents (data) of the entity with the given name.
	 */
	public List<MasterDataVO<?>> get(UID entityUid) {
		if (checkAndWarnIfNonSystemEntity(entityUid)) {
			return CollectionUtils.unsaveConvertToList(masterDataDelegate.getMasterData(entityUid));
		}
		List<MasterDataVO<?>> result = mp.get(entityUid);
		if (result == null) {
			result = CollectionUtils.unsaveConvertToList(masterDataDelegate.getMasterData(entityUid));
			if (!Boolean.FALSE.equals(isCacheable(entityUid))) {
				mp.put(entityUid, result);
			}
		}
		return result;
	}

	/**
	 * fetch data with the given id from the entity with the given name from the cache.
	 * If the entity is known not to be cacheable, ignores the cache.
	 * @param entityUid
	 * @param id
	 * @return the data (if found)
	 */
	public <PK> MasterDataVO<PK> get(UID entityUid, PK id) {
		if (checkAndWarnIfNonSystemEntity(entityUid)) {
			try {
				return masterDataDelegate.get(entityUid, id);
			} catch (CommonBusinessException cbe) {
				throw new CommonFatalException(cbe);
			}
		}
		List<MasterDataVO<PK>> result = CollectionUtils.unsaveCopyList(get(entityUid));
		for (MasterDataVO<PK> md : result) {
			if (LangUtils.equal(id,  md.getPrimaryKey())) {
				return md;
			}
		}
		return null;
	}

	/**
	 * §todo Whether an entity is cacheable or not should be known for each entity, not only for master data entities.
	 * 
	 * @param entityUid
	 * @return Ist the entity with the given name cacheable? <code>null</code> means unknown.
	 */
	public static Boolean isCacheable(UID entityUid) {
		final CollectableEntity clcte = DefaultCollectableEntityProvider.getInstance().getCollectableEntity(entityUid);
		if (clcte instanceof CollectableMasterDataEntity) {
			final CollectableMasterDataEntity clctmde = (CollectableMasterDataEntity) clcte;
			return clctmde.getMeta().isCacheable();
		}
		return null;
	}

	@ManagedOperation(description="invalidate/clear cache")
	public void invalidate() {
		invalidate(null);
	}
	/**
	 * invalidates the cache for the given entity or the whole cache, if <code>sEntityName == null</code>.
	 * @param entityUid
	 */
	public void invalidate(UID entityUid) {
		if (entityUid == null) {
			LOG.info("Invalidating the whole masterdata cache.");
			this.mp.clear();
			this.mpCollectableFieldsByName.clear();
			this.mpPrimaryKey.clear();
			masterDataDelegate.invalidateLayoutCache();
		}
		else {
			LOG.info("Removing entity " + entityUid + " from masterdata cache.");
			this.mp.remove(entityUid);
			this.mpPrimaryKey.remove(entityUid);

			for (CollectableFieldsByNameKey key : mpCollectableFieldsByName.keySet()) {
				if (entityUid.equals(key.entityUid)) {
					mpCollectableFieldsByName.remove(key);
				}
			}

			if (E.LAYOUT.checkEntityUID(entityUid) || E.LAYOUTUSAGE.checkEntityUID(entityUid)) {
				// invalidate the master data layout cache if a md layout (usage) has been changed:
				masterDataDelegate.invalidateLayoutCache();
			}
		}
	}

	/**
	 * get masterdata for entity as collectable fields.
	 * @param entityUid masterdata entity name.
	 * @param bCheckValidity Test for active sign and validFrom/validUntil
	 * @return list of collectable fields
	 */
	public List<CollectableField> getCollectableFields(UID entityUid, boolean bCheckValidity, UID mandator) throws CommonBusinessException {
		return getCollectableFieldsByName(entityUid, Utils.getNameIdentifierField(entityUid).getStringifiedDefinition(), bCheckValidity, mandator, null);
	}

	/**
	 * get masterdata for entity as collectable fields.
	 * @param entityUid masterdata entity name
	 * @param stringifiedFieldDefinition
	 * @param bCheckValidity Test for active sign and validFrom/validUntil
	 * @return list of collectable fields
	 */
	public List<CollectableField> getCollectableFieldsByName(UID entityUid, String stringifiedFieldDefinition, boolean bCheckValidity,
															 UID mandator, Pair<UID, CollectableSearchCondition> additionalCondition)
			throws CommonBusinessException {
		CollectableFieldsByNameKey cacheKey = new CollectableFieldsByNameKey();
		cacheKey.entityUid = entityUid;
		cacheKey.stringifiedFieldDefinition = stringifiedFieldDefinition;
		cacheKey.bCheckValidity = bCheckValidity;
		cacheKey.mandator = mandator;

		UID datalanguage = DataLanguageContext.getDataUserLanguage() != null ?
				DataLanguageContext.getDataUserLanguage() : DataLanguageContext.getDataSystemLanguage();
				
		List<CollectableField> result = mpCollectableFieldsByName.get(cacheKey);
		if (result == null) {
			result = entityFacadeDelegate.getCollectableFieldsByName(
					CollectableFieldsByNameParams.builder()
							.entityUid(entityUid)
							.stringifiedFieldDefinition(stringifiedFieldDefinition)
							.checkValidity(bCheckValidity)
							.mandator(mandator)
							.dataLanguage(datalanguage)
							.additionalCondition(additionalCondition)
							.build()
			);
			if (Boolean.TRUE.equals(isCacheable(entityUid))) {
				mpCollectableFieldsByName.put(cacheKey, result);
			}
		}
		return result;
	}

	private class CollectableFieldsByNameKey {
		UID entityUid;
		String stringifiedFieldDefinition;
		boolean bCheckValidity;
		UID mandator;

		@Override
		public boolean equals(Object obj) {

			if (obj instanceof CollectableFieldsByNameKey) {
				CollectableFieldsByNameKey other = (CollectableFieldsByNameKey) obj;
				return LangUtils.equal(this.entityUid, other.entityUid)
				    && LangUtils.equal(this.stringifiedFieldDefinition, other.stringifiedFieldDefinition)
				    && LangUtils.equal(this.bCheckValidity, other.bCheckValidity)
				    && LangUtils.equal(this.mandator, other.mandator);
			}

			return super.equals(obj);
		}
		
		@Override
		public int hashCode() {
			return LangUtils.hashCode(entityUid) ^ LangUtils.hashCode(stringifiedFieldDefinition) ^ LangUtils.hashCode(bCheckValidity) ^ LangUtils.hashCode(mandator);
		}
	}

	@ManagedAttribute(description="get the size (number of entries/entities) of mp")
	public int getNumberOfEntitiesInCache() {
		return mp.size();
	}

	/**
	 * For a better performance cacheable flag is ignored.
	 * Used from Utils.setDefaultValues
	 */
	public boolean exist(UID entityUid, Object id) {
		if (checkAndWarnIfNonSystemEntity(entityUid)) {
			return masterDataDelegate.exist(entityUid, id);
		}
		ConcurrentMap<Object, Boolean> cache = mpPrimaryKey.get(entityUid);
		if (cache == null) {
			cache = new ConcurrentHashMap<Object, Boolean>();
			mpPrimaryKey.put(entityUid, cache);
		}
		Boolean result = cache.get(id);
		if (result == null) {
			result = masterDataDelegate.exist(entityUid, id);
			cache.put(id, result);
		}
		assert result != null;
		return result;
	}

}	// class MasterDataCache
