//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata;

import java.awt.Component;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.EventListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.prefs.Preferences;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultBoundedRangeModel;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollBar;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.TransferHandler;
import javax.swing.event.ChangeEvent;
import javax.xml.stream.XMLStreamException;

import org.apache.log4j.Logger;
import org.nuclos.api.context.ScriptContext;
import org.nuclos.api.rule.CustomRule;
import org.nuclos.client.command.OvOpAdapter;
import org.nuclos.client.command.ResultListener;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.DependantCollectableMasterDataMap;
import org.nuclos.client.common.DetailsSubFormController;
import org.nuclos.client.common.EntityCollectController;
import org.nuclos.client.common.LafParameterProvider;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.MultiUpdateOfDependants;
import org.nuclos.client.common.NuclosCollectableListOfValues;
import org.nuclos.client.common.NuclosFocusTraversalPolicy;
import org.nuclos.client.common.NuclosResultPanel;
import org.nuclos.client.common.SearchConditionSubFormController;
import org.nuclos.client.common.SubFormController;
import org.nuclos.client.common.SubFormsLoaderClientWorker;
import org.nuclos.client.common.controller.NuclosCollectControllerCommonState;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.entityobject.EntityObjectDelegate;
import org.nuclos.client.explorer.ExplorerController;
import org.nuclos.client.explorer.ExplorerDelegate;
import org.nuclos.client.fileexport.FileExportDialog;
import org.nuclos.client.genericobject.GenerationController;
import org.nuclos.client.genericobject.GeneratorActions;
import org.nuclos.client.genericobject.valuelistprovider.MandatorCollectableFieldsProvider;
import org.nuclos.client.i18n.ui.TranslationsController;
import org.nuclos.client.main.MainController;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.datatransfer.MasterDataIdAndEntity;
import org.nuclos.client.masterdata.datatransfer.MasterDataVOTransferable;
import org.nuclos.client.masterdata.valuelistprovider.MasterDataCollectableFieldsProviderFactory;
import org.nuclos.client.rule.server.EventSupportDelegate;
import org.nuclos.client.scripting.context.CollectControllerScriptContext;
import org.nuclos.client.searchfilter.EntitySearchFilter;
import org.nuclos.client.searchfilter.SearchFilter;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.JInfoTabbedPane;
import org.nuclos.client.ui.LayoutComponentUtils;
import org.nuclos.client.ui.OverlayOptionPane;
import org.nuclos.client.ui.SplitPanePropertyChangeListener;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.CollectPanel;
import org.nuclos.client.ui.collect.CollectState;
import org.nuclos.client.ui.collect.CollectStateAdapter;
import org.nuclos.client.ui.collect.CollectStateEvent;
import org.nuclos.client.ui.collect.DefaultEditView;
import org.nuclos.client.ui.collect.EditView;
import org.nuclos.client.ui.collect.SearchOrDetailsPanel;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableComponentFactory;
import org.nuclos.client.ui.collect.component.CollectableComponentWithValueListProvider;
import org.nuclos.client.ui.collect.component.CollectableLocalizedComponent;
import org.nuclos.client.ui.collect.component.ICollectableListOfValues;
import org.nuclos.client.ui.collect.component.LabeledCollectableComponentWithVLP;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModel;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelProvider;
import org.nuclos.client.ui.collect.component.model.CollectableLocalizedComponentModel;
import org.nuclos.client.ui.collect.component.model.DetailsEditModel;
import org.nuclos.client.ui.collect.component.model.DetailsLocalizedComponentModel;
import org.nuclos.client.ui.collect.detail.DetailsPanel;
import org.nuclos.client.ui.collect.model.SortableCollectableTableModel;
import org.nuclos.client.ui.collect.result.NuclosResultController;
import org.nuclos.client.ui.collect.result.NuclosSearchResultStrategy;
import org.nuclos.client.ui.collect.result.ResultController;
import org.nuclos.client.ui.collect.result.ResultPanel;
import org.nuclos.client.ui.collect.search.ISearchStrategy;
import org.nuclos.client.ui.collect.search.SearchController;
import org.nuclos.client.ui.collect.search.SearchPanel;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.client.ui.collect.subform.SubForm.ParameterChangeListener;
import org.nuclos.client.ui.collect.subform.ToolbarFunction;
import org.nuclos.client.ui.collect.subform.ToolbarFunctionState;
import org.nuclos.client.ui.collect.toolbar.NuclosToolBarActions;
import org.nuclos.client.ui.gc.IReferenceHolder;
import org.nuclos.client.ui.layoutml.LayoutMLEditView;
import org.nuclos.client.ui.layoutml.LayoutMLParser;
import org.nuclos.client.ui.layoutml.LayoutRoot;
import org.nuclos.client.ui.table.TableUtils;
import org.nuclos.client.valuelistprovider.VLPClientUtils;
import org.nuclos.common.Actions;
import org.nuclos.common.DefaultComponentTypes;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.LafParameter;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.ControllerPresentation;
import org.nuclos.common.collect.DetailsPresentation;
import org.nuclos.common.collect.NuclosToolBarItems;
import org.nuclos.common.collect.ToolBarConfiguration;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProviderFactory;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSubCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.collect.collectable.searchcondition.SearchConditionUtils;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common.collect.exception.CollectableValidationException;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.DalSupportForMD;
import org.nuclos.common.dal.vo.DataLanguageMap;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.entityobject.CollectableEOEntityField;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;
import org.nuclos.common2.CloseableXStream;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.XStreamSupport;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.WriteProxyEmptyIdException;
import org.nuclos.common2.layoutml.exception.LayoutMLException;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.genericobject.ProxyList;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.navigation.treenode.MasterDataSearchResultNodeParameters;
import org.nuclos.server.navigation.treenode.MasterDataSearchResultTreeNode;
import org.nuclos.server.navigation.treenode.TreeNode;
import org.xml.sax.InputSource;

import com.thoughtworks.xstream.XStream;

/**
 * Controller for collecting master data. <br>
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author <a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class MasterDataCollectController<PK> extends EntityCollectController<PK,CollectableMasterDataWithDependants<PK>>
		implements IReferenceHolder {

	private static final Logger LOG = Logger.getLogger(MasterDataCollectController.class);

	public static final String FIELDNAME_ACTIVE = "active";
	public static final String FIELDNAME_VALIDFROM = "validfrom";
	public static final String FIELDNAME_VALIDUNTIL = "validuntil";

	private static int iFilter;
	private CollectableMasterDataWithDependants<PK> mdwdCurrent = null;
	
	// The following should be final but can't because they are
	// reset to null in close() to avoid memory leaks. (tp)
	
	protected JMenuItem btnShowResultInExplorer = new JMenuItem();
	protected final Action actMakeTreeRoot = NuclosToolBarActions.SHOW_IN_EXPLORER.init(new AbstractAction() {
		@Override
		public void actionPerformed(ActionEvent ev) {
			cmdJumpToTree();
		}
	});

	protected final Action actExecuteRule = NuclosToolBarActions.EXECUTE_RULE.init(new AbstractAction() {
		@Override
		public void actionPerformed(ActionEvent e) {
			cmdExecuteRuleByUser(MasterDataCollectController.this.getTab(), MasterDataCollectController.this.getEntityName(), MasterDataCollectController.this.getSelectedCollectable());
		}
	});
	
	protected final Action actShowHistory = NuclosToolBarActions.SHOW_HISTORY.init(new AbstractAction() {
		@Override
		public void actionPerformed(ActionEvent e) {
			cmdShowHistory();
		}
	});
	
	private final List<EventListener> refs = new LinkedList<EventListener>();

	private final boolean detailsWithScrollbar;

	protected final MasterDataDelegate mddelegate = MasterDataDelegate.getInstance();

	private Map<UID, DetailsSubFormController<PK,CollectableEntityObject<PK>>> mpsubformctlDetails;

	private MultiUpdateOfDependants<PK> multiupdateofdependants;

	private TranslationsController<PK> ctlTranslations;
	
	private final List<Component> toolbarCustomActionsDetails = new ArrayList<Component>();
	
	private int toolbarCustomActionsDetailsIndex = -1;
	
	private final SplitPanePropertyChangeListener pclSplitPane;

	/**
	 * Use <code>newMasterDataCollectController()</code> to create an instance
	 * of this class.
	 *
	 * You should use
	 * {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton}
	 * to get an instance.
	 *
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	public MasterDataCollectController(EntityMeta<PK> systemEntity, MainFrameTab tabIfAny, String customUsage) {
		this(systemEntity.getUID(), tabIfAny, customUsage);
	}

	/**
	 * You should use
	 * {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton} to
	 * get an instance.
	 *
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	protected MasterDataCollectController(EntityMeta<PK> systemEntity, MainFrameTab tabIfAny, 
			boolean detailsWithScrollbar, String customUsage) {
		this(systemEntity.getUID(), tabIfAny, detailsWithScrollbar, customUsage, ControllerPresentation.DEFAULT);
	}

	/**
	 * You should use
	 * {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton} to
	 * get an instance.
	 *
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	public MasterDataCollectController(UID entityUid, MainFrameTab tabIfAny, String customUsage) {
		this(entityUid, tabIfAny, true, customUsage, ControllerPresentation.DEFAULT);
	}
	
	public MasterDataCollectController(UID entityUid, MainFrameTab tabIfAny, String customUsage, ControllerPresentation viewmode) {
		this(entityUid, tabIfAny, true, customUsage, viewmode);		
	}

	/**
	 * You should use
	 * {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton} to
	 * get an instance.
	 *
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	protected MasterDataCollectController(UID entityUid, MainFrameTab tabIfAny, boolean detailsWithScrollbar, 
			String customUsage, ControllerPresentation viewmode) {
		
		this(entityUid, tabIfAny, detailsWithScrollbar, 
				new NuclosResultController<PK,CollectableMasterDataWithDependants<PK>>(
							entityUid, 
							new NuclosSearchResultStrategy<PK,CollectableMasterDataWithDependants<PK>>(),
							new NuclosCollectControllerCommonState()), 
						customUsage, viewmode);
	}

	/**
	 * You should use
	 * {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton} to
	 * get an instance.
	 *
	 * Use <code>newMasterDataCollectController()</code> to create an instance
	 * of this class.
	 */
	protected MasterDataCollectController(EntityMeta<PK> entity, MainFrameTab tabIfAny, 
			ResultController<PK,CollectableMasterDataWithDependants<PK>> rc, String customUsage) {
		this(entity.getUID(), tabIfAny, true, rc, customUsage, ControllerPresentation.DEFAULT);
	}

	/**
	 * You should use
	 * {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton} to
	 * get an instance.
	 *
	 * Use <code>newMasterDataCollectController()</code> to create an instance
	 * of this class.
	 */
	protected MasterDataCollectController(EntityMeta<PK> entity, MainFrameTab tabIfAny, boolean detailsWithScrollbar, 
			ResultController<PK,CollectableMasterDataWithDependants<PK>> rc, String customUsage) {
		this(entity.getUID(), tabIfAny, detailsWithScrollbar, rc, customUsage, ControllerPresentation.DEFAULT);
	}

	/**
	 * You should use
	 * {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton} to
	 * get an instance.
	 */
	protected MasterDataCollectController(UID entityUid, MainFrameTab tabIfAny, boolean detailsWithScrollbar, 
			ResultController<PK,CollectableMasterDataWithDependants<PK>> rc, String customUsage, ControllerPresentation viewMode) {
		super(entityUid, tabIfAny, rc, customUsage, viewMode);
		
		EntityMeta<?> eMeta = MetaProvider.getInstance().getEntity(entityUid);
		final boolean bSearchPanelAvailable = eMeta.isSearchable() || MainController.isOverrideSearchableForEntity(entityUid);
		final boolean bShowSearch = eMeta.isShowSearch();
		this.detailsWithScrollbar = detailsWithScrollbar;
		DetailsPresentation detailsPresent = DetailsPresentation.DEFAULT;
		if (getViewMode() == ControllerPresentation.DEFAULT) {
			detailsPresent = LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Details_Presentation, entityUid);
		}
		final CollectPanel<PK,CollectableMasterDataWithDependants<PK>> pnlCollect = new MasterDataCollectPanel(
				entityUid, bSearchPanelAvailable, bShowSearch, detailsPresent, getViewMode());
		getTab().setLayeredComponent(pnlCollect);
		this.initialize(pnlCollect);
		this.setupEditPanelForDetailsTab();
		this.setupShortcutsForTabs(getTab());
	
		this.getCollectStateModel().addCollectStateListener(new CollectStateAdapter() {
			@Override
			public void resultModeEntered(CollectStateEvent ev) {
				if (ev.getOldCollectState().getOuterState() != CollectState.OUTERSTATE_RESULT) {
					setupChangeListenerForResultTableVerticalScrollBar();
				}
			}

			@Override
			public void resultModeLeft(CollectStateEvent ev) {
				if (ev.getNewCollectState().getOuterState() != CollectState.OUTERSTATE_RESULT) {
					removePreviousChangeListenersForResultTableVerticalScrollBar();
				}
			}
		});
		this.getCollectStateModel().addCollectStateListener(new CollectStateAdapter() {
			@Override
			public void detailsModeLeft(CollectStateEvent ev)
					throws CommonBusinessException {
				if (!LangUtils.equal(ev.getNewCollectState().getOuterState(), ev.getOldCollectState().getOuterState())) {
					// remove keystrokes only if outer state change
					getLayoutMLButtonsActionListener().clearInputMapForParentPanel(getCollectPanel());
					getLayoutMLButtonsActionListener().clearInputMapForParentPanel(getDetailsPanel().getLayoutRoot().getRootComponent());
					
					setupShortcutsForTabs(getTab());
				}
				// reload layout after multi edit or view
				if (ev.getOldCollectState().isDetailsModeMultiViewOrEdit() && !ev.getNewCollectState().isDetailsModeMultiViewOrEdit()) {
					getDetailsPanel().setLayoutRoot(newLayoutRoot());
					setupEditPanelForDetailsTab();
				}

				if (ev.getNewCollectState().isResultMode() && getSelectedCollectable() != null) {
					getResultController().replaceCollectableInTableModel(getSelectedCollectable(), true);
				}
			}
			@Override
			public void detailsModeEntered(CollectStateEvent ev) {
				int iDetailsMode = ev.getNewCollectState().getInnerState();
				final boolean bViewingExistingRecord = (iDetailsMode == CollectState.DETAILSMODE_VIEW);
				UIUtils.invokeOnDispatchThread(new Runnable() {
					@Override
					public void run() {
						actMakeTreeRoot.setEnabled(bViewingExistingRecord);
						actExecuteRule.setEnabled(bViewingExistingRecord);
						actShowHistory.setEnabled(bViewingExistingRecord);
					}
				});

				final Collection<SubForm> collsubform = new HashSet<SubForm>();
				for (SubFormController subformctl : getSubFormControllersInDetails()) {
					collsubform.add(subformctl.getSubForm());
				}
				// current state, subsequent states and custom actions:

				switch (iDetailsMode) {
				case CollectState.DETAILSMODE_VIEW:
					if (isHistoricalView()) {
						getLayoutMLButtonsActionListener().setComponentsEnabled(false);
					} else {
						getLayoutMLButtonsActionListener().fireComponentEnabledStateUpdate(false);
					}
					final EditView view = getDetailsPanel().getEditView();
					if (view != null) {
						respectRights(view.getCollectableComponents(), collsubform, ev.getNewCollectState());
					}

					for(DetailsSubFormController dsc : getSubFormControllersInDetails()) {			
						UID sEntity = dsc.getSubForm().getEntityUID();
						
						boolean hasScripts = 
								dsc.getSubForm().getDeleteEnabledScript() != null || 
										dsc.getSubForm().getNewEnabledScript() != null ||
												dsc.getSubForm().getCloneEnabledScript() != null ||
														dsc.getSubForm().getEditEnabledScript() != null;
						
						if(!hasScripts && MasterDataLayoutHelper.isLayoutMLAvailable(sEntity, false) &&
								(SecurityCache.getInstance().isReadAllowedForEntity(sEntity))) {
							dsc.getSubForm().setToolbarFunctionState(ToolbarFunction.SUBFORM_DETAILS_VIEW, ToolbarFunctionState.ACTIVE);
						} else {
							dsc.getSubForm().setToolbarFunctionState(ToolbarFunction.SUBFORM_DETAILS_VIEW, ToolbarFunctionState.HIDDEN);
	 					}
						if (MasterDataLayoutHelper.isLayoutMLAvailable(sEntity, false) && MasterDataLayoutHelper.isLayoutMLAvailable(sEntity, false) && MetaProvider.getInstance().getEntity(sEntity).isStateModel()) {
							dsc.getSubForm().setToolbarFunctionState(ToolbarFunction.SUBFORM_CHANGE_STATE, 
									(!dsc.getSubForm().isReadOnly() && dsc.getSubForm().isEnabled()) ? ToolbarFunctionState.ACTIVE : ToolbarFunctionState.DISABLED);
						} else {
							dsc.getSubForm().setToolbarFunctionState(ToolbarFunction.SUBFORM_CHANGE_STATE, ToolbarFunctionState.HIDDEN);
						}
					}
					break;
				case CollectState.DETAILSMODE_EDIT:
					if (isHistoricalView()) {
						getLayoutMLButtonsActionListener().setComponentsEnabled(false);
					} else {
						getLayoutMLButtonsActionListener().fireComponentEnabledStateUpdate(true);
					}

					respectRights(getDetailsPanel().getEditView().getCollectableComponents(), collsubform, ev.getNewCollectState());

					for(DetailsSubFormController dsc : getSubFormControllersInDetails()) {						
						if((dsc.getSubForm().isIgnoreSubLayout() || dsc.getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_DETAILS_VIEW) != null) 
								&& dsc.getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_DETAILS_VIEW) != ToolbarFunctionState.HIDDEN) {
							dsc.getSubForm().setToolbarFunctionState(ToolbarFunction.SUBFORM_DETAILS_VIEW, ToolbarFunctionState.DISABLED);
						}
						if(dsc.getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_CHANGE_STATE) != null 
								&& dsc.getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_CHANGE_STATE) != ToolbarFunctionState.HIDDEN) {
							dsc.getSubForm().setToolbarFunctionState(ToolbarFunction.SUBFORM_CHANGE_STATE, ToolbarFunctionState.DISABLED);
						}
					}
					break;
				case CollectState.DETAILSMODE_NEW:
					setInitialComponentFocusInDetailsTab();
				case CollectState.DETAILSMODE_NEW_CHANGED:
				case CollectState.DETAILSMODE_NEW_SEARCHVALUE:
					if (iDetailsMode == CollectState.DETAILSMODE_NEW)
						getLayoutMLButtonsActionListener().setComponentsEnabled(false);
					else
						getLayoutMLButtonsActionListener().fireComponentEnabledStateUpdate(true);
					
					respectRights(getDetailsPanel().getEditView().getCollectableComponents(), collsubform, ev.getNewCollectState());

					for(DetailsSubFormController dsc : getSubFormControllersInDetails()) {		
						if((dsc.getSubForm().isIgnoreSubLayout() || dsc.getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_DETAILS_VIEW) != null) 
								&& dsc.getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_DETAILS_VIEW) != ToolbarFunctionState.HIDDEN) {
							dsc.getSubForm().setToolbarFunctionState(ToolbarFunction.SUBFORM_DETAILS_VIEW, ToolbarFunctionState.DISABLED);
						} 
						if(dsc.getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_CHANGE_STATE) != null 
								&& dsc.getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_CHANGE_STATE) != ToolbarFunctionState.HIDDEN) {
							dsc.getSubForm().setToolbarFunctionState(ToolbarFunction.SUBFORM_CHANGE_STATE, ToolbarFunctionState.DISABLED);
						}
					}
					break;
				case CollectState.DETAILSMODE_MULTIVIEW:
				case CollectState.DETAILSMODE_MULTIEDIT:
					getLayoutMLButtonsActionListener().setComponentsEnabled(false);
					getLayoutMLButtonsActionListener().setRuleButtonComponentsEnabled(true);

					respectRights(getDetailsPanel().getEditView().getCollectableComponents(), collsubform, ev.getNewCollectState());
					respectMultiEditable(getDetailsPanel().getEditView().getCollectableComponents(), ev.getNewCollectState());
					
					for(DetailsSubFormController dsc : getSubFormControllersInDetails()) {		
						if((dsc.getSubForm().isIgnoreSubLayout() || dsc.getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_DETAILS_VIEW) != null) 
								&& dsc.getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_DETAILS_VIEW) != ToolbarFunctionState.HIDDEN) {
							dsc.getSubForm().setToolbarFunctionState(ToolbarFunction.SUBFORM_DETAILS_VIEW, ToolbarFunctionState.HIDDEN);
						} 
						if(dsc.getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_CHANGE_STATE) != null 
								&& dsc.getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_CHANGE_STATE) != ToolbarFunctionState.HIDDEN) {
							dsc.getSubForm().setToolbarFunctionState(ToolbarFunction.SUBFORM_CHANGE_STATE, ToolbarFunctionState.HIDDEN);
						}
					}
					break;
				default:
				} // switch

				final EditView view = getDetailsPanel().getEditView();
				if (view != null) {
					respectRights(view.getCollectableComponents(), collsubform, ev.getNewCollectState());
				}
				
				getLayoutMLButtonsActionListener().clearInputMapForParentPanel(getCollectPanel());
				getLayoutMLButtonsActionListener().clearInputMapForParentPanel(getDetailsPanel().getLayoutRoot().getRootComponent());
				
				getLayoutMLButtonsActionListener().setInputMapForParentPanel(getCollectPanel(), getDetailsPanel().getLayoutRoot().getRootComponent());
				
				showCustomActions(iDetailsMode);
				
				setupShortcutsForTabs(getTab());
				
				setupSplitPane();
			}
		});
		this.getCollectStateModel().addCollectStateListener(new CollectStateAdapter() {
			@Override
			public void detailsModeEntered(CollectStateEvent ev) throws CommonBusinessException {
				if (ev.hasOuterStateChanged()) {
					setInitialComponentFocusInDetailsTab();
				}
			}
			@Override
			public void searchModeEntered(CollectStateEvent ev) throws CommonBusinessException {
				setInitialComponentFocusInSearchTab();

				for (CollectableComponent clctcomp : getSearchPanel().getEditView().getCollectableComponents()) {
					clctcomp.setEnabled(true);
				}
			}
			@Override
			public void resultModeEntered(CollectStateEvent ev) throws CommonBusinessException {
				refreshResultFilterView();
			}
		});
		
		this.pclSplitPane = new SplitPanePropertyChangeListener(getDetailsPanel(), this);
	}

	protected void resetLocalizedComponentModels() {

	}
	
	/**
	 * TODO: Make this protected.
	 */
	public void init() {
		try {
			super.init();
	
			if (this.isSearchPanelAvailable()) {
				initSearchSubforms();
			}
			this.setupSearchToolBar(); // setup search for filter selection in result -> refs NOAINT-452
			this.setupResultToolBar();
			this.setupDetailsToolBar();
			refreshFastFilter();
	
			initSubFormsLoader();
			setupDataTransfer();
			getDetailsPanel().getLayoutRoot().getRootComponent().setFocusCycleRoot(true);
			getDetailsPanel().getLayoutRoot().getRootComponent().setFocusTraversalPolicyProvider(true);
			getDetailsPanel().getLayoutRoot().getRootComponent().setFocusTraversalPolicy(new NuclosFocusTraversalPolicy(getDetailsPanel().getLayoutRoot()));
			
			initMatrixControllers();
	
			setupResultContextMenuGeneration();
			setupResultContextExecuteMenuCustomRule();
	
			if (E.getByUID(getEntityUid()) != null) {
				ctlTranslations = new TranslationsController<PK>(this);
				getCollectStateModel().addCollectStateListener(ctlTranslations);
			}
			
			ToolBarConfiguration configuration = 
					LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Tool_Bar_Detail, getEntityUid());
			this.getDetailsPanel().setToolBarConfiguration(configuration);			
			configuration = 
					LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Tool_Bar_Result, entityUid);
			this.getResultPanel().setToolBarConfiguration(configuration);
			configuration = 
					LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Tool_Bar_Search, entityUid);
			this.getSearchPanel().setToolBarConfiguration(configuration);
		} catch (RuntimeException re) {
			super.errorDuringInit(re);
		}
	}

	@Override
	public void addRef(EventListener o) {
		refs.add(o);
	}
	
	@Override
	protected void setupSearchToolBar() {
		super.setupSearchToolBar();
		refreshSearchFilterView();
		addButtonInvalidMasterdata();
	}
	
	/**
	 * This method is called by <code>cmdClearSearchFields</code>, that is when the user clicks
	 * the "Clear Search Fields" button. This implementation selects the default search filter.
	 */
	@Override
	protected void clearSearchCondition() {
		super.clearSearchCondition();
		// select the default filter (the first entry):
		selectDefaultFilter();
	}

	@Override
	protected String getTitle(int iTab, int iMode) {
		final SpringLocaleDelegate localeDelegate = getSpringLocaleDelegate();
		final String[] asTabs = { localeDelegate.getMessage("MasterDataCollectController.17", "Suche"), localeDelegate.getMessage("MasterDataCollectController.5", "Ergebnis"),
				localeDelegate.getMessage("MasterDataCollectController.2", "Details") };
		final String[] asDetailsMode = { localeDelegate.getMessage("MasterDataCollectController.18", "Undefiniert"), localeDelegate.getMessage("MasterDataCollectController.3", "Details"),
				localeDelegate.getMessage("MasterDataCollectController.1", "Bearbeiten"), localeDelegate.getMessage("MasterDataCollectController.10", "Neueingabe"),
				localeDelegate.getMessage("MasterDataCollectController.12", "Neueingabe (Ge\u00e4ndert)"), localeDelegate.getMessage("MasterDataCollectController.15", "Sammelbearbeitung"),
				localeDelegate.getMessage("MasterDataCollectController.16", "Sammelbearbeitung (Ge\u00e4ndert)"), localeDelegate.getMessage("MasterDataCollectController.11", "Neueingabe (\u00dcbernahme Suchwerte)") };

		String sPrefix;
		String sSuffix = "";
		final String sMode;

		switch (iTab) {
		case CollectState.OUTERSTATE_DETAILS:
			sPrefix = (iMode == 1 || iMode == 2) ? "" : this.getEntityLabel() + " ";
			sMode = asDetailsMode[iMode];
			if (CollectState.isDetailsModeViewOrEdit(iMode)) {
				EntityMeta<PK> metaDataVO = (EntityMeta<PK>) MasterDataDelegate.getInstance().getMetaData(getEntityUid());
				String sIdentifier = localeDelegate.getIdentifierLabel(
						getSelectedCollectable(), getEntityUid(), MetaProvider.getInstance(), 
						getLanguage());
				if (sIdentifier == null) {
					sIdentifier = "<>";
				}
				sPrefix += sIdentifier;
			}
			else if (CollectState.isDetailsModeMultiViewOrEdit(iMode)) {
				sSuffix = localeDelegate.getMessage("MasterDataCollectController.19", " von {0} Objekten", this.getSelectedCollectables().size());
			}
			break;
		default:
			sPrefix = this.getEntityLabel();
			sMode = asTabs[iTab];
		}
		
		final StringBuilder sb; 
		if (iTab == CollectState.OUTERSTATE_DETAILS && (iMode == 1 || iMode == 2))
			sb = new StringBuilder(sPrefix + (iMode == 2 ? " - " + sMode : "") + sSuffix);
		else
			sb = new StringBuilder(sPrefix + (sPrefix.length() > 0 ? " - " : "") + sMode + sSuffix);

		if (isSelectedCollectableMarkedAsDeleted())
			sb.append(getSpringLocaleDelegate().getMessage("MasterDataCollectController.21"," (Objekt ist als gel\u00f6scht markiert)"));
		else if (isHistoricalView())
			sb.append(getSpringLocaleDelegate().getMessage("MasterDataCollectController.22"," (Historischer Zustand vom {0})", getHistoricalDate()));

		return sb.toString();
	}

	protected final boolean isSelectedCollectableMarkedAsDeleted() {
		boolean result = false;
		if (getCollectState().getInnerState() == CollectState.DETAILSMODE_VIEW)
			if (!multipleCollectablesSelected()) {
				final CollectableMasterData<PK> clct = getSelectedCollectable();
				if (clct != null && clct.getMasterDataCVO().isRemoved())
					result = true;
			}
		return result;
	}

	@Override
	protected String getLabelForStartTab() {
		String result = null;

		boolean buildTreeView = false;
		switch (getCollectState().getOuterState()) {
		case CollectState.OUTERSTATE_DETAILS:
			buildTreeView = this.getCollectState().isDetailsModeViewOrEdit();
			break;
		case CollectState.OUTERSTATE_RESULT:
			buildTreeView = this.getSelectedCollectables().size() == 1;
			break;
		}

		if (buildTreeView) {
			result = getSpringLocaleDelegate().getTreeViewLabel(getSelectedCollectable(), getEntityUid(), 
					MetaProvider.getInstance(), getLanguage());
		}

		if (result == null) {
			return super.getLabelForStartTab();
		}
		else {
			return result.trim();
		}
	}

	/**
	 * adjusts the visibility and "enability" ;) of the fields according to the
	 * user rights.
	 *
	 * @param collclctcomp
	 * @param collectstate
	 */
	protected void respectRights(Collection<CollectableComponent> collclctcomp, Collection<SubForm> collsubform, CollectState collectstate) {
		boolean entityEditable = SecurityCache.getInstance().isWriteAllowedForMasterData(entityUid)
				&& getCollectableEntity().getMeta().isEditable()
				&& (collectstate.isDetailsModeNew() || getSelectedCollectable() == null || getSelectedCollectable().canWrite());
		for (CollectableComponent clctcomp : collclctcomp) {
			boolean fieldEditable = collectstate.isSearchMode()
					|| (entityEditable
						&& clctcomp.getEntityField().isModifiable()
						&& (ClientParameterProvider.getInstance().isEnabled(ParameterProvider.MODIFIABLE_READONLY)
							|| !MetaProvider.getInstance().getEntityField(clctcomp.getFieldUID()).isReadonly()));
			clctcomp.setReadOnly(!fieldEditable);
		}

		// adjust subforms:
		for (SubForm subform : collsubform) {

			// a subform may be explicitly disabled in the LayoutML definition.
			boolean editable = (collectstate.isSearchMode()
					|| (MasterDataDelegate.getInstance().getMetaData(subform.getEntityUID()).isEditable()
						&& entityEditable));
			subform.setReadOnly(!editable);
		}
	}
	
	protected void respectMultiEditable(Collection<CollectableComponent> collclctcomp, CollectState collectstate) {
		if (SecurityCache.getInstance().isWriteAllowedForMasterData(entityUid) || getCollectableEntity().getMeta().isEditable()) {
			for (CollectableComponent clctcomp : collclctcomp) {
				if (!clctcomp.isMultiEditable()) {
					clctcomp.setReadOnly(true);
				}
			} // for
		}
	}
	
	protected void setupResultToolBar() {
		super.setupResultToolBar();
		final NuclosResultPanel<PK, CollectableMasterDataWithDependants<PK>> rp = getResultPanel();

		rp.registerToolBarAction(NuclosToolBarItems.INFO, getPointerAction());
		getDetailsController().getDeleteCurrentCollectableAction().setEnabled(
				SecurityCache.getInstance().isDeleteAllowedForMasterData(entityUid));

		btnShowResultInExplorer.setIcon(Icons.getInstance().getIconTree16());
		btnShowResultInExplorer.setText(getSpringLocaleDelegate().getMessage("MasterDataCollectController.6",
				"Ergebnis in Explorer anzeigen"));
		btnShowResultInExplorer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ev) {
				cmdShowResultInExplorer();
			}
		});
		
		rp.addPopupExtraSeparator();
		rp.addPopupExtraMenuItem(btnShowResultInExplorer);

		addAndSetupExportResultButton();
	}

	protected void setupDetailsToolBar() {
		super.setupDetailsToolBar();
		getDetailsPanel().registerToolBarAction(NuclosToolBarItems.SHOW_IN_EXPLORER, actMakeTreeRoot);

		// Execute rule by user only for authorized personnel
		if (SecurityCache.getInstance().isActionAllowed(Actions.ACTION_EXECUTE_RULE_BY_USER)) {
			actExecuteRule.setEnabled(SecurityCache.getInstance().isActionAllowed(Actions.ACTION_EXECUTE_RULE_BY_USER));
			if (mddelegate.getUsesRuleEngine(getEntityUid())) {
				this.getDetailsPanel().registerToolBarAction(NuclosToolBarItems.EXECUTE_RULE, actExecuteRule);
			}
		}

		// Add Localization Button totoolbar we neceaary
		if (getCollectableEntity().getMeta().IsLocalized()) {
			final Action actShowLocalization = NuclosToolBarActions.LOCALIZATION.init(new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					cmdShowLocalization();
				}

				
			});
			actShowLocalization.setEnabled(true);
			this.getDetailsPanel().registerToolBarAction(NuclosToolBarItems.LOCALIZATION, actShowLocalization);
		}
		
		//add the history components only if loogbook tracking is enabled in this entity
		if (entityUid != null && MetaProvider.getInstance().getEntity(entityUid).isLogBookTracking()) {
			getDetailsPanel().registerToolBarAction(NuclosToolBarItems.SHOW_HISTORY, actShowHistory);
		}

		toolbarCustomActionsDetailsIndex = getDetailsPanel().getToolBarNextIndex();
		getDetailsPanel().registerToolBarAction(NuclosToolBarItems.INFO, getPointerAction());
		
		this.getDetailsPanel().registerToolBarAction(NuclosToolBarItems.PRINT_DETAILS, actPrintDetails);
		this.getDetailsPanel().registerToolBarAction(NuclosToolBarItems.LOCK, actLock);
	}

	private void showCustomActions(int iDetailsMode) {
		final boolean bSingle = CollectState.isDetailsModeViewOrEdit(iDetailsMode);
		final boolean bMulti = CollectState.isDetailsModeMultiViewOrEdit(iDetailsMode);
		final boolean bViewOrEdit = bSingle || bMulti;
		final boolean bView = bViewOrEdit && !CollectState.isDetailsModeChangesPending(iDetailsMode);

		this.getDetailsPanel().removeToolBarComponents(toolbarCustomActionsDetails);
		toolbarCustomActionsDetails.clear();
		if (toolbarCustomActionsDetailsIndex == -1) {
			return;
		}

		if (bViewOrEdit) {
			setupGeneratorActions(bView);
		}
		
		// button: "print details":
		if (bMulti) {
			actPrintDetails.setEnabled(bView && hasFormsAssigned(getSelectedCollectables()));
		} else {
			actPrintDetails.setEnabled(!isHistoricalView() && bView && hasFormsAssigned(getSelectedCollectable()));
		}
					
		this.getDetailsPanel().addToolBarComponents(toolbarCustomActionsDetails, toolbarCustomActionsDetailsIndex);
	}

	private static class RestorePreferences implements Serializable {
		private static final long serialVersionUID = 6637996725938917463L;

		UID searchFilterUID;
	}

	private void cmdShowLocalization() {
		
		UID locField = null;
		for (UID uid : getCollectableEntityForDetails().getFieldUIDs()) {
			if (getCollectableEntityForDetails().getEntityField(uid).isLocalized() && 
					!getCollectableEntityForDetails().getEntityField(uid).isCalculated()) {
				locField = uid;
			}
		}
		
		if (locField != null) {
			CollectableLocalizedComponentModel model = (CollectableLocalizedComponentModel)
					getDetailsComponentModel(locField);
			showLocalizationDialog(getCollectableEntity().getMeta(), model.getPrimaryKey(), model.getDataLanguageMap());			
		}
	
	}

	private static String toXML(RestorePreferences rp) {
		final XStreamSupport xs = XStreamSupport.getInstance();
		try (CloseableXStream closeable = xs.getCloseableStream()) {
			final XStream xstream = closeable.getStream();
			return xstream.toXML(rp);
		}
	}

	private static RestorePreferences fromXML(String xml) {
		final XStreamSupport xs = XStreamSupport.getInstance();
		try (CloseableXStream closeable = xs.getCloseableStream()) {
			final XStream xstream = closeable.getStream();
			return (RestorePreferences) xstream.fromXML(xml);
		}
	}

	@Override
	protected void storeInstanceStateToPreferences(Map<String, String> inheritControllerPreferences) {
		RestorePreferences rp = new RestorePreferences();

		SearchFilter filter = state != null ? state.getCurrentSearchFilter() : null;
		rp.searchFilterUID = (filter == null || filter.isDefaultFilter()) ? null : filter.getSearchFilterVO().getId();

		inheritControllerPreferences.put(MasterDataCollectController.class.getName(), toXML(rp));
		super.storeInstanceStateToPreferences(inheritControllerPreferences);
	}
	
	@Override
	protected void restoreInstanceStateFromPreferences(Map<String, String> inheritControllerPreferences) {
		String prefXml = inheritControllerPreferences.get(MasterDataCollectController.class.getName());
		if (prefXml != null) {
			RestorePreferences rp = fromXML(prefXml);
			setSearchFilter(rp.searchFilterUID);
		}
		super.restoreInstanceStateFromPreferences(inheritControllerPreferences);
	}

	/**
	 * §precondition this.isSearchPanelVisible()
	 */
	@Override
	@Deprecated
	protected void restoreSearchCriteriaFromPreferences(Preferences prefs) throws CommonBusinessException {
		if (!isSearchPanelAvailable())
			throw new IllegalStateException("!isSearchPanelVisible()");

		// Restore the settings for the chosen search filter in this module window (may override the global settings)
		restoreSelectedSearchFilterFromPreferences(prefs);

		super.restoreSearchCriteriaFromPreferences(prefs);
	}

	@Override
	public boolean save() throws CommonBusinessException {
		
		if (isHistoricalView()) {
			final int btn = JOptionPane.showConfirmDialog(
					getTab(),
					getSpringLocaleDelegate().getMessage(
							"GenericObjectCollectController.102",
							"Mit dem Wiederherstellen eines historischen Wertes werden aktuell bestehende Werte in der Datanbank überschrieben.\nSind Sie sicher?"),
					getSpringLocaleDelegate().getMessage(
							"GenericObjectCollectController.103",
							"Wiederherstellen eines historischen Wertes"),
					JOptionPane.YES_NO_OPTION);
			
			if (btn != JOptionPane.YES_OPTION) {
				return false;	
			}
		}
		
		try
		{
			return super.save();
		}catch(WriteProxyEmptyIdException e)
		{					
			this.stopEditingInDetails();
			this.setCollectState(CollectState.OUTERSTATE_RESULT, CollectState.RESULTMODE_NOSELECTION);

			return true;
		}
	}

	@Override
	protected void delegateBusinessRulesExecution(List<EventSupportSourceVO> lstRuleVO, CollectableMasterDataWithDependants<PK> clct,
												  String custom, boolean bCollectiveProcessing) throws CommonBusinessException {
		mddelegate.executeBusinessRules(lstRuleVO, clct.getMasterDataCVO(), getCustomUsage(), bCollectiveProcessing);
	}

	@Override
	protected EntitySearchFilter retrieveCurrentSearchFilterFromSearchPanel() throws CommonBusinessException {
		final EntitySearchFilter result = super.retrieveCurrentSearchFilterFromSearchPanel();

		result.setVisibleColumns(this.getSelectedFields());
		return result;
	}

	protected void cmdJumpToTree() {
		UIUtils.runCommand(this.getTab(), new CommonRunnable() {
			@Override
			public void run() throws CommonFinderException, CommonPermissionException {
				final PK pk = getSelectedCollectableId();
				final UID entityUid = getSelectedCollectable().getCollectableEntity().getUID();
				if (pk != null) {
					TreeNode treenode = null;
					// @todo create a MasterdataTreeNodeFactory and get the node
					// from the factory
					if (E.NUCLET.checkEntityUID(entityUid)) {
						treenode = ExplorerDelegate.getInstance().getNucletTreeNode((UID) pk);
					}
					else {
						treenode = ExplorerDelegate.getInstance().getMasterDataTreeNode(pk, entityUid, null);
					}
					getExplorerController().showInOwnTab(treenode);
				}
			}
		});
	}

	private ExplorerController getExplorerController() {
		return getMainController().getExplorerController();
	}

	protected void cmdShowResultInExplorer() {
		UIUtils.runCommand(this.getTab(), new CommonRunnable() {
			@Override
			public void run() throws CollectableFieldFormatException {
				final String sFilterName = getCollectableEntity().getLabel() + Integer.toString(++iFilter);
				final UID entityUid = getCollectableEntity().getUID();
				final ISearchStrategy<PK,CollectableMasterDataWithDependants<PK>> ss = getSearchStrategy();
				// final String sFilterName = "Gruppe " +
				// Integer.toString(++iFilter);
				// @todo create a MasterdataTreeNodeFactory and get the node
				// from the factory
				getExplorerController().showInOwnTab(new MasterDataSearchResultTreeNode<PK>(
						new MasterDataSearchResultNodeParameters(entityUid, ss.getCollectableSearchCondition(), sFilterName)));
			}
		});
	}

	@Override
	public void close() {
		getTab().removeLayeredComponent();
		
		super.close();
		
		this.btnShowResultInExplorer = null;

		//getSubFormsLoader().interruptAllClientWorkers();

		this.closeSubFormControllers();
		
		if (mpsubformctlDetails != null) {
			mpsubformctlDetails.clear();
		}
		mpsubformctlDetails = null;
		
		if (mpsubformctlDetails != null) {
			for (DetailsSubFormController<PK, CollectableEntityObject<PK>> dsc: mpsubformctlDetails.values()) {
				dsc.close();
			}
			mpsubformctlDetails.clear();
		}
		ctlTranslations = null;
		if (ctlSearch != null) {
			ctlSearch.close();
		}
	}

	/**
	 * @return the collectable (master data) entity for this controller.
	 *
	 *         TODO: Make this protected again.
	 */
	@Override
	public CollectableMasterDataEntity getCollectableEntity() {
		return (CollectableMasterDataEntity) super.getCollectableEntity();
	}

	/**
	 * @deprecated Move to SearchController and make protected again.
	 */
	@Override
	public CollectableFieldsProviderFactory getCollectableFieldsProviderFactoryForSearchEditor() {
		return valueListProviderCache.makeCachingFieldsProviderFactory(new MasterDataCollectableFieldsProviderFactory());
	}

	/**
	 * runs this controller, starting with the search panel or with the result
	 * panel, depending on the parameter.
	 *
	 * @param bStartWithSearchPanel
	 */
	@Override
	protected void run(boolean bStartWithSearchPanel) throws CommonBusinessException {
		if (bStartWithSearchPanel && isSearchPanelAvailable()) {
			final MasterDataSearchPanel<PK> searchPanel = getSearchPanel();
			final boolean bCanBeFiltered = isFilteringAppropriate();			
			searchPanel.filterByValidity(bCanBeFiltered);

			this.runSearch();
		}
		else {
			this.runViewAll();
		}
	}

	@Deprecated
	protected void setupEditPanelForDetailsTab() {
		closeSubFormControllers();

		// create a controller for each subform:
		Map<UID, SubForm> mpSubForm = getDetailsPanel().getLayoutRoot().getMapOfSubForms();
		mpsubformctlDetails = newDetailsSubFormControllers(mpSubForm);
      
		Map<SubForm, MasterDataSubFormController<PK>> mpSubFormController = new HashMap<SubForm, MasterDataSubFormController<PK>>();

		// create a map of subforms and their controllers that exists in the
		// layout
      final ParameterChangeListener changeListener = new ParameterChangeListener() {
		@Override
		public void stateChanged(final ChangeEvent e) {
			if (e != null && e.getSource() instanceof SubForm) {
			   	UIUtils.invokeOnDispatchThread(new Runnable() {
						@Override
						public void run() {
							SubForm subform = (SubForm)e.getSource();
							MasterDataCollectController.this.getSubFormsLoader().startLoading(subform.getEntityUID());
						}
					});
			}
		}
  	  };
      for (UID subFormEntityUid : mpsubformctlDetails.keySet()) {
         DetailsSubFormController<PK,CollectableEntityObject<PK>> subformcontroller = mpsubformctlDetails.get(subFormEntityUid);
         SubForm subform = subformcontroller.getSubForm();
         if (subformcontroller instanceof MasterDataSubFormController) {
        	subform.addParameterListener(changeListener);
            subformcontroller.setCollectController(MasterDataCollectController.this);
            mpSubFormController.put(subform, (MasterDataSubFormController<PK>)subformcontroller);
         }
      }

		// add child subforms to their parents
		for (SubForm subform : mpSubFormController.keySet()) {
			SubForm parentsubform = mpSubForm.get(subform.getParentSubForm());
			if (parentsubform != null) {
				MasterDataSubFormController<PK> subformcontroller = mpSubFormController.get(parentsubform);
				subformcontroller.addChildSubFormController(mpSubFormController.get(subform));
			}
		}
	}

	@Override
	public EditView newSearchEditView(LayoutRoot layoutroot) {
		return DefaultEditView.newSearchEditView(layoutroot.getRootComponent(), layoutroot, layoutroot.getInitialFocusEntityAndField());
	}
	
	protected LayoutRoot<PK> newLayoutRoot() {
		return newLayoutRoot(false);
	}
	
	protected LayoutRoot<PK> newSearchLayoutRoot() {
		return newLayoutRoot(true);
	}

	LayoutRoot<PK> newLayoutRoot(boolean bSearch) {
		final LayoutMLParser parser = new LayoutMLParser();

		final CollectableMasterDataEntity clcte = this.getCollectableEntity();
		final UID entityUid = clcte.getUID();
		LayoutRoot<PK> result;

		try {
			final Reader reader = MasterDataLayoutHelper.getLayoutMLReader(entityUid, bSearch, getCustomUsage());
			final UID layoutUID = MasterDataDelegate.getInstance().getLayoutUid(entityUid, bSearch, getCustomUsage());

			final InputSource isrc = new InputSource(new BufferedReader(reader));

			try {
				boolean bDetailModeAnyNew = bSearch ? false : isDetailmodeAnyNew();
				result = parser.getResult(layoutUID, isrc, clcte, bSearch, !bDetailModeAnyNew, getLayoutMLButtonsActionListener(), MasterDataCollectableFieldsProviderFactory.newFactory(entityUid, valueListProviderCache), CollectableComponentFactory.getInstance());

				if (bSearch) {
					for (CollectableComponent comp : result.getCollectableComponents()) {
						comp.getControlComponent().addFocusListener(collectableComponentSearchFocusListener);
					}
				}
				
				for (CollectableComponent c : result.getCollectableComponentsFor(SF.MANDATOR_UID.getUID(entityUid))) {
					c.setEnabled(bSearch);
					if (c instanceof CollectableComponentWithValueListProvider) {
						CollectableComponentWithValueListProvider cvlp = (CollectableComponentWithValueListProvider)c;
						if (cvlp.getValueListProvider() instanceof MandatorCollectableFieldsProvider) {
							cvlp.getValueListProvider().setParameter(MandatorCollectableFieldsProvider.ENTITY_UID, entityUid);
							cvlp.refreshValueList();
						}
					}
				}

				// Clear default keymaps of all JSplitPanes for consistent
				// hotkey handling:
				UIUtils.clearJComponentKeymap(result.getRootComponent(), JSplitPane.class);

				result.getRootComponent().setFocusCycleRoot(true);
			}
			catch (LayoutMLException ex) {
				throw new NuclosFatalException(ex);
			}
			catch (IOException ex) {
				throw new NuclosFatalException(ex);
			}
		}
		catch (NuclosBusinessException ex) {
			Errors.getInstance().showExceptionDialog(getTab(), ex);
			result = LayoutRoot.newEmptyLayoutRoot(true);
		}

		return result;
	}

	@Override
	protected void selectTab() {
		super.selectTab();

		setInitialComponentFocusInSearchTab();
	}

	@Override
	protected void setInitialComponentFocusInSearchTab() {
		if (this.isSearchPanelAvailable()) {
			setInitialComponentFocusThreaded(getSearchEditView(), mpsubformctlDetails);
		}
	}

	@Override
	protected void setInitialComponentFocusInDetailsTab() {
		setInitialComponentFocusThreaded(getDetailsEditView(), mpsubformctlDetails);
	}

	/**
	 * closes all subform controllers.
	 */
	private void closeSubFormControllers() {
		for (DetailsSubFormController<?,?> subformctl : this.getSubFormControllersInDetails()) {
			subformctl.close();
		}
	}

	@Override
	public MasterDataSearchPanel<PK> getSearchPanel() {
		return (MasterDataSearchPanel<PK>) super.getSearchPanel();
	}

	@Override
	public NuclosResultPanel<PK,CollectableMasterDataWithDependants<PK>> getResultPanel() {
		return (NuclosResultPanel<PK,CollectableMasterDataWithDependants<PK>>) super.getResultPanel();
	}

	@Override
	public MasterDataDetailsPanel<PK> getDetailsPanel() {
		return (MasterDataDetailsPanel) super.getDetailsPanel();
	}

	@Override
	protected void prepareCollectableForSaving(CollectableMasterDataWithDependants<PK> clctCurrent, CollectableEntity clcteCurrent) {
		super.prepareCollectableForSaving(clctCurrent, clcteCurrent);
	}
	
	@Override	
	protected LayoutRoot getLayoutRoot() {
		return getDetailsPanel().getLayoutRoot();
	}

	/**
	 * creates subform controllers for all given subforms.
	 *
	 * @param mpSubForms
	 * @return Map&lt;UID, SubForm&gt;
	 */
	protected Map<UID, DetailsSubFormController<PK,CollectableEntityObject<PK>>> newDetailsSubFormControllers(Map<UID, SubForm> mpSubForms) {
		final DetailsEditModel editmodelDetails = getDetailsPanel().getEditModel();
		Map<UID, DetailsSubFormController<PK,CollectableEntityObject<PK>>> result = CollectionUtils.transformMap(
				mpSubForms, new Transformer<SubForm, DetailsSubFormController<PK,CollectableEntityObject<PK>>>() {
			@Override
			public DetailsSubFormController<PK,CollectableEntityObject<PK>> transform(SubForm subform) {
				return newDetailsSubFormController(subform, getEntityUid(), editmodelDetails);
			}
		});
		getDetailsController().setSubFormControllers(result.values());
		return result;
	}

	/**
	 * Create a subform controller for a given subform
	 *
	 * §postcondition result != null
	 * 
	 * @param subform
	 * @param parentEntityUid
	 * @param clctcompmodelprovider
	 * @return the <code>DetailsSubFormController</code> belonging to the given
	 *         entity, if any.
	 */
	protected DetailsSubFormController<PK,CollectableEntityObject<PK>> newDetailsSubFormController(
			SubForm subform, UID parentEntityUid, CollectableComponentModelProvider clctcompmodelprovider) {

		// if parent of subform is another subform, change given parent entity
		// name
		UID parentSubFormUid = subform.getParentSubForm();
		if (parentSubFormUid != null) {
			parentEntityUid = parentSubFormUid;
		}

		return newDetailsSubFormController(subform, parentEntityUid, clctcompmodelprovider, this.getTab(), this.getDetailsPanel(), this.getPreferences(), this.getEntityPreferences(), this);
	}

	/**
	 * @return the <code>DetailsSubFormController</code> belonging to the given
	 *         entity, if any.
	 */
	public DetailsSubFormController<PK,CollectableEntityObject<PK>> getSubFormController(UID entityUid) {
		final Map<UID, DetailsSubFormController<PK,CollectableEntityObject<PK>>> map = getMapOfSubFormControllersInDetails();
		// can happend if the controller is already closed (tp)
		if (map == null) {
			return null;
		}
		return map.get(entityUid);
	}

	@Override
	protected void cloneSelectedCollectable() throws CommonBusinessException {
		final CollectableMasterDataWithDependants clctBackup = this.getCompleteSelectedCollectable(false, false);
		this.enterNewChangedMode();

		EntityObjectVO<PK> eovoClone = EntityObjectDelegate.getInstance().clone(clctBackup.getMasterDataWithDependantsCVO().getEntityObject(), null, false);
		CollectableMasterDataWithDependants clctClone = new CollectableMasterDataWithDependants(clctBackup.getCollectableEntity(), new MasterDataVO(eovoClone));

		unsafeFillDetailsPanel(clctClone);
	}

	/**
	 * @deprecated Move to DetailsController hierarchy and make protected again.
	 */
	@Override
	public void addAdditionalChangeListenersForDetails() {
		for (DetailsSubFormController<PK,CollectableEntityObject<PK>> subformctl : this.getSubFormControllersInDetails()) {
			subformctl.getSubForm().addChangeListener(this.changelistenerDetailsChanged);
		}
	}

	/**
	 * @deprecated Move to DetailsController and make protected again.
	 */
	@Override
	public void removeAdditionalChangeListenersForDetails() {
		for (DetailsSubFormController<PK,CollectableEntityObject<PK>> subformctl : getSubFormControllersInDetails()) {
			if (subformctl != null) {
				final SubForm sf = subformctl.getSubForm();
				if (sf != null) {
					sf.removeChangeListener(changelistenerDetailsChanged);
				}
			}
		}
	}

	@Override
	protected boolean stopEditingInDetails() {
		boolean result = super.stopEditingInDetails();
		if (result) {
			for (SubFormController subformctl : this.getSubFormControllersInDetails()) {
				result = result && subformctl.stopEditing();
			}
		}
		return result;
	}

	@Override
	protected boolean isDetailsModeViewLoadingWithoutDependants() {
		// Async loading implemented, but not in use (NUCLOS-5023)
		if (E.isNuclosEntity(getEntityUid())) {
			return super.isDetailsModeViewLoadingWithoutDependants();
		}
		return true;
	}

	@Override
	protected void unsafeFillDetailsPanel(final CollectableMasterDataWithDependants<PK> clct) throws NuclosBusinessException {
		/** @todo use super.unsafeFillDetailsPanel */
		
		// stop loading subforms (NUCLOS-5023)
		getSubFormsLoader().interruptAllClientWorkers();

		try {
			
			if (isHistoricalView()) {
				mdwdCurrent = new CollectableMasterDataWithDependants<PK>(
						getCollectableEntity(), mddelegate.getWithDependants(
								entityUid, getSelectedCollectableId(), -1));
			}
			
			MetaProvider metaprov = MetaProvider.getInstance();
			UID dataMandator = null;
			if (metaprov.getEntity(entityUid).isMandator()) {
				dataMandator = (UID) clct.getValueId(SF.MANDATOR_UID.getUID(entityUid));
			}
			
			for (UID fieldUid : getDetailsPanel().getLayoutRoot().getOrderedFieldUIDs()) {
				LOG.trace("sFieldName = " + fieldUid);
				final CollectableField clctfShown = clct.getField(fieldUid);
				setParameterMatrixVLP(fieldUid, clctfShown);
				
				for (CollectableComponent cc : getDetailsPanel().getLayoutRoot().getCollectableComponentsFor(fieldUid)) {
					if (cc instanceof CollectableComponentWithValueListProvider) {
						CollectableComponentWithValueListProvider ccwvlp = (CollectableComponentWithValueListProvider) cc;
						if (VLPClientUtils.setVLPBaseRestrictions(fieldUid, ccwvlp.getValueListProvider(), clct.getId(), dataMandator)) {
							ccwvlp.refreshValueList(true);
						}
					} else if (cc instanceof CollectableLocalizedComponent) {
						CollectableLocalizedComponent<PK> locTxtfield = (CollectableLocalizedComponent<PK>) cc;
						locTxtfield.getDetailsComponentModel().setPrimaryKey(clct.getPrimaryKey());
						if (isFillDuringClone())  {
							locTxtfield.getDetailsComponentModel().initDataLanguageMap(
									clct.getMasterDataCVO().getDataLanguageMap().copy(), getTab());							
						} else {
							locTxtfield.getDetailsComponentModel().initDataLanguageMap(
									clct.getMasterDataCVO().getDataLanguageMap(), getTab());
						}
						
					}
				}
				
				// iterate over the models rather than over the components:
				final CollectableComponentModel clctcompmodel = getDetailsPanel().getLayoutRoot().getCollectableComponentModelFor(fieldUid);
				if (isDetailmodeAnyNew()) {
					//if (!clctfShown.isNull()) //@see NUCLOS-1550
					if (isFillDuringClone() || isDetailsmodeNewChanged()) {
						clctcompmodel.setFieldInitial(clctfShown, true, false);
					} else {
						clctcompmodel.setField(clctfShown, false);  //@see NUCLOS-704
					}
				}
				else {
					clctcompmodel.setFieldInitial(clctfShown, true, true);					
				}
				
				// NUCLOS-1477
				final String dcType = clctcompmodel.getEntityField().getDefaultComponentType();
				if (DefaultComponentTypes.AUTONUMBER.equals(dcType)) {
					final Collection<CollectableComponent> clctComponents  = this.getDetailsPanel().getLayoutRoot().getCollectableComponentsFor(fieldUid);
					doAutoNumberStuff(clctComponents);
				}
				
				if (isHistoricalView()) {
					markFieldInHistoricalView(this.getDetailsPanel().getLayoutRoot(), mdwdCurrent, fieldUid, clctfShown);
				}
			}
			
			// update Layout Components
			notifyLayoutComponentListeners(getDetailsPanel().getLayoutRoot(), true);
			
			// fill subforms:
			if (clct.getId() != null) {
				UIUtils.invokeOnDispatchThread(new Runnable() {
					@Override
					public void run() {
						MasterDataCollectController.this.getSubFormsLoader().startLoading();
					}
				});
			}
			for (final DetailsSubFormController<PK,CollectableEntityObject<PK>> subformctl : getSubFormControllersInDetails()) {
				subformctl.setMandatorUID(dataMandator);
				
				if (clct.getId() instanceof Long) {
					subformctl.setIntidForVLP((Long)clct.getId());
				}

				// by object generation or clone
				final IDependentDataMap dependents = clct.getDependantMasterDataMap();
				final UID foreignKeyFieldUID = subformctl.getForeignKeyFieldUID();
				final FieldMeta<?> foreignKeyField = MetaProvider.getInstance().getEntityField(foreignKeyFieldUID);
				if (clct.getId() == null && !dependents.getData(foreignKeyField).isEmpty()) {
					final MasterDataSubFormController<PK> mdsubformctl = (MasterDataSubFormController<PK>) subformctl;
					final Collection<EntityObjectVO<PK>> sub = dependents.getDataPk(foreignKeyField);
					mdsubformctl.fillSubForm(null, sub);
					mdsubformctl.getSubForm().setNewEnabled(
							new CollectControllerScriptContext<>(
									this,
									new ArrayList<>(getSubFormControllersInDetails())
							)
					);
				}
				else if (clct.getId() == null) {
					final MasterDataSubFormController<PK> mdsubformctl = (MasterDataSubFormController<PK>) subformctl;
					mdsubformctl.clear();
					mdsubformctl.fillSubForm(null, new ArrayList<>());
					mdsubformctl.getSubForm().setNewEnabled(
							new CollectControllerScriptContext<>(
									this,
									new ArrayList<>(getSubFormControllersInDetails())
							)
					);
				}
				else {
					if (((MasterDataSubFormController<PK>) subformctl).isChildSubForm())
						continue;
					
					SubFormsInterruptableClientWorker sfClientWorker = new SubFormsLoaderClientWorker<>(subformctl, this, clct, null, mdwdCurrent);
	
					MasterDataCollectController.this.getSubFormsLoader().addSubFormClientWorker(
							subformctl.getCollectableEntity().getUID(), sfClientWorker);
	
					getLayoutMLButtonsActionListener().fireComponentEnabledStateUpdate(false);
				}
			}
			
			loadMatrices(clct, null);
			
			//@see NUCLOS-1391
			UIUtils.invokeOnDispatchThread(new Runnable() {
				@Override
				public void run() {
					ScriptContext ctx = new CollectControllerScriptContext<PK>(
							MasterDataCollectController.this, new ArrayList<DetailsSubFormController<PK,?>>(getSubFormControllersInDetails()));
					if (getDetailsPanel() != null) {
						final EditView view = getDetailsPanel().getEditView();
						if (view != null) {
							for (CollectableComponent c : view.getCollectableComponents()) {
								c.setComponentState(ctx, null);
								c.setBackgroundColor(ctx, null);
							}
						}
					}
					
					List<JTabbedPane> lst = new ArrayList<JTabbedPane>();
					searchTabbedPanes(getLayoutRoot().getRootComponent(), lst);
					
					for (JTabbedPane tabPane : lst) {
						if (tabPane instanceof JInfoTabbedPane && tabPane.getTabCount() > 0) {
							((JInfoTabbedPane)tabPane).notifyForInitialTabSelection();
						}
					}
					
				}
			});
			
		}
		catch (CommonFinderException e) {
			throw new NuclosFatalException(e);
		}
		catch (CommonPermissionException e) {
			throw new NuclosFatalException(e);
		}
		showMandator(clct);
		updateLockState(clct);
	}

	@Override
	protected void unsafeFillMultiEditDetailsPanel(Collection<CollectableMasterDataWithDependants<PK>> collclct) throws CommonBusinessException {
		
		UID dataMandator = getMandatorUID(collclct);
		
		for (UID fieldUid : getDetailsPanel().getLayoutRoot().getOrderedFieldUIDs()) {
			for (CollectableComponent cc : getDetailsPanel().getLayoutRoot().getCollectableComponentsFor(fieldUid)) {
				if (cc instanceof CollectableComponentWithValueListProvider) {
					CollectableComponentWithValueListProvider ccwvlp = (CollectableComponentWithValueListProvider) cc;
					if (VLPClientUtils.setVLPBaseRestrictions(fieldUid, ccwvlp.getValueListProvider(), new Long(-1), dataMandator)) {
						ccwvlp.refreshValueList(true);
					}
				}
			}
		}
		
		for (CollectableMasterDataWithDependants<PK> clct : collclct) {
			for (UID fieldUid : getDetailsPanel().getLayoutRoot().getOrderedFieldUIDs()) {
				for (CollectableComponent cc : getDetailsPanel().getLayoutRoot().getCollectableComponentsFor(fieldUid)) {
					if (cc instanceof CollectableLocalizedComponent) {
						CollectableLocalizedComponent<PK> locTxtfield = (CollectableLocalizedComponent<PK>) cc;
						locTxtfield.getDetailsComponentModel().setPrimaryKey(clct.getPrimaryKey());
						if (isFillDuringClone())  {
							locTxtfield.getDetailsComponentModel().initDataLanguageMap(
									clct.getMasterDataCVO().getDataLanguageMap().copy(), getTab());							
						} else {
							locTxtfield.getDetailsComponentModel().initDataLanguageMap(
									clct.getMasterDataCVO().getDataLanguageMap(), getTab());
						}
					}
				}
			}
		}
		
		// fill the Details panel with the common values:
		super.unsafeFillMultiEditDetailsPanel(collclct);

		
		// update Layout Components
		notifyLayoutComponentListeners(getDetailsPanel().getLayoutRoot(), false);
					
		// begin multi-update of dependants:
		this.multiupdateofdependants = new MultiUpdateOfDependants<PK>(getSubFormControllersInDetails(), collclct);

		getLayoutMLButtonsActionListener().setComponentsEnabled(false);
		getLayoutMLButtonsActionListener().setRuleButtonComponentsEnabled(true);
	}

	@Override
	public CollectableMasterDataWithDependants<PK> newCollectable() {
		final CollectableMasterDataEntity clctmde = this.getCollectableEntity();
		return CollectableMasterDataWithDependants.newInstance(clctmde, new MasterDataVO<PK>(clctmde.getMeta(), false));
	}
	
	protected boolean isCloneAllowed() {
		if (!this.getCollectableEntity().getMeta().isEditable()) {
			return false;
		}
		if (!SecurityCache.getInstance().isWriteAllowedForMasterData(getEntityUid())) {
			return false;
		}
		if (!isNotLoadingSubForms()) {
			return false;
		}
		return true;
	}
	
	/**
	 * Some master data entities may never be written to. Others require the
	 * right to write.
	 * @param useCurrentCollectable
	 */
	@Override
	protected boolean isSaveAllowed(final boolean useCurrentCollectable) {
		if (!this.getCollectableEntity().getMeta().isEditable()) {
			return false;
		}
		if (!SecurityCache.getInstance().isWriteAllowedForMasterData(getEntityUid())) {
			return false;
		}

		// REMOVED FOR NUCLOS-7782
		//if (!isNotLoadingSubForms()) {
		//	return false;
		//}

		// check selected only, if not in new mode
		CollectState state = getCollectState();
		if (state.getInnerState() != CollectState.DETAILSMODE_NEW &&
			state.getInnerState() != CollectState.DETAILSMODE_NEW_CHANGED) {
			CollectableMasterDataWithDependants selected = getSelectedCollectable();
			if (selected != null) {
				return selected.getMasterDataCVO().getEntityObject().canWrite();
			}
		}
		
		return true;
//		return this.getCollectableEntity().getEntityMeta().isEditable() 
//				&& SecurityCache.getInstance().isWriteAllowedForMasterData(getEntityUid())
//				&& ()getSelectedCollectable() != null && getSelectedCollectable().getMasterDataCVO().getEntityObject().canWrite()
//				&& isNotLoadingSubForms();
	}

	@Override
	protected boolean isDeleteSelectedCollectableAllowed() {
		return isDeleteAllowed(getSelectedCollectable());
	}
	
	/**
	 * generic objects may deleted if we are not in historical view <code>AND</code>
	 * ((Delete is allowed for this module and the current user and the user has the right to write the object)
	 * <code>OR</code> this object is in its initial state and the current user created this object.)
	 * @param clct
	 * @return Is the "Delete" action for the given Collectable allowed?
	 */
	@Override
	protected boolean isDeleteAllowed(CollectableMasterDataWithDependants<PK> clct) {
		return clct != null && !this.isHistoricalView() 
				&& clct.getMasterDataCVO().getEntityObject().canDelete()
					&& SecurityCache.getInstance().isDeleteAllowedForMasterData(getEntityUid()) 
						&& MasterDataDelegate.getInstance().getMetaData(getEntityUid()).isEditable();
	}

	/**
	 * §precondition clct != null
	 * 
	 * @return Is the "Read" action for the given Collectable allowed? May be
	 *         overridden by subclasses.
	 */
	@Override
	protected boolean isReadAllowed(CollectableMasterDataWithDependants<PK> clct) {
		return SecurityCache.getInstance().isReadAllowedForMasterData(getEntityUid());
	}

	/**
	 * §precondition clct != null
	 * 
	 * @return Is the "Read" action for the given set of Collectables allowed?
	 *         May be overridden by subclasses.
	 */
	@Override
	protected boolean isReadAllowed(List<CollectableMasterDataWithDependants<PK>> lsClct) {
		return lsClct.isEmpty() || SecurityCache.getInstance().isReadAllowedForMasterData(getEntityUid());
	}

	/**
	 * @return true
	 */
	@Override
	protected boolean isMultiEditAllowed() {
		return super.isMultiEditAllowed();
	}

	boolean isFilteringAppropriate() {
		return hasActiveSign() || hasValidityDate();
	}

	public boolean hasValidityDate() {
		Map<UID, FieldMeta<?>>  fields=MetaProvider.getInstance().getAllEntityFieldsByEntity(getCollectableEntity().getUID());
		boolean hasFrom=false;
		boolean hasUntil=false;
		for(FieldMeta meta:fields.values())
		{
			if(meta.getFieldName().toLowerCase().equals(FIELDNAME_VALIDFROM.toLowerCase()))
				hasFrom=true;
			if(meta.getFieldName().toLowerCase().equals(FIELDNAME_VALIDUNTIL.toLowerCase()))
				hasUntil=true;
		}
		return (hasUntil && hasFrom);
	}

	private boolean hasActiveSign() {
		return getCollectableEntity().getFieldUIDs().contains(FIELDNAME_ACTIVE);
	}

	public boolean isFilteringDesired() {							
		return !this.showInvalidMasterData && isFilteringAppropriate();
	}

	@Override
	public void runLookupCollectable(ICollectableListOfValues<PK> clctlovSource) throws CommonBusinessException {
		final Boolean bbFilterValidity = (Boolean) clctlovSource.getProperty(NuclosCollectableListOfValues.PROPERTY_FILTER_VALIDITY);
		if (LangUtils.defaultIfNull(bbFilterValidity, false)) {
			// When opened as a search box and the component does not forbid it,
			// valid filter is always active and checkbox is not shown
			hideInvalidityFilter();
		}

		super.runLookupCollectable(clctlovSource);
	}

	/**
	 * alternative entry point: view single historical object
	 * @param clct the object to view in details
	 */
	@Override
	public final void runViewSingleHistoricalCollectable(CollectableWithDependants<PK> clct, Date dateHistorical, boolean bUseNewHistory) {
		viewSingleHistoricalCollectable(clct, dateHistorical);
		
		try {
			// switch to edit mode:
			setCollectState(CollectState.OUTERSTATE_DETAILS, CollectState.DETAILSMODE_EDIT);
		} catch (CommonBusinessException e) {
			LOG.warn("Error setting detailsmode edit.", e);
		}			
		
		getTab().setVisible(true);
	}

	private void viewSingleHistoricalCollectable(CollectableWithDependants<PK> clct, Date dateHistorical) {
		// fill result table:
		final List<CollectableMasterDataWithDependants<PK>> lstResult = new ArrayList<CollectableMasterDataWithDependants<PK>>();
		lstResult.add((CollectableMasterDataWithDependants<PK>)clct);
		this.fillResultPanel(lstResult);

		getCollectPanel().getResultPanel().getResultTable().setRowSelectionInterval(0, 0);
		// select the one result row

		setHistoricalDate(dateHistorical);

		cmdEnterViewMode();
		disableToolbarButtonsForHistoricalView();
	}

	private void disableToolbarButtonsForHistoricalView() {
		final CollectPanel<PK,CollectableMasterDataWithDependants<PK>> collectPanel = getCollectPanel();
		collectPanel.setTabbedPaneEnabledAt(CollectState.OUTERSTATE_SEARCH, false);
		collectPanel.setTabbedPaneEnabledAt(CollectState.OUTERSTATE_RESULT, false);
		getDetailsController().getDeleteCurrentCollectableAction().setEnabled(false);
		getCloneAction().setEnabled(false);
		actExecuteRule.setEnabled(false);
		actMakeTreeRoot.setEnabled(false);
		getRefreshCurrentCollectableAction().setEnabled(false);
		actShowHistory.setEnabled(false);
		getLastAction().setEnabled(false);
		getFirstAction().setEnabled(false);
		getNewAction().setEnabled(false);
	}

	protected void hideInvalidityFilter() {
		if (isSearchPanelAvailable()) {
			final MasterDataSearchPanel<PK> searchPanel = getSearchPanel();
			searchPanel.filterByValidity(true);
		}
	}

	/**
	 * @return a specific table model, with support for chunkwise reading.
	 *
	 * @deprecated Move to ResultController hierarchy.
	 */
	@Override
	protected SortableCollectableTableModel<PK,CollectableMasterDataWithDependants<PK>> newResultTableModel() {
		final SortableCollectableTableModel<PK,CollectableMasterDataWithDependants<PK>> result = super.newResultTableModel();

		// clicking a column header is to cause a new search on the server:
		TableUtils.removeMouseListenersForSortingFromTableHeader(this.getResultTable());
		TableUtils.addMouseListenerForSortingToTableHeader(this.getResultTable(), result, new CommonRunnable() {
			@Override
			public void run() {
				getResultController().getSearchResultStrategy().cmdRefreshResult();
			}
		});

		return result;
	}

	/**
	 * sets up the change listener for the vertical scrollbar of the result
	 * table, only if the proxy list has been set (that is not before the first
	 * search).
	 */
	public void setupChangeListenerForResultTableVerticalScrollBar() {
		final ProxyList<PK,? extends Collectable<PK>> lstclct = getSearchStrategy().getCollectableProxyList();
		if (lstclct != null) {
			this.getResultPanel().setupChangeListenerForResultTableVerticalScrollBar(lstclct, this.getTab());
		}
	}

	public void removePreviousChangeListenersForResultTableVerticalScrollBar() {
		final JScrollBar scrlbarVertical = this.getResultPanel().getResultTableScrollPane().getVerticalScrollBar();
		final DefaultBoundedRangeModel model = (DefaultBoundedRangeModel) scrlbarVertical.getModel();
		NuclosResultPanel.removePreviousChangeListenersForResultTableVerticalScrollBar(model);
	}

	@Override
	public CollectableMasterDataWithDependants<PK> findCollectableById(UID entityUid, PK oId, final int dependantsDepth) throws CommonBusinessException {
		assert getCollectableEntity() != null;
		final MasterDataVO<PK> mdwdcvo = mddelegate.getWithDependants(entityUid, oId, dependantsDepth);
		final CollectableMasterDataWithDependants<PK> result = new CollectableMasterDataWithDependants<PK>(
				getCollectableEntity(), mdwdcvo);
		return result;
	}
	
	@Override
	protected CollectableMasterDataWithDependants<PK> findCollectableById(UID entityUid, PK oId, Collection<FieldMeta<?>> fields, final int dependantsDepth) throws CommonBusinessException {
		if (fields == null) {
			return findCollectableById(entityUid, oId, dependantsDepth);
		}
		
		Collection<UID> fieldUids = new HashSet<UID>();
		for (FieldMeta<?> fm : fields) {
			fieldUids.add(fm.getUID());
		}

		EntityObjectVO<PK> eovo = EntityObjectDelegate.getInstance().getByIdWithDependents(entityUid, oId, fieldUids, getCustomUsage());
		
		if (eovo == null) {
			return null;
		} 
		
		return new CollectableMasterDataWithDependants<PK>(
				getCollectableEntity(), DalSupportForMD.wrapEntityObjectVO(eovo));
	}

	@Override
	protected CollectableMasterDataWithDependants<PK> findCollectableByIdWithoutDependants(UID entityUid, PK oId) throws CommonBusinessException {
		assert this.getCollectableEntity() != null;

		final MasterDataVO<PK> mdcvo = mddelegate.get(entityUid, oId);
		final CollectableMasterDataWithDependants<PK> result = CollectableMasterDataWithDependants.newInstance(
				getCollectableEntity(), mdcvo);
		assert result.isComplete();
		return result;
	}

	@Override
	public Integer getVersionOfCollectableById(UID entityUid, PK oId) throws CommonBusinessException {
		if (entityUid == null) {
			throw new IllegalArgumentException("sEntityName");
		}
		if (oId == null) {
			throw new IllegalArgumentException("oId");
		}

		return this.mddelegate.getVersion(entityUid, oId);
	}

	/**
	 * @todo move to DetailsPanel/DetailsController
	 */
	private List<EntityAndField> getEntityAndForeignKeyFieldNamesFromSubForms() {
		final List<SubFormController> lstSubformControllers = new ArrayList<SubFormController>();
		for (SubFormController subFormController : getSubFormControllersInDetails()) {
			if (LangUtils.equal(subFormController.getParentEntityUID(), getEntityUid()))
				lstSubformControllers.add(subFormController);	
		}
		return CollectionUtils.transform(lstSubformControllers, new GetEntityAndForeignKeyFieldName());
	}

	/** 
	 * §todo this is just a workaround 
	 */
	protected IDependentDataMap readDependants(PK oId) throws CommonBusinessException {
		return this.mddelegate.getWithDependants(getEntityUid(), oId, -1).getDependents();
	}

	@Override
	protected CollectableMasterDataWithDependants<PK> insertCollectable(final CollectableMasterDataWithDependants<PK> clctNew) throws CommonBusinessException {
		if (clctNew.getId() != null) {
			throw new IllegalArgumentException("clctNew");
		}

		// We have to clear the ids for cloned objects:
		/**
		 * §todo eliminate this workaround - this is the wrong place. The right
		 *       place is the Clone action!
		 */
		final IDependentDataMap mpmdvoDependants = org.nuclos.common.Utils.clearIds(getAllSubFormData(null).toDependentDataMap());
		
		this.getAllMatrixData(mpmdvoDependants);	
		
		final AtomicReference<MasterDataVO<PK>> mdvoInserted = new AtomicReference<MasterDataVO<PK>>();
		
		invoke(new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				try {
					mdvoInserted.set(mddelegate.create(getEntityUid(), clctNew.getMasterDataCVO(), mpmdvoDependants, getCustomUsage()));
					clearRemovedMatrixData();
				} catch (CommonBusinessException cbe) {
					handleBusinessExceptionAfterSafe(cbe);
					throw cbe;
				}
			}
		}, null);
		
		return new CollectableMasterDataWithDependants<PK>(clctNew.getCollectableEntity(), 
				new MasterDataVO<PK>(mdvoInserted.get(), readDependants(mdvoInserted.get().getId())));
	}

	@Override
	protected CollectableMasterDataWithDependants<PK> updateCurrentCollectable(
			CollectableMasterDataWithDependants<PK> clctCurrent) throws CommonBusinessException {
		CollectableMasterDataWithDependants<PK> result;
		if (!isHistoricalView()) {
			LOG.debug("updateCollectable START");
			result = updateCollectable(clctCurrent, getAllSubFormData(clctCurrent.getId()), null);
			LOG.debug("updateCollectable DONE");
		}
		else {
			// Die Daten im Subform sind nicht aktuell sondern von der vorletzen Version
			// (da sie aus der Historie ermittelt werden, welche immer nur die vorletzte Version enthält).
			// Deshalb wird die aktuelle Version neu aus der DB geladen aber das remove-Flag der einzelnen
			// Datensätze aus dem Subform übernommen.
			// Das ganze ist nötig, damit beim Zurücksetzen auf einen älteren Stand die aktuellsten Subform-Daten
			// in die Historie geschrieben werden.
			DependantCollectableMasterDataMap subFormDependants = getAllSubFormData(clctCurrent.getId());
			DependantCollectableMasterDataMap dbDependants = new DependantCollectableMasterDataMap(readDependants(clctCurrent.getId()));
			for (IDependentKey dependentKey : dbDependants.getKeySet()) {
				for (CollectableEntityObject<?> dbObject : dbDependants.getValues(dependentKey)) {
					CollectableEntityObject<?> subformObject = subFormDependants.getValue(dependentKey, dbObject.getId());
					if (subformObject == null || subformObject.isMarkedRemoved()) {
						dbObject.markRemoved();
					}
				}
			}
			result = updateCollectable(clctCurrent, dbDependants, null);
		}
		return result;
	}

	
	protected CollectableMasterDataWithDependants<PK> updateCollectable(final CollectableMasterDataWithDependants<PK> clct, Object oAdditionalData, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		boolean isCollectiveProcessing = false;
		//TODO decide if collective processing
		return updateCollectable(clct, oAdditionalData, applyMultiEditContext, isCollectiveProcessing);
	}
	
	@Override
	protected CollectableMasterDataWithDependants<PK> updateCollectable(final CollectableMasterDataWithDependants<PK> clct, Object oAdditionalData, final Map<String, Serializable> applyMultiEditContext, final boolean isCollectiveProcessing) throws CommonBusinessException {
	final DependantCollectableMasterDataMap mpclctDependants = (DependantCollectableMasterDataMap) oAdditionalData;

	
	
	final AtomicReference<PK> oId = new AtomicReference<PK>();
	
	final IDependentDataMap mpmdvoDependants = mpclctDependants.toDependentDataMap();

	getAllMatrixData(mpmdvoDependants);
	
	final UID sMatrixPreferencesField = getMatrixPreferencesField();
	if(sMatrixPreferencesField != null) {
		String sPrefs = getMatrixPreferences();
		if(sPrefs != null) {
			clct.setField(sMatrixPreferencesField, new CollectableValueField(sPrefs));
		}
	}

	// read the datalanguage contents from the model annd merge it into the clct
		if(isCollectiveProcessing)
		{
			final EditView view = this.getEditView(false);
			for (CollectableComponent clctcomp : view.getCollectableComponents()) {				
				if (clctcomp.getEntityField().isLocalized()) {
                    if(clctcomp.isReadOnly()) 
                    	continue; 
					DetailsLocalizedComponentModel model = (DetailsLocalizedComponentModel) clctcomp.getModel();						
					 clct.getMasterDataCVO().updateDataLanguageMap(model.getDataLanguageMap());									
				}
			}//for
		}//if
		
	invoke(new CommonRunnable() {
		@Override
		public void run() throws CommonBusinessException {
			try {
				oId.set(mddelegate.update(getEntityName(), clct.getMasterDataCVO(), mpmdvoDependants, getCustomUsage(), isCollectiveProcessing));
				clearRemovedMatrixData();
			} catch (CommonBusinessException cbe) {
				handleBusinessExceptionAfterSafe(cbe);
				throw cbe;
			}
		}
	}, applyMultiEditContext);

	final MasterDataVO<PK> mdvoUpdated = mddelegate.get(getEntityUid(), oId.get());
	return new CollectableMasterDataWithDependants<PK>(clct.getCollectableEntity(), mdvoUpdated);
	
	}
	

	@Override
	protected DependantCollectableMasterDataMap getAdditionalDataForMultiUpdate(CollectableMasterDataWithDependants<PK> clct) throws CommonValidationException {
		return multiupdateofdependants.getDependantCollectableMapForUpdate(this.getSubFormControllersInDetails(), clct);
	}

	@Override
	protected void deleteCollectable(final CollectableMasterDataWithDependants<PK> clct, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		invoke(new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				try {
					mddelegate.remove(getEntityUid(), clct.getMasterDataCVO(), getCustomUsage());
					clearRemovedMatrixData();
				} catch (CommonBusinessException cbe) {
					handleBusinessExceptionAfterSafe(cbe);
					throw cbe;
				}
			}
		}, applyMultiEditContext);
	}

	@Override
	protected String getEntityLabel() {
		return SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(getCollectableEntity().getMeta());
	}

	
	/**
	 * enables drag and copy from rows.
	 */
	protected void setupDataTransfer() {

		setupDropListener(getResultTable());
		// enable drag&drop:
		final JTable tbl = this.getResultTable();

		
		tbl.setDragEnabled(true);
		tbl.setTransferHandler(new TransferHandler() {

			@Override
			public int getSourceActions(JComponent comp) {
				int result = NONE;
				if (comp == tbl) {
					final Collectable<PK> clctSelected = getSelectedCollectable();
					if (clctSelected != null) {
						result = COPY;
					}
				}
				return result;
			}

			private MasterDataIdAndEntity<PK> getTransferableObject(final JTable tbl, int iSelectedRow, final CollectableMasterDataWithDependants clct) {
				return new MasterDataIdAndEntity<PK>((PK)clct.getMasterDataCVO().getPrimaryKey(), 
						MasterDataCollectController.this.getEntityName(), null, 
							SpringLocaleDelegate.getInstance().getIdentifierLabel(clct, clct.getEntityUID(), 
						    MetaProvider.getInstance(), getLanguage()));
			}

			@Override
			protected Transferable createTransferable(JComponent comp) {
				Transferable result = null;
				if (comp == tbl) {
					final int[] aiSelectedRows = tbl.getSelectedRows();
					final List<MasterDataIdAndEntity<PK>> lstimp = new ArrayList<MasterDataIdAndEntity<PK>>(aiSelectedRows.length);
					for (int iSelectedRow : aiSelectedRows) {
						final CollectableMasterDataWithDependants<PK> clct = getResultTableModel().getCollectable(iSelectedRow);
						MasterDataIdAndEntity<PK> transferableObject = getTransferableObject(tbl, iSelectedRow, clct);
						lstimp.add(transferableObject);
					}
					if (!lstimp.isEmpty())
						result = new MasterDataVOTransferable<PK>(lstimp, null);
				}
				return result;
			}			
		});
		
	}

	@Override
	protected void setupSubFormController(Map<UID, SubForm> mpSubForm, Map<UID, ? extends SubFormController> mpSubFormController) {
		Map<SubForm, MasterDataSubFormController<PK>> mpSubFormController_tmp = new HashMap<SubForm, MasterDataSubFormController<PK>>();

		// create a map of subforms and their controllers
		for (UID subFormEntityUid : mpSubFormController.keySet()) {
			SubFormController subformcontroller = mpSubFormController.get(subFormEntityUid);
			SubForm subform = subformcontroller.getSubForm();
			if (subformcontroller instanceof DetailsSubFormController<?,?>) {
				((DetailsSubFormController<PK,CollectableMasterDataWithDependants<PK>>) subformcontroller).setCollectController(this);
				mpSubFormController_tmp.put(subform, (MasterDataSubFormController<PK>) subformcontroller);
			}
			// disable child subforms in searchpanel, because it's not possible
			// to search for data in those subforms
			else if (subformcontroller instanceof SearchConditionSubFormController) {
				if (subform.getParentSubForm() != null)
					subform.setEnabled(false);
				
				if (MetaProvider.getInstance().getEntity(subFormEntityUid).isProxy()) {
					subform.setVisible(false);
				}
			}
		}

		// assign child subforms to their parents
		for (SubForm subform : mpSubFormController_tmp.keySet()) {
			SubForm parentsubform = mpSubForm.get(subform.getParentSubForm());
			if (parentsubform != null) {
				MasterDataSubFormController<PK> subformcontroller = mpSubFormController_tmp.get(parentsubform);
				subformcontroller.addChildSubFormController(mpSubFormController_tmp.get(subform));
			}
		}
	}

	@Override
	protected Collection<? extends SubFormController> getSubFormControllers(boolean bSearchTab) {
		return bSearchTab ? (mpsubformctlSearch==null ? new ArrayList() : mpsubformctlSearch.values()) : this.getSubFormControllersInDetails();
	}

	@Override
	protected Map<UID, DetailsSubFormController<PK,CollectableEntityObject<PK>>> getMapOfSubFormControllersInDetails() {
		return this.mpsubformctlDetails;
	}

	private class GetEntityAndForeignKeyFieldName implements Transformer<SubFormController, EntityAndField> {
		@Override
		public EntityAndField transform(SubFormController subformctl) {
			return subformctl.getEntityAndForeignKeyField();
		}
	}

	class MasterDataCollectPanel extends CollectPanel<PK,CollectableMasterDataWithDependants<PK>> {

		MasterDataCollectPanel(UID entityUid, boolean bSearchPanelAvailable, boolean bShowSearch, DetailsPresentation detailsPresentation, ControllerPresentation controllerPresentation) {
			super(entityUid, bSearchPanelAvailable, bShowSearch, detailsPresentation, controllerPresentation, getFilterAction());
		}

		@Override
		public SearchPanel newSearchPanel(UID entityUid) {
			/** @todo creating an empty search panel is just a workaround! */
			final LayoutRoot<PK> lroot;
			if (containsSearchPanel()) {
				lroot = newSearchLayoutRoot();
			} else {
				lroot = LayoutRoot.newEmptyLayoutRoot(true);
			}
			final MasterDataSearchPanel<PK> result = new MasterDataSearchPanel(entityUid, lroot);		
			result.initToolBar(ctlSearch, getTab(), isFilteringAppropriate());
			registerPanelForClosing(result);
			return result;
		}

		@Override
		public DetailsPanel newDetailsPanel(UID entityId) {
			final MasterDataDetailsPanel mddp = new MasterDataDetailsPanel(entityId, newLayoutRoot(false), getDetailsPresentation(), getViewMode(), detailsWithScrollbar);
			registerPanelForClosing(mddp);
			return LayoutComponentUtils.setPreferences(getEntityPreferences(), mddp);
		}


		@Override
		public ResultPanel<PK,CollectableMasterDataWithDependants<PK>> newResultPanel(UID entityId) {
			return new NuclosResultPanel<>(entityId, getViewMode(),
					MasterDataCollectController.this.getFilterAction(), MasterDataCollectController.this);
		}

	}

	protected static class MasterDataSearchPanel<PK2> extends SearchPanel {
		private final LayoutRoot<PK2> layoutRoot;

		/*
		MasterDataSearchPanel(UID entityUid) {
			this(entityUid, newLayoutRoot(true));
		}
		 */

		/**
		 * You MUST call {@link #initToolBar(SearchController, MainFrameTab, boolean)} after creation. 
		 */
		MasterDataSearchPanel(UID entityUid, LayoutRoot<PK2> layoutroot) {
			super(entityUid);
			this.layoutRoot = layoutroot;
			
			/** @todo this could be done in super(...) */
			this.setEditView(new LayoutMLEditView(layoutroot));

			// this.setCustomToolBarArea(newToolBar());
			// initToolBar();
		}

		public void initToolBar(final SearchController<PK2, ? extends Collectable<PK2>> ctlSearch, 
				final MainFrameTab tab, final boolean isFilteringAppropriate) {
				
		
		}

		/**
		 * Activate or deactivate filtering by active sign/validFrom - validUntil.
		 *
		 * @param bFilter
		 */
		public void filterByValidity(boolean bFilter) {
			for (FieldMeta<?> field : MetaProvider.getInstance().getEntity(entityId).getFields()) {
				if (field.getFieldName().equals(FIELDNAME_VALIDFROM) ||field.getFieldName().equals(FIELDNAME_VALIDFROM.toLowerCase()) 
						|| field.getFieldName().equals(FIELDNAME_VALIDUNTIL)	|| field.getFieldName().equals(FIELDNAME_VALIDUNTIL.toLowerCase())
						|| field.getFieldName().equals(FIELDNAME_ACTIVE)|| field.getFieldName().equals(FIELDNAME_ACTIVE.toLowerCase())) {
					clearAndToggleEnableComponentsInSearchTab(field.getUID(), bFilter);
				}
			}
		}

		/**
		 * §precondition this.isSearchPanelAvailable()
		 */
		private void clearAndToggleEnableComponentsInSearchTab(UID fieldUid, boolean bFilter) {
			for (CollectableComponent clctcomp : getEditView().getCollectableComponentsFor(fieldUid)) {
				if (bFilter) {
					clctcomp.clear();
				}
				clctcomp.setEnabled(!bFilter);
			}
		}

		private SpringLocaleDelegate getSpringLocaleDelegate() {
			return SpringLocaleDelegate.getInstance();
		}

	} // inner class MasterDataSearchPanel

	protected static class MasterDataDetailsPanel<PK2> extends DetailsPanel {

		/**
		 * Must be non-final to {@link #close()}
		 */
		@Deprecated
		private LayoutRoot<PK2> layoutroot;

		/*
		protected MasterDataDetailsPanel(UID entityUid) {
			this(entityUid, newLayoutRoot(false));
		}

		protected MasterDataDetailsPanel(UID entityUid, boolean withScrollbar) {
			this(entityUid, newLayoutRoot(false), withScrollbar);
		}
		 */

		@Deprecated
		private MasterDataDetailsPanel(UID entityId, LayoutRoot<PK2> layoutroot, DetailsPresentation detailPresentation, ControllerPresentation controllerPresentation) {
			this(entityId, layoutroot, detailPresentation, controllerPresentation, true);
		}

		@Deprecated
		MasterDataDetailsPanel(UID entityId, LayoutRoot<PK2> layoutroot, DetailsPresentation detailPresentation, ControllerPresentation controllerPresentation, boolean withScrollbar) {
			super(entityId, detailPresentation, controllerPresentation, withScrollbar);
			setLayoutRoot(layoutroot);
		}

		public LayoutRoot<PK2> getLayoutRoot() {
			return this.layoutroot;
		}
		
		void setLayoutRoot(LayoutRoot<PK2> layoutRoot) {
			this.layoutroot = layoutRoot;
			this.setEditView(new LayoutMLEditView(layoutroot));
		}

		/**
		 * §todo pull down to SearchOrDetailsPanel and/or change signature into
		 *       EditView newEditView()
		 *       
		 * @param compRoot
		 *            the edit component according to the LayoutML
		 * @return the edit component to be used in the Details panel. Default
		 *         is <code>compRoot</code> itself. Successors may build their
		 *         own component/panel out of compRoot.
		 */
		public JComponent newEditComponent(JComponent compRoot) {
			return compRoot;
		}
		
		@Override
		public void close() {
			super.close();
			layoutroot = null;
		}

	} // inner class MasterDataDetailsPanel
	
	/**
	 * Get the fixed part of the result table, if any.
	 */
	@Override
	public JTable getFixedResultTable() {
		return getResultPanel().getFixedResultTable();
	}

	private void cmdExportXml() {
		LOG.info("cmdExportXml pressed");
		final FileExportDialog fed = new FileExportDialog(getEntityUid());
		final String title = getSpringLocaleDelegate().getMsg("xml.fileexport.outputfile.title");
		final OverlayOptionPane oop = OverlayOptionPane.showConfirmDialog(getTab(), fed, title,
				OverlayOptionPane.OK_CANCEL_OPTION, true,
				new OvOpAdapter() {
					public void done(int result) {
						if (result == OverlayOptionPane.OK_OPTION) {
							OutputStream out = null;
							// final List<CollectableMasterDataWithDependants<PK>> list = getResultTableModel().getCollectables();
							try {
								out = new FileOutputStream(fed.getSelectedFile());
								final byte[] export = fed.export(getSearchConditionFromModel());
								out.write(export);
							} catch (CommonPermissionException e) {
								Errors.getInstance().showExceptionDialog(getTab(), e);
							} catch (IOException e) {
								Errors.getInstance().showExceptionDialog(getTab(), e);
							} catch (XMLStreamException e) {
								Errors.getInstance().showExceptionDialog(getTab(), e);
							} catch (Exception e) {
								Errors.getInstance().showExceptionDialog(getTab(), e);
							} finally {
								if (out != null) {
									try {
										out.close();
									} catch (IOException e) {
										Errors.getInstance().showExceptionDialog(getTab(), e);
									}
								}
							}
						}
					}
				});
	}

	/**
	 * refresh valuelists after reloading current collectable (NUCLOSINT-851)
	 */
	@Override
	public void refreshCurrentCollectable() throws CommonBusinessException {
		super.refreshCurrentCollectable();
		for (CollectableComponent c : getDetailsEditView().getCollectableComponents()) {
			if (c instanceof LabeledCollectableComponentWithVLP) {
				final LabeledCollectableComponentWithVLP comboBox = (LabeledCollectableComponentWithVLP) c;
				comboBox.refreshValueList(false);
			}
		}
	}
	
	@Override
	public void enableToolbarButtonsForDetailsMode(final int iDetailsMode) {
		super.enableToolbarButtonsForDetailsMode(iDetailsMode);
		if (iDetailsMode == CollectState.DETAILSMODE_VIEW) {
			setInitialComponentFocusInDetailsTab(); // @see NUCLOS-1027
		}
	}
	
	@Override
	protected LayoutRoot getInitialLayoutMLDefinitionForSearchPanel() {
		LayoutRoot layoutRoot = getSearchPanel() != null ? getSearchPanel().layoutRoot : null;
		if (layoutRoot == null) {
			layoutRoot = newLayoutRoot(true);
		}
		getLayoutMLButtonsActionListener().setComponentsEnabled(false);
		return layoutRoot;
	}

	@Override
	protected void _clearSearchFields() {
		super._clearSearchFields();

		if (this.isSearchPanelAvailable()) {
			for (SubFormController subformctl : getSubFormControllers(true)) {
				if (subformctl instanceof SearchConditionSubFormController) {
					((SearchConditionSubFormController) subformctl).clear();
				}
			}
		}
	}

	@Override
	protected void _setSearchFieldsAccordingToSubCondition(CollectableSubCondition cond) throws CommonBusinessException {
		final UID sEntityNameSub = cond.getSubEntityUID();
		final SearchConditionSubFormController subformctl = mpsubformctlSearch.get(sEntityNameSub);
		if (subformctl == null)
			throw new NuclosFatalException(getSpringLocaleDelegate().getMessage("GenericObjectCollectController.40", "Ein Unterformular f\u00fcr die Entit\u00e4t {0} ist in der Suchbedingung, aber nicht im aktuellen Layout enthalten.",
					sEntityNameSub));
		subformctl.setCollectableSearchCondition(cond.getSubCondition());
	}

	/**
	 * §precondition this.isSearchPanelAvailable()
	 * §postcondition result == null || result.isSyntacticallyCorrect()
	 * 
	 * @param bMakeConsistent
	 * @return the search condition contained in the search panel's fields
	 *         (including the subforms' search fields).
	 *
	 * @deprecated Move to SearchController or SearchPanel and make protected
	 *             again.
	 */
	@Override
	public CollectableSearchCondition getCollectableSearchConditionFromSearchFields(boolean bMakeConsistent) throws CollectableFieldFormatException {
		if (!isSearchPanelAvailable())
			throw new IllegalStateException("!this.isSearchPanelAvailable()");
		final CollectableSearchCondition cond = super.getCollectableSearchConditionFromSearchFields(bMakeConsistent);

		final CompositeCollectableSearchCondition condAnd = new CompositeCollectableSearchCondition(LogicalOperator.AND);
		if (cond != null)
			condAnd.addOperand(cond);

		assert getMapOfSubFormControllersInSearch() != null;
		for (SearchConditionSubFormController subformctl : getMapOfSubFormControllersInSearch().values()) {
			subformctl.getSubForm().getSubformTable().getModel();

			for (CollectableSearchCondition subCond : subformctl.getCollectableSubformSearchConditions()) {
				if (subCond != null) {
					final UID entityUid = subformctl.getCollectableEntity().getUID();
					if (subformctl.getCollectableEntity().getFieldUIDs().contains(SF.LOGICALDELETED.getUID(entityUid))) {
						final CollectableEntityField clctEOEFdeleted = new CollectableEOEntityField(SF.LOGICALDELETED.getMetaData(entityUid));
						final CollectableSearchCondition condSearchDeleted = new CollectableComparison(
								clctEOEFdeleted, ComparisonOperator.EQUAL, new CollectableValueField(false));
						subCond = SearchConditionUtils.and(subCond, condSearchDeleted);
					}
					condAnd.addOperand(new CollectableSubCondition(
							subformctl.getCollectableEntity().getUID(), subformctl.getForeignKeyFieldUID(), subCond));
				}
			}
		}

		final CollectableSearchCondition result = SearchConditionUtils.simplified(condAnd);
		assert result == null || result.isSyntacticallyCorrect();
		return result;
	}

	/**
	 * stops editing in the Search panel. Derived classes may stop editing on
	 * fields, TableCellEditors etc. here
	 *
	 * @return Has the editing been stopped?
	 *
	 *         TODO: Make this protected again.
	 */
	@Override
	public boolean stopEditingInSearch() {
		if (getMapOfSubFormControllersInSearch() != null) {
			for (SearchConditionSubFormController subformctl : getMapOfSubFormControllersInSearch().values()) {
				subformctl.stopEditing();
			}
		}
		return true;
	}

	@Override
	public Map<UID, DetailsSubFormController<PK,CollectableEntityObject<PK>>> getDetailsSubforms() {
		return this.mpsubformctlDetails;
	}

	@Override
	public void cmdGenerateObject(GeneratorActionVO generatoractionvo) {
		cmdGenerateObject(generatoractionvo, null);
	}
	public void cmdGenerateObject(GeneratorActionVO generatoractionvo, ResultListener<?> resultListener) {
		Map<PK, UsageCriteria> sources = new HashMap<PK, UsageCriteria>();
		for (CollectableMasterDataWithDependants<PK> clct : getSelectedCollectables()) {
			sources.put(clct.getId(), null);
		}
		GenerationController controller = new GenerationController((Map<Long, UsageCriteria>) sources, generatoractionvo, this, getTab());
		controller.generateGenericObject(resultListener);
	}
	
	@Override
	public List<GeneratorActionVO> getGeneratorActions(boolean bForLayoutButtons) {
		if (getSelectedCollectableId() == null) {
			return Collections.emptyList();
		}
		UID mandatorUid = null;
		if (MetaProvider.getInstance().getEntity(getEntityUid()).isMandator()) {
			mandatorUid = (UID) getSelectedCollectable().getValueId(SF.MANDATOR_UID.getUID(getEntityUid()));
		}
		List<GeneratorActionVO> result = GeneratorActions.getActions(getEntityUid(), null, null, mandatorUid);
		if (!bForLayoutButtons) {
			// NUCLOS-7539 remove "internal Generation" except for LayoutButtons
			CollectionUtils.removeAll(result, GeneratorActionVO::isRuleOnly);
		}
		return GeneratorActions.sort(result);
	}
	
	@Override
	public List<GeneratorActionVO> getGeneratorActions(Collection<CollectableMasterDataWithDependants<PK>> selectedCollectablesFromResult) {
		return getGeneratorActions(false);
	}

	@Override
	public Collection<EventSupportSourceVO> getUserRules() {
		// Integer entityUid = IdUtils.unsafeToId(MetaProvider.getInstance().getEntity(getEntityUID()).getPrimaryKey());
		UsageCriteria uc = new UsageCriteria(getEntityUid(), null, null, getCustomUsage());

		// add EventSupports if existing
		final Collection<EventSupportSourceVO> foundRules = EventSupportDelegate.getInstance().findEventSupportsByUsageAndEvent(CustomRule.class.getCanonicalName(), uc);
		
		// remove inactive rules
		CollectionUtils.removeAll(foundRules, new Predicate<EventSupportSourceVO>() {
			@Override
			public boolean evaluate(EventSupportSourceVO rulevo) {
				return !rulevo.isActive();
			}
		});
		return foundRules;
	}

	@Override
	protected void readValuesFromEditPanel(CollectableMasterDataWithDependants<PK> clct, boolean bSearchTab) throws CollectableValidationException {
		super.readValuesFromEditPanel(clct, bSearchTab);
		if (ctlTranslations != null) {
			clct.getMasterDataCVO().setResources(ctlTranslations.getResources());
		}
	}

	@Override
	protected void setupShortcutsForTabs(MainFrameTab frame) {
		super.setupShortcutsForTabs(frame);
		
		// the focus actions
		final SearchOrDetailsPanel searchOrDetailsPanel =
				getCollectPanel().getTabbedPaneSelectedIndex() == CollectPanel.TAB_SEARCH ? getSearchPanel()
						: (getCollectPanel().getTabbedPaneSelectedIndex() == CollectPanel.TAB_DETAILS ? getDetailsPanel() : null);
		if (searchOrDetailsPanel != null) {
			bindInitialComponent(searchOrDetailsPanel);
			bindGenerationComponent(searchOrDetailsPanel);
		}
	}

	@Override
	public UID getCurrentLayoutUid() {
		return mddelegate.getLayoutUid(getEntityUid(), getCollectState().isSearchMode(), getCustomUsage());
	}
	
	
	protected void setupSplitPane() {
		final Collection<JSplitPane> colSp = UIUtils.findAllInstancesOf(getDetailsPanel(), JSplitPane.class);

		// add listener for split pane divider changes (update workspace preferences on change)
		for (final JSplitPane sp : colSp) {
			sp.removePropertyChangeListener(JSplitPane.DIVIDER_LOCATION_PROPERTY, pclSplitPane);
			sp.addPropertyChangeListener(JSplitPane.DIVIDER_LOCATION_PROPERTY, pclSplitPane);
		}

		// initialize split pane settings 
		readSplitPaneStateFromPrefs(getCurrentLayoutUid(), getPreferences(),
				getDetailsPanel());
	}
	
	protected void restoreLocalizedValuesIntoModel(CollectableMasterDataWithDependants clct, CollectableComponent clctcomp) {
	
		DetailsLocalizedComponentModel model = (DetailsLocalizedComponentModel) clctcomp.getModel();
		
		if (clct.getMasterDataCVO().getDataLanguageMap() == null) {
			clct.getMasterDataCVO().setDataLanguageMap(new DataLanguageMap());			
		}
		
		if (model.getDataLanguageMap() != null) {
			clct.getMasterDataCVO().setDataLanguageMap(
					model.getDataLanguageMap());		
		}
	}

	@Override
	public boolean inHistoricalView(UID subformEntityUID, UID subformFieldUID, UID foreignKeyFieldToParent, int row) {
		boolean retVal = false;
		
		if(isHistoricalView()) {
			if (foreignKeyFieldToParent != null && this.mdwdCurrent != null) {
				CollectableEntityObject<PK> selectedRow = this.getSubFormController(subformEntityUID).getCollectableTableModel().getRow(row);
				IDependentKey dependentKey = DependentDataMap.createDependentKey(foreignKeyFieldToParent);
				if (mdwdCurrent.getDependants(dependentKey) != null) {
					Iterator<CollectableEntityObject<PK>> iterator = mdwdCurrent.getDependants(dependentKey).iterator();
					while(iterator.hasNext()) {
						CollectableEntityObject<PK> next = iterator.next();
						if (next.getPrimaryKey().equals(selectedRow.getPrimaryKey())) {
							if (!next.getField(subformFieldUID).equals(selectedRow.getField(subformFieldUID))) {
								retVal = true;
							}
							break;
						}
					}
				}			
			}
		}
		
		return retVal;
	}
	
	
} // class MasterDataCollectController
