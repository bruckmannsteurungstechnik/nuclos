//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang.NullArgumentException;
import org.apache.log4j.Logger;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.entityobject.EntityObjectDelegate;
import org.nuclos.client.explorer.configuration.ConfigurationTreeBuilder;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.EntityTreeViewVO;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.SearchConditionUtils;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.TruncatableCollection;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.common.NuclosUpdateException;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.genericobject.ProxyList;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeRemote;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.ruleengine.NuclosFatalRuleException;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

/**
 * Business Delegate for <code>MasterDataFacadeBean</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author      <a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
// @Component
@ManagedResource(
		objectName="nuclos.client.cache:name=MasterDataDelegate",
		description="Nuclos Client MasterDataDelegate",
		currencyTimeLimit=15,
		log=false)
public class MasterDataDelegate {

	private static final Logger LOG = Logger.getLogger(MasterDataDelegate.class);

	private static MasterDataDelegate INSTANCE;

	// 

	private MasterDataFacadeRemote facade;

	private MasterDataLayoutCache mdlayoutcache;

	private Map<UID, EntityMeta<?>> metaDataCache;
	
	private SpringLocaleDelegate localeDelegate;
	
	private MasterDataCache masterDataCache;

	/**
	 * Use getInstance() to create an (the) instance of this class
	 * 
	 * @deprecated Must be protected for Spring JMX - but never invoke this constructor.
	 */
	protected MasterDataDelegate() throws RuntimeException {
		INSTANCE = this;
	}

	public static MasterDataDelegate getInstance() {
		if (INSTANCE == null) {
			// lazy support
			INSTANCE = SpringApplicationContextHolder.getBean(MasterDataDelegate.class);
		}
		return INSTANCE;
	}
	
	// @Autowired
	public final void setMasterDataFacadeRemote(MasterDataFacadeRemote masterDataFacadeRemote) {
		this.facade = masterDataFacadeRemote;
	}
	
	// @Autowired
	public final void setSpringLocaleDelegate(SpringLocaleDelegate cld) {
		this.localeDelegate = cld;
	}
	
	// @Autowired
	public final void setMasterDataCache(MasterDataCache masterDataCache) {
		this.masterDataCache = masterDataCache;
	}

	private MasterDataFacadeRemote getMasterDataFacade() {
		return facade;
	}

	private synchronized MasterDataLayoutCache getLayoutCache() {
		if (mdlayoutcache == null) {
			mdlayoutcache = new MasterDataLayoutCache();
		}
		return mdlayoutcache;
	}

	@ManagedOperation(description="invalidate/clear cache")
	public synchronized void invalidateLayoutCache() {
		mdlayoutcache = null;
		LOG.info("invalidate LayoutCache done");
	}
	
	public String getLayoutML(UID entityUid, boolean bSearchMode, String sCustom) {
		 return getLayoutCache().get(entityUid, bSearchMode, sCustom);
	 }

	 public UID getLayoutUid(UID entityUid, boolean searchMode, String sCustom) {
		 return getLayoutCache().getLayoutUid(entityUid, searchMode, sCustom);
	 }

	/**
	 * @param entityUid
	 * @param bSearchMode Search mode? Otherwise: Details mode.
	 * @return the (cached) layout ml document, if any, for the given entity and mode.
	 */
	 public String getLayoutML(UID entityUid, boolean bSearchMode) {
		 return getLayoutML(entityUid, bSearchMode, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
	 }

	 public UID getLayoutUid(UID entityUid, boolean searchMode) {
		 return getLayoutUid(entityUid, searchMode, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
	 }


	/**
	 * gets the meta data for all master data tables.
	 * @return Collection&lt;MasterDataMetaVO&gt;. A (possibly empty) collection of masterdata meta objects
	 */
	 public Collection<EntityMeta<?>> getMetaData() {
		 try{
			 return getMetaDataCache().values();
		 }catch(RuntimeException ex){
			 final String sMessage = localeDelegate.getMessage(
					 "MasterDataDelegate.1", "Fehler beim Laden der Metadaten f\u00fcr alle Entit\u00e4ten.");
			 throw new CommonFatalException(sMessage,ex);
		 }
	 }

	 public <PK> EntityObjectVO<PK> executeBusinessRules(
	 		List<EventSupportSourceVO> lstRuleVO,
			MasterDataVO<PK> mdvo,
			String customUsage,
			boolean isCollectiveProcessing
	 ) throws CommonBusinessException {
		 try {
			 return EntityObjectDelegate.getInstance().executeBusinessRules(lstRuleVO, mdvo.getEntityObject(), customUsage, isCollectiveProcessing);
		 }
		 catch (NuclosFatalRuleException ex) {
			 throw ex;
		 }
		 catch (RuntimeException ex) {
			 throw new CommonFatalException(ex);
		 }
	 }
	 
	 /**
	  * §postcondition result != null
	  * 
	  * @return the meta data for an entity
	  * @throws CommonFatalException if there is no entity with the given name.
	  */
	 public EntityMeta<?> getMetaData(UID entityUid) {
		 try{
			 EntityMeta<?> result = getMetaDataCache().get(entityUid);
			 if (result == null) {
				 result = getMetaDataNoCache(entityUid);
				 if(result == null) {
					 throw new CommonFatalException(localeDelegate.getMessage(
							 "MasterDataDelegate.2", "Keine Metadaten f\u00fcr die Entit\u00e4t {0} vorhanden.", entityUid));
				 }
				 else {
					 invalidateCaches();
				 }
			 }
			 assert result != null;
			 return result;
		 }catch(RuntimeException ex){
			 final String sMessage = localeDelegate.getMessage(
					 "MasterDataDelegate.3", "Fehler beim Laden der Metadaten f\u00fcr die Entit\u00e4t {0}.", entityUid);
			 throw new CommonFatalException(sMessage, ex);
		 }

	 }

	 public boolean hasEntity(UID entityUid) {
		 if(getMetaDataCache().get(entityUid) != null)
			 return true;
		 else
			 return false;
	 }
	 
	 /**
	  * @param moduleUid the id of the module whose subentities we are looking for
	  * @return the subentities having foreign keys to the given module.
	  */
	 public List<CollectableField> getSubEntities(UID moduleUid) throws NuclosBusinessException {
		 try {
			 return getMasterDataFacade().getSubEntities(moduleUid);
		 }
		 catch (RuntimeException ex) {
			 final String sMessage = localeDelegate.getMessage(
					 "MasterDataDelegate.5", "Fehler beim Ermitteln der verf\u00fcgbaren Unterentit\u00e4ten f\u00fcr das Modul {0}.", moduleUid);
			 throw new NuclosBusinessException(sMessage, ex);
		 }

	 }

	 /**
	  * gets all the masterdata for an entity
	  * 
	  * §postcondition result != null
	  * §postcondition !result.isTruncated()
	  * 
	  * @param entityUid
	  * @return a (possibly empty) collection of masterdata objects.
	  */
	 public <PK> Collection<MasterDataVO<PK>> getMasterData(UID entityUid) {
		 final TruncatableCollection<MasterDataVO<PK>> result = getMasterData(entityUid, null);

		 assert result != null;
		 assert !result.isTruncated();
		 return result;
	 }

	 /**
	  * gets the masterdata for an entity, using the given search condition, if any. The result is never truncated.
	  * 
	  * §postcondition result != null
	  * §postcondition !result.isTruncated()
	  * 
	  * @param entityUid
	  * @param cond may be <code>null</code>.
	  * @return a (possibly empty) collection of masterdata objects.
	  */
	 public <PK> TruncatableCollection<MasterDataVO<PK>> getMasterData(UID entityUid, CollectableSearchCondition cond) {
		 final TruncatableCollection<MasterDataVO<PK>> result = getMasterData(entityUid, cond, false);

		 assert result != null;
		 assert !result.isTruncated();
		 return result;
	 }

	 /**
	  * §postcondition result != null
	  */
	 public <PK> ProxyList<PK,MasterDataVO<PK>> getMasterDataProxyList(UID entityUid, List<CollectableEntityField> cefs, CollectableSearchExpression clctexpr) {
		 try {
			 List<UID> fields = null;
			 if (cefs != null) {
				 fields = new ArrayList<UID>();
				 for (CollectableEntityField cef : cefs) {
					 fields.add(cef.getUID());
				 }
			 }
			 
			 final ProxyList<PK,MasterDataVO<PK>> result = getMasterDataFacade().getMasterDataProxyList(entityUid, fields, clctexpr);
			 assert result != null;
			 return result;
		 }
		 catch (RuntimeException | CommonPermissionException ex) {
			 final String sName = localeDelegate.getLabelFromMetaDataVO(MetaProvider.getInstance().getEntity(entityUid));
			 final String sMessage = localeDelegate.getMessage("MasterDataDelegate.4", "Fehler beim Laden der Daten f\u00fcr die Entit\u00e4t \"{0}\".", sName);
			 throw new CommonFatalException(sMessage, ex);
		 }
	 }

	 /**
	  * gets the masterdata for an entity, using the given search condition, if any.
	  * 
	  * §postcondition result != null
	  * 
	  * @param entityUid
	  * @param cond may be <code>null</code>.
	  * @param bTruncate Are huge results to be truncated?
	  * @return a (possibly empty) collection of masterdata objects.
	  */
	 public <PK> TruncatableCollection<MasterDataVO<PK>> getMasterData(UID entityUid, CollectableSearchCondition cond, boolean bTruncate) {
		 try {
			 final TruncatableCollection<MasterDataVO<PK>> result = getMasterDataFacade().getMasterDataWithCheck(entityUid, cond, !bTruncate);
			 assert result != null;
			 return result;
		 }
		 catch (RuntimeException | CommonPermissionException ex) {
			 final String sMessage = localeDelegate.getMessage(
					 "MasterDataDelegate.4", "Fehler beim Laden der Daten f\u00fcr die Entit\u00e4t {0}.", entityUid);
			 throw new CommonFatalException(sMessage, ex);
		 }
	 }

	 /**
	  * gets the dependent masterdata for an entity.
	  * @param entityUid
	  * @param foreignKeyFieldUid
	  * @param filterCondition
      *@param params optional parameters (for charts)
      * @param limit set limit for number of dependents
      * @param oRelatedId the foreign key of the parent entity    @return Collection&lt;MasterDataVO&gt;. A (possibly empty) collection of masterdata objects
	  */
	 public <PK, F> Collection<EntityObjectVO<PK>> getDependentDataCollectionWithLimit(UID foreignKeyFieldUid,
                                                                                       CollectableSearchCondition filterCondition, Map<String, Object> params, UID layoutUid, Integer limit, F... oRelatedId) {
		 
		 try {
			 if (limit != null && limit.equals(0)) {
				 return new ArrayList<>();
			 }
			 Collection<EntityObjectVO<PK>> col = getMasterDataFacade().<PK, F>getDependentDataCollection(foreignKeyFieldUid, filterCondition, params, layoutUid, limit, oRelatedId);
			 return col;
		 }
		 catch (RuntimeException | CommonPermissionException ex) {
			 throw new CommonFatalException(ex);
		 }
	 }

	 // TODO Parameter entity is obsolete, remove and references to it.
	 public <PK, F> Collection<EntityObjectVO<PK>> getDependentDataCollection(UID entity, UID foreignKeyFieldUid,
														  Map<String, Object> params, F... oRelatedId) {
		 return getDependentDataCollectionWithLimit(foreignKeyFieldUid, null, params, null,null, oRelatedId);
	 }
	 
	 /**
	  * @param entityUid
	  * @param oId the object's id (primary key)
	  * @return the masterdata object with the given entity and id.
	  * @throws CommonFinderException
	  * @throws CommonPermissionException
	  */
	 public <PK> MasterDataVO<PK> get(UID entityUid, PK oId) throws CommonFinderException, CommonPermissionException {
		 if (oId == null) {
			 throw new NullArgumentException("oId");
		 }
		 try {
			 return this.getMasterDataFacade().get(entityUid, oId);
		 }
		 catch (RuntimeException ex) {
			 throw new CommonFatalException(ex);
		 }
	 }

	 /**
	  * @param entityUid
	  * @param oId the object's id (primary key)
	  * @return the masterdata object with the given entity and id.
	  * @throws CommonFinderException
	  * @throws CommonPermissionException
	  */
	 public <PK> MasterDataVO<PK> get(UID entityUid, final PK oId, boolean bUseCacheIfPossible) throws CommonFinderException, CommonPermissionException {
		 MasterDataVO<PK> result = null;
		 if (bUseCacheIfPossible) {
			 /** @todo optimize 
			 result = CollectionUtils.findFirst(masterDataCache.<PK>get(entityUid),
				 PredicateUtils.transformedInputEquals(new MasterDataVO.GetId<PK>(), oId));
				 */
			 result = (MasterDataVO<PK>) CollectionUtils.findFirst(masterDataCache.get(entityUid),
					 new Predicate<MasterDataVO<?>>() {
						@Override
						public boolean evaluate(MasterDataVO<?> t) {
							return oId.equals(t.getPrimaryKey());
						}
					});
		 }
		 if (result == null) {
			 result = get(entityUid, oId);
		 }
		 return result;
	 }

	 /**
	  * @param entityUid
	  * @param oId the object's id (primary key)
	  * @param dependantsDepth
	  * @return the masterdata object with the given entity and id.
	  * @throws CommonFinderException
	  * @throws CommonPermissionException
	  */
	 public <PK> MasterDataVO<PK> getWithDependants(UID entityUid, PK oId, final int dependantsDepth) throws CommonFinderException, CommonPermissionException {
		 if (oId == null) {
			 throw new NullArgumentException("oId");
		 }
		 try {
			 return this.getMasterDataFacade().getWithDependents(entityUid, oId, dependantsDepth);
		 }
		 catch (RuntimeException ex) {
			 throw new CommonFatalException(ex);
		 }
	 }

	 /**
	  * returns the version of the given masterdata
	  * @param entityUid
	  * @param oId
	  * @throws CommonFinderException
	  * @throws CommonPermissionException
	  */
	 public Integer getVersion(UID entityUid, Object oId) throws CommonFinderException, CommonPermissionException {
		 try {
			 return this.getMasterDataFacade().getVersion(entityUid, oId);
		 }
		 catch (RuntimeException ex) {
			 throw new CommonFatalException(ex);
		 }
	 }


	 public IDependentDataMap loadMatrixData(UID foreignKeyField, Object oRelatedId, List<EntityAndField> lstChildSubform,
											 UID layoutUID) {
	 	try {
			IDependentDataMap result = getMasterDataFacade().loadMatrixData(foreignKeyField, oRelatedId, lstChildSubform, layoutUID);
			return result;
		} catch (CommonBusinessException cpe) {
	 		throw new NuclosFatalException(cpe);
		}
	 }

	public Collection<MasterDataVO<Long>> loadMatrixXAxis(UID entity, UID layoutUID) {
		try {
			Collection<MasterDataVO<Long>> result = getMasterDataFacade().loadMatrixXAxis(entity, layoutUID);
			return result;
		} catch (CommonBusinessException cpe) {
			throw new NuclosFatalException(cpe);
		}
	}
	 
	 /**
	  * creates the given object, along with its dependants (if any).
	  * 
	  * §precondition mdvo.getId() != null
	  * §precondition mpDependants != null --&gt; for(m : mpDependants.values()) { m.getId() == null }
	  * 
	  * @param entityUid
	  * @param mdvo must have an empty (<code>null</code>) id.
	  * @param mpDependants If <code>!= null</code>, all elements must have an empty (<code>null</code>) id.
	  * @return the created object
	  * @throws NuclosBusinessException
	  */
	 public <PK> MasterDataVO<PK> create(UID entityUid, MasterDataVO<PK> mdvo, IDependentDataMap mpDependants, String customUsage)
	 throws CommonBusinessException {
		 try {
			 if (mdvo.getId() != null) {
				 throw new IllegalArgumentException("mdvo");
			 }
			 checkDependantsAreNew(mpDependants);
			 mdvo.setDependents(mpDependants);
			 return this.getMasterDataFacade().create(mdvo, customUsage);
		 }
		 catch (RuntimeException ex) {
			// RuntimeException has the BAD habit to include its cause' message in its own message.
			// the default message of NuclosUpdateException is not always correct ("duplicate key").
			// cause of the exception will be added at Errors.java			 			
			throw new CommonFatalException("MasterDataDelegate.7", ex);
		 }
		 catch (CommonCreateException ex) {
			 throw new NuclosBusinessException(ex.getMessage(), ex);
		 }
	 }

	 /**
	  * updates the given object, along with its dependants.
	  * 
	  * §precondition mdvo.getId() != null
	  * 
	  * @param entityUid
	  * @param mdvo
	  * @param mpDependants May be <code>null</code>.
	  * @return the id of the updated object
	  * @throws CommonBusinessException
	  */
	 public <PK> PK update(UID entityUid, MasterDataVO<PK> mdvo, IDependentDataMap mpDependants, String customUsage, boolean isCollectiveProcessing)
	 throws CommonBusinessException {
		 if (mdvo.getId() == null) {
			 throw new IllegalArgumentException("mdvo");
		 }

		 final Logger log = Logger.getLogger(MasterDataDelegate.class);
		 if (log.isDebugEnabled()) {
			 log.debug("UPDATE: " + mdvo.getDebugInfo());
			 if (mpDependants != null) {
				 log.debug("Dependants:");
				 for (List<EntityObjectVO<?>> mdvoDependant : mpDependants.getRoDataMap().values()) {
					 for (EntityObjectVO<?> eo: mdvoDependant) {
						 log.debug(eo.getDebugInfo());
					 }
				 }
			 }
		 }
		 try {
			 mdvo.setDependents(mpDependants);
			 if (E.NUCLET_INTEGRATION_POINT.checkEntityUID(entityUid)) {
				 try {
					 return EntityObjectDelegate.getInstance().update(mdvo.getEntityObject(), isCollectiveProcessing).getPrimaryKey();
				 } catch (CommonBusinessException e) {
					 throw new NuclosBusinessException(e);
				 }
			 } else {
				 return this.getMasterDataFacade().modify(mdvo, customUsage, isCollectiveProcessing);
			 }
		 }
		 catch (RuntimeException ex) {
			// RuntimeException has the BAD habit to include its cause' message in its own message.
			// the default message of NuclosUpdateException is not always correct ("duplicate key").
			// cause of the exception will be added at Errors.java
			throw new NuclosUpdateException("MasterDataDelegate.6", ex);
		 }
	 }

	 public void remove(UID entityUid, MasterDataVO<?> mdvo, String customUsage) throws CommonBusinessException {
		 try {
			 this.getMasterDataFacade().remove(mdvo.getEntityObject().getDalEntity(), mdvo.getPrimaryKey(), true, customUsage);
		 }
		 catch (RuntimeException ex) {
			 throw new CommonFatalException(ex);
		 }
	 }

	public Set<UID> getSubFormEntityUidsByMasterDataEntity(UID entityUid, String customUsage) {
		return CollectionUtils.transformIntoSet(getSubFormEntitiesByMasterDataEntity(entityUid, customUsage),
			new EntityAndField.GetEntityUID());
	}

	public Set<EntityAndField> getSubFormEntitiesByMasterDataEntity(UID entityUid, String customUsage) {
		try {
			UsageCriteria usage = new UsageCriteria(entityUid, null, null, customUsage);
			return getMasterDataFacade().getSubFormEntitiesByMasterDataEntity(usage);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}
	
	public boolean isSubformEntity(UID entityUid) {
		return getLayoutCache().getLayoutUid(entityUid, false, null) == null;
	}
	
	public Set<String> getCustomUsages(UID entityUid) {
		return getLayoutCache().getCustomUsages(entityUid);
	}

	 /**
		 }
		 catch (NuclosFatalRuleException ex) {
			 throw ex;
	  * Validate all masterdata entries against their meta information (length, format, min, max etc.).
	  * @param sOutputFileName the name of the csv file to which the results are written.
	  */
	 public void checkMasterDataValues(String sOutputFileName) {
		 try {
			 getMasterDataFacade().checkMasterDataValues(sOutputFileName);
		 }
		 catch (RuntimeException ex) {
			 throw new CommonFatalException(ex);
		 }
	 }

	 /** value list provider function
	  * @throws CommonPermissionException */
	  public Collection<MasterDataVO<UID>> getUserHierarchy(String rootUser) throws CommonPermissionException {
		  try {
			  return getMasterDataFacade().getUserHierarchy(rootUser);
		  } catch (RuntimeException ex) {
			  throw new NuclosFatalException(ex);
		  }
	  }

	 /**
	  * checks that all dependants (if any) have a <code>null</code> id.
	  * @param mpDependants may be <code>null</code>.
	  */
	 public static void checkDependantsAreNew(IDependentDataMap mpDependants) {
		 if (mpDependants != null && !mpDependants.areAllDependentsNew()) {
			 throw new IllegalArgumentException("mpDependants");
		 }
	 }

	 /** value list provider function */
	 public List<CollectableValueIdField> getProcessByEntity(UID entityUid, boolean bSearchMode) {
		 try {
			 return getMasterDataFacade().getProcessByEntity(entityUid, bSearchMode);
		 }
		 catch (RuntimeException ex) {
			 throw new CommonFatalException(ex);
		 }
	 }

	 /** value list provider function
	  * @throws CommonPermissionException */
	  public Collection<MasterDataVO<UID>> getAllReports() throws CommonPermissionException {
		  try {
			  return getMasterDataFacade().getAllReports();
		  }
		  catch (CommonFinderException ex) {
			  throw new NuclosFatalException(ex);
		  }
		  catch (RuntimeException ex) {
			  throw new NuclosFatalException(ex);
		  }
	  }

	 /** value list provider function
	  * @throws CommonPermissionException */
	  public Collection<MasterDataVO<UID>> getAllGenerations() throws CommonPermissionException {
		  try {
			  return getMasterDataFacade().getAllGenerations();
		  }
		  catch (CommonFinderException ex) {
			  throw new NuclosFatalException(ex);
		  }
		  catch (RuntimeException ex) {
			  throw new NuclosFatalException(ex);
		  }
	  }

	 /** value list provider function
	  * @throws CommonPermissionException */
	  public Collection<MasterDataVO<UID>> getAllRecordgrants() throws CommonPermissionException {
		  try {
			  return getMasterDataFacade().getAllRecordgrants();
		  }
		  catch (CommonFinderException ex) {
			  throw new NuclosFatalException(ex);
		  }
		  catch (RuntimeException ex) {
			  throw new NuclosFatalException(ex);
		  }
	  }

	  private Map<UID, EntityMeta<?>> getMetaDataCache() throws RuntimeException {
		  if (metaDataCache == null) {
			  LOG.debug("Initializing metadata cache");
			  final MetaProvider mProv = MetaProvider.getInstance();
			  final Collection<EntityMeta<?>> coll = mProv.getAllEntities();
			  metaDataCache = new HashMap<UID, EntityMeta<?>>(coll.size() * 2);
			  for (EntityMeta<?> mdmetavo : coll) {
				  metaDataCache.put(mdmetavo.getUID(), mdmetavo);
			  }
		  }
		  return metaDataCache;
	  }

	  private EntityMeta<?> getMetaDataNoCache(UID entityUid) throws RuntimeException {
		  final MetaProvider mProv = MetaProvider.getInstance();
		  return mProv.getEntity(entityUid);
	  }
	  
	@ManagedOperation(description = "invalidate/clear cache")
	public void invalidateCaches() {
		LOG.debug("Invalidating meta data cache.");
		metaDataCache = null;
	}

	public static class Key {

		private final UID entityUid;
		private final boolean bSearch;
		private final String sCustom;

		public Key(UID entityUid, boolean bSearch, String sCustom) {
			this.entityUid = entityUid;
			this.bSearch = bSearch;
			this.sCustom = sCustom;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}

			final Key key = (Key) o;

			if (bSearch != key.bSearch) {
				return false;
			}
			if (!LangUtils.equal(entityUid, key.entityUid)) {
				return false;
			}
			if (!LangUtils.equal(sCustom, key.sCustom)) {
				return false;
			}

			return true;
		}

		@Override
		public int hashCode() {
			return 29 * (entityUid.getString()+sCustom).hashCode() + (bSearch ? 1 : 0);
		}
	}

	  /**
	   * inner class MasterDataLayoutCache
	   */
	  private final class MasterDataLayoutCache {

		  /**
		   * (Key) layout key -> (Pair) layout uid | layout xml.
		   */
		  private final Map<Key, UID> mpUsages;
		  private final Map<UID, String> mpLayouts;
		  
		  private MasterDataLayoutCache() {
			  mpUsages = getLayoutUsagesMap();
			  mpLayouts = new HashMap<UID, String>();
		  }

		  private Map<Key, UID> getLayoutUsagesMap() {
			  final Map<Key, UID> result = new HashMap<Key, UID>();

			  try {
				  for (MasterDataVO<?> md : facade.getMasterDataWithCheck(E.LAYOUTUSAGE.getUID(), null, true)) {
					  final MasterDataVO<UID> mdvoUsage = (MasterDataVO<UID>) md;
					  final UID entityUid = mdvoUsage.getFieldUid(E.LAYOUTUSAGE.entity);
					  final boolean bSearch = mdvoUsage.getFieldValue(E.LAYOUTUSAGE.searchScreen);
					  final UID iLayoutId = mdvoUsage.getFieldUid(E.LAYOUTUSAGE.layout);
					  final String sCustom = mdvoUsage.getFieldValue(E.LAYOUTUSAGE.custom);
					  result.put(
							  new Key(entityUid, bSearch, sCustom),
							  iLayoutId
					  );
				  }
				  return result;
			  } catch (CommonPermissionException cpe) {
				    // Should never happen
			  		throw new NuclosFatalException(cpe);
			  }
		  }
		  
		  private String getLayout(UID layoutUID) {
			  if (layoutUID == null) {
				  return null;
			  }
			  if (!mpLayouts.containsKey(layoutUID)) {
				  try {
					  MasterDataVO<UID> md = facade.get(E.LAYOUT.getUID(), layoutUID);
					  mpLayouts.put(layoutUID, md.getFieldValue(E.LAYOUT.layoutML.getUID(), String.class));
				  } catch (CommonBusinessException cbe) {
					  LOG.error(cbe.getMessage(), cbe);
				  }
			  }
			  return mpLayouts.get(layoutUID);
		  }

		  /**
		   * @param entityUid
		   * @param bSearchMode Search mode? Otherwise: Details mode.
		   * @return the layout ml document, if any, for the given entity and mode.
		   */
		  public String get(UID entityUid, boolean bSearchMode, String sCustom) {
			  Key key = new Key(entityUid, bSearchMode, sCustom);
			  UID layoutUID = mpUsages.get(key);
			  if (sCustom != null && layoutUID == null) {
				  layoutUID = mpUsages.get(new Key(entityUid, bSearchMode, null));
			  }
			  return getLayout(layoutUID);
		  }

		  public UID getLayoutUid(UID entityUid, boolean bSearchMode, String sCustom) {
			  UID layoutUID = mpUsages.get(new Key(entityUid, bSearchMode, sCustom));
			  if (sCustom != null && layoutUID == null) {
				  layoutUID = mpUsages.get(new Key(entityUid, bSearchMode, null));
			  }
			  return layoutUID;
		  }
		  
		  public Set<String> getCustomUsages(UID entityUid) {
			  Set<String> result = new HashSet<String>();
			  for (Key key : mpUsages.keySet()) {
				  if (key.sCustom != null && LangUtils.equal(entityUid, key.entityUid)) {
					  result.add(key.sCustom);
				  }
			  }
			  return result;
		  }
		  
	  }       // class MasterDataLayoutCache
	
	/**
	  * @param entityUid
	  * @return Does the entity with the given uid use the rule engine?
	  */
	public boolean getUsesRuleEngine(UID entityUid) {
		try {
			return getMasterDataFacade().getUsesRuleEngine(entityUid);
		} catch (RuntimeException e) {
			throw new CommonFatalException(e);
		}
	}

	public Long countMasterDataRows(UID entity, final CollectableSearchExpression clctexpr) {
		try {
			return getMasterDataFacade().countMasterDataRows(entity, clctexpr);
		} catch (RuntimeException | CommonPermissionException e) {
			throw new CommonFatalException(e);
		}
	}

	@ManagedAttribute(description = "get the size (number of entries/entities) of metaDataCache")
	public int getNumberOfEntitiesInMetaDataCache() {
		return metaDataCache.size();
	}

	public boolean exist(UID entityUid, Object id) {
		return getMasterDataFacade().exist(entityUid, id);
	}
		
	/**
	 * Load subnodes from t_md_entity_subnodes table.
	 */
	public void loadTreeView(final UID uidEntity, final List<EntityTreeViewVO> lstTreeViewVO) {
		// clear tree view list
		lstTreeViewVO.clear();
		
		final NavigableSet<EntityTreeViewVO> views = new TreeSet<EntityTreeViewVO>();
		
		final Collection<MasterDataVO<UID>> colChildNodesTmp = getMasterData(E.ENTITYSUBNODES.getUID(),
				SearchConditionUtils.and(org.nuclos.common.SearchConditionUtils.newUidComparison(E.ENTITYSUBNODES.originentityid, ComparisonOperator.EQUAL, uidEntity),
				SearchConditionUtils.not(org.nuclos.common.SearchConditionUtils.newIsNullCondition(E.ENTITYSUBNODES.parentSubNode))
				));
		final List<EntityTreeViewVO> colChildNodes = CollectionUtils.transform(colChildNodesTmp, new Transformer<MasterDataVO<UID>, EntityTreeViewVO>(){
			@Override
			public EntityTreeViewVO transform(MasterDataVO<UID> mdvo) {
				return EntityTreeViewVO.wrapMasterData(mdvo, uidEntity);
			}
		});
		
		for(MasterDataVO<?> vo : MasterDataCache.getInstance().get(E.ENTITYSUBNODES.getUID())) {
			// filter top level nodes
			final UID uidOriginEntity = vo.getEntityObject().getFieldUid(E.ENTITYSUBNODES.originentityid.getUID());
			assert uidOriginEntity != null;
			if (uidOriginEntity.equals(uidEntity) &&
					null == vo.getEntityObject().getFieldUids().get(E.ENTITYSUBNODES.parentSubNode.getUID())) {
				final EntityTreeViewVO parentVO = EntityTreeViewVO.wrapMasterData((MasterDataVO<UID>)vo, uidEntity);
				views.add(parentVO);
				
				// create child node tree
				ConfigurationTreeBuilder.createChildNodes(parentVO, colChildNodes);
			}
		}
		
		// fill tree model
		lstTreeViewVO.addAll(views);
	}

}       
// class MasterDataDelegate
