package org.nuclos.client.masterdata;

import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.ui.collect.CollectControllerFactorySingleton;
import org.nuclos.common.UID;
import org.nuclos.common.collect.ControllerPresentation;

public class MasterDataSplitViewCollectController<PK> extends MasterDataCollectController<PK> {
		
	public MasterDataSplitViewCollectController(UID entityUid, MainFrameTab tabIfAny, String customUsage) {
		super(entityUid, tabIfAny, customUsage, ControllerPresentation.SPLIT_RESULT);
		
		MasterDataCollectController<PK> detailController = CollectControllerFactorySingleton.getInstance().newMasterDataCollectController(entityUid, new MainFrameTab(), customUsage, ControllerPresentation.SPLIT_DETAIL);
		setSplitDetailController(detailController);
	}
	
	@Override
	public void init() {
		super.init();
		initSplitView();
	}

	@Override
	public void close() {
		if (getRefreshOnCloseIfNecessary() != null) {
			setCancelRunningStatementsOnClose(false);
		}
		super.close();
	}
}
