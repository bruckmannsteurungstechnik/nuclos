//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.prefs.Preferences;

import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.MenuElement;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.log4j.Logger;
import org.nuclos.client.autonumber.AutonumberUiUtils;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.DetailsSubFormController;
import org.nuclos.client.common.EntityCollectController;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.NuclosCollectControllerFactory;
import org.nuclos.client.common.Utils;
import org.nuclos.client.common.controller.NuclosCollectControllerCommonState;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.dal.DalSupportForGO;
import org.nuclos.client.entityobject.CollectableEOEntityClientProvider;
import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.entityobject.CollectableEntityObject.MakeCollectable;
import org.nuclos.client.entityobject.CollectableEntityTemplate;
import org.nuclos.client.genericobject.CollectableGenericObjectWithDependants;
import org.nuclos.client.genericobject.GenericObjectCollectController;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.valuelistprovider.MasterDataCollectableFieldsProviderFactory;
import org.nuclos.client.scripting.ScriptEvaluator;
import org.nuclos.client.scripting.context.AbstractScriptContext;
import org.nuclos.client.scripting.context.SubformControllerCollectiveProcessingScriptContext;
import org.nuclos.client.scripting.context.SubformControllerScriptContext;
import org.nuclos.client.ui.Bubble;
import org.nuclos.client.ui.CommonMultiThreader;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.SizeKnownEvent;
import org.nuclos.client.ui.SizeKnownListener;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.client.ui.collect.CollectState;
import org.nuclos.client.ui.collect.ColletableEntityObjectLookup;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableComponentTableCellEditor;
import org.nuclos.client.ui.collect.component.CollectableListOfValues;
import org.nuclos.client.ui.collect.component.CollectableLocalizedComponent;
import org.nuclos.client.ui.collect.component.LabeledCollectableComponentWithVLP;
import org.nuclos.client.ui.collect.component.LookupEvent;
import org.nuclos.client.ui.collect.component.LookupListener;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelProvider;
import org.nuclos.client.ui.collect.detail.DetailsCollectableEventListener;
import org.nuclos.client.ui.collect.subform.Column;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.client.ui.collect.subform.SubForm.SubFormToolListener;
import org.nuclos.client.ui.collect.subform.SubFormFilterChangeEvent;
import org.nuclos.client.ui.collect.subform.SubFormFilterChangeListener;
import org.nuclos.client.ui.collect.subform.SubFormTable;
import org.nuclos.client.ui.collect.subform.ToolbarFunction;
import org.nuclos.client.ui.collect.subform.ToolbarFunctionState;
import org.nuclos.client.ui.collect.subform.TransferLookedUpValueAction;
import org.nuclos.client.ui.event.IPopupListener;
import org.nuclos.client.ui.event.PopupMenuMouseAdapter;
import org.nuclos.client.ui.gc.ListenerUtil;
import org.nuclos.client.ui.table.TableUtils;
import org.nuclos.client.valuelistprovider.cache.CollectableFieldsProviderCache;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.WorkspaceDescription2.EntityPreferences;
import org.nuclos.common.collect.collectable.AbstractCollectableField;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.DefaultCollectableEntityProvider;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.DefaultValueObjectList;
import org.nuclos.common.collection.ValueObjectList;
import org.nuclos.common.dal.DalSupportForMD;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.entityobject.CollectableEOEntity;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;

/**
 * Controller for collecting dependant masterdata (in a one-to-many relationship) in a subform.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * 
 * §todo rename to MasterDataDetailsSubFormController
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph Radig</a>
 * @author	<a href="mailto:Boris.Sander@novabit.de">Boris Sander</a>
 * @version 01.00.00
 */
public class MasterDataSubFormController<PK> extends DetailsSubFormController<PK,CollectableEntityObject<PK>> implements SubFormFilterChangeListener {

	private static final Logger LOG = Logger.getLogger(MasterDataSubFormController.class);

	//protected static final String TB_CLONE = "Toolbar.CLONE";

	private RowSelectionListener rowselectionlistener;
	private List<MasterDataSubFormController<PK>> lstChildSubController = new ArrayList<MasterDataSubFormController<PK>>();
	private CollectableMasterData<PK> clctParent;
	private EntityCollectController<PK,CollectableEntityObject<PK>> parentController;
	private List<PK> selectedRowsIds;

	int selectedColumn;

	/**
	 * @param tab
	 * @param clctcompmodelproviderParent provides <code>CollectableComponentModel</code>s. This avoids handing
	 * the whole <code>CollectController</code> to the DetailsSubFormController.
	 * @param parentEntityUid
	 * @param subform contains the subform (which in return containsPersonalSearchFilter the entity name for the subform).
	 * @param prefsUserParent the preferences of the parent (controller)
	 */
	public MasterDataSubFormController(MainFrameTab tab,
			CollectableComponentModelProvider clctcompmodelproviderParent, UID parentEntityUid, final SubForm subform,
			Preferences prefsUserParent, EntityPreferences entityPrefs, CollectableFieldsProviderCache valueListProviderCache,
			NuclosCollectControllerCommonState state, EntityCollectController<?, ?> eoController) {
		this(DefaultCollectableEntityProvider.getInstance().getCollectableEntity(subform.getEntityUID()), tab, clctcompmodelproviderParent,
				parentEntityUid, subform, prefsUserParent, entityPrefs, valueListProviderCache, state, eoController);
	}

	/**
	 * Ctor for creating a new MasterDataSubFormController with custom CollectableEntity.
	 */
	public MasterDataSubFormController(CollectableEntity clcte, MainFrameTab tab,
			CollectableComponentModelProvider clctcompmodelproviderParent, UID parentEntityUid, final SubForm subform,
			Preferences prefsUserParent, EntityPreferences entityPrefs, CollectableFieldsProviderCache valueListProviderCache,
			NuclosCollectControllerCommonState state, EntityCollectController<?, ?> eoController) {
		
		super(clcte, tab, clctcompmodelproviderParent, parentEntityUid, subform,
				prefsUserParent, entityPrefs, 
				MasterDataCollectableFieldsProviderFactory.newFactory(clcte.getUID(), valueListProviderCache), state, eoController);

		this.getSubForm().getSubFormFilter().addFilterChangeListener(this);

		getSubForm().getJTable().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
            public void valueChanged(ListSelectionEvent event) {
				final ListSelectionModel lsm = (ListSelectionModel) event.getSource();
				final ScriptEvaluator scriptEvaluator = ScriptEvaluator.getInstance();
				boolean enabled = !lsm.isSelectionEmpty() && getSubForm().canCreate() && MasterDataSubFormController.this.isEnabled();
				if (enabled && getSubForm().getCloneEnabledScript() != null) {
					for (int i : getSubForm().getJTable().getSelectedRows()) {
						Collectable<PK> c = getCollectables().get(getSubForm().getSubformTable().convertRowIndexToModel(i));
						Object o = null;
						try {
							o = scriptEvaluator.eval(getSubForm().getCloneEnabledScript(), 
									new SubformControllerScriptContext<PK>(getCollectController(), MasterDataSubFormController.this, c));
						} catch (InvocationTargetException e) {
							LOG.warn("Failed to evaluate script expression: " + e, e.getCause());
							o = false;
						} catch (Exception e) {
							LOG.warn("Failed to evaluate script expression: " + e, e);
							o = false;
						}
						if (o instanceof Boolean) {
							enabled = (Boolean) o;
						}
					}
				}
				final boolean bEnabled = enabled;

				UIUtils.invokeOnDispatchThread(new Runnable() {
					@Override
					public void run() {
						boolean enabled = bEnabled;
						//NUCLOS-2674. The old implementation didn't make any sense, as any changes on "enabled" didn't do anything.
						//final ToolbarFunctionState mainState = getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.CLONE);
						//final ToolbarFunctionState state = (enabled) ? ((null != mainState) ? mainState : ToolbarFunctionState.ACTIVE) : ToolbarFunctionState.DISABLED;
						final ToolbarFunctionState state = enabled ? ToolbarFunctionState.ACTIVE : ToolbarFunctionState.DISABLED;
						getSubForm().setToolbarFunctionState(ToolbarFunction.CLONE, state);
						getSubForm().setToolbarFunctionState(ToolbarFunction.LOCALIZATION, state);
					}
				});
			}
		});

		getSubForm().setToolbarFunctionState(ToolbarFunction.FILTER, ToolbarFunctionState.ACTIVE);

		UIUtils.invokeOnDispatchThread(new Runnable() {
			@Override
			public void run() {
				getSubForm().setToolbarFunctionState(ToolbarFunction.CLONE, ToolbarFunctionState.DISABLED);
			}
		});
		ListenerUtil.registerSubFormToolListener(getSubForm(), null, cloneListener);

		setupSubFormTableContextMenue();
		
	}

	private Date dateHistorical;

	public final boolean isHistoricalView() {
		return dateHistorical != null;
	}

	public void setHistoricalDate(Date dateHistorical) {
		this.dateHistorical = dateHistorical;
		for(MasterDataSubFormController<PK> child : getChildSubFormController())
			child.setHistoricalDate(dateHistorical);
	}

	protected final Date getHistoricalDate() {
		return dateHistorical;
	}

	protected void setupSubFormTableContextMenue() {
		IPopupListener popupMenuMouseAdapter = new PopupMenuMouseAdapter() {
			
			@Override
			public boolean doPopup(final MouseEvent e, JPopupMenu menu) {
				final JPopupMenu result = new JPopupMenu();

				final JMenuItem miDetails = new JMenuItem(getSpringLocaleDelegate().getMessage(
						"AbstractCollectableComponent.7","Details anzeigen..."));
				final JMenuItem miEdit = new JMenuItem(getSpringLocaleDelegate().getMessage(
						"AbstractCollectableComponent.21","Zelle bearbeiten"));

				miDetails.addActionListener(new ActionListener() {
					@Override
                    public void actionPerformed(ActionEvent ev) {
						try {
							cmdShowDetails();
						}
						catch(CommonFinderException e) {
							throw new NuclosFatalException(e);
						}
						catch(CommonPermissionException e) {
							throw new NuclosFatalException(e);
						}
						catch(CommonBusinessException e) {
							throw new NuclosFatalException(e);
						}
					}
				});
				result.add(miDetails);

				final int iRow = getSubForm().getJTable().rowAtPoint(e.getPoint());
				if (iRow >= 0) {
					final int iSelectedRowCount = getSubForm().getJTable().getSelectedRowCount();
					final UID entityUid = MasterDataSubFormController.this.getSelectedCollectable().getCollectableEntity().getUID();

					boolean bShowDetailsEnabled;
					try {
						bShowDetailsEnabled = iSelectedRowCount == 1
							&& getSelectedCollectable().getId() != null
								&& MasterDataLayoutHelper.isLayoutMLAvailable(entityUid, false);
					}
					catch (Exception ex) {
						bShowDetailsEnabled = false;
					}
					if (bShowDetailsEnabled) {
						bShowDetailsEnabled = SecurityCache.getInstance().isReadAllowedForEntity(entityUid);
					}
					
					miDetails.setEnabled(bShowDetailsEnabled);

					try {
						final int row = getSubForm().getJTable().rowAtPoint(e.getPoint());
						final int col = getSubForm().getJTable().columnAtPoint(e.getPoint());
						Object obj = getSubForm().getJTable().getValueAt(iRow, col);
						if(obj instanceof AbstractCollectableField) {
							AbstractCollectableField field = (AbstractCollectableField)obj;
							miEdit.setVisible(LangUtils.isValidURI(field.toString()));
							miEdit.addActionListener(new ActionListener() {
								@Override
								public void actionPerformed(ActionEvent e) {
									cmdEditCell(row, col);
								}
							});
							result.add(miEdit);
						}
						else {
							miEdit.setVisible(false);
						}
					}
					catch(Exception e1) {
						LOG.warn("setupSubFormTableContextMenue: " + e1);
						miEdit.setVisible(false);
					}
				}
				
				TableUtils.addCopyCellContentToPopupMenuIfPossible(result, getJTable(), e.getPoint(), true);
				
				if (menu != null) {
					result.addSeparator();
					for (MenuElement element : menu.getSubElements()) {
						if (element instanceof JMenuItem) {
							result.add((JMenuItem)element);
						}
					}
				}
				
				result.show((Component) e.getSource(), e.getX(), e.getY());
				
				return true;
			}
		};

		this.getSubForm().setPopupMenuListener(popupMenuMouseAdapter);
	}

	private void cmdEditCell(int row, int col) {
		this.getSubForm().getJTable().editCellAt(row, col);
		Component comp = this.getSubForm().getJTable().getEditorComponent();
		if(comp != null) {
			comp.requestFocusInWindow();
		}
	}

	private void cmdShowDetails() throws CommonBusinessException {
		UIUtils.runCommandForTabbedPane(getMainFrameTabbedPane(), new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				UID entityUid = MasterDataSubFormController.this.getSelectedCollectable().getCollectableEntity().getUID();
				showDetails(entityUid, MasterDataSubFormController.this.getSelectedCollectable().getId());
			}
		});
	}

	protected boolean getDefaultForOpenDetailsWithTabRecycling() {
		return false;
	}

	protected void showDetails(UID entityUID, Object id) {
		if (entityUID == null || id == null) {
			return;
		}
		final EntityMeta<?> eMeta = MetaProvider.getInstance().getEntity(entityUID);
		final boolean openDetailsWithTabRecycling;
		if (getSubForm().getOpenDetailsWithTabRecycling() == null) {
			//for backwards compatibility
			openDetailsWithTabRecycling = getDefaultForOpenDetailsWithTabRecycling();
		} else {
			openDetailsWithTabRecycling = getSubForm().getOpenDetailsWithTabRecycling();
		}
		if (openDetailsWithTabRecycling) {
			// with Tab-Recycling (NUCLOS-7224)
			final MainFrameTab tab = UIUtils.getTabForComponent(getSubForm().getJTable());
			if (eMeta.isUidEntity()) {
				final CollectController<UID,?> controller = (CollectController<UID, ?>) Main.getInstance().getMainController().getControllerForTab(tab);
				Main.getInstance().getMainController().showDetails(entityUID, (UID)id, false, tab, controller);
			} else {
				final CollectController<Long,?> controller = (CollectController<Long, ?>) Main.getInstance().getMainController().getControllerForTab(tab);
				Main.getInstance().getMainController().showDetails(entityUID, (Long)id, false, tab, controller);
			}
		} else {
			try {
				if (eMeta.isStateModel()) {
					showGenericObject((Long) id, entityUID);
				} else {
					showMasterData((PK) id, entityUID);
				}
			} catch (CommonBusinessException e) {
				Errors.getInstance().showExceptionDialog(getTab(), e);
			}
		}
	}

	private void showMasterData(final PK iMasterdataId,	UID entityUid) throws CommonBusinessException {
		if(MasterDataDelegate.getInstance().isSubformEntity(entityUid))
			return;
		MasterDataCollectController<PK> ctlMasterdata = NuclosCollectControllerFactory.getInstance().newMasterDataCollectController(
				entityUid, null, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
		ctlMasterdata.addCollectableEventListener(new DetailsCollectableEventListener(getCollectController(), ctlMasterdata));
		ctlMasterdata.runViewSingleCollectableWithId(iMasterdataId);
	}

	private void showGenericObject(final Long iGenericObjectId, UID entityUid) throws CommonBusinessException {
		if(MasterDataDelegate.getInstance().isSubformEntity(entityUid))
			return;
		final GenericObjectCollectController ctlGenericObject = NuclosCollectControllerFactory.getInstance().
				newGenericObjectCollectController(entityUid, null, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
		ctlGenericObject.addCollectableEventListener(new DetailsCollectableEventListener(getCollectController(), ctlGenericObject));
		ctlGenericObject.runViewSingleCollectableWithId(iGenericObjectId);
	}

	private static class LookupValuesListener<PK> implements LookupListener<PK> {

		private final SubFormTable subformtbl;

		private final boolean searchable;

		private final int row;

		private final Collection<TransferLookedUpValueAction> valueActions;

		private LookupValuesListener(SubFormTable subformtbl, boolean searchable, int row,
				Collection<TransferLookedUpValueAction> valueActions) {

			this.subformtbl = subformtbl;
			this.searchable = searchable;
			this.row = row;
			this.valueActions = valueActions;
		}

		@Override
		public void lookupSuccessful(LookupEvent<PK> ev) {
			if (ev.getSource() instanceof CollectableListOfValues) {
				final int iTargetColumn = subformtbl.getSubFormModel().findColumnByFieldUid(((CollectableListOfValues)ev.getSource()).getFieldUID());
				subformtbl.getSubFormModel().setValueAt(((CollectableListOfValues)ev.getSource()).getModel().getField(), subformtbl.getSelectedRow(), iTargetColumn);
			}
			SubForm.transferLookedUpValues(ev.getSelectedCollectable(), subformtbl, searchable,
					row, valueActions, true);
		}

		@Override
        public int getPriority() {
            return 1;
        }
	}

	/**
	 * @return true if new clct is inserted
	 * @throws NuclosBusinessException is thrown if reference entity could not be found in columns
	 */
	@Override
	public boolean insertNewRowWithReference(UID referenceEntity, Collectable<?> referenceClct, boolean validateWithVLP) throws NuclosBusinessException {
		if (getSubForm().getSubformTable().getCellEditor() != null)
			getSubForm().getSubformTable().getCellEditor().stopCellEditing();

		boolean clctChanged = false;

		CollectableEntityObject<PK> clct = insertNewRow();
		final int row = getCollectableTableModel().getRowCount()-1;

		getSubForm().getSubformTable().setRowSelectionInterval(row, row);
		
		boolean noReferenceFound = true;

		// find fields for copied entity objects
		for (final UID fieldUid : clct.getCollectableEntity().getFieldUIDs()) {
			CollectableEntityField clctef = clct.getCollectableEntity().getEntityField(fieldUid);
			if (referenceEntity.equals(clctef.getReferencedEntityUID())) {
				// for getting readable presentation and fire lookup use ListOfValues
                final CollectableListOfValues<PK> clctlov = new CollectableListOfValues<PK>(
                		clct.getCollectableEntity().getEntityField(fieldUid));
                try {
                	boolean insert = false;
                	//check provider if value exists in result

                	CollectableComponentTableCellEditor cellEditor = ((CollectableComponentTableCellEditor)getTableCellEditor(getJTable(), row, clctef));
                	if (cellEditor == null) {
                		// could happen, column is foreign column.
                		continue;
                	}
                	CollectableComponent clctcomp = cellEditor.getCollectableComponent();

                	noReferenceFound = false;

                	if (clctcomp instanceof LabeledCollectableComponentWithVLP) {
                		CollectableFieldsProvider provider = ((LabeledCollectableComponentWithVLP)clctcomp).getValueListProvider();
                		if (provider != null) {
                			if (validateWithVLP) {
	                            for (CollectableField clctField : provider.getCollectableFields()) {
	                            	if (clctField instanceof CollectableValueIdField) {
	                            		CollectableValueIdField clctValueIdField = (CollectableValueIdField) clctField;
	                            		if (referenceClct.getId().equals(clctValueIdField.getValueId())) {
	                            			insert = true;
	                            			break;
	                            		}
	                            	}
	                            }
                			} else {
                				insert = true;
                			}
                    	} else {
                    		insert = true;
                    	}
                	} else {
                		insert = true;
                	}

                	// set field
                    if (insert) {
                		final Collection<TransferLookedUpValueAction> valueActions =
                				getSubForm().getTransferLookedUpValueActions(fieldUid);
                        clctlov.addLookupListener(new LookupValuesListener<PK>(
                        		(SubFormTable)getJTable(), isSearchable(), row, valueActions));
                        try {
                            clctlov.acceptLookedUpCollectable((Collectable<PK>)referenceClct);
                            clct.setField(clctlov.getFieldUID(), clctlov.getField());
                            clctChanged = true;
                        }
                        catch(Exception e1) {
                            LOG.error("insertNewRowWithReference failed: " + e1, e1);
                        }
                    }
                }
                catch(CommonBusinessException e) {
                    LOG.error("insertNewRowWithReference failed: " + e, e);
                }
			}
		}

		if (!clctChanged) {
			getCollectableTableModel().remove(clct);
		}

		if (noReferenceFound)
			throw new NuclosBusinessException("Reference entity column " + referenceEntity + " not found.");

		return clctChanged;
	}
	
	/**
	 * @return true if new clct is inserted
	 * @throws NuclosBusinessException is thrown if reference entity could not be found in columns
	 */
	@Override
	public boolean insertNewRowWithAdditionalCollectable(CollectableEntityObject<PK> oldClct, Collectable<?> additionalClct, UID referencingEntityField) throws NuclosBusinessException {
		if (getSubForm().getSubformTable().getCellEditor() != null)
			getSubForm().getSubformTable().getCellEditor().stopCellEditing();

		boolean clctChanged = false;

		CollectableEntityObject<PK> clct = insertNewRow();
		final int row = getCollectableTableModel().getRowCount()-1;

		getSubForm().getSubformTable().setRowSelectionInterval(row, row);
		
		CollectableEntity clcte = clct.getCollectableEntity();
		
		for (UID uidField : clcte.getFieldUIDs()) {
			if (!uidField.equals(referencingEntityField)) {
				if (getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.CLONE) == ToolbarFunctionState.ACTIVE) {
					clct.setField(uidField, oldClct.getField(uidField));
				}
			} else {
				final CollectableListOfValues<PK> clctlov = new CollectableListOfValues<PK>(
                		clct.getCollectableEntity().getEntityField(uidField));
				final Collection<TransferLookedUpValueAction> valueActions =
        				getSubForm().getTransferLookedUpValueActions(uidField);
                clctlov.addLookupListener(new LookupValuesListener<PK>(
                		(SubFormTable)getJTable(), isSearchable(), row, valueActions));
                try {
                    clctlov.acceptLookedUpCollectable((Collectable<PK>)additionalClct);
                    clct.setField(clctlov.getFieldUID(), clctlov.getField());
                    clctChanged = true;
                } catch(Exception e1) {
                    LOG.error("insertNewRowWithReference failed: " + e1, e1);
                }
			}
		}
		
		
		
		return clctChanged;
	}

	private SubFormToolListener cloneListener = new SubFormToolListener() {
		@Override
		public void toolbarAction(String actionCommand) {
			ToolbarFunction cmd = ToolbarFunction.fromCommandString(actionCommand);
			//if(actionCommand.equals(TB_CLONE)) {
			if(cmd.equals(ToolbarFunction.CLONE)) {
				int[] selRows = getJTable().getSelectedRows();
				if (selRows == null) {
					return;
				}
				
				stopEditing();
				
				Boolean cloneDependents = null;
				for (int selRow : selRows) {
					if (selRow < 0) {
						continue;
					}
					
					int modelIndex = selRow;
					// for subform filters:
					if (getSubForm().getJTable().getRowSorter() != null) {
						modelIndex = getSubForm().getJTable().getRowSorter().convertRowIndexToModel(selRow);
					}

					// clone selected record
					CollectableEntityObject<PK> original = getCollectableTableModel().getCollectable(modelIndex);
					CollectableEntityObject<PK> clone = new CollectableEntityObject<PK>(
							original.getCollectableEntity(), original.getEntityObjectVO().copyFlat());

					clone.getEntityObjectVO().flagNew();

					// set selection for clone dependents
					getSubForm().getJTable().getSelectionModel().setSelectionInterval(selRow, selRow);
					
					try {
						cloneDependents = cloneRow(original, clone, cloneDependents);
					} catch (NuclosBusinessException e) {
						throw new CommonFatalException(e);
					}
				}
				
				getSubForm().getJTable().clearSelection();
				for (int selRow : selRows) {
					if (selRow < 0) {
						continue;
					}
					getSubForm().getJTable().getSelectionModel().addSelectionInterval(selRow, selRow);
				}
				
				// refresh sorting
				UIUtils.invokeOnDispatchThread(new Runnable() {
					@Override
					public void run() {
						AutonumberUiUtils.fixSubFormOrdering((SubFormTable) MasterDataSubFormController.this.getSubForm().getJTable());
					}
				});
			}
		}
	};
	
	/**
	 * 
	 * @param original
	 * @param clone
	 * @param cloneDependents:
	 * 		   true (clone dependents)
	 * 		   false (not clone dependents)
	 * 		   null (ask the user)
	 * @return cloneDependents:
	 * 		   true (clone dependents)
	 * 		   false (not clone dependents)
	 * 		   null (no answer yet)
	 * @throws NuclosBusinessException
	 */
	protected Boolean cloneRow(CollectableEntityObject<PK> original, CollectableEntityObject<PK> clone, Boolean cloneDependents) throws NuclosBusinessException {
		// check whether the selected record has some dependant data
		boolean hasDependantData = false;
		if (hasChildSubForm()) {
			for (MasterDataSubFormController<PK> mdsfctl : getChildSubFormController()) {
				if (!mdsfctl.getCollectables().isEmpty()) {
					hasDependantData = true;
					break;
				}
			}
		}

//		clone.getDependantMasterDataMap().clear();
//		clone.getDependantCollectableMasterDataMap().clear();

		int result = JOptionPane.NO_OPTION;
		if (cloneDependents == null && hasDependantData) {
			String sMessage = getSpringLocaleDelegate().getMessage(
					"MasterDataSubFormController.2", "Der zu klonende Datensatz besitzt abh\u00e4ngige Unterformulardaten. Sollen diese auch geklont werden?");
			result = JOptionPane.showConfirmDialog(getTab(), sMessage,
					getSpringLocaleDelegate().getMessage("MasterDataSubFormController.1", "Datensatz klonen"), JOptionPane.YES_NO_OPTION);
			cloneDependents = result == JOptionPane.YES_OPTION;
		}
		
		// add cloned data
		final UID entityUID = getSubForm().getEntityUID();
		// Do not clone this system attributes (NUCLOS-7095)
		clone.setField(SF.CREATEDAT.getUID(entityUID), CollectableValueField.NULL);
		clone.setField(SF.CREATEDBY.getUID(entityUID), CollectableValueField.NULL);
		clone.setField(SF.CHANGEDAT.getUID(entityUID), CollectableValueField.NULL);
		clone.setField(SF.CHANGEDBY.getUID(entityUID), CollectableValueField.NULL);
		clone.setField(SF.VERSION.getUID(entityUID), CollectableValueField.NULL);
		clone.setField(SF.STATE.getUID(entityUID), CollectableValueField.NULL);
		clone.setField(SF.STATEICON.getUID(entityUID), CollectableValueField.NULL);
		clone.setField(SF.STATENUMBER.getUID(entityUID), CollectableValueField.NULL);
		clone.setField(SF.SYSTEMIDENTIFIER.getUID(entityUID), CollectableValueField.NULL);
		for (Column column : getSubForm().getColumns()) {
			final UID columnUID = column.getUID();
			if (!column.isCloneable()) {
				// Set to default value... 'new like' (NUCLOS-7095)
				if (!Utils.setDefaultValue(clone, getCollectableEntity(), columnUID)) {
					clone.setField(columnUID, CollectableValueField.NULL);
				}
			}
		}
		CollectableEntityObject<PK> clctNew = insertNewRow(clone);
		setParentId(clctNew, getParentId());
		getCollectableTableModel().add(clctNew);

		// clone and add dependant data
		if (Boolean.TRUE.equals(cloneDependents) && hasDependantData) {
			cloneDependantData(original, clone);
		}
		
		return cloneDependents;
	}

	/**
	 * clones the dependant data of the given original collectable recursively
	 * @param clctmd_original original collectable masterdata
	 * @param clctmd_clone a clone of the original collectable masterdata
	 * @throws NuclosBusinessException
	 */
	protected void cloneDependantData(CollectableEntityObject<PK> clctmd_original, CollectableEntityObject<PK> clctmd_clone) throws NuclosBusinessException {
		for (MasterDataSubFormController<PK> mdsfctl : getChildSubFormController()) {
			Collection<CollectableEntityObject<PK>> lsclctmd = mdsfctl.readDependants(Collections.singleton(clctmd_original), null).get(clctmd_original);
			mdsfctl.setCollectableParent(clctmd_clone);

			for (CollectableEntityObject<PK> clctmd : lsclctmd) {
				if (!clctmd.isMarkedRemoved()) {
					CollectableEntityObject<PK> clone = new CollectableEntityObject<PK>(
							clctmd.getCollectableEntity(), clctmd.getEntityObjectVO().copyFlat());
					clone.setField(mdsfctl.getForeignKeyFieldUID(), CollectableValueIdField.NULL); // reset referencing field.- @see NUCLOS-736
					mdsfctl.insertNewRow(clone);
					mdsfctl.cloneDependantData(clctmd, clone);
				}
			}
		}
	}

	@Override
	protected void postCreate() {
		this.setupListSelectionListener();

		getSubForm().addLookupListener(new LookupListener<PK>() {
			@Override
			public void lookupSuccessful(LookupEvent<PK> ev) {
				if (ev.getAdditionalCollectables() != null) {
					for (Collectable<PK> clct : ev.getAdditionalCollectables()) {
						CollectableEntityField clctef = ev.getCollectableComponent().getEntityField();
//						clctef.getReferencedEntityUID();
						try {
							if (getSubForm().isReadOnly() || getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.NEW) != ToolbarFunctionState.ACTIVE) {
								return;
							}
		                    insertNewRowWithAdditionalCollectable(MasterDataSubFormController.this.getSelectedCollectable(), clct, clctef.getUID());
	                    }
	                    catch(NuclosBusinessException e) {
                            LOG.error("lookupSuccessful failed: " + e, e);
	                    }
					}
					AutonumberUiUtils.fixSubFormOrdering(getSubForm().getSubformTable());
				}
			}

			@Override
            public int getPriority() {
	            return 2;
            }
		});

		getSubForm().setLookupService(new ColletableEntityObjectLookup<PK>() {

			@Override
			public void setCollectableLocalizedComponent(
					CollectableLocalizedComponent<PK> lclFieldComponent, int row) {
				if (row >= 0 && getCollectables().size() > row) {
					CollectableEntityObject<PK> selectedCollectable = getCollectables().get(row);
					if (selectedCollectable != null) {
						lclFieldComponent.getDetailsComponentModel().initDataLanguageMap(selectedCollectable.getEntityObjectVO().getDataLanguageMap(), getTab());
						lclFieldComponent.getDetailsComponentModel().setPrimaryKey(selectedCollectable.getEntityObjectVO().getPrimaryKey());	
						lclFieldComponent.getDetailsComponentModel().setSubForm(getSubForm());
					}					
				}
			}});
		
		super.postCreate();
	}

	/**
	 * releases resources, removes listeners.
	 */
	@Override
	public void close() {
		this.removeListSelectionListener();
		if (ssfClientWorker != null) {
			ssfClientWorker.interrupt();
		}
		super.close();
	}

	private void setCollectableParent(CollectableMasterData<PK> clctParent) {
		this.clctParent = clctParent;
		if (clctParent != null) {
			this.setParentId(clctParent.getPrimaryKey());
		} else {
			this.setParentId(null);
		}
	}

	protected CollectableMasterData<PK> getCollectableParent() {
		return this.clctParent;
	}
	
	private Map<Object, CollectableMasterData<PK>> mapCollectableParents = new HashMap<Object, CollectableMasterData<PK>>();
	private void clearCollectableParentMap() {
		mapCollectableParents.clear();
	}
	
	private void addCollectableParentToMap(CollectableMasterData<PK> clctParent) {
		mapCollectableParents.put(clctParent.getPrimaryKey(), clctParent);
	}
	
	private List<CollectableMasterData<PK>> getCollectableParents() {
		List<CollectableMasterData<PK>> lstCollParents = new ArrayList<CollectableMasterData<PK>>();
		CollectableMasterData<PK> parent = getCollectableParent();
		if (parent != null) {
			lstCollParents.add(parent);
			return lstCollParents;
		}
		for (Object key : mapCollectableParents.keySet()) {
			lstCollParents.add(mapCollectableParents.get(key));
		}
		return lstCollParents;
	}

	/**
	 * @return a new collectable adapter object containing a MasterDataVO
	 */
	@Override
	public CollectableEntityObject<PK> newCollectable() {
		final CollectableEntity clctmde = getCollectableEntity();
		return new CollectableEntityTemplate<PK>(clctmde, (EntityObjectVO<PK>) EntityObjectVO.newObject(clctmde.getUID()));
	}

	@Override
	protected ValueObjectList<CollectableEntityObject<PK>> newValueObjectList(List<CollectableEntityObject<PK>> lstclct) {
		return new DefaultValueObjectList<CollectableEntityObject<PK>>(lstclct);
	}

	/**
	 * fills this subform by loading the dependant data.
	 * 
	 * §todo generalize/refactor
	 * 
	 * @param iParentId
	 * @throws NuclosBusinessException
	 */
	public void fillSubForm(Object iParentId) throws NuclosBusinessException {
		this.setParentId(iParentId);

		final Collection<EntityObjectVO<PK>> collmdvo = (Collection<EntityObjectVO<PK>>) (iParentId == null 
				? new ArrayList<EntityObjectVO<PK>>() 
				: MasterDataDelegate.getInstance().getDependentDataCollectionWithLimit(
						getForeignKeyFieldUID(), null, getSubForm().getMapParams(),
						getCurrentLayoutUid(),
						getSubForm().getMaxEntries(), iParentId));

		fillSubForm(iParentId, collmdvo);
	}

	/**
	 * fills this subform with data in collection.
	 * @param collmdvo
	 * @throws NuclosBusinessException
	 */
	public void fillSubForm(Object iParentId, final Collection<EntityObjectVO<PK>> collmdvo) throws NuclosBusinessException {
		setParentId(iParentId);
		UIUtils.invokeOnDispatchThread(new Runnable() {
			@Override
			public void run() {
				boolean bWasIgnorePreferencesUpdate = isIgnorePreferencesUpdate();
				setIgnorePreferencesUpdate(true);
				final SizeKnownListener listener = getSubForm().getSizeKnownListener();
				// Trigger the 'loading' display...
				if (listener != null) {
					listener.actionPerformed(new SizeKnownEvent(getSubForm(), null));
				}
				
				@SuppressWarnings("unchecked")
				// TODO NUCLOS-6048 Yet another transformation for nothing...
				List<CollectableEntityObject<PK>> lst = CollectionUtils.transform((Collection<EntityObjectVO<PK>>)
					collmdvo == null ? Collections.emptyList() : collmdvo,
					new CollectableEntityObject.MakeCollectable(getCollectableEntity()));

				if (getCollectController() != null) {
					CollectState clctState = getCollectController().getCollectState();
					if (clctState.getOuterState() == CollectState.OUTERSTATE_DETAILS && clctState.getInnerState() == CollectState.DETAILSMODE_NEW_CHANGED) {
						if (!getSubForm().isCloneable()) {
							lst = Collections.emptyList();
						} else {

							UID entityUid = getEntityAndForeignKeyField().getEntity();
							for (Column column : getSubForm().getColumns()) {
								if (!column.isCloneable()) {
									for (CollectableEntityObject<PK> clcteo : lst) {
										if (clcteo.getField(column.getUID()).getValue() instanceof Boolean) {
											clcteo.setField(column.getUID(), new CollectableValueField(Boolean.FALSE), true);
										} else {
											clcteo.setField(column.getUID(), clcteo.getField(column.getUID()).isIdField() ? CollectableValueIdField.NULL : CollectableValueField.NULL, true);
										}
									}
								}
							}

							List<UID> systemFields = Arrays.asList(SF.CHANGEDAT.getUID(entityUid),
									SF.CREATEDAT.getUID(entityUid),
									SF.CHANGEDBY.getUID(entityUid),
									SF.CREATEDBY.getUID(entityUid),
									SF.STATE.getUID(entityUid),
									SF.STATEICON.getUID(entityUid),
									SF.STATENUMBER.getUID(entityUid),
									SF.SYSTEMIDENTIFIER.getUID(entityUid));

							for (UID field : systemFields) {
								for (CollectableEntityObject<PK> clcteo : lst) {
									clcteo.setField(field, clcteo.getField(field).isIdField() ? CollectableValueIdField.NULL : CollectableValueField.NULL, true);
								}
							}
						}
					}
				}

				MasterDataSubFormController.this.updateTableModel(lst);
				setIgnorePreferencesUpdate(true);

				int size = lst.size();
				if (!getSubForm().isReadOnly()) {
					// count removed ones.
					if (collmdvo != null) {
						for (EntityObjectVO<PK> eo : collmdvo) {
							if (eo.isFlagRemoved()) {
								size--;
							}
						}
					}
				}

				// Trigger the 'size' display...
				if (listener != null) {
					listener.actionPerformed(new SizeKnownEvent(getSubForm(), size));
				}
				
				setIgnorePreferencesUpdate(bWasIgnorePreferencesUpdate);
			}
		});
	}
	
	// Child-Subform (Sub-SubForm) Section Start

	public void displayLimitBubble(final int limit) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				Bubble bubbleLimit = new Bubble(getSubForm().getToolbarButton(ToolbarFunction.SUBFORM_DETAILS_VIEW.name()),
						getSpringLocaleDelegate().getMessage("CollectController.47",
								"Die Anzahl der angezeigten Datensätze wurde auf {0} begrenzt. Weitere Datensätze sind in der Detail-Ansicht verfügbar.", limit),
						5);
				bubbleLimit.setVisible(true);
			}
		});
	}
	
	private class RowSelectionListener implements ListSelectionListener {

		@Override
        public void valueChanged(ListSelectionEvent event) {
			if (event.getValueIsAdjusting() || isMultiEdit()) {
				return;
			}
			
			List<CollectableEntityObject<PK>> lstCeo = getSelectedCollectables();
			
			for (MasterDataSubFormController<PK> controller : getChildSubFormController()) {
				controller.fillAsSubFormChild(lstCeo, false);
			}
		}
		
	}
	
	Collection<CollectableEntityObject<PK>> pendingCollectables;
	
	@Override
	public void selectionChanged(boolean b) {
		// NUCLOS-5754 load those sub-child which were postponed and are selected now
		if (b && pendingCollectables != null) {
			fillAsSubFormChild(pendingCollectables, true);
		}		
	}
	
	/**
	 * fillAsSubFormChild... (Re)Fill Subform with data dependend on parent-cltcs (clcts). Keep public in any case!
	 * @param clcts ParentData from which the dependendts should be loaded
	 * @param force if true: Load them now, even thoug the tab has not been yet selected 
	 */
	public void fillAsSubFormChild(final Collection<CollectableEntityObject<PK>> clcts, boolean force) {
		
		// NUCLOS-5754 Within a tabbed pane, load only sub-subforms in selected tabs. postpone the others	
		if (!force && !clcts.isEmpty() && getSubForm().isWithinTabbedPane() && !getSubForm().isSelected()) {
			pendingCollectables = clcts;
			return;
		}
		
		pendingCollectables = null;

		getSubForm().setLockedLayer();

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					fillAsSubFormChildLater(clcts);
				} catch (CommonBusinessException cbe) {
					LOG.warn(cbe.getMessage(), cbe);
				}
			}
		});

	}

	SSFClientWorker ssfClientWorker;

	private void fillAsSubFormChildLater(Collection<CollectableEntityObject<PK>> clcts) throws CommonBusinessException {

		if (ssfClientWorker != null) {
			ssfClientWorker.interrupt();
		}

		// NUCLOS-6151 Make Asynchronous
		ssfClientWorker = new SSFClientWorker(clcts);
		CommonMultiThreader.getInstance().execute(ssfClientWorker);
	}

	private class SSFClientWorker extends EntityCollectController.SubFormsInterruptableClientWorker {
		private final Collection<CollectableEntityObject<PK>> clcts;
		private final boolean multiEdit;
		private final boolean historical;
		private final CollectableEntityObject<PK> firstParent;
		private final AtomicBoolean bTruncated;

		private SSFClientWorker(Collection<CollectableEntityObject<PK>> clcts) {
			if (clcts.isEmpty()) {
				this.clcts = Collections.singleton(null);
			} else {
				this.clcts = new ArrayList<>(clcts);
			}
			this.multiEdit = this.clcts.size() >= 2;
			this.firstParent = this.clcts.iterator().next();
			this.historical = isHistoricalView();
			this.bTruncated = new AtomicBoolean();
		}

		@Override
		public void init() throws CommonBusinessException {
			if (interrupted || getSubForm() == null) {
				return;
			}

			getSubForm().setLockedLayer();
			getSubForm().setEnabled(multiEdit || firstParent != null); // enabled or disabled from Layout is respected via setEnabledBy

			if (interrupted || getSubForm() == null) {
				return;
			}

			setMultiEdit(multiEdit);
			setCollectableParent(null);

			if (!multiEdit) {
				setMultiUpdateOfDependants(null);
				setCollectableParent(firstParent);
			}

			if (isHistoricalView()) {

				if (multiEdit) { // Only non-historical Subforms can be multi-edited.
					getSubForm().setEnabled(false);
					setMultiEdit(false);
					return;
				}
				prepareSubformForHistoricalView(firstParent);
			}
		}

		private final Map<CollectableEntityObject<PK>, List<CollectableEntityObject<PK>>> data = new HashMap<>();

		@Override
		public  void work() throws CommonBusinessException {

			if (isHistoricalView() && multiEdit) {
				return;
			}

			synchronized (MasterDataSubFormController.this) {
				if (interrupted || getSubForm() == null) {
					return;
				}
				bTruncated.set(false);
				//NUCLOS-5741 1b.
				final Map<CollectableEntityObject<PK>, Collection<CollectableEntityObject<PK>>> mpDeps = readDependants(clcts, bTruncated);

				clearCollectableParentMap();
				data.clear();

				CollectState clctState = getCollectController().getCollectState();
				if (!(clctState.getOuterState() == CollectState.OUTERSTATE_DETAILS && clctState.getInnerState() == CollectState.DETAILSMODE_NEW_CHANGED && !getSubForm().isCloneable())) {
					for (CollectableEntityObject<PK> clct : clcts) {
						if (clct != null) {
							addCollectableParentToMap(clct);
						}
						data.put(clct, new ArrayList<>(mpDeps.get(clct)));
					}
				}
			}

		}

		@Override
		public void paint() throws CommonBusinessException {
			if (interruptedOrNotSupported() || getSubForm() == null) {
				return;
			}

			final CollectController<?, ?> clctCtrl = getCollectController();
			final MasterDataSubFormController<PK> _this = MasterDataSubFormController.this;
			final boolean bWasDetailsChangedIgnored = clctCtrl.isDetailsChangedIgnored();
			clctCtrl.setDetailsChangedIgnored(true);
			final AbstractScriptContext<PK> asc;

			if (!multiEdit) {
				List<CollectableEntityObject<PK>> lstclctmd = data.get(firstParent);
				if (lstclctmd == null) {
					lstclctmd = new ArrayList<>();
				}
				updateTableModel(lstclctmd);

				final SizeKnownListener listener = getSubForm().getSizeKnownListener();
				if (listener != null) {
					int size = lstclctmd.size();
					for (CollectableEntityObject<PK> eo : lstclctmd) {
						if (eo.getEntityObjectVO().isFlagRemoved()) {
							size--;
						}
					}
					listener.actionPerformed(new SizeKnownEvent(getSubForm(), size));
				}

				asc = new SubformControllerScriptContext<PK>(clctCtrl, _this, firstParent);
			}
			else {

				new MultiEditWithDependants<PK>(data, MasterDataSubFormController.this); //Note that things are done within the constructor, like registering itself
				asc = new SubformControllerCollectiveProcessingScriptContext<PK>(clctCtrl, _this, clcts);
			}

			getSubForm().setNewEnabled(asc);

			clctCtrl.setDetailsChangedIgnored(bWasDetailsChangedIgnored);

			if (bTruncated.get()) {
				displayLimitBubble(getSubForm().getMaxEntries());
			}

			getSubForm().forceUnlockFrame();

		}

		private boolean interruptedOrNotSupported() {
			return interrupted || (historical && multiEdit);
		}

		@Override
		public void handleError(Exception ex) {
			if (!interrupted) {
				Errors.getInstance().showExceptionDialog(getResultsComponent(), ex);
			}
		}

		@Override
		public JComponent getResultsComponent() {
			return getSubForm();
		}

	}

	// Child-Subform (Sub-SubForm) Section End
	
	/**
	 * read all dependant data for the given clct
	 * @param clcts
	 * @param bTruncated: AtomicBoolean if the reading has been truncted. if bTruncated == null, it will not be truncted;
	 * @return List<CollectableMasterData>
	 */
	private Map<CollectableEntityObject<PK>, Collection<CollectableEntityObject<PK>>> readDependants(Collection<CollectableEntityObject<PK>> clcts,
																									 AtomicBoolean bTruncated) {
		if (isHistoricalView()) { // must be an historical view of parent controller.
			return readDependentsForHistoricalView(clcts);
		}
		
		Map<CollectableEntityObject<PK>, Collection<CollectableEntityObject<PK>>> result = new HashMap<>();
		IDependentKey dependentKey = getEntityAndForeignKeyField().getDependentKey();
		
		List<PK> ids = new ArrayList<PK>();
		
		for (CollectableEntityObject<PK> clct : clcts) {
			if (clct == null) { 
				result.put(clct, Collections.<CollectableEntityObject<PK>>emptyList());
				continue;
			}
			
			Collection<CollectableEntityObject<PK>> loadedDependents = RigidUtils.uncheckedCast(clct.getDependantCollectableMasterDataMap().getValues(dependentKey));
			if (!loadedDependents.isEmpty() || clct.getPrimaryKey() == null) {
				result.put(clct, new ArrayList<CollectableEntityObject<PK>>(loadedDependents));
				continue;
			}
			
			ids.add(clct.getPrimaryKey());		
		}
		
		//The actual loading of the sub-sub-date. For collective processing the ids may have more than one element
		if (!ids.isEmpty()) {
			MakeCollectable<PK> makeCollectable = new MakeCollectable<PK>(getCollectableEntity());
			PK[] pks = RigidUtils.uncheckedCast(ids.toArray());
			
			final Integer maxEntries = bTruncated != null ? getSubForm().getMaxEntries() : null;

			final Collection<EntityObjectVO<PK>> collmdvo = MasterDataDelegate.getInstance().getDependentDataCollectionWithLimit(
					getForeignKeyFieldUID(), null, getSubForm().getMapParams(), getCurrentLayoutUid(), maxEntries, pks);
			
			// Truncated is always false for maxEntries == 0, as getDependentDataCollectionWithLimit() yields an empty list for limit == 0.
			final boolean truncated = maxEntries != null && collmdvo.size() == maxEntries + 1;
			if (truncated) {
				getSubForm().setReadOnlyWithDetailView(true, true);
				bTruncated.set(true);
			}
			
			//NUCLOS-5741 1b) Assignment of the collectables to their respective parent
			for (CollectableEntityObject<PK> clct : clcts) {
				if (result.containsKey(clct)) {
					continue;
				}				
				result.put(clct, new ArrayList<CollectableEntityObject<PK>>());
				
				for (EntityObjectVO<PK> eo : collmdvo) {
					Object fk = clct.getPrimaryKey() instanceof Long ? eo.getFieldId(dependentKey.getDependentRefFieldUID())
							: eo.getFieldUid(dependentKey.getDependentRefFieldUID());
					
					//NOTE: By the following comparison, objects without a foreign key (or not matching foreign keys) are omitted
					if (LangUtils.equal(clct.getPrimaryKey(), fk)) {
						CollectableEntityObject<PK> ceo = makeCollectable.transform(eo);
						result.get(clct).add(ceo);
					}
				}
				
	            clct.getDependantCollectableMasterDataMap().addValues(dependentKey, result.get(clct));
			}

		}
		
		return result;
	}

	private Map<CollectableEntityObject<PK>, Collection<CollectableEntityObject<PK>>> readDependentsForHistoricalView(Collection<CollectableEntityObject<PK>> clcts) {
		Map<CollectableEntityObject<PK>, Collection<CollectableEntityObject<PK>>> result = new HashMap<>();
		IDependentKey dependentKey = getEntityAndForeignKeyField().getDependentKey();
		
		MakeCollectable makeCollectable = new MakeCollectable<PK>(CollectableEOEntityClientProvider.getInstance().getCollectableEntity(
				getEntityAndForeignKeyField().getEntity()));
		
		for (CollectableEntityObject<PK> clct : clcts) {
			Collection<CollectableEntityObject<PK>> lstclctmd =
				CollectionUtils.transform(clct.getMasterDataCVO().getEntityObject().getDependents().getData(dependentKey), makeCollectable);			
			result.put(clct, lstclctmd);
		}
		
		return result;
	}
	
	private void prepareSubformForHistoricalView(final CollectableEntityObject<PK> clct) {
		if (clct == null) {
			return;
		}
		
		try {
			
			final EntityObjectVO<PK> eo = clct.getEntityObjectVO();
			final CollectableEntity clcte = clct.getCollectableEntity();
			final CollectableWithDependants<PK> clctWithDependants;
			if (!Modules.getInstance().isModule(clcte.getUID())) {
				clctWithDependants = CollectableMasterDataWithDependants.newInstance(clcte, DalSupportForMD.wrapEntityObjectVO(eo));
			} else {
				EntityObjectVO<Long> eoLong = RigidUtils.uncheckedCast(eo);
				clctWithDependants = RigidUtils.uncheckedCast(CollectableGenericObjectWithDependants.newCollectableGenericObjectWithDependants(
						DalSupportForGO.getGenericObjectVO(eoLong, (CollectableEOEntity)clcte)));
			}
			
			if (clctWithDependants instanceof CollectableMasterDataWithDependants) {
				((CollectableMasterDataWithDependants<PK>)clctWithDependants).getMasterDataWithDependantsCVO().setDependents(
						((CollectableEntityObject<PK>)clct).getEntityObjectVO().getDependents());
			}
			if (clctWithDependants instanceof CollectableGenericObjectWithDependants) {
				((CollectableGenericObjectWithDependants)clctWithDependants).getGenericObjectWithDependantsCVO().setDependents(
						((CollectableEntityObject<PK>)clct).getEntityObjectVO().getDependents());
			}
			
			if ((clct.getId() instanceof UID) || (clct.getId() instanceof Long && ((Long) clct.getId()).intValue() > 0)) {
				final CollectableWithDependants<PK> mdwdCurrent = new CollectableMasterDataWithDependants<PK>(getCollectableEntity(), 
						MasterDataDelegate.getInstance().getWithDependants(clct.getCollectableEntity().getUID(), 
						clct.getId(), -1));
				EntityCollectController.markSubformInHistoricalView(this, clctWithDependants, mdwdCurrent);
			}
			
		} catch (CommonBusinessException e) {
			throw new NuclosFatalException(e);
		}
	}

	private void setupListSelectionListener() {
		JTable tbl = getJTable();
		// initialize listener for row selection in the table:
		this.rowselectionlistener = new RowSelectionListener();
		tbl.getSelectionModel().addListSelectionListener(this.rowselectionlistener);
	}

	private void removeListSelectionListener() {
		if (getSubForm() != null) {
			getJTable().getSelectionModel().removeListSelectionListener(this.rowselectionlistener);
		}
		this.rowselectionlistener = null;
	}

	// add child subform controller to this subform
	public void addChildSubFormController(MasterDataSubFormController<PK> childSubFormController) {
		this.lstChildSubController.add(childSubFormController);
	}

	// get all child subform controller of this subform
	public List<MasterDataSubFormController<PK>> getChildSubFormController() {
		return this.lstChildSubController;
	}

	/**
	 * inserts a new row. The foreign key field to the parent entity will be set accordingly.
	 */
	@Override
	public CollectableEntityObject<PK> insertNewRow() throws NuclosBusinessException {
		CollectableEntityObject<PK> clct = super.insertNewRow();
		return insertNewRow(clct);
	}

	/**
	 * This method is required when changes in row selection during the insertion process are expected
	 * (i.e. in case the insertion happens within a {@link SwingWorker}
	 * 
	 * in the mentioned case, the assignment might happen to a wrong row if the parent collectable would be
	 * determined during the insertion process 
	 *  
	 * @see RSWORGA-172
	 * 
	 * @param clct			{@link CollectableEntityObject} for insertion. It has to be an existing Collectable created by this Controller instance
	 * @param collectableParent	{@link CollectableMasterData} parent object
	 * 
	 * @throws NuclosBusinessException
	 */
	public void insertNewChildRow(final CollectableEntityObject<PK> clct, final CollectableMasterData<PK> collectableParent) throws NuclosBusinessException {
		if (null == clct) {
			throw new IllegalArgumentException("collectable must not be null");
		}
		if (null == collectableParent) {
			throw new IllegalArgumentException("parent collectable must not be null");
		}

		if (this.isChildSubForm()) {
			CollectableEntityTemplate<PK> cet = null;
			if (cet == null) {
				collectableParent.getDependantCollectableMasterDataMap().addValue(getEntityAndForeignKeyField().getDependentKey(), clct);
				if (clct instanceof CollectableEntityTemplate)
					cet = (CollectableEntityTemplate<PK>) clct;
			} else {
				CollectableEntityObject<PK> ceo2 = newCollectable();
				cet.addSlave(ceo2);
				collectableParent.getDependantCollectableMasterDataMap().addValue(getEntityAndForeignKeyField().getDependentKey(), ceo2);
			}

			blockForMultiEdit(false);

			// this is required to fire change event of the table model
			// FIXME guarantee that clct is existing row
			insertExistingRow(clct);
		}
	}

	private CollectableEntityObject<PK> insertNewRow(CollectableEntityObject<PK> clct) throws NuclosBusinessException {
		if (this.isChildSubForm()) {
			List<CollectableMasterData<PK>> lstCmd = this.getCollectableParents();
			if (lstCmd == null || lstCmd.isEmpty()) {
				String sMessage = getSpringLocaleDelegate().getMessage(
						"MasterDataSubFormController.3", "Es kann kein Bezug zu einem \u00dcbergeordneten Datensatz hergestellt werden. "+
						"Bitte w\u00e4hlen Sie zuerst einen Datensatz aus dem \u00fcbergeordneten Unterformular aus.");
				throw new NuclosBusinessException(sMessage);
			}

			IDependentKey dependendKey = getEntityAndForeignKeyField().getDependentKey();
			CollectableEntityTemplate<PK> cet = null;
			for (CollectableMasterData<PK> cmd : lstCmd) {
				if (cet == null) {
					cmd.getDependantCollectableMasterDataMap().addValue(dependendKey, clct);
					if (clct instanceof CollectableEntityTemplate) {
						cet = (CollectableEntityTemplate<PK>) clct;						
					}
				} else {
					CollectableEntityObject<PK> ceo2 = newCollectable();
					cet.addSlave(ceo2);
					cmd.getDependantCollectableMasterDataMap().addValue(dependendKey, ceo2);
				}
			}
			if (lstCmd.size() >= 2) blockForMultiEdit(true);
		}
		return clct;
	}
	
	public void insertTransferedRows(CollectableEntityObject<PK> prototype, Map<PK, CollectableEntityObject<PK>> newCeos) {
		//TODO handle Prototype for later updates.
		if (this.isChildSubForm()) {
			List<CollectableMasterData<PK>> lstCmd = this.getCollectableParents();
			if (lstCmd != null) {
				for (CollectableMasterData<PK> cmd : lstCmd) {
					Long parentId = (Long) cmd.getPrimaryKey();
					CollectableEntityObject<PK> ceo = newCeos.get(parentId);
					if (ceo != null) {
						cmd.getDependantCollectableMasterDataMap().addValue(getEntityAndForeignKeyField().getDependentKey(), ceo);					
					}
				}
			}
			if (lstCmd.size() >= 2) {
				blockForMultiEdit(true);
			}
		}
	}

	@Override
	protected CollectableField getFieldFromParentSubform(UID fieldUid) {
		if(isChildSubForm()) {
			DetailsSubFormController<PK,CollectableEntityObject<PK>> subDetailsParent = 
					parentController.getDetailsSubforms().get(getSubForm().getParentSubForm());
			subDetailsParent.stopEditing();
			CollectableMasterData<PK> md = subDetailsParent.getSelectedCollectable();
			return md.getField(fieldUid);
		}
		return null;
	}
	
	protected void valueSet() {
		if (isChildSubForm()) {
			List<CollectableMasterData<PK>> lstCmd = this.getCollectableParents();
			if (lstCmd != null && lstCmd.size() >= 2) {
				blockForMultiEdit(true);				
			}			
		}
	}
	
	private void blockForMultiEdit(boolean block) {
		if(isChildSubForm()) {
			DetailsSubFormController<PK,CollectableEntityObject<PK>> subDetailsParent = parentController.getDetailsSubforms().get(getSubForm().getParentSubForm());
			String text = getSpringLocaleDelegate().getMessage("DetailsSubFormController.9", "Sammelbearbeitung aktiv. Bitte speichern zum Entsperren!");
			subDetailsParent.blockSubForm(text, block);
		}
	}

	@Override
	protected void removeSelectedRows() {
		// this is necessary for rows that have been added and again removed before saving
		for (CollectableEntityObject<PK> clct : this.getSelectedCollectables()) {
			clct.markRemoved();
		}
		super.removeSelectedRows();
		if (this.isChildSubForm()) {
			List<CollectableMasterData<PK>> lstCmd = this.getCollectableParents();
			if (lstCmd.size() >= 2) blockForMultiEdit(true);
		}
	}

	/**
	 * checks whether this subform is a child subform
	 * @return true, if this subform data depends on parent subform data, otherwise false
	 */
	public boolean isChildSubForm() {
		return (this.getSubForm().getParentSubForm() == null) ? false : true;
	}

	/**
	 * checks whether this subform has assigned one or more child subforms
	 * @return true, if child(ren) is/are assigned, otherwise false
	 */
	public boolean hasChildSubForm() {
		return !getChildSubFormController().isEmpty();
	}

	@Override
	public void selectFirstRow() {
		if (hasChildSubForm() && getJTable().getRowCount() > 0) {
			getJTable().clearSelection();
			getJTable().setRowSelectionInterval(0,0);
		}
	}

	// clears all child subformcontroller of this controller recursively
	public void clearChildSubFormController() {
		for (MasterDataSubFormController<PK> subformcontroller : lstChildSubController) {
			subformcontroller.clearChildSubFormController();
			subformcontroller.fillAsSubFormChild(Collections.<CollectableEntityObject<PK>>emptyList(), true);
			if (subformcontroller.getCollectableParent() != null) {
				subformcontroller.getCollectableParent().getDependantCollectableMasterDataMap().clear();
				subformcontroller.setCollectableParent(null);
			}
		}
	}

	/**
	 * removes all rows from this subform.
	 */
	@Override
	public void clear() {
		UIUtils.invokeOnDispatchThread(new Runnable() {
			@Override
			public void run() {
				boolean bWasIgnorePreferencesUpdate = isIgnorePreferencesUpdate();
				setIgnorePreferencesUpdate(true);
				//@see NUCLOS-1139
				final SizeKnownListener listener = getSubForm().getSizeKnownListener();
				// Trigger the 'loading' display...
				if (listener != null) {
					listener.actionPerformed(new SizeKnownEvent(getSubForm(), null));
				}
				
				boolean bWasDetailsChangedIgnored = false;
				if (getParentController() != null) {
					bWasDetailsChangedIgnored = getParentController().isDetailsChangedIgnored();
					getParentController().setDetailsChangedIgnored(true);
				}
				
				MasterDataSubFormController.super.clear();
				clearChildSubFormController();
				
				if (getParentController() != null) {
					getParentController().setDetailsChangedIgnored(bWasDetailsChangedIgnored);
				}
				setIgnorePreferencesUpdate(bWasIgnorePreferencesUpdate);
			}
		});
	}

	public void setParentController(EntityCollectController<PK,CollectableEntityObject<PK>> parentController) {
		this.parentController = parentController;
	}
	
	protected EntityCollectController<PK,CollectableEntityObject<PK>> getParentController() {
		return parentController;
	}

	public void filterChanged(SubFormFilterChangeEvent e) {
		if (e.getFilter() instanceof CollectableSearchCondition) {
			this.getParentController().refreshSubForm(this, (CollectableSearchCondition) e.getFilter());
		}
		getSubForm().setStatusbarRowCount(getSubForm().getJTable().getRowCount());
		//ignore RowFilter for now
	}

}	// class MasterDataSubFormController
