//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.valuelistprovider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.DefaultCollectableEntityProvider;
import org.nuclos.common2.exception.CommonBusinessException;

public class ReferencedEntityFieldCollectableFieldsProvider implements CollectableFieldsProvider {
	
	private static final Logger LOG = Logger.getLogger(ReferencedEntityFieldCollectableFieldsProvider.class);

	//Do not change this string-identifier!
	public static final String REFERENCING_ENTITIY_UID = "entityId";

	public static final String REFERENCING_FIELD_UID = "field";
	
	//

	private UID referencingEntityUid = null;
	
	private UID referencingFieldUid = null;
	
	/**
	 * @deprecated
	 */
	ReferencedEntityFieldCollectableFieldsProvider() {
	}
	
	public ReferencedEntityFieldCollectableFieldsProvider(UID referencingEntityUid, UID referencingFieldUid) {
		setParameter(REFERENCING_ENTITIY_UID, referencingEntityUid);
		setParameter(REFERENCING_FIELD_UID, referencingFieldUid);
	}

	/**
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String name, Object value) {
		if (name.equals(REFERENCING_ENTITIY_UID)) {
			referencingEntityUid = (UID) value;
		} else if (name.equals(REFERENCING_FIELD_UID)) {
			referencingFieldUid = (UID) value;
		}
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		final List<CollectableField> result = new ArrayList<CollectableField>();

		if (referencingEntityUid != null && referencingFieldUid != null) {
			CollectableEntityField referencingEF = DefaultCollectableEntityProvider.getInstance()
					.getCollectableEntity(referencingEntityUid).getEntityField(referencingFieldUid);
			if (referencingEF.isReferencing()) {
				final UID referencedEntityUid = referencingEF.getReferencedEntityUID();
				final Map<UID, FieldMeta<?>> fieldMap = MetaProvider.getInstance().getAllEntityFieldsByEntity(referencedEntityUid);
				for (UID fieldUid : fieldMap.keySet()) {
					final FieldMeta<?> fieldMeta = fieldMap.get(fieldUid);
					if (!SF.isEOField(referencingEntityUid, fieldUid)
							|| fieldUid.equals(SF.PROCESS.getUID(fieldUid)) 
							|| fieldUid.equals(SF.STATE.getUID(fieldUid))) {
						result.add(new CollectableValueIdField(fieldUid, fieldMeta.getFieldName()));
					}
				}

				Collections.sort(result);
			}
		}

		Collections.sort(result);
		return result;
	}
	
}
