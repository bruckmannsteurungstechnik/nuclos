//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.valuelistprovider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.LocalizedCollectableValueField;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * <code>ValueListProvider</code> for "all actions". This is used in the role administration dialog where all actions
 * must be displayed, regardless of user rights.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:corina.mandoki@novabit.de">Corina Mandoki</a>
 * @version 01.00.00
 */
public class RoleActionsCollectableFieldsProvider implements CollectableFieldsProvider {

	private static final Logger LOG = Logger.getLogger(RoleActionsCollectableFieldsProvider.class);
	
	public RoleActionsCollectableFieldsProvider() {
	}

	/**
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String sName, Object oValue) {
		//no parameters
		LOG.info("Unknown parameter " + sName + " with value " + oValue);
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		List<CollectableField> result = new ArrayList<CollectableField>();
		Collection<MasterDataVO<UID>> mdvos = MasterDataDelegate.getInstance().getMasterData(E.ACTION.getUID());
		for (MasterDataVO<UID> mdvo : mdvos) {
			String action = mdvo.getFieldValue(E.ACTION.action.getUID(), String.class);
			String label = mdvo.getFieldValue(E.ACTION.name.getUID(), String.class);
			String resId = mdvo.getFieldValue(E.ACTION.labelres.getUID(), String.class);
			if (resId != null) {
				label = SpringLocaleDelegate.getInstance().getTextFallback(resId, label);
			}
			result.add(new LocalizedCollectableValueField(action, label));
		}
		Collections.sort(result);
		return result;
	}

}	// class RoleActionsCollectableFieldsProvider
