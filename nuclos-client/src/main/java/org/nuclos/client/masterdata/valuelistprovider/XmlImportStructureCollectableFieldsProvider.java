//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.valuelistprovider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.common.E;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.fileimport.ImportMode;
import org.nuclos.common2.KeyEnum;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

public class XmlImportStructureCollectableFieldsProvider implements CollectableFieldsProvider {

	private static final Logger LOG = Logger.getLogger(XmlImportStructureCollectableFieldsProvider.class);
	
	public static final String IMPORT_MODE = "mode";

	// 
	
	private ImportMode mode = null;
	
	public XmlImportStructureCollectableFieldsProvider(ImportMode importMode) {
		this.mode = importMode;
	}

	/**
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String name, Object value) {
		if (name.equals(IMPORT_MODE)) {
			if (value != null) {
				mode = KeyEnum.Utils.findEnum(ImportMode.class, (String) value);
			}
			else {
				mode = null;
			}
		}
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		final List<CollectableField> result = new ArrayList<CollectableField>();
		if (mode != null) {
			final List<MasterDataVO<?>> isList = MasterDataCache.getInstance().get(E.XML_IMPORT.getUID());
			for (MasterDataVO<?> is : isList) {
				final Object oFieldValue = is.getFieldValue(E.XML_IMPORT.mode.getUID());
				if (is.getFieldValue(E.XML_IMPORT.mode.getUID()) != null && mode.equals(KeyEnum.Utils.findEnum(ImportMode.class, (String) oFieldValue))) {
					result.add(new CollectableValueIdField(is.getId(), is.getFieldValue(E.XML_IMPORT.name.getUID())));
				}
			}
		}
		Collections.sort(result);
		return result;
	}
}
