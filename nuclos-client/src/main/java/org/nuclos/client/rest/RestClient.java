package org.nuclos.client.rest;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.log4j.Logger;
import org.nuclos.client.remote.http.SecuredBasicAuthHttpInvokerRequestExecutor;
import org.nuclos.client.security.NuclosRemoteServerSession;
import org.nuclos.client.startup.NuclosProperties;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.startup.NuclosEnviromentConstants;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class RestClient {
	private static final Logger LOG = Logger.getLogger(RestClient.class);

	private HttpClient client;

	public RestClient() {
		Runtime.getRuntime().addShutdownHook(new Thread(this::logout));
	}

	public URL getRestUrl() {
		return (URL) NuclosProperties.getInstance().get(NuclosEnviromentConstants.REST_VARIABLE);
	}

	private ObjectMapper getMapper() {
		return new ObjectMapper();
	}

	public HttpClient getClient() {
		if (client == null) {
			client = SpringApplicationContextHolder.getBean(SecuredBasicAuthHttpInvokerRequestExecutor.class).getHttpClient();
		}

		return client;
	}

	private String getSessionId() {
		return NuclosRemoteServerSession.getInstance().getJSessionId();
	}

	public HttpResponse execute(final HttpRequestBase request) throws IOException {
		request.setHeader("sessionId", getSessionId());
		return getClient().execute(request);
	}

	public <T> T execute(
			final HttpRequestBase request,
			final Class<T> resultClass
	) throws IOException {
		HttpResponse response = execute(request);
		HttpEntity entity = response.getEntity();
		InputStream stream = entity.getContent();
		return getMapper().readValue(stream, resultClass);
	}

	private void logout() {
		HttpDelete request = new HttpDelete(getRestUrl().toString());
		try {
			HttpResponse response = execute(request);
			LOG.debug("Logout: " + response.getStatusLine());
		} catch (IOException e) {
			LOG.warn("Logout failed", e);
		}
	}
}
