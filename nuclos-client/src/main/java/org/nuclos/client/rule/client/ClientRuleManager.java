package org.nuclos.client.rule.client;

import javax.swing.JPanel;

import org.apache.log4j.Logger;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.main.mainframe.MainFrameTabbedPane;
import org.nuclos.client.main.mainframe.workspace.TabRestoreController;
import org.nuclos.client.rule.client.panel.ClientRuleManagerView;
import org.nuclos.client.ui.Controller;

public class ClientRuleManager extends Controller<MainFrameTabbedPane> {

	private static final Logger LOG = Logger.getLogger(ClientRuleManager.class);	
	
	public ClientRuleManager(MainFrameTabbedPane parent) {
		super(parent);
	}

	public static class NuclosESMRunnable implements Runnable {
		
		private final MainFrameTabbedPane desktopPane;
		
		public NuclosESMRunnable(MainFrameTabbedPane desktopPane) {
			this.desktopPane = desktopPane;
		}
		
		@Override
		public void run() {
			try {
				ClientRuleManager w = new ClientRuleManager(desktopPane);
				w.show(desktopPane);
			}
			catch (Exception e) {
				// Ok
				LOG.error("showWizard failed: " + e, e);
			}
		}
	}

	public void show(MainFrameTabbedPane desktopPane) {
		MainFrameTab ifrm = Main.getInstance().getMainController().newMainFrameTab(null, 
				getSpringLocaleDelegate().getMessage("miClientRuleManager", "Nucleus Entit\u00e4tenwizard <Neue Entit\u00e4t>"));
	
		ifrm.add(createClientRuleManagerPanel());
		ifrm.setTabRestoreController(new ClientRuleManagerTabRestoreController());
		ifrm.setTabIconFromNuclosResource("org.nuclos.client.resource.icon.main-blue.code.png"); 
			
		desktopPane.add(ifrm);
		ifrm.setVisible(true);	
	}
	
	private JPanel createClientRuleManagerPanel() {
		return new ClientRuleManagerView();
	}
	
	public static class ClientRuleManagerTabRestoreController extends TabRestoreController {
		
		@Override
		public void restoreFromPreferences(String preferencesXML,
				MainFrameTab tab) throws Exception {
			ClientRuleManagerView clientRuleManagerView = new ClientRuleManagerView();
			tab.add(clientRuleManagerView);
			tab.setTabRestoreController(new ClientRuleManagerTabRestoreController());
		}

		@Override
		public boolean validate(String preferencesXML) {
			return true;
		}
	}
}
