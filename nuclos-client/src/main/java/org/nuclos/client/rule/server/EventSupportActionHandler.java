package org.nuclos.client.rule.server;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.tree.TreePath;

import org.apache.log4j.Logger;
import org.nuclos.client.customcode.ServerCodeCollectController;
import org.nuclos.client.explorer.EventSupportExplorerView;
import org.nuclos.client.explorer.node.AbstractEventSupportExplorerNode;
import org.nuclos.client.explorer.node.EventSupportExplorerNode;
import org.nuclos.client.explorer.node.EventSupportTargetExplorerNode;
import org.nuclos.client.explorer.node.eventsupport.EventSupportTargetTreeNode;
import org.nuclos.client.explorer.node.eventsupport.EventSupportTargetType;
import org.nuclos.client.explorer.node.eventsupport.EventSupportTreeNode;
import org.nuclos.client.main.Main;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.rule.server.model.EventSupportPropertiesTableModel;
import org.nuclos.client.rule.server.panel.EventSupportSourceView;
import org.nuclos.client.rule.server.panel.EventSupportTargetView;
import org.nuclos.client.rule.server.panel.EventSupportView;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.collect.CollectControllerFactorySingleton;
import org.nuclos.client.ui.tree.TreeNodeAction;
import org.nuclos.common.E;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.customcode.valueobject.CodeVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportCommunicationPortVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportEventVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportGenerationVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportJobVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportTransitionVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportTypeVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;

public class EventSupportActionHandler {

	private static final Logger LOG = Logger.getLogger(EventSupportActionHandler.class);	
	
	public static enum EventSupportActions {
		ACTION_SAVE_EVENT, 
		ACTION_SAVE_ALL_EVENTS, 
		ACTION_DELETE_EVENT, 
		ACTION_MOVE_UP_EVENT, 
		ACTION_MOVE_DOWN_EVENT,
		
		ACTION_SAVE_STATETRANSITION,
		ACTION_SAVE_ALL_STATETRANSITION,
		ACTION_DELETE_STATETRANSITION, 
		ACTION_MOVE_UP_STATETRANSITION, 
		ACTION_MOVE_DOWN_STATETRANSITION,
		ACTION_CHANGE_TRANSITION_STATETRANSITION,
		
		ACTION_SAVE_ALL_JOBS,
		ACTION_DELETE_JOB, 
		ACTION_MOVE_UP_JOB, 
		ACTION_MOVE_DOWN_JOB,
		
		ACTION_SAVE_ALL_GENERATIONS,
		ACTION_DELETE_GENERATION, 
		ACTION_MOVE_UP_GENERATION, 
		ACTION_MOVE_DOWN_GENERATION,
		
		ACTION_SAVE_ALL_COMMUNICATION_PORTS,
		ACTION_DELETE_COMMUNICATION_PORT, 
		ACTION_MOVE_UP_COMMUNICATION_PORT, 
		ACTION_MOVE_DOWN_COMMUNICATION_PORT,
		
		ACTION_REFRESH_TARGETTREE, 
		ACTION_REFRESH_SOURCETREE,
		
		ACTION_SHOW_ERRORCOMPILEDIALOG,
		
		ACTION_RUN_SOURCETREE_SEARCH,
		ACTION_RUN_TARGETTREE_SEARCH
	}
		
	public static final String OPEN_EVENTSUPPORT = "OPENEVENTSUPPORT";
	public static final String DELETE_EVENTSUPPORT = "DELETEEVENTSUPPORT";
	public static final String OPEN_RULE = "OPENRULE";
	
	private final Map<EventSupportActions, AbstractAction> actionMap;
	
	private final EventSupportView esView;
	
	public EventSupportActionHandler(EventSupportView esView) {
		this.esView = esView;
		
		final Map<EventSupportActions, AbstractAction> map = new HashMap<EventSupportActionHandler.EventSupportActions, AbstractAction>();
		loadActions(map, esView);
		actionMap = Collections.unmodifiableMap(map);
	}
	
	public EventSupportView getEventSupportView() {
		return esView;
	}

	public Map<EventSupportActions, AbstractAction> getActionMaps() {
		return actionMap;
	}
	
	private static void loadActions(Map<EventSupportActions, AbstractAction> map, final EventSupportView view) {
		// Actions used in Entity - PropertyPanel 
		loadActionEventsEntites(map, view);
		
		// Actions used in StateModel - PropertyPanel 
		loadActionEventsStateModel(map, view);
		
		// Actions used in Jobs - PropertyPanel 
		loadActionEventsJobs(map, view);
		
		// Actions used in Generation - PropertyPanel 
		loadActionEventsGeneration(map, view);
		
		// Actions used in all CommunicationPort - PropertyPanel
		loadActionEventsCommunicationPort(map, view);

		// Actions used in all PropertyPanels
		loadActionEventsGenerealUsage(map, view);
	}
	
	private static void loadActionEventsGeneration(Map<EventSupportActions, AbstractAction> map, final EventSupportView view) {
		map.put(EventSupportActions.ACTION_SAVE_ALL_GENERATIONS, new EventSupportModifyAllAction(view) {

			@Override
			public EventSupportVO updateEventSupport(EventSupportVO entryByRowIndex, boolean updateRepository)
					throws CommonBusinessException {
				EventSupportGenerationVO oldEntry = (EventSupportGenerationVO)entryByRowIndex;
				EventSupportGenerationVO result
						= EventSupportDelegate.getInstance().modifyEventSupportGeneration(oldEntry);
				if (updateRepository) {
					EventSupportRepository.getInstance().updateEventSupports();
				}

				return result;
			}

			@Override
			public void deleteEventSupport(final EventSupportVO entryByRowIndex, boolean updateRepository) throws CommonBusinessException {
				EventSupportGenerationVO oldEntry = (EventSupportGenerationVO)entryByRowIndex;
				EventSupportDelegate.getInstance().deleteEventSupportGeneration(oldEntry, updateRepository);
			}

			@Override
			public JTable getPropertyTable(EventSupportTargetView targetView) {
				return targetView.getGenerationPropertiesPanel().getPropertyTable();
			}
		});
		
		map.put(EventSupportActions.ACTION_DELETE_GENERATION,new EventSupportRemoveUsageAction(view) {
			@Override
			public JTable getPropertyTable(EventSupportTargetView targetView) {
				return targetView.getGenerationPropertiesPanel().getPropertyTable();
			}
			@Override
			public void saveActionModifications(
					EventSupportTargetView estvElement, EventSupportVO removed) {
				ActionEvent act = new ActionEvent(removed, ActionEvent.ACTION_FIRST, "remove");
				estvElement.getGenerationPropertiesPanel().getActionMapping().get(EventSupportActions.ACTION_SAVE_ALL_GENERATIONS).actionPerformed(act);
			}
		});
		map.put(EventSupportActions.ACTION_MOVE_DOWN_GENERATION, new EventSupportMoveDownAction(view) {
			@Override
			public JTable getPropertyTable(EventSupportTargetView targetView) {
				return targetView.getGenerationPropertiesPanel().getPropertyTable();
			}
			@Override
			public void saveActionModifications(
					EventSupportTargetView estvElement) {
				estvElement.getGenerationPropertiesPanel().getActionMapping().get(EventSupportActions.ACTION_SAVE_ALL_GENERATIONS).actionPerformed(null);	
			}
		});
		map.put(EventSupportActions.ACTION_MOVE_UP_GENERATION, new EventSupportMoveUpAction(view) {
			@Override
			public JTable getPropertyTable(EventSupportTargetView targetView) {
				return targetView.getGenerationPropertiesPanel().getPropertyTable();
			}
			@Override
			public void saveActionModifications(
					EventSupportTargetView estvElement) {
				estvElement.getGenerationPropertiesPanel().getActionMapping().get(EventSupportActions.ACTION_SAVE_ALL_GENERATIONS).actionPerformed(null);	
			}
		});
	}
	
	private static void loadActionEventsCommunicationPort(Map<EventSupportActions, AbstractAction> map, final EventSupportView view) {
		map.put(EventSupportActions.ACTION_SAVE_ALL_COMMUNICATION_PORTS, new EventSupportModifyAllAction(view) {

			@Override
			public EventSupportVO updateEventSupport(EventSupportVO entryByRowIndex, boolean updateRepository)
					throws CommonBusinessException {
				EventSupportCommunicationPortVO oldEntry = (EventSupportCommunicationPortVO)entryByRowIndex;
				EventSupportCommunicationPortVO result
						= EventSupportDelegate.getInstance().modifyEventSupportCommunicationPort(oldEntry);
				if (updateRepository) {
					EventSupportRepository.getInstance().updateEventSupports();
				}

				return result;
			}

			@Override
			public void deleteEventSupport(final EventSupportVO entryByRowIndex, boolean updateRepository) throws CommonBusinessException {
				EventSupportCommunicationPortVO oldEntry = (EventSupportCommunicationPortVO)entryByRowIndex;
				EventSupportDelegate.getInstance().deleteEventSupportCommunicationPort(oldEntry, updateRepository);
			}

			@Override
			public JTable getPropertyTable(EventSupportTargetView targetView) {
				return targetView.getCommunicationPropertiesPanel().getPropertyTable();
			}
		});
		
		map.put(EventSupportActions.ACTION_DELETE_COMMUNICATION_PORT,new EventSupportRemoveUsageAction(view) {
			@Override
			public JTable getPropertyTable(EventSupportTargetView targetView) {
				return targetView.getCommunicationPropertiesPanel().getPropertyTable();
			}
			@Override
			public void saveActionModifications(
					EventSupportTargetView estvElement, EventSupportVO removed) {
				ActionEvent act = new ActionEvent(removed, ActionEvent.ACTION_FIRST, "remove");
				estvElement.getCommunicationPropertiesPanel().getActionMapping().get(EventSupportActions.ACTION_SAVE_ALL_COMMUNICATION_PORTS).actionPerformed(act);
			}
		});
		map.put(EventSupportActions.ACTION_MOVE_DOWN_COMMUNICATION_PORT, new EventSupportMoveDownAction(view) {
			@Override
			public JTable getPropertyTable(EventSupportTargetView targetView) {
				return targetView.getCommunicationPropertiesPanel().getPropertyTable();
			}
			@Override
			public void saveActionModifications(
					EventSupportTargetView estvElement) {
				estvElement.getCommunicationPropertiesPanel().getActionMapping().get(EventSupportActions.ACTION_SAVE_ALL_COMMUNICATION_PORTS).actionPerformed(null);	
			}
		});
		map.put(EventSupportActions.ACTION_MOVE_UP_COMMUNICATION_PORT, new EventSupportMoveUpAction(view) {
			@Override
			public JTable getPropertyTable(EventSupportTargetView targetView) {
				return targetView.getCommunicationPropertiesPanel().getPropertyTable();
			}
			@Override
			public void saveActionModifications(
					EventSupportTargetView estvElement) {
				estvElement.getCommunicationPropertiesPanel().getActionMapping().get(EventSupportActions.ACTION_SAVE_ALL_COMMUNICATION_PORTS).actionPerformed(null);	
			}
		});
	}

	private static void loadActionEventsJobs(Map<EventSupportActions, AbstractAction> map, final EventSupportView view) {
		map.put(EventSupportActions.ACTION_DELETE_JOB, new EventSupportRemoveUsageAction(view) {
			@Override
			public JTable getPropertyTable(EventSupportTargetView targetView) {
				return targetView.getJobPropertiesPanel().getPropertyTable();
			}
			@Override
			public void saveActionModifications(
					EventSupportTargetView estvElement, EventSupportVO removed) {
				ActionEvent act = new ActionEvent(removed, ActionEvent.ACTION_FIRST, "remove");
				estvElement.getJobPropertiesPanel().getActionMapping().get(EventSupportActions.ACTION_SAVE_ALL_JOBS).actionPerformed(act);
			}
		});		
		map.put(EventSupportActions.ACTION_SAVE_ALL_JOBS, new EventSupportModifyAllAction(view) {

			@Override
			public EventSupportVO updateEventSupport(EventSupportVO entryByRowIndex, boolean updateRepository)
					throws CommonBusinessException {
				EventSupportJobVO oldEntry = (EventSupportJobVO)entryByRowIndex;
				EventSupportJobVO result
						= EventSupportDelegate.getInstance().modifyEventSupportJob(oldEntry);
				if (updateRepository) {
					EventSupportRepository.getInstance().updateEventSupports();
				}

				return result;
			}

			@Override
			public void deleteEventSupport(final EventSupportVO entryByRowIndex, boolean updateRepository) throws CommonBusinessException {
				EventSupportJobVO oldEntry = (EventSupportJobVO)entryByRowIndex;
				EventSupportDelegate.getInstance().deleteEventSupportJob(oldEntry, updateRepository);
			}

			@Override
			public JTable getPropertyTable(EventSupportTargetView targetView) {
				return targetView.getJobPropertiesPanel().getPropertyTable();
			}
		});
		
		map.put(EventSupportActions.ACTION_MOVE_DOWN_JOB, new EventSupportMoveDownAction(view) {
			@Override
			public JTable getPropertyTable(EventSupportTargetView targetView) {
				return targetView.getJobPropertiesPanel().getPropertyTable();
			}
			@Override
			public void saveActionModifications(
					EventSupportTargetView estvElement) {
				estvElement.getGenerationPropertiesPanel().getActionMapping().get(EventSupportActions.ACTION_SAVE_ALL_JOBS).actionPerformed(null);	
			}
		});
		map.put(EventSupportActions.ACTION_MOVE_UP_JOB, new EventSupportMoveUpAction(view) {
			@Override
			public JTable getPropertyTable(EventSupportTargetView targetView) {
				return targetView.getJobPropertiesPanel().getPropertyTable();
			}
			@Override
			public void saveActionModifications(
					EventSupportTargetView estvElement) {
				estvElement.getGenerationPropertiesPanel().getActionMapping().get(EventSupportActions.ACTION_SAVE_ALL_JOBS).actionPerformed(null);	
			}
		});
	}

	private static void loadActionEventsGenerealUsage(Map<EventSupportActions, AbstractAction> map, final EventSupportView view) {
		map.put(EventSupportActions.ACTION_REFRESH_TARGETTREE, 
				new AbstractAction("", Icons.getInstance().getIconRefresh16()) {
					@Override
					public void actionPerformed(ActionEvent e) {
						try {
							EventSupportTargetView estvElement = view.getTargetViewPanel();
							EventSupportRepository.getInstance().updateEventSupports();
							
							EventSupportExplorerView explView = (EventSupportExplorerView) view.getTargetViewPanel().getExplorerView();
							EventSupportTargetTreeNode node= (EventSupportTargetTreeNode) estvElement.getExplorerView().getRootNode().getTreeNode();
							
							// Unset all search values
							explView.getSearchTextField().setText("");
							node.setSearchString(null);
							// reload all previous nodes
							estvElement.getExplorerView().getRootNode().refresh(estvElement.getTree());
							explView.openPreferenceNodes();
						} catch (RuntimeException | CommonFinderException | CommonPermissionException err) {
							// Ok
							LOG.error(err.getMessage(), err);
						}
					}
				});
		map.put(EventSupportActions.ACTION_REFRESH_SOURCETREE, 
				new AbstractAction("", Icons.getInstance().getIconRefresh16()) {
					@Override
					public void actionPerformed(ActionEvent e) {
						try {
							EventSupportSourceView estvElement = view.getSourceViewPanel();
							
							EventSupportExplorerView explView = (EventSupportExplorerView) view.getSourceViewPanel().getExplorerView();
							EventSupportTreeNode node= (EventSupportTreeNode) estvElement.getExplorerView().getRootNode().getTreeNode();
							
							// Unset all search values
							explView.getSearchTextField().setText(null);
							node.setSearchString(null);
						
							EventSupportRepository.getInstance().updateEventSupports();
							
							// Reload tree and show all previoud opened nodes
							estvElement.getExplorerView().getRootNode().refresh(estvElement.getTree());
							explView.openPreferenceNodes();
						} catch (RuntimeException | CommonFinderException | CommonPermissionException err) {
							// Ok
							LOG.error(err.getMessage(), err);
						}
					}
				});

		if (EventSupportRepository.getInstance().getCompileExceptionMessages(true).size() > 0) {
				map.put(EventSupportActions.ACTION_SHOW_ERRORCOMPILEDIALOG, 
						new AbstractAction("", Icons.getInstance().getEventSupportCodeCompileErrorIcon()) {
							@Override
							public void actionPerformed(ActionEvent e) {										
								Main.getInstance().getMainController().showCompileExceptionDialog(true);
							}
						});

				map.get(EventSupportActions.ACTION_SHOW_ERRORCOMPILEDIALOG).setEnabled(true);
			}
			else {
				map.put(EventSupportActions.ACTION_SHOW_ERRORCOMPILEDIALOG,
						new AbstractAction("", Icons.getInstance().getEventSupportInactiveRuleIcon()) {
							@Override
							public void actionPerformed(ActionEvent e) {
								Main.getInstance().getMainController().showCompileExceptionDialog(true);
							}
						});

				map.get(EventSupportActions.ACTION_SHOW_ERRORCOMPILEDIALOG).setEnabled(false);
			}
		
		map.put(EventSupportActions.ACTION_RUN_TARGETTREE_SEARCH, 
				new AbstractAction("", null) {
					@Override
					public void actionPerformed(ActionEvent e) {
						try {
							EventSupportTargetView estvElement = view.getTargetViewPanel();
							JTextField searchField = (JTextField) e.getSource();
							String sSearchText = searchField.getText().trim();
							EventSupportTargetExplorerNode rootNode = (EventSupportTargetExplorerNode) estvElement.getExplorerView().getRootNode();
							
							EventSupportTargetTreeNode treeNode = (EventSupportTargetTreeNode) rootNode.getTreeNode();
							
							// Empty Search string - show complete tree
							if (sSearchText.length() == 0) {
								treeNode.setSearchString(null);
								rootNode.removeAllChildren();
								rootNode.refresh(estvElement.getTree());	
								estvElement.getExplorerView().getRootNode().refresh(estvElement.getTree());
							}
							else {
								if (sSearchText.length() >= 3)  {
									treeNode.setSearchString(sSearchText);
									rootNode.removeAllChildren();
									rootNode.refresh(estvElement.getTree());
									
									int row = 0;
									 while (row < estvElement.getTree().getRowCount()) {
										 estvElement.getTree().expandRow(row);
									      row++;
									      }
								}
							}
						} catch (RuntimeException | CommonFinderException | CommonPermissionException err) {
							// Ok
							LOG.error(err.getMessage(), err);
						}
					}
				});
		
		map.put(EventSupportActions.ACTION_RUN_SOURCETREE_SEARCH, 
				new AbstractAction("", null) {
					@Override
					public void actionPerformed(ActionEvent e) {
						try {
							EventSupportSourceView estvElement = view.getSourceViewPanel();
							JTextField searchField = (JTextField) e.getSource();
							String sSearchText = searchField.getText().trim();
							EventSupportExplorerNode rootNode = (EventSupportExplorerNode) estvElement.getExplorerView().getRootNode();
							EventSupportTreeNode treeNode = rootNode.getTreeNode();
							
							// Check wether results can be found for this searchstring
							// if not, no need to refreh the tree
								
							if (sSearchText.length() == 0) {
								treeNode.setSearchString(null);
								rootNode.removeAllChildren();
								rootNode.refresh(estvElement.getTree());	
								rootNode.expandAllChildren(estvElement.getTree());
								estvElement.getExplorerView().getRootNode().refresh(estvElement.getTree());
							}
							else if (EventSupportRepository.getInstance().searchForEventSupports(sSearchText).size() > 0 ) {
								treeNode.setSearchString(sSearchText);
								rootNode.removeAllChildren();
								rootNode.refresh(estvElement.getTree());
								
								int row = 0;
								while (row < estvElement.getTree().getRowCount()) {
									estvElement.getTree().expandRow(row);
									row++;
								}
							}
						} catch (RuntimeException err) {
							// Ok
							LOG.error(err.getMessage(), err);
						} catch (CommonFinderException err) {
							LOG.error(err.getMessage(), err);
						} catch (NuclosBusinessException err) {
							LOG.error(err.getMessage(), err);
						} catch (CommonPermissionException err) {
							LOG.error(err.getMessage(), err);
						}
					}
				});
	}

	private static void loadActionEventsStateModel(Map<EventSupportActions, AbstractAction> map, final EventSupportView view) {
		map.put(EventSupportActions.ACTION_SAVE_STATETRANSITION, new EventSupportModifyAction(view) {
			
			@Override
			public EventSupportVO modifyEventSupport(EventSupportVO entryByRowIndex) 
					throws NuclosBusinessRuleException, CommonPermissionException, CommonValidationException, 
					CommonCreateException, CommonFinderException, CommonRemoveException, 
					CommonStaleVersionException, NuclosCompileException {
				
				EventSupportTransitionVO eseT = (EventSupportTransitionVO) entryByRowIndex;
				return EventSupportDelegate.getInstance().modifyEventSupportTransition(eseT, eseT.getTransition());
			}
			
			@Override
			public JTable getPropertyTable(EventSupportTargetView targetView) {
				return targetView.getStatePropertiesPanel().getPropertyTable();
			}
		});
			
		map.put(EventSupportActions.ACTION_SAVE_ALL_STATETRANSITION, new EventSupportModifyAllAction(view) {

			@Override
			public EventSupportVO updateEventSupport(EventSupportVO entryByRowIndex, boolean updateRepository)
					throws CommonBusinessException {
				EventSupportTransitionVO oldEntry = (EventSupportTransitionVO)entryByRowIndex;
				EventSupportTransitionVO result
						= EventSupportDelegate.getInstance().modifyEventSupportTransition(oldEntry, oldEntry.getTransition());
				if (updateRepository) {
					EventSupportRepository.getInstance().updateEventSupports();
				}

				return result;
			}

			@Override
			public void deleteEventSupport(final EventSupportVO entryByRowIndex, boolean updateRepository) throws CommonBusinessException {
				EventSupportTransitionVO oldEntry = (EventSupportTransitionVO)entryByRowIndex;
				EventSupportDelegate.getInstance().deleteEventSupportTransition(oldEntry);
			}

			@Override
			public JTable getPropertyTable(EventSupportTargetView targetView) {
				return targetView.getStatePropertiesPanel().getPropertyTable();
			}
		});
	
		map.put(EventSupportActions.ACTION_DELETE_STATETRANSITION, new EventSupportRemoveUsageAction(view) {
			
			@Override
			public JTable getPropertyTable(EventSupportTargetView targetView) {
				return targetView.getStatePropertiesPanel().getPropertyTable();
			}
			
			@Override
			public void saveActionModifications(
					EventSupportTargetView estvElement, EventSupportVO removed) {
				ActionEvent act = new ActionEvent(removed, ActionEvent.ACTION_FIRST, "remove");
				estvElement.getGenerationPropertiesPanel().getActionMapping().get(EventSupportActions.ACTION_SAVE_ALL_STATETRANSITION).actionPerformed(act);
			}
		});			
		
		map.put(EventSupportActions.ACTION_MOVE_DOWN_STATETRANSITION, new EventSupportMoveDownAction(view) {
			
			@Override
			public JTable getPropertyTable(EventSupportTargetView targetView) {
				return targetView.getStatePropertiesPanel().getPropertyTable();
			}
			
			@Override
			public void saveActionModifications(
					EventSupportTargetView estvElement) {
				estvElement.getGenerationPropertiesPanel().getActionMapping().get(EventSupportActions.ACTION_SAVE_ALL_STATETRANSITION).actionPerformed(null);	
			}
		});
		
		map.put(EventSupportActions.ACTION_MOVE_UP_STATETRANSITION, new EventSupportMoveUpAction(view) {
			
			@Override
			public JTable getPropertyTable(EventSupportTargetView targetView) {
				return targetView.getStatePropertiesPanel().getPropertyTable();
			}
			
			@Override
			public void saveActionModifications(
					EventSupportTargetView estvElement) {
				estvElement.getGenerationPropertiesPanel().getActionMapping().get(EventSupportActions.ACTION_SAVE_ALL_STATETRANSITION).actionPerformed(null);	
			}
		});
	}

	private static void loadActionEventsEntites(Map<EventSupportActions, AbstractAction> map, final EventSupportView view) {
		map.put(EventSupportActions.ACTION_SAVE_EVENT, new EventSupportModifyAction(view) {
			
					@Override
					public EventSupportVO modifyEventSupport(EventSupportVO entryByRowIndex) 
							throws NuclosBusinessRuleException, CommonPermissionException, CommonValidationException, 
							CommonFinderException, CommonCreateException, CommonRemoveException, 
							CommonStaleVersionException, NuclosCompileException {
						
						EventSupportEventVO eseVO =(EventSupportEventVO) entryByRowIndex;
						return EventSupportDelegate.getInstance().modifyEventSupportEvent(eseVO, eseVO.getStateUID(), eseVO.getProcessUID());
					}
					
					@Override
					public JTable getPropertyTable(EventSupportTargetView targetView) {
						return targetView.getEntityPropertiesPanel().getPropertyTable();
					}
		});
			
		map.put(EventSupportActions.ACTION_SAVE_ALL_EVENTS, new EventSupportModifyAllAction(view) {
			
					@Override
					public EventSupportVO updateEventSupport(EventSupportVO entryByRowIndex, boolean updateRepository)
							throws CommonBusinessException {
						final EventSupportEventVO oldEntry = (EventSupportEventVO)entryByRowIndex;
						EventSupportEventVO result
								= EventSupportDelegate.getInstance().modifyEventSupportEvent(oldEntry, oldEntry.getStateUID(), oldEntry.getProcessUID());
						if (updateRepository) {
							EventSupportRepository.getInstance().updateEventSupports();
						}

						return result;
					}

					@Override
					public void deleteEventSupport(final EventSupportVO entryByRowIndex, boolean updateRepository) throws CommonBusinessException {
						EventSupportEventVO oldEntry = (EventSupportEventVO)entryByRowIndex;
						EventSupportDelegate.getInstance().deleteEventSupportEvent(oldEntry, updateRepository);
					}

					@Override
					public JTable getPropertyTable(EventSupportTargetView targetView) {
						return targetView.getEntityPropertiesPanel().getPropertyTable();
					}
		});
			
		map.put(EventSupportActions.ACTION_DELETE_EVENT, new EventSupportRemoveUsageAction(view) {
			
			@Override
			public JTable getPropertyTable(EventSupportTargetView targetView) {
				return targetView.getEntityPropertiesPanel().getPropertyTable();
			}
			
			@Override
			public void saveActionModifications(
					EventSupportTargetView estvElement, EventSupportVO removed) {
				ActionEvent act = new ActionEvent(removed, ActionEvent.ACTION_FIRST, "remove");
				estvElement.getEntityPropertiesPanel().getActionMapping().get(EventSupportActions.ACTION_SAVE_ALL_EVENTS).actionPerformed(act);
			}
		});
		
		map.put(EventSupportActions.ACTION_MOVE_UP_EVENT, new EventSupportMoveUpAction(view) {
			
			@Override
			public JTable getPropertyTable(EventSupportTargetView targetView) {
				return targetView.getEntityPropertiesPanel().getPropertyTable();
			}
			
			@Override
			public void saveActionModifications(
					EventSupportTargetView estvElement) {
				estvElement.getEntityPropertiesPanel().getActionMapping().get(EventSupportActions.ACTION_SAVE_ALL_EVENTS).actionPerformed(null);	
			}
		});
		
		map.put(EventSupportActions.ACTION_MOVE_DOWN_EVENT, new EventSupportMoveDownAction(view) {
			
			@Override
			public JTable getPropertyTable(EventSupportTargetView targetView) {
				return targetView.getEntityPropertiesPanel().getPropertyTable();
			}
			
			@Override
			public void saveActionModifications(
					EventSupportTargetView estvElement) {
				estvElement.getEntityPropertiesPanel().getActionMapping().get(EventSupportActions.ACTION_SAVE_ALL_EVENTS).actionPerformed(null);	
			}
		});
	}

	public void fireAction(EventSupportActions act) {
		actionMap.get(act).actionPerformed(null);
	}
	
	public static abstract class EventSupportModifyAction extends AbstractAction {

		public abstract EventSupportVO modifyEventSupport(EventSupportVO entryByRowIndex) 
				throws NuclosBusinessRuleException, CommonPermissionException, CommonValidationException, 
				CommonCreateException, CommonFinderException, CommonRemoveException, 
				CommonStaleVersionException, NuclosCompileException;
		
		private final EventSupportView view;
		
		public EventSupportModifyAction(EventSupportView view) 
		{
			super("" , Icons.getInstance().getIconSave16());
			this.view = view;
		}
		
		public abstract JTable getPropertyTable(EventSupportTargetView targetView);
		
		@Override
		public void actionPerformed(ActionEvent actEv) {
			
			EventSupportTargetView estvElement = view.getTargetViewPanel();
			
			if (estvElement != null)
			{
				EventSupportPropertiesTableModel model = 
						(EventSupportPropertiesTableModel) getPropertyTable(estvElement).getModel();
				
				int selectedRow = getPropertyTable(estvElement).getSelectedRow();
				
				EventSupportVO entryByRowIndex = model.getEntryByRowIndex(selectedRow);
					
				// Modify entry
				if (entryByRowIndex.getId() != null) {
					try {
						entryByRowIndex.setVersion(modifyEventSupport(entryByRowIndex).getVersion());
					} catch (RuntimeException e) {
						// Ok
						LOG.error(e.getMessage(), e);
					} catch (NuclosBusinessRuleException e) {
						LOG.error(e.getMessage(), e);
					} catch (CommonPermissionException e) {
						LOG.error(e.getMessage(), e);
					} catch (CommonValidationException e) {
						LOG.error(e.getMessage(), e);
					} catch (CommonCreateException e) {
						LOG.error(e.getMessage(), e);
					} catch (CommonFinderException e) {
						LOG.error(e.getMessage(), e);
					} catch (CommonRemoveException e) {
						LOG.error(e.getMessage(), e);
					} catch (CommonStaleVersionException e) {
						LOG.error(e.getMessage(), e);
					} catch (NuclosCompileException e) {
						LOG.error(e.getMessage(), e);
					}
				}
			
			}
		}		
	}
	
	public static abstract class EventSupportModifyAllAction extends AbstractAction {
		
		private final EventSupportView view;

		public EventSupportModifyAllAction(EventSupportView view) 
		{
			super("" , Icons.getInstance().getIconSave16());
			this.view = view;
		}
		
		public abstract EventSupportVO updateEventSupport(EventSupportVO entryByRowIndex, boolean updateRepository)
				throws CommonBusinessException;

		public abstract void deleteEventSupport(EventSupportVO entryByRowIndex, boolean updateRepository)
				throws CommonBusinessException;

		public abstract JTable getPropertyTable(EventSupportTargetView targetView);
		
		@Override
		public void actionPerformed(ActionEvent actEv) {
			try {
				EventSupportTargetView estvElement = view.getTargetViewPanel();
				if (estvElement != null)
				{
					if (actEv != null && actEv.getSource() instanceof EventSupportVO) {
						EventSupportVO removed = (EventSupportVO)actEv.getSource();
						deleteEventSupport(removed, true);
					}

					EventSupportPropertiesTableModel model = 
							(EventSupportPropertiesTableModel) getPropertyTable(estvElement).getModel();
					
					for (int selectedRow = 0; selectedRow < model.getRowCount(); selectedRow++) {
						EventSupportVO entryByRowIndex = model.getEntryByRowIndex(selectedRow);
						// Modify entry
						try {
							EventSupportVO updateEventSupport = updateEventSupport(entryByRowIndex, false);
							model.removeEntry(selectedRow);
							model.addEntry(selectedRow, updateEventSupport);
							
						} catch (RuntimeException | CommonBusinessException e) {
							// Ok
							LOG.error(e.getMessage(), e);
						}
					}
					
				}
			} catch (RuntimeException | CommonBusinessException e) {
				// Ok
				LOG.error(e.getMessage(), e);
			} finally {
				EventSupportRepository.getInstance().updateEventSupports();
			}
		}		
	}
	
	public static abstract class EventSupportRemoveUsageAction extends AbstractAction {

		private final EventSupportView view;
		
		public EventSupportRemoveUsageAction(EventSupportView view) 
		{
			super("" , Icons.getInstance().getIconDelete16());
			this.view = view;
		}
		
		public abstract JTable getPropertyTable(EventSupportTargetView targetView);
		
		public abstract void saveActionModifications(EventSupportTargetView targetView, EventSupportVO removed);
		
		@Override
		public void actionPerformed(ActionEvent actEv) {
			final EventSupportTargetView estvElement = view.getTargetViewPanel();
			if (estvElement != null)
			{
				final EventSupportPropertiesTableModel model = 
						(EventSupportPropertiesTableModel) getPropertyTable(estvElement).getModel();
				final int selectedRow = getPropertyTable(estvElement).getSelectedRow();
				if (selectedRow == -1) {
					return;
				}
				final EventSupportVO entryByRowIndex = model.getEntryByRowIndex(selectedRow);
				TreePath pathToSelectAfterwards = estvElement.getTree().getSelectionModel().getSelectionPath();
				if (model.getRowCount() == 1) {
					pathToSelectAfterwards = estvElement.getTree().getSelectionModel().getSelectionPath().getParentPath();					
				}
				
				int order = 1;
				if (entryByRowIndex != null) {
					order = entryByRowIndex.getOrder();
				}
				// remove it from tablemodel
				model.removeEntry(selectedRow);
				
				// and refresh new order and store it in db
				for (int idx = selectedRow; idx < model.getRowCount(); idx++) {
					final EventSupportVO nextEntry = model.getEntryByRowIndex(idx);
					nextEntry.setOrder(order++);
				}
				saveActionModifications(estvElement, entryByRowIndex);
				
				// Call refresh of target tree
				//view.getActionsMap().get(EventSupportActions.ACTION_REFRESH_TARGETTREE).actionPerformed(null);
				estvElement.getTree().setSelectionPath(pathToSelectAfterwards);
				estvElement.getTree().scrollPathToVisible(pathToSelectAfterwards);
				
				final EventSupportTargetExplorerNode selectNode = (EventSupportTargetExplorerNode) 
						estvElement.getTree().getSelectionModel().getSelectionPath().getLastPathComponent();
				try {
					selectNode.refresh(estvElement.getTree(), true);
				} catch (Exception e) {
					LOG.warn("refresh failed: " + e, e);
				}
				estvElement.getTree().setSelectionPath(pathToSelectAfterwards);
				estvElement.getTree().scrollPathToVisible(pathToSelectAfterwards);
			}
		}		
	}
	
	public static abstract class EventSupportMoveUpAction extends AbstractAction {
		
		private final EventSupportView view;
		
		public EventSupportMoveUpAction(EventSupportView view) 
		{
			super("" , Icons.getInstance().getIconUp16());
			this.view = view;
		}
		
		public abstract JTable getPropertyTable(EventSupportTargetView targetView);
		
		public abstract void saveActionModifications(EventSupportTargetView targetView);
		
		@Override
		public void actionPerformed(ActionEvent actEv) {
			EventSupportTargetView estvElement = view.getTargetViewPanel();
			if (estvElement != null)
			{
				int selectedRow = getPropertyTable(estvElement).getSelectedRow();
				if (selectedRow <= 0) {
					return;
				}
				((EventSupportPropertiesTableModel) getPropertyTable(estvElement).getModel()).moveUp(selectedRow);
				
				saveActionModifications(estvElement);
				
				getPropertyTable(estvElement).setRowSelectionInterval(selectedRow-1, selectedRow-1);
				
			}
		}
	}
	
	public static abstract class EventSupportMoveDownAction extends AbstractAction {

		private final EventSupportView view;
		
		public EventSupportMoveDownAction(EventSupportView view) 
		{
			super("" , Icons.getInstance().getIconDown16());
			this.view = view;
		}
		
		public abstract JTable getPropertyTable(EventSupportTargetView targetView);
		
		public abstract void saveActionModifications(EventSupportTargetView targetView);
		
		@Override
		public void actionPerformed(ActionEvent actEv) {
			EventSupportTargetView estvElement = view.getTargetViewPanel();
			if (estvElement != null)
			{
				EventSupportPropertiesTableModel model = 
						(EventSupportPropertiesTableModel) getPropertyTable(estvElement).getModel();
				
				int selectedRow = getPropertyTable(estvElement).getSelectedRow();
				if (selectedRow == -1 || selectedRow >= model.getRowCount() - 1) {
					return;
				}
				model.moveDown(selectedRow);
				
				saveActionModifications(estvElement);
				getPropertyTable(estvElement).setRowSelectionInterval(selectedRow+1, selectedRow+1);
				
			}
			
		}
	}
	
	public static class OpenEventSupportAction extends TreeNodeAction {

		private CodeVO code;
		
		public OpenEventSupportAction(JTree tree, CodeVO pCode) {
			super(OPEN_EVENTSUPPORT, 
				  SpringLocaleDelegate.getInstance().getMessage("RelationExplorerNode.4", "RelationExplorerNode.4"), tree);
			this.code = pCode;
		}

		@Override
		public void actionPerformed(ActionEvent event) {
			// Get selected EventSupport and open RuleEditor
			final JTree tree = this.getJTree();
			final AbstractEventSupportExplorerNode currentNode = (AbstractEventSupportExplorerNode) tree.getSelectionPath().getLastPathComponent();
			final EventSupportTreeNode treeNode = currentNode.getTreeNode();
			
			try {
				EventSupportSourceVO eventSupportByClassname = EventSupportRepository.getInstance().getEventSupportByClassname(treeNode.getNodeName());
				
				if (eventSupportByClassname != null && EventSupportTargetType.EVENTSUPPORT.equals(treeNode.getTreeNodeType()) && code != null) {
					
					final CollectControllerFactorySingleton factory = CollectControllerFactorySingleton.getInstance();
					final ServerCodeCollectController rcc = factory.newServerCodeCollectController(E.SERVERCODE.getUID(), null);
					rcc.runViewSingleCollectableWithId(code.getId());					
				}
			} catch (RuntimeException e) {
				// Ok
				LOG.error(e.getMessage(), e);
			} catch (CommonBusinessException e) {
				LOG.error(e.getMessage(), e);
			}
		}		
	}
	
	public static class DeleteEventSupportAction extends TreeNodeAction {

		private CodeVO code;
		
		public DeleteEventSupportAction(JTree treeSource, CodeVO pCode) {
			super(DELETE_EVENTSUPPORT, 
				  SpringLocaleDelegate.getInstance().getMessage("RelationExplorerNode.5", "RelationExplorerNode.5"), treeSource);
			this.code = pCode;
		}

		@Override
		public void actionPerformed(ActionEvent event) {
			// Get selected EventSupport and open RuleEditor
			final JTree tree = this.getJTree();
			final AbstractEventSupportExplorerNode currentNode = (AbstractEventSupportExplorerNode) tree.getSelectionPath().getLastPathComponent();
			final EventSupportTreeNode treeNode = currentNode.getTreeNode();
			
			try {
				EventSupportSourceVO eventSupportByClassname = 
						EventSupportRepository.getInstance().getEventSupportByClassname(treeNode.getNodeName());
				
				if (eventSupportByClassname != null && EventSupportTargetType.EVENTSUPPORT.equals(treeNode.getTreeNodeType()) && code != null) {
					List<EventSupportVO> eventSupportTargets = EventSupportRepository.getInstance().getEventSupportTargets(eventSupportByClassname);
					
					TreePath pathToSelectAfterwards = tree.getSelectionModel().getSelectionPath().getParentPath();
					
					// Check if this rule is connected to a Nuclos Entity
					if (eventSupportTargets != null && eventSupportTargets.size() > 0) {
								
						int opt = JOptionPane.showConfirmDialog(null, createDeleteRulePanel("nuclos.entityfield.eventsupportevent.delRuleWithAssigns.description"),
								SpringLocaleDelegate.getInstance().getMessage("RelationExplorerNode.5", null),
							JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
						if (opt == JOptionPane.OK_OPTION) {
							
							// Remove usage
							EventSupportDelegate.getInstance().deleteEventSupportUsage(eventSupportTargets, false);
							// Remove rule (delete db-entry, remove file and rebuild jar-file)
							EventSupportDelegate.getInstance().deleteEventSupport(eventSupportByClassname, false);
			
							tree.setSelectionPath(pathToSelectAfterwards);
							tree.scrollPathToVisible(pathToSelectAfterwards);
							
							EventSupportExplorerNode selectNode = (EventSupportExplorerNode) tree.getSelectionModel().getSelectionPath().getPathComponent(0);
							tree.setSelectionRow(0);
							try {
								selectNode.refresh(tree, true);
							} catch (Exception e) {
								LOG.info("error refreshing selected node. " + e.getMessage());
							}
							
							tree.setSelectionPath(pathToSelectAfterwards);
							tree.scrollPathToVisible(pathToSelectAfterwards);	
							EventSupportExplorerView explView = (EventSupportExplorerView) treeNode.getController().getEventSupportView().getSourceViewPanel().getExplorerView();
							explView.openPreferenceNodes();	

							// Reload targetTree
							JTree targetTree = treeNode.getController().getEventSupportView().getTargetViewPanel().getTree();
						
							targetTree.setSelectionRow(0);
							EventSupportTargetExplorerNode selectTargetNode = (EventSupportTargetExplorerNode) targetTree.getSelectionModel().getSelectionPath().getLastPathComponent();
							try {
								selectTargetNode.refresh(targetTree, true);
								
							} catch (Exception e) {}
							selectTargetNode.getTreeNode().getController().showTargetSupportProperties(selectTargetNode.getTreeNode());
							EventSupportExplorerView explTargetView = (EventSupportExplorerView) treeNode.getController().getEventSupportView().getTargetViewPanel().getExplorerView();
							explTargetView.openPreferenceNodes();				
						}
					}
					else {

						int opt = JOptionPane.showConfirmDialog(null, createDeleteRulePanel("nuclos.entityfield.eventsupportevent.delRule.description"),
								SpringLocaleDelegate.getInstance().getMessage("RelationExplorerNode.5", null),
								JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
						if (opt == JOptionPane.OK_OPTION) {
							// Remove rule (delete db-entry, remove file and rebuild jar-file)
							EventSupportDelegate.getInstance().deleteEventSupport(eventSupportByClassname, false);			

							EventSupportExplorerNode selectNode = (EventSupportExplorerNode) tree.getSelectionModel().getSelectionPath().getPathComponent(tree.getSelectionModel().getSelectionPath().getPathCount() - 3);
						
							try {
								selectNode.refresh(tree, true);
								
							} catch (Exception e) {}
							
							tree.setSelectionPath(pathToSelectAfterwards);
							tree.scrollPathToVisible(pathToSelectAfterwards);	
							EventSupportExplorerView explView = (EventSupportExplorerView) treeNode.getController().getEventSupportView().getSourceViewPanel().getExplorerView();
							explView.openPreferenceNodes();		
						}
					}
				}
			} catch (RuntimeException e) {
				// Ok
				LOG.error(e.getMessage(), e);
			} catch (CommonFinderException e) {
				LOG.error(e.getMessage(), e);
			} catch (CommonPermissionException e) {
				LOG.error(e.getMessage(), e);
			} catch (CommonValidationException e) {
				LOG.error(e.getMessage(), e);
			} catch (NuclosCompileException e) {
				LOG.error(e.getMessage(), e);
			} catch (NuclosBusinessException e) {
				LOG.error(e.getMessage(), e);
			} catch (CommonRemoveException e) {
				LOG.error(e.getMessage(), e);
			} catch (CommonStaleVersionException e) {
				LOG.error(e.getMessage(), e);
			} finally {
				EventSupportRepository.getInstance().updateEventSupports();
			}
		}

		private JPanel createDeleteRulePanel(String msgText) {
		
			final JPanel pnlHeadline = new JPanel();
			pnlHeadline.setLayout(new BorderLayout());
			pnlHeadline.setBorder(BorderFactory.createEmptyBorder(5, 5, 0, 5));
			pnlHeadline.add(new JLabel(SpringLocaleDelegate.getInstance().getMessage(msgText, "")), BorderLayout.NORTH);
			
			return pnlHeadline;
		}		
	}
	
	public static class EventSupportEditor extends TreeNodeAction {
		
		private static final String ACTIONCOMMAND_SHOW_DETAILS = "SHOW DETAILS";
		
		public EventSupportEditor(JTree tree) {
			super(ACTIONCOMMAND_SHOW_DETAILS, 
				  SpringLocaleDelegate.getInstance().getMessage("nuclos.entityfield.eventsupportevent.newInstance.description", "EventSupport anlegen"), tree);
		}

		private void runCodeCollectController (EventSupportTypeVO eventSupportTypeByName, String sDescription, 
				String sClassname, UID nucletId) throws CommonBusinessException {
			
			final CollectControllerFactorySingleton factory = CollectControllerFactorySingleton.getInstance();
			
			String pPackage = "org.nuclet.businessentity";
			if (nucletId != null && !UID.UID_NULL.equals(nucletId)) {
				Collection<MasterDataVO<Object>> masterData = MasterDataDelegate.getInstance().getMasterData(E.NUCLET.getUID());
				for (MasterDataVO<Object> mdVO : masterData) {
					String sPackage = mdVO.getFieldValue(E.NUCLET.packagefield);
					if (mdVO.getId().equals(nucletId) && sPackage != null) {
						pPackage = sPackage;
						break;
					}
				}	
			}
						
			ServerCodeCollectController rcc = factory.newServerCodeCollectController(E.SERVERCODE.getUID(), null);
			CollectableMasterDataWithDependants clltable = rcc.newCollectable();
			
			clltable.setField(E.SERVERCODE.description.getUID(), new CollectableValueField(sDescription));
			clltable.setField(E.SERVERCODE.active.getUID(), new CollectableValueField(Boolean.TRUE));
			clltable.setField(E.SERVERCODE.nuclet.getUID(), new CollectableValueIdField(nucletId, nucletId));
			clltable.setField(E.SERVERCODE.source.getUID(), new CollectableValueField(
					createEventsupportByType(eventSupportTypeByName, sDescription, sClassname, pPackage)));
			
			rcc.runNewWith(clltable);
		}
		
		private String createEventsupportByType(EventSupportTypeVO eventSupportTypeByName, String sDescription, String sClassname, String pPackage) {
			
			StringBuilder sBuilder = new StringBuilder();
			String sImplements = eventSupportTypeByName.getClassname().substring(eventSupportTypeByName.getClassname().lastIndexOf(".") + 1); 
			final boolean bCommunicationRule = sImplements.equals("CommunicationRule"); 
				
			if (pPackage != null && !pPackage.equals("<Default>")) {
				sBuilder.append("package " + pPackage + "; \n\n");
			}
			
			// Imports
			for (String sImport : eventSupportTypeByName.getImports()) {
				sBuilder.append("import " + sImport + "; \n");
			}
			if (bCommunicationRule) {
				sBuilder.append("import org.nuclos.api.context.communication.PhoneCallNotificationContext; \n");
			}
			
			sBuilder.append("import org.nuclos.api.annotation.Rule; \n");
			sBuilder.append("import org.nuclos.api.exception.BusinessException; \n");
						
			sBuilder.append("\n/** @name        \n");
			sBuilder.append("  * @description \n");
			sBuilder.append("  * @usage       \n");
			sBuilder.append("  * @change      \n");
			sBuilder.append("*/\n");
			sBuilder.append("@Rule(name=\"" + sClassname+ "\", description=\"" + sDescription +"\")\n");
			sBuilder.append("public class " + validateJavaIdentifier(sClassname));
			sBuilder.append(" implements ");
			if (bCommunicationRule) {
				sImplements = "CommunicationRule<PhoneCallNotificationContext>";
			}
			sBuilder.append(sImplements);
			sBuilder.append(" {\n");
			
			// Methods
			if (bCommunicationRule) {
				sBuilder.append("\n\tpublic Class<PhoneCallNotificationContext> communicationContextClass() {");
				sBuilder.append("\n\t\treturn PhoneCallNotificationContext.class;");
				sBuilder.append("\n\t}");
				sBuilder.append("\n\tpublic void communicate(PhoneCallNotificationContext context)  throws BusinessException { ");
				sBuilder.append("\n\t}");
			} else {
				for (String m : eventSupportTypeByName.getMethods()) {
					sBuilder.append("\n\t" + m + " throws BusinessException { \n\t}");					
				}
			}
			
			sBuilder.append("\n}");
			
			
			return sBuilder.toString();
		}
		
		@Override
		public void actionPerformed(ActionEvent actEvent) {
			final JTree tree = this.getJTree();
			final EventSupportExplorerNode currentNode = (EventSupportExplorerNode) tree.getSelectionPath().getLastPathComponent();
			final EventSupportTreeNode treeNode = currentNode.getTreeNode();
			if (EventSupportTargetType.EVENTSUPPORT_TYPE.equals(treeNode.getTreeNodeType())) {
				
				final JTextField txtClassname = new JTextField();
				final JTextField txtDescription = new JTextField();
				// set focus on name text field
				txtClassname.addAncestorListener(new AncestorListener() {
					@Override
					public void ancestorAdded(AncestorEvent event) {
						event.getComponent().requestFocusInWindow();
					}

					@Override
					public void ancestorMoved(AncestorEvent event) {}

					@Override
					public void ancestorRemoved(AncestorEvent event) {}
					
				});
			
				try {
					EventSupportTypeVO eventSupportTypeByName = 
							EventSupportRepository.getInstance().getEventSupportTypeByName(treeNode.getNodeName());
					
					int answer = JOptionPane.showConfirmDialog(  
							null, createNewRulePanel(txtClassname, txtDescription), SpringLocaleDelegate.getInstance().getMessage("nuclos.eventsupport.newInstance.headline", "Neue Regel anlegen", eventSupportTypeByName.getName()), JOptionPane.OK_CANCEL_OPTION,  
							JOptionPane.PLAIN_MESSAGE);  
			    
					if (answer == JOptionPane.YES_OPTION)  
					{  
			    		runCodeCollectController(eventSupportTypeByName, txtDescription.getText(), 
			    				txtClassname.getText(), (UID) treeNode.getParentNode().getId());
			    		currentNode.refresh(tree);
					}	
				} catch (RuntimeException e) {
					LOG.error(e.getMessage(), e);
				} catch (CommonFinderException e) {
					LOG.error(e.getMessage(), e);
				} catch (CommonBusinessException e) {
					LOG.error(e.getMessage(), e);
				}	
			}
		}
		
		private static String validateJavaIdentifier(String value) {
			String retVal = value;
			
			if (retVal != null) {
				retVal = retVal.replace("ä", "ae");
				retVal = retVal.replace("ü", "ue");
				retVal = retVal.replace("ö", "oe");
				retVal = retVal.replace("Ä", "Ae");
				retVal = retVal.replace("Ü", "Ue");
				retVal = retVal.replace("Ö", "Oe");
				retVal = retVal.replace("ß", "ss");
				
				retVal = retVal.replaceAll("[^a-zA-Z0-9]+","");				
				
				if (Character.isDigit(retVal.charAt(0))) {
					retVal = retVal.substring(1);
				}
					
			}
				
			return retVal;
		}
		
		private JPanel createNewRulePanel(JTextField txtClassname, JTextField txtDescription) {
			
			JPanel retVal = new JPanel();
			retVal.setLayout(new BorderLayout());
			retVal.setBorder(BorderFactory.createEmptyBorder(5, 5, 0, 5));
			
			final JPanel pnlHeadline = new JPanel();
			pnlHeadline.setLayout(new BorderLayout());
			
			final JPanel pnlInputFields = new JPanel();
			pnlInputFields.setLayout(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.weightx = 0.0;
			c.gridx = 0;
			c.gridy = 0;
			c.gridwidth = 2;
			c.insets = new Insets(10,5,10,5);
			pnlHeadline.add(new JLabel(SpringLocaleDelegate.getInstance().getMessage("nuclos.eventsupport.newInstance.infotext.1", "")), BorderLayout.NORTH);
			pnlHeadline.add(new JLabel(SpringLocaleDelegate.getInstance().getMessage("nuclos.eventsupport.newInstance.infotext.2", "")), BorderLayout.CENTER);
			pnlHeadline.add(new JLabel(SpringLocaleDelegate.getInstance().getMessage("nuclos.eventsupport.newInstance.infotext.3", "")), BorderLayout.SOUTH);
			pnlInputFields.add(pnlHeadline, c);
			c.insets = new Insets(10,5,0,5);
			c.gridy = 1;
			c.gridwidth = 1;
			c.anchor = GridBagConstraints.PAGE_START;	
			
			pnlInputFields.add(new JLabel(SpringLocaleDelegate.getInstance().getMessage("R00012179", "Name")), c);
			c.gridy = 2;
			pnlInputFields.add(new JLabel(SpringLocaleDelegate.getInstance().getMessage("R00012263", "Beschreibung")), c);
			c.weightx = 1.0;
			c.gridx = 1;
			pnlInputFields.add(txtDescription, c);
			c.gridy = 1;
			pnlInputFields.add(txtClassname, c);
			c.gridy = 3;
			pnlInputFields.add(new JLabel(), c);
			
			retVal.add(pnlInputFields, BorderLayout.NORTH);
			
			return retVal;
		}
	}	

}
