package org.nuclos.client.rule.server.model;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.JComponent;

import org.apache.log4j.Logger;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.rule.server.EventSupportDelegate;
import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportTransitionVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.statemodel.valueobject.StateTransitionVO;

public class EventSupportStatePropertiesTableModel extends EventSupportPropertiesTableModel {

	private static final Logger LOG = Logger.getLogger(EventSupportStatePropertiesTableModel.class);
	
	// TODO: NEVER use spring stuff in static method or initializers. (tp)
	private static final String COL_EVENTSUPPORT = SpringLocaleDelegate.getInstance().getMessage(
			"EventSupportEntityPropertyModelColumn.4","EventSupport");
	private static final String COL_TRANSITION = SpringLocaleDelegate.getInstance().getMessage(
			"EventSupportEntityPropertyModelColumn.5","Transition");
	
	private static final String[] COLUMNS = new String[] {COL_EVENTSUPPORT, COL_TRANSITION};
	
	//
	
	private final List<EventSupportTransitionVO> entries = new ArrayList<EventSupportTransitionVO>();
	private final List<StateTransitionVO> transitions = new ArrayList<StateTransitionVO>();
	
	public EventSupportStatePropertiesTableModel(JComponent panel) {
		super(panel);
	}
			
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		boolean retVal = false;
		if (columnIndex == 1)
		{
			retVal = true;
		}
		return retVal;
	}
  	
	public void addTransitions(List<StateTransitionVO> pTransitions) {
		transitions.clear();
		transitions.addAll(pTransitions);
	}
	
	public String[] getTransitionsAsArray() {
		String[] vals = new String[transitions.size()];
		int  idx = 0;
		for (StateTransitionVO svo : getTransitions()) {
			vals[idx++] = createTransitionString(svo);
		}
		return vals;
	}

	public List<StateTransitionVO> getTransitions() {
		Collections.sort(transitions, new Comparator<StateTransitionVO>() {
			@Override
			public int compare(StateTransitionVO o1, StateTransitionVO o2) {
				return createTransitionString(o1).compareTo(createTransitionString(o2));
			}
		});
		return transitions;
	}
	
	public void addEntry(EventSupportTransitionVO eseVO) {
		entries.add(eseVO);
		fireTableRowsInserted(entries.size(), entries.size());
	}
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Object retVal = null;

		EventSupportTransitionVO eseVO = entries.get(rowIndex);
		
		switch (columnIndex) {
		case 0:
			retVal = eseVO.getEventSupportClass();			
			final EventSupportSourceVO ese = EventSupportRepository.getInstance()
					.getEventSupportByClassname(eseVO.getEventSupportClass());
			if (ese != null && ese.getName() != null && ese.isActive()) {
				setRowColour(rowIndex, Color.BLACK);
				retVal = ese.getName();
			} else {
				setRowColour(rowIndex, Color.GRAY);
			}
			break;
		case 1:
			if (eseVO.getTransition() != null) {
				StateTransitionVO stateTransition = null; 
				for (StateTransitionVO svo : transitions) {
					if (svo.getId().equals(eseVO.getTransition())) {
						stateTransition = svo;
						break;
					}
				}	
				retVal = stateTransition == null ? " " : createTransitionString(stateTransition);
			}
			break;
		default:
			break;
		}
		return retVal;
	}
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		if (aValue != null)  {
			 String value = (String) aValue;
			 if (columnIndex == 1) {
				 StateTransitionVO stateTransition = null; 
				 for (StateTransitionVO svo : transitions) {
					if (createTransitionString(svo).equals(value)) {
						stateTransition = svo;
						break;
					}
				 }
				 EventSupportTransitionVO esetVO = entries.get(rowIndex);
				 UID oldTrans = esetVO.getTransition();
				 
				 if (!esetVO.getTransition().equals(stateTransition.getId())) {
					 esetVO.setTransitionId(stateTransition == null ? null : stateTransition.getId());
					 esetVO.setTransitionName(stateTransition == null ? null : createTransitionString(stateTransition));
				
					 try {
						 EventSupportTransitionVO newTrans = 
								 EventSupportDelegate.getInstance().modifyEventSupportTransition(esetVO, oldTrans);
						 entries.remove(rowIndex);
						 entries.add(rowIndex, newTrans);
					 } catch (Exception e) {
						showAndLogException(e);
					 }					 
				 }
			 }
			 fireTableDataChanged();
		}
    }
	
	public static String createTransitionString(StateTransitionVO vo) {
		String sTransName = "";				

		MasterDataVO<UID> source = vo.getStateSourceUID() != null ?
								MasterDataCache.getInstance().get(E.STATE.getUID(), vo.getStateSourceUID()) : null;
		MasterDataVO<UID> target = MasterDataCache.getInstance().get(E.STATE.getUID(), vo.getStateTargetUID());
										
		sTransName = "-> " + target.getFieldValue(E.STATE.numeral) + " (" + target.getFieldValue(E.STATE.name) + ")";
		if (source != null) {
			sTransName = source.getFieldValue(E.STATE.numeral) + " (" + source.getFieldValue(E.STATE.name) + ") " + sTransName;
			}

		 return sTransName;
	}

	@Override
	public List<? extends EventSupportVO> getEntries() {
		return entries;
	}

	@Override
	public String[] getColumns() {
		return COLUMNS;
	}
	
	@Override
	public void addEntry(int rowId, EventSupportVO elm) {
		entries.add(rowId, (EventSupportTransitionVO) elm);
		fireTableRowsInserted(rowId, rowId);
	}
}
