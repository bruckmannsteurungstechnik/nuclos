package org.nuclos.client.rule.server.panel;

import java.awt.BorderLayout;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.border.Border;

import org.nuclos.client.explorer.EventSupportExplorerView;
import org.nuclos.client.explorer.ExplorerView;
import org.nuclos.client.rule.server.EventSupportPreferenceHandler;


public class EventSupportSourceView extends JPanel {
	
	private EventSupportExplorerView explorerView;
	private EventSupportSourcePropertyPanel sourcePanel;
	
	private JSplitPane splitPanelEventSupport;
	
	public EventSupportSourceView(EventSupportView view, Border b)
	{
		super(new BorderLayout());
		
		explorerView = new EventSupportExplorerView(view.getTreeEventSupports(), view.getActionsMap());
		explorerView.getJTree().setRootVisible(false);
		
		splitPanelEventSupport = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		
		JPanel pnlEventTypes = new JPanel();			
		pnlEventTypes.setLayout(new BorderLayout());
		pnlEventTypes.add(explorerView.getViewComponent(), BorderLayout.CENTER);
		pnlEventTypes.setBorder(b);
		
		// Tree		
		splitPanelEventSupport.setTopComponent(pnlEventTypes);
		// Properties
		splitPanelEventSupport.setBottomComponent(null);
		
		int sliderSize = EventSupportPreferenceHandler.getInstance().getSliderSize(EventSupportPreferenceHandler.PREF_NODE_EVENTSUPPORT_SOURCE_SLIDER, "mainSlider");
		if (sliderSize > 0) {
			splitPanelEventSupport.setDividerLocation(sliderSize);
		}
		else {
			splitPanelEventSupport.setResizeWeight(1d);
		}
		
		splitPanelEventSupport.addPropertyChangeListener(new PropertyChangeListener() {
			
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals("dividerLocation")) {
					Component bottomComponent = ((JSplitPane) evt.getSource()).getBottomComponent();
					if (evt.getNewValue() != null && bottomComponent != null) {
						EventSupportPreferenceHandler.getInstance().addSliderSize(
								EventSupportPreferenceHandler.PREF_NODE_EVENTSUPPORT_SOURCE_SLIDER, "mainSlider", (Integer) evt.getNewValue()); 
					}
				}
			}
		});
		
		this.add(splitPanelEventSupport, BorderLayout.CENTER);		
	}
	
	public ExplorerView getExplorerView() {
		return this.explorerView;
	}
	
	public JTree getTree()
	{
		return this.explorerView.getJTree();
	}

	public void showSourcePropertyPanel(EventSupportSourcePropertyPanel propertyPanelToShow)
	{
		// Remove old Property Panel 
		if (splitPanelEventSupport.getBottomComponent() != null)
			splitPanelEventSupport.remove(splitPanelEventSupport.getBottomComponent());
		

		// ...and load the new according to the given modeltype
		splitPanelEventSupport.setBottomComponent(propertyPanelToShow);
		
		int sliderSize = EventSupportPreferenceHandler.getInstance().getSliderSize(EventSupportPreferenceHandler.PREF_NODE_EVENTSUPPORT_SOURCE_SLIDER, "mainSlider");
		if (sliderSize > 0) {
			splitPanelEventSupport.setDividerLocation(sliderSize);
		}
		else {
			splitPanelEventSupport.setResizeWeight(1d);
		}
		
		splitPanelEventSupport.repaint();
	}
	
	public EventSupportSourcePropertyPanel getSourcePropertiesPanel() {
		// Use cached panel if exists
		if (sourcePanel == null) {
			sourcePanel = new EventSupportSourcePropertyPanel();
		}
		
		return this.sourcePanel;
	}
}
