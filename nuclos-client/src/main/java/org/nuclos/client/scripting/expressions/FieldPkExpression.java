package org.nuclos.client.scripting.expressions;

import org.nuclos.common.SF;
import org.nuclos.common.UID;

public class FieldPkExpression extends AbstractFieldExpression {

	public FieldPkExpression(UID nuclet, UID entity) {
		super(nuclet, entity, SF.PK_ID.getUID(entity));
	}
}
