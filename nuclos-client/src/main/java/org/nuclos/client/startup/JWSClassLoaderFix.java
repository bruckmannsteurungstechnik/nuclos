package org.nuclos.client.startup;

import java.awt.EventQueue;
import java.awt.Toolkit;
import java.lang.reflect.Field;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;

/**
 * A HACK solution to fix JWS application that work with Console shown but don't
 * work with console not shown or disabled.
 * 
 * see https://forums.oracle.com/thread/2552941
 * @author theskad81 
 */
public class JWSClassLoaderFix {
	
	private static final Logger LOG = Logger.getLogger(JWSClassLoaderFix.class);
	
	private JWSClassLoaderFix() {
		// Never invoked
	}

	public static void quickAndDirtyFixForProblemWithWebStartInJava7u25() {
		final String javaVersion = System.getProperty("java.version");
		if (!"1.7.0_25".equals(javaVersion)) {
			LOG.warn("NOT fixing JWS classloader: not on Java 1.7u25");
			return;
		}
		final String jwsVersion = System.getProperty("javawebstart.version");
		if (jwsVersion == null || "".equals(jwsVersion)) {
			LOG.warn("NOT fixing JWS classloader: not running on JWS");
		    return;
		}
		final ClassLoader cl = Thread.currentThread().getContextClassLoader();
		LOG.warn("TRYING to fix JWS classloader NOW");
		try {
			SwingUtilities.invokeAndWait(new Runnable() {

				@Override
				public void run() {
					try {
						// Change context in all future threads
						final Field field = EventQueue.class.getDeclaredField("classLoader");
						field.setAccessible(true);
						final EventQueue eq = Toolkit.getDefaultToolkit().getSystemEventQueue();
						field.set(eq, cl);
						
						// Change context in this thread
						Thread.currentThread().setContextClassLoader(cl);
						LOG.warn("FIXING JWS classloader SUCCEEDED");
					} catch (Exception ex) {
						// Call to java logging causes NPE :-( ...
						LOG.error("Unable to apply 'fix' for java 1.7u25: " + ex, ex);
					}
				}
			});
		} catch (Exception ex) {
			// Same as serr above
			LOG.error("Unable to apply 'fix' for java 1.7u25: " + ex, ex);
		}
	}

}
