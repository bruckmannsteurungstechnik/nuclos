//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.statemodel.models;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

import org.apache.log4j.Logger;
import org.nuclos.client.common.NuclosCollectableImage;
import org.nuclos.client.entityobject.CollectableEOEntityClientProvider;
import org.nuclos.client.resource.ResourceCache;
import org.nuclos.client.ui.collect.component.CollectableColorChooserButton;
import org.nuclos.client.ui.collect.component.CollectableResourceIconChooserButton;
import org.nuclos.common.E;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.AbstractCollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.resource.valueobject.ResourceVO;

/**
 * Model for the panel displaying the state models.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Boris@novabit.de">Boris</a>
 * @version 01.00.00
 */
public class StatePropertiesPanelModel extends NameAndDescriptionModel {

	private static final Logger LOG = Logger.getLogger(StatePropertiesPanelModel.class);
	
	// workaround - cause statemodel does not have an collectable metainformation yet.
	public class CollectableImageEntityField extends AbstractCollectableEntityField {

		@Override
		public int getFieldType() {
			return CollectableEntityField.TYPE_VALUEFIELD;
		}

		@Override
		public Class<?> getJavaClass() {
			return org.nuclos.common.NuclosImage.class;
		}

		@Override
		public String getFormatInput() {
			return null;
		}

		@Override
		public String getFormatOutput() {
			return null;
		}

		@Override
		public String getLabel() {
			return null;
		}

		@Override
		public String getDescription() {
			return null;
		}

		@Override
		public Integer getMaxLength() {
			return null;
		}

		@Override
		public Integer getPrecision() {
			return null;
		}

		@Override
		public boolean isNullable() {
			return false;
		}

		@Override
		public boolean isReferencing() {
			return false;
		}

		@Override
		public UID getReferencedEntityUID() {
			return null;
		}

		@Override
		public String getDefaultComponentType() {
			return null;
		}

		@Override
		public UID getUID() {			
			return UID.UID_NULL;
		}

		@Override
		public UID getEntityUID() {			
			return UID.UID_NULL;
		}

		@Override
		public boolean isCalculated() {
			return false;
		}

		@Override
		public boolean isCalcOndemand() {
			return false;
		}

		@Override
		public boolean isCalcAttributeAllowCustomization() {
			return false;
		}

		@Override
		public String getName() {
			return null;
		}	
		
	}
	
	public static final String PROPERTY_SHAPE_NAME = "ShapeName";
	public static final String PROPERTY_SHAPE_MNEMONIC = "ShapeMnemonic";
	public static final String PROPERTY_SHAPE_ICON = "ShapeIcon";
	public static final String PROPERTY_SHAPE_DESCRIPTION = "ShapeDescription";

	public Document docMnemonic = new PlainDocument();

	public ComboBoxModel modelTab = new DefaultComboBoxModel();
	public final NuclosCollectableImage clctImage;
	public Document docButtonLabelDE = new PlainDocument();
	public Document docButtonLabelEN = new PlainDocument();
	public CollectableResourceIconChooserButton clctButtonIcon;
	public CollectableColorChooserButton clctColor;

	protected static final Logger log = Logger.getLogger(StatePropertiesPanelModel.class);
	
	private Map<String, String> mpTabName = new HashMap<String, String>();
	
	public StatePropertiesPanelModel() {
		super();
		this.clctImage = new NuclosCollectableImage(new CollectableImageEntityField());
		this.clctButtonIcon = new CollectableResourceIconChooserButton(
				CollectableEOEntityClientProvider.getInstance().getCollectableEntity(E.STATE.getUID()).getEntityField(E.STATE.buttonIcon.getUID()));
		this.clctColor = new CollectableColorChooserButton(
				CollectableEOEntityClientProvider.getInstance().getCollectableEntity(E.STATE.getUID()).getEntityField(E.STATE.color.getUID()));
	}

	
	public String getButtonLabel(Locale locale) {
		String sResult = "";
		try {
			if (Locale.GERMAN.equals(locale)) {
				sResult = docButtonLabelDE.getText(0, docButtonLabelDE.getLength());
			} else {
				sResult = docButtonLabelEN.getText(0, docButtonLabelEN.getLength());
			}
		}
		catch (BadLocationException e) {
			// this should never happens
			LOG.warn("getButtonLabel failed: " + e, e);
		}
		return sResult;
	}
	
	public void setButtonLabel(Locale locale, String sButtonLabel) {
		try {
			if (Locale.GERMAN.equals(locale)) {
				this.docButtonLabelDE.remove(0, docButtonLabelDE.getLength());
				this.docButtonLabelDE.insertString(0, sButtonLabel, null);
			} else {
				this.docButtonLabelEN.remove(0, docButtonLabelEN.getLength());
				this.docButtonLabelEN.insertString(0, sButtonLabel, null);
			}
		}
		catch (BadLocationException ex) {
			throw new NuclosFatalException(ex);
		}
	}

	public Integer getNumeral() {
		Integer iNumeral = null;
		String strValue = "";
		try {
			strValue = docMnemonic.getText(0, docMnemonic.getLength());
		    /** otherwise clearing the field is resulting every time in a annoying dialog, the validation for null value is done later and is safe */
		    if (!StringUtils.isNullOrEmpty(strValue))
			iNumeral = new Integer(strValue);
		}
		catch (BadLocationException e) {
			// this should never happens
			LOG.warn("getNumeral failed: " + e, e);
		} catch (NumberFormatException e) {
			log.warn("Das Statusnumeral ["+strValue+"] ist kein numerischer Wert.", e);
		}
		return iNumeral;
	}

	public void setNumeral(Integer iNumeral) {
		try {
			this.docMnemonic.remove(0, docMnemonic.getLength());
			if (iNumeral != null) {
				this.docMnemonic.insertString(0, iNumeral.toString(), null);
			}
		}
		catch (BadLocationException ex) {
			throw new NuclosFatalException(ex);
		}
	}

	public void setIcon(org.nuclos.common.NuclosImage icon) {
		clctImage.setField(new CollectableValueField(icon));
	}

	public org.nuclos.common.NuclosImage getIcon() {
		try {
			return (org.nuclos.common.NuclosImage) clctImage.getField().getValue();
		}
		catch (CollectableFieldFormatException ex) {
			throw new NuclosFatalException(ex);
		}		
	}
	
	public void setButtonIcon(ResourceVO resButtonIcon) {
		clctButtonIcon.setField(new CollectableValueIdField(resButtonIcon==null?null:resButtonIcon.getId(), resButtonIcon==null?null:resButtonIcon.getName()));
	}
	
	public ResourceVO getButtonIcon() {
		try {
			CollectableValueIdField clctef = (CollectableValueIdField) clctButtonIcon.getField();			
			return ResourceCache.getInstance().getResourceById((UID) clctef.getValueId());
		}
		catch (CollectableFieldFormatException ex) {
			throw new NuclosFatalException(ex);
		}	
	}
	
	public void setColor(String color) {
		clctColor.setField(new CollectableValueField(color));
	}
	
	public String getColor() {
		try {
			return (String) clctColor.getField().getValue();
		}
		catch (CollectableFieldFormatException ex) {
			throw new NuclosFatalException(ex);
		}	
	}

	public String getTab() {
		String sReturn = "";
		for(String key : mpTabName.keySet()) {
			if(key != null && LangUtils.equal(mpTabName.get(key), modelTab.getSelectedItem())) {
				sReturn = key;
				break;
			}
		}
		 
		return sReturn;
	}
	
	public void setTab(String sTab) {	
		modelTab.setSelectedItem(sTab == null ? "" : mpTabName.get(sTab));
	}
	
	public void setTabModelList(Map<String, String> mp) {
		mpTabName = mp;
		if(modelTab instanceof DefaultComboBoxModel) {
			DefaultComboBoxModel model = (DefaultComboBoxModel)modelTab;
			model.removeAllElements();
			for(String sName : mp.values()) {
				model.addElement(sName);
			}
			// always add empty entry @see NUCLOS-722
			model.addElement("");
		}
	}
	
	
}
