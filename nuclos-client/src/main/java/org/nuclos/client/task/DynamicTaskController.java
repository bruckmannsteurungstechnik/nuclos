//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.task;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.prefs.Preferences;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.RowSorter.SortKey;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.RowSorterEvent;
import javax.swing.event.RowSorterListener;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;
import org.nuclos.client.common.KeyBinding;
import org.nuclos.client.common.KeyBindingProvider;
import org.nuclos.client.common.TimedPreferencesUpdater;
import org.nuclos.client.common.Utils;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.datasource.DatasourceDelegate;
import org.nuclos.client.entityobject.EntityObjectDelegate;
import org.nuclos.client.genericobject.ReportController;
import org.nuclos.client.main.Main;
import org.nuclos.client.mandator.ClientMandatorContext;
import org.nuclos.client.ui.CommonMultiThreader;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.client.ui.layer.LayerLock;
import org.nuclos.client.ui.model.ResultVOTableModel;
import org.nuclos.client.ui.table.NuclosTableRowSorter;
import org.nuclos.client.ui.table.TableUtils;
import org.nuclos.common.Actions;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.preferences.PreferencesProvider;
import org.nuclos.common.preferences.PreferencesUtils;
import org.nuclos.common.preferences.TablePreferences;
import org.nuclos.common.preferences.TablePreferencesManager;
import org.nuclos.common.report.valueobject.DynamicTasklistVO;
import org.nuclos.common.report.valueobject.ResultColumnVO;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common.tasklist.TasklistDefinition;
import org.nuclos.common2.ClientPreferences;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.PreferencesException;

/**
 * Controller for <code>DynamicTaskView</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	thomas.schiffmann
 * @version 01.00.00
 */
public class DynamicTaskController extends RefreshableTaskController<DynamicTaskView> {

	private static final Logger LOG = Logger.getLogger(DynamicTaskController.class);

	private static final String PREFS_NODE_DYNAMICTASKS_REFRESH_INTERVAL_SELECTED = "dynamicTasksRefreshSelected";

	//

	private Map<UID, DynamicTaskView> views;
	private Map<UID, TablePreferencesManager> tblprefManagers;

	private DynamicTaskFilterPanel filterPanel;
	private List<Future<LayerLock>> allLocks = new ArrayList<>();

	DynamicTaskController() {
		super();
		views= new HashMap<UID, DynamicTaskView>();
		tblprefManagers = new ConcurrentHashMap<>();
	}

	public DynamicTaskView newDynamicTaskView(TasklistDefinition def, int refreshInterval, DynamicTasklistVO dtl) {
		final DynamicTaskView taskview = new DynamicTaskView(def, dtl);
		taskview.init();
		views.put(def.getId(), taskview);
		refresh(taskview, true);

		setActions(taskview);
		setRenderers(taskview);
		setColumnModelListener(taskview);
		setPopupMenuListener(taskview);

		KeyBinding keybinding = KeyBindingProvider.REFRESH;
		taskview.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(keybinding.getKeystroke(), keybinding.getKey());
		taskview.getActionMap().put(keybinding.getKey(), new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				refresh(taskview, false);
			}
		});

		taskview.setRefreshInterval(migrateIntervalFromPreferences(taskview, refreshInterval));
		setRefreshIntervalForMultiViewRefreshable(taskview, taskview.getRefreshInterval());

		return taskview;
	}

	@Override
	public void setRefreshIntervalForMultiViewRefreshable(
			ScheduledRefreshable sRefreshable, int min) {
		super.setRefreshIntervalForMultiViewRefreshable(sRefreshable, min);
	}

	private Preferences getPreferences() {
		return ClientPreferences.getInstance().getUserPreferences().node("taskPanel").node("dynamicTasks");
	}

	private int migrateIntervalFromPreferences(DynamicTaskView taskview, int refreshInterval) {
		List<Integer> fromPreferences = readIntervalTasksFromPreferences(taskview);
		if (fromPreferences != null) {
			UID taskId = null;
			try {
				taskId = taskview.getDef().getId();
				PreferencesUtils.removeChild(getPreferences(), PREFS_NODE_DYNAMICTASKS_REFRESH_INTERVAL_SELECTED + "_" + taskId);
			} catch (Exception e1) {
				LOG.error("Error during preferences migration for refresh interval [taskviewID=" + taskId + "]: " + e1.getMessage(), e1);
			}
		}
		if (refreshInterval > 0) {
			return refreshInterval;
		} else if (fromPreferences != null && fromPreferences.size() > 0) {
			return RigidUtils.defaultIfNull(fromPreferences.get(0), 0);
		}
		return 0;
	}

	private List<Integer> readIntervalTasksFromPreferences(DynamicTaskView taskview) {
		List<Integer> refreshIntervalTasksSelectedList = null;
		try {
			refreshIntervalTasksSelectedList = PreferencesUtils.getIntegerList(getPreferences(), PREFS_NODE_DYNAMICTASKS_REFRESH_INTERVAL_SELECTED + "_" + taskview.getDef().getId());
		} catch (PreferencesException ex) {
			LOG.error(getSpringLocaleDelegate().getMessage(
					"PersonalTaskController.10","Der Filterzustand konnte nicht aus den Einstellungen gelesen werden"), ex);
		}
		return refreshIntervalTasksSelectedList;
	}

	private List<Object> getInternalValues(UID uid, JTable table) {
		if (!(table.getModel() instanceof  ResultVOTableModel)) {
			return Collections.emptyList();
		}

		final ResultVOTableModel model = (ResultVOTableModel) table.getModel();
		final RowSorter<ResultVOTableModel> rs = (RowSorter<ResultVOTableModel>) table.getRowSorter();
		final int index = model.getInternColumnIndex(uid);

		final List<Object> ret = new ArrayList<>();
		final int[] selection = table.getSelectedRows();
		for (int i : selection) {
			int mi = rs.convertRowIndexToModel(i);
			Object obj = model.getInternValueAt(mi, index);
			ret.add(obj);
		}

		return ret;
	}

	@Override
	protected List<Object> getIdsForCustomRule(final DynamicTaskView view) {
		return getInternalValues(view.getDef().getCustomRuleIdFieldUid(), view.getTable());
	}

	private void showCustomRuleContext(final DynamicTaskView view, MouseEvent ev) {
		final JTable table = view.getTable();

		int row = table.rowAtPoint(ev.getPoint());
		if (ev.isShiftDown() || ev.isControlDown()) {
			table.addRowSelectionInterval(row, row);
		} else {
			table.setRowSelectionInterval(row, row);
		}

		JPopupMenu menu = new JPopupMenu();
		menu.show(ev.getComponent(), ev.getX(), ev.getY());

		String sOpen = getSpringLocaleDelegate().getMessage(
				"DynamicTaskController.19","Aufgabe öffnen");

		JMenuItem itemOpen = new JMenuItem(sOpen);
		itemOpen.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				cmdShowDetails(view);
			}
		});

		menu.add(itemOpen);

		final TasklistDefinition tld = view.getDef();
		if (tld.getCustomRuleEntityFieldUid() == null || tld.getCustomRuleIdFieldUid() == null) {
			return;
		}

		if (!SecurityCache.getInstance().isActionAllowed(Actions.ACTION_EXECUTE_RULE_BY_USER)) {
			return;
		}

		List<Object> lstCREntities = getInternalValues(tld.getCustomRuleEntityFieldUid(), table);
		Collection<Object> set = new HashSet<>(lstCREntities);
		if (set.size() != 1) {
			// Multiple Records can only be processed when they share the same Custom Rule BO
			return;
		}
		final UID crEntity = UID.parseUID(lstCREntities.get(0).toString());

		addCustomRulesToPopupMenuIfAny(crEntity, view, menu);

	}

	private void setActions(final DynamicTaskView view) {
		super.addRefreshIntervalActions(view);

		//add mouse listener for context menu and double click in table:
		view.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent ev) {
				if (SwingUtilities.isRightMouseButton(ev)) {
					showCustomRuleContext(view, ev);
				} else if (ev.getClickCount() == 2) {
					cmdShowDetails(view);
				}
			}
		});

		view.getRefreshButton().setAction(new AbstractAction("", Icons.getInstance().getIconRefresh16()) {
			@Override
			public void actionPerformed(ActionEvent e) {
				cmdRefresh(view);
			}
		});

		view.getCancelButton().setVisible(false);

		view.getFilterButton().setAction(new AbstractAction("", Icons.getInstance().getIconFilter16()) {
			@Override
			public void actionPerformed(ActionEvent e) {
				cmdFilter(view);
			}
		});

		view.getResetMenuItem().setAction(new AbstractAction(
				getSpringLocaleDelegate().getMessage("ExplorerController.34", "Listenansicht zurücksetzen"),
				Icons.getInstance().getIconUndo16()) {
			@Override
			public void actionPerformed(ActionEvent e) {
				resetPreferences(view);
				refresh(view, true);
			}
			@Override
			public boolean isEnabled() {
				final TablePreferences tp = getTablePreferencesManager(view).getSelected();
				return tp.isShared() && tp.isCustomized();
			}
		});
	}

	private void setRenderers(DynamicTaskView view) {
		view.getTable().setRowHeight(SubForm.MIN_ROWHEIGHT);
		view.getTable().setTableHeader(new TaskViewTableHeader(view.getTable().getColumnModel()));

		view.getTable().setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				final Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				 if(ArrayUtils.contains(table.getSelectedRows(),row))
					 return c;


				ResultVOTableModel model = (ResultVOTableModel) table.getModel();
				Color bgColor = model.getRowColor(table.getRowSorter().convertRowIndexToModel(row));
				if (bgColor != null) {
					c.setBackground(bgColor);
					c.setForeground(Utils.getBestForegroundColor(bgColor));
				}

				return c;

			}
		});
	}

	private void setPopupMenuListener(final DynamicTaskView taskview){
		taskview.getTable().getTableHeader().addMouseListener(new MouseAdapter() {
			@SuppressWarnings("serial")
			public void mouseClicked(MouseEvent e) {
				if(SwingUtilities.isRightMouseButton(e)) {
					JPopupMenu pop = new JPopupMenu();

					if (SecurityCache.getInstance().isActionAllowed(Actions.ACTION_CUSTOMIZE_ENTITY_AND_SUBFORM_COLUMNS)) {
						final JMenuItem miPublishColumns = new JMenuItem(new AbstractAction(getSpringLocaleDelegate().getMessage(
								"DetailsSubFormController.4", "Spalten in Vorlage publizieren"),
								Icons.getInstance().getIconRedo16()) {
							@Override
							public void actionPerformed(ActionEvent e) {
								try {
									final TablePreferencesManager tblprefManager = getTablePreferencesManager(taskview);
									tblprefManager.update(tblprefManager.getSelected());
								} catch (Exception e1) {
									Errors.getInstance().showExceptionDialog(Main.getInstance().getMainController().getTaskController().getTabFor(taskview), e1);
								}
							}
						});
						pop.add(miPublishColumns);
					}

					JMenuItem miRestoreColumns = new JMenuItem(new AbstractAction(
							getSpringLocaleDelegate().getMessage("DetailsSubFormController.3", "Alle Spalten auf Vorlage zurücksetzen"),
							Icons.getInstance().getIconUndo16()) {
						@Override
						public void actionPerformed(ActionEvent e) {
							try {
								final TablePreferencesManager tblprefManager = getTablePreferencesManager(taskview);
								tblprefManager.reset(tblprefManager.getSelected());
								refresh(taskview, true);
							}
							catch (Exception e1) {
								Errors.getInstance().showExceptionDialog(Main.getInstance().getMainController().getTaskController().getTabFor(taskview), e1);
							}
						}
					});
					pop.add(miRestoreColumns);

					pop.setLocation(e.getLocationOnScreen());
					pop.show(taskview.getTable(), e.getX(), e.getY());
				}
			}

		});
	}

	private void cmdRefresh(final DynamicTaskView gotaskview) {
		UIUtils.runCommand(gotaskview, new CommonRunnable() {
			@Override
            public void run() {
				refresh(gotaskview, false);
			}
		});
	}

	private void cmdFilter(final DynamicTaskView gotaskview) {
		UIUtils.runCommand(gotaskview, new CommonRunnable() {
			@Override
            public void run() {
				filter(gotaskview, false);
			}
		});
	}

	private void cmdPrint(final DynamicTaskView gotaskview) {
		UIUtils.runCommand(gotaskview, new CommonRunnable() {
			@Override
            public void run() {
				print(gotaskview);
			}
		});
	}

	void filter(final DynamicTaskView taskview, final boolean fromPreferences) {
		if(taskview.getFilterPanel().isVisible()==false){
			// OPEN FILTER BAR
			Map<String, FilterComponent> columnFilters = new HashMap<String, FilterComponent>();
			Enumeration<TableColumn> b = taskview.getTable().getColumnModel().getColumns();
			while(b.hasMoreElements()){
				final TableColumn e = b.nextElement();
				FilterComponent fc = new FilterComponent(e.getIdentifier().toString());
				final KeyListener kl = new KeyAdapter() {
					@Override
					public void keyPressed(KeyEvent e) {
						if (KeyEvent.VK_ENTER == e.getKeyCode()) {
							refresh(taskview, fromPreferences);
						}
					}
				};
				fc.getJComponent().addKeyListener(kl);
				columnFilters.put(e.getIdentifier().toString(), fc);
			}
			filterPanel = new DynamicTaskFilterPanel(taskview.getTable().getColumnModel(), columnFilters);
			ActionListener resetActionListener = new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					filterPanel.resetFilter();
					refresh(taskview, fromPreferences);
				}
            };
			filterPanel.getResetFilterButton().addActionListener(resetActionListener);
			filterPanel.setVisible(true);
			taskview.getFilterPanel().add(filterPanel, BorderLayout.CENTER);
			taskview.getFilterPanel().setVisible(!taskview.getFilterPanel().isVisible());

			taskview.repaint();
		}else if(taskview.getFilterPanel().isVisible()==true){
			// CLOSE FILTER BAR
			if(filterPanel!=null){
				filterPanel.resetFilter();
				filterPanel.close();
				taskview.getFilterPanel().remove(filterPanel);
                filterPanel=null;
				refresh(taskview, fromPreferences);
			}
			taskview.getFilterPanel().setVisible(!taskview.getFilterPanel().isVisible());
		}
	}

	private ResultVO doFilter(ResultVO vo, Map<String, String> fvList) {
		if (fvList!=null && vo!=null && vo.getColumns().size()>0){
			ResultColumnVO formater = vo.getColumns().get(0);
			Map<Integer, String> fvIdx = new HashMap<Integer, String>();
			List<ResultColumnVO> cols = vo.getColumns();
			int i=0;
			for (ResultColumnVO col : cols) {
				fvIdx.put(i, fvList.get(col.getColumnLabel()));
				i++;
			}
			List<Object[]> rows = vo.getRows();
			for (int x=rows.size()-1; x>=0 ;x--) {
				boolean filterOut = false;
				Object[] row = rows.get(x);
				int j=0;
				for (Object object : row) {
					String fv = fvIdx.get(j);
					if(object!=null && !filterOut){
						String sObj = formater.format(object);
						filterOut = filterOut(fv, sObj);
					}
					j++;
				}
				if(filterOut){
					rows.remove(x);
				}
			}
			return vo;

		} else {
			return vo;
		}
	}

	private boolean filterOut(String filter, String value){
		if(filter!=null && !filter.equals("") && value!=null && !value.equals("")){
            return value.toLowerCase().indexOf(filter.toLowerCase()) == -1;
		}
		return false;
	}

	void resetPreferences(final DynamicTaskView taskView) {
		final TablePreferencesManager tblprefManager = getTablePreferencesManager(taskView);
		final TablePreferences tp = tblprefManager.getSelected();
		if (tp.isShared()) {
			tblprefManager.reset(tp);
		}
	}

	@Override
	protected void refresh(final DynamicTaskView taskView) {
		refresh(taskView, false);
	}

	void refresh(final DynamicTaskView taskview, final boolean fromPreferences) {
		final DynamicTasklistVO dtl = taskview.getDynamicTasklist();
		final TasklistDefinition def = taskview.getDef();
		final UID taskControllerUID = def.getId();
		final DynClientWorkerTaskControllerAdapter clientWorker = new DynClientWorkerTaskControllerAdapter(this, taskControllerUID) {

			Map<String, String> fvList = null;

			ResultVO vo = null;

			@Override
			public void init() throws CommonBusinessException {
				if (filterPanel!=null && filterPanel.isVisible()) {
					fvList = filterPanel.getFilterValues();
				}
				super.init();
			}

			@Override
			public void work() throws CommonBusinessException {
				vo = DatasourceDelegate.getInstance().getDynamicTasklistData(dtl.getId());
				vo = doFilter(vo, fvList);
			}

			@Override
			public void paint() throws CommonBusinessException {
				if (vo==null) {
					return;
				}
				if (fromPreferences) {
					taskview.setIgnorePreferencesUpdates(true);
					TableModel mdl = new ResultVOTableModel(taskview.getDef(), vo, getColumnOrderFromPreferences(taskview));
					final NuclosTableRowSorter<TableModel> rs = new NuclosTableRowSorter<TableModel>(mdl);
					taskview.getTable().setRowSorter(rs);
					taskview.getTable().setModel(mdl);
					rs.addRowSorterListener(new RowSorterListener() {
						@Override
						public void sorterChanged(RowSorterEvent e) {
							storeSortOrder(taskview);
						}
					});

					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							List<SortKey> sorting = getSortKeys(taskview);
							rs.setSortKeys(sorting);
						}
					});

					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							Map<UID, Integer> widths = getColumnWidthsFromPreferences(taskview);
							for (int iColumn = 0; iColumn < taskview.getTable().getColumnModel().getColumnCount(); iColumn++) {
								final String columnName = taskview.getTable().getColumnName(iColumn);
								final UID columnUID = new UID(columnName);
								final int iPreferredCellWidth;
								if (widths.containsKey(columnUID)) {
									// known column
									iPreferredCellWidth = widths.get(columnUID);
								} else {
									// new column
									iPreferredCellWidth = TableUtils.getPreferredColumnWidth(taskview.getTable(), iColumn, 50, TableUtils.TABLE_INSETS);
								}
								TableColumn column = taskview.getTable().getColumn(columnName);
								column.setPreferredWidth(iPreferredCellWidth);
								column.setWidth(iPreferredCellWidth);
							}
						}
					});

					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							taskview.setIgnorePreferencesUpdates(false);
						}
					});
				}
				else {
					ResultVOTableModel mdl = (ResultVOTableModel) taskview.getTable().getModel();
					mdl.setData(vo);
				}

				super.paint();
			}
		};

		CommonMultiThreader.getInstance().execute(clientWorker);
	}

	@Override
	protected void cancel(final DynamicTaskView taskView) {
		EntityObjectDelegate.getInstance().cancelRunningStatements(taskView.getDef().getTaskEntityUID());
	}

	void print(DynamicTaskView gotaskview) {
		try {
			new ReportController(getTabbedPane().getComponentPanel()).export(gotaskview.getDef().getTaskEntityUID(), null, gotaskview.getTable(), null);
		}
		catch (CommonBusinessException ex) {
			Errors.getInstance().showExceptionDialog(this.getTabbedPane().getComponentPanel(), ex);
		}
	}

	private void cmdShowDetails(final DynamicTaskView view) {
		final JTable table = view.getTable();
		final TasklistDefinition def = view.getDef();
		final UID entity = def.getTaskEntityUID();
		final UID idfield = def.getDynamicTasklistIdFieldUid();
		final UID entityfield = def.getDynamicTasklistEntityFieldUid();

		if (idfield != null && (entityfield != null || entity != null)) {
			if (table.getModel() instanceof ResultVOTableModel) {
				final RowSorter<ResultVOTableModel> rs = (RowSorter<ResultVOTableModel>) table.getRowSorter();
				final ResultVOTableModel model = (ResultVOTableModel) table.getModel();
				final int idIndex = model.getInternColumnIndex(idfield);
				final int entityIndex = model.getInternColumnIndex(entityfield != null ? entityfield : entity);

				final int[] selection = table.getSelectedRows();
				for (int i : selection) {
					int mi = rs.convertRowIndexToModel(i);
					final Object oId = model.getInternValueAt(mi, idIndex);
					final Long id;
					if (oId instanceof Double) {
						id = ((Double)oId).longValue();
					}
					else {
						String sObj = String.valueOf(oId);
						id = Long.parseLong(sObj);
					}
					final UID _entity = entityfield != null ? UID.parseUID((String) model.getInternValueAt(mi, entityIndex)) : entity;
					UIUtils.runCommandLater(Main.getInstance().getMainController().getTaskController().getTabFor(view), new Runnable() {
						@Override
						public void run() {
							Main.getInstance().getMainController().showDetails(_entity, id);
						}
					});
				}
			}
		}
	}

	@Override
	public ScheduledRefreshable getSingleScheduledRefreshableView() {
		throw new IllegalStateException();
	}

	@Override
	public void refreshScheduled(ScheduledRefreshable sRefreshable) {
		if (sRefreshable instanceof DynamicTaskView){
			refresh((DynamicTaskView)sRefreshable, false);
		}
		else {
			throw new IllegalStateException();
		}
	}

	private void setColumnModelListener(final DynamicTaskView view) {
		view.getTable().getColumnModel().addColumnModelListener(new PreferencesUpdateListener(view));
	}

	private void storePreferences(DynamicTaskView view) {
		if (view.isIgnorePreferencesUpdates()) {
			return;
		}
		final TablePreferencesManager tblprefManager = getTablePreferencesManager(view);
		List<UID> fields = new ArrayList<UID>();
		List<Integer> widths = new ArrayList<Integer>();
		for (int i = 0; i < view.getTable().getColumnModel().getColumnCount(); i++) {
			UID fieldUid = new UID(view.getTable().getColumnName(i));
			// for cleanup check exists
			if (!fields.contains(fieldUid)) {
				fields.add(fieldUid);
				TableColumn tc = view.getTable().getColumnModel().getColumn(i);
				widths.add(tc.getWidth());
			}
		}
		final TablePreferences tp = tblprefManager.getSelected();
		if (!tp.isUserdefinedName() || StringUtils.equals(view.getDef().getName(), tp.getName())) {
			// new prefs, we have to set a name, in order to differentiate it later from other tasklist prefs
			tp.setName(StringUtils.defaultIfNull(view.getDef().getName(), view.getDef().getId().getString()));
			tp.setUserdefinedName(true);
		}
		tblprefManager.setColumnPreferences(fields, widths, null);
	}

	private void storeSortOrder(final DynamicTaskView view) {
		if (view.isIgnorePreferencesUpdates()) {
			return;
		}
		final TablePreferencesManager tblprefManager = getTablePreferencesManager(view);
		tblprefManager.setSortKeys(view.getTable().getRowSorter().getSortKeys(),
				new TablePreferencesManager.IColumnFieldUidResolver() {
					@Override
					public UID getColumnFieldUid(int iColumn) {
						return new UID(view.getTable().getColumnName(iColumn));
					}
		});
	}

	private List<UID> getColumnOrderFromPreferences(DynamicTaskView view) {
		final TablePreferencesManager tblprefManager = getTablePreferencesManager(view);
		return tblprefManager.getSelectedColumns();
	}

	private Map<UID, Integer> getColumnWidthsFromPreferences(DynamicTaskView view) {
		final TablePreferencesManager tblprefManager = getTablePreferencesManager(view);
		return tblprefManager.getColumnWidthsMap();
	}

	private List<SortKey> getSortKeys(final DynamicTaskView view) {
		final TablePreferencesManager tblprefManager = getTablePreferencesManager(view);
		return tblprefManager.getSortKeys(new TablePreferencesManager.IColumnIndexResolver() {
			@Override
			public int getColumnIndex(UID columnIdentifier) {
				// Dynamic column model does not work with UIDs yet...
				String sColumnIdentifier = columnIdentifier.getString();
				return view.getTable().getColumnModel().getColumnIndex(sColumnIdentifier);
			}
		});
	}

	private TablePreferencesManager getTablePreferencesManager(DynamicTaskView view) {
		TablePreferencesManager result = tblprefManagers.get(view.getDef().getId());
		if (result == null) {
			result = PreferencesProvider.getInstance().
					getTablePreferencesManagerForTaskList(view.getDef().getId(),
							Main.getInstance().getMainController().getUserName(),
							ClientMandatorContext.getMandatorUID());
			tblprefManagers.put(view.getDef().getId(), result);
		}
		return result;
	}

	private static abstract class DynClientWorkerTaskControllerAdapter extends CommonClientWorkerTaskControllerAdapter {

		DynamicTaskController dynCtrl = null;

		public DynClientWorkerTaskControllerAdapter(DynamicTaskController dynCtrl, UID uidTaskDef) {
			super(dynCtrl, uidTaskDef);
			this.dynCtrl = dynCtrl;
		}

		@Override
		protected TaskView getTaskView(final UID uidTaskDef) {
			return dynCtrl.views.get(uidTaskDef);
		}

		@Override
		protected List<Future<LayerLock>> getAllLocksList() {
			return dynCtrl.allLocks;
		}
	}

	protected class PreferencesUpdateListener extends TimedPreferencesUpdater implements TableColumnModelListener {

		private final WeakReference<DynamicTaskView> weakView;

		public PreferencesUpdateListener(final DynamicTaskView view) {
			weakView = new WeakReference<>(view);
		}

		@Override
		public void columnSelectionChanged(ListSelectionEvent ev) {
		}

		@Override
		public void columnMoved(TableColumnModelEvent ev) {
			if (ev.getFromIndex() != ev.getToIndex()) {
				storePreferences();
			}
		}

		@Override
		public void columnMarginChanged(ChangeEvent ev) {
			storePreferences();
		}

		@Override
		public void columnAdded(TableColumnModelEvent ev) {
		}

		@Override
		public void columnRemoved(TableColumnModelEvent ev) {
		}

		private void storePreferences() {
			final DynamicTaskView dynamicTaskView = weakView.get();
			if (dynamicTaskView != null) {
				if (dynamicTaskView.isIgnorePreferencesUpdates()) {
					return;
				}
				super.registerWorker(new PreferencesStoreWorker(dynamicTaskView.getDef().getId()) {
					@Override
					public void storePreferences() {
						DynamicTaskController.this.storePreferences(dynamicTaskView);
					}
				});
			}
		}
	}

}


