//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.task;

import javax.swing.JComponent;
import javax.swing.JTextField;

public class FilterComponent  {

	String id;
	JTextField comp = new JTextField();
	
	public FilterComponent(String id) {
		this.id = id;
	}
	
	public JComponent getJComponent() {
		return comp;
	}
	
	public JComponent getControlComponent() {
		return comp;
	}
	
	public String getValue(){
		return comp.getText();
	}
	
	public String getId(){
		return id;
	}

	public void clear() {
		comp.setText(null);
	}

	public void resetFilter() {
		comp.setText(null);
	}

}
