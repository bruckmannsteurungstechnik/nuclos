//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.InputMismatchException;

import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import org.apache.log4j.Logger;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common2.SpringLocaleDelegate;

public class DocumentFactory {

	private static final Logger LOG = Logger.getLogger(DocumentFactory.class);

	private DocumentFactory() {
		// Never invoked.
	}

	public static AbstractDocument getDocument(final CollectableEntityField field) {
		final AbstractDocument document;
		final Class<?> clazz = field.getJavaClass();
		if (clazz.isAssignableFrom(Integer.class) || clazz.isAssignableFrom(Long.class)) {
			document = new IntegerLongDocument(field.getMaxLength(), clazz);
		} else if (clazz.isAssignableFrom(Double.class)) {
			document = new DoubleDocument();
		} else if (clazz.isAssignableFrom(String.class)) {
			document = new StringDocument(field.getMaxLength());
		} else {
			LOG.debug("No AbstractDocument for " + clazz);
			document = null;
		}
		return document;
	}

	private static class IntegerLongDocument extends PlainDocument {

		private final Integer maxLength;
		private final Class<?> clz;

		private IntegerLongDocument(Integer maxLength, Class<?> clz) {
			this.maxLength = maxLength;
			this.clz = clz;
		}

		@Override
		public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
			int textLength = getText(0, getLength()).length();
			if (maxLength != null) {
				int lengthRemaining = maxLength - textLength;
				if (lengthRemaining <= 0) {
					return;
				}
				if (str.length() > lengthRemaining) {
					str = str.substring(0, lengthRemaining);
				}
			}
			if (str.length() > 0) {
				try {
					if (clz.isAssignableFrom(Integer.class)) {
						Integer.parseInt(str);
					} else {
						Long.parseLong(str);
					}
					super.insertString(offs, str, a);
				} catch (NumberFormatException ex) {
					// no Integer
				}
			}
			if ("-".equals(str)) {
				super.insertString(offs, str, a);
			}
		}

	}

	private static class DoubleDocument extends PlainDocument {

		private DoubleDocument() {
		}

		@Override
		public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
			if (str.length() > 0) {
				try {
					if (".".equals(str) || ",".equals(str) || "-".endsWith(str)) {
						super.insertString(offs, str, a);
						return;
					}
					NumberFormat numberFormat = NumberFormat.getInstance(SpringLocaleDelegate.getInstance().getLocale());
					double d = numberFormat.parse(str).doubleValue();
					super.insertString(offs, str, a);
				} catch (ParseException ex) {
					LOG.info("not a number: " + str + ": " + ex);
				} catch (NumberFormatException ex) {
					LOG.info("not a number: " + str + ": " + ex);
				} catch (InputMismatchException ex) {
					LOG.info("not a number: " + str + ": " + ex);
				}
			} else {
				super.insertString(offs, str, a);
			}
		}
	}

	private static class StringDocument extends PlainDocument {

		private final Integer maxLength;

		private StringDocument(Integer maxLength) {
			this.maxLength = maxLength;
		}

		@Override
		public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
			int textLength = getText(0, getLength()).length();
			if (maxLength != null) {
				int lengthRemaining = maxLength - textLength;
				if (lengthRemaining <= 0) {
					return;
				}
				if (str.length() > lengthRemaining) {
					str = str.substring(0, lengthRemaining);
				}
			}
			if (str.length() > 0) {
				super.insertString(offs, str, a);
			}
		}

	}
}
