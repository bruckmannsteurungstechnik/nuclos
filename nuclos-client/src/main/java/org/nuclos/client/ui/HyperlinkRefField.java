//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui;

import java.util.ArrayList;

import javax.swing.Icon;

import org.nuclos.client.customcomp.resplan.CollectableHelper;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.client.ui.labeled.ILabeledComponentSupport;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common2.exception.CommonBusinessException;

public class HyperlinkRefField<PK> extends HyperlinkTextFieldWithButton {
	/**  */
	private static final long serialVersionUID = 1L;

	final CollectableEntityField clctef;
	private PK referencedObjectId;

	public HyperlinkRefField(CollectableEntityField clctef, ILabeledComponentSupport support, boolean bSearchable) {
		super(new ArrayList<Icon>(), support, bSearchable);

		this.clctef = clctef;
		this.setInputVerifier(null);
	}

	@Override
	public void openLink() {
		try {
			UID entityUID = clctef.getReferencedEntityUID();

			if (entityUID == null) {
				// Probably in a subform
				entityUID = clctef.getEntityUID();
			}

			if (entityUID == null) {
				return;
			}

			@SuppressWarnings("unchecked")
			CollectableHelper<PK, ?, ?> helper = (CollectableHelper<PK, ?, ?>) CollectableHelper.getForEntity(entityUID);
			CollectController<PK, ?> controller = helper.newCollectController();
			controller.runViewSingleCollectableWithId(referencedObjectId, true);
		} catch (CommonBusinessException e) {
			throw new NuclosFatalException("Could not open entity", e);
		}
	}

	public void setReferencedObjectId(PK id) {
		referencedObjectId = id;
	}

	@Override
	public boolean isEnabled() {
		// Enable only for search, not for editing
		return bSearchable;
	}
}
