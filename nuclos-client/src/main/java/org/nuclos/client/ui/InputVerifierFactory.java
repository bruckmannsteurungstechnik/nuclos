//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JTextField;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.collect.collectable.CollectableComponentTypes;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;

public abstract class InputVerifierFactory {
	
	public static InputVerifier getInputVerifier(final CollectableEntityField field) {
		Class clazz = field.getJavaClass();
		MetaProvider metaProvider = SpringApplicationContextHolder.getBean(MetaProvider.class);
		String sFieldName = "";

		if (metaProvider.checkEntityField(field.getUID())) {
			sFieldName = metaProvider != null ? metaProvider.getEntityField(field.getUID()).getFieldName() : "";
		}

		return getInputVerifier(
				clazz,
				field.getFormatInput(),
				field.getFormatOutput(),
				sFieldName
		);
	}

	public static InputVerifier getInputVerifier(final Class clazz, String sFormatIn, String sFormtOut, String sFieldName) {
		InputVerifier verifier = null;
		if (clazz.isAssignableFrom(Integer.class) || clazz.isAssignableFrom(Long.class)) {
			verifier = new InputVerifier() {

				@Override
				public boolean verify(JComponent input) {
					boolean throwException = false;
					String sError = "";
					JTextField textField = (JTextField) input;
					try {
						String text = textField.getText();
						if (text.length() == 0) {
							return true;
						}
						Number value = null;
						try {
							if (clazz.isAssignableFrom(Integer.class)) {
								value = Integer.parseInt(text);
							} else {
								value = Long.parseLong(text);
							}
						} catch (NumberFormatException e) {
							Pattern searchPattern = Pattern.compile("(\\d|\\*)+");
							Matcher searchMatcher = searchPattern.matcher(text);
							if (searchMatcher.matches()) {
								return true;
							} else {
								return false;
							}
						}
						if (sFormatIn != null) {
							String formatArray[] = sFormatIn.split(" ");
							boolean invalid = false;
							if (formatArray.length == 2) {
								Number minValue;
								Number maxValue;
								if (clazz.isAssignableFrom(Integer.class)) {
									minValue = Integer.parseInt(formatArray[0]);
									maxValue = Integer.parseInt(formatArray[1]);

									invalid = value.intValue() < minValue.intValue() || value.intValue() > maxValue.intValue();
								} else {
									minValue = Long.parseLong(formatArray[0]);
									maxValue = Long.parseLong(formatArray[1]);

									invalid = value.longValue() < minValue.longValue() || value.longValue() > maxValue.longValue();
								}
								if (invalid) {
									throwException = true;
									sError = SpringLocaleDelegate.getInstance().getMessage("InputVerifier.1", "Validierungsfehler", sFieldName, minValue, maxValue);
									return false;
								}
							} else {
								Number minValue;
								if (clazz.isAssignableFrom(Integer.class)) {
									minValue = Integer.parseInt(formatArray[0]);

									invalid = value.intValue() < minValue.intValue();
								} else {
									minValue = Long.parseLong(formatArray[0]);

									invalid = value.longValue() < minValue.longValue();
								}
								if (invalid) {
									throwException = true;
									sError = SpringLocaleDelegate.getInstance().getMessage("InputVerifier.1", "Validierungsfehler", sFieldName, minValue);
									return false;
								}
							}
						}

						return true;
					} finally {
						if (throwException) {
							Bubble bubble = new Bubble(textField, sError, 3);
							bubble.setVisible(true);
						}
					}
				}
			};
		} else if (clazz.isAssignableFrom(Double.class)) {
			verifier = new InputVerifier() {

				@Override
				public boolean verify(JComponent input) {
					boolean throwException = false;
					String sError = "";
					JTextField textField = (JTextField) input;
					try {
						String text = textField.getText();
						if (text.length() == 0) {
							return true;
						}

						NumberFormat numberFormat = NumberFormat.getInstance(SpringLocaleDelegate.getInstance().getLocale());
						double iValue = 0;
						try {
							iValue = numberFormat.parse(text).doubleValue();
						} catch (ParseException e) {
							Pattern searchPattern = Pattern.compile("(\\d|\\*|\\.|,)+");
							Matcher searchMatcher = searchPattern.matcher(text);
							if (searchMatcher.matches()) {
								return true;
							} else {
								return false;
							}

						}
						if (sFormatIn != null) {
							String formatArray[] = sFormatIn.split(" ");
							if (formatArray.length == 2) {
								Double iMinValue = Double.parseDouble(formatArray[0]);
								Double iMaxValue = Double.parseDouble(formatArray[1]);
								if (iValue < iMinValue || iValue > iMaxValue) {
									throwException = true;
									sError = SpringLocaleDelegate.getInstance().getMessage("InputVerifier.1", "Validierungsfehler", sFieldName, iMinValue, iMaxValue);
									return false;
								}
							} else {
								Double iMinValue = Double.parseDouble(formatArray[0]);
								if (iValue < iMinValue) {
									throwException = true;
									sError = SpringLocaleDelegate.getInstance().getMessage("InputVerifier.1", "Validierungsfehler", sFieldName, iMinValue);
									return false;
								}
							}
						}

						return true;
					} finally {
						if (throwException) {
							Bubble bubble = new Bubble(textField, sError, 3);
							bubble.setVisible(true);
						}
					}
				}
			};
		} else if (clazz.isAssignableFrom(Date.class)) {
			verifier = new InputVerifier() {

				@Override
				public boolean verify(JComponent input) {
					boolean throwException = false;
					String sError = "";
					JTextField textField = (JTextField) input;

					try {
						String text = textField.getText();
						if (text.length() == 0) {
							return true;
						}
						final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
						String sSpecialFormat = localeDelegate.getMessage("datechooser.today.label", "Heute");
						if (sSpecialFormat.toLowerCase().equals(text.toLowerCase())) {
							return true;
						}

						Date dValue = null;
						if (sFormtOut != null) {
							dValue = new SimpleDateFormat(sFormtOut).parse(text);
						} else {
							dValue = SpringLocaleDelegate.getInstance().parseDate(text, false);
						}

						if (sFormatIn != null) {
							String formatArray[] = sFormatIn.split(" ");
							if (formatArray.length == 2) {
								Date dMinValue = new Date(Long.parseLong(formatArray[0]));
								Date dMaxValue = new Date(Long.parseLong(formatArray[1]));
								if (dValue.before(dMinValue) || dValue.after(dMaxValue)) {
									throwException = true;
									String error1 = SpringLocaleDelegate.getInstance().formatDate(dMinValue);
									String error2 = SpringLocaleDelegate.getInstance().formatDate(dMaxValue);
									sError = SpringLocaleDelegate.getInstance().getMessage("InputVerifier.1", "Validierungsfehler", sFieldName, error2, error2);
									return false;
								}
							} else {
								Date dMinValue = new Date(Long.parseLong(formatArray[0]));
								if (dValue.before(dMinValue)) {
									throwException = true;
									String error = SpringLocaleDelegate.getInstance().formatDate(dMinValue);
									sError = SpringLocaleDelegate.getInstance().getMessage("InputVerifier.1", "Validierungsfehler", sFieldName, error);
									return false;
								}
							}
						}

						return true;
					} catch (ParseException ex) {
						sError = SpringLocaleDelegate.getInstance().getMessage("InputVerifier.2", "Bitte geben Sie ein gültiges Datum ein!");
						throwException = true;
						return false;
					} finally {
						if (throwException) {
							Bubble bubble = new Bubble(textField, sError, 3);
							bubble.setVisible(true);
						}
					}
				}
			};
		}
		return verifier;
	}

	public static InputVerifier getInputVerifier(final CollectableEntityField field, int type) {
		InputVerifier verifier = null;
		Class clazz = field.getJavaClass();
		if(clazz.isAssignableFrom(String.class) && type == CollectableComponentTypes.TYPE_PHONENUMBER) {
			
		} else 
		if(clazz.isAssignableFrom(String.class) && type == CollectableComponentTypes.TYPE_EMAIL) {
			verifier = new InputVerifier() {
				
				@Override
				public boolean verify(JComponent input) {
					boolean throwException = false;
					String sError = "";
					JTextField textField = (JTextField)input;
					try {
						String value = textField.getText();
						if(value.length() == 0) return true;
						InternetAddress adress = new InternetAddress(value);
						adress.validate();
						return true;
					}
					catch(AddressException ex) {
						sError = SpringLocaleDelegate.getInstance().getMessage("InputVerifier.3", "Bitte geben Sie ein gültige EMail ein!");
						throwException = true;
						return false;
					}
					finally {
						if(throwException) {
							Bubble bubble = new Bubble(textField, sError, 3);
							bubble.setVisible(true);
						}
					}
				}
			};
		}
		else if(clazz.isAssignableFrom(String.class) && type == CollectableComponentTypes.TYPE_HYPERLINK) {
			verifier = new InputVerifier() {
				
				@Override
				public boolean verify(JComponent input) {
					if (input instanceof HyperlinkRefField) {
						return true;
					}

					boolean throwException = false;
					String sError = "";
					JTextField textField = (JTextField)input;
					try {
						String value = textField.getText();
						if(value.length() == 0) return true;
						if(!LangUtils.isValidURI(value)){
							sError = SpringLocaleDelegate.getInstance().getMessage("InputVerifier.4", "Bitte geben Sie ein gültige Internetadresse ein!");
							throwException = true;
							return false;
						}
						
						return true;
					}
					finally {
						if(throwException) {
							Bubble bubble = new Bubble(textField, sError, 3);
							bubble.setVisible(true);
						}
					}
				}
			};
		}
		
		return verifier;
	}

}
