package org.nuclos.client.ui;

public interface MovableFlag extends Comparable<Object> {
	boolean isMovable();
	Object getItem();
}
