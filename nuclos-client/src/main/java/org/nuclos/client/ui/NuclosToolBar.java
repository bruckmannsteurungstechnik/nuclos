//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui;

import static org.nuclos.client.theme.NuclosThemeSettings.TOOLBAR_BUTTON_SIZE;
import static org.nuclos.client.theme.NuclosThemeSettings.TOOLBAR_SEPARATOR_H_MINIMUM;
import static org.nuclos.client.theme.NuclosThemeSettings.TOOLBAR_SEPARATOR_V_MINIMUM;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Closeable;
import java.util.Map;
import java.util.WeakHashMap;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;

import org.apache.http.util.LangUtils;
import org.nuclos.client.ui.collect.toolbar.ToolBarComponentConfigurator;
import org.nuclos.client.ui.collect.toolbar.ToolBarItemAction;
import org.nuclos.common.collect.ToolBarItem;
import org.nuclos.common.collect.ToolBarItemGroup;
import org.nuclos.common2.StringUtils;

public class NuclosToolBar extends JToolBar implements Closeable {
	
	private static class ActionButton extends JButton {
		
		private ActionButton(Action action) {
			super(action);
		}
		
		@Override
		public boolean isDefaultButton() {
			return false; // so focus isn´t painted anymore. but action for the default button works. 
		}
		
	}
	
	private static class ActionItemListener implements ItemListener {
		
		private final JComboBox combo;
		
		private final Action action;
		
		private ActionItemListener(JComboBox combo, Action action) {
			if (combo == null || action == null) {
				throw new NullPointerException();
			}
			this.combo = combo;
			this.action = action;
		}
		
		@Override
		public void itemStateChanged(ItemEvent e) {
			if (e.getStateChange() == ItemEvent.SELECTED) {
				if (!Boolean.TRUE.equals(action.getValue(ToolBarItemAction.ACTION_DISABLED))) {
					action.actionPerformed(new ActionEvent(this, combo.getSelectedIndex(), "selected"));
				}
			}
		}

	}
	
	private static class PropertyChangeListener1 implements PropertyChangeListener {
		
		private final JComboBox combo;
		
		private final Action action;
		
		private final ColoredLabel label;
		
		PropertyChangeListener1(JComboBox combo, Action action, ColoredLabel label) {
			if (combo == null || action == null) {
				throw new NullPointerException();
			}
			this.combo = combo;
			this.action = action;
			this.label = label;
		}
		
		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			if (ToolBarItemAction.ACTION_DISABLED.equals(evt.getPropertyName())) {
				if (Boolean.TRUE.equals(evt.getNewValue())) {
					combo.setEnabled(false);
				} else {
					combo.setEnabled(true);
				}
			} else if (ToolBarItemAction.ITEM_SELECTED_INDEX.equals(evt.getPropertyName())) {
				final Integer newIndex = (Integer) evt.getNewValue();
				if (newIndex == null) {
					if (combo.getItemCount() >= 1) {
						combo.setSelectedIndex(0);										
					}
				} else {
					final int len = combo.getItemCount();
					if (newIndex >= 0 && newIndex < len) {
						combo.setSelectedIndex(newIndex);
					}
				}
			} else if (ToolBarItemAction.ITEM_ARRAY.equals(evt.getPropertyName())) {
				Object[] items = (Object[]) evt.getNewValue();
				combo.setModel(items==null ? new DefaultComboBoxModel() : new DefaultComboBoxModel(items));
				Integer newIndex = (Integer) action.getValue(ToolBarItemAction.ITEM_SELECTED_INDEX);
				if (!LangUtils.equals(newIndex, combo.getSelectedIndex()) && combo.getItemCount() > 0) {
					if (newIndex == null) {
						if (combo.getItemCount() >= 1) {
							combo.setSelectedIndex(0);
						}
					} else {
						final int len = combo.getItemCount();
						if (newIndex >= 0 && newIndex < len) {
							combo.setSelectedIndex(newIndex);
						}
					}
				}
			} else if (ToolBarItemAction.MENU_LABEL_COLOR.equals(evt.getPropertyName())) {
				Color color = (Color) action.getValue(ToolBarItemAction.MENU_LABEL_COLOR);
				if (label != null) {
					label.setColoredBackground(color);
				}
			}
		}
	}
	
	private static class PropertyChangeListener2 implements PropertyChangeListener {
		
		private final JComponent[] result;

		
		PropertyChangeListener2(JComponent[] result) {
			if (result == null) {
				throw new NullPointerException();
			}
			this.result = result;
		}
		
		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			if (ToolBarItemAction.VISIBLE.equals(evt.getPropertyName())) {
				for (JComponent c : result) {
					if (Boolean.FALSE.equals(evt.getNewValue())) {
						c.setVisible(false);
					} else {
						c.setVisible(true);
					}
				}
			}
		}
	}
	
	private static class PropertyChangeListenerButton implements PropertyChangeListener {
		
		private final AbstractButton btn;
		
		public PropertyChangeListenerButton(AbstractButton btn) {
			this.btn = btn;
		}

		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			if (Action.NAME.equals(evt.getPropertyName())) {
				Object oHideActionText = btn.getClientProperty("hideActionText");
				boolean hideActionText = oHideActionText == null || Boolean.TRUE.equals(btn.getClientProperty("hideActionText"));
				if (!hideActionText) {
					final Object newValue = evt.getNewValue();
					btn.setText(newValue==null?null:newValue.toString());
				}
			} else if (ToolBarItemAction.VISIBLE.equals(evt.getPropertyName())) {
				final Object newValue = evt.getNewValue();
				final boolean visible;
				if (newValue instanceof String) {
					visible = Boolean.TRUE.toString().equalsIgnoreCase(newValue.toString());
				} else if (newValue instanceof Boolean) {
					visible = Boolean.TRUE.equals(newValue);
				} else {
					visible = false;
				}
				btn.setVisible(visible);
			}
		}
		
	}
	
	private ToolBarComponentConfigurator configurator;
	
	private Map<ToolBarItem, JComponent[]> mpParentComps;

	private Color gradientColor;

	private Color gradientColor2;

	public NuclosToolBar() {
		super();
		mpParentComps = new WeakHashMap<ToolBarItem, JComponent[]>();
		setLayout(new WrapLayout(WrapLayout.LEFT));
		init();
	}
	
	public NuclosToolBar(int orientation) {
		super();
		mpParentComps = new WeakHashMap<ToolBarItem, JComponent[]>();
		setLayout(orientation==JToolBar.HORIZONTAL? new WrapLayout(WrapLayout.LEFT) : new BoxLayout(this, BoxLayout.Y_AXIS));
		init();
	}
	
	public NuclosToolBar(int orientation, int wrapLayout) {
		super();
		mpParentComps = new WeakHashMap<ToolBarItem, JComponent[]>();
		setLayout(orientation==JToolBar.HORIZONTAL? new WrapLayout(wrapLayout) : new BoxLayout(this, BoxLayout.Y_AXIS));
		init();
	}
	
	private void init() {
		setMinimumSize(new Dimension(4, 4));
		configurator = new ToolBarComponentConfigurator(this) {
			
			@Override
			protected JComponent[] createStatic(ToolBarItem item) {
				switch (item.getType()) {
				case SEPARATOR:
					return new JComponent[] {new JSeparator()};
				default:
					return null;
				}
			}
			
			@Override
			protected JComponent[] create(ToolBarItem item, final Action action) {
				final JComponent[] result;
				switch (item.getType()) {
				case ACTION:
					result = new JComponent[] { new ActionButton(action) };
					break;
				case TOGGLE_ACTION:
					result = new JComponent[] {new JToggleButton(action)};
					break;
				case CHECKBOX:
					result = new JComponent[] {new JCheckBox(action)};
					break;
				case RADIOBUTTON:
					result = new JComponent[] {new JRadioButton(action)};
					break;
				case PR_ACTION:
				case PSD_ACTION:
				case PSR_ACTION:
					Object[] items = (Object[]) action.getValue(ToolBarItemAction.ITEM_ARRAY);
					Integer iSelected = (Integer) action.getValue(ToolBarItemAction.ITEM_SELECTED_INDEX);
					final JComboBox combo = items==null ? new JComboBox() : new JComboBox(items);
					if (iSelected != null) {
						combo.setSelectedIndex(iSelected);
					}
					
					combo.putClientProperty("JComboBox.isTableCellEditor", Boolean.TRUE); //so we can use the arrow keys to navigate.
					combo.addItemListener(new ActionItemListener(combo, action));
					
					final ColoredLabel label;
					String sMenuLabel = (String) action.getValue(ToolBarItemAction.MENU_LABEL);
					if (!StringUtils.looksEmpty(sMenuLabel)) {
						label = new ColoredLabel(combo, sMenuLabel);
					} else {
						label = null;
					}
					action.addPropertyChangeListener(new PropertyChangeListener1(combo, action, label));
					
					result = new JComponent[] {label==null ? combo : label};
					break;
				default:
					result = null;
				}
				if (result != null && 
						(action.getValue(Action.SMALL_ICON) != null || 
						 action.getValue(Action.LARGE_ICON_KEY) != null)) {
					switch (item.getType()) {
					case ACTION:
					case TOGGLE_ACTION:
						if (action.getValue(Action.SHORT_DESCRIPTION) == null && action.getValue(Action.NAME) != null) {
							result[0].setToolTipText((String) action.getValue(Action.NAME));
						}
						result[0].setFocusable(false);
						break;
					default:
						break;
					}
				}
				if (result != null) {
					for (JComponent c : result) {
						if (Boolean.FALSE.equals(action.getValue(ToolBarItemAction.VISIBLE))) {
							c.setVisible(false);
						} else {
							c.setVisible(true);
						}
						if (c instanceof AbstractButton) {
							final AbstractButton btn = (AbstractButton)result[0];
							action.addPropertyChangeListener(new PropertyChangeListenerButton(btn));
						}
					}
					action.addPropertyChangeListener(new PropertyChangeListener2(result));	
				}
				mpParentComps.put(item, result);
				return result;
			}

			@Override
			protected boolean isHideLabelPossible() {
				return true;
			}
		};
	}
	
	public JComponent[] getComponents(ToolBarItem item) {
		return mpParentComps.get(item);
	}
	
	@Override
	public void setVisible(boolean b) {
		super.setVisible(b);
		for (Component c : getComponents()) {
			if (c instanceof RecyclableComponent) {
				((RecyclableComponent)c).parentVisible(b);
			}
		}
	}
	
	@Override
	protected void addImpl(Component comp, Object constraints, int index) {
		boolean registered = false;
		if (comp instanceof JComponent) {
			registered = configurator.registerComponent((JComponent) comp);
		}
		if (!registered) {
			super.addImpl(ensureComponentDefaultSize(comp), constraints, index);
			if (getLayout() instanceof BoxLayout && comp instanceof JComponent) {
				((JComponent)comp).setAlignmentX(Component.LEFT_ALIGNMENT);
			}
		}
	}
	
	public void registerAction(ToolBarItem item, Action action) {
		configurator.registerAction(item, action);
	}
	
	/**
	 * Sets the group for this tool bar.
	 * @param toolBarGroup
	 */
	public void setToolBarItemGroup(ToolBarItemGroup toolBarGroup) {
		configurator.setup(toolBarGroup);
	}
	
	@Override
	public void close() {
		if (configurator != null) {
			configurator.close();
		}
		configurator = null;
		if (mpParentComps != null) {
			mpParentComps.clear();
		}
		mpParentComps = null;
		removeAll();
	}
	
	@Override
	protected void finalize() throws Throwable {
		close();
		super.finalize();
	}

	@Override
	public Dimension getMinimumSize() {
		return new Dimension(super.getMinimumSize().width, super.getPreferredSize().height);
	}

	/**
	 * Buttons are scaled to big during painting when there is no preferred size set.
	 * Maybe this is a bug in the UI.
	 *  
	 * @param c
	 * @return
	 */
	private Component ensureComponentDefaultSize(Component c) {
		if (c instanceof JButton) {
			JButton btn = (JButton) c;
			if (StringUtils.looksEmpty(btn.getText())) {
				// set only if it is not a text button
				btn.setPreferredSize(TOOLBAR_BUTTON_SIZE);
			}
		} else if (c instanceof JToolBar.Separator) {
			JToolBar.Separator sep = (JToolBar.Separator) c;
			Dimension ensured = ensureSeparatorMinimumSize(sep.getSeparatorSize());
			if (!ensured.equals(sep.getSeparatorSize())) {
				((JToolBar.Separator) c).setSeparatorSize(ensured);
			}
		}
		return c;
	}

	/**
	 * Separators are scaled to small during painting if we don't set this minimum size.
	 * Maybe this is a bug in the UI.
	 * 
	 * @param size
	 * @return
	 */
	private Dimension ensureSeparatorMinimumSize(Dimension size) {
		if (getOrientation() == HORIZONTAL) {
			if (size == null)
				return TOOLBAR_SEPARATOR_H_MINIMUM;
			else {
				return new Dimension(size.width < TOOLBAR_SEPARATOR_H_MINIMUM.width   ? TOOLBAR_SEPARATOR_H_MINIMUM.width  : size.width,
									 size.height < TOOLBAR_SEPARATOR_H_MINIMUM.height ? TOOLBAR_SEPARATOR_H_MINIMUM.height : size.height);
			}
		} else {
			if (size == null)
				return TOOLBAR_SEPARATOR_V_MINIMUM;
			else {
				return new Dimension(size.width < TOOLBAR_SEPARATOR_V_MINIMUM.width   ? TOOLBAR_SEPARATOR_V_MINIMUM.width  : size.width,
					 				 size.height < TOOLBAR_SEPARATOR_V_MINIMUM.height ? TOOLBAR_SEPARATOR_V_MINIMUM.height : size.height);
			}
		}
	}

	public void setGradientColor(Color color) {
		this.gradientColor = color;
		this.gradientColor2 = color;
		if (color != null) {
			this.gradientColor2 = new Color(color.getRed(), color.getGreen(), color.getBlue(), 0);
		}
		this.repaint();
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		if (gradientColor != null) {
			Graphics2D g2 = (Graphics2D) g;
			g2.setPaint(new GradientPaint(0, 0, gradientColor, 0, 10, gradientColor2));
			g2.fillRect(0, 0, getWidth(), getHeight());
		}
	}
	
}
