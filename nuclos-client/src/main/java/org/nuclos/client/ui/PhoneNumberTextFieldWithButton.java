//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Window.Type;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JWindow;

import org.apache.log4j.Logger;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.main.MainController;
import org.nuclos.client.ui.labeled.ILabeledComponentSupport;
import org.nuclos.client.ui.util.TableLayoutBuilder;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.security.UserFacadeRemote;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;

import info.clearthought.layout.TableLayout;

public class PhoneNumberTextFieldWithButton extends HyperlinkTextFieldWithButton {

	private static final long serialVersionUID = 1L;
	
	private static final Logger LOG = Logger.getLogger(PhoneNumberTextFieldWithButton.class);
	
	private static final List<Icon> PHONEBUTTONS = new ArrayList<Icon>();
	
	private UID entity;
	
	private Long id;
	
	static {
		PHONEBUTTONS.add(Icons.getInstance().getIconTextFieldButtonHyperlink());
	}
	
	public PhoneNumberTextFieldWithButton(ILabeledComponentSupport support, boolean bSearchable) {
		super(PHONEBUTTONS, support, bSearchable);
	}
	
	public void setEntity(UID entity) {
		this.entity = entity;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Click or Ctrl+Enter
	 */
	@Override
	public void openLink() {
		if (!StringUtils.looksEmpty(getText())) {
			LOG.debug("Dial phone number '" + getText() + "'...");
			showCallDialog();
		}
	}
	
	private void showCallDialog() {
		final JWindow window = new JWindow(MainController.getMainFrame());
		//window.setAlwaysOnTop(true);
		//frame.setResizable(true);
		window.setType(Type.NORMAL);
		
		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		int width = gd.getDisplayMode().getWidth() - 40;
		int height = gd.getDisplayMode().getHeight();
		Rectangle fBounds = new Rectangle(20, height / 2 - 200, width, 400);
		window.setBounds(fBounds);
		
		final JPanel jpnMain = new JPanel(new BorderLayout());
		jpnMain.setBackground(Color.WHITE);
		jpnMain.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
		
		final JLabel jlNumber = new JLabel(getText());
		Font font = new Font(Font.MONOSPACED, Font.PLAIN, 12);
		jlNumber.setFont(resizeFont(font, width - 40));
		
		JButton jbtnCall = new JButton(new AbstractAction(SpringLocaleDelegate.getInstance().getMsg("Dial_number")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					SpringApplicationContextHolder.getBean(UserFacadeRemote.class).requestPhoneCall(entity, id, getText());
				} catch (CommonBusinessException e1) {
					Errors.getInstance().showExceptionDialog(window, e1);
				}
			}
		});
		
		JButton jbtnClose = new JButton(new AbstractAction(SpringLocaleDelegate.getInstance().getMsg("MainFrameTab.1")) {
			@Override
			public void actionPerformed(ActionEvent e) {
				window.dispose();
			}
		});
		
		if (id != null && SecurityCache.getInstance().hasCommunicationAccountPhone()) {
			jpnMain.add(createCenterPanel(jbtnCall), BorderLayout.NORTH);
		}
		jpnMain.add(jlNumber, BorderLayout.CENTER);
		jpnMain.add(createCenterPanel(jbtnClose), BorderLayout.SOUTH);
		
		window.setContentPane(jpnMain);
		window.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				jlNumber.setFont(resizeFont(jlNumber.getFont(), jpnMain.getWidth()));
				window.getContentPane().invalidate();
				window.getContentPane().revalidate();
				window.getContentPane().repaint();
			}
		});
		//frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		window.setVisible(true);
		
	}
	
	private JPanel createCenterPanel(JComponent comp) {
		JPanel result = new JPanel();
		result.setOpaque(false);
		TableLayoutBuilder tbllay = new TableLayoutBuilder(result).columns(TableLayout.FILL, TableLayout.PREFERRED, TableLayout.FILL);
		tbllay.newRow(10.0);
		tbllay.newRow().skip().add(comp);
		tbllay.newRow(10.0);
		return result;
	}
	
	private Font resizeFont(Font font, double width) {
		width = width - 40;
		FontMetrics metrics = getFontMetrics(font);
		float size = font.getSize2D();
		float newSize = size;
		float textWidth = metrics.stringWidth(getText());
		if (width > 0 && textWidth > 0 && size > 0) {
			newSize = (float) Math.floor(width * size / textWidth);
			if (newSize > 0) {
				LOG.debug("new font size " + newSize);
			} else {
				newSize = size;
			}
		}
		return font.deriveFont(newSize);
	}
	
}
