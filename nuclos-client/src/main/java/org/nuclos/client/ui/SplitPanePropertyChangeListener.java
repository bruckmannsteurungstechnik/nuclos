package org.nuclos.client.ui;

import java.awt.Component;
import java.awt.Container;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JSplitPane;

import org.nuclos.client.common.EntityCollectController;
import org.nuclos.client.common.WorkspaceUtils;
import org.nuclos.common.WorkspaceDescription2.LayoutPreferences;
import org.nuclos.common.WorkspaceDescription2.Split;

/**
 * Update workspace split pane preferences
 *  
 * @author Moritz Neuhaeuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public class SplitPanePropertyChangeListener implements PropertyChangeListener {
	final List<JSplitPane> lstSp; 
	final JComponent component;
	final EntityCollectController<?,?> ctl;
	
	public SplitPanePropertyChangeListener(final JComponent component, final EntityCollectController<?,?> ctl) {
		this.lstSp = new ArrayList<JSplitPane>(); 
	    lstSp.addAll(UIUtils.findAllInstancesOf(component, JSplitPane.class));
	    this.ctl = ctl;
	    this.component = component;
	}


	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		final JSplitPane sp = (JSplitPane)evt.getSource();
		final String name = sp.getName();

		final JComponent rootComp = this.component;
		for (JSplitPane pane : lstSp) {
			String pname = pane.getName();
			if(pname == null) {
				// Plan B -> Name wird generiert als Folge von Indices ab rootComp
				StringBuilder sb = new StringBuilder();
				Component c = pane;
				while(c != null && c != rootComp) {
					Container parent = c.getParent();
					if(c instanceof JSplitPane) {
						int idx = -1;
						for(int i = 0, n = parent.getComponentCount(); i < n; i++)
							if(parent.getComponent(i) == c)
								idx = i;
						sb.insert(0, "#" + idx);
					}
					c = parent;
				}
				pname = sb.toString();
			}
			if(pname != null) {
				storePreferences(sp);
			}
		}
		
		// lstSp might be empty
		storePreferences(sp);
	}

	private void storePreferences(JSplitPane sp) {
		String name = sp.getName();
		final LayoutPreferences lp = WorkspaceUtils.getInstance().getLayoutPreferences(WorkspaceUtils.getInstance().getWorkspace(), ctl.getCurrentLayoutUid());
		if (lp != null) {
			Split spp = WorkspaceUtils.getInstance().getSplitPanePreferences(lp, name);
			int expandedState = 0;
			if (sp.getOrientation() == JSplitPane.VERTICAL_SPLIT) {
				if (sp.getDividerLocation() <= 0) {
					expandedState = Split.EXPANDED_STATE_RIGHT;
				} else if (sp.getDividerLocation() + sp.getDividerSize() == sp.getHeight()){
					expandedState = Split.EXPANDED_STATE_LEFT;
				} else {
					expandedState = Split.EXPANDED_STATE_NONE;
				}
			} else {
				if (sp.getDividerLocation() <= 0) {
					expandedState = Split.EXPANDED_STATE_RIGHT;
				} else if (sp.getDividerLocation() + sp.getDividerSize() == sp.getWidth()){
					expandedState = Split.EXPANDED_STATE_LEFT;
				} else {
					expandedState = Split.EXPANDED_STATE_NONE;
				}
			}
			spp.setExpandedState(expandedState);
			spp.setPosition(sp.getDividerLocation());
			spp.setLastDividerLocation(sp.getLastDividerLocation());
		}
	}
}
