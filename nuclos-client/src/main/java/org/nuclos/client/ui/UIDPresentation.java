//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui;

import org.nuclos.common.UID;

/**
 *  simple uid/label pair
 */
public class UIDPresentation {

	final private UID uid;
	final private String label;

	public UIDPresentation(UID uid, String label) {
		super();
		if (uid == null || label == null) {
			throw new IllegalArgumentException();
		}
		this.uid = uid;
		this.label = label;
	}

	public UID getUid() {
		return uid;
	}

	public String getLabel() {
		return label;
	}

	@Override
	public String toString() {
		return label;
	}

	@Override
	public boolean equals(Object arg0) {
		if (this == arg0)
			return true;

		if (arg0 instanceof UIDPresentation)
			return uid.equals(((UIDPresentation) arg0).uid);

		return super.equals(arg0);
	}

	@Override
	public int hashCode() {
		return uid.hashCode();
	}
}
