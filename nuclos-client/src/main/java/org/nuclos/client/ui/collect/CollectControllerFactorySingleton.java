//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect;

import java.util.Collection;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.controller.NuclosCollectControllerCommonState;
import org.nuclos.client.communication.CommunicationPortCollectController;
import org.nuclos.client.customcode.ClientCodeCollectController;
import org.nuclos.client.customcode.ServerCodeCollectController;
import org.nuclos.client.datasource.admin.CalcAttributeCollectController;
import org.nuclos.client.datasource.admin.ChartCollectController;
import org.nuclos.client.datasource.admin.CollectableDataSource;
import org.nuclos.client.datasource.admin.DatasourceCollectController;
import org.nuclos.client.datasource.admin.DynamicEntityCollectController;
import org.nuclos.client.datasource.admin.DynamicTasklistCollectController;
import org.nuclos.client.datasource.admin.RecordGrantCollectController;
import org.nuclos.client.datasource.admin.ValuelistProviderCollectController;
import org.nuclos.client.entityobject.CollectableEOEntityClientProvider;
import org.nuclos.client.fileimport.CsvImportCollectController;
import org.nuclos.client.fileimport.CsvImportStructureCollectController;
import org.nuclos.client.fileimport.XmlFileImportCollectController;
import org.nuclos.client.fileimport.XmlImportStructureCollectController;
import org.nuclos.client.generation.GenerationCollectController;
import org.nuclos.client.genericobject.CollectableGenericObjectWithDependants;
import org.nuclos.client.genericobject.GenericObjectCollectController;
import org.nuclos.client.genericobject.GenericObjectSplitViewCollectController;
import org.nuclos.client.job.JobControlCollectController;
import org.nuclos.client.layout.admin.GenericObjectLayoutCollectController;
import org.nuclos.client.ldap.LdapServerCollectController;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.mandator.MandatorCollectController;
import org.nuclos.client.mandator.MandatorLevelCollectController;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.DbObjectCollectController;
import org.nuclos.client.masterdata.DbSourceCollectController;
import org.nuclos.client.masterdata.MasterDataCollectController;
import org.nuclos.client.masterdata.MasterDataSplitViewCollectController;
import org.nuclos.client.masterdata.NucletCollectController;
import org.nuclos.client.masterdata.NucletIntegrationPointCollectController;
import org.nuclos.client.masterdata.RelationTypeCollectController;
import org.nuclos.client.masterdata.SearchFilterCollectController;
import org.nuclos.client.masterdata.locale.LocaleCollectController;
import org.nuclos.client.masterdata.user.RoleCollectController;
import org.nuclos.client.masterdata.user.UserCollectController;
import org.nuclos.client.parameter.ParameterCollectController;
import org.nuclos.client.printservice.PrintServiceCollectController;
import org.nuclos.client.process.ProcessCollectController;
import org.nuclos.client.relation.EntityRelationShipCollectController;
import org.nuclos.client.relation.EntityRelationshipModel;
import org.nuclos.client.report.admin.ReportCollectController;
import org.nuclos.client.report.admin.ReportExecutionCollectController;
import org.nuclos.client.resource.admin.ResourceCollectController;
import org.nuclos.client.statemodel.admin.CollectableStateModel;
import org.nuclos.client.statemodel.admin.StateModelCollectController;
import org.nuclos.client.transfer.OldExportImportCollectController;
import org.nuclos.client.ui.collect.search.CalcAttributeSearchStrategy;
import org.nuclos.client.ui.collect.search.ChartSearchStrategy;
import org.nuclos.client.ui.collect.search.DatasourceSearchStrategy;
import org.nuclos.client.ui.collect.search.DynamicEntitySearchStrategy;
import org.nuclos.client.ui.collect.search.DynamicTasklistSearchStrategy;
import org.nuclos.client.ui.collect.search.EntityRelationShipSearchStrategy;
import org.nuclos.client.ui.collect.search.GenericObjectViaEntityObjectSearchStrategy;
import org.nuclos.client.ui.collect.search.ISearchStrategy;
import org.nuclos.client.ui.collect.search.LayoutSearchStrategy;
import org.nuclos.client.ui.collect.search.MasterDataSearchStrategy;
import org.nuclos.client.ui.collect.search.RecordGrantSearchStrategy;
import org.nuclos.client.ui.collect.search.ReportExecutionSearchStrategy;
import org.nuclos.client.ui.collect.search.ReportSearchStrategy;
import org.nuclos.client.ui.collect.search.StateModelSearchStrategy;
import org.nuclos.client.ui.collect.search.ValueListProviderSearchStrategy;
import org.nuclos.client.ui.collect.strategy.CompleteCollectableMasterDataStrategy;
import org.nuclos.client.ui.collect.strategy.CompleteCollectableStateModelsStrategy;
import org.nuclos.client.ui.collect.strategy.CompleteGenericObjectsStrategy;
import org.nuclos.client.wizard.DataTypeCollectController;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.ControllerPresentation;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntityProvider;
import org.nuclos.common.entityobject.CollectableEOEntity;
import org.nuclos.common.report.valueobject.CalcAttributeVO;
import org.nuclos.common.report.valueobject.ChartVO;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.DynamicEntityVO;
import org.nuclos.common.report.valueobject.DynamicTasklistVO;
import org.nuclos.common.report.valueobject.RecordGrantVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;

public class CollectControllerFactorySingleton {
	
	private static final CollectControllerFactorySingleton INSTANCE = new CollectControllerFactorySingleton();
	
	//
	
	private CollectControllerFactorySingleton() {
	}
	
	public static CollectControllerFactorySingleton getInstance() {
		return INSTANCE;
	}
	
	// MasterDataCollectController
	
	private <PK,Clct extends Collectable<PK>> void initMasterData(MasterDataCollectController<PK> mdcc) {
		mdcc.getSearchStrategy().setCompleteCollectablesStrategy(new CompleteCollectableMasterDataStrategy<PK>(mdcc));
		mdcc.init();
	}
	
	/**
	 * @deprecated
	 */
	public DataTypeCollectController newDataTypeCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID,CollectableMasterDataWithDependants<UID>> ss = new MasterDataSearchStrategy<UID>();
		final DataTypeCollectController result = new DataTypeCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}

	/**
	 * @deprecated
	 */
	public SearchFilterCollectController newSearchFilterCollectController(MainFrameTab tabIfAny, NuclosCollectControllerCommonState state) {
		final ISearchStrategy<UID,CollectableMasterDataWithDependants<UID>> ss = new MasterDataSearchStrategy<UID>();
		final SearchFilterCollectController result = new SearchFilterCollectController(tabIfAny, state);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}

	/**
	 * @deprecated
	 */
	public <PK> OldExportImportCollectController<PK> newExportImportCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<PK,CollectableMasterDataWithDependants<PK>> ss = new MasterDataSearchStrategy<PK>();
		final OldExportImportCollectController<PK> result = new OldExportImportCollectController<PK>(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}

	/**
	 * @deprecated
	 */
	public ResourceCollectController newResourceCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID,CollectableMasterDataWithDependants<UID>> ss = new MasterDataSearchStrategy<UID>();
		final ResourceCollectController result = new ResourceCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}

	/**
	 * @deprecated
	 */
	public UserCollectController newUserCollectController(MainFrameTab tabIfAny, NuclosCollectControllerCommonState state) {
		final ISearchStrategy<UID,CollectableMasterDataWithDependants<UID>> ss = new MasterDataSearchStrategy<UID>();
		final UserCollectController result = new UserCollectController(tabIfAny, state);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}

	/**
	 * @deprecated
	 */
	public RoleCollectController newRoleCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID,CollectableMasterDataWithDependants<UID>> ss = new MasterDataSearchStrategy<UID>();
		final RoleCollectController result = new RoleCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}

	/**
	 * @deprecated
	 */
	public RelationTypeCollectController newRelationTypeCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID,CollectableMasterDataWithDependants<UID>> ss = new MasterDataSearchStrategy<UID>();
		final RelationTypeCollectController result = new RelationTypeCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}
	
	/**
	 * @deprecated
	 */
	public GenerationCollectController newGenerationCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID,CollectableMasterDataWithDependants<UID>> ss = new MasterDataSearchStrategy<UID>();
		final GenerationCollectController result = new GenerationCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}
	
	/**
	 * @deprecated
	 */
	public CommunicationPortCollectController newCommunicationPortCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID,CollectableMasterDataWithDependants<UID>> ss = new MasterDataSearchStrategy<UID>();
		final CommunicationPortCollectController result = new CommunicationPortCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}

	/**
	 * @deprecated
	 */
	public LdapServerCollectController newLdapServerCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID,CollectableMasterDataWithDependants<UID>> ss = new MasterDataSearchStrategy<UID>();
		final LdapServerCollectController result = new LdapServerCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}

	/**
	 * @deprecated
	 */
	public DbSourceCollectController newDbSourceCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID,CollectableMasterDataWithDependants<UID>> ss = new MasterDataSearchStrategy<UID>();
		final DbSourceCollectController result = new DbSourceCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}

	/**
	 * @deprecated
	 */
	public DbObjectCollectController newDbObjectCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID,CollectableMasterDataWithDependants<UID>> ss = new MasterDataSearchStrategy<UID>();
		final DbObjectCollectController result = new DbObjectCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}

	/**
	 * @deprecated
	 */
	public NucletCollectController newNucletCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID,CollectableMasterDataWithDependants<UID>> ss = new MasterDataSearchStrategy<UID>();
		final NucletCollectController result = new NucletCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}

	/**
	 * @deprecated
	 */
	public LocaleCollectController newLocaleCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID,CollectableMasterDataWithDependants<UID>> ss = new MasterDataSearchStrategy<UID>();
		final LocaleCollectController result = new LocaleCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}

	/**
	 * @deprecated
	 */
	public LocaleCollectController newLocaleCollectController(Collection<String> collresids, MainFrameTab tabIfAny) {
		final ISearchStrategy<UID,CollectableMasterDataWithDependants<UID>> ss = new MasterDataSearchStrategy<UID>();
		final LocaleCollectController result = new LocaleCollectController(collresids, tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}

	/**
	 * @deprecated
	 */
	public JobControlCollectController newJobControlCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID,CollectableMasterDataWithDependants<UID>> ss = new MasterDataSearchStrategy<UID>();
		final JobControlCollectController result = new JobControlCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}

	public CsvImportStructureCollectController newCsvImportStructureCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID,CollectableMasterDataWithDependants<UID>> ss = new MasterDataSearchStrategy<UID>();
		final CsvImportStructureCollectController result = new CsvImportStructureCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}

	public CsvImportCollectController newCsvImportCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID,CollectableMasterDataWithDependants<UID>> ss = new MasterDataSearchStrategy<UID>();
		final CsvImportCollectController result = new CsvImportCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}

	public XmlImportStructureCollectController newXmlImportStructureCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID,CollectableMasterDataWithDependants<UID>> ss = new MasterDataSearchStrategy<UID>();
		final XmlImportStructureCollectController result = new XmlImportStructureCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}

	public XmlFileImportCollectController newXmlFileImportCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID,CollectableMasterDataWithDependants<UID>> ss = new MasterDataSearchStrategy<UID>();
		final XmlFileImportCollectController result = new XmlFileImportCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}

	public ProcessCollectController newProcessCollectController(UID entity, MainFrameTab tabIfAny) {
		final ISearchStrategy<UID, CollectableMasterDataWithDependants<UID>> ss = new MasterDataSearchStrategy();
		final ProcessCollectController result = new ProcessCollectController(entity, tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}
	
	public ServerCodeCollectController newServerCodeCollectController(UID entity, MainFrameTab tabIfAny) {
		final ISearchStrategy<UID, CollectableMasterDataWithDependants<UID>> ss = new MasterDataSearchStrategy();
		final ServerCodeCollectController result = new ServerCodeCollectController(entity, tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}

	public ClientCodeCollectController newClientCodeCollectController(UID entity, MainFrameTab tabIfAny) {
		final ISearchStrategy<UID, CollectableMasterDataWithDependants<UID>> ss = new MasterDataSearchStrategy();
		final ClientCodeCollectController result = new ClientCodeCollectController(entity, tabIfAny ); 
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}
	
	/**
	 * @deprecated
	 */
	public ParameterCollectController newParameterCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID, CollectableMasterDataWithDependants<UID>> ss = new MasterDataSearchStrategy();
		final ParameterCollectController result = new ParameterCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}
	

	/**
	 * @deprecated
	 */
	public ReportExecutionCollectController newReportExecutionCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID,CollectableMasterDataWithDependants<UID>> ss = new ReportExecutionSearchStrategy();
		final ReportExecutionCollectController result = new ReportExecutionCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}

	/**
	 * @deprecated
	 */
	public ReportCollectController newReportCollectController(UID entityUid, MainFrameTab tabIfAny) {
		final ISearchStrategy<UID,CollectableMasterDataWithDependants<UID>> ss = new ReportSearchStrategy(entityUid);
		final ReportCollectController result = new ReportCollectController(entityUid, tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}
	
	/**
	 * @deprecated
	 */
	public <PK> MasterDataCollectController<PK> newMasterDataCollectController(EntityMeta<PK> systemEntityMeta, MainFrameTab tabIfAny, String customUsage) {
		final ISearchStrategy<PK,CollectableMasterDataWithDependants<PK>> ss = new MasterDataSearchStrategy<PK>();
		final MasterDataCollectController<PK> result = new MasterDataCollectController<PK>(systemEntityMeta, tabIfAny, customUsage);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}
	
	/**
	 * @deprecated
	 */
	public <PK> MasterDataCollectController<PK> newMasterDataCollectController(UID entityUid, MainFrameTab tabIfAny, String customUsage) {
		final ISearchStrategy<PK,CollectableMasterDataWithDependants<PK>> ss = new MasterDataSearchStrategy<PK>();
		final MasterDataCollectController<PK> result = new MasterDataCollectController<PK>(entityUid, tabIfAny, customUsage);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}
	
	/**
	 * @deprecated
	 */
	public <PK2> MasterDataCollectController<PK2> newMasterDataCollectController(UID entityUid, MainFrameTab tabIfAny, String customUsage, ControllerPresentation viewMode) {
		final ISearchStrategy<PK2,CollectableMasterDataWithDependants<PK2>> ss = new MasterDataSearchStrategy<PK2>();
		final MasterDataCollectController<PK2> result = new MasterDataCollectController<PK2>(entityUid, tabIfAny, customUsage, viewMode);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}
	
	public <PK2> MasterDataSplitViewCollectController<PK2> newMasterDataCollectControllerSplitView(UID entityUid, MainFrameTab tabIfAny, String customUsage) {
		final ISearchStrategy<PK2,CollectableMasterDataWithDependants<PK2>> ss = new MasterDataSearchStrategy<PK2>();
		final MasterDataSplitViewCollectController<PK2> result = new MasterDataSplitViewCollectController<PK2>(entityUid, tabIfAny, customUsage);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		initMasterData(result);
		return result;
	}
	
	
	// End of MasterDataCollectController
	
	// GenericObjectCollectController
	
	/**
	 * @deprecated
	 */
	public GenericObjectCollectController newGenericObjectCollectController(UID moduleUid, boolean bAutoInit, MainFrameTab tabIfAny, String customUsage) {
		UID processId = null;
		return newGenericObjectCollectController(moduleUid, processId, bAutoInit, tabIfAny, customUsage);
	}
	
	public GenericObjectCollectController newGenericObjectCollectController(UID moduleUid, UID processId, boolean bAutoInit, MainFrameTab tabIfAny, String customUsage) {
		final ISearchStrategy<Long,CollectableGenericObjectWithDependants> ss;
		final EntityMeta<?> mdvo = MetaProvider.getInstance().getEntity(moduleUid);
		final CollectableEntityProvider cep = CollectableEOEntityClientProvider.getInstance();
		ss = new GenericObjectViaEntityObjectSearchStrategy((CollectableEOEntity) cep.getCollectableEntity(mdvo.getUID()));
		final GenericObjectCollectController result = new GenericObjectCollectController(moduleUid, false, tabIfAny, customUsage);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		ss.setCompleteCollectablesStrategy(new CompleteGenericObjectsStrategy());
		result.setProcessId(processId);
		result.setCalledByProcessMenu(processId != null);
		if (bAutoInit) {
			result.init();
		}
		return result;
	}

	/**
	 * @deprecated
	 */
	public GenericObjectCollectController newGenericObjectCollectController(UID moduleUid, boolean bAutoInit, MainFrameTab tabIfAny, String customUsage, ControllerPresentation viewmode) {
		UID processId = null;
		return newGenericObjectCollectController(moduleUid, processId, bAutoInit, tabIfAny, customUsage, viewmode);
	}

	public GenericObjectCollectController newGenericObjectCollectController(UID moduleUid, UID processId, boolean bAutoInit, MainFrameTab tabIfAny, String customUsage, ControllerPresentation viewmode) {
		final ISearchStrategy<Long,CollectableGenericObjectWithDependants> ss;
		final CollectableEntityProvider cep = CollectableEOEntityClientProvider.getInstance();
		ss = new GenericObjectViaEntityObjectSearchStrategy((CollectableEOEntity) cep.getCollectableEntity(moduleUid));
		final GenericObjectCollectController result = new GenericObjectCollectController(moduleUid, false, tabIfAny, customUsage, viewmode);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		ss.setCompleteCollectablesStrategy(new CompleteGenericObjectsStrategy());
		result.setProcessId(processId);
		if (bAutoInit) {
			result.init();
		}
		return result;
	}
	
	/**
	 * @deprecated
	 */
	public GenericObjectSplitViewCollectController newGenericObjectCollectControllerSplitView(UID moduleUid, boolean bAutoInit, MainFrameTab tabIfAny, String customUsage) {
		final ISearchStrategy<Long,CollectableGenericObjectWithDependants> ss;
		final CollectableEntityProvider cep = CollectableEOEntityClientProvider.getInstance();
		ss = new GenericObjectViaEntityObjectSearchStrategy((CollectableEOEntity) cep.getCollectableEntity(moduleUid));
		final GenericObjectSplitViewCollectController result = new GenericObjectSplitViewCollectController(moduleUid, false, tabIfAny, customUsage);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		ss.setCompleteCollectablesStrategy(new CompleteGenericObjectsStrategy());
		if (bAutoInit) {
			result.init();
		}
		return result;
	}

	
	// LayoutCollectController
	
	public GenericObjectLayoutCollectController newGenericObjectLayoutCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID,CollectableMasterDataWithDependants<UID>> ss = new LayoutSearchStrategy(); 
		final GenericObjectLayoutCollectController result = new GenericObjectLayoutCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		result.init();
		return result;
	}
	
	// AbstractDatasourceCollectController
	
	public ValuelistProviderCollectController newValuelistProviderCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID,CollectableDataSource<ValuelistProviderVO>> ss = new ValueListProviderSearchStrategy(); 
		final ValuelistProviderCollectController result = new ValuelistProviderCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		result.init();
		return result;
	}
	
	public DatasourceCollectController newDatasourceCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID,CollectableDataSource<DatasourceVO>> ss = new DatasourceSearchStrategy(); 
		final DatasourceCollectController result = new DatasourceCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		result.init();
		return result;
	}
	
	public DynamicEntityCollectController newDynamicEntityCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID,CollectableDataSource<DynamicEntityVO>> ss = new DynamicEntitySearchStrategy(); 
		final DynamicEntityCollectController result = new DynamicEntityCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		result.init();
		return result;
	}
	
	public RecordGrantCollectController newRecordGrantCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID,CollectableDataSource<RecordGrantVO>> ss = new RecordGrantSearchStrategy(); 
		final RecordGrantCollectController result = new RecordGrantCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		result.init();
		return result;
	}
	
	public DynamicTasklistCollectController newDynamicTasklistCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID,CollectableDataSource<DynamicTasklistVO>> ss = new DynamicTasklistSearchStrategy();
		final DynamicTasklistCollectController result = new DynamicTasklistCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		result.init();
		return result;
	}
	
	public ChartCollectController newChartCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID,CollectableDataSource<ChartVO>> ss = new ChartSearchStrategy();
		final ChartCollectController result = new ChartCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		result.init();
		return result;
	}
	
	public CalcAttributeCollectController newCalcAttributeCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID,CollectableDataSource<CalcAttributeVO>> ss = new CalcAttributeSearchStrategy(); 
		final CalcAttributeCollectController result = new CalcAttributeCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		result.init();
		return result;
	}
	
	// EntityRelationShipCollectController
	
	/**
	 * @deprecated
	 */
	public EntityRelationShipCollectController newEntityRelationShipCollectController(MainFrame mf, MainFrameTab tabIfAny, NuclosCollectControllerCommonState state) {
		final ISearchStrategy<UID,EntityRelationshipModel> ss = new EntityRelationShipSearchStrategy();
		final EntityRelationShipCollectController result = new EntityRelationShipCollectController(mf, tabIfAny, state);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		result.init();
		return result;
	}

	
	// End of EntityRelationShipCollectController
	
	/**
	 * @deprecated
	 */
	public StateModelCollectController newStateModelCollectController(MainFrameTab tabIfAny, NuclosCollectControllerCommonState state) {
		final ISearchStrategy<UID,CollectableStateModel> ss = new StateModelSearchStrategy();
		final StateModelCollectController result = new StateModelCollectController(tabIfAny, state);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		ss.setCompleteCollectablesStrategy(new CompleteCollectableStateModelsStrategy(result));
		result.init();
		return result;
	}
	
	public MandatorCollectController newMandatorCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID, CollectableMasterDataWithDependants<UID>> ss = new MasterDataSearchStrategy();
		final MandatorCollectController result = new MandatorCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		result.init();
		return result;
	}
	
	public MandatorLevelCollectController newMandatorLevelCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID, CollectableMasterDataWithDependants<UID>> ss = new MasterDataSearchStrategy();
		final MandatorLevelCollectController result = new MandatorLevelCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		result.init();
		return result;
	}
	
	public PrintServiceCollectController newPrintServiceCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID, CollectableMasterDataWithDependants<UID>> ss = new MasterDataSearchStrategy();
		final PrintServiceCollectController result = new PrintServiceCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		result.init();
		return result;
	}

	public NucletIntegrationPointCollectController newNucletIntegrationPointCollectController(MainFrameTab tabIfAny) {
		final ISearchStrategy<UID, CollectableMasterDataWithDependants<UID>> ss = new MasterDataSearchStrategy();
		final NucletIntegrationPointCollectController result = new NucletIntegrationPointCollectController(tabIfAny);
		ss.setCollectController(result);
		result.setSearchStrategy(ss);
		result.init();
		return result;
	}

}
