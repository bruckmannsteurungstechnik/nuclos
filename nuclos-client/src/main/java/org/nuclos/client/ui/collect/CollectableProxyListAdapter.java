package org.nuclos.client.ui.collect;

import java.util.ArrayList;
import java.util.Collection;

import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.dal.vo.HasPrimaryKey;
import org.nuclos.server.genericobject.ProxyList;

public abstract class CollectableProxyListAdapter<T,VO extends HasPrimaryKey<T>, Clct extends Collectable<T>> 
	extends CollectableListAdapter<T,VO, Clct> 
	implements ProxyList<T,Clct> {

	protected CollectableProxyListAdapter(ProxyList<T,VO> lstvoAdaptee) {
		super(lstvoAdaptee);
	}

	protected ProxyList<T,VO> plAdaptee() {
		return (ProxyList<T,VO>) adaptee();
	}

	@Override
	public void fetchDataIfNecessary(int iIndex, ProxyList.IChangeListener changelistener) {
		plAdaptee().fetchDataIfNecessary(iIndex, changelistener);
	}

	@Override
	public int getLastIndexRead() {
		return plAdaptee().getLastIndexRead();
	}

	@Override
	public boolean hasObjectBeenReadForIndex(int index) {
		return plAdaptee().hasObjectBeenReadForIndex(index);
	}

	@Override
	public int getIndexById(T oId) {
		return plAdaptee().getIndexById(oId);
	}

	@Override
	public Clct getRaw(int index) {
		return makeCollectable(plAdaptee().getRaw(index));
	}

	@Override
	public Clct getRawById(Object id) {
		return makeCollectable(plAdaptee().getRawById((T) id));
	}

	@Override
	public int indexOf(Clct element) {
		return plAdaptee().indexOf(extractAdaptee(element));
	}

	@Override
	public T getIdByIndex(int index) {
		return plAdaptee().getIdByIndex(index);
	}

	@Override
	public Collection<T> getAllLoadedIds() {
		return (Collection<T>) plAdaptee().getAllLoadedIds();
	}
	
	@Override
	public boolean isHugeList() {
		return plAdaptee().isHugeList();
	}
	
	@Override
	public int totalSize(boolean forceCount, final Long limit) {
		return plAdaptee().totalSize(forceCount, limit);
	}
	
	@Override
	public Collection<Clct> getBulk(int[] indizes) {
		Collection<Clct> res = new ArrayList<Clct>();
		for (VO vo : plAdaptee().getBulk(indizes)) {
			res.add(makeCollectable(vo));
		}
		return res;
	}

}
