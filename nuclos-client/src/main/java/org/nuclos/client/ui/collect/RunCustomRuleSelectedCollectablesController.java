//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.ui.layer.LockingResultListener;
import org.nuclos.client.ui.multiaction.MultiCollectablesActionController;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;

/**
 * Controller for executing custom rules on multiple <code>Collectable</code>s.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
public class RunCustomRuleSelectedCollectablesController<PK,Clct extends Collectable<PK>>
		extends MultiCollectablesActionController<PK,Clct, Object> {

	private static class CustomRuleAction<PK,Clct extends Collectable<PK>> extends AbstractUpdateAction<PK, Clct, Clct> {

		private final List<EventSupportSourceVO> lstRuleVO;
		private final LockingResultListener<Exception> exResult;

		CustomRuleAction(CollectController<PK,Clct> ctl, List<EventSupportSourceVO> lstRuleVO, LockingResultListener<Exception> exResult) throws CommonBusinessException {
			super(ctl);
			this.lstRuleVO = lstRuleVO;
			this.exResult = exResult;
		}

		@Override
		public Object perform(Clct clct, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {

			if (!clct.isComplete()) {
				clct = ctl.readCollectable(clct, 0);
			}

			if (!changedFields.isEmpty()) {
				for (Map.Entry<UID, CollectableField> e : changedFields.entrySet()) {
					if (e.getValue().getValue() instanceof GenericObjectDocumentFile) {
						clct.setField(e.getKey(), prepareFieldForMultiUpdate(e));
					}
				}
			}

			ctl.executeBusinessRules(this.lstRuleVO, clct, applyMultiEditContext);
			return null;
		}

		@Override
		public String getText(Clct clct) {
			return SpringLocaleDelegate.getInstance().getMessage(
					"RunCustomRuleSelectedCollectablesController.1","Benutzerregel wird für {0} ausgef\u00fchrt...",
					SpringLocaleDelegate.getInstance().getIdentifierLabel(clct, ctl.getEntityUid(), MetaProvider.getInstance()));
		}

		@Override
		public String getSuccessfulMessage(Clct clct, Object oResult) {
			return SpringLocaleDelegate.getInstance().getMessage(
					"RunCustomRuleSelectedCollectablesController.2","Benutzerregel für {0} erfolgreich ausgef\u00fchrt.",
					SpringLocaleDelegate.getInstance().getIdentifierLabel(clct, ctl.getEntityUid(), MetaProvider.getInstance()));
		}

		@Override
		public String getConfirmStopMessage() {
			return SpringLocaleDelegate.getInstance().getMessage(
					"RunCustomRuleSelectedCollectablesController.3","Wollen Sie das Ausführen an dieser Stelle beenden?\n(Bisher veränderte Datensätze bleiben bestehen.)");
		}

		@Override
		public String getExceptionMessage(Clct clct, Exception ex) {
			return SpringLocaleDelegate.getInstance().getMessage(
					"RunCustomRuleSelectedCollectablesController.4","Relgeausführung für Datensatz {0} ist fehlgeschlagen.", 
					SpringLocaleDelegate.getInstance().getIdentifierLabel(clct, ctl.getEntityUid(), MetaProvider.getInstance())) + ex.getMessage();
		}

		@Override
		public void executeFinalAction() throws CommonBusinessException {
			if (exResult != null) {
				exResult.done(null);
			} else {
				// NUCLOSINT-884 refresh afterwards
				ctl.getResultController().getSearchResultStrategy().refreshResult();
			}
			ctl.setDetailsChangedIgnored(false);
		}
	}

	public RunCustomRuleSelectedCollectablesController(CollectController<PK,Clct> ctl, List<EventSupportSourceVO> lstRuleVO, LockingResultListener<Exception> exResult) throws CommonBusinessException {
		super(ctl, SpringLocaleDelegate.getInstance().getMessage(
				"RunCustomRuleSelectedCollectablesController.5","Regel ausführen"), new CustomRuleAction<PK,Clct>(ctl, lstRuleVO, exResult),
				ctl.getSelectedCollectables());
	}

}  // class RunCustomRuleSelectedCollectablesController
