package org.nuclos.client.ui.collect;

import org.nuclos.client.ui.collect.component.model.CollectableLocalizedComponentModel;

public interface SubControllerLocalisationLookUp {
	void initLocalizationComponentModel(CollectableLocalizedComponentModel model);
}
