//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang.ObjectUtils;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.multiaction.MultiCollectablesActionController;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller for updating multiple (selected) <code>Collectable</code>s.
 * 
 * {@link UpdateSelectedCollectablesController} considering lazy loading of collectables
 * 
 * @author Moritz Neuhaeuser <moritz.neuhaeuser@nuclos.de>
 *
 * @param <PK>	primary key
 */
public class UpdateSelectedIdsController<PK,C extends Collectable<PK>, Ctl extends CollectController<PK,C>> extends MultiCollectablesActionController<PK,PK, Object> {
	private final static Logger LOG = LoggerFactory.getLogger(UpdateSelectedIdsController.class);
	
	public static class UpdateAction<PK2, C2 extends Collectable<PK2>, Ctl2 extends CollectController<PK2,C2>> extends AbstractUpdateAction<PK2, C2, PK2> {
		private final static Logger LOG = LoggerFactory.getLogger(UpdateAction.class);
		
		private final Ctl2 ctl;

		protected final Set<PK2> pks;

		private final Map<PK2, C2> processedCollectablesById;

		/**
		 * 
		 * @param ctl	{@link CollectController}
		 * @param pks	Set of primary keys for processing
		 * 
		 * @throws CommonBusinessException
		 */
		protected UpdateAction(final Ctl2 ctl, final Set<PK2> pks) throws CommonBusinessException {
			super(ctl);
			this.processedCollectablesById = new ConcurrentHashMap<PK2, C2>();
			this.ctl = ctl;
			this.pks = pks;
		}
		
		
		private C2 findCollectable(final PK2 pk) throws CommonBusinessException {
			return ctl.findCollectableById(ctl.getEntityUid(), pk, -1);
		}
		
		
		public C2 getCollectable(final PK2 pk) {
			C2 clct = processedCollectablesById.get(pk);
			if (null == clct) {
				try {
					clct = findCollectable(pk);
					processedCollectablesById.put(pk, clct);
				} catch (CommonBusinessException e) {
					LOG.error("could not find collectable with pk: " + pk, e);
					clct = null;
				}
			}
			return clct;
		}

		@Override
		public Object perform(PK2 pk, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
			//assert null == getCollectable(pk);
			final C2 clct = findCollectable(pk);
			processedCollectablesById.put(pk, clct);
			
			if (!changedFields.isEmpty()) {
				for (Map.Entry<UID, CollectableField> e : changedFields.entrySet()) {
					LOG.debug("Field to be changed: {}", e.getKey());
					clct.setField(e.getKey(), e.getValue());
				}
			}

			if (!ctl.getCollectState().isResultMode()) {
				final Object additional = ((CollectController<PK2,Collectable<PK2>>) ctl).getAdditionalDataForMultiUpdate(clct);
				final C2 c = ctl.updateCollectable(clct, additional, applyMultiEditContext, true);
				ctl.getResultController().replaceCollectableInTableModel(c);
			}
			return null;
		}

		@Override
		public String getText(PK2 pk) {
			final C2 clct = getCollectable(pk);
			assert null != clct && ObjectUtils.equals(clct.getPrimaryKey(), pk);
			return SpringLocaleDelegate.getInstance().getMessage(
					"UpdateSelectedCollectablesController.1", "Datensatz {0} wird ge\u00e4ndert...", 
					SpringLocaleDelegate.getInstance().getIdentifierLabel(clct, ctl.getEntityUid(), 
							MetaProvider.getInstance(), ctl.getLanguage()));
		}

		@Override
		public String getSuccessfulMessage(PK2 pk, Object oResult) {
			final C2 clct = getCollectable(pk);
			assert null != clct && ObjectUtils.equals(clct.getPrimaryKey(), pk);
			return SpringLocaleDelegate.getInstance().getMessage(
					"UpdateSelectedCollectablesController.2", "Datensatz {0} erfolgreich ge\u00e4ndert.", 
					SpringLocaleDelegate.getInstance().getIdentifierLabel(clct, ctl.getEntityUid(), MetaProvider.getInstance(), ctl.getLanguage()));
		}

		@Override
		public String getConfirmStopMessage() {
			return SpringLocaleDelegate.getInstance().getMessage(
					"UpdateSelectedCollectablesController.3", "Wollen Sie das \u00c4ndern der Datens\u00e4tze an dieser Stelle beenden?\n(Die bisher ge\u00e4nderten Datens\u00e4tze bleiben in jedem Fall ge\u00e4ndert.)");
		}

		@Override
		public String getExceptionMessage(PK2 pk, Exception ex) {
			final C2 clct = getCollectable(pk);
			assert null != clct && ObjectUtils.equals(clct.getPrimaryKey(), pk);
			return SpringLocaleDelegate.getInstance().getMessage(
					"UpdateSelectedCollectablesController.4", "Datensatz {0} konnte nicht ge\u00e4ndert werden.", 
					SpringLocaleDelegate.getInstance().getIdentifierLabel(clct, ctl.getEntityUid(), MetaProvider.getInstance(), ctl.getLanguage())) + ex.getMessage();
		}

		@Override
		public void executeFinalAction() throws CommonBusinessException {
			// cleanup
			LOG.debug("clear {} processed items" + processedCollectablesById.size());
			processedCollectablesById.clear();
		}
	}

	public UpdateSelectedIdsController(final Ctl ctl, final Set<PK> ids) throws CommonBusinessException {
		super(ctl, SpringLocaleDelegate.getInstance().getMessage(
				"UpdateSelectedCollectablesController.5", "Datens\u00e4tze \u00e4ndern"), new UpdateAction<PK,C, Ctl>(ctl,ids), ids);
	}

}  // class UpdateSelectedCollectablesController
