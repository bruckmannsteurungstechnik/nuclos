//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.component;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.InputVerifier;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableCellRenderer;

import org.apache.commons.lang.NullArgumentException;
import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.Utils;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.masterdata.valuelistprovider.DatasourceBasedCollectableFieldsProvider;
import org.nuclos.client.masterdata.valuelistprovider.DefaultValueProvider;
import org.nuclos.client.masterdata.valuelistprovider.MandatorRestrictionCollectableFieldsProvider;
import org.nuclos.client.ui.Bubble;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.ListOfValues;
import org.nuclos.client.ui.ListOfValues.QuickSearchResulting;
import org.nuclos.client.ui.collect.CollectController.CollectableEventListener;
import org.nuclos.client.ui.collect.CollectController.MessageType;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelEvent;
import org.nuclos.client.ui.collect.component.model.SearchComponentModelEvent;
import org.nuclos.client.ui.labeled.LabeledComponentSupport;
import org.nuclos.client.ui.labeled.LabeledListOfValues;
import org.nuclos.client.ui.popupmenu.JPopupMenuListener;
import org.nuclos.client.valuelistprovider.cache.CollectableFieldsProviderCache.CachingCollectableFieldsProvider;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.searchcondition.AbstractCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.AtomicCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.ToHumanReadablePresentationVisitor;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.masterdata.ejb3.EntityFacadeRemote;

/**
 * A <code>CollectableComponent</code> that presents a value in a <code>ListOfValues</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class CollectableListOfValues<PK> extends LabeledCollectableComponentWithVLP implements ICollectableListOfValues<PK> {

	private static final Logger LOG = Logger.getLogger(CollectableListOfValues.class);

	/**
	 * the value id "remembered in the view", as the JTextField only holds the value.
	 */
	private Object oValueId;

	private boolean blnIsLookupEntity = false;

	private final List<LookupListener<PK>> lstLookupListeners = new LinkedList<LookupListener<PK>>();

	private DocumentListener documentlistener;
	
	private EntityFacadeRemote entityFacadeRemote;
	
	/**
	 * inner class <code>CollectableListOfValues.Event</code>.
	 */
	public static class Event<PK2> extends CollectableComponentEvent {

		/**
		 * §precondition clctlovSource != null
		 * 
		 * @param clctlovSource the <code>CollectableListOfValues</code> that triggered this event.
		 */
		public Event(CollectableListOfValues<PK2> clctlovSource) {
			super(clctlovSource);
		}

		/**
		 * §postcondition result != null
		 * 
		 * @return the <code>CollectableListOfValues</code> that triggered this event.
		 */
		public CollectableListOfValues<PK2> getCollectableListOfValues() {
			return (CollectableListOfValues<PK2>) this.getCollectableComponent();
		}

	}	// inner class Event

	/**
	 * inner class <code>LOVListener</code>.
	 */
	public interface LOVListener<PK2> extends ReferencingListener {

		/**
		 * performs a lookup on the entity referenced by this component.
		 * The default action is to let the user perform a search within this entity and pick one object.
		 * @param ev
		 */
		void lookup(Event<PK2> ev);

		/**
		 * performs a search and shows search results on the entity referenced by this component using a search condition.
		 * The default action is to let the user pick one object in the shown search result.
		 * @param ev
		 */
		void viewSearchResults(Event<PK2> ev);
	}	// inner class LOVListener

	private final CollectableEventListener<PK> clctEventListenerForNew;
	private final CollectableEventListener<PK> clctEventListenerForDetails;

	/**
	 * §postcondition this.isDetailsComponent()
	 */
	public CollectableListOfValues(CollectableEntityField clctef) {
		this(clctef, false);

		assert this.isDetailsComponent();
	}

	public CollectableListOfValues(final CollectableEntityField clctef, boolean bSearchable) {
		this(clctef, bSearchable, false);
	}

	/**
	 * §precondition clctef != null
	 * §precondition clctef.isIdField()
	 * §precondition clctef.isReferencing()
	 */
	public CollectableListOfValues(final CollectableEntityField clctef, boolean bSearchable, boolean bComboBoxLookAndFeel) {
		super(clctef, new LabeledListOfValues(new LabeledComponentSupport()), bSearchable);
		setDropdownBtn(bComboBoxLookAndFeel);
		setLovBtn(!bComboBoxLookAndFeel);
		init();
		
		this.clctEventListenerForNew = new CollectableEventListener<PK>() {
			@Override
			public void handleCollectableEvent(Collectable<PK> collectable, MessageType messageType) {
				switch (messageType) {
					case NEW_DONE:
						acceptLookedUpCollectable(collectable);
						break;
					default:
						clctEventListenerForDetails.handleCollectableEvent(collectable, messageType);
				}
			}
		};
		this.clctEventListenerForDetails = new CollectableEventListener<PK>() {
			@Override
			public void handleCollectableEvent(Collectable<PK> collectable, MessageType messageType) {
				switch (messageType) {
					case EDIT_DONE:
						//@see NUCLOS-1690
						if (collectable != null && LangUtils.equal(collectable.getId(), getModel().getField().getValueId())) {
							Object oForeignValue = Utils.getRepresentation(getEntityField().getUID(),  collectable);
							setFieldInitial(new CollectableValueIdField(collectable.getId(), oForeignValue));
							// do not fire lookup successful - NUCLOS-2498
						}
						break;
					case STATECHANGE_DONE:
						//@see NUCLOS-1690
						if (collectable != null && LangUtils.equal(collectable.getId(), getModel().getField().getValueId())) {
							Object oForeignValue = Utils.getRepresentation(getEntityField().getUID(),  collectable);
							setFieldInitial(new CollectableValueIdField(collectable.getId(), oForeignValue));
							fireLookupSuccessful(new LookupEvent(CollectableListOfValues.this, collectable, null));
						}
						break;
				}
			}
		};
	}
	
	final void init() {
		final CollectableEntityField clctef = getEntityField();
		if (clctef == null) {
			throw new NullArgumentException("clctef");
		}
		if (!clctef.isIdField()) {
			throw new IllegalArgumentException(StringUtils.getParameterizedExceptionMessage("collectable.listofvalues.exception.1", clctef.getUID()));
				//"Das Feld \"" + clctef.getUID() + "\" ist kein Id-Feld und kann daher nicht in einem LOV (Suchfeld) dargestellt werden.");
		}

		FieldMeta<?> efMeta = MetaProvider.getInstance().getEntityField(clctef.getUID());

		if (!clctef.isReferencing()) {
			throw new IllegalArgumentException(StringUtils.getParameterizedExceptionMessage("collectable.listofvalues.exception.2", clctef.getUID()));
				//"Das Feld \"" + clctef.getUID() + "\" ist kein Fremdschl\u00fcssel-Feld und kann daher nicht in einem LOV (Suchfeld) dargestellt werden.");
		}
		assert !this.isInsertable();
		this.setInsertable(this.isSearchComponent());

		setEntityFacadeRemote((EntityFacadeRemote)SpringApplicationContextHolder.getBean("entityService"));
		getListOfValues().setQuickSearchResulting(new QuickSearchResulting() {
			@Override
			protected List<CollectableValueIdField> getQuickSearchResult(String inputString, boolean bLimit) throws CommonBusinessException {
				UID vlpId = null;
				Map<String, Object> vlpParameter = null;
				CollectableFieldsProvider provider = getValueListProvider();
				UID mandator = null;
				if (provider instanceof CachingCollectableFieldsProvider) {
					provider = ((CachingCollectableFieldsProvider) provider).getDelegate();
				}
				if (provider instanceof DatasourceBasedCollectableFieldsProvider) {
					DatasourceBasedCollectableFieldsProvider dsProvider = ((DatasourceBasedCollectableFieldsProvider) provider);
					if (dsProvider.isValuelistProviderDatasource()) {
						
						for (LabeledCollectableComponentWithVLP source : getVlrSources()) {
							if (!source.isSearchComponent()) {
								continue;
							}
							
							Map<UID, String> mapTags = source.getSearchModel().getMappingOfRefreshValueListParamaters();
							CollectableSearchCondition cond = source.getSearchConditionFromView();
							if (cond instanceof CollectableInCondition) {
								CollectableInCondition<?> cic = (CollectableInCondition<?>) cond;
								Set<Object> ids = cic.getKeyMap() != null ? cic.getKeyMap().keySet() : null;
								if (ids != null) {
									String tag = mapTags.get(source.getFieldUID());
									if (tag != null) {
										dsProvider.setParameter(tag, ids);
									}
								}
							}
						}
						
						vlpId = dsProvider.getDatasourceVO().getId();
						vlpParameter = dsProvider.getValueListParameter();
					}
				}
				if (provider instanceof MandatorRestrictionCollectableFieldsProvider) {
					mandator = ((MandatorRestrictionCollectableFieldsProvider) provider).getMandator();
				}

				return CollectableListOfValues.this.getQuickSearchResult(clctef, inputString, vlpId, vlpParameter, mandator, bLimit);
			}
		});

		// MultiSelect only -> Search only
		// On details -> NUCLOS-7289
		if (isMultiSelect() && isSearchComponent()) {
			getListOfValues().getJTextField().addFocusListener(new FocusAdapter() {

				@Override
				public void focusLost(FocusEvent e) {
					refreshValueListsOfAllTargets();
				}
			});
		}
		
		this.setupLookupListener();

		this.getListOfValues().setQuickSearchEnabled(enableQuickSearch(clctef));
		this.getListOfValues().setQuickSearchSelectedListener(new ListOfValues.QuickSearchSelectedListener() {
			@Override
			public void actionPerformed(CollectableValueIdField itemSelected) {
				if (itemSelected == null) {
					clearListOfValues();
				} else {
					try {
						Collectable<PK> c = (Collectable<PK>) Utils.getReferencedCollectable(clctef.getEntityUID(), clctef.getUID(), itemSelected.getValueId());
						CollectableListOfValues.this.acceptLookedUpCollectable(c);
					} catch(Exception e) {
						Errors.getInstance().showExceptionDialog(CollectableListOfValues.this.getListOfValues(), e);
					}
				}
			}
		});
		this.getListOfValues().setQuickSearchCanceledListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				CollectableListOfValues.this.setViewLocked(true);
				CollectableListOfValues.this.modelToView();
				CollectableListOfValues.this.setViewLocked(false);
			}
		});
		if (this.isSearchComponent()) {
			this.getListOfValues().setSearchOnLostFocus(false);
		}

		blnIsLookupEntity = efMeta.getLookupEntity() != null;
		
		if (this.isSearchComponent() && efMeta.getForeignEntity() != null) {
			this.getListOfValues().getJTextField().setInputVerifier(createBlockingInputverifier(this));
		}

		this.getListOfValues().setQuickSearchOnly(!SecurityCache.getInstance().isReadAllowedForEntity(efMeta.getForeignEntity() != null ? efMeta.getForeignEntity() : efMeta.getLookupEntity()));

		assert this.isInsertable() == this.isSearchComponent();
	}
	
	final void setEntityFacadeRemote(EntityFacadeRemote entityFacadeRemote) {
		this.entityFacadeRemote = entityFacadeRemote;
	}

	/**
	 *
	 * @param clctef
	 * @return
	 */
	private static boolean enableQuickSearch(final CollectableEntityField clctef) {
		return true;
	}

	/**
	 *
	 * @param dsvo
	 * @param collectableFieldsProvider
	 * @param clctef
	 * @param inputString
	 * @param mandator
	 * @param bLimit
	 * @return
	 */
	private List<CollectableValueIdField> getQuickSearchResult(final CollectableEntityField clctef, final String inputString, UID vlpId, Map<String, Object> vlpParameter, UID mandator, boolean bLimit)
		 throws CommonBusinessException {

		CollectableEntity clcte = clctef.getCollectableEntity();
		if (clcte == null)
			return Collections.emptyList();

		FieldMeta<?> fm = MetaProvider.getInstance().getEntityField(clctef.getUID());
		List<CollectableValueIdField> lstQuickSearchResult = entityFacadeRemote.getQuickSearchResult(
				fm, inputString, vlpId, vlpParameter, bLimit ? QUICKSEARCH_MAX : null, mandator);
		return lstQuickSearchResult;
	}

	/**
	 * adds a "Clear" entry to the popup menu, for a non-searchable component.
	 */
	@Override
	public JPopupMenu newJPopupMenu() {
		final JPopupMenu result = super.newJPopupMenu();
		if (!this.isSearchComponent()) {
			final JMenuItem miClear = new JMenuItem(
					SpringLocaleDelegate.getInstance().getMessage("CollectableFileNameChooserBase.1","Zur\u00fccksetzen"));
			boolean bClearEnabled;
			try {
				bClearEnabled = this.getBrowseButton().isEnabled() && (this.getField().getValueId() != null);
			}
			catch (CollectableFieldFormatException ex) {
				bClearEnabled = false;
			}
			miClear.setEnabled(bClearEnabled);

			miClear.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent ev) {
					CollectableListOfValues.this.clearListOfValues();
				}
			});
			result.add(miClear);
		}
		return result;
	}

	private void clearListOfValues() {
		clear();
		fireLookupSuccessful(new LookupEvent<PK>(CollectableListOfValues.this, null, null));
		getListOfValues().enableButton();
	}

	@Override
	protected void setupJPopupMenuListener(JPopupMenuListener popupmenulistener) {
		this.getJTextField().addMouseListener(popupmenulistener);
		// add a listener to the button because the textfield may not be visible for some instances:
		this.getBrowseButton().addMouseListener(popupmenulistener);
	}

	/**
	 * sets up the listeners for "lookup".
	 */
	private void setupLookupListener() {
		this.getBrowseButton().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ev) {
				if(CollectableListOfValues.this.getCollectableSearchCondition() != null){
					fireViewSearchResults();
				} else {
					fireLookup();
				}
			}
		});
	}

	/**
	 * @return the internal control component
	 */
	public ListOfValues getListOfValues() {
		return ((LabeledListOfValues) this.getLabeledComponent()).getListOfValues();
	}

	@Override
	public void setColumns(int iColumns) {
		this.getListOfValues().setColumns(iColumns);
	}

	/**
	 * @deprecated Not used in tabbed GUI any more. (Thomas Pasch)
	 */
	public JButton getBrowseButton() {
		return this.getListOfValues().getBrowseButton();
	}

	/**
	 * @return the textfield that is part of the component.
	 */
	public JTextField getJTextField() {
		return this.getListOfValues().getJTextField();
	}

	@Override
	public JComponent getFocusableComponent() {
		return this.getJTextField();
	}
	
	@Override
	public void setMnemonic(char cMnemonic) {
		super.setMnemonic(cMnemonic);
		getJTextField().setFocusAccelerator(cMnemonic);
	}

	/**
	 * @return Is this component insertable? That means: Is the textfield editable?
	 */
	public boolean isInsertable() {
		return (this.documentlistener != null);
	}

	/**
	 * insertable means: it is possible to enter the value in the textfield directly. In this case,
	 * the value id is set to null.
	 * @param bInsertable
	 */
	@Override
	public void setInsertable(boolean bInsertable) {
		if (this.isInsertable() != bInsertable) {
			/** @todo respect "enabled" property */
			this.getJTextField().setEditable(bInsertable);
			if (bInsertable) {
				this.registerDocumentListener();
			}
			else {
				this.unregisterDocumentListener();
			}
		}
		assert this.isInsertable() == bInsertable;
	}

	@Override
	protected void setEnabledState(boolean bEnabled) {
		if (this.isSearchComponent()) {
			/** @todo respect "insertable" property */
			super.setEnabledState(bEnabled);
		}
		else {
			getListOfValues().setEnabled(bEnabled);
		}
	}

	/**
	 * @deprecated Not used in tabbed GUI. (Thomas Pasch)
	 */
	public void setBrowseButtonVisibleOnly(boolean bVisible) {
		this.getJTextField().setVisible(!bVisible);
	}

	private void registerDocumentListener() {
		if (this.documentlistener != null) {
			throw new IllegalStateException();
		}

		this.documentlistener = newDocumentListenerForTextComponentWithComparisonOperator();

		this.getJTextField().getDocument().addDocumentListener(this.documentlistener);
	}

	private void unregisterDocumentListener() {
		if (this.documentlistener == null) {
			throw new IllegalStateException();
		}
		this.getJTextField().getDocument().removeDocumentListener(this.documentlistener);
		this.documentlistener = null;
	}

	@Override
	public boolean hasComparisonOperator() {
		return true;
	}

	@Override
	public void setComparisonOperator(ComparisonOperator compop) {
		super.setComparisonOperator(compop);

		if (compop.getOperandCount() < 2) {
			this.runLocked(new Runnable() {
				@Override
				public void run() {
					getJTextField().setText(null);
				}
			});
		}
	}

	@Override
	public CollectableField getFieldFromView() throws CollectableFieldFormatException {

		if (this.oValueId == null && !blnIsLookupEntity) {
			if (isSearchComponent()) {
				if (bSubformComponent && !StringUtils.isNullOrEmpty(this.getJTextField().getText())) {
					String s = getEntityField().getName() + " validation message: " + SpringLocaleDelegate.getInstance().getMessage("InputVerifier.5", "Bitte warten Sie auf das Ergebnis!");;
					LOG.debug(s);
					throw new CollectableFieldFormatException(s);
				}
				return new CollectableValueIdField(this.oValueId, CollectableTextComponentHelper.write(this.getJTextField(), this.getEntityField()).getValue());
			}
			return new CollectableValueIdField(this.oValueId, null);
		} else {
			if (blnIsLookupEntity)
				return new CollectableValueIdField(null, CollectableTextComponentHelper.write(this.getJTextField(), this.getEntityField()).getValue());
			else {
				Object value = CollectableTextComponentHelper.write(this.getJTextField(), this.getEntityField()).getValue();
				if (value == null)
					this.oValueId = null;
				return new CollectableValueIdField(this.oValueId, CollectableTextComponentHelper.write(this.getJTextField(), this.getEntityField()).getValue());
			}
		}
	}
	
	@Override
	public boolean isMultiSelect() {
		return getListOfValues().isMultiSelect();
	}
	
	@Override
	protected List<Object> getCurrentItems() {
		return getListOfValues().getMultiSelectItems();
	}

	@Override
	protected CollectableSearchCondition getSearchConditionFromView() throws CollectableFieldFormatException {
		if (isMultiSelect()) {
			if(super.getSearchConditionFromComboItems() != null)
				return super.getSearchConditionFromComboItems(); // no Multiselect items selected, perhabs a single object search		
		}
		return this.getSearchConditionFromViewImpl(this.getJTextField().getText());
	}

	@Override
	protected void updateView(CollectableField clctfValue) {		
		this.getJTextField().setText(LangUtils.toString(clctfValue));

		// ensure the start of the text is visible (instead of the end) when the text is too long
		// to be fully displayed:
		this.getJTextField().setCaretPosition(0);

		this.adjustAppearance();

		this.oValueId = clctfValue.getValueId();
	}

	@Override
	protected void adjustBackground() {
//		this.getJTextField().setBackground(this.getBackgroundColor());
	}

	/**
	 * Implementation of <code>CollectableComponentModelListener</code>.
	 * This event is (and must be) ignored for a searchable text field.
	 * @param ev
	 */
	@Override
	public void collectableFieldChangedInModel(CollectableComponentModelEvent ev) {
		if (this.isSearchComponent()) {
			if (!blnIsLookupEntity) {
				// the text is set in searchConditionChangedInModel, but the value id is set here:
				this.oValueId = ev.getNewValue().getValueId();
				if (ev.getNewValue().getValueId() == null && ev.getNewValue().getValue() == null && getListOfValues().isMultiSelect()) {
					this.getListOfValues().setSelection(this.oValueId);
				}
			}
		}
		else {
			super.collectableFieldChangedInModel(ev);
		}
	}

	/**
	 * Implementation of <code>CollectableComponentModelListener</code>.
	 * @param ev
	 */
	@Override
	public void searchConditionChangedInModel(final SearchComponentModelEvent ev) {
		// update the view:
		this.runLocked(new Runnable() {
			@Override
			public void run() {
				// Note: CollectableTextComponent itself can only handle atomic search conditions.
				// If the following class cast should ever fail for a special text field, redefine searchConditionChangedInModel in your subclass:
				final AtomicCollectableSearchCondition atomiccond = (AtomicCollectableSearchCondition) ev.getSearchComponentModel().getSearchCondition();

				modelToView(atomiccond, CollectableListOfValues.this.getJTextField());
				// Note that the value id is set in collectableFieldChangedInModel.
			}
		});
	}

	private void fireLookup() {
		final ReferencingListener reflistener = this.getReferencingListener();
		if (reflistener != null && reflistener instanceof LOVListener) {
			final LOVListener<PK> lovlistener = (LOVListener<PK>) reflistener;
			lovlistener.lookup(new Event<PK>(this));
		}
	}

	private void fireViewSearchResults() {
		final ReferencingListener reflistener = this.getReferencingListener();
		if (reflistener != null && reflistener instanceof LOVListener) {
			final LOVListener<PK> lovlistener = (LOVListener<PK>) reflistener;
			lovlistener.viewSearchResults(new Event<PK>(this));
		}
	}

	/**
	 * @see org.nuclos.client.ui.collect.component.ICollectableListOfValues#acceptLookedUpCollectable(org.nuclos.common.collect.collectable.Collectable)
	 */
	@Override
	public void acceptLookedUpCollectable(Collectable<PK> clctLookedUp) {
		this.acceptLookedUpCollectable(clctLookedUp, null);
	}

	/**
	 * accepts the given <code>Collectable</code>, that was selected by the user in a lookup operation.
	 * Notifies all registered <code>LookupListener</code>s.
	 * 
	 * §precondition clctLookedUp != null
	 * §precondition clctLookedUp.isComplete()
	 */
	public void acceptLookedUpCollectable(final Collectable<PK> clctLookedUp, List<Collectable<PK>> additionalCollectables) {
		if (clctLookedUp == null) {
			throw new NullArgumentException("clctLookedUp");
		}
		Object oForeignValue = Utils.getRepresentation(this.getEntityField().getUID(), clctLookedUp);
		this.setField(new CollectableValueIdField(clctLookedUp.getId(), oForeignValue));
		this.fireLookupSuccessful(new LookupEvent<PK>(this, clctLookedUp, additionalCollectables));
	}

	private void setFieldInitial(CollectableField clctfValue) {
		getModel().setFieldInitial(clctfValue);
	}
	
	@Override
	public void setField(CollectableField clctfValue) {
		super.setField(clctfValue);
	}

	/**
	 * @see org.nuclos.client.ui.collect.component.ICollectableListOfValues#addLookupListener(org.nuclos.client.ui.collect.component.LookupListener)
	 */
	@Override
	public synchronized void addLookupListener(LookupListener<PK> listener) {
		this.lstLookupListeners.add(listener);
	}

	/**
	 * @see org.nuclos.client.ui.collect.component.ICollectableListOfValues#removeLookupListener(org.nuclos.client.ui.collect.component.LookupListener)
	 */
	@Override
	public synchronized void removeLookupListener(LookupListener<PK> listener) {
		this.lstLookupListeners.remove(listener);
	}

	/**
	 * notifies all registered LookupListeners
	 * @param ev
	 */
	protected void fireLookupSuccessful(LookupEvent<PK> ev) {
		Collections.sort(lstLookupListeners, new Comparator<LookupListener<PK>>() {
			@Override
            public int compare(LookupListener<PK> o1, LookupListener<PK> o2) {
	            return Integer.valueOf(o1.getPriority()).compareTo(o2.getPriority());
            }});

		for (LookupListener<PK> listener : lstLookupListeners) {
			listener.lookupSuccessful(ev);
		}
	}

	protected CollectableSearchCondition cSearchCondition;

	/**
	 * @see org.nuclos.client.ui.collect.component.ICollectableListOfValues#getCollectableSearchCondition()
	 */
	@Override
	public CollectableSearchCondition getCollectableSearchCondition(){
		return cSearchCondition;
	}

	protected void setCollectableSearchCondition(CollectableSearchCondition searchCondition){
		this.cSearchCondition = searchCondition;
	}

	@Override
	public void refreshValueList(boolean async, boolean bSetDefault, boolean isInitial) {
		try {
			//NUCLOS-4936 - There is no obvious reason to fetch the "DefaultValue" for Search-Components
			if (bSetDefault && !isSearchComponent()) {
				
				if (getField() != null && getField().isNull() && getValueListProvider() instanceof DefaultValueProvider) {
					final CollectableField cf = ((DefaultValueProvider) getValueListProvider()).getDefaultValue();
					if (cf != null && getField().isNull()) {
						if (!isInitial) {
							getModel().setField(cf);							
						} else {
							getModel().setFieldInitial(cf);
						}
					}
				}				
			}
		}
		catch(Exception e) {
			Errors.getInstance().showExceptionDialog(CollectableListOfValues.this.getJComponent(), e);
		}
	}
	
	@Override
	protected void refreshValueListFromApplyParameters(boolean async, boolean isInitial) {
		refreshValueList(async, true, isInitial);
	}

	@Override
	public void refreshValueList() {
		refreshValueList(false);
	}
	
	@Override
	protected void refreshValueListFromRestrictingSources(Set<LabeledCollectableComponentWithVLP> restricting) {
		getListOfValues().enableButton();
	}
	
	public CollectableEventListener<PK> getCollectableEventListenerForNew() {
		return clctEventListenerForNew;
	}
	public CollectableEventListener<PK> getCollectableEventListenerForDetails() {
		return clctEventListenerForDetails;
	}

	private class My2CollectableComponentDetailTableCellRenderer extends CollectableComponentDetailTableCellRenderer {

		private My2CollectableComponentDetailTableCellRenderer() {
		}		
		
		@Override
        public Component getTableCellRendererComponent(JTable tbl, Object oValue, boolean bSelected, boolean bHasFocus, int iRow, int iColumn) {
			
			final Component result = super.getTableCellRendererComponent(tbl, oValue, bSelected, bHasFocus, iRow, iColumn);
			
			if (result instanceof JLabel) {
				((JLabel) result).setBorder(new JTextField().getBorder());
				((JLabel) result).setVerticalAlignment(SwingConstants.CENTER);
			}

			return result;
		}
		
		@Override
		protected void setValue(Object value) {
			if (value instanceof AbstractCollectableSearchCondition)
				setToolTipText(((CollectableSearchCondition)value).accept(new ToHumanReadablePresentationVisitor()));
			if (value instanceof CollectableComparison)
				value = ((CollectableComparison) value).getComparand();
			if (value instanceof AtomicCollectableSearchCondition)
				value = ((AtomicCollectableSearchCondition)value).getComparandAsString();

			super.setValue(value);
		}
	}

	@Override
	public TableCellRenderer getTableCellRenderer(boolean subform) {
		if (isSearchComponent()){
			//NOAINT-215
			return new My2CollectableComponentDetailTableCellRenderer();
		}
		else {
			return new CollectableComponentDetailTableCellRenderer();
		}
	}
	
	/**
	 * unmodifiable list of {@link LookupListener}
	 * @return
	 */
	public List<LookupListener<PK>> getLookupListener() {
		return Collections.unmodifiableList(lstLookupListeners);
	}

	@Override
	public void bindLayoutNavigationSupportToProcessingComponent() {
		getListOfValues().setLayoutNavigationCollectable(this);
	}
	
	@Override
	public void setMultiSelect(boolean bMultiSelect) {
		getListOfValues().setMultiSelect(bMultiSelect);
	}
	
	public void setLovBtn(Boolean bLovBtn) {
		getListOfValues().setLovBtn(bLovBtn);
	}
	
	public boolean hasLovSearch() {
		return getListOfValues().hasLovSearch();
	}
	
	public void setLovSearch(Boolean bLovSearch) {
		getListOfValues().setLovSearch(bLovSearch);
	}

	public void setDropdownBtn(Boolean bDropdownBtn) {
		getListOfValues().setDropdownBtn(bDropdownBtn);
	}
	
	@Override
	public JComboBox getJComboBox() {
		return getListOfValues().getComboBox();
	}

	@Override
	protected Integer getColumns() {
		return null;
	}

	@Override
	protected CollectableField getClctfExtra() {
		return null;
	}
	
	@Override
	protected ComparisonOperator[] getSupportedComparisonOperators() {
		if (bSubformComponent) {
			return new ComparisonOperator[] {
					ComparisonOperator.NONE,
					ComparisonOperator.EQUAL,
					ComparisonOperator.NOT_EQUAL,
					ComparisonOperator.IS_NULL,
					ComparisonOperator.IS_NOT_NULL,
					ComparisonOperator.IN,
					ComparisonOperator.NOT_IN
			};
		}
		return super.getSupportedComparisonOperators();
	}
	
	@Override
	protected void resetWithComparison() {
		super.resetWithComparison();
		// new input in search component... reset valueId if any
		if (bSubformComponent && oValueId != null) {
			oValueId = null;
		}
	}
	
	protected final static <PK> InputVerifier createBlockingInputverifier(CollectableListOfValues<PK> clctLOV) {
		final WeakReference<CollectableListOfValues<PK>> weakClctLOV = new WeakReference<CollectableListOfValues<PK>>(clctLOV);
		return new InputVerifier() {
			
			@Override
			public boolean verify(JComponent arg0) {
				CollectableListOfValues<PK> clctLOV = weakClctLOV.get();
				if (clctLOV != null) {
					if (!clctLOV.bSubformComponent) {
						return true;
					}
					String s = clctLOV.getJTextField().getText();
					if (StringUtils.isNullOrEmpty(s)) {
						return true;
					}
					if (clctLOV.oValueId != null) {
						return true;
					} else {
						String sBubble = SpringLocaleDelegate.getInstance().getMessage("InputVerifier.5", "Bitte warten Sie auf das Ergebnis!");
						Bubble bubble = new Bubble(clctLOV.getFocusableComponent(), sBubble, 3);
						bubble.setVisible(true);	
					}
				}
				return false;
			}
		};
	}

}	// class CollectableListOfValues
