//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.component;

import java.awt.Color;
import java.util.List;

import javax.swing.table.TableCellRenderer;

import org.apache.commons.lang.NotImplementedException;
import org.apache.log4j.Logger;
import org.nuclos.client.genericobject.valuelistprovider.ISelfIdCollectableFieldsProvider;
import org.nuclos.client.ui.InputVerifierFactory;
import org.nuclos.client.ui.PhoneNumberTextFieldWithButton;
import org.nuclos.client.ui.labeled.LabeledComponentSupport;
import org.nuclos.client.ui.labeled.LabeledPhoneNumber;
import org.nuclos.common.collect.collectable.CollectableComponentTypes;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * <code>CollectableComponent</code> to display/enter a phone number.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:maik.stueker@nuclos.de">Maik Stueker</a>
 * @version	01.00.00
 */
public class CollectablePhoneNumber extends CollectableHyperlink 
	// VLP for retrieving INTID
	implements CollectableComponentWithValueListProvider, CollectableFieldsProvider, ISelfIdCollectableFieldsProvider {

	private static final Logger LOG = Logger.getLogger(CollectablePhoneNumber.class);
	
	/**
	 * §postcondition this.isDetailsComponent()
	 */
	public CollectablePhoneNumber(CollectableEntityField clctef) {
		this(clctef, false);
		this.overrideActionMap();

		assert this.isDetailsComponent();
	}

	/**
	 * @param clctef
	 * @param bSearchable
	 */
	public CollectablePhoneNumber(CollectableEntityField clctef, boolean bSearchable) {
		super(clctef, bSearchable, 
				new LabeledPhoneNumber(new LabeledComponentSupport(),
						clctef.isNullable(), bSearchable));
		
		getPhoneNumber().setEntity(clctef.getEntityUID());
		
		getJTextComponent().setInputVerifier(InputVerifierFactory.getInputVerifier(clctef, CollectableComponentTypes.TYPE_PHONENUMBER));
	}

	public PhoneNumberTextFieldWithButton getPhoneNumber() {
		return ((LabeledPhoneNumber) getJComponent()).getPhoneNumber();
	}
	@Override
	protected Color getBackgroundColor() {
		// TODO Auto-generated method stub
		return super.getBackgroundColor();
	}
	@Override
	public TableCellRenderer getTableCellRenderer(boolean subform) {
		final TableCellRenderer parentRenderer = super.getTableCellRenderer(subform);
		return new HyperlinkCellRenderer(parentRenderer, getHyperlink().getFont(), getHyperlink().getButtonIcons(), subform);
	}

	@Override
	public void setValueListProvider(CollectableFieldsProvider clctfsprovider) {
		throw new NotImplementedException("Static component VLP only!");
	}

	@Override
	public CollectableFieldsProvider getValueListProvider() {
		return this;
	}

	@Override
	public void refreshValueList() {
		// do nothing
	}

	@Override
	public void refreshValueList(boolean async) {
		// do nothing
	}

	@Override
	public void refreshValueList(boolean async, boolean bSetDefault) {
		// do nothing
	}

	@Override
	public void refreshValueList(boolean async, boolean bSetDefault, boolean isInitial) {
		// do nothing
	}

	@Override
	public void setParameter(String sName, Object oValue) {
		if (SELF_ID.equals(sName)) {
			getPhoneNumber().setId((Long) oValue);
		}
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		throw new NotImplementedException("Dummy VLP only");
	}
	
}  // class CollectableEmail
