//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.component;

import java.awt.Component;
import java.awt.Insets;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.ui.CommonJTextField;
import org.nuclos.client.ui.EllipsisJTextField;
import org.nuclos.client.ui.FormatUtils;
import org.nuclos.client.ui.labeled.LabeledComponentSupport;
import org.nuclos.client.ui.labeled.LabeledTextField;
import org.nuclos.client.ui.message.MessageExchange;
import org.nuclos.client.ui.message.MessageExchange.MessageExchangeListener;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collection.Pair;

/**
 * <code>CollectableComponent</code> that presents a value in a <code>JTextField</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @version 01.00.00
 * @author    <a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 */
public class CollectableTextField extends CollectableTextComponent implements MessageExchangeListener {

	private static final Logger LOG = Logger.getLogger(CollectableTextField.class);

	private CollectableEntityField clctef;

	/**
	 * §postcondition this.isDetailsComponent()
	 */
	public CollectableTextField(CollectableEntityField clctef) {
		this(clctef, false);
		assert this.isDetailsComponent();
	}

	public CollectableTextField(CollectableEntityField clctef, boolean bSearchable) {
		super(clctef, new LabeledTextField(new LabeledComponentSupport(),
				clctef.isNullable(), clctef.getJavaClass(), clctef.getFormatInput(), bSearchable), bSearchable);
		this.clctef = clctef;
		FormatUtils.setupTextField(clctef, this.getJTextField(), bSearchable);
		MessageExchange.addListener(this);
	}

	// @todo return JTextField
	public EllipsisJTextField getJTextField() {
		return (EllipsisJTextField) this.getJTextComponent();
	}

	@Override
	public void setColumns(int iColumns) {
		this.getJTextField().setColumns(iColumns);
	}

	@Override
	public void setComparisonOperator(ComparisonOperator compop) {
		super.setComparisonOperator(compop);

		if (compop.getOperandCount() >= 0 && compop.getOperandCount() < 2) {
			this.runLocked(new Runnable() {
				@Override
				public void run() {
					try {
						getJTextComponent().setText(null);
					} catch (Exception e) {
						LOG.error("CollectableTextField.setComparisionOperator: " + e, e);
					}
				}
			});
		}
	}

	@Override
	public TableCellRenderer getTableCellRenderer(boolean subform) {
		final TableCellRenderer parentRenderer = super.getTableCellRenderer(subform);
		final CommonJTextField ntf = getJTextField();
		return new CollectableTextFieldCellRenderer(parentRenderer, ntf, clctef.getJavaClass());
	}

	@Override
	public void receive(Object id, ObjectType type, MessageType msg) {
		if (clctef != null && clctef.getCollectableEntity() != null) {
			Pair<UID, UID> idPair = new Pair<UID, UID>(clctef.getCollectableEntity().getUID(), clctef.getUID());
			if (idPair.equals(id)) {
				if (type == MessageExchangeListener.ObjectType.TEXTFIELD) {
					if (msg == MessageExchangeListener.MessageType.REFRESH) {
						if (isSearchComponent()) {
							FormatUtils.addAutoComplete(clctef, getJTextField(), FormatUtils.getAutoCompletePreferences(clctef));
						}
					}
				}
			}
		}
	}

	private static class CollectableTextFieldCellRenderer implements TableCellRenderer {

		private final TableCellRenderer parentRenderer;

		// Don't use, this triggers a memory leak! (tp)
		// private final CommonJTextField ntf;

		private final int horizontalAlignment;
		private final int paddingRight;
		private final Class<?> clz;

		private CollectableTextFieldCellRenderer(TableCellRenderer parentRenderer, CommonJTextField ntf, final Class<?> clz) {
			this.parentRenderer = parentRenderer;
			this.horizontalAlignment = ntf.getHorizontalAlignment();
			this.clz = clz;
			this.paddingRight = Integer.valueOf(StringUtils.defaultIfBlank(
					Number.class.isAssignableFrom(clz)
							? ClientParameterProvider.getInstance().getValue(ParameterProvider.PADDING_RIGHT_NUMERIC_CELLS)
							: null,
					"1"));
		}

		@Override
		public Component getTableCellRendererComponent(JTable tbl, Object oValue, boolean bSelected, boolean bHasFocus,
													   int iRow, int iColumn) {
			final Component comp = parentRenderer.getTableCellRendererComponent(tbl, oValue, bSelected, bHasFocus, iRow,
					iColumn);
			if (comp instanceof JLabel) {
				final JLabel lb = (JLabel) comp;
				lb.setHorizontalAlignment(horizontalAlignment);
				Border border = lb.getBorder();
				Insets insets = border.getBorderInsets(lb);
				insets.right = paddingRight;
				try {
					Constructor<? extends Border> constructor = border.getClass().getConstructor(Insets.class);
					lb.setBorder(constructor.newInstance(insets));
				} catch (InvocationTargetException | NoSuchMethodException | InstantiationException | IllegalAccessException e) {
					LOG.error(e);
				}
			}
			return comp;
		}
	}
}    // class CollectableTextField
