package org.nuclos.client.ui.collect.component.model;

import javax.swing.table.TableCellEditor;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.i18n.language.data.DataLanguageContext;
import org.nuclos.client.i18n.language.data.DataLanguageDelegate;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.dal.vo.IDataLanguageMap;
import org.nuclos.server.i18n.language.data.DataLanguageLocalizedEntityEntry;
import org.nuclos.server.i18n.language.data.DataLanguageUtils;
import org.nuclos.server.i18n.language.data.DataLanguageVO;

public class DetailsLocalizedComponentModel extends DetailsComponentModel implements CollectableLocalizedComponentModel<Long>{

	UID datalangUID;
	UID userDataLangUID;
	IDataLanguageMap dataLanguageMap;
	EntityMeta entityMeta;
	FieldMeta fieldMeta;
	Long primaryKey;
	MetaProvider metaProvider;
	MainFrameTab mainTab;
	boolean skipReset = false;
	private SubForm subForm;
	
	public DetailsLocalizedComponentModel(CollectableEntityField clctef) {		
		super(clctef);
			
		this.userDataLangUID = DataLanguageContext.getDataUserLanguage();
		this.datalangUID = DataLanguageContext.getDataSystemLanguage();
		this.metaProvider = MetaProvider.getInstance();
		this.entityMeta = metaProvider.getEntity(clctef.getEntityUID());
		this.fieldMeta = metaProvider.getEntityField(clctef.getUID());
		this.primaryKey = null;
		MainFrameTab mainTab = null;
	}
	
	public void initDataLanguageMap(IDataLanguageMap dataLanguageMap, MainFrameTab mainTab) {
		this.dataLanguageMap = dataLanguageMap;
		this.mainTab = mainTab;
		for (DataLanguageVO langUID : DataLanguageDelegate.getInstance().getSelectedLocales()) {
			if (!this.dataLanguageMap.getLanguageMap().containsKey(langUID.getPrimaryKey()))
				this.dataLanguageMap.getLanguageMap().put(langUID.getPrimaryKey(), null);
		}
	}
	
	public CollectableField getField(boolean readingCollectController) {
		
		DataLanguageLocalizedEntityEntry dataLanguage = this.dataLanguageMap.getDataLanguage(this.datalangUID);
		
		String value = null;
		
		if (dataLanguage != null) {
			UID extractFieldUID = DataLanguageUtils.extractFieldUID(getFieldUID());
			if (dataLanguage.getFieldValue(extractFieldUID) != null) {
				value = (String) dataLanguage.getFieldValue(extractFieldUID);
			}
		}
		
		return new CollectableValueField(value);
	}
	
	public void assign(DetailsComponentModel that) {
		if (that instanceof DetailsLocalizedComponentModel) {
			
			DetailsLocalizedComponentModel locModel = 
					(DetailsLocalizedComponentModel)that;
			
			initDataLanguageMap(locModel.getDataLanguageMap(), locModel.getTab());
			setPrimaryKey(locModel.getPrimaryKey());
		}
		super.assign(that);
		
	}
	
	@Override
	public CollectableField getField() {
		
		CollectableValueField retVal = null;
		
		UID extractFieldUID = DataLanguageUtils.extractFieldUID(getFieldUID());
		
		if (this.dataLanguageMap == null) {
			return new CollectableValueField(null);
		}
		
		if (this.userDataLangUID == null || this.userDataLangUID.equals(this.datalangUID)) {
			retVal = new CollectableValueField(
					super.getField().getValue());
		} else {				
		
			if (this.dataLanguageMap.getDataLanguage(this.userDataLangUID) != null) {				
				Object fieldValue = this.dataLanguageMap.getDataLanguage(this.userDataLangUID).getFieldValue(extractFieldUID);				
				retVal =  new CollectableValueField(fieldValue != null ? (String) fieldValue : null);					
			} else {
				if (this.dataLanguageMap.getDataLanguage(this.datalangUID) == null) {
					retVal = new CollectableValueField(
							super.getField().getValue());
				} else {
					Object fieldValue = this.dataLanguageMap.getDataLanguage(this.datalangUID).getFieldValue(extractFieldUID);				
					retVal =  new CollectableValueField(fieldValue != null ? (String) fieldValue : null);					
				}					
			}
		}
		
		return retVal;
	}
	
	@Override
	public void setField(final CollectableField clctfValue) {		
		CollectableValueField retVal = null;

		if (clctfValue == null ) {
			super.setField(new CollectableValueField(null)); 
			return;
		}
		
		retVal =  new CollectableValueField(clctfValue.getValue());
		
		UID selectedLanguage = userDataLangUID != null ? userDataLangUID : datalangUID;
		String value = clctfValue.getValue() != null ? clctfValue.getValue().toString() : null;
		
		DataLanguageUtils.setLocalizedValueAndValidateMap(
				this.dataLanguageMap, getPrimaryKey(),this.entityMeta, selectedLanguage, datalangUID, userDataLangUID, getFieldUID(), 
				DataLanguageDelegate.getInstance().getSelectedLocaleUIDs(), value);
		
		super.setField(retVal, true, true);			
	}
	
	public UID getSystemPrimaryDataLanguage() {
		return this.datalangUID;
	}
	
	public UID getUserPrimaryDataLanguage() {
		return this.userDataLangUID;
	}
	
	public IDataLanguageMap getDataLanguageMap() {
		return this.dataLanguageMap;
	}

	public EntityMeta getEntityMeta() {
		return entityMeta;
	}

	public FieldMeta getFieldMeta() {
		return fieldMeta;
	}

	public MetaProvider getMetaProvider() {
		return metaProvider;
	}

	public Long getPrimaryKey() {
		return primaryKey;
	}
	
	public void setPrimaryKey(Long pk) {
		primaryKey = pk;;
	}

	@Override
	public MainFrameTab getTab() {
		return mainTab;
	}

	@Override
	public void setDataLanguageMap(IDataLanguageMap fieldLanguageMap) {
		this.dataLanguageMap = fieldLanguageMap;
	}

	@Override
	public void setField(CollectableValueField collectableValueField,
			boolean skipReset, boolean setDirty) {
		this.skipReset = skipReset;
		// NUCLOS-7873 With second parameter == false the main record will be set dirty
		this.setField(collectableValueField, setDirty ? Boolean.FALSE : Boolean.TRUE);
		this.skipReset = !skipReset;
	}

	@Override
	public boolean isDataLanguageMapAsDirty() {
		boolean retVal = false;
		
		if (this.dataLanguageMap == null) {
			retVal = true;
		} else {
			for (DataLanguageVO langObj : DataLanguageDelegate.getInstance().getSelectedLocales()) {
				if (this.dataLanguageMap.getDataLanguage(langObj.getPrimaryKey()) == null) {
					retVal = true;
					break;
				} else {
					DataLanguageLocalizedEntityEntry dataLanguage = 
							this.dataLanguageMap.getDataLanguage(langObj.getPrimaryKey());
					
					UID fieldFlagged = DataLanguageUtils.extractFieldUID(getFieldMeta().getUID(), true);
					if (!Boolean.FALSE.equals(dataLanguage.getFieldValue(fieldFlagged))) {
						retVal = true;
						break;
					}
				}
			}
			
		}
		
		return retVal;
	}

	@Override
	public void setSubForm(SubForm subForm) {
		this.subForm = subForm;
	}
	
	public void cancelEditing() {
		changeEditingMode(false);
	}
	public void stopEditing() {
		changeEditingMode(true);
	}
	
	private void changeEditingMode(boolean stopEditing) {
		if (this.subForm != null) {
			TableCellEditor tableCellEditor = this.subForm.getTableCellEditorProvider().getTableCellEditor(
					this.subForm.getJTable(), this.subForm.getJTable().getSelectedRow(), getEntityField());
			if (tableCellEditor != null) {
				if (stopEditing) {
					tableCellEditor.stopCellEditing();					
				} else {
					tableCellEditor.cancelCellEditing();
				}					
			}
		}
	}

	@Override
	public void clear() {
		this.setField(this.getEntityField().getNullField());
		this.dataLanguageMap.clear();
	}
}
