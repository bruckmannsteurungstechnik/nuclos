//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.detail;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.SwingUtilities;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;

import org.apache.commons.lang.ObjectUtils;
import org.apache.log4j.Logger;
import org.nuclos.api.context.ScriptContext;
import org.nuclos.client.common.DetailsSubFormController;
import org.nuclos.client.common.EntityCollectController;
import org.nuclos.client.common.ISplitViewController;
import org.nuclos.client.common.LafParameterHelper;
import org.nuclos.client.common.LafParameterHelper.NotifyListener;
import org.nuclos.client.common.LafParameterHelper.SplitViewSettingsLafParameterEditor;
import org.nuclos.client.common.MatrixController;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.Utils;
import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.masterdata.valuelistprovider.DefaultValueProvider;
import org.nuclos.client.scripting.ScriptEvaluator;
import org.nuclos.client.scripting.context.CollectControllerScriptContext;
import org.nuclos.client.scripting.context.SubformControllerScriptContext;
import org.nuclos.client.scripting.expressions.ExpressionParser;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.client.ui.collect.CommonController;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableComponentTableCellEditor;
import org.nuclos.client.ui.collect.component.CollectableListOfValues;
import org.nuclos.client.ui.collect.component.LabeledCollectableComponentWithVLP;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModel;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelAdapter;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelEvent;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelListener;
import org.nuclos.client.ui.collect.component.model.DetailsComponentModel;
import org.nuclos.client.ui.collect.component.model.DetailsComponentModelEvent;
import org.nuclos.client.ui.collect.component.model.DetailsEditModel;
import org.nuclos.client.ui.collect.subform.Column;
import org.nuclos.client.ui.collect.subform.RefreshValueListAction;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.client.ui.collect.toolbar.DeleteOrRestoreToggleAction;
import org.nuclos.client.ui.collect.toolbar.NuclosToolBarActions;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.LafParameter;
import org.nuclos.common.LafParameterStorage;
import org.nuclos.common.NuclosScript;
import org.nuclos.common.SplitViewSettings;
import org.nuclos.common.UID;
import org.nuclos.common.collect.ControllerPresentation;
import org.nuclos.common.collect.NuclosToolBarItems;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * Controller for the Details panel.
 */
public class DetailsController<PK,Clct extends Collectable<PK>> extends CommonController<PK,Clct> {
	
	private static final Logger LOG = Logger.getLogger(DetailsController.class);

	//

	/**
	 * Cannot be final because set to null in close(). (tp)
	 */
	private CollectableComponentModelListener ccmlistener = new CollectableComponentModelAdapter() {
		@Override
		public void collectableFieldChangedInModel(CollectableComponentModelEvent ev) {
			final CollectController<PK,Clct> cc = getCollectController();
			//assert cc.getCollectStateModel().getOuterState() == CollectState.OUTERSTATE_DETAILS;

			// Note that we don't check ev.collectableFieldHasChanged() here, as we want to set "details changed"
			// on every change, not only valid changes, esp. the case that the user starts typing a date
			// in an empty date field.
			cc.detailsChanged(ev.getCollectableComponentModel());
		}

		@Override
		public void valueToBeChanged(DetailsComponentModelEvent ev) {
			getCollectController().detailsChanged(ev.getCollectableComponentModel());
		}
	};

	private final ScriptObserver so = new ScriptObserver();
	
	private ScriptEvaluator scriptEvaluator;

	private DeleteOrRestoreToggleAction _actDeleteCurrentCollectable;

	private final List<DetailsSubFormController<PK,?>> sfcs = new ArrayList<DetailsSubFormController<PK,?>>();
	
	private final List<MatrixController> lstMController = new ArrayList<MatrixController>();

	private final CollectableComponentModelListener mdlListener = new CollectableComponentModelAdapter() {
		@Override
		public void collectableFieldChangedInModel(final CollectableComponentModelEvent ev) {
			final CollectableComponentModel model = ev.getCollectableComponentModel();
			if (!model.isInitializing()) {
				//@see NUCLOSINT-1572. we removed using invokeLater here.
				UID source = ev.getCollectableComponentModel().getFieldUID();
				setSubformDefaultValues(null, source,null);
				model.getMappingOfRefreshValueListParamaters();
				refreshMatrix(source, model.getField());
				

				//@see NUCLOSINT-1572. we removed using invokeLater here.
				process(ExpressionParser.getExpression(getCollectController().getEntityUid(), model.getFieldUID()));
			}
		}
	};
	
	private final NotifyListener<SplitViewSettings, String> splitViewSettingsListener = new NotifyListener<SplitViewSettings, String>() {
		@Override
		public void valueChanged(
				LafParameter<SplitViewSettings, String> parameter,
				LafParameterStorage storage, UID entityUid,
				SplitViewSettings value) {
			((ISplitViewController)getCollectController()).updateSplitViewSettings(storage);
		}
	};

	public DetailsController(CollectController<PK,Clct> cc) {
		super(cc);
	}
	
	
	private ScriptEvaluator getScriptEvaluator() {
		if (scriptEvaluator == null) {
			scriptEvaluator = ScriptEvaluator.getInstance();
		}
		return scriptEvaluator;
	}

	public DeleteOrRestoreToggleAction getDeleteCurrentCollectableAction() {
		if (_actDeleteCurrentCollectable == null) {
			_actDeleteCurrentCollectable = NuclosToolBarActions.DELETE.init(new DeleteOrRestoreToggleAction() {

				@Override
		        public void actionPerformed(ActionEvent ev) {
					getCollectController().cmdDeleteCurrentCollectableInDetails();
				}
			});
		}
		return _actDeleteCurrentCollectable;
	}

	/**
	 * display the number of the current record and the total number of records in the details panel's status bar
	 *
	 * TODO: Make this private again.
	 */
	public void displayCurrentRecordNumberInDetailsPanelStatusBar(){
		final CollectController<PK,Clct> cc = getCollectController();
		getDetailsPanel().setStatusBarText(getSpringLocaleDelegate().getMessage("CollectController.8","Datensatz") +
				" " + (cc.getResultTable().getSelectedRow() +1 ) + "/" + cc.getResultTable().getRowCount());
	}

	@Override
	protected boolean isSearchPanel() {
		return false;
	}

	/**
	 * TODO: Make protected again.
	 */
	@Override
	public Collection<DetailsComponentModel> getCollectableComponentModels() {
		if (null == getCollectController()) {
			LOG.warn("unable to receive collectable components because assigned collect controller is net set");
			return Collections.emptyList();
		}
		final DetailsEditModel dem = getCollectController().getDetailsPanel().getEditModel();
		// could be null if controller is closing
		if (dem == null) {
			return Collections.emptyList();
		}
		return dem.getCollectableComponentModels();
	}

	@Override
	protected CollectableComponentModelListener getCollectableComponentModelListener() {
		return this.ccmlistener;
	}

	@Override
	protected void addAdditionalChangeListeners() {
		getCollectController().addAdditionalChangeListenersForDetails();
		for (CollectableComponentModel m : getCollectableComponentModels()) {
			 m.addCollectableComponentModelListener(null, mdlListener);
		}
	}

	@Override
	protected void removeAdditionalChangeListeners() {
		getCollectController().removeAdditionalChangeListenersForDetails();
		for (CollectableComponentModel m : getCollectableComponentModels()) {
			m.removeCollectableComponentModelListener(mdlListener);
		}
	}

	private DetailsPanel getDetailsPanel() {
		return getCollectController().getDetailsPanel();
	}

	/**
	 * TODO: Make this private again.
	 */
	public void setupDetailsPanel() {
		final CollectController<PK,Clct> cc = getCollectController();
		// Details panel:
		// action: Save
		final DetailsPanel pnlDetails = this.getDetailsPanel();
//		pnlDetails.btnSave.setAction(cc.getSaveAction());
		pnlDetails.registerToolBarAction(NuclosToolBarItems.SAVE, cc.getSaveAction());

		// action: Refresh
//		pnlDetails.btnRefreshCurrentCollectable.setAction(cc.getRefreshCurrentCollectableAction());
		pnlDetails.registerToolBarAction(NuclosToolBarItems.REFRESH, cc.getRefreshCurrentCollectableAction());
		
		// action: Delete
//		pnlDetails.btnDelete.setAction(this.actDeleteCurrentCollectable);
		pnlDetails.registerToolBarAction(NuclosToolBarItems.DELETE, this.getDeleteCurrentCollectableAction());

		// action: New
//		pnlDetails.btnNew.setAction(cc.getNewAction());
		pnlDetails.registerToolBarAction(NuclosToolBarItems.NEW, cc.getNewAction());

		// action: Clone
//		pnlDetails.btnClone.setAction(cc.getCloneAction());
		pnlDetails.registerToolBarAction(NuclosToolBarItems.CLONE, cc.getCloneAction());

		// action: Open in new tab
//		pnlDetails.btnOpenInNewTab.setAction(cc.getOpenInNewTabAction());
		pnlDetails.registerToolBarAction(NuclosToolBarItems.OPEN_IN_NEW_TAB, cc.getOpenInNewTabAction());

		// action: Bookmark
//		pnlDetails.btnBookmark.setAction(cc.getBookmarkAction());
		pnlDetails.registerToolBarAction(NuclosToolBarItems.BOOKMARK, cc.getBookmarkAction());
		
		// Action Show OnlineHelp
		if (!E.isNuclosEntity(cc.getEntityUid()))
			pnlDetails.registerToolBarAction(NuclosToolBarItems.HELP, cc.getOnlineHelpAction());
		
		if (cc.getViewMode() == ControllerPresentation.SPLIT_DETAIL) {
			pnlDetails.btnSplitOptions.addActionListener(new ActionListener() {	
				@Override
				public void actionPerformed(ActionEvent e) {
					((ISplitViewController)cc).switchSplitViewMode();
				}
			});
			final SplitViewSettingsLafParameterEditor editor = new LafParameterHelper.SplitViewSettingsLafParameterEditor(
					LafParameter.nuclos_LAF_Split_View_Settings, cc.getEntityUid(), pnlDetails.btnSplitOptions);
			LafParameterHelper.installPopup(editor, splitViewSettingsListener);
			
		} else {
			pnlDetails.registerToolBarAction(NuclosToolBarItems.RECORDNAV_FIRST, cc.getFirstAction());
			pnlDetails.registerToolBarAction(NuclosToolBarItems.RECORDNAV_PREVIOUS, cc.getPreviousAction());
			pnlDetails.registerToolBarAction(NuclosToolBarItems.RECORDNAV_NEXT, cc.getNextAction());
			pnlDetails.registerToolBarAction(NuclosToolBarItems.RECORDNAV_LAST, cc.getLastAction());
	
			// navigation actions:
			Action actFirst = new AbstractAction(null, MainFrame.resizeAndCacheIcon(Icons.getInstance().getIconFirstWhite16(), DetailsPanel.recordNavIconSize)) {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					cc.getFirstAction().actionPerformed(arg0);
				}
				@Override
				public boolean isEnabled() {
					return cc.getFirstAction().isEnabled();
				}
			};
			pnlDetails.btnFirst.setAction(actFirst);
			cc.getFirstAction().addPropertyChangeListener(new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					if ("enabled".equals(evt.getPropertyName())) {
						pnlDetails.btnFirst.setEnabled((Boolean) evt.getNewValue());
					}
				}
			});
			
			Action actLast = new AbstractAction(null, MainFrame.resizeAndCacheIcon(Icons.getInstance().getIconLastWhite16(), DetailsPanel.recordNavIconSize)) {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					cc.getLastAction().actionPerformed(arg0);
				}
				@Override
				public boolean isEnabled() {
					return cc.getLastAction().isEnabled();
				}
			};
			pnlDetails.btnLast.setAction(actLast);
			cc.getLastAction().addPropertyChangeListener(new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					if ("enabled".equals(evt.getPropertyName())) {
						pnlDetails.btnLast.setEnabled((Boolean) evt.getNewValue());
					}
				}
			});
			
			Action actPrevious = new AbstractAction(null, MainFrame.resizeAndCacheIcon(Icons.getInstance().getIconPreviousWhite16(), DetailsPanel.recordNavIconSize)) {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					cc.getPreviousAction().actionPerformed(arg0);
				}
				@Override
				public boolean isEnabled() {
					return cc.getPreviousAction().isEnabled();
				}
			};
			pnlDetails.btnPrevious.setAction(actPrevious);
			cc.getPreviousAction().addPropertyChangeListener(new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					if ("enabled".equals(evt.getPropertyName())) {
						pnlDetails.btnPrevious.setEnabled((Boolean) evt.getNewValue());
					}
				}
			});
			
			Action actNext = new AbstractAction(null, MainFrame.resizeAndCacheIcon(Icons.getInstance().getIconNextWhite16(), DetailsPanel.recordNavIconSize)) {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					cc.getNextAction().actionPerformed(arg0);
				}
				@Override
				public boolean isEnabled() {
					return cc.getNextAction().isEnabled();
				}
			};
			pnlDetails.btnNext.setAction(actNext);
			cc.getNextAction().addPropertyChangeListener(new PropertyChangeListener() { 
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					if ("enabled".equals(evt.getPropertyName())) {
						pnlDetails.btnNext.setEnabled((Boolean) evt.getNewValue());
					}
				}
			});
		}
		
		UIUtils.readSplitPaneStateFromPrefs(getCurrentLayoutUid(), cc.getPreferences(), getDetailsPanel());
		
	
		
	}

	@Override
	public void close() {
		if (!isClosed()) {
			final DetailsPanel pnlDetails = this.getDetailsPanel();
			pnlDetails.closeToolBar();
//			pnlDetails.btnSave.setAction(null);
//			pnlDetails.btnRefreshCurrentCollectable.setAction(null);
//			pnlDetails.btnDelete.setAction(null);
//			pnlDetails.btnNew.setAction(null);
//			pnlDetails.btnClone.setAction(null);
//			pnlDetails.btnOpenInNewTab.setAction(null);
//			pnlDetails.btnBookmark.setAction(null);

			pnlDetails.btnFirst.setAction(null);
			pnlDetails.btnLast.setAction(null);
			pnlDetails.btnPrevious.setAction(null);
			pnlDetails.btnNext.setAction(null);
			pnlDetails.btnSplitOptions.close();

			UIUtils.writeSplitPaneStateToPrefs(getCurrentLayoutUid(), getCollectController().getPreferences(), getDetailsPanel());

			ccmlistener = null;
			for (DetailsSubFormController<PK,?> sfc: sfcs) {
				sfc.close();
			}
			sfcs.clear();
			for (MatrixController mc: lstMController) {
				mc.close();
			}
			lstMController.clear();
			_actDeleteCurrentCollectable = null;

			super.close();
		}
	}

	/**
	 * TODO: Make this private again.
	 */
	public void updateStatusBarIfNecessary() {
		//log.debug("CollectController.updateStatusBarIfNecessary");
		if (getCollectController().getCollectState().isDetailsModeMultiViewOrEdit()) {
			this.showMultiEditChangeInStatusBar();
		}
	}

	/**
	 * §precondition CollectController.this.getCollectState().isDetailsModeMultiViewOrEdit()
	 */
	private void showMultiEditChangeInStatusBar() {
		final CollectController<PK,Clct> cc = getCollectController();
		if (!cc.getCollectState().isDetailsModeMultiViewOrEdit()) {
			throw new IllegalStateException();
		}

		final String sChange = cc.getMultiEditChangeString();
		final String sStatus = getSpringLocaleDelegate().getMessage(
				"CollectController.5","\u00c4nderung")+ ": " + (StringUtils.looksEmpty(sChange) ? "<"
				+ getSpringLocaleDelegate().getMessage("CollectController.21","keine") + ">" : sChange);
		this.getDetailsPanel().setStatusBarText(sStatus);
	}
	
	public void setMatrixController(Collection<MatrixController> colController) {
		this.lstMController.clear();
		this.lstMController.addAll(colController);
	}

	private Map<DetailsSubFormController<PK,?>, DetailsSubFormController<PK,?>> parentsOfSubform() {
		Map<DetailsSubFormController<PK,?>,DetailsSubFormController<PK,?>> mp = new ConcurrentHashMap<DetailsSubFormController<PK,?>, DetailsSubFormController<PK,?>>();
		for (final DetailsSubFormController<PK,?> sfc : this.sfcs) {
			for (DetailsSubFormController<PK,?> psfc : DetailsController.this.sfcs) {
				if (psfc.getEntityAndForeignKeyField().getEntity().equals(sfc.getSubForm().getParentSubForm())) {
					mp.put(sfc, psfc);
				}

			}
		}
		return mp;
	}
	public void setSubFormControllers(Collection<DetailsSubFormController<PK,CollectableEntityObject<PK>>> sfcs) {
		this.sfcs.clear();
		this.sfcs.addAll(sfcs);
		// fill subform assignment to avoid redundant loops
		// FIXME this should be moved to a class field and initially available
		final Map<DetailsSubFormController<PK,?>,DetailsSubFormController<PK,?>> parentsFirstLevel = new HashMap<DetailsSubFormController<PK,?>, DetailsSubFormController<PK,?>>();
		for (final DetailsSubFormController<PK,?> sfc : this.sfcs) {
			for (DetailsSubFormController<PK,?> psfc : DetailsController.this.sfcs) {
				if (psfc.getEntityAndForeignKeyField().getEntity().equals(sfc.getSubForm().getParentSubForm())) {
					parentsFirstLevel.put(sfc, psfc);
				}

			}
		}
		
		for (final DetailsSubFormController<PK,?> sfc : this.sfcs) {
			if (null != sfc.getSubForm().getEditEnabledScript()) {
				LOG.debug("attached client rule evaluation: editable yes/no");
				sfc.setIsFieldEditableEvaluator(new NuclosScriptIsFieldEditable(parentsFirstLevel.get(sfc), sfc.getSubForm().getEditEnabledScript()));
			}
			sfc.getSubForm().getSubformTable().getModel().addTableModelListener(new TableModelListener() {
				@Override
				public void tableChanged(final TableModelEvent e) {
					switch (e.getType()) {
						case TableModelEvent.INSERT:													
							SwingUtilities.invokeLater(new Runnable() {
								@Override
								public void run() {
									setSubformDefaultValues(null, null,e.getLastRow());

									if (sfc != null && !sfc.isClosed()) {
										if (sfc.getSubForm().getParentSubForm() != null) {
											for (DetailsSubFormController<PK,?> psfc : DetailsController.this.sfcs) {
												if (psfc.getEntityAndForeignKeyField().getEntity().equals(sfc.getSubForm().getParentSubForm())) {
													evaluateNewRow(psfc, sfc, e.getFirstRow());
												}
											}
										}
										else {
											evaluateNewRow(null, sfc, e.getFirstRow());
										}
									}
								}
							});	
							break;
						case TableModelEvent.UPDATE:
							if (e.getColumn() >= 0) {
								final CollectableEntityField column = sfc.getCollectableTableModel().getCollectableEntityField(e.getColumn());
								
								final String key = ExpressionParser.getExpression(column.getEntityUID(), column.getUID());

								//@see NUCLOSINT-1572. we removed using invokeLater here.
								setSubformDefaultValues(column.getEntityUID(), column.getUID(),null);

								if (sfc != null && !sfc.isClosed()) {
									if (sfc.getSubForm().getParentSubForm() != null) {
										for (DetailsSubFormController<PK,?> psfc : DetailsController.this.sfcs) {
											if (psfc.getEntityAndForeignKeyField().getEntity().equals(sfc.getSubForm().getParentSubForm())) {
												process(key, psfc, sfc, e.getFirstRow());
											}
										}
									} else {
										process(key, null, sfc, e.getFirstRow());
									}
								}
							}
							break;
						case TableModelEvent.DELETE:
							for (int i = 0; i < sfc.getCollectableTableModel().getColumnCount(); i++) {
								final CollectableEntityField column = sfc.getCollectableTableModel().getCollectableEntityField(i);
								final EntityMeta<?> meta = MetaProvider.getInstance().getEntity(column.getEntityUID());
								final String key = ExpressionParser.getExpression(meta.getUID(), column.getUID());

								//@see NUCLOSINT-1572. we removed using invokeLater here.
								setSubformDefaultValues(meta.getUID(), column.getUID(),null);

								if (sfc != null && !sfc.isClosed()) {
									if (sfc.getSubForm().getParentSubForm() != null) {
										for (DetailsSubFormController<PK,?> psfc : DetailsController.this.sfcs) {
											if (psfc.getEntityAndForeignKeyField().getEntity().equals(sfc.getSubForm().getParentSubForm())) {
												process(key, psfc, sfc, e.getFirstRow());
											}
										}
									}
									else {
										process(key, null, sfc, e.getFirstRow());
									}
								}
							}
							break;
					}
				}
			});
		}
	}

	private void process(final String sourceExpression) {
		if (getCollectController().isDetailsChangedIgnored()) {
			//return; //@see NUCLOSINT-1572. stopEditing will cause no update of groovy script changes on save.
		}

		ScriptContext ctx = new CollectControllerScriptContext(getCollectController(), sfcs);
		for (CollectableComponent c : getDetailsPanel().getEditView().getCollectableComponents()) {
			FieldMeta<?> fieldmeta = MetaProvider.getInstance().getEntityField(c.getEntityField().getUID());
			if (fieldmeta.getCalculationScript() != null) {
				if (ExpressionParser.contains(fieldmeta.getCalculationScript(), sourceExpression)) {
					//if(!this.so.containsScript(fieldmeta.getCalculationScript())) { @see NUCLOS-1358
					{
						//so.putScript(fieldmeta.getCalculationScript());

						CollectableComponentModel m = getDetailsPanel().getEditModel().getCollectableComponentModelFor(fieldmeta.getUID());
						
						Object oOld = null;
						if(fieldmeta.getForeignEntityField() != null) {
							oOld = m.getField().getValueId();	
						}
						else {
							oOld = m.getField().getValue();
						}

						Object o = null;
						try {
							o = getScriptEvaluator().eval(fieldmeta.getCalculationScript(), ctx);
						} catch (InvocationTargetException e) {
							LOG.warn("Failed to evaluate script expression: " + e, e.getCause());
							o = m.getField().getValue();
						} catch (Exception e) {
							LOG.warn("Failed to evaluate script expression: " + e, e);
							o = m.getField().getValue();
						}
						if(ObjectUtils.equals(o, oOld)) {
							// NUCLOS-3555 continue not return
							continue;
						}
						setCollectableComponentValue(c, o, false);
					}
				}
			}
			
			c.setComponentState(ctx, sourceExpression);
			c.setBackgroundColor(ctx, sourceExpression);
		}
	}

	private void process(final String sourceExpression, final DetailsSubFormController<PK,?> psf, 
			final DetailsSubFormController<PK,?> sf, final Integer row) {
		if (getCollectController().isDetailsChangedIgnored()) {
			//return; //@see NUCLOSINT-1572. stopEditing will cause no update of groovy script changes on save.
		}

		process(sourceExpression);
		final SubForm subform = sf.getSubForm();
		if (subform != null && subform.getParentSubForm() != null) {
			for (DetailsSubFormController<PK,?> sfc : sfcs) {
				if (sfc.getEntityAndForeignKeyField().getEntity().equals(subform.getParentSubForm())) {
					process(sourceExpression, null, sfc, sfc.getSubForm().getJTable().getSelectedRow());
				}
			}
		}
		for (int i = 0; i < sf.getCollectableTableModel().getColumnCount(); i++) {
			final CollectableEntityField cef = sf.getCollectableTableModel().getCollectableEntityField(i);
			final FieldMeta<?> fieldmeta = MetaProvider.getInstance().getEntityField(cef.getUID());
			if (fieldmeta.getCalculationScript() != null) {
				if (ExpressionParser.contains(fieldmeta.getCalculationScript(), sourceExpression)) {
					if (sf.getCollectableTableModel().getRowCount() > row) {
						//if(!this.so.containsScript(fieldmeta.getCalculationScript())) {
						{
							final int col = i;
							// we have to put it in invoke later. otherwise we did not get the right clct. @see NUCLOS-1367
							SwingUtilities.invokeLater(new Runnable() {
								@Override
								public void run() {
									Object o = null;
									try {
										so.putScript(fieldmeta.getCalculationScript());
										o = getScriptEvaluator().eval(fieldmeta.getCalculationScript(),
												new SubformControllerScriptContext(getCollectController(), psf, sf, sf.getCollectableTableModel().getRow(row)));
									} catch (InvocationTargetException e) {
										LOG.warn("Failed to evaluate script expression: " + e, e.getCause());
										o = null;
									} catch (Exception e) {
										LOG.warn("Failed to evaluate script expression: " + e, e);
										o = null;
									}
									setSubFormValue(sf, row, col, cef, o);
								}
							});
						}
					}
				}
			}
		}
	}

	public void evaluateNewCollectable() {
		for (CollectableComponent c : getDetailsPanel().getEditView().getCollectableComponents()) {
			FieldMeta<?> fieldmeta = MetaProvider.getInstance().getEntityField(c.getEntityField().getUID());
			if (fieldmeta.getCalculationScript() != null) {
				CollectableComponentModel m = getDetailsPanel().getEditModel().getCollectableComponentModelFor(fieldmeta.getUID());
				Object o = null;
				try {
					o = getScriptEvaluator().eval(fieldmeta.getCalculationScript(), 
							new CollectControllerScriptContext(getCollectController(), sfcs));
				} catch (InvocationTargetException e) {
					LOG.warn("Failed to evaluate script expression: " + e, e.getCause());
					o = m.getField().getValue();
				} catch (Exception e) {
					LOG.warn("Failed to evaluate script expression: " + e, e);
					o = m.getField().getValue();
				}
				setCollectableComponentValue(c, o, true);
			}
		}
	}

	private void evaluateNewRow(final DetailsSubFormController<PK,?> psf, final DetailsSubFormController<PK,?> sf, final Integer row) {
		for (int i = 0; i < sf.getCollectableTableModel().getColumnCount(); i++) {
			CollectableEntityField cef = sf.getCollectableTableModel().getCollectableEntityField(i);
			FieldMeta<?> fieldmeta = MetaProvider.getInstance().getEntityField(cef.getUID());
			if (fieldmeta.getCalculationScript() != null) {
				Object o = null;
				try {
					o = getScriptEvaluator().eval(fieldmeta.getCalculationScript(),
							new SubformControllerScriptContext(getCollectController(), psf, sf, sf.getSelectedCollectable()));
				} catch (InvocationTargetException e) {
					LOG.warn("Failed to evaluate script expression: " + e, e.getCause());
					o = null;
				} catch (Exception e) {
					LOG.warn("Failed to evaluate script expression: " + e, e);
					o = null;
				}
				setSubFormValue(sf, row, i, cef, o);
			}
			process(ExpressionParser.getExpression(cef.getEntityUID(), cef.getUID()), psf, sf, row);
		}
	}

	private void setCollectableComponentValue(CollectableComponent c, Object value, boolean bNew) {
		if (value == ScriptContext.CANCEL) {
			return;
		}
		if (c instanceof LabeledCollectableComponentWithVLP) {
			setValueByCompWithVLP((LabeledCollectableComponentWithVLP)c, (PK)value, bNew);
			
		} else {
			if (!bNew) {
				c.getModel().setField(new CollectableValueField(value));				
			} else {
				c.getModel().setFieldInitial(new CollectableValueField(value));				
			}
		}
	}

	private void setSubFormValue(DetailsSubFormController<PK,?> ctl, int row, int column, CollectableEntityField cef, Object value) {
		if (value == ScriptContext.CANCEL) {
			return;
		}

		TableCellEditor editor = ctl.getTableCellEditor(ctl.getSubForm().getJTable(), row, cef);
		if (editor instanceof CollectableComponentTableCellEditor) {
			((CollectableComponentTableCellEditor) editor).setLastEditingRow(row);
			CollectableComponent c = ((CollectableComponentTableCellEditor) editor).getCollectableComponent();
			if (c instanceof LabeledCollectableComponentWithVLP) {
				setValueByCompWithVLP((LabeledCollectableComponentWithVLP) c, (PK)value, false);
			
			} else {
				c.getModel().setField(new CollectableValueField(value));
			}
			editor.stopCellEditing();
			try {
				ctl.getCollectableTableModel().setValueAt(c.getField(), row, column);
			}
			catch (CollectableFieldFormatException e) {
				LOG.warn("Failed to set script calculated value.", e);
			}
		}
		else {
			ctl.getCollectableTableModel().setValueAt(new CollectableValueField(value), row, column);
		}
	}

	private void setValueByCompWithVLP(LabeledCollectableComponentWithVLP c, PK value, boolean bNew) {
		if (value != null) {
			Collectable<PK> clct;
			try {
				clct = Utils.getReferencedCollectable(c.getEntityField().getEntityUID(), c.getEntityField().getUID(), value);
				
				if (c instanceof CollectableListOfValues) {
					((CollectableListOfValues)c).acceptLookedUpCollectable(clct);

				} else {
					Object oForeignValue = Utils.getRepresentation(c.getEntityField().getUID(), clct);
					if (!bNew) {
						c.getModel().setField(new CollectableValueIdField(value, oForeignValue));						
					} else {
						c.getModel().setFieldInitial(new CollectableValueIdField(value, oForeignValue));						
					}
				}
				
			}
			catch (CommonBusinessException e) {
				LOG.warn("Failed to set script calculated value.", e);
			}
		} else {
			if (!bNew) {
				c.getModel().setField(CollectableValueIdField.NULL);			
			} else {
				c.getModel().setFieldInitial(CollectableValueIdField.NULL);
			}
		}
	}
	
	public List<DetailsSubFormController<PK,?>> getSubFormControllers() {
		return new ArrayList<DetailsSubFormController<PK,?>>(this.sfcs);
	}
	
	public DetailsSubFormController<PK,?> getSubFormController(UID uid) {
		for (DetailsSubFormController<PK,?> sfc : sfcs) {
			if (sfc.getSubForm() != null && sfc.getSubForm().getEntityUID().equals(uid)) {
				return sfc;
			}
		}
		return null;
	}


	private void setSubformDefaultValues(UID sourceEntity, UID sourceField,Integer rowIndex) {
		for (DetailsSubFormController<PK,?> sfc : sfcs) {
			if (sfc.isClosed()) {
				continue;
			}
			AbstractTableModel subFormTableModel = (AbstractTableModel)sfc.getCollectableTableModel();
			TableModelListener[] savedListeners = subFormTableModel.getTableModelListeners();
			// save and remove listeners to prevent change events (which reset pending row)
			if (savedListeners != null) {
				for (int i=0; i < savedListeners.length; i++) {
					subFormTableModel.removeTableModelListener(savedListeners[i]);
				}
			}
			for (CollectableEntityField cef : getFieldsWithDefaultValueProvider(sfc, sourceEntity, sourceField)) {
				//for (int j = 0; j < sfc.getCollectableTableModel().getRowCount(); j++) { //@see NUCLOS-1368
				int j = sfc.getSubForm().getSubformTable().getSelectedRow();
				
				if(rowIndex==null)
				{
					// use as default the selected row
				}else
				{
					j = rowIndex;
				}
				 
				if (j != -1 && j < sfc.getCollectableTableModel().getRowCount())
				{
					CollectableField cfOld = sfc.getCollectableTableModel().getValueAt(j, sfc.getCollectableTableModel().getColumn(cef));
					if (cfOld != null && !cfOld.isNull()) {
						continue;
					}

					TableCellEditor editor = sfc.getTableCellEditor(sfc.getSubForm().getJTable(), j, cef);
					if (editor instanceof CollectableComponentTableCellEditor) {
						CollectableComponent c = ((CollectableComponentTableCellEditor) editor).getCollectableComponent();
						if (c instanceof LabeledCollectableComponentWithVLP) {
							LabeledCollectableComponentWithVLP vlp = (LabeledCollectableComponentWithVLP) c;

							if (vlp.getValueListProvider() != null && vlp.getValueListProvider() instanceof DefaultValueProvider) {
								DefaultValueProvider dvp = (DefaultValueProvider) vlp.getValueListProvider();

								if (dvp.isSupported()) {
									CollectableField cf = dvp.getDefaultValue();

									if (cf.getValueId() != null) {
										setValueByCompWithVLP((LabeledCollectableComponentWithVLP) c, (PK) cf.getValueId(), false);
										
									} else {
										c.getModel().setField(cf);
									}
									editor.stopCellEditing();
									try {
										sfc.getCollectableTableModel().setValueAt(c.getField(), j, sfc.getCollectableTableModel().getColumn(cef));
									}
									catch (CollectableFieldFormatException e) {
										LOG.warn("Failed to set default value.", e);
									}
								}
							}
						}
					}
				}
			}
			// restore saved listeners
			if (savedListeners != null) {
				for (int i=0; i < savedListeners.length; i++) {
					subFormTableModel.addTableModelListener(savedListeners[i]);
				}
			}
		}	
	}
	
	private void refreshMatrix(UID uidField, CollectableField cf) {
		CollectController ctrl = this.getCollectController();
		if(ctrl instanceof EntityCollectController) {
			EntityCollectController ectrl = (EntityCollectController)ctrl;
			Map<UID, MatrixController> mpController = ectrl.getMatrixController();
			for(UID uid : mpController.keySet()) {
				MatrixController mc = mpController.get(uid);
				try {
					mc.setParameterValueListProvider(uidField, cf);
				} catch (CommonBusinessException e) {
					LOG.warn(e);
				}
			}
		}
	}

	private List<CollectableEntityField> getFieldsWithDefaultValueProvider(DetailsSubFormController<PK,?> sfc, UID sourceEntity, UID sourceField) {
		final List<CollectableEntityField> result = new ArrayList<CollectableEntityField>();
		for (Column col : sfc.getSubForm().getColumns()) {
			if (sourceField != null) {
				if (col.getRefreshValueListActions() != null && col.getValueListProvider() != null) {
					if (col.getValueListProvider() instanceof DefaultValueProvider && ((DefaultValueProvider)col.getValueListProvider()).isSupported()) {
						for (RefreshValueListAction act : col.getRefreshValueListActions()) {
							UID target = act.getTargetComponentUID();
							if (LangUtils.equal(sourceEntity, act.getParentComponentEntityUID()) && LangUtils.equal(sourceField, act.getParentComponentUID())) {
								for (int i = 0; i < sfc.getCollectableTableModel().getColumnCount(); i++) {
									CollectableEntityField cef = sfc.getCollectableTableModel().getCollectableEntityField(i);
									if (LangUtils.equal(target, cef.getUID())) {
										result.add(cef);
									}
								}
							}
						}
					}
				}
			}
			else {
				if (col.getValueListProvider() != null) {
					if (col.getValueListProvider() instanceof DefaultValueProvider && ((DefaultValueProvider)col.getValueListProvider()).isSupported()) {
						for (int i = 0; i < sfc.getCollectableTableModel().getColumnCount(); i++) {
							CollectableEntityField cef = sfc.getCollectableTableModel().getCollectableEntityField(i);
							if (LangUtils.equal(col.getName(), cef.getUID())) {
								result.add(cef);
							}
						}
					}
				}
			}
		}
		return result;
	}
	
	static class ScriptObserver {
		
		private final Set<NuclosScript> setScript;
		
		public ScriptObserver() {
			setScript = new HashSet<NuclosScript>();
		}
		
		public void putScript(NuclosScript script) {
			setScript.add(script);
		}
		
		public boolean containsScript(NuclosScript script) {
			boolean contains = false;
			if (setScript.contains(script)) {
				setScript.clear();
				contains = true;
			}
			return contains;
		}
		
	}

	public CollectableComponentModelListener getMdlListener() {
		return mdlListener;
	}
	
}	// class DetailsController
