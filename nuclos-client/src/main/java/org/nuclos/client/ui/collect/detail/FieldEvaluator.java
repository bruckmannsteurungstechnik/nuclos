//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>
package org.nuclos.client.ui.collect.detail;

import org.nuclos.client.common.AbstractDetailsSubFormController;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntityField;

/**
 * field evaluation for {@link CollectableEntityField}
 * 
 * can be used if the parent {@link CollectController} is needed to apply
 * certain evaluations 
 * 
 * @param <T> evaluation result
 * 
 * @author Moritz Neuhaeuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public interface FieldEvaluator<T> {
	
	/**
	 * evaluate
	 * 
	 * @param parentClctl	parent {@link CollectController}	
	 * @param subformCtl	{@link AbstractDetailsSubFormController} the current execution context
	 * @param collectable	{@link Collectable} i.e. row 
	 * @param collectableEntityField {@link CollectableEntityField} that is evaluated
	 * @return
	 */
	public T evaluate(
			final CollectController<?,?> parentClctl, 
			final AbstractDetailsSubFormController<?,?> subformCtl,
			final Collectable<?> collectable, 
			final CollectableEntityField collectableEntityField);
}
