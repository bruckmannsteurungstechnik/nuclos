//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import javax.swing.event.TableModelEvent;

import org.apache.commons.lang.NullArgumentException;
import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.masterdata.CollectableWithDependants;
import org.nuclos.client.ui.model.AbstractListTableModel;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.genericobject.ProxyList;

/**
 * <br>TableModel implementation for <code>Collectable</code>s
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version	01.00.00
 */

public class CollectableTableModelImpl<PK,Clct extends Collectable<PK>>
		extends AbstractListTableModel<Clct>
		implements CollectableTableModel<PK,Clct> {

	private static final Logger LOG = Logger.getLogger(CollectableTableModelImpl.class);
	
	/**
	 * the list of columns.
	 */
	private List<CollectableEntityField> lstclctefColumns;

	/*
	 * entity name
	 */
	private final UID baseEntityName;

	/**
	 * constructs a table model, using lstRows as implementation.
	 * 
	 * §precondition lstclct != null
	 * §postcondition this.getRows() == lstclct
	 * §postcondition this.getRowCount == lstclct.size()
	 * §postcondition this.getColumnCount == 0
	 * §postcondition this.getSortedColumn() == -1
	 * 
	 * @param lstclct is taken directly, not copied.
	 */
	public CollectableTableModelImpl(UID sEntityName, List<Clct> lstclct) {
		/* §todo check precondition */
		super(lstclct);
		this.lstclctefColumns = new ArrayList<CollectableEntityField>();
		this.baseEntityName = sEntityName;

		// assert we don't copy the list:
		assert this.getRows() == lstclct;
		assert this.getRowCount() == lstclct.size();
		assert this.getColumnCount() == 0;
	}

	@Override
	public UID getBaseEntityUid() {
		return baseEntityName;
	}

	/**
	 * §precondition lstclctefColumns != null
	 * §postcondition this.getColumnCount() == lstclctefColumns.size()
	 * §postcondition this.getSortedColumn() &lt; this.getColumnCount()
	 * 
	 * @param lstclctefColumns List&lt;CollectableEntityField&gt;
	 */
	@Override
	public void setColumns(List<? extends CollectableEntityField> lstclctefColumns) {
		if(lstclctefColumns == null) {
			throw new NullArgumentException("lstclctefColumns");
		}
		this.lstclctefColumns = new ArrayList<CollectableEntityField>(lstclctefColumns);
		super.fireTableStructureChanged();
		assert this.getColumnCount() == lstclctefColumns.size();
	}

	/**
	 * @return the number of columns
	 */
	@Override
	public int getColumnCount() {
		return this.lstclctefColumns.size();
	}

	@Override
	public CollectableField getValueAsCollectableField(Object oValue) {
		return (CollectableField) oValue;
	}

	/**
	 * adds <code>clctef</code> as column number <code>iColumn</code>
	 * @param iColumn
	 * @param clctef
	 */
	@Override
	public void addColumn(int iColumn, CollectableEntityField clctef) {
		this.lstclctefColumns.add(iColumn, clctef);
		super.fireTableChanged(new TableModelEvent(this, TableModelEvent.HEADER_ROW, TableModelEvent.HEADER_ROW, iColumn));
	}

	/**
	 * removes column number <code>iColumn</code>
	 * 
	 * §postcondition this.getSortedColumn() &lt; this.getColumnCount()
	 */
	@Override
	public void removeColumn(int iColumn) {
		this.lstclctefColumns.remove(iColumn);

		super.fireTableStructureChanged();
	}

	/**
	 * §precondition iColumn &gt;= 0 &amp;&amp; iColumn &lt; this.getColumnCount()
	 */
	@Override
	public CollectableEntityField getCollectableEntityField(int iColumn) {
		return lstclctefColumns.get(iColumn);
	}

	public UID getColumnFieldUid(int columnIndex) {
		return getCollectableEntityField(columnIndex).getUID();
	}
	
	@Override
	public int getColumn(CollectableEntityField field) {
		final int size = lstclctefColumns.size();
		for (int i = 0; i < size; ++i) {
			final CollectableEntityField f = lstclctefColumns.get(i);
			if (field.equals(f)) return i;
		}
		return -1;
	}

	/**
	 * @param fieldUid
	 * @return the index of the column with the given fieldname. -1 if none was found.
	 * 
	 * @deprecated Strongly consider to use {@link #getColumn(CollectableEntityField)} instead.
	 */
	@Override
	public int findColumnByFieldUid(UID fieldUid) {
		int result = -1;
		for (int iColumn = 0; iColumn < this.getColumnCount(); ++iColumn) {
			if (this.getCollectableEntityField(iColumn).getUID().equals(fieldUid)) {
				result = iColumn;
				break;
			}
		}
		return result;
	}

	/**
	 * @param oId id of the <code>Collectable</code> to find.
	 * @return the index of the column with the given id. -1 if none was found.
	 */
	@Override
	public int findRowById(PK oId) {
		int result = -1;

		if (this.getRows() instanceof ProxyList) {
			ProxyList<Object,?> proxylist = (ProxyList<Object,?>) this.getRows();
			return proxylist.getIndexById(oId);
		}

		for(int iRow = 0; iRow < this.getRowCount(); ++iRow) {
			if(LangUtils.equal(this.getCollectable(iRow).getId(), oId)) {
				result = iRow;
				break;
			}
		}
		return result;
	}

	/**
	 * Return the label of the underlying CollecableEntityField.
	 * 
	 * @param iColumn
	 * @return the name of column <code>iColumn</code>, as shown in the table header
	 */
	@Override
	public String getColumnName(int iColumn) {
		return getCollectableEntityField(iColumn).getLabel();
	}

	/**
	 * @deprecated Strongly consider to use {@link #getCollectableEntityField(int)} instead.
	 */
	@Override
	public Class<?> getColumnClass(int iColumn) {
		return CollectableField.class;
	}

	/**
	 * @param iRow
	 * @return the <code>Collectable</code> contained in row number <code>iRow</code>
	 * 
	 * @deprecated Use {@link #getRow(int)}.
	 */
	@Override
	public Clct getCollectable(int iRow) {
		return this.getRow(iRow);
	}
	
	@Override
	public Collection<Clct> getRows(int[] rows) {
		List<Clct> lstRows = getRows();
		
		if (lstRows instanceof ProxyList) {
			return ((ProxyList<PK, Clct>)lstRows).getBulk(rows);
		}
		
		Collection<Clct> result = new ArrayList<Clct>(rows.length);
		for (int i : rows) {
			result.add(lstRows.get(i));
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCollectable(int iRow, Clct clct) {
		try {
			this.getRows().set(iRow, clct);
			this.fireTableRowsUpdated(iRow, iRow);
		} catch (java.lang.ArrayIndexOutOfBoundsException ex) {
			LOG.error("SetCollectable at " + iRow + " failed! Collectable=" + clct, ex);
		}
	}

	@Override
	public List<Clct> getCollectables() {
		return Collections.unmodifiableList(this.getRows());
	}

	@Override
	public void setCollectables(List<Clct> lstclct) {
		this.setRows(lstclct); // fires tableDataChanged
	}

	/**
	 * @param clct
	 * @return the number of the row containing the Collectable with the given id
	 * @throws NoSuchElementException if no matching Collectable can be found
	 */
	private int indexOf(Collectable<PK> clct) {
		int result = 0;
		for (Iterator<? extends Collectable<PK>> iterator = this.getRows().iterator(); iterator.hasNext(); ++result) {
			final Collectable<PK> clct1 = iterator.next();
			if (clct.equals(clct1)) {
				break;
			}
		}
		if (result > this.getRows().size()) {
			throw new NoSuchElementException();
		}
		return result;
	}

	@Override
	public void remove(Collectable<PK> clct) {
		remove(indexOf(clct));
	}

	@Override
	public void setValueAt(Object oValue, int iRow, int iColumn) {
		final Collectable<PK> clct = getCollectable(iRow);
		final CollectableEntityField clctef = this.getCollectableEntityField(iColumn);
		CollectableField clctfValue = (CollectableField) oValue;

		// for compatibility reasons, we allow that oValue == null:
		if(clctfValue == null) {
			clctfValue = clctef.getNullField();
		}

		final UID fieldUid = clctef.getUID();
		
		if (clctef.isLocalized() && !clctef.isCalculated()) {
			if (clct.getDataLanguageMap() != null) {
				boolean retVal = false;
				
				if (clct.getDataLanguageMap() != null) {
					for (UID lang : clct.getDataLanguageMap().getLanguageMap().keySet()){
						if (clct.getDataLanguageMap().getLanguageMap().get(lang) != null && 
							!clct.getDataLanguageMap().getLanguageMap().get(lang).isFlagUnchanged()) {
							retVal = true;
							break;
						}
					}
				}
				
				if (retVal) {
					clct.setField(fieldUid, clctfValue);
					fireTableCellUpdated(iRow, iColumn);
				}
			}
		} else {
			if (!clct.getField(fieldUid).equals(clctfValue, true)) {
				clct.setField(fieldUid, clctfValue);
				fireTableCellUpdated(iRow, iColumn);
			}			
		}
	}

	/**
	 * @param iRow
	 * @param iColumn
	 * @return the <code>CollectableField</code> in the cell specified by <code>iRow</code> and <code>iColumn</code>
	 */
	@Override
	public CollectableField getValueAt(int iRow, int iColumn) {
		final Collectable<PK> clct = getCollectable(iRow);
		
		final CollectableEntityField clctefwe = getCollectableEntityField(iColumn);
		
		CollectableField result;
		// field of base entity
		if (clctefwe.getEntityUID().equals(getBaseEntityUid())) {
			try {
				return clct.getField(clctefwe.getUID());
			} catch (CommonFatalException cfe) {
				result = null;
			}
		}
		// field of subform entity
		else if (clct instanceof CollectableWithDependants){
			CollectableWithDependants<PK> cwd = (CollectableWithDependants<PK>)clct;
			result = getFieldValueOfSubEntity(clctefwe, cwd.getDepenentDataMap());
		}
		else {
			result = null;
		}
		
		return result;
	}

	@Override
	public void addCollectable(int iRow, Clct clct) {
		this.getRows().add(iRow, clct);
		this.fireTableDataChanged();
	}
	
	protected CollectableField getFieldValueOfSubEntity(final CollectableEntityField clctefwe, IDependentDataMap dependents) {
		MetaProvider metaProvider = MetaProvider.getInstance();
		
		for (IDependentKey dependentKey : dependents.getKeySet()) {
			UID refFieldEntityUid = metaProvider.getEntityField(dependentKey.getDependentRefFieldUID()).getEntity();
			if (refFieldEntityUid.equals(clctefwe.getEntityUID())) {
				//subform / key found
				final Collection<EntityObjectVO<?>> collmdvo = dependents.getData(dependentKey);
				List<Object> values = CollectionUtils.transform(collmdvo, new Transformer<EntityObjectVO<?>, Object>() {
					@Override
					public Object transform(EntityObjectVO<?> i) {
						return i.getFieldValue(clctefwe.getUID());
					}
				});
				return new CollectableValueField(values);
			}
		}
		// subform not found
		return new CollectableValueField(null);
		
	}
	
}  //  class CollectableTableModelImpl
