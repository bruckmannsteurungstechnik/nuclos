package org.nuclos.client.ui.collect.result;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FontMetrics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.model.CollectableEntityFieldBasedTableModel;
import org.nuclos.client.ui.table.CommonJTable;
import org.nuclos.common.ParameterProvider;

/**
 * Created by guenthse on 3/2/2017.
 */
public class FilterableTable extends CommonJTable {

	private JTextPane overlayTextPane;
	private JDialog overlayDialog;
	private FontMetrics fm;
	private Rectangle currentCellRect = null;

	private ChangeListener changeListener = e -> {
		if (currentCellRect != null) {
			SwingUtilities.invokeLater(() -> {
				JTable table = FilterableTable.this;
				Rectangle cellRect = new Rectangle(currentCellRect);

				Component viewPort = table.getParent();

				if (viewPort.isShowing()) {
					Point topLeft = viewPort.getLocationOnScreen();
					Point bottomRight = new Point(topLeft);
					bottomRight.translate(viewPort.getWidth(), viewPort.getHeight());

					System.out.println(viewPort.getLocationOnScreen() + " " + viewPort.getWidth() + " " + viewPort.getHeight());
					System.out.println(topLeft + " " + bottomRight);

					cellRect.setLocation(Math.min(bottomRight.x, Math.max(topLeft.x, cellRect.x + table.getLocationOnScreen().x)), Math.min(bottomRight.y, Math.max(topLeft.y, cellRect.y + table.getLocationOnScreen().y)));
					setOverlayBounds(cellRect);
				}
			});
		}
	};

	public FilterableTable() {

		if (ClientParameterProvider.getInstance().isEnabled(ParameterProvider.SHOW_OVERLAY_FOR_TOO_SMALL_TEXTFIELDS, false)) {
			this.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(final MouseEvent e) {
					if ((e.getModifiers() & InputEvent.BUTTON1_MASK) != 0 && e.isAltDown()) {
						if (FilterableTable.this.getParent() instanceof JViewport) {
							JViewport viewport = (JViewport) FilterableTable.this.getParent();
							boolean clFound = false;
							for (ChangeListener cl : viewport.getChangeListeners()) {
								if (cl.equals(changeListener)) {
									clFound = true;
								}
							}

							if (!clFound) {
								viewport.addChangeListener(changeListener);
							}
						}

						SwingUtilities.invokeLater(() -> {
							JTable table = FilterableTable.this;
							TableModel model = table.getModel();
							int row = table.rowAtPoint(e.getPoint());
							int column = table.columnAtPoint(e.getPoint());

							row = table.convertRowIndexToModel(row);
							column = table.convertColumnIndexToModel(column);

							TableCellRenderer renderer = table.getCellRenderer(row, column);

							Component component = renderer.getTableCellRendererComponent(table, model.getValueAt(row, column), false, true, row, column);
							Rectangle cellRect = table.getCellRect(row, column, true);

							currentCellRect = new Rectangle(cellRect);

							cellRect.setLocation(cellRect.x + table.getLocationOnScreen().x, cellRect.y + table.getLocationOnScreen().y);
							if (component instanceof JLabel && !model.isCellEditable(row,  column)) {
								showOverlay((JLabel) component, cellRect);
							}
						});
					}
				}
			});
		}
	}

	private void init() {
		overlayDialog = new JDialog();
		overlayDialog.setUndecorated(true);
		overlayTextPane = new JTextPane();
		overlayTextPane.setContentType("text/html");
		final JScrollPane overlayScrollPane = new JScrollPane(overlayTextPane);
		overlayTextPane.setVisible(true);
		overlayTextPane.setEditable(false);
		overlayDialog.setLayout(new BorderLayout());
		overlayDialog.add(overlayScrollPane, BorderLayout.CENTER);
		overlayDialog.setFocusable(true);

		Action overlayCloseAction = new AbstractAction() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				closeOverlay();
			}
		};

		overlayTextPane.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(final FocusEvent e) {
				if (overlayDialog.isVisible()) {
					closeOverlay();
				}
			}
		});

		overlayTextPane.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "closeOverlay");
		overlayTextPane.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, KeyEvent.CTRL_MASK), "closeOverlay");
		overlayTextPane.getActionMap().put("closeOverlay", overlayCloseAction);
	}

	private void showOverlay(JLabel label, final Rectangle cellRect) {
		if (overlayDialog == null || overlayTextPane == null) {
			init();
		}

		if (isTextTooLong(label, cellRect)) {
			overlayDialog.setVisible(true);
			overlayDialog.setSize(Math.max((int) cellRect.getWidth(), 200), 200);
			setOverlayBounds(cellRect);
			overlayTextPane.setText(label.getText());
			overlayTextPane.setBorder(getBorder());
			overlayTextPane.requestFocusInWindow();
		}
	}

	private void setOverlayBounds(final Rectangle cellRect) {
		if (overlayDialog.isVisible()) {
			boolean fitsDownwards = true;
			boolean fitsRight = true;
			Point root = new Point(cellRect.getLocation());
			int widthOverlay = overlayDialog.getWidth();
			int heigthOverlay = overlayDialog.getHeight();
			int widthField = (int) cellRect.getWidth();
			int heightField = (int) cellRect.getHeight();

			Window mainWindow = UIUtils.getWindowForComponent(this);
			if (root.y + heigthOverlay > mainWindow.getLocationOnScreen().y + mainWindow.getHeight()) {
				fitsDownwards = false;
			}
			if (root.x + widthOverlay > mainWindow.getLocationOnScreen().x + mainWindow.getWidth()) {
				fitsRight = false;
			}

			if (!fitsDownwards) {
				root.y = root.y - heigthOverlay + heightField;
			}
			if (!fitsRight) {
				root.x = root.x - widthOverlay + widthField;
			}
			overlayDialog.setBounds(
					root.x,
					root.y,
					widthOverlay,
					heigthOverlay);
		}
	}

	private void closeOverlay() {
		SwingUtilities.invokeLater(() -> {
			overlayDialog.setVisible(false);
			currentCellRect = null;
		});
	}

	private boolean isTextTooLong(JLabel label, final Rectangle cellRect) {
		return cellRect.getWidth() - label.getInsets().left - label.getInsets().right < getCachedFontMetrics().stringWidth(label.getText());
	}

	private FontMetrics getCachedFontMetrics() {
		if (fm == null) {
			fm = getFontMetrics(getFont());
		}
		return fm;
	}

    public final CollectableEntityFieldBasedTableModel getCollectableEntityFieldBasedTableModel() {
        return (CollectableEntityFieldBasedTableModel) super.getModel();
    }
}
