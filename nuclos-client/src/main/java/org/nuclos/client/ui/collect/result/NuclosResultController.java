//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.result;

import java.awt.Component;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;
import org.nuclos.client.command.CommandHandler;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.NuclosCollectController;
import org.nuclos.client.common.NuclosResultPanel;
import org.nuclos.client.common.controller.NuclosCollectControllerCommonState;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.client.ui.collect.CollectableTableHelper;
import org.nuclos.client.ui.collect.SelectFixedColumnsController;
import org.nuclos.client.ui.collect.SelectFixedColumnsPanel;
import org.nuclos.client.ui.collect.ToolTipsTableHeader;
import org.nuclos.client.ui.collect.component.model.ChoiceEntityFieldList;
import org.nuclos.client.ui.collect.model.CollectableEntityFieldBasedTableModel;
import org.nuclos.client.ui.collect.model.CollectableTableModel;
import org.nuclos.client.ui.collect.model.SortableCollectableTableModel;
import org.nuclos.client.ui.collect.subform.FixedColumnRowHeader;
import org.nuclos.client.ui.popupmenu.AbstractJPopupMenuListener;
import org.nuclos.client.ui.profile.ProfileMenu;
import org.nuclos.client.ui.profile.ProfileSupport;
import org.nuclos.client.ui.profile.ResultProfilesController;
import org.nuclos.client.ui.table.SortableTableModel;
import org.nuclos.client.ui.table.TableUtils;
import org.nuclos.common.Actions;
import org.nuclos.common.UID;
import org.nuclos.common.WorkspaceDescription2.EntityPreferences;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableUtils;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.preferences.TablePreferencesManager;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * A specialization of ResultController for use with an {@link NuclosCollectController}.
 * <p>
 * This implementation adds support for 'fixed' columns in the shown result.
 * </p>
 * @author Thomas Pasch
 * @since Nuclos 3.1.01
 */
public class NuclosResultController<PK,Clct extends Collectable<PK>> extends ResultController<PK,Clct> {

	private static final Logger LOG = Logger.getLogger(NuclosResultController.class);
	
	private final NuclosCollectControllerCommonState state;
	
	private ResultProfilesController profilesCtl;
	
	private TableHeaderColumnPopupListener tableHeadercolumnPopupListener;

	public NuclosResultController(CollectableEntity clcte, ISearchResultStrategy<PK,Clct> srs, NuclosCollectControllerCommonState state) {
		super(clcte, srs);
		this.state = state;
	}

	/**
	 * @deprecated You should really provide a CollectableEntity here.
	 */
	public NuclosResultController(UID entityUid, ISearchResultStrategy<PK,Clct> srs, NuclosCollectControllerCommonState state) {
		super(entityUid, srs);
		this.state = state;
	}
	
	@Override
	public void close() {
		super.close();
		tableHeadercolumnPopupListener = null;
		profilesCtl = null;
	}

	/**
	 * Factory method for instantiating a {@link SelectFixedColumnsController} suited 
	 * for the NuclosResultController. 
	 * <p>
	 * Maybe overridden by subclasses.
	 * </p>
	 */
	public SelectFixedColumnsController newSelectColumnsController(Component parent) {
		return new SelectFixedColumnsController(parent, new SelectFixedColumnsPanel());
	}

	/**
	 * TODO: Make protected again.
	 * 
	 * Initializes the <code>fields</code> field as follows: 
	 * <ol>
	 *   <li>Calling {@link ResultController#initializeFields(ChoiceEntityFieldList, List)}</li>
	 *   <li>(re-)set the fixed column set by reading the preferences. The set will only contain fixed columns that
	 *     have been <em>selected</em>.</li>
	 * </ol>
	 */
	@Override
	public void initializeFields() {
		super.initializeFields();

		final CollectController<PK,Clct> clctctl = getCollectController();
		List<UID> lstSelectedFieldNames = clctctl.getTablePreferencesManager().getFixedColumns();

		ChoiceEntityFieldList fields = clctctl.getFields();
		for (CollectableEntityField clctef : fields.getSelectedFields()) {
			if (lstSelectedFieldNames.contains(clctef.getUID())) {
				// this.stFixedColumns.add(clctef);
				getNuclosResultPanel().getFixedColumns().add(clctef);
			}
		}
	}

	/**
	 * initializes the <code>fields</code> field.
	 */
	public void initializeFields(final ChoiceEntityFieldList fields, 
			final List<CollectableEntityField> lstSelectedNew, final Set<CollectableEntityField> lstFixedNew, 
			final Map<UID,Integer> lstColumnWiths) 
	{
		final NuclosCollectController<PK,Clct> elisaController = (NuclosCollectController<PK,Clct>) getCollectController();
		final List<CollectableEntityField> lstSelected = new ArrayList<CollectableEntityField>(fields.getSelectedFields());
		final NuclosResultPanel<PK,Clct> panel = getNuclosResultPanel();
		final JTable tblResult = panel.getResultTable();
		UIUtils.runCommand(elisaController.getTab(),new CommandHandler() {
			
			@Override
			public void commandStarted(Component parent) {
				panel.setVisibleTable(false);
			}

			@Override
			public void commandFinished(Component parent) {
				//don't set setVisibleTable(true) here
				//see finishSearchObserver in this.refreshResult(... and this.run(...
			}
		},
		new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				final int iSelRow = tblResult.getSelectedRow();
				NuclosResultController.super.initializeFields(fields, lstSelectedNew);

				panel.setFixedColumns(new HashSet<CollectableEntityField>(lstFixedNew));

				final List<CollectableEntityField> lstSelectedNewAfterMove = (List<CollectableEntityField>) fields.getSelectedFields();
				((SortableCollectableTableModel<PK,Clct>) tblResult.getModel()).setColumns(lstSelectedNewAfterMove);
				TableColumnModel variableColumnModel = tblResult.getColumnModel();
				TableColumnModel fixedColumnModel = panel.getFixedResultTable().getColumnModel();

				adjustColumnModels(lstSelectedNewAfterMove, variableColumnModel, fixedColumnModel);

				final Collection<CollectableEntityField> collNewlySelected = CollectionUtils.subtract(lstSelectedNewAfterMove, lstSelected);
				if (!collNewlySelected.isEmpty() && !elisaController.getSearchStrategy().getCollectablesInResultAreAlwaysComplete()) {
					// refresh the result:
					refreshResult(elisaController);
				} else {
					panel.setVisibleTable(true);
				}

				// reselect the previously selected row (which gets lost by refreshing the model)
				if (iSelRow != -1) {
					tblResult.setRowSelectionInterval(iSelRow, iSelRow);
				}

				// restore the widths of the still present columns
				isIgnorePreferencesUpdate = true;
				panel.restoreColumnWidths(lstSelectedNew, lstColumnWiths);
				isIgnorePreferencesUpdate = false;

				// panel.invalidateFixedTable();
				panel.setupTableCellRenderers(tblResult);
			}

			private void refreshResult(final CollectController<PK,Clct> clctctl) throws CommonBusinessException {
				//((ElisaCollectController) clctctl).refreshResult();
				Observer finishSearchObserver = new Observer() {
					@Override
					public void update(Observable beobachtbarer, Object arg) {
						panel.setVisibleTable(true);
					}
				};
				List<Observer> lstObserver = new ArrayList<Observer>();
				lstObserver.add(finishSearchObserver);
				getSearchResultStrategy().cmdRefreshResult(lstObserver);
			}
		});
	}
	
	public NuclosCollectControllerCommonState getState() {
		return state;
	}

	private void adjustColumnModels(final List<? extends CollectableEntityField> lstclctefSelected,
			TableColumnModel columnmodelVariable, TableColumnModel columnmodelFixed) 
	{

		for (CollectableEntityField clctefSelected : lstclctefSelected) {
			if (getNuclosResultPanel().getFixedColumns().contains(clctefSelected)) {
				try {
					final int iSelectedIndex = columnmodelVariable.getColumnIndex(clctefSelected.getLabel());
					columnmodelVariable.removeColumn(columnmodelVariable.getColumn(iSelectedIndex));
				}
				catch (IllegalArgumentException ex) {
					// ignore
				}
			}
			else {
				try {
					final int iSelectedIndex = columnmodelFixed.getColumnIndex(clctefSelected.getLabel());
					columnmodelFixed.removeColumn(columnmodelFixed.getColumn(iSelectedIndex));
				}
				catch (IllegalArgumentException ex) {
					// ignore
				}
			}
		}
	}

	protected void setSelectColumns(final ChoiceEntityFieldList fields, 
			final SortedSet<CollectableEntityField> lstAvailableObjects, final List<CollectableEntityField> lstSelectedObjects, 
			final Set<CollectableEntityField> stFixedObjects, final boolean restoreWidthsFromPreferences, final Map<UID, Integer> mpWidths, final boolean restoreOrder) 
	{
		LOG.info("setSelectColumns: fixed=" + stFixedObjects + " other=" + lstSelectedObjects);
		
		final CollectController<PK,Clct>  clctctl = getCollectController();
		final NuclosResultPanel<PK,Clct> panel = getNuclosResultPanel();
		final JTable tblResult = panel.getResultTable();

		UIUtils.runCommand(clctctl.getTab(), new CommandHandler() {
			
			@Override
			public void commandStarted(Component parent) {
				panel.setVisibleTable(false);
			}

			@Override
			public void commandFinished(Component parent) {
				//don't set setVisibleTable(true) here
				//see finishSearchObserver in this.refreshResult(... and this.run(...
			}
		},
		new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				final int iSelRow = tblResult.getSelectedRow();
				final List<CollectableEntityField> lstSelectedOld = (List<CollectableEntityField>) fields.getSelectedFields();
				fields.set(lstAvailableObjects, lstSelectedObjects, clctctl.getResultController().getCollectableEntityFieldComparator());

				final List<CollectableEntityField> lstSelectedNew = (List<CollectableEntityField>) fields.getSelectedFields();
				panel.setFixedColumns(stFixedObjects);

					((SortableCollectableTableModel<PK,Clct>) tblResult.getModel()).setColumns(lstSelectedNew);
				TableColumnModel variableColumnModel = tblResult.getColumnModel();
				TableColumnModel fixedColumnModel = panel.getFixedResultTable().getColumnModel();

				adjustColumnModels(lstSelectedNew, variableColumnModel, fixedColumnModel);

				final Collection<? extends CollectableEntityField> collNewlySelected = CollectionUtils.subtract(lstSelectedNew, lstSelectedOld);

				boolean doRefresh = !collNewlySelected.isEmpty() && !clctctl.getSearchStrategy().getCollectablesInResultAreAlwaysComplete();
				
				if (!doRefresh && !getResultPanel().getResultFilter().isCollapsed()) {
					final Collection<? extends CollectableEntityField> collRemovedSelected = CollectionUtils.subtract(lstSelectedOld, lstSelectedNew);
					doRefresh = !collRemovedSelected.isEmpty();
				}

				if (doRefresh) {
					// refresh the result:
					refreshResult(clctctl);
				} else {
					panel.setVisibleTable(true);
				}

				if (!collNewlySelected.isEmpty()) {
					getResultPanel().getResultFilter().updateFilterColumns();
				}

				// reselect the previously selected row (which gets lost be refreshing the model)
				if (iSelRow != -1) {
					tblResult.setRowSelectionInterval(iSelRow, iSelRow);
				}

				final boolean isIgnorePreferencesUpdateBefore = isIgnorePreferencesUpdate;
				isIgnorePreferencesUpdate = true;
				if (restoreWidthsFromPreferences) {
					// restore widths from preferences
					panel.setColumnWidths(getNuclosResultPanel().getResultTable(), getCollectController().getTablePreferencesManager());
				} else {
					// restore the widths of the still present columns
					panel.restoreColumnWidths(lstSelectedNew, mpWidths);
				}
				isIgnorePreferencesUpdate = isIgnorePreferencesUpdateBefore;

				panel.invalidateFixedTable();

				panel.setupTableCellRenderers(tblResult);

				if (restoreOrder) {
					getCollectController().restoreColumnOrderFromPreferences(false);
					getCollectController().getSearchStrategy().search(true);
				}

				if (!isIgnorePreferencesUpdate) {
					// write preferences after column width are restored
					writeSelectedFieldsAndWidthsToPreferences(mpWidths);
				}
			}

				private void refreshResult(final CollectController<PK,Clct> clctctl) throws CommonBusinessException {
				Observer finishSearchObserver = new Observer() {
					@Override
					public void update(Observable beobachtbarer, Object arg) {
						panel.setVisibleTable(true);
					}
				};
				List<Observer> lstObserver = new ArrayList<Observer>();
				lstObserver.add(finishSearchObserver);
				getSearchResultStrategy().cmdRefreshResult(lstObserver);
			}

		});
	}

	/**
	 * TODO: Make this protected again.
	 */
	@Override
	public void setModel(CollectableTableModel<PK,Clct> tblmodel) {
		super.setModel(tblmodel);

		final NuclosResultPanel<PK,Clct> panel = getNuclosResultPanel();
		final JTable fixedTable = panel.getFixedResultTable();
		fixedTable.setModel(tblmodel);

		ToolTipsTableHeader tblHeader = new ToolTipsTableHeader(tblmodel, fixedTable.getColumnModel());

		tblHeader.setName("tblHeader");
		fixedTable.setTableHeader(tblHeader);

		TableUtils.addMouseListenerForSortingToTableHeader(fixedTable, (SortableTableModel) tblmodel, new CommonRunnable() {
			@Override
			public void run() {
				getSearchResultStrategy().cmdRefreshResult();
			}
		});

		removeColumnsFromFixedTable();
		removeColumnsFromResultTable();

		setFixedTable();
		//		TableUtils.setOptimalColumnWidths(fixedTable);

		this.tableHeadercolumnPopupListener = new TableHeaderColumnPopupListener() {
			@Override
			protected void removeColumnVisibility(TableColumn column) {
				try {
					final CollectController<PK,Clct>  clctctl = getCollectController();
					final Map<UID, Integer> mpWidths = panel.getVisibleColumnWidth(clctctl.getFields().getSelectedFields());
					final CollectableEntityField clctef = ((CollectableEntityFieldBasedTableModel) panel.getResultTable().getModel()).getCollectableEntityField(column.getModelIndex());
					cmdRemoveColumn(clctctl.getFields(), clctef);
					isIgnorePreferencesUpdate = true;
					panel.restoreColumnWidths(clctctl.getFields().getSelectedFields(), mpWidths);
					isIgnorePreferencesUpdate = false;
				} catch (ArrayIndexOutOfBoundsException e) {
					// ignore @see NUCLOS-590
				}
			}
		};
		
		panel.getResultTable().getTableHeader().addMouseListener(tableHeadercolumnPopupListener);
	}

	/**
	 * removes the columns from the fixed table, which are not in the stFixedColumns
	 */
	private void removeColumnsFromFixedTable() {
		final NuclosResultPanel<PK,Clct> panel = getNuclosResultPanel();
		final JTable fixedTable = panel.getFixedResultTable();

		TableColumnModel fixedColumnModel = fixedTable.getColumnModel();
		Set<TableColumn> columnsToRemove = new HashSet<TableColumn>();
		for (Enumeration<TableColumn> columnEnum = fixedColumnModel.getColumns(); columnEnum.hasMoreElements();) {

			TableColumn varColumn = columnEnum.nextElement();
			boolean doRemove = true;
			for (CollectableEntityField clctefFixed : panel.getFixedColumns()) {
				if (clctefFixed.getLabel().equals(varColumn.getIdentifier())) {
					doRemove = false;
				}
			}
			if (doRemove) {
				columnsToRemove.add(varColumn);
			}
		}

		for (TableColumn columnToRemove : columnsToRemove) {
			fixedColumnModel.removeColumn(columnToRemove);
		}
	}

	/**
	 * removes the columns from the result table, which are  in the stFixedColumns
	 */
	private void removeColumnsFromResultTable() {
		final NuclosResultPanel<PK,Clct> panel = getNuclosResultPanel();
		final JTable resultTable = panel.getResultTable();

		TableColumnModel resultColumnModel = resultTable.getColumnModel();
		Set<TableColumn> columnsToRemove = new HashSet<TableColumn>();
		for (Enumeration<TableColumn> columnEnum = resultColumnModel.getColumns(); columnEnum.hasMoreElements();) {

			final TableColumn varColumn = columnEnum.nextElement();
			boolean doRemove = false;
			for (CollectableEntityField clctefFixed : panel.getFixedColumns()) {
				if (clctefFixed.getLabel().equals(varColumn.getIdentifier())) {
					doRemove = true;
				}
			}
			if (doRemove) {
				columnsToRemove.add(varColumn);
			}
		}

		for (TableColumn columnToRemove : columnsToRemove) {
			resultColumnModel.removeColumn(columnToRemove);
		}
	}

	/**
	 */
	private void setFixedTable() {
		final NuclosResultPanel<PK,Clct> panel = getNuclosResultPanel();
		final JTable fixedTable = panel.getFixedResultTable();

		fixedTable.setRowHeight(panel.getResultTable().getRowHeight());
		panel.getResultTableScrollPane().setRowHeaderView(fixedTable);
		panel.getResultTableScrollPane().getRowHeader().setBackground(fixedTable.getBackground());
		panel.getResultTableScrollPane().getRowHeader().setPreferredSize(fixedTable.getPreferredSize());
		panel.getResultTableScrollPane().setCorner(JScrollPane.UPPER_LEFT_CORNER, fixedTable.getTableHeader());

		final TableCellRenderer originalRenderer = fixedTable.getTableHeader().getDefaultRenderer();
		TableCellRenderer headerRenderer = new TableCellRenderer() {
			
			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				Component renderComp = originalRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				if (renderComp != null && renderComp.getBackground() != null) {
					renderComp.setBackground(FixedColumnRowHeader.FIXED_HEADER_BACKGROUND);
				}
				return renderComp;
			}
		};

		fixedTable.getTableHeader().setDefaultRenderer(headerRenderer);
		panel.invalidateFixedTable();
	}

	/**
	 * command: select columns
	 * Lets the user select the columns to show in the result list.
	 */
	@Override
	public final void cmdSelectColumns(final ChoiceEntityFieldList fields) {
		final NuclosResultPanel<PK,Clct> panel = getNuclosResultPanel();
		final NuclosCollectController<PK,Clct> nucleusctl = (NuclosCollectController<PK,Clct>) getCollectController();

		final SelectFixedColumnsController ctl = newSelectColumnsController(nucleusctl.getTab());
		final SortedSet<CollectableEntityField> lstAvailable = new TreeSet<>(fields.getAvailableFields());
		// Remove hidden fields from list
		final Iterator<CollectableEntityField> itAvailableFields = lstAvailable.iterator();
		while (itAvailableFields.hasNext()) {
			if (itAvailableFields.next().isHidden()) {
				itAvailableFields.remove();
			}
		}
		final List<CollectableEntityField> lstSelected = fields.getSelectedFields();
		final ChoiceEntityFieldList ro = new ChoiceEntityFieldList(panel.getFixedColumns());
		ro.set(lstAvailable, lstSelected, nucleusctl.getResultController().getCollectableEntityFieldComparator());

		// remember the widths of the currently visible columns
		final Map<UID, Integer> mpWidths = panel.getVisibleColumnWidth(lstSelected);
		ctl.setModel(ro);
		final boolean bOK = ctl.run(  
				SpringLocaleDelegate.getInstance().getMessage("SelectColumnsController.1","Anzuzeigende Spalten ausw\u00e4hlen"));

		if (bOK) {
			setSelectColumns(fields, ctl.getAvailableObjects(), ctl.getSelectedObjects(), ctl.getFixedObjects(), false, mpWidths, false);
		}
	}

	@Override
	protected final void cmdAddColumn(ChoiceEntityFieldList fields, TableColumn columnBefore, UID sFieldNameToAdd) throws CommonBusinessException {
		super.cmdAddColumn(fields, columnBefore, sFieldNameToAdd);

		final NuclosResultPanel<PK,Clct> panel = getNuclosResultPanel();
		final JTable table = panel.getResultTable();
		final JTable fixedTable = panel.getFixedResultTable();

		final TableColumnModel columnmodelVariable = table.getColumnModel();
		final TableColumnModel columnmodelFixed = fixedTable.getColumnModel();
		adjustColumnModels(fields.getSelectedFields(), columnmodelVariable, columnmodelFixed);
	}

	@Override
	protected final void cmdRemoveColumn(ChoiceEntityFieldList fields, CollectableEntityField clctef) {
		List<CollectableEntityField> lstSelectedNew = new ArrayList<CollectableEntityField>(fields.getSelectedFields());
		lstSelectedNew.remove(clctef);
		if (lstSelectedNew.size() == 0) {
			JOptionPane.showMessageDialog(null, 
					SpringLocaleDelegate.getInstance().getMessage(
							"SelectFixedColumnsController.3","Es d\u00fcrfen nicht alle Spalten ausgeblendet oder fixiert werden."));
		}
		else {
			final NuclosResultPanel<PK,Clct> panel = getNuclosResultPanel();
			// remember the widths of the currently visible columns
			final Map<UID, Integer> mpWidths = panel.getVisibleColumnWidth(fields.getSelectedFields());

			super.cmdRemoveColumn(fields, clctef);

			panel.getFixedColumns().remove(clctef);
			// TODO: Is the copy really needed? (Thomas Pasch)
			SortedSet<CollectableEntityField> lstAvailableFields = new TreeSet<CollectableEntityField>(fields.getComparatorForAvaible());
			lstAvailableFields.addAll(fields.getAvailableFields());
			// TODO: Is the copy really needed? (Thomas Pasch)
			List<CollectableEntityField> lstSelectedFields = new ArrayList<CollectableEntityField>(fields.getSelectedFields());
			setSelectColumns(fields, lstAvailableFields, lstSelectedFields, panel.getFixedColumns(), false, mpWidths, false);
		}
	}

	@Override
	protected void writeSelectedFieldsAndWidthsToPreferences(
			TablePreferencesManager tblprefManager,
			List<? extends CollectableEntityField> lstclctefSelected, Map<UID, Integer> mpWidths) {
		super.writeSelectedFieldsAndWidthsToPreferences(tblprefManager,
				lstclctefSelected, mpWidths, CollectableUtils.getFieldUidsFromCollectableEntityFields(getNuclosResultPanel().getFixedColumns()));
	}

	@Override
	protected List<Integer> getFieldWidthsForPreferences() {
		final List<Integer> lstFieldWidths = CollectableTableHelper.getColumnWidths(getNuclosResultPanel().getFixedResultTable());
		lstFieldWidths.addAll(CollectableTableHelper.getColumnWidths(getNuclosResultPanel().getResultTable()));
		return lstFieldWidths;
	}

	protected NuclosResultPanel<PK,Clct> getNuclosResultPanel() {
		return (NuclosResultPanel<PK,Clct>) getCollectController().getResultPanel();
	}

	@Override
	public void setupResultPanel() {
		super.setupResultPanel();
		getNuclosResultPanel().addFixedColumnModelListener(newResultTablePreferencesUpdateListener());
	}

	/**
	 * Popup menu for the columns in the Result table.
	 */
	protected abstract class TableHeaderColumnPopupListener extends AbstractJPopupMenuListener {

		private Point ptLastOpened;
		protected JPopupMenu popupmenuColumn = new JPopupMenu();
		protected final JTableHeader usedHeader;

		public TableHeaderColumnPopupListener() {
			super();
			this.usedHeader = getNuclosResultPanel().getResultTable().getTableHeader();

			final SortableTableModel tblmodel = (SortableTableModel)getNuclosResultPanel().getResultTable().getModel();
			final JMenuItem miPopupSortThisColumnAsc = new JMenuItem(
					SpringLocaleDelegate.getInstance().getMessage("NuclosResultController.4","Aufsteigen sortieren"));
			miPopupSortThisColumnAsc.setIcon(Icons.getInstance().getIconUp16());
			miPopupSortThisColumnAsc.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent ev) {
					final int iColumn = usedHeader.columnAtPoint(getLatestOpenPoint());
					List<SortKey> sortKeys = new ArrayList<SortKey>(tblmodel.getSortKeys());
					if (sortKeys.isEmpty())
						sortKeys.add(new SortKey(iColumn, SortOrder.ASCENDING));
					else {
						for (SortKey sortKey : tblmodel.getSortKeys()) {
							int idx = tblmodel.getSortKeys().indexOf(sortKey);
							if (sortKey.getColumn() == iColumn) {
								sortKeys.remove(sortKey);
								sortKeys.add(idx, new SortKey(sortKey.getColumn(), SortOrder.ASCENDING));
							}
						}
					}
					tblmodel.setSortKeys(sortKeys, false);
					getSearchResultStrategy().cmdRefreshResult();
				}
			});
			popupmenuColumn.add(miPopupSortThisColumnAsc);
			final JMenuItem miPopupSortThisColumnDec = new JMenuItem(
					SpringLocaleDelegate.getInstance().getMessage("NuclosResultController.5","Absteigen sortieren"));
			miPopupSortThisColumnDec.setIcon(Icons.getInstance().getIconDown16());
			miPopupSortThisColumnDec.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent ev) {
					final int iColumn = usedHeader.columnAtPoint(getLatestOpenPoint());
					List<SortKey> sortKeys = new ArrayList<SortKey>(tblmodel.getSortKeys());
					if (sortKeys.isEmpty())
						sortKeys.add(new SortKey(iColumn, SortOrder.DESCENDING));
					else {
						for (SortKey sortKey : tblmodel.getSortKeys()) {
							int idx = tblmodel.getSortKeys().indexOf(sortKey);
							if (sortKey.getColumn() == iColumn) {
								sortKeys.remove(sortKey);
								sortKeys.add(idx, new SortKey(sortKey.getColumn(), SortOrder.DESCENDING));
							}
						}
					}
					tblmodel.setSortKeys(sortKeys, false);
					getSearchResultStrategy().cmdRefreshResult();
				}
			});
			popupmenuColumn.add(miPopupSortThisColumnDec);
			final JMenuItem miPopupSortThisColumnNone = new JMenuItem(
					SpringLocaleDelegate.getInstance().getMessage("NuclosResultController.6","Sortierung aufheben"));
			miPopupSortThisColumnNone.setIcon(Icons.getInstance().getIconUndo16());
			miPopupSortThisColumnNone.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent ev) {
					final int iColumn = usedHeader.columnAtPoint(getLatestOpenPoint());
					List<SortKey> sortKeys = new ArrayList<SortKey>(tblmodel.getSortKeys());
					for (SortKey sortKey : tblmodel.getSortKeys()) {
						if (sortKey.getColumn() == iColumn) {
							sortKeys.remove(sortKey);
						}
					}
					tblmodel.setSortKeys(sortKeys, false);
					getSearchResultStrategy().cmdRefreshResult();
				}
			});
			popupmenuColumn.add(miPopupSortThisColumnNone);
			
			final JMenuItem miPopupSortAllColumnsNone = new JMenuItem(
					SpringLocaleDelegate.getInstance().getMessage("NuclosResultController.7","Alle Sortierungen zurücksetzen"));
			miPopupSortAllColumnsNone.setIcon(Icons.getInstance().getIconInsertTable16());
			miPopupSortAllColumnsNone.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent ev) {
					final int iColumn = usedHeader.columnAtPoint(getLatestOpenPoint());
					List<SortKey> sortKeys = new ArrayList<SortKey>(tblmodel.getSortKeys());
					for (SortKey sortKey : tblmodel.getSortKeys()) {
						sortKeys.remove(sortKey);
					}
					tblmodel.setSortKeys(sortKeys, false);
					getSearchResultStrategy().cmdRefreshResult();
				}
			});
			popupmenuColumn.add(miPopupSortAllColumnsNone);

			if (SecurityCache.getInstance().isActionAllowed(Actions.ACTION_CUSTOMIZE_ENTITY_AND_SUBFORM_COLUMNS) ||
					!getMainFrame().getWorkspace().isAssigned()) {
				this.popupmenuColumn.addSeparator();
				this.popupmenuColumn.add(createHideColumnItem());
			}

			if (SecurityCache.getInstance().isActionAllowed(Actions.ACTION_WORKSPACE_ASSIGN) &&
					getMainFrame().getWorkspace().isAssigned()) {
				this.popupmenuColumn.addSeparator();
				/*
				 * deprecated by NUCLOS-1479
				this.popupmenuColumn.add(createPublishColumnsItem());
				*/
			}
			/*
			 * deprecated by NUCLOS-1479
			this.popupmenuColumn.add(createRestoreColumnsItem());
			*/
		}
		
		public JPopupMenu getPopupMenuColumn() {
			return this.popupmenuColumn;
		}

		private JMenuItem createHideColumnItem() {
			final JMenuItem miPopupHideThisColumn = new JMenuItem(
					SpringLocaleDelegate.getInstance().getMessage("NuclosResultController.1","Diese Spalte ausblenden"));
			miPopupHideThisColumn.setIcon(Icons.getInstance().getIconRemoveColumn16());
			miPopupHideThisColumn.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent ev) {
					final int iColumnIndex = usedHeader.columnAtPoint(getLatestOpenPoint());
					removeColumnVisibility(usedHeader.getColumnModel().getColumn(iColumnIndex));
				}
			});

			return miPopupHideThisColumn;
		}

		@Override
		protected final JPopupMenu getJPopupMenu(MouseEvent ev) {
			this.ptLastOpened = ev.getPoint();
			return popupmenuColumn;
		}

		/**
		 * the point where the popup menu was opened latest
		 */
		public Point getLatestOpenPoint() {
			return this.ptLastOpened;
		}

		protected abstract void removeColumnVisibility(TableColumn column);

	}  // inner class PopupMenuColumnListener

	private final EntityPreferences getEntityPrefs() {
		return getCollectController().getEntityPreferences();
	}

	private final void updateColumns() {
		final List<CollectableEntityField> allFields = CollectionUtils.concat(
				getFields().getAvailableFields(), 
				getFields().getSelectedFields());

		final List<CollectableEntityField> selected = getCollectController().getTablePreferencesManager().getSelectedFields(allFields);
		getCollectController().makeSureSelectedFieldsAreNonEmpty(getEntity(), selected);

		final SortedSet<CollectableEntityField> available = new TreeSet<CollectableEntityField>(new Comparator<CollectableEntityField>() {
			@Override
			public int compare(CollectableEntityField o1, CollectableEntityField o2) {
				int result = StringUtils.compareIgnoreCase(
						SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(MetaProvider.getInstance().getEntity(o1.getEntityUID())),
								SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(MetaProvider.getInstance().getEntity(o2.getEntityUID())));
				if (result == 0)
					result = StringUtils.compareIgnoreCase(o1.getLabel(), o2.getLabel());
				return result;
			}
		});
		available.addAll(CollectionUtils.subtract(allFields, selected));


		setSelectColumns(
				getFields(), 
				available, 
				selected,
				getCollectController().getTablePreferencesManager().getFixedFields(selected),
				true, null, 
				true);
	}

	@Override
	public void postInitialisation() {
		UIUtils.invokeOnDispatchThread(new Runnable() {

			@Override
			public void run() {
				initProfiles();
			}
		});

	}
	
	public ResultProfilesController getProfilesController() {
		return this.profilesCtl;
	}

	
	public void initProfiles() {
		// NUCLOS-1479
		this.profilesCtl = new ResultProfilesController(getCollectController().getTablePreferencesManager(),
				new ResultControllerProfileSupport(), getCollectController().getEntityUid());
		if (this.profilesCtl.isAvailable()) {
			final JToolBar toolbar = getCollectController().getResultPanel().getToolBar();
			toolbar.add(ProfileMenu.createPopupButton(profilesCtl, state));
			this.tableHeadercolumnPopupListener.getPopupMenuColumn().add(ProfileMenu.createMenu(getProfilesController(), state));
		}	
	}
	
	
	public class ResultControllerProfileSupport implements ProfileSupport {

		@Override
		public void updateBySelectedProfile() {
			final boolean isIgnorePreferencesUpdateBefore = NuclosResultController.this.isIgnorePreferencesUpdate;
			NuclosResultController.this.isIgnorePreferencesUpdate = true;
			try {
				updateColumns();
			} finally {
				NuclosResultController.this.isIgnorePreferencesUpdate = isIgnorePreferencesUpdateBefore;
			}
		}
	}
	
}
