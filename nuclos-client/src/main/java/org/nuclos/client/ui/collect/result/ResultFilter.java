package org.nuclos.client.ui.collect.result;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EventListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;

import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.RowFilter;
import javax.swing.SwingUtilities;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import org.apache.log4j.Logger;
import org.nuclos.client.ui.DateChooser;
import org.nuclos.client.ui.FormatUtils;
import org.nuclos.client.ui.ListOfValues;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.CollectableResultComponent;
import org.nuclos.client.ui.collect.component.AbstractCollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableCheckBox;
import org.nuclos.client.ui.collect.component.CollectableComboBox;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableComponentFactory;
import org.nuclos.client.ui.collect.component.CollectableDateChooser;
import org.nuclos.client.ui.collect.component.CollectableListOfValues;
import org.nuclos.client.ui.collect.component.CollectableTextField;
import org.nuclos.client.ui.collect.component.LabeledCollectableComponentWithVLP;
import org.nuclos.client.ui.collect.model.CollectableEntityFieldBasedTableModel;
import org.nuclos.client.ui.gc.IReferenceHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableFieldsProviderFactory;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.searchcondition.AtomicCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableLikeCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common.preferences.PreferencesUtils;
import org.nuclos.common2.ClientPreferences;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.PreferencesException;

/**
 * Created by guenthse on 3/2/2017.
 */
public abstract class ResultFilter implements IReferenceHolder, ActionListener {

    protected static final Logger LOG = Logger.getLogger(ResultFilter.class);

    protected JToggleButton filterButton;

    private ResultFilterPanel fixedResultFilter;
    private ResultFilterPanel externalResultFilter;

    protected CollectableResultComponent resultComponent;
    protected FilterableTable externalTable;

    protected CollectableFieldsProviderFactory collectableFieldsProviderFactory;

    protected List<RowFilter<TableModel, Integer>> fixedRowFilters = new ArrayList<RowFilter<TableModel, Integer>>();

    protected boolean filteringActive = false;

    protected boolean closed = false;
    protected boolean created;

    protected boolean filterByString = false;

    protected final List<Object> ref = new LinkedList<Object>();
    private UID parentEntityUid;
    private Map<UID, CollectableSearchCondition> searchConditionsOld = null;

    public ResultFilter(CollectableResultComponent resultComponent) {
        this.resultComponent = resultComponent;
    }

    public void setupFilter(UID parentEntityUid,
                     CollectableFieldsProviderFactory collectableFieldsProviderFactory) {
        this.parentEntityUid = parentEntityUid;
        this.collectableFieldsProviderFactory = collectableFieldsProviderFactory;

        getToggleButton().addActionListener(this);
    }

    public void createFilter(final boolean nonEmptyFilterLoaded) {
        if (created) {
            return;
        }

        created = true;

        _createFilter();
//        setSubFormParameterProvider(parameterProvider);

        if (nonEmptyFilterLoaded) {
            loadTableFilter();
        } else {
            doFiltering();
        }
    }

    protected void _createFilter() {

        // get fixed and external tables
        final JTable fixedTable = getFixedTable();
        final FilterableTable externalTable = getExternalTable();

        this.externalTable = externalTable;
        this.filterButton = getToggleButton();

        setFixedResultFilter(fixedTable, fixedTable.getColumnModel(), getSearchFilterComponents());        
        setExternalResultFilter(externalTable, externalTable.getColumnModel(), getSearchFilterComponents());

        addActionListener();

        setupDirectResponse(getAllComponents());

        setupTableHeaderForScrollPane(resultComponent.getScrollPane());
    }

    /**
     * Is filtering currently active?
     * @return true if filtering, false if not
     */
    public boolean isFilteringActive() {
        if (created) {
            return filteringActive;
        }
        return false;
    }

    public void setFilteringActive(boolean filteringActive) {
        this.filteringActive = filteringActive;
    }

    protected abstract JTable getFixedTable();

    protected abstract FilterableTable getExternalTable();

    @Override
    public void actionPerformed(ActionEvent e) {
        final boolean nonEmptyFilterLoaded = isFilterStored();
        createFilter(nonEmptyFilterLoaded);
    }

    public void close() {
        // Close is needed for avoiding memory leaks
        // If you want to change something here, please consult me (tp).
        if (!closed) {
            // storeTableFilter();
            LOG.debug("close(): " + this);
            resultComponent = null;
            externalTable = null;
            fixedResultFilter = null;

            if (externalResultFilter != null) {
                externalResultFilter.close();
            }
            externalResultFilter = null;
            if (fixedResultFilter != null) {
                fixedResultFilter.close();
            }
            fixedResultFilter = null;

            fixedRowFilters.clear();
            fixedRowFilters = null;

            ref.clear();
            closed = true;

            created = false;
        }
    }

    /**
     * actionlistener to collapse or expand the searchfilter panels
     */
    protected abstract void addActionListener();

    /**
     * create collectablecomponents as search components and assign them to the corresponding column
     */
    protected Map<UID, CollectableComponent> getSearchFilterComponents() {
        final Map<UID, CollectableComponent> column2component = new HashMap<UID, CollectableComponent>();
        final CollectableEntityFieldBasedTableModel tableModel = (CollectableEntityFieldBasedTableModel) externalTable.getModel();
        final UID sfEntityUid = resultComponent.getEntityUID();

        for (int index = 0; index < tableModel.getColumnCount(); ++index) {
            final CollectableEntityField cef = tableModel.getCollectableEntityField(index);
            final UID cefUid = cef.getUID();
            final UID columnName = tableModel.getColumnFieldUid(index);
            final CollectableComponent clctcomp = CollectableComponentFactory.getInstance().newCollectableComponent(
                    cef, resultComponent.getCollectableComponentType(cefUid, true), true);

            // FIXME: NUCLOS-6975 4) The explicite disalloance of CollectableListOfValues renders a code block without function
            if (clctcomp instanceof LabeledCollectableComponentWithVLP
                    && !(clctcomp instanceof CollectableListOfValues)) {
                handleVLP(sfEntityUid, cef, columnName, clctcomp, resultComponent);

                // handle listener
                if (clctcomp instanceof CollectableComboBox) {
                    final JComboBox cbx = (JComboBox) clctcomp.getControlComponent();
                    cbx.addItemListener(new ItemListener() {
                        //NUCLEUSINT-789 i
                        @Override
                        public void itemStateChanged(ItemEvent e) {
                            if (e.getStateChange() == ItemEvent.SELECTED) {
                                if (!closed) {
//TODO REMOVE ALL                   filter();
                                }
                            }
                        }
                    });
                    if (cbx.getEditor().getEditorComponent() instanceof JTextField) {
                        final JTextField tf = (JTextField) cbx.getEditor().getEditorComponent();
                        tf.addKeyListener(new KeyAdapter() {
                            @Override
                            public void keyPressed(KeyEvent e) {
                                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                                    boolean doFilter = false;
                                    String text = tf.getText();
                                    if (text != null) {
                                        doFilter = true;
                                    }
                                    if (doFilter) {
                                        try {
                                            saveSearchTerm(sfEntityUid, cefUid, clctcomp);
                                        } catch (PreferencesException e1) {
                                            LOG.info("saveSearchTerm failed: " + e1, e1);
                                        }
                                        if (!closed) {
                                            filter();
                                        }
                                    }
                                }
                            }
                        });
                    }
                }
                if (clctcomp instanceof CollectableListOfValues) {
                    final ListOfValues lov = (ListOfValues) clctcomp.getControlComponent();
                    final JTextField tf = lov.getJTextField();
                    tf.addKeyListener(new KeyAdapter() {
                        @Override
                        public void keyPressed(KeyEvent e) {
                            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                                boolean doFilter = false;
                                String text = tf.getText();
                                List<CollectableValueIdField> lstCVIdF = null;
                                try {
                                    lstCVIdF = lov.getQSR(text);
                                } catch (CommonBusinessException cbe) {
                                    LOG.error(cbe.getMessage(), cbe);
                                }

                                if (lstCVIdF != null && lstCVIdF.size() == 1) {
                                    tf.setText((String) lstCVIdF.get(0).getValue());
                                    doFilter = true;
                                } else if (text == null || text.isEmpty()) {
                                    doFilter = true;
                                }

                                if (doFilter) {
                                    try {
                                        saveSearchTerm(sfEntityUid, cefUid, clctcomp);
                                    } catch (PreferencesException e1) {
                                        LOG.info("saveSearchTerm failed: " + e1, e1);
                                    }
                                    if (!closed) {
//TODO REMOVE ALL                       filter();
                                    }
                                }
                            }
                        }
                    });
                    tf.addFocusListener(new FocusAdapter() {
                        //NUCLEUSINT-789
                        @Override
                        public void focusLost(FocusEvent e) {
                            try {
                                saveSearchTerm(sfEntityUid, cefUid, clctcomp);
                            } catch (PreferencesException e1) {
                                LOG.info("saveSearchTerm failed: " + e1, e1);
                            }
                            if (!closed) {
//TODO REMOVE ALL               //filter();
                            }
                        }
                    });
                }
            } else if (clctcomp instanceof CollectableCheckBox) {
                ((JCheckBox) clctcomp.getControlComponent()).addItemListener(new ItemListener() {
                    @Override
                    public void itemStateChanged(ItemEvent arg0) {
                        if (!closed) {
                            // wait for set of comparison operator from ItemListener in CollectableCheckBox...
                            SwingUtilities.invokeLater(new Runnable() {
                                @Override
                                public void run() {
                                    filter();
                                }
                            });
                        }
                    }
                });
                clctcomp.clear();
            } else if (clctcomp instanceof CollectableDateChooser) {
                ((CollectableDateChooser) clctcomp).getDateChooser().getJTextField()
                        .addFocusListener(new FocusAdapter() {
                            @Override
                            public void focusLost(FocusEvent e) {
                                if (!closed) {
//TODO REMOVE ALL                   //filter();
                                }
                            }
                        });
            } else {
                JComponent comp = clctcomp.getControlComponent();
                comp.addFocusListener(new FocusAdapter() {
                    //NUCLEUSINT-789
                    @Override
                    public void focusLost(FocusEvent e) {
                        try {
                            saveSearchTerm(sfEntityUid, cefUid, clctcomp);
                        } catch (PreferencesException e1) {
                            LOG.info("saveSearchTerm failed: " + e1, e1);
                        }
                        if (!closed) {
//TODO REMOVE ALL           //filter();
                        }
                    }
                });
            }
            if (clctcomp != null) {
                column2component.put(cefUid, clctcomp);
            }
            // autocompletion for text fields
            if (clctcomp instanceof CollectableTextField) {
                final Preferences acPref = getAutoCompletePreferences(cef.getEntityUID());
                FormatUtils.addAutoComplete(cef, ((CollectableTextField) clctcomp).getJTextField(), acPref);
            }
        }
        return column2component;
    }

    protected void handleVLP(final UID sfEntityUid, final CollectableEntityField cef, final UID columnName, final CollectableComponent clctcomp, final CollectableResultComponent resultComponent) {
        //do nothing
    }

    // saves the (current) search term for auto-completion functionality
    private void saveSearchTerm(UID entityName, UID fieldName, CollectableComponent clctcomp) throws PreferencesException {
        if (clctcomp instanceof CollectableTextField) {
            final Preferences acPref = getAutoCompletePreferences(entityName);
            final List<String> l = PreferencesUtils.getStringList(acPref, fieldName.toString());
            final Object o = getFilterValue(clctcomp);
            if (o != null) {
                final String s = o.toString();
                if (l.contains(s)) {
                    l.remove(s);
                }
                l.add(0, s);
            }
            // limit auto-completion size to 10
            while (l.size() > 10) {
                l.remove(l.size() - 1);
            }
            PreferencesUtils.putStringList(acPref, fieldName.toString(), l);
            FormatUtils.addAutoComplete(clctcomp.getEntityField(),
                    ((CollectableTextField) clctcomp).getJTextField(), acPref);
        }
    }

    private Preferences getAutoCompletePreferences(UID entityName) {
        return ClientPreferences.getInstance().getUserPreferences().node("collect").node("entity")
                .node(entityName.getString()).node("resultfilter").node("autocompletion");
    }

    public void addFixedRowFilter(RowFilter<TableModel, Integer> rowFilter) {
        if (created) {
            fixedRowFilters.add(rowFilter);
        }
    }

    public void clearFixedRowFilter() {
        if (created) {
            fixedRowFilters.clear();
        }
    }

    public void setFilterByString(boolean filterByString) {
        if (created) {
            this.filterByString = filterByString;
        }
    }

    private Collection<CollectableComponent> getAllComponents() {
		final Collection<CollectableComponent> colComponents = new HashSet<CollectableComponent>();
		colComponents.addAll(getExternalResultFilter().getActiveFilterComponents().values());
		colComponents.addAll(getFixedResultFilter().getActiveFilterComponents().values());
		return colComponents;
	}

    /**
     * NUCLOS-1897
     */
    protected void setupDirectResponse(Collection<CollectableComponent> colComponents) {
        final FocusListener fl = new FocusAdapter() {

            @Override
            public void focusLost(FocusEvent e) {
//TODO REMOVE ALL   doFiltering();
            }
        };

        final KeyListener kl = new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (KeyEvent.VK_ENTER == e.getKeyCode()) {
                    doFiltering();
                }
            }
        };

        final AbstractCollectableComponent.ComparisonPopupBecomesInvisibleListener pl = new AbstractCollectableComponent.ComparisonPopupBecomesInvisibleListener() {

            @Override
            public void popupBecomesInvisible(AbstractCollectableComponent.ComparisonPopupBecomesInvisibleEvent event) {
                SwingUtilities.invokeLater(new Runnable() {

                    @Override
                    public void run() {
                        doFiltering();
                    }
                });
            }
        };

        for (final CollectableComponent cc : colComponents) {
            JComponent component = cc.getControlComponent();
            if (component instanceof DateChooser) {
                final DateChooser dateChooser = (DateChooser) component;
                dateChooser.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        filter();
                    }
                });
            } else if (component instanceof ListOfValues) {
                final ListOfValues lov = (ListOfValues) component;
                final ListOfValues.QuickSearchSelectedListener oldListener = lov.getQuickSearchSelectedListener();
                lov.setQuickSearchSelectedListener(new ListOfValues.QuickSearchSelectedListener() {

                    @Override
                    public void actionPerformed(CollectableValueIdField itemSelected) {
                        if (null != oldListener) {
                            oldListener.actionPerformed(itemSelected);
                        }
                        filter();
                    }
                });
            }
            component.addFocusListener(fl);
            component.addKeyListener(kl);

            final AbstractCollectableComponent ac = (AbstractCollectableComponent) cc;
            ac.addPopupBecomesVisibleListener(pl);
        }
    }

    @Override
    public void addRef(EventListener o) {
        ref.add(o);
    }

    protected void setExternalResultFilter(JTable table, TableColumnModel columnModel,
                                          Map<UID, CollectableComponent> searchFilterComponents) {
        this.externalResultFilter = new ResultFilterPanel(table, columnModel, searchFilterComponents, false);
    }

    public ResultFilterPanel getExternalResultFilter() {
        return this.externalResultFilter;
    }

    protected void setFixedResultFilter(JTable table, TableColumnModel columnModel,
                                       Map<UID, CollectableComponent> searchFilterComponents) {
        this.fixedResultFilter = new ResultFilterPanel(table, columnModel, searchFilterComponents, true);
        setupResetFilterAction();
    }

    public ResultFilterPanel getFixedResultFilter() {
        return this.fixedResultFilter;
    }

    /**
     * handles the action when the reset filter button was pressed
     */
    private void setupResetFilterAction() {
        this.fixedResultFilter.getResetFilterButton().addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                Map<UID, CollectableComponent> columnFilters = getAllFilterComponents();

                for (CollectableComponent comp : columnFilters.values()) {
                    if (comp != null) {
                        comp.clear();
                    }
                }
                getFixedResultFilter().repaint();
                getExternalResultFilter().repaint();
                filter();
            }
        });
    }

    public final void arrangeFilterComponents() {
        if (created) {
            this.fixedResultFilter.arrangeFilterComponents();
            this.externalResultFilter.arrangeFilterComponents();
        }
    }

    public void doFiltering() {
        if (created) {
            filter();
        }
    }

    protected abstract void filter();

    public abstract void clearFilter();

    public void removeFiltering() {
        if (created) {
            // Why the heck is the filtering state handled so complicated?!?  There's a reason why Swing has
            // Actions (with SELECTED_KEY) and Button/ItemModels etc...
            clearFilter();
            getFixedResultFilter().setCollapsed(true);
            getExternalResultFilter().setCollapsed(true);
            filterButton.setSelected(false);
        }
    }

    /**
     * returns all CollectableComponents used in the filter panels
     */
    public final Map<UID, CollectableComponent> getAllFilterComponents() {
        if (created) {
            // get all CollectableComponents used in the filter panels
            Map<UID, CollectableComponent> columnFilters = fixedResultFilter.getActiveFilterComponents();
            columnFilters.putAll(externalResultFilter.getActiveFilterComponents());

            return columnFilters;
        }
        return null;
    }

    /**
     * Get the filter object from the (search) collectable component.
     *
     * @return <code>null</code> or the filter object.
     * 		For value fields the type is
     * 		clctcomp.getEntityField().getJavaClass().
     */
    protected static Object getFilterValue(CollectableComponent clctcomp) {
        try {
            CollectableSearchCondition cond = clctcomp.getSearchCondition();

            // no search condition means no filtering, so return null
            if (cond == null) {
                return null;
            }

            if (cond instanceof AtomicCollectableSearchCondition) {
                if (cond instanceof CollectableLikeCondition) {
                    return ((CollectableLikeCondition) cond).getLikeComparand();
                } else {
                    return clctcomp.getField().getValue();
                }
            }
        } catch (CollectableFieldFormatException e) {
            LOG.warn("getFilterValue failed: " + e, e);
            JOptionPane.showConfirmDialog(
                    null,
                    SpringLocaleDelegate.getInstance().getMessage(
                            "subform.filter.exception", "Das Format der Suchkomponente '{0}' ist nicht korrekt.",
                            clctcomp.getEntityField().getLabel()),
                    SpringLocaleDelegate.getInstance().getMessage("subform.filter.exception.title", "Formatfehler"),
                    JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }

    public void setupTableHeaderForScrollPane(final JScrollPane scrollPane) {
        if (created) {
            if (UIUtils.findJComponent(scrollPane, "CornerPanel") == null) {
                // add resultfilter for fixed columns
                Component corner = scrollPane.getCorner(JScrollPane.UPPER_LEFT_CORNER);
                JPanel panel2 = new JPanel(new BorderLayout());
                panel2.setName("CornerPanel");
                panel2.add(corner, BorderLayout.CENTER);
                panel2.add(getFixedResultFilter(), BorderLayout.NORTH);
                scrollPane.setCorner(JScrollPane.UPPER_LEFT_CORNER, panel2);
            }
            if (UIUtils.findJComponent(scrollPane, "ColumnHeader") == null) {
                // add resultfilter for external table
                Component origColumnHeader = scrollPane.getColumnHeader().getView();
                JPanel panel1 = new JPanel(new BorderLayout());
                panel1.setName("ColumnHeader");
                panel1.add(origColumnHeader, BorderLayout.CENTER);
                panel1.add(getExternalResultFilter(), BorderLayout.NORTH);
                scrollPane.setColumnHeaderView(panel1);
            }
        }
    }

    public boolean isCollapsed() {
        if (created) {
            return getFixedResultFilter().isCollapsed();
        }
        return true;
    }

    public boolean isFilterSetup() {
        return collectableFieldsProviderFactory != null;
    }

    public void updateFilterColumns() {
    	if (created) {
			setupDirectResponse(externalResultFilter.updateColumn2Components(getSearchFilterComponents()));
			setupDirectResponse(fixedResultFilter.updateColumn2Components(getSearchFilterComponents()));

			if (!isCollapsed()) {
				externalResultFilter.arrangeFilterComponents();
				fixedResultFilter.arrangeFilterComponents();
			}
		}
	}

    public boolean isFilterStored() {
        return false;
    }

    protected abstract void loadTableFilter();

    public abstract JToggleButton getToggleButton();

    protected abstract JCheckBoxMenuItem getMenuItem();
}
