package org.nuclos.client.ui.collect.result;

import static java.awt.event.ActionEvent.ACTION_PERFORMED;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.JToggleButton;
import javax.swing.text.JTextComponent;

import org.nuclos.client.common.NuclosResultPanel;
import org.nuclos.client.common.controller.SearchConditionSource;
import org.nuclos.client.genericobject.valuelistprovider.MandatorCollectableFieldsProvider;
import org.nuclos.client.genericobject.valuelistprovider.ProcessCollectableFieldsProvider;
import org.nuclos.client.genericobject.valuelistprovider.StatusCollectableFieldsProvider;
import org.nuclos.client.genericobject.valuelistprovider.StatusNumeralCollectableFieldsProvider;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.ListOfValues;
import org.nuclos.client.ui.collect.CollectableResultComponent;
import org.nuclos.client.ui.collect.component.AbstractCollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.LabeledCollectableComponentWithVLP;
import org.nuclos.common.NuclosConstants;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.NuclosToolBarItems;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common.collection.Pair;
import org.nuclos.common2.LangUtils;

/**
 * Created by guenthse on 3/6/2017.
 */
public class ResultPanelFilter extends ResultFilter {

    private Action actFilter;
    private SearchConditionSource searchConditionSource;

    public ResultPanelFilter(ResultPanel pnlResult, Action actFilter, SearchConditionSource searchConditionSource) {
        super(pnlResult);
        this.actFilter = actFilter;
        this.searchConditionSource = searchConditionSource;
    }

    @Override
    protected JTable getFixedTable() {
        if (!(resultComponent instanceof NuclosResultPanel)) {
            return null;
        }

        NuclosResultPanel pnlResult = (NuclosResultPanel) resultComponent;
        return pnlResult.getFixedResultTable();
    }

    @Override
    protected FilterableTable getExternalTable() {
        if (!(resultComponent instanceof ResultPanel)) {
            return null;
        }

        ResultPanel pnlResult = (ResultPanel) resultComponent;

        return pnlResult.getResultTable();
    }

    /**
     * actionlistener to collapse or expand the searchfilter panels
     */
    protected void addActionListener() {
        //do nothing
//        if (!(resultComponent instanceof ResultPanel)) {
//            return;
//        }
//
//        ResultPanel pnlResult = (ResultPanel) resultComponent;
//
//        // action: Filter
//
//        pnlResult.registerToolBarAction(NuclosToolBarItems.FILTER, new CommonAbstractAction() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                if (!getFixedResultFilter().isCollapsed() || !getExternalResultFilter().isCollapsed()) {
//                    clearFilter();
//
//                    filterButton.setSelected(false);
//                } else {
//                    filter();
//
//                    filterButton.setSelected(true);
//                }
//
//                getFixedResultFilter().setCollapsed(!getFixedResultFilter().isCollapsed());
//                getExternalResultFilter().setCollapsed(!getExternalResultFilter().isCollapsed());
//            }
//        });
    }

    @Override
    protected void filter() {
        if (getFixedResultFilter() == null || getExternalResultFilter() == null ||  !(resultComponent instanceof ResultPanel))
            return;


        Map<UID, CollectableComponent> filterComponents = getAllFilterComponents();
        if (filterComponents == null) {
            return;
        }

        getFixedResultFilter().setVisible(true);
        getExternalResultFilter().setVisible(true);

        if (actFilter != null) {
            actFilter.actionPerformed(new ActionEvent(this, ACTION_PERFORMED, "filter"));
        }

    }

    @Override
    public void clearFilter() {
        Icon icon = Icons.getInstance().getIconFilter16();
        this.filterButton.setIcon(icon);

        for (CollectableComponent clctcomp : getAllFilterComponents().values()) {
            if (clctcomp instanceof AbstractCollectableComponent) {
                ((AbstractCollectableComponent) clctcomp).setSearchCondition(null);
            }
        }

        filteringActive = false;
    }

    @Override
    protected void loadTableFilter() {

    }

	@Override
    public void close() {
		super.close();
    	actFilter = null;
    	searchConditionSource = null;
	}

    @Override
    public JToggleButton getToggleButton() {
        if (!(resultComponent instanceof ResultPanel)) {
            return null;
        }

        ResultPanel pnlResult = (ResultPanel) resultComponent;
        JComponent[] components = pnlResult.getToolBar().getComponents(NuclosToolBarItems.FILTER);
        if (components == null) {
        	return filterButton;
		}
        for (JComponent component : components) {
            if (component instanceof JToggleButton) {
                return (JToggleButton) component;
            }
        }
        return null;
    }

    @Override
    protected JCheckBoxMenuItem getMenuItem() {
        return null;
    }

    private void setAdditionalSearchCondition(CollectableFieldsProvider valuelistprovider, UID refField) {
		// NUCLOS-6975 Try to restrict the content of the filters.
		if (searchConditionSource != null) {
			try {
				CollectableSearchCondition cond = searchConditionSource.getCollectableSearchConditionFromSearchPanel(false);
				if (cond != null) {
					Pair<UID, CollectableSearchCondition> par = new Pair<>(refField, cond);
					valuelistprovider.setParameter(NuclosConstants.VLP_ADDITIONAL_CONDITION_FOR_RESULTFILTER, par);
				}

			} catch (CollectableFieldFormatException cffe) {
				LOG.warn(cffe.getMessage(), cffe);
			}
		}
	}

    @Override
    protected void handleVLP(final UID sfEntityUid, final CollectableEntityField cef, final UID columnName, final CollectableComponent clctcomp, final CollectableResultComponent clctResultComponent) {

        if (!(clctResultComponent instanceof ResultPanel)) {
            return;
        }

		//BMWFDM-322 et.al: In the search (and only there) the ValueListProviders will not be considered for List of Values (LOV).
        //This has been already the case for standard search mask and from now for Subform-Search-Filters as well.

        final ResultPanel pnlResult = (ResultPanel) clctResultComponent;
        // handle valuelistprovider
        final LabeledCollectableComponentWithVLP clctWithVLP = (LabeledCollectableComponentWithVLP) clctcomp;
        final UID fieldUid = clctWithVLP.getFieldUID();
        CollectableFieldsProvider valuelistprovider = null;
        if (cef.isReferencing()) {

			valuelistprovider = collectableFieldsProviderFactory
                    .newDefaultCollectableFieldsProvider(fieldUid);
			setAdditionalSearchCondition(valuelistprovider, fieldUid);

        }
        clctWithVLP.setValueListProvider(valuelistprovider);
        clctWithVLP.refreshValueList(true);
		Set<UID> focusAlreadyGained = new HashSet<>();
        final FocusAdapter refreshVLPAdapter = new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
            	if (focusAlreadyGained.contains(fieldUid)) {
            		// No need to reload the data
            		return;
				}
				focusAlreadyGained.add(fieldUid);
                // set the value list provider (dynamically):
                CollectableFieldsProvider valuelistprovider = null;
                if (valuelistprovider == null) {
                    // If no provider was set, use the default provider for static cell editors by default:
                    if (LangUtils.equal(SF.STATE.getUID(sfEntityUid), fieldUid)) {
                        valuelistprovider = new StatusCollectableFieldsProvider(sfEntityUid, null);
                    } else if (LangUtils.equal(SF.STATENUMBER.getUID(sfEntityUid),
                            fieldUid)) {
                        valuelistprovider = new StatusNumeralCollectableFieldsProvider(sfEntityUid, null);
                    } else if (LangUtils.equal(SF.PROCESS.getUID(sfEntityUid),
                            fieldUid)) {
                        valuelistprovider = new ProcessCollectableFieldsProvider(sfEntityUid);
                    } else if (LangUtils.equal(SF.MANDATOR.getUID(sfEntityUid),
                            fieldUid)) {
                        valuelistprovider = new MandatorCollectableFieldsProvider(sfEntityUid, null);
                    } else {
                        valuelistprovider = collectableFieldsProviderFactory
                                .newDefaultCollectableFieldsProvider(fieldUid);
                        setAdditionalSearchCondition(valuelistprovider, fieldUid);
                    }
                }
                clctWithVLP.setValueListProvider(valuelistprovider);

                JTextComponent compText = null;
                JComponent comp = clctcomp.getControlComponent();
                if (comp instanceof ListOfValues) {
                    compText = ((ListOfValues) comp).getJTextField();
                } else if (comp instanceof JComboBox) {
                    compText = (JTextComponent) ((JComboBox) comp).getEditor().getEditorComponent();
                }

                // remember old value here.
                String clctfValue = compText.getText();

				// refresh value list:
                clctWithVLP.refreshValueList(false);
                compText.setText(clctfValue);
            }

			@Override
			public void focusLost(final FocusEvent e) {
            	focusAlreadyGained.remove(fieldUid);
			}
		};
        JComponent comp = clctcomp.getControlComponent();
        if (comp instanceof ListOfValues) {
            ((ListOfValues) comp).getJTextField().addFocusListener(refreshVLPAdapter);
        } else if (comp instanceof JComboBox) {
        	for (Component c : comp.getComponents()) {
                if (c instanceof JButton)
                    c.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseEntered(MouseEvent e) {
                            refreshVLPAdapter.focusGained(null);
                        }
                    });
            }
            comp.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseExited(final MouseEvent e) {
					refreshVLPAdapter.focusLost(null);
				}
			});
            ((JComboBox) comp).getEditor().getEditorComponent().addFocusListener(refreshVLPAdapter);
        }
    }
}
