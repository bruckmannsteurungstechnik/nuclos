//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.search;

import java.awt.Cursor;
import java.util.List;

import org.nuclos.client.datasource.DatasourceDelegate;
import org.nuclos.client.datasource.admin.CollectableDataSource;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.SearchConditionToPredicateVisitor;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.report.valueobject.ChartVO;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

public class ChartSearchStrategy extends CollectSearchStrategy<UID,CollectableDataSource<ChartVO>> {

	private final DatasourceDelegate datasourcedelegate = DatasourceDelegate.getInstance();

	public ChartSearchStrategy() {
		//...
	}

	@Override
	public void search() {
		final CollectController cc = getCollectController();
		final MainFrameTab mft = cc.getTab();
		try {
			mft.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

			List<MasterDataVO<UID>> lstmdvos = CollectionUtils.transform(this.datasourcedelegate.getAllCharts(), new MakeMasterDataVO());
			
			final CollectableSearchExpression clctexpr
				= new CollectableSearchExpression(getCollectableSearchCondition(), cc.getResultController().getCollectableSortingSequence());
			final List<CollectableEntityField> cefs = cc.getResultController().getFields().getSelectedFields();
			final String isp = cc.getResultPanel().fetchIncrSearchPattern();
			ClientSearchUtils.addTextSearchToSearchExpression(isp, cefs, clctexpr, cc.getEntityUid());
			
			final CollectableSearchCondition cond = clctexpr.getSearchCondition();
			if (cond != null)
				lstmdvos = CollectionUtils.<MasterDataVO<UID>>applyFilter(lstmdvos, cond.accept(new SearchConditionToPredicateVisitor()));
			
			List<CollectableDataSource> result = CollectionUtils.transform(lstmdvos, new MakeCollectable());
			if (getCollectableIdListCondition() != null) {
				result = CollectionUtils.applyFilter(result, new CollectableIdPredicate(getCollectableIdListCondition().getIds()));
			}
			
			cc.fillResultPanel(result);
		} catch (Exception ex) {
			Errors.getInstance().showExceptionDialog(mft, null, ex);
		} finally {
			mft.setCursor(Cursor.getDefaultCursor());
		}
	}

	private static class MakeCollectable implements Transformer<MasterDataVO<UID>, CollectableDataSource> {
		@Override
		public CollectableDataSource transform(MasterDataVO<UID> mdVO) {
			return new CollectableDataSource(getDatasourceVO(mdVO));
		}
		public static ChartVO getDatasourceVO(MasterDataVO<UID> mdVO) {
			ChartVO vo = new ChartVO(new NuclosValueObject<UID>(
					mdVO.getPrimaryKey(), mdVO.getCreatedAt(), mdVO.getCreatedBy(), mdVO.getChangedAt(), mdVO.getChangedBy(), mdVO.getVersion()),
				mdVO.getFieldValue(E.CHART.name),
				mdVO.getFieldValue(E.CHART.description),
				mdVO.getFieldUid(E.CHART.detailsEntity),
				mdVO.getFieldUid(E.CHART.parentEntity),
				mdVO.getFieldValue(E.CHART.valid),
				mdVO.getFieldValue(E.CHART.source),
				mdVO.getFieldUid(E.CHART.nuclet));
			
			vo.setNuclet((String) mdVO.getFieldValue(E.CHART.nuclet.getUID()));
			vo.setDetailsEntity((String) mdVO.getFieldValue(E.CHART.detailsEntity.getUID()));
			vo.setParentEntity((String) mdVO.getFieldValue(E.CHART.parentEntity.getUID()));
			vo.setMeta(mdVO.getFieldValue(E.CHART.meta));
			vo.setQuery(mdVO.getFieldValue(E.CHART.query));
			
			return vo;
		}
	}	
	private static class MakeMasterDataVO implements Transformer<ChartVO, MasterDataVO<UID>> {
		@Override
		public MasterDataVO<UID> transform(ChartVO vo) {
			return wrapDatasourceVO(vo);
		}
		public static MasterDataVO<UID> wrapDatasourceVO(DatasourceVO vo) {
			MasterDataVO<UID> result = new MasterDataVO<UID>(E.CHART.getUID(), vo.getPrimaryKey(), 
					vo.getChangedAt(), vo.getCreatedBy(), vo.getChangedAt(), vo.getChangedBy(), vo.getVersion());
			result.setFieldValue(E.CHART.name, vo.getName());
			result.setFieldValue(E.CHART.description, vo.getDescription());
			result.setFieldUid(E.CHART.detailsEntity, ((ChartVO) vo).getDetailsEntityUID());
			result.setFieldValue(E.CHART.detailsEntity.getUID(), ((ChartVO) vo).getDetailsEntity());
			result.setFieldUid(E.CHART.parentEntity, ((ChartVO) vo).getParentEntityUID());
			result.setFieldValue(E.CHART.parentEntity.getUID(), ((ChartVO) vo).getParentEntity());
			result.setFieldValue(E.CHART.valid, vo.getValid());
			result.setFieldValue(E.CHART.source, vo.getSource());
			result.setFieldUid(E.CHART.nuclet, vo.getNucletUID());
			result.setFieldValue(E.CHART.nuclet.getUID(), vo.getNuclet());
			
			result.setFieldValue(E.CHART.meta, vo.getMeta());
			result.setFieldValue(E.CHART.query, vo.getQuery());
			
			return result;
		}
	}
}

