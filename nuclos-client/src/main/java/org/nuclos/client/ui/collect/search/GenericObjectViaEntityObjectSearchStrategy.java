//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Observer;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.dal.DalSupportForGO;
import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.entityobject.CollectableEntityObjectProxyListAdapter;
import org.nuclos.client.entityobject.EntityObjectDelegate;
import org.nuclos.client.genericobject.CollectableGenericObjectWithDependants;
import org.nuclos.client.genericobject.GenericObjectCollectController;
import org.nuclos.common.CollectableEntityFieldWithEntityForExternal;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.collect.collectable.searchcondition.SearchConditionUtils;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.entityobject.CollectableEOEntityField;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.genericobject.ProxyList;
import org.nuclos.server.genericobject.searchcondition.CollectableGenericObjectSearchExpression;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

public class GenericObjectViaEntityObjectSearchStrategy extends CollectSearchStrategy<Long,CollectableGenericObjectWithDependants> {

	// static

	private static final Logger LOG = Logger.getLogger(GenericObjectViaEntityObjectSearchStrategy.class);

	// Caches eo2Go results
	private final Cache<MultiKey, CollectableGenericObjectWithDependants> cache = CacheBuilder.newBuilder()
			.expireAfterAccess(5, TimeUnit.MINUTES)
			.maximumSize(1000)
			.build();

	private final class MyProxyListAdapter implements ProxyList<Long,CollectableGenericObjectWithDependants> {

		private final class MyListIterator implements ListIterator<CollectableGenericObjectWithDependants> {

			private final ListIterator<CollectableEntityObject<Long>> iter;

			public MyListIterator(ListIterator<CollectableEntityObject<Long>> iter) {
				this.iter = iter;
			}

			@Override
			public boolean hasNext() {
				return iter.hasNext();
			}

			@Override
			public CollectableGenericObjectWithDependants next() {
				return eo2Go(iter.next());
			}

			@Override
			public boolean hasPrevious() {
				return iter.hasPrevious();
			}

			@Override
			public CollectableGenericObjectWithDependants previous() {
				return eo2Go(iter.previous());
			}

			@Override
			public int nextIndex() {
				return iter.nextIndex();
			}

			@Override
			public int previousIndex() {
				return iter.previousIndex();
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}

			@Override
			public void set(CollectableGenericObjectWithDependants e) {
				iter.set(go2Eo(e));
			}

			@Override
			public void add(CollectableGenericObjectWithDependants e) {
				throw new UnsupportedOperationException();
			}

		}

		private final CollectableEntityObjectProxyListAdapter<Long> list;

		private MyProxyListAdapter(CollectableEntityObjectProxyListAdapter<Long> list) {
			this.list = list;
		}

		@Override
		public int size() {
			return list.size();
		}

		@Override
		public boolean isEmpty() {
			return list.isEmpty();
		}

		@Override
		public boolean contains(Object o) {
			if (!(o instanceof CollectableGenericObjectWithDependants)) return false;
			final CollectableGenericObjectWithDependants go = (CollectableGenericObjectWithDependants) o;
			return getIndexById(go.getId()) >= 0;
		}

		@Override
		public Object[] toArray() {
			throw new UnsupportedOperationException();
		}

		@Override
		public <T> T[] toArray(T[] a) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean add(CollectableGenericObjectWithDependants e) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean remove(Object o) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean containsAll(Collection<?> c) {
			for (Object i: c) {
				if (!contains(i)) return false;
			}
			return true;
		}

		@Override
		public boolean addAll(Collection<? extends CollectableGenericObjectWithDependants> c) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean addAll(int index, Collection<? extends CollectableGenericObjectWithDependants> c) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean removeAll(Collection<?> c) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean retainAll(Collection<?> c) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void clear() {
			list.clear();
		}

		@Override
		public CollectableGenericObjectWithDependants get(int index) {
			return eo2Go(list.get(index));
		}

		@Override
		public CollectableGenericObjectWithDependants set(int index, CollectableGenericObjectWithDependants element) {
			CollectableEntityObject<Long> eo = go2Eo(element);

			cache.invalidate(eoCacheKey(eo));

			// TODO: Do we really have to wrap the eo again?
			return eo2Go(list.set(index, eo));
		}

		@Override
		public void add(int index, CollectableGenericObjectWithDependants element) {
			throw new UnsupportedOperationException();
		}

		@Override
		public CollectableGenericObjectWithDependants remove(int index) {
			return eo2Go(list.remove(index));
		}

		@Override
		public int indexOf(Object o) {
			if (!(o instanceof CollectableGenericObjectWithDependants)) return -1;
			final CollectableGenericObjectWithDependants go = (CollectableGenericObjectWithDependants) o;
			return getIndexById(go.getId());
		}

		@Override
		public int lastIndexOf(Object o) {
			throw new UnsupportedOperationException();
		}

		@Override
		public ListIterator<CollectableGenericObjectWithDependants> listIterator() {
			return new MyListIterator(list.listIterator());
		}

		@Override
		public ListIterator<CollectableGenericObjectWithDependants> listIterator(int index) {
			return new MyListIterator(list.listIterator(index));
		}

		@Override
		public List<CollectableGenericObjectWithDependants> subList(int fromIndex, int toIndex) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void fetchDataIfNecessary(int iIndex, ProxyList.IChangeListener changelistener) {
			list.fetchDataIfNecessary(iIndex, changelistener);
		}

		@Override
		public int getLastIndexRead() {
			return list.getLastIndexRead();
		}

		@Override
		public boolean hasObjectBeenReadForIndex(int index) {
			return list.hasObjectBeenReadForIndex(index);
		}

		@Override
		public Iterator<CollectableGenericObjectWithDependants> iterator() {
			return listIterator();
		}

		@Override
		public int getIndexById(Long oId) {
			return list.getIndexById(oId);
		}

		@Override
		public CollectableGenericObjectWithDependants getRaw(int index) {
			return eo2Go(list.getRaw(index));
		}

		@Override
		public CollectableGenericObjectWithDependants getRawById(Long id) {
			return eo2Go(list.getRawById(id));
		}

		@Override
		public int indexOf(CollectableGenericObjectWithDependants element) {
			return list.indexOf(element);
		}

		@Override
		public Long getIdByIndex(int index) {
			return list.getIdByIndex(index);
		}

		@Override
		public Collection<Long> getAllLoadedIds() {
			return list.getAllLoadedIds();
		}
		
		@Override
		public boolean isHugeList() {
			return list.isHugeList();
		}
		
		@Override
		public int totalSize(boolean forceCount, final Long limit) {
			return list.totalSize(forceCount, limit);
		}
		
		@Override
		public Collection<CollectableGenericObjectWithDependants> getBulk(int[] indizes) {
			Collection<CollectableGenericObjectWithDependants> coll = new ArrayList<CollectableGenericObjectWithDependants>();
			for (CollectableEntityObject<Long> ceo : list.getBulk(indizes)) {
				coll.add(eo2Go(ceo));
			}
			return coll;
		}
		
		@Override
		public boolean isOnlySequentialPaging() {
			return list.isOnlySequentialPaging();
		}

	}

	// instance

	private final EntityObjectDelegate lodelegate = EntityObjectDelegate.getInstance();

	private final CollectableEntity meta;

	private ProxyList<Long,CollectableGenericObjectWithDependants> proxylstclct;

	// contructor

	public GenericObjectViaEntityObjectSearchStrategy(CollectableEntity meta) {
		this.meta = meta;
	}

	protected final GenericObjectCollectController getGenericObjectController() {
		return (GenericObjectCollectController) getCollectController();
	}

	/**
	 * §postcondition result != null
	 * §own-thread
	 *
	 * TODO: Make this private again.
	 * 
	 * @return List&lt;Collectable&gt;
	 * @throws CollectableFieldFormatException
	 */
	@Override
	public ProxyList<Long,CollectableGenericObjectWithDependants> getSearchResult() throws CollectableFieldFormatException {
		final CollectableGenericObjectSearchExpression clctexprInternal = (CollectableGenericObjectSearchExpression) getSearchExpressionForExport();
		
		final GenericObjectCollectController gocc = getGenericObjectController();
		CollectableSearchExpression clctexpr = null;
		if (gocc.getProcess() != null && gocc.isCalledByProcessMenu()) {
			CompositeCollectableSearchCondition clctsc = new CompositeCollectableSearchCondition(LogicalOperator.AND);
			if (clctexprInternal.getSearchCondition() != null) {
				clctsc.addOperand(clctexprInternal.getSearchCondition());
			}
			clctsc.addOperand(new CollectableComparison(gocc.getCollectableEntity().getEntityField(SF.PROCESS_UID.getUID(gocc.getEntityUid())), ComparisonOperator.EQUAL, gocc.getProcess()));
			clctexpr = new CollectableGenericObjectSearchExpression(SearchConditionUtils.simplified(clctsc), clctexprInternal.getSortingOrder(), clctexprInternal.getSearchDeleted());
		}
		
		Collection<UID> fields = new HashSet<UID>();
		for (FieldMeta<?> fm : getFields()) {
			fields.add(fm.getUID());
		}
		try {
			final ProxyList<Long,EntityObjectVO<Long>> proxylstlovwdvo =
					lodelegate.getEntityObjectProxyList(gocc.getModuleId(), clctexpr != null ? clctexpr : clctexprInternal, fields, gocc.getCustomUsage());
			return new MyProxyListAdapter(new CollectableEntityObjectProxyListAdapter<Long>(proxylstlovwdvo, meta));
		} catch (CommonPermissionException cpe) {
			throw new NuclosFatalException(cpe);
		}

	}
	
	@Override
	public Collection<FieldMeta<?>> getFields() {
		final Collection<FieldMeta<?>> fields = new ArrayList<FieldMeta<?>>();

		for (CollectableEntityField f: getCollectableFields()) {
			// We need a FieldMeta from the CollectableEntityField.

			// Case 0: Wrapped CollectableEntityField: unwrap.
			if (f instanceof CollectableEntityFieldWithEntityForExternal) {
				f = ((CollectableEntityFieldWithEntityForExternal) f).getField();
			}
			// Case 1: CollectableEOEntityField: already there!
			if (f instanceof CollectableEOEntityField) {
				final CollectableEOEntityField field = (CollectableEOEntityField) f;
				fields.add(field.getMeta());
			}
			// Case 2: Use MetaDataProv
			else {
				final IMetaProvider mdProv = MetaProvider.getInstance();
				fields.add(mdProv.getEntityField(f.getUID()));
			}
		}
		return fields;
	}
	
	

	@Override
	public SearchWorker<Long,CollectableGenericObjectWithDependants> getSearchWorker() {
		return new GenericObjectObservableSearchWorker(getGenericObjectController());
	}

	@Override
	public SearchWorker<Long,CollectableGenericObjectWithDependants> getSearchWorker(List<Observer> lstObservers) {
		GenericObjectObservableSearchWorker observableSearchWorker = new GenericObjectObservableSearchWorker(getGenericObjectController());
		for (Observer observer : lstObservers)
			observableSearchWorker.addObserver(observer);
		return observableSearchWorker;
	}

	@Override
	public void search() throws CommonBusinessException {
		this.search(false);
	}

	@Override
	public void search(boolean bRefreshOnly) throws CommonBusinessException {
		final GenericObjectCollectController cc = getGenericObjectController();
		if (cc.getResultController().getSearchResultStrategy().isInitializing()) {
			LOG.debug("PREVENT search during init");
			return;
		}
		LOG.debug("START search");
		cc.makeConsistent(true);
		cc.removePreviousChangeListenersForResultTableVerticalScrollBar();
		final ProxyList<Long,CollectableGenericObjectWithDependants> lstclctResult = getSearchResult();
		setCollectableProxyList(lstclctResult);
		cc.fillResultPanel(lstclctResult, lstclctResult.totalSize(false, null), false);
		cc.setupChangeListenerForResultTableVerticalScrollBar();
		LOG.debug("FINISHED search");
	}
	
	@Override
	public CollectableSearchCondition getCollectableSearchCondition() throws CollectableFieldFormatException {
		return (getCollectableIdListCondition() != null) ? addSearchFilter(getCollectableIdListCondition()) : super.getCollectableSearchCondition();
	}
	
	
	/**
	 * @return search expression containing the internal version of the collectable search condition,
	 *         that is used for performing the actual search, and the sorting sequence.
	 */
	@Override
	public CollectableGenericObjectSearchExpression getInternalSearchExpression() throws CollectableFieldFormatException {
		final GenericObjectCollectController cc = getGenericObjectController();
		final CollectableGenericObjectSearchExpression clctGOSearchExpression = new CollectableGenericObjectSearchExpression(getInternalSearchCondition(),
					cc.getResultController().getCollectableSortingSequence(), cc.getSearchDeleted());
		return clctGOSearchExpression;
	}

	private CollectableEntityObject<Long> go2Eo(CollectableGenericObjectWithDependants go) {
		return new CollectableEntityObject<>(meta,
				DalSupportForGO.wrapGenericObjectVO(go.getGenericObjectCVO()));
	}

	private CollectableGenericObjectWithDependants eo2Go(CollectableEntityObject<Long> eo) {
		if (eo == null) {
			LOG.warn("eo2Go: CollectableEntityObject is null");
			return null;
		}

		try {
			return cache.get(
					eoCacheKey(eo),
					() -> CollectableGenericObjectWithDependants.newCollectableGenericObjectWithDependants(
									DalSupportForGO.getGenericObjectWithDependantsVO(
											eo.getEntityObjectVO(),
											meta
									)
							)
			);
		} catch (ExecutionException e) {
			throw new NuclosFatalException(e);
		}
	}

	private static MultiKey eoCacheKey(CollectableEntityObject<Long> eo) {
		return new MultiKey(
				eo.getId(),
				eo.getMasterDataCVO().getFieldValues().hashCode()
		);
	}

	/**
	 * TODO: Make this protected again.
	 *
	 * @deprecated Move to ObservableSearchWorker???
	 */
	@Override
	public void setCollectableProxyList(ProxyList<Long,CollectableGenericObjectWithDependants> proxylstclct) {
		this.proxylstclct = proxylstclct;
	}

	// @todo move to ResultController or ResultPanel?
	@Override
	public ProxyList<Long,CollectableGenericObjectWithDependants> getCollectableProxyList() {
		return proxylstclct;
	}

}
