package org.nuclos.client.ui.collect.subform;

import java.awt.Component;

import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import org.nuclos.client.ui.collect.DynamicRowHeightSupport;
import org.nuclos.common2.StringUtils;

/**
 * Created by Oliver Brausch on 17.07.17.
 */
public class DynamicRowHeightCellRenderer implements TableCellRenderer {

	private final TableCellRenderer mainRenderer;

	DynamicRowHeightSupport support;

	private final int col;

	private final RowHeightController ctrl;

	public DynamicRowHeightCellRenderer(TableCellRenderer mainRenderer, DynamicRowHeightSupport support, int col, RowHeightController ctrl) {
		super();
		this.mainRenderer = mainRenderer;
		this.support = support;
		this.col = col;
		this.ctrl = ctrl;
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		Component c = mainRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

		ctrl.setHeight(col, row, support.getHeight(c));
		if (c instanceof JScrollPane && value != null) {
			Component comp = ((JScrollPane)c).getViewport().getView();
			((JComponent)comp).setToolTipText(StringUtils.splitTextToMultilineHtml(value.toString()));
		}
		return c;
	}
}
