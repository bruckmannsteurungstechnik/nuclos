//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.subform;

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.InvocationEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.RowSorter.SortKey;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.RowSorterEvent;
import javax.swing.event.RowSorterListener;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import org.apache.log4j.Logger;
import org.nuclos.client.common.WorkspaceUtils;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.SelectFixedColumnsController;
import org.nuclos.client.ui.collect.SelectFixedColumnsPanel;
import org.nuclos.client.ui.collect.ToolTipsTableHeader;
import org.nuclos.client.ui.collect.component.model.ChoiceEntityFieldList;
import org.nuclos.client.ui.collect.model.CollectableEntityFieldBasedTableModel;
import org.nuclos.client.ui.table.CommonJTable;
import org.nuclos.client.ui.table.SortableTableModel;
import org.nuclos.client.ui.table.TableCellEditorProvider;
import org.nuclos.client.ui.table.TableCellRendererProvider;
import org.nuclos.client.ui.table.TableHeaderMouseListenerForSorting;
import org.nuclos.common.Actions;
import org.nuclos.common.ProfileUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableUtils;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.preferences.TablePreferencesManager;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.PreferencesException;

/**
 *
 * @author <a href="mailto:rainer.schneider@novabit.de">rainer.schneider</a>
 *
 */
public class FixedColumnRowHeader extends SubformRowHeader {

	private static final Logger LOG = Logger.getLogger(FixedColumnRowHeader.class);
	
	public static final Color FIXED_HEADER_BACKGROUND = new Color(100, 100, 100, 50);

	private List<UID> lstFixedColumnCollNames;
	private Map<Object, Integer> mpFixedColumnCollWidths;
	private List<Integer> lstHeaderColumnWidthsFromPref;
	private ToolTipsTableHeader tableHeader;
	private TableHeaderMouseListenerForSorting sortingListener;
	private TableColumnModelListener headerColumnModelListener;
	
	private final SubForm subform;
	private final TablePreferencesManager tblprefManager;
	
	public FixedColumnRowHeader(SubForm subform, TablePreferencesManager tblprefManager) {
		super();
		this.subform = subform;
		this.tblprefManager = tblprefManager;
		this.lstFixedColumnCollNames = new ArrayList<UID>();
		this.mpFixedColumnCollWidths = new HashMap<Object, Integer>();
		this.initColumnModelListener();
		getHeaderTable().getColumnModel().addColumnModelListener(headerColumnModelListener);
	}
	
	private void initColumnModelListener() {

		this.headerColumnModelListener = new TableColumnModelListener() {
			@Override
            public void columnAdded(TableColumnModelEvent e) {
			}

			@Override
            public void columnRemoved(TableColumnModelEvent e) {
			}

			@Override
            public void columnMoved(TableColumnModelEvent e) {
				if (e.getFromIndex() != e.getToIndex()) {
					lstFixedColumnCollNames = getColumnNames();
					mpFixedColumnCollWidths = getVisibleColumnWidth();
				}
			}

			@Override
            public void columnMarginChanged(ChangeEvent e) {
				invalidateHeaderTable();
				mpFixedColumnCollWidths = getVisibleColumnWidth();
			}

			@Override
            public void columnSelectionChanged(ListSelectionEvent e) {
			}

		};
	}

	@Override
	protected JTable createHeaderTable() {

		HeaderTable newHeaderTable = new HeaderTable();
		this.tableHeader = new FixedRowToolTipsTableHeader(null, newHeaderTable.getColumnModel());
		newHeaderTable.setTableHeader(this.tableHeader);
		return newHeaderTable;
	}

	@Override
	protected RowIndicatorTableModel createHeaderTableModel() {
		return new FixedRowIndicatorTableModel();
	}

	@Override
	protected void setRowHeight(int rowHeight) {
		super.setRowHeight(rowHeight);
		invalidateHeaderTable();
	}

	@Override
	protected void setRowHeightInRow(int row, int iRowHeight) {
		super.setRowHeightInRow(row, iRowHeight);
	}

	@Override
	protected void setExternalTable(SubFormTable tableToAddHeader, JScrollPane scrlpOriginalTable) {
		super.setExternalTable(tableToAddHeader, scrlpOriginalTable);
		
		tableToAddHeader.addPropertyChangeListener("rowSorter", new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if ("rowSorter".equals(evt.getPropertyName())) {
					rowSorterChanged();
				}
			}
		});
		rowSorterChanged();
		
		((HeaderTable) getHeaderTable()).setTableCellEditorProvider(tableToAddHeader.getTableCellEditorProvider());
		((HeaderTable) getHeaderTable()).setTableCellRendereProvider(tableToAddHeader.getTableCellRendererProvider());
		((HeaderTable) getHeaderTable()).setExternalTable(tableToAddHeader);
		getHeaderTable().setBackground(getHeaderTable().getBackground());
		getScrlpnOriginalTable().setRowHeaderView(getHeaderTable());
		getScrlpnOriginalTable().getRowHeader().setBackground(getHeaderTable().getBackground());
		getScrlpnOriginalTable().getRowHeader().setPreferredSize(getHeaderTable().getPreferredSize());
		getScrlpnOriginalTable().setCorner(JScrollPane.UPPER_LEFT_CORNER, getHeaderTable().getTableHeader());

		invalidateHeaderTable();
	}
	
	private void rowSorterChanged() {
		final SubFormTable external = getExternalTable();
		if (external != null) {
			RowSorter<? extends TableModel> rowSorter = getExternalTable().getRowSorter();
			if (rowSorter != null) {
				rowSorter = new FixedRowIndicicatorRowSorter<FixedRowIndicatorTableModel>(
						(FixedRowIndicatorTableModel) getHeaderTable().getModel(), rowSorter);
			}
			getHeaderTable().setRowSorter(rowSorter);
		}
	}

	/**
	 * command: select columns
	 * Lets the user select the columns to show in the result list.
	 */
	private void cmdSelectColumns(final SortableTableModel tblmodel) {
		if (!SecurityCache.getInstance().isActionAllowed(Actions.ACTION_CUSTOMIZE_ENTITY_AND_SUBFORM_COLUMNS) &&
				WorkspaceUtils.getInstance().getWorkspace().isAssigned()) {
			return;
		}
		
		final SelectFixedColumnsController ctl = new SelectFixedColumnsController(this.getHeaderTable(), new SelectFixedColumnsPanel());
		final Comparator<CollectableEntityField> comp = (Comparator<CollectableEntityField>) getCollectableEntityFieldComparator();
		final SortedSet<CollectableEntityField> lstAvailable = getAllAvailableFields(comp);
		final List<CollectableEntityField> lstFixed = getDisplayedHeaderTableFields();
		final List<CollectableEntityField> lstSelected = new ArrayList<CollectableEntityField>(lstFixed);
		final ChoiceEntityFieldList ro = new ChoiceEntityFieldList(new HashSet<CollectableEntityField>(lstFixed));
		ro.set(lstAvailable, lstSelected, comp);

		lstSelected.addAll(getDisplayedExternalTableFields());

		for (CollectableEntityField curField : lstSelected) {
			lstAvailable.remove(curField);
		}

		// remember the widths of the currently visible columns
		final Map<Object,Integer> mpWidths = getVisibleColumnWidth();
		ctl.setModel(ro);
		
		final List<CollectableEntityField> lstSelectedOld = new ArrayList<CollectableEntityField>((List<CollectableEntityField>) ro.getSelectedFields());
		try {
			final boolean bOK = ctl.run(SpringLocaleDelegate.getInstance().getMessage(
					"SelectColumnsController.1", "Anzuzeigende Spalten ausw\u00e4hlen"));

			if (bOK) {
				final List<CollectableEntityField> lstSelectedNew = ctl.getSelectedObjects();
				final List<Integer> lstSortColumns = new ArrayList<Integer>();
				final Collection<? extends CollectableEntityField> collDeselected = CollectionUtils.subtract(lstSelectedOld, lstSelectedNew);
				for (CollectableEntityField colEntityField : collDeselected) {
					lstSortColumns.add(this.getExternalModel().findColumnByFieldUid(colEntityField.getUID()));
				}
				changeSelectedColumns(ctl.getSelectedObjects(), ctl.getFixedObjects(), null, mpWidths, null, new AbstractAction() {
					@Override
					public void actionPerformed(ActionEvent e) {
						List<? extends SortKey> sortKeys = new ArrayList<SortKey>(tblmodel.getSortKeys());
						for (Iterator iterator = lstSortColumns.iterator(); iterator.hasNext(); ) {
							Integer iColumn = (Integer) iterator.next();

							for (SortKey sortKey : tblmodel.getSortKeys()) {
								if (sortKey.getColumn() == iColumn.intValue())
									sortKeys.remove(sortKey);
							}
						}
						tblmodel.setSortKeys(sortKeys, true);
					}
				});

				writeColumnChangesToPreferences(lstSelectedNew, mpWidths);
			}
		} finally {
			ctl.close();
		}
	}
	
	public void hideCollectableEntityFieldColumn(CollectableEntityField clctefToHide, final Action actAfterSelection) {
		final List<CollectableEntityField> lstFixed = getDisplayedHeaderTableFields();
		
		final List<CollectableEntityField> lstSelected = new ArrayList<CollectableEntityField>(lstFixed);
		lstSelected.addAll(getDisplayedExternalTableFields());
		final List<CollectableEntityField> lstSelectedNew = new ArrayList<CollectableEntityField>(lstSelected);
		lstSelectedNew.remove(clctefToHide);
		
		if (lstSelectedNew.size() == 0) {
			JOptionPane.showMessageDialog(null, 
					SpringLocaleDelegate.getInstance().getMessage(
							"SelectFixedColumnsController.3","Es d\u00fcrfen nicht alle Spalten ausgeblendet oder fixiert werden."));
		} else {
			changeSelectedColumns(lstSelectedNew, new HashSet<CollectableEntityField>(lstFixed), null, getVisibleColumnWidth(), null, actAfterSelection);

			writeColumnChangesToPreferences(lstSelectedNew, getVisibleColumnWidth());
		}
	}

	private void writeColumnChangesToPreferences(final List<CollectableEntityField> lstSelectedNew, final Map<Object, Integer> mpWidths) {
		final List<UID> selectedFieldUIDs = new ArrayList<>();
		final List<Integer> fieldwidths = new ArrayList<>();
		for (CollectableEntityField selectedField : lstSelectedNew) {
			UID fieldUID = selectedField.getUID();
			selectedFieldUIDs.add(fieldUID);
			if (mpWidths.containsKey(fieldUID)) {
				fieldwidths.add(mpWidths.get(fieldUID));
			} else {
				fieldwidths.add(ProfileUtils.getMinimumColumnWidth(selectedField.getJavaClass()));
			}
		}
		// fixed is stored later...
		tblprefManager.setColumnPreferences(selectedFieldUIDs, fieldwidths, null);
	}
	
	/**
	 * 
	 * @param lstSelectedNew
	 * @param setFixedNew
	 * @param mpWidths
	 */
	public void changeSelectedColumns(
			final List<CollectableEntityField> lstSelectedNew, 
			final Set<CollectableEntityField> setFixedNew, 
			final List<Integer> allFieldWidths,
			final Map<Object,Integer> mpWidths,
			final Action actResetToDefaultWidths,
			final Action actAfterSelection) {
		UIUtils.runCommand(this.getHeaderTable(), new CommonRunnable() {
			@Override
            public void run() /* throws CommonBusinessException */ {
				final SubFormTable external = getExternalTable();
				if (external == null) {
					return;
				}
				
				final int iSelRow = external.getSelectedRow();
				final List<CollectableEntityField> lstFixedNew = new ArrayList<CollectableEntityField>(setFixedNew.size());
				for (CollectableEntityField curField : lstSelectedNew) {
					if (setFixedNew.contains(curField)) {
						lstFixedNew.add(curField);
					}
				}
				
				lstFixedColumnCollNames = CollectableUtils.getFieldUidsFromCollectableEntityFields(lstFixedNew);
				synchronizeColumnsInHeaderTable(lstFixedColumnCollNames);
				lstSelectedNew.removeAll(lstFixedNew);
				
				synchronizeColumnsInExternalTable(lstSelectedNew);

				// reselect the previously selected row (which gets lost be refreshing the model)
				if (iSelRow != -1) {
					external.setRowSelectionInterval(iSelRow, iSelRow);
				}
				
				if (allFieldWidths != null && !allFieldWidths.isEmpty()) { 
					restoreColumnWidthsFromPrefs(allFieldWidths);
				} else if (mpWidths != null && !mpWidths.isEmpty()) {
					restoreColumnWidthsInExternalTable(mpWidths);
					restoreColumnWidthsInHeaderTable(mpWidths);
				} else if (actResetToDefaultWidths != null) {
					actResetToDefaultWidths.actionPerformed(new ActionEvent(FixedColumnRowHeader.this, 1, "reset to default widths"));
				}
				mpFixedColumnCollWidths = getVisibleColumnWidth();
				invalidateHeaderTable();
				
				if (actAfterSelection != null)
					actAfterSelection.actionPerformed(new ActionEvent(FixedColumnRowHeader.this, 2, "after selection"));
			}
		});
	}

	/**
	 * Get all possible column field from the table model
	 * @return List of CollectableEntityField
	 */
	private SortedSet<CollectableEntityField> getAllAvailableFields(Comparator<CollectableEntityField> comp) {
		final SortedSet<CollectableEntityField> resultList = new TreeSet<CollectableEntityField>(comp);
		resultList.addAll(getAllAvailableFields());
		return resultList;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<CollectableEntityField> getAllAvailableFields() {
		final List<CollectableEntityField> resultList = new ArrayList<CollectableEntityField>();
		final CollectableEntityFieldBasedTableModel subformtblmdl = getExternalModel();
		for (int iColumnNr = 0; iColumnNr < subformtblmdl.getColumnCount(); iColumnNr++) {
			final CollectableEntityField clctef = subformtblmdl.getCollectableEntityField(iColumnNr);
			if (!clctef.isHidden()) {
				resultList.add(clctef);
			}
		}
		return resultList;
	}

	/**
	 * get the CollectableEntityField of the displayed columns of the external table
	 * @return List of CollectableEntityField
	 */
	private List<CollectableEntityField> getDisplayedExternalTableFields() {
		final SubFormTable external = getExternalTable();
		if (external != null) {
			TableColumnModel externalColumnModel = external.getColumnModel();
			List<CollectableEntityField> resultList = new ArrayList<CollectableEntityField>();
			for (Enumeration<TableColumn> columnEnum = externalColumnModel.getColumns(); columnEnum.hasMoreElements();) {
				TableColumn varColumn = columnEnum.nextElement();
				final int iModelColumn = varColumn.getModelIndex();
				final CollectableEntityField clctefTarget = getExternalModel().getCollectableEntityField(iModelColumn);
				resultList.add(clctefTarget);
			}
			return resultList;
		} else {
			return Collections.emptyList();
		}
	}

	/**
	 * get the CollectableEntityField of the displayed columns of the header table
	 * @return List of CollectableEntityField
	 */
	private List<CollectableEntityField> getDisplayedHeaderTableFields() {
		TableColumnModel externalColumnModel = getHeaderTable().getColumnModel();
		List<CollectableEntityField> resultList = new ArrayList<CollectableEntityField>();
		for (Enumeration<TableColumn> columnEnum = externalColumnModel.getColumns(); columnEnum.hasMoreElements();) {
			TableColumn varColumn = columnEnum.nextElement();
			if (varColumn.getModelIndex() != FixedRowIndicatorTableModel.ROWMARKERCOLUMN_INDEX) {
				final int iModelColumn = varColumn.getModelIndex() - FixedRowIndicatorTableModel.ROWMARKERCOLUMN_COUNT;
				final CollectableEntityField clctefTarget = getExternalModel().getCollectableEntityField(iModelColumn);
				resultList.add(clctefTarget);
			}
		}
		return resultList;
	}

	private Map<Object, Integer> getVisibleColumnWidth() {
		final SubFormTable external = getExternalTable();
		final Map<Object, Integer> mpWidths = new HashMap<Object, Integer>(20);

		// remember the widths of the currently visible columns
		if (external != null) {
			TableColumnModel externalColumnModel = external.getColumnModel();
			for (Enumeration<TableColumn> columnEnum = externalColumnModel.getColumns(); columnEnum.hasMoreElements();) {
				TableColumn varColumn = columnEnum.nextElement();
				mpWidths.put(varColumn.getIdentifier(), new Integer(varColumn.getWidth()));
			}
			TableColumnModel headerColumnModel = getHeaderTable().getColumnModel();
			for (Enumeration<TableColumn> columnEnum = headerColumnModel.getColumns(); columnEnum.hasMoreElements();) {
				TableColumn varColumn = columnEnum.nextElement();
				if (varColumn.getIdentifier() != null) {
					mpWidths.put(varColumn.getIdentifier(), new Integer(varColumn.getWidth()));
				}
			}
		}
		return mpWidths;
	}

	private void restoreColumnWidthsInExternalTable(final Map<Object,Integer> mpWidths) {
		// restore the widths of the still present columns
		final SubFormTable external = getExternalTable();
		if (external != null) {
			TableColumnModel externalColumnModel = getExternalTable().getColumnModel();
			for (Enumeration<TableColumn> columnEnum = externalColumnModel.getColumns(); columnEnum.hasMoreElements();) {
				TableColumn varColumn = columnEnum.nextElement();
				Integer width = mpWidths.get(varColumn.getIdentifier());
				if (width != null) {
					varColumn.setPreferredWidth(width.intValue());
					varColumn.setWidth(width.intValue());
				}
			}
		}
	}

	private void restoreColumnWidthsInHeaderTable(final Map<Object,Integer> mpWidths) {
		TableColumnModel headerColumnModel = getHeaderTable().getColumnModel();
		for (Enumeration<TableColumn> columnEnum = headerColumnModel.getColumns(); columnEnum.hasMoreElements();) {
			TableColumn varColumn = columnEnum.nextElement();
			if (varColumn.getIdentifier() != null) {
				Integer width = mpWidths.get(varColumn.getIdentifier());
				if (width != null) {
					varColumn.setPreferredWidth(width.intValue());
					varColumn.setWidth(width.intValue());
				}
			}
		}
	}

	private void restoreColumnWidthsFromPrefs(final List<Integer> lstWidthsFromPref) {
		Iterator<Integer> widthIter = lstWidthsFromPref.iterator();
		
		TableColumnModel headerColumnModel = getHeaderTable().getColumnModel();
		for (Enumeration<TableColumn> columnEnum = headerColumnModel.getColumns(); columnEnum.hasMoreElements();) {
			TableColumn varColumn = columnEnum.nextElement();
			if (varColumn.getIdentifier() != null && !"".equals(varColumn.getIdentifier())) {
				if (widthIter.hasNext()) {
					Integer width = widthIter.next();
					varColumn.setPreferredWidth(width.intValue());
					varColumn.setWidth(width.intValue());
				}
			}
		}
		
		final SubFormTable external = getExternalTable();
		if (external != null) {
			TableColumnModel externalColumnModel = external.getColumnModel();
			for (Enumeration<TableColumn> columnEnum = externalColumnModel.getColumns(); columnEnum.hasMoreElements();) {
				TableColumn varColumn = columnEnum.nextElement();
				if (varColumn.getIdentifier() != null && !"".equals(varColumn.getIdentifier())) {
					if (widthIter.hasNext()) {
						Integer width = widthIter.next();
						varColumn.setPreferredWidth(width.intValue());
						varColumn.setWidth(width.intValue());
					}
				}
			} 
		}
	}

	/**
	 * §postcondition result != null
	 * 
	 * @return the <code>Comparator</code> used for <code>CollectableEntityField</code>s (columns in the Result).
	 * The default is to compare the column labels.
	 */
	private Comparator<? extends CollectableEntityField> getCollectableEntityFieldComparator() {
		return new Comparator<CollectableEntityField>() {
			@Override
			public int compare(CollectableEntityField o1, CollectableEntityField o2) {
				return o1.compareTo(o2);
				/*int result = StringUtils.compareIgnoreCase(
						SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(MetaProvider.getInstance().getEntity(o1.getEntityUID())),
								SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(MetaProvider.getInstance().getEntity(o2.getEntityUID())));
				if (result == 0)
					result = StringUtils.compareIgnoreCase(o1.getLabel(), o2.getLabel());
				return result;*/
			}			
		};
	}

	/**
	 * adds a mouse listener to the table header to trigger a table sort when a column heading is clicked in the JTable.
	 * @param tbl
	 * @param tblmodel
	 * @param runnableSort Runnable to execute for sorting the table. If <code>null</code>, <code>tblmodel.sort()</code> is performed.
	 */
	private void addMouseListenerForSortingToTableHeader(final JTable tbl, final SortableTableModel tblmodel, CommonRunnable runnableSort) {
		// clicking header does not mean column selection, but sorting:

		if (this.sortingListener == null) {
			tbl.setColumnSelectionAllowed(false);
			this.sortingListener = new TableHeaderMouseListenerForSorting(tbl, tblmodel, runnableSort) {
				@Override
				protected int convertColumnIndexToModel(int viewColumnIndex) {
					return super.convertColumnIndexToModel(viewColumnIndex) - FixedRowIndicatorTableModel.ROWMARKERCOLUMN_COUNT;
				}

				// misuse sort column to start select columns
				@Override
				protected void sortColumn(int iColumn, Component windowComponent) throws CommonBusinessException {
					if (iColumn == -1) {
						cmdSelectColumns(tblmodel);
					}
					else {
						super.sortColumn(iColumn, windowComponent);
						final SubFormTable external = getExternalTable();
						if (external != null) {
							external.repaint();
						}
					}
				}
			};
			tbl.getTableHeader().addMouseListener(sortingListener);
		}
		else {
			this.sortingListener.setTableModel(tblmodel);
		}
	}

	@Override
	protected void synchronizeModel() {

		getHeaderTable().getColumnModel().removeColumnModelListener(headerColumnModelListener);

		List<CollectableEntityField> selectedFields = getDisplayedExternalTableFields();
		// remember the widths of the currently visible columns
		final Map<Object,Integer> mpWidths = getVisibleColumnWidth();
		getScrlpnOriginalTable().getRowHeader().setBackground(getHeaderTable().getBackground());

		super.synchronizeModel();

		final SubFormTable external = getExternalTable();
		final HeaderTable header = (HeaderTable) getHeaderTable();
		if (external != null) {
			header.setTableCellEditorProvider(external.getTableCellEditorProvider());
			header.setTableCellRendereProvider(external.getTableCellRendererProvider());
			if (external.getModel() instanceof SortableTableModel) {
				addMouseListenerForSortingToTableHeader(getHeaderTable(), (SortableTableModel) external.getModel(), null);
			}
		}
		tableHeader.setExternalModel(getExternalModel());
		
		synchronizeColumnsInHeaderTable(lstFixedColumnCollNames);
		restoreColumnWidthsInHeaderTable(this.mpFixedColumnCollWidths);
		// remove row header columns from external columns
		for (Iterator<CollectableEntityField> fieldIter = selectedFields.iterator(); fieldIter.hasNext();) {
			CollectableEntityField curField = fieldIter.next();
			if (lstFixedColumnCollNames.contains(curField.getUID())) {
				fieldIter.remove();
			}
		}
		synchronizeColumnsInExternalTable(selectedFields);

		// set table header renderer
		if (this.getHeaderTable().getColumnModel().getColumnCount() > 0) {
			TableColumn column = this.getHeaderTable().getColumnModel().getColumn(0);
			column.setHeaderRenderer(new ColumnSelectionTableCellRenderer(getHeaderTable()));
		}

		invalidateHeaderTable();
		getHeaderTable().getColumnModel().addColumnModelListener(headerColumnModelListener);
		
		if (lstHeaderColumnWidthsFromPref != null) {
			restoreColumnWidthsFromPrefs(lstHeaderColumnWidthsFromPref);
			lstHeaderColumnWidthsFromPref = null;
		}
		else {
			restoreColumnWidthsInExternalTable(mpWidths);
			restoreColumnWidthsInHeaderTable(mpWidths);
		}
	}

	/**
	 * invalidate and repaint Header table, for example after the size of a column has changed
	 */
	public void invalidateHeaderTable() {
		final SubFormTable external = getExternalTable();
		// NUCLOS-6288, just don't remove all colums as the header disappars, too and columns can't be selected anymore
		if (external.getColumnCount() == 0) {
			external.addColumn(new TableColumn());
		}
		getScrlpnOriginalTable().getRowHeader().setPreferredSize(getHeaderTable().getPreferredSize());
		if (subform.isDynamicRowHeights() && external != null) {
			for (int i = 0; i < external.getRowCount(); i++) {
				if (i < getHeaderTable().getRowCount()) {
					getHeaderTable().setRowHeight(i, Math.max(subform.getMinRowHeight(), external.getRowHeight(i)));
				}
			}
		} else {
			if (external != null) {
				getHeaderTable().setRowHeight(external.getRowHeight());
			}
		}
		getHeaderTable().revalidate();
		getHeaderTable().invalidate();
		getHeaderTable().repaint();

		getScrlpnOriginalTable().revalidate();
		getScrlpnOriginalTable().invalidate();
		getScrlpnOriginalTable().repaint();

		subform.getSubFormFilter().arrangeFilterComponents();
	}

	/**
	 * removed/add the columns from the external table, according to the given list
	 * @param lstSelection	List of CollectableEntityField
	 *
	 */
	private void synchronizeColumnsInExternalTable(List<CollectableEntityField> lstSelection) {

		if (getExternalTable().isEditing()) {	// don't do it in editing, because last input is lost
			return;							    // because current editor is removed by externalColumnModel.removeColumn(..)
		}
		
		TableColumnModel externalColumnModel = getExternalTable().getColumnModel();
		Set<TableColumn> columnsToRemove = new HashSet<TableColumn>();
		// remove columns if not selected any more
		for (Enumeration<TableColumn> columnEnum = externalColumnModel.getColumns(); columnEnum.hasMoreElements();) {
			
			TableColumn varColumn = columnEnum.nextElement();
			boolean remove = true;
			// @todo is this needed? @see NUCLOS-1169
			/*for (CollectableEntityField clctef : lstSelection) {
				if (LangUtils.equals(clctef.getLabel(), varColumn.getIdentifier())) {
					remove = false;
				}
			}*/
			if (remove) {
				columnsToRemove.add(varColumn);
			}
		}
		for (Iterator<TableColumn> colIter = columnsToRemove.iterator(); colIter.hasNext();) {
			externalColumnModel.removeColumn(colIter.next());
		}

		// add inserted columns
		for (Iterator<CollectableEntityField> fieldIter = lstSelection.iterator(); fieldIter.hasNext();) {

			try {
				boolean doInsert = true;
				CollectableEntityField curField = fieldIter.next();

				// @todo is this needed? @see NUCLOS-1169
				/*for (Enumeration<TableColumn> columnEnum = externalColumnModel.getColumns(); doInsert && columnEnum.hasMoreElements();) {
	
					TableColumn varColumn = columnEnum.nextElement();
					if (curField.getLabel().equals(varColumn.getIdentifier())) {
						doInsert = false;
					}
				}*/
	
				if (doInsert) {
					int index = ((SubFormTableModel) getExternalTable().getModel()).findColumnByFieldUid(curField.getUID());
					int preferredWidth = getExternalTable().getSubFormModel().getMinimumColumnWidth(index);
					
					TableColumn newColumn = new TableColumn(index, preferredWidth);
					newColumn.setIdentifier(curField.getUID());
					String sLabel = ((SubFormTableModel) getExternalTable().getModel()).getColumnName(index);
					newColumn.setHeaderValue(sLabel);
					newColumn.setIdentifier(curField.getUID());
					externalColumnModel.addColumn(newColumn);
				}
			}
			catch(Exception e) {
				// only add Column on non exception
				// column may be removed (dummy entities like menu setup in bo wizard...)
				//LOG.warn("synchronizeColumnsInExternalTable: " + e + " (column removed?)");
			}
		}
	}

	/**
	 * removed/add the columns from the fixed table, according to the given list
	 * @param lstFixedNew List of String
	 *
	 */
	private void synchronizeColumnsInHeaderTable(List<UID> lstFixedNew) {

		TableColumnModel fixedColumnModel = getHeaderTable().getColumnModel();
		Set<TableColumn> columnsToRemove = new HashSet<TableColumn>();
		// remove all columns
		for (Enumeration<TableColumn> columnEnum = fixedColumnModel.getColumns(); columnEnum.hasMoreElements();) {

			TableColumn varColumn = columnEnum.nextElement();
			if ((varColumn.getIdentifier() instanceof String && // NUCLOS-2772 in diesem Fall ist zusätzlich eine UID-Spalte vorhanden, die ignoriert werden muss
					StringUtils.nullIfEmpty((String) varColumn.getIdentifier()) != null) 
			    || (varColumn.getIdentifier() instanceof UID)) {
				columnsToRemove.add(varColumn);
			}
		}

		for (TableColumn colIter : columnsToRemove) {
			try {
				fixedColumnModel.removeColumn(colIter);
			} catch (ArrayIndexOutOfBoundsException aie) {
				LOG.warn(aie.getMessage(), aie);
			}
		}

		// add inserted columns
		for (Iterator<UID> fieldIter = lstFixedNew.iterator(); fieldIter.hasNext();) {
			try {
				UID curField = fieldIter.next();
				int index = ((SubFormTableModel) getExternalTable().getModel()).findColumnByFieldUid(curField);
				TableColumn newColumn = new TableColumn(index + FixedRowIndicatorTableModel.ROWMARKERCOLUMN_COUNT);
				newColumn.setIdentifier(curField);
				String sLabel = ((SubFormTableModel) getExternalTable().getModel()).getColumnName(index);
				newColumn.setHeaderValue(sLabel);
				newColumn.setHeaderRenderer(new ColumnSelectionTableCellRenderer(getHeaderTable()));
				fixedColumnModel.addColumn(newColumn);
			}
			catch(Exception e) {
				// only add Column on non exception
				// column may be removed
				LOG.warn("synchronizeColumnsInHeaderTable: " + e + " (column removed?)");
			}
		}
	}

	private List<UID> getColumnNames() {
		final List<UID> result = new ArrayList<UID>();
		final Enumeration<TableColumn> enumeration = getHeaderTable().getColumnModel().getColumns();
		while (enumeration.hasMoreElements()) {
			final TableColumn column = enumeration.nextElement();
			if (column.getIdentifier() != null) {
				CollectableEntityField clctbl = getExternalModel().getCollectableEntityField(column.getModelIndex() - FixedRowIndicatorTableModel.ROWMARKERCOLUMN_COUNT);
				result.add(clctbl.getUID());
			}
		}
		return result;
	}

	public void writeFieldToPreferences(TablePreferencesManager tblprefManager) throws PreferencesException {
		final List<Integer> lstWidths = new ArrayList<Integer>();
		final Enumeration<TableColumn> enumeration = getHeaderTable().getColumnModel().getColumns();
		while (enumeration.hasMoreElements()) {
			final TableColumn column = enumeration.nextElement();
			if (column.getIdentifier() != null && !"".equals(column.getIdentifier())) { // column for row selection
				lstWidths.add(column.getWidth());
			}
		}
		tblprefManager.addFixedColumns(lstFixedColumnCollNames, lstWidths);
	}

	public void initializeFieldsFromPreferences(TablePreferencesManager tblprefManager) {
		this.lstFixedColumnCollNames = tblprefManager.getFixedColumns();
		this.lstHeaderColumnWidthsFromPref = tblprefManager.getColumnWidths();
	}

	/**
	 * Header table model which provides editors and renderers for the CollectableEntityField
	 */
	public class HeaderTable extends CommonJTable {

		private TableCellEditorProvider cellEditorProvider;
		private TableCellRendererProvider cellRendererProvider;

		private SubFormTable externalTable;

		public HeaderTable() {
		}

		@Override
		public void setRowHeight(int rowHeight) {
			super.setRowHeight(rowHeight);
		}

		@Override
		public void setRowHeight(int row, int rowHeight) {
			super.setRowHeight(row, rowHeight);
		}

		private UID prevComponent = null;
		private UID getComponentBefore(UID identifier) {
			UID result = null;			
			for (Column column : subform.getColumns()) {
				if (column.getNextFocusField() != null && column.getNextFocusField().equals(identifier)) {
					result = column.getUID();
					if (prevComponent == null || prevComponent.equals(result))
						return result;
				}
			}
			return result;
		}
		
		private void setPrevComponent(UID prevComponent) {
			this.prevComponent = prevComponent;
			if (externalTable != null) { 
				externalTable.setPreviousComponent(prevComponent);
			}
		}
		public void setPreviousComponent(UID prevComponent) {
			this.prevComponent = prevComponent;
		}
		
		@Override
		public void changeSelection(int rowIndex, final int columnIndex, boolean toggle, boolean extend) {
			final AWTEvent event = EventQueue.getCurrentEvent();
			if (event instanceof KeyEvent) {
				final KeyEvent ke = (KeyEvent)event;
	            if((ke.isShiftDown() || ke.isControlDown()) && ke.VK_TAB == ke.getKeyCode()) {
	            	if (columnIndex  == 0)
	            		rowIndex = rowIndex - 1 == 0 ? 0 : rowIndex - 1;
	            }
			}
			changeSelection(rowIndex, columnIndex, toggle, extend, false);
		}

		public void changeSelection(final int rwIndex, final int clIndex, boolean toggle, boolean extend, boolean external) {
			AWTEvent event = EventQueue.getCurrentEvent();
			if(event instanceof KeyEvent) {
				((KeyEvent) event).consume();
			}
			int iSelRow = getSelectedRow();
			if(event instanceof KeyEvent || event instanceof InvocationEvent) {
				int colCount = getColumnCount();
				int selColumn = getSelectedColumn();
				UID colNextFocus = null;
				if (getColumnModel().getColumn(selColumn).getIdentifier() instanceof UID) {
					colNextFocus = (UID) getColumnModel().getColumn(selColumn).getIdentifier();
				} else {
					colNextFocus = new UID( getColumnModel().getColumn(selColumn).getIdentifier().toString());
				}
				
				UID sNextColumn = selColumn == -1 || external ? null : subform.getColumnNextFocusField(colNextFocus);
				final int rowIndex = rwIndex;
				int colIndex;
				try {
					if (event instanceof KeyEvent) {
						final KeyEvent ke = (KeyEvent)event;
			            if((ke.isShiftDown() || ke.isControlDown())) {
			            	Object identifier = getColumnModel().getColumn(selColumn).getIdentifier();
			            	if (identifier instanceof UID) {
			            		sNextColumn = getComponentBefore((UID)getColumnModel().getColumn(selColumn).getIdentifier());
			            	} else {
			            		return;
			            	}
			            }
					}
					
					setPrevComponent(null);
					colIndex = sNextColumn == null || external ? clIndex : getColumnModel().getColumnIndex(sNextColumn);
					if (sNextColumn != null) {
						UID colIdentifier = (UID)getColumnModel().getColumn(selColumn).getIdentifier();
						if (event instanceof KeyEvent) {
							final KeyEvent ke = (KeyEvent)event;
				            if(ke.isShiftDown() || ke.isControlDown()) {
				            	;//sNextColumn = null;
				            } else
				            	setPrevComponent(colIdentifier);
						} else
			            	setPrevComponent(colIdentifier);
					}
				} catch (IllegalArgumentException e) {
					if (externalTable != null) { 
						super.changeSelection(rwIndex, clIndex, toggle, extend);
						colIndex = externalTable.getColumnModel().getColumnIndex(sNextColumn);
						if (event instanceof KeyEvent) {
							final KeyEvent ke = (KeyEvent)event;
							if(ke.isShiftDown() || ke.isControlDown()) {
								iSelRow = iSelRow - 1 == -1 ? 0 : iSelRow - 1;
							}
						}
						externalTable.changeSelection(iSelRow, colIndex, toggle, extend, true);
						return;
					} else
						colIndex = clIndex;
				}
					
				final int columnIndex = colIndex;
				super.changeSelection(rwIndex, clIndex, toggle, extend);

				if(!external && (columnIndex == 0 || columnIndex == colCount)) {
					if (externalTable != null) { 
						if (event instanceof KeyEvent) {
							final KeyEvent ke = (KeyEvent)event;
							if (ke.VK_TAB == ke.getKeyCode() && (!ke.isShiftDown() && !ke.isControlDown())) {
								externalTable.changeSelection(iSelRow, 0, toggle, extend, true);
							} else if (ke.VK_TAB == ke.getKeyCode() && (ke.isShiftDown() || ke.isControlDown())) {
								if (rowIndex != -1)
									externalTable.changeSelection(rowIndex, externalTable.getColumnCount() - 1, toggle, extend, true);
								else
									externalTable.changeSelection(externalTable.getRowCount() - 1, externalTable.getColumnCount() - 1, toggle, extend, true);
							} else {
								externalTable.changeSelection(rowIndex != -1 ? rowIndex : 0, 0, toggle, extend, true);
							}
						} else
							externalTable.changeSelection(iSelRow, 0, toggle, extend, true);
						return;
					}
				}
				
				if(isCellEditable(rowIndex, columnIndex)) {
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							if (editCellAt(rowIndex, columnIndex)) {
								Component editor = getEditorComponent();
								if(editor != null)
									editor.requestFocusInWindow();
							}
						}
					});
				} else {
					boolean bShift = false;
					if (event instanceof KeyEvent) {
						final KeyEvent ke = (KeyEvent)event;
						if(ke.isShiftDown() || ke.isControlDown()) {
							bShift = true;
						}
					}
					final int rowCol[] = getNextEditableCell(this, rowIndex, columnIndex, bShift);
					if(!external && (rowCol[1] == 0 || rowCol[1] == colCount || rowIndex != rowCol[0])) {
						if (externalTable != null) { 
							externalTable.changeSelection(rowCol[0], !bShift ? 0 : externalTable.getColumnCount() - 1 , toggle, extend, true);
							return;
						}
					}
					if (isCellEditable(rowCol[0], rowCol[1])) {
						if (editCellAt(rowCol[0], rowCol[1])) {
							Component editor = getEditorComponent();
							if(editor != null) {
								editor.requestFocusInWindow();
								if(rowCol[0] < getRowCount())
									changeSelection(rowCol[0], rowCol[1], false, false);
							}
						}
					} else {
						if((rowCol[1] == 0 || rowCol[1] == colCount - 1)) {
							if (externalTable != null) { 
								externalTable.changeSelection(iSelRow, 0, toggle, extend, true);
								return;
							}
						} else if((rowCol[1] == 1)) {
							if (externalTable != null) { 
								externalTable.changeSelection(iSelRow == 0 ? getRowCount() - 1 : iSelRow - 1, externalTable.getColumnCount() - 1, toggle, extend, true);
								return;
							}
						}
					}
				}
			}
			else
				super.changeSelection(rwIndex, clIndex, toggle, extend);
		}
		
		private int[] getNextEditableCell(JTable table, int row, int col, boolean bReverse) {
			int rowCol[] = {row,col};
			int colCount = getColumnCount();
			boolean colFound = false;
			if (!bReverse) {
				for(int i = col; i < colCount; i++) {
					if(table.isCellEditable(row, i)) {
						colFound = true;
						rowCol[1] = i;
						break;
					}
				}
				if(!colFound) {
					row++;
					if(row >= getRowCount())
						return rowCol;
					for(int i = 0; i < col; i++) {
						if(table.isCellEditable(row, i)) {
							rowCol[0] = row;
							rowCol[1] = i;
							break;
						}
					}
				}
			} else {
				for(int i = col; i >= 0; i--) {
					if(table.isCellEditable(row, i)) {
						colFound = true;
						rowCol[1] = i;
						break;
					}
				}
				if(!colFound) {
					row--;
					if(row <= 0)
						return rowCol;
					for(int i = colCount - 1; i > 0; i--) {
						if(table.isCellEditable(row, i)) {
							rowCol[0] = row;
							rowCol[1] = i;
							break;
						}
					}
				}
			}

			return rowCol;
		}

		@Override
		public TableCellRenderer getCellRenderer(int iRow, int iColumn) {

			TableCellRenderer result = null;

			if (cellRendererProvider != null && getModel() instanceof FixedRowIndicatorTableModel) {
				final int iModelColumn = getColumnModel().getColumn(iColumn).getModelIndex();
				if (iModelColumn != FixedRowIndicatorTableModel.ROWMARKERCOLUMN_INDEX) {
					FixedRowIndicatorTableModel fixedModel = (FixedRowIndicatorTableModel) getModel();
					final CollectableEntityField clctefTarget = ((SubFormTableModel) fixedModel.getExternalModel()).getCollectableEntityField(iModelColumn - FixedRowIndicatorTableModel.ROWMARKERCOLUMN_COUNT);
					result = cellRendererProvider.getTableCellRenderer(clctefTarget);
				}
			}
			if (result == null) {
				result = super.getCellRenderer(iRow, iColumn);
			}
			return result;
		}

		public TableCellEditorProvider getTableCellEditorProvider() {
			return this.cellEditorProvider;
		}

		public void setTableCellEditorProvider(TableCellEditorProvider aCellEditorProvider) {
			this.cellEditorProvider = aCellEditorProvider;
		}

		public void setTableCellRendereProvider(TableCellRendererProvider aCellRendererProvider) {
			this.cellRendererProvider = aCellRendererProvider;
		}

		@Override
		public TableCellEditor getCellEditor(int iRow, int iColumn) {
			TableCellEditor result = null;
		
			if (cellEditorProvider != null && getModel() instanceof FixedRowIndicatorTableModel) {
				final int iModelColumn = getColumnModel().getColumn(iColumn).getModelIndex();
				if (iModelColumn != FixedRowIndicatorTableModel.ROWMARKERCOLUMN_INDEX) {
					FixedRowIndicatorTableModel headerModel = (FixedRowIndicatorTableModel) getModel();
					final CollectableEntityField clctefTarget = ((SubFormTableModel) headerModel.externalModel).getCollectableEntityField(iModelColumn - FixedRowIndicatorTableModel.ROWMARKERCOLUMN_COUNT);
					result = cellEditorProvider.getTableCellEditor(this, iRow, clctefTarget);
				}
			}
			if (result == null) {
				result = super.getCellEditor(iRow, iColumn);
			}
			return result;
		}

		public void setExternalTable(SubFormTable aExternalTable) {
			this.externalTable = aExternalTable;
		}
		
		public SubFormTable getExternalTable() {
			return this.externalTable;
		}

		@Override
		public void setEditingRow(int aRow) {
			super.setEditingRow(aRow);
			if (this.externalTable != null) {
				this.externalTable.setEditingRow(aRow);
			}
		}

		/**
		 * set the row height in one row (used by the ElisaCollectableTextArea)
		 * @param iRow
		 * @param iRowHeight
		 */
		public void setRowHeightFromTextArea(int iRow, int iRowHeight) {
			if (iRowHeight != this.getRowHeight(iRow)) {
				this.setRowHeight(iRow, iRowHeight);
			}

			if (this.externalTable != null) {
				if (iRowHeight != this.externalTable.getRowHeight(iRow)) {
					this.externalTable.setRowHeight(iRow, iRowHeight);
				}
			}
		}

	}	// inner class Table

	/**
	 * table model which provides a row indicator column and all columns of the wrapped external model
	 * @author <a href="mailto:rainer.schneider@novabit.de">rainer.schneider</a>
	 */
	public static class FixedRowIndicatorTableModel extends SubformRowHeader.RowIndicatorTableModel {

		private static final int ROWMARKERCOLUMN_INDEX = 0;
		public static final int ROWMARKERCOLUMN_COUNT = 1;
		private TableModel externalModel = new DefaultTableModel(0, 0);

		@Override
		public void setExternalDataModel(TableModel aExternalModel) {
			externalModel = (externalModel != null) ? aExternalModel : new DefaultTableModel(0, 0);
			fireTableStructureChanged();
		}

		@Override
		public Object getValueAt(int row, int column) {
			return (column == ROWMARKERCOLUMN_INDEX) ? null : externalModel.getValueAt(row, column - ROWMARKERCOLUMN_COUNT);
		}

		@Override
		public void setValueAt(Object aValue, int row, int column) {
			if (column != ROWMARKERCOLUMN_INDEX) {
				externalModel.setValueAt(aValue, row, column - ROWMARKERCOLUMN_COUNT);
			}
		}

		@Override
		public int getRowCount() {
			return externalModel.getRowCount();
		}

		@Override
		public int getColumnCount() {
			return externalModel.getColumnCount() + ROWMARKERCOLUMN_COUNT;
		}

		@Override
		public boolean isCellEditable(int row, int column) {
			if (column == 0)
				return false;
			return (column != ROWMARKERCOLUMN_INDEX) && externalModel.isCellEditable(row, column - ROWMARKERCOLUMN_COUNT);
		}

		@Override
		public String getColumnName(int column) {
			return (column == ROWMARKERCOLUMN_INDEX) ? "" : externalModel.getColumnName(column - ROWMARKERCOLUMN_COUNT);
		}

		TableModel getExternalModel() {
			return externalModel;
		}
	}

	/**
	 * displays the column select icon in the first column of the table
	 */
	public class ColumnSelectionTableCellRenderer implements TableCellRenderer {
		private JTable rendererTable;

		public ColumnSelectionTableCellRenderer(JTable headerTable) {
			this.rendererTable = headerTable;
		}

		@Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

			Component renderer = rendererTable.getTableHeader().getDefaultRenderer()
					.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			if (renderer instanceof JLabel) {
				if (column == 0) {
					if (SecurityCache.getInstance().isActionAllowed(Actions.ACTION_CUSTOMIZE_ENTITY_AND_SUBFORM_COLUMNS) ||
							!WorkspaceUtils.getInstance().getWorkspace().isAssigned()) {
						((JLabel) renderer).setIcon(MainFrame.resizeAndCacheIcon(Icons.getInstance().getIconSelectVisibleColumns16(), 12));
						((JLabel) renderer).setBorder(BorderFactory.createEmptyBorder(2, 2, 1, 1));
					}
				}
			}
			
			if (rendererTable.getColumnCount() <= 1) {
				renderer.setBackground(FIXED_HEADER_BACKGROUND);
			} else {
				renderer.setBackground(FIXED_HEADER_BACKGROUND);
			}
			
			return renderer;
		}

	}
	
	private static class FixedRowToolTipsTableHeader extends ToolTipsTableHeader {

		public FixedRowToolTipsTableHeader(CollectableEntityFieldBasedTableModel aTableModel, TableColumnModel cm) {
			super(aTableModel, cm);
		}
		
		@Override
		public TableCellRenderer getDefaultRenderer() {
			TableCellRenderer tcr = new TableCellRenderer() {
				@Override
                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
						boolean hasFocus, int row, int column) {
					
					Component comp = FixedRowToolTipsTableHeader.super.getDefaultRenderer().getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
					//comp.setBackground(Color.lightGray);
					
					return comp;
				}
			};
			
			return tcr;
		};

		@Override
		protected int adjustColumnIndex(int iColumn) {
			return iColumn - FixedRowIndicatorTableModel.ROWMARKERCOLUMN_COUNT;
		}
	}
	
	/**
	 * A RowSorter which listens to a base row sorter and propagates all sorting changes.
	 * The sort keys are delegated directly to the base row sorter, i.e. 
	 */
	public static class FixedRowIndicicatorRowSorter<M extends FixedRowIndicatorTableModel> extends RowSorter<M> {
		
		private final RowSorterListener externalRowSorterListener;
		private final M tableModel;
		private final RowSorter<?> externalRowSorter;
		
		public FixedRowIndicicatorRowSorter(M tableModel, RowSorter<?> externalRowSorter) {
			this.externalRowSorterListener = new RowSorterListener() {
				@Override
				public void sorterChanged(RowSorterEvent e) {
					externalSorterChanged(e);
				}
			};
			this.tableModel = tableModel;
			this.externalRowSorter = externalRowSorter;
			this.externalRowSorter.addRowSorterListener(externalRowSorterListener);
		}
		
		@Override
		public M getModel() {
			return tableModel;
		}

		@Override
		public int convertRowIndexToModel(int index) {
			return externalRowSorter.convertRowIndexToModel(index);
		}

		@Override
		public int convertRowIndexToView(int index) {
			return externalRowSorter.convertRowIndexToView(index);
		}

		@Override
		public int getViewRowCount() {
			return externalRowSorter.getViewRowCount();
		}

		@Override
		public int getModelRowCount() {
			return externalRowSorter.getModelRowCount();
		}
		
		protected void externalSorterChanged(RowSorterEvent e) {
			switch (e.getType()) {
			case SORT_ORDER_CHANGED:
				fireSortOrderChanged();
				break;
			case SORTED:
				// TODO: provide access to the old indices
				fireRowSorterChanged(null);
				break;
			default:
				throw new IllegalArgumentException("Invalid row sorter event type " + e.getType());
			}
		}
		
		@Override
		public void setSortKeys(List<? extends javax.swing.RowSorter.SortKey> keys) {
			List<SortKey> externalKeys = new ArrayList<SortKey>();
			for (SortKey key : keys) {
				if (key.getColumn() > FixedRowIndicatorTableModel.ROWMARKERCOLUMN_COUNT)
					externalKeys.add(shiftSortKey(key, - FixedRowIndicatorTableModel.ROWMARKERCOLUMN_COUNT));
			}
			externalRowSorter.setSortKeys(externalKeys);
		}

		@Override
		public List<? extends javax.swing.RowSorter.SortKey> getSortKeys() {
			List<SortKey> keys = new ArrayList<SortKey>();
			List<? extends SortKey> externalKeys = externalRowSorter.getSortKeys();
			for (SortKey externalKey : externalKeys) {
				keys.add(shiftSortKey(externalKey, + FixedRowIndicatorTableModel.ROWMARKERCOLUMN_COUNT));
			}
			return keys;
		}

		@Override
		public void toggleSortOrder(int column) {
			if (column > FixedRowIndicatorTableModel.ROWMARKERCOLUMN_COUNT)
				externalRowSorter.toggleSortOrder(column - FixedRowIndicatorTableModel.ROWMARKERCOLUMN_COUNT);
		}

		@Override
		public void modelStructureChanged() {
		}

		@Override
		public void allRowsChanged() {
		}

		@Override
		public void rowsInserted(int firstRow, int endRow) {
		}

		@Override
		public void rowsDeleted(int firstRow, int endRow) {
		}

		@Override
		public void rowsUpdated(int firstRow, int endRow) {
		}

		@Override
		public void rowsUpdated(int firstRow, int endRow, int column) {
		}
		
		private static SortKey shiftSortKey(SortKey key, int delta) {
			return new SortKey(key.getColumn() + delta, key.getSortOrder());
		}
	}
	
}
