package org.nuclos.client.ui.collect.subform;

import java.util.Collection;

import org.nuclos.client.ui.collect.component.CollectableListOfValues;
import org.nuclos.client.ui.collect.component.LookupEvent;
import org.nuclos.client.ui.collect.component.LookupListener;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModel;
import org.nuclos.client.ui.collect.component.model.SearchComponentModel;

/**
 * Created by Oliver Brausch on 17.07.17.
 */
class LookupValuesListener<PK2> implements LookupListener<PK2> {

	private final SubFormTableModel subformtblmdl;

	private final boolean bSearchable;

	private final SubFormTable subformtbl;

	private final Collection<TransferLookedUpValueAction> collTransferValueActions;

	LookupValuesListener(SubFormTableModel subformtblmdl, boolean bSearchable,
						 SubFormTable subformtbl, Collection<TransferLookedUpValueAction> collTransferValueActions) {

		this.subformtblmdl = subformtblmdl;
		this.bSearchable = bSearchable;
		this.subformtbl = subformtbl;
		this.collTransferValueActions = collTransferValueActions;
	}

	@Override
	public void lookupSuccessful(LookupEvent<PK2> ev) {
		if (ev.getSource() instanceof CollectableListOfValues) {
			final int iTargetColumn = subformtblmdl.findColumnByFieldUid(((CollectableListOfValues) ev.getSource()).getFieldUID());
			CollectableComponentModel model = ((CollectableListOfValues) ev.getSource()).getModel();
			if (model instanceof SearchComponentModel)
				subformtblmdl.setValueAt(((SearchComponentModel) model).getSearchCondition(), subformtbl.getSelectedRow(), iTargetColumn);
			else
				subformtblmdl.setValueAt(model.getField(), subformtbl.getSelectedRow(), iTargetColumn);
		}
		SubForm.transferLookedUpValues(ev.getSelectedCollectable(), subformtbl, bSearchable,
				subformtbl.getEditingRow(), collTransferValueActions, true);
	}

	@Override
	public int getPriority() {
		return 1;
	}
}
