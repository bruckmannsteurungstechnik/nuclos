//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.subform;

import java.util.Map;

import javax.swing.JTable;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.result.ResultFilterPanel;
import org.nuclos.common.UID;

/**
 * A collapsible panel which consists of filter components (CollectableComponents)
 * which are used to filter SubForm table data. This panel is responsible for arranging
 * the filter components so they appear directly above the corresponding column. 
 *
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:martin.weber@novabit.de">Martin Weber</a>
 * @version 01.00.00
 */
public class SubFormFilterPanel extends ResultFilterPanel {
	
	private static final Logger LOG = Logger.getLogger(SubFormFilterPanel.class);

	//
	
	public SubFormFilterPanel(JTable table, TableColumnModel columnModel, Map<UID, CollectableComponent> column2component, Boolean fixedTable) {
		super(table, columnModel, column2component, fixedTable);
	}

}
