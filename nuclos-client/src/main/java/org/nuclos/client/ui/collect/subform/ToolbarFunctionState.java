package org.nuclos.client.ui.collect.subform;

import javax.swing.AbstractButton;

/**
 * Created by Oliver Brausch on 17.07.17.
 */
public enum ToolbarFunctionState {
	ACTIVE(true, true),
	DISABLED(false, true),
	HIDDEN(false, false);

	private boolean isEnabled;
	private boolean isVisible;

	private ToolbarFunctionState(boolean isEnabled, boolean isVisible) {
		this.isEnabled = isEnabled;
		this.isVisible = isVisible;
	}

	public void set(AbstractButton a) {
		a.setEnabled(isEnabled);
		a.setVisible(isVisible);
	}
}
