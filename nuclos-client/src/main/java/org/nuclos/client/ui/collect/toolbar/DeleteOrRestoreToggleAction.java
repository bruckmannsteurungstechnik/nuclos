package org.nuclos.client.ui.collect.toolbar;

import org.nuclos.client.ui.CommonAbstractAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Action which handles the two different states of the delete button
 * 
 * - DELETE
 * - RESTORE
 * 
 * @author Moritz Neuhaeuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public abstract class DeleteOrRestoreToggleAction extends CommonAbstractAction {
	private final static Logger LOG = LoggerFactory.getLogger(DeleteOrRestoreToggleAction.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static enum Mode {
		DELETE,
		RESTORE
	}

	private Mode mode;
	
	public DeleteOrRestoreToggleAction() {
		setMode(Mode.DELETE);
	}
	
	/**
	 * set toggle mode
	 * 
	 * @param mode
	 */
	public void setMode(final Mode newMode) {
		LOG.trace("toggle mode {} => {}", this.mode, newMode);
		this.mode = newMode;
	}
	
	/**
	 * get toggle mode
	 * 
	 * @return
	 */
	public Mode getMode() {
		return mode;
	}


}
