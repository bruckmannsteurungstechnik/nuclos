//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.toolbar;

import org.nuclos.client.ui.Icons;
import org.nuclos.common.collect.NuclosToolBarItems;
import org.nuclos.common.collect.ToolBarItem;

public class NuclosToolBarActions {

	public static final ToolBarItemAction DELETE = new ToolBarItemAction(NuclosToolBarItems.DELETE, "event.Delete", "CollectController.15", Icons.getInstance().getIconDelete16());
	public static final INuclosToolBarItem DELETE_REAL = new ToolBarItemAction(NuclosToolBarItems.DELETE_REAL, "event.DeleteReal", "GenericObjectCollectController.60", Icons.getInstance().getIconRealDelete16());
	public static final INuclosToolBarItem RESTORE = new ToolBarItemAction(NuclosToolBarItems.RESTORE, "GenericObjectCollectController.99", "GenericObjectCollectController.38", Icons.getInstance().getIconDeleteRestore16());
	public static final INuclosToolBarItem MULTI_DELETE = new ToolBarItemAction(NuclosToolBarItems.MULTI_DELETE, "event.Delete", "ResultController.5", Icons.getInstance().getIconDelete16());
	public static final INuclosToolBarItem MULTI_DELETE_REAL = new ToolBarItemAction(NuclosToolBarItems.MULTI_DELETE_REAL, "event.DeleteReal", "GenericObjectCollectController.11", Icons.getInstance().getIconRealDelete16());
	public static final INuclosToolBarItem MULTI_RESTORE = new ToolBarItemAction(NuclosToolBarItems.MULTI_RESTORE, "GenericObjectCollectController.99", "GenericObjectCollectController.13", Icons.getInstance().getIconDeleteRestore16());
	public static final INuclosToolBarItem MULTI_EDIT = new ToolBarItemAction(NuclosToolBarItems.MULTI_EDIT, "ResultPanel.4", "ResultPanel.2", Icons.getInstance().getIconEdit16());
	public static final INuclosToolBarItem MULTI_SHOW_IN_EXPLORER = new ToolBarItemAction(NuclosToolBarItems.MULTI_SHOW_IN_EXPLORER, "GenericObjectCollectController.50", "GenericObjectCollectController.43", Icons.getInstance().getIconTree16());
	public static final ToolBarItemAction QUICKSELECTION = new ToolBarItemAction(NuclosToolBarItems.QUICKSELECTION, "quickselection", "SubForm.6", Icons.getInstance().getIconMultiEdit16());
	public static final ToolBarItemAction CLONE = new ToolBarItemAction(NuclosToolBarItems.CLONE, "CollectController.38", "CollectController.7", Icons.getInstance().getIconClone16());
	public static final INuclosToolBarItem SAVE = new ToolBarItemAction(NuclosToolBarItems.SAVE, "CollectController.40", "CollectController.6", Icons.getInstance().getIconSave16());
	public static final INuclosToolBarItem REFRESH = new ToolBarItemAction(NuclosToolBarItems.REFRESH, "CollectController.37", "CollectController.4", Icons.getInstance().getIconRefresh16());
	public static final INuclosToolBarItem REFRESH_LIST = new ToolBarItemAction(NuclosToolBarItems.REFRESH_LIST, "CollectController.37", "ResultPanel.9", Icons.getInstance().getIconRefresh16());
	public static final ToolBarItemAction NEW = new ToolBarItemAction(NuclosToolBarItems.NEW, "CollectController.39", "CollectController.23", Icons.getInstance().getIconNew16());
	public static final INuclosToolBarItem INFO = new ToolBarItemAction(NuclosToolBarItems.INFO, "", Icons.getInstance().getIconAbout16());
	public static final INuclosToolBarItem BOOKMARK = new ToolBarItemAction(NuclosToolBarItems.BOOKMARK, "CollectController.105", "CollectController.106", Icons.getInstance().getIconBookmark16());
	public static final INuclosToolBarItem HELP = new ToolBarItemAction(NuclosToolBarItems.HELP, "CollectController.110", "CollectController.110", Icons.getInstance().getIconHelp());
	public static final INuclosToolBarItem OPEN_IN_NEW_TAB = new ToolBarItemAction(NuclosToolBarItems.OPEN_IN_NEW_TAB, "CollectController.107", "CollectController.108", Icons.getInstance().getIconOpenInNewTab16());
	public static final INuclosToolBarItem SHOW_IN_EXPLORER = new ToolBarItemAction(NuclosToolBarItems.SHOW_IN_EXPLORER, "GenericObjectCollectController.50", Icons.getInstance().getIconMakeTreeRoot16());
	// public static final NuclosToolBarMenuAction XML_EXPORT_GO = new NuclosToolBarMenuAction("CollectController.xmlexport", Icons.getInstance().getIconMakeTreeRoot16());
	// public static final NuclosToolBarMenuAction XML_EXPORT_MD = new NuclosToolBarMenuAction("CollectController.xmlexport", Icons.getInstance().getIconMakeTreeRoot16());
	public static final INuclosToolBarItem EXECUTE_RULE = new ToolBarItemAction(NuclosToolBarItems.EXECUTE_RULE, "GenericObjectCollectController.62", Icons.getInstance().getIconExecuteRule16());
	public static final INuclosToolBarItem SHOW_STATE_HISTORY = new ToolBarItemAction(NuclosToolBarItems.SHOW_STATE_HISTORY, "GenericObjectCollectController.84", Icons.getInstance().getIconStateHistory16());
	public static final INuclosToolBarItem SHOW_HISTORY = new ToolBarItemAction(NuclosToolBarItems.SHOW_HISTORY, "GenericObjectCollectController.108", Icons.getInstance().getIconHistory16());
	public static final INuclosToolBarItem PRINT_DETAILS = new ToolBarItemAction(NuclosToolBarItems.PRINT_DETAILS, "GenericObjectCollectController.48", Icons.getInstance().getIconPrintReport16());
	public static final INuclosToolBarItem CANCEL_EDIT = new ToolBarItemAction(NuclosToolBarItems.CANCEL_EDIT, "GenericObjectCollectController.6", "DetailsPanel.1", Icons.getInstance().getIconCancel16());
	public static final INuclosToolBarItem CONFIG_COLUMNS = new ToolBarItemAction(NuclosToolBarItems.CONFIG_COLUMNS, "ResultPanel.11", Icons.getInstance().getIconSelectVisibleColumns16());
	public static final INuclosToolBarItem RESET_FILTER = new ToolBarItemAction(NuclosToolBarItems.RESET_FILTER, "CollectController.43", Icons.getInstance().getIconFilterActive16());
	public static final INuclosToolBarItem SET_FILTER = new ToolBarItemAction(NuclosToolBarItems.SET_FILTER, "CollectController.Search.Filter", Icons.getInstance().getIconFilter16());
	public static final INuclosToolBarItem LOAD_FILTER = new ToolBarItemAction(NuclosToolBarItems.LOAD_FILTER, "CollectController.Search.Filter", Icons.getInstance().getIconFilter16());
	public static final INuclosToolBarItem CHANGE_STATE = new ToolBarItemAction(NuclosToolBarItems.CHANGE_STATE, "nuclos.entity.statetransition.label", "GenericObjectCollectController.8", Icons.getInstance().getIconStateChange16());
	public static final INuclosToolBarItem GENERATE = new ToolBarItemAction(NuclosToolBarItems.GENERATE, "nuclos.entity.generation.label", Icons.getInstance().getIconGenerate16());
	public static final INuclosToolBarItem SEARCH = new ToolBarItemAction(NuclosToolBarItems.SEARCH, "CollectController.30", Icons.getInstance().getIconFind16());
	public static final INuclosToolBarItem CLEAR_SEARCH_FORM = new ToolBarItemAction(NuclosToolBarItems.CLEAR_SEARCH_FORM, "CollectController.27", Icons.getInstance().getIconClearSearch16());
	public static final INuclosToolBarItem NEW_FROM_SEARCH = new ToolBarItemAction(NuclosToolBarItems.NEW_FROM_SEARCH, "CollectController.39", "CollectController.31", Icons.getInstance().getIconNewWithSearchValues16());
	public static final INuclosToolBarItem SEARCH_EDITOR = new ToolBarItemAction(NuclosToolBarItems.SEARCH_EDITOR, "CollectController.28", "CollectController.29", Icons.getInstance().getIconSearchEditor16());
	public static final INuclosToolBarItem SAVE_FILTER = new ToolBarItemAction(NuclosToolBarItems.SAVE_FILTER, "NuclosCollectController.9", "NuclosCollectController.5", Icons.getInstance().getIconFilterSave16());
	public static final INuclosToolBarItem DELETE_FILTER = new ToolBarItemAction(NuclosToolBarItems.DELETE_FILTER, "NuclosCollectController.8", "NuclosCollectController.2", Icons.getInstance().getIconFilterDelete16());
	public static final ToolBarItemAction TRANSFER = new ToolBarItemAction(NuclosToolBarItems.TRANSFER, "apply", "SubForm.ToolbarFunction.TRANSFER", Icons.getInstance().getIconCopy16());
	public static final ToolBarItemAction FILTER = new ToolBarItemAction(NuclosToolBarItems.FILTER, "CollectController.Search.Filter", "SubForm.5", Icons.getInstance().getIconFilter16());
	public static final ToolBarItemAction DOCUMENT_IMPORT = new ToolBarItemAction(NuclosToolBarItems.DOCUMENT_IMPORT, "SubForm.ToolbarFunction.DOCUMENTIMPORT", "SubForm.ToolbarFunction.DOCUMENTIMPORT", Icons.getInstance().getIconImport16());
	public static final ToolBarItemAction PRINT_REPORT = new ToolBarItemAction(NuclosToolBarItems.PRINT_REPORT, "SubForm.8", "SubForm.8", Icons.getInstance().getIconPrintReport16());
	public static final ToolBarItemAction AUTONUMBER_PUSH_UP = new ToolBarItemAction(NuclosToolBarItems.AUTONUMBER_PUSH_UP, "SubForm.9", Icons.getInstance().getIconSortAscending());
	public static final ToolBarItemAction AUTONUMBER_PUSH_DOWN = new ToolBarItemAction(NuclosToolBarItems.AUTONUMBER_PUSH_DOWN, "SubForm.10", Icons.getInstance().getIconSortDescending());
	public static final ToolBarItemAction SUBFORM_DETAILS_VIEW = new ToolBarItemAction(NuclosToolBarItems.SUBFORM_DETAILS_VIEW, "SubForm.16", "SubForm.16", Icons.getInstance().getSubformDetails());
	public static final ToolBarItemAction SUBFORM_DETAILS_ZOOM = new ToolBarItemAction(NuclosToolBarItems.SUBFORM_DETAILS_ZOOM, "SubForm.18", Icons.getInstance().getIconZoomIn());
	public static final INuclosToolBarItem RECORDNAV_FIRST = new ToolBarItemAction(NuclosToolBarItems.RECORDNAV_FIRST, "CollectController.33", Icons.getInstance().getIconFirst16());
	public static final INuclosToolBarItem RECORDNAV_PREVIOUS = new ToolBarItemAction(NuclosToolBarItems.RECORDNAV_PREVIOUS, "CollectController.36", Icons.getInstance().getIconPrevious16());
	public static final INuclosToolBarItem RECORDNAV_NEXT = new ToolBarItemAction(NuclosToolBarItems.RECORDNAV_NEXT, "CollectController.35", Icons.getInstance().getIconNext16());
	public static final INuclosToolBarItem RECORDNAV_LAST = new ToolBarItemAction(NuclosToolBarItems.RECORDNAV_LAST, "CollectController.34", Icons.getInstance().getIconLast16());
	public static final ToolBarItemAction SUBFORM_CHANGE_STATE = new ToolBarItemAction(NuclosToolBarItems.SUBFORM_CHANGE_STATE, "SubForm.17", null/*Icons.getInstance().getIconStateChange16()*/);
	public static final ToolBarItemAction LOCALIZATION = new ToolBarItemAction(NuclosToolBarItems.LOCALIZATION, "wizard.step.attributeproperties.78", Icons.getInstance().getDataLanguageIcon());
	public static final ToolBarItemAction LOCK = new ToolBarItemAction(NuclosToolBarItems.LOCK, "wizard.step.permission.locking.label", null);
	public static final ToolBarItemAction SEARCH_OWNER = new ToolBarItemAction(NuclosToolBarItems.SEARCH_OWNER, "nuclos_searchOwner", null);
	
	static {
		LOCK.setProperty(ToolBarItem.PROPERTY_SHOW_TEXT, Boolean.TRUE.toString());
	}
	
}
