package org.nuclos.client.ui.error;

import java.awt.Component;

import org.nuclos.common2.ILocalizable;

/**
 * An attempt to get exceptions displayed to the user more structured.
 * <p>
 * Normally implementation of this interface will be of type Throwable or
 * its sub types. However, this is NOT required.
 * </p>
 * @author Thomas Pasch
 * @since Nuclos 4.4.1
 */
public interface INuclosExceptionForDisplay extends ILocalizable {
	
	/**
	 * If true, this exception could happen user giving inconsistent input.
	 * If false, this is exception is due to programming error/bug. 
	 * <p>
	 * It is intended to get away from the Runtime/Exception difference to
	 * be also used for this purpose.
	 * </p>
	 */
	boolean isExpected();
	
	/**
	 * True if this error stems from the use of the Nuclos (rule) API.
	 */
	boolean isBusinesssException();
	
	/**
	 * Severity of the exception. Might be taken into account if this 
	 * is a expected exception. Never null.
	 */
	ESeverity getSeverity();
	
	/**
	 * Return the GUI component that is associated with the exception. 
	 * Maybe null.
	 */
	Component getAssociatedComponent();
	
	/**
	 * Expected way to display the exception to the user. Never null.
	 */
	EDisplayType getDisplayType();

}
