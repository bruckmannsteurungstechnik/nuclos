//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractButton;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

import org.apache.fop.fo.flow.TableHeader;

/**
 * A mouse listener suited for being registered to a {@link TableHeader}.
 * <p>
 * Pairs of ({@link AbstractButton}s, {@link ITablePopupMenuListener})
 * can subsequently be registered to a instance of this adapter, 
 * see {@link #register(AbstractButton, ITablePopupMenuListener)}.
 * <p>
 * The instance then adapts between the {@link ActionListener} of the button
 * and the (stored) {@link MouseEvent} to emit {@link TablePopupMenuEvent}s.
 * <p>
 * Part of the support abstraction for popup menus on {@link JTable}s needed
 * for http://project.nuclos.de/browse/LIN-165.
 * 
 * @author Thomas Pasch
 * @since Nuclos 4.3.0
 */
public class Mouse2TableHeaderPopupMenuAdapter implements ActionListener, MouseListener {

	private final JTable table;

	private final Map<AbstractButton, ITablePopupMenuListener> listenerMap = new HashMap<AbstractButton, ITablePopupMenuListener>();

	private MouseEvent mouseEvent;

	public Mouse2TableHeaderPopupMenuAdapter(JTable table) {
		if (table == null) {
			throw new NullPointerException();
		}
		this.table = table;
	}

	public void register(AbstractButton source, ITablePopupMenuListener listener) {
		ITablePopupMenuListener old = null;
		synchronized (listener) {
			old = listenerMap.put(source, listener);
		}
		if (old != null) {
			throw new IllegalStateException();
		}
		source.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (mouseEvent == null) {
			throw new IllegalStateException();
		}
		final int colIndex = table.columnAtPoint(mouseEvent.getPoint());
		final int rowIndex = table.rowAtPoint(mouseEvent.getPoint());
		final TablePopupMenuEvent event = new TablePopupMenuEvent(table, TablePopupMenuEvent.HEADER_EVENT, colIndex,
				rowIndex);
		final ITablePopupMenuListener listener;
		synchronized (listenerMap) {
			listener = listenerMap.get(e.getSource());
		}
		if (listener == null) {
			throw new IllegalStateException();
		}
		listener.tablePopupMenu(event);
		mouseEvent = null;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (SwingUtilities.isRightMouseButton(e)) {
			this.mouseEvent = e;
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

}
