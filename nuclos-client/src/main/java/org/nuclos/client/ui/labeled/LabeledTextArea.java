//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.labeled;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.ToolTipManager;
import javax.swing.event.DocumentListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.JTextComponent;

import org.apache.log4j.Logger;
import org.nuclos.client.ui.ColorProvider;
import org.nuclos.client.ui.CommonJScrollPane;
import org.nuclos.client.ui.DocumentFactory;
import org.nuclos.client.ui.LayoutNavigationCollectable;
import org.nuclos.client.ui.LayoutNavigationProcessor;
import org.nuclos.client.ui.ToolTipTextProvider;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.DefaultLayoutNavigationSupportContext;
import org.nuclos.client.ui.collect.LayoutNavigationSupport;
import org.nuclos.client.ui.collect.LayoutNavigationSupport.ExecutionPoint;
import org.nuclos.client.ui.collect.component.TextModuleSupport;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.textmodule.TextModuleSettings;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;

/**
 * <code>CollectableComponent</code> that presents a value in a <code>JTextArea</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version	01.00.00
 */

public class LabeledTextArea extends LabeledTextComponent implements TextModuleSupport {
	
	public static class InnerTextArea extends JTextArea implements LayoutNavigationProcessor {

		private final ILabeledComponentSupport support;
		
		private LayoutNavigationCollectable lnc;
		
		public InnerTextArea(ILabeledComponentSupport support) {
			if (support == null) {
				throw new NullPointerException();
			}
			this.support = support;
		}
		
		public ILabeledComponentSupport getLabeledComponentSupport() {
			return support;
		}

		@Override
		public String getToolTipText(MouseEvent ev) {
			String toolTip = super.getToolTipText(ev);
			if (toolTip != null) {
				return toolTip;
			}
			final ToolTipTextProvider provider = support.getToolTipTextProvider();
			return StringUtils.concatHtml(false, provider != null ? provider.getDynamicToolTipText() : super.getToolTipText(ev),
					support.getValidationToolTip());
		}
		
		@Override
		public Color getBackground() {
			// support could be null on construction (tp)
			final Color colorDefault = super.getBackground();
			if (support == null) {
				return colorDefault;
			}
			final ColorProvider colorproviderBackground = support.getColorProvider();
			if (colorproviderBackground == null) {
				return colorDefault;
			}
			return colorproviderBackground.getColor(colorDefault);
		}

//		NUCLOS-6800
//		@Override
//		public void paste() {
//			if (this.isEditable()) {
//				Clipboard clipboard = getToolkit().getSystemClipboard();
//				try {
//					// The MacOS MRJ doesn't convert \r to \n,
//					// so do it here
//					String selection = ((String) clipboard.getContents(this).getTransferData(DataFlavor.stringFlavor)).replace('\r', '\n');
//					if (selection.endsWith("\n")) {
//						selection = selection.substring(0, selection.length()-1);
//					}
//					//NUCLEUSINT-1139
//					replaceSelection(selection.trim()); // trim selection. @see NUCLOS-1112
//				}
//				catch (Exception e) {
//					getToolkit().beep();
//					LOG.warn("Clipboard does not contain a string: " + e, e);
//				}
//			}
//		}
		
		@Override
		protected boolean processKeyBinding(final KeyStroke ks, final KeyEvent e, final int condition, final boolean pressed) {
			boolean processed = false;
			if (null != lnc) {
				final LayoutNavigationSupport lns = lnc.getLayoutNavigationSupport();
				if (lns != null) {
					final DefaultLayoutNavigationSupportContext ctx = new DefaultLayoutNavigationSupportContext(pressed, ks, e, condition, this, lnc);
					processed = lns.processLayoutNavigationEvent(ctx, ExecutionPoint.BEFORE);
					if (!processed) {
						processed = super.processKeyBinding(ks, e, condition, pressed);
						//if (!processed) {
							ctx.setProcessed(processed);
							processed = lns.processLayoutNavigationEvent(ctx, ExecutionPoint.AFTER);
						//}
					}
				} else {
					processed = super.processKeyBinding(ks, e, condition, pressed);					
				}
			} else {
				processed = super.processKeyBinding(ks, e, condition, pressed);
			}
			return processed;
		}
		
		@Override
		public void setLayoutNavigationCollectable(LayoutNavigationCollectable lnc) {
			this.lnc = lnc;
		}
		
	}
	
	/**
	 * @deprecated This is the main reason for many swing initialization problems with {@link LabeledTextArea}. Avoid! (tp)
	 */
	public static class TextareaScrollPane extends CommonJScrollPane {

		public TextareaScrollPane(JTextArea ta, int verticalScrollbarAsNeeded, int horizontalScrollbarNever) {
			super(ta, verticalScrollbarAsNeeded, horizontalScrollbarNever);
		}

		@Override
		public boolean hasFocus() {
			final Component view = getView();
			if (view == null) {
				return super.hasFocus();
			}
			return view.hasFocus();
		}
		
		@Override
		public boolean requestFocusInWindow() {
			final Component view = getView();
			if (view == null) {
				return super.requestFocusInWindow();
			}
			return view.requestFocusInWindow();
		}
		
		@Override
		public Font getFont() {
			final Component view = getView();
			if (view == null) {
				return super.getFont();
			}
			return view.getFont();
		}
		
		@Override
		public void setFont(Font font) {
			final Component view = getView();
			if (view == null) {
				super.setFont(font);
			} else {
				view.setFont(font);
			}
		}
		
	}
	
	private static final Logger LOG = Logger.getLogger(LabeledTextArea.class);

	private final InnerTextArea ta;

	private final JScrollPane scrlpn;
	
	private TextModuleSettings tms;
	
	private LayoutNavigationCollectable lnc;
	
	public LabeledTextArea(InnerTextArea support) {
		this(support, true, String.class, null, false);
	}

	public LabeledTextArea(InnerTextArea inner, boolean isNullable, Class<?> javaClass, String inputFormat, boolean bSearchable) {
		super(inner.getLabeledComponentSupport(), isNullable, javaClass, inputFormat, bSearchable);
		scrlpn = new TextareaScrollPane(inner, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		this.ta = inner;
		
		initValidation(isNullable, javaClass, inputFormat);
		if(this.validationLayer != null){
			this.addControl(this.validationLayer);
		} else {
			this.addControl(this.scrlpn);
		}
		this.getJLabel().setLabelFor(this.ta);

		// always enable wrapping:
		this.ta.setLineWrap(true);
		this.ta.setWrapStyleWord(true);
	}
	
	public void setFormat(CollectableEntityField clctef) {
		final AbstractDocument docNew = DocumentFactory.getDocument(clctef);
		if(docNew != null) {
			final AbstractDocument docOld = (AbstractDocument)ta.getDocument();
			
			ta.setDocument(docNew); // set document before adding rest of listeners.
			for(DocumentListener dl : docOld.getDocumentListeners()) {
				docNew.addDocumentListener(dl);
			}
		}
	}

	@Override
	protected JComponent getLayeredComponent(){
		return this.ta;
	}

	@Override
	protected JTextComponent getLayeredTextComponent(){
		return this.ta;
	}
	
	public JScrollPane getJScrollPane() {
		return this.scrlpn;
	}

	public JTextArea getJTextArea() {
		return this.ta;
	}

	/**
	 * @return the text area
	 */
	@Override
	public JTextComponent getJTextComponent() {
		return this.ta;
	}

	@Override
	public JComponent getControlComponent() {
		return this.getJScrollPane();
	}
	
	@Override
	public boolean hasFocus() {
		return this.ta.hasFocus();
	}

	/**
	 * sets the static tooltip text for the label and the control component (not for the panel itself).
	 * The static tooltip is shown in the control component only if no tooltiptextprovider was set for the control.
	 * 
	 * §postcondition LangUtils.equals(this.getToolTipText(), sToolTipText)
	 * 
	 * @param sToolTipText
	 */
	@Override
	public void setToolTipText(String sToolTipText) {
		this.getJLabel().setToolTipText(sToolTipText);
		this.ta.setToolTipText(sToolTipText);

		assert LangUtils.equal(this.getToolTipText(), sToolTipText);
	}

	@Override
	protected void setToolTipTextProviderForControl(ToolTipTextProvider tooltiptextprovider) {
		super.setToolTipTextProviderForControl(tooltiptextprovider);
		if (tooltiptextprovider != null) {
			// This is necessary to enable dynamic tooltips for the text area:
			ToolTipManager.sharedInstance().registerComponent(this.getJTextArea());
		}
	}

	@Override
	protected GridBagConstraints getGridBagConstraintsForLabel() {
		final GridBagConstraints result = (GridBagConstraints) super.getGridBagConstraintsForLabel().clone();

		result.anchor = GridBagConstraints.NORTHWEST;

		return result;
	}

	@Override
	protected GridBagConstraints getGridBagConstraintsForControl(boolean bFill) {
		final GridBagConstraints result = (GridBagConstraints) super.getGridBagConstraintsForControl(bFill).clone();

		// always fill vertically:
		switch (result.fill) {
			case GridBagConstraints.NONE:
				result.fill = GridBagConstraints.VERTICAL;
				break;
			case GridBagConstraints.HORIZONTAL:
				result.fill = GridBagConstraints.BOTH;
				break;
			case GridBagConstraints.BOTH:
				result.fill = GridBagConstraints.BOTH;
				break;
			default:
				assert false;
		}

		result.weighty = 1.0;

		// no top/bottom insets:
		result.insets.top = 0;
		result.insets.bottom = 0;

		return result;
	}

	/**
	 * sets the number of columns of the textarea
	 * @param iColumns
	 */
	@Override
	public void setColumns(int iColumns) {
		this.getJTextArea().setColumns(iColumns);
	}

	@Override
	public void setRows(int iRows) {
		this.getJTextArea().setRows(iRows);
	}

	@Override
	public void setName(String sName) {
		super.setName(sName);
		UIUtils.setCombinedName(this.ta, sName, "ta");
	}
	
	public TextModuleSettings getTextModuleSettings() {
		return tms;
	}

	public void setTextModuleSettings(TextModuleSettings tms) {
		this.tms = tms;
	}

	@Override
	public JTextComponent getTargetTextComponent() {
		return getJTextArea();
	}
	
}  // class LabeledTextArea
