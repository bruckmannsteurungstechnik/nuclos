package org.nuclos.client.ui.labeled;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.EventObject;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.AbstractCellEditor;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.event.ChangeEvent;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import org.nuclos.client.i18n.language.data.DataLanguageContext;
import org.nuclos.client.i18n.language.data.DataLanguageDelegate;
import org.nuclos.common.EntityMetaVO;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableComponentTypes;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.IDataLanguageMap;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.server.i18n.language.data.DataLanguageLocalizedEntityEntry;
import org.nuclos.server.i18n.language.data.DataLanguageUtils;
import org.nuclos.server.i18n.language.data.DataLanguageVO;

public class LocalizedEntityFieldFillInPanel extends JPanel {
	private EntityMetaVO entityMeta;
	private Long primaryKey;
	private IDataLanguageMap languageMap;
	private List<Pair<FieldMeta, Integer>> fields;
	private List<FieldMeta> lockedfields;
	private JTextField txtEditingString;
	private JTextArea txtEditingMemo;
	private JTextArea txtEditingLargeTextObjects;

	public LocalizedEntityFieldFillInPanel(EntityMetaVO entityMeta, List<Pair<FieldMeta, Integer>> fields, Long primaryKey, IDataLanguageMap languageMap,
			List<FieldMeta> lockedfields) {
		super(new BorderLayout());
		this.fields = fields;
		this.entityMeta = entityMeta;
		this.languageMap = languageMap;
		this.primaryKey = primaryKey;
		this.lockedfields = lockedfields;

		initComponents();
	}

	public void initComponents() {

		LocalizedEntityFieldFillInTableModel model = new LocalizedEntityFieldFillInTableModel(entityMeta, fields, primaryKey, this.languageMap, lockedfields);

		final JTable tblLanguages = new JTable(model) {

			@Override
			public void editingStopped(ChangeEvent e) {
				setRowHeight(getEditingRow(), 16);
				super.editingStopped(e);
			}

			@Override
			public void editingCanceled(ChangeEvent e) {
				setRowHeight(getEditingRow(), 16);
				removeEditor();
			}

			public boolean editCellAt(int row, int column, EventObject e) {				
				boolean retVal = super.editCellAt(row, column, e);
				if (retVal) {
					LocalizedEntityFieldFillInTableModel model = (LocalizedEntityFieldFillInTableModel) getModel();
					Pair<FieldMeta, Integer> field = model.getFields().get(column - 1);

					if (field.getY().equals(CollectableComponentTypes.TYPE_TEXTAREA)) {

						int newHeight = model.getValueAt(row, column) != null ? (model.getValueAt(row, column).toString().split("\n").length * 16) + 100 : 100;

						if (newHeight > getRowHeight(row)) {
							setRowHeight(row, newHeight);
						}
					}
				}
				return retVal;
			}

			public TableCellEditor getCellEditor(int row, int column) {		
				if (column <= 0) {
					return null;
				}

				LocalizedEntityFieldFillInTableModel model = (LocalizedEntityFieldFillInTableModel) getModel();
				Pair<FieldMeta, Integer> field = model.getFields().get(column - 1);

				if (field.getY().equals(CollectableComponentTypes.TYPE_TEXTAREA)) {

					return new MemoCellTableCellEditor();
				} else {
					return super.getCellEditor(row, column);
				}
			}
		};

		tblLanguages.setDefaultRenderer(Object.class, new LocalizedEntityFieldFillInTableRenderer(model));
		tblLanguages.addMouseListener(new JTableButtonMouseListener(tblLanguages));

		tblLanguages.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
		tblLanguages.setColumnSelectionAllowed(true);
		tblLanguages.setRowSelectionAllowed(true);
		tblLanguages.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		Action escapeAction2 = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				if (tblLanguages.isEditing()) {
					tblLanguages.editingCanceled(null);
				}
			}
		};

		KeyStroke escape = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false);
		tblLanguages.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(escape, "ESCAPE");
		tblLanguages.getActionMap().put("ESCAPE", escapeAction2);

		JScrollPane scrPane = new JScrollPane(tblLanguages, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		this.add(scrPane, BorderLayout.CENTER);

		Dimension preferredSize = getPreferredSize();
		preferredSize.setSize(model.getColumnCount() * 250, getPreferredSize().getHeight());
		this.setPreferredSize(preferredSize);
	}

	public IDataLanguageMap getFieldLanguageMap() {
		return this.languageMap;
	}

	public class LocalizedEntityFieldFillInTableRenderer extends DefaultTableCellRenderer {
		LocalizedEntityFieldFillInTableModel model;

		int[] colWidth = null;

		public LocalizedEntityFieldFillInTableRenderer(LocalizedEntityFieldFillInTableModel model) {
			this.model = model;
			this.colWidth = new int[model.getColumnCount()];
		}

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

			Object valueToString = value;
			if (value != null) {
				if (value.toString().split("\n").length > 1) {
					valueToString = value.toString().substring(0, value.toString().indexOf("\n")) + "...";
				}
			}

			Component tableCellRendererComponent = super.getTableCellRendererComponent(table, valueToString, isSelected, hasFocus, row, column);

			if (column == 2 && row>0) 
				return new JTableButtonRenderer().getTableCellRendererComponent(table, valueToString, isSelected, hasFocus, row, column);
			if (column == 2 && row==0) // first row no button because this is the primary language
				return tableCellRendererComponent;
				
				
			if (isSelected) {
				tableCellRendererComponent.setForeground(table.getSelectionForeground());
			} else {
				tableCellRendererComponent.setForeground(table.getForeground());
				if (!model.isCellEditable(row, column) && column > 0) {
					tableCellRendererComponent.setBackground(Color.decode("#f0f0f0"));
				} else {
					tableCellRendererComponent.setBackground(Color.WHITE);
				}
			}

			FontMetrics fontMetrics = table.getFontMetrics(table.getFont());
			int newWidth = 150;
			if (valueToString.toString().trim().length() > 0) {
				if (value.toString().split("\n").length > 1) {
					newWidth = fontMetrics.stringWidth(value.toString().substring(0, value.toString().indexOf("\n")) + "...") + 50;
				} else {
					newWidth = fontMetrics.stringWidth(value.toString()) + 50;
				}
			}

			if (column > 0) {
				if (this.colWidth[column] == 0) {
					table.getColumnModel().getColumn(column).setPreferredWidth(newWidth);
					// table.getColumnModel().getColumn(column).setMinWidth(newWidth);
					this.colWidth[column] = newWidth;
				} else {
					if (newWidth > this.colWidth[column]) {
						table.getColumnModel().getColumn(column).setPreferredWidth(newWidth);
						// table.getColumnModel().getColumn(column).setMinWidth(newWidth);
						this.colWidth[column] = newWidth;
					}
				}
			} else {
				table.getColumnModel().getColumn(column).setPreferredWidth(120);
				table.getColumnModel().getColumn(column).setMinWidth(120);
			}

			if (row >= 0) {
				if (column > 0) {
					if (model.isInitial(column, row)) {
						if (isSelected) {
							tableCellRendererComponent.setForeground(table.getSelectionForeground());
						} else {
							// tableCellRendererComponent.setForeground(new
							// Color(153,51,0));
							tableCellRendererComponent.setForeground(new Color(153, 51, 0));
						}
					}
				} else if (column == 0) {
					if (model.isPrimarySelected(row)) {
						tableCellRendererComponent.setFont(tableCellRendererComponent.getFont().deriveFont(Font.BOLD));
					} else if (model.isUserDataSelected(row)) {
						tableCellRendererComponent.setFont(tableCellRendererComponent.getFont().deriveFont(Font.ITALIC));
					}
				}
			}

			return tableCellRendererComponent;
		}
	}

	public class LocalizedEntityFieldFillInTableModel extends DefaultTableModel {

		private List<UID> fieldsAsList;
		private List<Pair<FieldMeta, Integer>> fields;
		private EntityMetaVO entityMeta;
		private IDataLanguageMap languageMap;
		private Map<UID, DataLanguageVO> selectedlanguages;
		private List<UID> selectedlanguagesAsList;
		private Long primaryKey;
		private List<FieldMeta> lockedfields;

		public LocalizedEntityFieldFillInTableModel(EntityMetaVO entityMeta, List<Pair<FieldMeta, Integer>> fields, Long primaryKey, IDataLanguageMap languageMap,
				List<FieldMeta> lockedfields) {
			this.fieldsAsList = new ArrayList(CollectionUtils.transformIntoSet(fields, new Transformer<Pair<FieldMeta, Integer>, UID>() {
				@Override
				public UID transform(Pair<FieldMeta, Integer> i) {
					return i.getX().getUID();
				}
			}));
			this.fields = fields;
			this.entityMeta = entityMeta;
			this.languageMap = languageMap;
			this.primaryKey = primaryKey;
			this.lockedfields = lockedfields;
			this.selectedlanguages = CollectionUtils.transformIntoMap(DataLanguageDelegate.getInstance().getSelectedLocales(), new Transformer<DataLanguageVO, UID>() {
				@Override
				public UID transform(DataLanguageVO i) {
					return i.getPrimaryKey();
				}
			});
			this.selectedlanguagesAsList = new ArrayList(this.selectedlanguages.keySet());
			Collections.sort(this.selectedlanguagesAsList, new Comparator<UID>() {

				@Override
				public int compare(UID o1, UID o2) {
					return selectedlanguages.get(o1).getOrder().compareTo(selectedlanguages.get(o2).getOrder());
				}

			});
		}

		public boolean isPrimarySelected(int row) {
			return selectedlanguagesAsList.get(row).equals(DataLanguageDelegate.getInstance().getPrimaryDataLanguage());
		}

		public boolean isUserDataSelected(int row) {
			return selectedlanguagesAsList.get(row).equals(DataLanguageContext.getDataUserLanguage());
		}

		public List<Pair<FieldMeta, Integer>> getFields() {
			return this.fields;
		}

		public boolean isInitial(int col, int row) {

			UID selectedLanguage = selectedlanguagesAsList.get(row);
			UID field = fieldsAsList.get(col - 1);
			UID fieldFlaggedDL = DataLanguageUtils.extractFieldUID(field, true);

			if (languageMap.getDataLanguage(selectedLanguage) != null && languageMap.getDataLanguage(selectedLanguage).getFieldValue(fieldFlaggedDL) != null) {
				return Boolean.TRUE.equals(languageMap.getDataLanguage(selectedLanguage).getFieldValue(fieldFlaggedDL));
			} else {
				return Boolean.FALSE;
			}
		}

		@Override
		public int getColumnCount() {
			return fieldsAsList.size() + 1 + 1;
		}

		public Class<?> getColumnClass(int columnIndex) {
			return String.class;
		}

		@Override
		public int getRowCount() {
			return this.languageMap != null && this.languageMap.getLanguageMap() != null ? this.languageMap.getLanguageMap().keySet().size() : 0;
		}

		@Override
		public String getColumnName(int column) {
			if (column == 0)
				return "";
			else if (column == 2)
				return "";
			else
				return SpringLocaleDelegate.getInstance().getText(entityMeta.getField(fieldsAsList.get(column - 1)).getLocaleResourceIdForLabel());
		}

		@Override
		public void setValueAt(Object aValue, int row, int column) {

			UID field = fieldsAsList.get(column - 1);
			UID selectedLanguage = selectedlanguagesAsList.get(row);
			UID primLanguage = DataLanguageContext.getDataSystemLanguage();
			UID userLanguage = DataLanguageContext.getDataUserLanguage();

			String value = (String) aValue;
			if (this.languageMap != null) {
				DataLanguageUtils.setLocalizedValueAndValidateMap(this.languageMap, primaryKey, entityMeta, selectedLanguage, primLanguage, userLanguage, field,
						selectedlanguagesAsList, value);
			}
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			if (columnIndex == 2 && rowIndex!=0)// dirty buttpn
			{
				Pair<FieldMeta, Integer> pair = fields.get(0);
				JButton button = new DirtyButton(rowIndex, SpringLocaleDelegate.getInstance().getMessage("LocalizedEntityFieldFillInPanel.btnDirty.1", "leeren"));
				if (this.lockedfields.contains(pair.x)) {
					button.setEnabled(false);
				}
				return button;
			}else if (columnIndex == 2 && rowIndex==0)
			{
				return "";
			}
			
			
			String retVal = "";

			if (rowIndex >= 0 && columnIndex >= 0) {

				UID selectedLanguage = selectedlanguagesAsList.get(rowIndex);

				if (columnIndex == 0) {
					retVal = selectedlanguages.get(selectedLanguage).toString();
				} else {
					UID field = fieldsAsList.get(columnIndex - 1);
					UID fieldDL = DataLanguageUtils.extractFieldUID(field);
					if (this.languageMap.getDataLanguage(selectedLanguage) != null) {
						if (this.languageMap.getDataLanguage(selectedLanguage).getFieldValue(fieldDL) != null) {
							retVal = (String) this.languageMap.getDataLanguage(selectedLanguage).getFieldValue(fieldDL);
						}
					}
				}
			}

			return retVal;
		}

		private class DirtyButton extends JButton implements ActionListener {
			private int row;
			public DirtyButton(int row, String name) {
				super(name);
				this.row = row;
				addActionListener(this);
			}
			@Override
			public void actionPerformed(ActionEvent e) {
				UID selectedLanguage = selectedlanguagesAsList.get(row);
				UID field = fieldsAsList.get(0);
				UID fieldFlaggedDL = DataLanguageUtils.extractFieldUID(field, true);

				DataLanguageLocalizedEntityEntry existBlock = languageMap.getDataLanguage(selectedLanguage);
								
				existBlock.setFieldValue(fieldFlaggedDL, true);
				if (existBlock.getPrimaryKey() == null) {
					existBlock.flagNew();
				} else {
					existBlock.flagUpdate();
				}
				languageMap.setDataLanguage(selectedLanguage, existBlock);
			}

		}

		@Override
		public boolean isCellEditable(int row, int column) {
			if (column == 2)
				return false;
			boolean retVal = false;

			if (column != 0) {
				Pair<FieldMeta, Integer> pair = fields.get(column - 1);
				if (this.lockedfields.contains(pair.x)) {
					retVal = false;
				} else {
					retVal = true;
				}
			}
			return retVal;
		}

	}

	class MemoCellTableCellEditor extends AbstractCellEditor implements TableCellEditor {
		private JComponent component;
		private JScrollPane scrollPane;

		public MemoCellTableCellEditor() {
			component = new JTextArea();
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(component);
		}

		@Override
		public boolean isCellEditable(EventObject anEvent) {
			if (anEvent instanceof MouseEvent) {
				return ((MouseEvent) anEvent).getClickCount() >= 2;
			}
			return true;
		}

		public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int rowIndex, int vColIndex) {
			((JTextArea) component).setText((String) value);
			return scrollPane;
		}

		public Object getCellEditorValue() {
			return ((JTextArea) component).getText();
		}
	}
	
	public static class JTableButtonRenderer implements TableCellRenderer {
		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			if (value instanceof JButton) {
				JButton button = (JButton) value;
				return button;
			}
			return null;

		}
	}

	private static class JTableButtonMouseListener extends MouseAdapter {
		private final JTable table;

		public JTableButtonMouseListener(JTable table) {
			this.table = table;
		}
		public void mouseClicked(MouseEvent e) {
			int column = table.getColumnModel().getColumnIndexAtX(e.getX());
			int row = e.getY() / table.getRowHeight();

			if (row < table.getRowCount() && row >= 0 && column < table.getColumnCount() && column >= 0) {
				Object value = table.getValueAt(row, column);
				if (value instanceof JButton) {
					((JButton) value).doClick();
				}
			}
		}
	}
}
