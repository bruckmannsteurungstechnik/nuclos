package org.nuclos.client.ui.layer;

import java.awt.Component;
import java.util.concurrent.Future;

import org.nuclos.client.command.CommandHandler;
import org.nuclos.client.ui.UIUtils;

/**
 * default command handler: shows a wait cursor during the execution of a command.
 */
public final class LockingCommandHandler implements CommandHandler {
	
	private Future<LayerLock> lock;
	
	@Override
    public void commandStarted(Component parent) {
		lock = UIUtils.lockFrame(parent);
	}

	@Override
    public void commandFinished(Component parent) {
		UIUtils.unLockFrame(parent, lock);
	}

}	// class DefaultCommandHandler
