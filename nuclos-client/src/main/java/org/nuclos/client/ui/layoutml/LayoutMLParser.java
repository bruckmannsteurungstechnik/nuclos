//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.layoutml;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.EventListener;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultButtonModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import org.apache.commons.lang.NullArgumentException;
import org.apache.log4j.Logger;
import org.nuclos.api.Property;
import org.nuclos.api.ui.layout.LayoutComponent;
import org.nuclos.api.ui.layout.LayoutComponent.LayoutComponentType;
import org.nuclos.api.ui.layout.LayoutComponentFactory;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.NuclosCollectableStateComboBox;
import org.nuclos.client.common.Utils.CollectableLookupProvider;
import org.nuclos.client.entityobject.CollectableEOEntityClientProvider;
import org.nuclos.client.layout.LayoutComponentContextImpl;
import org.nuclos.client.layout.LayoutComponentHolder;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels.STATIC_BUTTON;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.nuclet.NucletComponentRepository;
import org.nuclos.client.resource.ResourceCache;
import org.nuclos.client.ui.CommonJScrollPane;
import org.nuclos.client.ui.CommonJSeparator;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.JInfoTabbedPane;
import org.nuclos.client.ui.LineLayout;
import org.nuclos.client.ui.StrictSizeComponent;
import org.nuclos.client.ui.TitledSeparator;
import org.nuclos.client.ui.collect.Chart;
import org.nuclos.client.ui.collect.CollectableComponentsProvider;
import org.nuclos.client.ui.collect.DefaultCollectableComponentsProvider;
import org.nuclos.client.ui.collect.DefaultLayoutComponentsProvider;
import org.nuclos.client.ui.collect.component.CollectableComboBox;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableComponentFactory;
import org.nuclos.client.ui.collect.component.CollectableComponentType;
import org.nuclos.client.ui.collect.component.CollectableDateChooser;
import org.nuclos.client.ui.collect.component.CollectableIdTextField;
import org.nuclos.client.ui.collect.component.CollectableListOfValues;
import org.nuclos.client.ui.collect.component.CollectableOptionGroup;
import org.nuclos.client.ui.collect.component.CollectableTextField;
import org.nuclos.client.ui.collect.component.DelegatingCollectablePanel;
import org.nuclos.client.ui.collect.component.LabeledCollectableComponent;
import org.nuclos.client.ui.collect.component.LabeledCollectableComponentWithVLP;
import org.nuclos.client.ui.collect.component.LookupEvent;
import org.nuclos.client.ui.collect.component.LookupListener;
import org.nuclos.client.ui.collect.component.Parameterisable;
import org.nuclos.client.ui.collect.component.TextModuleSupport;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModel;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelAdapter;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelEvent;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelListener;
import org.nuclos.client.ui.collect.component.model.CollectableLocalizedComponentModel;
import org.nuclos.client.ui.collect.component.model.DetailsComponentModel;
import org.nuclos.client.ui.collect.component.model.DetailsComponentModelEvent;
import org.nuclos.client.ui.collect.component.model.DetailsLocalizedComponentModel;
import org.nuclos.client.ui.collect.component.model.SearchComponentModel;
import org.nuclos.client.ui.collect.component.model.SearchComponentModelEvent;
import org.nuclos.client.ui.collect.subform.Column;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.client.ui.collect.subform.SubFormTableModel;
import org.nuclos.client.ui.gc.IReferenceHolder;
import org.nuclos.client.ui.labeled.LabeledComponent;
import org.nuclos.client.ui.labeled.LabeledComponentSupport;
import org.nuclos.client.ui.labeled.LabeledTextArea;
import org.nuclos.client.ui.labeled.LabeledTextArea.InnerTextArea;
import org.nuclos.client.ui.layoutml.DefaultComponentBuilder.CollectableComponentBuilder;
import org.nuclos.client.ui.layoutml.DefaultComponentBuilder.TabbedPaneConstraints;
import org.nuclos.client.ui.layoutml.TargetComponentAction.ClearAction;
import org.nuclos.client.ui.layoutml.TargetComponentAction.RefreshValueListAction;
import org.nuclos.client.ui.layoutml.TargetComponentAction.ReinitSubformAction;
import org.nuclos.client.ui.layoutml.TargetComponentAction.TransferLookedUpValueAction;
import org.nuclos.client.ui.matrix.JMatrixComponent;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.EntityMetaVO;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosScript;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SF;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.TranslationMap;
import org.nuclos.common.UID;
import org.nuclos.common.caching.GenCache;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableComponentTypes;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableFieldsProviderFactory;
import org.nuclos.common.collect.collectable.CollectableUtils;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.DefaultCollectableEntityField;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.collection.multimap.MultiListHashMap;
import org.nuclos.common.collection.multimap.MultiListMap;
import org.nuclos.common.dal.vo.IDataLanguageMap;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common.textmodule.TextModuleSettings;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.KeyEnum;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.layoutml.MyLayoutConstants;
import org.nuclos.common2.layoutml.exception.LayoutMLException;
import org.nuclos.common2.layoutml.exception.LayoutMLParseException;
import org.nuclos.server.i18n.language.data.DataLanguageLocalizedEntityEntry;
import org.nuclos.server.i18n.language.data.DataLanguageUtils;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import info.clearthought.layout.TableLayout;
import info.clearthought.layout.TableLayoutConstraints;

/**
 * Parser for the LayoutML. Constructs a panel containing components for display/editing
 * from a LayoutML definition file.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class LayoutMLParser extends org.nuclos.common2.layoutml.LayoutMLParser {

	private static final Logger LOG = Logger.getLogger(LayoutMLParser.class);

	public LayoutMLParser() {
		super();
	}
	
	/**
	 * parses the LayoutML definition in <code>inputsource</code> and builds a component out of it.
	 * 
	 * §precondition clcte != null
	 * 
	 *
	 * @param layoutUID
	 * @param inputsource the <code>InputSource</code> containing the LayoutML definition
	 * @param clcte the meta information about the objects to collect in this form
	 * @param bCreateSearchableComponents Create searchable components? (false: create editable components)
	 * @param actionlistenerButtons the common <code>ActionListener</code>, if any, for all button elements.
	 * @param valuelistproviderfactory the factory that creates providers for value lists in <code>CollectableComboBoxes</code>.
	 * @param collectableComponentFactory
	 * @return the <code>LayoutRoot</code> that contains the objects constructed from the LayoutML definition
	 * @throws LayoutMLParseException when a parse exception occurs.
	 * @throws LayoutMLException when a general exception occurs.
	 * @throws IOException when an I/O error occurs (rather fatal)
	 */
	public <PK> LayoutRoot<PK> getResult(final UID layoutUID, InputSource inputsource, CollectableEntity clcte, boolean bCreateSearchableComponents, boolean bExistingBo, ActionListener actionlistenerButtons,
										 CollectableFieldsProviderFactory valuelistproviderfactory, CollectableComponentFactory collectableComponentFactory)
			throws LayoutMLException, IOException {

		if (clcte == null) {
			throw new NullArgumentException("clcte");
		}
		final BuildFormHandler handler = new BuildFormHandler(layoutUID, clcte, this, bCreateSearchableComponents, bExistingBo,
				actionlistenerButtons, collectableComponentFactory, valuelistproviderfactory);
		try {
			this.parse(inputsource, handler);
			
			if (bCreateSearchableComponents) {
				EntityMeta<?> eMeta = MetaProvider.getInstance().getEntity(clcte.getUID());
				if (eMeta.isMandator()) {
					final CollectableEntityField clctefSearch = new DefaultCollectableEntityField(clcte.getEntityField(SF.MANDATOR.getUID(clcte.getUID())), clcte.getUID());
					handler.getCollectableComponentsProvider().addCollectableComponent(new NuclosCollectableStateComboBox(clctefSearch, true));
				}
				if (eMeta.isOwner()) {
					final CollectableEntityField clctefSearch = new DefaultCollectableEntityField(clcte.getEntityField(SF.OWNER.getUID(clcte.getUID())), clcte.getUID());
					handler.getCollectableComponentsProvider().addCollectableComponent(new CollectableListOfValues<UID>(clctefSearch, true, true));
				}
				if (handler.getCollectableComponentsProvider().getCollectableComponentsFor(SF.CREATEDBY.getUID(clcte.getUID())).isEmpty()) {
					final CollectableEntityField clctefSearch = new DefaultCollectableEntityField(clcte.getEntityField(SF.CREATEDBY.getUID(clcte.getUID())), clcte.getUID());
					handler.getCollectableComponentsProvider().addCollectableComponent(new CollectableTextField(clctefSearch, true));
				}
				if (handler.getCollectableComponentsProvider().getCollectableComponentsFor(SF.CHANGEDBY.getUID(clcte.getUID())).isEmpty()) {
					final CollectableEntityField clctefSearch = new DefaultCollectableEntityField(clcte.getEntityField(SF.CHANGEDBY.getUID(clcte.getUID())), clcte.getUID());
					handler.getCollectableComponentsProvider().addCollectableComponent(new CollectableTextField(clctefSearch, true));
				}
				if (handler.getCollectableComponentsProvider().getCollectableComponentsFor(SF.CREATEDAT.getUID(clcte.getUID())).isEmpty()) {
					final CollectableEntityField clctefSearch = new DefaultCollectableEntityField(clcte.getEntityField(SF.CREATEDAT.getUID(clcte.getUID())), clcte.getUID());
					clctefSearch.setJavaClass(Date.class);
					handler.getCollectableComponentsProvider().addCollectableComponent(new CollectableDateChooser(clctefSearch, true, true));
				}
				if (handler.getCollectableComponentsProvider().getCollectableComponentsFor(SF.CHANGEDAT.getUID(clcte.getUID())).isEmpty()) {
					final CollectableEntityField clctefSearch = new DefaultCollectableEntityField(clcte.getEntityField(SF.CHANGEDAT.getUID(clcte.getUID())), clcte.getUID());
					clctefSearch.setJavaClass(Date.class);
					handler.getCollectableComponentsProvider().addCollectableComponent(new CollectableDateChooser(clctefSearch, true, true));
				}
			}

			return new LayoutRoot<PK>(bCreateSearchableComponents, handler.getRootComponent(), handler.getCollectableComponentsProvider(), handler.getLayoutComponentsProvider(),
					handler.getMapOfCollectableComponentModels(), handler.stack.getMapOfSubForms(), handler.stack.getMapOfMatrix(), handler.getMultiMapOfDependencies(),
					handler.getInitialFocusEntityAndField());
		} catch (SAXParseException ex) {
			throw new LayoutMLParseException(ex);
		} catch (SAXException ex) {
			throw new LayoutMLException(ex, handler.getDocumentLocator());
		} catch (OutOfMemoryError e) {
			Throwable thr = e;
			while (thr.getCause() != null) {
				thr = thr.getCause();
			}
			LOG.error("LayoutMLParser failed: " + e, thr);
			throw e;
		}
		finally {
			handler.close();
		}
	}
	
	public static abstract class LayoutComponentDelegator {
		
		private boolean found = false;
		
		public LayoutComponentDelegator(Component c) {
			if (c instanceof LayoutComponent) {
				found = true;
				foundLC((LayoutComponent) c);
			} else if (c instanceof LayoutComponentHolder) {
				LayoutComponentHolder lch = (LayoutComponentHolder) c;
				if (lch.getLayoutComponent() != null) {
					found = true;
					foundLC(lch.getLayoutComponent());
				}
			}
		}
		
		public boolean found() {
			return this.found;
		}
		
		public abstract void foundLC(LayoutComponent lc);
		
	}

	/**
	 * inner class <code>BuildFormHandler</code>. The handler listens to the events that are issued by
	 * the underlying SAX parser.
	 */
	static class BuildFormHandler extends BasicHandler implements Closeable {

		private static class Event {

			Event() {
			}

			/** @todo encapsulate fields */
			String sType;
			UID sSourceComponentName;
			SubForm subform;

			@Override
			public String toString() {
				final StringBuilder result = new StringBuilder();
				result.append("LayoutMLParser.Event[type=").append(sType);
				result.append(", src=").append(sSourceComponentName);
				result.append(", subform=").append(subform);
				result.append("]");
				return result.toString();
			}
		}

		private static class LookupCollectableComponentModelListener implements CollectableComponentModelListener {

			private final GenCache<Object, Collectable> cache;

			private final CollectableComponentsProvider provider;

			private final List<Pair<UID, CollectableComponentModel>> transfers;

			private final JComponent rootComponent;

			//

			private LookupCollectableComponentModelListener(GenCache<Object, Collectable> cache,
					CollectableComponentsProvider provider,
					List<Pair<UID, CollectableComponentModel>> transfers,
					JComponent rootComponent) {
				this.cache = cache;
				this.provider = provider;
				this.transfers = transfers;
				this.rootComponent = rootComponent;
			}

			@Override
			public void valueToBeChanged(DetailsComponentModelEvent ev) {
			}

			@Override
			public void searchConditionChangedInModel(SearchComponentModelEvent ev) {
			}

			@Override
			public void collectableFieldChangedInModel(CollectableComponentModelEvent ev) {
				CollectableComponentModel model = ev.getCollectableComponentModel();
				//NUCLOS-4936 Step 5 - If an existing BO is initializing, there is no need to transfer looked up values
				if (model.isInitializing() && model.isExistingBo()) {
					return;
				}
				
				Object id = ev.getNewValue().getValueId();
				Collectable<?> clctSelected = null;
				try {
					clctSelected = cache.get(id);
					for (Pair<UID, CollectableComponentModel> t : transfers) {
						if (!t.getY().equals(ev.getCollectableComponentModel())) { // prevent stackoverflow. 
							boolean bMultiEditable = false;
							if(ev.getCollectableComponentModel() instanceof DetailsComponentModel) {
								DetailsComponentModel compModel = (DetailsComponentModel)ev.getCollectableComponentModel();
								bMultiEditable = compModel.isMultiEditable();
							}
							if(!bMultiEditable) {
								transferValue(provider, clctSelected, t.getX(), t.getY(), ev.getCollectableComponentModel(), ev);
							}
						}
					}
				} catch (Exception ex) {
					Errors.getInstance().showExceptionDialog(rootComponent, ex);
				}
			}
		}

		private static class RuleLookupListener<PK2> implements LookupListener<PK2> {

			private final CollectableComponentsProvider provider;

			private final UID sourceFieldName;

			private final CollectableComponentModel clctcompmodelSource;

			private final CollectableComponentModel clctcompmodelTarget;

			private RuleLookupListener(CollectableComponentsProvider provider, UID sourceFieldName,
					CollectableComponentModel clctcompmodelSource, CollectableComponentModel clctcompmodelTarget) {
				this.provider = provider;
				this.sourceFieldName = sourceFieldName;
				this.clctcompmodelSource = clctcompmodelSource;
				this.clctcompmodelTarget = clctcompmodelTarget;
			}

			@Override
			public void lookupSuccessful(LookupEvent ev) {
				if (!clctcompmodelSource.isInitializing()) {
					transferValue(provider, ev.getSelectedCollectable(), sourceFieldName, clctcompmodelTarget, clctcompmodelSource, null);
				}
			}

			@Override
            public int getPriority() {
                return 1;
            }
		}

		private static class HandleClearLookupListener<PK2> implements LookupListener<PK2> {

			private final CollectableComponentModel clctcompmodelSource;

			private final CollectableComponentModel clctcompmodelTarget;

			private HandleClearLookupListener(
					CollectableComponentModel clctcompmodelSource, CollectableComponentModel clctcompmodelTarget) {
				this.clctcompmodelSource = clctcompmodelSource;
				this.clctcompmodelTarget = clctcompmodelTarget;
			}

			@Override
            public void lookupSuccessful(LookupEvent ev) {
				if (!clctcompmodelSource.isInitializing()) {
					clctcompmodelTarget.clear();
				}
			}

			@Override
            public int getPriority() {
                return 1;
            }
		}

		private class Rule implements IReferenceHolder {
			
			private final List<EventListener> refs = new LinkedList<EventListener>();

			private final Collection<TargetComponentAction> collActions = new LinkedList<TargetComponentAction>();

			private Event event;

			Rule() {
			}
			
			@Override
			public void addRef(EventListener e) {
				refs.add(e);
			}

			@Override
			public String toString() {
				final StringBuilder result = new StringBuilder();
				result.append("LayoutMLParser.Rule[event=").append(event).append("]");
				return result.toString();
			}

			public void finish() throws SAXException {
				/** @todo q&d - refactor: make this oo! */

				if (event == null) {
					/** @todo Why is this necessary here? The SAX parser should validate this before. */
					throw new SAXException("Das rule-Element ben\u00f6tigt ein event-Element.");
				}

				if (event.subform != null) {
					if (event.sType.equals(ATTRIBUTEVALUE_LOOKUP)) {
						handleLookUpEventInSubForm();
					}
					else if (event.sType.equals(ATTRIBUTEVALUE_VALUECHANGED)) {
						handleValueChangedEventInSubForm();
					}
					else {
						throw new SAXException(StringUtils.getParameterizedExceptionMessage("LayoutMLParser.1", event.sType));
					}
				}
				else {
					if (event.sType.equals(ATTRIBUTEVALUE_VALUECHANGED)) {
						handleValueChangedEvent();
					}
					else if (event.sType.equals(ATTRIBUTEVALUE_LOOKUP)) {
						handleLookupEvent();
					}
					else {
						throw new SAXException(StringUtils.getParameterizedExceptionMessage("LayoutMLParser.1", event.sType));
					}
				}
			}	// finish

			private void handleLookUpEventInSubForm() throws SAXException {
				for (TargetComponentAction act : this.collActions) {
					if (act instanceof TransferLookedUpValueAction) {
						final TransferLookedUpValueAction transferaction = (TransferLookedUpValueAction) act;
						Column subformcolumn = event.subform.getColumn(event.sSourceComponentName);
						if (subformcolumn == null) {
							subformcolumn = new Column(event.sSourceComponentName);
							event.subform.addColumn(subformcolumn);
						}
						subformcolumn.addTransferLookedUpValueAction(new org.nuclos.client.ui.collect.subform.TransferLookedUpValueAction(transferaction.getTargetComponentName(), transferaction.getSourceFieldName()));
					}	// TransferLookedUpValueAction
					else {
						throw new SAXException(StringUtils.getParameterizedExceptionMessage("LayoutMLParser.2"));//"Unbekannte Aktion f\u00fcr Unterformular.");
					}
				}	// for
			}

			private void handleValueChangedEventInSubForm() throws SAXException {
				for (TargetComponentAction action : collActions) {
					if (action instanceof RefreshValueListAction) {
						final RefreshValueListAction rpvact = (RefreshValueListAction) action;

						// if an entity is specified in refresh-possible-values, it must be equal to the entity of the event:
						if (rpvact.getTargetComponentEntityName() != null) {
							if (!rpvact.getTargetComponentEntityName().equals(event.subform.getEntityUID())) {
								//throw new SAXException(StringUtils.getParameterizedExceptionMessage("LayoutMLParser.3", rpvact.getTargetComponentEntityName(), event.subform.getEntityName()));
									//"Entit\u00e4t in refresh-possible-values (" + rpvact.getTargetComponentEntityName() +
										//") stimmt nicht mit der Entit\u00e4t des event (" + event.subform.getEntityName() + ") \u00fcberein.");
							}
						}
						final UID sTargetComponentName = rpvact.getTargetComponentName();
						SubForm targetSubform;
						if (rpvact.sTargetComponentEntityName == null) {
							targetSubform = event.subform;
						} else {
							targetSubform = stack.getSubFormForEntity(rpvact.sTargetComponentEntityName);
							if (targetSubform == null) {
								throw new SAXException(String.format("RefreshValueListAction Error: Entity %s not found!",	rpvact.sTargetComponentEntityName));
							}
						}

						Column subformcolumn = targetSubform.getColumn(sTargetComponentName);
						if (subformcolumn == null) {
							subformcolumn = new Column(sTargetComponentName);
							targetSubform.addColumn(subformcolumn);
						}
                        
						subformcolumn.addRefreshValueListAction(new org.nuclos.client.ui.collect.subform.RefreshValueListAction(sTargetComponentName,
								event.subform.getEntityUID(), event.sSourceComponentName, rpvact.getParameterNameForSourceComponent()));
					} else if (action instanceof ClearAction) {
						final ClearAction clract = (ClearAction) action;
						Column subformcolumn = event.subform.getColumn(event.sSourceComponentName);
						if (subformcolumn == null) {
							subformcolumn = new Column(event.sSourceComponentName);
							event.subform.addColumn(subformcolumn);
						}
						subformcolumn.addClearAction(new org.nuclos.client.ui.collect.subform.ClearAction(clract.getTargetComponentName()));
					} else {
						throw new SAXException(StringUtils.getParameterizedExceptionMessage("LayoutMLParser.2"));
					}
				}	// for
			}

			private void handleLookupEvent() throws SAXException {
				// get the event's source components:
				final Collection<CollectableComponent> collclctcompSource = BuildFormHandler.this.getCollectableComponentsProvider().getCollectableComponentsFor(event.sSourceComponentName);
				if (collclctcompSource.isEmpty()) {
					throw new SAXException(StringUtils.getParameterizedExceptionMessage("LayoutMLParser.4", event.sSourceComponentName));
						//"Die Quellkomponente (f\u00fcr ein Lookup-Event) namens \"" + event.sSourceComponentName + "\" ist im Layout nicht vorhanden.");
				}
				for (final CollectableComponent clctcompSource : collclctcompSource) {
		
						if (clctcompSource instanceof CollectableComboBox || clctcompSource instanceof CollectableListOfValues) {
						if (bCreateSearchableComponents) {
							return; //NUCLOSINT-1160
						}
//							alle Filtern/Gruppieren, so nur TransferActions mit gleicher SourceComp
//							dann, pro Gruppe ein Listener
						// List of associated transfers actions as pair (sourceFieldName -> targetCollectableComponentModel)
						final List<Pair<UID, CollectableComponentModel>> transfers = new ArrayList<Pair<UID, CollectableComponentModel>>();
						for (final TargetComponentAction act : this.collActions) {
							if (act instanceof TransferLookedUpValueAction) {
								TransferLookedUpValueAction transferAct = (TransferLookedUpValueAction) act;
								final UID sourceFieldName = transferAct.getSourceFieldName();
								final UID targetComponentName = transferAct.getTargetComponentName();
								final CollectableComponentModel targetClctCompModel = mpclctcompmodel.get(targetComponentName);
								if (targetClctCompModel == null) {
									throw new SAXException(StringUtils.getParameterizedExceptionMessage("LayoutMLParser.5", targetComponentName));
										//"Zielkomponente ist nicht im Layout vorhanden: " + sTargetComponentName);
								}
								transfers.add(Pair.makePair(sourceFieldName, targetClctCompModel));
							}
						}

						if (!transfers.isEmpty()) {
							UID referencedEntityName = clctcompSource.getEntityField().getReferencedEntityUID();
							if (referencedEntityName != null) {
								final GenCache<Object, Collectable> cache = new GenCache<Object, Collectable>(
										new CollectableLookupProvider(clctcompSource.getEntityField().getEntityUID(),
												clctcompSource.getEntityField().getUID()));

								clctcompSource.getModel().addCollectableComponentModelListener(null, 
										new LookupCollectableComponentModelListener(
										cache, getCollectableComponentsProvider(), transfers, getRootComponent()));
							}
						}
					}
					else {
						LOG.info("Die Quellkomponente (f\u00fcr ein Lookup-Event) namens \"" + event.sSourceComponentName + "\" ist keine Liste von Werten (LOV).");
					}
				}
			}

			private void handleValueChangedEvent() throws SAXException {
				assert event.subform == null;

				// get the event's source component:
				final CollectableComponentModel clctcompmodelSource = mpclctcompmodel.get(event.sSourceComponentName);
				if (clctcompmodelSource == null) {
					throw new SAXException(StringUtils.getParameterizedExceptionMessage("LayoutMLParser.7", event.sSourceComponentName));
				}
				for (TargetComponentAction act : collActions) {
					if (act instanceof ClearAction) {
						final ClearAction clearaction = (ClearAction) act;
						final UID sTargetComponentName = clearaction.getTargetComponentName();

						// add a dependency for this rule so this needn't be specified manually:
						BuildFormHandler.this.addDependency(sTargetComponentName, event.sSourceComponentName);

						final CollectableComponentModel clctcompmodelTarget = mpclctcompmodel.get(sTargetComponentName);
						if (clctcompmodelTarget == null) {
							throw new SAXException(StringUtils.getParameterizedExceptionMessage("LayoutMLParser.5", sTargetComponentName));
						}
						clctcompmodelSource.addCollectableComponentModelListener(
								null, 
								new CollectableComponentModelAdapter() {
							@Override
							public void collectableFieldChangedInModel(CollectableComponentModelEvent ev) {
								/** @todo check if this is correct! */
								if (!ev.getCollectableComponentModel().isInitializing()) {
//									if (clctcompmodelTarget != ev.getSource()) {
									if (!ev.getSources().contains(clctcompmodelTarget)) {
										if (ev.getOldValue() != ev.getNewValue()) {
											clctcompmodelTarget.clear(ev);
										}
									}
								}
							}
						});
					} else if (act instanceof ReinitSubformAction) {
						final ReinitSubformAction rpvact = (ReinitSubformAction) act;
						final UID refence = rpvact.getTargetComponentName();
						final UID subform = rpvact.getSubformEntity();
						final SubForm subFormForEntity = stack.getSubFormForEntity(subform);
						
						clctcompmodelSource.addCollectableComponentModelListener(
								null, 
								new CollectableComponentModelAdapter() {
							@Override
							public void collectableFieldChangedInModel(CollectableComponentModelEvent ev) {
								/** @todo check if this is correct! */
								if (!ev.getCollectableComponentModel().isInitializing()) {
									SubFormTableModel subFormModel = subFormForEntity.getSubformTable().getSubFormModel();
									while(subFormModel.getRowCount() > 0) {
										subFormModel.remove(subFormModel.getRowCount() - 1);
									}
								}
							}
						});
						
					}
					else if (act instanceof RefreshValueListAction) {
						final RefreshValueListAction rpvact = (RefreshValueListAction) act;
						final UID sTargetComponentName = rpvact.getTargetComponentName();
						final UID sTargetComponentEntity = rpvact.getTargetComponentEntityName();
						if (sTargetComponentEntity != null) {
							// subform:
							final SubForm subform = stack.getSubFormForEntity(sTargetComponentEntity);
							JMatrixComponent matrix = null;
							if(subform != null) {
								Column subformcolumn = subform.getColumn(sTargetComponentName);
								if (subformcolumn == null) {
									subformcolumn = new Column(sTargetComponentName);
									subform.addColumn(subformcolumn);
								}
								subformcolumn.addRefreshValueListAction(new org.nuclos.client.ui.collect.subform.RefreshValueListAction(sTargetComponentName,
										BuildFormHandler.this.getCollectableEntity().getUID(), event.sSourceComponentName, rpvact.getParameterNameForSourceComponent()));
							}
							else {								
								matrix = stack.getVLPMatrixForEntity(sTargetComponentEntity);
								if(matrix != null) {
									JMatrixComponent.Column matrixColumn = matrix.getColumn(sTargetComponentName);
									if(matrixColumn == null) {
										matrixColumn = new JMatrixComponent.Column(sTargetComponentName);
										matrix.addColumn(matrixColumn);
									}
									matrixColumn.addRefreshValueListAction(new JMatrixComponent.RefreshValueListAction(sTargetComponentName,									
											BuildFormHandler.this.getCollectableEntity().getUID(), event.sSourceComponentName, rpvact.getParameterNameForSourceComponent()));
								}
							}
							
							if (subform == null && matrix == null) {								
								/** @todo prepend the name of the rule/event/action in the error message */								
								throw new SAXException(StringUtils.getParameterizedExceptionMessage("LayoutMLParser.8", sTargetComponentEntity));
									//"Unterformular f\u00fcr Entit\u00e4t " + sTargetComponentEntity + " nicht gefunden.");
							}
							
						}
						else {
							// regular form:
							final Collection<CollectableComponent> collclctcompTarget = BuildFormHandler.this.getCollectableComponentsProvider().getCollectableComponentsFor(sTargetComponentName);
							if (collclctcompTarget.isEmpty()) {
								throw new SAXException(StringUtils.getParameterizedExceptionMessage("LayoutMLParser.5", sTargetComponentName));
							}

							// add a dependency for this rule so this needn't be specified manually:
							BuildFormHandler.this.addDependency(sTargetComponentName, event.sSourceComponentName);

							for (CollectableComponent clctcompTarget : collclctcompTarget) {
								if (!(clctcompTarget instanceof Parameterisable)) {
									LOG.info("Zielkomponente " + sTargetComponentName + " in action refresh-value-list muss eine parametrisierbare Komponente sein (Auswahlliste etc.).");
									return;
								}
								final Parameterisable clctParameterisableTarget = (Parameterisable) clctcompTarget;

								if(clctParameterisableTarget instanceof LabeledCollectableComponentWithVLP) {
									LabeledCollectableComponentWithVLP clctWithVLP = (LabeledCollectableComponentWithVLP) clctParameterisableTarget;
									// set a "dependant" value list provider if the combobox hasn't one yet:
									if (clctWithVLP.getValueListProvider() == null) {
										clctWithVLP.setValueListProvider(BuildFormHandler.this.valueListProviderFactory.newDependantCollectableFieldsProvider(clctWithVLP.getFieldUID()));
									}
								}

								clctcompmodelSource.addCollectableComponentModelListener(null, 
										new RefreshValueListCollectableComponentModelAdapter(event, clctcompmodelSource,
												clctParameterisableTarget, rpvact.getParameterNameForSourceComponent()));
							}	// for
						}
					}	// RefreshValueListAction
				}	// for
			}

		}	// inner class Rule

		private static class RefreshValueListCollectableComponentModelAdapter extends CollectableComponentModelAdapter 			
			implements IField2Tag {

			private final Event event;
			private final CollectableComponentModel clctcompmodelParent;
			private final Parameterisable clctParameterisable;
			private final String sParameterNameForSourceComponent;

			RefreshValueListCollectableComponentModelAdapter(Event event, CollectableComponentModel clctcompmodelParent, Parameterisable clctcmbbxTarget, String sParameterNameForSourceComponent) {
				this.event = event;
				this.clctcompmodelParent = clctcompmodelParent;
				this.clctParameterisable = clctcmbbxTarget;
				this.sParameterNameForSourceComponent = sParameterNameForSourceComponent;
			}

			@Override
			public Map<UID, String> mpField2Tag() {
				Map<UID, String> mp = new HashMap<UID, String>();
				mp.put(clctcompmodelParent.getFieldUID(), sParameterNameForSourceComponent);
				return mp;
			}

			@Override
			public void collectableFieldChangedInModel(CollectableComponentModelEvent ev) {
				try {
					final CollectableField field = clctcompmodelParent.getField();
					final Object oRelatedId;
					if (field.isIdField()) {
						oRelatedId = clctcompmodelParent.getField().getValueId();
					}
					else {
						oRelatedId = field.getValue();
					}
					clctParameterisable.setParameter(sParameterNameForSourceComponent, oRelatedId);
				}
				catch (UnsupportedOperationException ex) {
					// If this happens, there is nothing we can do about it:
					throw new CommonFatalException("set parameters [" + sParameterNameForSourceComponent + "] to " + clctParameterisable + " failed:\n"
							+ "parent model: " + clctcompmodelParent + " field: " + clctcompmodelParent.getFieldUID()
							+ " on rule event " + event, ex);
				}
				try {
					LOG.trace("LayoutMLParser$BuildFormHandler$Rule.collectableFieldChangedInModel: refreshValueList()");
					
					//NUCLOS-4636: Introduce the "refreshing" components to each other
					//NUCLOS-4683: Do it always even when initializing (important for actions)
					Collection<Parameterisable> collParSource = clctcompmodelParent.getListenerSupport().getListenersOfType(Parameterisable.class);
					for (Parameterisable parSource : collParSource) {
						clctParameterisable.introduceValueListRefreshingSource(parSource);
						parSource.introduceValueListRefreshingTarget(clctParameterisable);
					}
					
					clctParameterisable.applyParameters(clctcompmodelParent.isInitializing(), clctcompmodelParent.isInitializing());
				}
				catch (CommonBusinessException ex) {
					// If this happens, there is nothing we can do about it:
					throw new CommonFatalException("apply parameters [" + sParameterNameForSourceComponent + "] to " + clctParameterisable
							+ " failed on rule event " + event, ex);
				}
			}
		}	// inner class RefreshValueListCollectableComponentModelAdapter

		/**
		 * Transfers looked-up values.
		 */
		private static void transferValue(CollectableComponentsProvider provider, Collectable clctSelected, UID sourceFieldName, 
				CollectableComponentModel clctcompmodelTarget, CollectableComponentModel clctcompmodelSource, final CollectableComponentModelEvent clctcompmodelEvent) {
			
			// @see NUCLOS-5662
			if (clctcompmodelEvent != null) {
				Set<CollectableComponentModel> sources = clctcompmodelEvent.getSources();
				if (sources != null && sources.contains(clctcompmodelTarget)) {
					return;
				}
			}
			
			//NUCLOSINT-1160
			if (clctcompmodelTarget instanceof SearchComponentModel) {
				return; 
			}
			
			// @see NUCLOS-1281
			if (clctcompmodelSource.isInitializing()) {
				return;
			}
			
			if (clctSelected == null) {
				clctcompmodelTarget.clear();
				
			}
			else {
				CollectableField clctfSourceTemp = clctSelected.getField(sourceFieldName);
				
				// create a new CollectableField based on the target entity field type:
				final CollectableEntityField clctefTarget = clctcompmodelTarget.getEntityField();
				
				FieldMeta<?> sourceMeta = MetaProvider.getInstance().getEntityField(sourceFieldName);
				Boolean sourceIsLocalized = sourceMeta.isLocalized();
				
				// In case of localization target and source must both be (non) localized;
				// if they are, all localized values must be transfered as well
				Boolean targetIsLocalized = Boolean.valueOf(clctcompmodelTarget instanceof CollectableLocalizedComponentModel);
				if (sourceIsLocalized.equals(targetIsLocalized)) {
					if (Boolean.TRUE.equals(targetIsLocalized)) {
						CollectableEntityField clctefSource = CollectableEOEntityClientProvider.getInstance().getCollectableEntity(sourceMeta.getEntity()).getEntityField(sourceFieldName);
						CollectableComponentModel transferLocalizedValuesSourceModel = new DetailsLocalizedComponentModel(clctefSource);
						((DetailsLocalizedComponentModel)transferLocalizedValuesSourceModel).initDataLanguageMap(clctSelected.getDataLanguageMap(), null);
						((DetailsLocalizedComponentModel)transferLocalizedValuesSourceModel).setFieldInitial(clctfSourceTemp, false, true);
						transferLocalizedValues(transferLocalizedValuesSourceModel, clctcompmodelTarget);
					}
				} else {
					// localized or not both fields must have the same settings
					throw new CommonFatalException("Cannot transfer field '" + clctefTarget.getUID() + "' because one field supports localization, the other does not.");
				}
								
				//BMWFDM-753. A lookup of the process of a different entity. It won't fit, so try to find the right one.
				//It must be a process of the target entity.
				if (SF.PROCESS.getUID(clctefTarget.getEntityUID()).equals(clctefTarget.getUID())) {
					
					List<CollectableValueIdField> lstProcesses = MasterDataDelegate.getInstance().getProcessByEntity(clctefTarget.getEntityUID(), false);
					
					for (CollectableField process : lstProcesses) {
						if (process.getValue().equals(clctfSourceTemp.getValue())) {
							clctfSourceTemp = process;
							break;
						}
					}
				}
				
				final CollectableField clctfSource = clctfSourceTemp;
				
				final CollectableField clctfTarget = CollectableUtils.copyCollectableField(clctfSource, clctefTarget, false);
				if (clctfTarget != null) {
					if (!clctcompmodelSource.isInitializing()) 
						clctcompmodelTarget.setField(clctfTarget, true, clctcompmodelEvent);
					else
						clctcompmodelTarget.setFieldInitial(clctfTarget);
				} else {
					if (clctfSource.isNull()) {
						// This is a special case we can always allow:
						if (!clctcompmodelSource.isInitializing()) 
							clctcompmodelTarget.clear();
						else
							clctcompmodelTarget.setFieldInitial(clctcompmodelTarget.getEntityField().getNullField());
					}
					else if (clctefTarget.isIdField() && !clctfSource.isIdField()) {
						// if the target field already has a valueId use this one
						if(clctcompmodelTarget.getField().getValueId() != null) {
							if (!clctcompmodelSource.isInitializing()) 
								clctcompmodelTarget.setField(new CollectableValueIdField(clctcompmodelTarget.getField().getValueId(), clctfSource.getValue()));
							else
								clctcompmodelTarget.setFieldInitial(new CollectableValueIdField(clctcompmodelTarget.getField().getValueId(), clctfSource.getValue()));
						} else {
							// if the source field does not have a value id and the target field requires a value id,
							// try to match the value:
							try {
								final Collection<CollectableComponent> collclctcompTarget = provider.getCollectableComponentsFor(clctefTarget.getUID());
								assert !clctfSource.isNull();
								
								final CollectableComponent clctcomp = collclctcompTarget.iterator().next();
								if (clctcomp instanceof LabeledCollectableComponentWithVLP) {
									final LabeledCollectableComponentWithVLP clctcmbbx = (LabeledCollectableComponentWithVLP) clctcomp;
									try {
										if (!clctcompmodelSource.isInitializing()) {
											clctcompmodelTarget.setField(CollectableUtils.findCollectableFieldByValue(clctcmbbx.getValueList(), clctfSource));
											
										} else {
											clctcompmodelTarget.setFieldInitial(CollectableUtils.findCollectableFieldByValue(clctcmbbx.getValueList(), clctfSource));
											
										}
									} catch (NoSuchElementException e) {
										if (clctcmbbx.isInsertable()) {
											if (!clctcompmodelSource.isInitializing()) {
												clctcompmodelTarget.setField(new CollectableValueIdField(null, clctfSource.getValue()));
												
											} else {
												clctcompmodelTarget.setFieldInitial(new CollectableValueIdField(null, clctfSource.getValue()));											
												
											}
										}
									}
								} else if (clctcomp instanceof CollectableListOfValues) {
									final CollectableListOfValues clctlov = (CollectableListOfValues) clctcomp;
									if (clctlov.isInsertable())
										if (!clctcompmodelSource.isInitializing()) 
											clctcompmodelTarget.setField(new CollectableValueIdField(null, clctfSource.getValue()));
										else
											clctcompmodelTarget.setFieldInitial(new CollectableValueIdField(null, clctfSource.getValue()));
								}
							}
							catch (Exception ex2) {
								throw new CommonFatalException(StringUtils.getParameterizedExceptionMessage("LayoutMLParser.6", clctefTarget.getUID()), ex2);
							}
						}
					} else {
						throw new CommonFatalException("Cannot transfer field " + clctefTarget.getUID());
					}
				}
			}
		}

		private static void transferLocalizedValues(CollectableComponentModel clctcompmodelSource, 
				CollectableComponentModel clctcompmodelTarget) {
			
			CollectableLocalizedComponentModel sourceModel = (CollectableLocalizedComponentModel) clctcompmodelSource;
			CollectableLocalizedComponentModel targetModel = (CollectableLocalizedComponentModel) clctcompmodelTarget;
			
			UID sourceFieldLangUID = DataLanguageUtils.extractFieldUID(clctcompmodelSource.getFieldUID());
			UID sourceFieldFlaggedLangUID = DataLanguageUtils.extractFlaggedFieldFromLangField(sourceFieldLangUID);
			UID targetFieldLangUID = DataLanguageUtils.extractFieldUID(clctcompmodelTarget.getFieldUID());
			UID targetFieldFlaggedLangUID = DataLanguageUtils.extractFlaggedFieldFromLangField(targetFieldLangUID);
			
			Long primKey = targetModel.getPrimaryKey();
			EntityMeta meta = MetaProvider.getInstance().getEntity(clctcompmodelTarget.getEntityField().getEntityUID());
			
			IDataLanguageMap mpTarget = targetModel.getDataLanguageMap();
			IDataLanguageMap mpSource = sourceModel.getDataLanguageMap();
			
			for (UID curLangUID : sourceModel.getDataLanguageMap().getLanguageMap().keySet()) {
				if (mpTarget.getDataLanguage(curLangUID) == null) {
					mpTarget.setDataLanguage(curLangUID, new DataLanguageLocalizedEntityEntry(
							new EntityMetaVO(meta,  true), primKey, curLangUID, new HashMap<UID, Object>()));
				}
				
				DataLanguageLocalizedEntityEntry dlTargetCurLanguage = mpTarget.getDataLanguage(curLangUID);	
				DataLanguageLocalizedEntityEntry dlSourceCurLanguage = mpSource.getDataLanguage(curLangUID);
				
				dlTargetCurLanguage.setFieldValue(targetFieldLangUID, dlSourceCurLanguage.getFieldValue(sourceFieldLangUID));
				dlTargetCurLanguage.setFieldValue(targetFieldFlaggedLangUID, dlSourceCurLanguage.getFieldValue(sourceFieldFlaggedLangUID));
				
				if (dlTargetCurLanguage.getPrimaryKey() == null) {
					dlTargetCurLanguage.flagNew();
				} else {
					dlTargetCurLanguage.flagUpdate();
				}
			}
		}
		
		/**
		 * Is set to null in {@link #close()} and thus cannot be final. (tp)
		 */
		private ComponentBuilderStack stack = new ComponentBuilderStack();

		/** @todo comment */
		private final ActionListener alButtons;
		
		/** @todo comment */
		private final LayoutMLParser layoutMLParser;

		/** @todo comment */
		private final CollectableFieldsProviderFactory valueListProviderFactory;

		/**
		 * the current rule, if any.
		 * @todo use stack?
		 */
		private Rule rule;

		/**
		 * the current subform column, if any.
		 * @todo use stack?
		 */
		private Column subformcolumn;
		
		/**
		 * the current matrix column, if any.
		 * @todo use stack?
		 */
		private JMatrixComponent.Column matrixColumn;
				
		/**
		 * the current valuelistprovider, if any.
		 * @todo use stack?
		 */
		private CollectableFieldsProvider valuelistprovider;

		/** the target (component) of the localization; if not set the current component builder is used.. */
		private LocalizationHandler localizationHandler;

		/** the current translations (language -> translation), if any. */
		private Map<String, String> translations;


		private final CollectableComponentFactory clctcompfactory;

		/**
		 * the root component
		 */
		private JComponent compRoot;

		/**
		 * the chars that are collected between start and end tags.
		 */
		private StringBuffer sbChars;

		/**
		 * the (optional) <code>CollectableEntity</code> that defines the structure for
		 * <code>CollectableFields</code> in the LayoutML
		 */
		private final CollectableEntity clcte;

		/**
		 * create searchable <code>CollectableComponent</code>s?
		 */
		private final boolean bCreateSearchableComponents;
		
		/**
		 * maps elements to <code>ElementProcessor</code>s
		 */
		private final Map<String, ElementProcessor> mpElementProcessors = new HashMap<String, ElementProcessor>();
		
		{
			mpElementProcessors.put(ELEMENT_PANEL, new PanelElementProcessor());
			mpElementProcessors.put(ELEMENT_EMPTYPANEL, new PanelElementProcessor());
			mpElementProcessors.put(ELEMENT_TABBEDPANE, new TabbedPaneElementProcessor());
			mpElementProcessors.put(ELEMENT_SCROLLPANE, new ScrollPaneElementProcessor());
			mpElementProcessors.put(ELEMENT_SPLITPANE, new SplitPaneElementProcessor());
			mpElementProcessors.put(ELEMENT_MATRIX, new MatrixElementProcessor());
			mpElementProcessors.put(ELEMENT_MATRIXCOLUMN, new MatrixColumnElementProcessor());
			mpElementProcessors.put(ELEMENT_SUBFORM, new SubFormElementProcessor());
			mpElementProcessors.put(ELEMENT_SUBFORMCOLUMN, new SubFormColumnElementProcessor());
			mpElementProcessors.put(ELEMENT_CHART, new ChartElementProcessor());
			mpElementProcessors.put(ELEMENT_LABEL, new LabelElementProcessor());
			mpElementProcessors.put(ELEMENT_IMAGE, new ImageElementProcessor());
			mpElementProcessors.put(ELEMENT_TEXTFIELD, new TextFieldElementProcessor());
			mpElementProcessors.put(ELEMENT_TEXTAREA, new TextAreaElementProcessor());
			mpElementProcessors.put(ELEMENT_TEXTMODULE, new TextModuleElementProcessor());
			mpElementProcessors.put(ELEMENT_COMBOBOX, new ComboBoxElementProcessor());
			mpElementProcessors.put(ELEMENT_BUTTON, new ButtonElementProcessor());
			mpElementProcessors.put(ELEMENT_COLLECTABLECOMPONENT, new CollectableComponentElementProcessor());
			mpElementProcessors.put(ELEMENT_DESCRIPTION, new DescriptionElementProcessor());
			mpElementProcessors.put(ELEMENT_BORDERLAYOUT, new BorderLayoutElementProcessor());
			mpElementProcessors.put(ELEMENT_FLOWLAYOUT, new FlowLayoutElementProcessor());
			mpElementProcessors.put(ELEMENT_GRIDLAYOUT, new GridLayoutElementProcessor());
			mpElementProcessors.put(ELEMENT_GRIDBAGLAYOUT, new GridBagLayoutElementProcessor());
			mpElementProcessors.put(ELEMENT_TABLELAYOUT, new TableLayoutElementProcessor());
			mpElementProcessors.put(ELEMENT_BOXLAYOUT, new BoxLayoutElementProcessor());
			mpElementProcessors.put(ELEMENT_ROWLAYOUT, new RowLayoutElementProcessor());
			mpElementProcessors.put(ELEMENT_COLUMNLAYOUT, new ColumnLayoutElementProcessor());
			mpElementProcessors.put(ELEMENT_BORDERLAYOUTCONSTRAINTS, new BorderLayoutConstraintsElementProcessor());
			mpElementProcessors.put(ELEMENT_TABBEDPANECONSTRAINTS, new TabbedPaneConstraintsElementProcessor());
			mpElementProcessors.put(ELEMENT_SPLITPANECONSTRAINTS, new SplitPaneConstraintsElementProcessor());
			mpElementProcessors.put(ELEMENT_GRIDBAGCONSTRAINTS, new GridBagConstraintsElementProcessor());
			mpElementProcessors.put(ELEMENT_TABLELAYOUTCONSTRAINTS, new TableLayoutConstraintsElementProcessor());
			mpElementProcessors.put(ELEMENT_MINIMUMSIZE, new MinimumSizeElementProcessor());
			mpElementProcessors.put(ELEMENT_PREFERREDSIZE, new PreferredSizeElementProcessor());
			mpElementProcessors.put(ELEMENT_STRICTSIZE, new StrictSizeElementProcessor());
			mpElementProcessors.put(ELEMENT_CLEARBORDER, new ClearBorderElementProcessor());
			mpElementProcessors.put(ELEMENT_EMPTYBORDER, new EmptyBorderElementProcessor());
			mpElementProcessors.put(ELEMENT_ETCHEDBORDER, new EtchedBorderElementProcessor());
			mpElementProcessors.put(ELEMENT_BEVELBORDER, new BevelBorderElementProcessor());
			mpElementProcessors.put(ELEMENT_LINEBORDER, new LineBorderElementProcessor());
			mpElementProcessors.put(ELEMENT_TITLEDBORDER, new TitledBorderElementProcessor());
			mpElementProcessors.put(ELEMENT_FONT, new FontElementProcessor());
			mpElementProcessors.put(ELEMENT_TEXTFORMAT, new TextFormatElementProcessor());
			mpElementProcessors.put(ELEMENT_TEXTCOLOR, new TextColorElementProcessor());
			mpElementProcessors.put(ELEMENT_BACKGROUND, new BackgroundElementProcessor());
			mpElementProcessors.put(ELEMENT_SEPARATOR, new SeparatorElementProcessor());
			mpElementProcessors.put(ELEMENT_TITLEDSEPARATOR, new TitledSeparatorElementProcessor());
			mpElementProcessors.put(ELEMENT_OPTIONS, new OptionsElementProcessor());
			mpElementProcessors.put(ELEMENT_OPTION, new OptionElementProcessor());
			mpElementProcessors.put(ELEMENT_RULE, new RuleElementProcessor());
			mpElementProcessors.put(ELEMENT_EVENT, new EventElementProcessor());
			mpElementProcessors.put(ELEMENT_TRANSFERLOOKEDUPVALUE, new TransferLookedUpValueElementProcessor());
			mpElementProcessors.put(ELEMENT_CLEAR, new ClearElementProcessor());
			mpElementProcessors.put(ELEMENT_REINIT_SUBFORM, new ReinitSubformElementProcessor());
			mpElementProcessors.put(ELEMENT_REFRESHVALUELIST, new RefreshValueListElementProcessor());
			mpElementProcessors.put(ELEMENT_DEPENDENCY, new DependencyElementProcessor());
			mpElementProcessors.put(ELEMENT_VALUELISTPROVIDER, new ValueListProviderElementProcessor());
			mpElementProcessors.put(ELEMENT_PARAMETER, new ParameterElementProcessor());

			mpElementProcessors.put(ELEMENT_PROPERTY, new PropertyElementProcessor());
			mpElementProcessors.put(ELEMENT_PROPERTY_SIZE, new PropertySizeElementProcessor());
			mpElementProcessors.put(ELEMENT_PROPERTY_COLOR, new PropertyColorElementProcessor());
			mpElementProcessors.put(ELEMENT_PROPERTY_FONT, new PropertyFontElementProcessor());
			mpElementProcessors.put(ELEMENT_PROPERTY_SCRIPT, new PropertyScriptElementProcessor());
			mpElementProcessors.put(ELEMENT_PROPERTY_TRANSLATIONS, new PropertyTranslationsElementProcessor());
			mpElementProcessors.put(ELEMENT_PROPERTY_VALUELIST_PROVIDER, new PropertyValuelistProviderElementProcessor());

			mpElementProcessors.put(ELEMENT_INITIALFOCUSCOMPONENT, new InitialFocusComponentProcessor());
			mpElementProcessors.put(ELEMENT_INITIALSORTINGORDER, new InitialSortingOrderProcessor());
			mpElementProcessors.put(ELEMENT_TRANSLATIONS, new TranslationsElementProcessor());
			mpElementProcessors.put(ELEMENT_TRANSLATION, new TranslationElementProcessor());
			mpElementProcessors.put(ELEMENT_LAYOUTCOMPONENT, new LayoutComponentElementProcessor());
			mpElementProcessors.put(ELEMENT_WEBADDON, new WebAddonElementProcessor());

			mpElementProcessors.put(ELEMENT_ENABLED, new ScriptElementProcessor());
			mpElementProcessors.put(ELEMENT_NEW_ENABLED, new ScriptElementProcessor());
			mpElementProcessors.put(ELEMENT_EDIT_ENABLED, new ScriptElementProcessor());
			mpElementProcessors.put(ELEMENT_DELETE_ENABLED, new ScriptElementProcessor());
			mpElementProcessors.put(ELEMENT_CLONE_ENABLED, new ScriptElementProcessor());
			mpElementProcessors.put(ELEMENT_DYNAMIC_ROW_COLOR, new ScriptElementProcessor());
		}

		/**
		 * maps field names to (multiple) <code>CollectableComponent</code>s
		 */
		private final DefaultCollectableComponentsProvider clctcompprovider = new DefaultCollectableComponentsProvider();
		
		/**
		 * maps field names to (multiple) {@link LayoutComponent}
		 */
		private final DefaultLayoutComponentsProvider layoutComponentsProvider = new DefaultLayoutComponentsProvider();

		/**
		 * maps field names to <code>CollectableComponentModel</code>s.
		 */
		private final Map<UID, CollectableComponentModel> mpclctcompmodel = new HashMap<UID, CollectableComponentModel>();

		/**
		 * contains the parsed dependencies
		 */
		private final MultiListMap<UID, UID> mmpDependencies = new MultiListHashMap<UID, UID>();

		/**
		 * maps XML attribute values (e.g. <code>ATTRIBUTEVALUE_TEXTFIELD</code>)
		 * to control types (e.g. <code>TYPE_TEXTFIELD</code>)
		 */
		private static final Map<String, Integer> mpEnumeratedControlTypes = new HashMap<String, Integer>();

		private static final Map<String, Integer> mpFlowLayoutConstants = new HashMap<String, Integer>(5);
		private static final Map<String, String> mpBorderLayoutConstraints = new HashMap<String, String>(5);
		private static final Map<String, Integer> mpTabLayoutPolicies = new HashMap<String, Integer>(2);
		private static final Map<String, Integer> mpTabPlacementConstants = new HashMap<String, Integer>(4);
		private static final Map<String, Integer> mpGridBagConstraintAnchor = new HashMap<String, Integer>(10);
		private static final Map<String, Integer> mpGridBagConstraintFill = new HashMap<String, Integer>(4);
		private static final Map<String, Integer> mpHorizontalScrollBarPolicies = new HashMap<String, Integer>(3);
		private static final Map<String, Integer> mpVerticalScrollBarPolicies = new HashMap<String, Integer>(3);
		private static final Map<String, String> mpSplitPaneConstraints = new HashMap<String, String>(4);
		private static final Map<String, Integer> mpSplitPaneOrientation = new HashMap<String, Integer>(2);
		private static final Map<String, Integer> mpLineLayoutOrientation = new HashMap<String, Integer>(2);
		private static final Map<String, Integer> mpSeparatorOrientation = new HashMap<String, Integer>(2);
		private static final Map<String, Integer> mpMatrixLayoutPolicies = new HashMap<String, Integer>(3);
		
		static {
			// control types:
			mpEnumeratedControlTypes.put(ATTRIBUTEVALUE_TEXTFIELD, CollectableComponentTypes.TYPE_TEXTFIELD);
			mpEnumeratedControlTypes.put(ATTRIBUTEVALUE_IDTEXTFIELD, CollectableComponentTypes.TYPE_IDTEXTFIELD);
			mpEnumeratedControlTypes.put(ATTRIBUTEVALUE_TEXTAREA, CollectableComponentTypes.TYPE_TEXTAREA);
			mpEnumeratedControlTypes.put(ATTRIBUTEVALUE_COMBOBOX, CollectableComponentTypes.TYPE_COMBOBOX);
			mpEnumeratedControlTypes.put(ATTRIBUTEVALUE_CHECKBOX, CollectableComponentTypes.TYPE_CHECKBOX);
			mpEnumeratedControlTypes.put(ATTRIBUTEVALUE_DATECHOOSER, CollectableComponentTypes.TYPE_DATECHOOSER);
			mpEnumeratedControlTypes.put(ATTRIBUTEVALUE_HYPERLINK, CollectableComponentTypes.TYPE_HYPERLINK);
			mpEnumeratedControlTypes.put(ATTRIBUTEVALUE_EMAIL, CollectableComponentTypes.TYPE_EMAIL);
			mpEnumeratedControlTypes.put(ATTRIBUTEVALUE_PHONENUMBER, CollectableComponentTypes.TYPE_PHONENUMBER);
			mpEnumeratedControlTypes.put(ATTRIBUTEVALUE_OPTIONGROUP, CollectableComponentTypes.TYPE_OPTIONGROUP);
			mpEnumeratedControlTypes.put(ATTRIBUTEVALUE_LISTOFVALUES, CollectableComponentTypes.TYPE_LISTOFVALUES);
			mpEnumeratedControlTypes.put(ATTRIBUTEVALUE_FILECHOOSER, CollectableComponentTypes.TYPE_FILECHOOSER);
			mpEnumeratedControlTypes.put(ATTRIBUTEVALUE_IMAGE, CollectableComponentTypes.TYPE_IMAGE);
			//NUCLEUSINT-1142
			mpEnumeratedControlTypes.put(ATTRIBUTEVALUE_PASSWORDFIELD, CollectableComponentTypes.TYPE_PASSWORDFIELD);

			// flow layoutml constants:
			mpFlowLayoutConstants.put("left", FlowLayout.LEFT);
			mpFlowLayoutConstants.put("right", FlowLayout.RIGHT);
			mpFlowLayoutConstants.put("leading", FlowLayout.LEADING);
			mpFlowLayoutConstants.put("trailing", FlowLayout.TRAILING);
			mpFlowLayoutConstants.put("center", FlowLayout.CENTER);

			// border layout constants:
			mpBorderLayoutConstraints.put("north", BorderLayout.NORTH);
			mpBorderLayoutConstraints.put("south", BorderLayout.SOUTH);
			mpBorderLayoutConstraints.put("west", BorderLayout.WEST);
			mpBorderLayoutConstraints.put("east", BorderLayout.EAST);
			mpBorderLayoutConstraints.put("center", BorderLayout.CENTER);

			// tab layoutml policies:
			mpTabLayoutPolicies.put("scroll", JTabbedPane.SCROLL_TAB_LAYOUT);
			mpTabLayoutPolicies.put("wrap", JTabbedPane.WRAP_TAB_LAYOUT);

			// tab placement constants:
			mpTabPlacementConstants.put("top", JTabbedPane.TOP);
			mpTabPlacementConstants.put("bottom", JTabbedPane.BOTTOM);
			mpTabPlacementConstants.put("left", JTabbedPane.LEFT);
			mpTabPlacementConstants.put("right", JTabbedPane.RIGHT);

			// horizontal scrollbar policies:
			mpHorizontalScrollBarPolicies.put("always", JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
			mpHorizontalScrollBarPolicies.put("never", JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
			mpHorizontalScrollBarPolicies.put("asneeded", JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

			// vertical scrollbar policies:
			mpVerticalScrollBarPolicies.put("always", JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
			mpVerticalScrollBarPolicies.put("never", JScrollPane.VERTICAL_SCROLLBAR_NEVER);
			mpVerticalScrollBarPolicies.put("asneeded", JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

			// constants for GridBagConstraints "anchor" parameter (absolute values only):
			mpGridBagConstraintAnchor.put("north", GridBagConstraints.NORTH);
			mpGridBagConstraintAnchor.put("northeast", GridBagConstraints.NORTHEAST);
			mpGridBagConstraintAnchor.put("east", GridBagConstraints.EAST);
			mpGridBagConstraintAnchor.put("southeast", GridBagConstraints.SOUTHEAST);
			mpGridBagConstraintAnchor.put("south", GridBagConstraints.SOUTH);
			mpGridBagConstraintAnchor.put("southwest", GridBagConstraints.SOUTHWEST);
			mpGridBagConstraintAnchor.put("west", GridBagConstraints.WEST);
			mpGridBagConstraintAnchor.put("northwest", GridBagConstraints.NORTHWEST);
			mpGridBagConstraintAnchor.put("center", GridBagConstraints.CENTER);

			// constants for GridBagConstraints "fill" parameter:
			mpGridBagConstraintFill.put("none", GridBagConstraints.NONE);
			mpGridBagConstraintFill.put("horizontal", GridBagConstraints.HORIZONTAL);
			mpGridBagConstraintFill.put("vertical", GridBagConstraints.VERTICAL);
			mpGridBagConstraintFill.put("both", GridBagConstraints.BOTH);

			mpSplitPaneConstraints.put("top", JSplitPane.TOP);
			mpSplitPaneConstraints.put("bottom", JSplitPane.BOTTOM);
			mpSplitPaneConstraints.put("left", JSplitPane.LEFT);
			mpSplitPaneConstraints.put("right", JSplitPane.RIGHT);

			mpSplitPaneOrientation.put("horizontal", JSplitPane.HORIZONTAL_SPLIT);
			mpSplitPaneOrientation.put("vertical", JSplitPane.VERTICAL_SPLIT);

			// constants for LineLayout "orientation" parameter:
			mpLineLayoutOrientation.put("horizontal", LineLayout.HORIZONTAL);
			mpLineLayoutOrientation.put("vertical", LineLayout.VERTICAL);
			
			mpMatrixLayoutPolicies.put("checkicion", 1);
			mpMatrixLayoutPolicies.put("textfield", 2);
			mpMatrixLayoutPolicies.put("combobox", 3);
		}

		private EntityAndField eafnInitialFocus;

		private UID sInitialSortingColumnName;
		private String sInitialSortingOrder;
		private boolean bExistingBo;
		private final UID layoutUID;

		/**
		 * initializes the maps.
		 * 
		 * §precondition clcte != null
		 * 
		 * @param clcte the optional <code>CollectableEntity</code> that defines the structure of
		 * collectable-component elements
		 * @param bCreateSearchableComponents
		 * @param alButtons the common <code>ActionListener</code>, if any, for all button elements.
		 * @param valueListProviderFactory
		 */
		private BuildFormHandler(UID layoutUID, CollectableEntity clcte, LayoutMLParser layoutMLParser, boolean bCreateSearchableComponents, boolean bExistingBo,
				ActionListener alButtons, CollectableComponentFactory collectableComponentFactory, CollectableFieldsProviderFactory valueListProviderFactory) {
			this.layoutUID = layoutUID;
			this.clcte = clcte;
			this.layoutMLParser = layoutMLParser;
			
			this.bCreateSearchableComponents = bCreateSearchableComponents;
			this.bExistingBo = bExistingBo;
			this.alButtons = alButtons;
			this.clctcompfactory = collectableComponentFactory;
			this.valueListProviderFactory = valueListProviderFactory;
		}

		private UID getFieldUIDFromCollectableEntity(String sFieldName) {
			return layoutMLParser.getFieldUID(sFieldName);
		}
		
		@Override
		public void close() {
			stack = null;
		}
		
		/**
		 * event: start document. Called by the underlying SAX parser.
		 * @throws SAXException
		 */
		@Override
		public void startDocument() throws SAXException {
			this.compRoot = null;
		}

		/**
		 * event: end document. Called by the underlying SAX parser.
		 * @throws SAXException
		 */
		@Override
		public void endDocument() throws SAXException {
			this.sbChars = null;

			this.setDefaultValueListProviders();
			try {
				this.refreshValueListsComponentsWithVLP();
			}
			catch (CommonBusinessException ex) {
				throw new SAXException("LayoutMLParser.9", ex);//"Beim F\u00fcllen der Dropdowns ist ein Fehler aufgetreten.", ex);
			}
		}

		/**
		 * sets the default value list provider for all comboboxes that don't have a provider yet.
		 */
		private void setDefaultValueListProviders() {
			for (CollectableComponent clctcomp : this.getCollectableComponentsProvider().getCollectableComponents()) {
				if (clctcomp instanceof LabeledCollectableComponentWithVLP) {
					final LabeledCollectableComponentWithVLP clctWithVLP = (LabeledCollectableComponentWithVLP) clctcomp;
					final UID sEntity = BuildFormHandler.this.getCollectableEntity().getUID();
					assert sEntity != null;
					if (clctWithVLP.getValueListProvider() == null) {
						clctWithVLP.setValueListProvider(this.valueListProviderFactory.newDefaultCollectableFieldsProvider(clctcomp.getFieldUID()));
					}
				} else {
					/*if (clctcomp instanceof DelegatingCollectableComboBox) {
						final DelegatingCollectableComboBox clctcmbbx = (DelegatingCollectableComboBox) clctcomp;
						final String sEntity = BuildFormHandler.this.getCollectableEntity().getName();
						assert sEntity != null;
						if (clctcmbbx.getValueListProvider() == null) {
							clctcmbbx.setValueListProvider(this.valueListProviderFactory.newDefaultCollectableFieldsProvider(sEntity, clctcomp.getFieldName()));
						}
					} else {*/
						if (clctcomp instanceof DelegatingCollectablePanel) {
							final DelegatingCollectablePanel clctpnl = (DelegatingCollectablePanel) clctcomp;
							final UID sEntity = BuildFormHandler.this.getCollectableEntity().getUID();
							assert sEntity != null;
							if (clctpnl.needValueListProvider() && clctpnl.getValueListProvider() == null) {
								clctpnl.setValueListProvider(this.valueListProviderFactory.newDefaultCollectableFieldsProvider(clctcomp.getFieldUID()));
							}
						}
					//}
				}
			}
		}

		private void refreshValueListsComponentsWithVLP() throws CommonBusinessException {
			for (CollectableComponent clctcomp : this.getCollectableComponentsProvider().getCollectableComponents()) {
				if (clctcomp instanceof LabeledCollectableComponentWithVLP) {
					final LabeledCollectableComponentWithVLP clctcmbbx = (LabeledCollectableComponentWithVLP) clctcomp;
					// RSWORGA-254 dirty flag is set even when the view is initializing.
					// apply isInitializing as initial value
					clctcmbbx.refreshValueList(true, !bExistingBo, true);
				} else {
					if (clctcomp instanceof DelegatingCollectablePanel) {
						final DelegatingCollectablePanel clctpnl = (DelegatingCollectablePanel) clctcomp;
						if (clctpnl.needValueListProvider()) {
							clctpnl.refreshValueList();
						}
					}
				}
			}
		}

		/**
		 * §postcondition result != null
		 * 
		 * @return the (optional) <code>CollectableEntity</code> that defines the structure of
		 * collectable-component elements used in the parsed LayoutML document
		 */
		CollectableEntity getCollectableEntity() {
			return this.clcte;
		}

		/**
		 * @return the root component constructed from the LayoutML
		 */
		JComponent getRootComponent() {
			return this.compRoot;
		}

		/**
		 * provides access to the map of collectable components
		 * @return the map of parsed/constructed collectable components.
		 * Maps field names to <code>CollectableComponent</code>s
		 */
		DefaultCollectableComponentsProvider getCollectableComponentsProvider() {
			return this.clctcompprovider;
		}
		
		/**
		 * provides access to the map of {@link LayoutComponent}
		 * @return the map of parsed/constructed {@link LayoutComponent}
		 * Maps field names to {@link LayoutComponent}
		 */
		DefaultLayoutComponentsProvider getLayoutComponentsProvider() {
			return this.layoutComponentsProvider;
		}

		Map<UID, CollectableComponentModel> getMapOfCollectableComponentModels() {
			return this.mpclctcompmodel;
		}

		/**
		 * @return the entity name and field name of the component, if any, that is to get the focus initially.
		 */
		public EntityAndField getInitialFocusEntityAndField() {
			return this.eafnInitialFocus;
		}

		/**
		 * @return the map of inter-field dependencies
		 */
		MultiListMap<UID, UID> getMultiMapOfDependencies() {
			return this.mmpDependencies;
		}

		/**
		 * @param sControlType
		 * @param sControlTypeClass
		 * @return the CollectableComponentType for the given parameters.
		 * @throws SAXException
		 */
		private CollectableComponentType getCollectableComponentType(String sControlType, String sControlTypeClass)
				throws SAXException {
			final Integer iEnumeratedControlType;
			final Class<CollectableComponent> clsclctcomp;
			if (sControlTypeClass == null) {
				iEnumeratedControlType = mpEnumeratedControlTypes.get(sControlType);
				clsclctcomp = null;
			}
			else {
				if (sControlType != null) {
					throw new SAXException("LayoutMLParser.10");//"Die Attribute controltype und controltypeclass k\u00f6nnen nicht gleichzeitig angegeben werden.");
				}
				try {
					iEnumeratedControlType = null;
					/** @todo explicitly check that the class implements CollectableComponent */
					clsclctcomp = (Class<CollectableComponent>) Class.forName(sControlTypeClass);
							LangUtils.getClassLoaderThatWorksForWebStart().loadClass(sControlTypeClass);
				}
				catch (ClassNotFoundException ex) {
					final String sMessage = StringUtils.getParameterizedExceptionMessage("LayoutMLParser.11", sControlTypeClass);//"Unbekannte Klasse: " + sControlTypeClass;
					throw new SAXException(sMessage, ex);
				}
			}
			return new CollectableComponentType(iEnumeratedControlType, clsclctcomp);
		}

		/**
		 * Handles stored resource ids for legacy reasons. The localization target
		 * must already be set.
		 */
		private void handleLegacyResourceId(Attributes attributes) {
			final String resourceId = attributes.getValue(ATTRIBUTE_LOCALERESOURCEID);
			if (resourceId != null) {
				String text = SpringLocaleDelegate.getInstance().getTextForStaticLabel(resourceId);
				getStaticLocalizationHandler().setTranslation(text);
			}
		}

		private LocalizationHandler getStaticLocalizationHandler() {
			if (localizationHandler != null)
				return localizationHandler;
			return stack.peekComponentBuilder();
		}

		/**
		 * adds a border to the top component in the stack. The old border, if any, is preserved
		 * as inner border.
		 * @param borderNew
		 */
		private void addBorder(final Border borderNew) {
			final JComponent comp = stack.peekComponent();
			final Border borderOld = comp.getBorder();
			if (borderOld == null) {
				comp.setBorder(borderNew);
			}
			else {
				comp.setBorder(BorderFactory.createCompoundBorder(borderNew, borderOld));
			}
		}

		/**
		 * adds a dependency to the multimap of dependencies.
		 * @param sDependantFieldName
		 * @param sDependsOnFieldName
		 */
		private void addDependency(final UID sDependantFieldName, final UID sDependsOnFieldName) {
			/** @todo Do we have to check redundant/contradictory entries? */
			BuildFormHandler.this.mmpDependencies.addValue(sDependantFieldName, sDependsOnFieldName);
		}

		/**
		 * inner interface <code>ElementProcessor</code>. Processes an XML element.
		 */
		private interface ElementProcessor {
			/**
			 * is called when a start element event occurs in the underlying SAX parser.
			 * @param sUriNameSpace
			 * @param sSimpleName
			 * @param sQualifiedName
			 * @param attributes the XML attributes of this element
			 */
			void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException;

			/**
			 * called when an end element event occurs in the underlying SAX parser
			 * @param sUriNameSpace
			 * @param sSimpleName
			 * @param sQualifiedName
			 */
			void endElement(String sUriNameSpace, String sSimpleName, String sQualifiedName) throws SAXException;
		}

		/**
		 * inner class <code>AbstractElementProcessor</code>. Useful for the most element processors.
		 */
		private abstract class AbstractElementProcessor implements ElementProcessor {
			/**
			 * default action for most element processors: do nothing on end element events.
			 * @param sUriNameSpace
			 * @param sSimpleName
			 * @param sQualifiedName
			 */
			@Override
            public void endElement(String sUriNameSpace, String sSimpleName, String sQualifiedName) {
				// do nothing
			}
		}

		/**
		 * inner class <code>ComponentElementProcessor</code>.
		 * Useful for element processors that contain a component.
		 */
		private abstract class ComponentElementProcessor implements ElementProcessor {
			/**
			 * default action for component processors: <code>finishComponent()</code>
			 * @param sUriNameSpace
			 * @param sSimpleName
			 * @param sQualifiedName
			 */
			@Override
            public void endElement(String sUriNameSpace, String sSimpleName, String sQualifiedName) throws SAXException {
				compRoot = stack.finishComponent();
			}
		}

		/**
		 * inner class <code>CollectableComponentElementProcessor</code>.
		 */
		private class CollectableComponentElementProcessor extends ComponentElementProcessor {
			/**
			 * <ul>
			 *   <li>constructs a <code>CollectableComponent</code></li>
			 *   <li>adds it to the map of collectable components</li>
			 *   <li>configures it according to the XML attributes</li>
			 *   <li>and pushes it on the stack.</li>
			 * </ul>
			 * @param sUriNameSpace
			 * @param sSimpleName
			 * @param sQualifiedName
			 * @param attributes
			 */
			@Override
			public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				final String sFieldName = attributes.getValue(ATTRIBUTE_NAME);
				final UID fieldUID = getFieldUIDFromCollectableEntity(sFieldName);
				
				final String sControlType = attributes.getValue(ATTRIBUTE_CONTROLTYPE);
				final String sControlTypeClass = attributes.getValue(ATTRIBUTE_CONTROLTYPECLASS);

				final CollectableEntity clcte = BuildFormHandler.this.getCollectableEntity();
				final CollectableEntityField clctef = clcte.getEntityField(fieldUID);

				final CollectableComponentType clctcomptype =
						BuildFormHandler.this.getCollectableComponentType(sControlType, sControlTypeClass);

				
				final CollectableComponent clctcomp = clctcompfactory.newCollectableComponent(clctef,
						clctcomptype, bCreateSearchableComponents);

				/** @todo add preferences to clctcomp */

				// If there is a CollectableComponentModel already with this fieldname, reuse it (share it):
				final CollectableComponentModel clctcompmodel = BuildFormHandler.this.mpclctcompmodel.get(fieldUID);
				if (clctcompmodel != null) {
						clctcomp.setModel(clctcompmodel);
				}

				BuildFormHandler.this.mpclctcompmodel.put(fieldUID, clctcomp.getModel());

				//NUCLEUSINT-442
				if ((!ATTRIBUTE_CONTROLTYPE.equals(ATTRIBUTEVALUE_TEXTFIELD)) && (!ATTRIBUTEVALUE_LABEL.equals(attributes.getValue(ATTRIBUTE_SHOWONLY)))) {
					BuildFormHandler.this.clctcompprovider.addCollectableComponent(clctcomp);
				} else {
					BuildFormHandler.this.clctcompprovider.addCollectableLabel(clctcomp);
				}
					
				// enabled:
				boolean bEnabled = false;
				final String sEnabled = attributes.getValue(ATTRIBUTE_ENABLED);
				if (sEnabled != null) {
					// override default:
					bEnabled = sEnabled.equals(ATTRIBUTEVALUE_YES);
					clctcomp.setEnabled(bEnabled);
				}
				
				// Id text fields may never be enabled except in search mode!
				if (clctcomp instanceof CollectableIdTextField && !BuildFormHandler.this.bCreateSearchableComponents) {
					clctcomp.setEnabled(false);
				}
				
				// cloneable:
				boolean bCloneable = true;
				final String sNotCloneable = attributes.getValue(ATTRIBUTE_NOT_CLONEABLE);
				if (sNotCloneable != null && !bCreateSearchableComponents) {
					// override default:
					bCloneable = !sNotCloneable.equals(ATTRIBUTEVALUE_YES);
				}
				clctcomp.setCloneable(bCloneable);
				
				// multieditable:
				String sDefaultMultieditable = ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_DEFAULT_FIELD_MULTIEDITING);
				boolean bMultieditable = Boolean.parseBoolean(StringUtils.defaultIfNull(sDefaultMultieditable, "true"));
				final String sMultieditable = attributes.getValue(ATTRIBUTE_MULTIEDITABLE);
				if (sMultieditable != null && !bCreateSearchableComponents) {
					// override default:
					bMultieditable = sMultieditable.equals(ATTRIBUTEVALUE_YES);
				}

				// insertable:
				// The default setting is taken care of by the CollectableComponentFactory.
				// It can be overridden in the LayoutML though.
				final String sInsertable = attributes.getValue(ATTRIBUTE_INSERTABLE);
				if (!BuildFormHandler.this.bCreateSearchableComponents && sInsertable != null) {
					// override:
					// NUCLOSINT-442 only disable, but never allow to insert a new value! We need a new concept for insertable...
					clctcomp.setInsertable(bEnabled && sInsertable.equals(ATTRIBUTEVALUE_YES));
					if (sInsertable.equals(ATTRIBUTEVALUE_NO)) {
						clctcomp.setInsertable(false);
					}
				}

				// multiselect:
				final String sMultiSelect = attributes.getValue(ATTRIBUTE_MULTISELECT);
				if (BuildFormHandler.this.bCreateSearchableComponents && sMultiSelect != null) {
					clctcomp.setMultiSelect(sMultiSelect.equals(ATTRIBUTEVALUE_YES));
				}
				
				//LOV-stuff
				if (clctcomp instanceof CollectableListOfValues) {
					CollectableListOfValues clov = (CollectableListOfValues) clctcomp;

					final String sLOVBtn = attributes.getValue(ATTRIBUTE_LOV_BUTTON);
					if (sLOVBtn != null) {
						clov.setLovBtn(sLOVBtn.equals(ATTRIBUTEVALUE_YES));
					}
					
					final String sLOVSearch = attributes.getValue(ATTRIBUTE_LOV_SEARCHMASK);
					if (sLOVSearch != null) {
						clov.setLovSearch(sLOVSearch.equals(ATTRIBUTEVALUE_YES));
					}

					final String sDropDownDtn = attributes.getValue(ATTRIBUTE_DROPDOWN_BUTTON);
					if (sDropDownDtn != null) {
						clov.setDropdownBtn(sDropDownDtn.equals(ATTRIBUTEVALUE_YES));
					}
				}

				final String sScalable = attributes.getValue(ATTRIBUTE_SCALABLE);
				if(sScalable != null) {
					if(sScalable.equals(ATTRIBUTEVALUE_YES)) {
						clctcomp.setScalable(true);
					}
				}

				final String sKeepAspectRatio = attributes.getValue(ATTRIBUTE_ASPECTRATIO);
				if(sKeepAspectRatio != null) {
					if(sKeepAspectRatio.equals(ATTRIBUTEVALUE_YES)) {
						clctcomp.setKeepAspectRatio(true);
					}
				}

				// next focus field
				final String sNextFocusField = attributes.getValue(ATTRIBUTE_NEXTFOCUSFIELD);
				if(sNextFocusField != null) {
					clctcomp.setNextFocusComponent(sNextFocusField);
					clctcomp.getControlComponent().putClientProperty(ATTRIBUTE_NEXTFOCUSFIELD, sNextFocusField);
					clctcomp.getJComponent().putClientProperty(ATTRIBUTE_NEXTFOCUSFIELD, sNextFocusField);
				}
				
				// next focus component
				final String sNextFocusComponent = attributes.getValue(ATTRIBUTE_NEXTFOCUSCOMPONENT);
				if(sNextFocusComponent != null) {
					clctcomp.setNextFocusComponent(sNextFocusComponent);
					clctcomp.getControlComponent().putClientProperty(ATTRIBUTE_NEXTFOCUSCOMPONENT, sNextFocusComponent);
					clctcomp.getJComponent().putClientProperty(ATTRIBUTE_NEXTFOCUSCOMPONENT, sNextFocusComponent);
				}

				// visible:
				boolean bVisible = true;
				final String sVisible = attributes.getValue(ATTRIBUTE_VISIBLE);
				if (sVisible != null) {
					// override default:
					bVisible = sVisible.equals(ATTRIBUTEVALUE_YES);
					clctcomp.setVisible(bVisible);
				}

				boolean bHideLabel = false;
				if (!bVisible) {
					clctcomp.setIsHidden(true);
				}
				if (bVisible) {
					// show-only:
					boolean bHideControl = false;
					boolean bShowBrowseButtonOnly = false;
					final String sShowOnly = attributes.getValue(ATTRIBUTE_SHOWONLY);
					if (sShowOnly != null) {
						if (sShowOnly.equals(ATTRIBUTEVALUE_LABEL)) {
							bHideControl = true;
						}
						else if (sShowOnly.equals(ATTRIBUTEVALUE_CONTROL)) {
							bHideLabel = true;
						}
						else if (sShowOnly.equals(ATTRIBUTEVALUE_BROWSEBUTTON)) {
							bShowBrowseButtonOnly = true;
							bHideLabel = true;
						}
					}
					if (clctcomp instanceof LabeledCollectableComponent) {
						final LabeledComponent labcomp = ((LabeledCollectableComponent) clctcomp).getLabeledComponent();
						labcomp.getJLabel().setVisible(!bHideLabel);
						labcomp.getControlComponent().setVisible(!bHideControl);

						if (bShowBrowseButtonOnly && (clctcomp instanceof CollectableListOfValues)) {
							final CollectableListOfValues clctlov = (CollectableListOfValues) clctcomp;
							clctlov.setBrowseButtonVisibleOnly(true);
						}
					}

					if (clctcomp instanceof DelegatingCollectablePanel) {
						final DelegatingCollectablePanel delpnl = (DelegatingCollectablePanel) clctcomp;
						delpnl.setVisibleLabel(!bHideLabel);
						delpnl.setVisibleControl(!bHideControl);
					}
				}	// if (bVisible)

				final boolean hasLabel = !bHideLabel;
				if (hasLabel) {
					String sLabel = attributes.getValue(ATTRIBUTE_LABEL);
					if (sLabel == null) {
						sLabel = clctef.getLabel();
					} 
					clctcomp.setLabelText(sLabel);
				}
				localizationHandler = new LocalizationHandler() {
					@Override
					public void setTranslation(String text) {
						if (hasLabel)
							clctcomp.setLabelText(text);
					}
				};

				// fill-control-horizontally:
				final String sFillControlHorizontally = attributes.getValue(ATTRIBUTE_FILLCONTROLHORIZONTALLY);
				if (sFillControlHorizontally != null) {
					clctcomp.setFillControlHorizontally(sFillControlHorizontally.equals(ATTRIBUTEVALUE_YES));
				}

				// rows:
				final Integer iRows = getIntegerValue(attributes, ATTRIBUTE_ROWS);
				if (iRows != null) {
					clctcomp.setRows(iRows);
				}

				// columns:
				Integer iColumns = getIntegerValue(attributes, ATTRIBUTE_COLUMNS);
				if (iColumns == null) {
					iColumns = clctef.getMaxLength();
				}
				if (iColumns != null) {
					clctcomp.setColumns(iColumns);
				}

				// mnemonic:
				final String sMnemonic = attributes.getValue(ATTRIBUTE_MNEMONIC);
				if (sMnemonic != null && sMnemonic.length() > 0) {
					clctcomp.setMnemonic(sMnemonic.charAt(0));
				}

				
				clctcomp.setToolTipText(clctef.getDescription());
				// set default tooltip from the field's description
				// may be overwritten by LayoutML description element

				// opaqueness / transparency:
				final String sOpaque = attributes.getValue(ATTRIBUTE_OPAQUE);
				if (sOpaque != null) {
					final boolean bOpaque = sOpaque.equals(ATTRIBUTEVALUE_YES);
					clctcomp.setOpaque(bOpaque);
				}

				// custom usage for search layout
				final String customUsageSearch = attributes.getValue(ATTRIBUTE_CUSTOM_USAGE_SEARCH);
				clctcomp.setCustomUsageSearch(customUsageSearch);

//				// constraining entity:
//				final String sConstrainingEntity = attributes.getValue(ATTRIBUTE_CONSTRAININGENTITY);
//				clctcomp.setConstrainingEntity(sConstrainingEntity);

				stack.addCollectableComponent(clctcomp);

				// The component that is parsed first gets the initial focus by default:
				if (BuildFormHandler.this.eafnInitialFocus == null) {
					// only if the initial focus hasn't been set already:
					if ((!ATTRIBUTE_CONTROLTYPE.equals(ATTRIBUTEVALUE_TEXTFIELD)) && (!ATTRIBUTEVALUE_LABEL.equals(attributes.getValue(ATTRIBUTE_SHOWONLY)))) {
						//NUCLEUSINT-442
						BuildFormHandler.this.eafnInitialFocus = new EntityAndField(null, fieldUID);
					}
				}
				
				handleLegacyResourceId(attributes);
			}

			@Override
			public void endElement(String sUriNameSpace, String sSimpleName, String sQualifiedName) throws SAXException {
				localizationHandler = null;
				super.endElement(sUriNameSpace, sSimpleName, sQualifiedName);
			}

		}	// inner class CollectableComponentElementProcessor

		/**
		 * inner class <code>InitialFocusComponentProcessor</code>. Processes a initial-focus-component element.
		 */
		private class InitialFocusComponentProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes) {
				final UID sEntityName = UID.parseUID(attributes.getValue(ATTRIBUTE_ENTITY));
				final UID sFieldName = UID.parseUID(attributes.getValue(ATTRIBUTE_NAME));
				BuildFormHandler.this.eafnInitialFocus = new EntityAndField(sEntityName, sFieldName);
			}
		} // inner class InitialFocusComponentProcessor

		/**
		 * inner class <code>InitialFocusComponentProcessor</code>. Processes a initial-focus-component element.
		 */
		private class InitialSortingOrderProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes) {
				final UID sName = UID.parseUID(attributes.getValue(ATTRIBUTE_NAME));
				final String sOrder = attributes.getValue(ATTRIBUTE_SORTINGORDER);
				BuildFormHandler.this.setInitialSortingColumnName(sName);
				BuildFormHandler.this.setInitialSortingOrder(sOrder);
			}

			@Override
			public void endElement(String sUriNameSpace, String sSimpleName, String sQualifiedName) {
				final SubForm subform = (SubForm) stack.peekComponent();
				subform.setInitialSortingOrder(BuildFormHandler.this.getInitialSortingColumnName(), BuildFormHandler.this.getInitialSortingOrder());
			}
		} // inner class InitialFocusComponentProcessor

		private String getInitialSortingOrder() {
			return this.sInitialSortingOrder;
		}

		private UID getInitialSortingColumnName() {
			return this.sInitialSortingColumnName;
		}

		private void setInitialSortingOrder(String sOrder) {
			this.sInitialSortingOrder = sOrder;
		}

		private void setInitialSortingColumnName(UID sSortingColumnName) {
			this.sInitialSortingColumnName = sSortingColumnName;
		}

		/**
		 * inner class <code>PanelElementProcessor</code>. Processes a panel element.
		 */
		private class PanelElementProcessor extends ComponentElementProcessor {
			/**
			 * constructs a <code>JPanel</code>, configures it according to the XML attributes
			 * and pushes it on the stack.
			 * @param sUriNameSpace
			 * @param sSimpleName
			 * @param sQualifiedName
			 * @param attributes
			 */
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes) {
				final JPanel pnl = new JPanel();

				// name:
				final String sName = attributes.getValue(ATTRIBUTE_NAME);
				if (sName != null) {
					pnl.setName(sName);
				}

				// visible:
				final String sVisible = attributes.getValue(ATTRIBUTE_VISIBLE);
				if (sVisible != null) {
					// override default:
					final boolean bVisible = sVisible.equals(ATTRIBUTEVALUE_YES);
					pnl.setVisible(bVisible);
				}

				// opaque:
				final String sOpaque = attributes.getValue(ATTRIBUTE_OPAQUE);
				final boolean bOpaque = (sOpaque != null && sOpaque.equals(ATTRIBUTEVALUE_YES));
				pnl.setOpaque(bOpaque);

				stack.addComponent(pnl);
			}
		}	// inner class PanelElementProcessor

		/**
		 * inner class <code>TabbedPaneElementProcessor</code>. Processes a tabbedpane element.
		 */
		private class TabbedPaneElementProcessor extends ComponentElementProcessor {
			/**
			 * constructs a <code>JInfoTabbedPane</code>, configures it according to the XML attributes
			 * and pushes it on the stack.
			 * @param sUriNameSpace
			 * @param sSimpleName
			 * @param sQualifiedName
			 * @param attributes
			 */
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes) {
				//final JInfoTabbedPane tbdpn = new JInfoTabbedPane();
				final JInfoTabbedPane tbdpn = new JInfoTabbedPane();
				final String sName = attributes.getValue(ATTRIBUTE_NAME);
				if (sName != null) {
					tbdpn.setName(sName);
				}

				final String sTabLayoutPolicy = attributes.getValue(ATTRIBUTE_TABLAYOUTPOLICY);
				if (sTabLayoutPolicy != null) {
					tbdpn.setTabLayoutPolicy(BuildFormHandler.this.mpTabLayoutPolicies.get(sTabLayoutPolicy));
				}

				final String sTabPlacement = attributes.getValue(ATTRIBUTE_TABPLACEMENT);
				if (sTabPlacement != null) {
					tbdpn.setTabPlacement(BuildFormHandler.this.mpTabPlacementConstants.get(sTabPlacement));
				}

				stack.addTabbedPane(tbdpn);
			}

		}	// inner class TabbedPaneElementProcessor

		/**
		 * inner class <code>SplitPaneElementProcessor</code>. Processes a splitpane element.
		 */
		private class SplitPaneElementProcessor extends ComponentElementProcessor {
			/**
			 * constructs a <code>JSplitPane</code>, configures it according to the XML attributes
			 * and pushes it on the stack.
			 * @param sUriNameSpace
			 * @param sSimpleName
			 * @param sQualifiedName
			 * @param attributes
			 */
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes) {
				final JSplitPane splitpn = new JSplitPane();

				final String sOrientation = attributes.getValue(ATTRIBUTE_ORIENTATION);
				final String sDividerSize = attributes.getValue(ATTRIBUTE_DIVIDERSIZE);
				final String sResizeWeight = attributes.getValue(ATTRIBUTE_RESIZEWEIGHT);
				final String sExpandable = attributes.getValue(ATTRIBUTE_EXPANDABLE);
				final String sContinuousLayout = attributes.getValue(ATTRIBUTE_CONTINUOUSLAYOUT);
				final String sName = attributes.getValue(ATTRIBUTE_NAME);

				splitpn.setOrientation(BuildFormHandler.this.mpSplitPaneOrientation.get(sOrientation));

				if (sDividerSize != null) {
					splitpn.setDividerSize(Integer.parseInt(sDividerSize));
				}
				if (sResizeWeight != null) {
					splitpn.setResizeWeight(Double.parseDouble(sResizeWeight));
				}
				if (sExpandable != null) {
					splitpn.setOneTouchExpandable(sExpandable.equals(ATTRIBUTEVALUE_YES));
				}
				if (sContinuousLayout != null) {
					splitpn.setContinuousLayout(sContinuousLayout.equals(ATTRIBUTEVALUE_YES));
				}
				if (sName != null) {
					splitpn.setName(sName);
				}

				stack.addComponent(splitpn);
			}
		}	// inner class SplitPaneElementProcessor
		
		/**
		 * inner class <code>MatrixElementProcessor</code>. Processes a subform element.
		 */
		private class MatrixElementProcessor extends ComponentElementProcessor {

			private JMatrixComponent matrix;

			/**
			 * constructs a <code>SubForm</code>, configures it according to the XML attributes
			 * and pushes it on the stack.
			 * @param sUriNameSpace
			 * @param sSimpleName
			 * @param sQualifiedName
			 * @param attributes
			 */
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {

				final UID entity = UID.parseUID(attributes.getValue(ATTRIBUTE_ENTITY_MATRIX));
				final UID entityMatixParentField = UID.parseUID(attributes.getValue(ATTRIBUTE_MATRIX_PARENT_FIELD));
				final UID entityX = UID.parseUID(attributes.getValue(ATTRIBUTE_ENTITY_X));
				final UID entityY = UID.parseUID(attributes.getValue(ATTRIBUTE_ENTITY_Y));				
				
				final UID fieldCategorie = UID.parseUID(attributes.getValue(ATTRIBUTE_ENTITY_FIELD_CATEGORIE));
				final UID fieldX = UID.parseUID(attributes.getValue(ATTRIBUTE_ENTITY_FIELD_X));
				final UID fieldY = UID.parseUID(attributes.getValue(ATTRIBUTE_ENTITY_FIELD_Y));
				final UID entityYParentField = UID.parseUID(attributes.getValue(ATTRIBUTE_ENTITY_Y_PARENT_FIELD));
				final UID entityXRefField = UID.parseUID(attributes.getValue(ATTRIBUTE_MATRIX_X_REF_FIELD));
				final UID matrixValueField = UID.parseUID(attributes.getValue(ATTRIBUTE_MATRIX_VALUE_FIELD));
				final UID matrixPreferences = UID.parseUID(attributes.getValue(ATTRIBUTE_ENTITY_FIELD_MATRIX_PREFERENCES));
				
				final String cellInputType = attributes.getValue(ATTRIBUTE_CELL_INPUT_TYPE);
				
				final String entityMatrixReferenceField = attributes.getValue(ATTRIBUTE_ENTITY_MATRIX_REFERENCE_FIELD);

				String sMatrixNumberState = attributes.getValue(ATTRIBUTE_MATRIX_NUMBER_STATE);

				final String entityXVlpId = attributes.getValue(ATTRIBUTE_ENTITY_X_VLP_ID);
				final String entityXVlpIdFieldName = attributes.getValue(ATTRIBUTE_ENTITY_X_VLP_IDFIELDNAME);
				final String entityXVlpFieldName = attributes.getValue(ATTRIBUTE_ENTITY_X_VLP_FIELDNAME);
				final String entityXVlpReferenceParamName = attributes.getValue(ATTRIBUTE_ENTITY_X_VLP_REFERENCE_PARAM_NAME);

				final String entityXHeader = attributes.getValue(ATTRIBUTE_ENTITY_X_HEADER);
				final String entityYHeader = attributes.getValue(ATTRIBUTE_ENTITY_Y_HEADER);

				final String entityXSortingFields = attributes.getValue(ATTRIBUTE_ENTITY_X_SORTING_FIELDS);
				final String entityYSortingFields = attributes.getValue(ATTRIBUTE_ENTITY_Y_SORTING_FIELDS);

				matrix = new JMatrixComponent(false);
				
				// enabled:
				boolean bEnabled = true;
				final String sEnabled = attributes.getValue(ATTRIBUTE_ENABLED);
				if (sEnabled != null && !bCreateSearchableComponents) {
					// override default:
					bEnabled = sEnabled.equals(ATTRIBUTEVALUE_YES);
				}
				

				UID groupField = UID.parseUID(attributes.getValue(ATTRIBUTE_ENTITY_FIELD_CATEGORIE));
				matrix.setEntityFieldGrouped(groupField);					
				matrix.setMatrixEntity(entity);
				matrix.setMatrixEntityParentField(entityMatixParentField);
				matrix.setEntityX(entityX);
				matrix.setEntityY(entityY);
				matrix.setFieldCategorie(fieldCategorie);
				matrix.setFieldX(fieldX);
				matrix.setFieldY(fieldY);
				matrix.setEntityYParentField(entityYParentField);
				matrix.setEntityXRefField(entityXRefField);
				matrix.setEntityMatrixValueField(matrixValueField);
				matrix.setPreferencesField(matrixPreferences);
				
				if(sMatrixNumberState == null) {
					sMatrixNumberState = "3"; // default number of states
				}
				try {
					Integer.parseInt(sMatrixNumberState);
				} catch (Exception e) {
					LOG.warn("Not a Number, matrix number of states");
					sMatrixNumberState = "3"; // default number of states
				}
					
				matrix.setNumberState(Integer.parseInt(sMatrixNumberState));
				
				matrix.setCellInputType(cellInputType);
				
				matrix.setEntityMatrixReferenceField(entityMatrixReferenceField);
				
				matrix.setEntityXVlpId(entityXVlpId);
				matrix.setEntityXVlpIdFieldName(entityXVlpIdFieldName);
				matrix.setEntityXVlpFieldName(entityXVlpFieldName);
				matrix.setEntityXVlpReferenceParamName(entityXVlpReferenceParamName);

				matrix.setEntityXHeader(entityXHeader);
				matrix.setEntityYHeader(entityYHeader);

				matrix.setEntityXSortingFields(entityXSortingFields);
				matrix.setEntityYSortingFields(entityYSortingFields);
				
				
				matrix.setEditable(bEnabled || bCreateSearchableComponents);
				matrix.setEnabled(bEnabled || bCreateSearchableComponents);

				final String sControllerType = attributes.getValue(ATTRIBUTE_CONTROLLERTYPE);
				matrix.setControllerType(sControllerType);
				
				stack.addMatrix(entity, matrix);
				stack.addVLPMatrix(entityY, matrix);
			}


		}	// inner class MatrixElementProcessor
		
		/**
		 * inner class <code>SubFormColumnElementProcessor</code>. Processes a subform element.
		 */
		private class MatrixColumnElementProcessor extends AbstractElementProcessor {
			/**
			 * constructs a <code>SubForm.Column</code>, configures it according to the XML attributes
			 * and adds it to the <code>SubForm</code> on top of the stack.
			 * @param sUriNameSpace
			 * @param sSimpleName
			 * @param sQualifiedName
			 * @param attributes
			 */
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {

				final JMatrixComponent matrix = (JMatrixComponent) stack.peekComponent();
				final UID sName = layoutMLParser.getFieldUID(attributes.getValue(ATTRIBUTE_NAME));

				String sLabel = attributes.getValue(ATTRIBUTE_LABEL);

				final String sControlType = attributes.getValue(ATTRIBUTE_CONTROLTYPE);
				final String sControlTypeClass = attributes.getValue(ATTRIBUTE_CONTROLTYPECLASS);

				final CollectableComponentType clctcomptype =
						BuildFormHandler.this.getCollectableComponentType(sControlType, sControlTypeClass);

				// visible:
				boolean bVisible = true;
				final String sVisible = attributes.getValue(ATTRIBUTE_VISIBLE);
				if (sVisible != null) {
					// override default:
					bVisible = sVisible.equals(ATTRIBUTEVALUE_YES);
				}

				// enabled:
				boolean bEnabled = true;
				final String sEnabled = attributes.getValue(ATTRIBUTE_ENABLED);
				if (sEnabled != null && !bCreateSearchableComponents) {
					// override default:
					bEnabled = sEnabled.equals(ATTRIBUTEVALUE_YES);
				}
				
				// cloneable:
				boolean bCloneable = true;
				final String sNotCloneable = attributes.getValue(ATTRIBUTE_NOT_CLONEABLE);
				if (sNotCloneable != null && !bCreateSearchableComponents) {
					// override default:
					bCloneable = !sNotCloneable.equals(ATTRIBUTEVALUE_YES);
				}

				// insertable:
				boolean bInsertable = false;
				final String sInsertable = attributes.getValue(ATTRIBUTE_INSERTABLE);
				if (sInsertable != null) {
					// override default:
					bInsertable = sInsertable.equals(ATTRIBUTEVALUE_YES);
				}

				// rows & columns
				final Integer iRows = getIntegerValue(attributes, ATTRIBUTE_ROWS);
				final Integer iColumns = getIntegerValue(attributes, ATTRIBUTE_COLUMNS);

				// width
				final Integer width = getIntegerValue(attributes, ATTRIBUTE_WIDTH);

				// next focus component
				final UID nextFocusFieldUID = UID.parseUID(attributes.getValue(ATTRIBUTE_NEXTFOCUSFIELD));

				final JMatrixComponent.Column column = new JMatrixComponent.Column(sName, sLabel, clctcomptype, bVisible, bEnabled, bCloneable, bInsertable, iRows, iColumns, width, nextFocusFieldUID);

				localizationHandler = new LocalizationHandler() {
					@Override public void setTranslation(String translation) {
						column.setLabel(translation);
					}
				};

				BuildFormHandler.this.matrixColumn = column;

				handleLegacyResourceId(attributes);
			}

			@Override
			public void endElement(String sUriNameSpace, String sSimpleName, String sQualifiedName) {
				localizationHandler = null;
				final JMatrixComponent matrix = (JMatrixComponent) stack.peekComponent();
				matrix.addColumn(BuildFormHandler.this.matrixColumn);
				BuildFormHandler.this.matrixColumn = null;
			}

		}	// inner class SubFormColumnElementProcessor


		/**
		 * inner class <code>SubFormElementProcessor</code>. Processes a subform element.
		 */
		private class SubFormElementProcessor extends ComponentElementProcessor {

			private SubForm subform;

			/**
			 * constructs a <code>SubForm</code>, configures it according to the XML attributes
			 * and pushes it on the stack.
			 * @param sUriNameSpace
			 * @param sSimpleName
			 * @param sQualifiedName
			 * @param attributes
			 */
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {

				final UID entityUID = layoutMLParser.getEntityUID(attributes.getValue(ATTRIBUTE_ENTITY));
				// entity is a required attribute:
				assert entityUID != null;

				// An attribute "visible" is not a good idea as inserting the component in a tab
				// implicitly sets visible to false. This is true for all components, not only subforms.

				// enabled:
				boolean bEnabled = true;
				final String sEnabled = attributes.getValue(ATTRIBUTE_ENABLED);
				if (sEnabled != null && !bCreateSearchableComponents) {
					// override default:
					bEnabled = sEnabled.equals(ATTRIBUTEVALUE_YES);
				}
				
				// cloneable:
				boolean bCloneable = true;
				final String sNotCloneable = attributes.getValue(ATTRIBUTE_NOT_CLONEABLE);
				if (sNotCloneable != null && !bCreateSearchableComponents) {
					// override default:
					bCloneable = !sNotCloneable.equals(ATTRIBUTEVALUE_YES);
				}
				
				// multieditable:
				String sDefaultMultieditable = ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_DEFAULT_SUBFORM_MULTIEDITING);
				boolean bMultieditable = Boolean.parseBoolean(StringUtils.defaultIfNull(sDefaultMultieditable, "true"));
				final String sMultieditable = attributes.getValue(ATTRIBUTE_MULTIEDITABLE);
				if (sMultieditable != null && !bCreateSearchableComponents) {
					// override default:
					bMultieditable = sMultieditable.equals(ATTRIBUTEVALUE_YES);
				}

				// toolbar orientation:
				Integer iOrientation = null;
				final String sOrientation = attributes.getValue(ATTRIBUTE_TOOLBARORIENTATION);
				if (sOrientation != null) {
					iOrientation = MyLayoutConstants.MPTOOLBARORIENTATION.get(sOrientation);
				}

				// field referencing parent entity:
				final UID foreignKeyFieldToParentUID = layoutMLParser.getFieldUID(attributes.getValue(ATTRIBUTE_FOREIGNKEYFIELDTOPARENT));

				subform = new SubForm(entityUID, bCreateSearchableComponents, 
						LangUtils.defaultIfNull(iOrientation, JToolBar.HORIZONTAL), layoutUID, foreignKeyFieldToParentUID);

				boolean bShowStatusbar = attributes.getValue(ATTRIBUTE_STATUSBAR) != null && attributes.getValue(ATTRIBUTE_STATUSBAR).equals(ATTRIBUTEVALUE_YES);
				subform.setShowStatusbar(bShowStatusbar);

				String sOpenDetailsWithTabRecycling = attributes.getValue(ATTRIBUTE_OPEN_DETAILS_WITH_TAB_RECYCLING);
				if (sOpenDetailsWithTabRecycling != null) {
					subform.setOpenDetailsWithTabRecycling(ATTRIBUTEVALUE_YES.equals(sOpenDetailsWithTabRecycling));
				}

				subform.setEnabled(bEnabled || bCreateSearchableComponents);
				subform.setEnabledByLayout(bEnabled || bCreateSearchableComponents);
				
				subform.setCloneable(bCloneable);
				subform.setMultieditable(bMultieditable);

				final String sControllerType = attributes.getValue(ATTRIBUTE_CONTROLLERTYPE);
				subform.setControllerType(sControllerType);

				Boolean autoSorting = attributes.getValue(ATTRIBUTE_AUTONUMBER_SORTING) != null ? 
						attributes.getValue(ATTRIBUTE_AUTONUMBER_SORTING).equals(ATTRIBUTEVALUE_YES) : Boolean.TRUE;
						
				subform.setAutonumberSorting(autoSorting);
				
				final UID uniqueMasterColumnUid = layoutMLParser.getFieldUID(attributes.getValue(ATTRIBUTE_UNIQUEMASTERCOLUMN));
				subform.setUniqueMasterColumnUid(uniqueMasterColumnUid);

				// referenced parent subform
				final UID parentSubFormUID = layoutMLParser.getEntityUID(attributes.getValue(ATTRIBUTE_PARENTSUBFORM));
				subform.setParentSubForm(parentSubFormUID);

				// dynamic cell heights default:
				final String sDynamicCellHeightsDefault = attributes.getValue(ATTRIBUTE_DYNAMIC_CELL_HEIGHTS_DEFAULT);
				if (sDynamicCellHeightsDefault != null && !bCreateSearchableComponents) {
					// override default:
					if (sDynamicCellHeightsDefault.equals(ATTRIBUTEVALUE_YES)) {
						subform.setDynamicRowHeightsDefault();
					}
				}

				// ignore sub layout:
				final String sIgnoreSubLayout = attributes.getValue(ATTRIBUTE_IGNORE_SUB_LAYOUT);
				if (sIgnoreSubLayout != null && !bCreateSearchableComponents) {
					// override default:
					if (sIgnoreSubLayout.equals(ATTRIBUTEVALUE_YES)) {
						subform.setIgnoreSubLayout();
					}
				}
				
				//max entries
				final String sMaxEntries = attributes.getValue(ATTRIBUTE_MAX_ENTRIES);
				if (sMaxEntries != null && !bCreateSearchableComponents) {
					// override default:
					try {
						subform.setMaxEntries(Integer.valueOf(sMaxEntries));
					} catch (NumberFormatException ex) {
						//do nothing
					}
				}

				stack.addSubForm(entityUID, subform);
			}


		}	// inner class SubFormElementProcessor
		
		/**
		 * inner class <code>SubFormColumnElementProcessor</code>. Processes a subform element.
		 */
		private class SubFormColumnElementProcessor extends AbstractElementProcessor {
			/**
			 * constructs a <code>SubForm.Column</code>, configures it according to the XML attributes
			 * and adds it to the <code>SubForm</code> on top of the stack.
			 * @param sUriNameSpace
			 * @param sSimpleName
			 * @param sQualifiedName
			 * @param attributes
			 */
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {

				final SubForm subform = (SubForm) stack.peekComponent();
				final UID sName = layoutMLParser.getFieldUID(attributes.getValue(ATTRIBUTE_NAME));

				String sLabel = attributes.getValue(ATTRIBUTE_LABEL);

				final String sControlType = attributes.getValue(ATTRIBUTE_CONTROLTYPE);
				final String sControlTypeClass = attributes.getValue(ATTRIBUTE_CONTROLTYPECLASS);

				final CollectableComponentType clctcomptype =
						BuildFormHandler.this.getCollectableComponentType(sControlType, sControlTypeClass);

				// visible:
				boolean bVisible = true;
				final String sVisible = attributes.getValue(ATTRIBUTE_VISIBLE);
				if (sVisible != null) {
					// override default:
					bVisible = sVisible.equals(ATTRIBUTEVALUE_YES);
				}

				// enabled:
				boolean bEnabled = true;
				final String sEnabled = attributes.getValue(ATTRIBUTE_ENABLED);
				if (sEnabled != null && !bCreateSearchableComponents) {
					// override default:
					bEnabled = sEnabled.equals(ATTRIBUTEVALUE_YES);
				}
				
				// cloneable:
				boolean bCloneable = true;
				final String sNotCloneable = attributes.getValue(ATTRIBUTE_NOT_CLONEABLE);
				
				if (sNotCloneable != null && !bCreateSearchableComponents) {
					// override default:
					bCloneable = !sNotCloneable.equals(ATTRIBUTEVALUE_YES);
				}

				// Multiselectsearchfilter:
				boolean bMultiselectsearchfilter = true;
				final String sbMultiselectsearchfilter = attributes.getValue(ATTRIBUTE_MULTISELECT_SEARCHFILTER);
				
				if (sbMultiselectsearchfilter != null && !bCreateSearchableComponents) {
					// override default:
					bMultiselectsearchfilter = sbMultiselectsearchfilter.equals(ATTRIBUTEVALUE_YES);
				}

				
				// insertable:
				boolean bInsertable = false;
				final String sInsertable = attributes.getValue(ATTRIBUTE_INSERTABLE);
				if (sInsertable != null) {
					// override default:
					bInsertable = sInsertable.equals(ATTRIBUTEVALUE_YES);
				}
				
				// lovsearch
				boolean bLovSearch = false;
				final String sLovSearch = attributes.getValue(ATTRIBUTE_LOV_SEARCHMASK);
				if (sLovSearch != null) {
					bLovSearch = sLovSearch.equals(ATTRIBUTEVALUE_YES);
				}

				// rows & columns
				final Integer iRows = getIntegerValue(attributes, ATTRIBUTE_ROWS);
				final Integer iColumns = getIntegerValue(attributes, ATTRIBUTE_COLUMNS);

				// width
				final Integer width = getIntegerValue(attributes, ATTRIBUTE_WIDTH);

				// next focus component
				final UID nextFocusFieldUID = UID.parseUID(attributes.getValue(ATTRIBUTE_NEXTFOCUSFIELD));
				
				// custom usage for search layout
				final String customUsageSearch = attributes.getValue(ATTRIBUTE_CUSTOM_USAGE_SEARCH);


				final Column column = new Column(sName, sLabel, clctcomptype, bVisible, bEnabled, bCloneable, bInsertable,bMultiselectsearchfilter, bLovSearch, iRows, iColumns, customUsageSearch, width, nextFocusFieldUID);


				localizationHandler = new LocalizationHandler() {
					@Override public void setTranslation(String translation) {
						column.setLabel(translation);
					}
				};

				BuildFormHandler.this.subformcolumn = column;

				handleLegacyResourceId(attributes);
			}

			@Override
			public void endElement(String sUriNameSpace, String sSimpleName, String sQualifiedName) {
				localizationHandler = null;
				final SubForm subform = (SubForm) stack.peekComponent();
				subform.addColumn(BuildFormHandler.this.subformcolumn);
				BuildFormHandler.this.subformcolumn = null;
			}

		}	// inner class SubFormColumnElementProcessor

		/**
		 * inner class <code>ChartElementProcessor</code>. Processes a chart element.
		 */
		private class ChartElementProcessor extends ComponentElementProcessor {

			private Chart chart;

			/**
			 * constructs a <code>Chart</code>, configures it according to the XML attributes
			 * and pushes it on the stack.
			 * @param sUriNameSpace
			 * @param sSimpleName
			 * @param sQualifiedName
			 * @param attributes
			 */
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {

				final UID sEntityName = UID.parseUID(attributes.getValue(ATTRIBUTE_ENTITY));
				// entity is a required attribute:
				assert sEntityName != null;

				// An attribute "visible" is not a good idea as inserting the component in a tab
				// implicitly sets visible to false. This is true for all components, not only subforms.

				// enabled:
				boolean bEnabled = true;
				final String sEnabled = attributes.getValue(ATTRIBUTE_ENABLED);
				if (sEnabled != null && !bCreateSearchableComponents) {
					// override default:
					bEnabled = sEnabled.equals(ATTRIBUTEVALUE_YES);
				}

				// toolbar orientation:
				Integer iOrientation = null;
				final String sOrientation = attributes.getValue(ATTRIBUTE_TOOLBARORIENTATION);
				if (sOrientation != null) {
					iOrientation = MyLayoutConstants.MPTOOLBARORIENTATION.get(sOrientation);
				}

				// scrollpane:
				Integer iScrollPane = null;
				final String sScrollpane = attributes.getValue(ATTRIBUTE_SCROLLPANE);
				if (sScrollpane != null) {
					iScrollPane = MyLayoutConstants.MPSCROLLPANE.get(sScrollpane);
				}

				// field referencing parent entity:
				final UID sForeignKeyFieldToParent = UID.parseUID(attributes.getValue(ATTRIBUTE_FOREIGNKEYFIELDTOPARENT));
				
				chart = new Chart(sEntityName, 
						LangUtils.defaultIfNull(iScrollPane, -1),
						LangUtils.defaultIfNull(iOrientation, JToolBar.HORIZONTAL),
						layoutUID,
						sForeignKeyFieldToParent, false, bCreateSearchableComponents);

				chart.setEnabled(bEnabled);
				chart.setReadOnly(!bEnabled && !bCreateSearchableComponents);
				
				stack.addChart(sEntityName, chart);
			}
			
		}	// inner class ChartElementProcessor

		/**
		 * inner class <code>ScrollPaneElementProcessor</code>. Processes a scrollpane element.
		 */
		private class ScrollPaneElementProcessor extends ComponentElementProcessor {
			/**
			 * constructs a <code>JScrollPane</code>, configures it according to the XML attributes
			 * and pushes it on the stack.
			 * @param sUriNameSpace
			 * @param sSimpleName
			 * @param sQualifiedName
			 * @param attributes
			 */
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes) {
				/** @todo maybe add an attribute setMinimumSizeToPreferredSize or shrinkBelowPreferredSize */
				final JScrollPane scrlpn = new CommonJScrollPane();

				final String sHorizontalScrollBarPolicy = attributes.getValue(ATTRIBUTE_HORIZONTALSCROLLBARPOLICY);
				if (sHorizontalScrollBarPolicy != null) {
					scrlpn.setHorizontalScrollBarPolicy(BuildFormHandler.this.mpHorizontalScrollBarPolicies.get(sHorizontalScrollBarPolicy));
				}

				final String sVerticalScrollBarPolicy = attributes.getValue(ATTRIBUTE_VERTICALSCROLLBARPOLICY);
				if (sVerticalScrollBarPolicy != null) {
					scrlpn.setVerticalScrollBarPolicy(BuildFormHandler.this.mpVerticalScrollBarPolicies.get(sVerticalScrollBarPolicy));
				}

				// set unit increments to fix values:
				scrlpn.getHorizontalScrollBar().setUnitIncrement(10);
				scrlpn.getVerticalScrollBar().setUnitIncrement(10);

				stack.addScrollPane(scrlpn);
			}
		}	// inner class ScrollPaneElementProcessor

		/**
		 * inner class <code>LabelElementProcessor</code>. Processes a label element.
		 */
		private class LabelElementProcessor extends ComponentElementProcessor {
			/**
			 * constructs a <code>JLabel</code>, configures it according to the XML attributes
			 * and pushes it on the stack.
			 * @param sUriNameSpace
			 * @param sSimpleName
			 * @param sQualifiedName
			 * @param attributes
			 */
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes) {
				final LabeledComponent.ComponentLabel lab = new LabeledComponent.ComponentLabel();

				// name:
				final String sName = attributes.getValue(ATTRIBUTE_NAME);

				if (sName != null) {
					lab.setName(sName);
				}

				lab.setText(attributes.getValue(ATTRIBUTE_TEXT));

				stack.addComponent(lab);
				handleLegacyResourceId(attributes);
			}

			@Override
			public void endElement(String sUriNameSpace, String sSimpleName, String sQualifiedName) throws SAXException {
				JComponent comp = stack.peekComponent();
				String text = stack.peekComponentBuilder().getTranslation();
				if (comp instanceof JLabel && text != null) {
					((JLabel) comp).setText(text);
				}
				super.endElement(sUriNameSpace, sSimpleName, sQualifiedName);
			}
		}	// inner class LabelElementProcessor

		/**
		 * LocaleResourceElementProcessor processes the provided set of translations.
		 */
		private class TranslationsElementProcessor extends AbstractElementProcessor {
			@Override
			public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes) throws SAXException {
				translations = new HashMap<String, String>();
			}

			@Override
			public void endElement(String sUriNameSpace, String sSimpleName, String sQualifiedName) {
				String text = SpringLocaleDelegate.getInstance().selectBestTranslation(translations);
				if (text != null) {
					getStaticLocalizationHandler().setTranslation(text);
				}
				translations = null;
			}
		}

		/**
		 * LocaleResourceElementProcessor processes the provided set of translations.
		 */
		private class PropertyTranslationsElementProcessor extends AbstractElementProcessor {
			String sProperty;

			@Override
			public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes) throws SAXException {
				sProperty = attributes.getValue(ATTRIBUTE_NAME);
				translations = new TranslationMap();
			}

			@Override
			public void endElement(String sUriNameSpace, String sSimpleName, String sQualifiedName) {
				new LayoutComponentDelegator(stack.peekComponent()) {
					@Override
					public void foundLC(LayoutComponent lc) {
						if (lc.getComponentProperties() != null) {
							for (Property pt : lc.getComponentProperties()) {
								if (pt.name.equals(sProperty)) {
									lc.setProperty(sProperty, translations);
									break;
								}
							}
						}
					}
				};

				translations = null;
			}
		}

		/**
		 * LocaleResourceElementProcessor processes one translation.
		 */
		private class TranslationElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes) throws SAXException {
				String attributeLang = attributes.getValue(ATTRIBUTE_LANG);
				String attributeText = attributes.getValue(ATTRIBUTE_TEXT);
				translations.put(attributeLang, attributeText);
			}
		}

		/**
		 * inner class <code>TextFieldElementProcessor</code>. Processes a textfield element.
		 */
		private class ImageElementProcessor extends ComponentElementProcessor {
			/**
			 * constructs a <code>JTextField</code>, configures it according to the XML attributes
			 * and pushes it on the stack.
			 * @param sUriNameSpace
			 * @param sSimpleName
			 * @param sQualifiedName
			 * @param attributes
			 */
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				final JLabel lb = new JLabel() {
					/**
					 * set the minimum size equal to the preferred size in order to avoid
					 * GridBagLayout flaws.
					 * @return the value of the <code>preferredSize</code> property
					 */
					@Override
					public Dimension getMinimumSize() {
						return this.getPreferredSize();
					}
				};

				// name:
				final String sName = attributes.getValue(ATTRIBUTE_NAME);
				if (sName != null) {
					lb.setName(sName);
				}

				// columns:
				final Integer iColumns = getIntegerValue(attributes, ATTRIBUTE_COLUMNS);
				if (iColumns != null) {

				}

				// enabled:
				boolean bEnabled = true;
				final String sEnabled = attributes.getValue(ATTRIBUTE_ENABLED);
				if (sEnabled != null) {
					// override default:
					bEnabled = sEnabled.equals(ATTRIBUTEVALUE_YES);
				}
				lb.setEnabled(bEnabled);

				// editable:
				boolean bEditable = true;
				final String sEditable = attributes.getValue(ATTRIBUTE_EDITABLE);
				if (sEditable != null) {
					// override default:
					bEditable = sEditable.equals(ATTRIBUTEVALUE_YES);
				}


				stack.addComponent(lb);
			}
		}	// inner class TextFieldElementProcessor

		/**
		 * inner class <code>TextFieldElementProcessor</code>. Processes a textfield element.
		 */
		private class TextFieldElementProcessor extends ComponentElementProcessor {
			/**
			 * constructs a <code>JTextField</code>, configures it according to the XML attributes
			 * and pushes it on the stack.
			 * @param sUriNameSpace
			 * @param sSimpleName
			 * @param sQualifiedName
			 * @param attributes
			 */
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				final JTextField tf = new JTextField() {
					/**
					 * set the minimum size equal to the preferred size in order to avoid
					 * GridBagLayout flaws.
					 * @return the value of the <code>preferredSize</code> property
					 */
					@Override
					public Dimension getMinimumSize() {
						return this.getPreferredSize();
					}
				};

				// name:
				final String sName = attributes.getValue(ATTRIBUTE_NAME);
				if (sName != null) {
					tf.setName(sName);
				}

				// columns:
				final Integer iColumns = getIntegerValue(attributes, ATTRIBUTE_COLUMNS);
				if (iColumns != null) {
					tf.setColumns(iColumns);
				}

				// enabled:
				boolean bEnabled = true;
				final String sEnabled = attributes.getValue(ATTRIBUTE_ENABLED);
				if (sEnabled != null) {
					// override default:
					bEnabled = sEnabled.equals(ATTRIBUTEVALUE_YES);
				}
				tf.setEnabled(bEnabled);
				
				// next focus field
				final String sNextFocusField = attributes.getValue(ATTRIBUTE_NEXTFOCUSFIELD);
				if(sNextFocusField != null) {
					tf.putClientProperty(ATTRIBUTE_NEXTFOCUSFIELD, sNextFocusField);
				}
				
				// next focus component
				final String sNextFocusComponent = attributes.getValue(ATTRIBUTE_NEXTFOCUSCOMPONENT);
				if(sNextFocusComponent != null) {
					tf.putClientProperty(ATTRIBUTE_NEXTFOCUSCOMPONENT, sNextFocusComponent);
				}

				// editable:
				boolean bEditable = true;
				final String sEditable = attributes.getValue(ATTRIBUTE_EDITABLE);
				if (sEditable != null) {
					// override default:
					bEditable = sEditable.equals(ATTRIBUTEVALUE_YES);
				}
				tf.setEditable(bEditable);

				stack.addComponent(tf);
			}
		}	// inner class TextFieldElementProcessor

		/**
		 * inner class <code>TextAreaElementProcessor</code>. Processes a textarea element.
		 */
		private class TextAreaElementProcessor extends ComponentElementProcessor {
			/**
			 * constructs a <code>JTextArea</code>, configures it according to the XML attributes
			 * and pushes it on the stack.
			 * @param sUriNameSpace
			 * @param sSimpleName
			 * @param sQualifiedName
			 * @param attributes
			 */
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {

				//NUCLEUSINT-317 for scrollbars etc
				final InnerTextArea ita = new InnerTextArea(new LabeledComponentSupport());
				final LabeledTextArea ta = new LabeledTextArea(ita);
				ta.getJLabel().setVisible(false);

				// name:
				final String sName = attributes.getValue(ATTRIBUTE_NAME);
				if (sName != null) {
					ta.setName(sName);
				}

				// rows:
				final Integer iRows = getIntegerValue(attributes, ATTRIBUTE_ROWS);
				if (iRows != null) {
					ta.setRows(iRows);
				}

				// columns:
				final Integer iColumns = getIntegerValue(attributes, ATTRIBUTE_COLUMNS);
				if (iColumns != null) {
					ta.setColumns(iColumns);
				}

				// enabled:
				boolean bEnabled = true;
				final String sEnabled = attributes.getValue(ATTRIBUTE_ENABLED);
				if (sEnabled != null) {
					// override default:
					bEnabled = sEnabled.equals(ATTRIBUTEVALUE_YES);
				}
				ta.setEnabled(bEnabled);

				// next focus field
				final String sNextFocusField = attributes.getValue(ATTRIBUTE_NEXTFOCUSFIELD);
				if(sNextFocusField != null) {
					ta.putClientProperty(ATTRIBUTE_NEXTFOCUSFIELD, sNextFocusField);
				}
				
				// next focus component
				final String sNextFocusComponent = attributes.getValue(ATTRIBUTE_NEXTFOCUSCOMPONENT);
				if(sNextFocusComponent != null) {
					ta.putClientProperty(ATTRIBUTE_NEXTFOCUSCOMPONENT, sNextFocusComponent);
				}

				// editable:
				boolean bEditable = true;
				final String sEditable = attributes.getValue(ATTRIBUTE_EDITABLE);
				if (sEditable != null) {
					// override default:
					bEditable = sEditable.equals(ATTRIBUTEVALUE_YES);
				}
				ta.setEditable(bEditable);

				stack.addComponent(ta);
			}
		}	// inner class TextAreaElementProcessor

		private class TextModuleElementProcessor extends AbstractElementProcessor {

			@Override
			public void startElement(String sUriNameSpace, String sSimpleName,
					String sQualifiedName, Attributes attributes)
							throws SAXException {
				final UID textModuleEntity = UID.parseUID(attributes.getValue(ATTRIBUTE_TEXTMODULE_ENTITY));
				final UID textModuleEntityField = UID.parseUID(attributes.getValue(ATTRIBUTE_TEXTMODULE_ENTITY_FIELD));
				final UID textModuleEntityFieldName = UID.parseUID(attributes.getValue(ATTRIBUTE_TEXTMODULE_ENTITY_FIELD_NAME));
				final UID textModuleEntityFieldSort = UID.parseUID(attributes.getValue(ATTRIBUTE_TEXTMODULE_ENTITY_FIELD_SORT));

				Object o = stack.peekComponent();
				if (null != textModuleEntity && null != textModuleEntityField) {
					final TextModuleSettings tms = new TextModuleSettings(
							textModuleEntity, textModuleEntityFieldName, textModuleEntityField, textModuleEntityFieldSort);
					if (o instanceof SubForm) {
						// handle subform text module settings
						final SubForm sf = (SubForm) o;
						if (BuildFormHandler.this.subformcolumn != null) {
							final SubForm subform = (SubForm) stack.peekComponent();
							final Column column = BuildFormHandler.this.subformcolumn;
							if (null != textModuleEntity && null != textModuleEntityField) {
								subform.putTextModuleSettings(column.getName(), tms);
							}
						}
					} else if (o instanceof TextModuleSupport) {
						final TextModuleSupport ta = (TextModuleSupport)stack.peekComponent();
						ta.setTextModuleSettings(tms);
					}
				}
			}
		}

		/**
		 * inner class <code>ComboBoxElementProcessor</code>. Processes a combobox element.
		 */
		private class ComboBoxElementProcessor extends ComponentElementProcessor {
			/**
			 * constructs a <code>JComboBox</code>, configures it according to the XML attributes
			 * and pushes it on the stack.
			 * @param sUriNameSpace
			 * @param sSimpleName
			 * @param sQualifiedName
			 * @param attributes
			 */
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes) {
				final JComboBox cmbbx = new JComboBox();

				// name:
				final String sName = attributes.getValue(ATTRIBUTE_NAME);
				if (sName != null) {
					cmbbx.setName(sName);
				}

				// editable:
				final String sEditable = attributes.getValue(ATTRIBUTE_EDITABLE);
				final boolean bEditable = sEditable != null && sEditable.equals("yes");
				cmbbx.setEditable(bEditable);

				//NUCLEUSINT-407 enabled:
				final String sEnabled = attributes.getValue(ATTRIBUTE_ENABLED);
				final boolean bEnabled = sEnabled != null && sEnabled.equals("yes");
				cmbbx.setEnabled(bEnabled);

				// next focus field
				final String sNextFocusField = attributes.getValue(ATTRIBUTE_NEXTFOCUSFIELD);
				if(sNextFocusField != null) {
					cmbbx.putClientProperty(ATTRIBUTE_NEXTFOCUSFIELD, sNextFocusField);
				}
				
				// next focus component
				final String sNextFocusComponent = attributes.getValue(ATTRIBUTE_NEXTFOCUSCOMPONENT);
				if(sNextFocusComponent != null) {
					cmbbx.putClientProperty(ATTRIBUTE_NEXTFOCUSCOMPONENT, sNextFocusComponent);
				}

				stack.addComponent(cmbbx);
			}
		}	// inner class ComboBoxElementProcessor

		public static class LayoutMLButton extends JButton {

			private String sActionkey;
			
			private LayoutMLButton() {
				setModel(new DefaultButtonModel() {
					@Override
					protected void fireActionPerformed(ActionEvent e) {
						super.fireActionPerformed(e);
					}
				});
			}
			
			public void setActionKey(String sActionkey) {
				this.sActionkey = sActionkey;
			}
			public String getActionKey() {
				return sActionkey;
			}

			@Override
			public void setActionCommand(String actionCommand) {
				super.setActionCommand(actionCommand);

				ActionListener[] als = getActionListeners();
				for (int i = 0; i < als.length; i++) {
					ActionListener al = als[i];
					if (al instanceof LayoutMLButtonActionListener)
						((LayoutMLButtonActionListener)al).setParentComponent(this, actionCommand);
				}
			}
			
			@Override
			public void addActionListener(ActionListener l) {
				boolean added = false;
				ActionListener[] als = getActionListeners();
				for (int i = 0; i < als.length; i++) {
					if (als[i] == l) {
						added = true;
						break;
					}
				}
				if (!added) {
					super.addActionListener(l);
				}
			}

			public void setDisableDuringEdit(boolean bDisableDuringEdit) {
				putClientProperty("disableDuringEdit", bDisableDuringEdit);
			}
			
			@Override
			public void setEnabled(boolean enabled) {
				super.setEnabled(enabled);
			}

		}

		public static class LayoutMLButtonLocalizationHandler implements LocalizationHandler {

			private final JButton btn;

			private LayoutMLButtonLocalizationHandler(JButton btn) {
				this.btn = btn;
			}

			@Override
			public void setTranslation(String translation) {
				btn.setText(translation);
			}
		}

		/**
		 * inner class <code>ButtonElementProcessor</code>. Processes a button element.
		 */
		private class ButtonElementProcessor extends ComponentElementProcessor {

			/**
			 * constructs a <code>JButton</code>, configures it according to the XML attributes
			 * and pushes it on the stack.
			 * @param sUriNameSpace
			 * @param sSimpleName
			 * @param sQualifiedName
			 * @param attributes
			 */
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes) {
				final LayoutMLButton btn = new LayoutMLButton();
				btn.addActionListener(BuildFormHandler.this.alButtons);
				
				// name:
				final String sName = attributes.getValue(ATTRIBUTE_NAME);
				if (sName != null) {
					btn.setName(sName);
				}

				final String sActionCommand = attributes.getValue(ATTRIBUTE_ACTIONCOMMAND);

				final String sLabel = attributes.getValue(ATTRIBUTE_LABEL);

				boolean bEnabled = true;
				final String sEnabled = attributes.getValue(ATTRIBUTE_ENABLED);
				if (sEnabled != null && sEnabled.equals("no")) {
					bEnabled = false;
				}
				btn.setEnabled(bEnabled);
				btn.putClientProperty("enabledByLayout", bEnabled);

				btn.setText(sLabel);
				localizationHandler = new LayoutMLButtonLocalizationHandler(btn);
				handleLegacyResourceId(attributes);

				final String sToolTip = attributes.getValue(ATTRIBUTE_TOOLTIP);
				btn.setToolTipText(sToolTip);

				btn.setActionCommand(sActionCommand);

				// next focus field
				final String sNextFocusField = attributes.getValue(ATTRIBUTE_NEXTFOCUSFIELD);
				if(sNextFocusField != null) {
					btn.putClientProperty(ATTRIBUTE_NEXTFOCUSFIELD, sNextFocusField);
				}
				// next focus component
				final String sNextFocusComponent = attributes.getValue(ATTRIBUTE_NEXTFOCUSCOMPONENT);
				if(sNextFocusComponent != null) {
					btn.putClientProperty(ATTRIBUTE_NEXTFOCUSCOMPONENT, sNextFocusComponent);
				}
				
				// next focus on action
				final String sNextFocusOnAction = attributes.getValue(ATTRIBUTE_NEXTFOCUSONACTION);
				if(sNextFocusOnAction != null) {
					btn.putClientProperty(ATTRIBUTE_NEXTFOCUSONACTION, sNextFocusOnAction.equals("yes") ? Boolean.TRUE : Boolean.FALSE);
				}
				
				final String sActionkey = attributes.getValue(ATTRIBUTE_ACTIONKEYSTROKE);
				btn.setActionKey(sActionkey);

				final String sIcon = attributes.getValue(ATTRIBUTE_ICON);
				if (!StringUtils.isNullOrEmpty(sIcon)) {
					try {
						ImageIcon ico = ResourceCache.getInstance().getIconResource(UID.parseUID(sIcon));
						btn.setIcon(ico);
					}
					catch (Exception ex) {
						LOG.warn("Icon not found or invalid", ex);
					}
				}
				
				btn.setDisableDuringEdit(ATTRIBUTEVALUE_YES.equals(attributes.getValue(ATTRIBUTE_DISABLE_DURING_EDIT)));

				if (BuildFormHandler.this.alButtons != null) {
					btn.addActionListener(BuildFormHandler.this.alButtons);
				}
				stack.addComponent(btn);
				handleLegacyResourceId(attributes);
			}

			@Override
			public void endElement(String sUriNameSpace, String sSimpleName, String sQualifiedName) throws SAXException {
				localizationHandler = null;
				super.endElement(sUriNameSpace, sSimpleName, sQualifiedName);
			}
		}	// inner class ButtonElementProcessor

		/**
		 * inner class <code>SeparatorElementProcessor</code>. Processes a separator element.
		 */
		private class SeparatorElementProcessor extends ComponentElementProcessor {
			/**
			 * constructs a <code>JSeparator</code>, configures it according to the XML attributes
			 * and pushes it on the stack.
			 * @param sUriNameSpace
			 * @param sSimpleName
			 * @param sQualifiedName
			 * @param attributes the XML attributes of this element
			 */
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {

				Integer iOrientation = null;
				final String sOrientation = attributes.getValue(ATTRIBUTE_ORIENTATION);
				if (sOrientation != null) {
					iOrientation = BuildFormHandler.this.mpSeparatorOrientation.get(sOrientation);
				}
				final JSeparator separator = (iOrientation == null) ? new CommonJSeparator() : new CommonJSeparator(iOrientation);

				stack.addComponent(separator);
			}
		}	// class SeparatorElementProcessor

		/**
		 * inner class <code>TitledSeparatorElementProcessor</code>. Processes a titled-separator element.
		 */
		private class TitledSeparatorElementProcessor extends ComponentElementProcessor {
			/**
			 * constructs a <code>TitledSeparator</code>, configures it according to the XML attributes
			 * and pushes it on the stack.
			 * @param sUriNameSpace
			 * @param sSimpleName
			 * @param sQualifiedName
			 * @param attributes the XML attributes of this element
			 */
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {

				String sTitle = attributes.getValue(ATTRIBUTE_TITLE);

				final TitledSeparator separator = new TitledSeparator(sTitle);
				stack.addComponent(separator);
				handleLegacyResourceId(attributes);
			}

			@Override
			public void endElement(String sUriNameSpace, String sSimpleName, String sQualifiedName) throws SAXException {
				JComponent comp = stack.peekComponent();
				String text = stack.peekComponentBuilder().getTranslation();
				if (comp instanceof TitledSeparator && text != null) {
					((TitledSeparator) comp).setTitle(text);
				}
				super.endElement(sUriNameSpace, sSimpleName, sQualifiedName);
			}

		}	// class SeparatorElementProcessor

		/**
		 * inner class <code>OptionsElementProcessor</code>. Processes a options element.
		 */
		private class OptionsElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				final CollectableComponentBuilder ccb = (CollectableComponentBuilder) stack.peekComponentBuilder();

				if (!BuildFormHandler.this.bCreateSearchableComponents) {
					// set a default for non-searchable components:
					ccb.setDefaultOption(attributes.getValue(ATTRIBUTE_DEFAULT));
				}

				final String sOrientation = attributes.getValue(ATTRIBUTE_ORIENTATION);
				if (sOrientation != null) {
					final CollectableOptionGroup clctoptgrp = (CollectableOptionGroup) ccb.getCollectableComponent();
					clctoptgrp.setOrientation(MyLayoutConstants.MPSWINGCONSTANTSORIENTATIONORIENTATION.get(sOrientation));
				}
			}
		}

		/**
		 * inner class <code>OptionElementProcessor</code>. Processes a option element.
		 */
		private class OptionElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				final String sValue = attributes.getValue(ATTRIBUTE_VALUE);
				final String sLabel = attributes.getValue(ATTRIBUTE_LABEL);
				final String sMnemonic = attributes.getValue(ATTRIBUTE_MNEMONIC);

				final CollectableComponentBuilder ccb = (CollectableComponentBuilder) stack.peekComponentBuilder();
				final String[] addOption = ccb.addOption(sValue, sLabel, sMnemonic);
				localizationHandler = new LocalizationHandler() {
					@Override
					public void setTranslation(String translation) {
						addOption[1] = translation;
					}
				};
			}

			@Override
			public void endElement(String sUriNameSpace, String sSimpleName, String sQualifiedName) {
				super.endElement(sUriNameSpace, sSimpleName, sQualifiedName);
				localizationHandler = null;
			}
		}

		/**
		 * inner class <code>ValueListProviderElementProcessor</code>. Processes a valuelist-provider element.
		 */
		private class ValueListProviderElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {

				final String sType = attributes.getValue(ATTRIBUTE_TYPE);
				final String sValue = attributes.getValue(ATTRIBUTE_VALUE);

				final UID entityUID;
				final UID fieldUID;

				// <valuelist-provider> may have <collectable-component> or <subform-column> as parent:
				if (BuildFormHandler.this.subformcolumn != null) {
					final SubForm subform = (SubForm) stack.peekComponent();
					entityUID = subform.getEntityUID();
					assert entityUID != null;
					fieldUID = BuildFormHandler.this.subformcolumn.getUID();
				}
				else if (BuildFormHandler.this.matrixColumn != null) {
					final JMatrixComponent matrix = (JMatrixComponent) stack.peekComponent();
					entityUID = matrix.getEntityY();
					assert entityUID != null;
					fieldUID = BuildFormHandler.this.matrixColumn.getUID();
				}
				else {
					final CollectableComponentBuilder ccb = (CollectableComponentBuilder) stack.peekComponentBuilder();
					entityUID = BuildFormHandler.this.getCollectableEntity().getUID();
					assert entityUID != null;
					fieldUID = ccb.getCollectableComponent().getFieldUID();
				}

				final CollectableFieldsProvider valuelistprovider;
				final ValuelistProviderVO.Type type = KeyEnum.Utils.findEnum(ValuelistProviderVO.Type.class, sType);
				if (LangUtils.equal(type, ValuelistProviderVO.Type.DEFAULT)) {
					// default provider:
					valuelistprovider = valueListProviderFactory.newDefaultCollectableFieldsProvider(fieldUID);
				}
				else if (LangUtils.equal(type, ValuelistProviderVO.Type.DEPENDANT)) {
					// "dependant" provider:
					valuelistprovider = valueListProviderFactory.newDependantCollectableFieldsProvider(fieldUID);
				}
				else {
					// custom provider:
					valuelistprovider = valueListProviderFactory.newCustomCollectableFieldsProvider(sValue, fieldUID);
				}

				BuildFormHandler.this.valuelistprovider = valuelistprovider;
			}

			@Override
			public void endElement(String sUriNameSpace, String sSimpleName, String sQualifiedName) {
				assert BuildFormHandler.this.valuelistprovider != null;
				
				if (BuildFormHandler.this.subformcolumn != null) {
					BuildFormHandler.this.subformcolumn.setValueListProvider(BuildFormHandler.this.valuelistprovider);
				}
				else if(BuildFormHandler.this.matrixColumn != null) {
					BuildFormHandler.this.matrixColumn.setValueListProvider(BuildFormHandler.this.valuelistprovider);
				}
				else {
					final CollectableComponentBuilder ccb = (CollectableComponentBuilder) stack.peekComponentBuilder();
					assert ccb != null;
					if (ccb.getCollectableComponent() instanceof LabeledCollectableComponentWithVLP) {
						((LabeledCollectableComponentWithVLP) ccb.getCollectableComponent()).setValueListProvider(BuildFormHandler.this.valuelistprovider);
					}
				}

				BuildFormHandler.this.valuelistprovider = null;
			}
		}

		/**
		 * inner class <code>PropertyValuelistProviderElementProcessor</code>. Processes a property-valuelist-provider element.
		 */
		private class PropertyValuelistProviderElementProcessor extends AbstractElementProcessor {

			String sProperty;

			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				sProperty = attributes.getValue(ATTRIBUTE_PROP_NAME);

				final String sType = attributes.getValue(ATTRIBUTE_TYPE);
				final String sValue = attributes.getValue(ATTRIBUTE_VALUE);

				final UID entityUID = layoutMLParser.getFieldUID(attributes.getValue(ATTRIBUTE_ENTITY));
				final UID fieldUID = layoutMLParser.getFieldUID(attributes.getValue(ATTRIBUTE_FIELD));

				final CollectableFieldsProvider valuelistprovider;
				final ValuelistProviderVO.Type type = KeyEnum.Utils.findEnum(ValuelistProviderVO.Type.class, sType);
				if (LangUtils.equal(type, ValuelistProviderVO.Type.DEFAULT)) {
					// default provider:
					valuelistprovider = valueListProviderFactory.newDefaultCollectableFieldsProvider(fieldUID);
				}
				else if (LangUtils.equal(type, ValuelistProviderVO.Type.DEPENDANT)) {
					// "dependant" provider:
					valuelistprovider = valueListProviderFactory.newDependantCollectableFieldsProvider(fieldUID);
				}
				else {
					// custom provider:
					valuelistprovider = valueListProviderFactory.newCustomCollectableFieldsProvider(sValue, fieldUID);
				}
				
				BuildFormHandler.this.valuelistprovider = valuelistprovider;
			}

			@Override
			public void endElement(String sUriNameSpace, String sSimpleName, String sQualifiedName) {
				assert BuildFormHandler.this.valuelistprovider != null;

				new LayoutComponentDelegator(stack.peekComponent()) {
					@Override
					public void foundLC(LayoutComponent lc) {
						if (lc.getComponentProperties() != null) {
							for (Property pt : lc.getComponentProperties()) {
								if (pt.name.equals(sProperty)) {
									lc.setProperty(sProperty, BuildFormHandler.this.valuelistprovider);
									break;
								}
							}
						}
					}
				};
				BuildFormHandler.this.valuelistprovider = null;
			}
		}

		/**
		 * inner class <code>ParameterElementProcessor</code>. Processes a parameter element.
		 */
		private class ParameterElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				final String sName = attributes.getValue(ATTRIBUTE_NAME);
				final String sValue = attributes.getValue(ATTRIBUTE_VALUE);
				
				/** @todo this _still_ must be changed if <parameter> is used in other than for valuelist-provider */
				assert BuildFormHandler.this.valuelistprovider != null;

				final Object oValue;
				if (UID.isStringifiedUID(sValue))
					oValue = UID.parseUID(sValue);
				else if (layoutMLParser.getEntityUID(sValue) != null)
					oValue = layoutMLParser.getEntityUID(sValue); 
				// getFieldUIDFromCollectableEntity not necessary any more... 
				// (returns always null if sValue is no stringified UID) 
//				else if (getFieldUIDFromCollectableEntity(sValue) != null)
//					oValue = getFieldUIDFromCollectableEntity(sValue);
				else
					oValue = sValue;
				BuildFormHandler.this.valuelistprovider.setParameter(sName, oValue);
			}
		}

		/**
		 * inner class <code>PropertyElementProcessor</code>. Processes a property element.
		 */
		private class PropertyElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				final String sName = attributes.getValue(ATTRIBUTE_NAME);
				final String oValue = attributes.getValue(ATTRIBUTE_VALUE);

				if (BuildFormHandler.this.subformcolumn != null) {
					BuildFormHandler.this.subformcolumn.setProperty(sName, oValue);
				} else {
					//NUCLEUSINT-1159
					//NUCLOSINT-743
					ComponentBuilder c = stack.peekComponentBuilder();
					LayoutComponentDelegator lcd = new LayoutComponentDelegator(stack.peekComponent()) {
						@Override
						public void foundLC(LayoutComponent lc) {
							if (lc.getComponentProperties() != null) {
								for (Property pt : lc.getComponentProperties()) {
									if (pt.name.equals(sName)) {
										if (Boolean.class.equals(pt.type) || boolean.class.equals(pt.type)) {
											lc.setProperty(sName, ATTRIBUTEVALUE_YES.equals(oValue));
										} else if (Integer.class.equals(pt.type) || int.class.equals(pt.type)) {
											lc.setProperty(sName, StringUtils.looksEmpty(oValue)? null : Integer.parseInt(oValue));
										} else {
											lc.setProperty(sName, oValue);
										}
										break;
									}
								}
							}
						}
					}; 
					if (lcd.found) {
						// do nothing here
					} else
					if (c.getComponent() instanceof JButton) {
						JButton button = ((JButton)c.getComponent());
						if (STATIC_BUTTON.STATE_CHANGE_ACTION.equals(button.getActionCommand())) {
							if (STATIC_BUTTON.STATE_CHANGE_ACTION_ARGUMENT.equals(sName)) {
								button.setActionCommand(button.getActionCommand() + "_" + STATIC_BUTTON.STATE_CHANGE_ACTION_ARGUMENT + "=" + oValue);
							}
						} else if (STATIC_BUTTON.EXECUTE_RULE_ACTION.equals(button.getActionCommand())) {
							if (STATIC_BUTTON.EXECUTE_RULE_ACTION_ARGUMENT.equals(sName)) {
								button.setActionCommand(button.getActionCommand() + "_" + STATIC_BUTTON.EXECUTE_RULE_ACTION_ARGUMENT + "=" + oValue);
							}
						} else if (STATIC_BUTTON.GENERATOR_ACTION.equals(button.getActionCommand())) {
							if (STATIC_BUTTON.GENERATOR_ACTION_ARGUMENT.equals(sName)) {
								button.setActionCommand(button.getActionCommand() + "_" + STATIC_BUTTON.GENERATOR_ACTION_ARGUMENT + "=" + oValue);
							}
						} else if (STATIC_BUTTON.HYPERLINK_ACTION.equals(button.getActionCommand())) {
							if (STATIC_BUTTON.HYPERLINK_ACTION_ARGUMENT.equals(sName)) {
								button.setActionCommand(button.getActionCommand() + "_" + STATIC_BUTTON.HYPERLINK_ACTION_ARGUMENT + "=" + oValue);
							}
						} else
							if (stack.peekComponentBuilder() instanceof CollectableComponentBuilder) {
								final CollectableComponentBuilder ccb = (CollectableComponentBuilder) stack.peekComponentBuilder();
								ccb.getCollectableComponent().setProperty(sName, oValue);
							}
					} else
					if (c.getComponent() instanceof Chart) {
						Chart chart = ((Chart)c.getComponent());
						chart.setProperty(sName, oValue);
					} else
					if (stack.peekComponentBuilder() instanceof CollectableComponentBuilder) {
						final CollectableComponentBuilder ccb = (CollectableComponentBuilder) stack.peekComponentBuilder();
						ccb.getCollectableComponent().setProperty(sName, oValue);
					}
				}
			}
		}

		/**
		 * inner class <code>DescriptionElementProcessor</code>. Processes a description element.
		 */
		private class DescriptionElementProcessor implements ElementProcessor {
			/**
			 * starts the accumulation of characters
			 * @param sUriNameSpace
			 * @param sSimpleName
			 * @param sQualifiedName
			 * @param attributes
			 */
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes) {
				// begin accumulating characters (in between the description element):
				BuildFormHandler.this.sbChars = new StringBuffer();
			}

			/**
			 * fetches the accumulated characters and sets the resulting String as the peek component's
			 * tooltip.
			 * @param sUriNameSpace
			 * @param sSimpleName
			 * @param sQualifiedName
			 */
			@Override
            public void endElement(String sUriNameSpace, String sSimpleName, String sQualifiedName) {
				// Note that we copy the String from the StringBuffer in order not to waste memory.
				final String sDescription = new String(BuildFormHandler.this.sbChars.toString());
				stack.peekComponent().setToolTipText(sDescription);
				BuildFormHandler.this.sbChars = null;
			}
		}	// inner class DescriptionElementProcessor

		/**
		 * inner class <code>BorderLayoutElementProcessor</code>. Processes a borderlayout element.
		 */
		private class BorderLayoutElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				final int iHGap = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_HGAP, 0);
				final int iVGap = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_VGAP, 0);

				stack.peekComponent().setLayout(new BorderLayout(iHGap, iVGap));
			}
		}	// inner class BorderLayoutElementProcessor

		/**
		 * inner class <code>FlowLayoutElementProcessor</code>. Processes a flowlayout element.
		 */
		private class FlowLayoutElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				final int iHGap = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_HGAP, 0);
				final int iVGap = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_VGAP, 0);

				final String sAlign = attributes.getValue(ATTRIBUTE_ALIGN);
				Integer iAlign = BuildFormHandler.this.mpFlowLayoutConstants.get(sAlign);
				if (iAlign == null) {
					iAlign = FlowLayout.CENTER;
				}

				stack.peekComponent().setLayout(new FlowLayout(iAlign, iHGap, iVGap));
			}
		}	// inner class FlowLayoutElementProcessor

		/**
		 * inner class <code>GridLayoutElementProcessor</code>. Processes a gridlayout element.
		 */
		private class GridLayoutElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				final int iColumns = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_COLUMNS, 0);
				final int iRows = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_ROWS, 0);
				final int iHGap = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_HGAP, 0);
				final int iVGap = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_VGAP, 0);

				stack.peekComponent().setLayout(new GridLayout(iRows, iColumns, iHGap, iVGap));
			}
		}	// inner class GridLayoutElementProcessor

		/**
		 * inner class <code>GridBagLayoutElementProcessor</code>. Processes a gridbaglayout element.
		 */
		private class GridBagLayoutElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes) {
				stack.peekComponent().setLayout(new GridBagLayout());
			}
		}	// inner class GridBagLayoutElementProcessor

		/**
		 * inner class <code>TableLayoutElementProcessor</code>. Processes a tablelayout element.
		 */
		private class TableLayoutElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes) throws SAXException {
				final char cSeparator = '|';
				String sCols = attributes.getValue(ATTRIBUTE_COLUMNS);
				String sRows = attributes.getValue(ATTRIBUTE_ROWS);

				try {
					double[] columns = StringUtils.getDoubleArrayFromString(sCols, cSeparator);
					double[] rows = StringUtils.getDoubleArrayFromString(sRows, cSeparator);

					double[][] tablelayoutDescription = { {}, {} };
					tablelayoutDescription[0] = columns;
					tablelayoutDescription[1] = rows;

					stack.peekComponent().setLayout(new TableLayout(tablelayoutDescription));
				}
				catch (NumberFormatException ex) {
					throw new SAXException("LayoutMLParser.17");
						//"Liste der Spalten und Zeilen eines TableLayouts darf nur Dezimalzahlen enthalten.");
				}
			}
		}	// inner class TableLayoutElementProcessor

		/**
		 * inner class <code>BoxLayoutElementProcessor</code>. Processes a boxlayout element.
		 */
		private class BoxLayoutElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {

				// axis:
				final String sAxis = attributes.getValue(ATTRIBUTE_AXIS);
				if (sAxis == null || sAxis.length() == 0) {
					throw new SAXException("LayoutMLParser.18");
						//"Das Attribut axis muss angegeben werden.");
				}

				final int iAxis;
				switch (Character.toUpperCase(sAxis.charAt(0))) {
					case 'X':
						iAxis = BoxLayout.X_AXIS;
						break;
					case 'Y':
						iAxis = BoxLayout.Y_AXIS;
						break;
					default:
						throw new SAXException("LayoutMLParser.19");
							//"x/y als Wert f\u00fcr das Attribut axis erwartet.");
				}

				final JComponent comp = stack.peekComponent();
				comp.setLayout(new BoxLayout(comp, iAxis));
			}
		}	// inner class BoxLayoutElementProcessor

		/**
		 * inner class <code>RowLayoutElementProcessor</code>. Processes a rowlayout element.
		 */
		private class RowLayoutElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {

				// gap:
				final int iGap = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_GAP, 0);

				// fill:
				boolean bFill = true;
				final String sFill = attributes.getValue(ATTRIBUTE_FILLVERTICALLY);
				if (sFill != null) {
					bFill = sFill.equals(ATTRIBUTEVALUE_YES);
				}

				stack.peekComponent().setLayout(new LineLayout(LineLayout.HORIZONTAL, iGap, bFill));
			}
		}	// inner class RowLayoutElementProcessor

		/**
		 * inner class <code>ColumnLayoutElementProcessor</code>. Processes a rowlayout element.
		 */
		private class ColumnLayoutElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {

				// gap:
				final int iGap = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_GAP, 0);

				// fill:
				boolean bFill = true;
				final String sFill = attributes.getValue(ATTRIBUTE_FILLHORIZONTALLY);
				if (sFill != null) {
					bFill = sFill.equals(ATTRIBUTEVALUE_YES);
				}

				stack.peekComponent().setLayout(new LineLayout(LineLayout.VERTICAL, iGap, bFill));
			}
		}	// inner class ColumnLayoutElementProcessor

		/**
		 * inner class <code>BorderLayoutConstraintsElementProcessor</code>. Processes a borderlayout-constraints element.
		 */
		private class BorderLayoutConstraintsElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes) {
				final String sPosition = attributes.getValue(ATTRIBUTE_POSITION);
				final String sConstraint = BuildFormHandler.this.mpBorderLayoutConstraints.get(sPosition);
				stack.peekComponentBuilder().setConstraints(sConstraint);
			}
		}	// inner class BorderLayoutConstraintsElementProcessor

		/**
		 * inner class <code>TabbedPaneConstraintsElementProcessor</code>. Processes a tabbedpane-constraints element.
		 */
		private class TabbedPaneConstraintsElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes) {
				final TabbedPaneConstraints tbc = new TabbedPaneConstraints();
				tbc.sTitle = attributes.getValue(ATTRIBUTE_TITLE);

				final String sEnabled = attributes.getValue(ATTRIBUTE_ENABLED);
				if (sEnabled != null) {
					tbc.bEnabled = sEnabled.equals(ATTRIBUTEVALUE_YES);
				}

				final String sInternalname = attributes.getValue(ATTRIBUTE_INTERNALNAME);
				if(sInternalname != null) {
					tbc.sInternalname = sInternalname;
				}
				final String sMnemonic = attributes.getValue(ATTRIBUTE_MNEMONIC);
				if(sMnemonic != null) {
					tbc.sMnemonic = sMnemonic;
				}

				localizationHandler = tbc;
				handleLegacyResourceId(attributes);

				stack.peekComponentBuilder().setConstraints(tbc);
			}

			@Override
			public void endElement(String sUriNameSpace, String sSimpleName, String sQualifiedName) {
				localizationHandler = null;
				super.endElement(sUriNameSpace, sSimpleName, sQualifiedName);
			}
		}	// inner class TabbedPaneConstraintsElementProcessor

		/**
		 * inner class <code>SplitPaneConstraintsElementProcessor</code>. Processes a splitpane-constraints element.
		 */
		private class SplitPaneConstraintsElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes) {
				final String sConstraints = BuildFormHandler.this.mpSplitPaneConstraints.get(attributes.getValue(ATTRIBUTE_POSITION));

				stack.peekComponentBuilder().setConstraints(sConstraints);
			}
		}	// inner class SplitPaneConstraintsElementProcessor

		/**
		 * inner class <code>ElementGridBagLayoutConstraintsProcessor</code>. Processes a gridbaglayout-constraints element.
		 */
		private class GridBagConstraintsElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				final int iGridX = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_GRIDX, GridBagConstraints.RELATIVE);
				final int iGridY = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_GRIDY, GridBagConstraints.RELATIVE);
				final int iGridWidth = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_GRIDWIDTH, 1);
				final int iGridHeight = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_GRIDHEIGHT, 1);

				final double dWeightX = BuildFormHandler.getDoubleValue(attributes, ATTRIBUTE_WEIGHTX, 0.0);
				final double dWeightY = BuildFormHandler.getDoubleValue(attributes, ATTRIBUTE_WEIGHTY, 0.0);

				final String sAnchor = attributes.getValue(ATTRIBUTE_ANCHOR);
				final int iAnchor = (sAnchor == null) ? GridBagConstraints.CENTER : BuildFormHandler.this.mpGridBagConstraintAnchor.get(sAnchor);

				final String sFill = attributes.getValue(ATTRIBUTE_FILL);
				final int iFill = (sFill == null) ? GridBagConstraints.NONE : BuildFormHandler.this.mpGridBagConstraintFill.get(sFill);

				final int iInsetTop = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_INSETTOP, 0);
				final int iInsetLeft = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_INSETLEFT, 0);
				final int iInsetBottom = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_INSETBOTTOM, 0);
				final int iInsetRight = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_INSETRIGHT, 0);

				final Insets insets = new Insets(iInsetTop, iInsetLeft, iInsetBottom, iInsetRight);

				final int iPadX = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_PADX, 0);
				final int iPadY = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_PADY, 0);

				final GridBagConstraints gbc = new GridBagConstraints(iGridX, iGridY, iGridWidth, iGridHeight, dWeightX, dWeightY,
						iAnchor, iFill, insets, iPadX, iPadY);
				stack.peekComponentBuilder().setConstraints(gbc);
			}
		}	// inner class GridBagConstraintsElementProcessor

		/**
		 * inner class <code>ElementGridBagLayoutConstraintsProcessor</code>. Processes a gridbaglayout-constraints element.
		 */
		private class TableLayoutConstraintsElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				final TableLayoutConstraints constraint = new TableLayoutConstraints();
				constraint.hAlign = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_HALIGN, 0);
				constraint.vAlign = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_VALIGN, 0);
				constraint.col1 = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_COL1, 0);
				constraint.row1 = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_ROW1, 0);
				constraint.col2 = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_COL2, 0);
				constraint.row2 = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_ROW2, 0);

				stack.peekComponentBuilder().setConstraints(constraint);
			}
		}	// inner class ElementGridBagLayoutConstraintsProcessor

		/**
		 * inner class <code>MinimumSizeElementProcessor</code>. Processes a minimum-size element.
		 */
		private class MinimumSizeElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				final int iWidth = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_WIDTH, 0);
				final int iHeight = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_HEIGHT, 0);

				stack.peekComponent().setMinimumSize(new Dimension(iWidth, iHeight));
			}
		}	// inner class MinimumSizeElementProcessor

		/**
		 * inner class <code>PreferredSizeElementProcessor</code>. Processes a preferred-size element.
		 */
		private class PreferredSizeElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				final int iWidth = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_WIDTH, 0);
				final int iHeight = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_HEIGHT, 0);

				stack.peekComponent().setPreferredSize(new Dimension(iWidth, iHeight));
			}
		}	// inner class PreferredSizeElementProcessor

		/**
		 * inner class <code>StrictSizeElementProcessor</code>. Processes a strict-size element.
		 */
		private class StrictSizeElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				final int iWidth = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_WIDTH, 0);
				final int iHeight = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_HEIGHT, 0);

				if (stack.peekComponent() instanceof StrictSizeComponent) {
					((StrictSizeComponent)stack.peekComponent()).setStrictSize(new Dimension(iWidth, iHeight));
				} else {
					throw new NuclosFatalException("StrictSize not available for class " + stack.peekComponent().getClass().getName());
				}
			}
		}	// inner class StrictSizeElementProcessor

		/**
		 * inner class <code>PropertySizeElementProcessor</code>. Processes a property-size element.
		 */
		private class PropertySizeElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				final String sProperty = attributes.getValue(ATTRIBUTE_NAME);
				final int iWidth = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_WIDTH, 0);
				final int iHeight = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_HEIGHT, 0);

				new LayoutComponentDelegator(stack.peekComponent()) {
					@Override
					public void foundLC(LayoutComponent lc) {
						if (lc.getComponentProperties() != null) {
							for (Property pt : lc.getComponentProperties()) {
								if (pt.name.equals(sProperty)) {
									lc.setProperty(sProperty, new Dimension(iWidth, iHeight));
								}
							}
						}
					}
				};
			}
		}	// inner class PropertySizeElementProcessor
		
		
		
		

		/**
		 * inner class <code>ClearBorderElementProcessor</code>. Processes a clear-border element.
		 */
		private class ClearBorderElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {

				stack.peekComponent().setBorder(null);
			}
		}	// inner class ClearBorderElementProcessor

		/**
		 * inner class <code>EmptyBorderElementProcessor</code>. Processes an empty-border element.
		 */
		private class EmptyBorderElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				final int iTop = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_TOP, 0);
				final int iLeft = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_LEFT, 0);
				final int iBottom = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_BOTTOM, 0);
				final int iRight = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_RIGHT, 0);

				BuildFormHandler.this.addBorder(BorderFactory.createEmptyBorder(iTop, iLeft, iBottom, iRight));
			}
		}	// inner class EmptyBorderElementProcessor

		/**
		 * inner class <code>EtchedBorderElementProcessor</code>. Processes an etched-border element.
		 */
		private class EtchedBorderElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes) {
				int iType = BevelBorder.LOWERED;	// default;
				final String sType = attributes.getValue(ATTRIBUTE_TYPE);
				if (sType != null && sType.equals(ATTRIBUTEVALUE_RAISED)) {
					iType = BevelBorder.RAISED;
				}
				BuildFormHandler.this.addBorder(BorderFactory.createEtchedBorder(iType));
			}
		}	// inner class EtchedBorderElementProcessor

		/**
		 * inner class <code>BevelBorderElementProcessor</code>. Processes a bevel-border element.
		 */
		private class BevelBorderElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes) {
				int iType = BevelBorder.LOWERED;	// default;
				final String sType = attributes.getValue(ATTRIBUTE_TYPE);
				if (sType != null && sType.equals(ATTRIBUTEVALUE_RAISED)) {
					iType = BevelBorder.RAISED;
				}
				BuildFormHandler.this.addBorder(BorderFactory.createBevelBorder(iType));
			}
		}	// inner class BevelBorderElementProcessor

		/**
		 * inner class <code>LineBorderElementProcessor</code>. Processes a line-border element.
		 */
		private class LineBorderElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				final int iRed = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_RED, 0);
				final int iGreen = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_GREEN, 0);
				final int iBlue = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_BLUE, 0);
				final Color color = new Color(iRed, iGreen, iBlue);

				final int iThickness = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_THICKNESS, 1);

				BuildFormHandler.this.addBorder(BorderFactory.createLineBorder(color, iThickness));
			}
		}	// inner class LineBorderElementProcessor

		/**
		 * inner class <code>TitledBorderElementProcessor</code>. Processes a titled-border element.
		 */
		private class TitledBorderElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes) {
				String sTitle = attributes.getValue(ATTRIBUTE_TITLE);

				final TitledBorder titledBorder = BorderFactory.createTitledBorder(sTitle);

				localizationHandler = new LocalizationHandler() {
					@Override
					public void setTranslation(String translation) {
						titledBorder.setTitle(translation);
					}
				};
				handleLegacyResourceId(attributes);

				BuildFormHandler.this.addBorder(titledBorder);
			}

			@Override
			public void endElement(String sUriNameSpace, String sSimpleName, String sQualifiedName) {
				localizationHandler = null;
				super.endElement(sUriNameSpace, sSimpleName, sQualifiedName);
			}
		}	// inner class TitledBorderElementProcessor

		/**
		 * inner class <code>FontElementProcessor</code>. Processes a font element.
		 */
		private class FontElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes) throws SAXException {
				final int iRelativeSize = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_SIZE, 0);

				JComponent comp = stack.peekComponent();

				if (comp instanceof LabeledComponent) {
					if (comp instanceof LabeledTextArea) {
						// NUCLEUSINT-276 NUCLEUSINT-192
						setFontSize((((LabeledTextArea) comp).getJLabel()), iRelativeSize);
						setFontSize((((LabeledTextArea) comp).getJTextArea()), iRelativeSize);
					} else {
						// NUCLEUSINT-276 NUCLEUSINT-192
						setFontSize((((LabeledComponent) comp).getJLabel()), iRelativeSize);
						setFontSize((((LabeledComponent) comp).getControlComponent()), iRelativeSize);
					}
				} else if (comp instanceof TitledSeparator) {
					// NUCLEUSINT-276 NUCLEUSINT-192
					setFontSize((((TitledSeparator) comp).getJLabel()), iRelativeSize);
				} else {
					setFontSize(comp, iRelativeSize);
				}
			}

			/**
			 * Externalised for avoing duplicate code
			 *
			 * NUCLEUSINT-276
			 * @param comp
			 * @param iRelativeSize
			 */
			private void setFontSize(JComponent comp, int iRelativeSize) {
				final Font fontOld = comp.getFont();
				if (fontOld != null) {
					final float fNewFontSize = fontOld.getSize() + iRelativeSize;
					final Font fontNew = fontOld.deriveFont(fNewFontSize);
					comp.setFont(fontNew);
				}
			}
		}	// inner class FontElementProcessor

		/**
		 * inner class <code>TextFormatElementProcessor</code>. Processes a text-format element.
		 */
		private class TextFormatElementProcessor extends AbstractElementProcessor {
			@Override
			public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes) throws SAXException {
				JComponent comp = stack.peekComponent();
				LabeledComponent.ComponentLabel lab = null;
				if (comp instanceof LabeledComponent) {
					lab = ((LabeledComponent) comp).getJLabel();
				} else if (comp instanceof LabeledComponent.ComponentLabel) {
					lab = (LabeledComponent.ComponentLabel) comp;
				}
				if (lab != null) {
					final boolean bold = BuildFormHandler.getBooleanValue(attributes, ATTRIBUTE_BOLD);
					final boolean italic = BuildFormHandler.getBooleanValue(attributes, ATTRIBUTE_ITALIC);
					final boolean underline = BuildFormHandler.getBooleanValue(attributes, ATTRIBUTE_UNDERLINE);
					lab.setBold(bold);
					lab.setItalic(italic);
					lab.setUnderline(underline);
				}
			}

		}	// inner class TextFormatElementProcessor

		/**
		 * inner class <code>TextColorElementProcessor</code>. Processes a text-color element.
		 */
		private class TextColorElementProcessor extends AbstractElementProcessor {
			@Override
			public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				final int iRed = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_RED, 0);
				final int iGreen = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_GREEN, 0);
				final int iBlue = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_BLUE, 0);
				final Color color = new Color(iRed, iGreen, iBlue);

				final JComponent comp = stack.peekComponent();
				comp.setForeground(color);
			}
		}	// inner class TextColorElementProcessor

		/**
		 * inner class <code>PropertyFontElementProcessor</code>. Processes a property font element.
		 */
		private class PropertyFontElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes) throws SAXException {
				final String sProperty = attributes.getValue(ATTRIBUTE_NAME);
				final int iRelativeSize = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_SIZE, 0);

				new LayoutComponentDelegator(stack.peekComponent()) {
					@Override
					public void foundLC(LayoutComponent lc) {
						if (lc.getComponentProperties() != null) {
							for (Property pt : lc.getComponentProperties()) {
								if (pt.name.equals(sProperty)) {
									lc.setProperty(sProperty, getFont(stack.peekComponent(), iRelativeSize));
									break;
								}
							}
						}
					}
				};
			}

			private Font getFont(JComponent comp, int iRelativeSize) {
				final Font fontOld = comp.getFont();
				if (fontOld != null) {
					final float fNewFontSize = fontOld.getSize() + iRelativeSize;
					final Font fontNew = fontOld.deriveFont(fNewFontSize);
					return fontNew;
				}
				return fontOld;
			}
		}	// inner class FontElementProcessor

		/**
		 * inner class <code>BackgroundElementProcessor</code>. Processes a background element.
		 */
		private class BackgroundElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				final int iRed = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_RED, 0);
				final int iGreen = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_GREEN, 0);
				final int iBlue = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_BLUE, 0);
				final Color color = new Color(iRed, iGreen, iBlue);

				final JComponent comp = stack.peekComponent();
				comp.setBackground(color);
				comp.setOpaque(true);
				// set opaque implicitly in order so the background color is shown
			}
		}	// inner class BackgroundElementProcessor

		/**
		 * inner class <code>PropertyColorElementProcessor</code>. Processes a property color.
		 */
		private class PropertyColorElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				final String sProperty = attributes.getValue(ATTRIBUTE_NAME);
				final int iRed = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_RED, 0);
				final int iGreen = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_GREEN, 0);
				final int iBlue = BuildFormHandler.getIntValue(attributes, ATTRIBUTE_BLUE, 0);

				new LayoutComponentDelegator(stack.peekComponent()) {
					@Override
					public void foundLC(LayoutComponent lc) {
						if (lc.getComponentProperties() != null) {
							for (Property pt : lc.getComponentProperties()) {
								if (pt.name.equals(sProperty)) {
									lc.setProperty(sProperty, new Color(iRed, iGreen, iBlue));
								}
							}
						}
					}	
				};
			}
		}	// inner class PropertyColorElementProcessor

		/**
		 * inner class <code>DependencyElementProcessor</code>. Processes a dependency element.
		 */
		private class DependencyElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				final UID sDependantFieldName = UID.parseUID(attributes.getValue(ATTRIBUTE_DEPENDANTFIELD));
				final UID sDependsOnFieldName = UID.parseUID(attributes.getValue(ATTRIBUTE_DEPENDSONFIELD));
				BuildFormHandler.this.addDependency(sDependantFieldName, sDependsOnFieldName);
			}
		}

		/**
		 * inner class <code>RuleElementProcessor</code>. Processes a rule element.
		 */
		private class RuleElementProcessor implements ElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				BuildFormHandler.this.rule = new Rule();
			}

			@Override
            public void endElement(String sUriNameSpace, String sSimpleName, String sQualifiedName) throws SAXException {
				BuildFormHandler.this.rule.finish();
				BuildFormHandler.this.rule = null;
			}
		}

		/**
		 * inner class <code>EventElementProcessor</code>. Processes an event element.
		 */
		private class EventElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				BuildFormHandler.this.rule.event = new Event();
				BuildFormHandler.this.rule.event.sType = attributes.getValue(ATTRIBUTE_TYPE);

				final UID entityUID = layoutMLParser.getEntityUID(attributes.getValue(ATTRIBUTE_ENTITY));
				if (entityUID != null) {
					final SubForm subform = stack.getSubFormForEntity(entityUID);
					BuildFormHandler.this.rule.event.subform = subform;
				}
				BuildFormHandler.this.rule.event.sSourceComponentName = layoutMLParser.getFieldUID(attributes.getValue(ATTRIBUTE_SOURCECOMPONENT));
			}
		}

		private class TransferLookedUpValueElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				final UID entityUID = layoutMLParser.getEntityUID(attributes.getValue(ATTRIBUTE_ENTITY));
				final UID sourcefieldUID = layoutMLParser.getFieldUID(attributes.getValue(ATTRIBUTE_SOURCEFIELD));
				final UID targetComponentUID = layoutMLParser.getFieldUID(attributes.getValue(ATTRIBUTE_TARGETCOMPONENT));
				final TransferLookedUpValueAction act = new TransferLookedUpValueAction(targetComponentUID, sourcefieldUID);
				BuildFormHandler.this.rule.collActions.add(act);
			}
		}

		private class ReinitSubformElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				final UID entityUID = layoutMLParser.getEntityUID(attributes.getValue(ATTRIBUTE_ENTITY));
				final UID targetComponentUID = layoutMLParser.getFieldUID(attributes.getValue(ATTRIBUTE_TARGETCOMPONENT));
				final ReinitSubformAction act = new ReinitSubformAction(targetComponentUID, entityUID);
				BuildFormHandler.this.rule.collActions.add(act);
			}
		}
		
		private class ClearElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				final UID entityUID = layoutMLParser.getEntityUID(attributes.getValue(ATTRIBUTE_ENTITY));
				final UID targetComponentUID = layoutMLParser.getFieldUID(attributes.getValue(ATTRIBUTE_TARGETCOMPONENT));
				final ClearAction act = new ClearAction(targetComponentUID);
				BuildFormHandler.this.rule.collActions.add(act);
			}
		}

		private class RefreshValueListElementProcessor extends AbstractElementProcessor {
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {
				String sParameterNameForSourceComponent = attributes.getValue(ATTRIBUTE_PARAMETER_FOR_SOURCECOMPONENT);
				if (sParameterNameForSourceComponent == null) {
					sParameterNameForSourceComponent = "relatedId";
				}
				final UID entityUID = layoutMLParser.getEntityUID(attributes.getValue(ATTRIBUTE_ENTITY));
				final UID targetComponentUID = layoutMLParser.getFieldUID(attributes.getValue(ATTRIBUTE_TARGETCOMPONENT));
				final RefreshValueListAction act = new RefreshValueListAction(
						targetComponentUID, entityUID,
						sParameterNameForSourceComponent);
				BuildFormHandler.this.rule.collActions.add(act);
			}
		}

		private class ScriptElementProcessor implements ElementProcessor {

			private NuclosScript script;

			@Override
			public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes) throws SAXException {
				sbChars = new StringBuffer();
				script = new NuclosScript();
				script.setLanguage(attributes.getValue(ATTRIBUTE_LANGUAGE));
			}

			@Override
			public void endElement(String sUriNameSpace, String sSimpleName, String sQualifiedName) throws SAXException {
				script.setSource(sbChars.toString().trim());
				if (stack.peekComponent() instanceof SubForm) {
					SubForm subform = (SubForm) stack.peekComponent();
					if (ELEMENT_NEW_ENABLED.equals(sQualifiedName)) {
						subform.setNewEnabledScript(script);
					}
					else if (ELEMENT_EDIT_ENABLED.equals(sQualifiedName)) {
						subform.setEditEnabledScript(script);
					}
					else if (ELEMENT_DELETE_ENABLED.equals(sQualifiedName)) {
						subform.setDeleteEnabledScript(script);
					}
					else if (ELEMENT_CLONE_ENABLED.equals(sQualifiedName)) {
						subform.setCloneEnabledScript(script);
					}
					else if (ELEMENT_DYNAMIC_ROW_COLOR.equals(sQualifiedName)) {
						subform.setDynamicRowColorScript(script);
					}
				}
				else if (stack.peekComponent() instanceof JMatrixComponent) {
					JMatrixComponent matrix = (JMatrixComponent) stack.peekComponent();
					if (ELEMENT_NEW_ENABLED.equals(sQualifiedName)) {
						matrix.setNewEnabledScript(script);
					}
					else if (ELEMENT_EDIT_ENABLED.equals(sQualifiedName)) {
						matrix.setEditEnabledScript(script);
					}
					else if (ELEMENT_DELETE_ENABLED.equals(sQualifiedName)) {
						matrix.setDeleteEnabledScript(script);
					}
					else if (ELEMENT_CLONE_ENABLED.equals(sQualifiedName)) {
						matrix.setCloneEnabledScript(script);
					}
				}
				else {
					final CollectableComponentBuilder ccb = (CollectableComponentBuilder) stack.peekComponentBuilder();
					assert ccb != null;
					if (ELEMENT_ENABLED.equals(sQualifiedName)) {
						ccb.clctcomp.setEnabledScript(script);
						ccb.clctcomp.setMultiEditable(false);
					}
				}
				sbChars = null;
			}

		}

		private class PropertyScriptElementProcessor implements ElementProcessor {

			private String sProperty;
			private NuclosScript script;

			@Override
			public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes) throws SAXException {
				sProperty = attributes.getValue(ATTRIBUTE_NAME);
				sbChars = new StringBuffer();
				script = new NuclosScript();
				script.setLanguage(attributes.getValue(ATTRIBUTE_LANGUAGE));
			}

			@Override
			public void endElement(String sUriNameSpace, String sSimpleName, String sQualifiedName) throws SAXException {
				script.setSource(sbChars.toString().trim());

				new LayoutComponentDelegator(stack.peekComponent()) {
					@Override
					public void foundLC(LayoutComponent lc) {
						if (lc.getComponentProperties() != null) {
							for (Property pt : lc.getComponentProperties()) {
								if (pt.name.equals(sProperty)) {
									lc.setProperty(sProperty, script);
									break;
								}
							}
						}
					}
				};
				sbChars = null;
			}

		}

		/**
		 * inner class <code>LayoutComponentElementProcessor</code>. Processes a layout component element.
		 */
		class LayoutComponentElementProcessor extends ComponentElementProcessor {

			public LayoutComponentElementProcessor() {
			}

			/**
			 * constructs a <code>LayoutComponent</code>, configures it according to the XML attributes
			 * and pushes it on the stack.
			 * @param sUriNameSpace
			 * @param sSimpleName
			 * @param sQualifiedName
			 * @param attributes
			 */
			@Override
            public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {

				// name: (used for preferences)
				final String sName = attributes.getValue(ATTRIBUTE_NAME);
				// class:
				final String sClass = attributes.getValue(ATTRIBUTE_CLASS);
				LayoutComponentFactory<?> layoutComponentFactory = null;
				if (sClass != null) {
					NucletComponentRepository nucletComponentRepository
						= (NucletComponentRepository)SpringApplicationContextHolder.getBean("nucletComponentRepository");
					 
					for (LayoutComponentFactory lcf : nucletComponentRepository.getLayoutComponentFactories()) {
						if (lcf.getClass().getName().equals(sClass)) {
							layoutComponentFactory = lcf;
						}
					}
					if (layoutComponentFactory == null) {
						stack.addComponent(new JLabel(String.format("LayoutComponent %s not found", sClass)));
						return;
					}
					
				} else {
					stack.addComponent(new JLabel("LayoutComponent class must not be null"));
					return;
				}
			
				final UID sEntityName = clcte.getUID();
				final LayoutComponentHolder holder = new LayoutComponentHolder(layoutComponentFactory.newInstance(new LayoutComponentContextImpl(sEntityName, LayoutComponentType.DETAIL)), false);
				
				// add to layout component provider
				layoutComponentsProvider.addComponent(holder.getLayoutComponent());
				
				holder.setName(sName);
				stack.addComponent(holder);
			}
		}	// inner class LayoutComponentElementProcessor

		/**
		 * inner class <code>WebAddonElementProcessor</code>.
		 */
		class WebAddonElementProcessor extends ComponentElementProcessor {

			public WebAddonElementProcessor() {
			}

			/**
			 * Ignores a web addon element.
			 * @param sUriNameSpace
			 * @param sSimpleName
			 * @param sQualifiedName
			 * @param attributes
			 */
			@Override
			public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
					throws SAXException {

				// desktop client completely ignores web addons...

				stack.addComponent(new JPanel());
			}
		}	// inner class LayoutComponentElementProcessor

		/**
		 * called by the underlying SAX parser when a start element event occurs.
		 * Maps the element to the corresponding element processor and delegates the processing to
		 * <code>startElement()</code> method of the element processor.
		 * @param sUriNameSpace
		 * @param sSimpleName
		 * @param sQualifiedName
		 * @param attributes
		 * @throws SAXException
		 */
		@Override
		public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
				throws SAXException {
			final ElementProcessor elementProcessor = mpElementProcessors.get(sQualifiedName);
			if (elementProcessor != null) {
				elementProcessor.startElement(sUriNameSpace, sSimpleName, sQualifiedName, attributes);
			}
		}

		/**
		 * called by the underlying SAX parser when an end element event occurs.
		 * Maps the element to the corresponding element processor and delegates the processing to
		 * <code>endElement()</code> method of the element processor.
		 * @param sUriNameSpace
		 * @param sSimpleName
		 * @param sQualifiedName
		 * @throws SAXException
		 */
		@Override
		public void endElement(String sUriNameSpace, String sSimpleName, String sQualifiedName) throws SAXException {
			final ElementProcessor elementProcessor = mpElementProcessors.get(sQualifiedName);
			if (elementProcessor != null) {
				elementProcessor.endElement(sUriNameSpace, sSimpleName, sQualifiedName);
			}
		}

		/**
		 * called by the underlying SAX parser when "free characters" are parsed.
		 * They are accumulated here.
		 * @param ac
		 * @param start
		 * @param length
		 * @throws SAXException
		 */
		@Override
		public void characters(char[] ac, int start, int length) throws SAXException {
			if (this.sbChars != null) {
				this.sbChars.append(ac, start, length);
			}
		}

	}	// class BuildFormHandler

}	// class LayoutMLParser
