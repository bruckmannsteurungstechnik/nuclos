//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.matrix;

import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.EventObject;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractCellEditor;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableCellEditor;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.NuclosCollectableComboBox;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.ui.DateChooser;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.collect.component.custom.FileChooserComponent;
import org.nuclos.client.ui.labeled.LabeledComponentSupport;
import org.nuclos.client.ui.matrix.JMatrixComponent.Column;
import org.nuclos.common.CollectableEntityFieldWithEntity;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.layoutml.LayoutMLConstants;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

public class MatrixSelectCellEditor extends AbstractCellEditor implements TableCellEditor {
	
	Logger LOG = Logger.getLogger(getClass());
	
	MatrixCollectable myValue;
	
	List<ChangeListener> lstchangelistener;
	
	List<Icon> lstIcons;	
	
	Integer iNumberState;
	
	Object cellValue;

	String cellInputType;
	boolean editable = true;

	Map<UID, Column> mpColumns = new LinkedHashMap<UID, Column>();

	String matrixCellValueDataType;

	public MatrixSelectCellEditor(String cellInputType, String matrixCellValueDataType, Integer iNumberState) {
		this.cellInputType = cellInputType;
		this.matrixCellValueDataType = matrixCellValueDataType;

		if(cellInputType == null || LayoutMLConstants.ATTRIBUTE_CELL_INPUT_TYPE_CHECKICON.equals(cellInputType)) {
			this.iNumberState = iNumberState;
			if(this.iNumberState > 3) {
				this.iNumberState = 3;
			}
			initIcons();
		}
	}
	
	
	protected void initIcons() {
		
		Icon iconPlus =  Icons.getInstance().getIconPlus16();
		Icon iconMinus = Icons.getInstance().getIconMinus16();
		
		List<Icon> lstIconsTmp = new ArrayList<Icon>();
		
		lstIconsTmp.add(Icons.getInstance().getIconEmpty16());
		lstIconsTmp.add(iconPlus);
		lstIconsTmp.add(iconMinus);
		
		lstIcons = new ArrayList<Icon>();
		for(int i = 0; i < iNumberState; i++) {
			lstIcons.add(lstIconsTmp.get(i));
		}
		
	}	
	
	public void setColumns(Map<UID, Column> mpColumns) {
		this.mpColumns = mpColumns;
	}
	
	
	@Override
	public Object getCellEditorValue() {
		if(myValue == null) {
			myValue = new MatrixCollectable(null);
		}
		return myValue;
	}

	@Override
	public boolean isCellEditable(EventObject anEvent) {
		return editable;
	}

	@Override
	public boolean shouldSelectCell(EventObject anEvent) {		
		return true;
	}

	@Override
	public boolean stopCellEditing() {
		super.stopCellEditing();
		return true;
	}

	@Override
	public void cancelCellEditing() {
		super.cancelCellEditing();		
	}
	
	public void setChangeListener(List<ChangeListener> lstchangelistener) {
		this.lstchangelistener = lstchangelistener;
	}

	@Override
	public void addCellEditorListener(CellEditorListener l) {
		super.addCellEditorListener(l);		
	}

	@Override
	public void removeCellEditorListener(CellEditorListener l) {
		super.removeCellEditorListener(l);		
	}

	private Component getTableCellEditorComponentForMatrixCell(MatrixCollectable cl, Object value) {
		if (LayoutMLConstants.ATTRIBUTE_CELL_INPUT_TYPE_TEXTFIELD.equals(cellInputType)) { // textfield
			String metaDataType = cl.getDataType();
			Object tableCellValue = cl.getValue(UID.UID_NULL);

			final boolean isInt = tableCellValue instanceof Integer ||
					"java.lang.Integer".equals(metaDataType) || "java.lang.Integer".equals(matrixCellValueDataType);
			final boolean isLong = tableCellValue instanceof Long ||
					"java.lang.Long".equals(metaDataType) || "java.lang.Long".equals(matrixCellValueDataType);
			final boolean isDouble = tableCellValue instanceof Double ||
					"java.lang.Double".equals(metaDataType) || "java.lang.Double".equals(matrixCellValueDataType);

			final JTextField textField = new JTextField();
			textField.setText(value.toString());
			textField.setHorizontalAlignment(JTextField.RIGHT);

			// update myValue if text field value has changed
			textField.getDocument().addDocumentListener(
					new DocumentListener() {
						public void changedUpdate(DocumentEvent e) {
							update();
						}
						public void removeUpdate(DocumentEvent e) {
							update();
						}
						public void insertUpdate(DocumentEvent e) {
							update();
						}
						private void update() {
							if (isInt) {
								Integer intValue = null;
								if (textField.getText() != null && textField.getText().length() != 0) {
									intValue = Integer.parseInt(textField.getText());
								}
								myValue = new MatrixCollectable((Long) cl.getId(), intValue, cl.getVersion());
							} else if (isLong) {
								Long lValue = null;
								if (textField.getText() != null && textField.getText().length() != 0) {
									lValue = Long.parseLong(textField.getText());
								}
								myValue = new MatrixCollectable((Long) cl.getId(), lValue, cl.getVersion());
							} else if (isDouble) {
								Double doubleValue = null;
								if (textField.getText() != null && textField.getText().length() != 0) {
									doubleValue = Double.parseDouble(textField.getText());
								}
								myValue = new MatrixCollectable((Long) cl.getId(), doubleValue, cl.getVersion());

							} else {
								myValue = new MatrixCollectable((Long) cl.getId(), textField.getText(), cl.getVersion());
							}
						}
					}
			);

			return textField;

		} else { // check icon

			Integer iIconCount = (Integer) cl.getValue(UID.UID_NULL);
			if (iIconCount == null) {
				iIconCount = 0;
			}
			Icon icon = lstIcons.get(iIconCount);
			myValue = new MatrixCollectable((Long) cl.getId(), iIconCount, cl.getVersion());

			final MatrixSelectLabel lb = new MatrixSelectLabel(icon, myValue);
			lb.addMouseListener(new MouseAdapter() {

				@Override
				public void mousePressed(MouseEvent e) {
					Integer i = (Integer) myValue.getValue(UID.UID_NULL);
					if (i == null) {
						i = 0;
					}
					i++;
					if (i >= lstIcons.size()) {
						i = 0;
						myValue = new MatrixCollectable((Long) cl.getId(), null, cl.getVersion()); // set value to null so it won't be saved to DB
					} else {
						myValue = new MatrixCollectable((Long) cl.getId(), i, cl.getVersion());
					}

					Icon icon = lstIcons.get(i);
					lb.setIcon(icon);
					fireChangeEvent();
				}

			});
			lb.setOpaque(true);

			return lb;
		}
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, final Object value, boolean isSelected, int row, int column) {
		if (!(value instanceof MatrixCollectable)) {
			myValue = new MatrixCollectable(null);
			return new JLabel("");
		}

		final MatrixCollectable cl = (MatrixCollectable)value;
		Object tableCellValue = cl.getValue(UID.UID_NULL);
		myValue = new MatrixCollectable(tableCellValue);
		myValue.setYId(cl.getYId());

		if (cl.getEntityFieldMetaDataVO() == null) { // matrix cell - sub-sub-form field
			return getTableCellEditorComponentForMatrixCell(cl, value);
		}

			String metaDataType = cl.getEntityFieldMetaDataVO() != null ? cl.getEntityFieldMetaDataVO().getDataType() : null;
			if(cl.getDataType() != null) {
				metaDataType = cl.getDataType();
			}

			final boolean isInt = tableCellValue instanceof Integer || "java.lang.Integer".equals(metaDataType) || "java.lang.Integer".equals(matrixCellValueDataType);
			final boolean isLong = tableCellValue instanceof Long || "java.lang.Long".equals(metaDataType) || "java.lang.Long".equals(matrixCellValueDataType);
			final boolean isBoolean = tableCellValue instanceof Boolean || "java.lang.Boolean".equals(metaDataType) || "java.lang.Boolean".equals(matrixCellValueDataType);;
			final boolean isDateOrTime = tableCellValue instanceof Date || tableCellValue instanceof InternalTimestamp  || "java.util.Date".equals(metaDataType) || "org.nuclos.common2.InternalTimestamp".equals(metaDataType) || "java.lang.Date".equals(matrixCellValueDataType);;
			final boolean isFile = tableCellValue instanceof GenericObjectDocumentFile || "org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile".equals(metaDataType) || "org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile".equals(matrixCellValueDataType);;
			final boolean isString = tableCellValue instanceof String || "java.lang.String".equals(metaDataType) || "java.lang.String".equals(matrixCellValueDataType);

				if(isBoolean) {
					Boolean bValue = Boolean.FALSE;
					if(cl.getValue(UID.UID_NULL) != null) {
						bValue = (Boolean) cl.getValue(UID.UID_NULL);
					}
					final JCheckBox box = new JCheckBox();
					box.setHorizontalAlignment(SwingConstants.CENTER);
					box.setSelected(bValue);
					box.setHideActionText(true);
					box.addItemListener((ItemEvent e) -> {
							if(e.getStateChange() == ItemEvent.SELECTED) {
								box.setSelected(true);
							}
							else {
								box.setSelected(false);
							}
							myValue = new MatrixCollectable((Long)cl.getId(), box.isSelected(), cl.getVersion());
							myValue.setEntityFieldMetaDataVO(cl.getEntityFieldMetaDataVO());
							myValue.setField(cl.getField());
							myValue.setVO(cl.getVO());
							table.getModel().setValueAt(myValue, row, column);//
							fireChangeEvent();
					});
					return box;
				}
				else if((cl.getEntityFieldMetaDataVO() != null && cl.getEntityFieldMetaDataVO().getForeignEntity() != null)) { // reference field - combobox
					myValue = new MatrixCollectable((Long) cl.getId(), cl.getValue(UID.UID_NULL), cl.getVersion());
					myValue.setEntityFieldMetaDataVO(cl.getEntityFieldMetaDataVO());
					myValue.setField(cl.getField());
					myValue.setVO(cl.getVO());

					final UID entity = cl.getEntityFieldMetaDataVO().getForeignEntity();
					CollectableMasterDataEntity clctEntity = new CollectableMasterDataEntity(MetaProvider.getInstance().getEntity(entity));
					String sUid = cl.getEntityFieldMetaDataVO().getForeignEntityField();
					UID uFieldReference = UID.parseUID(sUid);
					final UID fieldUID = cl.getField();
					CollectableEntityFieldWithEntity clctef = new CollectableEntityFieldWithEntity(clctEntity, uFieldReference);
					NuclosCollectableComboBox box = new NuclosCollectableComboBox(clctef, false);

					JMatrixComponent.Column col = mpColumns.get(cl.getField());
					if(col.getValueListProvider() != null) {
						box.setValueListProvider(col.getValueListProvider());
						box.refreshValueList();
					}
					else {
						Collection<CollectableField> colFields = new ArrayList<>();

						Collection<MasterDataVO<Long>> colValues = MasterDataDelegate.getInstance().getMasterData(entity);

						for(MasterDataVO vo : colValues) {
							CollectableField cf = new CollectableValueIdField(vo.getPrimaryKey(), vo.getFieldValue(uFieldReference));
							colFields.add(cf);
						}
						box.setComboBoxModel(colFields);
					}

					CollectableField selected = null;
					if(cl.getVO() != null) {
						selected = new  CollectableValueIdField(cl.getVO().getFieldId(cl.getField()), cl.getValue(UID.UID_NULL));
					}
					box.getJComboBox().setSelectedItem(selected);

					box.getJComboBox().addItemListener((ItemEvent e) -> {
							if (!(e.getItem() instanceof CollectableValueIdField)) {
								return;
							}
							CollectableValueIdField field = (CollectableValueIdField)e.getItem();
							if (e.getStateChange() == ItemEvent.DESELECTED) {
								field = new CollectableValueIdField(null, null);
							}

								myValue = new MatrixCollectable((Long) cl.getId(), field.getValue(), cl.getVersion());
								myValue.setEntityFieldMetaDataVO(cl.getEntityFieldMetaDataVO());
								myValue.setField(cl.getField());

								if(cl.getVO() == null) {
									EntityObjectVO<Long> newVO = new EntityObjectVO<>(entity);
									myValue.setVO(newVO);
									cl.setVO(newVO);
								}
								else {
									myValue.setVO(cl.getVO());
								}
								myValue.getVO().setFieldId(fieldUID, (Long)field.getValueId());
								fireChangeEvent();

					});

					return box.getJComboBox();

				}
				else if(isDateOrTime) {
					Date date = (Date)cl.getValue(UID.UID_NULL);
					myValue = new MatrixCollectable((Long) cl.getId(), date, cl.getVersion());
					myValue.setEntityFieldMetaDataVO(cl.getEntityFieldMetaDataVO());
					myValue.setField(cl.getField());

					DateChooser datChooser = new DateChooser(new LabeledComponentSupport(), date);
					final JTextField textField = datChooser.getJTextField();

					textField.getDocument().addDocumentListener(new DocumentListener() {
						@Override
						public void removeUpdate(DocumentEvent e) {
							changeText(e);
						}
						@Override
						public void insertUpdate(DocumentEvent e) {
							changeText(e);
						}
						@Override
						public void changedUpdate(DocumentEvent e) {
							changeText(e);
						}

						private void changeText(DocumentEvent e) {
							String s = textField.getText();
							SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
							try {
								myValue = new MatrixCollectable((Long) cl.getId(), sdf.parseObject(s), cl.getVersion());
							} catch (Exception ex) {
								myValue = new MatrixCollectable((Long) cl.getId(), null, cl.getVersion());
							}
							myValue.setField(cl.getField());
							myValue.setEntityFieldMetaDataVO(cl.getEntityFieldMetaDataVO());
							fireChangeEvent();
						}

					});

					return datChooser;
				}
				else if(isFile) {
					FileChooserComponent fileChooser = new FileChooserComponent();
					GenericObjectDocumentFile file = (GenericObjectDocumentFile)cl.getValue(UID.UID_NULL);
					myValue = new MatrixCollectable((Long) cl.getId(), file, cl.getVersion());
					myValue.setEntityFieldMetaDataVO(cl.getEntityFieldMetaDataVO());
					myValue.setField(cl.getField());
					if(file != null) {
						String sFileName = file.getFilename();
						fileChooser.setFileName(sFileName);
					}

					return fileChooser;
				}
				else if (isInt || isLong) {
					Number iValue = (Number) cl.getValue(UID.UID_NULL);
					myValue = new MatrixCollectable((Long) cl.getId(), iValue, cl.getVersion());
					myValue.setEntityFieldMetaDataVO(cl.getEntityFieldMetaDataVO());
					myValue.setField(cl.getField());

					final JTextField textField = new JTextField();
					if(iValue != null) {
						textField.setText(iValue.toString());
					}
					textField.addKeyListener(new KeyAdapter() {
						@Override
						public void keyTyped(KeyEvent e) {
							setText(e);
						}

						private void setText(KeyEvent ev) {
							SwingUtilities.invokeLater(() -> {
									String s = textField.getText();
									if(s == null || s.length() == 0) {
										myValue = new MatrixCollectable((Long) cl.getId(), null, cl.getVersion());
									}
									else {
										try {
											myValue = new MatrixCollectable((Long) cl.getId(), Integer.parseInt(s), cl.getVersion());
										}
										catch(NumberFormatException e){
											LOG.warn(e.getMessage());
										}
									}
									myValue.setField(cl.getField());
									myValue.setEntityFieldMetaDataVO(cl.getEntityFieldMetaDataVO());
									fireChangeEvent();
							});
						}
					});
					return textField;
				} else if(isString) {
					String sValue = LangUtils.nullIfEmpty((String) cl.getValue(UID.UID_NULL));
					myValue = new MatrixCollectable((Long) cl.getId(), sValue, cl.getVersion());
					myValue.setEntityFieldMetaDataVO(cl.getEntityFieldMetaDataVO());
					myValue.setField(cl.getField());
					final JTextField textField = new JTextField(sValue);

					textField.getDocument().addDocumentListener(new DocumentListener() {
						@Override
						public void insertUpdate(final DocumentEvent e) {
							changeText();
						}

						@Override
						public void removeUpdate(final DocumentEvent e) {
							changeText();
						}

						@Override
						public void changedUpdate(final DocumentEvent e) {
							changeText();
						}

						private void changeText() {
							SwingUtilities.invokeLater(() -> {
								String s = LangUtils.nullIfEmpty(textField.getText());
								myValue = new MatrixCollectable((Long) cl.getId(), s, cl.getVersion());
								myValue.setField(cl.getField());
								myValue.setEntityFieldMetaDataVO(cl.getEntityFieldMetaDataVO());
								myValue.setVO(cl.getVO());

								fireChangeEvent();
							});
						}
					});

					return textField;
				}
				else if(cl.getValue(UID.UID_NULL) instanceof String) {
					String sValue = (String) cl.getValue(UID.UID_NULL);
					myValue = new MatrixCollectable(sValue);
					return new JLabel(sValue);
				}

		myValue = new MatrixCollectable(null);
		return new JLabel("");		
	}

	private void fireChangeEvent() {
		for(ChangeListener cl : lstchangelistener) {
			final ChangeEvent ev = new ChangeEvent(this);
			cl.stateChanged(ev);
		}
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

}
