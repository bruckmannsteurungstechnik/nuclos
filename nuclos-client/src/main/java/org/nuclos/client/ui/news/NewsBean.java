package org.nuclos.client.ui.news;

import java.awt.Frame;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JFrame;

import org.apache.commons.lang.StringUtils;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.log4j.Logger;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.rest.RestClient;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.schema.rest.News;
import org.springframework.stereotype.Component;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Component
public class NewsBean {
	private static final Logger LOG = Logger.getLogger(NewsOverviewTableModel.class);

	private final RestClient restClient;

	private List<News> news = new ArrayList<>();
	private List<News> unreadNews = new ArrayList<>();

	NewsBean(
			final RestClient restClient
	) {
		this.restClient = restClient;
	}

	public void refresh() {
		news = loadCurrentNews();
		unreadNews = loadUnreadNews();
	}

	private List<News> loadCurrentNews() {
		URL restUrl = restClient.getRestUrl();
		try {
			News[] news = restClient.execute(
					new HttpGet(restUrl + "/news"),
					News[].class
			);
			return Arrays.asList(news);
		} catch (IOException e) {
			LOG.error("Failed to load News", e);
			return new ArrayList<>();
		}
	}

	private List<News> loadUnreadNews() {
		URL restUrl = restClient.getRestUrl();
		try {
			News[] news = restClient.execute(
					new HttpGet(restUrl + "/news/unread"),
					News[].class
			);
			return Arrays.asList(news);
		} catch (IOException e) {
			LOG.error("Failed to load News", e);
			return new ArrayList<>();
		}
	}

	public List<News> loadUnconfirmedNews() {
		URL restUrl = restClient.getRestUrl();
		try {
			News[] news = restClient.execute(
					new HttpGet(restUrl + "/news/unconfirmed"),
					News[].class
			);
			return Arrays.asList(news);
		} catch (IOException e) {
			LOG.error("Failed to load News", e);
			return new ArrayList<>();
		}
	}

	boolean isUnread(News news) {
		if (unreadNews == null) {
			return false;
		}

		return unreadNews.stream().anyMatch(
				unread -> StringUtils.equals(unread.getId(), news.getId())
		);
	}

	private void markNewsViewed(final News news) {
		try {
			sendPut(restClient.getRestUrl() + "/news/" + news.getId() + "/viewed");

			unreadNews = unreadNews.stream().filter(
					it -> !StringUtils.equals(it.getId(), news.getId())
			).collect(Collectors.toList());
		} catch (IOException e) {
			LOG.error("Failed to mark news as viewed", e);
		}
	}

	private void markNewsConfirmed(final News news) {
		try {
			sendPut(restClient.getRestUrl() + "/news/" + news.getId() + "/confirmed");
		} catch (IOException e) {
			LOG.error("Failed to mark news as confirmed", e);
		}
	}

	private void sendPut(final String url) throws IOException {
		HttpPut request = new HttpPut(url);
		restClient.execute(request);
	}

	List<News> getNews() {
		return news;
	}

	private List<News> getUnreadNews() {
		return unreadNews;
	}

	public void showAllUnreadNews() {
		List<News> newsUnread = getUnreadNews();
		showAll(newsUnread);
	}

	/**
	 * Displays all News in the given list one by one in separate dialogs.
	 */
	void showAll(final List<News> newsList) {
		if (newsList.isEmpty()) {
			return;
		}

		JFrame mainFrame = MainFrame.getInstance();

		for (News news: newsList) {
			boolean confirmed = new NewsDialog(
					mainFrame,
					news,
					!isUnread(news)
			).setConfirmButtonText(getLocaleDelegate().getTextFallback("news.dialog.confirm", "Confirm"))
					.setCloseButtonText(getLocaleDelegate().getTextFallback("news.dialog.close", "Close"))
					.showAndReturnResult();

			if (confirmed) {
				markNewsViewed(news);
			}
		}
	}

	public boolean showNewsAndConfirm(final Frame parent, final News news) {
		boolean confirmed = new NewsDialog(
				parent,
				news,
				false
		).setConfirmButtonText(getLocaleDelegate().getTextFallback("news.dialog.accept", "Accept"))
				.setCloseButtonText(getLocaleDelegate().getTextFallback("news.dialog.decline", "Decline"))
				.showAndReturnResult();

		if (confirmed) {
			markNewsConfirmed(news);
		}

		return confirmed;
	}

	private SpringLocaleDelegate getLocaleDelegate() {
		return SpringLocaleDelegate.getInstance();
	}
}
