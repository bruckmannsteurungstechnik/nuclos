package org.nuclos.client.ui.news;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Window;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;

import org.apache.log4j.Logger;
import org.nuclos.schema.rest.News;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class NewsDialog extends JDialog {
	private static final Logger LOG = Logger.getLogger(NewsDialog.class);

	private boolean result = false;

	private JButton confirmButton = new JButton("Confirm");
	private JButton closeButton = new JButton("Close");

	NewsDialog(
			final Window parent,
			final News news,
			final boolean confirmed
	) {
		super(parent, ModalityType.APPLICATION_MODAL);

		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		initGui(news, confirmed);
	}

	private void initGui(
			final News news,
			final boolean confirmed
	) {
		setTitle(news.getTitle());
		setSize(new Dimension(375, 500));
		setAlwaysOnTop(true);

		addEditorPane(news);

		JPanel buttonPanel = new JPanel();

		if (!confirmed) {
			addConfirmButton(buttonPanel);
		}
		addCloseButton(buttonPanel);

		add(buttonPanel, BorderLayout.SOUTH);
	}

	private void addEditorPane(final News news) {

		// TODO: Consolidate with org.nuclos.client.help.HtmlPanel
		JEditorPane jEditorPane = new JEditorPane() {
			@Override
			public void scrollRectToVisible(final Rectangle aRect) {
				// Do nothing here.
				// This is called automatically when the Dialog becomes visible and would scroll to the wrong position.
			}
		};
		jEditorPane.setEditable(false);

		HTMLEditorKit kit = new HTMLEditorKit();
		jEditorPane.setEditorKit(kit);

		jEditorPane.addHyperlinkListener(e -> {
			if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
				try {
					Desktop.getDesktop().browse(e.getURL().toURI());
				} catch (IOException | URISyntaxException e1) {
					LOG.warn("Failed to open hyperlink: " + e.getURL(), e1);
				}
			}
		});

		Document doc = kit.createDefaultDocument();
		jEditorPane.setDocument(doc);
		jEditorPane.setText(news.getContent());

		JScrollPane jScrollPane = new JScrollPane(jEditorPane);
		jScrollPane.getVerticalScrollBar().setValue(0);

		add(jScrollPane, BorderLayout.CENTER);
	}

	private void addConfirmButton(final JPanel buttonPanel) {
		confirmButton.addActionListener(e -> confirmed());
		buttonPanel.add(confirmButton);
	}

	private void addCloseButton(final JPanel buttonPanel) {
		closeButton.addActionListener(e -> dispose());
		buttonPanel.add(closeButton);
	}

	boolean showAndReturnResult() {
		setLocationRelativeTo(getParent());
		setAlwaysOnTop(true);
		setVisible(true);

		return result;
	}

	private void confirmed() {
		result = true;
		dispose();
	}

	NewsDialog setConfirmButtonText(final String text) {
		confirmButton.setText(text);
		return this;
	}

	NewsDialog setCloseButtonText(final String text) {
		closeButton.setText(text);
		return this;
	}
}
