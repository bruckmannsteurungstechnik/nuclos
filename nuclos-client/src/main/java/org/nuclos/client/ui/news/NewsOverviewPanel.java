package org.nuclos.client.ui.news;

import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;

import org.jdesktop.swingx.JXTable;
import org.nuclos.client.ui.NuclosToolBar;
import org.nuclos.client.ui.util.TableLayoutBuilder;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.schema.rest.News;

public class NewsOverviewPanel extends JPanel {

	private final NewsOverviewTableModel model;

	private final JXTable table;

	private final TableRowSorter sorter;

	private final SpringLocaleDelegate sld;

	public NewsOverviewPanel(
			final NewsOverviewTableModel model,
			final NewsBean newsBean
	) {
		if (model == null) {
			throw new NullPointerException();
		}
		this.model = model;

		sld = SpringApplicationContextHolder.getBean(SpringLocaleDelegate.class);
		sorter = new TableRowSorter(model);
		table = new JXTable(model);
		table.setFillsViewportHeight(true);
		table.setRowSorter(sorter);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setEditable(false);
		table.setSortable(true);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent event) {
				final Point p = event.getPoint();
				if (!event.isPopupTrigger()) {
					final int row = table.rowAtPoint(p);
					if (row >= 0) {
						News news = NewsOverviewPanel.this.model.getRow(row);
						newsBean.showAll(Arrays.asList(news));
					}
				}
			}
		});

		final TableColumnModel tcm = table.getColumnModel();
		int i = 0;
		for (TableColumn tc : CollectionUtils.iterableEnum(tcm.getColumns())) {
			switch (i) {
				case 0:
					tc.setPreferredWidth(300);
					break;
				case 1:
					tc.setPreferredWidth(100);
					break;
				case 2:
					tc.setPreferredWidth(100);
					break;
				case 3:
					tc.setPreferredWidth(150);
					break;
				default:
					tc.setMinWidth(100);
					tc.setPreferredWidth(200);
					tc.setMaxWidth(300);
			}
			++i;
		}

		TableLayoutBuilder tbllay = new TableLayoutBuilder(this).columns(TableLayoutBuilder.FILL, 500.0, TableLayoutBuilder.FILL);
		tbllay.newRow(20.0).addFullSpan(new NuclosToolBar());
		tbllay.newRow(TableLayoutBuilder.FILL).skip().add(new JScrollPane(table));
		tbllay.newRow(10.0);
	}
}
