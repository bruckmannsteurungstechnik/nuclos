//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.profile;

import org.nuclos.client.ui.UIUtils;
import org.nuclos.common.UID;
import org.nuclos.common.preferences.TablePreferencesManager;
import org.nuclos.common.profile.ProfileItem;
import org.nuclos.common.profile.PublishType;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.PreferencesException;
import org.nuclos.common2.exception.WorkspaceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * ProfilesController
 * 
 *  NUCLOS-1479 Profiles
 *
 */
public final class ResultProfilesController extends AbstractProfilesController {

	private final static Logger LOG = LoggerFactory.getLogger(ResultProfilesController.class);
	
	public ResultProfilesController(final TablePreferencesManager tblprefManager, ProfileSupport profileSupport, UID targetEntity) {
		super(tblprefManager, profileSupport, targetEntity);
	}

	@Override
	public void publishProfile(final ProfileItem profile, final PublishType publishType) throws CommonPermissionException, PreferencesException {
		if (!isPublishAllowed(profile)) {
			LOG.error("publish profile " + profile + " failed. No Permission.");
			throw new CommonPermissionException(SpringLocaleDelegate.getInstance().getMessage(
					"Workspace.Profile.ProfilesController.1", "Keine Berechtigung!"));
		}
		LOG.info("published profile " + profile);
		getTablePreferencesManager().share(profile.getPreferences(), publishType);
	}
	
	@Override
	public void resetSelectedProfile() throws CommonPermissionException, WorkspaceException {
		if (!isResetPossible(getModel().getSelectedProfile())) {
			LOG.error("restore profile " + getModel().getSelectedProfile() + " failed. No Permission.");
			throw new CommonPermissionException(SpringLocaleDelegate.getInstance().getMessage(
					"Workspace.Profile.ProfilesController.1", "Keine Berechtigung!"));
		}

		getTablePreferencesManager().reset(getModel().getSelectedProfile().getPreferences());
		switchToProfile(getModel().getSelectedProfile());
		LOG.info("restored profile " + getModel().getSelectedProfile());
	}

	@Override
	public void switchToProfile(final ProfileItem profile) {
		if (!getTablePreferencesManager().getSelected().equals(profile.getPreferences())) {
			getTablePreferencesManager().select(profile.getPreferences());
			UIUtils.invokeOnDispatchThread(new Runnable() {

				@Override
				public void run() {
					getProfileSupport().updateBySelectedProfile();
				}
			});
		}
	}
	
	public void switchToProfile(String profile) {
		ProfileItem found = null;
		ProfileItem defaultPi = null;
		for (ProfileItem pi: getModel().getPersonalProfiles()) {
			if (profile != null ? profile.equals(pi.getName()) : pi.getName() == null) {
				found = pi;
				break;
			}
			if (pi.getName() == null) {
				defaultPi = pi;
			}
		}
		if (found == null) {
			for (ProfileItem pi: getModel().getPublicProfiles()) {
				if (profile != null ? profile.equals(pi.getName()) : pi.getName() == null) {
					found = pi;
					break;
				}
				if (pi.getName() == null) {
					defaultPi = pi;
				}
			}
		}
		if (found != null) {
			switchToProfile(found);
		} else if (defaultPi != null){
			switchToProfile(defaultPi);
		}
	}
}
