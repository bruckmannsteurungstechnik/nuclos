package org.nuclos.client.ui.table;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.swing.RowSorter.SortKey;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelListener;

import org.nuclos.client.ui.collect.model.SortableCollectableTableModel;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;

public class DummyTableModel<PK,Cltl extends Collectable<PK>> implements SortableCollectableTableModel<PK, Cltl> {
	
	public static final DummyTableModel INSTANCE = new DummyTableModel();
	
	private DummyTableModel() {
	}

	// TableModel
	
	@Override
	public int getRowCount() {
		return 0;
	}

	@Override
	public int getColumnCount() {
		return 0;
	}

	@Override
	public String getColumnName(int columnIndex) {
		return null;
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return String.class;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
	public CollectableField getValueAt(int rowIndex, int columnIndex) {
		return null;
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
	}

	@Override
	public void addTableModelListener(TableModelListener l) {
	}

	@Override
	public void removeTableModelListener(TableModelListener l) {
	}

	// SortableCollectableTableModel

	@Override
	public Cltl getCollectable(int iRow) {
		return null;
	}

	@Override
	public Cltl getRow(int row) {
		return null;
	}
	
	@Override
	public Collection<Cltl> getRows(int[] rows) {
		return Collections.EMPTY_LIST;
	}

	@Override
	public void setCollectable(int iRow, Cltl clct) {
	}

	@Override
	public void addCollectable(int iRow, Cltl clct) {
	}

	@Override
	public List<Cltl> getCollectables() {
		return null;
	}

	@Override
	public void setCollectables(List<Cltl> lstclct) {
	}

	@Override
	public void add(int iRow, Cltl clct) {
	}

	@Override
	public void add(Cltl clct) {
	}

	@Override
	public void remove(int iRow) {
	}

	@Override
	public void remove(Collectable<PK> clct) {
	}

	@Override
	public int findRowById(PK oId) {
		return -1;
	}

	@Override
	public void clear() {
	}

	@Override
	public void addAll(Collection<Cltl> collclct) {
	}

	@Override
	public void addColumn(int iColumn, CollectableEntityField clctef) {
	}

	@Override
	public void removeColumn(int iColumn) {
	}

	@Override
	public void setColumns(List<? extends CollectableEntityField> lstclctefColumns) {
	}

	@Override
	public int findColumnByFieldUid(UID fieldUid) {
		return -1;
	}

	@Override
	public CollectableEntityField getCollectableEntityField(int iColumn) {
		return null;
	}

	@Override
	public UID getColumnFieldUid(int columnIndex) {
		return null;
	}

	@Override
	public int getColumn(CollectableEntityField field) {
		return -1;
	}

	@Override
	public CollectableField getValueAsCollectableField(Object oValue) {
		return null;
	}

	@Override
	public UID getBaseEntityUid() {
		return null;
	}

	@Override
	public boolean isSortable(int column) {
		return false;
	}

	@Override
	public List<? extends SortKey> getSortKeys() {
		return null;
	}

	@Override
	public void setSortKeys(List<? extends SortKey> sortKeys, boolean sortImmediately) throws IllegalArgumentException {
	}

	@Override
	public void toggleSortOrder(int column, boolean sortImmediately) {
	}

	@Override
	public void sort() {
	}

	@Override
	public void addSortingListener(ChangeListener listener) {
	}

	@Override
	public void removeSortingListener(ChangeListener listener) {
	}
	
}
