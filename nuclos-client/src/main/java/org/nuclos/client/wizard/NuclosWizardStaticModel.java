package org.nuclos.client.wizard;

import java.awt.event.ActionEvent;
import java.lang.ref.WeakReference;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.nuclos.client.ui.UIUtils;
import org.pietschy.wizard.InvalidStateException;
import org.pietschy.wizard.Wizard;
import org.pietschy.wizard.WizardStep;
import org.pietschy.wizard.models.StaticModel;

public class NuclosWizardStaticModel extends StaticModel {

	private static final Logger LOG = Logger.getLogger(NuclosWizardStaticModel.class);

	private boolean navigationEnabled = true;

	private WeakReference<Wizard> weakWizard;

	protected Wizard getWizard() {
		if (weakWizard == null) {
			return null;
		}
		return weakWizard.get();
	}

	public void setWizard(Wizard wizard) {
		this.weakWizard = new WeakReference<Wizard>(wizard);
	}

	public void setNavigationEnabled(boolean navigationEnabled) {
		this.navigationEnabled = navigationEnabled;
	}

	public Action getActionNextStep() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						nextIfPossible();
					}
				});
			}
		};
	}

	public Action getActionPrevStep() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						prevIfPossible();
					}
				});
			}
		};
	}

	public Action getActionFinish() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						finishIfComplete();
					}
				});
			}
		};
	}

	public Action getActionLast() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						try {
							UIUtils.setWaitCursor();
							if (!navigationEnabled) {
								return;
							}
							final WizardStep activeStep = getActiveStep();
							final boolean busy = activeStep != null && activeStep.isBusy();
							if (!busy && isLastAvailable()) {
								try {
									activeStep.applyState();
									lastStep();
									finishIfComplete();
								} catch (InvalidStateException e) {
									LOG.error(e.getMessage(), e);
									refreshModelState();
								}
							}
						} finally {
							UIUtils.setDefaultCursor();
						}
					}
				});
			}
		};
	}

	protected void nextIfPossible() {
		if (!navigationEnabled) {
			return;
		}
		final WizardStep activeStep = getActiveStep();
		final boolean busy = activeStep != null && activeStep.isBusy();
		if (!busy && !activeStep.isComplete()) {
			return;
		}
		try {
			activeStep.applyState();
			nextStep();
		} catch (InvalidStateException e) {
			LOG.error(e.getMessage(), e);
			refreshModelState();
		}
	}

	protected void prevIfPossible() {
		if (!navigationEnabled) {
			return;
		}
		previousStep();
	}

	protected void finishIfComplete() {
		if (!navigationEnabled && weakWizard==null) {
			return;
		}
		Wizard wizard = weakWizard.get();
		if (wizard == null) {
			return;
		}

		final WizardStep activeStep = getActiveStep();
		final boolean busy = activeStep != null && activeStep.isBusy();
		if (!busy && isLastStep(activeStep) && activeStep.isComplete()) {
			try {
				activeStep.applyState();
				wizard.close();
			} catch (InvalidStateException e) {
				LOG.error(e.getMessage(), e);
				refreshModelState();
				return;
			}
		}
	}

}
