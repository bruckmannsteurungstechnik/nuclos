//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.wizard.steps;

import java.awt.Component;
import java.awt.IllegalComponentStateException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.Collator;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.combobox.ListComboBoxModel;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.client.attribute.AttributeDelegate;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.NuclosCollectControllerFactory;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.entityobject.EntityFacadeDelegate;
import org.nuclos.client.entityobject.EntityObjectDelegate;
import org.nuclos.client.genericobject.GenericObjectDelegate;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.MainController;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.masterdata.MasterDataLayoutHelper;
import org.nuclos.client.masterdata.MetaDataDelegate;
import org.nuclos.client.scripting.ScriptEditor;
import org.nuclos.client.ui.Bubble;
import org.nuclos.client.ui.BubbleUtils.Position;
import org.nuclos.client.ui.DateChooser;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.InputVerifierFactory;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.CalcAttributeClientHelper;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.client.ui.collect.CollectController.CollectableEventListener;
import org.nuclos.client.ui.collect.CollectController.MessageType;
import org.nuclos.client.ui.collect.WeakCollectableEventListener;
import org.nuclos.client.ui.collect.component.ICollectableListOfValues;
import org.nuclos.client.ui.collect.component.LookupListener;
import org.nuclos.client.ui.labeled.LabeledComponentSupport;
import org.nuclos.client.ui.popupmenu.DefaultJPopupMenuListener;
import org.nuclos.client.ui.popupmenu.JPopupMenuFactory;
import org.nuclos.client.ui.popupmenu.JPopupMenuListener;
import org.nuclos.client.ui.util.TableLayoutBuilder;
import org.nuclos.client.wizard.NuclosEntityAttributeWizardStaticModel;
import org.nuclos.client.wizard.NuclosEntityWizardStaticModel;
import org.nuclos.client.wizard.model.Attribute;
import org.nuclos.client.wizard.model.ValueList;
import org.nuclos.client.wizard.util.DefaultValue;
import org.nuclos.client.wizard.util.NuclosWizardUtils;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.FieldMetaVO;
import org.nuclos.common.NuclosEntityValidator;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosScript;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableFieldFormat;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dblayer.IFieldUIDRef;
import org.nuclos.common.format.RefValueExtractor;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.ForeignEntityFieldUIDParser;
import org.nuclos.common2.IdUtils;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.RelativeDate;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.genericobject.searchcondition.CollectableGenericObjectSearchExpression;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.pietschy.wizard.InvalidStateException;
import org.springframework.context.i18n.LocaleContextHolder;

import info.clearthought.layout.TableLayout;

/**
* <br>
* Created by Novabit Informationssysteme GmbH <br>
* Please visit <a href="http://www.novabit.de">www.novabit.de</a>
*
* @author <a href="mailto:marc.finke@novabit.de">Marc Finke</a>
* @version 01.00.00
*/
public class NuclosEntityAttributeCommonPropertiesStep extends NuclosEntityAttributeAbstractStep {
	private static final Logger LOG = Logger.getLogger(NuclosEntityAttributeCommonPropertiesStep.class);

	public static class ListOfValues<PK> extends org.nuclos.client.ui.ListOfValues implements ICollectableListOfValues<PK>, JPopupMenuFactory, CollectableEventListener<PK> {
		private FieldMeta<?> efMeta;
		private SelectedListener selectedListener;
		private CollectableValueIdField selectedfield;
		private NuclosEntityAttributeWizardStaticModel model; 
		
		public static abstract class SelectedListener {
			public abstract void actionPerformed(CollectableValueIdField itemSelected);
		}
		
		public ListOfValues() {
			super(new LabeledComponentSupport());			
			super.setQuickSearchResulting(new QuickSearchResulting() {
				@Override
				protected List<CollectableValueIdField> getQuickSearchResult(String inputString, boolean bLimit) throws CommonBusinessException {
					return EntityFacadeDelegate.getInstance().getQuickSearchResult(
							efMeta, inputString, null, null, ICollectableListOfValues.QUICKSEARCH_MAX, null);
				}
			});
			super.setQuickSearchCanceledListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					actionPerformedQuickSearchSelected(true);
				}
			});
			super.setQuickSearchSelectedListener(new QuickSearchSelectedListener() {
				@Override
				public void actionPerformed(CollectableValueIdField itemSelected) {
					ListOfValues.this.selectedfield = itemSelected;
					if (selectedListener != null)
						selectedListener.actionPerformed(itemSelected);
					
					if (itemSelected == null) {
						getJTextField().setText("");
					} else {
						try {
							final MasterDataVO<?> mdvo = MasterDataDelegate.getInstance().get(efMeta.getForeignEntity() != null ? efMeta.getForeignEntity() : efMeta.getLookupEntity(), itemSelected.getValueId());
							final ForeignEntityFieldUIDParser parser = new ForeignEntityFieldUIDParser(efMeta, MetaProvider.getInstance());
							final StringBuffer sValue = new StringBuffer();
							final Iterator<IFieldUIDRef> it = parser.iterator();
							while(it.hasNext()) {
								IFieldUIDRef ref = it.next();
								if (ref.isConstant()) {
									sValue.append(ref.getConstant());
								}
								if (ref.isUID()) {
									Object value = mdvo.getFieldValue(ref.getUID());
									if (value != null) {
										sValue.append(value.toString());
									}
								}
							}

							getJTextField().setText(sValue.toString());
						} catch(Exception e) {
							getJTextField().setText("");
						}
					}
				}
			});	
			
			getBrowseButton().addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent ev) {
					Component c = UIUtils.getTabOrWindowForComponent(ListOfValues.this);
					final MainFrameTab tab;
					if (c instanceof MainFrameTab) {
						tab = (MainFrameTab) c;
					} else {
						MainFrameTab selectedTab = null;
						try {
							selectedTab = MainFrame.getSelectedTab(ListOfValues.this.getLocationOnScreen());
						} catch (IllegalComponentStateException e) {
							//
						} finally {
							tab = selectedTab;
						}
					}
					UIUtils.runCommandLater(getParent(), new CommonRunnable() {
						@Override
						public void run() throws CommonBusinessException {
								final UID referencedEntity = efMeta.getForeignEntity();
								final MainFrameTab overlay = new MainFrameTab();
								final CollectController<PK,?> ctl = (CollectController<PK, ?>) NuclosCollectControllerFactory.getInstance().newCollectController(referencedEntity, overlay, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
								Main.getInstance().getMainController().initMainFrameTab(ctl, overlay);
								tab.add(overlay);
								ctl.runLookupCollectable(ListOfValues.this);
						}
					});
				}
			});
			
			setupJPopupMenuListener(newJPopupMenuListener());
		}

		public void setEfMetaDataVO(FieldMeta<?> efMeta) {
			this.efMeta = efMeta;
		}
		
		@Override
		public void acceptLookedUpCollectable(Collectable<PK> clctLookedUp) {
			acceptLookedUpCollectable(clctLookedUp, null);
		}
		
		@Override
		public final void setQuickSearchCanceledListener(ActionListener al) {}
		@Override
		public final void setQuickSearchResulting(QuickSearchResulting quickSearchResulting) {}
		@Override
		public final void setQuickSearchSelectedListener(QuickSearchSelectedListener qssl) {}

		public void setSelectedListener(final SelectedListener sl) {
			this.selectedListener = sl;
		}
		public void setModel(NuclosEntityAttributeWizardStaticModel model) {
			this.model = model;
		}
		
		@Override
		public void acceptLookedUpCollectable(Collectable<PK> clctLookedUp, List<Collectable<PK>> additionalCollectables) {
			if (getQuickSearchSelectedListener() != null && model != null) {
				if (clctLookedUp == null) {
					getQuickSearchSelectedListener().actionPerformed(
						CollectableValueIdField.NULL);
					return;
				}
				
				String sField = model.getAttribute().getField();
				
				//NUCLOS-4687: There is no stringified value needed. Just get the first one, if combined.
				if (sField.matches(".*\\s+.*")) {
					sField = sField.split("\\s+")[0];
				}

				String fieldValue = "" + clctLookedUp.getField(UID.parseUID(sField));
				
				getQuickSearchSelectedListener().actionPerformed(
						new CollectableValueIdField(clctLookedUp.getId(), fieldValue));
			}
		}
		
		public void acceptLookedUpCollectable(MasterDataVO<PK> clctLookedUp, List<Collectable<PK>> additionalCollectables) {
			if (getQuickSearchSelectedListener() != null && model != null) {
				if (clctLookedUp == null) {
					getQuickSearchSelectedListener().actionPerformed(
						CollectableValueIdField.NULL);
					return;
				}
				
				String sField = model.getAttribute().getField();
				Pattern referencedEntityPattern = Pattern.compile("[u][i][d][{][\\w\\[\\]]+[}]");
				Matcher referencedEntityMatcher = referencedEntityPattern.matcher(sField);
				StringBuffer sb = new StringBuffer();

				while (referencedEntityMatcher.find()) {
					Object value = referencedEntityMatcher.group().substring(4, referencedEntityMatcher.group().length() - 1);

					String sUID = value.toString();
					Object fieldValue = clctLookedUp.getFieldValue(UID.parseUID(sUID));
					if (fieldValue != null)
						referencedEntityMatcher.appendReplacement(sb, fieldValue.toString());
					else
						referencedEntityMatcher.appendReplacement(sb, "");
				}

				// complete the transfer to the StringBuffer
				referencedEntityMatcher.appendTail(sb);
				sField = sb.toString();
				
				getQuickSearchSelectedListener().actionPerformed(
						new CollectableValueIdField(clctLookedUp.getId(), sField));
			}
		}
		
		private void setupJPopupMenuListener(JPopupMenuListener popupmenulistener) {
			addMouseListener(popupmenulistener);
			getJTextField().addMouseListener(popupmenulistener);
		}
		
		private JPopupMenuListener newJPopupMenuListener() {
			return new DefaultJPopupMenuListener(this);
		}

		@Override
		public JPopupMenu newJPopupMenu() {
			final JPopupMenu result = new JPopupMenu();
				result.add(newShowDetailsEntry());
				result.add(newInsertEntry());
				result.add(newClearEntry());
			return result;
		}
		protected final JMenuItem newClearEntry() {
			final JMenuItem result = new JMenuItem(
					SpringLocaleDelegate.getInstance().getMessage("CollectableFileNameChooserBase.1","Zur\u00fccksetzen"));
			boolean bClearEnabled = this.getBrowseButton().isEnabled() && selectedfield != null && selectedfield.getValueId() != null;
			result.setEnabled(bClearEnabled);
			result.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent ev) {
					actionPerformedQuickSearchSelected(true);
				}
			});
			return result;
		}
		protected final JMenuItem newShowDetailsEntry() {
			final JMenuItem result = new JMenuItem(SpringLocaleDelegate.getInstance().getMessage(
					"AbstractCollectableComponent.22","Details anzeigen..."));
			final UID referencedEntity = efMeta.getForeignEntity();
			boolean bShowDetailsEnabled = selectedfield != null && selectedfield.getValueId() != null 
					&& MasterDataLayoutHelper.isLayoutMLAvailable(referencedEntity, false);
			if (bShowDetailsEnabled) {
				bShowDetailsEnabled = SecurityCache.getInstance().isReadAllowedForEntity(referencedEntity);
			}
			result.setEnabled(bShowDetailsEnabled);
			result.addActionListener(new ActionListener() {
				@Override
	            public void actionPerformed(ActionEvent ev) {
					UIUtils.runCommandLater(getParent(), new CommonRunnable() {
						@Override
						public void run() throws CommonBusinessException {
							final Main main = Main.getInstance();
							final MainController mc = main.getMainController();
							final UID referencedEntity = efMeta.getForeignEntity();
							final CollectController<Object,?> controller = (CollectController<Object, ?>) mc.getControllerForTab((MainFrameTab) 
									main.getMainFrame().getHomePane().getSelectedComponent());
							Object oId = selectedfield.getValueId();
							if(oId instanceof Long) {
								Long l = (Long)oId;
								oId = new Integer(l.intValue());
							}
							mc.showDetails(referencedEntity, oId, true, null, controller, new WeakCollectableEventListener(ListOfValues.this));
						}
					});
				}
			});
			return result;
		}
		protected final JMenuItem newInsertEntry() {
			final JMenuItem result = new JMenuItem(SpringLocaleDelegate.getInstance().getMessage(
					"AbstractCollectableComponent.context.new","Neu..."));
			final UID referencedEntity = efMeta.getForeignEntity();
			boolean bInsertEnabled =
					MasterDataLayoutHelper.isLayoutMLAvailable(referencedEntity, false);
			if (bInsertEnabled) {
				if (Modules.getInstance().isModule(referencedEntity)) {
					bInsertEnabled = SecurityCache.getInstance().isNewAllowedForModule(referencedEntity);
				} else {
					bInsertEnabled = SecurityCache.getInstance().isWriteAllowedForMasterData(referencedEntity);
				}

				boolean blnEntityIsEditable = MetaProvider.getInstance().getEntity(referencedEntity).isEditable();
				if (!blnEntityIsEditable)
					bInsertEnabled = blnEntityIsEditable;
			}
			result.setEnabled(bInsertEnabled && isEnabled());

			Component c = UIUtils.getTabOrWindowForComponent(this);
			final MainFrameTab tab;
			if (c instanceof MainFrameTab) {
				tab = (MainFrameTab) c;
			} else {
				MainFrameTab selectedTab = null;
				try {
					selectedTab = MainFrame.getSelectedTab(this.getLocationOnScreen());
				} catch (IllegalComponentStateException e) {
					//
				} finally {
					tab = selectedTab;
				}
			};
			
			result.addActionListener(new ActionListener() {
				@Override
	            public void actionPerformed(ActionEvent ev) {
					UIUtils.runCommandLater(getParent(), new CommonRunnable() {
						@Override
						public void run() throws CommonBusinessException {
							final UID referencedEntity = efMeta.getForeignEntity();
							final CollectableEventListener<PK> listener = new WeakCollectableEventListener<PK>(ListOfValues.this);
							Main.getInstance().getMainController().showNew(referencedEntity, tab, listener);
						}
					});

				}
			});
			return result;
		}

		@Override
		public void handleCollectableEvent(Collectable<PK> collectable, MessageType messageType) {
			switch (messageType) {
				case EDIT_DONE:
				case STATECHANGE_DONE:
					if (LangUtils.equal(IdUtils.toLongId(collectable.getId()), selectedfield.getValueId())) {
						acceptLookedUpCollectable(collectable);
					}
				case NEW_DONE:
					acceptLookedUpCollectable(collectable);
					break;
			}
		}

		@Override
		public void addLookupListener(LookupListener listener) {
			//...
		}

		@Override
		public void removeLookupListener(LookupListener listener) {
			//...
		}

		@Override
		public CollectableSearchCondition getCollectableSearchCondition() {
			return null;
		}

		@Override
		public boolean isSearchComponent() {
			return false;
		}

		@Override
		public Object getProperty(String sName) {
			return null;
		}
	}


	private JLabel lbLabel;
	private JTextField tfLabel;
	private JLabel lbDefaultValue;
	private JTextField tfDefaultValue;
	private JComboBox cbxDefaultValue;
	private ListOfValues<?> lovDefaultValue;
	private DateChooser dateDefaultValue;
	private JCheckBox cbDefaultValue;
	private JLabel lbDBFieldName;
	private JTextField tfDBFieldName;
	private JLabel lbDBFieldNameComplete;
	private JTextField tfDBFieldNameComplete;

	private JLabel lbDistinct;
	private JCheckBox cbDistinct;
	private JLabel lbDistinctMsg;
	private JLabel lbLogBook;
	private JCheckBox cbLogBook;
	private JLabel lbMandatory;
	private JCheckBox cbMandatory;

	private JTextField tfMandatory;
	private JComboBox cbxMandatory;
	private ListOfValues<?> lovMandatory;
	private DateChooser dateMandatory;
	private JCheckBox cbMandatoryValue;

	private JLabel lbIndexed;
	private JCheckBox cbIndexed;

	private JLabel lbAttributeGroup;
	private JComboBox cbxAttributeGroup;

	private JLabel lbCalcFunction;
	private JComboBox cbxCalcFunction;
	
	private JLabel lbCalcFunctionParameter;
	private JTextField tfCalcFunctionParameter;
	
	private JLabel lbCalcAttributeAllowCustomization;
	private JCheckBox cbCalcAttributeAllowCustomization;
	
	private JLabel lbCalcOndemand;
	private JCheckBox cbCalcOndemand;

	private JLabel lbCalculationScript;
	private JButton btCalculationScript;

	private JLabel lbBackgroundColorScript;
	private JButton btBackgroundColorScript;

	private boolean blnLabelModified;
	private boolean blnDefaultSelected;

	private JLabel lbHidden;
	private JCheckBox cbHidden;

	private JLabel lbModifiable;
	private JCheckBox cbModifiable;

	private NuclosEntityWizardStaticModel parentWizardModel;

	boolean requiresDefaultOld;
	
	boolean prepareRunning = false;
	boolean distinctChecked = false;
	
	private Map<String, String> callableFunctionsWithDescription;
	
	public NuclosEntityAttributeCommonPropertiesStep() {
		initComponents();
	}

	public NuclosEntityAttributeCommonPropertiesStep(String name, String summary) {
		super(name, summary);
		initComponents();
	}

	public NuclosEntityAttributeCommonPropertiesStep(String name, String summary, Icon icon) {
		super(name, summary, icon);
		initComponents();
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void initComponents() {
		final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
		final LabeledComponentSupport support = new LabeledComponentSupport();

		lbLabel = new JLabel(localeDelegate.getMessage("wizard.step.attributeproperties.10", "Feldname"));
		tfLabel = new JTextField();
		tfLabel.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());
		tfLabel.setToolTipText(localeDelegate.getMessage("wizard.step.attributeproperties.tooltip.10", "Feldname"));

		lbDefaultValue = new JLabel(localeDelegate.getMessage("wizard.step.attributeproperties.11", "Standardwert"));
		tfDefaultValue = new JTextField();
		tfDefaultValue.setToolTipText(localeDelegate.getMessage("wizard.step.attributeproperties.tooltip.11", "Standardwert"));
		tfDefaultValue.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());
		
		cbxDefaultValue = new JComboBox();
		cbxDefaultValue.setVisible(false);
		cbxDefaultValue.setToolTipText(localeDelegate.getMessage("wizard.step.attributeproperties.tooltip.11", "Standardwert"));

		lovDefaultValue = new ListOfValues();
		lovDefaultValue.setVisible(false);
		lovDefaultValue.setToolTipText(localeDelegate.getMessage("wizard.step.attributeproperties.tooltip.11", "Standardwert"));

		dateDefaultValue = new DateChooser(support, true);
		dateDefaultValue.setVisible(false);
		dateDefaultValue.setToolTipText(localeDelegate.getMessage("wizard.step.attributeproperties.tooltip.11", "Standardwert"));

		cbDefaultValue = new JCheckBox();
		cbDefaultValue.setVisible(false);
		cbDefaultValue.setToolTipText(localeDelegate.getMessage("wizard.step.attributeproperties.tooltip.11", "Standardwert"));

		lbDistinct = new JLabel(localeDelegate.getMessage("wizard.step.attributeproperties.7", "Eindeutig?"));
		cbDistinct = new JCheckBox();
		cbDistinct.setToolTipText(localeDelegate.getMessage("wizard.step.attributeproperties.tooltip.7", "Eindeutig?"));
		lbDistinctMsg = new JLabel("");

		lbLogBook = new JLabel(localeDelegate.getMessage("wizard.step.attributeproperties.8", "Historie?"));
		cbLogBook = new JCheckBox();
		cbLogBook.setToolTipText(localeDelegate.getMessage("wizard.step.attributeproperties.tooltip.8", "Historie?"));

		lbMandatory = new JLabel(localeDelegate.getMessage("wizard.step.attributeproperties.9", "Pflichtfeld?"));
		cbMandatory = new JCheckBox();
		cbMandatory.setToolTipText(localeDelegate.getMessage("wizard.step.attributeproperties.tooltip.9", "Pflichtfeld?"));

		tfMandatory = new JTextField();
		tfMandatory.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());
		tfMandatory.setToolTipText(localeDelegate.getMessage("wizard.step.attributeproperties.tooltip.27", "Defaultwert für Pflichtfeld"));

		cbxMandatory = new JComboBox();
		cbxMandatory.setVisible(false);
		cbxMandatory.setToolTipText(localeDelegate.getMessage("wizard.step.attributeproperties.tooltip.27", "Defaultwert für Pflichtfeld"));

		lovMandatory = new ListOfValues();
		lovMandatory.setVisible(false);
		lovMandatory.setToolTipText(localeDelegate.getMessage("wizard.step.attributeproperties.tooltip.27", "Defaultwert für Pflichtfeld"));
		
		dateMandatory = new DateChooser(support);
		dateMandatory.setVisible(false);
		dateMandatory.setToolTipText(localeDelegate.getMessage("wizard.step.attributeproperties.tooltip.27", "Defaultwert für Pflichtfeld"));
		
		cbMandatoryValue = new JCheckBox();
		cbMandatoryValue.setVisible(false);
		cbMandatoryValue.setToolTipText(localeDelegate.getMessage("wizard.step.attributeproperties.tooltip.27", "Defaultwert für Pflichtfeld"));

		lbHidden = new JLabel(localeDelegate.getMessage("wizard.step.attributeproperties.14a", "Versteckt"));
		cbHidden = new JCheckBox();
		cbHidden.setToolTipText(localeDelegate.getMessage("wizard.step.attributeproperties.14a", "Versteckt"));

		lbModifiable = new JLabel(localeDelegate.getMessage("wizard.step.attributeproperties.14b", "Veränderbar?"));
		cbModifiable = new JCheckBox();
		cbModifiable.setToolTipText(localeDelegate.getMessage("wizard.step.attributeproperties.14b", "Veränderbar?"));
		
		lbDBFieldName = new JLabel(localeDelegate.getMessage("wizard.step.attributeproperties.12", "DB-Spaltename"));
		tfDBFieldName = new JTextField();
		tfDBFieldName.setToolTipText(localeDelegate.getMessage("wizard.step.attributeproperties.tooltip.12", "DB-Spaltename"));
		tfDBFieldName.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());

		lbDBFieldNameComplete = new JLabel(localeDelegate.getMessage("wizard.step.attributeproperties.18", "Vollst\u00e4ndiger Spaltenname"));
		tfDBFieldNameComplete = new JTextField();
		tfDBFieldNameComplete.setToolTipText(localeDelegate.getMessage("wizard.step.attributeproperties.tooltip.18", "Vollständiger Name der Datenbank-Tabellenspalte"));
		tfDBFieldNameComplete.setEnabled(false);

		lbAttributeGroup = new JLabel(localeDelegate.getMessage("wizard.step.attributeproperties.19", "Attributegruppe"));
		cbxAttributeGroup = new JComboBox();
		cbxAttributeGroup.setToolTipText(localeDelegate.getMessage("wizard.step.attributeproperties.tooltip.19", "Attributegruppe"));
		
		lbCalcFunction = new JLabel(localeDelegate.getMessage("wizard.step.attributeproperties.20", "Berechungsvorschrift"));
		cbxCalcFunction = new JComboBox();
		cbxCalcFunction.setToolTipText(localeDelegate.getMessage("wizard.step.attributeproperties.tooltip.20", "Berechungsvorschrift"));
		cbxCalcFunction.setRenderer(new DefaultListCellRenderer() {
			public Component getListCellRendererComponent(JList list,Object value,int index,boolean isSelected,boolean cellHasFocus){
				if (value != null && value instanceof String && callableFunctionsWithDescription != null) {
					if (callableFunctionsWithDescription.get((String) value) != null) {
						value = (String) value + " (" + callableFunctionsWithDescription.get((String) value) + ")";
					}
				}
				return (DefaultListCellRenderer) super.getListCellRendererComponent(list,value,index,isSelected,cellHasFocus);
			}
		});
		
		lbCalcFunctionParameter = new JLabel(localeDelegate.getMessage("wizard.step.attributeproperties.32", "Berechungsparameter"));
		tfCalcFunctionParameter = new JTextField();
		tfCalcFunctionParameter.setEditable(false);
		
		lbCalcAttributeAllowCustomization = new JLabel(localeDelegate.getMessage("wizard.step.attributeproperties.35", "Anpassung durch Benutzer"));
		cbCalcAttributeAllowCustomization = new JCheckBox();
		cbCalcAttributeAllowCustomization.setToolTipText(localeDelegate.getMessage("wizard.step.attributeproperties.tooltip.31", "Ein Benutzer darf die Parameterwerte ändern und kann sich das Attribut mehrfach einblenden."));
		
		lbCalcOndemand = new JLabel(localeDelegate.getMessage("wizard.step.attributeproperties.33", "Berechnung nur auf Anfrage"));
		cbCalcOndemand = new JCheckBox();
		cbCalcOndemand.setToolTipText(localeDelegate.getMessage("wizard.step.attributeproperties.tooltip.30", "Einblendbar in Suchergebnislisten. Nicht verfügbar in Masken und Regeln."));

		lbCalculationScript = new JLabel(localeDelegate.getMessage("wizard.step.attributeproperties.calculationscript.label", "Berechnungsausdruck konfigurieren"));
		lbCalculationScript.setToolTipText(localeDelegate.getMessage("wizard.step.attributeproperties.calculationscript.description", "Berechnungsausdruck konfigurieren"));
		btCalculationScript = new JButton("...");
		btCalculationScript.setToolTipText(localeDelegate.getMessage("wizard.step.attributeproperties.calculationscript.description", "Berechnungsausdruck konfigurieren"));
		btCalculationScript.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ScriptEditor editor = new ScriptEditor();
				if (getModel().getAttribute().getCalculationScript() != null) {
					editor.setScript(getModel().getAttribute().getCalculationScript());
				}
				editor.run(btCalculationScript);
				NuclosScript script = editor.getScript();
				if (org.nuclos.common2.StringUtils.isNullOrEmpty(script.getSource())) {
					script = null;
				}
				getModel().getAttribute().setCalculationScript(script);
			}
		});

		lbBackgroundColorScript = new JLabel(localeDelegate.getMessage("wizard.step.attributeproperties.backgroundcolorscript.label", "Hintergrundfarbe der Komponente konfigurieren"));
		lbBackgroundColorScript.setToolTipText(localeDelegate.getMessage("wizard.step.attributeproperties.backgroundcolorscript.description", "Hintergrundfarbe der Komponente konfigurieren"));
		btBackgroundColorScript = new JButton("...");
		btBackgroundColorScript.setToolTipText(localeDelegate.getMessage("wizard.step.attributeproperties.backgroundcolorscript.description", "Hintergrundfarbe der Komponente konfigurieren"));
		btBackgroundColorScript.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ScriptEditor editor = new ScriptEditor();
				if (getModel().getAttribute().getBackgroundColorScript() != null) {
					editor.setScript(getModel().getAttribute().getBackgroundColorScript());
				}
				editor.run(NuclosEntityAttributeCommonPropertiesStep.this);
				NuclosScript script = editor.getScript();
				if (org.nuclos.common2.StringUtils.isNullOrEmpty(script.getSource())) {
					script = null;
				}
				getModel().getAttribute().setBackgroundColorScript(script);
			}
		});

		lbIndexed = new JLabel(localeDelegate.getMessage("wizard.step.attributeproperties.26", "Indiziert?"));
		cbIndexed = new JCheckBox();
		cbIndexed.setToolTipText(localeDelegate.getMessage("wizard.step.attributeproperties.tooltip.26", "Indiziert?"));

		TableLayoutBuilder tbllay = new TableLayoutBuilder(this)
				.columns(TableLayout.PREFERRED, 20, 300, 150)
				.gaps(5 ,3);

		tbllay.newRow(20).add(lbLabel).add(tfLabel, 2);
		tbllay.newRow(20).add(lbDBFieldName).add(tfDBFieldName, 2);
		tbllay.newRow(20).add(lbDBFieldNameComplete).add(tfDBFieldNameComplete, 2);
		tbllay.newRow(20).add(lbAttributeGroup).add(cbxAttributeGroup, 2);
		tbllay.newRow(15);
		tbllay.newRow(20).add(lbMandatory).add(cbMandatory).add(tfMandatory);
		tbllay.newRow(20).add(lbDistinct).add(cbDistinct).add(lbDistinctMsg);
		tbllay.newRow(20).add(lbIndexed).add(cbIndexed);
		tbllay.newRow(20).add(lbDefaultValue).add(tfDefaultValue, 2);
		tbllay.newRow(15);
		tbllay.newRow(20).add(lbCalculationScript).add(btCalculationScript);
		tbllay.newRow(20).add(lbCalcFunction).addFullSpan(cbxCalcFunction);
		tbllay.newRow(20).skip().add(cbCalcOndemand).add(lbCalcOndemand);
		tbllay.newRow(20).add(lbCalcFunctionParameter).addFullSpan(tfCalcFunctionParameter);
		tbllay.newRow(20).skip().add(cbCalcAttributeAllowCustomization).add(lbCalcAttributeAllowCustomization);
		tbllay.newRow(15);
		tbllay.newRow(20).add(lbBackgroundColorScript).add(btBackgroundColorScript);
		tbllay.newRow(20).add(lbLogBook).add(cbLogBook);
		tbllay.newRow(20).add(lbHidden).add(cbHidden);
		tbllay.newRow(20).add(lbModifiable).add(cbModifiable);

		this.add(cbxDefaultValue, tbllay.getTableLayout().getConstraints(tfDefaultValue));
		this.add(lovDefaultValue, tbllay.getTableLayout().getConstraints(tfDefaultValue));
		this.add(dateDefaultValue, tbllay.getTableLayout().getConstraints(tfDefaultValue));
		this.add(cbDefaultValue, tbllay.getTableLayout().getConstraints(tfDefaultValue));

		this.add(cbxMandatory, tbllay.getTableLayout().getConstraints(tfMandatory));
		this.add(lovMandatory, tbllay.getTableLayout().getConstraints(tfMandatory));
		this.add(dateMandatory, tbllay.getTableLayout().getConstraints(tfMandatory));
		this.add(cbMandatoryValue, tbllay.getTableLayout().getConstraints(tfMandatory));

		tfLabel.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
				blnLabelModified = true;
				if (e.getKeyChar() == '_') {
					// NUCLOS-6405
					e.consume();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) {
				blnLabelModified = true;
			}
			@Override
			public void keyPressed(KeyEvent e) {
				blnLabelModified = true;
			}
		});

		tfLabel.setDocument(new SpecialCharacterDocument());
			tfLabel.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
			protected void doSomeWork(DocumentEvent e) {
				int size = e.getDocument().getLength();
				if(size > 0) {
					NuclosEntityAttributeCommonPropertiesStep.this.setComplete(true);
				} else  {
					NuclosEntityAttributeCommonPropertiesStep.this.setComplete(false);
				}
				
				try {
					NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().setInternalName(e.getDocument().getText(0, e.getDocument().getLength()));
					if(!NuclosEntityAttributeCommonPropertiesStep.this.getModel().isEditMode()
							&& !NuclosEntityAttributeCommonPropertiesStep.this.getModel().isProxy()
							&& !NuclosEntityAttributeCommonPropertiesStep.this.getModel().isGeneric()) {
						String sPrefix = Attribute.getDBPrefix(NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute());
						tfDBFieldName.setText(sPrefix + e.getDocument().getText(0, e.getDocument().getLength()));
					}
				} catch (BadLocationException ex) {
					Errors.getInstance().showExceptionDialog(NuclosEntityAttributeCommonPropertiesStep.this, ex);
				}
			}
		});

		tfDefaultValue.getDocument().addDocumentListener(new DefaultValueDocumentListener());

		tfMandatory.getDocument().addDocumentListener(new MandatoryValueDocumentListener());
		tfMandatory.setLocale(SpringLocaleDelegate.getInstance().getLocale());

		dateMandatory.getDocument().addDocumentListener(new MandatoryValueDocumentListener());
		dateMandatory.setLocale(SpringLocaleDelegate.getInstance().getLocale());
		
		lovMandatory.getJTextField().getDocument().addDocumentListener(new MandatoryValueDocumentListener());

		tfDBFieldName.setDocument(new LimitCharacterDocument());
		tfDBFieldName.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
			protected void doSomeWork(DocumentEvent e) {
				try {
					NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().setDbName(e.getDocument().getText(0, e.getDocument().getLength()));

					String s = e.getDocument().getText(0, e.getDocument().getLength());
					if(getModel().getAttribute().getForeignIntegrationPoint() != null ||
							(getModel().getAttribute().getMetaVO() != null && getModel().getAttribute().getField() != null)){
						s = "STRVALUE_" + s;
					}
					else if(getModel().getAttribute().getMetaVO() != null && getModel().getAttribute().getField() == null){
						s = "INTID_" + s;
					}
					if (!NuclosEntityAttributeCommonPropertiesStep.this.getModel().isProxy() &&
							!NuclosEntityAttributeCommonPropertiesStep.this.getModel().isGeneric()) {
						tfDBFieldNameComplete.setText(s);
					}
				} catch (BadLocationException ex) {
					Errors.getInstance().showExceptionDialog(NuclosEntityAttributeCommonPropertiesStep.this, ex);
				}
			}
		});

		cbDistinct.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				handleDistinctCheckBox((JCheckBox)e.getItem());
			}
		});

		cbLogBook.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				JCheckBox cb = (JCheckBox)e.getItem();
				NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().setLogBook(cb.isSelected());
			}
		});

		cbMandatory.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				final JCheckBox cb = (JCheckBox)e.getItem();
				NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().setMandatory(cb.isSelected());
				if (!NuclosEntityAttributeCommonPropertiesStep.this.getModel().isProxy()
					&& !NuclosEntityAttributeCommonPropertiesStep.this.getModel().isGeneric()
						//NUCLOS-6243: && NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().getForeignIntegrationPoint()==null
						) {
					if(NuclosEntityAttributeCommonPropertiesStep.this.parentWizardModel.isEditMode() && cb.isSelected() && !parentWizardModel.isVirtual()) {
						if(NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().getMandatoryValue() == null &&
						   !getModel().getAttribute().getDatatyp().getJavaType().equals("java.lang.Boolean")) {
							(new Bubble(cb, SpringLocaleDelegate.getInstance().getMessage(
									"wizard.step.attributeproperties.tooltip.28", "Bitte tragen Sie einen Wert ein mit dem das Feld vorbelegt werden kann!"), 3, Position.NW)).setVisible(true);
							NuclosEntityAttributeCommonPropertiesStep.this.setComplete(false);
						}
					} else {
						NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().setMandatoryValue(null);
						NuclosEntityAttributeCommonPropertiesStep.this.setComplete(true);
						if (lovMandatory.isVisible()) {
							lovMandatory.acceptLookedUpCollectable(null);
						} else if (dateMandatory.isVisible()) {
							dateMandatory.setDate(null);
						}
					}
				}
			}
		});

		cbIndexed.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				JCheckBox cb = (JCheckBox)e.getItem();
				NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().setIndexed(cb.isSelected());
			}
		});

		cbHidden.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				JCheckBox cb = (JCheckBox)e.getItem();
				NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().setHidden(cb.isSelected());
			}
		});

		cbModifiable.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				JCheckBox cb = (JCheckBox)e.getItem();
				NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().setModifiable(cb.isSelected());
			}
		});

		cbxDefaultValue.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					getModel().getAttribute().setIdDefaultValue((DefaultValue)e.getItem());
				}
			}
		});

		cbxMandatory.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					getModel().getAttribute().setMandatoryValue(((DefaultValue)e.getItem()).getValue());
				}
				checkMandatory();
			}
		});

		dateDefaultValue.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				doSomeWork(e);			
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				doSomeWork(e);		
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
			protected void doSomeWork(DocumentEvent e) {
				try {
					String value = e.getDocument().getText(0, e.getDocument().getLength());
					if("Heute".equalsIgnoreCase(value)) {
						value = RelativeDate.today().toString();
					}
					NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().setDefaultValue(value);
				} catch (BadLocationException ex) {
					Errors.getInstance().showExceptionDialog(NuclosEntityAttributeCommonPropertiesStep.this, ex);
				}
			}
		});

		cbDefaultValue.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				JCheckBox cb = (JCheckBox)e.getItem();
				if(cb.isSelected()) {
					NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().setDefaultValue("ja");
				} else {
					NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().setDefaultValue("nein");
				}
			}
		});

		cbMandatoryValue.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				JCheckBox cb = (JCheckBox)e.getItem();
				if(cb.isSelected()) {
					NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().setMandatoryValue(Boolean.TRUE);
				} else {
					NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().setMandatoryValue(Boolean.FALSE);
				}
			}
		});

		cbxCalcFunction.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					Object item = e.getItem();
					NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().setCalcFunction(null);
					NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().setCalcAttributeDS(null);
					if (item instanceof String && !" ".equals(item)) {
						NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().setCalcFunction((String) item);
						cbCalcOndemand.setEnabled(true);
						setCalcAttributeParamValue(null);
					}
					if (item instanceof CalcAttributeDS) {
						NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().setCalcAttributeDS(((CalcAttributeDS) item).getEO().getPrimaryKey());
						cbCalcOndemand.setEnabled(true);
						setCalcAttributeParamValue(null);
						showCalcAttributesParamValueEditor();
					}
					if (item == null || " ".equals(item)) {
						setCalcAttributeParamValue(null);
						cbCalcOndemand.setSelected(false);
						cbCalcOndemand.setEnabled(false);
					}
				}
			}
		});
		
		tfCalcFunctionParameter.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				showCalcAttributesParamValueEditor();
			}
		});
		
		cbCalcOndemand.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (prepareRunning) {
					return;
				}
				JCheckBox cb = (JCheckBox)e.getItem();
				NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().setCalcOndemand(cb.isSelected());
			}
		});
		
		cbCalcAttributeAllowCustomization.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				JCheckBox cb = (JCheckBox)e.getItem();
				NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().setCalcAttributeAllowCustomization(cb.isSelected());
				if (cb.isSelected()) {
					if (!prepareRunning) {
						cbCalcOndemand.setSelected(true);
					}
					cbCalcOndemand.setEnabled(false);
				} else {
					cbCalcOndemand.setEnabled(true);
				}
			}
		});
	}
	
	private void handleDistinctCheckBox(final JCheckBox cb) {
		if (cb.isSelected() && !distinctChecked) {
			
			Attribute attr = NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute();
			if (NuclosEntityAttributeCommonPropertiesStep.this.parentWizardModel.hasRows() && attr.getUID() != null) {
				try {
					cb.setEnabled(false);
					lbDistinctMsg.setText("Prüfung läuft...."); //Doesn't work here because it's within the Swing Thread
					boolean blnAllowed = StringUtils.isNullOrEmpty(attr.getInternalName()) ? false : MetaDataDelegate.getInstance().isChangeDatabaseColumnToUniqueAllowed(attr.getUID());
		
					if (!blnAllowed && !attr.isDistinct()) {
						cb.setSelected(false);
						cb.setEnabled(false);
						cb.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
								"wizard.step.entitysqllayout.6", "Das Feld {0} kann nicht auf ein eindeutiges Feld umgestellt werden.", attr.getLabel()));
						lbDistinctMsg.setText("Eindeutigkeit nicht möglich!");
					} else if (blnAllowed) {
						cb.setEnabled(true);
						lbDistinctMsg.setText("");
					}
				} catch (Exception e) {
					cb.setEnabled(true);
					// seems to be a new attribute- so do nothing here.
				}
				distinctChecked = true;
			}
		}
		
		NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().setDistinct(cb.isSelected());
		if(!cb.isSelected()) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					cbMandatory.setEnabled(true);
				}
			});
		}
	}
	
	private void checkMandatory() {
		if (cbMandatory.isSelected()) {
			NuclosEntityAttributeCommonPropertiesStep.this.setComplete(getModel().getAttribute().getDatatyp().getJavaType().equals("java.lang.Boolean") ||
					getModel().getAttribute().getMandatoryValue() != null);
		}
	}
	
	private void setCalcAttributeParamValue(String value) {
		NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().setCalcAttributeParamValues(value);
		tfCalcFunctionParameter.setText(value);
		cbCalcAttributeAllowCustomization.setEnabled(value != null);
		if (!prepareRunning) {
			if (NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().isCalcAttributeAllowCustomization()) {
				cbCalcAttributeAllowCustomization.setSelected(value != null);
			}
		}
	}
	
	private void showCalcAttributesParamValueEditor() {
		UID calcAttributeDS = NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().getCalcAttributeDS();
		if (calcAttributeDS == null) {
			return;
		}
		String paramValues = CalcAttributeClientHelper.showCalcAttributesParamValueEditor(NuclosEntityAttributeCommonPropertiesStep.this, null, calcAttributeDS, 
				NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().getCalcAttributeParamValues());
		setCalcAttributeParamValue(paramValues);
	}
	
	private class CalcAttributeDS {
		private final EntityObjectVO<UID> cads;

		public CalcAttributeDS(EntityObjectVO<UID> cads) {
			super();
			this.cads = cads;
		}
		
		public EntityObjectVO<UID> getEO() {
			return this.cads;
		}
		
		@Override
		public String toString() {
			String sNuclet = (String) cads.getFieldValue(E.CALCATTRIBUTE.nuclet.getUID());
			if (sNuclet != null) {
				return (sNuclet + "." + cads.getFieldValue(E.CALCATTRIBUTE.name));
			} else {
				return cads.getFieldValue(E.CALCATTRIBUTE.name);
			}
		}
	}
	
	private void setCalcFunctionSelectedItem() {
		ItemListener ilArray[] = cbxCalcFunction.getItemListeners();
		for(ItemListener il : ilArray) {
			cbxCalcFunction.removeItemListener(il);
		}
		
		cbCalcOndemand.setEnabled(false);
		
		String sCalcFunction = getModel().getAttribute().getCalcFunction();
		UID calcAttributeDS = getModel().getAttribute().getCalcAttributeDS();
		if (!RigidUtils.isNullOrEmpty(sCalcFunction)) {
			cbxCalcFunction.setSelectedItem(sCalcFunction);
			if (cbxCalcFunction.getSelectedIndex() <= 0) {
				Errors.getInstance().showExceptionDialog(NuclosEntityAttributeCommonPropertiesStep.this, new BusinessException("Calculated attribute function " + sCalcFunction + " not found!"));
			} else {
				cbCalcOndemand.setEnabled(true);
			}
		} else if (calcAttributeDS != null) {
			for (int i = 0; i < cbxCalcFunction.getModel().getSize(); i++) {
				Object o = cbxCalcFunction.getModel().getElementAt(i);
				if (o instanceof CalcAttributeDS) {
					CalcAttributeDS cads = (CalcAttributeDS) o;
					if (cads.getEO().getPrimaryKey().equals(calcAttributeDS)) {
						cbxCalcFunction.setSelectedIndex(i);
						cbCalcOndemand.setEnabled(true);
					}
				}
			}
		}
		
		for(ItemListener il : ilArray) {
			cbxCalcFunction.addItemListener(il);
		}
	}

	private void fillCalcFunctionBox() {
		ItemListener ilArray[] = cbxCalcFunction.getItemListeners();
		for(ItemListener il : ilArray) {
			cbxCalcFunction.removeItemListener(il);
		}
		cbxCalcFunction.removeAllItems();
		cbxCalcFunction.addItem(" ");
		
		try {
			final List<CalcAttributeDS> calcAttribueDatasources = new ArrayList<NuclosEntityAttributeCommonPropertiesStep.CalcAttributeDS>();
			for (MasterDataVO md : MasterDataCache.getInstance().get(E.CALCATTRIBUTE.getUID())) {
				calcAttribueDatasources.add(new CalcAttributeDS(md.getEntityObject()));
			}
			
			// sort callable functions (CA_*) alphabetically
			final Locale locale = LocaleContextHolder.getLocale();
			final Collator collator = Collator.getInstance(locale);
			collator.setStrength(Collator.PRIMARY);
			Collections.sort(calcAttribueDatasources, new Comparator<CalcAttributeDS>() {
				@Override
				public int compare(CalcAttributeDS o1, CalcAttributeDS o2) {
					return StringUtils.compareIgnoreCase(o1.getEO().getFieldValue(E.CALCATTRIBUTE.name), 
							o2.getEO().getFieldValue(E.CALCATTRIBUTE.name));
				}
			});
			
			for(CalcAttributeDS cads : calcAttribueDatasources) {
				cbxCalcFunction.addItem(cads);
			}	
		} catch (Exception e) {
			LOG.warn("error getting calculation attributes " + e.getMessage());
		}
		
		try {
			
			if (callableFunctionsWithDescription == null) {
				callableFunctionsWithDescription = AttributeDelegate.getInstance().getCallableFunctions(
						Class.forName(model.getAttribute().getDatatyp().getJavaType()), -1);
			}
					
			final List<String> callableFunctions = new ArrayList<String>(callableFunctionsWithDescription.keySet());
			
			// sort callable functions (CA_*) alphabetically
			final Locale locale = LocaleContextHolder.getLocale();
			final Collator collator = Collator.getInstance(locale);
			collator.setStrength(Collator.PRIMARY);
			Collections.sort(callableFunctions, collator);
			
			for(String sFunction : callableFunctions) {
				cbxCalcFunction.addItem(sFunction);
			}	
		} catch (Exception e) {
			LOG.warn("error getting calculation functions " + e.getMessage());
		}

		for(ItemListener il : ilArray) {
			cbxCalcFunction.addItemListener(il);
		}
	}
	
	private String internalNameOrig;
	
	@Override
	public void prepare() {
		super.prepare();
		prepareRunning = true;

		cbMandatory.setEnabled(true);

		requiresDefaultOld = getModel().getAttribute().isMandatory();
		
		fillCalcFunctionBox();

		Attribute.fillAttributeGroupBox(cbxAttributeGroup, getModel().getAttribute(), getModel().isEditMode());

		if (getModel().getAttribute().getDatatyp().getJavaType().equals("java.lang.Integer")
				|| getModel().getAttribute().getDatatyp().getJavaType().equals("java.lang.Long")
				|| getModel().getAttribute().getDatatyp().getJavaType().equals("java.lang.Double")) {
			try {
				tfDefaultValue.setInputVerifier(InputVerifierFactory.getInputVerifier(
						Class.forName(getModel().getAttribute().getDatatyp().getJavaType()),
						null,
						null,
						lbDefaultValue.getText()));
				tfDefaultValue.getDocument().addDocumentListener(new DefaultValueDocumentListener());

				tfMandatory.setInputVerifier(InputVerifierFactory.getInputVerifier(
						Class.forName(getModel().getAttribute().getDatatyp().getJavaType()),
						null,
						null,
						lbMandatory.getText()));
				tfMandatory.getDocument().addDocumentListener(new MandatoryValueDocumentListener());
			} catch (ClassNotFoundException e) {
				throw new NuclosFatalException(e);
			}
		}

		internalNameOrig = getModel().getAttribute().getInternalName();
		if(getModel().isEditMode()) {
			tfLabel.setText(internalNameOrig);
			//cbxCalcFunction.setSelectedItem(getModel().getAttribute().getCalcFunction());
			setCalcFunctionSelectedItem();
			setCalcAttributeParamValue(getModel().getAttribute().getCalcAttributeParamValues());
			cbCalcAttributeAllowCustomization.setSelected(getModel().getAttribute().isCalcAttributeAllowCustomization());
			cbCalcOndemand.setSelected(getModel().getAttribute().isCalcOndemand());
			cbDistinct.setSelected(getModel().getAttribute().isDistinct());
			ItemListener ilArray[] = cbMandatory.getItemListeners();
			for(ItemListener il : ilArray) {
				cbMandatory.removeItemListener(il);
			}
			cbMandatory.setSelected(getModel().getAttribute().isMandatory());
			for(ItemListener il : ilArray) {
				cbMandatory.addItemListener(il);
			}
			cbLogBook.setSelected(getModel().getAttribute().isLogBook());
			cbHidden.setSelected(getModel().getAttribute().isHidden());
			cbModifiable.setSelected(getModel().getAttribute().isModifiable());
			cbIndexed.setSelected(getModel().getAttribute().isIndexed());
			if(getModel().getAttribute().getDatatyp().getJavaType().equals("java.util.Date")) {
				String str = getModel().getAttribute().getDefaultValue();
				if(RelativeDate.today().toString().equals(str)) {
					dateDefaultValue.setDate(new Date(System.currentTimeMillis()));
					dateDefaultValue.getJTextField().setText("Heute");
				}
				else if ("Heute".equalsIgnoreCase(str)) {
					dateDefaultValue.setDate(new Date(System.currentTimeMillis()));
					dateDefaultValue.getJTextField().setText("Heute");
				}
				else {
					SimpleDateFormat result = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY);
					result.setLenient(false);
					try {
						dateDefaultValue.setDate((str == null) ? null : result.parse(str));
					}
					catch(Exception e) {
						// set no day
						LOG.warn("prepare failed: " + e, e);
					}
				}
			}
			else if(getModel().getAttribute().getDatatyp().getJavaType().equals("java.lang.Boolean")) {
			   String value = getModel().getAttribute().getDefaultValue();
				if(value != null && value.equalsIgnoreCase("ja")) {
					cbDefaultValue.setSelected(true);
				}
				else {
					cbDefaultValue.setSelected(false);
				}
			}
			else {
				tfDefaultValue.setText(getModel().getAttribute().getDefaultValue());
			}

			if(getModel().getAttribute().getDbName() != null) {
				String sModifiedDBName = new String(getModel().getAttribute().getDbName());

				tfDBFieldName.setText(sModifiedDBName.replaceFirst("^STRVALUE_", "").replaceFirst("^INTID_", "").replaceFirst("^STRUID_", ""));
				if(getModel().getAttribute().getForeignIntegrationPoint() != null ||
						(getModel().getAttribute().getMetaVO() != null && getModel().getAttribute().getField() != null)){
					tfDBFieldNameComplete.setText("STRVALUE_"+ getModel().getAttribute().getDbName());
				}
				else if(getModel().getAttribute().getMetaVO() != null && getModel().getAttribute().getField() == null){
					tfDBFieldNameComplete.setText("INTID_"+ getModel().getAttribute().getDbName());
				}
			}
		}
		else {
			if (!blnLabelModified) {
				// NUCLOS-6405
				String text = getModel().getName().toLowerCase();
				tfLabel.setText(text.replaceAll("_", ""));
			}

			if(getModel().getAttributeCount() == 0 && !blnDefaultSelected) {
				cbDistinct.setSelected(true);
				cbMandatory.setSelected(true);
				blnDefaultSelected = true;
			}
			cbDistinct.setEnabled(!this.parentWizardModel.hasRows());

			cbIndexed.setSelected(getModel().isReferenceType());
			cbLogBook.setSelected(this.parentWizardModel.isLogbook());
			
			if(getModel().getAttribute().getDbName() != null) {
				String sModifiedDBName = new String(getModel().getAttribute().getDbName());

				tfDBFieldName.setText(sModifiedDBName.replaceFirst("^STRVALUE_", "").replaceFirst("^INTID_", "").replaceFirst("^STRUID_", ""));
				if(getModel().getAttribute().getForeignIntegrationPoint() != null ||
						(getModel().getAttribute().getMetaVO() != null && getModel().getAttribute().getField() != null)){
					tfDBFieldNameComplete.setText("STRVALUE_"+ getModel().getAttribute().getDbName());
				}
				else if(getModel().getAttribute().getMetaVO() != null && getModel().getAttribute().getField() == null){
					tfDBFieldNameComplete.setText("INTID_"+ getModel().getAttribute().getDbName());
				}
			}

			cbModifiable.setSelected(true);
		}

		Object objMandatoryValue = getModel().getAttribute().getMandatoryValue();

		if((getModel().isReferenceType() && getModel().getAttribute().getMetaVO() != null) || getModel().isLookupType()) {

			ItemListener listener[] = cbxDefaultValue.getItemListeners();
			for(ItemListener il : listener){
				cbxDefaultValue.removeItemListener(il);
			}
			ItemListener listenerMandatory[] = cbxDefaultValue.getItemListeners();
			for(ItemListener il : listenerMandatory){
				cbxMandatory.removeItemListener(il);
			}
			boolean isValueList = getModel().getAttribute().isValueListProvider();

			cbxDefaultValue.setVisible(!isValueList);
			lovDefaultValue.setVisible(isValueList);
			tfDefaultValue.setVisible(false);
			dateDefaultValue.setVisible(false);
			cbDefaultValue.setVisible(false);

			cbxMandatory.setVisible(!isValueList && this.parentWizardModel.isEditMode());
			lovMandatory.setVisible(isValueList && this.parentWizardModel.isEditMode());
			tfMandatory.setVisible(false);
			dateMandatory.setVisible(false);
			cbMandatoryValue.setVisible(false);

			List<DefaultValue> defaultModel = new ArrayList<DefaultValue>();
			List<DefaultValue> mandatoryModel = new ArrayList<DefaultValue>();

			DefaultValue mandatoryValue = null;
			final UID entity = !getModel().isLookupType() ? getModel().getAttribute().getMetaVO().getUID() : getModel().getAttribute().getLookupMetaVO().getUID();
			
			if (!isValueList && !getModel().isGeneric()) {
				String sField = getModel().getAttribute().getField();
				
				if (sField != null) {
					
					Collection<UID> fields = new HashSet<UID>();
					Iterator<IFieldUIDRef> it = new ForeignEntityFieldUIDParser(sField, null, null).iterator();
					while (it.hasNext()) {
						IFieldUIDRef ref = it.next();
						if (ref.isUID()) {
							fields.add(ref.getUID());							
						}
					}
					
					ResultParams resultParams = new ResultParams(fields, 0L, 10000L, false);
					try {
						final Collection<EntityObjectVO<Object>> colVO = EntityObjectDelegate.getInstance()
								.getEntityObjectsChunk(entity, CollectableSearchExpression.TRUE_SEARCH_EXPR, resultParams, null);

						for (EntityObjectVO<Object> vo : colVO) {
							final DefaultValue dv = new DefaultValue(vo.getPrimaryKey(), RefValueExtractor.get(vo, sField, null));

							defaultModel.add(dv);
							mandatoryModel.add(dv);
							if (dv.getId() instanceof UID) {
								if (dv.getId().equals(UID.parseUID(objMandatoryValue == null ? null : objMandatoryValue.toString()))) {
									mandatoryValue = dv;
								}
							} else {
								String objMandatoryValueString = objMandatoryValue == null ? null : objMandatoryValue.toString();
								if ((dv.getId() != null && dv.getId().toString().equals(objMandatoryValueString)) || (dv.getValue() != null && dv.getValue().equals(objMandatoryValueString))){
									mandatoryValue = dv;
								}
							}
						}
					} catch (CommonPermissionException cpe) {
						// This should never happen as only super-user can use Bo-Wizard.
						throw new NuclosFatalException(cpe);
					}
				}
			
				Collections.sort(defaultModel);
				Collections.sort(mandatoryModel);

				defaultModel.add(0, new DefaultValue(null, null));
				mandatoryModel.add(0, new DefaultValue(null, null));
	
				cbxDefaultValue.setModel(new ListComboBoxModel<DefaultValue>(defaultModel));
				cbxDefaultValue.setSelectedItem(getModel().getAttribute().getIdDefaultValue());
	
				cbxMandatory.setModel(new ListComboBoxModel<DefaultValue>(mandatoryModel));
				if (mandatoryValue != null) {
					cbxMandatory.setSelectedItem(mandatoryValue);
				}
	
				for(ItemListener il : listener){
					cbxDefaultValue.addItemListener(il);
				}
	
				for(ItemListener il : listenerMandatory){
					cbxMandatory.addItemListener(il);
				}
			} else {
				final FieldMetaVO<?> efMetaDataVO = new FieldMetaVO();
				efMetaDataVO.setUID(model.getAttribute().getUID());
				efMetaDataVO.setFieldName(model.getAttribute().getInternalName());
				efMetaDataVO.setDbColumn(model.getAttribute().getDbName());
				efMetaDataVO.setDataType(String.class.getName());
				efMetaDataVO.setForeignEntity(entity);
				efMetaDataVO.setForeignEntityField(model.getAttribute().getField());
				efMetaDataVO.setSearchField(getModel().getAttribute().getSearchField());
				//NUCLOS-4687
				if (model.getAttribute().getMetaVO() != null) {
					efMetaDataVO.setEntity(model.getAttribute().getMetaVO().getUID());					
				}

				lovDefaultValue.setModel(getModel());
				lovDefaultValue.setEfMetaDataVO(efMetaDataVO);
				
				if(model.getAttribute().getDefaultValue()!=null) {
					lovDefaultValue.getJTextField().setText(model.getAttribute().getDefaultValue());
				}
				
				lovDefaultValue.setSelectedListener(new ListOfValues.SelectedListener() {
					@Override
					public void actionPerformed(CollectableValueIdField itemSelected) {
						if (itemSelected == null) {
							getModel().getAttribute().setIdDefaultValue(new DefaultValue(null, null));
						} else {
							getModel().getAttribute().setIdDefaultValue(
								new DefaultValue(IdUtils.unsafeToId(itemSelected.getValueId()), (String)itemSelected.getValue()));
						}
					}
				});
				lovMandatory.setModel(getModel());
				lovMandatory.setEfMetaDataVO(efMetaDataVO);

				if(model.getAttribute().getMandatoryValue() !=null) {
					lovMandatory.getJTextField().setText(model.getAttribute().getMandatoryValue().toString());
				}

				lovMandatory.setSelectedListener(new ListOfValues.SelectedListener() {
					@Override
					public void actionPerformed(CollectableValueIdField itemSelected) {
						if (itemSelected == null) {
							getModel().getAttribute().setMandatoryValue(new DefaultValue(null, null).getId());
						} else {
							getModel().getAttribute().setMandatoryValue
									(new DefaultValue(IdUtils.unsafeToId(itemSelected.getValueId()), (String)itemSelected.getValue()).getValue());
						}
						checkMandatory();
					}
				});

				if (objMandatoryValue != null && objMandatoryValue instanceof Integer) {
					try {
						final MasterDataVO mdvo = MasterDataDelegate.getInstance().get(efMetaDataVO.getForeignEntity() != null ? efMetaDataVO.getForeignEntity() : efMetaDataVO.getLookupEntity(), IdUtils.unsafeToId(objMandatoryValue));
						lovMandatory.acceptLookedUpCollectable(mdvo, null);
					} catch (Exception e) {
						// ignore
					}
				}
			}
		}
		else if(getModel().getAttribute().getDatatyp().getJavaType().equals("java.util.Date")) {
			dateDefaultValue.setVisible(true);
			cbxDefaultValue.setVisible(false);
			lovDefaultValue.setVisible(false);
			tfDefaultValue.setVisible(false);
			cbDefaultValue.setVisible(false);

			cbxMandatory.setVisible(false);
			lovMandatory.setVisible(false);
			tfMandatory.setVisible(false);
			dateMandatory.setVisible(this.parentWizardModel.isEditMode());
			if(objMandatoryValue != null) {
				String str = objMandatoryValue.toString();
				if(RelativeDate.today().toString().equals(str)) {
					dateMandatory.setDate(new Date(System.currentTimeMillis()));
					dateMandatory.getJTextField().setText("Heute");
				}
				else if ("Heute".equalsIgnoreCase(str)) {
					dateMandatory.setDate(new Date(System.currentTimeMillis()));
					dateMandatory.getJTextField().setText("Heute");
				}
				else {
					SimpleDateFormat result = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY);
					result.setLenient(false);
					try {
						dateMandatory.setDate((str == null) ? null : result.parse(str));
					}
					catch(Exception e) {
						// set no day
						LOG.warn("prepare failed: " + e, e);
					}
				}
			}

			cbMandatoryValue.setVisible(false);
		}
		else if(getModel().getAttribute().getDatatyp().getJavaType().equals("java.lang.Boolean")) {
			cbxDefaultValue.setVisible(false);
			lovDefaultValue.setVisible(false);
			tfDefaultValue.setVisible(false);
			dateDefaultValue.setVisible(false);
			cbDefaultValue.setVisible(true);
			
			cbMandatory.setSelected(true);
			cbMandatory.setEnabled(false);

			cbxMandatory.setVisible(false);
			lovMandatory.setVisible(false);
			tfMandatory.setVisible(false);
			dateMandatory.setVisible(false);
			cbMandatoryValue.setVisible(this.parentWizardModel.isEditMode());
			if(objMandatoryValue != null && objMandatoryValue instanceof Boolean) {
				cbMandatoryValue.setSelected((Boolean)objMandatoryValue);
			}
		}
		else if(getModel().getAttribute().isValueList()) {
			ItemListener listener[] = cbxDefaultValue.getItemListeners();
			for(ItemListener il : listener){
				cbxDefaultValue.removeItemListener(il);
			}

			cbxCalcFunction.setEnabled(false);
			cbxDefaultValue.setVisible(true);
			lovDefaultValue.setVisible(false);
			tfDefaultValue.setVisible(false);
			dateDefaultValue.setVisible(false);
			cbDefaultValue.setVisible(false);

			cbxDefaultValue.removeAllItems();
			cbxDefaultValue.addItem(new DefaultValue(null, null));
			cbxMandatory.addItem(new DefaultValue(null, null));
			for(ValueList valueList : getModel().getAttribute().getValueList()) {
				cbxDefaultValue.addItem(new DefaultValue(valueList.getId() != null ? valueList.getId().intValue() : null, valueList.getLabel()));
			}

			cbxDefaultValue.setSelectedItem(getModel().getAttribute().getDefaultValue());
			for(ItemListener il : listener){
				cbxDefaultValue.addItemListener(il);
			}

			listener = cbxMandatory.getItemListeners();
			for(ItemListener il : listener){
				cbxMandatory.removeItemListener(il);
			}

			for(ValueList valueList : getModel().getAttribute().getValueList()) {
				DefaultValue dv = new DefaultValue(valueList.getId() != null ? valueList.getId().intValue() : null, valueList.getLabel());
				cbxMandatory.addItem(dv);
				if(dv.getId() != null && dv.getId().equals(objMandatoryValue)) {
					cbxMandatory.setSelectedItem(dv);
				}
			}

			for(ItemListener il : listener){
				cbxMandatory.addItemListener(il);
			}

			cbMandatory.setEnabled(getModel().getAttribute().getUID() != null);
			cbxMandatory.setVisible(true);
			lovMandatory.setVisible(parentWizardModel.isEditMode() && getModel().getAttribute().getUID() != null);
			tfMandatory.setVisible(false);
			dateMandatory.setVisible(false);
			cbMandatoryValue.setVisible(false);
		}
		else if(getModel().getAttribute().isImage() || getModel().getAttribute().isPasswordField() || getModel().getAttribute().isFileType()) {
			cbMandatory.setEnabled(false);
			cbxMandatory.setVisible(false);
			lovMandatory.setVisible(false);
			tfMandatory.setVisible(false);
			dateMandatory.setVisible(false);
			cbMandatoryValue.setVisible(false);
		}
		else {
			cbxDefaultValue.setVisible(false);
			lovDefaultValue.setVisible(false);
			tfDefaultValue.setVisible(true);
			dateDefaultValue.setVisible(false);
			cbDefaultValue.setVisible(false);
			cbxMandatory.setVisible(false);
			lovMandatory.setVisible(false);
			tfMandatory.setVisible(this.parentWizardModel.isEditMode());
			if(objMandatoryValue != null) {
				tfMandatory.setText(objMandatoryValue.toString());
			}
			dateMandatory.setVisible(false);
			cbMandatoryValue.setVisible(false);
		}

		if (parentWizardModel.isVirtual()) {
			disableBunchOfInputs();
			tfDefaultValue.setEnabled(false);
			dateDefaultValue.setEnabled(false);
			cbxDefaultValue.setEnabled(false);
			cbDefaultValue.setEnabled(false);
		}
		
		if (parentWizardModel.isProxy() || parentWizardModel.isGeneric()) {
			disableBunchOfInputs();
			if (parentWizardModel.isGeneric()) {
				cbMandatory.setEnabled(true);
			}
			cbIndexed.setSelected(false);
			tfDBFieldName.setText(parentWizardModel.isProxy() ? "[proxy]" : "[generic]");
			tfDBFieldNameComplete.setText(parentWizardModel.isProxy() ? "[proxy]" : "[generic]");
		}

		if (parentWizardModel.isGeneric()) {
			tfDefaultValue.setEnabled(false);
			btCalculationScript.setEnabled(false);
			btBackgroundColorScript.setEnabled(false);
			cbHidden.setEnabled(false);
			cbModifiable.setEnabled(false);
		}
		
		if (this.model.getAttribute().isSystem()) {
			disableBunchOfInputs();
			tfLabel.setEditable(false);
			tfDefaultValue.setEditable(false);
			btCalculationScript.setEnabled(false);
		}

		if (this.model.getAttribute().getForeignIntegrationPoint() != null) {
			// NUCLOS-6243
			/*lbMandatory.setVisible(false);
			cbMandatory.setVisible(false);

			lbDistinct.setVisible(false);
			cbDistinct.setVisible(false);

			tfMandatory.setVisible(false);
			cbxMandatory.setVisible(false);
			lovMandatory.setVisible(false);
			dateMandatory.setVisible(false);
			cbMandatoryValue.setVisible(false);

			lbDefaultValue.setVisible(false);
			lovDefaultValue.setVisible(false);
			tfDefaultValue.setVisible(false);
			dateMandatory.setVisible(false);
			cbDefaultValue.setVisible(false);
			cbxDefaultValue.setVisible(false);*/

			lbCalcAttributeAllowCustomization.setVisible(false);
			lbCalcFunction.setVisible(false);
			lbCalcOndemand.setVisible(false);
			lbCalculationScript.setVisible(false);
			lbCalcFunctionParameter.setVisible(false);
			btCalculationScript.setVisible(false);
			cbxCalcFunction.setVisible(false);
			cbCalcOndemand.setVisible(false);
			tfCalcFunctionParameter.setVisible(false);
			cbCalcAttributeAllowCustomization.setVisible(false);

		}
		
		prepareRunning = false;

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				tfLabel.requestFocusInWindow();
			}
		});
	}
	
	private void disableBunchOfInputs() {
		cbDistinct.setSelected(false);
		cbDistinct.setEnabled(false);
		tfMandatory.setEnabled(false);
		dateMandatory.setEnabled(false);
		cbMandatory.setEnabled(false);
		cbxMandatory.setEnabled(false);
		cbMandatoryValue.setEnabled(false);
		lovMandatory.setEnabled(false);
		cbxCalcFunction.setEnabled(false);
		cbCalcAttributeAllowCustomization.setEnabled(false);
		cbCalcOndemand.setEnabled(false);
		cbLogBook.setEnabled(false);
		cbIndexed.setEnabled(false);
		tfDBFieldName.setEnabled(false);
	}

	public void setParentWizardModel(NuclosEntityWizardStaticModel model) {
		this.parentWizardModel = model;
	}
	
	@Override
	public void close() {
		lbLabel = null;
		tfLabel = null;
		lbDefaultValue = null;
		tfDefaultValue = null;
		cbxDefaultValue = null;
		lovDefaultValue = null;
		dateDefaultValue = null;
		cbDefaultValue = null;
		lbDBFieldName = null;
		tfDBFieldName = null;
		lbDBFieldNameComplete = null;
		tfDBFieldNameComplete = null;

		lbDistinct = null;
		cbDistinct = null;
		lbLogBook = null;
		cbLogBook = null;
		lbMandatory = null;
		cbMandatory = null;
		lbHidden = null;
		cbHidden = null;
		lbModifiable = null;
		cbModifiable = null;

		tfMandatory = null;
		cbxMandatory = null;
		lovMandatory = null;
		dateMandatory = null;
		cbMandatoryValue = null;

		lbIndexed = null;
		cbIndexed = null;

		lbAttributeGroup = null;
		cbxAttributeGroup = null;

		lbCalcFunction = null;
		cbxCalcFunction = null;

		lbCalculationScript = null;
		btCalculationScript = null;

		parentWizardModel = null;
		
		super.close();
	}

	@Override
	public void applyState() throws InvalidStateException {
		Attribute attr = getModel().getAttribute();
		if (attr.isSystem()) {
			super.applyState();
			return;
		}

		String sDBName = attr.getDbName();
		String sInternalName = attr.getInternalName();

		//NUCLOS-5107 3) These checks and errors only make sense if something has been changed
		if (!sInternalName.equals(internalNameOrig)) {
			
			if(!getModel().isEditMode() && !parentWizardModel.isProxy() && !parentWizardModel.isGeneric()) {
				for(Attribute attribute : this.parentWizardModel.getAttributeModel().getNucletAttributes()) {
					if(attribute.getDbName().equalsIgnoreCase(sDBName)
							|| attribute.getInternalName().equalsIgnoreCase(sInternalName)) {
						String sMessage = SpringLocaleDelegate.getInstance().getMessage(
								"wizard.step.attributeproperties.14", "<html>Der Spaltenname existiert schon in der Tabelle.<p> " +
							"Bitte \u00e4ndern Sie den Namen oder vergeben einen im Expertenmodus selbst.</html>");
	
						JLabel lb = new JLabel(sMessage);
						JOptionPane.showMessageDialog(this, lb);
						throw new InvalidStateException(sMessage);
					}
				}
			}
	
			if(sInternalName.length() > 250) {
				String sMessage = SpringLocaleDelegate.getInstance().getMessage(
						"wizard.step.attributeproperties.15", "<html>Der Feldname ist zu lang.<p> " +
				"Bitte \u00e4ndern Sie den Namen.</html>");
	
				JLabel lb = new JLabel(sMessage);
				JOptionPane.showMessageDialog(this, lb);
				throw new InvalidStateException(sMessage);
			}
	
			if (NuclosEntityValidator.isReservedName(sInternalName)) {
				String sMessage = SpringLocaleDelegate.getInstance().getMessage(
						"wizard.step.attributeproperties.16", "<html>Der Spaltenname wird vom System vergeben.<p> " +
								"Bitte \u00e4ndern Sie den Namen oder vergeben einen im Expertenmodus selbst.</html>");
				JLabel lb = new JLabel(sMessage);
				JOptionPane.showMessageDialog(this, lb);
				throw new InvalidStateException(sMessage);
			}			
	
			if (NuclosEntityValidator.isReservedDbColumnName(sDBName)) {
				String sMessage = SpringLocaleDelegate.getInstance().getMessage(
						"wizard.step.attributeproperties.16", "<html>Der Spaltenname wird vom System vergeben.<p> " +
								"Bitte \u00e4ndern Sie den Namen oder vergeben einen im Expertenmodus selbst.</html>");
				JLabel lb = new JLabel(sMessage);
				JOptionPane.showMessageDialog(this, lb);
				throw new InvalidStateException(sMessage);
			}
	
			checkAttributeNameCollisions(sInternalName);
			
		}

		int iCheck = 30;
		if(getModel().getAttribute().getMetaVO() != null) {
			iCheck -= 9;
		}

		if(sDBName.length() > iCheck) {
			String sMessage = SpringLocaleDelegate.getInstance().getMessage(
					"wizard.step.attributeproperties.17", "<html>Der Spaltenname ist zu lang.<p> " +
				"Bitte k\u00fcrzen Sie den Namen oder vergeben einen im Expertenmodus selbst.</html>");
			JLabel lb = new JLabel(sMessage);
			JOptionPane.showMessageDialog(this, lb);
			throw new InvalidStateException(sMessage);
		}


		checkReferenceAllowed();

		boolean requiresDefault = cbMandatory.isSelected() && !parentWizardModel.isVirtual() && !parentWizardModel.isProxy() && !parentWizardModel.isGeneric()
				; // NUCLOS-6243: && this.getModel().getAttribute().getForeignIntegrationPoint()==null;

		// NUCLOS-6543 2) shouldCheckDefault only make sense when requiresDefault == true, avoid count()
		boolean shouldCheckDefault = requiresDefault && this.parentWizardModel.isEditMode();
		if (shouldCheckDefault && !this.parentWizardModel.isProxy() && !this.parentWizardModel.isGeneric()) {
			final UID entityUid = this.parentWizardModel.getUID();
			if (Modules.getInstance().isModule(entityUid)) {
				shouldCheckDefault = GenericObjectDelegate.getInstance().countGenericObjectRows(entityUid,
						new CollectableGenericObjectSearchExpression(null, CollectableGenericObjectSearchExpression.SEARCH_BOTH)) > 0;
			} else {
				shouldCheckDefault = MasterDataDelegate.getInstance().countMasterDataRows(entityUid, new CollectableSearchExpression(null)) > 0;
			}
		}		
				
		if(this.getModel().getAttribute().getDatatyp().getJavaType().equals("java.util.Date") && shouldCheckDefault && requiresDefault) {
			Date date;
			try {
				date = dateMandatory.getDate();
			}
			catch(CommonValidationException e) {
				LOG.warn("applyState failed: " + e, e);
				String sMessage = e.toString();
				JLabel lb = new JLabel(sMessage);
				JOptionPane.showMessageDialog(this, lb);
				throw new InvalidStateException(sMessage);
			}
			if(date == null && (requiresDefaultOld != requiresDefault)) {
				String sMessage = SpringLocaleDelegate.getInstance().getMessage(
						"wizard.step.attributeproperties.tooltip.29", "Sie haben keinen Wert für das Pflichtfeld angegeben!");
				JLabel lb = new JLabel(sMessage);
				JOptionPane.showMessageDialog(this, lb);
				throw new InvalidStateException(sMessage);
			}
			else {
				this.getModel().getAttribute().setMandatoryValue(date);
			}
		}
		else if ((this.getModel().getAttribute().getDatatyp().getJavaType().equals("java.lang.Integer")
				|| this.getModel().getAttribute().getDatatyp().getJavaType().equals("java.lang.Long"))
				&& shouldCheckDefault && requiresDefault) {
			int iScale = this.getModel().getAttribute().getDatatyp().getScale();
			if (tfMandatory.getText().length() > iScale) {
				String sMessage = SpringLocaleDelegate.getInstance().getMessage(
						"wizard.step.attributeproperties.27", "Der angegebene Defaultwert hat keinen gültigen Wert!");
				JLabel lb = new JLabel(sMessage);
				JOptionPane.showMessageDialog(this, lb);
				throw new InvalidStateException(sMessage);
			}
		}
		else if(this.getModel().getAttribute().getDatatyp().getJavaType().equals("java.lang.Double") && shouldCheckDefault && requiresDefault) {
			try {
				CollectableFieldFormat.getInstance(Double.class).parse(this.getModel().getAttribute().getOutputFormat(), tfMandatory.getText());
			} catch (Exception e) {
				String sMessage = SpringLocaleDelegate.getInstance().getMessage(
						"wizard.step.attributeproperties.27", "Der angegebene Defaultwert hat keinen gültigen Wert!");
				JLabel lb = new JLabel(sMessage);
				JOptionPane.showMessageDialog(this, lb);
				throw new InvalidStateException("");
			}
		}
		else if(this.getModel().getAttribute().getDatatyp().getJavaType().equals("java.lang.Boolean") && shouldCheckDefault && requiresDefault) {
			if (getModel().getAttribute().getMandatoryValue() == null) {
				getModel().getAttribute().setMandatoryValue(Boolean.FALSE);
			}
		}

		if(shouldCheckDefault && requiresDefault) {
			Object obj = getModel().getAttribute().getMandatoryValue();
			String sMessage = null;
			if(obj != null && obj instanceof String && (requiresDefaultOld != requiresDefault)) {
				String s = (String)obj;
				if(s.length() < 1)
					sMessage = SpringLocaleDelegate.getInstance().getMessage(
							"wizard.step.attributeproperties.tooltip.29", "Sie haben keinen Wert für das Pflichtfeld angegeben!");
			}
			else if(obj == null && (requiresDefaultOld != requiresDefault)) {
				sMessage = SpringLocaleDelegate.getInstance().getMessage(
						"wizard.step.attributeproperties.tooltip.29", "Sie haben keinen Wert für das Pflichtfeld angegeben!");
			}

			if(sMessage != null) {
				JLabel lb = new JLabel(sMessage);
				JOptionPane.showMessageDialog(this, lb);
				throw new InvalidStateException(sMessage);
			}
		} else {
			this.getModel().getAttribute().setMandatoryValue(null);
		}

		if (parentWizardModel.isVirtual()) {
			attr.setReadonly(!parentWizardModel.isEditable());
		}

		if (getModel().getAttribute().getForeignIntegrationPoint() != null
				&& getModel().getAttribute().getMetaVO() == null) {
			// optional or not yet integrated point (NUCLOS-6248)
			// create a dummy hidden attribute
			attr.setMetaVO(E.DUMMY);
			attr.setField(E.stringify(E.DUMMY.name));
		}
		
		super.applyState();
	}

	private void checkAttributeNameCollisions(final String sInternalName) throws InvalidStateException {
		final UID entityUid = this.getModel().getWizardModel().getUID();
		if (!NuclosEntityValidator.isValidAttributeName(sInternalName, entityUid, MetaProvider.getInstance())) {
			final String sMessage = SpringLocaleDelegate.getInstance().getMessage(
					"wizard.step.attributeproperties.82", "<html>Der Attributname ist nicht zulässig.<p> " +
							"Der angegebene Attributname ist ungültig, da er zu Namens-Kollisionen führt (z.B. aufgrund von Referenzen auf diese Entität). " +
							"Bitte ändern Sie den Namen.</html>");
			JLabel lb = new JLabel(sMessage);
			JOptionPane.showMessageDialog(this, lb);
			throw new InvalidStateException(sMessage);
		}
	}

	private void checkReferenceAllowed() throws InvalidStateException {
		if (this.getModel().isReferenceType()) {
			final String entityName = this.getModel().getWizardModel().getEntityName();
			EntityMeta<?> foreignEntityMetaVO = this.getModel().getAttribute().getMetaVO();
			if (foreignEntityMetaVO != null && !NuclosEntityValidator.isValidReference(entityName, foreignEntityMetaVO)) {
				final String sMessage = SpringLocaleDelegate.getInstance().getMessage(
						"wizard.step.attributeproperties.81", "<html>Die Zielentität kann nicht referenziert werden.<p> " +
								"Die Zielentität enthält bereits ein Attribut mit dem gleichen Namen wie diese Entität, daher kann die Referenz nicht erstellt werden. " +
								"Entfernen Sie zuerst das betroffene Attribut oder ändern Sie den Namen dieser Entität.</html>");
				JLabel lb = new JLabel(sMessage);
				JOptionPane.showMessageDialog(this, lb);
				throw new InvalidStateException(sMessage);
			}
		}
	}

	static class LimitCharacterDocument extends PlainDocument {
		@Override
		public void insertString(int offset, String str, javax.swing.text.AttributeSet a)  throws javax.swing.text.BadLocationException {
			if(offset > 22) {
				return;
			}
			super.insertString(offset, str, a);
	    }
	}

	class DefaultValueDocumentListener implements DocumentListener {

		@Override
		public void changedUpdate(DocumentEvent e) {
			doSomeWork(e);
		}
		@Override
		public void insertUpdate(DocumentEvent e) {
			doSomeWork(e);
		}
		@Override
		public void removeUpdate(DocumentEvent e) {
			doSomeWork(e);
		}
		protected void doSomeWork(DocumentEvent e) {
			try {
				NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().setDefaultValue(e.getDocument().getText(0, e.getDocument().getLength()));

			} catch (BadLocationException ex) {
				Errors.getInstance().showExceptionDialog(NuclosEntityAttributeCommonPropertiesStep.this, ex);
			}
		}
	}

	class MandatoryValueDocumentListener implements DocumentListener {
		@Override
		public void changedUpdate(DocumentEvent e) {
			doSomeWork(e);
		}

		@Override
		public void insertUpdate(DocumentEvent e) {
			doSomeWork(e);
		}

		@Override
		public void removeUpdate(DocumentEvent e) {
			doSomeWork(e);
		}

		protected void doSomeWork(DocumentEvent e) {
			try {
				String sJavaType = NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().getDatatyp().getJavaType();
				String sValue = e.getDocument().getText(0, e.getDocument().getLength());
				if (sJavaType.equals("java.lang.Integer") || sJavaType.equals("java.lang.Long")) {
					try {
						Number value = sJavaType.equals("java.lang.Integer") ? Integer.valueOf(sValue) : Long.valueOf(sValue);
						NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().setMandatoryValue(value);
					} catch (Exception e1) {
						LOG.warn("doSomeWork failed: " + e1, e1);
						NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().setMandatoryValue(null);
					}
				} else if (sJavaType.equals("java.lang.Double")) {
					sValue = sValue.replace(',', '.');
					try {
						Double d = new Double(sValue);
						NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().setMandatoryValue(d);
					} catch (Exception e1) {
						LOG.warn("doSomeWork failed: " + e1, e1);
						NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().setMandatoryValue(null);
					}
				} else if (sJavaType.equals("java.util.Date")) {
					try {
						String value = e.getDocument().getText(0, e.getDocument().getLength());
						if ("Heute".equalsIgnoreCase(value)) {
							value = RelativeDate.today().toString();
						}
						if ("".equals(value)) {
							value = null;
						}
						NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().setMandatoryValue(value);
					} catch (BadLocationException ex) {
						Errors.getInstance().showExceptionDialog(NuclosEntityAttributeCommonPropertiesStep.this, ex);
					}
				} else {
					if ("".equals(sValue)) {
						sValue = null;
					}
					NuclosEntityAttributeCommonPropertiesStep.this.getModel().getAttribute().setMandatoryValue(sValue);
				}
				checkMandatory();
			} catch (BadLocationException ex) {
				Errors.getInstance().showExceptionDialog(NuclosEntityAttributeCommonPropertiesStep.this, ex);
			}
		}
	}

	static class SpecialCharacterDocument extends PlainDocument {
		@Override
		public void insertString(int offset, String str, javax.swing.text.AttributeSet a)  throws javax.swing.text.BadLocationException {
			str = StringUtils.replace(str, "\u00e4", "ae");
			str = StringUtils.replace(str, "\u00f6", "oe");
			str = StringUtils.replace(str, "\u00fc", "ue");
			str = StringUtils.replace(str, "\u00c4", "ae");
			str = StringUtils.replace(str, "\u00d6", "oe");
			str = StringUtils.replace(str, "\u00dc", "ue");
			str = StringUtils.replace(str, "\u00df", "ss");
			str = str.replaceAll("[^\\w]", "");
			super.insertString(offset, str, a);
	    }
	}
}
