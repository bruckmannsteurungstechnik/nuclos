//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.wizard.steps;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;

import org.apache.log4j.Logger;
import org.nuclos.client.common.EntityUtils;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.masterdata.MetaDataDelegate;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.wizard.NuclosEntityWizardStaticModel;
import org.nuclos.client.wizard.util.NuclosWizardUtils;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.pietschy.wizard.InvalidStateException;

import info.clearthought.layout.TableLayout;

/**
* <br>
* Created by Novabit Informationssysteme GmbH <br>
* Please visit <a href="http://www.novabit.de">www.novabit.de</a>
*
* @author <a href="mailto:marc.finke@novabit.de">Marc Finke</a>
* @version 01.00.00
*/
public class NuclosEntityAttributeLookupShipStep extends NuclosEntityAttributeAbstractStep {

	private static final Logger LOG = Logger.getLogger(NuclosEntityAttributeLookupShipStep.class);

	private JLabel lbEntity;
	private JComboBox cbxEntity;

	private JScrollPane scrollPane;
	private JList listFields;
	private JButton btSelect;
	private JButton btSelectSearch;
	private JLabel lblSelectSearch;

	private JLabel lbInfo;

	private JTextField tfAlternativeLabel;
	private JTextField tfAlternativeSearchLabel;

	private JLabel lbValueListProvider;
	private JCheckBox cbValueListProvider;
	
	private NuclosEntityWizardStaticModel parentWizardModel;

	public NuclosEntityAttributeLookupShipStep(String name, String summary) {
		super(name, summary);
		initComponents();
	}

	@Override
	protected void initComponents() {
		double size [][] = {{TableLayout.PREFERRED,40, TableLayout.FILL}, {20,20,20,20,50,20,TableLayout.PREFERRED, TableLayout.FILL}};

		TableLayout layout = new TableLayout(size);
		layout.setVGap(3);
		layout.setHGap(5);
		this.setLayout(layout);

		lbEntity = new JLabel(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributerelationship.1", "Verweis auf Entit\u00e4t")+": ");
		cbxEntity = new JComboBox();
		cbxEntity.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributerelationship.tooltip.1", "Verweis auf Entit\u00e4t"));

		tfAlternativeLabel = new JTextField();
		tfAlternativeLabel.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributerelationship.tooltip.3", "Fremdschl\u00fcsselaufbau"));
		tfAlternativeLabel.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());

		tfAlternativeSearchLabel = new JTextField();
		tfAlternativeSearchLabel.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributerelationship.tooltip.3a", "Suchschl\u00fcsselaufbau"));
		tfAlternativeSearchLabel.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());

		lbValueListProvider = new JLabel(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributerelationship.4", "Suchfeld"));
		cbValueListProvider = new JCheckBox();
		cbValueListProvider.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributerelationship.tooltip.4", "Suchfeld"));

		lbInfo = new JLabel();

		listFields = new JList();
		listFields.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane = new JScrollPane(listFields);
		scrollPane.setPreferredSize(new Dimension(150, 25));
		btSelect = new JButton(">");
		btSelectSearch = new JButton(">");
		
		lblSelectSearch = new JLabel(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributerelationship.9", "Optionale Suchkriterien")+": ");
		lblSelectSearch.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributerelationship.tooltip.9", "Optionale Suchkriterien, die bei einer Suche herangezogen werden. Wenn leer, werden die normalen Kriterien genutzt."));
		
		// set search field visible
		lblSelectSearch.setVisible(false);
		btSelectSearch.setVisible(false);
		tfAlternativeSearchLabel.setVisible(false);

		this.add(lbEntity, "0,0, 1,0");
		this.add(cbxEntity, "2,0");

		this.add(lbValueListProvider, "0,1, 1,1");
		this.add(cbValueListProvider, "2,1");


		this.add(scrollPane, "0,2, 0,7");

		this.add(btSelect, "1,2");

		this.add(tfAlternativeLabel, "2,2");
		this.add(lbInfo, "0,6, 2,6");

	    this.add(lblSelectSearch, "1,4,2,0");
		this.add(btSelectSearch, "1,5");
		this.add(tfAlternativeSearchLabel, "2,5");
		
		cbxEntity.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(final ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					final Object obj = e.getItem();
					if(obj instanceof EntityMeta<?>) {
						final EntityMeta<?> entity = (EntityMeta<?>)obj;
						
						final List<FieldMeta<?>> lstFields
								= new ArrayList<FieldMeta<?>>(MetaProvider.getInstance().getAllEntityFieldsByEntity(entity.getUID()).values());
						Collections.sort(lstFields, EntityUtils.getMetaComparator(FieldMeta.class));
					
						final List<FieldMeta<?>> lstFieldItems = new ArrayList<FieldMeta<?>>();
						for(FieldMeta<?> field : lstFields) {
							//@see NUCLOSINT-1232
							if(field.getForeignEntity() == null
									//&& voField.getLookupEntity() == null
									&& !field.isCalculated()
									&& field.getCalculationScript() == null)
								lstFieldItems.add(EntityUtils.wrapMetaData(field));
						}
					
						listFields.setListData(lstFieldItems.toArray());
						
						NuclosEntityAttributeLookupShipStep.this.model.getAttribute().setLookupMetaVO(entity);
						NuclosEntityAttributeLookupShipStep.this.setComplete(true);

						if (StringUtils.isNullOrEmpty(model.getAttribute().getField())) {
							//@see NUCLOS-1037
							final String sLabel = entity.getLocaleResourceIdForTreeView() == null ? ""
									: SpringLocaleDelegate.getInstance().getTextForStaticLabel(entity.getLocaleResourceIdForTreeView());
							if (!StringUtils.isNullOrEmpty(sLabel)) {
								tfAlternativeLabel.setText(sLabel.indexOf("$") == -1 ? "" : sLabel);
								final String sFieldUids = RigidUtils.replaceParameters(RigidUtils.PARAM_PATTERN_UID, sLabel, new Transformer<String, String>() {
									@Override
									public String transform(String i) {
										for(FieldMeta<?> field : entity.getFields()) {
											if (field.getFieldName().equals(i))
												return field.getUID().getStringifiedDefinition();
										}
										return "";
									}					
								});
								NuclosEntityAttributeLookupShipStep.this.model.getAttribute().setField(sFieldUids);
							} else {
								tfAlternativeLabel.setText("");
								NuclosEntityAttributeLookupShipStep.this.model.getAttribute().setField(null);
							}
						}
						
						NuclosEntityAttributeLookupShipStep.this.setComplete(
								!StringUtils.isNullOrEmpty(NuclosEntityAttributeLookupShipStep.this.model.getAttribute().getField()));

						try {
							checkReferenceField();
						} catch(Exception e1) {
							LOG.debug("itemStateChanged: checkReference failed: " + e1);
							tfAlternativeLabel.setText("");
							NuclosEntityAttributeLookupShipStep.this.model.getAttribute().setField(null);
							NuclosEntityAttributeLookupShipStep.this.setComplete(false);
						}
						try {
							checkReferenceSearchField();
						}
						catch(Exception e1) {
							LOG.debug("itemStateChanged: checkReference failed: " + e1);
							tfAlternativeSearchLabel.setText("");
							NuclosEntityAttributeLookupShipStep.this.model.getAttribute().setSearchField(null);
							NuclosEntityAttributeLookupShipStep.this.setComplete(false);
						}
					}
					else {
						NuclosEntityAttributeLookupShipStep.this.model.getAttribute().setLookupMetaVO(null);
						NuclosEntityAttributeLookupShipStep.this.setComplete(false);
					}
				}
			}
		});

		btSelect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
 				 setKeyDescription();
			}
		});
		btSelectSearch.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
 				 setSearchKeyDescription();
			}
		});

		listFields.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(SwingUtilities.isLeftMouseButton(e)) {
					if(e.getClickCount() == 2) {
						 setKeyDescription();
					}
				}
			}
		});

		cbValueListProvider.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				final JCheckBox cb = (JCheckBox)e.getItem();
				NuclosEntityAttributeLookupShipStep.this.model.getAttribute().setValueListProvider(cb.isSelected());

				// set search field visible
				lblSelectSearch.setVisible(cb.isSelected());
				btSelectSearch.setVisible(cb.isSelected());
				tfAlternativeSearchLabel.setVisible(cb.isSelected());
			}
		});

		tfAlternativeLabel.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
			protected void doSomeWork(DocumentEvent e) {
				try {
					final String sText = e.getDocument().getText(0, e.getDocument().getLength());
					final String sFieldUids = RigidUtils.replaceParameters(RigidUtils.PARAM_PATTERN_$, sText, new Transformer<String, String>() {
						@Override
						public String transform(String i) {
							for(FieldMeta<?> field : model.getAttribute().getLookupMetaVO().getFields()) {
								if (field.getFieldName().equals(i))
									return field.getUID().getStringifiedDefinition();
							}
							return "";
						}					
					});
					model.getAttribute().setField(sFieldUids);
					NuclosEntityAttributeLookupShipStep.this.model.getAttribute().setField(sFieldUids);
				}
				catch (BadLocationException ex) {
					Errors.getInstance().showExceptionDialog(NuclosEntityAttributeLookupShipStep.this, ex);
				}				

				//tfAlternativeLabel.setForeground(Color.black);
				NuclosEntityAttributeLookupShipStep.this.setComplete(e.getDocument().getLength() > 0);
				
				try {
					checkReferenceField();
				} catch (Exception ex) {
					//tfAlternativeLabel.setForeground(Color.red);
					NuclosEntityAttributeLookupShipStep.this.setComplete(false);
				}
			}
		});
		tfAlternativeSearchLabel.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
	
			@Override
			public void insertUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
	
			@Override
			public void changedUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
	
			protected void doSomeWork(DocumentEvent e) {
				try {
					final String sText = e.getDocument().getText(0, e.getDocument().getLength());
					final String sFieldUids = RigidUtils.replaceParameters(RigidUtils.PARAM_PATTERN_$, sText, new Transformer<String, String>() {
						@Override
						public String transform(String i) {
							for(FieldMeta<?> field : model.getAttribute().getLookupMetaVO().getFields()) {
								if (field.getFieldName().equals(i))
									return field.getUID().getStringifiedDefinition();
							}
							return "";
						}					
					});
					model.getAttribute().setSearchField(sFieldUids);
					NuclosEntityAttributeLookupShipStep.this.model.getAttribute().setSearchField(sFieldUids);
				}
				catch (BadLocationException ex) {
					Errors.getInstance().showExceptionDialog(NuclosEntityAttributeLookupShipStep.this, ex);
				}				
	
				//tfAlternativeSearchLabel.setForeground(Color.black);
				//NuclosEntityAttributeRelationShipStep.this.setComplete(e.getDocument().getLength() > 0);
				
				try {
					checkReferenceSearchField();
				} catch (Exception ex) {
					//tfAlternativeLabel.setForeground(Color.red);
					//NuclosEntityAttributeRelationShipStep.this.setComplete(false);
				}
			}
		});
	}


	@Override
	public void prepare() {
		super.prepare();
		
		fillEntityCombobox();
		
		final EntityMeta<?> metaOld =this.model.getAttribute().getLookupMetaVO();
		final String searchFieldOld = model.getAttribute().getSearchField();
		
		if(this.model.getAttribute().getLookupMetaVO() != null) {
			if(model.getAttribute().getField() != null) {
				final String sField = RigidUtils.replaceParameters(RigidUtils.PARAM_PATTERN_UID, model.getAttribute().getField(), new Transformer<String, String>() {
					@Override
					public String transform(String i) {
						for(FieldMeta<?> field : model.getAttribute().getLookupMetaVO().getFields()) {
							if (field.getUID().getStringifiedDefinition().equals(i))
								return "${" + field.getFieldName() + "}";
						}
						return "";
					}					
				});
				tfAlternativeLabel.setText(sField);
				
				cbxEntity.setSelectedItem(model.getAttribute().getLookupMetaVO());
				
				cbValueListProvider.setSelected(this.model.getAttribute().isValueListProvider());
			}
			else {
				cbxEntity.setSelectedItem(this.model.getAttribute().getLookupMetaVO());
			}
			if(searchFieldOld != null && metaOld.getUID().equals(this.model.getAttribute().getLookupMetaVO().getUID())) {
				final String sField = RigidUtils.replaceParameters(RigidUtils.PARAM_PATTERN_UID, model.getAttribute().getSearchField(), new Transformer<String, String>() {
					@Override
					public String transform(String i) {
						for(FieldMeta<?> field : model.getAttribute().getLookupMetaVO().getFields()) {
							if (field.getUID().getStringifiedDefinition().equals(i))
								return "${" + field.getFieldName() + "}";
						}
						return "";
					}					
				});
				
				tfAlternativeSearchLabel.setText(sField);
//				model.getAttribute().setSearchField(sField);
			}
			
			// set search field visible
			lblSelectSearch.setVisible(cbValueListProvider.isSelected());
			btSelectSearch.setVisible(cbValueListProvider.isSelected());
			tfAlternativeSearchLabel.setVisible(cbValueListProvider.isSelected());
		}

		if(parentWizardModel.hasRows() && model.isEditMode() && !parentWizardModel.isVirtual()) {
			cbxEntity.setEnabled(false);
			cbxEntity.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributerelationship.tooltip.5", "Verweis kann nicht geändert werden. Da bereits Datensätze vorhanden sind."));
		}
		else {
			cbxEntity.setEnabled(true);
			cbxEntity.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributerelationship.tooltip.1", "Verweis auf Entit\u00e4t"));
		}

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				cbxEntity.requestFocusInWindow();
			}
		});
	}

	private void fillEntityCombobox() {
		final ItemListener ilArray[] = cbxEntity.getItemListeners();
		for(ItemListener il : ilArray) {
			cbxEntity.removeItemListener(il);
		}

		cbxEntity.removeAllItems();

		final List<EntityMeta<?>> lstEntities
			= new ArrayList<EntityMeta<?>>(MetaProvider.getInstance().getAllEntities());
		Collections.sort(lstEntities, EntityUtils.getMetaComparator(EntityMeta.class));

		cbxEntity.addItem(EntityUtils.wrapMetaData(EntityMeta.NULL));
		
		for(EntityMeta<?> entity : lstEntities) {
			if (E.isNuclosEntity(entity.getUID())) {
				if(E.PROCESS.getUID().equals(entity.getUID()) ||
				   E.USER.getUID().equals(entity.getUID()) ||
				   E.ROLE.getUID().equals(entity.getUID()))
				cbxEntity.addItem(EntityUtils.wrapMetaData(entity));
			} else {
				cbxEntity.addItem(EntityUtils.wrapMetaData(entity));
			}
		}

		for(ItemListener il : ilArray) {
			cbxEntity.addItemListener(il);
		}
	}

	public void setParentWizardModel(NuclosEntityWizardStaticModel model) {
		this.parentWizardModel = model;
	}

	@Override
	public void close() {
		lbEntity = null;
		cbxEntity = null;

		scrollPane = null;
		listFields = null;
		btSelect = null;

		lbInfo = null;
		lblSelectSearch = null;

		tfAlternativeLabel = null;
		tfAlternativeSearchLabel = null;

		lbValueListProvider = null;
		cbValueListProvider = null;
		
		parentWizardModel = null;
		
		super.close();
	}

	@Override
	public void applyState() throws InvalidStateException {
		checkReferenceField();
		checkReferenceSearchField();
		this.model.nextStep();
		this.model.nextStep();
		this.model.refreshModelState();
		
		super.applyState();
	}

	private void checkReferenceField() throws InvalidStateException {
		final String sField = this.model.getAttribute().getField();
		if (StringUtils.isNullOrEmpty(sField))
			return;
		
		final Pattern referencedEntityPattern = Pattern.compile ("uid[{][\\w|\\-\\[\\]]+[}]");
		final Matcher referencedEntityMatcher = referencedEntityPattern.matcher(sField);
		
		boolean invalid = true;
		while (referencedEntityMatcher.find()) {
			String sUID = referencedEntityMatcher.group().substring(4, referencedEntityMatcher.group().length() - 1);
			try {
				final FieldMeta<?> voField = MetaDataDelegate.getInstance().getEntityField(new UID(sUID));
				//@see NUCLOSINT-1232
				if(voField.getForeignEntity() == null
						//&& voField.getLookupEntity() == null
						&& !voField.isCalculated()
						&& voField.getCalculationScript() == null)
				invalid = false;
				else {
					invalid = true;
					break;
				}
		  }
		  catch(Exception e){
			  throw new InvalidStateException(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributerelationship.7", "Es wurde ein ungültiger Eintrag gefunden: " + sUID, sUID));
		  }
		}

		if(invalid){
			throw new InvalidStateException(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributerelationship.8", "Es wurde kein gültiger Referenzeintrag gefunden!"));
		}
	}
	
	private void checkReferenceSearchField() throws InvalidStateException {
		String sField = this.model.getAttribute().getSearchField();
		if (sField == null || sField.length() < 1)
			return;
		final Pattern referencedEntityPattern = Pattern.compile ("uid[{][\\w|\\-\\[\\]]+[}]");
		Matcher referencedEntityMatcher = referencedEntityPattern.matcher(sField);
		boolean invalid = true;
		while (referencedEntityMatcher.find()) {
			String sName = referencedEntityMatcher.group().substring(4, referencedEntityMatcher.group().length() - 1);
			try {
				FieldMeta<?> voField = MetaDataDelegate.getInstance().getEntityField(new UID(sName));
				//@see NUCLOSINT-1232
				if(voField.getForeignEntity() == null
						//&& voField.getLookupEntity() == null
						&& !voField.isCalculated())
				invalid = false;
				else {
					invalid = true;
					break;
				}
			}
			catch (Exception e) {
				throw new InvalidStateException(SpringLocaleDelegate.getInstance().getMessage(
						"wizard.step.attributerelationship.7a", "Es wurde ein ungültiger Eintrag für die Suche gefunden: " + sName,
						sName));
			}
		}

		if (invalid) {
			final String msg = "Es wurde kein gültiger Referenzeintrag für die Suche gefunden: Feld '" 
					+ sField + "'";
			throw new InvalidStateException(SpringLocaleDelegate.getInstance().getMessage(
					"wizard.step.attributerelationship.8a", msg));
			// LOG.warn(msg);
		}
	}


	private void setKeyDescription() {
		 final FieldMeta<?> field = (FieldMeta<?>)listFields.getSelectedValue();
		 if(field == null) {
			 return;
		 }
		 String strText = tfAlternativeLabel.getText();
		 strText += "${" + field.getFieldName() + "}";
		 
		 tfAlternativeLabel.setText(strText);
	}

	private void setSearchKeyDescription() {
		final FieldMeta<?> field = (FieldMeta<?>)listFields.getSelectedValue();
		 if(field == null) {
			 return;
		 }
		 String strText = tfAlternativeSearchLabel.getText();
		 strText += "${" + field.getFieldName() + "}";

		 tfAlternativeSearchLabel.setText(strText);
	}

}
