//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.wizard.steps;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.nuclos.client.common.EntityUtils;
import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.client.masterdata.MetaDataDelegate;
import org.nuclos.client.ui.table.TableUtils;
import org.nuclos.client.wizard.model.Attribute;
import org.nuclos.client.wizard.model.DataTyp;
import org.nuclos.client.wizard.model.EntityAttributeSelectTableModel;
import org.nuclos.client.wizard.model.EntityAttributeTableModel;
import org.nuclos.client.wizard.util.NuclosWizardUtils;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.TranslationVO;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.pietschy.wizard.InvalidStateException;

import info.clearthought.layout.TableLayout;

/**
* <br>
* Created by Novabit Informationssysteme GmbH <br>
* Please visit <a href="http://www.novabit.de">www.novabit.de</a>
*
* @author <a href="mailto:marc.finke@novabit.de">Marc Finke</a>
* @version 01.00.00
*/
public class NuclosEntityOptionStep extends NuclosEntityAbstractStep {

	private static final Logger LOG = Logger.getLogger(NuclosEntityOptionStep.class);

	private JLabel lbName;

	private ButtonGroup bGroup;
	private JRadioButton rbGatherAttributes;
	private JRadioButton rbCopyAttributes;
	private JRadioButton rbAssignAttributes;
	private JRadioButton rbImportTable;

	private JLabel lbEntity;
	private JComboBox cbxEntity;
	private JScrollPane scrollFields;
	private JTable tblFields;

	private JComboBox cbxTable;

	private JPanel pnlImport;

	private JLabel lbImportServer;
	private JTextField tfImportServer;
	private JLabel lbImportPort;
	private JTextField tfImportPort;
	private JLabel lbImportDatabase;
	private JComboBox cbxImportDatabase;
	private JLabel lbImportUser;
	private JTextField tfImportUser;
	private JLabel lbImportPassword;
	private JTextField tfImportPassword;
	private JLabel lbImportSSID;
	private JTextField tfImportSSID;

	private JLabel lbUrl;
	private JTextField tfUrl;

	private JButton btConnect;

	private JLabel lbImportTables;
	private JComboBox cbxImportTables;

	public NuclosEntityOptionStep(String name, String summary) {
		super(name, summary);
		initComponents();
	}

	protected void initPanel() {
		double size [][] = {{TableLayout.PREFERRED, TableLayout.PREFERRED,TableLayout.PREFERRED,TableLayout.FILL}, {20,20,20,20,20,20,20,20,20,200, TableLayout.FILL}};

		TableLayout layout = new TableLayout(size);
		layout.setVGap(3);
		layout.setHGap(5);

		pnlImport = new JPanel();
		pnlImport.setLayout(layout);

		lbImportServer = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entityoption.1", "Server")+":");
		tfImportServer = new JTextField();
		tfImportServer.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());
		lbImportPort = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entityoption.2", "Port")+":");
		tfImportPort = new JTextField();
		tfImportPort.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());
		lbImportDatabase = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entityoption.3", "Datenbanktyp")+":");
		cbxImportDatabase = new JComboBox();
		cbxImportDatabase.addItem("Oracle");
		cbxImportDatabase.addItem("MS SQL");
		cbxImportDatabase.addItem("Postgres");
		cbxImportDatabase.addItem("Sybase");
		lbImportUser = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entityoption.4", "User"));
		tfImportUser = new JTextField();
		tfImportUser.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());
		lbImportPassword = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entityoption.5", "Passwort"));
		tfImportPassword = new JTextField();
		tfImportPassword.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());
		lbImportSSID = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entityoption.6", "Datenbank"));
		tfImportSSID = new JTextField();
		lbUrl = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entityoption.7", "Datenbank URL"));
		tfUrl = new JTextField();
		tfUrl.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());
		btConnect = new JButton(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entityoption.8", "Verbindung \u00f6ffnen"));
		btConnect.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					String jdbcUrl = getJDBCUrl();
					if(tfUrl.getText() != null && tfUrl.getText().length() > 0)
						jdbcUrl = tfUrl.getText().trim();
					List<String> lstTables = MetaDataDelegate.getInstance().getTablesFromSchema(jdbcUrl, tfImportUser.getText().trim(),
						tfImportPassword.getText().trim(), tfImportUser.getText().trim());
					cbxImportTables.removeAllItems();
					for(String strTable : lstTables) {
						cbxImportTables.addItem(strTable);
					}
					tfUrl.setText(jdbcUrl);
				}
				catch(Exception e1) {
					LOG.info("actionPerformed failed: " + e1, e1);
					JOptionPane.showMessageDialog(NuclosEntityOptionStep.this, "Datenbankverbindung konnte nicht erstellt werden!");
				}
			}
		});


		lbImportTables = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entityoption.9", "Tabelle"));
		cbxImportTables = new JComboBox();


		cbxImportTables.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				NuclosEntityOptionStep.this.setComplete(true);
			}
		});

		pnlImport.add(lbImportServer, "0,0");
		pnlImport.add(tfImportServer, "1,0");
		pnlImport.add(lbImportPort, "2,0");
		pnlImport.add(tfImportPort, "3,0");
		pnlImport.add(lbImportUser, "0,1");
		pnlImport.add(tfImportUser, "1,1");
		pnlImport.add(lbImportPassword, "2,1");
		pnlImport.add(tfImportPassword, "3,1");
		pnlImport.add(lbImportDatabase, "0,2");
		pnlImport.add(cbxImportDatabase, "1,2");
		pnlImport.add(lbImportSSID, "2,2");
		pnlImport.add(tfImportSSID, "3,2");
		pnlImport.add(btConnect, "0,3");
		pnlImport.add(lbImportTables, "2,3");
		pnlImport.add(cbxImportTables, "3,3");
		pnlImport.add(lbUrl, "0,4");
		pnlImport.add(tfUrl, "1,4,3,4");


		pnlImport.setVisible(false);

	}

	@Override
	protected void initComponents() {

		double size [][] = {{TableLayout.PREFERRED, TableLayout.FILL}, {20,20,20,20,20,20,20, TableLayout.FILL}};

		TableLayout layout = new TableLayout(size);
		layout.setVGap(3);
		layout.setHGap(5);
		this.setLayout(layout);
		lbName = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entityoption.10", "Wie m\u00f6chten Sie Attribute erfassen")+" ");
		rbGatherAttributes = new JRadioButton(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entityoption.11", "Attribute manuell erfassen"));
		rbGatherAttributes.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entityoption.tooltip.11", "Attribute manuell erfassen"));
		rbCopyAttributes = new JRadioButton(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entityoption.12", "Attribute von anderer Entit\u00e4t selektiv \u00fcbernehmen"));
		rbCopyAttributes.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entityoption.tooltip.12", "Attribute von anderer Entit\u00e4t selektiv \u00fcbernehmen"));
		rbAssignAttributes = new JRadioButton(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entityoption.13", "\u00dcbernahme aus bestehender Datenbanktabelle"));
		rbAssignAttributes.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entityoption.tooltip.13", "\u00dcbernahme aus bestehender Datenbanktabelle"));
		rbImportTable = new JRadioButton(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entityoption.15", "Entit\u00e4t importieren"));
		rbImportTable.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entityoption.tooltip.15", "Entit\u00e4t importieren"));
		rbImportTable.setVisible(false);
		lbEntity = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entityoption.16", "Bitte w\u00e4hlen Sie eine Entit\u00e4t aus")+":");
		lbEntity.setVisible(false);
		cbxEntity = new JComboBox();
		cbxEntity.setVisible(false);
		tblFields = new JTable(new EntityAttributeSelectTableModel());
		tblFields.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tblFields.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblFields.getTableHeader().setReorderingAllowed(false);

		cbxTable = new JComboBox();
		cbxTable.setVisible(false);

		scrollFields = new JScrollPane(tblFields);
		scrollFields.setVisible(false);

		bGroup = new ButtonGroup();
		bGroup.add(rbGatherAttributes);
		bGroup.add(rbCopyAttributes);
		bGroup.add(rbAssignAttributes);
		bGroup.add(rbImportTable);

		rbGatherAttributes.setSelected(true);
		NuclosEntityOptionStep.this.setComplete(true);

		rbGatherAttributes.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(final ItemEvent e) {
				SwingUtilities.invokeLater(new Runnable() {

					@Override
					public void run() {
						int state = e.getStateChange();
						if (state == ItemEvent.SELECTED) {
							NuclosEntityOptionStep.this.setComplete(true);
							pnlImport.setVisible(false);
							cbxTable.setVisible(false);
							cbxEntity.setVisible(false);
							scrollFields.setVisible(false);
						}
					}
				});

			}
		});

		rbImportTable.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(final ItemEvent e) {
				SwingUtilities.invokeLater(new Runnable() {

					@Override
					public void run() {
						int state = e.getStateChange();
						if (state == ItemEvent.SELECTED) {
							pnlImport.setVisible(true);
							NuclosEntityOptionStep.this.setComplete(false);
						}
						else {
							pnlImport.setVisible(false);
							NuclosEntityOptionStep.this.setComplete(true);
						}
					}
				});

			}
		});

		rbCopyAttributes.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(final ItemEvent e) {
				SwingUtilities.invokeLater(new Runnable() {

					@Override
					public void run() {
						int state = e.getStateChange();
						if (state == ItemEvent.SELECTED) {
							NuclosEntityOptionStep.this.setComplete(true);
							tblFields.setModel(new EntityAttributeSelectTableModel());
							lbEntity.setVisible(true);
							cbxEntity.setSelectedIndex(0);
							cbxEntity.setVisible(true);
							cbxTable.setVisible(false);
							scrollFields.setVisible(true);
						}
						else {
							lbEntity.setVisible(false);
							cbxEntity.setVisible(false);
							cbxTable.setVisible(false);
							scrollFields.setVisible(false);
						}

					}
				});

			}
		});

		rbAssignAttributes.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(final ItemEvent e) {
				SwingUtilities.invokeLater(new Runnable() {

					@Override
					public void run() {
						int state = e.getStateChange();
						if (state == ItemEvent.SELECTED) {
							NuclosEntityOptionStep.this.setComplete(true);
							tblFields.setModel(new EntityAttributeSelectTableModel());
							cbxTable.setSelectedIndex(0);
							cbxTable.setVisible(true);
							cbxEntity.setVisible(false);
							scrollFields.setVisible(true);
						}
					}
				});

			}
		});

		cbxEntity.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(final ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					final Object obj = e.getItem();
					try {
						if(obj instanceof EntityMeta) {
							final EntityAttributeSelectTableModel model = new EntityAttributeSelectTableModel();
							
							final EntityMeta<?> vo = (EntityMeta<?>)obj;
							final Collection<FieldMeta<?>> fields = MetaProvider.getInstance().getAllEntityFieldsByEntity(vo.getUID()).values();
							
							final ArrayList<FieldMeta<?>> lstSorted = CollectionUtils.sorted(fields, new Comparator<FieldMeta<?>>() {
								@Override
								public int compare(FieldMeta o1, FieldMeta o2) {
									Integer order1 = (o1.getOrder()==null)?0:o1.getOrder();
									Integer order2 = (o2.getOrder()==null)?0:o2.getOrder();
									return order1.compareTo(order2);
								}
							});
							for(FieldMeta<?> field: lstSorted) {
								if(NuclosWizardUtils.isSystemField(field)) {
									continue;									
								}
								
								Attribute attr = new Attribute(false);
								attr.setUID(field.getUID());
								if(attr.getUID() == null)
									attr.setUID(new UID());
								attr.setResume(true);
								attr.setLabel(SpringLocaleDelegate.getInstance().getResource(
										field.getLocaleResourceIdForLabel(), field.getFallbackLabel()));
								attr.setLabelResource(field.getLocaleResourceIdForLabel());
								attr.setDescription(SpringLocaleDelegate.getInstance().getResource(
										field.getLocaleResourceIdForDescription(), ""));
								attr.setDescriptionResource(field.getLocaleResourceIdForDescription());
								attr.setDistinct(field.isUnique());
								attr.setMandatory(!field.isNullable());
								attr.setLogBook(field.isLogBookTracking());
								attr.setHidden(field.isHidden());
								attr.setInternalName(field.getFieldName());
								attr.setDbName(field.getDbColumn());
								attr.setField(field.getFieldName());
								attr.setOldInternalName(field.getFieldName());
								attr.setSearchField(field.getSearchField());
								attr.setValueListProvider(field.isSearchable());
								attr.setModifiable(field.isModifiable());
								attr.setDefaultValue(field.getDefaultValue());
								attr.setCalcFunction(field.getCalcFunction());
								attr.setCalcAttributeDS(field.getCalcAttributeDS());
								attr.setCalcAttributeParamValues(field.getCalcAttributeParamValues());
								attr.setCalcAttributeAllowCustomization(field.isCalcAttributeAllowCustomization());
								attr.setCalcOndemand(field.isCalcOndemand());
								attr.setIsLocalized(field.isLocalized());
								attr.setOutputFormat(field.getFormatOutput());
								attr.setInputValidation(field.getFormatInput());
								attr.setIndexed(Boolean.TRUE.equals(field.isIndexed()));
								attr.setCalculationScript(field.getCalculationScript());
								attr.setBackgroundColorScript(field.getBackgroundColorScript());
								UID foreignEntity = field.getForeignEntity();
								UID lookupEntity = field.getLookupEntity();
								if (field.isFileDataType()) {
									attr.setDatatyp(NuclosWizardUtils.getDataTyp(field.getDataType(), field.getDefaultComponentType(), 
											field.getScale(), field.getPrecision(), field.getFormatInput(),
											field.getFormatOutput()));
									attr.setField(field.getForeignEntityField());
									attr.setForeignIntegrationPoint(field.getForeignIntegrationPoint());
									EntityMeta<?> voForeignEntity = MetaProvider.getInstance().getEntity(foreignEntity);
									attr.setMetaVO(voForeignEntity);
								}
								else if(foreignEntity != null) {
									attr.setForeignIntegrationPoint(field.getForeignIntegrationPoint());
									EntityMeta<?> voForeignEntity = MetaProvider.getInstance().getEntity(foreignEntity);
									attr.setMetaVO(voForeignEntity);
									attr.setOnDeleteCascade(field.isOnDeleteCascade());
									attr.setField(field.getForeignEntityField());
					        		attr.setDatatyp(DataTyp.getReferenzTyp());
									if(!Modules.getInstance().isModule(foreignEntity) && field.getForeignEntityField() != null) {
										String sForeignField = field.getForeignEntityField();
										if(sForeignField.indexOf("uid{") >= 0) {
											attr.setDatatyp(DataTyp.getReferenzTyp());
										}
										else {
											FieldMeta<?> voField = MetaProvider.getInstance().getEntityField(UID.parseUID(field.getForeignEntityField()));
											attr.getDatatyp().setJavaType(voField.getDataType());
											if(voField.getPrecision() != null)
												attr.getDatatyp().setPrecision(voField.getPrecision());
											if(voField.getScale() != null)
												attr.getDatatyp().setScale(voField.getScale());
											if(voField.getFormatOutput() != null)
												attr.getDatatyp().setOutputFormat(voField.getFormatOutput());
										}
									}
									if(voForeignEntity.isFieldValueEntity()) {
						        		attr.setDatatyp(DataTyp.getDefaultStringTyp());
						        		attr.setValueListNew(false);
						        		NuclosEntityNameStep.loadValueList(attr);
									}
								} else if(lookupEntity != null) {
									attr.setLookupMetaVO(MetaProvider.getInstance().getEntity(lookupEntity));
									attr.setOnDeleteCascade(field.isOnDeleteCascade());
									attr.setField(field.getLookupEntityField());
									attr.setDatatyp(DataTyp.getLookupTyp());
									if(!Modules.getInstance().isModule(lookupEntity) && field.getLookupEntityField() != null) {
										String sLookupField = field.getLookupEntityField();
										if(sLookupField.indexOf("uid{") >= 0) {
											attr.setDatatyp(DataTyp.getLookupTyp());
										}
										else {
											FieldMeta voField = MetaProvider.getInstance().getEntityField(UID.parseUID(field.getLookupEntityField()));

											attr.getDatatyp().setJavaType(voField.getDataType());
											if(voField.getPrecision() != null)
												attr.getDatatyp().setPrecision(voField.getPrecision());
											if(voField.getScale() != null)
												attr.getDatatyp().setScale(voField.getScale());
											if(voField.getFormatOutput() != null)
												attr.getDatatyp().setOutputFormat(voField.getFormatOutput());
										}
									}
								} else {
									attr.setDatatyp(NuclosWizardUtils.getDataTyp(field.getDataType(), field.getDefaultComponentType(), 
										field.getScale(), field.getPrecision(), field.getFormatInput(),
										field.getFormatOutput()));
								}
								
								attr.setAttributeGroup(field.getFieldGroup());
								
								model.addAttribute(attr);
							}
							SwingUtilities.invokeLater(new Runnable() {

								@Override
								public void run() {
									tblFields.setModel(model);
									TableUtils.setOptimalColumnWidths(tblFields);
								}
							});
						}
						else if(obj instanceof String) {
							EntityAttributeSelectTableModel model = new EntityAttributeSelectTableModel();
							tblFields.setModel(model);
						}
					}
					catch(Exception e1) {
						LOG.info("itemStateChanged failed: " + e1, e1);
					}
				}
			}
		});

		cbxTable.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					final Object obj = e.getItem();
					if(obj instanceof String) {
						String strTable = (String)obj;
						final EntityAttributeSelectTableModel model = new EntityAttributeSelectTableModel();
						if(strTable.length() == 0) {
							tblFields.setModel(model);
							return;
						}
						
						Map<String, MasterDataVO<?>> mp = MetaDataDelegate.getInstance().getColumnsFromTable(strTable);
						for(String sCol : mp.keySet()) {
							if(NuclosWizardUtils.isSystemField(sCol)) {
								continue;								
							}
							
							Attribute attr = new Attribute(false);
							attr.setUID(new UID());
							attr.setResume(true);
							attr.setLabel(sCol);
							attr.setDbName(sCol);
							attr.setDescription(sCol);
							attr.setInternalName(sCol);
							MasterDataVO<?> vo = mp.get(sCol);

							DataTyp typ = new DataTyp();
							typ.setPrecision(vo.getFieldValue(E.DATATYPE.precision));
							typ.setScale(vo.getFieldValue(E.DATATYPE.scale));
							typ.setName(vo.getFieldValue(E.DATATYPE.name));
							typ.setJavaType(vo.getFieldValue(E.DATATYPE.javatyp));

							attr.setDatatyp(typ);
							setTranslationForAttribute(attr, NuclosEntityOptionStep.this.model.getAttributeModel());

							model.addAttribute(attr);
						}
						SwingUtilities.invokeLater(new Runnable() {

							@Override
							public void run() {
								tblFields.setModel(model);
							}
						});


					}
				}
			}
		});

		initPanel();

		this.add(lbName, "0,0");
		this.add(rbGatherAttributes, "1,0");
		this.add(rbCopyAttributes, "1,1");
		this.add(rbAssignAttributes, "1,2");
		this.add(rbImportTable, "1,3");
		this.add(lbEntity, "0,4");
		this.add(cbxEntity, "0,5");
		this.add(cbxTable, "0,5");
		this.add(pnlImport, "0,6, 1,6");
		this.add(scrollFields, "0,7 ,1,7");

		fillEntityCombobox();
		fillTableCombobox();

	}

	@Override
	public void prepare() {
		super.prepare();
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				rbGatherAttributes.requestFocusInWindow();
			}
		});
	}

	@Override
	public void close() {
		lbName = null;

		bGroup = null;
		rbGatherAttributes = null;
		rbCopyAttributes = null;
		rbAssignAttributes = null;
		rbImportTable = null;

		lbEntity = null;
		cbxEntity = null;
		scrollFields = null;
		tblFields = null;

		cbxTable = null;

		pnlImport = null;

		lbImportServer = null;
		tfImportServer = null;
		lbImportPort = null;
		tfImportPort = null;
		lbImportDatabase = null;
		cbxImportDatabase = null;
		lbImportUser = null;
		tfImportUser = null;
		lbImportPassword = null;
		tfImportPassword = null;
		lbImportSSID = null;
		tfImportSSID = null;

		lbUrl = null;
		tfUrl = null;

		btConnect = null;

		lbImportTables = null;
		cbxImportTables = null;

		super.close();
	}

	@Override
	public void applyState() throws InvalidStateException {
		if(rbCopyAttributes.isSelected() || rbAssignAttributes.isSelected()) {
			EntityAttributeSelectTableModel model =  (EntityAttributeSelectTableModel)tblFields.getModel();
			List<Attribute> lstAttribute = model.getAttributes();
			EntityAttributeTableModel modelAttribute = new EntityAttributeTableModel(null);
			for(Attribute attr : lstAttribute) {
				if(attr.isForResume())
					modelAttribute.addAttribute(attr);
			}
			for(Attribute attr : modelAttribute.getNucletAttributes()) {
				if(!existAttribute(attr)) {
					attr.setUID(new UID());
					if (this.model.isEditMode()) {
						//attr.setDistinct(false); // @todo uncomment this. some databases can not handle unique and nullable fields
						attr.setMandatory(false);
					}
					this.model.getAttributeModel().addAttribute(attr);
					setTranslationForAttribute(attr, this.model.getAttributeModel());
					attr.setLabelResource(null);
				    attr.setDescriptionResource(null);
				}
			}
		}
		else if(rbImportTable.isSelected()) {
			String jdbc = getJDBCUrl();

			this.model.setJdbcUrl(jdbc);
			this.model.setExternalUser(tfImportUser.getText().trim());
			this.model.setExternalPassword(tfImportPassword.getText().trim());
			this.model.setExternalTable((String)cbxImportTables.getSelectedItem());
			this.model.setImportTable(true);
			List<MasterDataVO<UID>> lstVO = MetaDataDelegate.getInstance().transformTable(jdbc, model.getExternalUser(), model.getExternalPassword(),
				model.getExternalUser().trim().toUpperCase(), model.getExternalTable());
			EntityAttributeTableModel modelAttribute = new EntityAttributeTableModel(null);
			for(MasterDataVO<UID> vo: lstVO) {
				Attribute attr = new Attribute(false);
				attr.setUID(new UID());
				attr.setResume(true);
				attr.setField(vo.getFieldValue(E.ENTITYFIELD.field));
				attr.setInternalName(vo.getFieldValue(E.ENTITYFIELD.field));
				attr.setDbName(vo.getFieldValue(E.ENTITYFIELD.dbfield));
				attr.setDescription(vo.getFieldValue(E.ENTITYFIELD.field));
				DataTyp datatyp = new DataTyp();
				datatyp.setJavaType(vo.getFieldValue(E.ENTITYFIELD.datatype));
				datatyp.setPrecision(vo.getFieldValue(E.ENTITYFIELD.dataprecision));
				datatyp.setScale(vo.getFieldValue(E.ENTITYFIELD.datascale));
				attr.setDatatyp(datatyp);
				attr.setLogBook(vo.getFieldValue(E.ENTITYFIELD.logbooktracking));
				attr.setDistinct(vo.getFieldValue(E.ENTITYFIELD.unique));
				attr.setMandatory(vo.getFieldValue(E.ENTITYFIELD.nullable));
				attr.setHidden(vo.getFieldValue(E.ENTITYFIELD.hidden));
				attr.setLabel(vo.getFieldValue(E.ENTITYFIELD.field));
				modelAttribute.addAttribute(attr);
			}
			this.model.setAttributeModel(modelAttribute);
		}
		
		super.applyState();
	}

	private boolean existAttribute(Attribute attr) {
	    boolean exist = false;
	    if(this.model.getAttributeModel() != null) {
	    	for(Attribute attrexist : this.model.getAttributeModel().getNucletAttributes()) {
	    		if(attrexist.getInternalName().equals(attr.getInternalName())) {
	    			exist = true;
	    		}
	    	}
	    }
	    return exist;
    }

	private void setTranslationForAttribute(Attribute attr, EntityAttributeTableModel model) {
	    List<TranslationVO> lstTranslation = new ArrayList<TranslationVO>();
	    
	    for(LocaleInfo info : LocaleDelegate.getInstance().getAllLocales(false)){
	    	Map<String, String> mpValues = new HashMap<String, String>();
	    	mpValues.put(TranslationVO.LABELS_FIELD[0], LocaleDelegate.getInstance().getResourceByStringId(info, attr.getLabelResource()));
	    	mpValues.put(TranslationVO.LABELS_FIELD[1], LocaleDelegate.getInstance().getResourceByStringId(info, attr.getDescriptionResource()));
	    	TranslationVO voTranslation = new TranslationVO(info, mpValues);
	    	lstTranslation.add(voTranslation);
	    }
	    model.addTranslation(attr, lstTranslation);
    }

	private void fillTableCombobox() {
		List<String> lstTables = MetaDataDelegate.getInstance().getDBTables();
		cbxTable.addItem("");
		for(String str : lstTables) {
			cbxTable.addItem(str);
		}
	}

	private void fillEntityCombobox() {
		final List<EntityMeta<?>> lstMasterdata = new ArrayList<EntityMeta<?>>(MetaProvider.getInstance().getAllEntities());
		
		Collections.sort(lstMasterdata, EntityUtils.getMetaComparator(EntityMeta.class));

		cbxEntity.addItem(EntityUtils.wrapMetaData(EntityMeta.NULL));

		for(EntityMeta<?> vo : lstMasterdata) {
			if(!E.isNuclosEntity(vo.getUID()))
				cbxEntity.addItem(EntityUtils.wrapMetaData(vo));
		}
	}

	protected String getJDBCUrl() {
		String sDB = (String)cbxImportDatabase.getSelectedItem();
		StringBuffer jdbcUrl = new StringBuffer();
		if(sDB.equals("Oracle")) {
			jdbcUrl.append("jdbc:oracle:thin:@");
			jdbcUrl.append(tfImportServer.getText().trim());
			jdbcUrl.append(":");
			jdbcUrl.append(tfImportPort.getText().trim());
			jdbcUrl.append(":");
			jdbcUrl.append(tfImportSSID.getText().trim());
		}
		else if(sDB.equals("MS SQL")) {
			jdbcUrl.append("jdbc:sqlserver://");
			jdbcUrl.append(tfImportServer.getText().trim());
			jdbcUrl.append(":");
			jdbcUrl.append(tfImportPort.getText().trim());
			jdbcUrl.append(";DatabaseName=");
			jdbcUrl.append(tfImportSSID.getText().trim());
		}

		return jdbcUrl.toString();
	}

}
