//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.wizard.steps;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.AbstractAction;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DropMode;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.text.BadLocationException;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.explorer.configuration.ConfigurationEntityTreeNode;
import org.nuclos.client.explorer.configuration.ConfigurationEntityTreeSubNode;
import org.nuclos.client.explorer.configuration.ConfigurationExplorerDelegate;
import org.nuclos.client.explorer.configuration.ConfigurationTreeBuilder;
import org.nuclos.client.explorer.configuration.ConfigurationTreeModel;
import org.nuclos.client.explorer.configuration.ConfigurationTreeNode;
import org.nuclos.client.explorer.configuration.DefaultConfigurationExplorerView;
import org.nuclos.client.explorer.configuration.TreeTransferHandler;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.MetaDataDelegate;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.dnd.IReorderable;
import org.nuclos.client.ui.gc.ListenerUtil;
import org.nuclos.client.wizard.model.Attribute;
import org.nuclos.client.wizard.util.MoreOptionPanel;
import org.nuclos.client.wizard.util.NuclosWizardUtils;
import org.nuclos.common.E;
import org.nuclos.common.EntityTreeViewVO;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.TranslationVO;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.pietschy.wizard.InvalidStateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import info.clearthought.layout.TableLayout;

/**
 * Entity wizard, entity tree representation, step 5.
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * <br>
 * @author <a href="mailto:marc.finke@novabit.de">Marc Finke</a>
 * @version 01.00.00
 */
public class NuclosEntityTreeValueStep extends NuclosEntityAbstractStep {

	private static final Logger LOG = LoggerFactory.getLogger(NuclosEntityTreeValueStep.class);

	private JScrollPane scrollAttribute;
	private JList lAttribute;
	private List<Attribute> lstAttribute;

	private JLabel lbValue;
	private JButton btnAddToValueField;
	private JTextField tfValue;

	private JLabel lbTooltip;
	private JTextField tfTooltip;
	private JButton btnAddToTooltipField;

	private JLabel lbMultiEditEquation;
	private JTextField tfMultiEditEquation;
	private JButton btnMultiEditEquation;

	private JScrollPane paneTreeView;
	private JTable tblTreeView;

	private JLabel lbTreeViewSubform;
	private List<UID> subForms;

	private JComboBox cbxSubformRefField;

	private JPanel pnlMoreOptions;


	private DefaultConfigurationExplorerView treeConfigurationTreeView;

	private JPanel pnlTreeView;

	public NuclosEntityTreeValueStep(MainFrameTab parent, String name, String summary) {
		this(parent, name, summary, null);
	}

	public NuclosEntityTreeValueStep(MainFrameTab parent, String name, String summary, Icon icon) {
		super(name, summary, icon);
		setParentComponent(parent);
		initComponents();
	}

	@Override
	protected void initComponents() {

		subForms = new ArrayList<UID>();

		double size [][] = {{TableLayout.PREFERRED, 50, TableLayout.FILL}, {20,20,5,20,20,5,20,20,5,20,20,5,20,20,7,150, TableLayout.FILL}};

		TableLayout layout = new TableLayout(size);
		layout.setVGap(3);
		layout.setHGap(5);
		this.setLayout(layout);

		lAttribute = new JList();
		lAttribute.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lAttribute.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "addToValueField");
		lAttribute.getActionMap().put("addToValueField", new AbstractAction() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				addToValueField();
			}
		});
		scrollAttribute = new JScrollPane();
		scrollAttribute.setPreferredSize(new Dimension(150, 100));
		scrollAttribute.getViewport().add(lAttribute);
		btnAddToValueField = new JButton(">>");
		btnAddToTooltipField = new JButton(">>");
		btnMultiEditEquation = new JButton(">>");
		lbValue = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitytreevalue.1", "Anzeige Knotendarstellung & dynamischer Fenster-Titel *"));
		tfValue = new JTextField();
		tfValue.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());
		tfValue.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitytreevalue.tooltip.1", "Anzeige Knotendarstellung & dynamischer Fenster-Titel *"));

		lbTooltip = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitytreevalue.2", "Bezeichnung des Knoten Tooltips"));
		tfTooltip = new JTextField();
		tfTooltip.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitytreevalue.tooltip.2", "Bezeichnung des Knoten Tooltips"));
		tfTooltip.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());

		lbMultiEditEquation = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitytreevalue.3", "Felder f\u00fcr Vergleich in der Sammelbearbeitung"));
		tfMultiEditEquation = new JTextField();
		tfMultiEditEquation.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitytreevalue.tooltip.3", "Felder f\u00fcr Vergleich in der Sammelbearbeitung"));
		tfMultiEditEquation.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());

		lbTreeViewSubform = new JLabel("Baumdarstellung f\u00fcr Unterformulare");
		cbxSubformRefField = new JComboBox();


		btnAddToValueField.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addToValueField();
			}
		});

		tfValue.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
			protected void doSomeWork(DocumentEvent e) {
				try {
					final String label = e.getDocument().getText(0, e.getDocument().getLength());
					if (!LangUtils.equal(NuclosEntityTreeValueStep.this.model.getNodeLabel(), label)) {
						for (TranslationVO translationVO : NuclosEntityTreeValueStep.this.model.getTranslation()) {
							translationVO.getLabels().put("treeview", NuclosEntityTreeValueStep.transformStringToUid(lstAttribute, label));
						}
					}
					NuclosEntityTreeValueStep.this.model.setNodeLabel(label);
					// see isComplete()
					//NuclosEntityTreeValueStep.this.setComplete(e.getDocument().getLength() > 0);
					setComplete(isComplete());
				} catch (BadLocationException ex) {
					Errors.getInstance().showExceptionDialog(NuclosEntityTreeValueStep.this, ex);
				}
			}
		});

		btnAddToTooltipField.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Attribute attr = (Attribute)lAttribute.getSelectedValue();
				if(attr == null) {
					return;
				}
				String strText = tfTooltip.getText();
				strText += NuclosWizardUtils.getStapledString(attr.getInternalName());
				tfTooltip.setText(strText);
				tfTooltip.setCaretPosition(strText.length());
				tfTooltip.requestFocusInWindow();
			}
		});

		ListenerUtil.registerDocumentListener(tfTooltip.getDocument(), this, new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
			protected void doSomeWork(DocumentEvent e) {
				try {
					final String label = e.getDocument().getText(0, e.getDocument().getLength());
					if (!LangUtils.equal(NuclosEntityTreeValueStep.this.model.getNodeTooltip(), label)) {
						for (TranslationVO translationVO : NuclosEntityTreeValueStep.this.model.getTranslation()) {
							translationVO.getLabels().put("treeviewdescription", NuclosEntityTreeValueStep.transformStringToUid(lstAttribute, label));
						}
					}
					NuclosEntityTreeValueStep.this.model.setNodeTooltip(label);
				} catch (BadLocationException ex) {
					Errors.getInstance().showExceptionDialog(NuclosEntityTreeValueStep.this, ex);
				}
			}
		});

		btnMultiEditEquation.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Attribute attr = (Attribute)lAttribute.getSelectedValue();
				if(attr == null) {
					return;
				}
				String strText = tfMultiEditEquation.getText();
				if(strText.length() > 0) {
					strText += "," + NuclosWizardUtils.getStapledString(attr.getInternalName());
				}
				else {
					strText += NuclosWizardUtils.getStapledString(attr.getInternalName());
				}
				tfMultiEditEquation.setText(strText);
				tfMultiEditEquation.setCaretPosition(strText.length());
				tfMultiEditEquation.requestFocusInWindow();
			}
		});

		tfMultiEditEquation.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
			protected void doSomeWork(DocumentEvent e) {
				try {
					String inputNames = e.getDocument().getText(0, e.getDocument().getLength()).replaceAll("\\$\\{", "").replaceAll("}", "");
					String[] names = inputNames.split(",");
					List<UID> uids = new ArrayList<UID>();
					for (int j = 0; j < lAttribute.getModel().getSize(); j++) {
						final Attribute attr = (Attribute) lAttribute.getModel().getElementAt(j);
						for (String name : names) {
							name = name.trim();
							if (attr.getInternalName().equals(name)) {
								uids.add(attr.getUID());
							}
						}
					}
					NuclosEntityTreeValueStep.this.model.setMultiEditEquation(uids.toArray(new UID[] {}));
				} catch (BadLocationException ex) {
					Errors.getInstance().showExceptionDialog(NuclosEntityTreeValueStep.this, ex);
				}
			}
		});

		double sizeMoreOptions [][] = {{50,5, TableLayout.FILL,5}, {20,20,5,20,20,5,20,20, TableLayout.FILL}};

		pnlMoreOptions = new JPanel();

		TableLayout tblLayout = new TableLayout(sizeMoreOptions);

		layout.setVGap(3);
		layout.setHGap(5);

		pnlMoreOptions.setLayout(tblLayout);
		//pnlMoreOptions.add(lbReportName, "0,3,2,3");
		//pnlMoreOptions.add(btnReportName, "0,4");
		//pnlMoreOptions.add(tfReportName, "2,4");
		pnlMoreOptions.add(lbMultiEditEquation, "0,6,2,6");
		pnlMoreOptions.add(btnMultiEditEquation, "0,7");
		pnlMoreOptions.add(tfMultiEditEquation, "2,7");

		MoreOptionPanel optionPanel = new MoreOptionPanel(pnlMoreOptions);

		this.add(scrollAttribute, "0,0,0,14");
		this.add(lbValue, "1,0,2,0");
		this.add(btnAddToValueField, "1,1");
		this.add(tfValue, "2,1");

		this.add(lbTooltip, "1,3,2,3");
		this.add(btnAddToTooltipField, "1,4");
		this.add(tfTooltip, "2,4");

		this.add(optionPanel, "1,6, 2,14");

		this.add(lbTreeViewSubform, "0,14");
		//this.add(paneTreeView, "0,15,2,15");
		
		pnlTreeView = new JPanel(new BorderLayout());
		this.add(pnlTreeView, "0,15,2,16");
		
		this.treeConfigurationTreeView = new DefaultConfigurationExplorerView(parent, null);
		this.treeConfigurationTreeView.getJTree().setModel(null);
		
		// Drag & Drop
		this.treeConfigurationTreeView.getJTree().setDragEnabled(true);
		this.treeConfigurationTreeView.getJTree().setDropMode(DropMode.INSERT);
		this.treeConfigurationTreeView.getJTree().setTransferHandler(new TreeTransferHandler());

	}

	private void addToValueField() {
		Attribute attr = (Attribute)lAttribute.getSelectedValue();
		if(attr == null) {
			return;
		}
		String strText = tfValue.getText();
		strText += NuclosWizardUtils.getStapledString(attr.getInternalName());
		tfValue.setText(strText);
		tfValue.setCaretPosition(strText.length());
		tfValue.requestFocusInWindow();
	}

	private DefaultConfigurationExplorerView getExplorerView() {
		return treeConfigurationTreeView;
	}

	@Override
	public void prepare() {
		super.prepare();

		lstAttribute = new ArrayList<Attribute>(model.getAttributeModel().getNucletAttributes());

		try {
	        lstAttribute.add(NuclosEntityNameStep.wrapEntityMetaFieldVO(SF.CHANGEDAT.getMetaData(model.getUID()), true));
	        lstAttribute.add(NuclosEntityNameStep.wrapEntityMetaFieldVO(SF.CHANGEDBY.getMetaData(model.getUID()), true));
	        lstAttribute.add(NuclosEntityNameStep.wrapEntityMetaFieldVO(SF.CREATEDAT.getMetaData(model.getUID()), true));
	        lstAttribute.add(NuclosEntityNameStep.wrapEntityMetaFieldVO(SF.CREATEDBY.getMetaData(model.getUID()), true));
	        if(this.model.isStateModel()) {
	        	lstAttribute.add(NuclosEntityNameStep.wrapEntityMetaFieldVO(SF.STATENUMBER.getMetaData(model.getUID()), true));
	        	lstAttribute.add(NuclosEntityNameStep.wrapEntityMetaFieldVO(SF.STATEICON.getMetaData(model.getUID()), true));
	        	lstAttribute.add(NuclosEntityNameStep.wrapEntityMetaFieldVO(SF.STATE.getMetaData(model.getUID()), true));
	        	lstAttribute.add(NuclosEntityNameStep.wrapEntityMetaFieldVO(SF.PROCESS.getMetaData(model.getUID()), true));
	        	lstAttribute.add(NuclosEntityNameStep.wrapEntityMetaFieldVO(SF.SYSTEMIDENTIFIER.getMetaData(model.getUID()), true));
	        }
        }
        catch(CommonFinderException e) {
	        throw new CommonFatalException(e);
        }
        catch(CommonPermissionException e) {
        	throw new CommonFatalException(e);
        }

        Collections.sort(lstAttribute, new Comparator<Attribute>() {
			@Override
            public int compare(Attribute o1,Attribute o2) {
	            return o1.getInternalName().compareToIgnoreCase(o2.getInternalName());
            }
        });
		lAttribute.setListData(lstAttribute.toArray());

		if(this.model.isEditMode()) {
			String sLabel = this.model.getNodeLabel();
			sLabel = transformStringFromUid(lstAttribute, sLabel);
			sLabel = removeAttributesFromString(sLabel);
			sLabel = changeAttributesInString(sLabel);
			tfValue.setText(sLabel);

			sLabel = this.model.getNodeTooltip();
			sLabel = transformStringFromUid(lstAttribute, sLabel);
			sLabel = removeAttributesFromString(sLabel);
			sLabel = changeAttributesInString(sLabel);
			tfTooltip.setText(sLabel);
			
			sLabel = "";
			if (this.model.getMultiEditEquation() != null) {
				for (UID uid : this.model.getMultiEditEquation()) {
					if (uid != null) {
						for (int j = 0; j < lstAttribute.size(); j++) {
							final Attribute attr = (Attribute) lstAttribute.get(j);
							if (UID.parseUID(uid.getString()).equals(attr.getUID())) {
								if (!sLabel.isEmpty()) {
									sLabel = sLabel + ", ";
								}
								sLabel = sLabel + NuclosWizardUtils.getStapledString(attr.getInternalName());
							}
						}						
					}
				}
			}
			tfMultiEditEquation.setText(sLabel);

			// loadSubforms();
			initConfigurationTree();
		}
		else {
			if (null != paneTreeView) {
				paneTreeView.setVisible(false);
				lbTreeViewSubform.setVisible(false);
			}
		}

		//@see NUCLOS-1037
		Attribute attrName = null;
		for (Attribute attribute : lstAttribute) {
			if (!attribute.isRemove() && StringUtils.equalsIgnoreCase(attribute.getInternalName(), "name")) {
				attrName = attribute;
				break;
			}	
		}
		String sLabel = this.model.getNodeLabel();
		if (attrName != null && StringUtils.isNullOrEmpty(sLabel)) {
			tfValue.setText(NuclosWizardUtils.getStapledString(attrName.getInternalName()));
		}
		sLabel = this.model.getNodeTooltip();
		if (attrName != null && StringUtils.isNullOrEmpty(sLabel)) {
			tfTooltip.setText(NuclosWizardUtils.getStapledString(attrName.getInternalName()));
		}

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				lAttribute.requestFocusInWindow();
			}
		});
	}
	
	public static String transformStringToUid(final List lAttribute, String sText) {
		if(sText == null)
			return null;
		final String sTransformed = RigidUtils.replaceParameters(RigidUtils.PARAM_PATTERN_$, sText, new Transformer<String, String>() {
			@Override
			public String transform(String i) {
				for (int j = 0; j < lAttribute.size(); j++) {
					final Attribute attr = (Attribute) lAttribute.get(j);
					if (attr.getInternalName().equals(i))
						return attr.getUID().getStringifiedDefinition();
				}
				return "";
			}					
		});
		return sTransformed;
	}
	
	public static String transformStringFromUid(final List lAttribute, String sText) {
		if(sText == null)
			return null;
		sText = transformStringToUid(lAttribute, sText); // for backwards compatibility.
		final String sTransformed = RigidUtils.replaceParameters(RigidUtils.PARAM_PATTERN_UID, sText, new Transformer<String, String>() {
			@Override
			public String transform(String i) {
				for (int j = 0; j < lAttribute.size(); j++) {
					final Attribute attr = (Attribute) lAttribute.get(j);
					if (attr.getUID().getStringifiedDefinition().equals(i))
						return "${" + attr.getInternalName() + "}";
				}
				return "";
			}					
		});
      	return sTransformed;
	}

	private String removeAttributesFromString(String sNodeLabel) {
		if(sNodeLabel == null)
			return null;
		String sField = sNodeLabel;
		Pattern referencedEntityPattern = Pattern.compile ("[$][{][\\w\\[\\]]+[}]");
	    Matcher referencedEntityMatcher = referencedEntityPattern.matcher (sField);
	    StringBuffer sb = new StringBuffer();

		while (referencedEntityMatcher.find()) {
		  Object value = referencedEntityMatcher.group().substring(2,referencedEntityMatcher.group().length()-1);

		  String sName = value.toString();
		  for(Attribute attr : this.model.getAttributeModel().getRemoveAttributes()) {
			  if(attr.getInternalName().equals(sName)){
				  referencedEntityMatcher.appendReplacement (sb, "");
				  break;
			  }
		  }
		}

		// complete the transfer to the StringBuffer
		referencedEntityMatcher.appendTail (sb);
		sField = sb.toString();
		return sField;
	}
	
	private String changeAttributesInString(String sNodeLabel) {
		if(sNodeLabel == null)
			return null;
		String sField = sNodeLabel;
		Pattern referencedEntityPattern = Pattern.compile ("[$][{][\\w\\[\\]]+[}]");
	    Matcher referencedEntityMatcher = referencedEntityPattern.matcher (sField);
	    StringBuffer sb = new StringBuffer();

		while (referencedEntityMatcher.find()) {
		  Object value = referencedEntityMatcher.group().substring(2,referencedEntityMatcher.group().length()-1);

		  String sName = value.toString();
		  for(Attribute attr : this.lstAttribute) {
			  if(!attr.hasInternalNameChanged())
				  continue;
			  if(attr.getOldInternalName().equals(sName)){
				  // ${*} has to be escaped
				  referencedEntityMatcher.appendReplacement (sb, "\\" +NuclosWizardUtils.getStapledString(attr.getInternalName()));
				  break;
			  }
		  }
		}

		// complete the transfer to the StringBuffer
		referencedEntityMatcher.appendTail (sb);
		sField = sb.toString();
		return sField;
	}

	private void initConfigurationTree() {
		final ConfigurationEntityTreeNode rootNode = ConfigurationExplorerDelegate.getInstance().getEntityTreeNode(model.getUID());
		getExplorerView().getJTree().setModel(ConfigurationTreeBuilder.build(rootNode, model.getTreeView()));
		
		pnlTreeView.add(getExplorerView().getViewComponent(), BorderLayout.CENTER);
		// tree model listener to keep model in sync
		final ConfigurationTreeModel treeModel = (ConfigurationTreeModel)getExplorerView().getJTree().getModel();
		treeModel.addTreeModelListener(new TreeModelListener() {

			private void removeWithChildNodes(final EntityTreeViewVO etvVO) {
				LOG.info("remove configuration tree {}", etvVO);
				etvVO.flagRemove();
				/*
				for (final EntityTreeViewVO childVO : etvVO.getChildNodes()) {
					childVO.flagRemove();
					if (!childVO.getChildNodes().isEmpty()) {
						removeWithChildNodes(childVO);
					}
				}
				*/
			}
			
			private void fixSortOrder(final TreePath treePath) {
				final ConfigurationTreeNode<?> node = (ConfigurationTreeNode<?>) treePath.getLastPathComponent();
				LOG.info("fix configuration tree sort order {}", treePath);
				int countChild = node.getChildCount();
				for (int i = 0; i<countChild; ++i) {
					final ConfigurationEntityTreeSubNode childVO = (ConfigurationEntityTreeSubNode)node.getChildAt(i);
					childVO.getContent().setSortOrder(i);
					if (childVO.getContent().isFlagUnchanged()) {
						childVO.getContent().flagUpdate();
					}
				}
			}
			
			@Override
			public void treeStructureChanged(TreeModelEvent e) {}

			@Override
			public void treeNodesRemoved(TreeModelEvent e) {
				// FIXME getChildren
				if (treeModel.ignoreListeners()) {
					return;
				}
				
				final ConfigurationEntityTreeSubNode node = ((ConfigurationEntityTreeSubNode)e.getChildren()[0]);
				removeWithChildNodes(node.getContent());
				fixSortOrder(e.getTreePath());
				
				
			}

			@Override
			public void treeNodesInserted(TreeModelEvent e) {
				// FIXME getChildren
				if (treeModel.ignoreListeners()) {
					return;
				}
				
				final ConfigurationEntityTreeSubNode node = ((ConfigurationEntityTreeSubNode)e.getChildren()[0]);
				final EntityTreeViewVO etvVO = node.getContent();
				LOG.info("insert configuration tree {}", etvVO);
				
				final TreeNode parentNode = node.getParent();
				if (parentNode instanceof ConfigurationEntityTreeSubNode) {
					final EntityTreeViewVO topLevelNode = ((ConfigurationEntityTreeSubNode)parentNode).getContent();
					if (topLevelNode.getChildNodes().contains(etvVO)) {
						LOG.info("child nodes already contain node {} ", etvVO);
					} else {
						topLevelNode.addChildNode(etvVO);
						
					}
				} else if (parentNode instanceof ConfigurationEntityTreeNode) {
					if (model.getTreeView().contains(etvVO)) {
						LOG.info("model already contains node {} ", etvVO);
					} else {
						model.getTreeView().add(etvVO);
					}
				}
				
			}

			@Override
			public void treeNodesChanged(TreeModelEvent e) {}
		});
	}
	
	@Override
	public void close() {
		scrollAttribute = null;
		lAttribute = null;

		lbValue = null;
		btnAddToValueField = null;
		tfValue = null;

		lbTooltip = null;
		tfTooltip = null;
		btnAddToTooltipField = null;

		lbMultiEditEquation = null;
		tfMultiEditEquation = null;
		btnMultiEditEquation = null;

		paneTreeView = null;
		tblTreeView = null;

		lbTreeViewSubform = null;
		
		if (subForms != null) {
			subForms.clear();
		}
		subForms = null;
		

		cbxSubformRefField = null;

		pnlMoreOptions = null;


		super.close();
	}

	@Override
	public void applyState() throws InvalidStateException {
		super.applyState();
	}

	private List<UID> getRefFieldTo(UID subformEntity) {
		final List<UID> result = new ArrayList<UID>();
		final Set<FieldMeta<?>> strange = new HashSet<FieldMeta<?>>();
		for(FieldMeta<?> voField : MetaDataDelegate.getInstance().getAllEntityFieldsByEntity(subformEntity).values()) {
			final UID fEntity = voField.getForeignEntity();
			if(model.getEntityName().equals(fEntity) || E.GENERICOBJECT.getUID().equals(fEntity)) {
				result.add(voField.getUID());
			}
			// TODO: sometimes we don't find the 'right' base entity reference with the if clause above,
			// thus the result set is empty. See http://support.novabit.de/browse/NUCLOSINT-1192 for details.
			// We log this case now, to get an (better) idea what is happening here.
			else if(fEntity != null) {
				strange.add(voField);
			}
		}
		if (result.isEmpty()) {
			LOG.warn("NuclosEntityTreeValueStep.getRefFieldTo: unable to find a ref to " + model.getEntityName() + ", using fallback...");
			for (FieldMeta<?> md: strange) {
				result.add(md.getUID());
			}
		}
		if (!strange.isEmpty()) {
			final StringBuilder msg = new StringBuilder();
			msg.append("NuclosEntityTreeValueStep.getRefFieldTo: fallback ref to " + model.getEntityName() + " contains: [\n");
			for (FieldMeta<?> md: strange) {
				msg.append("\t(field: ").append(md.getUID());
				msg.append(", entity: ").append(md.getEntity());
				msg.append(", foreign entity:").append(md.getForeignEntity());
				msg.append(", foreign field:").append(md.getForeignEntityField());
				msg.append(")\n");
			}
			msg.append("]\n");
			LOG.warn(msg.toString());
		}
		return result;
	}

	private class TreeValueTableCellEditor extends DefaultCellEditor implements IReorderable {

		private List<DefaultCellEditor> refToBaseEntityEditor = new ArrayList<DefaultCellEditor>();

		public TreeValueTableCellEditor(JComboBox comboBox) {
			super(comboBox);
		}

		public void clear() {
			refToBaseEntityEditor.clear();
		}

		@SuppressWarnings("unchecked")
		public void initCellEditors(UID subformEntity) {
			// Treat the cells in field name column special:
			JComboBox editBox = new JComboBox();
			DefaultCellEditor edit = new DefaultCellEditor(editBox);
			editBox.setRenderer(new DefaultListCellRenderer() {
				@Override
				public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
					final JLabel lbl = (JLabel)super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
					if (value instanceof UID) {
						lbl.setText(MetaProvider.getInstance().getEntityField((UID)value).getFieldName());
					}
					return lbl;
				}
			});
			refToBaseEntityEditor.add(edit);
			for(UID voField : getRefFieldTo(subformEntity)) {
				editBox.addItem(voField);
			}
		}

		@Override
		public Component getTableCellEditorComponent(JTable table,
			Object value, boolean isSelected, int row, int column) {
			if(column == 1)
				return refToBaseEntityEditor.get(row).getComponent();

			return super.getTableCellEditorComponent(table, value, isSelected,
				row, column);
		}

		@Override
		public Object getCellEditorValue() {
			final int row = tblTreeView.getSelectedRow();
			if (row == -1)
				return null;
			DefaultCellEditor celleditor = refToBaseEntityEditor.get(row);
			JComboBox box = (JComboBox)celleditor.getComponent();
			return box.getSelectedItem();
		}

		@Override
		public void reorder(int fromModel, int toModel) {
			// swap editors
			DefaultCellEditor from = refToBaseEntityEditor.get(fromModel);
			refToBaseEntityEditor.set(fromModel, refToBaseEntityEditor.get(toModel));
			refToBaseEntityEditor.set(toModel, from);
		}
	}

	@Override
	public boolean isComplete() {
		if (model.isGeneric()) {
			return true;
		} else {
			String nodeLabel = NuclosEntityTreeValueStep.this.model.getNodeLabel();
			return nodeLabel != null && nodeLabel.trim().length() > 0;
		}
	}
}

