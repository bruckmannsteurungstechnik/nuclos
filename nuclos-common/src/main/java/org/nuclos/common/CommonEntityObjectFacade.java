//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.util.Collection;
import java.util.List;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.genericobject.ProxyList;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;

/**
 * Common interface implemented by EntityObject -Delegates, -Facades, and -Providers.
 *
 * @author Thomas Pasch
 * @since Nuclos 3.1.01
 */
public interface CommonEntityObjectFacade {

	@RolesAllowed("Login")
	<PK> EntityObjectVO<PK> get(UID entity, PK pk) throws CommonPermissionException;

	@RolesAllowed("Login")
	<PK> EntityObjectVO<PK> getReferenced(UID referencingEntity, UID referencingEntityField, PK pk) throws CommonBusinessException;

	/**
	 * gets the ids of all leased objects that match a given search expression (ordered, when necessary)
	 * 
	 * @param entity name to search for leased objects in
	 * @param cse condition that the leased objects to be found must satisfy
	 * @return List&lt;Long&gt; list of leased object ids
	 * @throws CommonPermissionException 
	 */
	@RolesAllowed("Login")
	<PK> List<PK> getEntityObjectIds(UID entity, CollectableSearchExpression cse) throws CommonPermissionException;

	/**
	 * gets all generic objects along with its dependents, that match a given search condition
	 * §precondition stRequiredSubEntityNames != null
	 * §todo rename to getGenericObjectProxyList?
	 * @param entity uid of module to search for generic objects in
	 * @param clctexpr value object containing search expression
	 * @return list of generic object value objects
	 */
	@RolesAllowed("Login")
	<PK> ProxyList<PK,EntityObjectVO<PK>> getEntityObjectProxyList(
			UID entity, CollectableSearchExpression clctexpr,
			Collection<UID> fields, String customUsage) throws CommonPermissionException;

	/**
	 * gets more leased objects that match a given search condition
	 */
	@RolesAllowed("Login")
	<PK> EntityObjectVO<PK> getByIdWithDependents(UID entity, PK id, Collection<UID> fields, String customUsage)
		throws CommonPermissionException;

	@RolesAllowed("Login")
	<PK> Collection<EntityObjectVO<PK>> getEntityObjectsChunk(
			UID entity,
			CollectableSearchExpression clctexpr,
			ResultParams resultParams,
			String customUsage
	) throws CommonPermissionException;

	@RolesAllowed("Login")
	Long countEntityObjectRows(UID entity, CollectableSearchExpression clctexpr) throws  CommonPermissionException;

	@RolesAllowed("Login")
	Long countEntityObjectRowsWithLimit(UID entity, CollectableSearchExpression clctexpr, Long limit) throws  CommonPermissionException;

	/**
	 * gets the dependent master data records for the given entity, using the given foreign key field and the given id as foreign key.
	 * §precondition oRelatedId != null
	 * §todo restrict permissions by entity name
	 * @param entity uid of the entity to get all dependent master data records for
	 * @param oreignKeyField uid of the field relating to the foreign entity
	 * @param oRelatedId id by which sEntityName and sParentEntity are related
	 * @return
	 */

	@RolesAllowed("Login")
	<PK> Collection<EntityObjectVO<PK>> getDependentEntityObjects(UID entity, UID oreignKeyField, PK oRelatedId)
			throws CommonPermissionException;

	@RolesAllowed("Login")
	void removeEntity(UID entity, Object pk) throws CommonPermissionException;

	@RolesAllowed("Login")
    void remove(EntityObjectVO<?> object) throws CommonPermissionException;

	@RolesAllowed("Login")
	<PK> void createOrUpdatePlain(EntityObjectVO<PK> entity) throws CommonPermissionException;

	/**
	 * Get current version number.
	 * Used for high-performance version checks that do not require to load the entire object.
	 *
	 * @param entity Name of entity
	 * @param id Id of object
	 * @return Current version from database layer
	 * @throws CommonPermissionException If user is not allowed to read data of that entity.
	 */
	@RolesAllowed("Login")
	Integer getVersion(UID entity, Object id) throws CommonPermissionException;
	
	<PK> EntityObjectVO<PK> insert(EntityObjectVO<PK> object) throws CommonCreateException,
		CommonPermissionException, NuclosBusinessRuleException;

	<PK> void delete(UID entity, PK pk, boolean logicalDeletion, boolean bRemoveDependents)
			throws CommonFinderException,
			CommonRemoveException, CommonStaleVersionException, NuclosBusinessException,
			CommonPermissionException, CommonCreateException;
	
	<PK> EntityObjectVO<PK> update(EntityObjectVO<PK> eoVO, boolean bCollectiveProcessing)
			throws CommonCreateException, CommonFinderException, CommonRemoveException, 
			CommonStaleVersionException, CommonValidationException, CommonPermissionException, NuclosBusinessException;

	<PK> EntityObjectVO<PK> clone(EntityObjectVO<PK> eoVO, UID layout, final boolean fetchFromDb) throws CommonBusinessException;
	
	<PK> void setDefaultValues(EntityObjectVO<PK> eo);
	
	void cancelRunningStatements(UID entity);

	<PK> EntityObjectVO<PK> executeBusinessRules(List<EventSupportSourceVO> lstRuleVO, EntityObjectVO<PK> eoVO, String customUsage, boolean isCollectiveProcessing)
			throws CommonBusinessException;

	<PK> void executeBusinessRuleForPks(EventSupportSourceVO ruleVO, UID entity, Collection<PK> pks)
			throws CommonBusinessException;
}
