package org.nuclos.common;

public class EventSupportNotification extends SimpleClientNotification {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9098931818896026299L;

	EntityMeta<UID> entityChanged;
	
	public static final String COMPILERRORS = "COMPILERRORS";
	public static final String COMPILED = "COMPILED";
	
	public EventSupportNotification(EntityMeta<UID> pEntity) {
		this(Priority.NORMAL, pEntity.getEntityName() + " has changed");
		this.entityChanged = pEntity;
	}
	public EventSupportNotification(Priority priority, String sMessage) {
		super(priority, sMessage);
	}
	public EntityMeta<UID> getEntityChanged() {
		return entityChanged;
	}

}
