//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;

public class FieldMetaUtils {

	public static final String REF_PREFIX = "REF_";
	
	public static Collection<FieldMeta<?>> toFieldMetas(Collection<FieldMetaVO<?>> fields) {
		return new ArrayList<FieldMeta<?>>(CollectionUtils.transform(fields, new ToFieldMetaTransformer()));
	}
	
	public static class ToFieldMetaTransformer implements Transformer<FieldMetaVO<?>, FieldMeta<?>> {
		@Override
		public FieldMeta<?> transform(FieldMetaVO<?> i) {
			return i;
		}	
	}
	
	public static void putPkFields(Map<UID, FieldMeta<?>> into, Collection<EntityMeta<?>> fromEntityMetaSet) {
		for (EntityMeta<?> eMeta : fromEntityMetaSet) {
			putPkField(into, eMeta);
		}
	}
	
	public static void putPkField(Map<UID, FieldMeta<?>> into, EntityMeta<?> eMeta) {
		FieldMeta<?> pkMeta = eMeta.getPk().getMetaData(eMeta);
		into.put(pkMeta.getUID(), pkMeta);
	}
	
	public static String getColumnForEquality(String columnName) {
		String result = columnName.toUpperCase();
		if (result.startsWith("STRUID_")) {
			result = result.replaceFirst("^STRUID_", REF_PREFIX);
		} else if (result.startsWith("INTID_")) {
			result = result.replaceFirst("^INTID_", REF_PREFIX);
		} else if (result.startsWith("STRVALUE_")) {
			result = result.replaceFirst("^STRVALUE_", REF_PREFIX);
		} else if (result.startsWith("INTVALUE_")) {
			result = result.replaceFirst("^INTVALUE_", REF_PREFIX);
		} else if (result.startsWith("OBJVALUE_")) {
			result = result.replaceFirst("^OBJVALUE_", REF_PREFIX);
		}
		return result;
	}
	
	public static boolean isRefColumn(String columnName) {
		return getColumnForEquality(columnName).startsWith(REF_PREFIX);
	}
	
	public static boolean isPkColumn(String columnName) {
		return columnName.equalsIgnoreCase(SF.PK_ID.getDbColumn()) 
				|| columnName.equalsIgnoreCase(SF.PK_UID.getDbColumn());
	}
	
}
