//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.io.StringReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.layoutml.LayoutMLParser;
import org.nuclos.common2.layoutml.exception.LayoutMLException;
import org.nuclos.server.attribute.valueobject.AttributeCVO;
import org.nuclos.server.attribute.valueobject.LayoutUsageVO;
import org.xml.sax.InputSource;

/**
 * Leased object meta data CVO to be transferred from server to clients.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 * @deprecated use EntityMeta
 */
@Deprecated
public class GenericObjectMetaDataVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9082871433082045176L;

	private final LayoutCache layoutcache;

	private final LayoutUsageCache layoutusagecache;
	
	private final Map<UID, Set<UID>> attributes;

	/**
	 * @param mpLayouts Map&lt;Integer iLayoutId, String sLayoutML&gt;
	 * @param colllayoutusagevo Collection&lt;LayoutUsageVO&gt;
	 */
	public GenericObjectMetaDataVO(Map<UID, Set<UID>> attributes, Map<UID, String> mpLayouts, Collection<LayoutUsageVO> colllayoutusagevo) {
		// fill caches - information needed:

		// 1. attributes
		this.attributes = attributes;

		// 2. layouts
		this.layoutcache = new LayoutCache(mpLayouts);

		// 3. layoutusages
		this.layoutusagecache = new LayoutUsageCache(colllayoutusagevo);

		// 4. modules - do not need to be initialized
	}

	/**
	 * §postcondition result != null
	 * 
	 * @param attrprovider
	 * @param module UID may be <code>null</code>
	 * @param bSearchable
	 * @return Collection&lt;AttributeCVO&gt;
	 */
	public Collection<AttributeCVO> getAttributeCVOsByModule(AttributeProvider attrprovider, UID module, Boolean bSearchable) {
		return this.getAttributeCVOs(module, attrprovider, this.getAttributesByModule(module, bSearchable));
	}

	/**
	 * §postcondition result != null
	 * 
	 * @param module UID may be <code>null</code>
	 * @param bSearchable may be <code>null</code>.
	 * @return Set&lt;UID&gt;
	 */
	public Set<UID> getAttributesByModule(UID module, Boolean bSearchable) {
		return this.attributes.get(module);
	}

	public Set<UID> getLyoutsByModule(UID module, Boolean bSearchable) {
		return getLayoutsByModuleAndSearchFlag(this.layoutusagecache, module, bSearchable);
	}
	
	public Set<UID> getAttributesByLayout(UID layout, IMetaProvider metaProvider) {
		return this.layoutcache.getAttributes(layout, metaProvider);
	}

	/**
	 * §postcondition result != null
	 */
	public Set<UID> getSubFormEntityByLayout(UID layout, IMetaProvider metaProvider) {
		return this.layoutcache.getSubFormEntities(layout, metaProvider);
	}

	public Set<UID> getAllLayoutUids() {
		return this.layoutcache.getAllLayoutUids();
	}

	/**
	 * §postcondition result != null
	 */
	public Collection<EntityAndField> getSubFormEntityAndForeignKeyFieldsByLayout(UID layoutUid, IMetaProvider metaProvider) {
		return this.layoutcache.getSubFormEntityAndForeignKeyFields(layoutUid, metaProvider);
	}

	private Collection<AttributeCVO> getAttributeCVOs(UID entity, AttributeProvider attrprovider, Collection<UID> collAttributes) {
		return CollectionUtils.transform(collAttributes, new AttributeProvider.GetAttributeByUID(attrprovider));
	}

	/**
	 * §postcondition result != null
	 */
	public Set<UID> getBestMatchingLayoutAttributes(UsageCriteria usagecriteria, IMetaProvider metaProvider) throws CommonFinderException {
		final Set<UID> result = this.getAttributesByLayout(this.getBestMatchingLayout(usagecriteria, false), metaProvider);
		assert result != null;
		return result;
	}
	
	public Set<UID> getBestMatchingLayoutSubformEntities(UsageCriteria usagecriteria, IMetaProvider metaProvider) throws CommonFinderException {
		return this.getSubFormEntityByLayout(this.getBestMatchingLayout(usagecriteria, false), metaProvider);
	}
	
	public String getBestMatchingLayoutML(UsageCriteria usagecriteria, boolean bSearchScreen) throws CommonFinderException {
		final String result = this.getLayoutML(this.getBestMatchingLayout(usagecriteria, bSearchScreen));
		assert result != null;
		return result;
	}

	public String getLayoutML(UID layout) {
		return this.layoutcache.getLayoutML(layout);
	}

	public UID getBestMatchingLayout(UsageCriteria usagecriteria, boolean bSearchScreen) throws CommonFinderException {
		final Map<UsageCriteria, UID> mpLayoutUsages = this.layoutusagecache.getLayoutUsages(bSearchScreen);
		final UsageCriteria usagecriteriaBestMatching = UsageCriteria.getBestMatchingUsageCriteria(mpLayoutUsages.keySet(), usagecriteria);
		if (usagecriteriaBestMatching == null) {
			throw new CommonFinderException("No matching layout was found for usagecriteria " + usagecriteria + ".");
		}
		return mpLayoutUsages.get(usagecriteriaBestMatching);
	}

	/**
	 * @param moduleUid UID module may be <code>null</code>.
	 * @return Set&lt;UID&gt; of subform entities used in the module (details only) 
	 * 		with the given uid. If module id is <code>null</code>,
	 * 		the names of all subform entities used in any module (details only).
	 */
	public Set<UID> getSubFormEntityNamesByModuleId(UID moduleUid, IMetaProvider metaProvider) {
		final Set<UID> layouts = getLayoutsByModuleAndSearchFlag(this.layoutusagecache, moduleUid, false);
		final Set<UID> result = new HashSet<UID>();
		for (UID layoutUid : layouts) {
			result.addAll(this.getSubFormEntityByLayout(layoutUid, metaProvider));
		}
		return result;
	}

	/**
	 * Find layouts for Search or Details.
	 * @param layoutusagecache
	 * @param UID module may be <code>null</code>
	 * @param bSearchable
	 * @return Set<UID layout>
	 */
	private static Set<UID> getLayoutsByModuleAndSearchFlag(LayoutUsageCache layoutusagecache, UID module, boolean bSearchable) {
		final Set<UID> result = new HashSet<UID>();
		final Map<UsageCriteria, UID> mpLayoutUsages = layoutusagecache.getLayoutUsages(bSearchable);
		for (UsageCriteria usagecriteria : mpLayoutUsages.keySet()) {
			if (module == null || LangUtils.equal(usagecriteria.getEntityUID(), module)) {
				result.add(mpLayoutUsages.get(usagecriteria));
			}
		}
		assert result != null;
		return result;
	}

	/**
	 * Layout cache.
	 */
	private static class LayoutCache implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -1782766803213070099L;

		/**
		 * Map<UID layout, String sLayoutML>
		 */
		private final Map<UID, String> mpLayoutML;

		/**
		 * Map<UID layout, Set<UID>>
		 */
		private transient Map<UID, Set<UID>> mpAttributes = CollectionUtils.newHashMap();

		/**
		 * Map<UID layout, Collection<EntityAndField>>
		 */
		private transient Map<UID, Collection<EntityAndField>> mpSubFormEntityAndForeignKeyFields = CollectionUtils.newHashMap();

		LayoutCache(Map<UID, String> mpLayouts) {
			this.mpLayoutML = new HashMap<UID, String>(mpLayouts);
		}

		private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
			ois.defaultReadObject();
			this.mpAttributes = new HashMap<UID, Set<UID>>();
			this.mpSubFormEntityAndForeignKeyFields = new HashMap<UID, Collection<EntityAndField>>();
		}

		/**
		 * @param UID layout
		 * @return the LayoutML definition for the layout with the given uid.
		 */
		public String getLayoutML(UID layout) {
			return this.mpLayoutML.get(layout);
		}

		/**
		 * Get all layoutUIDs
		 * @return Set of layout UIDs
		 */
		public Set<UID> getAllLayoutUids() {
			return this.mpLayoutML.keySet();
		}

		/**
		 * §postcondition result != null
		 * 
		 * @param UID layout
		 * @return the uid of the attributes contained in the layout with the given uid.
		 */
		public synchronized Set<UID> getAttributes(UID layoutUid, IMetaProvider metaProvider) {
			Set<UID> result = this.mpAttributes.get(layoutUid);
			if (result == null) {
				final String sLayoutML = this.getLayoutML(layoutUid);
				if (StringUtils.isNullOrEmpty(sLayoutML)) {
					result = new HashSet<UID>();
				}
				else {
					try {
						result = new LayoutMLParser().getCollectableFieldUids(new InputSource(new StringReader(sLayoutML)));
					}
					catch (LayoutMLException ex) {
						throw new NuclosFatalException(ex);
					}
				}
				this.mpAttributes.put(layoutUid, result);
			}
			assert result != null;
			return result;
		}

		/**
		 * §postcondition result != null
		 * 
		 * @param layout UID
		 */
		public synchronized Set<UID> getSubFormEntities(UID layout, IMetaProvider metaProvider) {
			return new HashSet<UID>(CollectionUtils.transform(
					this.getSubFormEntityAndForeignKeyFields(layout, metaProvider), new EntityAndField.GetEntityUID()));
		}

		/**
		 * §postcondition result != null
		 * 
		 * @param layout UID
		 */
		public synchronized Collection<EntityAndField> getSubFormEntityAndForeignKeyFields(UID layoutUid, IMetaProvider metaProvider) {
			Collection<EntityAndField> result = this.mpSubFormEntityAndForeignKeyFields.get(layoutUid);
			if (result == null) {
				final String sLayoutML = this.getLayoutML(layoutUid);
				if (StringUtils.isNullOrEmpty(sLayoutML)) {
					result = new HashSet<EntityAndField>();
				}
				else {
					try {
						result = new LayoutMLParser().getSubFormEntityAndForeignKeyFields(new InputSource(new StringReader(sLayoutML)));
					}
					catch (LayoutMLException ex) {
						throw new NuclosFatalException(ex);
					}
				}
				this.mpSubFormEntityAndForeignKeyFields.put(layoutUid, result);
			}
			assert result != null;
			return result;
		}

	}	// inner class LayoutCache

	/**
	 * Layout usage cache.
	 */
	class LayoutUsageCache implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 4000105954040063752L;

		/**
		 * Map<UsageCriteria, UID layout>
		 */
		private final Map<UsageCriteria, UID> mpLayoutUsagesSearch = CollectionUtils.newHashMap();

		/**
		 * Map<UsageCriteria, UID layout>
		 */
		private final Map<UsageCriteria, UID> mpLayoutUsagesDetails = CollectionUtils.newHashMap();

		LayoutUsageCache(Collection<LayoutUsageVO> colllayoutusagevo) {
			for (LayoutUsageVO layoutusagevo : colllayoutusagevo) {
				this.getLayoutUsages(layoutusagevo.isSearchScreen()).put(layoutusagevo.getUsageCriteria(), layoutusagevo.getLayout());
			}
		}

		public Map<UsageCriteria, UID> getLayoutUsages(boolean bSearchScreen) {
			return (bSearchScreen ? mpLayoutUsagesSearch : mpLayoutUsagesDetails);
		}
	}	// inner class LayoutUsageCache

}	// class GenericObjectMetaDataVO
