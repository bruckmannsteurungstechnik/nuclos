//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.awt.Color;
import java.io.Serializable;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import org.nuclos.common.collect.DetailsPresentation;
import org.nuclos.common.collect.NuclosDetailToolBarConfiguration;
import org.nuclos.common.collect.NuclosDetailsSubformToolBarConfiguration;
import org.nuclos.common.collect.NuclosResultToolBarConfiguration;
import org.nuclos.common.collect.NuclosSearchSubformToolBarConfiguration;
import org.nuclos.common.collect.NuclosSearchToolBarConfiguration;
import org.nuclos.common.collect.ToolBarConfiguration;
import org.nuclos.common2.StringUtils;

/**
 * @param <T> given type
 * @param <S> stored type
 * @author tpasch2
 */
public interface LafParameter<T, S> {

	String LAF_PREFIX = "nuclos_LAF_";

	String VALUE_POSITION_TOP = LAF_PREFIX + "Position_top";
	String VALUE_POSITION_BOTTOM = LAF_PREFIX + "Position_bottom";

	Map<String, LafParameter<?, ?>> PARAMETERS = new HashMap<>();

	LafParameterStorage[] ALL_STORAGES = {LafParameterStorage.SYSTEMPARAMETER, LafParameterStorage.WORKSPACE, LafParameterStorage.ENTITY};

	LafParameter<DetailsPresentation, String> nuclos_LAF_Details_Presentation = new DetailsPresentationLafParameterImpl(
			LAF_PREFIX + "Details_Presentation",
			DetailsPresentation.DEFAULT,
			new DetailsPresentation[]{DetailsPresentation.OVERLAY, DetailsPresentation.POPUP}, ALL_STORAGES);

	LafParameter<Integer, String> nuclos_LAF_Result_Dynamic_Actions_Fixed_Height = new IntegerLafParameterImpl(
			LAF_PREFIX + "Result_Dynamic_Actions_Fixed_Height", -1, ALL_STORAGES);

	LafParameter<String, String> nuclos_LAF_Result_Dynamic_Actions_Position = new LafParameterImpl<>(
			LAF_PREFIX + "Result_Dynamic_Actions_Position", VALUE_POSITION_BOTTOM,
			new String[]{VALUE_POSITION_TOP, VALUE_POSITION_BOTTOM}, ALL_STORAGES);

	LafParameter<Boolean, String> nuclos_LAF_Result_Dynamic_Actions_Collapse = new BoolLafParameterImpl(
			LAF_PREFIX + "Result_Dynamic_Actions_Collapse", true, ALL_STORAGES);

	LafParameter<String, String> nuclos_LAF_Result_Selection_Buttons_Position = new LafParameterImpl<>(
			LAF_PREFIX + "Result_Selection_Buttons_Position", VALUE_POSITION_BOTTOM,
			new String[]{VALUE_POSITION_TOP, VALUE_POSITION_BOTTOM}, ALL_STORAGES);

	LafParameter<Boolean, String> nuclos_LAF_Result_Show_Details_For_Single_Search_Result = new BoolLafParameterImpl(
			LAF_PREFIX + "Result_Show_Details_For_Single_Search_Result", false,
			LafParameterStorage.SYSTEMPARAMETER, LafParameterStorage.WORKSPACE, LafParameterStorage.ENTITY);

	LafParameter<Boolean, String> nuclos_LAF_Live_Search_Enabled = new BoolLafParameterImpl(
			LAF_PREFIX + "Live_Search_Enabled", true, LafParameterStorage.SYSTEMPARAMETER, LafParameterStorage.WORKSPACE);

	LafParameter<Boolean, String> nuclos_LAF_Details_Navigation_Enabled = new BoolLafParameterImpl(
			LAF_PREFIX + "Details_Navigation_Enabled", true, LafParameterStorage.SYSTEMPARAMETER, LafParameterStorage.WORKSPACE, LafParameterStorage.ENTITY);

	LafParameter<NuclosSearchToolBarConfiguration, String> nuclos_LAF_Tool_Bar_Search = new NuclosSearchLafParameterImpl(
			LAF_PREFIX + "Tool_Bar_Search", NuclosSearchToolBarConfiguration.DEFAULT,
			LafParameterStorage.SYSTEMPARAMETER, LafParameterStorage.WORKSPACE, LafParameterStorage.ENTITY);

	LafParameter<NuclosResultToolBarConfiguration, String> nuclos_LAF_Tool_Bar_Result = new NuclosResultLafParameterImpl(
			LAF_PREFIX + "Tool_Bar_Result", NuclosResultToolBarConfiguration.DEFAULT,
			LafParameterStorage.SYSTEMPARAMETER, LafParameterStorage.WORKSPACE, LafParameterStorage.ENTITY);

	LafParameter<NuclosDetailToolBarConfiguration, String> nuclos_LAF_Tool_Bar_Detail = new NuclosDetailsLafParameterImpl(
			LAF_PREFIX + "Tool_Bar_Detail", NuclosDetailToolBarConfiguration.DEFAULT,
			LafParameterStorage.SYSTEMPARAMETER, LafParameterStorage.WORKSPACE, LafParameterStorage.ENTITY);

	LafParameter<NuclosDetailsSubformToolBarConfiguration, String> nuclos_LAF_Tool_Bar_Detail_Subform = new NuclosDetailsSubformLafParameterImpl(
			LAF_PREFIX + "Tool_Bar_Detail_Subform", NuclosDetailsSubformToolBarConfiguration.DEFAULT,
			LafParameterStorage.SYSTEMPARAMETER, LafParameterStorage.WORKSPACE, LafParameterStorage.ENTITY);

	LafParameter<NuclosSearchSubformToolBarConfiguration, String> nuclos_LAF_Tool_Bar_Search_Subform = new NuclosSearchSubformLafParameterImpl(
			LAF_PREFIX + "Tool_Bar_Search_Subform", NuclosSearchSubformToolBarConfiguration.DEFAULT,
			LafParameterStorage.SYSTEMPARAMETER, LafParameterStorage.WORKSPACE, LafParameterStorage.ENTITY);

	LafParameter<String, String> nuclos_LAF_Webclient_Popup = new StringLafParameterImpl(
			LAF_PREFIX + "Webclient_Popup", "", LafParameterStorage.ENTITY);

	LafParameter<Boolean, String> nuclos_LAF_Webclient_Subform_Show_Selection_Column = new BoolLafParameterImpl(
			LAF_PREFIX + "Webclient_Subform_Show_Selection_Column", true, LafParameterStorage.SYSTEMPARAMETER, LafParameterStorage.ENTITY);

	LafParameter<Boolean, String> nuclos_LAF_Single_Sort = new BoolLafParameterImpl(
			LAF_PREFIX + "Single_Sort", false, LafParameterStorage.SYSTEMPARAMETER, LafParameterStorage.WORKSPACE);

	LafParameter<Boolean, String> nuclos_LAF_Details_All_Meta_Information_Enabled = new BoolLafParameterImpl(
			LAF_PREFIX + "Details_All_Meta_Information_Enabled", false,
			LafParameterStorage.SYSTEMPARAMETER, LafParameterStorage.WORKSPACE, LafParameterStorage.ENTITY);

	LafParameter<Boolean, String> nuclos_LAF_Layout_Tab_Show_Row_Count = new BoolLafParameterImpl(
			LAF_PREFIX + "Layout_Tab_Show_Row_Count", true,
			LafParameterStorage.SYSTEMPARAMETER, LafParameterStorage.WORKSPACE, LafParameterStorage.ENTITY);

	LafParameter<SplitViewSettings, String> nuclos_LAF_Split_View_Settings = new SplitViewSettingsLafParameterImpl(
			LAF_PREFIX + "Split_View_Settings", new SplitViewSettings(), ALL_STORAGES);

	// For Webclient only
	LafParameter<Boolean, String> nuclos_LAF_Empty_Result_List_If_No_Search_Condition = new BoolLafParameterImpl(
			LAF_PREFIX + "Empty_Result_List_If_No_Search_Condition", false,
			LafParameterStorage.ENTITY);

	/**
	 * RSWORGA-183
	 */
	Color MFTabbedPane_Inactive_BgColor = new Color(Color.BLUE.getRed(), Color.BLUE.getGreen(), Color.BLUE.getBlue(), 50);
	LafParameter<Color, String> nuclos_LAF_MFTabbedPane_Home_Inactive_BgColor = new ColorLafParameterImpl(
			LAF_PREFIX + "MFTabbedPane_Home_Inactive_BgColor",
			MFTabbedPane_Inactive_BgColor,
			LafParameterStorage.SYSTEMPARAMETER,
			LafParameterStorage.WORKSPACE);

	String getName();

	T getDefault();

	Class<T> getParameterClass();

	T[] getFixedValueList();

	boolean isStoragePossible(LafParameterStorage storage);

	S convertToStore(T t);

	T convertFromStore(S s) throws ParseException;

	default public T getValue(UID entityUid, WorkspaceDescription2 woDesc, ParameterProvider parameterProvider, IMetaProvider metaProvider) {
		T result = null;

		// 1. Entity
		if (result == null && entityUid != null && metaProvider != null && isStoragePossible(LafParameterStorage.ENTITY)) {
			LafParameterMap entityLafParameters = metaProvider.getLafParameters(entityUid);
			if (entityLafParameters != null) {
				result = (T) entityLafParameters.getValue(this);
			}
		}

		// 2. Workspace
		if (result == null && woDesc != null && isStoragePossible(LafParameterStorage.WORKSPACE)) {
			try {
				result = convertFromStore((S) woDesc.getParameter(getName()));
			} catch (ParseException e) {
				result = getDefault();
			}
		}

		// 3. Systemparameter
		if (result == null && parameterProvider != null && isStoragePossible(LafParameterStorage.SYSTEMPARAMETER)) {
			try {
				result = convertFromStore((S) parameterProvider.getValue(getName()));
			} catch (ParseException e) {
				result = getDefault();
			}
		}

		// 4. Nuclos default
		if (result == null) {
			result = getDefault();
		}

		return result;
	}

	abstract class AbstractLafParameterImpl<T, S> implements LafParameter<T, S>, Serializable {

		/**
		 *
		 */
		private static final long serialVersionUID = 5782063615239503229L;

		private final String name;

		private final T defaultValue;

		private final LafParameterStorage[] storages;

		private final T[] fixedValueList;

		AbstractLafParameterImpl(String name, T defaultValue, LafParameterStorage... storages) {
			this(name, defaultValue, null, storages);
		}

		AbstractLafParameterImpl(String name, T defaultValue, T[] fixedValueList, LafParameterStorage... storages) {
			assert defaultValue != null;

			this.name = name;
			this.defaultValue = defaultValue;
			this.fixedValueList = fixedValueList;
			this.storages = storages;
			PARAMETERS.put(name, this);
		}

		@Override
		public String getName() {
			return name;
		}

		@SuppressWarnings("unchecked")
		@Override
		public Class<T> getParameterClass() {
			return (Class<T>) defaultValue.getClass();
		}

		public T getDefault() {
			return defaultValue;
		}

		@Override
		public T[] getFixedValueList() {
			return fixedValueList;
		}

		public boolean isStoragePossible(LafParameterStorage storage) {
			for (final LafParameterStorage storage1 : storages) {
				if (storage1 == storage) {
					return true;
				}
			}
			return false;
		}

		@Override
		public int hashCode() {
			return name.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj instanceof AbstractLafParameterImpl) {
				return name.equals(((AbstractLafParameterImpl<?, ?>) obj).getName());
			}
			return super.equals(obj);
		}

	}

	class ColorLafParameterImpl extends AbstractLafParameterImpl<Color, String> {

		/**
		 *
		 */
		private static final long serialVersionUID = 5195034323193487704L;
		final ColorFormat format = ColorFormat.getInstance();

		ColorLafParameterImpl(String name, Color defaultValue, LafParameterStorage... storages) {
			super(name, defaultValue, storages);
		}

		@Override
		public String convertToStore(Color c) {
			return format.format(c);
		}

		@Override
		public Color convertFromStore(String s) throws ParseException {
			return (Color) format.parseObject(s);
		}

	}

	class LafParameterImpl<T> extends AbstractLafParameterImpl<T, T> {

		private static final long serialVersionUID = 2560045731145642607L;

		LafParameterImpl(String name, T defaultValue, T[] fixedValueList, LafParameterStorage... storages) {
			super(name, defaultValue, fixedValueList, storages);
		}

		@Override
		public T convertToStore(T t) {
			return t;
		}

		@Override
		public T convertFromStore(T s) {
			return s;
		}

	}

	class IntegerLafParameterImpl extends AbstractLafParameterImpl<Integer, String> {

		/**
		 *
		 */
		private static final long serialVersionUID = 322776448880808396L;

		IntegerLafParameterImpl(String name, Integer defaultValue, LafParameterStorage... storages) {
			this(name, defaultValue, null, storages);
		}

		IntegerLafParameterImpl(String name, Integer defaultValue, Integer[] fixedValueList, LafParameterStorage... storages) {
			super(name, defaultValue, fixedValueList, storages);
		}

		@Override
		public String convertToStore(Integer t) {
			if (t == null) {
				return "";
			}
			return Integer.toString(t);
		}

		@Override
		public Integer convertFromStore(String s) {
			if (StringUtils.isNullOrEmpty(s)) {
				return null;
			}
			return Integer.parseInt(s);
		}
	}

	class BoolLafParameterImpl extends AbstractLafParameterImpl<Boolean, String> {

		/**
		 *
		 */
		private static final long serialVersionUID = 4609080171091402948L;

		BoolLafParameterImpl(String name, Boolean defaultValue, LafParameterStorage... storages) {
			this(name, defaultValue, null, storages);
		}

		BoolLafParameterImpl(String name, Boolean defaultValue, Boolean[] fixedValueList, LafParameterStorage... storages) {
			super(name, defaultValue, fixedValueList, storages);
		}

		@Override
		public String convertToStore(Boolean t) {
			if (t == null) {
				return "";
			}
			return Boolean.toString(t);
		}

		@Override
		public Boolean convertFromStore(String s) {
			if (StringUtils.isNullOrEmpty(s)) {
				return null;
			}
			return Boolean.parseBoolean(s);
		}

	}

	class StringLafParameterImpl extends AbstractLafParameterImpl<String, String> {

		/**
		 *
		 */
		private static final long serialVersionUID = 559206563810745101L;

		StringLafParameterImpl(String name, String defaultValue, LafParameterStorage... storages) {
			this(name, defaultValue, null, storages);
		}

		StringLafParameterImpl(String name, String defaultValue, String[] fixedValueList, LafParameterStorage... storages) {
			super(name, defaultValue, fixedValueList, storages);
		}

		@Override
		public String convertToStore(String t) {
			if (t == null) {
				return "";
			}
			return t;
		}

		@Override
		public String convertFromStore(String s) {
			return s;
		}
	}

	abstract class ToolBarLafParameterImpl<T extends ToolBarConfiguration> extends AbstractLafParameterImpl<T, String> {

		/**
		 *
		 */
		private static final long serialVersionUID = -2639526736061724171L;

		ToolBarLafParameterImpl(String name, T defaultValue, T[] fixedValueList, LafParameterStorage... storages) {
			super(name, defaultValue, fixedValueList, storages);
		}

		@Override
		public String convertToStore(ToolBarConfiguration t) {
			if (t == null) {
				return "";
			}
			return t.toString();
		}
	}

	class NuclosDetailsSubformLafParameterImpl extends ToolBarLafParameterImpl<NuclosDetailsSubformToolBarConfiguration> {

		/**
		 *
		 */
		private static final long serialVersionUID = 2264915971167295511L;

		NuclosDetailsSubformLafParameterImpl(String name, NuclosDetailsSubformToolBarConfiguration defaultValue, LafParameterStorage... storages) {
			this(name, defaultValue, null, storages);
		}

		NuclosDetailsSubformLafParameterImpl(String name, NuclosDetailsSubformToolBarConfiguration defaultValue, NuclosDetailsSubformToolBarConfiguration[] fixedValueList, LafParameterStorage... storages) {
			super(name, defaultValue, fixedValueList, storages);
		}

		@Override
		public NuclosDetailsSubformToolBarConfiguration convertFromStore(String s) {
			if (StringUtils.isNullOrEmpty(s)) {
				return null;
			}
			return new NuclosDetailsSubformToolBarConfiguration(s);
		}

	}

	class NuclosDetailsLafParameterImpl extends ToolBarLafParameterImpl<NuclosDetailToolBarConfiguration> {

		/**
		 *
		 */
		private static final long serialVersionUID = 643302564018149816L;

		NuclosDetailsLafParameterImpl(String name, NuclosDetailToolBarConfiguration defaultValue, LafParameterStorage... storages) {
			this(name, defaultValue, null, storages);
		}

		NuclosDetailsLafParameterImpl(String name, NuclosDetailToolBarConfiguration defaultValue, NuclosDetailToolBarConfiguration[] fixedValueList, LafParameterStorage... storages) {
			super(name, defaultValue, fixedValueList, storages);
		}

		@Override
		public NuclosDetailToolBarConfiguration convertFromStore(String s) {
			if (StringUtils.isNullOrEmpty(s)) {
				return null;
			}
			return new NuclosDetailToolBarConfiguration(s);
		}

	}

	class NuclosResultLafParameterImpl extends ToolBarLafParameterImpl<NuclosResultToolBarConfiguration> {

		/**
		 *
		 */
		private static final long serialVersionUID = 122578185225207441L;

		NuclosResultLafParameterImpl(String name, NuclosResultToolBarConfiguration defaultValue, LafParameterStorage... storages) {
			this(name, defaultValue, null, storages);
		}

		NuclosResultLafParameterImpl(String name, NuclosResultToolBarConfiguration defaultValue, NuclosResultToolBarConfiguration[] fixedValueList, LafParameterStorage... storages) {
			super(name, defaultValue, fixedValueList, storages);
		}

		@Override
		public NuclosResultToolBarConfiguration convertFromStore(String s) {
			if (StringUtils.isNullOrEmpty(s)) {
				return null;
			}
			return new NuclosResultToolBarConfiguration(s);
		}
	}

	class NuclosSearchSubformLafParameterImpl extends ToolBarLafParameterImpl<NuclosSearchSubformToolBarConfiguration> {
		/**
		 *
		 */
		private static final long serialVersionUID = 3921272861789336893L;

		NuclosSearchSubformLafParameterImpl(String name, NuclosSearchSubformToolBarConfiguration defaultValue, LafParameterStorage... storages) {
			this(name, defaultValue, null, storages);
		}

		NuclosSearchSubformLafParameterImpl(String name, NuclosSearchSubformToolBarConfiguration defaultValue, NuclosSearchSubformToolBarConfiguration[] fixedValueList, LafParameterStorage... storages) {
			super(name, defaultValue, fixedValueList, storages);
		}

		@Override
		public NuclosSearchSubformToolBarConfiguration convertFromStore(String s) {
			if (StringUtils.isNullOrEmpty(s)) {
				return null;
			}
			return new NuclosSearchSubformToolBarConfiguration(s);
		}

	}

	class NuclosSearchLafParameterImpl extends ToolBarLafParameterImpl<NuclosSearchToolBarConfiguration> {
		/**
		 *
		 */
		private static final long serialVersionUID = 5974903080527215391L;

		NuclosSearchLafParameterImpl(String name, NuclosSearchToolBarConfiguration defaultValue, LafParameterStorage... storages) {
			this(name, defaultValue, null, storages);
		}

		NuclosSearchLafParameterImpl(String name, NuclosSearchToolBarConfiguration defaultValue, NuclosSearchToolBarConfiguration[] fixedValueList, LafParameterStorage... storages) {
			super(name, defaultValue, fixedValueList, storages);
		}

		@Override
		public NuclosSearchToolBarConfiguration convertFromStore(String s) {
			if (StringUtils.isNullOrEmpty(s)) {
				return null;
			}
			return new NuclosSearchToolBarConfiguration(s);
		}
	}

	class DetailsPresentationLafParameterImpl extends AbstractLafParameterImpl<DetailsPresentation, String> {

		/**
		 *
		 */
		private static final long serialVersionUID = -2608582161963608920L;

		DetailsPresentationLafParameterImpl(String name, DetailsPresentation defaultValue, DetailsPresentation[] fixedValueList, LafParameterStorage... storages) {
			super(name, defaultValue, fixedValueList, storages);
		}

		@Override
		public String convertToStore(DetailsPresentation t) {
			if (t == null) {
				return null;
			}
			return t.toString();
		}

		@Override
		public DetailsPresentation convertFromStore(String s) {
			if (StringUtils.isNullOrEmpty(s)) {
				return null;
			}
			return DetailsPresentation.get(s);
		}
	}

	class SplitViewSettingsLafParameterImpl extends AbstractLafParameterImpl<SplitViewSettings, String> {

		/**
		 *
		 */
		private static final long serialVersionUID = 4833849179766576480L;

		SplitViewSettingsLafParameterImpl(String name, SplitViewSettings defaultValue, LafParameterStorage... storages) {
			super(name, defaultValue, null, storages);
		}

		@Override
		public String convertToStore(SplitViewSettings t) {
			if (t == null) {
				return null;
			}
			return t.toString();
		}

		@Override
		public SplitViewSettings convertFromStore(String s) {
			if (StringUtils.isNullOrEmpty(s)) {
				return null;
			}
			return SplitViewSettings.fromString(s);
		}
	}
}
