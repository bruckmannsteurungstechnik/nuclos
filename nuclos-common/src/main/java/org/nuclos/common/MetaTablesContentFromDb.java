package org.nuclos.common;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * Created by Oliver Brausch on 16.10.17.
 */
public class MetaTablesContentFromDb implements Serializable {

	private static final long serialVersionUID = 9051582952482335457L;

	private final List<NucletEntityMeta> nucletEntities;
	private final List<NucletEntityMeta> dataLangEntities;
	private final List<NucletEntityMeta> dynamicEntities;
	private final List<NucletEntityMeta> chartEntities;
	private final List<NucletEntityMeta> dynamicListEntities;

	public MetaTablesContentFromDb(final List<NucletEntityMeta> nucletEntities, final List<NucletEntityMeta> dataLangEntities, final List<NucletEntityMeta> dynamicEntities, final List<NucletEntityMeta> chartEntities, final List<NucletEntityMeta> dynamicListEntities) {
		this.nucletEntities = nucletEntities;
		this.dataLangEntities = dataLangEntities;
		this.dynamicEntities = dynamicEntities;
		this.chartEntities = chartEntities;
		this.dynamicListEntities = dynamicListEntities;
	}

	public List<NucletEntityMeta> getNucletEntities() {
		return Collections.unmodifiableList(nucletEntities);
	}

	public List<NucletEntityMeta> getDataLangEntities() {
		return Collections.unmodifiableList(dataLangEntities);
	}

	public List<NucletEntityMeta> getDynamicEntities() {
		return Collections.unmodifiableList(dynamicEntities);
	}

	public List<NucletEntityMeta> getChartEntities() {
		return Collections.unmodifiableList(chartEntities);
	}

	public List<NucletEntityMeta> getDynamicListEntities() {
		return Collections.unmodifiableList(dynamicListEntities);
	}
}
