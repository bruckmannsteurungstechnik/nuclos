package org.nuclos.common;

import org.nuclos.common.dal.vo.IDalVO;
import org.nuclos.common.dal.vo.IVersionVO;
import org.nuclos.common2.InternalTimestamp;

public class NucletEntityContext extends EntityContextVO implements IDalVO<UID>, IVersionVO<UID> {

	private static final long serialVersionUID = -1000949374703660582L;

	private String processor;
	private int state;

	private UID primaryKey;
	private InternalTimestamp createdAt;
	private String createdBy;
	private InternalTimestamp changedAt;
	private String changedBy;
	private int version = -1;

	public NucletEntityContext(UID entityUID) {
		super();
		if (!E.ENTITYCONTEXT.getUID().equals(entityUID)) {
			throw new IllegalArgumentException();
		}
	}

	@Override
	public UID getPrimaryKey() {
		return primaryKey;
	}

	@Override
	public void setPrimaryKey(UID primaryKey) {
		this.primaryKey = primaryKey;
	};

	@Override
	public final void flagNew() {
		this.state = STATE_NEW;
	}

	@Override
	public final void flagUpdate() {
		this.state = STATE_UPDATED;
	}

	@Override
	public final void flagRemove() {
		this.state = STATE_REMOVED;
	}

	@Override
	public final boolean isFlagNew() {
		return this.state == STATE_NEW;
	}

	@Override
	public final boolean isFlagUpdated() {
		return this.state == STATE_UPDATED;
	}

	@Override
	public final boolean isFlagRemoved() {
		return this.state == STATE_REMOVED;
	}

	@Override
	public boolean isFlagUnchanged() {
		return this.state == STATE_UNCHANGED;
	}

	@Override
	public UID getDalEntity() {
		return E.ENTITYCONTEXT.getUID();
	}

	@Override
	public String processor() {
		return processor;
	}

	@Override
	public void processor(String p) {
		this.processor = p;
	}

	@Override
	public InternalTimestamp getCreatedAt() {
		return createdAt;
	}

	@Override
	public void setCreatedAt(InternalTimestamp createdAt) {
		this.createdAt = createdAt;
	}

	@Override
	public String getCreatedBy() {
		return createdBy;
	}

	@Override
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Override
	public InternalTimestamp getChangedAt() {
		return changedAt;
	}

	@Override
	public void setChangedAt(InternalTimestamp changedAt) {
		this.changedAt = changedAt;
	}

	@Override
	public String getChangedBy() {
		return changedBy;
	}

	@Override
	public void setChangedBy(String changedBy) {
		this.changedBy = changedBy;
	}

	@Override
	public int getVersion() {
		return version;
	}

	@Override
	public void setVersion(Integer version) {
		if (version == null) {
			version = Integer.valueOf(-1);
		}
		this.version = version;
	}
}
