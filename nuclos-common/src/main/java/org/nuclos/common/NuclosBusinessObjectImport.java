package org.nuclos.common;

/**
 * All static imports that are used in the resulting BO classes should be listed here.
 * {@link NuclosEntityValidator} uses this information for name validation.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 * @see <a href="http://support.nuclos.de/browse/NUCLOS-2790">NUCLOS-2790</a>
 */
public enum NuclosBusinessObjectImport implements INuclosEntityImport {
	// org.nuclos.api
	UID(org.nuclos.api.UID.class),

	// org.nuclos.api.businessobject
	DEPENDENT(org.nuclos.api.businessobject.Dependent.class),
	FLAG(org.nuclos.api.businessobject.Flag.class),
	PROCESS(org.nuclos.api.businessobject.Process.class),

	// org.nuclos.api.businessobject.attribute
	ATTRIBUTE(org.nuclos.api.businessobject.attribute.Attribute.class),
	PRIMARY_KEY_ATTRIBUTE(org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute.class),
	NUMERIC_ATTRIBUTE(org.nuclos.api.businessobject.attribute.NumericAttribute.class),
	STRING_ATTRIBUTE(org.nuclos.api.businessobject.attribute.StringAttribute.class),
	FOREIGN_KEY_ATTRIBUTE(org.nuclos.api.businessobject.attribute.ForeignKeyAttribute.class),

	// java.util
	DATE(java.util.Date.class),
	ARRAY_LIST(java.util.ArrayList.class),
	LIST(java.util.List.class);

	private final Class<?> cls;

	NuclosBusinessObjectImport(final Class<?> cls) {
		this.cls = cls;
	}

	public Class<?> getImportClass() {
		return cls;
	}
}
