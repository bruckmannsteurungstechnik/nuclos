//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.attribute;

import java.io.Serializable;

import org.nuclos.common.AttributeProvider;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.attribute.valueobject.AttributeCVO;
import org.nuclos.server.genericobject.valueobject.CanonicalAttributeFormat;



/**
 * Value object representing a dynamic leased object attribute.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @version 01.00.000
 */
public class DynamicAttributeVO implements Serializable, Cloneable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3819820907560838449L;
	private boolean bRemoved = false;
	private UID attribute;
	private Long valueId;
	private UID valueUid;
	private Object oValue;

	/**
	 * constructor to be called by server only
	 * 
	 * @param attribute uid of underlying database record
	 * @param valueId id of underlying database record
	 * @param sCanonicalValue value of underlying database record
	 */
	public DynamicAttributeVO(UID attribute, Long valueId, UID valueUid, String sCanonicalValue) throws CommonValidationException {
		this(attribute, valueId, valueUid, sCanonicalValue, 
				(AttributeProvider)SpringApplicationContextHolder.getBean("attributeProvider"));
	}

	/**
	 * constructor to be called by server only
	 * 
	 * @param attribute uid of underlying database record
	 * @param valueId id of underlying database record
	 * @param sCanonicalValue value of underlying database record
	 * @param attrprovider provides the attribute for the given attribute id
	 */
	public DynamicAttributeVO(UID attribute, Long valueId, UID valueUid, String sCanonicalValue,
			AttributeProvider attrprovider) throws CommonValidationException {
		this.attribute = attribute;
		this.valueId = valueId;
		this.valueUid = valueUid;
		setCanonicalValue(sCanonicalValue, attrprovider);
	}

	/**
	 * constructor to be called by client only
	 * 
	 * @param attribute UID of underlying database record
	 * @param valueId value id of underlying database record
	 * @param oValue value of underlying database record
	 */
	public DynamicAttributeVO(UID attribute, Long valueId, UID valueUid, Object oValue) {
		super();
		this.attribute = attribute;
		this.valueId = valueId;
		this.valueUid = valueUid;
		this.oValue = oValue;
		/** @todo check oValue? */
	}

	/**
	 * @return a clone from this
	 */
	@Override
	public final DynamicAttributeVO clone() {
		try {
			return (DynamicAttributeVO) super.clone();
			// Note that oValue should be immutable.
		}
		catch (CloneNotSupportedException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * §todo make this a regular ctor again
	 * 
	 * @param attribute
	 * @param valueId
	 * @param valueUid
	 * @param sCanonicalValue
	 * @param attrprovider provides the attribute for the given attribute id
	 * @return
	 * @throws CommonValidationException
	 */
	public static DynamicAttributeVO createGenericObjectAttributeVOCanonical(UID attribute, Long valueId, UID valueUid, String sCanonicalValue, AttributeProvider attrprovider) throws CommonValidationException {
		final AttributeCVO attrcvo = attrprovider.getAttribute(attribute);
		try {
			return new DynamicAttributeVO(attribute, valueId, valueUid, getCanonicalFormat(attrcvo).parse(sCanonicalValue));
		}
		catch (CommonValidationException ex) {
			throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage("dynamicattrvo.invalid.field.value.1", getDisplayableFieldName(attrcvo), ex.getMessage()), ex);
				//"Fehlerhafter Wert im Feld " + getDisplayableFieldName(attrcvo) + ": " + ex.getMessage(), ex);
		}
	}

	/**
	 * @return attribute id of underlying database record
	 */
	public UID getAttributeUID() {
		return attribute;
	}

	/**
	 * @return value id of underlying database record
	 */
	public Long getValueId() {
		return valueId;
	}

	/**
	 * @param valueId value id of underlying database record
	 */
	public void setValueId(Long valueId) {
		this.valueId = valueId;
	}

	public UID getValueUid() {
		return valueUid;
	}

	public void setValueUid(UID valueUid) {
		this.valueUid = valueUid;
	}

	public static CanonicalAttributeFormat getCanonicalFormat(Class<?> cls) {
		return CanonicalAttributeFormat.getInstance(cls);
	}

	/**
	 * §precondition attrcvo != null
	 * 
	 * @param attrcvo
	 * @return the canonical attribute format for the given attribute.
	 */
	public static CanonicalAttributeFormat getCanonicalFormat(AttributeCVO attrcvo) {
		return getCanonicalFormat(attrcvo.getJavaClass());
	}

	/**
	 * @param attrprovider provides the attribute for <code>this</code>' attribute id
	 * @return the canonical representation of <code>getValue()</code>.
	 */
	public String getCanonicalValue(AttributeProvider attrprovider) {
		return this.getCanonicalValue(attrprovider.getAttribute(getAttributeUID()));
	}

	private String getCanonicalValue(AttributeCVO attrcvo) {
		/** @todo check if attrcvo.getId() == this.getAttributeId() */
		return getCanonicalFormat(attrcvo.getJavaClass()).format(oValue);
	}

	/**
	 * sets the value using the given canonical representation.
	 * @param sCanonicalValue
	 * @param attrprovider provides the attribute for <code>this</code>' attribute id
	 * @throws CommonValidationException
	 */
	public void setCanonicalValue(String sCanonicalValue, AttributeProvider attrprovider) throws CommonValidationException {
		if (getAttributeUID() == null) {
			throw new IllegalStateException("attributeUID");
		}
		final AttributeCVO attrcvo = attrprovider.getAttribute(getAttributeUID());
		if (attrcvo == null || !getAttributeUID().equals(attrcvo.getId())) {
			throw new IllegalArgumentException("attrcvo");
		}

		this.oValue = getCanonicalFormat(attrcvo.getJavaClass()).parse(sCanonicalValue);
	}

	/**
	 * We don't demand the attributecvo here, but we should maybe...
	 * <p>
	 * Attention: This method normally does not do what you expect as it needs very special
	 * formatted values (e.g. 'ja' and 'nein' String for Boolean, special formatted String for 
	 * Date etc.). I recommend to use {@link #setParsedValue}. (tp)
	 * <p>
	 * §todo adjust comment
	 * 
	 * @param oValue
	 */
	public void setValue(Object oValue) {
		this.oValue = oValue;
	}
	
	/**
	 * This is a more save alternative to {@link #setValue(Object)} as it honours the strange 
	 * conversion requirements of {@link DynamicAttributeVO}.
	 * 
	 * @see #setValue(Object)
	 * @param o - value to set
	 * @param prov - attribute provider to use
	 * @throws CommonValidationException
	 */
	public void setParsedValue(Object o, AttributeProvider prov) throws CommonValidationException {
		final AttributeCVO attrcvo = prov.getAttribute(getAttributeUID());
		if (getAttributeUID() == null) {
			throw new IllegalStateException("attributeId");
		}
		if (attrcvo == null || !getAttributeUID().equals(attrcvo.getId())) {
			throw new IllegalArgumentException("attrcvo");
		}
		final CanonicalAttributeFormat format = getCanonicalFormat(attrcvo.getJavaClass());
		String formatted = null;
		try {
			formatted = format.format(o);
		}
		catch (ClassCastException e) {
			// We must catch here as there are defects in s = format.parse(format.format(s'))
			// I consider this as design flaw. (tp)
			// 
			// Example: StringFormat defines:
			// public String format(Object oValue) {
			// 	return (String) oValue;
			// }
			// But is seems to get Integer as input ...

			if (o == null) {
				formatted = null;
			}
			else {
				formatted = String.valueOf(o);
			}
		}
		this.oValue = format.parse(formatted);
	}

	public Object getValue() {
		return this.oValue;
	}

	/**
	 * is underlying database record to be removed from database?
	 * @return boolean value
	 */
	public boolean isRemoved() {
		return this.bRemoved;
	}

	/**
	 * Mark underlying database record as to be removed from database.
	 * Set content to empty to prevent saving of removed attributes.
	 */
	public void remove() {
		valueId = null;
		oValue = null;
		this.bRemoved = true;
	}

	/**
	 * reverse operation of remove.
	 */
	public void unremove() {
		this.bRemoved = false;
	}

	/**
	 * §precondition attrcvo != null
	 * 
	 * @param attrcvo
	 * @return the label + the name of attrcvo, for display in an error message.
	 */
	private static String getDisplayableFieldName(AttributeCVO attrcvo) {
		//"\"" + attrcvo.getLabel() + "\" (Attributname: \"" + attrcvo.getName() + "\")";
		return StringUtils.getParameterizedExceptionMessage("dynamicattrvo.invalid.field.value.7", attrcvo.getLabel(), attrcvo.getName());
		//SpringLocaleDelegate.getMessage("DynamicAttributeVO.1","\"{0}\" (Attributname: \"{1}\")", CommonLocaleDelegate.getLabelFromAttributeCVO(attrcvo), attrcvo.getName());
	}
	
	@Override
	public String toString() {
		final StringBuffer result = new StringBuffer();
		result.append("DaVO[aId=");
		result.append(attribute);
		if (oValue != null) {
			result.append(",value=").append(oValue);
		}
		if (valueId != null) {
			result.append(",vId=").append(valueId);
		}
		if (bRemoved) {
			result.append(",removed=").append(bRemoved);
		}
		result.append("]");
		return result.toString();
	}

}	// class DynamicAttributeVO
