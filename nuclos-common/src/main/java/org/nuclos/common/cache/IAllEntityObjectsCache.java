package org.nuclos.common.cache;

import java.util.Collection;

import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;

public interface IAllEntityObjectsCache<PK> extends IEntityObjectCache<PK> {

	void register(UID entity, boolean fillImmediate) throws CommonPermissionException;

	void register(UID entity, CollectableSearchExpression expr, boolean fillImmediate) throws CommonPermissionException;

	void refill(UID entity) throws CommonPermissionException;

	void invalidate(UID entity);
	
	void delete(UID entity, PK pk, boolean deleteInDb) throws CommonPermissionException;
	
	void add(EntityObjectVO<PK> instance, boolean addToDb)
			throws CommonPermissionException, CommonCreateException, CommonFinderException,
			CommonRemoveException, CommonStaleVersionException, CommonValidationException, 
			NuclosBusinessException;

	Collection<EntityObjectVO<PK>> getAll(UID entity) throws CommonPermissionException;

}
