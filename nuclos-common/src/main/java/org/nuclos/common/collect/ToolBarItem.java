//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.collect;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.nuclos.common2.StringUtils;

public class ToolBarItem implements Serializable {
	
	private static final long serialVersionUID = -4814428798709408034L;
	
	private static final Logger LOG = Logger.getLogger(ToolBarItem.class);
	
	/**
	 * Property key for storing ToolBarItem in component client properties 
	 */
	public static final String COMPONENT_PROPERTY_KEY = "nuclos_ToolBarItem";
	
	/**
	 * ACTION and TOGGLE_ACTION on ToolBar by default shows only icon.<br>
	 * If you want to show the text also, set this property to "true".<br>
	 * Value: <code>java.lang.String</code> --&gt; "true" or "false"
	 */
	public static final String PROPERTY_SHOW_TEXT = "showtext";
	
	public final Map<String, String> properties = new HashMap<String, String>();
	
	public static enum Type {
				
		/**
		 * group items: PopupButton in ToolBar, Submenu in Menu
		 */
		GROUP, 
		
		/**
		 * JSeparator in ToolBar, JPopupMenu.Separator in Menu
		 */
		SEPARATOR,
		
		/**
		 * JButton in ToolBar, JMenuItem in Menu
		 */
		ACTION, 
		
		/**
		 * JToggleButton in ToolBar, JMenuItem in Menu
		 */
		TOGGLE_ACTION,
		
		/**
		 * JComboBox in ToolBar and SubMenu with JMenuItems in Menu
		 * P = Pulldown
		 * S = SubMenu
		 * D = Default -&gt; MenuItem
		 */
		PSD_ACTION,
		
		/**
		 * JComboBox in ToolBar and JRadioButton in Menu
		 * P = Pulldown
		 * R = RadioButton
		 */
		PR_ACTION,
		
		/**
		 * JComboBox in ToolBar and SubMenu with JRadioButton in Menu
		 * P = Pulldown
		 * S = SubMenu
		 * R = RadioButton
		 */
		PSR_ACTION,
		
		/**
		 * JComboBox in ToolBar and hidden in Menu
		 */
		DROPDOWN_ACTION,
		
		/**
		 * DateChooser in ToolBar and Menu
		 */
//		DATECHOOSER,
		
		/**
		 * JCheckBox in ToolBar, JCheckBoxMenuItem in Menu
		 */
		CHECKBOX,
		
		/**
		 * JRadioButton in ToolBar, JRadioButtonMenuItem in Menu
		 */
		RADIOBUTTON,
		
		/**
		 * JTextField in ToolBar and hidden in Menu
		 */
		TEXTFIELD,
		
		/**
		 * ListOfValues in ToolBar and hidden in Menu
		 */
		LOV
	}
	
	private final Type type;
	private final String key;

	public ToolBarItem(Type type, String key) {
		super();
		if (type == null || key == null) {
			try {
				validateString(key);
			} catch (Exception ex) {
				throw new IllegalArgumentException(String.format("ToolBarItem [Type=%s, Key=%s]", type, key), ex);
			}
			throw new IllegalArgumentException(String.format("ToolBarItem [Type=%s, Key=%s]", type, key));
		}
		this.type = type;
		this.key = key;
	}
	
	public final Type getType() {
		return this.type;
	}

	public final String getKey() {
		return key;
	}

	@Override
	public int hashCode() {
		return key.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj == this) 
			return true;
		if (obj instanceof ToolBarItem) {
			return ((ToolBarItem) obj).getType() == type 
					&& ((ToolBarItem) obj).getKey().equals(key);
		}
		return super.equals(obj);
	}

	@Override
	public String toString() {
		return ToolBarItemParser.toString(this);
	}

	public String getProperty(String key) {
		return properties.get(key);
	}
	
	public void setProperty(String key, String value) {
		try {
			validateString(key);
			if (value == null) {
				properties.remove(key);
			} else {
				validateString(value);
				properties.put(key, value);
			}
		} catch (Exception ex) {
			throw new IllegalArgumentException(String.format("ToolBarItem.Property [Key=%s, Value=%s]", key, value), ex);
		}
	}
	
	public Map<String, String> getProperties() {
		return Collections.unmodifiableMap(properties);
	}
	
	public static void validateString(String s) {
		if (StringUtils.looksEmpty(s)) {
			throw new IllegalArgumentException("String must not be null");
		}
		if (!s.matches("[0-9a-zA-Z\\s_-]*")) {
			throw new IllegalArgumentException(String.format("Illegal character in string \"%s\"", s));
		}
	}
	
	public ToolBarItem clone() {
		ToolBarItem clone = new ToolBarItem(type, key);
		for (Entry<String, String> property : properties.entrySet()) {
			clone.setProperty(property.getKey(), property.getValue());
		}
		return clone;
	}
	
}
