//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.collect.collectable;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.prefs.Preferences;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.nuclos.common.NuclosDateTime;
import org.nuclos.common.NuclosImage;
import org.nuclos.common.NuclosPassword;
import org.nuclos.common.NuclosScript;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.exception.CollectableFieldValidationException;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.preferences.PreferencesUtils;
import org.nuclos.common2.DateUtils;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.RelativeDate;
import org.nuclos.common2.ServiceLocator;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.PreferencesException;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeRemote;


/**
 * Helper class for <code>CollectableField</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public abstract class CollectableUtils {

	private static final Logger log = Logger.getLogger(CollectableUtils.class);

	private static final String PREFS_KEY_FIELDTYPE = "fieldType";
	private static final String PREFS_KEY_VALUE = "value";
	private static final String PREFS_KEY_VALUEID = "valueId";

	/**
	 * validates the field type.
	 * @param clctf
	 * @param clctef
	 * @throws CollectableFieldValidationException if the field type of field differs from that of the entity field.
	 */
	public static void validateFieldType(CollectableField clctf, CollectableEntityField clctef)
			throws CollectableFieldValidationException {
		if (clctf.getFieldType() != clctef.getFieldType() && !SF.isEOFieldWithForceValueSearch(clctef.getEntityUID(), clctef.getUID())) {
			String msg;
			if(clctef.getFieldType() == CollectableEntityField.TYPE_VALUEFIELD)
				msg = SpringLocaleDelegate.getInstance().getMessage(
						"CollectableUtils.2","Das Feld \"{0}\" darf keine Id enthalten.", clctef.getLabel());
			else
				msg = SpringLocaleDelegate.getInstance().getMessage(
						"CollectableUtils.4","Das Feld \"{0}\" muss eine Id enthalten.", clctef.getLabel());
			throw new CollectableFieldValidationException(msg);
		}
	}

	/**
	 * validates the value's class.
	 *
	 * @param clctf
	 * @param clctef
	 * @throws CollectableFieldValidationException
	 */
	public static void validateValueClass(CollectableField clctf, CollectableEntityField clctef) throws CollectableFieldValidationException {
		// check value/class:
		final Object oValue = clctf.getValue();
		if (oValue != null) {
			final Class<?> clsValue = oValue.getClass();
			final Class<?> clsEntity = clctef.getJavaClass();
			if(InternalTimestamp.class.equals(clsEntity) && Date.class.isAssignableFrom(clsValue)) {
				// it is okay!
				return;
			}
			if(clctef.isIdField() && clctf.isIdField()) {
				// it is okay!
				return;
			}
			//NUCLEUSINT-1142
			if (!clsEntity.isAssignableFrom(clsValue) && !NuclosPassword.class.equals(clsEntity)) {
				String msg = SpringLocaleDelegate.getInstance().getMessage(
						"CollectableUtils.5","Der Wert \"{0}\" hat nicht den f\u00fcr das Feld \"{1}\" vorgeschriebenen Datentyp {2}, sondern den Datentyp {3}.", oValue, clctef.getLabel(), clsEntity.getName(), clsValue.getName());
				throw new CollectableFieldValidationException(msg);
			}
		}

	}

	public static void putCollectableField(Preferences prefs, String sNodeName, CollectableField clctf) throws PreferencesException {
		final Preferences prefsChild = prefs.node(sNodeName);
		final int iFieldType = clctf.getFieldType();
		prefsChild.putInt(PREFS_KEY_FIELDTYPE, iFieldType);
		PreferencesUtils.putSerializable(prefs, PREFS_KEY_VALUE, clctf.getValue());
		if (iFieldType == CollectableField.TYPE_VALUEIDFIELD) {
			PreferencesUtils.putSerializable(prefs, PREFS_KEY_VALUEID, clctf.getValueId());
		}
	}

	/**
	 * @param prefs
	 * @param sNodeName
	 * @return the <code>CollectableField</code> stored in <code>prefs.node(sNodeName)</code>, if any.
	 * @throws PreferencesException
	 */
	public static CollectableField getCollectableField(Preferences prefs, String sNodeName) throws PreferencesException {
		final CollectableField result;
		if (!PreferencesUtils.nodeExists(prefs, sNodeName)) {
			result = null;
		}
		else {
			final Preferences prefsChild = prefs.node(sNodeName);
			final int iFieldType = prefsChild.getInt(PREFS_KEY_FIELDTYPE, CollectableField.TYPE_UNDEFINED);
			switch (iFieldType) {
				case CollectableField.TYPE_UNDEFINED: {
					result = null;
					break;
				}
				case CollectableField.TYPE_VALUEFIELD: {
					final Object oValue = PreferencesUtils.getSerializable(prefs, PREFS_KEY_VALUE);
					result = new CollectableValueField(oValue);
					break;
				}
				case CollectableField.TYPE_VALUEIDFIELD: {
					final Object oValue = PreferencesUtils.getSerializable(prefs, PREFS_KEY_VALUE);
					final Object oValueId = PreferencesUtils.getSerializable(prefs, PREFS_KEY_VALUEID);
					result = new CollectableValueIdField(oValueId, oValue);
					break;
				}
				default:
					throw new CommonFatalException(SpringLocaleDelegate.getInstance().getMessage(
							"CollectableUtils.11","Unbekannter Feldtyp: {0}", iFieldType));
			}
		}
		return result;
	}

	/**
	 * @param clcte
	 * @return List&lt;CollectableEntityField&gt;. Contains all of the 
	 * 		fields contained in <code>clcte</code>.
	 */
	public static List<CollectableEntityField> getCollectableEntityFields(CollectableEntity clcte) {
		return getCollectableEntityFieldsFromFieldUids(clcte, clcte.getFieldUIDs());
	}

	/**
	 * @param clcte
	 * @param collFieldUIDs Collection&lt;String&gt;
	 * @return List&lt;CollectableEntityField&gt; the <code>CollectableEntityField
	 * 		</code>s from the given <code>CollectableEntity</code> and the 
	 * 		given collection of field names.
	 */
	public static List<CollectableEntityField> getCollectableEntityFieldsFromFieldUids(CollectableEntity clcte, Collection<UID> collFieldUIDs) {
		return CollectionUtils.transform(collFieldUIDs, new CollectableEntity.GetEntityField(clcte));
	}

	/**
	 * @param collclctef Collection&lt;CollectableEntityField&gt;
	 * @return List&lt;String&gt; the field names from the <code>CollectableEntityField</code>s.
	 *
	 * @deprecated Not really usefull. Use transform directly.
	 */
	public static List<UID> getFieldUidsFromCollectableEntityFields(Collection<? extends CollectableEntityField> collclctef) {
		return CollectionUtils.transform(collclctef, new CollectableEntityField.GetUID());
	}

	/**
	 * creates a copy of the given CollectableField.
	 * The result's field type (value/valueId) is the same as the given entity field's type.
	 * <ul>
	 *   <li>If the field types of the target and the source match, a direct copy is returned.</li>
	 *   <li>If the source is a value id field, the target is a value field, only the value component is copied.</li>
	 *   <li>If the source is a value field, the target is a value id field:  If <code>allowSettingTargetIdToNull</code> is true,
	 *     only the value component is copied and the id component of the target is set to null.
	 *     If <code>allowSettingTargetIdToNull</code> is false, this method returns null.
	 * </ul>
	 * @param clctfSource the source collectable
	 * @param clctefTarget the field type of the result.
	 * @param allowSettingTargetIdToNull See above. Be careful when setting this to true.
	 */
	public static CollectableField copyCollectableField(CollectableField clctfSource, CollectableEntityField clctefTarget,
			boolean allowSettingTargetIdToNull) {
		final Object oValue = clctfSource.getValue();
		final CollectableField result;
		if (clctefTarget.isIdField()) {
			if (clctfSource.isIdField()) {
				// As CollectableFields are immutable, we may just return the source object here:
				result = clctfSource;
			}
			else {
				if (allowSettingTargetIdToNull) {
					result = new CollectableValueIdField(null, oValue);
				} else {
					result = null;
				}
			}
		}
		else {
			result = new CollectableValueField(oValue);
		}
		return result;
	}

	/**
	 * §postcondition result != null
	 * §postcondition result.isNull()
	 * §postcondition result.getFieldType() == iFieldType
	 * 
	 * @param iFieldType
	 * @return a null field for the given field type
	 */
	public static CollectableField getNullField(int iFieldType, boolean isLocalized) {
		final CollectableField result;
		switch (iFieldType) {
			case CollectableEntityField.TYPE_VALUEFIELD:
				result = CollectableValueField.NULL;
				break;
			case CollectableEntityField.TYPE_VALUEIDFIELD:
				result = CollectableValueIdField.NULL;
				break;
			default:
				throw new IllegalStateException(SpringLocaleDelegate.getInstance().getMessage(
						"CollectableUtils.9","Invalid fieldtype: {0}", iFieldType));
		}
		assert result != null;
		assert result.isNull();
		assert result.getFieldType() == iFieldType;

		return result;
	}

	/**
	 * §postcondition result != null
	 * §postcondition result.isNull()
	 * §postcondition result.getFieldType() == clctef.getFieldType()
	 * 
	 * @param clctef
	 * @return a null field for the given entity field
	 */
	public static CollectableField getNullField(CollectableEntityField clctef) {
		final CollectableField result = getNullField(clctef.getFieldType(), clctef.isLocalized());
		assert result != null;
		assert result.isNull();
		assert result.getFieldType() == clctef.getFieldType();
		return result;
	}

	/**
	 * §postcondition result != null
	 * §postcondition result.isIdField() &lt;--&gt; clctef.isIdField()
	 * §postcondition result.getValue() == oValue
	 * §postcondition clctef.isIdField() --&gt; result.getValueId() == null
	 * 
	 * @param clctef
	 * @param oValue
	 * @return a new CollectableField containing the given value.
	 */
	public static CollectableField newCollectableFieldForValue(CollectableEntityField clctef, Object oValue) {
		final CollectableField result = clctef.isIdField() ? (CollectableField) new CollectableValueIdField(null, oValue) : new CollectableValueField(oValue);

		assert result != null;
		assert result.isIdField() == clctef.isIdField();
		assert result.getValue() == oValue;
		assert !clctef.isIdField() || result.getValueId() == null;

		return result;
	}
	
	public static CollectableField newCollectableValueIdFieldFromLong(CollectableEntityField clctef, Long valueId, Object value) {
		if (!clctef.isIdField()) {
			throw new CommonFatalException("Kein ValueId Feld");
		}

		final CollectableField result = new CollectableValueIdField(valueId, value);

		assert result != null;
		assert result.isIdField() == clctef.isIdField();
		assert result.getValueId() == valueId;

		return result;
	}

	public static CollectableField newCollectableValueIdFieldFromUID(CollectableEntityField clctef, UID valueId, Object value) {
		if (!clctef.isIdField()) {
			throw new CommonFatalException("Kein ValueId Feld");
		}

		final CollectableField result = new CollectableValueIdField(valueId, value);

		assert result != null;
		assert result.isIdField() == clctef.isIdField();
		assert result.getValueId() == valueId;

		return result;
	}

	public static CollectableField newCollectableValueIdFieldFromKey(CollectableEntityField clctef, Object valueId, Object value) {
		if (!clctef.isIdField()) {
			throw new CommonFatalException("Kein ValueId Feld");
		}

		final CollectableField result = new CollectableValueIdField(valueId, value);

		assert result != null;
		assert result.isIdField() == clctef.isIdField();
		assert result.getValueId() == valueId;

		return result;
	}

	/**
	 * @deprecated Use one of the newCollectableValueIdFieldFrom* variants.
	 */
	public static CollectableField newCollectableValueIdField(CollectableEntityField clctef, Integer valueId, Object value) {
		if (!clctef.isIdField()) {
			throw new CommonFatalException("Kein ValueId Feld");
		}

		final CollectableField result = new CollectableValueIdField(valueId, value);

		assert result != null;
		assert result.isIdField() == clctef.isIdField();
		assert result.getValueId() == valueId;

		return result;
	}

	public static CollectableField newCollectableValueIdFieldForLong(CollectableEntityField clctef, Long valueId) {
		return newCollectableValueIdFieldFromLong(clctef, valueId, null);
	}
	
	public static CollectableField newCollectableValueIdFieldForUID(CollectableEntityField clctef, UID valueId) {
		return newCollectableValueIdFieldFromUID(clctef, valueId, null);
	}
	
	public static CollectableField newCollectableValueIdFieldForKey(CollectableEntityField clctef, Object valueId) {
		return newCollectableValueIdFieldFromKey(clctef, valueId, null);
	}
	
	/**
	 * @deprecated Use one of the other newCollectableValueIdFieldFor* variants.
	 */
	public static CollectableField newCollectableValueIdFieldForValueId(CollectableEntityField clctef, Integer valueId) {
		return newCollectableValueIdField(clctef, valueId, null);
	}
	
	public static CollectableField newCollectableValueIdField(CollectableEntityField clctef, Long valueId, Object value) {
		if (!clctef.isIdField()) {
			throw new CommonFatalException("Kein ValueId Feld");
		}

		final CollectableField result = new CollectableValueIdField(valueId, value);

		assert result != null;
		assert result.isIdField() == clctef.isIdField();
		assert result.getValueId() == valueId;

		return result;
	}
	
	public static CollectableField newCollectableValueIdFieldForValueId(CollectableEntityField clctef, UID uid) {
		return newCollectableValueIdField(clctef, uid, null);
	}
	
	public static CollectableField newCollectableValueIdField(CollectableEntityField clctef, UID uid, Object value) {
		if (!clctef.isIdField()) {
			throw new CommonFatalException("Kein ValueId Feld");
		}

		final CollectableField result = new CollectableValueIdField(uid, value);

		assert result != null;
		assert result.isIdField() == clctef.isIdField();
		assert result.getValueId() == uid;

		return result;
	}

	public static CollectableField newCollectableValueIdFieldForValueId(CollectableEntityField clctef, Long valueId) {
		return newCollectableValueIdField(clctef, valueId, null);
	}


	/**
	 * sets all fields in the given <code>Collectable</code> to their 
	 * respective default values (according to the given <code>CollectableEntity</code>).
	 * 
	 * §precondition clct != null
	 * §precondition clct.isComplete()
	 * §precondition clcte != null
	 * 
	 * @param clct the <code>Collectable</code> to be changed.
	 * @param clcte the <code>CollectableEntity</code> containing the required information about the fields in <code>clct</code>.
	 */
	public static <PK> void setDefaultValues(Collectable<PK> clct, CollectableEntity clcte) {
		final MasterDataFacadeRemote mdFacade = ServiceLocator.getInstance().getFacade(MasterDataFacadeRemote.class);
		for (UID field : clcte.getFieldUIDs()) {
			final CollectableField clctf = clcte.getEntityField(field).getDefault();
			if (clctf.getValue() != null && clctf.getValue().toString().equals(RelativeDate.today().toString())) {
				clct.setField(field, new CollectableValueField(DateUtils.today()));
			}
			else if (clcte.getEntityField(field).getJavaClass() == Boolean.class &&
				clctf.getValue() == null) {
				//null default values for boolean fields
			}
			else {
				CollectableField cf = clctf;
				if (clcte.getEntityField(field).isReferencing() && !cf.isNull())
					try {
						mdFacade.get(clcte.getEntityField(field).getReferencedEntityUID(), ((CollectableValueIdField)clctf).getValueId());
					} catch (Exception e) {
						log.warn("referenced entity object not found for " + field);
						cf = clcte.getEntityField(field).getNullField();
					}				
				clct.setField(field, cf);
			}
		}
	}

	/**
	 * §precondition collclct != null
	 * §precondition sFieldName != null
	 * 
	 * @param collclct
	 * @param field
	 * @return the common value (if any) of the field with the given name, in all Collectables in the given collection.
	 *         <code>null</code> if there is no common value.
	 */
	public static CollectableField getCommonValue(Collection<? extends Collectable> collclct, UID field) {
		CollectableField result = null;
		for (Collectable clct : collclct) {
			final CollectableField clctf = clct.getField(field);
			if (result == null) {
				result = clctf;
			}
			else {
				if (!result.equals(clctf)) {
					result = null;
					break;
				}
			}
		}
		return result;
	}

	public static int getCollectableComponentTypeForClass(Class<?> clazz) {
		if (clazz.equals(Boolean.class)) {
			return CollectableComponentTypes.TYPE_CHECKBOX;
		} else if (clazz.equals(Date.class) || clazz.equals(InternalTimestamp.class) || clazz.equals(NuclosDateTime.class)) {
			return CollectableComponentTypes.TYPE_DATECHOOSER;
		} else if (org.nuclos.common2.File.class.isAssignableFrom(clazz)) {
			return CollectableComponentTypes.TYPE_FILECHOOSER;
		} else if(clazz.equals(NuclosImage.class)) {
			return CollectableComponentTypes.TYPE_IMAGE;
		} else if(clazz.equals(NuclosPassword.class)) {
			//NUCLEUSINT-1142
			return CollectableComponentTypes.TYPE_PASSWORDFIELD;
		} else if(clazz.equals(NuclosScript.class)) {
			return CollectableComponentTypes.TYPE_SCRIPT;
		} else {
			return CollectableComponentTypes.TYPE_TEXTFIELD;
		}
	}

	/**
	 * §postcondition result != null
	 */
	public static CollectableField findCollectableFieldByValue(List<CollectableField> lstclctfValues, CollectableField clctfSource) {
		CollectableField result = null;
		for (CollectableField clctfValue : lstclctfValues) {
			if (clctfSource.getValue().equals(clctfValue.getValue())) {
				result = clctfValue;
				break;
			}
		}
		if (result == null) {
			throw new NoSuchElementException(clctfSource.toString());
		}
		assert result != null;
		return result;
	}

	public static String formatFieldExpression(String expr, final Collectable clct) {
		if (expr == null || expr.isEmpty()) {
			return "Empty field expression";
		}
		final Transformer<String, String> paramTransformer = new Transformer<String, String>() {
			@Override public String transform(String param) {
				String result = null;

				// Try to find and replace all UIDs
				List<UID> uids = UID.parseAllUIDs(param);
				if (!uids.isEmpty()) {
					result = param;
					for (UID uid : uids) {
						CollectableField field = clct.getField(uid);
						if (field != null) {
							result = result.replaceAll("(?:uid\\{)?" + Pattern.quote(uid.toString()) + "(?:\\})?", field.toString());
						}
					}
				}

				if (result == null) {
					// TODO: Is this code block ever reached?
					// TODO: What if the string contains multiple params?
					result = "${" + param + "}";
				}

				return result;
			}
		};
		if (!expr.contains("${")) {
			return paramTransformer.transform(expr);
		} else {
			return StringUtils.replaceParameters(expr,paramTransformer);
		}
	}

	/**
	 * Comparator for sorting collectable fields along a given string list
	 */
	public static class GivenFieldOrderComparator implements Comparator<CollectableEntityField> {
		
		private final List<UID> lstFieldNames;

		public GivenFieldOrderComparator(List<UID> lstFieldNames) {
			this.lstFieldNames = lstFieldNames;
		}

		@Override
        public int compare(CollectableEntityField clctef1, CollectableEntityField clctef2) {
			final Integer pos1 = (lstFieldNames.indexOf(clctef1.getUID()) == -1) ? Integer.MAX_VALUE : lstFieldNames.indexOf(clctef1.getUID());
			final Integer pos2 = (lstFieldNames.indexOf(clctef2.getUID()) == -1) ? Integer.MAX_VALUE : lstFieldNames.indexOf(clctef2.getUID());
			return pos1.compareTo(pos2);
		}
	}

}	// class CollectableUtils
