//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.collect.collectable.searchcondition;

import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.searchcondition.visit.AtomicVisitor;
import org.nuclos.common.collect.collectable.searchcondition.visit.Visitor;

/**
 * Atomic collectable search condition. This class and its subclasses are immutable.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version	01.00.00
 */
public abstract class AtomicCollectableSearchCondition extends AbstractCollectableSearchCondition {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3980284384022839937L;

	private CollectableEntityField clctef;
	
	private ComparisonOperator compop;

	AtomicCollectableSearchCondition(CollectableEntityField clctef, ComparisonOperator compop) {
		if (clctef == null) {
			throw new NullPointerException("clctef");
		}
		if (compop == null) {
			throw new NullPointerException("compop");
		}
		if (compop == ComparisonOperator.NONE) {
			throw new IllegalArgumentException("compop");
		}
		if (clctef.getEntityUID() == null) {
			throw new NullPointerException("clctef.getEntityUID()");
		}
		this.clctef = clctef;
		this.compop = compop;
	}

	/**
	 * @deprecated Don't use this constant in new applications.
	 */
	@Override
	public int getType() {
		return TYPE_ATOMIC;
	}

	public CollectableEntityField getEntityField() {
		return clctef;
	}

	public UID getFieldUID() {
		return clctef.getUID();
	}

	public String getFieldLabel() {
		return clctef.getLabel() != null ? clctef.getLabel() : clctef.getUID().getString();
	}

	public ComparisonOperator getComparisonOperator() {
		return compop;
	}

	@Override
	public boolean isSyntacticallyCorrect() {
		return true;
	}

	/**
	 * §precondition this.getComparisonOperator().getOperandCount() &gt; 1
	 * §todo clarify the purpose/usage of this method!
	 * 
	 * @return the comparand of this search condition (if any) as <code>String</code>.
	 * Especially useful to display the comparand in text fields.
	 */
	public abstract String getComparandAsString();

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof AtomicCollectableSearchCondition))
			return false;
		final AtomicCollectableSearchCondition that = (AtomicCollectableSearchCondition) o;

		return clctef.equals(that.clctef) && compop.equals(that.compop);
	}

	@Override
	public int hashCode() {
		return clctef.hashCode() ^ compop.hashCode();
	}


	@Override
	public <O, Ex extends Exception> O accept(Visitor<O, Ex> visitor) throws Ex {
		return visitor.visitAtomicCondition(this);
	}

	/**
	 * dispatch method for Visitor pattern.
	 * For a description of the Visitor pattern, see the "GoF" Patterns book.
	 * 
	 * §precondition visitor != null
	 * 
	 * @param visitor
	 * @return the result of the visitor's respective method.
	 */
	public abstract <O, Ex extends Exception> O accept(AtomicVisitor<O, Ex> visitor) throws Ex;

	@Override
	public String toString() {
		return getClass().getName() + ":" + getConditionName() + ":" + compop + ":" + clctef;
	}
	
	protected static void appendConditionInfo(AtomicCollectableSearchCondition c, StringBuilder sb) {
		sb.append(':');
		sb.append(c.getComparisonOperator());
		sb.append(':');
		//TODO: The entity this missing for full reconstruction of the field
		sb.append(c.getEntityField().getName());

	}
	
	public boolean compareEntity(UID entity) {
		if (entity == null) {
			return false;
		}
		
		return entity.equals(clctef.getEntityUID());
	}
	
	public boolean isComparandAnId() {
		return false; //TODO: Make abstract and implement in all subclasses.
	}
	
	public boolean isForeignKeyComparision() {
		return false;
	}
			
}  // class AtomicCollectableSearchCondition
