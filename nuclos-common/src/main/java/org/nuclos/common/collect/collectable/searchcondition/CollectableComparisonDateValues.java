//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.common.collect.collectable.searchcondition;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.NullArgumentException;
import org.nuclos.common.NuclosDateTime;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableFieldFormat;
import org.nuclos.common.collect.collectable.searchcondition.visit.AtomicVisitor;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.RelativeDate;
import org.nuclos.common2.SpringLocaleDelegate;

/**
 * A comparison with a parameter.
 */
public final class CollectableComparisonDateValues extends AtomicCollectableSearchCondition {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2836612730272899414L;

	public static class ComparisonDateValues implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = -3802182511960424807L;
		private final Date fromDate;
		private final Date toDate;

		public ComparisonDateValues(Date fromDate, Date toDate) {
			this.fromDate = fromDate;
			this.toDate = toDate;
		}
		
		public Date getFromDate() {
			return fromDate;
		}
		
		public Date getToDate() {
			return toDate;
		}
		
		public String getInternalName() {
			return (fromDate == null ? "" : fromDate == RelativeDate.today() ? fromDate.toString() : fromDate.getTime()) + ";"
					+ (toDate == null ? "" : toDate == RelativeDate.today() ? toDate.toString() : toDate.getTime());
		}
		
		@Override
		public String toString() {
			String result = "";
			if (fromDate != null) {
				result += SpringLocaleDelegate.getInstance().getResource("CollectableComparisonDateValues.1", "von") + " " + CollectableFieldFormat.getInstance(Date.class).format(null, fromDate);
			}
			if (fromDate != null && toDate != null)
				result += " " + SpringLocaleDelegate.getInstance().getResource("logicalOperator.AND.label", "UND") + " ";
				
			if (toDate != null) {
				result += SpringLocaleDelegate.getInstance().getResource("CollectableComparisonDateValues.2", "bis") + " " + CollectableFieldFormat.getInstance(Date.class).format(null, toDate);
			}
				
			return result;
		}
		
		public static ComparisonDateValues parse(String s) {
			final String[] sa = s.split(";");
			final Date fromDate = sa[0].equals("") ? null : sa[0].equals(RelativeDate.today().toString()) ? RelativeDate.today() : new Date(Long.parseLong(sa[0]));
			final Date toDate = sa[1].equals("") ? null : sa[1].equals(RelativeDate.today().toString()) ? RelativeDate.today() : new Date(Long.parseLong(sa[1]));
			return new ComparisonDateValues(fromDate, toDate);
		}
		
		public static boolean canCompareBetweenValues(CollectableEntityField clctef) {
			if (clctef == null)
				return false;
			
			if (clctef.getJavaClass() != Date.class && clctef.getJavaClass() != InternalTimestamp.class && clctef.getJavaClass() != NuclosDateTime.class) {
				return false; // currently only dates allowed.	
			}
			return true;
		}
	}
	
	private final ComparisonDateValues dateValues;

	public CollectableComparisonDateValues(CollectableEntityField clctef, ComparisonOperator compop, ComparisonDateValues dateValues) {
		super(clctef, compop);
		if (compop.getOperandCount() != 2) {
			throw new IllegalArgumentException("compop: " + compop);
		}
		if (dateValues == null) {
			throw new NullArgumentException("dateValues");
		}
		if (!ComparisonDateValues.canCompareBetweenValues(clctef)) {
			throw new IllegalArgumentException("datatypes don't match - cannot compare " +
				clctef.getJavaClass().getName() + " with " + dateValues);
		}
		
		this.dateValues = dateValues;
	}

	public ComparisonDateValues getDateValues() {
		return dateValues;
	}
	
	@Override
	public String getComparandAsString() {
		// null is valid here (see e.g. CollectableComparisonWithOtherField),
		// but maybe parameter.getInternalName() would be a better choice??
		return null;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof CollectableComparisonDateValues)) {
			return false;
		}
		final CollectableComparisonDateValues that = (CollectableComparisonDateValues) o;

		return super.equals(that) && this.dateValues.equals(that.dateValues);
	}

	@Override
	public int hashCode() {
		return super.hashCode() ^ this.dateValues.hashCode();
	}

	@Override
	public <O, Ex extends Exception> O accept(AtomicVisitor<O, Ex> visitor) throws Ex {
		return visitor.visitComparisonDateValues(this);
	}

	@Override
	public String toString() {
		return getClass().getName() + ":" + getConditionName() + ":" + getComparisonOperator() + 
			":" + getEntityField() + ":" + dateValues;
	}
	
}  // class CollectableComparisonWithParameter
