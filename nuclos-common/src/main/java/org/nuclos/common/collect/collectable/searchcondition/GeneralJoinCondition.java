//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.collect.collectable.searchcondition;

import org.nuclos.common.DbField;
import org.nuclos.common.collect.collectable.searchcondition.visit.Visitor;

/**
 * General 
 * <p>
 * The use of this condition results in a (left equi) join between the referenced
 * tables/entities. 
 * </p>
 * @author Thomas Pasch
 * @since Nuclos 4.1
 * 
 * @see org.nuclos.server.dal.processor.jdbc.impl.EOSearchExpressionUnparser.UnparseVisitor
 * @see org.nuclos.server.dal.processor.ColumnToRefFieldVOMapping
 */
public final class GeneralJoinCondition extends AbstractCollectableSearchCondition {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2326708969189346677L;

	private final DbField<?> field1;

	private final String tableAlias1;
	
	private final DbField<?> field2;

	private final String tableAlias2;
	
	/**
	 * @param sSubEntityName
	 * @param sForeignKeyFieldName
	 * @param condSub
	 * @precondition sSubEntityName != null
	 * @precondition sForeignKeyFieldName != null
	 */
	public GeneralJoinCondition(DbField<?> boFieldAlreadyJoined, String tableAliasAlreadyJoined, DbField<?> boFieldToJoin, String tableAliasToJoin) {
		if (boFieldAlreadyJoined == null || tableAliasAlreadyJoined == null || boFieldToJoin == null || tableAliasToJoin == null) {
			throw new NullPointerException();
		}
		this.field1 = boFieldAlreadyJoined;
		this.tableAlias1 = tableAliasAlreadyJoined;
		this.field2 = boFieldToJoin;
		this.tableAlias2 = tableAliasToJoin;
	}

	public DbField<?> getField1() {
		return field1;
	}
	
	public String getTableAlias1() {
		return tableAlias1;
	}

	public DbField<?> getField2() {
		return field2;
	}
	
	public String getTableAlias2() {
		return tableAlias2;
	}

	/**
	 * @deprecated Don't use this constant in new applications.
	 */
	@Override
	public int getType() {
		return GENERAL_JOIN;
	}

	@Override
	public boolean isSyntacticallyCorrect() {
		// ???
		return true;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof GeneralJoinCondition)) {
			return false;
		}
		final GeneralJoinCondition that = (GeneralJoinCondition) o;
		return field1.equals(that.getField1()) && tableAlias1.equals(that.getTableAlias1())
				&& field2.equals(that.getField2()) && tableAlias2.equals(that.getTableAlias2());
	}

	@Override
	public int hashCode() {
		return field1.hashCode() + field2.hashCode() + 93731;
	}

	@Override
	public <O, Ex extends Exception> O accept(Visitor<O, Ex> visitor) throws Ex {
		return visitor.visitGeneralJoinCondition(this);
	}

	@Override
	public String toString() {
		return getClass().getName() + ":" + getConditionName() + ":" + field1 + "<->" + tableAlias1 + 
				":" + field2 + "<->" + tableAlias2;
	}
	
}  // class RefJoinCondition
