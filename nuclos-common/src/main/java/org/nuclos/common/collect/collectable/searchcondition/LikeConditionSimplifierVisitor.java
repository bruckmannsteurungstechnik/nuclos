package org.nuclos.common.collect.collectable.searchcondition;

import org.nuclos.common.collect.collectable.searchcondition.visit.Visitor;

/**
 * Created by Sebastian Debring on 3/25/2019.
 */
public class LikeConditionSimplifierVisitor implements Visitor<CollectableSearchCondition, RuntimeException> {

	@Override
	public CollectableSearchCondition visitTrueCondition(final TrueCondition truecond) throws RuntimeException {
		return truecond;
	}

	@Override
	public CollectableSearchCondition visitAtomicCondition(final AtomicCollectableSearchCondition atomiccond) throws RuntimeException {
		if (atomiccond instanceof CollectableLikeCondition) {
			CollectableLikeCondition origCond = (CollectableLikeCondition) atomiccond;
			CollectableLikeCondition newCond = new CollectableLikeCondition(origCond.getEntityField(), origCond.getLikeComparand().replaceAll("(\\*)\\1+", "$1"));
			return newCond;
		}
		return atomiccond;
	}

	@Override
	public CollectableSearchCondition visitCompositeCondition(final CompositeCollectableSearchCondition compositecond) throws RuntimeException {
		return compositecond;
	}

	@Override
	public CollectableSearchCondition visitIdCondition(final CollectableIdCondition idcond) throws RuntimeException {
		return idcond;
	}

	@Override
	public CollectableSearchCondition visitIdListCondition(final CollectableIdListCondition collectableIdListCondition) throws RuntimeException {
		return collectableIdListCondition;
	}

	@Override
	public <T> CollectableSearchCondition visitInCondition(final CollectableInCondition<T> collectableInCondition) throws RuntimeException {
		return collectableInCondition;
	}

	@Override
	public CollectableSearchCondition visitSubCondition(final CollectableSubCondition subcond) throws RuntimeException {
		return subcond;
	}

	@Override
	public CollectableSearchCondition visitRefJoinCondition(final RefJoinCondition joincond) throws RuntimeException {
		return joincond;
	}

	@Override
	public CollectableSearchCondition visitGeneralJoinCondition(final GeneralJoinCondition joincond) throws RuntimeException {
		return joincond;
	}

	@Override
	public CollectableSearchCondition visitReferencingCondition(final ReferencingCollectableSearchCondition refcond) throws RuntimeException {
		return refcond;
	}
}
