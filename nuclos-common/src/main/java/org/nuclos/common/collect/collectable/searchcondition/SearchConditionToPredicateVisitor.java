package org.nuclos.common.collect.collectable.searchcondition;

import java.util.Collection;
import java.util.Comparator;

import org.apache.commons.lang.ObjectUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.searchcondition.visit.AtomicVisitor;
import org.nuclos.common.collect.collectable.searchcondition.visit.CompositeVisitor;
import org.nuclos.common.collect.collectable.searchcondition.visit.Visitor;
import org.nuclos.common.collection.BinaryPredicate;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.collection.PredicateUtils;
import org.nuclos.common.collection.SymmetricBinaryPredicate;
import org.nuclos.common.collection.Transformer;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

public class SearchConditionToPredicateVisitor implements Visitor<Predicate<MasterDataVO<UID>>, RuntimeException>, CompositeVisitor<Predicate<MasterDataVO<UID>>, RuntimeException>, Transformer<CollectableSearchCondition, Predicate<MasterDataVO<UID>>> {

	private static class FieldPredicate<T> implements Predicate<MasterDataVO<UID>> {

		protected final UID	fieldUID;
		protected final Class<T> cls;
		protected final Predicate<? super T>	predicate;

		public FieldPredicate(UID fieldUID, Class<T> cls, Predicate<? super T> predicate) {
			this.fieldUID = fieldUID;
			this.cls = cls;
			this.predicate = predicate;
		}

		@Override
		public boolean evaluate(MasterDataVO<UID> mdvo) {
			return predicate.evaluate(mdvo.getFieldValue(fieldUID, cls));
		}
	}

	static class ComparisonOperatorPredicate<T> implements SymmetricBinaryPredicate<T> {

		protected final ComparisonOperator operator;
		protected final Class<T> clazz;
		protected final Comparator<? super T> comparator;

		public ComparisonOperatorPredicate(ComparisonOperator operator, Class<T> clazz, Comparator<? super T> comparator) {
			this.operator = operator;
			this.clazz = clazz;
			this.comparator = comparator;
		}

		@Override
		public boolean evaluate(T t1, T t2) {
			if (t1 == null || t2 == null)
				return false;
			switch (operator) {
			case EQUAL:
				return t1.equals(t2);
			case NOT_EQUAL:
				return !t1.equals(t2);
			case GREATER:
				return compare(t1, t2, comparator) > 0;
			case GREATER_OR_EQUAL:
				return compare(t1, t2, comparator) >= 0;
			case LESS:
				return compare(t1, t2, comparator) < 0;
			case LESS_OR_EQUAL:
				return compare(t1, t2, comparator) <= 0;
			}
			throw new IllegalStateException("Invalid comparison operator " + operator);
		}

		private static <T> int compare(T t1, T t2, Comparator<? super T> comparator) {
			return (comparator != null) ? comparator.compare(t1, t2) : ((Comparable<? super T>) t1).compareTo(t2);
		}
	}
	
	private static class AlwaysFalsePredicate implements Predicate<MasterDataVO<UID>> {
		@Override
		public boolean evaluate(MasterDataVO<UID> t) {
			return false;
		}
	}

	private static class FieldIdPredicate implements Predicate<MasterDataVO<UID>> {

		private final Object id;

		public FieldIdPredicate(Object id) {
			this.id = id;
		}

		@Override
		public boolean evaluate(MasterDataVO<UID> mdvo) {
			return ObjectUtils.equals(mdvo.getId(), id);
		}
	}

	private static class FieldIdListPredicate implements Predicate<MasterDataVO<UID>> {

		private final Collection<? extends Object> ids;

		public FieldIdListPredicate(Collection<? extends Object> ids) {
			this.ids = ids;
		}

		@Override
		public boolean evaluate(MasterDataVO<UID> mdvo) {
			return ids.contains(mdvo.getId());
		}
	}
	
	private static class FieldInPredicate implements Predicate<MasterDataVO<UID>> {

		private final UID	fieldUID;
		private final Collection<?> values;

		public FieldInPredicate(UID fieldUID, Collection<?> values) {
			this.fieldUID = fieldUID;
			this.values = values;
		}

		@Override
		public boolean evaluate(MasterDataVO<UID> mdvo) {
			return values.contains(mdvo.getFieldValue(fieldUID));
		}
	}

	static class AtomicConditionToPredicateVisitor implements AtomicVisitor<Predicate<MasterDataVO<UID>>, RuntimeException>{

		@Override
		public Predicate<MasterDataVO<UID>> visitComparison(CollectableComparison comparison) {
			CollectableEntityField entityField = comparison.getEntityField();
			CollectableField comparand = comparison.getComparand();
			final Object id = comparand.isIdField() ? comparand.getValueId() : null;
			if (id != null) { /* && operator == EQUALS, but the SQL unparser doesn't check either */
				return new FieldPredicate(entityField.getUID(), Object.class, PredicateUtils.isEqual(id));
			} else {
				if (comparand.getValue() == null) {
					// We're are simulating SQL semantics where NULL comparisons yield always false.
					return PredicateUtils.alwaysFalse();
				}
				Class<Object> javaClass = (Class<Object>) entityField.getJavaClass();
				BinaryPredicate<Object,Object> pred = 
						new ComparisonOperatorPredicate<Object>(comparison.getComparisonOperator(), javaClass, null);
				return new FieldPredicate<Object>(entityField.getUID(), javaClass, PredicateUtils.bindSecond(pred,comparand.getValue()));
			}
		}

		@Override
		public Predicate<MasterDataVO<UID>> visitComparisonWithParameter(CollectableComparisonWithParameter comparisonwp) {
			return new AlwaysFalsePredicate();//throw new UnsupportedOperationException();
		}
		@Override
		public Predicate<MasterDataVO<UID>> visitComparisonDateValues(CollectableComparisonDateValues comparisondv) throws RuntimeException {
			return new AlwaysFalsePredicate();//throw new UnsupportedOperationException();
		}

		@Override
		public Predicate<MasterDataVO<UID>> visitComparisonWithOtherField(CollectableComparisonWithOtherField comparisonwf) {
			return new AlwaysFalsePredicate();//throw new UnsupportedOperationException();
		}
		
		@Override
		public <T> Predicate<MasterDataVO<UID>> visitInCondition(CollectableInCondition<T> comparisonwf) {
			return new AlwaysFalsePredicate();//throw new UnsupportedOperationException();
		}

		@Override
		public Predicate<MasterDataVO<UID>> visitIsNullCondition(CollectableIsNullCondition isnullcond) {
			return new FieldPredicate<Object>(isnullcond.getFieldUID(), Object.class, PredicateUtils.isNull());
		}

		@Override
		public Predicate<MasterDataVO<UID>> visitLikeCondition(CollectableLikeCondition likecond) {
			return new FieldPredicate<String>(likecond.getFieldUID(), String.class, PredicateUtils.wildcardFilterList(likecond.getComparandAsString().toLowerCase())) {
				@Override
				public boolean evaluate(MasterDataVO<UID> mdvo) {
					final Object val = mdvo.getFieldValue(fieldUID);
					if (val == null)
						return false;
					return predicate.evaluate(val.toString().toLowerCase());
				}
			};
		}
	}
	
	public SearchConditionToPredicateVisitor() {
		//...
	}

	@Override
	public Predicate<MasterDataVO<UID>> visitTrueCondition(TrueCondition truecond) {
		return PredicateUtils.alwaysTrue();
	}

	@Override
	public Predicate<MasterDataVO<UID>> visitAtomicCondition(AtomicCollectableSearchCondition atomiccond) throws RuntimeException {
		// Note that we call the overloaded method accept(AtomicThingy...) which will then call one
		// of the specialized visit from the AtomicVisitor interface.
		return atomiccond.accept(new AtomicConditionToPredicateVisitor());
	}

	@Override
	public Predicate<MasterDataVO<UID>> visitCompositeCondition(CompositeCollectableSearchCondition compositecond) {
		Predicate<MasterDataVO<UID>>[] operands = CollectionUtils.transform(compositecond.getOperands(), this).toArray(new Predicate[0]);
		switch (compositecond.getLogicalOperator()) {
		case NOT:
			if (operands.length != 1) {
				throw new IllegalArgumentException("mdsearch.unparser.error.invalid.condition");
			}
			return PredicateUtils.not(operands[0]);
		case AND:
			return PredicateUtils.and(operands);
		case OR:
			return PredicateUtils.or(operands);
		default:
			throw new IllegalArgumentException("Illegal logical operator " + compositecond.getLogicalOperator());
		}
	}

	@Override
	public Predicate<MasterDataVO<UID>> visitIdCondition(CollectableIdCondition idcond) {
		return new FieldIdPredicate(idcond.getId());
	}

	@Override
	public Predicate<MasterDataVO<UID>> visitSubCondition(CollectableSubCondition subcond) {
		return new AlwaysFalsePredicate();//throw new IllegalArgumentException("subcond");
	}

	@Override
	public Predicate<MasterDataVO<UID>> visitRefJoinCondition(RefJoinCondition joincond) {
		return new AlwaysFalsePredicate();//throw new IllegalArgumentException("refjoincond");
	}

	@Override
	public Predicate<MasterDataVO<UID>> visitGeneralJoinCondition(GeneralJoinCondition joincond) {
		return new AlwaysFalsePredicate();//throw new IllegalArgumentException("refjoincond");
	}
	
	@Override
	public Predicate<MasterDataVO<UID>> visitReferencingCondition(ReferencingCollectableSearchCondition refcond) {
		return new AlwaysFalsePredicate();//throw new IllegalArgumentException("refcond");
	}

	//
	// CompositeVisitor
	//

	@Override
	public Predicate<MasterDataVO<UID>> visitPlainSubCondition(PlainSubCondition subcond) {
		return new AlwaysFalsePredicate();//throw new IllegalArgumentException("Plain SQL sub queries are not supported for system data");
	}

	@Override
	public Predicate<MasterDataVO<UID>> visitSelfSubCondition(CollectableSelfSubCondition subcond) {
		return new AlwaysFalsePredicate();//throw new IllegalArgumentException("Self-sub queries are not supported for system data");
	}

	@Override
	public Predicate<MasterDataVO<UID>> transform(CollectableSearchCondition cond) {
		return cond.accept(this);
	}

	@Override
    public Predicate<MasterDataVO<UID>> visitIdListCondition(CollectableIdListCondition collectableIdListCondition) throws RuntimeException {
        return new FieldIdListPredicate(collectableIdListCondition.getIds());
    }

	@Override
	public <T> Predicate<MasterDataVO<UID>> visitInCondition(CollectableInCondition<T> collectableInCondition) throws RuntimeException {
		return new FieldInPredicate(collectableInCondition.getFieldUID(), collectableInCondition.getInComparands());
	}
}