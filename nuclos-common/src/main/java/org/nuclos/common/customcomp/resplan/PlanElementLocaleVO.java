//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.common.customcomp.resplan;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.nuclos.common.UID;

@XmlType(name="planElementLocale")
public class PlanElementLocaleVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6368280180838164530L;
	public static final String LOCALE = "locale";
	public static final String BOOKING_L = "booking.label";
	public static final String BOOKING_TT = "booking.tooltip";

	@XmlAttribute(name = "version", required = true)
	static final String VERSION = "0.1";

	private UID localeId;
	private String localeLabel;

	private String bookingLabel;
	private String bookingTooltip;

	@XmlElement(name=LOCALE)
	public UID getLocaleId() {
		return localeId;
	}

	public void setLocaleId(UID localeId) {
		this.localeId = localeId;
	}

	public String getLocaleLabel() {
		return localeLabel;
	}

	public void setLocaleLabel(String localeLabel) {
		this.localeLabel = localeLabel;
	}

	@XmlElement(name=BOOKING_L)
	public String getBookingLabel() {
		return bookingLabel;
	}

	public void setBookingLabel(String bookingLabel) {
		this.bookingLabel = bookingLabel;
	}

	@XmlElement(name=BOOKING_TT)
	public String getBookingTooltip() {
		return bookingTooltip;
	}

	public void setBookingTooltip(String bookingTooltip) {
		this.bookingTooltip = bookingTooltip;
	}
}
