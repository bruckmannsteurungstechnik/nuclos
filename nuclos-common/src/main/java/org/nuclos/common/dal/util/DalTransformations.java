//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.dal.util;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.IDalVO;
import org.nuclos.common.report.valueobject.DynamicEntityVO;
import org.nuclos.common2.exception.CommonFatalException;

public class DalTransformations {
	private DalTransformations() {}

	public static <PK, S extends IDalVO<PK>> Transformer<S, PK> getId() {
		return new Transformer<S, PK>() {
			@Override
			public PK transform(S d) {
				return d.getPrimaryKey();
			}
		};
	}
	
	public static <PK, S extends IDalVO<PK>> Transformer<S, PK> getId(Class<PK> pkClass) {
		return new Transformer<S, PK>() {
			@Override
			public PK transform(S d) {
				return d.getPrimaryKey();
			}
		};
	}

	private static final Transformer<EntityMeta<?>, UID> GET_ENTITY
	= new Transformer<EntityMeta<?>, UID>() {
		@Override
		public UID transform(EntityMeta<?> md) {
			return md.getUID();
		}
	};
	
	private static final Transformer<DynamicEntityVO, UID> GET_DYNAMIC_ENTITY
	= new Transformer<DynamicEntityVO, UID>() {
		@Override
		public UID transform(DynamicEntityVO md) {
			return md.getEntityUID();
		}
	};

	public static Transformer<EntityMeta<?>, UID> getEntity() {
		return GET_ENTITY;
	}
	
	public static Transformer<DynamicEntityVO, UID> getDynamicEntity() {
		return GET_DYNAMIC_ENTITY;
	}
	
	private static final Transformer<FieldMeta<?>, UID> GET_FIELD
	= new Transformer<FieldMeta<?>, UID>() {
		@Override
		public UID transform(FieldMeta<?> md) {
			return md.getUID();
		}
	};

	public static Transformer<FieldMeta<?>, UID> getField() {
		return GET_FIELD;
	}

	public static Map<UID, FieldMeta<?>> flattenEntityMetaMap(Map<UID, EntityMeta<?>> mapEntityMeta) {
		Map<UID, FieldMeta<?>> result = new HashMap<UID, FieldMeta<?>>();
		for (EntityMeta<?> eMeta : mapEntityMeta.values()) {
			for (FieldMeta<?> fMeta : eMeta.getFields()) {
				result.put(fMeta.getUID(), fMeta);
			}
		}
		return result;
	}
	
	public static Map<UID, FieldMeta<?>> flattenFieldMetaMap(Map<UID, Map<UID, FieldMeta<?>>> mapEntityFieldMetaData) {
		final Map<UID, FieldMeta<?>> result = new HashMap<UID, FieldMeta<?>>();
		for (Entry<UID, Map<UID, FieldMeta<?>>> entry  : mapEntityFieldMetaData.entrySet()) {
			for (final FieldMeta<?> fMeta : entry.getValue().values()) {
				result.put(fMeta.getUID(), fMeta );
			}
		}
		return result;
	}
	
	public static Map<UID, FieldMeta<?>> transformFieldMap(Collection<FieldMeta<?>> fields) {
		if (fields == null || fields.isEmpty()) {
			return Collections.emptyMap();
		}
		Map<UID, FieldMeta<?>> result = new HashMap<UID, FieldMeta<?>>();
		for (FieldMeta<?> fieldMeta : fields) {
			result.put(fieldMeta.getUID(), fieldMeta);
		}
		return result;
	}
	
	public static Map<String, FieldMeta<?>> transformFieldMapUnsafe(Collection<FieldMeta<?>> fields) {
		if (fields == null || fields.isEmpty()) {
			return Collections.emptyMap();
		}
		Map<String, FieldMeta<?>> result = new HashMap<String, FieldMeta<?>>();
		for (FieldMeta<?> fieldMeta : fields) {
			if (result.containsKey(fieldMeta.getFieldName())) {
				throw new CommonFatalException("Field " + fieldMeta.getFieldName() + " is not unique in entity " + fieldMeta.getEntity());
			}
			result.put(fieldMeta.getFieldName(), fieldMeta);
		}
		return result;
	}

}
