//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.dal.vo;

import java.io.Serializable;

import org.nuclos.common.UID;

public abstract class AbstractDalVOBasic<PK> implements IDalVO<PK>, Serializable {
	private static final long serialVersionUID = 1012920874365879641L;
	
	private final UID dalEntity;
	
	private String processor;
	private int state;
	
	private PK primaryKey;
	
	public AbstractDalVOBasic(UID dalEntity) {
		super();
		this.dalEntity = dalEntity;
	}
	
	public AbstractDalVOBasic(AbstractDalVOBasic<PK> dalVO) {
		this(dalVO.getDalEntity());
		this.primaryKey = dalVO.getPrimaryKey();
	}
	
	@Override
	public UID getDalEntity() {
		return dalEntity;
	}

	@Override
	public final String processor() {
		return processor;
	}
	
	@Override
	public final void processor(String p) {
		processor = p;
	}

	@Override
	public PK getPrimaryKey() {
		return primaryKey;
	}

	@Override
	public void setPrimaryKey(PK primaryKey) {
		this.primaryKey = primaryKey;
	}
	
	public final void reset() {
		this.state = STATE_UNCHANGED;
	}
		
	@Override
	public final void flagNew() {
		this.state = STATE_NEW;
	}
	
	@Override
	public final void flagUpdate() {
		this.state = STATE_UPDATED;
	}
	
	@Override
	public final void flagRemove() {
		this.state = STATE_REMOVED;
	}

	@Override
	public final boolean isFlagNew() {
		return this.state == STATE_NEW;
	}

	@Override
	public final boolean isFlagUpdated() {
		return this.state == STATE_UPDATED;
	}

	@Override
	public final boolean isFlagUnchanged() {
		return this.state == STATE_UNCHANGED;
	}
	
	@Override
	public final boolean isFlagRemoved() {
		return this.state == STATE_REMOVED;
	}
	
	protected int getState() {
		return state;
	}
	
	protected void appendState(StringBuilder result) {
		String s;
		switch (state) {
		case STATE_UNCHANGED:
			s = "UNCHANGED";
			break;
		case STATE_NEW:
			s = "NEW";
			break;
		case STATE_UPDATED:
			s = "UPDATED";
			break;
		case STATE_REMOVED:
			s = "REMOVED";
			break;
		default:
			s = "?" + state + "?";
		}
		result.append(s);
	}
	
	public String getPKStringRepresentation() {
		PK pk = getPrimaryKey();
		
		if (pk == null) {
			return null;
		}
		
		if (pk instanceof UID) {
			return ((UID)pk).getString();
		}
		
		if (pk instanceof Long) {
			return Long.toString((Long)pk);
		}
		
		return pk.toString();
	}

}
