//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.dbtransfer;

public interface TransferConstants {

	/**
	 * 2: since Nuclos 3.8
	 * 3: since Nuclos 4.0
	 * 4: since Nuclos 4.12
	 * 5: since Nuclos 4.18 (NUCLOS-5884 "Refactoring Spalteneinstellungen")
	 * 6: since Nuclos 4.19 (NUCLOS-6134 "Berechtigungen auf Unterformulare im Statusmodell")
	 *
	 * Wiki: http://wiki.nuclos.de/display/Konfiguration/Nuclet+Datei-Version
	 */
	public static final Integer TRANSFER_VERSION = 6;
	
	public static final String NUCLET_FILE_EXTENSION = ".nuclet";

	public static final String ROOT_ENTRY_NAME = "nuclet.xml";
	
	public static final String FIELD_FOR_EXTERNALIZED_FILES = "_externalizedFiles_";
	
	public static final String FIELD_FOR_LOCALE_RESOURCES = "_localeResource_";
	
	public static final String DEFAULT_NUCLET = "_default_";

}
