//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.dbtransfer;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dal.vo.EntityObjectVO;

public class TransferEO implements Serializable {

	private static final long serialVersionUID = -2737297162333773651L;
	
	public final EntityObjectVO<UID> eo;
	public final Map<FieldMeta<?>, String> externalizedFiles = new HashMap<FieldMeta<?>, String>();
	public final Set<FieldMeta<?>> _localizedFields  = new HashSet<FieldMeta<?>>();
	
	public Integer existingVersion;
	public Integer lastImportVersion;
	
	public TransferEO(EntityObjectVO<UID> eo) {
		super();
		this.eo = eo;
		if (eo == null) {
			throw new IllegalArgumentException("eo must not be null");
		}
		if (eo.getPrimaryKey() == null) {
			throw new IllegalArgumentException("eo.pk must not be null");
		}
	}

	public UID getUID() {
		return eo.getPrimaryKey();
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) return true;

		if (!(o instanceof TransferEO)) return false;

		final TransferEO that = (TransferEO) o;

		return new EqualsBuilder()
				.append(getUID(), that.getUID())
				.append(eo.getDalEntity(), that.eo.getDalEntity())
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(getUID())
				.append(eo.getDalEntity())
				.toHashCode();
	}

	public static class Transformer implements org.nuclos.common.collection.Transformer<TransferEO, EntityObjectVO<UID>> {
		@Override
		public EntityObjectVO<UID> transform(TransferEO teo) {
			return teo.eo;
		}
	}
	
	public static List<EntityObjectVO<UID>> transformToEntityObjectVO(List<TransferEO> teos) {
		return CollectionUtils.transform(teos, new Transformer());
	}
}
