//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.dbtransfer;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Keys for the option maps used by the <code>TransferFacade</code>.
 * The keys that represent simple flags will be added with
 * <code>null</code> values to the maps.
 */
public enum TransferOption {

	IS_NUCLON, IS_DIRECTORY_MODE;

	public static Map<TransferOption, Serializable> copyOptionMap(Map<TransferOption, Serializable> src) {
		return src.isEmpty() ? new HashMap<TransferOption, Serializable>() : new HashMap<TransferOption, Serializable>(src);
	}
	
}
