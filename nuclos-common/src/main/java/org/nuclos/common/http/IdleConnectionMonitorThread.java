package org.nuclos.common.http;

import org.apache.http.conn.HttpClientConnectionManager;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class IdleConnectionMonitorThread extends Thread {

	private volatile boolean shutdown;
	private final NuclosHttpClientFactory clientFactory;

	IdleConnectionMonitorThread(final NuclosHttpClientFactory clientFactory) {
		super();

		this.clientFactory = clientFactory;

		setName(getClass().getSimpleName());
	}

	@Override
	public void run() {
		try {
			while (!shutdown) {
				synchronized (this) {
					wait(5000);

					HttpClientConnectionManager connectionManager = clientFactory.getConnectionManager();

					// Close expired connections
					connectionManager.closeExpiredConnections();
				}
			}
		} catch (InterruptedException ex) {
			// terminate
		}
	}

	public void shutdown() {
		shutdown = true;
		synchronized (this) {
			notifyAll();
		}
	}

}
