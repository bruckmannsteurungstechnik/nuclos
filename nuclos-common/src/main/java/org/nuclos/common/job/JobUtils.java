//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.job;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class JobUtils {
	
	/**
	 * Seconds 	YES 	0-59 	, - * /
	 * Minutes 	YES 	0-59 	, - * /
	 * Hours 	YES 	0-23 	, - * /
	 * Day of month 	YES 	1-31 	, - * ? / L W
	 * Month 	YES 	1-12 or JAN-DEC 	, - * /
	 * Day of week 	YES 	1-7 or SUN-SAT 	, - * ? / L #
	 * Year 	NO 	empty, 1970-2099 	, - * /
	 * 
	 * {0} minute
	 * {1} hour (of day)
	 * {2} day (of month)
	 * {3} month (of year) (as int)
	 * {4} (given) interval (as int)
	 */
	public static String getCronExpressionFromInterval(IntervalUnit unit, Integer interval, Calendar calendar) {
		// special handling for WEEK
		if (IntervalUnit.WEEK == unit) {
			unit = IntervalUnit.DAY;
			interval *= 7;
		}
		
		final String seconds = "0";
		String minutes = "*";
		String hours = "*";
		String dayOfMonth = "*";
		String month = "*";
		String dayOfWeek = "?";

		switch (unit) {
		case MINUTE:
			minutes = getExpression(0, 59, calendar.get(Calendar.MINUTE), interval);
			break;
		case HOUR:
			minutes = "" + calendar.get(Calendar.MINUTE);
			hours = getExpression(0, 23, calendar.get(Calendar.HOUR_OF_DAY), interval);
			break;
		case DAY:
			minutes = "" + calendar.get(Calendar.MINUTE);
			hours = "" + calendar.get(Calendar.HOUR_OF_DAY);
			dayOfMonth = getExpression(1, 31, calendar.get(Calendar.DAY_OF_MONTH), interval);
			break;
		case MONTH:
			minutes = "" + calendar.get(Calendar.MINUTE);
			hours = "" + calendar.get(Calendar.HOUR_OF_DAY);
			dayOfMonth = "" + calendar.get(Calendar.DAY_OF_MONTH);
			month = getExpression(1, 12, calendar.get(Calendar.MONTH)+1, interval);
			break;
		default:
			break;
		}
		
		return MessageFormat.format("{0} {1} {2} {3} {4} {5}", seconds, minutes, hours, dayOfMonth, month, dayOfWeek);
	}

	private static String getExpression(int min, int max, int start, int interval) {
		if (interval <= 1) {
			return "*";
		}
		else if (interval > max) {
			return "" + start;
		}
		else if (start % interval == 0) {
			return MessageFormat.format("*/{0}", interval);
		}

		List<Integer> values = new ArrayList<Integer>();
		values.add(start);

		int nextValue = start + interval;
		while (nextValue <= max) {
			values.add(nextValue);
			nextValue += interval;
		}
		nextValue = (nextValue % (max + 1)) + min;
		while (nextValue < start) {
			values.add(nextValue);
			nextValue += interval;
		}
		Collections.sort(values);

		return StringUtils.join(values, ",");
	}
}
