//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.metadata;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.TreeSet;

import javax.annotation.security.RolesAllowed;

import org.apache.commons.lang.ObjectUtils;
import org.jfree.util.Log;
import org.nuclos.common.CommonEntityObjectFacade;
import org.nuclos.common.E;
import org.nuclos.common.EntityTreeViewVO;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.EntityObjectToEntityTreeViewVO;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.entityobject.CollectableEOEntityProvider;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.TruncatableCollection;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeRemote;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

/**
* Provider for tree meta data
*/
@RolesAllowed("Login")
public class TreeMetaProvider {

	private CommonEntityObjectFacade eoFacade;
	private MasterDataFacadeRemote mdFacade;
	private IMetaProvider metaProvider;
	private CollectableEOEntityProvider collectableEntityProvider;

	public CommonEntityObjectFacade getEoFacade() {
		return eoFacade;
	}

	@Autowired
	@Qualifier("entityObjectFacade")
	public void setEoFacade(CommonEntityObjectFacade eoFacade) {
		this.eoFacade = eoFacade;
	}
	
	@Autowired
    @Qualifier("masterDataService")
    public void setEoFacade(MasterDataFacadeRemote mdFacade) {
        this.mdFacade = mdFacade;
    }

	@Autowired
	public void setMetaProvider(IMetaProvider metaProvider) {
		this.metaProvider = metaProvider;
	}

	@Autowired
	final void setCollectableEOEntityProvider(CollectableEOEntityProvider collectableEOEntityProvider) {
		this.collectableEntityProvider = collectableEOEntityProvider;
	}

	/**
	 * get tree node
	 * 
	 * @param uidNode {@link Long} node id
	 * @return {@link EntityTreeViewVO}
	 * @throws CommonPermissionException
	 */
	@Cacheable(value="eoTreeNode", key="#p0")
	public EntityTreeViewVO getNode(final UID uidNode) throws CommonPermissionException {
		final EntityObjectVO<UID> eoVO = getEoFacade().get(E.ENTITYSUBNODES.getUID(), uidNode);
		if (eoVO == null) {
			return null;
		}
		final EntityObjectToEntityTreeViewVO eoTransformer = new EntityObjectToEntityTreeViewVO();
		EntityTreeViewVO etvo = eoTransformer.transform(eoVO);
		
		CollectableSearchCondition csc = SearchConditionUtils.and(
				SearchConditionUtils.newUidComparison(E.ENTITYSUBNODES.parentSubNode, ComparisonOperator.EQUAL, etvo.getPrimaryKey())				
		);
		
		CollectableSearchExpression clctexpr = new CollectableSearchExpression(csc);
		
		ResultParams resultParams = new ResultParams(10000L, false);
		Collection<EntityObjectVO<UID>> collChildren = getEoFacade().getEntityObjectsChunk(E.ENTITYSUBNODES.getUID(), clctexpr, resultParams, null);
		for (EntityObjectVO<UID> eoChild : collChildren) {
			EntityTreeViewVO etvoChild = eoTransformer.transform(eoChild);
			etvo.addChildNode(etvoChild);
		}
		
		return etvo;			
	}
	
	@CacheEvict(value="eoTreeNode", allEntries = true)
	public void invalidate() {
		
	}

	//TODO Similar Data as in the second part of getNode (loadingOfChildNodes, above) but less fast. To unite!
	public List<EntityTreeViewVO> getTopLevelNodes(final UID uidEntity) throws CommonPermissionException {

		final CompositeCollectableSearchCondition condition = SearchConditionUtils.and(
				SearchConditionUtils.newIsNullCondition(E.ENTITYSUBNODES.parentSubNode),
				SearchConditionUtils.newUidComparison(E.ENTITYSUBNODES.originentityid, ComparisonOperator.EQUAL, uidEntity)
				);
		final List<UID> lstIds= getEoFacade().getEntityObjectIds(E.ENTITYSUBNODES.getUID(), new CollectableSearchExpression(condition));
		final EntityObjectToEntityTreeViewVO eoTransformer = new EntityObjectToEntityTreeViewVO();
		final List<EntityTreeViewVO> lstEtvVO = CollectionUtils.transform(lstIds, new Transformer<UID, EntityTreeViewVO>() {

			@Override
			public EntityTreeViewVO transform(UID uidEo) {
				try {
					return eoTransformer.transform(getEoFacade().get(E.ENTITYSUBNODES.getUID(), uidEo));
				} catch (final CommonPermissionException ex) {
					throw new IllegalStateException(ex);
				}
			}

		});
		return lstEtvVO;

	}

	/**
	 * get subnodes by parent {@link EntityTreeViewVO}
	 * 
	 * @param uidParentNode	parent {@link EntityTreeViewVO};
	 * @return
	 */
	public List<EntityTreeViewVO> getSubNodes(final UID uidParentNode) {
		try {
			Collection<EntityObjectVO<UID>> lstEoVO = getEntityObjectByRef(E.ENTITYSUBNODES.getUID(),E.ENTITYSUBNODES.parentSubNode.getUID(), uidParentNode);
			final EntityObjectToEntityTreeViewVO eoTransformer = new EntityObjectToEntityTreeViewVO();
			return CollectionUtils.transform(lstEoVO, eoTransformer);
		} catch (CommonPermissionException e) {
			throw new IllegalStateException("Client call?!");
		}
	}

	public String getNodeDescription(final UID uidNode, final Map<UID, Object> mpReadableFields, UID language) {
		try {
			final EntityTreeViewVO etvVO = getNode(uidNode);
			// FIXME is this correct - getPrimaryKey?
			return SpringLocaleDelegate.getInstance().getTreeViewDescription(
					mpReadableFields, etvVO, metaProvider, language);
		} catch (final CommonPermissionException ex) {
			throw new NuclosFatalException(ex);
		}
	}

	private final Collection<EntityObjectVO<UID>> getEntityObjectByRef(final UID uidEntity, final UID uidField, final UID uidReferenceEntity) throws CommonPermissionException {
		if (uidReferenceEntity == null) {
			return Collections.emptyList();
		}
		final CompositeCollectableSearchCondition condition = SearchConditionUtils.and(
				SearchConditionUtils.newUidComparison(uidField, ComparisonOperator.EQUAL, uidReferenceEntity)
				);
		final List<UID> lstUIDs = getEoFacade().getEntityObjectIds(uidEntity, new CollectableSearchExpression(condition));
		final List<EntityObjectVO<UID>> lstResult = CollectionUtils.transform(lstUIDs, new Transformer<UID, EntityObjectVO<UID>>() {

			@Override
			public EntityObjectVO<UID> transform(final UID idEo) {
				try {
					return getEoFacade().get(uidEntity, idEo);
				} catch (final CommonPermissionException ex) {
					throw new NuclosFatalException(ex);
				}
			}
		});
		return lstResult;
	}
	
	public List<UID> getFieldsFromStringifiedConfiguration(String sConfig, UID entity) {
		List<UID> ret = null;
		if (sConfig == null || sConfig.isEmpty()) {
			ret = Collections.emptyList();
			
		} else {
			ret = getFieldsFromStringifiedConfiguration(sConfig, metaProvider.getAllEntityFieldsByEntity(entity));
			
		}
		
		return ret;
	}
	
	public static List<UID> getFieldsFromStringifiedConfiguration(String sConfig, Map<UID, FieldMeta<?>> mapFields) {
		List<UID> ret = null;
		if (sConfig == null || sConfig.isEmpty()) {
			ret = Collections.emptyList();
			
		} else {
			try {
				ret = new ArrayList<UID>();
				
				for (String g : sConfig.split(" ")) {
					UID suid = UID.parseUID(g.trim());
					if (mapFields.containsKey(suid)) {
						ret.add(suid);
					}
				}
				
			} catch (Exception e) {
				Log.warn(e.getMessage(), e);
			}
		}
		
		return ret;
	}
	
	/**
     * Load subnodes from t_md_entity_subnodes table.
     */
    public void loadTreeConfiguration(final UID uidEntity, final List<EntityTreeViewVO> lstTreeViewVO) {
        // clear tree view list
        lstTreeViewVO.clear();
        
        final NavigableSet<EntityTreeViewVO> views = new TreeSet<EntityTreeViewVO>();
        
        final Collection<MasterDataVO<UID>> colChildNodesTmp = getMasterData(E.ENTITYSUBNODES.getUID(),
                SearchConditionUtils.and(org.nuclos.common.SearchConditionUtils.newUidComparison(E.ENTITYSUBNODES.originentityid, ComparisonOperator.EQUAL, uidEntity),
                SearchConditionUtils.not(org.nuclos.common.SearchConditionUtils.newIsNullCondition(E.ENTITYSUBNODES.parentSubNode))
                ));
        final List<EntityTreeViewVO> colChildNodes = CollectionUtils.transform(colChildNodesTmp, new Transformer<MasterDataVO<UID>, EntityTreeViewVO>(){
            @Override
            public EntityTreeViewVO transform(MasterDataVO<UID> mdvo) {
                return EntityTreeViewVO.wrapMasterData(mdvo, uidEntity);
            }
        });
        //CollectableSearchCondition cond;
        List<MasterDataVO<?>> voList = CollectionUtils.unsaveConvertToList(this.getMasterData(E.ENTITYSUBNODES.getUID()));
        for(MasterDataVO<?> vo : voList) {
            // filter top level nodes
            final UID uidOriginEntity = vo.getEntityObject().getFieldUid(E.ENTITYSUBNODES.originentityid.getUID());
            assert uidOriginEntity != null;
            if (uidOriginEntity.equals(uidEntity) && null == vo.getEntityObject().getFieldUids().get(E.ENTITYSUBNODES.parentSubNode.getUID())) {
                final EntityTreeViewVO parentVO = EntityTreeViewVO.wrapMasterData((MasterDataVO<UID>)vo, uidEntity);
                views.add(parentVO);
                // create child node tree
                createChildNodes(parentVO, colChildNodes);
            }
        }
        
        // fill tree model
        lstTreeViewVO.addAll(views);
    }
    
    
    /**
     * gets all the masterdata for an entity
     * 
     * §postcondition result != null
     * §postcondition !result.isTruncated()
     * 
     * @param entityUid
     * @return a (possibly empty) collection of masterdata objects.
     */
    public <PK> Collection<MasterDataVO<PK>> getMasterData(UID entityUid) {
        final TruncatableCollection<MasterDataVO<PK>> result = getMasterData(entityUid, null);

        assert result != null;
        assert !result.isTruncated();
        return result;
    }

    /**
     * gets the masterdata for an entity, using the given search condition, if any. The result is never truncated.
     * 
     * §postcondition result != null
     * §postcondition !result.isTruncated()
     * 
     * @param entityUid
     * @param cond may be <code>null</code>.
     * @return a (possibly empty) collection of masterdata objects.
     */
    public <PK> TruncatableCollection<MasterDataVO<PK>> getMasterData(UID entityUid, CollectableSearchCondition cond) {
        final TruncatableCollection<MasterDataVO<PK>> result = getMasterData2(entityUid, cond, false);

        assert result != null;
        assert !result.isTruncated();
        return result;
    }

    /**
     * gets the masterdata for an entity, using the given search condition, if any.
     * 
     * §postcondition result != null
     * 
     * @param entityUid
     * @param cond may be <code>null</code>.
     * @param bTruncate Are huge results to be truncated?
     * @return a (possibly empty) collection of masterdata objects.
     */
    public <PK> TruncatableCollection<MasterDataVO<PK>> getMasterData2(UID entityUid, CollectableSearchCondition cond, boolean bTruncate) {
        try {
            final TruncatableCollection<MasterDataVO<PK>> result = mdFacade.getMasterDataWithCheck(entityUid, cond, !bTruncate);
            assert result != null;
            return result;
        }
        catch (RuntimeException | CommonPermissionException ex) {
            final String sMessage = "MasterDataDelegate.4"+"Fehler beim Laden der Daten f\u00fcr die Entit\u00e4t {0}.";
            throw new CommonFatalException(sMessage, ex);
        }
    }
    
    
    private void createChildNodes(EntityTreeViewVO parentVO, List<EntityTreeViewVO> views) {
        for (final EntityTreeViewVO vo : views) {
            if (ObjectUtils.equals(parentVO.getPrimaryKey(), vo.getParentSubnodeId())) {
                parentVO.addChildNode(vo);
                createChildNodes(vo, views);
            }
        }
    }


}
