//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.preferences;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import java.util.prefs.PreferencesFactory;

import javax.annotation.PreDestroy;

import org.apache.log4j.Logger;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.server.common.ejb3.PreferencesFacadeRemote;
import org.nuclos.server.common.valueobject.PreferencesVO;

/**
 * <code>PreferencesFactory</code> for <code>NuclosPreferences</code>.
 * In order to use <code>NuclosPreferences</code> as <code>Preferences</code>,
 * you have to set the system property
 * <code>java.util.prefs.PreferencesFactory</code> to
 * <code>NuclosPreferencesFactory.class.getName()</code>.
 * <br>Based on Code found in Sun's Bug Database, Bug #4788410.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @author Thomas Pasch
 * @version 01.00.00
 */
// @Component
public class NuclosPreferencesSingleton implements PreferencesFactory {
	
	private static final Logger LOG = Logger.getLogger(NuclosPreferencesSingleton.class);
	
	//
	
	private static NuclosPreferencesSingleton INSTANCE;
	
	// Spring injection
	
	private PreferencesFacadeRemote preferencesFacadeRemote;
	
	private DOMPreferencesFactory domPreferencesFactory;
	
	// end of Spring injection
	
	private Preferences prefsUser;
	
	private Preferences prefsSystem;
	
	NuclosPreferencesSingleton() {
		// do nothing here
		INSTANCE = this;
	}
	
	public final void setPreferencesFacadeRemote(PreferencesFacadeRemote preferencesFacadeRemote) {
		this.preferencesFacadeRemote = preferencesFacadeRemote;
	}
	
	public final void setPreferencesFactory(DOMPreferencesFactory preferencesFactory) {
		this.domPreferencesFactory = preferencesFactory;
	}
	
	@PreDestroy
	public void destroy() {
		try {
			prefsUser.flush();
			// DOMPreferences can't save themselves
			if (prefsUser instanceof DOMPreferences) {
				saveDOMPreferences();
			}
			LOG.info("saved user preferences: " + prefsUser);
			dumpPref(prefsUser);
		}
		catch (BackingStoreException ex) {
			throw new NuclosFatalException(SpringLocaleDelegate.getInstance().getMessage(
					"NuclosPreferencesFactory.2", "Die Benutzer-Einstellungen konnten nicht gespeichert werden."), ex);
		}
		catch (Exception ex) {
			throw new NuclosFatalException(SpringLocaleDelegate.getInstance().getMessage(
					"NuclosPreferencesFactory.2", "Die Benutzer-Einstellungen konnten nicht gespeichert werden."), ex);
		}
	}
	
	private void saveDOMPreferences() throws BackingStoreException {
		final DOMPreferences root = (DOMPreferences) prefsUser;
		if (!root.isUserNode()) {
			throw new IllegalStateException();
		}
		try {
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			prefsUser.exportSubtree(baos);
			final byte[] bytes = baos.toByteArray();
			preferencesFacadeRemote.modifyUserPreferences(new PreferencesVO(bytes));
		}
		catch (Exception ex) {
			throw new BackingStoreException(ex);
		}
	}
	
	static NuclosPreferencesSingleton getInstance() {
		if (INSTANCE == null) {
			throw new IllegalStateException("too early");
		}
		return INSTANCE;
	}
	
	/**
	 * provides dummy preferences for those who insist on using the system preferences.
	 * @return dummy (system) preferences
	 */
	@Override
	public synchronized Preferences systemRoot() {
		LOG.warn("NuclosPreferencesFactory.systemRoot() wird nicht unterst\u00fctzt.");

		if (prefsSystem == null) {
			prefsSystem = new DummyPreferences();
		}
		return prefsSystem;
	}

	/**
	 * Someone in java.awt tries to access the userRoot() before the user was logged in. So we *try*
	 * to get the preferences from the server, but in case of a security exception leave the userRoot()
	 * uninitialized. java.awt prints a stack trace, but continues.
	 * @return the user root. May be null, if the user was not logged in.
	 */
	@Override
	public synchronized Preferences userRoot() {
		if (prefsUser == null) {

			final String sErrorMsg = SpringLocaleDelegate.getInstance().getMessage(
					"NuclosPreferencesFactory.1", "Die Benutzereinstellungen konnten nicht geladen werden.");

			PreferencesVO prefsvo;
			try {
				prefsvo = preferencesFacadeRemote.getUserPreferences();
			}
			catch (CommonFinderException ex) {
				// ignore: this happens when the user logs in the first time - there are no preferences yet.
				prefsvo = null;
			}
			catch (RuntimeException e) {
				throw e;
			}
			catch (Exception e) {
				LOG.warn("userRoot failed: " + e, e);
				prefsvo = null;
			}
			// prefsUser = new NuclosPreferencesRoot(preferencesFacadeRemote);

			if (prefsvo != null && prefsvo.getPreferencesBytes().length > 0) {
				// import the read preferences:
				final byte[] abXml = prefsvo.getPreferencesBytes();
				final ByteArrayInputStream is = new ByteArrayInputStream(abXml);
				try {
					// Preferences.importPreferences(is);
					prefsUser = domPreferencesFactory.read(is, true);
					dumpPref(prefsUser);
				}
				catch (IOException e) {
					LOG.warn("userRoot: preferences problem: " + e, e);
					throw new NuclosFatalException(sErrorMsg, e);
				}
				/*
				catch (InvalidPreferencesFormatException ex) {
					throw new NuclosFatalException(sErrorMsg, ex);
				}
				 */
				finally {
					try {
						is.close();
					} catch (IOException e) {
						// ignore
					}
				}
			} else {
				prefsUser = domPreferencesFactory.userRoot();
			}
		}	// if
		assert prefsUser != null;
		return prefsUser;
	}
	
	private void dumpPref(Preferences node) {
		final DOMPreferences prefs = (DOMPreferences) node;
		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			domPreferencesFactory.write(prefs, out, true);
		} catch (IOException e) {
			// ignore
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				// ignore
			}
		}
		LOG.trace("dom prefs:\n" + out.toString());
	}

}	// class NuclosPreferencesFactory
