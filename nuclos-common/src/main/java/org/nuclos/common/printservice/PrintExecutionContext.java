//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.printservice;

import java.io.Serializable;

import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;

/**
 * {@link PrintExecutionContext} setup several execution params for 
 * the printout process
 * 
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public class PrintExecutionContext implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7775811211648713906L;
	private UsageCriteria usagecriteria;
	private final Long pk;
	private final UID attachmentSubEntityUID;
	private final UID[] attachmentSubEntityAttributeUIDs;

	public PrintExecutionContext(final UsageCriteria usagecriteria, final Long pk, final UID attachmentSubEntityUID, final UID[] attachmentSubEntityAttributeUIDs) {
		this.pk = pk;
		this.usagecriteria = usagecriteria;
		this.attachmentSubEntityUID = attachmentSubEntityUID;
		this.attachmentSubEntityAttributeUIDs = attachmentSubEntityAttributeUIDs;
	}

	public UsageCriteria getUsagecriteria() {
		return usagecriteria;
	}

	public void setUsagecriteria(final UsageCriteria usagecriteria) {
		this.usagecriteria = usagecriteria;
	}

	public Long getPk() {
		return pk;
	}
	
	public UID getAttachmentSubEntityUID() {
		return attachmentSubEntityUID;
	}

	public final UID[] getAttachmentSubEntityAttributeUIDs() {
		return attachmentSubEntityAttributeUIDs;
	}

	@Override
	public String toString() {
		return "PrintExecutionContext [usagecriteria=" + usagecriteria
				+ ", pk=" + pk + "]";
	}



}
