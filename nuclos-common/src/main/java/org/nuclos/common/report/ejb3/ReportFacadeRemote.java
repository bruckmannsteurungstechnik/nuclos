//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.ejb3;

import java.io.ObjectInputStream;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.print.DocFlavor;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;

import org.nuclos.common.NuclosFile;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.report.NuclosReportException;
import org.nuclos.common.report.NuclosReportPrintJob;
import org.nuclos.common.report.NuclosReportRemotePrintService;
import org.nuclos.common.report.valueobject.DefaultReportOutputVO;
import org.nuclos.common.report.valueobject.DefaultReportVO;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.nuclos.common.report.valueobject.ReportOutputVO.PageOrientation;
import org.nuclos.common.report.valueobject.ReportVO.ReportType;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;

// @Remote
public interface ReportFacadeRemote {

	/**
	 * @return all reports
	 * @throws CommonPermissionException
	 */
	Collection<DefaultReportVO> getReports()
		throws CommonPermissionException;

	/**
	 * Get all reports which have outputs containing the given datasourceId and have the given type (report, form or template).
	 * We go a little indirection so that we can use the security mechanism of the ReportBean.
	 * 
	 * @param dataSourceUID datasource UID
	 * @param type
	 * @return set of reports
	 */
	Collection<DefaultReportVO> getReportsForDatasourceId(
		final UID dataSourceUID, final ReportType type)
		throws CommonPermissionException;

	/**
	 * create new report
	 * @param mdvo value object
	 * @param mpDependants
	 * @return new report
	 */
	MasterDataVO<UID> create(MasterDataVO<UID> mdvo, IDependentDataMap mpDependants) throws CommonCreateException,
		NuclosReportException, CommonPermissionException,
		NuclosBusinessRuleException;

	/**
	 * modify an existing report
	 * 
	 * @param mdvo value object
	 * @param mpDependants
	 * @return modified report
	 */
	UID modify(MasterDataVO<UID> mdvo, IDependentDataMap mpDependants) throws CommonBusinessException;

	/**
	 * delete an existing report
	 * 
	 * @param mdvo value object
	 */
	<PK> void remove(MasterDataVO<PK> mdvo)
		throws CommonFinderException, CommonRemoveException,
		CommonStaleVersionException, CommonPermissionException,
		CommonCreateException, NuclosBusinessRuleException;

	/**
	 * get output formats for report
	 * @param reportUID report UID
	 * @return collection of output formats
	 */
	Collection<DefaultReportOutputVO> getReportOutputs(final UID reportUID);

	/**
	 * get output format for reportoutput UID
	 * @param reportOutputUID reportoutput UID
	 * @return reportoutput
	 */
	DefaultReportOutputVO getReportOutput(UID reportOutputUID)
		throws CommonFinderException, CommonPermissionException;

	NuclosFile testReport(final UID reportOutputUID) throws NuclosReportException;
	
	/**
	 * gets a report/form filled with data
	 * @param reportOutputUID reportoutput UID
	 * @param mpParams parameters
	 * @return report/form filled with data
	 */
	NuclosFile prepareReport(UID reportOutputUID, Map<String, Object> mpParams, Integer iMaxRowCount, UID language) throws CommonFinderException, NuclosReportException,
		CommonPermissionException;

	/**
	 * gets search result report filled with data
	 * <p>
	 * TODO: Don't serialize CollectableEntityField and/or CollectableEntity! (tp)
	 * Refer to {@link org.nuclos.common.CollectableEntityFieldWithEntity#readObject(ObjectInputStream)} for details.
	 * </p>
	 * @param clctexpr search expression
	 * @param entityUID module id of module to be displayed
	 * @param bIncludeSubModules Include submodules in search?
	 * @return search result report filled with data
	 */
	@RolesAllowed("Login")
	NuclosFile prepareSearchResult(CollectableSearchExpression clctexpr,
		List<? extends CollectableEntityField> lstclctefweSelected,
		List<Integer> selectedFieldWidth,
		final UID entityUID, ReportOutputVO.Format format, String customUsage, PageOrientation orientation, boolean columnScaled)
		throws NuclosReportException;

	/**
	 * Export any {@link ResultVO} (converted TableModels for instance)
	 * 
	 * @return Exported ResultVO as document 
	 * @throws NuclosReportException
	 */
	@RolesAllowed("Login")
	NuclosFile prepareExport(ResultVO resultvo, ReportOutputVO.Format format) throws NuclosReportException;

	/**
	 * Export any {@link ResultVO} (converted TableModels for instance)
	 * 
	 * @return Exported ResultVO as document 
	 * @throws NuclosReportException
	 */
	@RolesAllowed("Login")
	NuclosFile prepareExport(String searchCondition, ResultVO resultvo, ReportOutputVO.Format format, ReportOutputVO.PageOrientation orientation, boolean columnScaled) throws NuclosReportException;

	/**
	 * @param reportUID report/form UID
	 * @return Is save allowed for the report/form with the given id?
	 */
	@RolesAllowed("Login")
	boolean isSaveAllowed(final UID reportUID);

	/**
	 * finds reports readable for current user
	 * @return collection of report UIDs
	 */
	Collection<UID> getReadableReportUIDsForCurrentUser();

	/**
	 * finds reports (forms) by usage criteria
	 * @param usagecriteria
	 * @return collection of reports (forms)
	 */
	@RolesAllowed("Login")
	Collection<DefaultReportVO> findReportsByUsage(
		UsageCriteria usagecriteria);

	public NuclosReportRemotePrintService lookupDefaultPrintService() throws NuclosReportException;
	
	public NuclosReportRemotePrintService[] lookupPrintServices(DocFlavor flavor, AttributeSet as) throws NuclosReportException;
		
	public void printViaPrintService(NuclosReportRemotePrintService ps, NuclosReportPrintJob pj, PrintRequestAttributeSet aset, byte[] data)  throws NuclosReportException;

	
}
