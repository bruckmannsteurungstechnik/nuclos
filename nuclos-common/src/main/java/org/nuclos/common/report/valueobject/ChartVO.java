//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.valueobject;

import org.nuclos.common.UID;
import org.nuclos.server.common.valueobject.NuclosValueObject;

public class ChartVO extends DatasourceVO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7023993521496503932L;
	private String detailsEntity;
	private UID detailsEntityUID;
	
	private String parentEntity;
	private UID parentEntityUID;
	
	public ChartVO(final NuclosValueObject<UID> evo, final String name,
		final String sDescription, final UID detailsEntityUID, UID parentEntityUID, final Boolean bValid, final String sDatasourceXML, final UID nucletUID) {
		super(evo, name, sDescription, bValid, sDatasourceXML, nucletUID, PERMISSION_NONE);
		setDetailsEntityUID(detailsEntityUID);
		setParentEntityUID(parentEntityUID);
	}

	public ChartVO(final String name, String sDescription,
			final String sDatasourceXML, final Boolean bValid, final UID nucletUID) {
		super(name, sDescription, sDatasourceXML, bValid, nucletUID);
	}
	
	/*
	 * Method is needed!
	 *  - @see handling as BeanPropertyCollectableField
	 * @deprecated
	 */
	public String getDetailsEntity() {
    	return detailsEntity;
    }
	public void setDetailsEntity(final String entity) {
    	this.detailsEntity = entity;
    }
	
	public UID getDetailsEntityUID() {
    	return detailsEntityUID;
    }
	public void setDetailsEntityUID(final UID entityUID) {
    	this.detailsEntityUID = entityUID;
    }
	
	/*
	 * Method is needed!
	 *  - @see handling as BeanPropertyCollectableField
	 * @deprecated
	 */
	public String getParentEntity() {
    	return parentEntity;
    }
	public void setParentEntity(final String entity) {
    	this.parentEntity = entity;
    }
	
	public UID getParentEntityUID() {
    	return parentEntityUID;
    }
	public void setParentEntityUID(final UID entityUID) {
    	this.parentEntityUID = entityUID;
    }
}
