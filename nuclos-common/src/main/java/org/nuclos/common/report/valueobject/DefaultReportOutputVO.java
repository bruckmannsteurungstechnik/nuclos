//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.valueobject;

import java.io.Serializable;

import org.nuclos.api.print.PrintProperties;
import org.nuclos.common.UID;
import org.nuclos.common.report.ByteArrayCarrier;
import org.nuclos.server.common.valueobject.INuclosValueObject;
import org.nuclos.server.common.valueobject.NuclosValueObject;

/**
 * Value object representing a report output definition.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @version 01.00.00
 * @author	<a href="mailto:Boris.Sander@novabit.de">Boris Sander</a>
 */
public class DefaultReportOutputVO extends NuclosValueObject<UID> implements ModifiableReportOutputVO, Serializable {


	private static final long serialVersionUID = 1L;


	private UID reportUID;


	private Format format;
	private Destination destination;
	private String sParameter;
	private String sFilename;
	private String sSourceFile;

	private PageOrientation pageOrientation;
	private boolean bColumnScaled;

	private ByteArrayCarrier oSourceFileContent;

	private UID dataSourceUID;

	private String sSheetName;

	private String sDescription;
	private ByteArrayCarrier oReportCLS;

	private String locale;

	private boolean bFirstOfMany = true;
	private boolean bLastOfMany = true;

	private boolean bAttach;

	private String customParameter;
	private UID roleId;
	private UID userId;
	private boolean isMandatory;
	private PrintProperties printProperties;


	private NuclosValueObject<UID> vo;

	public DefaultReportOutputVO() {
		
	}
	public DefaultReportOutputVO(ReportOutputVOContext context) {
 
		super((INuclosValueObject<UID>)context.getNuclosVO());
		this.vo = context.getNuclosVO();
		this.reportUID = context.getReportId();
		this.format = context.getFormat();
		this.destination = context.getDestination();
		this.sParameter = context.getParameter();
		this.sFilename = context.getFilename();
		this.sSourceFile = context.getSourceFile();

		this.oReportCLS = context.getReportCLS();
		this.oSourceFileContent = context.getSourceFileContent();
		this.dataSourceUID = context.getDataSourceId();
		this.sSheetName = context.getSheetName();
		this.sDescription = context.getDescription();

		this.locale = context.getLocale();
		this.bAttach = Boolean.TRUE.equals(context.getAttach());
		
		this.printProperties = context.getProperties();
		this.customParameter = context.getCustomParameter();
		this.roleId = context.getRoleId();
		this.userId = context.getUserId();
		this.isMandatory = context.getIsMandatory();
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportOutputVO#getReportUID()
	 */
	@Override
	public UID getReportUID() {
		return reportUID;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportOutputVO#getDestination()
	 */
	@Override
	public Destination getDestination() {
		return destination;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportOutputVO#getFormat()
	 */
	@Override
	public Format getFormat() {
		return format;
	}
	
	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportOutputVO#getPageOrientation()
	 */
	@Override
	public PageOrientation getPageOrientation() {
		return pageOrientation;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportOutputVO#isColumnScaled()
	 */
	@Override
	public boolean isColumnScaled() {
		return bColumnScaled;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportOutputVO#getParameter()
	 */
	@Override
	public String getParameter() {
		return sParameter;
	}
	
	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportOutputVO#getFilename()
	 */
	@Override
	public String getFilename() {
		return sFilename;
	}


	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportOutputVO#getSourceFile()
	 */
	@Override
	public String getSourceFile() {
		return sSourceFile;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportOutputVO#getReportCLS()
	 */
	@Override
	public ByteArrayCarrier getReportCLS() {
		return oReportCLS;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportOutputVO#getDatasourceUID()
	 */
	@Override
	public UID getDatasourceUID() {
		return dataSourceUID;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportOutputVO#getSheetname()
	 */
	@Override
	public String getSheetname() {
		return sSheetName;
	}
	
	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportOutputVO#getSourceFileContent()
	 */
	@Override
	public ByteArrayCarrier getSourceFileContent() {
		return this.oSourceFileContent;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportOutputVO#isFirstOfMany()
	 */
	@Override
	public boolean isFirstOfMany() {
		return bFirstOfMany;
	}
	
	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportOutputVO#isLastOfMany()
	 */
	@Override
	public boolean isLastOfMany() {
		return bLastOfMany;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportOutputVO#getDescription()
	 */
	@Override
	public String getDescription() {
		return sDescription;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportOutputVO#getLocale()
	 */
	@Override
	public String getLocale() {
		return locale;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportOutputVO#getAttachDocument()
	 */
	@Override
	public boolean getAttachDocument() {
		return bAttach;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportOutputVO#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("id=").append(getId());
		result.append(",srcFile=").append(getSourceFile());
		result.append(",format=").append(getFormat());
		result.append(",locale=").append(getLocale());
		result.append(",copies=").append(getPrintProperties().getCopies());
		result.append(",attach=").append(getAttachDocument());
		result.append("]");
		return result.toString();
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportOutputVO#getIsMandatory()
	 */
	@Override
	public boolean getIsMandatory() {
		return this.isMandatory;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportOutputVO#getUserId()
	 */
	@Override
	public UID getUserId() {
		return this.userId;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportOutputVO#getRoleId()
	 */
	@Override
	public UID getRoleId() {
		return this.roleId;
	}


	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportOutputVO#getCustomParameter()
	 */
	@Override
	public String getCustomParameter() {
		return this.customParameter;
	}

	@Override
	public NuclosValueObject<UID> getValueObject() {
		return vo;
	}


	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ReportOutputVO#getPrintProperties()
	 */
	@Override
	public PrintProperties getPrintProperties() {
		return this.printProperties;
	}


	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ModifiableReportOutputVO#setIsFirstOfMany(boolean)
	 */
	@Override
	public void setIsFirstOfMany(boolean firstOfMany) {
		this.bFirstOfMany = firstOfMany;
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ModifiableReportOutputVO#setIsLastOfMany(boolean)
	 */
	@Override
	public void setIsLastOfMany(boolean lastOfMany) {
		this.bLastOfMany = lastOfMany;
		
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ModifiableReportOutputVO#setParameter(String)
	 */
	@Override
	public void setParameter(String parameter) {
		this.sParameter = parameter;
		
	}

	/* (non-Javadoc)
	 * @see org.nuclos.common.report.valueobject.ModifiableReportOutputVO#setReportCLS(ByteArrayCarrier)
	 */
	@Override
	public void setReportCLS(ByteArrayCarrier reportCLS) {
		this.oReportCLS = reportCLS;
		
	}

	@Override
	public void setReportUID(UID reportUID) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public void setDestination(Destination destination) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public void setFormat(Format format) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public void setPageOrientation(PageOrientation pageOrientation) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public void setColumnScaled(boolean bColumnScaled) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public void setFilename(String sFilename) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public void setSourceFile(String sSourceFile) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public void setDatasourceUID(UID dataSourceUID) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public void setSheetname(String sSheetname) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public void setSourceFileContent(ByteArrayCarrier oSourceFileContent) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public void setDescription(String sDescription) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public void setLocale(String locale) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public void setAttachDocument(boolean bAttach) {
		throw new UnsupportedOperationException();
		
	}
	@Override
	public PrintProperties getProperties() {
		return printProperties;
	}
}
