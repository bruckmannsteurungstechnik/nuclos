//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.valueobject;

import org.nuclos.api.UID;
import org.nuclos.api.print.PrintProperties;
import org.nuclos.common.report.ByteArrayCarrier;
import org.nuclos.server.common.valueobject.NuclosValueObject;

public class NuclosOutputFormat implements ReportOutputVO {

	private final ReportOutputVO reportOutputVO;


	public NuclosOutputFormat(final ReportOutputVO reportOutputVO) {
		this.reportOutputVO = reportOutputVO;
	}
	
	@Override
	public UID getId() {
		return reportOutputVO.getId();
	}
	
	@Override
	public PrintProperties getProperties() {
		return reportOutputVO.getProperties();
	}
	
	@Override
	public NuclosValueObject<org.nuclos.common.UID> getValueObject() {
		return reportOutputVO.getValueObject();
	}

	@Override
	public org.nuclos.common.UID getReportUID() {
		return reportOutputVO.getReportUID();
	}

	@Override
	public Destination getDestination() {
		return reportOutputVO.getDestination();
	}

	@Override
	public Format getFormat() {
		return reportOutputVO.getFormat();
	}

	@Override
	public PageOrientation getPageOrientation() {
		return reportOutputVO.getPageOrientation();
	}

	@Override
	public boolean isColumnScaled() {
		return reportOutputVO.isColumnScaled();
	}

	@Override
	public String getParameter() {
		return reportOutputVO.getParameter();
	}

	@Override
	public String getFilename() {
		return reportOutputVO.getFilename();
	}

	@Override
	public String getSourceFile() {
		return reportOutputVO.getSourceFile();
	}

	@Override
	public ByteArrayCarrier getReportCLS() {
		return reportOutputVO.getReportCLS();
	}

	@Override
	public org.nuclos.common.UID getDatasourceUID() {
		return reportOutputVO.getDatasourceUID();
	}

	@Override
	public String getSheetname() {
		return reportOutputVO.getSheetname();
	}

	@Override
	public ByteArrayCarrier getSourceFileContent() {
		return reportOutputVO.getSourceFileContent();
	}

	@Override
	public boolean isFirstOfMany() {
		return reportOutputVO.isFirstOfMany();
	}

	@Override
	public boolean isLastOfMany() {
		return reportOutputVO.isLastOfMany();
	}

	@Override
	public String getDescription() {
		return reportOutputVO.getDescription();
	}

	@Override
	public String getLocale() {
		return reportOutputVO.getLocale();
	}

	@Override
	public boolean getAttachDocument() {
		return reportOutputVO.getAttachDocument();
	}

	@Override
	public String toString() {
		return reportOutputVO.toString();
	}

	@Override
	public PrintProperties getPrintProperties() {
		return reportOutputVO.getPrintProperties();
	}

	@Override
	public boolean getIsMandatory() {
		return reportOutputVO.getIsMandatory();
	}

	@Override
	public org.nuclos.common.UID getUserId() {
		return reportOutputVO.getUserId();
	}

	@Override
	public org.nuclos.common.UID getRoleId() {
		return reportOutputVO.getRoleId();
	}

	@Override
	public String getCustomParameter() {
		return reportOutputVO.getCustomParameter();
	}

}
