//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.valueobject;

import java.io.Serializable;

import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.context.PrintResult;


/**
 * {@link PrintResultTO} provides default implementation for
 * {@link PrintResult}
 *  
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public class PrintResultTO implements PrintResult, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3322497027470043295L;
	private final OutputFormatTO outputFormat;
	private final NuclosFile output;
	private final Exception exception;
	private final String sMessage;
	
	private String sourceObjectIdentifierLabel;

	public PrintResultTO(final OutputFormatTO outputFormat, final NuclosFile output) {
		this.outputFormat = outputFormat;
		this.output = output;
		
		this.exception = null;
		this.sMessage = null;
	}
	
	public PrintResultTO(final OutputFormatTO outputFormat, Exception ex, String sMessage, String sourceObjectIdentifierLabel) {
		this.outputFormat = outputFormat;
		this.exception = ex;
		this.sMessage = sMessage;
		this.sourceObjectIdentifierLabel = sourceObjectIdentifierLabel;
		
		this.output = null;
	}

	@Override
	public NuclosFile getOutput() {
		return output;
	}

	@Override
	public OutputFormatTO getOutputFormat() {
		return outputFormat;
	}

	public Exception getException() {
		return exception;
	}

	public String getMessage() {
		return sMessage;
	}

	public String getSourceObjectIdentifierLabel() {
		return sourceObjectIdentifierLabel;
	}

	public void setSourceObjectIdentifierLabel(String sourceObjectIdentifierLabel) {
		this.sourceObjectIdentifierLabel = sourceObjectIdentifierLabel;
	}
	
}
