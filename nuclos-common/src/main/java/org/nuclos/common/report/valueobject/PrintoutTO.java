//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.valueobject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.nuclos.api.UID;
import org.nuclos.api.printout.Printout;

/**
 * {@link PrintoutTO} transport object
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public class PrintoutTO implements Printout, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4995388381407510777L;

	private final String name;
	
	private UID idPrintout;
	private Long idBo;
	private List<OutputFormatTO> outputFormats;
	private final ReportVO.OutputType outputType;
	private UID idDatasource;
	
	public PrintoutTO(final UID idPrintout, final String name, final ReportVO.OutputType outputType, final UID idDatasource) {
		this.idPrintout = idPrintout;
		this.idDatasource = idDatasource;
		this.name = name;
		this.outputFormats = new ArrayList<OutputFormatTO>();
		this.outputType = outputType;
	}
	
	public PrintoutTO clone() {
		PrintoutTO clone = new PrintoutTO(idPrintout, name, outputType, idDatasource);
		clone.idBo = idBo;
		for (OutputFormatTO format : outputFormats) {
			clone.getOutputFormats().add(format.clone());
		}
		return clone;
	}
	
	@Override
	public UID getId() {
		return idPrintout;
	}

	@Override
	public void setBusinessObjectId(Long idBo) {
		this.idBo = idBo;
	}

	@Override
	public Long getBusinessObjectId() {
		return idBo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OutputFormatTO> getOutputFormats() {
		return outputFormats;
	}

	public String getName() {
		return name;
	}

	public ReportVO.OutputType getOutputType() {
		return outputType;
	}
	
	public UID getDatasourceId() {
		return idDatasource;
	}

	@Override
	public String toString() {
		return "PrintoutTO [name=" + name + ", idPrintout=" + idPrintout
				+ ", idBo=" + idBo + ", outputFormats=" + outputFormats + "]";
	}

	

}
