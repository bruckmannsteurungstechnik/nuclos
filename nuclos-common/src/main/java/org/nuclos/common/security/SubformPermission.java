package org.nuclos.common.security;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.nuclos.common.UID;

/**
 * Created by oliver brausch on 14.07.17.
 */
public class SubformPermission implements IPermission {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1346755671771761641L;
	public final static SubformPermission ALL = new SubformPermission(true);
	public final static SubformPermission NONE = new SubformPermission(false);

	private final boolean create;

	private final boolean delete;

	private boolean all = false;

	private Map<UID, Permission> subformGroupPermissions = new HashMap<>();

	private SubformPermission(boolean all) {
		this(all, all);
		this.all = all;
	}

	public SubformPermission(boolean create, boolean delete) {
		this.create = create;
		this.delete = delete;
	}

	public boolean includesReading() {
		if (all) {
			return true;
		}

		for (Permission p : subformGroupPermissions.values()) {
			if (p.includesReading()) {
				return true;
			}
		}

		return false;
	}

	public boolean includesWriting() {
		if (create || delete) {
			return true;
		}

		for (Permission p : subformGroupPermissions.values()) {
			if (p.includesWriting()) {
				return true;
			}
		}

		return false;
	}

	public boolean canCreate() {
		return create;
	}

	public boolean canDelete() {
		return delete;
	}

	/**
	 * @param other
	 * @return
	 */
	public SubformPermission max(SubformPermission other) {
		if (all || other == null) {
			return this;
		}

		if (other.all) {
			return other;
		}

		SubformPermission ret = new SubformPermission(create || other.create, delete || other.delete);

		for (UID group : subformGroupPermissions.keySet()) {
			Permission pThis = subformGroupPermissions.get(group);
			Permission pOther = other.subformGroupPermissions.get(group);

			if (pOther == null) {
				ret.subformGroupPermissions.put(group, pThis);
			} else {
				ret.subformGroupPermissions.put(group, pOther.max(pThis));
			}
		}

		return ret;
	}

	/**
	 * @param other
	 * @return
	 */
	public SubformPermission min(SubformPermission other) {
		if (all || other == null) {
			return other;
		}

		if (other.all) {
			return this;
		}

		SubformPermission ret = new SubformPermission(create && other.create, delete && other.delete);

		for (UID group : subformGroupPermissions.keySet()) {
			Permission pThis = subformGroupPermissions.get(group);
			Permission pOther = other.subformGroupPermissions.get(group);

			if (pOther != null) {
				ret.subformGroupPermissions.put(group, pOther.min(pThis));
			}
		}

		return ret;
	}


	public Permission getSimplePermission() {
		return includesWriting() ? Permission.READWRITE :
				(includesReading() ? Permission.READONLY : Permission.NONE);
	}

	public void addGroupPermission(UID groupUID, Permission permission) {

		Permission exstPermission = subformGroupPermissions.get(groupUID);
		if (exstPermission != null) {
			permission = permission.max(exstPermission);
		}

		subformGroupPermissions.put(groupUID, permission);

	}

	public Permission getGroupPermission(UID groupUID) {
		if (all) {
			return Permission.READWRITE;
		}
		Permission permission = subformGroupPermissions.get(groupUID);
		return permission != null ? permission : Permission.NONE;
	}

	/**
	 *
	 * @return Collection<UID> Attribute Groups where read is allowed. NULL means, ALL are allowed
	 */
	public Collection<UID> getReadAllowedGroups() {
		if (all) {
			return null;
		}

		Collection<UID> readWriteAllowed = getReadWriteAllowedGroups();
		readWriteAllowed.addAll(getReadOnlyAllowedGroups());
		return readWriteAllowed;
	}

	/**
	 *
	 * @return Collection<UID> Attribute Groups where read only is allowed. NULL means, ALL are allowed
	 */
	public Collection<UID> getReadOnlyAllowedGroups() {
		return getAllowedGroups(Permission.READONLY);
	}

	/**
	 *
	 * @return Collection<UID> Attribute Groups where read and write is allowed. NULL means, ALL are allowed
	 */
	public Collection<UID> getReadWriteAllowedGroups() {
		return getAllowedGroups(Permission.READWRITE);
	}


	private Collection<UID> getAllowedGroups(Permission which) {
		if (all) {
			return null;
		}
		Collection<UID> ret = new HashSet<>();
		for (UID group : subformGroupPermissions.keySet()) {
			if (subformGroupPermissions.get(group) == which) {
				ret.add(group);
			}
		}
		return ret;
	}
}
