//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.spring;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.transform.Source;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Role;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.core.type.filter.TypeFilter;
import org.springframework.oxm.UncategorizedMappingException;
import org.springframework.oxm.XmlMappingException;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.oxm.mime.MimeContainer;
import org.springframework.util.ClassUtils;

/**
 * An extension to the Jaxb2Marshaller that scans the classpath for classes annotated
 * with the @XmlRootElement (and others) annotation. I don't like typing in the class in
 * classesToBeBound since I often forget which ones I've done!
 * <p>
 * Stolen from 
 * http://dharrigan.com/2012/01/03/automatically-adding-jaxb2-classes-to-spring/
 * and modified by Thomas Pasch.
 * 
 * @author dharrigan (David Harrigan)
 * @author Thomas Pasch
 */
public class AnnotationJaxb2Marshaller extends Jaxb2Marshaller {

	private static final String RESOURCE_PATTERN = "/**/*.class";

	private static final Logger LOG = Logger.getLogger(AnnotationJaxb2Marshaller.class);
	
	private final ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
	private final TypeFilter[] jaxb2TypeFilters = new TypeFilter[] {
			new AnnotationTypeFilter(XmlRootElement.class, false),
			new AnnotationTypeFilter(XmlType.class, false),
			new AnnotationTypeFilter(XmlSeeAlso.class, false),
			new AnnotationTypeFilter(XmlEnum.class, false),
	};
	
	private List<Class<?>> scannedPackages;

	/**
	 * Scan packages looking for any classes annotated with the @XmlRootElement annotation.
	 */
	protected List<Class<?>> scanPackages() {
		if (scannedPackages != null) {
			return scannedPackages;
		}
		final List<Class<?>> annotatedClasses = new ArrayList<Class<?>>();
		try {
			final String[] packagesToScan = getPackagesToScan();
			if (packagesToScan != null) {				
				LOG.info("PackagesToScan=" + packagesToScan.length);
				final MetadataReaderFactory metadataReaderFactory = new CachingMetadataReaderFactory(resourcePatternResolver);

				for (final String pkg : packagesToScan) {
					String ressourcePattern = RESOURCE_PATTERN;
					
					//FIXME NUCLOS-4827 Within signed jars the performance of the scanning is very bad, 
					//this is why the classes to-be-scanned are reduced
					if ("org.nuclos.client.customcomp.resplan".equals(pkg)) {
						ressourcePattern = "/**/Client*Plan*.class";
					}
					
					final String pattern = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX 
							+ ClassUtils.convertClassNameToResourcePath(pkg) + ressourcePattern;
					
					final Resource[] resources = resourcePatternResolver.getResources(pattern);
					
					for (final Resource resource : resources) {
						final MetadataReader metadataReader = metadataReaderFactory.getMetadataReader(resource);
						final String className = metadataReader.getClassMetadata().getClassName();
						if (matchesFilter(metadataReader, metadataReaderFactory)) {
							final Class<?> jaxb2AnnotatedClass = resourcePatternResolver.getClassLoader().loadClass(className);
							annotatedClasses.add(jaxb2AnnotatedClass);							
						}
					}
				}
				
				LOG.info("ScannedPackaged=" + annotatedClasses);

			}
			
		} catch (final IOException ex) {
			throw new UncategorizedMappingException("Failed to scan classpath for unlisted classes", ex);
		} catch (final ClassNotFoundException ex) {
			throw new UncategorizedMappingException("Failed to load annoted classes from classpath", ex);
		}
		scannedPackages = annotatedClasses;
		return annotatedClasses;
	}

	/**
	 * Determine if any of the classes matches our list of acceptable annotations.
	 *
	 * @param metadataReader for the resource.
	 * @param metadataReaderFactory for the resource.
	 * @return true if the class contains the annotation.
	 * @throws IOException if anything goes wrong.
	 */
	protected boolean matchesFilter(final MetadataReader metadataReader,
			final MetadataReaderFactory metadataReaderFactory) throws IOException {
		if (jaxb2TypeFilters != null) {
			for (final TypeFilter typeFilter : jaxb2TypeFilters) {
				if (typeFilter.match(metadataReader, metadataReaderFactory)) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public Object unmarshal(Source source) throws XmlMappingException {
		if (source == null) {
			return null;
		}
		return super.unmarshal(source);
	}

	@Override
	public Object unmarshal(Source source, MimeContainer mimeContainer) throws XmlMappingException {
		if (source == null) {
			return null;
		}
		return super.unmarshal(source, mimeContainer);		
	}
	
	/**
	 * Why oh why is the afterPropertiesSet in the super class final???
	 */
	@Override
	public synchronized JAXBContext getJaxbContext() {
		if (getPackagesToScan().length > 0) {
			// We will try *my* way :-)
			final List<Class<?>> annotatedClasses = scanPackages();
			if (annotatedClasses.size() > 0) {
				setClassesToBeBound(annotatedClasses.toArray(new Class<?>[0]));
			}
		}
		return super.getJaxbContext();
	}

}
