//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.spring;

import java.util.concurrent.Callable;

import org.nuclos.common.NuclosFatalException;
import org.springframework.cache.Cache;

/**
 * Spring {@link Cache} adapter implementation on top of a
 * Guava {@link com.google.common.cache.Cache} instance.
 *
 * @author Thomas Pasch
 * @since Nuclos 4.0.18
 *
 * @param <K> key type
 * @param <V> value type
 */
public class GuavaCache<K,V> implements Cache {
	
	private final String name;
	
	private final com.google.common.cache.Cache<K,V> wrapped;
	
	public GuavaCache(String name, com.google.common.cache.Cache<K,V> wrapped) {
		if (name == null || wrapped == null) {
			throw new NullPointerException();
		}
		this.name = name;
		this.wrapped = wrapped;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Object getNativeCache() {
		return wrapped;
	}

	@Override
	public ValueWrapper get(Object key) {
		final V value = wrapped.getIfPresent(key);
		if (value == null) {
			return null;
		}
		return new ValueWrapper() {
			
			@Override
			public Object get() {
				return value;
			}
		};
	}

	// @Override
	public <T> T get(Object key, Class<T> type) {
		return (T) wrapped.getIfPresent(key);
	}

	@Override
	public <T> T get(final Object key, final Callable<T> valueLoader) {
		T result = (T)get(key);
		if (result == null) {
			try {
				result = (T)valueLoader.call();
				put(key, result);
			} catch (Exception e) {
				throw new NuclosFatalException(e);
			}
		}
		return result;
	}

	@Override
	public void put(Object key, Object value) {
		wrapped.put((K) key, (V) value);
	}

	@Override
	public void evict(Object key) {
		wrapped.invalidate(key);
	}

	@Override
	public void clear() {
		wrapped.invalidateAll();
		wrapped.cleanUp();
	}

	/**
	 * Non-atomic implementation based on Google guava Cache.
	 * 
	 * @see http://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/cache/Cache.html
	 */
	// @Override
	public ValueWrapper putIfAbsent(Object key, Object value) {
		final ValueWrapper existingValue = get(key);
		if (existingValue == null) {
			wrapped.put((K) key, (V) value);
			return null;
		} else {
			return existingValue;
		}
	}

}
