package org.nuclos.common.startup;

public class NuclosEnviromentConstants {
	
	/**
	 * ${url.jms} is the replacement variable of the
	 * (HTTP tunneled) connection to the (server) JMS broker.
	 * 
	 * @deprecated Use {@link #SERVER_VARIABLE}.
	 */
	public static final String JMS_VARIABLE = "url.jms";
	
	/**
	 * ${url.remoting} is the replacement variable of the
	 * base URL to the remote services of the nuclos server.
	 * 
	 * @deprecated Use {@link #SERVER_VARIABLE}.
	 */
	public static final String REMOTE_VARIABLE = "url.remoting";
	
	/**
	 * ${server} is the replacement variable of the
	 * base URL of the nuclos server.
	 */
	public static final String SERVER_VARIABLE = "server";

	/**
	 * ${rest} is the replacement variable of the
	 * REST base URL of the nuclos server.
	 */
	public static final String REST_VARIABLE = "rest";

	/**
	 * ${webclient} is the replacement variable of the
	 * base URL to the remote services of the nuclos server.
	 */
	public static final String WEBCLIENT_VARIABLE = "webclient";
	
	public static final String CODEBASE_ENDING = "/app";
	
	public static final String REMOTE_ENDING = "/remoting";
	
	public static final String JMS_ENDING = "/jmsbroker";

	public static final String REST_ENDING = "/rest";

	private NuclosEnviromentConstants() {
		// Never invoked.
	}
}
