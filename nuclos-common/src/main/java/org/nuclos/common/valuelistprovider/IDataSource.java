package org.nuclos.common.valuelistprovider;

import java.util.List;
import java.util.Map;

import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.report.valueobject.CalcAttributeVO;
import org.nuclos.common.report.valueobject.ChartVO;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;

public interface IDataSource {

	/**
	 * get valuelist provider value object
	 *
	 * @param valuelistProviderUid valuelistprovider UID
	 *                             name of valuelist provider
	 * @return valuelist provider value object
	 */
	ValuelistProviderVO getValuelistProvider(UID valuelistProviderUid) throws CommonFinderException, CommonPermissionException;

	List<DatasourceParameterVO> getParametersFromXML(String sDatasourceXML) throws CommonBusinessException;

	DatasourceVO getDatasourceByName(String sDataSource) throws CommonBusinessException;

	List<String> getColumnsFromVLP(UID valuelistProviderUid, Class clazz) throws CommonBusinessException;

	CollectableField getDefaultValue(UID datasource, String valuefield, String idfield, String defaultfield, Map<String, Object> params, UID baseEntityUID, UID mandatorUID, UID languageUID) throws CommonBusinessException;

	ChartVO getChart(UID iChartId) throws CommonFinderException, CommonPermissionException;

	int cacheExpiration();

	List<CollectableValueIdField> executeQueryForVLP(VLPQuery query) throws CommonBusinessException;

	boolean isMandator(UID baseEntityUID);

	CalcAttributeVO getCalcAttribute(UID calcAttributeUid)
			throws CommonFinderException, CommonPermissionException;

}
