//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common2;

import java.math.BigDecimal;

import org.nuclos.common.UID;

/**
 * Temporary Util class to make the primary key transition from Integer to Long
 * more easy.
 *
 * @author Thomas Pasch
 * @since 3.1.01
 * 
 * @deprecated With introduction of UID (and Long) as PK, this is no longer needed.
 */
public class IdUtils {

	private IdUtils() {
		// Never invoked.
	}

	public static Long toLongId(Integer i) {
		if (i == null) return null;
		return Long.valueOf(i.longValue());
	}
	
	public static Long toLongId(Number i) {
		if (i == null) return null;
		return Long.valueOf(i.longValue());
	}

	public static Long toLongId(int i) {
		return Long.valueOf(i);
	}

	public static Long toLongId(Object o) {
		if (o == null) return null;
		if (o instanceof Integer) return toLongId((Integer) o);
		else if (o instanceof Long) return (Long) o;
		else if (o instanceof BigDecimal) return ((BigDecimal) o).longValue();
		else throw new IllegalArgumentException("toLongId(" + o + ")");
	}

	public static Integer unsafeToId(Object o) {
		if (o == null) return null;
		if (o instanceof Integer) return (Integer) o;
		else if (o instanceof Long) return unsafeToId((Long) o);
		else throw new IllegalArgumentException("unsafeToId(" + o + ")");
	}

	public static Object unsafeToIdIfPossible(Object o) {
		if (o == null) return null;
		if (o instanceof Integer) return (Integer) o;
		else if (o instanceof Long) return unsafeToId((Long) o);
		else return o;
	}

	public static Integer unsafeToId(Long id) {
		if (id == null) return null;
		return Integer.valueOf(id.intValue());
	}

	public static Integer unsafeToId(Number id) {
		if (id == null) return null;
		return Integer.valueOf(id.intValue());
	}

	public static Integer unsafeToId(long id) {
		return Integer.valueOf((int) id);
	}

	//Anyway it was a stupid idea to call those method "equals". And those who read this are even more...
	public static boolean areEqual(Long i1, Object i2) {
		if (i1 == null) return i2 == null;
		return i1.equals(toLongId(i2));
	}

	public static boolean areEqual(Integer i1, Object i2) {
		if (i1 == null) return i2 == null;
		return toLongId(i1).equals(toLongId(i2));
	}

	public static boolean areEqual(Number i1, Number i2) {
		if (i1 == null) return i2 == null;
		return toLongId(i1).equals(toLongId(i2));
	}

	public static boolean areEqual(Object i1, Object i2) {
		if (i1 == null) return i2 == null;
		if (i1 instanceof UID) {
			if (i2 instanceof UID) {
				return LangUtils.equal(i1, i2);
			}
			return false;
		}
		if (i1 instanceof Integer) {
			return areEqual((Integer) i1, i2);
		}
		else if (i1 instanceof Long) {
			return areEqual((Long) i1, i2);
		}
		else {
			throw new IllegalArgumentException("IdUtils.areEqual on " + i1 + " (" + i1.getClass().getName()+ ")");
		}
	}

}
