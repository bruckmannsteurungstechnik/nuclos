//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common2.exception;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.nuclos.common.IMetaProvider;
import org.nuclos.common.PointerCollection;
import org.nuclos.common.UID;
import org.nuclos.common.validation.FieldValidationError;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;

/**
 * General exception for validation of values.
 *
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @version	00.01.000
 */
public class CommonValidationException extends CommonBusinessException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2909068236093891908L;

	private Set<String> errors;

	private Set<FieldValidationError> fielderrors;
	
	private String fullErrorMessage;
	private String fullLogMessage;

	public CommonValidationException() {
		super(CommonBusinessException.VALIDATION);
	}

	/**
	 * @param tCause wrapped exception
	 */
	public CommonValidationException(Throwable tCause) {
		super(CommonBusinessException.VALIDATION, tCause);
	}

	/**
	 * @param sMessage exception message
	 */
	public CommonValidationException(String sMessage) {
		super(sMessage, null);
	}

	/**
	 * @param sMessage exception message
	 * @param tCause wrapped exception
	 */
	public CommonValidationException(String sMessage, Throwable tCause) {
		super(sMessage, tCause);
	}

	public CommonValidationException(Set<String> errors, Set<FieldValidationError> fielderrors) {
		super(CommonBusinessException.VALIDATION);
		this.errors = errors;
		this.fielderrors = fielderrors;
	}

	public Set<String> getErrors() {
		return errors;
	}

	@Override
	public String getMessage() {
		if (fullLogMessage != null) {
			return fullLogMessage;
		}
		return super.getMessage();
	}

	public Set<FieldValidationError> getFieldErrors() {
		return fielderrors;
	}

	@Override
	public String toString() {
		if (fullLogMessage != null) {
			return fullLogMessage;
		}
		if (fullErrorMessage != null) {
			return fullErrorMessage;
		}
		return "CommonValidationException [getMessage()=" + getMessage() + ", errors=" + errors + ", fielderrors=" + fielderrors + "]";
	}
	
	/**
	 * 
	 * @param localeDelegate, SpringLocaleDelegate
	 * @param metaProvider, MetaDataProvider
	 * @param mainEntity, String, can be null
	 * @param pointerCollection, PointerCollection, can be null
	 * @return String: Full ErrorMessage in HTML
	 */
	private List<String> getFullMessages(SpringLocaleDelegate localeDelegate, IMetaProvider metaProvider, UID mainEntity, PointerCollection pointerCollection) {
		List<String> messages = new ArrayList<String>();
		messages.add(localeDelegate.getText("common.exception.novabitvalidationexception"));

		if (getErrors() != null) {
			for (String error : getErrors()) {
				messages.add(localeDelegate.getMessageFromResource(error));
			}
		}

		if (getFieldErrors() != null) {
			for (FieldValidationError error : getFieldErrors()) {
				if (!error.getEntity().equals(mainEntity)) {
					messages.add(localeDelegate.getMessage("EntityCollectController.Subform", "Subform \"{0}\": ",
							localeDelegate.getResource(metaProvider.getEntity(error.getEntity()).getLocaleResourceIdForLabel(),
									localeDelegate.getLabelFromMetaDataVO(metaProvider.getEntity(error.getEntity()))))
							+ localeDelegate.getMessageFromResource(error.getMessage()));
				}
				else {
					if (pointerCollection != null) pointerCollection.addEmptyFieldPointer(error.getField());
					messages.add(localeDelegate.getMessageFromResource(error.getMessage()));
				}
			}
		}

		if (messages.size() == 1) {
			messages.add(localeDelegate.getMessageFromResource(getMessage()));
		}

		return messages;
	}


	public String getFullMessage(SpringLocaleDelegate localeDelegate, IMetaProvider metaProvider, UID mainEntity, PointerCollection pointerCollection) {
		List<String> messages = getFullMessages(localeDelegate, metaProvider, mainEntity, pointerCollection);
		String message = StringUtils.concatHtml(true, messages.toArray(new String[messages.size()]));
		if (pointerCollection != null) pointerCollection.setMainPointer(message);
		return message;
	}

	public String getFullMessage() {
		return fullErrorMessage;
	}

	public void setFullMessage(SpringLocaleDelegate localeDelegate, IMetaProvider metaProvider, UID mainEntity) {
		List<String> messages = getFullMessages(localeDelegate, metaProvider, mainEntity, null);
		fullErrorMessage = StringUtils.concatHtml(true, messages.toArray(new String[messages.size()]));
		fullLogMessage = messages.toString();
	}

}
