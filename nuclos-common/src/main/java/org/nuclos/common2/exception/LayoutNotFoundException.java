package org.nuclos.common2.exception;

/**
 * Created by Sebastian Debring on 3/25/2019.
 */
public class LayoutNotFoundException extends CommonBusinessException {

	public LayoutNotFoundException(final String sMessage, final CommonFinderException ex) {
		super(sMessage, ex);
	}
}
