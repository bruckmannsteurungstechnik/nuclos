package org.nuclos.common2.exception;

import org.springframework.security.authentication.CredentialsExpiredException;

/**
 * Created by Oliver Brausch on 02.05.19.
 */
public class NuclosCredentialsExpiredException extends CredentialsExpiredException {
	public NuclosCredentialsExpiredException(String msg) {
		super(msg);
		NuclosExceptions.shortenStackTrace(this, 1);
	}
}
