package org.nuclos.common2.exception;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Created by Oliver Brausch on 02.05.19.
 */
public class NuclosUsernameNotFoundException extends UsernameNotFoundException {
	public NuclosUsernameNotFoundException(String msg) {
		super(msg);
		NuclosExceptions.shortenStackTrace(this, 1);
	}
}
