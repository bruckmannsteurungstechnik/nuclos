package org.nuclos.common2.layoutml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.xml.sax.Attributes;

public abstract class AbstractWebContainer extends AbstractWebComponent implements IWebContainer {
	private final String name;
	private final List<AbstractWebComponent> components = new ArrayList<AbstractWebComponent>();

	protected AbstractWebContainer(Attributes attributes) {
		super(attributes);
		this.name = attributes.getValue(ATTRIBUTE_NAME);
	}

	public String getName() {
		return name;
	}

	@Override
	public void addComponent(AbstractWebComponent comp) {
		components.add(comp);
	}
	
	@Override
	public List<AbstractWebComponent> getComponents() {
		return Collections.unmodifiableList(components);
	}
	
	protected abstract void setWebTableLayout(WebTableLayout layout);
	
	@Override
	public void liftGrandChildren() {
		if (components.size() == 1 && components.get(0) instanceof IWebContainer) {
			IWebContainer subContainer = (IWebContainer)components.get(0);
			components.clear();
			components.addAll(subContainer.getComponents());
			
			if (subContainer instanceof IHasTableLayout) {
				WebTableLayout layout = ((IHasTableLayout)subContainer).getWebTableLayout();
				if (layout != null) {
					setWebTableLayout(layout);
				}
			}
		}	
	}
	
	protected final String componentsString() {
		return components + "";
	}
	
}
