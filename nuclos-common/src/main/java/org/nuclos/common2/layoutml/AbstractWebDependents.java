package org.nuclos.common2.layoutml;

import org.nuclos.common.UID;
import org.xml.sax.Attributes;

public abstract class AbstractWebDependents extends AbstractWebComponent {

	protected AbstractWebDependents(Attributes attributes) {
		super(attributes);
	}
	
	public abstract UID getForeignkeyfield();
	
	public abstract String getServiceIdentifier();

}
