package org.nuclos.common2.layoutml;

import org.nuclos.common.UID;
import org.xml.sax.Attributes;

public class WebStaticComponent extends AbstractWebComponent implements ButtonConstants {
	
	private final UID uid;
	
	WebStaticComponent(String type, Attributes attributes) {
		super(attributes);
		addProperty("type", type);
		
		final String name;
		final String text;
		if (ELEMENT_LABEL.equals(type)) {
			name = attributes.getValue(ATTRIBUTE_NAME);
			text = attributes.getValue(ATTRIBUTE_TEXT);
			
		} else if (ELEMENT_TITLEDSEPARATOR.equals(type)) {
			name = attributes.getValue(ATTRIBUTE_TITLE);
			text = attributes.getValue(ATTRIBUTE_TITLE);

//TOOD: Implement Separator
//		} else if (ELEMENT_SEPARATOR.equals(type)) {
			
		} else if (ELEMENT_BUTTON.equals(type)) {
			name = attributes.getValue(ATTRIBUTE_NAME);
			text = attributes.getValue(ATTRIBUTE_LABEL);
			formatAndAddProperty(ATTRIBUTE_ACTIONCOMMAND, attributes);
			formatAndAddProperty(ATTRIBUTE_ACTIONKEYSTROKE, attributes);
			formatAndAddProperty(ATTRIBUTE_DISABLE_DURING_EDIT, attributes);
			addProperty(ATTRIBUTE_ICON, attributes);

		} else {
			name = "";
			text = "";
			
		}
		
		addProperty("name", name);
		addProperty("text", text);
		this.uid = name != null ? UID.parseUID(name) : UID.UID_NULL;
	}
	
	@Override
	public UID getUID() {
		return uid;
	}
	
	public boolean isButton() {
		return ELEMENT_BUTTON.equals(getProperties().get("type"));
	}

	public boolean isActionSupportedAndCompleteForWeb() {
		String action = getProperties().get(LayoutMLConstants.ATTRIBUTE_ACTIONCOMMAND);
		
		boolean bSupported = isActionSupportedInWeb(action);
		
		if (ACTION_CHANGESTATEBUTTON.equals(action)) {
			bSupported = getProperties().containsKey("targetState");
		}

		return bSupported;
	}
	
	private static boolean isActionSupportedInWeb(String action) {
		boolean bSupported = false;
		
		for (String supported : SUPPORTEDACTIONSINWEB) {
			if (supported.equals(action)) {
				bSupported = true;
			}			
		}
		
		return bSupported;
	}
	
	private void formatAndAddProperty(String key, Attributes attributes) {
		String value = attributes.getValue(key);
		
		if (value != null) {
			int n = value.lastIndexOf('.');
			if (n > -1) {
				value = value.substring(n + 1);				
			}
			
			addProperty(key, value);
		}
	}
	
}