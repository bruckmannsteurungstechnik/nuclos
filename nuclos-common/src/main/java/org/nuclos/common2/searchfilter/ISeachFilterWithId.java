package org.nuclos.common2.searchfilter;

import org.nuclos.common.UID;

public interface ISeachFilterWithId {

	UID getSearchFilterId();
	
}
