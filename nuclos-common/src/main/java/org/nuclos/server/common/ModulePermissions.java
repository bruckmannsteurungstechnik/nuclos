//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common;

import java.io.Serializable;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.nuclos.common.UID;

/**
 * Contains the module permissions for a user, for all modules.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:christoph.radig@novabit.de">christoph.radig</a>
 * @version 01.00.00
 */
public class ModulePermissions implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1290751847819468110L;
	private final Map<UID, ModulePermission> mpByModuleUid;
	private final Map<UID, Boolean> mpNewAllowedByModuleUid;
	private final Map<UID, Set<UID>> mpNewAllowedProcessesByModuleUid;

	public Map<UID, Boolean> getNewAllowedByModuleUid() {
    	return mpNewAllowedByModuleUid;
    }

	/**
	 * Create the module (aka GenericObject) permission object.
	 * This are the permissions for an (individual, pre-selected) user.
	 * 
	 * @param mpByModuleUid
	 * 			Map of Pair((UID) module, (UID) object group) -&gt; ModulePermission.
	 * 			Null of PairX is not allowed.
	 * 			Null for PairY is for 'no object group'.
	 * @param mpNewAllowedByModuleUid
	 * 			Map of ((UID) module) -&gt; Boolean
	 * @param mpNewAllowedProcessesByModuleUid
	 * 			Map of ((UID) module) -&gt; Set of ((UID) processIds/actions).
	 * 			The result set includes 'null' if new is allowed for the default (no action) case.
	 */
	public ModulePermissions(
			Map<UID, ModulePermission> mpByModuleUid,
			Map<UID, Boolean> mpNewAllowedByModuleUid,
			Map<UID, Set<UID>> mpNewAllowedProcessesByModuleUid) {
		this.mpByModuleUid = mpByModuleUid;
		this.mpNewAllowedByModuleUid = mpNewAllowedByModuleUid;
		this.mpNewAllowedProcessesByModuleUid = mpNewAllowedProcessesByModuleUid;
	}

	public Map<UID, ModulePermission> getPermissionsByModuleUid() {
		return mpByModuleUid;
	}

	public Map<UID, Set<UID>> getNewAllowedProcessesByModuleUid() {
    	return mpNewAllowedProcessesByModuleUid;
    }

	/**
	 * @return the maximum right for an entity and the genericobject
	 * 		and ModulePermission.NO for no permissions.
	 */
	public ModulePermission getMaxPermissionForGO(UID entityUID) {
		final ModulePermission result = getMaxPermissionForGenericObject(entityUID);
		return result == null ? ModulePermission.NO : result;
	}

	/**
	 * @return the maximum right for an entity and the genericobject
	 * 		and null for no permissions.
	 */
	public ModulePermission getMaxPermissionForGenericObject(UID entityUID) {
		ModulePermission maxpermission = null;

		for (UID moduleUid : mpByModuleUid.keySet()) {
			// NUCLOS-150: for each iteration, permissionFound must be reseted 
			// to avoid privilege escalation.
			boolean permissionFound = false;
			// get maximum right for entity independant of a genericobject (new data record, general read operation)
			if (moduleUid.equals(entityUID)) {
				permissionFound = true;
			}

			if (permissionFound == true && (maxpermission == null || mpByModuleUid.get(moduleUid).compareTo(maxpermission) > 0)) {
				maxpermission = mpByModuleUid.get(moduleUid);

				if (maxpermission == ModulePermission.DELETE_PHYSICALLY) {
					break;
				}
			}
		}
		return maxpermission;
	}

	Set<Entry<UID, ModulePermission>> getEntries() {
		return this.mpByModuleUid.entrySet();
	}

	@Override
	public String toString() {
		return "ByModuleUid=" + mpByModuleUid
				+"\nNewAllowedByModuleUid=" + mpNewAllowedByModuleUid
				+"\nNewAllowedProcessesByModuleUid=" + mpNewAllowedProcessesByModuleUid;
	}

}	// class ModulePermissions
