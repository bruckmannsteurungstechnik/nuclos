//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common.ejb3;

import java.util.Collection;
import java.util.Map;

import javax.annotation.security.RolesAllowed;

import org.nuclos.api.Settings;
import org.nuclos.common.UID;
import org.nuclos.common.WorkspaceVO;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.PreferencesException;
import org.nuclos.common2.exception.WorkspaceException;
import org.nuclos.server.common.valueobject.PreferencesVO;

public interface CommonPreferencesFacade {

	/**
	 * @return the preferences for the user currently logged
	 */
	@RolesAllowed("Login")
	PreferencesVO getUserPreferences()
		throws CommonFinderException;
	
	/**
	 * @return the preferences for the user defined as template user, exception if not defined in t_ad_parameter
	 */
	@RolesAllowed("Login")
	PreferencesVO getTemplateUserPreferences() throws CommonFinderException;

	/**
	 * stores the preferences for the user currently logged
	 * @param prefsvo the preferences for the user currently logged
	 */
	@RolesAllowed("Login")
	void modifyUserPreferences(PreferencesVO prefsvo)
		throws CommonFinderException;

	/**
	 * sets the preferences for the user with the given name (administrative use only).
	 * @param sUserName
	 * @param prefsvo the preferences for the user currently logged. If <code>null</code>, the preferences for the given user will be reset.
	 * @throws CommonFinderException
	 */
	@RolesAllowed("UseManagementConsole")
	void setPreferencesForUser(String sUserName,
		PreferencesVO prefsvo) throws CommonFinderException;

	/**
	 * sets the preferences for the user with the given name (administrative use only).
	 * @param sUserName
	 * @throws CommonFinderException
	 */
	@RolesAllowed("UseManagementConsole")
	PreferencesVO getPreferencesForUser(String sUserName)
		throws CommonFinderException;

	/**
	 * Merges the given preferences into the preferences for the user with the given name (administrative use only).
	 * @throws CommonFinderException
	 */
	@RolesAllowed("UseManagementConsole")
	void mergePreferencesForUser(String name, Map<String, Map<String, String>> preferencesToMerge)
		throws CommonFinderException;
	
	Collection<WorkspaceVO> getWorkspaceHeaderOnly();
	
	WorkspaceVO getWorkspace(final UID id) throws WorkspaceException;
		
	WorkspaceVO storeWorkspace(WorkspaceVO wovo) throws PreferencesException;
	
	void storeWorkspaceHeaderOnly(WorkspaceVO wovo) throws PreferencesException;
	
	void removeWorkspace(final UID id);
	
	/**
	 * 
	 * @param wovo 
	 * 			private or customized workspace
	 * @param roleUids
	 * @throws CommonBusinessException 
	 * return private or customized workspace
	 */
	WorkspaceVO assignWorkspace(WorkspaceVO wovo, Collection<UID> roleUids)
		throws WorkspaceException;
	
	Collection<EntityObjectVO<UID>> getAssignableRoles();
	
	Collection<UID> getAssignedRoleIds(UID assignedWorkspaceUid);

	void publishWorkspaceChanges(WorkspaceVO customizedWovo, boolean isPublishStructureChanged, 
			boolean isPublishStructureUpdate, boolean isPublishStarttabConfiguration, 
			boolean isPublishToolbarConfiguration)
			throws WorkspaceException;

	WorkspaceVO restoreWorkspace(WorkspaceVO customizedWovo) throws WorkspaceException;

	boolean isWorkspaceStructureChanged(UID uid1, UID uid2) throws WorkspaceException;

	Map<String, Settings> getApiUserSettings();

	void setApiUserSettings(Map<String, Settings> userSettings);

	Settings getApiUserSettings(String key);

	void setApiUserSettings(String key, Settings userSettings);

}
