//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dbtransfer;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.UID;
import org.nuclos.common.dbtransfer.NucletContentMap;
import org.nuclos.common.dbtransfer.Transfer;
import org.nuclos.common.dbtransfer.TransferEO;
import org.nuclos.common.dbtransfer.TransferNuclet;
import org.nuclos.common.dbtransfer.TransferOption;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.navigation.treenode.nuclet.content.AbstractNucletContentEntryTreeNode;

// @Remote
public interface TransferFacadeRemote {

	/**
	 * creates a file for configuration transfer.
	 *
	 * @param exportOptions table filters
	 * @return the file content as byte array
	 * @throws NuclosBusinessException 
	 */
	byte[] createTransferFile(UID uid, Map<TransferOption, Serializable> exportOptions) throws NuclosBusinessException;
	
	/**
	 * 
	 * @param nuclet
	 * @param nucletContentMap
	 * @return
	 * @throws NuclosBusinessException
	 */
	byte[] createTransferFile(TransferEO nuclet, final NucletContentMap nucletContentMap) throws NuclosBusinessException;

	/**
	 * @param bytes the content of a transfer file
	 * @param fileName used for nuclet archive
	 * @param bUseNewImportWithChangesTree
	 * @return a <code>Transfer</code> object describing how the
	 * current configuration would change if the transfer is executed
	 * @throws NuclosBusinessException 
	 */
	Transfer prepareTransfer(byte[] bytes, String fileName, final boolean bUseNewImportWithChangesTree) throws NuclosBusinessException;

	/**
	 * execute a transfer
	 *
	 * @param transfer
	 * @return a message object informing the client about success or failure
	 * @throws NuclosBusinessException 
	 */
	Transfer.Result runTransfer(Transfer transfer) throws NuclosBusinessException;
	
	String getDatabaseType();
	
	List<TransferNuclet> getAvailableNuclets();

	Map<TransferOption, Serializable> getOptions(byte[] bytes) throws NuclosBusinessException;
	
	void updateNucletContents(UID nuclet, Set<AbstractNucletContentEntryTreeNode> contentsToAdd, Set<AbstractNucletContentEntryTreeNode> contentsToRemove) throws NuclosBusinessException, CommonPermissionException;
	
	void addNucletContents(UID nuclet, Set<AbstractNucletContentEntryTreeNode> contents) throws CommonPermissionException, NuclosBusinessException;
	
	void removeNucletContents(Set<AbstractNucletContentEntryTreeNode> contents) throws CommonPermissionException, NuclosBusinessException;

	UID getNucletUIDFromMetaDataRoot(byte[] bytes, boolean bCheckTransferVersion) throws NuclosBusinessException;

	void revalidateCaches();
	
}
