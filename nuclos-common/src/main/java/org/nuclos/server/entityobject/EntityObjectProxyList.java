//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.entityobject;

import java.rmi.RemoteException;
import java.util.Collection;

import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.common.ejb3.EntityObjectFacadeRemote;
import org.nuclos.server.genericobject.AbstractProxyList;
import org.nuclos.server.genericobject.ProxyListProvider;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;

/**
 * Proxy list for entity object search results.
 * <p>
 * Created by Novabit Informationssysteme GmbH
 * </p><p>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * </p>
 * @author Thomas Pasch
 * @since Nuclos 3.1.01
 */
public class EntityObjectProxyList<PK> extends AbstractProxyList<PK, EntityObjectVO<PK>> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -888372203683177573L;

	private static EntityObjectFacadeRemote FACADE;

	private final String customUsage;
	
	public EntityObjectProxyList(UID entity, CollectableSearchExpression clctexpr, Collection<UID> fields, ProxyListProvider plProvider, String customUsage) {
		super(entity, clctexpr, fields, plProvider);
		this.customUsage = customUsage;
		
		this.initialize();
	}
	
	protected void initialize() {
		proxyListProvider.addEssentialFields(fields, entity);
		super.initialize();
	}
	
	@Override
	protected Collection<EntityObjectVO<PK>> fetchChunk(ResultParams resultParams) throws RemoteException, CommonPermissionException {
		return this.getEntityObjectFacade().getEntityObjectsChunk(this.entity, this.clctexpr, resultParams, this.customUsage);
	}

	@Override
	protected Integer countMasterDataRows(final Long limit) throws RemoteException, CommonPermissionException {
		if (limit == null) {
			return this.getEntityObjectFacade().countEntityObjectRows(this.entity, this.clctexpr).intValue();
		}
		return this.getEntityObjectFacade().countEntityObjectRowsWithLimit(this.entity, this.clctexpr, limit).intValue();
	}
	
	@Override
	protected Object getValue(EntityObjectVO<PK> obj, UID field) {
		return obj.getFieldValue(field);
	}
	
	private EntityObjectFacadeRemote getEntityObjectFacade() {
		if (FACADE == null) {
			try {
				FACADE = SpringApplicationContextHolder.getBean(EntityObjectFacadeRemote.class);
			}
			catch (RuntimeException ex) {
				throw new NuclosFatalException(ex);
			}
		}
		return FACADE;
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		final int size = size();
		result.append("EntityObjectProxyList[");
		result.append("size=").append(size);
		result.append(",reqFields=").append(fields);
		result.append(",search=").append(clctexpr);
		mapDescription(result, index2Loaded, 5);
		result.append("]");
		return result.toString();
	}

	@Override
	protected boolean blockSorting() {
		return false;
	}	
}
