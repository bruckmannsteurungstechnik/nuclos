//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.eventsupport.valueobject;

import org.nuclos.common.UID;
import org.nuclos.server.common.valueobject.NuclosValueObject;

public class CommunicationPortVO extends NuclosValueObject<UID> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9107680601910125607L;
	private String  sName;
	private String  sDescription;
	
	public CommunicationPortVO(NuclosValueObject<UID> nvo, String sName, String sDescription) {
		super(nvo);
		
		this.sDescription = sDescription;
		this.sName = sName;	
	}

	
	public String getName() {
		return sName;
	}

	public String getDescription() {
		return sDescription;
	}

}
