package org.nuclos.server.eventsupport.valueobject;

import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.valueobject.NuclosValueObject;

public class EventSupportGenerationVO extends EventSupportVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7704461345060505258L;
	
	private UID generationUID;
	
	
	public EventSupportGenerationVO(NuclosValueObject<UID> nvo, Integer pOrder, UID pGenerationUID, String pEventSupportClass, String pEventSupportType) {
		super(nvo, pOrder, pEventSupportClass, pEventSupportType);
	
		this.generationUID = pGenerationUID;
	}
	
	public EventSupportGenerationVO(EventSupportVO eseVO, UID pGenerationUID) {
		super(eseVO, eseVO.getOrder(), eseVO.getEventSupportClass(), eseVO.getEventSupportClassType());
		this.generationUID = pGenerationUID;
	}
	
	public EventSupportGenerationVO(Integer pOrder, UID pGenerationUID, String pEventSupportClass, String pEventSupportType) {
		super(pOrder, pEventSupportClass, pEventSupportType);
	
		this.generationUID = pGenerationUID;
	}
	
	public UID getGeneration() {
		return generationUID;
	}

	public void setGeneration(UID pGenerationUID) {
		this.generationUID = pGenerationUID;
	}

	/**
	 * validity checker
	 */
	@Override
	public void validate() throws CommonValidationException {
		
		super.validate();
		
		if (getGeneration() == null) {
			throw new CommonValidationException("ruleengine.error.validation.rule.name");
		}	
	}
	
	public EventSupportGenerationVO clone() {
		return new EventSupportGenerationVO(super.clone(), this.getGeneration());
	}
	
}
