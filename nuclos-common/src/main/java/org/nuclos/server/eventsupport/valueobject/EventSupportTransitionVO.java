package org.nuclos.server.eventsupport.valueobject;

import org.apache.commons.lang.SerializationUtils;
import org.nuclos.common.UID;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.valueobject.NuclosValueObject;

/**
 * @author reichama
 * @version 00.01.000
 */
public class EventSupportTransitionVO extends EventSupportVO
{	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8013640325861467820L;
	private UID transition;
	private String  sTransitionName;
	
	public EventSupportTransitionVO(NuclosValueObject<UID> nvo, String sEventSupportClass, String pEventSupportType, 
			UID transition, Integer iOrder) {
		super(nvo, iOrder, sEventSupportClass, pEventSupportType);
		this.transition = transition;
	}

	public EventSupportTransitionVO(String sEventSupportClass,String pEventSupportType,
			UID transition, Integer iOrder) {
		super(iOrder, sEventSupportClass, pEventSupportType);
		this.transition = transition;
	}

	@Override
	public String toString() {
		return " ID: " + this.getId() + " Transition: " + this.getTransition()
				+ " ES-Class: " + this.getEventSupportClass();
	}

	public UID getTransition() {
		return transition;
	}

	public void setTransitionId(UID transition) {
		this.transition = transition;
	}

	public String getTransitionName() {
		return sTransitionName;
	}

	public void setTransitionName(String sTransitionName) {
		this.sTransitionName = sTransitionName;
	}
	
	/**
	 * validity checker
	 */
	@Override
	public void validate() throws CommonValidationException {
		super.validate();
		if (getTransition() == null) {
			throw new CommonValidationException("ruleengine.error.validation.rule.name");
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof EventSupportTransitionVO) {
			return hashCode() == obj.hashCode();
		}
		return false;
	}
	 
	@Override
	public int hashCode() {
		return LangUtils.hashCode(transition) ^ LangUtils.hashCode(getEventSupportClass());
	}

	public EventSupportTransitionVO deepClone() {
		return (EventSupportTransitionVO) SerializationUtils.clone(this);
	}

}
