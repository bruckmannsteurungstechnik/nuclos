package org.nuclos.server.genericobject.searchcondition;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;

public class ResultParams implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -525770922219591470L;

	private static final Long ROWCOUNTLIMIT = 250000L;
	
	private final Collection<UID> fields;
	private final Long lOffset;
	private final Long lLimit;
	private final boolean bSortResult;
	private final List<CollectableSearchCondition> anchor;

	private boolean bLoadThumbnailsOnly;

	private boolean bIdOnlySelection = false;

	public ResultParams(Collection<UID> fields, Long lOffset, Long lLimit, boolean bSortResult) {
		this.fields = fields;
		this.lOffset = lOffset;
		this.lLimit = lLimit;
		this.bSortResult = bSortResult;
		this.anchor = new ArrayList<CollectableSearchCondition>();
	}

	public ResultParams(final Collection<UID> fields, final Long lOffset, final Long lLimit, final boolean bSortResult, final boolean bLoadThumbnailsOnly) {
		this(fields, lOffset, lLimit, bSortResult);
		this.bLoadThumbnailsOnly = bLoadThumbnailsOnly;
	}

	public ResultParams(Long lLimit, boolean bSortResult) {
		this(null, null, lLimit, bSortResult);
	}

	public ResultParams(Collection<UID> fields) {
		this(fields, null, ROWCOUNTLIMIT, false);
	}

	public Collection<UID> getFields() {
		return fields;
	}

	public Long getOffset() {
		return lOffset;
	}

	public Long getLimit() {
		return lLimit;
	}

	public boolean isSortResult() {
		return bSortResult;
	}

	public List<CollectableSearchCondition> getAnchor() {
		return anchor;
	}

	public boolean isIdOnlySelection() {
		return bIdOnlySelection;
	}

	public void setIdOnlySelection(final boolean bIdOnlySelection) {
		this.bIdOnlySelection = bIdOnlySelection;
	}

	public boolean loadThumbnailsOnly() {
		return bLoadThumbnailsOnly;
	}

	public static ResultParams DEFAULT_RESULT_PARAMS = new ResultParams(null, false);

}
