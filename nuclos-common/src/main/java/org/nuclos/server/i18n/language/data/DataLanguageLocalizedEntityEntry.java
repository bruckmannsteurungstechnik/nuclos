package org.nuclos.server.i18n.language.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.InternalTimestamp;

public class DataLanguageLocalizedEntityEntry extends EntityObjectVO implements
		Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8166434797927862132L;
	public UID FIELD_LANGUGAE;
	public UID FIELD_ENTITY_ENTRY;
	public List<UID> FIELDS_LOCALIZED_FIELDS;
	
	private EntityMeta<?> boMeta;
	
	public DataLanguageLocalizedEntityEntry(EntityMeta<?> boMeta,
								   Long referencedEntityEntry, 
								   UID referencedDataLanguage,
								   Map<UID, Object> mpFieldValues) {
		
		super(getDataLanguageMeta(boMeta));
		
		setCreatedAt(InternalTimestamp.toInternalTimestamp(new Date()));
		setChangedAt(InternalTimestamp.toInternalTimestamp(new Date()));
		setCreatedBy("nuclos");
		setChangedBy("nuclos");
		setVersion(1);
		
		initMetaStructure(boMeta.getUID());
		
		this.boMeta = boMeta;
		
		FIELDS_LOCALIZED_FIELDS = new ArrayList<UID>();
		
		// set values of all fields
		for (UID field : mpFieldValues.keySet()) {
			setFieldValue(field, mpFieldValues.get(field));		
			FIELDS_LOCALIZED_FIELDS.add(field);
		}
		
		setFieldId(this.FIELD_ENTITY_ENTRY, referencedEntityEntry);
		setFieldUid(this.FIELD_LANGUGAE, referencedDataLanguage);
	
		flagNew();
		
	}
	
	private void initMetaStructure(UID entityUID) {
		this.FIELD_LANGUGAE = DataLanguageUtils.extractDataLanguageReference(entityUID);
		this.FIELD_ENTITY_ENTRY = DataLanguageUtils.extractForeignEntityReference(entityUID);
		
	}

	public DataLanguageLocalizedEntityEntry(EntityMeta<?> boMeta, EntityObjectVO<?> eovo) {
		
		super(getDataLanguageMeta(boMeta));
		initMetaStructure(boMeta.getUID());
		
		this.boMeta = boMeta;
		
		setPrimaryKey(eovo.getPrimaryKey());
		
		setVersion(eovo.getVersion());
		setChangedAt(eovo.getChangedAt());
		setChangedBy(eovo.getChangedBy());
		setCreatedAt(eovo.getCreatedAt());
		setCreatedBy(eovo.getCreatedBy());
		
		for (UID field : eovo.getFieldValues().keySet()) {
			setFieldValue(field, eovo.getFieldValues().get(field));
		}
		setFieldId(this.FIELD_ENTITY_ENTRY, eovo.getFieldId(this.FIELD_ENTITY_ENTRY));
		setFieldUid(this.FIELD_LANGUGAE, eovo.getFieldUid(this.FIELD_LANGUGAE));
		
		reset();
	}
	
	public String getValue(UID field) {
		return this.getFieldValue(field, String.class) != null ? 
				(String) this.getFieldValue(field, String.class) : null;
	}
	
	public Map<UID, String> getValueMap() {
		return this.getFieldValues();
	}
	
	public void setValue(UID field, String value) {
		this.setFieldValue(field, value);
		if (!isFlagRemoved() && !isFlagNew()) flagUpdate();
	}
	
	public boolean isInitialValueSet(UID field) {
		UID fieldFlagged = DataLanguageUtils.extractFlaggedFieldFromLangField(field);
		return getFieldValue(fieldFlagged) != null ? 
				(Boolean) getFieldValue(fieldFlagged) : Boolean.FALSE;
	}
	
	public void setEntityEntryPrimaryKey(Long value) {
		this.setFieldId(this.FIELD_ENTITY_ENTRY, value);
	}
	
	public Long getEntityEntryPrimaryKey() {
		return this.getFieldId(this.FIELD_ENTITY_ENTRY);
	}
	
	public void setLanguage(UID value) {
		this.setFieldUid(this.FIELD_LANGUGAE, value);
	}
	
	public UID getLanguage() {
		return this.getFieldUid(this.FIELD_LANGUGAE);
	}
	
	public DataLanguageLocalizedEntityEntry copy() {
		return this.copy(false);
	}
	
	public DataLanguageLocalizedEntityEntry clone() {
		return this.copy(true);
	}
	
	private DataLanguageLocalizedEntityEntry copy(boolean isCloneWithKeys) {
		DataLanguageLocalizedEntityEntry copy = 
				new DataLanguageLocalizedEntityEntry(this.boMeta, this);
		
		if (!isCloneWithKeys) {
			copy.setPrimaryKey(null);
			copy.setEntityEntryPrimaryKey(null);
			copy.flagNew();			
		}
		
		return copy;
	}

	public void resetInitialFlags(Collection<FieldMeta<?>> fields) {
		for(FieldMeta field : fields) {
			UID fieldFlagged = DataLanguageUtils.extractFieldUID(field.getUID(), true);
			setFieldValue(fieldFlagged, Boolean.TRUE);
		}
	}
	
	private static EntityMeta<?> getDataLanguageMeta(EntityMeta<?> boMeta) {
		return getMetaProvider().getEntity(NucletEntityMeta.getEntityLanguageUID(boMeta));
	}
}
