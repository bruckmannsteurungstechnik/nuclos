//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.masterdata.ejb3;

import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.exception.CommonBusinessException;

// @Remote
public interface EntityFacadeRemote {

	@RolesAllowed("Login")
	Map<EntityAndField, UID> getSubFormEntityAndParentSubFormEntityNames(UID layoutUID);

	/**
	 * §todo this method should be used in CollectableFieldsProviders
	 *
	 * @return list of collectable fields
	 */
	@RolesAllowed("Login")
	List<CollectableField> getCollectableFieldsByName(
			final CollectableFieldsByNameParams params
	) throws CommonBusinessException;

	@RolesAllowed("Login")
	List<CollectableValueIdField> getQuickSearchResult(
			UID field,
			String search,
			UID vlpUID,
			Map<String, Object> vlpParameter,
			Long iMaxRowCount,
			UID mandator
	) throws CommonBusinessException;

	@RolesAllowed("Login")
	List<CollectableValueIdField> getQuickSearchResult(
			FieldMeta<?> efMeta,
			String search,
			UID vlpUID,
			Map<String, Object> vlpParameter,
			Long iMaxRowCount,
			UID mandator
	) throws CommonBusinessException;


	/**
	 * Get the base entity name of a dynamic entity.
	 *
	 * @param dynamicentityUID The UID of the dynamic entity.
	 * @return Returns the base entity name. Returns the original entity name if there is no dynamic entity with the given name.
	 */
	@RolesAllowed("Login")
	UID getBaseEntity(UID dynamicentityUID);

}
