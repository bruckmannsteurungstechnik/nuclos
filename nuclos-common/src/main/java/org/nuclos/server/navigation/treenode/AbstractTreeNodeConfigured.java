//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.navigation.treenode;

import java.util.HashMap;
import java.util.Map;

import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.metadata.TreeMetaProvider;
import org.nuclos.common2.LangUtils;


public abstract class AbstractTreeNodeConfigured<Id> extends AbstractTreeNode<Id> implements IGroupableNode, Comparable<AbstractTreeNodeConfigured<Id>> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3800947895904207464L;
	private final UID uidNode;
	private Map<UID, Object> groupValues;
	private boolean forceLeaf;

	public AbstractTreeNodeConfigured(final Id id, final UID uidNode) {
		super(id);
		this.uidNode = uidNode;
	}

	public TreeMetaProvider getTreeprovider() {
		//return treeprovider;
		return (TreeMetaProvider)SpringApplicationContextHolder.getBean("treeService");

	}

	public UID getNodeId() {
		return uidNode;
	}
	
	public boolean allowsChildren() {
		return !forceLeaf;
	}
	
	public void setForceLeaf(boolean b) {
		this.forceLeaf = b;
	}

	public String getDescription(final Map<UID, Object> mpReadableFields, final Long idNode, UID language) {
		String result = "";
		if (null != idNode) {
			result = getTreeprovider().getNodeDescription(uidNode, mpReadableFields, language);
		}
		return result;
	}
	
	@Override
	public int compareTo(AbstractTreeNodeConfigured<Id> that) {
		return LangUtils.compareComparables(this.getLabel(), that.getLabel());
	}

	@Override
	public void setGroupingValue(UID field, Object value) {
		if (groupValues == null) {
			groupValues = new HashMap<UID, Object>();
		}
		groupValues.put(field, value);
	}

	@Override
	public Object getGroupingValue(UID field) {
		if (groupValues != null) {
			return groupValues.get(field);
		}
		return null;
	}
	
}
