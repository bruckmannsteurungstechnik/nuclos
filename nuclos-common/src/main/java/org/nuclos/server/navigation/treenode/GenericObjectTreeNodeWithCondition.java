package org.nuclos.server.navigation.treenode;

import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;

public class GenericObjectTreeNodeWithCondition extends GenericObjectTreeNode implements
		ITreeNodeWithGenericObjectTreeNodeParameters {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2725020083644442158L;
	private CollectableSearchCondition cond;	
	private String nodeType;
	
	public GenericObjectTreeNodeWithCondition(GenericObjectTreeNodeParameters params) {
		super(params);
		this.cond = params.getCondition();
		this.nodeType = params.getNodeType();
	}

	public CollectableSearchCondition getCondition() {
		return cond;
	}

	public void setCondition(CollectableSearchCondition cond) {
		this.cond = cond;
	}
	
	public String getNodeType() {
		return nodeType;
	}
	
	public void setNodeType(String nodeType) {
		this.nodeType = nodeType;
	}
	
	@Override
	public GenericObjectTreeNodeParameters getGenericObjectTreeNodeParameters() {
		GenericObjectTreeNodeParameters params = super.getGenericObjectTreeNodeParameters();
		params.setCondition(cond);
		params.setNodeType(nodeType);
		return params;
	}
}
