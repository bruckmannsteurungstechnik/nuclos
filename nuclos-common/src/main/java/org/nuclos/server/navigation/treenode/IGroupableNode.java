package org.nuclos.server.navigation.treenode;

import org.nuclos.common.UID;

public interface IGroupableNode {
	
	Object getGroupingValue(UID field);
	
	void setGroupingValue(UID field, Object value);

}
