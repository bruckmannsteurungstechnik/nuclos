package org.nuclos.server.navigation.treenode;

import java.rmi.RemoteException;
import java.util.List;

public class LabelTreeNode<Id> extends AbstractStaticTreeNode<Id> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3778764499980249387L;
	private List<? extends TreeNode> lstSubNodes;

	public LabelTreeNode(Id id, String sLabel, String sDescription, List<? extends TreeNode> lstSubNodes) {
		super(id, sLabel, sDescription);
		this.lstSubNodes = lstSubNodes;
	}

	@Override
	public String getLabel() {
		return super.getLabel();
	}
	
	@Override
	public String getDescription() {
		return super.getDescription();
	}
	
	@Override
	protected List<? extends TreeNode> getSubNodesImpl() throws RemoteException {
		return lstSubNodes;
	}

}
