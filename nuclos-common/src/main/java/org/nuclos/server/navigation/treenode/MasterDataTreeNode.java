//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.navigation.treenode;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Tree node implementation representing a master data object.
 * 
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @version 00.01.000
 */
public abstract class MasterDataTreeNode<Id> extends AbstractTreeNodeConfigured<Id> {


	/**
	 * 
	 */
	private static final long serialVersionUID = 8463507256064972820L;
	
	private final UID entity;

	/**
	 * @param id id of tree node
	 */
	public MasterDataTreeNode(UID entity, Id id, String label, String description) {
		super(id, null);
		this.entity = entity;
	}
	
	@Override
	public UID getEntityUID() {
		return this.entity;
	}

	protected String getIdentifier(MasterDataVO<Id> mdVO, UID language) {
		final IMetaProvider metaProvider = SpringApplicationContextHolder.getBean(IMetaProvider.class);
		return getSpringLocaleDelegate().getTreeViewLabel(mdVO.getFieldValues(), mdVO.getEntityObject().getDalEntity(), metaProvider, language);
	}

	protected String getDescription(MasterDataVO<Id> mdVO, UID language) {
		final IMetaProvider metaProvider = SpringApplicationContextHolder.getBean(IMetaProvider.class);
		return getSpringLocaleDelegate().getTreeViewDescription(mdVO.getFieldValues(), mdVO.getEntityObject().getDalEntity(), metaProvider, language);
	}

	@Override
	public UID getNodeId() {
		throw new NotImplementedException("getNodeId not implemented for " + this);
	}

	@Override
	public void cancel() {
		getEntityObjectFacade().cancelRunningStatements(getEntityUID());
	}
} // class MasterDataTreeNode
