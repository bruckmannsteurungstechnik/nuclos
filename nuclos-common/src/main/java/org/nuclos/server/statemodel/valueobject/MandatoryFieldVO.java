//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.statemodel.valueobject;

import org.apache.commons.lang.SerializationUtils;
import org.nuclos.common.UID;
import org.nuclos.server.common.valueobject.NuclosValueObject;

/**
 * Value object representing a mandatory entity field.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @version 01.00.00
 */
public class MandatoryFieldVO extends NuclosValueObject<UID> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 784507800328350775L;
	private UID field;
	private UID state;

	/**
	 * constructor
	 * @param field uid of entity field
	 * @param state uid of state entity
	 */
	public MandatoryFieldVO(UID field, UID state) {
		super();
		this.field = field;
		this.state = state;
	}

	/**
	 * constructor
	 * @param evo contains the common fields
	 * @param field uid of entity field
	 * @param state uid of state
	 */
	public MandatoryFieldVO(NuclosValueObject<UID> evo, UID field, UID state) {
		super(evo);
		this.field = field;
		this.state = state;
	}

	/**
     * @return the field uid
     */
    public UID getField() {
    	return field;
    }

	/**
     * @param field the field uid to set
     */
    public void setField(UID field) {
    	this.field = field;
    }

	/**
	 * get state uid
	 * @return state uid
	 */
	public UID getState() {
		return state;
	}

	/**
	 * set state uid
	 * @param state uid
	 */
	public void setState(UID state) {
		this.state = state;
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("uid=").append(getId());
		result.append(",field=").append(getField());
		result.append(",state=").append(getState());
		result.append("]");
		return result.toString();
	}

	public MandatoryFieldVO deepClone() {
		return (MandatoryFieldVO) SerializationUtils.clone(this);
	}

}
