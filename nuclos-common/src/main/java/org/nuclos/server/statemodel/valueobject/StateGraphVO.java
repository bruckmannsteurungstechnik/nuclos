//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.statemodel.valueobject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.SerializationUtils;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonValidationException;

/**
 * Value object representing a complete state model with all its states and
 * transitions. <br>
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * <p>
 * TODO: This should be cached and NOT be transfered between server and client.
 * Instead, it should be re-constructed on the client side from information
 * available from (client) caches. (tp)
 * <p>
 * @author <a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @version 01.00.00
 */
public class StateGraphVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -281998091892861409L;

	private final StateModelVO statemodel;

	private Set<StateVO> mpStates = new HashSet<>();

	private Set<StateTransitionVO> mpTransitions = new HashSet<>();

	public StateGraphVO() {
		this(null);
	}

	public StateGraphVO(StateModelVO statemodelvo) {
		this.statemodel = statemodelvo;
	}

	public StateGraphVO deepClone() {
		return (StateGraphVO) SerializationUtils.clone(this);
	}
	
	/**
	 * Return if this StateGraphVO is not (yet) valid/complete.
	 * 
	 * @return true if the state graph represented is not (yet) valid/complete.
	 */
	public boolean isEmpty() {
		if (statemodel == null || mpStates == null || mpTransitions == null) {
			return true;
		}
		return mpStates.isEmpty() || mpTransitions.size() <= 1; 
	}

	/**
	 * get state model of graph
	 * 
	 * @return state model of graph
	 */
	public StateModelVO getStateModel() {
		return this.statemodel;
	}

	/**
	 * get states of graph
	 * 
	 * @return states of graph
	 */
	public Set<StateVO> getStates() {
		return this.mpStates;
	}

	/**
	 * set states of graph
	 * 
	 * @param states
	 *            states of graph
	 */
	public void setStates(Set<StateVO> states) {
		// this.mpStates = states;
		this.mpStates = new HashSet<StateVO>();
		for (StateVO state : states) {
			if (state != null)
				this.mpStates.add(state);
		}
	}

	/**
	 * get transitions of graph
	 * 
	 * @return transitions of graph
	 */
	public Set<StateTransitionVO> getTransitions() {
		return this.mpTransitions;
	}

	/**
	 * set transitions of graph
	 * 
	 * @param transitions
	 *            transitions of graph
	 */
	public void setTransitions(Set<StateTransitionVO> transitions) {
		this.mpTransitions = transitions;
	}

	/**
	 * validites this object.
	 */
	public void validate() throws CommonValidationException {
		/** @todo shouldn't it be: StateGraphVO.isRemoved()? */
		if (!this.getStateModel().isRemoved()) {
			if (StringUtils.isNullOrEmpty(this.getStateModel().getName())) {
				throw new CommonValidationException(
						"statemachine.error.validation.graph.modelname");
			}
			// NUCLOSINT-600
			/*
			 * if
			 * (StringUtils.isNullOrEmpty(this.getStateModel().getDescription(
			 * ))) { throw new CommonValidationException(
			 * "statemachine.error.validation.graph.modeldescription"); }
			 */

			/** @todo validate StateModelLayout! */

			this.validateTransitions(this.validateStates());
		}
	}

	/**
	 * validates the states and builds a map for checkTransitions.
	 * 
	 * @return Map<Integer iClientStateId, String sStateName>
	 * @throws CommonValidationException
	 */
	private Map<UID, Pair<String,String>> validateStates() throws CommonValidationException {
		final Map<UID, Pair<String,String>> result = CollectionUtils.newHashMap();
		final Map<Integer, Pair<String,String>> numerals = CollectionUtils.newHashMap();
		for (StateVO statevo : this.getStates()) {
			if (!statevo.isRemoved()) { // ignore removed states
				if (StringUtils.isNullOrEmpty(statevo.getStatename(Locale.GERMAN))) {
					throw new CommonValidationException(
							"statemachine.error.validation.graph.statename");
				}
				if (StringUtils.isNullOrEmpty(statevo.getStatename(Locale.ENGLISH))) {
					throw new CommonValidationException(
							"statemachine.error.validation.graph.statename");
				}
				
				if (statevo.getNumeral() == null) {
					throw new CommonValidationException(
							StringUtils
									.getParameterizedExceptionMessage(
											"statemachine.error.validation.graph.statenumeral",
											statevo.getStatename(Locale.ENGLISH)));
				}
				/**
				 * numeral is set as maximum 3,0 in the database, everything
				 * higher than 999 will cause a exception
				 */
				if (statevo.getNumeral().intValue() > 999 || statevo.getNumeral().intValue()<0) {
					throw new CommonValidationException(
							StringUtils
									.getParameterizedExceptionMessage(
											"statemachine.error.validation.graph.statenumeral.invalid",
											statevo.getStatename(Locale.ENGLISH)));
				}

				if (result.containsKey(statevo.getClientUID())) {
					throw new CommonValidationException(
							"statemachine.error.validation.graph.duplicateid");
				}
				if (result.containsValue(statevo.getStatename(Locale.ENGLISH))) {
					throw new CommonValidationException(
							"statemachine.error.validation.graph.duplicatestate");
				}
				if (result.containsValue(statevo.getStatename(Locale.GERMAN))) {
					throw new CommonValidationException(
							"statemachine.error.validation.graph.duplicatestate");
				}
				if (numerals.containsKey(statevo.getNumeral())) {
					throw new CommonValidationException(
							StringUtils
									.getParameterizedExceptionMessage(
											"statemachine.error.validation.graph.duplicatenumerals",
											numerals.get(statevo.getNumeral()),
											statevo.getStatename(Locale.ENGLISH)));
				}
				result.put(statevo.getClientUID(), new Pair<String, String>(statevo.getStatename(Locale.GERMAN), statevo.getStatename(Locale.ENGLISH)));
				numerals.put(statevo.getNumeral(), new Pair<String, String>(statevo.getStatename(Locale.GERMAN), statevo.getStatename(Locale.ENGLISH)));
			}
		}
		return result;
	}

	/**
	 * validates the transitions.
	 * 
	 * @param mpStates
	 *            Map<Integer iClientStateId, String sStateName>
	 * @throws CommonValidationException
	 */
	private void validateTransitions(Map<UID, Pair<String, String>> mpStates)
			throws CommonValidationException {
		int iStartTransitionCount = 0;
		for (StateTransitionVO statetransitionvo : this.getTransitions()) {
			// ignore deleted transitions
			if (!statetransitionvo.isRemoved()) {
				if (statetransitionvo.getStateSourceUID() == null) {
					// we have a start transition here:
					++iStartTransitionCount;
					if (!statetransitionvo.isAutomatic()) {
						statetransitionvo.setAutomatic(true);
					}
				} else {
					if (!mpStates.containsKey(statetransitionvo
							.getStateSourceUID())) {
						throw new CommonValidationException(
								"statemachine.error.validation.graph.invalidstart");
					}
				}
				if (!mpStates
						.containsKey(statetransitionvo.getStateTargetUID())) {
					throw new CommonValidationException(
							"statemachine.error.validation.graph.invalidend");
				}
				if (LangUtils.equal(statetransitionvo.getStateTargetUID(),
						statetransitionvo.getStateSourceUID())) {
					throw new CommonValidationException(
							"statemachine.error.validation.graph.startequalsend");
				}

				this.checkDuplicateTransition(statetransitionvo);
				this.checkDuplicateDefaultTransition(statetransitionvo);
				this.checkDefaultPathNotStartsFromInitialTransition(statetransitionvo);
			}
		}

		if (iStartTransitionCount == 0) {
			throw new CommonValidationException(
					"statemachine.error.validation.graph.nostartstate");
		}
		if (iStartTransitionCount > 1) {
			throw new CommonValidationException(
					"statemachine.error.validation.graph.toomanystartstates");
		}

		this.validateDefaultTransitions();
	}

	/**
	 * @param statetransitionvo
	 * @throws CommonValidationException
	 *             if the given transition is duplicated.
	 */
	private void checkDuplicateTransition(StateTransitionVO statetransitionvo)
			throws CommonValidationException {
		List<StateTransitionVO> duplicateTransitions = getDuplicateTransitions(statetransitionvo);
		if (duplicateTransitions != null && !duplicateTransitions.isEmpty()) {
			throw new CommonValidationException(
					"statemachine.error.validation.graph.duplicatetransition");
		}
	}

	private List<StateTransitionVO> getDuplicateTransitions(
			StateTransitionVO statetransitionvo) {
		List<StateTransitionVO> duplicateTransitions = new ArrayList<StateTransitionVO>();
		for (StateTransitionVO statetransitionvo2 : this.getTransitions()) {
			if (!statetransitionvo2.isRemoved()) {
				if (!LangUtils.equal(statetransitionvo.getClientUID(),
						statetransitionvo2.getClientUID())) {
					if ((statetransitionvo.getStateSourceUID() != null)
							&& (LangUtils.equal(
									statetransitionvo.getStateSourceUID(),
									statetransitionvo2.getStateSourceUID()))
							&& (LangUtils.equal(
									statetransitionvo.getStateTargetUID(),
									statetransitionvo2.getStateTargetUID()))) {
						duplicateTransitions.add(statetransitionvo2);
					}
				}
			}
		}
		return duplicateTransitions;
	}

	/**
	 * @param statetransitionvo
	 * @throws CommonValidationException
	 *             if the given transition is duplicated.
	 */
	private void checkDefaultPathNotStartsFromInitialTransition(
			StateTransitionVO statetransitionvo)
			throws CommonValidationException {
		if (!getDefaultPathNotStartsFromInitialTransition(statetransitionvo)) {
			throw new CommonValidationException(
					"statemachine.error.validation.graph.defaulttransition");
		}
	}

	private boolean getDefaultPathNotStartsFromInitialTransition(
			final StateTransitionVO statetransitionvo) {
		List<StateTransitionVO> checkedTransitions = new ArrayList<StateTransitionVO>();
		if (!statetransitionvo.isDefault() || statetransitionvo.isRemoved())
			return true;

		// find start transition - there has to be one because of the checks
		// before.
		List<StateTransitionVO> transitionVOs = new LinkedList<StateTransitionVO>(
				getTransitions());
		StateTransitionVO startTransition = CollectionUtils.findFirst(
				transitionVOs, new Predicate<StateTransitionVO>() {
					@Override
					public boolean evaluate(StateTransitionVO t) {
						return !t.isRemoved() && t.getStateSourceUID() == null
								&& t.isAutomatic() == true;
					}
				});

		if (startTransition == null
				|| statetransitionvo.getStateSourceUID() == null
				|| LangUtils.equal(statetransitionvo.getStateSourceUID(),
						startTransition.getStateTargetUID()))
			return true;

		for (StateTransitionVO statetransitionvo2 : this.getTransitions()) {
			if (!statetransitionvo2.isRemoved()) {
				if (!LangUtils.equal(statetransitionvo.getClientUID(),
						statetransitionvo2.getClientUID())) {
					if ((statetransitionvo.getStateSourceUID() != null)
							&& (statetransitionvo2.getStateTargetUID() != null)
							&& (LangUtils.equal(
									statetransitionvo2.getStateTargetUID(),
									statetransitionvo.getStateSourceUID()))) {
						checkedTransitions.add(statetransitionvo2);
					}
				}
			}
		}
		if (checkedTransitions.isEmpty())
			return true;
		for (StateVO state : getStates()) {
			if (LangUtils.equal(state.getId(),
					statetransitionvo.getStateSourceUID())) {
				break;
			}
		}

		for (Iterator<StateTransitionVO> iterator = checkedTransitions
				.iterator(); iterator.hasNext();) {
			StateTransitionVO checkedTransition = (StateTransitionVO) iterator
					.next();
			if (checkedTransition.isDefault()) {
				for (StateVO state : getStates()) {
					if (LangUtils.equal(state.getId(),
							statetransitionvo.getStateSourceUID())) {
						break;
					}
				}
				return true;
			}
		}
		return false;
	}

	/**
	 * @param statetransitionvo
	 * @throws CommonValidationException
	 *             if the given transition is duplicated.
	 */
	private void validateDefaultTransitions() throws CommonValidationException {
		// validate if there is an valid path from an start to an end state.
		List<StateTransitionVO> transitionVOs = new LinkedList<StateTransitionVO>(
				getTransitions());
		if (CollectionUtils.indexOfFirst(transitionVOs,
				new Predicate<StateTransitionVO>() {
					@Override
					public boolean evaluate(StateTransitionVO t) {
						return !t.isRemoved() && t.isDefault();
					}
				}) != -1) {
			// find start transition - there has to be one because of the checks
			// before.
			StateTransitionVO startTransition = CollectionUtils.findFirst(
					transitionVOs, new Predicate<StateTransitionVO>() {
						@Override
						public boolean evaluate(StateTransitionVO t) {
							return !t.isRemoved()
									&& t.getStateSourceUID() == null
									&& t.isAutomatic() == true;
						}
					});

			List<UID> checkedStateNumerals = new LinkedList<UID>();
			boolean isFirst = true;
			// finde alle trans die als source den end der letzten haben.
			UID subsequentStateUid = startTransition.getStateTargetUID();
			while (subsequentStateUid != null) {
				if (checkedStateNumerals.contains(subsequentStateUid)) {
					throw new CommonValidationException(
							"statemachine.error.validation.graph.defaulttransition");
				}

				final UID subsequentStateSourceUid = subsequentStateUid;
				StateTransitionVO subsequentTransition = CollectionUtils
						.findFirst(transitionVOs,
								new Predicate<StateTransitionVO>() {
									@Override
									public boolean evaluate(StateTransitionVO t) {
										return !t.isRemoved()
												&& t.getStateSourceUID() == subsequentStateSourceUid
												&& t.isDefault() == true;
									}
								});

				if (subsequentTransition == null) {
					if (isFirst)
						throw new CommonValidationException(
								"statemachine.error.validation.graph.defaulttransition");
					break;
				}

				isFirst = false;

				// iterate next.
				checkedStateNumerals.add(subsequentStateUid);
				subsequentStateUid = subsequentTransition.getStateTargetUID();
			}
		}
	}

	/**
	 * @param statetransitionvo
	 * @throws CommonValidationException
	 *             if the given transition is duplicated.
	 */
	private void checkDuplicateDefaultTransition(
			StateTransitionVO statetransitionvo)
			throws CommonValidationException {
		List<StateTransitionVO> duplicateTransitions = getDuplicateDefaultTransitions(statetransitionvo);
		if (duplicateTransitions != null && !duplicateTransitions.isEmpty()) {
			throw new CommonValidationException(
					"statemachine.error.validation.graph.duplicatedefaulttransition");
		}
	}

	private List<StateTransitionVO> getDuplicateDefaultTransitions(
			StateTransitionVO statetransitionvo) {
		List<StateTransitionVO> duplicateTransitions = new ArrayList<StateTransitionVO>();
		for (StateTransitionVO statetransitionvo2 : this.getTransitions()) {
			if (!statetransitionvo2.isRemoved()) {
				if (!LangUtils.equal(statetransitionvo.getClientUID(),
						statetransitionvo2.getClientUID())) {
					if ((statetransitionvo.getStateSourceUID() != null)
							&& (LangUtils.equal(
									statetransitionvo.getStateSourceUID(),
									statetransitionvo2.getStateSourceUID()))
							&& (statetransitionvo.isDefault() && statetransitionvo2
									.isDefault())) {
						duplicateTransitions.add(statetransitionvo2);
					}
				}
			}
		}
		return duplicateTransitions;
	}
	
	/**
	 * id of the starting state
	 */
	public static final UID STARTING_STATE_UID = new UID("_START_");

	public static StateModelLayout newLayoutInfo(StateGraphVO stategraphvo) {
		final StateModelLayout result = new StateModelLayout();
		for (StateVO statevo : stategraphvo.getStates()) {
			result.insertStateLayout(statevo.getClientUID(), new StateLayout(0d, 0d, 120d, 48d));
		}

		// StateModelStartShape
		result.insertStateLayout(STARTING_STATE_UID, new StateLayout(8d, 8d, 12d, 12d));

		for (StateTransitionVO statetransitionvo : stategraphvo.getTransitions()) {
			final int iConnectionStart = (statetransitionvo.getStateSourceUID() != null) ? 4 /*AbstractShape.CONNECTION_NE*/ : -1;
			final int iConnectionEnd = (statetransitionvo.getStateTargetUID() != null) ? 2 /*AbstractShape.CONNECTION_N*/ : -1;
			final TransitionLayout transitionlayout = new TransitionLayout(statetransitionvo.getId(), iConnectionStart, iConnectionEnd);

			result.insertTransitionLayout(statetransitionvo.getId(), transitionlayout);
		}
		return result;
	}

	public StateGraphVO deepCopyAndDisableProtection() {
		StateGraphVO clone = this.deepClone();
		clone.mpStates = clone.mpStates.parallelStream().map(StateVO::deepCloneAndDisableProtection).collect(Collectors.toSet());
		clone.mpTransitions = clone.mpTransitions.parallelStream().map(StateTransitionVO::deepCloneAndDisableProtection).collect(Collectors.toSet());
		return clone;
	}

} // class StateGraphVO
