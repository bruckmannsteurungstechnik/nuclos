package org.nuclos.common2;

import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.nuclos.common2.StringUtils.compare;

import org.junit.Test;

public class StringUtilsTest {

	@Test
	public void testCompare() {
		String s1 = null;
		String s2 = null;
		int result = compare(s1, s2);
		assertEquals(result, 0);
		result = compare(s2, s1);
		assertEquals(result, 0);
		
		s2 = "test";
		result = compare(s1, s2);
		assertEquals(result, 1);
		result = compare(s2, s1);
		assertEquals(result, -1);
		
		s1 = "TEST";
		result = compare(s1, s2);
		assertTrue(result == 0);
		result = compare(s2, s1);
		assertTrue(result == 0);
		
		s1 = "TEST1";
		result = compare(s1, s2);
		assertTrue(result > 0);
		result = compare(s2, s1);
		assertTrue(result < 0);
	}

	@Test
	public void firstLine() {
		assertNull(StringUtils.firstLine(null));
		assertEquals(StringUtils.firstLine("test"), "test");
		assertEquals(StringUtils.firstLine("test\n"), "test");
		assertEquals(StringUtils.firstLine("\ntest\n"), "");
		assertEquals(StringUtils.firstLine("te\nst\n"), "te");
	}
}
