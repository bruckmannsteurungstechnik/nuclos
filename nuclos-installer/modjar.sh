#!/bin/bash
# No default output! Otherwise keypass and storepass would be printed to log. 

if [[ -z $KEYSTORE ]]; then
	echo "WARNING: Novabit keystore (Certificated) not found, using Nuclos default."
	KEYSTORE="../nuclos-war/.keystore"
	ALIAS='nuclos'
	KEYPASS='solcun'
	STOREPASS='solcun'
else
	echo "Novabit keystore (Certificated) found."
fi
CACHE="$HOME/jar.cache"
# use timestamp service
#SIGN_OPTIONS="-tsa http://tsa.starfieldtech.com/ -tsacert $ALIAS"
# Use apple time service for adding the signing time in the jars:
SIGN_OPTIONS="-tsa http://sha256timestamp.ws.symantec.com/sha256/timestamp -tsacert $ALIAS"

# don't use timestamp service
# SIGN_OPTIONS=""
# strip unknown attributes
PACK200_OPTIONS="-Ustrip -J-XX:-UseGCOverheadLimit -J-Xmx2048m"
# don't strip unknown attributes
#PACK200_OPTIONS=""

if [ -z "$JAVA_HOME" ]; then
	JARSIGNER=jarsigner
	JAR=jar
	PACK200=pack200
elif [[ $JAVA_HOME =~ ^(.*)/jre(/)?$ ]]; then
	# http://stackoverflow.com/questions/1891797/capturing-groups-from-a-grep-regex
	JAVA_HOME=${BASH_REMATCH[1]}
	JARSIGNER=$JAVA_HOME/bin/jarsigner
	JAR=$JAVA_HOME/bin/jar
	PACK200=$JAVA_HOME/bin/pack200
else
	JARSIGNER=$JAVA_HOME/bin/jarsigner
	JAR=$JAVA_HOME/bin/jar
	PACK200=$JAVA_HOME/bin/pack200
fi
if [ -z "$SIGNER_JAVA_HOME" ]; then
	: # do nothing - user jar, jarsigner, pack200 from JAVA_HOME
else
	JARSIGNER=$SIGNER_JAVA_HOME/bin/jarsigner
	JAR=$SIGNER_JAVA_HOME/bin/jar
	PACK200=$SIGNER_JAVA_HOME/bin/pack200
	if [ ! -f "$JARSIGNER" ]; then
		echo "SIGNER_JAVA_HOME set to $SIGNER_JAVA_HOME, but jarsigner not found!"
		exit -1
	fi
fi
if [ -z "$TMPDIR" ]; then
	TMPDIR="/tmp"
fi
if [ ! -d "$TMPDIR" ]; then
	echo "TMPDIR set to $TMPDIR, but no directory"
	exit -1
fi
BUILDDIR=`mktemp -d "$TMPDIR/modjarXXXXXXX"`
	
unsign() {
	local JAR1="$1"
	shift
	zip -d "$JAR1" META-INF/\*.SF META-INF/\*.DSA META-INF/\*.RSA META-INF/INDEX.LIST META-INF/MANIFEST.MF
}

sign() {
	local JAR1="$1"
	shift
	$JARSIGNER $SIGN_OPTIONS -keystore ".keystore" -storepass "$STOREPASS" -keypass "$KEYPASS" "$JAR1" "$ALIAS" >/dev/null
	if [[ "$?" != 0 ]]; then
		echo "signing of $JAR1 failed; unable to proceed"
		$JARSIGNER $SIGN_OPTIONS -keystore ".keystore" -storepass "$STOREPASS" -keypass "$KEYPASS" "$JAR1" "$ALIAS"
		exit -1
	fi
}

verify() {
	local JAR1="$1"
	shift
	echo "verifing $JAR1"
	$JARSIGNER -keystore ".keystore" -storepass "$STOREPASS" -keypass "$KEYPASS" -verify "$JAR1" "$ALIAS"
	if [[ "$?" != 0 ]]; then
		echo "verifing of $JAR1 failed; unable to proceed"
		$JARSIGNER -keystore ".keystore" -storepass "$STOREPASS" -keypass "$KEYPASS" -verify -verbose "$JAR1" "$ALIAS"
		exit -1
	fi
}

index() {
	local JAR1="$1"
	shift
	
	$JAR umf "$MANIFEST_ATTR" "$JAR1"
	# removed index generation because of 
	# * http://support.nuclos.de/browse/NUCLOS-1537 and
	# * https://jira.springsource.org/browse/SPR-6383
	# jar i "$JAR" || reindex "$JAR"
}

reindex() {
	local JAR1="$1"
	shift
	local TMPDIR1=`mktemp -d "$TMPDIR/modjarXXXXXXX"`
	echo "Bogus $JAR: repacking/reindexing"
	unzip -q "$JAR1" -d "$TMPDIR1"
	rm "$JAR1"
	$JAR cmf "$JAR1" "$MANIFEST_ATTR" -C "$TMPDIR1" .
	index "$JAR1"
	rm -r "$TMPDIR1"
}

repack() {
	local JAR1="$1"
	shift
	$PACK200 $PACK200_OPTIONS --repack "$JAR1"
	if [[ "$?" != 0 ]]; then
		echo "repacking of $JAR1 failed; unable to proceed"
		exit -1
	fi
}

pack() {
	local JAR1="$1"
	shift
	$PACK200 $PACK200_OPTIONS "$JAR1.pack.gz" "$JAR1"
	if [[ "$?" != 0 ]]; then
		echo "packing of $JAR1 failed; unable to proceed"
		exit -1
	fi
	rm "$JAR1"
}

allinone() {
	local DIR="$1"
	shift
	local TARGET="$1"
	shift
	
	pushd $BUILDDIR
		unzip -uq "$DIR/$TARGET"
		rm "$DIR/$TARGET" META-INF/MANIFEST.MF
		# http://superuser.com/questions/91935/how-to-chmod-755-all-directories-but-no-file-recursively
		find . -type d -execdir chmod 755 {} \+
	popd 
}

modjar() {
	local DIR="$1"
	shift
	local TARGET="$1"
	shift
	
	local CACHED="$CACHE/$TARGET.pack.gz"
	if [[ -f "$CACHED" && ! "$TARGET" =~ $CACHE_REGEX ]]; then
		echo "Using cached $CACHED"
		cp "$CACHED" "$TARGET.pack.gz"
		rm "$TARGET"
	else 
		echo "Processing $TARGET"
		unsign "$TARGET"
		(index "$TARGET" && \
			repack "$TARGET" && \
			sign "$TARGET" && \
			pack "$TARGET" ) || \
	#		verify "$TARGET") || \
			exit -1
		if [[ ! "$TARGET" =~ $CACHE_REGEX ]]; then
			cp "$TARGET.pack.gz" "$CACHE"
		fi
	fi
}

modjar2() {
	local DIR="$1"
	shift
	local TARGET="$1"
	shift
	
	local CACHED="$CACHE/$TARGET"
	if [[ -f "$CACHED" && ! "$TARGET" =~ $CACHE_REGEX ]]; then
		echo "Using cached $CACHED"
		cp "$CACHED" "$TARGET"
	else 
		echo "Processing2 $TARGET"
		unsign "$TARGET"
		(index "$TARGET" && \
			repack "$TARGET" && \
			sign "$TARGET" && \
	#		pack "$TARGET" ) || \
			verify "$TARGET") || \
			exit -1
		if [[ ! "$TARGET" =~ $CACHE_REGEX ]]; then
			cp "$TARGET" "$CACHE"
		fi
	fi
}

DIR="$1"
shift

# for a few JARs pack200 just don't work - I don't know why (tp)
# added spring as it is not ready to cope with all-in-one JAR policy (tp)
REGEX="(xalan|.*jdk14|jxlayer|activemq|spring|nuclos-native|jfreechart|jnlp)-.*jar"
# only cache jar that we don't build and are not SNAPSHOTs
CACHE_REGEX="(nuclos-(\?\!.*(native|api))|.*-SNAPSHOT\.|.*-dirty\.|.*-r\.|.*-rjenkins-|.*-r${USER}-|all-in-one).*"
# create a manifest template with the new required attributes
# http://docs.oracle.com/javase/7/docs/technotes/guides/jweb/no_redeploy.html
# http://docs.oracle.com/javase/7/docs/technotes/guides/jweb/mixed_code.html
MANIFEST_ATTR=`mktemp "$TMPDIR/modjarXXXXXXX"`
cat <<END_M >$MANIFEST_ATTR
Manifest-Version: 1.0
Application-Name: nuclos.de ERP
Permissions: all-permissions
Codebase: *
Trusted-Only: true
Trusted-Library: true
END_M

mkdir -p "$CACHE"
if [ ! -d "$CACHE" ]; then
	echo "Can't create/access jar cache at $CACHE"
	exit -1
fi

cp $KEYSTORE $DIR/.keystore
pushd $DIR
	ALLINONE=`mktemp "$TMPDIR/modjarXXXXXXX"`
	for i in $*; do
		if [[ "$i" =~ $REGEX ]]; then
			echo "No packing of $i"
			modjar2 "$DIR" "$i"
		elif [[ "$i" =~ $CACHE_REGEX ]]; then
			echo "No packing of $i"
			modjar2 "$DIR" "$i" 
		else 
			# modjar "$DIR" "$i" 
			# allinone "$DIR" "$i"
			 echo "$i" >>"$ALLINONE"
		fi
		# only start 4 concurrent jobs
		# while [ `jobs | wc -l` -ge 4 ]; do
		# 		sleep 2
		# done
	done
	sort "$ALLINONE"
	SHA1=`shasum "$ALLINONE" | cut -d ' ' -f 1`
	CACHEDJAR="all-in-one-$SHA1.jar"
	CACHEDFILE="${CACHEDJAR}.pack.gz"
	if [ -f "$CACHE/$CACHEDFILE" ]; then
		echo "same sum $SHA1 using $CACHE/$CACHEDFILE"
		cp "$CACHE/$CACHEDFILE" .
		echo "remove all jars that are included in cached all-in-one jar"
		cat "$ALLINONE" | xargs rm 
	else 
		echo "not same sum: building $CACHEDFILE"
		for i in `cat $ALLINONE`; do
			# echo "ALLINONE: $i"
			allinone "$DIR" "$i"
		done
		pushd $BUILDDIR
			# http://superuser.com/questions/91935/how-to-chmod-755-all-directories-but-no-file-recursively
			find . -type d -execdir chmod 755 {} \+
			#chmod 755 $(find . -type d)
			find . -type f -execdir chmod 644 {} \+
			# chmod 644 $(find . -type f)
			zip -9r "$DIR/$CACHEDJAR" .
		popd
		modjar "$DIR" "$CACHEDJAR"
		cp "$CACHEDFILE" "$CACHE"
	fi
	
	# wait for all jobs (last job is this shell script)
	# while [ `jobs | wc -l` -gt 1 ]; do
	# 		sleep 2
	# done
	# sleep 5
	
	rm ".keystore" "$ALLINONE"
popd

rm $MANIFEST_ATTR
rm .keystore
rm -r $BUILDDIR
