package org.nuclos.installer.unpack.extension;

import java.io.File;
import java.util.Arrays;
import java.util.List;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class ServerExtensionInstaller extends ExtensionInstaller {
	ServerExtensionInstaller(
			final InstallationContext context
	) {
		super(context);
	}

	@Override
	File getSourceDirectory() {
		return new File(context.getExtensionsDir(), DirectoryName.SERVER);
	}

	@Override
	List<File> getTargetDirectories() {
		return Arrays.asList(
				new File(context.getWebappDir(), "WEB-INF/lib/")
		);
	}
}
