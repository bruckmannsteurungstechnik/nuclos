package org.nuclos.test


import javax.ws.rs.core.Response

import org.apache.logging.log4j.Level
import org.json.JSONObject
import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.experimental.categories.Category
import org.junit.rules.TestRule
import org.junit.runners.MethodSorters
import org.nuclos.test.log.CallTrace
import org.nuclos.test.log.Log
import org.nuclos.test.rest.RESTException
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.ServerLogger
import org.nuclos.test.rest.SuperuserRESTClient
import org.nuclos.test.webclient.FailureHandler
import org.nuclos.test.webclient.utils.Utils

import groovy.transform.CompileStatic
import groovy.transform.stc.ClosureParams
import groovy.transform.stc.SimpleType

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
abstract class AbstractNuclosTest {
	static SuperuserRESTClient nuclosSession

	static NuclosTestContext context = NuclosTestContext.instance

	@Rule
	public final TestRule testRule = new NuclosTestRule()

	@BeforeClass
	static void setup() {
		Log.debug 'Setup'

		truncateTablesAndCaches()
		nuclosSession.managementConsole('disableIndexer')

		nuclosSession.setServerLogLevel(ServerLogger.SQL_TIMER, Level.DEBUG)
		setupSystemParameters()
	}

	static SuperuserRESTClient getNuclosSession() {
		if (!nuclosSession) {
			CallTrace.trace('Login as "nuclos"') {
				nuclosSession = new SuperuserRESTClient('nuclos', '').login()
			}
		}

		return nuclosSession
	}

	static void truncateTablesAndCaches() {
		CallTrace.trace('Truncate tables and Caches') {
			Map truncateTable =
					[
							boMetaId  : 'nuclet_test_utils_TruncateTables',
							attributes: [
									'name': 'Truncate User Tables' + new Date().getTime()
							]
					]
			JSONObject bo = RESTHelper.createBo(truncateTable, getNuclosSession())
			bo.getJSONObject('attributes').put('nuclosState',  [id: 'nuclet_test_utils_TruncateTablesSM_State_20'])
			RESTHelper.updateBo(bo, getNuclosSession())

			Log.info getNuclosSession().invalidateServerCaches()
		}
	}

	/**
	 * Works only for requests done directly via RESTClient,
	 * does not capture Webclient requests.
	 */
	static void expectErrorStatus(
			Response.Status expectedStatus,
			Closure<?> c
	) {
		expectRestException(c) {
			assert it.status == expectedStatus
		}
	}

	/**
	 * Catches any RestException that occurs while executing the given closure.
	 * Hands the RestException to the given exceptionHandler.
	 */
	static void expectRestException(
			Closure<?> c,

			@ClosureParams(value = SimpleType, options = ['org.nuclos.test.rest.RESTException'])
					Closure<?> exceptionHandler
	) {
		Exception e = null

		try {
			c()
		} catch (RESTException e2) {
			e = e2
		}

		if (e) {
			exceptionHandler(e)
		} else {
			throw new IllegalStateException("Expected exception did not occur")
		}
	}

	static void setupSystemParameters() {
		nuclosSession.setSystemParameters([:])
	}

	/**
	 * Deprecated: Use {@link org.nuclos.test.webclient.AbstractWebclientTest#waitFor(groovy.lang.Closure) or a custom WebDriverWait instead, if this is a Webclient test.
	 */
	@Deprecated()
	static void sleep(long millis) {
		try {
			Thread.sleep(millis)
		} catch (InterruptedException e) {
			Log.error(e.getMessage(), e)
		}
	}

	/**
	 * Waits for the given Closure to return true.
	 * Fails the build if the condition is not met within the given timeout.
	 */
	static boolean waitFor(int timeoutInSeconds, Closure condition) {
		if (!doWaitFor(timeoutInSeconds, condition)) {
			String source = Utils.closureToString(condition)
			fail('Failed to wait for condition: ' + source, new Throwable(source))
		}

		return true
	}

	static boolean waitFor(Closure condition) {
		waitFor(NuclosTestContext.instance.DEFAULT_TMEOUT, condition)
	}

	/**
	 * Waits for the given Closure to return true.
	 *
	 * @param timeoutInSeconds
	 * @param condition
	 * @return true , if the condition was met within the timeout
	 */
	static boolean doWaitFor(int timeoutInSeconds, Closure condition) {
		try {
			Thread t = new Thread() {
				@Override
				void run() {
					def result = condition()
					while (!result && !isInterrupted()) {
						Log.info "Waiting for condition ${condition.toString()}..."
						sleep(500)
						result = condition()
					}
				}
			}
			t.start()
			try {
				t.join(timeoutInSeconds * 1000)
			} catch (InterruptedException ex) {
				Log.warn "Thread interrupted", ex
			}

			if (t.alive) {
				t.interrupt()
				return false
			}
			return true
		} catch (Exception ex) {
			Log.warn("Failed to wait for condition", ex)
			return false
		}
	}

	/**
	 * Forcefully fails the build immediately.
	 */
	static void fail(String failure, Throwable t = null) {
		FailureHandler.fail(failure, t)
	}
}
