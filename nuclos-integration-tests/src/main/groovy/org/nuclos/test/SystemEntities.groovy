package org.nuclos.test

import com.google.common.base.CaseFormat

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
enum SystemEntities implements EntityClass<String> {
	PARAMETER,
	USER,
	ROLE,
	ROLEUSER,
	NUCLET,
	NUCLET_PARAMETER,
	MANDATOR,
	NEWS

	@Override
	String getFqn() {
		String formattedName = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, name())

		return 'org_nuclos_system_' + formattedName
	}
}
