package org.nuclos.test.rest

import java.nio.charset.StandardCharsets

import org.json.JSONArray
import org.json.simple.JSONObject
import org.springframework.http.HttpMethod

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class QueryOptions {
	String where
	String search
	Set<String> attributes
	Long chunkSize
	Long offset
	Set<String> orderBy

	// Overrides the request method, if set
	HttpMethod method

	String toQuery() {
		Set<String> params = new TreeSet<>()

		if (where) {
			params << "where=" + URLEncoder.encode(where, StandardCharsets.UTF_8.name())
		}
		if (search) {
			params << "search=" + URLEncoder.encode(search, StandardCharsets.UTF_8.name())
		}
		if (attributes) {
			params << "attributes=" + attributes.collect{ URLEncoder.encode(it, StandardCharsets.UTF_8.name()) }.join(',')
		}
		if (chunkSize) {
			params << "chunkSize=" + chunkSize
		}
		if (offset) {
			params << "offset=" + offset
		}
		if (orderBy) {
			params << "orderBy=" + orderBy.collect{ URLEncoder.encode(it, StandardCharsets.UTF_8.name()) }.join(',')
		}

		params.join('&')
	}

	String toJson() {
		JSONObject json = new JSONObject()

		if (where) {
			json.put('where', where)
		}
		if (search) {
			json.put('search', search)
		}
		if (attributes) {
			json.put('attributes', new JSONArray(attributes))
		}
		if (chunkSize) {
			json.put('chunkSize', chunkSize)
		}
		if (offset) {
			json.put('offset', offset)
		}
		if (orderBy) {
			json.put('orderBy', orderBy.join(','))
		}

		json.toString()
	}
}
