package org.nuclos.test.rest

import org.apache.http.HttpException
import org.apache.http.HttpHost
import org.apache.http.HttpRequest
import org.apache.http.HttpRequestInterceptor
import org.apache.http.client.config.CookieSpecs
import org.apache.http.client.config.RequestConfig
import org.apache.http.client.methods.CloseableHttpResponse
import org.apache.http.client.methods.HttpGet
import org.apache.http.conn.routing.HttpRoute
import org.apache.http.conn.routing.HttpRoutePlanner
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.apache.http.impl.conn.DefaultProxyRoutePlanner
import org.apache.http.impl.conn.DefaultRoutePlanner
import org.apache.http.impl.conn.DefaultSchemePortResolver
import org.apache.http.protocol.HttpContext
import org.json.JSONObject
import org.nuclos.common.UID
import org.nuclos.schema.rest.News
import org.nuclos.server.rest.services.helper.RecursiveDependency
import org.nuclos.test.EntityClass
import org.nuclos.test.EntityObject
import org.nuclos.test.NuclosTestProxy
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.LoginParams

import groovy.transform.CompileStatic
import net.lightbody.bmp.core.har.Har
import net.lightbody.bmp.proxy.CaptureType

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class RESTClient {

	final ThreadLocal<Map<String, String>> additionalHeaders = new ThreadLocal<>()

	final LoginParams loginParams

	CloseableHttpClient httpClient
	String sessionId

	/**
	 * The IP address of this client, as seen by the server.
	 * If there is a proxy in between, might be the address of the proxy.
	 */
	String ipAddress

	CloseableHttpResponse getUrl(final String url) {
		def request = new HttpGet(url)
		httpClient.execute(request)
	}

	InputStream getInputStream(final String url) {
		RESTHelper.requestInputStream(url, org.springframework.http.HttpMethod.GET, sessionId)
	}

	class DynamicProxyRoutePlanner implements HttpRoutePlanner {

		private HttpRoutePlanner defaultRoutePlanner = new DefaultRoutePlanner(DefaultSchemePortResolver.INSTANCE)
		private HttpRoutePlanner proxyRoutePlanner

		DynamicProxyRoutePlanner(HttpHost host) {
			if (host) {
				proxyRoutePlanner = new DefaultProxyRoutePlanner(host)
			}
		}

		void setProxy(HttpHost host) {
			if (host) {
				proxyRoutePlanner = new DefaultProxyRoutePlanner(host)
			} else {
				proxyRoutePlanner = null
			}
		}

		HttpRoute determineRoute(
				HttpHost target,
				HttpRequest request,
				HttpContext context
		) {
			if (proxyRoutePlanner) {
				proxyRoutePlanner.determineRoute(target, request, context)
			} else {
				defaultRoutePlanner.determineRoute(target, request, context)
			}
		}
	}

	DynamicProxyRoutePlanner proxyRoutePlanner = new DynamicProxyRoutePlanner(null)

	RESTClient(final String username, final String password) {
		this(
				new LoginParams(
						username: username,
						password: password
				)
		)
	}

	RESTClient(final LoginParams loginParams) {
		this.loginParams = loginParams


		RequestConfig requestConfig = RequestConfig.copy(RequestConfig.DEFAULT)
				.setCookieSpec(CookieSpecs.STANDARD)
				.build()

		this.httpClient = HttpClients.custom()
				.setDefaultRequestConfig(requestConfig)
				.addInterceptorFirst(new ResponseSqlInterceptor())
				.addInterceptorFirst(new HttpRequestInterceptor() {
			@Override
			void process(final HttpRequest request, final HttpContext context) throws HttpException, IOException {
				additionalHeaders.get()?.each { key, value ->
					request.addHeader(key, value)
				}
			}
		}).setRoutePlanner(proxyRoutePlanner)
				.build()
	}

	void changePassword(EntityObject<UID> user) {
		changePassword(user.getAttribute('username').toString(), user.getAttribute('oldPassword').toString(), user.getAttribute('newPassword').toString())
	}

	void changePassword(String username, String pwOld, String pwNew) {
		RESTHelper.changePassword(username, pwOld, pwNew, this)
	}

	RESTClient login() {
		def loginResult = RESTHelper.login(this)

		sessionId = loginResult.get('sessionId')
		ipAddress = loginResult.get('clientIp')

		return this
	}

	void logout() {
		RESTHelper.logout(this)
	}

	public <PK> PK save(EntityObject<PK> eo) {
		RESTHelper.save(eo, this)
	}

	public <PK> EntityObject<PK> clone(EntityObject<PK> eo, String layout = null) {
		RESTHelper.clone(eo, this, layout)
	}

	public <PK> boolean delete(final String entityClassId, final PK id) {
		RESTHelper.delete(entityClassId, id, this)
	}

	public <PK> boolean delete(EntityObject<PK> eo) {
		RESTHelper.delete(eo, this)
	}

	public <PK> void deleteAll(Collection<EntityObject<PK>> eos) {
		RESTHelper.deleteAll(eos, this)
	}

	/**
	 * TODO: Proper typing instead of JSONObject.
	 */
	JSONObject getEntityMeta(final EntityClass entityClass) {
		RESTHelper.getEntityMeta(entityClass, this)
	}

	public <PK> EntityObject<PK> getEntityObject(
			EntityClass<PK> entityClass,
			PK id
	) {
		RESTHelper.getEntityObject(entityClass, id, this)
	}

	public <PK> List<EntityObject<PK>> getEntityObjects(
			EntityClass<PK> entityClass,
			QueryOptions options = null
	) {
		RESTHelper.getEntityObjects(entityClass, this, options)
	}

	public <PK, SubPK> List<EntityObject<SubPK>> loadDependents(
			EntityObject<PK> eo,
			EntityClass<SubPK> subEoClass,
			String referenceAttributeId
	) {
		RESTHelper.loadDependents(eo, subEoClass, referenceAttributeId, this)
	}

	public <PK, SubPK> List<EntityObject<SubPK>> loadDependentsRecursively(
			EntityObject<PK> eo,
			EntityClass<SubPK> subEoClass,
			RecursiveDependency dependency,
			QueryOptions queryOptions = null
	) {
		RESTHelper.loadDependentsRecursively(this, eo, subEoClass, dependency, queryOptions)
	}

	public <PK> void executeCustomRule(
			EntityObject<PK> eo,
			String ruleName
	) {
		RESTHelper.executeCustomRule(eo, ruleName, this)
	}

	JSONObject getSessionData() {
		RESTHelper.getSessionData(this)
	}

	void saveAll(final List<EntityObject> entityObjects) {
		RESTHelper.saveAll(entityObjects, this)
	}

	public <PK> void changeState(EntityObject<PK> eo, int statusNumeral) {
		RESTHelper.changeState(eo, statusNumeral, this)
	}

	/**
	 * TODO: Do not mix test code with framework code
	 */
	@Deprecated
	public void expectSearchResults(String search, int hits, int wait) {
		RESTHelper.expectSearchResults(search, hits, this, wait)
	}

	/**
	 * TODO: Do not mix test code with framework code
	 */
	@Deprecated
	public void expectSearchResults(String search, int hits) {
		RESTHelper.expectSearchResults(search, hits, this, 0)
	}

	public <PK, PK2> EntityObject<PK2> generateObject(
			final EntityObject<PK> eo,
			final String generator,
			final EntityClass<PK2> resultClass
	) {
		return RESTHelper.generateObject(eo, generator, resultClass, this)
	}

	File exportResultList(
			final TestEntities entity,
			final String outputFormat
	) {
		return RESTHelper.exportResultList(entity, outputFormat, this)
	}

	byte[] exportNuclet(String nucletName) {
		return RESTHelper.exportNuclet(nucletName, this)
	}

	JSONObject getMenuStructure() {
		RESTHelper.getMenuStructure(this)
	}

	/**
	 * Adds the given HTTP headers to all direct REST requests that are made from the given Closure via this RESTClient.
	 */
	public <T> T withHeaders(Map<String, String> headers, Closure<T> c) {
		def previousHeaders = additionalHeaders.get()

		additionalHeaders.set(headers)

		try {
			return c()
		} finally {
			additionalHeaders.set(previousHeaders)
		}
	}

	/**
	 * Captures a HAR containing the requests / responses made by this RESTClient
	 * within the given Closure.
	 */
	Har getHar(Closure c) {
		def proxy = new NuclosTestProxy()
		proxy.start()

		proxyRoutePlanner.setProxy(proxy.httpHost)

		try {
			return proxy.getHar(
					[
							CaptureType.RESPONSE_HEADERS,
							CaptureType.RESPONSE_COOKIES
					],
					c
			)
		} finally {
			proxyRoutePlanner.setProxy(null)
			proxy.stop()
		}
	}

	String getDescriptionToStatus(String statusId) {
		return RESTHelper.getDescriptionToStatus(statusId, this)
	}

	List<News> getUnconfirmedNews() {
		RESTHelper.getUnconfirmedNews(this)
	}

	List<News> getCurrentNews() {
		RESTHelper.getCurrentNews(this)
	}
}
