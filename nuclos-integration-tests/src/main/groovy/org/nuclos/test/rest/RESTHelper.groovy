package org.nuclos.test.rest

import static org.nuclos.test.log.CallTrace.trace

import java.nio.file.Files

import org.apache.commons.io.Charsets
import org.apache.http.HttpHeaders
import org.apache.http.HttpResponse
import org.apache.http.client.methods.HttpDelete
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.client.methods.HttpPut
import org.apache.http.client.methods.HttpRequestBase
import org.apache.http.client.methods.HttpUriRequest
import org.apache.http.entity.StringEntity
import org.apache.logging.log4j.Level
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.nuclos.schema.rest.News
import org.nuclos.server.rest.services.helper.RecursiveDependency
import org.nuclos.test.EntityClass
import org.nuclos.test.EntityObject
import org.nuclos.test.NuclosTestContext
import org.nuclos.test.SystemEntities
import org.springframework.http.HttpMethod
import org.springframework.util.StreamUtils
import org.springframework.web.client.RestClientException

import com.fasterxml.jackson.databind.ObjectMapper

import groovy.json.JsonOutput
import groovy.transform.CompileStatic
import groovy.transform.PackageScope

/**
 * Performs REST requests directly, instead of using the Webclient GUI.
 *
 */
@CompileStatic
class RESTHelper {

	static NuclosTestContext context = NuclosTestContext.instance
	static String REST_BASE_URL = context.nuclosServerProtocol + '://' +
			context.nuclosServerHost + ':' +
			context.nuclosServerPort +
			(context.nuclosServerContext.length() > 0 ? '/' + context.nuclosServerContext : '') +
		'/rest'

	static JSONObject requestJson(String url, HttpMethod httpMethod, String sessionId = null, String json = null) {
		String response = requestString(url, httpMethod, sessionId, json)
		parseJsonObject(response)
	}

	static JSONArray requestJsonArray(String url, HttpMethod httpMethod, String sessionId = null, String json = null) {
		String response = requestString(url, httpMethod, sessionId, json)
		parseJsonArray(response)
	}

	static String requestString(
			String urlOrPath,
			HttpMethod httpMethod,
			String sessionId = null,
			String json = null
	) {
		return getStreamText(requestInputStream(urlOrPath, httpMethod, sessionId, json))
	}

	/**
	 * @deprecated Get rid of HttpURLConnection, use only Apache HttpClient from RESTClient!
	 */
	@Deprecated
	static InputStream requestInputStream(
			String urlOrPath,
			HttpMethod httpMethod,
			String sessionId = null,
			String json = null
	) {
		// Protocol-relative URL
		if (urlOrPath.startsWith('//')) {
			urlOrPath = 'http:' + urlOrPath
		}

		if (!urlOrPath.startsWith('http://') && !urlOrPath.startsWith('https://')) {
			if (!urlOrPath.startsWith('/')) {
				urlOrPath = '/' + urlOrPath
			}
			urlOrPath = REST_BASE_URL + urlOrPath
		}

		HttpURLConnection urlConnection = (HttpURLConnection) new URL(urlOrPath).openConnection();
		urlConnection.setRequestMethod(httpMethod.name())
		urlConnection.setRequestProperty("Accept", "application/json, text/plain, */*")
		urlConnection.setRequestProperty("Content-Type", "application/json")

		if (sessionId != null) {
			urlConnection.setRequestProperty("Cookie", "JSESSIONID=" + sessionId)
		}

		urlConnection.doOutput = true

		if (json != null) {
			def writer = new OutputStreamWriter(urlConnection.outputStream)
			writer.write(json.trim())
			writer.flush()
			writer.close()
		}
		urlConnection.connect()

		try {
			urlConnection.getInputStream()
		} catch (Exception e) {
			int statusCode = urlConnection.getResponseCode()
			String errorText = getStreamText(urlConnection.getErrorStream())

			throw new RESTException(statusCode, errorText, e)
		}
	}

	private static String getStreamText(InputStream is) {
		StreamUtils.copyToString(
				is,
				Charsets.UTF_8
		)
	}

	static JSONObject login(final RESTClient client) {
		def params = client.loginParams

		JSONObject postData = new JSONObject()
				.put('username', params.username)
				.put('password', params.password)
				.put('datalanguage', params.datalanguage)
				.put('autologin', params.autologin)
				.put('locale', params.locale)
				.put('mandator', params.mandator)

		HttpPost post = new HttpPost(REST_BASE_URL)
		post.setEntity(new StringEntity(postData.toString()))
		post.setHeader(HttpHeaders.CONTENT_TYPE, 'application/json')

		return requestJsonObject(client, post)
	}

	static void logout(final RESTClient client) {
		HttpDelete request = new HttpDelete(REST_BASE_URL)

		requestString(client, request)
	}

	@PackageScope
	static JSONObject getSessionData(RESTClient client) {
		def result = requestJson(REST_BASE_URL, HttpMethod.GET, client.sessionId, null)
		return result
	}

	@PackageScope
	static JSONObject getMenuStructure(RESTClient client) {
		requestJson(REST_BASE_URL + '/meta/menustructure', HttpMethod.GET, client.sessionId)
	}

	/**
	 * Use {@link #save(org.nuclos.test.EntityObject, RESTClient)}.
	 */
	@Deprecated
	static JSONObject createBo(Map bo, RESTClient client) {
		String entityClassId = bo['boMetaId']
		trace("Create EO ${entityClassId}") {
			def url = REST_BASE_URL + '/bos/' + entityClassId
			JSONObject result = requestJson(url, HttpMethod.POST, client.sessionId, JsonOutput.toJson(bo))

			return result
		} as JSONObject
	}

	/**
	 * Use {@link #save(org.nuclos.test.EntityObject, RESTClient)}.
	 */
	@Deprecated
	static Object createBo(
			String boMetaId,
			Map<String, Serializable> attributes,
			RESTClient client
	) {
		trace('Creat EO: ' + boMetaId) {
			def bo = [
					boMetaId  : boMetaId,
					attributes: [:]
			]

			for (Map.Entry<String, Serializable> attribute : attributes.entrySet()) {
				bo['attributes'][attribute.key] = attribute.value
			}

			def url = REST_BASE_URL + '/bos/' + boMetaId
			return requestJson(url, HttpMethod.POST, client.sessionId, JsonOutput.toJson(bo))
		}
	}

	/**
	 * Use {@link #save(org.nuclos.test.EntityObject, RESTClient)}.
	 */
	@Deprecated
	static void updateBo(Map bo, RESTClient client) {
		String entityClassId = bo['boMetaId']
		trace("Update EO ${entityClassId}") {
			def url = REST_BASE_URL + '/bos/' + entityClassId + '/' + bo['boId']
			requestJson(url, HttpMethod.PUT, client.sessionId, JsonOutput.toJson(bo))
		}
	}

	/**
	 * Use {@link #save(org.nuclos.test.EntityObject, RESTClient)}.
	 */
	@Deprecated
	static void updateBo(JSONObject bo, RESTClient client) {
		String entityClassId = bo['boMetaId']
		trace("Update EO ${entityClassId}") {
			def url = REST_BASE_URL + '/bos/' + entityClassId + '/' + bo['boId']
			requestJson(url, HttpMethod.PUT, client.sessionId, bo.toString())
		}
	}

	/**
	 * Use {@link #getEntityObjects(org.nuclos.test.EntityClass, RESTClient)}.
	 */
	@Deprecated
	static JSONObject getBos(String boMetaId, RESTClient client, String searchFilterId = null) {
		def url = REST_BASE_URL + '/bos/' + boMetaId
		if (searchFilterId != null) {
			url += '?searchFilter=' + searchFilterId
		}
		return requestJson(url, HttpMethod.GET, client.sessionId, null)
	}

	/**
	 * Use {@link #getEntityObjects(org.nuclos.test.EntityClass, RESTClient)}.
	 */
	@Deprecated
	static JSONObject getBosByQueryContext(String boMetaId, Map queryContext, RESTClient client) {
		def url = REST_BASE_URL + '/bos/' + boMetaId + '/query';
		return requestJson(url, HttpMethod.POST, client.sessionId, JsonOutput.toJson(queryContext));
	}

	static byte[] exportNuclet(String nucletName, RESTClient client) {
		def url = REST_BASE_URL + '/maintenance/nucletexport/' + nucletName
		InputStream is = requestInputStream(url, HttpMethod.POST, client.getSessionId());
		return StreamUtils.copyToByteArray(is)
	}

	/**
	 * Use the system entity "User" and save it via {@link #save(org.nuclos.test.EntityObject, RESTClient)}.
	 */
	@Deprecated
	static JSONObject createUser(
			String name,
			String password,
			List roles,
			RESTClient client
	) {

		def url = REST_BASE_URL + '/user'

		def user = null
		try {
			// user  exists
			return requestJson(url + '/' + name, HttpMethod.GET, client.sessionId, null)
		} catch (ignore) {
		}

		def userData = [
				"email"      : name + "@nuclos.de",
				"email2"     : name + "@nuclos.de",
				"firstname"  : 'Firstname',
				"lastname"   : 'Lastname',
				"locked"     : false,
				"username"   : name,
				"newPassword": password,
				"superuser"  : false,
				"roles"      : roles
		]

		user = requestJson(url, HttpMethod.POST, client.sessionId, JsonOutput.toJson(userData))
		return user
	}

	static void changePassword(String username, String pwold, String pwnew, RESTClient client) {
		String url = REST_BASE_URL + "/user/" + username + "/password"
		requestString(url, HttpMethod.PUT, client.sessionId, JsonOutput.toJson(["oldPassword":pwold, "newPassword":pwnew]))
	}

	@PackageScope
	static boolean deleteUserByEmail(
			String email,
			SuperuserRESTClient client
	) {
		QueryOptions options = new QueryOptions(where: "org_nuclos_system_User_email='$email'")
		List<EntityObject<String>> results = getEntityObjects(SystemEntities.USER, client, options)

		if (!results.empty) {
			delete(results[0], client)
			return true
		}

		return false
	}

	@PackageScope
	static void executeCustomRule(
			final EntityObject eo,
			final String ruleName,
			final RESTClient client
	) {
		eo.executeCustomRule = ruleName
		save(eo, client)
	}

	@PackageScope
	static String managementConsole(
			String command,
			String arguments,
			RESTClient client
	) {

		def url = REST_BASE_URL + '/maintenance/managementconsole/' + command + '/'
		if (arguments) {
			String urlEncodedArguments = URLEncoder.encode(arguments, "UTF-8")
			url += urlEncodedArguments + '/'
		}

		requestString(url, HttpMethod.POST, client.sessionId)
	}

	@PackageScope
	static String invalidateServerCaches(SuperuserRESTClient client) {
		managementConsole('invalidateAllCaches', null, client)
	}

	@PackageScope
	static <PK> List<EntityObject<PK>> getEntityObjects(
			EntityClass<PK> entityClass,
			RESTClient client,
			QueryOptions options = null
	) {
		String url = REST_BASE_URL + '/bos/' + entityClass.fqn

		if (options) {
			// TODO: Make sure that url does not already contain a query
			url += '/query?' + options.toQuery()
		}

		JSONObject json = requestJson(url, HttpMethod.GET, client.sessionId, null)
		if (json == null) {
			return null
		}

		json.getJSONArray('bos').collect { JSONObject eoJson ->
			eoFromJson(entityClass, eoJson, client)
		}
	}

	@PackageScope
	static JSONArray getSearchResult(String search, RESTClient client) {
		String url = REST_BASE_URL + '/data/search/' + search
		return requestJsonArray(url, HttpMethod.GET, client.sessionId, null)
	}

	/**
	 * TODO: Do not mix test code with framework code
	 */
	@Deprecated
	@PackageScope
	static void expectSearchResults(String search, int hits, RESTClient client, int wait) {
		if (wait > 0) {
			sleep(wait)
		}
		assert getSearchResult(search, client).length() == hits
	}

	@PackageScope
	static <PK, SubPK> List<EntityObject<SubPK>> loadDependents(
			EntityObject<PK> eo,
			EntityClass<SubPK> subEoClass,
			String referenceAttributeId,
			RESTClient client
	) {
		String url = REST_BASE_URL +
				'/bos/' + eo.entityClass.fqn +
				'/' + eo.id +
				'/subBos/' + referenceAttributeId

		return requestSubEoList(url, client, subEoClass)
	}

	@PackageScope
	static <PK, SubPK> List<EntityObject<SubPK>> loadDependentsRecursively(
			final RESTClient client,
			final EntityObject<PK> eo,
			EntityClass<SubPK> subEoClass,
			final RecursiveDependency dependency,
			final QueryOptions queryOptions = null
	) {
		String url = REST_BASE_URL +
				'/bos/' + eo.entityClass.fqn +
				'/' + eo.id +
				'/subBos/recursive' +
				dependency.toUrlPath()

		return requestSubEoList(url, client, subEoClass, queryOptions)
	}

	private static <SubPK> List<EntityObject<SubPK>> requestSubEoList(
			String url,
			RESTClient client,
			EntityClass<SubPK> subEoClass,
			final QueryOptions queryOptions = null
	) {

		String jsonBody = null
		HttpMethod method =  HttpMethod.GET

		if (queryOptions) {
			if (queryOptions.method == HttpMethod.POST) {
				// TODO: Make sure that url does not already contain a query
				url += '/query'
				jsonBody = queryOptions.toJson()
			} else {
				// TODO: Make sure that url does not already contain a query
				url += '?' + queryOptions.toQuery()
			}
		}


		JSONObject json = requestJson(url, method, client.sessionId, jsonBody)
		if (json == null) {
			return null
		}

		json.getJSONArray('bos').collect { JSONObject eoJson ->
			eoFromJson(subEoClass, eoJson, client)
		}
	}

	/**
	 * TODO: Proper typing instead of JSONObject.
	 */
	@PackageScope
	static JSONObject getEntityMeta(
			final EntityClass entityClass,
			final RESTClient client
	) {
		String url = REST_BASE_URL + '/boMetas/' + entityClass.fqn

		requestJson(url, HttpMethod.GET, client.sessionId, null)
	}

	@PackageScope
	static <PK> EntityObject<PK> getEntityObject(EntityClass<PK> entityClass, PK id, RESTClient client) {
		String url = REST_BASE_URL + '/bos/' + entityClass.fqn + '/' + id

		JSONObject json = requestJson(url, HttpMethod.GET, client.sessionId, null)

		if (json == null) {
			return null
		}
		eoFromJson(entityClass, json, client)
	}

	@PackageScope
	static <PK> void saveAll(List<EntityObject<PK>> eos, RESTClient client) {
		// TODO: Save all at once (see NUCLOS-5650)
		eos.each {
			save(it, client)
		}
	}

	@PackageScope
	static <PK> PK save(EntityObject<PK> eo, RESTClient client) {
		JSONObject json
		String url = REST_BASE_URL + '/bos/' + eo.getEntityClass().fqn

		if (eo.new) {
			json = postEntityObject(url, eo, client)
		} else {
			url += '/' + eo.id
			json = putEntityObject(url, eo, client)
		}

		if (json != null) {
			eo.updateFromJson(json)
		}

		eo.client = client

		return eo.getId()
	}

	@PackageScope
	static <PK> EntityObject<PK> clone(EntityObject<PK> eo, RESTClient client, String layout = null) {
		String url = REST_BASE_URL + '/bos/' + eo.getEntityClass().fqn + '/' + eo.id + '/clone' +
				'?layoutid=' + layout

		EntityObject<PK> clonedEo = new EntityObject<PK>(eo.entityClass);
		clonedEo.client = client;
		clonedEo.updateFromJson(requestJson(url, HttpMethod.GET, client.sessionId))

		return clonedEo
	}

	@PackageScope
	static <PK> boolean delete(EntityObject<PK> eo, RESTClient client) {
		boolean result = delete(
				eo.entityClass.fqn,
				eo.id,
				client
		)
		if (result) {
			eo.setBoId(null)
		}
		return result
	}

	@PackageScope
	static <PK> boolean delete(
			final String entityClassId,
			final PK id,
			RESTClient client
	) {
		String url = REST_BASE_URL + '/bos/' + entityClassId + '/' + id
		String response = requestString(url, HttpMethod.DELETE, client.sessionId)
		if (response == null) {
			return false
		}
		return true
	}

	@PackageScope
	static <PK> void changeState(EntityObject<PK> eo, int statusNumeral, RESTClient client) {
		String url = REST_BASE_URL + '/boStateChanges/' + eo.getEntityClass().fqn + '/' + eo.getId() + '/' + statusNumeral

		requestString(url, HttpMethod.PUT, client.sessionId, '{}')
	}

	private static <PK> JSONObject postEntityObject(String url, EntityObject<PK> eo, RESTClient client) {
		postJson(url, eo.toJson(), client)
	}

	static JSONObject postJson(final String url, final String json, final RESTClient client) {
		requestJson(url, HttpMethod.POST, client.sessionId, json)
	}

	static JSONObject putJson(final String url, final String json, final RESTClient client) {
		requestJson(url, HttpMethod.PUT, client.sessionId, json)
	}

	private static <PK> JSONObject putEntityObject(String url, EntityObject<PK> eo, RESTClient client) {
		requestJson(url, HttpMethod.PUT, client.sessionId, eo.toJson())
	}

	@PackageScope
	static Map<String, String> getSimpleSystemParameters(
			SuperuserRESTClient client
	) {
		getSystemParameters(client).collectEntries {
			[(it.getAttribute('name')): it.getAttribute('value')]
		}
	}

	@PackageScope
	static List<EntityObject<String>> getSystemParameters(
			SuperuserRESTClient client
	) {
		getEntityObjects(SystemEntities.PARAMETER, client)
	}

	@PackageScope
	static List<EntityObject<String>> setSystemParameters(
			Map<String, String> newParams,
			SuperuserRESTClient client
	) {
		List<EntityObject<String>> params = getSystemParameters(client)

		params.each {
			delete(it, client)
		}

		patchSystemParameters(newParams, client)
	}

	@PackageScope
	static List<EntityObject<String>> patchSystemParameters(
			Map<String, String> newParams,
			SuperuserRESTClient client
	) {
		List<EntityObject<String>> params = getSystemParameters(client)

		newParams.each { String key, String value ->
			EntityObject<String> param = params.find { it.getAttribute('name') == key }

			if (!param) {
				param = new EntityObject(SystemEntities.PARAMETER)

				param.setAttribute('name', key)
				param.setAttribute('description', key)

				params << param
			}

			param.setAttribute('value', value)
			save(param, client)
		}

		return params
	}

	private static <PK> EntityObject<PK> eoFromJson(
			EntityClass<PK> entityClass,
			JSONObject json,
			RESTClient client
	) {
		EntityObject<PK> eo = new EntityObject(entityClass)

		eo.updateFromJson(json)
		eo.client = client

		return eo
	}

	static JSONArray getReferenceList(
			String referenceAttributeId,
			RESTClient client) {
		String url = REST_BASE_URL +
				'/data/referencelist/' + referenceAttributeId
		JSONArray jsonArray = requestJsonArray(url, HttpMethod.GET, client.sessionId, null)
		return jsonArray
	}

	static JSONArray getVlpData(String referenceAttributeId,
								String vlpType, String vlpUID, String idColumn, String nameColumn,
								String intid, RESTClient client) {
		String url = REST_BASE_URL +
				'/data/vlpdata' +
				'?reffield=' + referenceAttributeId +
				'&vlptype=' + vlpType +
				'&vlpvalue=' + vlpUID +
				'&id-fieldname=' + idColumn +
				'&fieldname=' + nameColumn +
				'&intid=' + intid
		JSONArray jsonArray = requestJsonArray(url, HttpMethod.GET, client.sessionId, null)
		return jsonArray
	}

	private static JSONObject requestJsonObject(
			final RESTClient client,
			final HttpRequestBase request
	) {
		request.setHeader(HttpHeaders.ACCEPT, 'application/json')
		String response = requestString(client, request)
		parseJsonObject(response)
	}

	private static JSONObject parseJsonObject(String response) {
		JSONObject result = null
		if (response != null) {
			try {
				result = new JSONObject(response)
			} catch (JSONException e) {
				e.printStackTrace()
			}
		}
		result
	}

	private static JSONArray parseJsonArray(String response) {
		JSONArray result = null
		if (response != null) {
			try {
				result = new JSONArray(response)
			} catch (JSONException e) {
				e.printStackTrace()
			}
		}
		result
	}

	static String requestString(
			final RESTClient client,
			final HttpUriRequest request
	) {
		HttpResponse response = client.httpClient.execute(request)
		String responseText = null

		if (response.entity) {
			responseText = getStreamText(response.entity.content)
		}

		if ((response.statusLine.statusCode / 100).toInteger() != 2) {
			throw new RESTException(
					response.statusLine.statusCode,
					responseText
			)
		}

		return responseText
	}

	static RestResponse requestResponse(
			final RESTClient client,
			final HttpRequestBase request
	) {
		HttpResponse response = client.httpClient.execute(request)

		new RestResponse(response)
	}

	@PackageScope
	static void putServerLogLevel(
			final SuperuserRESTClient client,
			final ServerLogger serverLogger,
			final Level level) {
		String url = "$REST_BASE_URL/maintenance/logging/$serverLogger/level"

		JSONObject data = new JSONObject()
		data.put("level", level.toString())

		HttpPut put = new HttpPut(url)
		addJson(put, data)

		requestString(client, put)
	}

	private static void addJson(
			HttpEntityEnclosingRequestBase request,
			JSONObject data
	) {
		StringEntity entity = new StringEntity(data.toString())
		request.setEntity(entity)
		request.setHeader(HttpHeaders.CONTENT_TYPE, 'application/json')
	}

	static <PK, PK2> EntityObject<PK2> generateObject(
			final EntityObject<PK> eo,
			final String generatorFqn,
			final EntityClass<PK2> resultClass,
			final RESTClient client
	) {
		final String url = REST_BASE_URL + '/boGenerations' +
				'/' + eo.getEntityClass().fqn +
				'/' + eo.getId() +
				'/' + eo.getVersion() +
				'/generate/' + generatorFqn

		final JSONObject json = postJson(url, new JSONObject().toString(), client)

		if (json.has('businessError')) {
			throw new RestClientException("Business Error: " + json.get('businessError'))
		}

		EntityObject.fromJson(resultClass, json.getJSONObject('bo'))
	}

	static <PK> void deleteAll(
			final Collection<EntityObject<PK>> entityObjects,
			final RESTClient restClient
	) {
		// TODO: Delete all at once (see NUCLOS-5650)
		entityObjects.each {
			delete(it, restClient)
		}
	}

	static File exportResultList(
			final EntityClass entity,
			final String outputFormat,
			final RESTClient client
	) {
		final String url = REST_BASE_URL + '/bos/' + entity.fqn +
				'/boListExport' +
				'/' + outputFormat +
				'/false' +
				'/false'
		JSONObject json = requestJson(
				url,
				HttpMethod.POST,
				client.getSessionId(),
				new JSONObject().toString()
		)
		String downloadUrl = json.getJSONObject('links').getJSONObject('export').getString('href')

		// URL might be protocol-relative
		if (downloadUrl.startsWith('//')) {
			downloadUrl = 'http:' + downloadUrl
		}

		downloadFile(downloadUrl.toURL(), client)
	}

	static File downloadFile(final URL url, final RESTClient client) {
		HttpGet request = new HttpGet(url.toURI())
		request.setHeader("JSESSIONID", client.getSessionId())
		RestResponse response = requestResponse(client, request)
		InputStream responseStream = response.getHttpResponse().getEntity().getContent()

		File file = Files.createTempFile('nuclos_rest_download', null).toFile()

		BufferedOutputStream stream = file.newOutputStream()
		stream << responseStream
		stream.close()

		return file
	}

	static String getDescriptionToStatus(
			String statusId,
			RESTClient client) {
		String url = REST_BASE_URL +
				'/data/statusinfo/' + statusId
		JSONObject json = requestJson(
				url,
				HttpMethod.GET,
				client.getSessionId()
		)
		return json.get('description')
	}

	static List<News> getCurrentNews(final RESTClient client) {
		getNews(
				REST_BASE_URL + '/news',
				client
		)
	}

	static List<News> getUnconfirmedNews(final RESTClient client) {
		getNews(
				REST_BASE_URL + '/news/unconfirmed',
				client
		)
	}

	static List<News> getNews(final String url, final RESTClient client) {
		final String json = requestString(
				url,
				HttpMethod.GET,
				client.getSessionId()
		)

		new ObjectMapper().readValue(json, News[].class).toList()
	}

	static String callCustomRestRuleWithSqlCountCheck(RESTClient client, HttpRequestBase base,
	                                                  int maxSqlCount, int maxSqlWriteCount) {
		base.setHeader(HttpHeaders.CONTENT_TYPE, 'application/json')

		RestResponse response = RESTHelper.requestResponse(client, base)

		assert  response.getSqlCount() <= maxSqlCount
		assert  response.getSqlInsertUpdateDeleteCount() <= maxSqlWriteCount

		InputStream is = response.getHttpResponse().getEntity().getContent()
		String s = StreamUtils.copyToString(is, Charsets.UTF_8)

		return s
	}
}
