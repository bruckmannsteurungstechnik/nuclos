package org.nuclos.test.webclient

import java.util.concurrent.Callable
import java.util.concurrent.TimeUnit

import org.codehaus.groovy.reflection.ReflectionUtils
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.IntegrationTest
import org.nuclos.test.NuclosTestContext
import org.nuclos.test.log.Log
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.request.RequestCounts
import org.nuclos.test.webclient.pageobjects.AuthenticationComponent
import org.nuclos.test.webclient.pageobjects.LocaleChooser
import org.nuclos.test.webclient.pageobjects.MessageModal
import org.nuclos.test.webclient.util.Screenshot
import org.nuclos.test.webclient.util.ValueFormatter
import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.NoSuchWindowException
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.logging.LogEntries
import org.openqa.selenium.logging.LogEntry
import org.openqa.selenium.logging.LogType
import org.openqa.selenium.remote.RemoteWebDriver
import org.openqa.selenium.support.ui.Select
import org.testcontainers.containers.BrowserWebDriverContainer

import groovy.transform.CompileStatic
import net.lightbody.bmp.core.har.Har
import net.lightbody.bmp.proxy.CaptureType
import org.awaitility.Awaitility;

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
abstract class AbstractWebclientTest extends AbstractNuclosTest {

	/**
	 * Callback script for Selenium that waits for the Angular 2 app to become stable
	 * and have no pending macro tasks (e.g. asynchronous requests).
	 */
	static final String WAIT_FOR_ANGULAR_SCRIPT = '''
// Check if we really are on a Nuclos Webclient page
if (document.getElementsByTagName('nuc-root').length == 0) {
	return true;
}

// NgZone may not be available yet, we have to retry later
if (!window['NgZone']) {
	console.error('No NgZone!');
	return false;
}

if (NgZone.isStable && !NgZone.hasPendingMacrotasks){
	return true;
} else {
	console.log('waitForAngular: has macro tasks');
	return false;
}
'''

	static RemoteWebDriver driver

	/**
	 * If a Docker container is started for this test, we should stop it again after each test class.
	 * It would also be automatically stopped when the JVM shuts down, but there could be lots of
	 * unnecessarily running containers in the meantime, if we run a lot of tests.
	 */
	static BrowserWebDriverContainer browserContainer

	@BeforeClass
	static void setup() {
		setup(true)
	}

	/**
	 * @param callbackBeforeWebclientStart
	 * 	Will be called after "truncate tables" and before the Webclient ist started.
	 * 	Can be used to prepare test data (via REST) that must be available for the initial Webclient startup.
	 */
	static void setup(boolean withTestUserLogin, Closure<?> callbackBeforeWebclientStart = null) {
		Log.debug 'Setup'

		Runtime.addShutdownHook {
			shutdown()
		}

		AbstractNuclosTest.setup()

		try {
			if (callbackBeforeWebclientStart) {
				callbackBeforeWebclientStart()
			}

			driver = WebclientTestContext.instance.initDriver()

			resizeBrowserWindow()
			getUrl(WebclientTestContext.instance.jscoverageClearStorageUrl)
			openStartpage()
			waitForAngularRequestsToFinish()

			// clear localStorage - needed for phantomjs
//			executeScript('localStorage.clear();')

			nuclosSession.managementConsole('resetCompleteMandatorSetup')

			if (withTestUserLogin) {
				RESTHelper.createUser('test', 'test', ['Example user'], nuclosSession)

				login('test', 'test')
			}

		}

		catch (Throwable ex) {
			fail(ex.message, ex)
		}

		Log.debug 'Setup done'
	}

	@AfterClass
	static void teardown() {
		waitForAngularRequestsToFinish()
		try {
			executeScript("jscoverage_report();")
			executeAsyncScript('var callback = arguments[arguments.length - 1];callback();')
		}
		catch (Exception ignore) {
			Log.warn "Could not create jscoverage report"
		}

		checkForPageError()

		shutdown()
	}

	private static void checkForPageError() {
		// There should be no unhandled script errors
		def error = pageError
		if (error) {
			printBrowserDebugInfos()
			throw new AssertionError("Found unhandled error: $error")
		}
	}

	/**
	 * Extract possible (JavaScript) errors that occured on the page.
	 */
	static String getPageError() {
		$('body').getAttribute('error')
	}

	static NuclosWebElement $(String selector) {
		Log.info("Selecting: \"$selector\"")
		waitForAngularRequestsToFinish()
		List<WebElement> elements = driver.findElements(By.cssSelector(selector))
		elements.empty ? null : new NuclosWebElement(elements.get(0))
	}

	static NuclosWebElement $(WebElement e, String selector) {
		List<WebElement> elements = e.findElements(By.cssSelector(selector))
		elements.empty ? null : new NuclosWebElement(elements.get(0))
	}

	static List<NuclosWebElement> $$(String selector) {
		Log.info("Selecting: \"$selector\"")
		driver.findElements(By.cssSelector(selector)).collect { new NuclosWebElement(it) }
	}

	static List<NuclosWebElement> $$(WebElement e, String selector) {
		e.findElements(By.cssSelector(selector)).collect { new NuclosWebElement(it) }
	}

	static void openStartpage() {
		getUrl(WebclientTestContext.instance.nuclosWebclientBaseURL)
	}

	/**
	 * Counts all browser requests.
	 * Does not count direct REST requests via RESTClient / RESTHelper.
	 */
	static RequestCounts countBrowserRequests(Closure c) {
		WebclientTestContext.instance.browserProxy.countRequests(c)
	}

	/**
	 * Gets the HAR using default capture settings.
	 * Does not capture response content.
	 * Does not capture direct REST request via RESTClient / RESTHelper.
	 */
	static Har getBrowserHar(Closure c) {
		WebclientTestContext.instance.browserProxy.getHar([], c)
	}

	/**
	 * Gets the HAR using the given capture settings.
	 * Here you can capture resopnse content via {@link net.lightbody.bmp.proxy.CaptureType#RESPONSE_CONTENT}
	 */
	static Har getBrowserHar(List<CaptureType> captureTypes, Closure c) {
		WebclientTestContext.instance.browserProxy.getHar(captureTypes, c)
	}

	static boolean login(String username = 'nuclos', String password = '', boolean autologin = false) {
		return login(new LoginParams(
				username: username,
				password: password,
				autologin: autologin
		))
	}

	/**
	 * Performs a Login with the given credentials and the current locale, if not logged in already.
	 */
	static boolean login(LoginParams params) {
		if (isPresent('#logout')) {
			return false
		}

		loginUnsafe(params)

		assert $('#logout')
		return true
	}

	static boolean isLoggedIn() {
		isPresent('#logout')
	}

	/**
	 * Performs a Login with the given credentials and the current locale.
	 * Without any checks for login success etc.
	 */
	static void loginUnsafe(LoginParams params) {
		if (params.locale) {
			LocaleChooser.locale = params.locale
		} else {
			LocaleChooser.locale = context.locale
		}

		AuthenticationComponent.username = params.username
		AuthenticationComponent.password = params.password
		AuthenticationComponent.autologin = params.autologin

		AuthenticationComponent.submit()

		if (params.mandator) {
			if (params.mandator == 'null') {
				// superuser login without any mandator selection
				$('#mandatorless-session').click()
			} else {
				WebElement chooseMandator = $('#choose-mandator')
				Select droplist = new Select(chooseMandator)
				droplist.selectByVisibleText(params.mandator)
				$('#choose-mandator-button').click()
			}
		}
	}

	/**
	 * Does not fail the build, if "waitForAngular" did not succeed.
	 */
	static void tryToWaitForAngular(int timeout) {
		Closure waitClosure = {
			windowClosed || executeScript(WAIT_FOR_ANGULAR_SCRIPT)
		}
		if (!doWaitFor(timeout, waitClosure)) {
			Log.warn("Failed to wait for angular requests to finish")
		}
	}

	static void waitForAngularRequestsToFinish() {
		waitForAngularRequestsToFinish(NuclosTestContext.instance.DEFAULT_TMEOUT)
	}

	static void waitUntilTrue(Callable<Boolean> conditionEvaluator) {
		Awaitility.await().atMost(30, TimeUnit.SECONDS).until(conditionEvaluator);
	}

	/**
	 * Waits for Angular-2 to finish loading by waiting for the "onStable" event of NgZone (if not already stable).
	 * NgZone is set as a global Javascript variable by the Webclient2 app component.
	 *
	 * @param timeout
	 */
	static void waitForAngularRequestsToFinish(int timeout) {
		Log.debug "Waiting for Angular requests to finish"
		waitFor(timeout) {
			windowClosed || executeScript(WAIT_FOR_ANGULAR_SCRIPT)
		}
	}

	static String getCurrentUrl() {
		try {
			waitForAngularRequestsToFinish()
			return driver.getCurrentUrl()
		}
		catch (Throwable ex) {
			Log.error 'Could not get current URL', ex
		}

		return null
	}

	/**
	 * Takes a screenshot and saves it under the given name,
	 * if screenshots are enabled or "force" is true.
	 */
	static void screenshot(String name, boolean force = false) {
		if (!WebclientTestContext.instance.takeScreenshots && !force) {
			Log.debug("Skipping screenshot '$name'")
			return
		}

		waitForAngularRequestsToFinish()
		String caller = ReflectionUtils.callingClass.simpleName
		// get caller - ignore superclass in stacktrace
		for (def l = 0; l < 10; l++) {
			def callerName = ReflectionUtils.getCallingClass(l).name
			if (callerName.indexOf("AbstractWebclient") == -1) {
				caller = callerName
				break
			}
		}
		Screenshot.take(caller, name)
	}

	static boolean logout() {
		if (!isPresent('#logout')) {
			return false
		}

		waitForAngularRequestsToFinish()

		$('#user-menu .dropdown').click()
		$('#logout').click()

		waitForAngularRequestsToFinish()

		assert !$('#logout')

		return true
	}

	static boolean isPresent(String selector) {
		try {
			def element = $(selector)
			return !!element
		}
		catch (Exception ignore) {
			return false
		}
	}

	/**
	 * Reloads the current page in the browser.
	 */
	static void refresh() {
		Log.info "Refreshing: $currentUrl"

		// getDriver().navigate().refresh() does not always work with PhantomJS
		if (WebclientTestContext.instance.getBrowser() != Browser.PHANTOMJS) {
			driver.navigate().refresh()
		} else {
			getUrl(currentUrl)
		}

		waitForAngularRequestsToFinish()
	}

	static void getUrl(String url) {
		Log.debug "Get: $url"
		driver.get(url)

		// Give the browser and app some time to start loading/redirecting
		sleep(1000)
	}

	/**
	 * @param hash The URL hash without leading '#'
	 */
	static void getUrlHash(String hash) {
		Log.debug "Get hash: $hash"
		try {
			executeScript("window.location.hash='$hash'")
			waitForAngularRequestsToFinish()
		}
		catch (Exception ex) {
			Log.warn "Could not set URL hash $hash", ex
		}
	}

	static void assertNotFound() {
		assertError('404')
	}

	static void assertAccessDenied() {
		assert driver.currentUrl.contains('/error/403')
		assertError('403')
	}

	static void assertError(String s = '') {
		String error = errorMessage

		assert error
		assert !error.empty
		assert error.contains(s)
	}

	static String getErrorMessage() {
		return $('nuc-error .card-header')?.text
	}

	/**
	 * Returns a currently displayed modal message dialog.
	 */
	static MessageModal getMessageModal() {
		def element = $('.modal-dialog')
		if (!element) {
			return null
		}

		String title = element.$('.modal-title').text.trim()
		String message = element.$('.modal-body').text.trim()

		assert !(title ==~ /.*\{\d*}.*/), 'Message parameters were not resolved'
		assert !(message ==~ /.*\{\d*}.*/), 'Message parameters were not resolved'

		new MessageModal(
				element: element,
				title: title,
				message: message
		)
	}

	static void assertNoMessageModel() {
		MessageModal messageModal = getMessageModal()

		if (messageModal) {
			// show message in test result
			assert !messageModal.message
		}

		assert !messageModal
	}

	static void assertMessageModalAndConfirm(String title, String partOfMessage) {
		MessageModal messageModal = getMessageModal()
		assert messageModal

		assert messageModal.title == title

		if (partOfMessage != null) {
			assert messageModal.message.contains(partOfMessage)
		}

		messageModal.confirm()

		assert !$('.modal-dialog')
	}

	/**
	 * @return The handle of the newly opened window.
	 */
	static String duplicateCurrentWindow() {
		Set<String> handles = driver.windowHandles

		String url = currentUrl
		executeScript("window.open('$url', '_blank')")

		Set<String> newHandles = driver.windowHandles
		assert newHandles.size() == handles.size() + 1

		String newWindowHandle = (newHandles - handles).first()
		driver.switchTo().window(newWindowHandle)
		getUrl(url)

		return newWindowHandle
	}

	static boolean isWindowClosed() {
		try {
			!driver.windowHandles.contains(driver.windowHandle)
		} catch (NoSuchWindowException ignore) {
			return true
		}
	}

	static List<WebElement> luceneSearch(String s) {

		WebElement fullTextSearch = $('#full-text-search')
		assert fullTextSearch != null

		WebElement input = fullTextSearch.findElement(By.className('full-text-search-input'))
		assert input != null

		input.clear()
		input.sendKeys(s)

		screenshot('afterLuceneSearch')

		return fullTextSearch.findElements(By.className('full-text-search-result-item'))
	}

	static void luceneTest(String s, int expected) {
		waitForAngularRequestsToFinish()

		try {
			waitFor(10) {
				luceneSearch(s).size() == expected
			}
		} catch (Exception ignore) {
			def size = luceneSearch(s).size()
			screenshot('beforeExceptionInLuceneTest')
			assert size == expected
		}

		screenshot('afterLuceneSearchAndWait')
	}

	static void sendKeysAndWaitForAngular(CharSequence... keys) {
		sendKeys(keys)
		waitForAngularRequestsToFinish()
	}

	static void sendNumber(Number number) {
		sendKeys(ValueFormatter.formatNumber(number))
	}

	/**
	 * Sends the keys to the active element, how a real user would do it.
	 */
	static void sendKeys(CharSequence... keys) {
		new Actions(driver)
				.sendKeys(keys)
				.build()
				.perform()
	}

	/**
	 * Performs a general click at the location of the given element,
	 * like a real user would to it.
	 */
	static void click(NuclosWebElement e) {
		new Actions(driver)
				.click(e.element)
				.build()
				.perform()
	}

	static void shutdown() {
		try {
			driver?.quit()
		}
		catch (Throwable t) {
			Log.warn "Could not quit selenium driver", t
		}

		try {
			browserContainer?.stop()
		} catch (Throwable t) {
			Log.warn "Could not stop the browser container", t
		}

		try {
			WebclientTestContext.instance.browserstackLocal?.stop()
		} catch (Exception e) {
			Log.warn 'Failed to stop BrowserStack Local', e
		}
	}

	static void resizeBrowserWindow() {
		driver.manage().window().setSize(WebclientTestContext.instance.preferredWindowSize)
	}

	static void printBrowserDebugInfos() {
		if (!driver) {
			Log.warn 'Could not print browser debug infos - browser not started'
			return
		}

		printBrowserLog()
		printCookies()

		Log.error "Last URL: $currentUrl"
		screenshot('failure', true)
	}

	private static void printBrowserLog() {
		try {
			LogEntries logEntries = driver.manage().logs().get(LogType.BROWSER)
			for (LogEntry entry : logEntries) {
				Log.info "[$WebclientTestContext.instance.browser] " + new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage()
			}
		}
		catch (Exception ex) {
			Log.warn "Failed to get browser logs for $WebclientTestContext.instance.browser: $ex.message"
		}
	}

	private static void printCookies() {
		try {
			Log.info "Browser Cookies:"
			driver.manage().cookies.each {
				Log.info it.toString()
			}
		}
		catch (Exception ex) {
			Log.warn "Failed to get browser cookies for $WebclientTestContext.instance.browser: $ex.message"
		}
	}

	/**
	 * Switches to another browser window, if more than 1 window exists.
	 */
	static String switchToOtherWindow() {
		def currentWindow = driver.windowHandle
		def otherWindow = driver.windowHandles.find { it != currentWindow }

		if (otherWindow) {
			driver.switchTo().window(otherWindow)
//			resizeBrowserWindow()
			waitForAngularRequestsToFinish()
			return otherWindow
		}

		return null
	}

	static void closeOtherWindows() {
		def currentWindow = driver.windowHandle

		driver.windowHandles.each {
			if (it != currentWindow) {
				driver.switchTo().window(it)
				driver.close()
			}
		}

		driver.switchTo().window(currentWindow)
	}

	static void blur() {
		executeScript('document.activeElement.blur();')
	}

	static Object executeScript(String script, Object... args) {
		((JavascriptExecutor) driver).executeScript(script, args)
	}

	static Object executeAsyncScript(String script, Object... args) {
		((JavascriptExecutor) driver).executeAsyncScript(script, args)
	}

	/**
	 * Downloads a file via AJAX in the browser (as the current user).
	 * Use only for small files!
	 */
	static byte[] downloadFile(String url) {
		executeAsyncScript("""
var callback = arguments[arguments.length - 1];
var oReq = new XMLHttpRequest();
oReq.open("GET", "$url", true);
oReq.responseType = "arraybuffer";
oReq.withCredentials = true;

oReq.onload = oEvent => {
	var arrayBuffer = oReq.response; // Note: not oReq.responseText
	if (arrayBuffer) {
		var byteArray = new Uint8Array(arrayBuffer);
		callback(byteArray);
	} else {
		callback(arrayBuffer)
	}
};
oReq.onerror = error => {
	callback(error);
};

oReq.send(null);
""") as byte[]
	}

	static boolean isHorizontalScrollbarPresent() {
		executeScript('return document.documentElement.scrollWidth > document.documentElement.clientWidth;')
	}

	static boolean isVerticalScrollbarPresent() {
		executeScript('return document.documentElement.scrollHeight > document.documentElement.clientHeight;')
	}

	static highlight(WebElement element) {
		executeScript('arguments[0].style.border="3px solid red"', element)
	}

	static RemoteWebDriver getDriver() {
		driver
	}

	static Locale getLocale() {
		context.locale
	}
}
