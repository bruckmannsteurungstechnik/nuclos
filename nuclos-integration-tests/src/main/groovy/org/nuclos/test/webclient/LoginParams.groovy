package org.nuclos.test.webclient

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class LoginParams {
	String username
	String password = ''
	boolean autologin = false
	String mandator = null
	Locale locale

	// TODO: Should be a Locale?!
	String datalanguage
}
