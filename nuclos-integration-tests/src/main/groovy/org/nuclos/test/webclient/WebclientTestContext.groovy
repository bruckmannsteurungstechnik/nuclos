package org.nuclos.test.webclient

import static org.nuclos.test.NuclosTestContext.initFromSystemProperty

import java.util.concurrent.TimeUnit

import org.nuclos.test.NuclosTestContext
import org.openqa.selenium.Dimension
import org.openqa.selenium.remote.LocalFileDetector
import org.openqa.selenium.remote.RemoteWebDriver

import com.browserstack.local.Local
import com.icegreen.greenmail.util.ServerSetup

import groovy.transform.CompileStatic

/**
 * The context should not be changed by the tests!
 * It is therefore marked as @Immutable, although it still has some fields that are not
 * immutable. Those are ignored for now via "knownImmutableClasses".
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
@Singleton(lazy = true)
class WebclientTestContext {

	final NuclosTestContext context = NuclosTestContext.instance

	final int DEFAULT_TIMEOUT = context.DEFAULT_TMEOUT

	// The following settings can be overriden with System Properties
	final String nuclosWebclientProtocol = initFromSystemProperty('nuclos.webclient.protocol', 'http')
	final String nuclosWebclientHost = initFromSystemProperty('nuclos.webclient.host', context.nuclosServerHost)
	final String nuclosWebclientPort = initFromSystemProperty('nuclos.webclient.port', '4200')

	//Mail Server Settings for testing (Greenmail)
	final String greenmailHost = initFromSystemProperty('greenmail.host', ServerSetup.SMTP.bindAddress)
	final String greenmailPort = initFromSystemProperty('greenmail.port', ServerSetup.SMTP.protocol)

	final String seleniumServer = initFromSystemProperty('selenium.server', null)
	final Browser browser = initFromSystemProperty('browser', { String name ->
		Browser.find {
			"$it".toLowerCase() == name.toLowerCase()
		}
	}, Browser.CHROME)

	final String nuclosWebclientServer = "$nuclosWebclientProtocol://$nuclosWebclientHost:$nuclosWebclientPort"
	final String baseUrl = "$nuclosWebclientServer/index.html"
	final String jscoverageClearStorageUrl = "$nuclosWebclientServer/jscoverage-clear-local-storage.html"

	final Dimension preferredWindowSize = new Dimension(1024, 768)

	final String nuclosWebclientBaseHref = initFromSystemProperty('nuclos.webclient.basehref', '')
	final String nuclosWebclientBaseURL = "$nuclosWebclientServer$nuclosWebclientBaseHref/index.html"

	final boolean takeScreenshots = initFromSystemProperty('nuclos.webclient.screenshots', false)

	final BrowserProxy browserProxy = initProxy()

	final String browserstackBuild = initFromSystemProperty('browserstack.build', null)
	final String browserstackProject = initFromSystemProperty('browserstack.project', null)
	final String browserstackUser = initFromSystemProperty('browserstack.user', null)
	final String browserstackKey = initFromSystemProperty('browserstack.key', null)
	final Map<String, String> browserstackArgs
	final Local browserstackLocal = initBrowserstackLocal()

	RemoteWebDriver initDriver() {
		RemoteWebDriver driver = browser.createDriver(this)
		driver.setFileDetector(new LocalFileDetector())
		driver.manage().timeouts().setScriptTimeout(context.DEFAULT_TMEOUT, TimeUnit.SECONDS)

		return driver
	}

	private BrowserProxy initProxy() {
		def proxy = new BrowserProxy(this)
		proxy.start()
		return proxy
	}

	/**
	 * Starts the BrowserStack Local binary, which opens a tunnel to BrowserStack.
	 * This must be done for every test because of possibly different proxy settings.
	 */
	private Local initBrowserstackLocal() {
		Local local = new Local()
		if (initBrowserstackArgs()) {
			local.start(browserstackArgs)
		}
		return local
	}

	protected Map<String, String> initBrowserstackArgs() {
		if (browserstackArgs) {
			return browserstackArgs
		}

		final String key = System.getProperty('browserstack.key')
		if (key) {
			browserstackArgs << [
					'key'         : key,
					'v'           : 'true',
					'force'       : 'true',
					'onlyAutomate': 'true',
			]

			if (browserProxy) {
				browserstackArgs['-local-proxy-host'] = browserProxy.hostname
				browserstackArgs['-local-proxy-port'] = browserProxy.port.toString()
				browserstackArgs['-force-proxy'] = 'true'
			}
		}

		return browserstackArgs
	}

	Locale getLocale() {
		return context.locale
	}
}
