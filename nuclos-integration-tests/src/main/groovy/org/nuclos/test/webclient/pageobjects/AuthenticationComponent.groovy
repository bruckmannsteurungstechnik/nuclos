package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.$

import org.nuclos.test.webclient.NuclosWebElement

import groovy.transform.CompileStatic

/**
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class AuthenticationComponent extends AbstractPageObject {
	static String getErrorMessage() {
		$('div.alert')?.text
	}

	static void setUsername(String username) {
		NuclosWebElement element = $('#username')
		element.value = username
	}

	static void setPassword(String password) {
		NuclosWebElement element = $('#password')
		element.value = password
	}

	static void setAutologin(boolean autologin) {
		if (autologin) {
			$('#autologin').click()
		}
	}

	static void submit() {
		$('#submit').click()
	}

	/**
	 * Returns the trimmed text of the username label.
	 *
	 * @return
	 */
	static String getUsernameLabel() {
		$('label[for="username"]').text.trim()
	}

	/**
	 * Returns the trimmed text of the username label.
	 *
	 * @return
	 */
	static String getPasswordLabel() {
		$('label[for="password"]').text.trim()
	}

	static void clickSelfRegistration() {
		$('a[href$="/register"]').click()
	}

	static NuclosWebElement getForgotLoginDetailsLink() {
		$('#forgot-login-details-link')
	}
}
