package org.nuclos.test.webclient.pageobjects

import org.nuclos.test.webclient.AbstractWebclientTest
import org.openqa.selenium.By

import groovy.transform.CompileStatic

@CompileStatic
class Busy extends AbstractPageObject {

	static boolean isBusy() {
		!AbstractWebclientTest.driver.findElements(
				By.cssSelector('ngx-loading > .backdrop')
		).empty
	}
}
