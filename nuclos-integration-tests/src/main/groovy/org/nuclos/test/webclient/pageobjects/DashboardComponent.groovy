package org.nuclos.test.webclient.pageobjects

import groovy.transform.CompileStatic

/**
 * Represents the Dashboard component.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class DashboardComponent extends AbstractPageObject {

}
