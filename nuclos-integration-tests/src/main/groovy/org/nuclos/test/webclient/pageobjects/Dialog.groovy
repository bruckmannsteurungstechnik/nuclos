package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.$
import static org.nuclos.test.webclient.AbstractWebclientTest.$$
import static org.nuclos.test.webclient.AbstractWebclientTest.waitFor

import groovy.transform.CompileStatic

@CompileStatic
class Dialog {

	static close() {
		def nrOfmodals = $$('.modal').size()
		def button = $('nuc-confirm-modal-component #button-cancel, nuc-alert-modal-component button')

		button?.click()
		// wait until modal is closed correctly
		waitFor {
			$$('.modal').size() < nrOfmodals
		}

		return button
	}

}
