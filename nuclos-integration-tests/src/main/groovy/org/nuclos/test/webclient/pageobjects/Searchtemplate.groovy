package org.nuclos.test.webclient.pageobjects


import static org.nuclos.test.webclient.AbstractWebclientTest.$
import static org.nuclos.test.webclient.AbstractWebclientTest.$$
import static org.nuclos.test.webclient.AbstractWebclientTest.getDriver
import static org.nuclos.test.webclient.AbstractWebclientTest.waitForAngularRequestsToFinish

import org.apache.commons.lang.NotImplementedException
import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic

/**
 * TODO: Remove this completely.
 */
@CompileStatic
@Deprecated
class Searchtemplate extends AbstractPageObject {

	static WebElement getSearchfilterValueTextInput(String attributeFqn) { $('#search-value-string-' + attributeFqn) }

	static WebElement getSearchfilterValueDatepickerInput() {
		def elems = $$('.searchfilter-popover-content [ngbdatepicker]')
		return elems.size() > 0 ? elems.get(0) : null
	}

	static class SearchTemplateItem {
		String name
		String operator
		String value
	}

	static void setSearchCondition(String attributeFqn, SearchTemplateItem searchTemplateItem) {
		// select operator
		if (searchTemplateItem.operator == null) { // boolean
			throw new NotImplementedException("boolean operator is not implemented");
			// TODO implement boolean operator
		} else {
			def searchOp = driver.findElement(By.id('search-operator-' + attributeFqn))
			// default string input
			WebElement input
			try {
				input = driver.findElement(By.id('search-value-string-' + attributeFqn));
				input.clear()
				input.sendKeys('' + searchTemplateItem.value)
			} catch (Exception ex) {
				// continue search...
			}
			try {
				input = driver.findElement(By.id('search-value-lov-' + attributeFqn)).findElement(By.tagName('input'));
				input.sendKeys('' + searchTemplateItem.value)
				waitForAngularRequestsToFinish()
				input.sendKeys(Keys.ARROW_DOWN)
				input.sendKeys(Keys.ARROW_DOWN)
				input.sendKeys(Keys.ENTER)
				waitForAngularRequestsToFinish()
			} catch (Exception ex) {
				// continue search...
			}
			if (input == null) {
				throw new NotImplementedException("datepicker values are not implemented")
			}
			searchOp.findElement(By.id('operator-' + searchTemplateItem.operator)).click()

			// TODO implement dropdown and datepicker
//			List<NuclosWebElement> dropdowns = $$('.searchfilter-attribute-popover .dropdown')
//			if (dropdowns.size() > 0) {
//				// reference
//
//				ListOfValues lov = new ListOfValues()
//				lov.lov = dropdowns[0]
//				if (!lov.open) {
//					lov.open()
//				}
//				lov.selectEntry(searchTemplateItem.value)
//			} else {
//				def datepickerInput = getSearchfilterValueDatepickerInput()
//				if (datepickerInput != null) {
//					// datepicker
//					// TODO sendKeys will not update the model ????
//					datepickerInput.clear()
//					datepickerInput.sendKeys('' + searchTemplateItem.value + Keys.TAB)
//				}
//			}
		}
		waitForAngularRequestsToFinish()
	}
}