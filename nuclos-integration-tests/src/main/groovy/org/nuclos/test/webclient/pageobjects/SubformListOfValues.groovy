package org.nuclos.test.webclient.pageobjects

import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.subform.Subform

import groovy.transform.CompileStatic

@CompileStatic
class SubformListOfValues extends ListOfValues {
	Subform subform

	@Override
	NuclosWebElement getLovContainer() {
		subform.subformElement.$('.lov-container[name$="' + this.attributeName + '"]')
	}
}
