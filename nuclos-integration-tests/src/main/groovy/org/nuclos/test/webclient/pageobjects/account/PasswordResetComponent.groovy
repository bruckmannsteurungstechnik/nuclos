package org.nuclos.test.webclient.pageobjects.account

import static org.nuclos.test.webclient.AbstractWebclientTest.$

import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.AbstractPageObject

import groovy.transform.CompileStatic

@CompileStatic
class PasswordResetComponent extends AbstractPageObject {

	static String getUsername() {
		$('#username').text
	}

	static void setNewPassword(String password) {
		NuclosWebElement element = $('#newPassword')
		element.value = password
	}

	static void setConfirmNewPassword(String password) {
		NuclosWebElement element = $('#confirmNewPassword')
		element.value = password
	}

	static void submit() {
		submitElement.click()
	}

	static String getErrorMessage() {
		errorMessageElement?.text
	}

	static NuclosWebElement getErrorMessageElement() {
		$('div.alert.alert-danger')
	}

	static String getSuccessMessage() {
		successMessageElement?.text
	}

	static NuclosWebElement getSuccessMessageElement() {
		$('div.alert.alert-success')
	}

	static NuclosWebElement getSubmitElement() {
		$('button[type="submit"]')
	}

	static void submitAndAssertError(List<String> expectedMessage = null) {
		submit()

		String errorMessage = errorMessage
		assert errorMessage

		if (expectedMessage) {
			assert errorMessage && expectedMessage.find { errorMessage.contains(it) }
		}
	}
}
