package org.nuclos.test.webclient.pageobjects.businesstest


import static org.nuclos.test.webclient.AbstractWebclientTest.$
import static org.nuclos.test.webclient.AbstractWebclientTest.getUrlHash
import static org.nuclos.test.webclient.AbstractWebclientTest.waitFor

import org.apache.commons.lang.StringEscapeUtils
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.AbstractPageObject

import groovy.transform.CompileStatic

/**
 * Represents the details page of a single business test.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class BusinessTestsDetails extends AbstractPageObject {

	static boolean isStarted() {
		!$('#saveButton').enabled
	}

	static void createNewTest(String name, String script) {
		assert name
		assert script

		getUrlHash('#/businesstests/new')
		$('#testName').sendKeys(name)
		setTestScript(script)
		saveTest()
	}

	static String getTestUID() {
		// Hidden element text can only be accessed via JavaScript
 		executeScript('return $(\'#testUID\').text();').toString()
	}

	/**
	 * Replaces the ACE editor contents with the given script.
	 *
	 * @param script
	 */
	static void setTestScript(String script) {
		String javascript = 'ace.edit("testSource").setValue("' + StringEscapeUtils.escapeJavaScript(script) + '")'
		executeScript(javascript)
	}

	static String getTestScript() {
		String javascript = 'return ace.edit("testSource").getValue()'
		executeScript(javascript).toString()
	}

	static String getTestResult() {
		$('#testResult').text
	}

	static String getTestLog() {
		$('#testLog').getAttribute('value')
	}

	static String getTestState() {
		$('#testState').text
	}

	static int getWarningCount() {
		$('#testWarningCount').text.toInteger()
	}

	static int getErrorCount() {
		$('#testErrorCount').text.toInteger()
	}

	static void saveTest() {
		$('#saveButton').click()
	}

	static void executeTest() {
		$('#executeButton').click()
		waitFor(60) {
			!started
		}
	}

	static void deleteTest() {
		$('#deleteLink').click()
	}

	/**
	 * Checks if the row number (0-based) is visible.
	 *
	 * @param rowNumber
	 * @return
	 */
	static boolean isRowVisible(int rowNumber) {
		final String javascript = "return ace.edit('testSource').isRowVisible($rowNumber)"
		final Boolean result = ((org.openqa.selenium.JavascriptExecutor) AbstractWebclientTest.driver).executeScript(javascript)
		return result
	}

	/**
	 * Returns a List of Markers for all highlighted lines (because of errors / warnings).
	 *
	 * @return
	 */
	static List<Marker> getHighlights() {
		final String javascript = '''
var markers =  ace.edit("testSource").getSession().getMarkers();
var result = [];
for (var i in markers) {
	var marker = markers[i];

	if (!marker.range) {
		continue;
	}

	result.push(marker.range.start.row + ':' + marker.clazz);
}
return result.join(';');
'''

		final String result = ((org.openqa.selenium.JavascriptExecutor) AbstractWebclientTest.getDriver()).executeScript(javascript).toString();
		final List<Marker> markers = result
				.split(';')
				.collect { Marker.parse("$it") }
				.findAll{ it }
				.sort { it.line }
				.unique()
		markers.remove(null)

		return markers;
	}

	static class Marker {
		/**
		 * Line number starting from 1
		 */
		int line;

		TYPE type;

		static Marker parse(String text) {
			String[] parts = text.split(':')
			int line = Integer.parseInt(parts[0]) + 1
			String clazz = parts[1]
			if (clazz == 'ace_error') {
				return new Marker(line: line, type: Marker.TYPE.ERROR)
			} else if (clazz == 'ace_warning') {
				return new Marker(line: line, type: Marker.TYPE.WARNING)
			}
			return null;
		}

		public static enum TYPE {
			WARNING,
			ERROR;
		}
	}
}