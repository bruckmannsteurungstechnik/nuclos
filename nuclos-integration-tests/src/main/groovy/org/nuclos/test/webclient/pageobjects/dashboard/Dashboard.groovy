package org.nuclos.test.webclient.pageobjects.dashboard

import static org.nuclos.test.webclient.AbstractWebclientTest.$
import static org.nuclos.test.webclient.AbstractWebclientTest.$$

import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.AbstractPageObject
import org.nuclos.test.webclient.pageobjects.ListOfValues

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class Dashboard extends AbstractPageObject {

	static void addNew() {
		newButton.click()
	}

	static NuclosWebElement getNewButton() {
		$('#newDashboard')
	}

	static boolean isConfigMode() {
		configComponent as boolean
	}

	static NuclosWebElement getConfigComponent() {
		$('nuc-dashboard-config')
	}

	static String getName() {
		nameInput.value
	}

	static void setName(String name) {
		nameInput.value = name
	}

	static NuclosWebElement getNameInput() {
		$('#dashboardName')
	}

	static List<NuclosWebElement> getTabs() {
		$$('.dashboard-tab-title')
	}

	static List<String> getTabTitles() {
		tabs*.text*.trim()
	}

	static void addTaskListItem(final String taskListName) {
		taskListSelect.selectEntry(taskListName)
	}

	static ListOfValues getTaskListSelect() {
		ListOfValues.fromElement(null, $('#taskLists'))
	}

	static List<DashboardItem> getItems() {
		$$('gridster-item').collect {
			new DashboardItem(
					type: it.getAttribute('item-type'),
					x: it.getAttribute('item-x') as Integer,
					y: it.getAttribute('item-y') as Integer,
					rows: it.getAttribute('item-rows') as Integer,
					cols: it.getAttribute('item-cols') as Integer,
			)
		}
	}

	static void configure() {
		if (!isConfigMode()) {
			$('a[name=toggle-config]').click()
		}
	}
}