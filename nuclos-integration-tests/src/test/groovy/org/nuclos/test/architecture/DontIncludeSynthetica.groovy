package org.nuclos.test.architecture

import com.tngtech.archunit.core.importer.ImportOption
import com.tngtech.archunit.core.importer.Location

import groovy.transform.CompileStatic

/**
 * Prevents thirdparty Synthetica classes from being imported into the architecture test.
 */
@CompileStatic
class DontIncludeSynthetica implements ImportOption {
	@Override
	boolean includes(final Location location) {
		return !location.contains("de/javasoft/plaf/synthetica")
	}
}
