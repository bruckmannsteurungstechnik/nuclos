package org.nuclos.test.architecture

import java.nio.file.Path
import java.nio.file.Paths

import com.tngtech.archunit.core.importer.Location
import com.tngtech.archunit.junit.LocationProvider

import groovy.transform.CompileStatic

/**
 * Provides the location of the root Nuclos directory.
 */
@CompileStatic
class NuclosClassesLocationProvider implements LocationProvider {
	@Override
	Set<Location> get(Class<?> testClass) {
		Path filePath = Paths.get(testClass.getProtectionDomain().getCodeSource().getLocation().getPath())
		Path nuclosPath = findNuclosRoot(filePath)

		return new HashSet<>(Arrays.asList(
				Location.of(nuclosPath)
		))
	}

	private Path findNuclosRoot(Path path) {
		if (path == null) {
			return path
		}

		// Go up until we reach a "nuclos-..." directory (a Nuclos module)
		while (!path.fileName.toString().startsWith('nuclos-') && path.parent != null) {
			path = path.parent
		}

		assert path.fileName.toString().startsWith('nuclos-')

		// Go up once more to reach the root - assuming all nuclos modules are directly below the root
		path = path.parent

		return path
	}
}
