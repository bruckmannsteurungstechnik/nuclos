package org.nuclos.test.server

import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.common.UID
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityClass
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.SystemEntities
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.QueryOptions
import org.nuclos.test.rest.RESTClient

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class NucletIntegrationPointTest extends AbstractNuclosTest {

	static RESTClient client

	static Long customerId

	@Test
	void _00_setup() {
		client = new RESTClient('nuclos', '')
		client.login()
		EntityObject<Long> cutomer = new EntityObject(TestEntities.EXAMPLE_REST_CUSTOMER)
		cutomer.setAttribute('customerNumber', 1)
		cutomer.setAttribute('name', 'Mr. Integrationspunkt')
		client.save(cutomer)
		customerId = cutomer.getId()
	}

	@Test
	void _01_testNucletIntegrationInsertFinalRuleExecution() {
		EntityObject<Long> order = new EntityObject(TestEntities.EXAMPLE_REST_ORDER)
		order.setAttribute('orderNumber', 4711)
		client.save(order) // die erste Lagerbuchung (InsertFinal)
		List<EntityObject<Long>> lagerbuchungen = client.getEntityObjects(TestEntities.NUCLET_TEST_OTHER_LAGERBUCHUNG)
		assert lagerbuchungen.size() == 1
		assert lagerbuchungen.getAt(0).getAttribute("artikel").getAt("name") == '4711 '
	}

	@Test
	void _02_testNucletIntegrationUpdateRuleExecution() {
		EntityObject<Long> order = new EntityObject(TestEntities.EXAMPLE_REST_ORDER)
		order.setAttribute('orderNumber', 4712)
		client.save(order) // eine weitere Lagerbuchung (InsertFinal)
		order.setAttribute('orderNumber', 4713)
		client.save(order); // eine weitere Lagerbuchung (Update)
		List<EntityObject<Long>> lagerbuchungen = client.getEntityObjects(TestEntities.NUCLET_TEST_OTHER_LAGERBUCHUNG)
		assert lagerbuchungen.size() == 3
		// ORDER BY t.INTID DESC ....
		assert lagerbuchungen.getAt(0).getAttribute("artikel").getAt("name") == '4713 '
		assert lagerbuchungen.getAt(1).getAttribute("artikel").getAt("name") == '4713 '
	}

	@Test
	void _03_testNucletIntegrationStateChangeRuleExecution() {
		EntityObject<Long> order = new EntityObject(TestEntities.EXAMPLE_REST_ORDER)
		order.setAttribute('orderNumber', 4714)
		order.setAttribute('customer', [id: customerId])
		client.save(order) // eine weitere Lagerbuchung (InsertFinal)
		client.changeState(order, 80) // // eine weitere Lagerbuchung (StateChange)
		List<EntityObject<Long>> lagerbuchungen = client.getEntityObjects(TestEntities.NUCLET_TEST_OTHER_LAGERBUCHUNG)
		assert lagerbuchungen.size() == 5
		assert lagerbuchungen.getAt(0).getAttribute("artikel").getAt("name") == '4714 Mr. Integrationspunkt'
	}

	@Test
	void _04_testDefaultRuleExecutionBeforeIntegrationPointRule() {
		EntityObject<Long> order = new EntityObject(TestEntities.EXAMPLE_REST_ORDER)
		order.setAttribute('orderNumber', 4715)
		client.save(order);

		// UpdateRule test here only...
		order.setAttribute('note', 'Hello default rule!')
		client.save(order);

		assert order.getAttribute('note') == 'Hello default rule! --- Hi NucletIntegrationPointTest :)'
	}

	@Test
	@Ignore
	void _05_testNucletIntegrationReadOnlyIPValidation() {
		EntityObject<UID> ipLagerArtikelRo = client.getEntityObject(new EntityClass<UID>() {
			@Override
			String getFqn() {
				'kIL5'
			}
		}, UID.parseUID('8CFptzXui0KsGiYuDIr9'))

		ipLagerArtikelRo.setAttribute('readonly', false);
		ipLagerArtikelRo.save()

		//there should be no errors
		assert client.loadDependents(ipLagerArtikelRo, new EntityClass<UID>() {
			@Override
			String getFqn() {
				'0xHU'
			}
		}, '0xHUa').size() == 0

		ipLagerArtikelRo.setAttribute('readonly', true);
		ipLagerArtikelRo.save()

		//there should be no errors
		assert client.loadDependents(ipLagerArtikelRo, new EntityClass<UID>() {
			@Override
			String getFqn() {
				'0xHU'
			}
		}, '0xHUa').size() == 5
	}

	@Test
	@Ignore
	void _06_testNucletIntegrationReadOnlyIPWithReadOnlyBO() {
		EntityObject<Long> order = new EntityObject(TestEntities.EXAMPLE_REST_ORDER)

		order.setAttribute('orderNumber', 4716)
		client.save(order)
		QueryOptions query = new QueryOptions(
				orderBy: (Set) [TestEntities.NUCLET_TEST_OTHER_TESTRULELOG.fqn + '_primaryKey desc']
		)
		EntityObject<Long> log = client.getEntityObjects(TestEntities.NUCLET_TEST_OTHER_TESTRULELOG, query).first()
		assert log.getAttribute('log') == '"insert" recognized'

		order.setAttribute('note','test')
		client.save(order)
		log = client.getEntityObjects(TestEntities.NUCLET_TEST_OTHER_TESTRULELOG, query).first()
		assert log.getAttribute('log') == '"update" recognized'

		client.delete(order)
		log = client.getEntityObjects(TestEntities.NUCLET_TEST_OTHER_TESTRULELOG, query).first()
		assert log.getAttribute('log') == '"delete" recognized'
	}

	@Test
	void _07_testIntegrationPointWithSystemEntity() {
		EntityObject<Long> order = new EntityObject<>(TestEntities.EXAMPLE_REST_ORDER)
		order.setAttribute('orderNumber', 4717)
		order.setAttribute('agent', [id: 'nuclos1000'])
		Long orderId = client.save(order)

		EntityObject<Long> buchung = new EntityObject<>(TestEntities.NUCLET_TEST_OTHER_LAGERBUCHUNG)
		buchung.setAttribute('datum', new Date())
		buchung.setAttribute('menge', 1)
		buchung.setAttribute('artikel', [id: orderId])

		client.executeCustomRule(buchung, 'nuclet.test.other.IntegrationPointWithSystemEntityTest')
	}
}
