package org.nuclos.test.server


import org.apache.http.client.methods.HttpPost
import org.apache.http.client.methods.HttpPut
import org.apache.http.entity.StringEntity
import org.json.JSONArray
import org.json.JSONObject
import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.QueryOptions
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper

import groovy.transform.CompileStatic

/**
 * @author Oliver Brausch <oliver.brausch@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ResultListTest extends AbstractNuclosTest {
	Map wordStart = [text: 'Word Start', times: 150, agent: 'test']
	static RESTClient client

	@Test
	void _01_setup() {
		nuclosSession.managementConsole('enableIndexerSynchronously')
	}

	@Test
	void _02_createTestUserAndLogin() {
		RESTHelper.createUser('test', 'test', ['Example user'], nuclosSession)
		client = new RESTClient('test', 'test')
		client.login()
	}

	@Test
	void _03_createNewEntries() {
		EntityObject<Long> eo = new EntityObject(TestEntities.EXAMPLE_REST_WORD)
		eo.setAttributes(new HashMap<String, Object>(wordStart))

		assert !eo.id
		client.save(eo)

		assert eo.id
		assert eo.getAttribute('date') == '1970-01-02';

		Object cobj = eo.getAttribute('countletters')
		assert cobj instanceof Integer
		assert ((Integer)cobj) > 0

		client.executeCustomRule(eo, 'example.rest.RegisterWords')

		List<EntityObject<Long>> eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD)
		assert eos.size() >= 40
	}

	@Test
	void _04_textSearch() {
		QueryOptions opt = new QueryOptions(
				search: 'Medizin'
		)

		List<EntityObject<Long>> eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 1

		EntityObject<Long> eo = eos.get(0)

		Object cobj = eo.getAttribute('countletters')
		assert cobj instanceof Integer
		assert ((Integer)cobj) > 0

		opt = new QueryOptions(
				search: 'her'
		)

		List<EntityObject<Long>> eos2 = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos2.size() == 7


		QueryOptions opt150 = new QueryOptions(
				search: '150'
		)

		List<EntityObject<Long>> eos3 = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt150)
		assert eos3.size() == 1

		eo.setAttribute('parent', [id: eos3.get(0).id])
		client.save(eo)
	}

	// NUCLOS-6375
	@Test
	void _05_textReferencesDisplayingNumber() {
		// The search for "150" yields two results: One for a direct number, other for reference to a number
		QueryOptions opt150 = new QueryOptions(
				search: '150'
		)

		List<EntityObject<Long>> eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt150)
		assert eos.size() == 2


		// The search for "6" should not yield any result, as the referencing value is a number
		QueryOptions opt6 = new QueryOptions(
				search: '50'
		)

		eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt6)
		assert eos.size() == 0

	}

	@Test
	void _06_MultiWordsInOneColumnsTest() {
		// NUCLOS-6256 Word and Star are in one column

		QueryOptions opt = new QueryOptions(
				search: 'Wor Star'
		)

		List<EntityObject<Long>> eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 1

		opt = new QueryOptions(
				search: 'Wor*Star'
		)

		eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 1

		opt = new QueryOptions(
				search: 'Star*Wor'
		)

		eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 0

		opt = new QueryOptions(
				search: '"Wor Star"'
		)

		eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 0


		opt = new QueryOptions(
				search: '"Word Star"'
		)

		eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 1
	}

	@Test
	void _07_MultiWordsInSeparateColumnsTest() {
		// NUCLOS-6256  and test are in different columns

		QueryOptions opt = new QueryOptions(
				search: 'Med test'
		)

		List<EntityObject<Long>> eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 1

		opt = new QueryOptions(
				search: 'test Medizin'
		)

		eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 1

		opt = new QueryOptions(
				search: 'Medizin*test'
		)

		eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 0

		opt = new QueryOptions(
				search: '"Medizin test"'
		)

		eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 0

		opt = new QueryOptions(
				search: 'Wor xyz'
		)

		eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 0

		opt = new QueryOptions(
				search: 'Wor test'
		)

		eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 2

		opt = new QueryOptions(
				search: 'Wor Sta test'
		)

		eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 1

		opt = new QueryOptions(
				search: 'Wor*Sta test'
		)

		eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 1

		opt = new QueryOptions(
				search: 'Wor Sta*test'
		)

		eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 0

	}

	@Ignore
	@Test
	void _09_luceneTest() {
		client.expectSearchResults("doktor", 2, 300)
	}

	@Test
	void _20_dynamicTaskListWithSQLKeywordInAlias() {
		List<EntityObject<Long>> bos = client.getEntityObjects(
				TestEntities.NUCLET_TEST_OTHER_RESERVEDSQLKEYWORDINALIASDTL
		)

		assert !bos.empty
	}


	final static String BASE_PATH = '/execute/example.rest.BulkWritingRule/'

	@Test
	void _50_testInsertBulkMethod() {
		JSONArray jsonArray = new JSONArray();
		for (int i = 0; i < 5; i++) {
			jsonArray.put('TestWord' + i)
		}
		JSONArray jsonArray2 = new JSONArray();
		for (int i = 5; i < 10; i++) {
			jsonArray2.put('TestWord' + i)
		}

		JSONObject postData = new JSONObject().put('words', jsonArray)
		JSONObject postData2 = new JSONObject().put('words', jsonArray2)

		HttpPost post = new HttpPost(RESTHelper.REST_BASE_URL + BASE_PATH + 'insertBulk')
		post.setEntity(new StringEntity(postData.toString()))

		HttpPost post2 = new HttpPost(RESTHelper.REST_BASE_URL + BASE_PATH + 'insertBulk')
		post2.setEntity(new StringEntity(postData2.toString()))

		int maxSqlWriteCount = 5
		int maxSqlCount = 11
		// 1x Security Cache
		// 5x IDFACTORY
		// 5x INSERT

		String s = RESTHelper.callCustomRestRuleWithSqlCountCheck(nuclosSession, post, maxSqlCount, maxSqlWriteCount)
		maxSqlCount-- // - 1x Security Cache
		s = s + "+" + RESTHelper.callCustomRestRuleWithSqlCountCheck(nuclosSession, post2, maxSqlCount, maxSqlWriteCount)

		assert s == '5+5'

	}

	@Test
	void _65_testInsertSuccessFul() {
		testCountRecords('Testword', 10)
	}

	@Test
	void _60_testUpdateBulkMethod() {
		JSONObject postData = new JSONObject().put('words', 'TestWord')

		HttpPut put = new HttpPut(RESTHelper.REST_BASE_URL + BASE_PATH + 'updateBulk')
		put.setEntity(new StringEntity(postData.toString()))

		int maxSqlWriteCount = 10
		int maxSqlCount = 21
		// 1x SELECT .... t.STRtext ILIKE '%TestWord%'
		// 10x SELECT
		// 10x UPDATE

		String s = RESTHelper.callCustomRestRuleWithSqlCountCheck(nuclosSession, put, maxSqlCount, maxSqlWriteCount)

		assert s == '10'

	}

	@Test
	void _65_testUpdateSuccessFul() {
		testCountRecords('Testword7C', 1)
	}

	@Test
	void _70_testDeleteBulk() {
		JSONObject postData = new JSONObject().put('words', 'TestWord')

		HttpPut delete = new HttpPut(RESTHelper.REST_BASE_URL + BASE_PATH + 'deleteBulk')
		delete.setEntity(new StringEntity(postData.toString()))

		int maxSqlWriteCount = 10
		int maxSqlCount = 21
		// 1x SELECT .... t.STRtext ILIKE '%TestWord%'
		// 10x SELECT
		// 10x DELETE

		String s = RESTHelper.callCustomRestRuleWithSqlCountCheck(nuclosSession, delete, maxSqlCount, maxSqlWriteCount)

		assert s == '10'

	}

	@Test
	void _75_testDeleteSuccessFul() {
		testCountRecords('Testword', 0)
	}


	@Test
	void _80_orderByCalculatedAttribute() {
		nuclosSession.setSystemParameters(
				['SORT_CALCULATED_ATTRIBUTES': 'true']
		)

		3.times {
			EntityObject eo = new EntityObject<>(TestEntities.NUCLET_TEST_OTHER_CALCULATEDATTRIBUTE)
			eo.setAttribute('text', "Test $it")
			nuclosSession.save(eo)
		}

		List<EntityObject<Long>> eos = nuclosSession.getEntityObjects(TestEntities.NUCLET_TEST_OTHER_CALCULATEDATTRIBUTE)
		assert eos.size() == 3

		List<EntityObject<Long>> sortedEos = nuclosSession.getEntityObjects(
				TestEntities.NUCLET_TEST_OTHER_CALCULATEDATTRIBUTE,
				new QueryOptions(
						orderBy: [
								TestEntities.NUCLET_TEST_OTHER_CALCULATEDATTRIBUTE.fqn + '_calculated asc'
						].toSet()
				)
		)
		assert sortedEos*.getId() == eos*.getId().toSorted()

		sortedEos = nuclosSession.getEntityObjects(
				TestEntities.NUCLET_TEST_OTHER_CALCULATEDATTRIBUTE,
				new QueryOptions(
						orderBy: [
								TestEntities.NUCLET_TEST_OTHER_CALCULATEDATTRIBUTE.fqn + '_calculated desc'
						].toSet()
				)
		)
		assert sortedEos*.getId() == eos*.getId().toSorted().reverse()
	}

	@Test
	void _99_disableIndexer() {
		nuclosSession.managementConsole('disableIndexer')
	}

	static void testCountRecords(String search, int count) {
		QueryOptions opt = new QueryOptions(
				search: search
		)

		List<EntityObject<Long>> eos = nuclosSession.getEntityObjects(TestEntities.NUCLET_TEST_UTILS_BULKTEST, opt)
		assert eos.size() == count
	}


}
