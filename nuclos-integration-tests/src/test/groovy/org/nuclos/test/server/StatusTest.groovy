package org.nuclos.test.server

import org.json.JSONObject
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper

import groovy.transform.CompileStatic

/**
 * @author Oliver Brausch <oliver.brausch@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class StatusTest extends AbstractNuclosTest {
	static RESTClient client
	static long id

	@Test
	void _02_createTestUserAndLogin() {
		RESTHelper.createUser('test', 'test', ['Example user'], nuclosSession)
		client = new RESTClient('test', 'test')
		client.login()
	}

	@Test
	void _03_createNewEntry() {
		EntityObject<Long> eo = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TESTSTATECHANGE)
		eo.setAttribute('name', 'test')

		assert !eo.id
		client.save(eo)

		assert eo.id
		id = eo.id

	}

	@Test
	void _05_changeStatesSmallRound() {

		EntityObject<Long> eo  = client.getEntityObject(TestEntities.NUCLET_TEST_OTHER_TESTSTATECHANGE, id);

		assert eo.getAttribute('name') == 'test'

		assert eo.nextStates.containsKey("Status B")
		assert eo.nextStates.containsKey("Status ☠")

		eo.changeState(0)

		eo = client.getEntityObject(TestEntities.NUCLET_TEST_OTHER_TESTSTATECHANGE, id);

		assert eo.nextStates.containsKey("Status A")
		assert !eo.nextStates.containsKey("Status B")
		assert !eo.nextStates.containsKey("Auf Initialstatus setzen")

		String nuclosStateId = ((JSONObject)eo.nextStates.get("Status A")).get("nuclosStateId")
		String description = client.getDescriptionToStatus(nuclosStateId)

		// FIXME NUCLOS-7882 This should be 'Hinweis auf dem Übergang.'
		assert description == 'Initialer Status.'

		eo.changeState(10)

	}

	@Test
	void _10_changeStatesBigRound() {

		EntityObject<Long> eo  = client.getEntityObject(TestEntities.NUCLET_TEST_OTHER_TESTSTATECHANGE, id);

		assert eo.getAttribute('name') == 'test'

		assert eo.nextStates.containsKey("Status B")

		eo.changeState(20)

		eo = client.getEntityObject(TestEntities.NUCLET_TEST_OTHER_TESTSTATECHANGE, id);

		assert !eo.nextStates.containsKey("Status B")
		assert eo.nextStates.containsKey("Status C")

		eo.changeState(30)

		eo = client.getEntityObject(TestEntities.NUCLET_TEST_OTHER_TESTSTATECHANGE, id);

		// It's in Status 40 now (automatic transition)
		assert !eo.nextStates.containsKey("Status A")
		assert !eo.nextStates.containsKey("Status D")
		assert eo.nextStates.containsKey("Status E")
		assert eo.nextStates.containsKey("Zurücksetzen")

		eo.changeState(10)

	}

}
