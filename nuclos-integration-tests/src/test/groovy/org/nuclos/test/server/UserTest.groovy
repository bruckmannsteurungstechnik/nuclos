package org.nuclos.test.server

import javax.ws.rs.core.Response

import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.common.UID
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.UIDEntities
import org.nuclos.test.rest.QueryOptions
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class UserTest extends AbstractNuclosTest {
	static RESTClient noRightsclient
	static RESTClient userClient
	static RESTClient superuserClient = new RESTClient('nuclos', '')

	static EntityObject<UID> testUser
	static EntityObject<UID> testSuperUser

	@Test
	void _01_setup() {
		nuclosSession.managementConsole('enableIndexerSynchronously')
	}

	@Test
	void _02_createTestUserAndLogin() {
		RESTHelper.createUser('test', 'test', ['Example user'], nuclosSession)
		noRightsclient = new RESTClient('test', 'test')
		noRightsclient.login()

		RESTHelper.createUser('testct', 'testct', ['Example controlling',  'Example readonly'], nuclosSession)
		userClient = new RESTClient('testct', 'testct')
		userClient.login()


		superuserClient.login()
	}

	@Test
	void _03_createNewUsers() {
		testUser = new EntityObject(UIDEntities.NUCLOS_USER)
		testUser.setAttribute("username", "testUser")
		testUser.setAttribute('firstname', 'First Name')
		testUser.setAttribute('lastname', 'Last Name')

		assert !testUser.id
		expectErrorStatus(Response.Status.FORBIDDEN) {
			noRightsclient.save(testUser)
		}
		assert !testUser.id
		userClient.save(testUser)
		
		superuserClient.save(testUser)

		assert testUser.id

		testSuperUser = new EntityObject(UIDEntities.NUCLOS_USER)
		testSuperUser.setAttribute("username", "testuser2")
		testSuperUser.setAttribute('firstname', 'First Name')
		testSuperUser.setAttribute('lastname', 'Last Name')
		testSuperUser.setAttribute('superuser', true)

		assert !testSuperUser.id
		expectErrorStatus(Response.Status.FORBIDDEN) {
			noRightsclient.save(testSuperUser)
		}

		assert !testSuperUser.id
		expectErrorStatus(Response.Status.FORBIDDEN) {
			userClient.save(testSuperUser)
		}

		assert !testSuperUser.id
		superuserClient.save(testSuperUser)

		assert testSuperUser.id
	}

	@Test
	void _04_modifySuperUserFlag() {

		//not Superuser -> Superuser
		assert testUser.getAttribute('superuser') == false

		testUser.setAttribute('superuser', true)
		expectErrorStatus(Response.Status.FORBIDDEN) {
			noRightsclient.save(testUser)
		}

		expectErrorStatus(Response.Status.FORBIDDEN) {
			testUser = noRightsclient.getEntityObject(UIDEntities.NUCLOS_USER, testUser.getId())
		}

		testUser.setAttribute('superuser', true)
		expectErrorStatus(Response.Status.FORBIDDEN) {
			userClient.save(testUser)
		}
		testUser = userClient.getEntityObject(UIDEntities.NUCLOS_USER, testUser.getId())
		assert testUser.getAttribute('superuser') == false

		testUser.setAttribute('superuser', true)
		superuserClient.save(testUser)
		testUser = superuserClient.getEntityObject(UIDEntities.NUCLOS_USER, testUser.getId())
		assert testUser.getAttribute('superuser') == true

		//Superuser -> non Superuser
		testUser.setAttribute('superuser', false)
		expectErrorStatus(Response.Status.FORBIDDEN) {
			noRightsclient.save(testUser)
		}
		expectErrorStatus(Response.Status.FORBIDDEN) {
			testUser = noRightsclient.getEntityObject(UIDEntities.NUCLOS_USER, testUser.getId())
		}

		testUser.setAttribute('superuser', false)
		expectErrorStatus(Response.Status.FORBIDDEN) {
			userClient.save(testUser)
		}
		testUser = userClient.getEntityObject(UIDEntities.NUCLOS_USER, testUser.getId())
		assert testUser.getAttribute('superuser') == true

		testUser.setAttribute('superuser', false)
		superuserClient.save(testUser)
		testUser = superuserClient.getEntityObject(UIDEntities.NUCLOS_USER, testUser.getId())
		assert testUser.getAttribute('superuser') == false
	}

	@Test
	void _05_modify_SuperUserData() {

		//no Superuser
		assert testUser.getAttribute('firstname') == "First Name"

		testUser.setAttribute("firstname", 'Hans-Peter')

		expectErrorStatus(Response.Status.FORBIDDEN) {
			noRightsclient.save(testUser)
		}

		testUser.setAttribute("firstname", 'Hans-Peter')
		userClient.save(testUser)
		testUser = userClient.getEntityObject(UIDEntities.NUCLOS_USER, testUser.getId())
		assert testUser.getAttribute('firstname') == "Hans-Peter"

		testUser.setAttribute('firstname', 'First Name')
		superuserClient.save(testUser)
		testUser = superuserClient.getEntityObject(UIDEntities.NUCLOS_USER, testUser.getId())
		assert testUser.getAttribute('firstname') == "First Name"

		//Superuser

		assert testSuperUser.getAttribute('firstname') == "First Name"

		testSuperUser.setAttribute('firstname', "Claus-Otto")
		expectErrorStatus(Response.Status.FORBIDDEN) {
			noRightsclient.save(testSuperUser)
		}

		testSuperUser.setAttribute('firstname', "Claus-Otto")
		expectErrorStatus(Response.Status.FORBIDDEN) {
			userClient.save(testSuperUser)
		}
		testSuperUser = userClient.getEntityObject(UIDEntities.NUCLOS_USER, testSuperUser.getId())
		assert testSuperUser.getAttribute('firstname') == "First Name"

		testSuperUser.setAttribute('firstname', 'Claus-Otto')
		superuserClient.save(testSuperUser)
		testSuperUser = superuserClient.getEntityObject(UIDEntities.NUCLOS_USER, testSuperUser.getId())
		assert testSuperUser.getAttribute('firstname') == "Claus-Otto"
	}

	@Test
	void _06_changePassword() {

		testUser.setAttribute('oldPassword', '')
		testUser.setAttribute('newPassword', 'test')

		expectErrorStatus(Response.Status.FORBIDDEN) {
			noRightsclient.save(testUser)
		}

		expectErrorStatus(Response.Status.FORBIDDEN) {
			noRightsclient.changePassword(testUser)
		}

		userClient.changePassword(testUser)

		testSuperUser.setAttribute('oldPassword', '')
		testSuperUser.setAttribute('newPassword', 'rnd()')

		expectErrorStatus(Response.Status.FORBIDDEN) {
			noRightsclient.save(testSuperUser)
		}

		expectErrorStatus(Response.Status.FORBIDDEN) {
			noRightsclient.changePassword(testSuperUser)
		}

		expectErrorStatus(Response.Status.FORBIDDEN) {
			userClient.save(testSuperUser)
		}

		expectErrorStatus(Response.Status.FORBIDDEN) {
			userClient.changePassword(testSuperUser)
		}

		superuserClient.changePassword(testSuperUser)

	}

	@Test
	void _07_changePasswordIPBlock() {
		testUser.setAttribute('oldPassword', '1234')
		testUser.setAttribute('newPassword', 'test1')

		// Allow 1 attempt per second
		nuclosSession.patchSystemParameters([
				'SECURITY_IP_BLOCK_ATTEMPTS'         : '1',
				'SECURITY_IP_BLOCK_PERIOD_IN_SECONDS': '1',
				'SECURITY_LOCK_ATTEMPTS' : '5'
		])

		3.times {
			expectErrorStatus(Response.Status.FORBIDDEN) {
				userClient.changePassword(testUser)
			}
			sleep(1000)
		}

		expectErrorStatus(Response.Status.FORBIDDEN) {
			userClient.changePassword(testUser)
		}

		expectErrorStatus(Response.Status.TOO_MANY_REQUESTS) {
			userClient.changePassword(testUser)
		}


		expectErrorStatus(Response.Status.TOO_MANY_REQUESTS) {
			superuserClient.changePassword(testUser) //IP blocked
		}

		sleep(1001)

		expectErrorStatus(Response.Status.FORBIDDEN) { //not blocked and not locked
			userClient.changePassword(testUser)
		}

		sleep(1001)

		expectRestException({
			userClient.changePassword(testUser)
		}) {
			assert it.statusCode == 423 //we're locked from now on
		}

		sleep(1001)

		testUser.setAttribute('oldPassword', 'test') //right value

		expectErrorStatus(Response.Status.FORBIDDEN) { //locked
			userClient.changePassword(testUser)
		}

		expectRestException({
			userClient.login()
		}) {
			assert it.statusCode == 423 //locked
		}

		expectErrorStatus(Response.Status.TOO_MANY_REQUESTS) {
			userClient.changePassword(testUser)
		}

		EntityObject<UID> testct = superuserClient.getEntityObjects(UIDEntities.NUCLOS_USER, new QueryOptions(where: UIDEntities.NUCLOS_USER.fqn + "_username = 'testct'")).get(0)
		testct.setAttribute('locked', false)
		superuserClient.save(testct)

		sleep(1001)
		userClient.login()

		userClient.changePassword(testUser)

		nuclosSession.patchSystemParameters([
				'SECURITY_IP_BLOCK_ATTEMPTS'         : '0',
				'SECURITY_IP_BLOCK_PERIOD_IN_SECONDS': '0',
				'SECURITY_LOCK_ATTEMPTS' : '0'
		])
	}

	@Test
	@Ignore //NUCLOS-8223
	void _08_selfRemoveSuperuserRole() {
		RESTClient testSuperuserClient = new RESTClient('testuser2', '')
		testSuperuserClient.login()

		assert testSuperUser.getAttribute('superuser') == true
		testSuperUser.setAttribute('superuser', false)
		testSuperuserClient.save(testSuperUser)
		testSuperUser = superuserClient.getEntityObject(UIDEntities.NUCLOS_USER, testSuperUser.getId())
		assert testSuperUser.getAttribute('superuser') == false

		testSuperUser.setAttribute('superuser', true)
		expectErrorStatus(Response.Status.FORBIDDEN) {
			testSuperuserClient.save(testSuperUser)
		}

		superuserClient.save(testSuperUser)
		testSuperUser = superuserClient.getEntityObject(UIDEntities.NUCLOS_USER, testSuperUser.getId())
		assert testSuperUser.getAttribute('superuser') == true
	}

	@Test
	void _10_deleteUsers() {

		testUser = userClient.getEntityObject(UIDEntities.NUCLOS_USER, testUser.getId())
		testSuperUser = userClient.getEntityObject(UIDEntities.NUCLOS_USER, testSuperUser.getId())

		expectErrorStatus(Response.Status.FORBIDDEN) {
			noRightsclient.delete(testUser)
		}

		userClient.delete(testUser)

		//NUCLOS-8235
		/*expectErrorStatus(Response.Status.INTERNAL_SERVER_ERROR) {
			superuserClient.getEntityObject(UIDEntities.NUCLOS_USER, testUser.getId())
		}*/

		expectErrorStatus(Response.Status.FORBIDDEN) {
			noRightsclient.delete(testSuperUser)
		}

		expectErrorStatus(Response.Status.FORBIDDEN) {
			userClient.delete(testSuperUser)
		}

		superuserClient.delete(testSuperUser)

		//NUCLOS-8235
		/*expectErrorStatus(Response.Status.INTERNAL_SERVER_ERROR) {
			superuserClient.getEntityObject(UIDEntities.NUCLOS_USER, testSuperUser.getId())
		}*/
	}

}
