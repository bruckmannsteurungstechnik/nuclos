package org.nuclos.test.server.businesstest

import org.json.JSONArray
import org.junit.After
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities

import groovy.transform.CompileStatic

/**
 * TODO: Explicitly run specific tests here instead of just all available tests
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class BusinessTestsParameterTest extends AbstractNuclosTest {

	@After
	void cleanup() {
		nuclosSession.deleteAll(
				nuclosSession.getEntityObjects(TestEntities.EXAMPLE_REST_ORDER)
		)
	}

	@Test
	void _05_defaultSettings() {
		// Defaults - Rollback between tests and at the end
		nuclosSession.setSystemParameters([:])

		// Second test should fail because it depends on data from first test (which will be rolled back)
		JSONArray output = nuclosSession.businesstestService.runAllTests()
		assert output.size() > 0
		assert nuclosSession.businesstestService.statusSimple == 'YELLOW'

		// Test data should have been rolled back
		assert nuclosSession.getEntityObjects(TestEntities.EXAMPLE_REST_ORDER).empty
	}

	@Test
	void _10_noRollback() {
		// No rollbacks at all
		nuclosSession.setSystemParameters([
				'BUSINESS_TEST_ROLLBACK': 'false'
		])

		// Should success for both tests
		JSONArray output = nuclosSession.businesstestService.runAllTests()
		assert output.size() > 0
		assert nuclosSession.businesstestService.statusSimple == 'GREEN'

		// Test data should still be there
		assert !nuclosSession.getEntityObjects(TestEntities.EXAMPLE_REST_ORDER).empty
	}

	@Test
	void _15_noRollbackBetweenTests() {
		// No rollbacks between tests
		nuclosSession.setSystemParameters([
				'BUSINESS_TEST_ROLLBACK_BETWEEN_TESTS': 'false',
		])

		JSONArray output = nuclosSession.businesstestService.runAllTests()
		assert output.size() > 0
		assert nuclosSession.businesstestService.statusSimple == 'GREEN'

		// Test data should have been rolled back
		assert nuclosSession.getEntityObjects(TestEntities.EXAMPLE_REST_ORDER).empty
	}

	@Test
	void _20_rollbackBetweenTestsButNoRollback() {
		nuclosSession.setSystemParameters([
				'BUSINESS_TEST_ROLLBACK': 'false',

				// Should have no effect, if BUSINESS_TEST_ROLLBACK is false
				'BUSINESS_TEST_ROLLBACK_BETWEEN_TESTS': 'true',
		])

		JSONArray output = nuclosSession.businesstestService.runAllTests()
		assert output.size() > 0
		assert nuclosSession.businesstestService.statusSimple == 'GREEN'

		// Test data should still be there
		assert !nuclosSession.getEntityObjects(TestEntities.EXAMPLE_REST_ORDER).empty
	}
}
