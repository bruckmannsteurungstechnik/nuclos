package org.nuclos.test.webclient

import org.junit.AfterClass
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.webclient.pageobjects.AuthenticationComponent
import org.nuclos.test.webclient.pageobjects.MaintenanceModeAdminComponent

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class MaintenanceModeTest extends AbstractWebclientTest {

	@AfterClass
	static void cleanup() {
		nuclosSession.stopMaintenance()
	}

	@Test
	void _00_setup() {
		logout()
		login('nuclos')
	}

	@Test()
	void _01_activateMaintananceMode() {
		MaintenanceModeAdminComponent.open()
		assert MaintenanceModeAdminComponent.status() == "off"
		MaintenanceModeAdminComponent.activateMaintenaceMode()
		assert MaintenanceModeAdminComponent.status() == "on"
	}

	@Test()
	void _02_loginWithOtherUserExpectNoSuccess() {
		logout()
		loginUnsafe(new LoginParams(username: 'test', password: 'test'))
		assert AuthenticationComponent.getErrorMessage().contains("Wartungsmodus") ||
				AuthenticationComponent.getErrorMessage().contains("maintenance")
	}


	@Test()
	void _03_deactivateMaintananceMode() {
		login('nuclos', '')
		MaintenanceModeAdminComponent.open()
		MaintenanceModeAdminComponent.deactivateMaintenaceMode()
	}

	@Test()
	void _04_loginWithOtherUserExpectSuccess() {
		logout()
		login('test', 'test')
	}

}
