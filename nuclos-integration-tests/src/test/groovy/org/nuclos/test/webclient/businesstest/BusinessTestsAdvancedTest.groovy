package org.nuclos.test.webclient.businesstest

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.businesstest.BusinessTestsAdvanced
import org.openqa.selenium.By

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class BusinessTestsAdvancedTest extends AbstractWebclientTest {
	@Test
	void _00_setup() {
		logout()
		login('nuclos')
	}

	@Test
	void _01_deleteTests() {
		BusinessTestsAdvanced.open()
		BusinessTestsAdvanced.deleteAllTests()
	}

	@Test
	void _02_generateAllTests() {
		BusinessTestsAdvanced.generateAllTests()

		String log = BusinessTestsAdvanced.log
		assert log.startsWith('Generating all business tests...')
		assert !log.contains('[ERROR]')
		assert log.endsWith('Generating all business tests: Done')

		assert !BusinessTestsAdvanced.tests.empty

		List<String> rows = BusinessTestsAdvanced.tests*.text
		assert rows == rows.sort()
	}

	@Test
	void _03_executeAllTests() {
		BusinessTestsAdvanced.executeAllTests()

		String log = BusinessTestsAdvanced.log
		assert log.startsWith('Running all business tests...')
		assert !log.contains('[ERROR]')
		assert log.endsWith('Running all business tests: Done')

		BusinessTestsAdvanced.tests.each {
			// Assert there are no empty table cells now
			def emptyCell = $$(it, 'td').find {
				!it.text && it.findElements(By.xpath('child::*')).empty
			}
			assert !emptyCell, "Found empty table cell $emptyCell in the business tests overview row $it"

			final String testName = $(it, 'td').text
			assert log.contains(testName)
		}

		assert BusinessTestsAdvanced.testsTotal > 0
		assert BusinessTestsAdvanced.testsTotal == BusinessTestsAdvanced.tests.size()
		assert ['OK', 'WARNING', 'ERROR'].contains(BusinessTestsAdvanced.state)

		assert BusinessTestsAdvanced.testsGreen + BusinessTestsAdvanced.testsYellow + BusinessTestsAdvanced.testsRed == BusinessTestsAdvanced.testsTotal
		assert BusinessTestsAdvanced.duration > 0
	}

	@Test
	void _04_deleteSingleTests() {
		def tests = BusinessTestsAdvanced.tests
		int countBefore = tests.size()

		BusinessTestsAdvanced.deleteTest(tests[0].getAttribute('id'))
		BusinessTestsAdvanced.deleteTest(tests[-1].getAttribute('id'))

		tests = BusinessTestsAdvanced.tests

		assert tests.size() == countBefore - 2
	}

	@Test
	void _05_deleteAllTests() {
		BusinessTestsAdvanced.deleteAllTests()

		assert BusinessTestsAdvanced.tests.empty
	}
}
