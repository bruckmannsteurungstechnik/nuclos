package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.subform.Subform

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class AutonumberTest extends AbstractWebclientTest {

	@Test
	void _05_autonumberInSubform() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTAUTONUMBER)
		eo.addNew()
		eo.setAttribute('name', 'Autonumber test')

		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTAUTONUMBERSUBFORM.fqn + '_parent')

		createEntries:
		{
			5.times {
				Subform.Row row = subform.newRow()
				assert row.getValue('autonumber', Integer.class) == it + 1
			}

			assert subform.getColumnValues('autonumber', Integer.class) == [5, 4, 3, 2, 1]
		}

		deleteNewEntry:
		{
			subform.getRow(2).setSelected(true)
			subform.deleteSelectedRows()

			assert subform.getColumnValues('autonumber', Integer.class) == [4, 3, 2, 1]
		}

		save:
		{
			eo.save()
			assert subform.getColumnValues('autonumber', Integer.class) == [4, 3, 2, 1]
		}

		deleteSavedEntry:
		{
			subform.getRow(0).setSelected(true)
			subform.getRow(2).setSelected(true)
			subform.deleteSelectedRows()

			assert subform.getColumnValues('autonumber', Integer.class) == [2, 1, null, null]

			eo.save()
			assert subform.getColumnValues('autonumber', Integer.class) == [2, 1]
		}

		addAndClone:
		{
			subform.newRow()

			assert subform.getColumnValues('autonumber', Integer.class) == [3, 2, 1]
			assert subform.getRow(0).dirty
			assert !subform.getRow(1).dirty
			assert !subform.getRow(2).dirty

			subform.getRow(1).setSelected(true)
			subform.getRow(2).setSelected(true)

			subform.cloneSelectedRows()

			assert subform.getColumnValues('autonumber', Integer.class) == [5, 4, 3, 2, 1]
			assert subform.getRow(0).dirty
			assert subform.getRow(1).dirty
			assert subform.getRow(2).dirty
			assert !subform.getRow(3).dirty
			assert !subform.getRow(4).dirty

			eo.save()
		}

		editManually:
		{
			subform.getRow(2).enterValue('autonumber', '8')

			assert subform.getColumnValues('autonumber', Integer.class) == [5, 4, 3, 2, 1]
			assert subform.getRow(0).dirty
			assert subform.getRow(1).dirty
			assert subform.getRow(2).dirty
			assert !subform.getRow(3).dirty
			assert !subform.getRow(4).dirty
		}

		eo.cancel()
	}

	@Test
	void _10_moveViaArrows() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail().addNew()


		eo.setAttribute('name', 'Test 2')

		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTAUTONUMBERSUBFORM, 'parent')
		assert subform


		createEntries:
		{
			5.times {
				Subform.Row row = subform.newRow()
				assert row.getValue('autonumber', Integer.class) == it + 1
				row.enterValue('text', "${97 + it as char}")
			}

			assert subform.getColumnValues('autonumber', Integer.class) == [5, 4, 3, 2, 1]
			assert subform.getColumnValues('text', String.class) == ['e', 'd', 'c', 'b', 'a']
		}

		eo.save()

		assert !subform.moveUpEnabled
		assert !subform.moveDownEnabled

		subform.toggleSelection(0)

		assert !subform.moveUpEnabled
		assert subform.moveDownEnabled

		assert !eo.dirty
		subform.moveSelectedRowsDown()
		assert eo.dirty

		// Moving down the first row should update only first and second row
		assert subform.getRow(0).dirty
		assert subform.getRow(1).dirty
		assert !subform.getRow(2).dirty

		assert subform.getColumnValues('text', String.class) == ['d', 'e', 'c', 'b', 'a']

		assert subform.moveUpEnabled
		assert subform.moveDownEnabled

		4.times {
			subform.moveSelectedRowsDown()
		}

		assert subform.getColumnValues('autonumber', Integer.class) == [5, 4, 3, 2, 1]
		assert subform.getColumnValues('text', String.class) == ['d', 'c', 'b', 'a', 'e']

		assert subform.moveUpEnabled
		assert !subform.moveDownEnabled

		subform.toggleSelection(0)

		// First and last row toggled now -> can't move
		assert !subform.moveUpEnabled
		assert !subform.moveDownEnabled

		subform.toggleSelection(4)

		assert !subform.moveUpEnabled
		assert subform.moveDownEnabled

		subform.toggleSelection(0)

		// No rows toggled now -> can't move
		assert !subform.moveUpEnabled
		assert !subform.moveDownEnabled

		eo.save()
		assert subform.getColumnValues('autonumber', Integer.class) == [5, 4, 3, 2, 1]
		assert subform.getColumnValues('text', String.class) == ['d', 'c', 'b', 'a', 'e']

		// Move multiple non-consecutive rows at once
		subform.toggleSelection(1)
		subform.toggleSelection(3)

		assert subform.moveUpEnabled
		assert subform.moveDownEnabled

		subform.moveSelectedRowsDown()
		assert subform.moveUpEnabled
		assert !subform.moveDownEnabled
		assert subform.getColumnValues('text', String.class) == ['d', 'b', 'c', 'e', 'a']

		subform.moveSelectedRowsUp()
		assert subform.moveUpEnabled
		assert subform.moveDownEnabled
		assert subform.getColumnValues('text', String.class) == ['d', 'c', 'b', 'a', 'e']

		subform.moveSelectedRowsUp()
		assert !subform.moveUpEnabled
		assert subform.moveDownEnabled
		assert subform.getColumnValues('text', String.class) == ['c', 'd', 'a', 'b', 'e']

		// Move multiple consecutive rows at once
		subform.toggleSelection(1)

		subform.moveSelectedRowsDown()
		assert subform.moveUpEnabled
		assert subform.moveDownEnabled
		assert subform.getColumnValues('text', String.class) == ['b', 'c', 'd', 'a', 'e']
	}
}
