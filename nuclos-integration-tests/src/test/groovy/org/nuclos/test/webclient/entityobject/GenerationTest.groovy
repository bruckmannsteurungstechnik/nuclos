package org.nuclos.test.webclient.entityobject

import javax.ws.rs.core.Response

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.rest.request.RequestType
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.EntityObjectModal
import org.nuclos.test.webclient.pageobjects.GenerationComponent
import org.nuclos.test.webclient.pageobjects.LocaleChooser
import org.nuclos.test.webclient.pageobjects.MessageModal
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.subform.Subform

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class GenerationTest extends AbstractWebclientTest {

	static long eoId

	Map<String, ?> eoData = [
			name              : 'test',
			mandatorytext     : 'test',
			exceptionbeiinsert: false,
			document          : TestDataHelper.nuclosDocxFile,
			image             : TestDataHelper.nuclosPngFile,
	]
	Map<String, ?> eoData2 = [
			name              : 'test2',
			mandatorytext     : 'test2',
			exceptionbeiinsert: false
	]

	@Test
	void _00_createEO() {
		LocaleChooser.locale = Locale.ENGLISH
		EntityObjectComponent eo = EntityObjectComponent.createEO(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR, eoData)
		eoId = eo.id
	}

	@Test()
	void _05_generateEO() {
		String oldWindow

		generateEo:
		{
			screenshot('before-calling-generator')
			countBrowserRequests {
				int generatorCount = GenerationComponent.generateObjectAndConfirm('Test Objektgenerator')
				assert generatorCount == 8

				GenerationComponent.openResult()

				assert driver.getWindowHandles().size() == 2

				oldWindow = driver.windowHandle
				String newWindow = switchToOtherWindow()

				assert oldWindow != newWindow
			}.with {
				// There should be no update, because the EO is not dirty
				assert it.getRequestCount(RequestType.EO_UPDATE) == 0

				assert it.getRequestCount(RequestType.EO_GENERATION) == 1

				// There is 1 request to load the generated EO (new browser tab)
				// + 1 for an empty list to set the right "canCreateBo" flag
				assert it.getRequestCount(RequestType.EO_READ) == 1
				assert it.getRequestCount(RequestType.EO_READ_LIST_EMPTY) == 1
			}
			screenshot('after-calling-generator')

		}

		checkGeneratedObject:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			assert eo.getAttribute('name') == 'test test test'
			assert eo.getAttribute("valuefrominsertbefore") == 'before'
			assert eo.getAttribute("valuefrominsertafter") == 'after'
			assert eo.getAttribute("valuefromgenrulebefore") == 'before'
			assert eo.getAttribute("valuefromgenruleafter") == 'after'
			assert eo.getFileComponent('document').text == TestDataHelper.nuclosDocxFile.name
			assert eo.getFileComponent('image').imageUrl.contains("/$eo.id/")
		}

		deleteFileInSourceEo:
		{
			switchToOtherWindow()

			EntityObjectComponent eo = EntityObjectComponent.forDetail()
			eo.getFileComponent('document').clearFile()

			// FIXME: Can not be cleared due to problems with the file component
//			eo.getFileComponent('image').clearFile()

			eo.save()
		}

		/**
		 * Files should still be available in the generated EO.
		 */
		checkFileInGeneratedEo:
		{
			switchToOtherWindow()

			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			eo.refresh()
			assert !eo.dirty
			assert Sidebar.listEntryCount == 2

			assert eo.getFileComponent('document').text == TestDataHelper.nuclosDocxFile.name
			assert eo.getFileComponent('image').imageUrl.contains("/$eo.id/")

			// TODO: Download files, check content

			driver.close()
			driver.switchTo().window(oldWindow)
		}

		screenshot('generate-1-window-1')
	}

	@Test
	void _07_staleVersion() {
		EntityObject<Long> eo = nuclosSession.getEntityObject(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR, eoId)
		EntityObject<Long> eo2 = nuclosSession.getEntityObject(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR, eoId)

		eo.setAttribute('name', eo.getAttribute('name').toString() + ' 2')
		eo.save()

		checkRestStatusCode:
		{
			eo2.setAttribute('name', '...')
			expectErrorStatus(Response.Status.CONFLICT) {
				eo2.save()
			}
		}

		checkConflictInWebclient:
		{
			EntityObjectComponent eoComponent = EntityObjectComponent.forDetail()

			eoComponent.clickButton('Objekt generieren')
			messageModal.confirm()

			MessageModal modal = messageModal
			assert modal.message.toLowerCase().contains('version')
			modal.confirm()

			refresh()
		}
	}

	@Test()
	void _10_generateEOWithException() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		screenshot('with-exception-before-update')
		eo.setAttribute('exceptionbeiinsert', true)
		eo.save()

		screenshot('with-exception-before-calling-generator-2')
		int generatorCount = GenerationComponent.generateObjectAndConfirm('Test Objektgenerator')

		assert generatorCount == 8
		screenshot('with-exception-after-calling-generator-2')

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		assert oldWindow != newWindow

		assert eo.alertText == 'Test Exception'

		// close alert dialog
		eo.clickButtonOk()


		driver.close()
		driver.switchTo().window(oldWindow)
		eo.setAttribute('exceptionbeiinsert', false)

		refresh()
		assert Sidebar.listEntryCount == 2


		screenshot('with-exception-generate-1-window-2-after-save')
	}

	@Test()
	void _15_generateEOViaButton() {
		EntityObjectComponent eo = EntityObjectComponent.createEO(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR, eoData2)

		eo.checkButton('Intern generieren', true, true)
		eo.clickButton('Objekt generieren')
		messageModal.confirm()

		GenerationComponent.openResult()

		assert driver.getWindowHandles().size() == 2

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		assert oldWindow != newWindow

		assert !eo.dirty

		driver.close()
		driver.switchTo().window(oldWindow)
	}

	@Test()
	void _17_generateEOViaButtonWithSave() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		generateEo:
		{
			eo.setAttribute('name', 'test 2')

			assert eo.dirty

			countBrowserRequests {
				eo.clickButton('Objekt generieren')
				messageModal.confirm()
				GenerationComponent.openResult()

				assert driver.getWindowHandles().size() == 2

				String oldWindow = driver.windowHandle
				String newWindow = switchToOtherWindow()

				assert oldWindow != newWindow

				assert !eo.dirty

				driver.close()
				driver.switchTo().window(oldWindow)
			}.with {
				// 1 request to save the dirty EO
				assert it.getRequestCount(RequestType.EO_UPDATE) == 1

				assert it.getRequestCount(RequestType.EO_GENERATION) == 1

				// 1 request to read the generated EO
				// + 1 for an empty list to set the right "canCreateBo" flag
				assert it.getRequestCount(RequestType.EO_READ) == 1
				assert it.getRequestCount(RequestType.EO_READ_LIST_EMPTY) == 1
			}

			assert !eo.dirty
		}
	}

	@Test()
	void _20_generateDependent() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform('nuclet_test_other_TestObjektgeneratorDependent_testobjektgen')
		assert subform.rowCount == 0

		eo.clickButton('Dependent generieren')
		messageModal.confirm()

		assert subform.rowCount == 1
	}

	@Test()
	void _25_closeOnException() {
		GenerationComponent.generateObjectAndConfirm('Generate with close on exception')
		MessageModal modal = getMessageModal()

		assert modal.message == 'Exception from GenerateRule'

		modal.confirm()
	}

	@Test()
	void _30_generateWithDependentException() {
		GenerationComponent.generateObjectAndConfirm('Test Objektgenerator With Incomplete Dependents')
		MessageModal modal = getMessageModal()

		assert modal.message.contains('Validation')
		assert modal.message.contains('Mandatory Text')

		modal.confirm()
	}

	@Test()
	void _35_generateEOWithMandatoryNull() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.setAttribute('clearmandatorytextduringgen', true)
		eo.save()

		int generatorCount = GenerationComponent.generateObjectAndConfirm('Test Objektgenerator')

		assert generatorCount == 8

		assert driver.getWindowHandles().size() == 2

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		assert oldWindow != newWindow

		assert eo.dirty
		assert eo.alertText == 'Validation errors occurred.\nThe field "Mandatory Text" must not be empty'

		eo.clickButtonOk()

		assert eo.getAttribute("valuefrominsertbefore") == 'before'
		assert eo.getAttribute("valuefrominsertafter") == ''
		assert eo.getAttribute("valuefromgenrulebefore") == 'before'
		assert eo.getAttribute("valuefromgenruleafter") == ''

		eo.setAttribute("mandatorytext", "not empty")
		eo.save()

		eo.refresh()

		assert Sidebar.listEntryCount == 6
		assert !eo.dirty

		driver.close()
		driver.switchTo().window(oldWindow)
	}

	@Test
	void _40_generateWithoutSaving() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.addNew()
		eo.setAttribute('name', 'test 40')
		eo.setAttribute('mandatorytext', 'mandatory text')
		eo.save()

		int generatorCount = GenerationComponent.generateObjectAndConfirm('Test objectgenerator without saving after generation')

		assert generatorCount == 8

		assert driver.getWindowHandles().size() == 2

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		assert oldWindow != newWindow

		assert eo.dirty
		assert eo.getAlertText() == null

		assert eo.getAttribute("valuefrominsertbefore") == ''
		assert eo.getAttribute("valuefrominsertafter") == ''
		assert eo.getAttribute("valuefromgenrulebefore") == 'before'
		assert eo.getAttribute("valuefromgenruleafter") == ''

		eo.save()

		assert eo.dirty
		assert eo.alertText == 'Test Exception'
		eo.clickButtonOk()

		driver.close()
		driver.switchTo().window(oldWindow)
	}

	@Test
	void _45_generateInOverlay() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.addNew()
		eo.setAttribute('name', 'test 45')
		eo.setAttribute('mandatorytext', 'mandatory text 45')
		eo.save()

		GenerationComponent.generateObjectAndConfirm('Generate dependent and show in Overlay')

		assert driver.getWindowHandles().size() == 1

		editGeneratedObjectInModal:
		{
			EntityObjectModal eoModal = EntityObjectComponent.forModal()
			assert eoModal.getAttribute('mandatorytext') == 'mandatory text 45'
			eoModal.setAttribute('name', 'changed-in-modal')
			eoModal.save()
			eoModal.clickButtonClose()
		}

		assert !eo.dirty

		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATORDEPENDENT, 'testobjektgen')
		assert subform.rowCount == 1
		assert subform.getRow(0).getValue('name') == 'changed-in-modal'
	}

	@Test
	void _46_generateInOverlayAndDelete() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.addNew()
		eo.setAttribute('name', 'test 46')
		eo.setAttribute('mandatorytext', 'mandatory text 46')
		eo.save()

		GenerationComponent.generateObjectAndConfirm('Generate dependent and show in Overlay')

		assert driver.getWindowHandles().size() == 1

		editGeneratedObjectInModal:
		{
			EntityObjectModal eoModal = EntityObjectComponent.forModal()
			assert eoModal.getAttribute('mandatorytext') == 'mandatory text 46'
			eoModal.delete()
		}

		assert !eo.dirty

		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATORDEPENDENT, 'testobjektgen')
		assert subform.rowCount == 0
	}

	@Test
	void _50_generateInOverlayAndCompleteInput() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.addNew()
		eo.setAttribute('name', 'test 50')
		eo.setAttribute('mandatorytext', 'mandatory text 50')
		eo.save()

		GenerationComponent.generateObjectAndConfirm('Generate incomplete dependent and show in Overlay')

		assert driver.getWindowHandles().size() == 1

		editGeneratedObjectInModal:
		{
			EntityObjectModal eoModal = EntityObjectComponent.forModal()
			assert eoModal.alertText == 'Validation errors occurred.\n' +
					'The field "Mandatory Text" must not be empty'
			assert !eoModal.dirty
			eoModal.clickButtonOk()

			eoModal.setAttribute('mandatorytext', 'input-in-modal')
			eoModal.save()
			eoModal.clickButtonClose()
		}

		assert !eo.dirty

		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATORDEPENDENT, 'testobjektgen')
		assert subform.rowCount == 1
		assert subform.getRow(0).getValue('mandatorytext') == 'input-in-modal'
	}
}
