package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class HyperlinkTest extends AbstractWebclientTest {

	private final String hyperlink = 'hyperlink'
	private final String disabledHyperlink = 'disabledhyperlink'

	private final String testUrl = 'http://127.0.0.1'
	private final String dataURL = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAABGdBTUEAALGPC/' +
			'xhBQAAAAlwSFlzAAALEwAACxMBAJqcGAAAAAd0SU1FB9YGARc5KB0XV+IAAAAddEVYdENvbW1lbnQAQ3JlYX' +
			'RlZCB3aXRoIFRoZSBHSU1Q72QlbgAAAF1JREFUGNO9zL0NglAAxPEfdLTs4BZM4DIO4C7OwQg2JoQ9LE1exd' +
			'lYvBBeZ7jqch9//q1uH4TLzw4d6+ErXMMcXuHWxId3KOETnnXXV6MJpcq2MLaI97CER3N0vr4MkhoXe0rZig' +
			'AAAABJRU5ErkJggg=='

	@Test
	void _00_setup() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTHYPERLINK)
		eo.addNew()
		eo.save()
	}

	@Test
	void _05_testEmptyLink() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		assert !eo.getAttribute(hyperlink)
		assert !eo.getAttribute(disabledHyperlink)

		NuclosWebElement hyperlink = eo.getAttributeElement(hyperlink)
		hyperlink.doubleClick()
		assert getDriver().getWindowHandles().size() == 1, 'Clicking an empty link should not open a new window'
	}

	@Test
	void _10_testEditLink() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.setAttribute(hyperlink, dataURL)
		eo.getAttributeElement(hyperlink).doubleClick()

		checkNewWindowAndCloseIt() {
			List<NuclosWebElement> images = $$('img')
			assert images.size() == 1
			assert images[0].getAttribute('src') == dataURL
		}
	}

	private void checkNewWindowAndCloseIt(Closure check) {
		Set<String> handles = getDriver().getWindowHandles()

		assert handles.size() == 2

		switchToOtherWindow()

		check()

		switchToOtherWindow()
		closeOtherWindows()
	}

	@Test
	void _15_testDisabledLink() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		// Use about:blank as URL because data URLs can't be opened anymore in newer browser versions
		eo.setAttribute(hyperlink, testUrl)

		eo.save()
		assert eo.getText(disabledHyperlink) == testUrl

		NuclosWebElement link = eo.getAttributeElement(disabledHyperlink)
		assert link.tagName == 'a', 'A disabled hyperlink component should just be displayed as a standard HTML link'
		link.click()

		checkNewWindowAndCloseIt() {
			getDriver().currentUrl == testUrl
		}
	}

	@Test
	void _20_testLinkButton() {
		EntityObjectComponent.forDetail().clickButton('Hyperlink Button')

		checkNewWindowAndCloseIt() {
			getDriver().currentUrl == testUrl
		}
	}
}
