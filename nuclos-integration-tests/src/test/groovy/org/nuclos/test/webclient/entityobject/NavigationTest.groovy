package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class NavigationTest extends AbstractWebclientTest {

	@Test
	void _01setupBos() {
		assert $('#logout')
		TestDataHelper.insertTestData(nuclosSession)
	}


	@Test
	void _02navigateToEmptyBoList() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		assert eo.getDeleteButton() != null
		assert eo.getNewButton() != null

		// navigate to an entity with no data
		eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CITY)

		assert eo.getDeleteButton() == null // no item in list -> no first item to select -> no delete button
		assert eo.getNewButton() != null

	}

}
