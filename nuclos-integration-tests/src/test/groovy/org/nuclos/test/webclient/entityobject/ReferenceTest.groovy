package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.schema.layout.web.WebListofvalues
import org.nuclos.schema.layout.web.WebTextfield
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.ListOfValues
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.search.Searchbar
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.pageobjects.subform.SubformReferenceCell
import org.nuclos.test.webclient.pageobjects.viewconfiguration.SideviewConfiguration
import org.openqa.selenium.Keys

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ReferenceTest extends AbstractWebclientTest {

	private allArticleChoices = ['1001 Nuclos', '1002 Text processor', '1003 Notebook Pro', '1004 Mouse']

	@Test
	void _00_createEO() {
		TestDataHelper.insertTestData(nuclosSession)

		def customer4 =
				[
						boMetaId  : TestEntities.EXAMPLE_REST_CUSTOMER.fqn,
						attributes: [
								name          : 'Test-Customer 4',
								customerNumber: 2003,
								discount      : 22.5,
								active        : true
						]
				]
		RESTHelper.createBo(customer4, nuclosSession)

		def rechnung =
				[
						boMetaId  : TestEntities.EXAMPLE_REST_RECHNUNG.fqn,
						attributes: [
								name: 'Rechnung 1',
						]
				]
		RESTHelper.createBo(rechnung, nuclosSession)

		def auftragsposition =
				[
						boMetaId  : TestEntities.EXAMPLE_REST_AUFTRAGSPOSITION.fqn,
						attributes: [
								name: 'Fremdposition',
						]
				]
		RESTHelper.createBo(auftragsposition, nuclosSession)
	}

	@Test
	void _05_openCreatedEO() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		waitUntilTrue({ -> "Test-Customer" == eo.getAttribute('customer') })
	}

	@Test
	void _07_testOverlayVisibility() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		ListOfValues customerLov = ListOfValues.findByAttribute('customer')
		assert customerLov.isSearchVisible()
		assert customerLov.isAddVisible()
		assert customerLov.isOpenVisible()

		// NUCLOS-6489: Test if overlays are still the same after refresh (important)
		Sidebar.refresh()

		customerLov = ListOfValues.findByAttribute('customer')
		assert customerLov.isSearchVisible()
		assert customerLov.isAddVisible()
		assert customerLov.isOpenVisible()

		eo.setAttribute('customer', '')

		assert customerLov.isSearchVisible()
		assert customerLov.isAddVisible()
		assert !customerLov.isOpenVisible()

		ListOfValues rechnungDropdown = ListOfValues.findByAttribute('rechnung')

		assert rechnungDropdown.isSearchVisible()
		assert !rechnungDropdown.isAddVisible()
		assert !rechnungDropdown.isOpenVisible()

		eo.setAttribute('rechnung', 'Rechnung 1')

		assert rechnungDropdown.isSearchVisible()
		assert !rechnungDropdown.isAddVisible()
		assert rechnungDropdown.isOpenVisible()

		eo.cancel()

		assert customerLov.isSearchVisible()
		assert customerLov.isAddVisible()
		assert customerLov.isOpenVisible()

		assert rechnungDropdown.isSearchVisible()
		assert !rechnungDropdown.isAddVisible()
		assert !rechnungDropdown.isOpenVisible()
	}

	@Test
	void _08_testOverlayVisibilitySubform() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order')
		subform.openViewConfiguration()

		// don't show reference column to main EO in column configuration
		waitUntilTrue({ -> SideviewConfiguration.findUnselectedColumnOption(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order') == null })

		SideviewConfiguration.deselectColumn(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_name')
		SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_fremdposition')
		SideviewConfiguration.clickButtonOk()

		SubformReferenceCell subformReferenceCell = new SubformReferenceCell(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn, 'category')
		SubformReferenceCell subformReferenceCell2 = new SubformReferenceCell(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn, 'fremdposition')

		// assert subformReferenceCell.isSearchVisible() ... search only in active editor (see NUCLOS-5330, tested by org.nuclos.test.webclient.entityobject.SubformLovTest)
		// assert subformReferenceCell.isAddVisible() ... add only in active editor (see NUCLOS-5330, tested by org.nuclos.test.webclient.entityobject.SubformLovTest)
		assert !subformReferenceCell.isOpenVisible()

		subformReferenceCell.setValue('Hardware')

		subformReferenceCell = new SubformReferenceCell(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn, 'category')

		// assert subformReferenceCell.isSearchVisible() ... search only in active editor (see NUCLOS-5330, tested by org.nuclos.test.webclient.entityobject.SubformLovTest)
		// assert subformReferenceCell.isAddVisible() ... add only in active editor (see NUCLOS-5330, tested by org.nuclos.test.webclient.entityobject.SubformLovTest)
		assert subformReferenceCell.isOpenVisible()

		assert !subformReferenceCell2.isSearchVisible()
		assert !subformReferenceCell2.isAddVisible()
		assert !subformReferenceCell2.isOpenVisible()

		subformReferenceCell2.setValue('Fremdposition')

		waitUntilTrue({ -> subform.getRow(0).getValue('fremdposition') == 'Fremdposition' })

		subformReferenceCell2 = new SubformReferenceCell(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn, 'fremdposition')

		assert !subformReferenceCell2.isSearchVisible()
		assert !subformReferenceCell2.isAddVisible()
		assert !subformReferenceCell2.isOpenVisible()

		eo.cancel()

		subformReferenceCell = new SubformReferenceCell(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn, 'category')
		// assert subformReferenceCell.isSearchVisible() ... search only in active editor (see NUCLOS-5330, tested by org.nuclos.test.webclient.entityobject.SubformLovTest)
		// assert subformReferenceCell.isAddVisible() ... add only in active editor (see NUCLOS-5330, tested by org.nuclos.test.webclient.entityobject.SubformLovTest)
		assert !subformReferenceCell.isOpenVisible()

		subform.openViewConfiguration()
		SideviewConfiguration.deselectColumn(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_fremdposition')
		SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_name')
		SideviewConfiguration.clickButtonOk()

	}

	@Test
	void _10_testReopenCombobox() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		String entry1 = 'Test 1, 12345, Test 1'
		String entry2 = 'Test 2, 54321, Test 2'

		waitUntilTrue({ -> eo.getAttribute('customer') == 'Test-Customer' })
		assert !eo.getText('customerAddress')?.trim()

		ListOfValues lov = eo.getLOV('customerAddress')
		assert !lov.open
		assert !lov.textValue

		lov.open()
		assert !lov.textValue
		waitUntilTrue({ -> lov.choices == ['', entry1, entry2] })
		lov.close()

		lov.selectEntry(entry2)
		waitUntilTrue({ -> eo.getAttribute('customerAddress') == entry2 })

		lov.open()
		waitUntilTrue({ -> lov.choices == ['', entry1, entry2] })
		lov.close()

		eo.save()
		lov = eo.getLOV('customerAddress')
		assert !lov.open

		lov.open()
		waitUntilTrue({ -> lov.choices == ['', entry1, entry2] })
		lov.close()
	}

	@Test
	void _12_testReopenComboboxInSubform() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order')
		Subform.Row row = subform.getRow(0)

		row.clickCell('category')
		ListOfValues lov = row.getLOV('category')
		assert lov.open
		assert !lov.textValue
		assert lov.choices.size() > 1

		2.times {
			lov.clickDropdownButton()
			assert !lov.open

			lov.clickDropdownButton()
			assert lov.open
			assert lov.choices.size() > 1
		}

		lov.closeViaEscape()
	}

	/**
	 * Tests if the selected text is still visible in the input field of the LOV,
	 * when the LOV is open.
	 */
	@Test
	void _13_testTextValueWithOpenPanel() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order')
		Subform.Row row = subform.getRow(0)

		row.clickCell('category')
		ListOfValues lov = row.getLOV('category')
		assert lov.open
		assert !lov.textValue
		assert lov.choices.size() > 1

		lov.selectEntry('Hardware')

		row.clickCell('category')
		lov = row.getLOV('category')
		assert lov.open
		waitUntilTrue({ -> lov.textValue == 'Hardware' })

		2.times {
			lov.clickDropdownButton()
			assert !lov.open
			waitUntilTrue({ -> lov.textValue == 'Hardware' })

			lov.clickDropdownButton()
			assert lov.open
			waitUntilTrue({ -> lov.textValue == 'Hardware' })
		}

		eo.cancel()
	}

	/**
	 * Tests if the highlighting via up/down keys works.
	 */
	@Test
	void _14_testSubformLovArrowKeys() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order')
		Subform.Row row = subform.getRow(0)

		waitUntilTrue({ -> row.getValue('article') == '1004 Mouse' })

		row.clickCell('article')
		ListOfValues lov = row.getLOV('article')
		assert lov.open

		waitUntilTrue({ -> lov.choices == allArticleChoices })
		waitUntilTrue({ -> lov.selectedEntryFromDropdown == '1004 Mouse' })
		waitUntilTrue({ -> lov.highlightedEntry == '1004 Mouse' })

		// Down-Arrow should not do anything, if the last entry is already highlighted
		sendKeys(Keys.ARROW_DOWN)
		waitUntilTrue({ -> lov.selectedEntryFromDropdown == '1004 Mouse' })
		waitUntilTrue({ -> lov.highlightedEntry == '1004 Mouse' })

		sendKeys(Keys.ARROW_UP)
		lov = row.getLOV('article')
		waitUntilTrue({ -> lov.selectedEntryFromDropdown == '1004 Mouse' })
		waitUntilTrue({ -> lov.highlightedEntry == '1003 Notebook Pro' })

		sendKeys(Keys.ARROW_UP)
		waitUntilTrue({ -> lov.selectedEntryFromDropdown == '1004 Mouse' })
		waitUntilTrue({ -> lov.highlightedEntry == '1002 Text processor' })

		sendKeys(Keys.ARROW_UP)
		waitUntilTrue({ -> lov.selectedEntryFromDropdown == '1004 Mouse' })
		waitUntilTrue({ -> lov.highlightedEntry == '1001 Nuclos' })

		// Up-Arrow should not do anything, if the first entry is already highlighted
		sendKeys(Keys.ARROW_UP)
		waitUntilTrue({ -> lov.selectedEntryFromDropdown == '1004 Mouse' })
		waitUntilTrue({ -> lov.highlightedEntry == '1001 Nuclos' })

		sendKeys(Keys.TAB)
		assert !lov.open

		waitUntilTrue({ -> row.getValue('article') == '1001 Nuclos' })

		// FIXME: Editing does not work with a single click after changing LOV value and tabbing out
		row.clickCell('article')
		row.clickCell('article')
		lov = row.getLOV('article')

		waitUntilTrue({ -> lov.selectedEntryFromDropdown == '1001 Nuclos' })
		waitUntilTrue({ -> lov.highlightedEntry == '1001 Nuclos' })

		eo.cancel()

		row.clickCell('article')
		lov = row.getLOV('article')

		waitUntilTrue({ -> lov.selectedEntryFromDropdown == '1004 Mouse' })
		waitUntilTrue({ -> lov.highlightedEntry == '1004 Mouse' })

		sendKeys(Keys.ESCAPE)
		assert !eo.dirty
	}

	@Test
	void _15_testAutocompletionBehavior() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order')
		Subform.Row row = subform.getRow(0)

		row.clickCell('article')
		ListOfValues lov = row.getLOV('article')
		assert lov.open

		// FIXME: String representation is not consistent between input field and dropdown
		waitUntilTrue({ -> lov.choices == allArticleChoices })
		waitUntilTrue({ -> lov.selectedEntryFromDropdown == '1004 Mouse' })
		waitUntilTrue({ -> lov.highlightedEntry == '1004 Mouse' })

		testAutocompletionBetweenKeyPresses:
		{
			sendKeys(Keys.chord(Keys.CONTROL, 'a'))
			sendKeys(Keys.BACK_SPACE)
			waitForAngularRequestsToFinish()
			waitUntilTrue({ -> lov.choices == allArticleChoices })
			// Clearing the input should also clear the model/selection
			assert !lov.selectedEntryFromDropdown
			assert !lov.highlightedEntry

			sendKeys('1')
			waitForAngularRequestsToFinish()
			assert eo.dirty
			waitUntilTrue({ -> lov.choices == allArticleChoices })
			assert !lov.selectedEntryFromDropdown
			assert !lov.highlightedEntry

			sendKeys('1')
			waitForAngularRequestsToFinish()
			assert lov.choices.empty
			assert !lov.selectedEntryFromDropdown
			assert !lov.highlightedEntry

			sendKeys(Keys.BACK_SPACE)
			waitForAngularRequestsToFinish()
			waitUntilTrue({ -> lov.choices == allArticleChoices })
			assert !lov.selectedEntryFromDropdown
			assert !lov.highlightedEntry

			sendKeys('*')
			waitForAngularRequestsToFinish()
			waitUntilTrue({ -> lov.choices == allArticleChoices })
			assert !lov.selectedEntryFromDropdown
			assert !lov.highlightedEntry

			sendKeys('1')
			waitForAngularRequestsToFinish()
			waitUntilTrue({ -> lov.choices == ['1001 Nuclos'] })
			assert !lov.selectedEntryFromDropdown
			waitUntilTrue({ -> lov.highlightedEntry == '1001 Nuclos' })
		}

		sendKeys(Keys.ENTER)
		waitForAngularRequestsToFinish()
		waitUntilTrue({ -> row.getValue('article') == '1001 Nuclos' })

		row.clickCell('article')
		lov = row.getLOV('article')
		assert lov.open

		testAutocompletionForBackspaces:
		{
			waitUntilTrue({ -> lov.selectedEntryFromDropdown == '1001 Nuclos' })
			waitUntilTrue({ -> lov.highlightedEntry == '1001 Nuclos' })

			sendKeys(Keys.ARROW_RIGHT)

			sendKeys(Keys.BACK_SPACE)
			sendKeys(Keys.BACK_SPACE)
			sendKeys(Keys.BACK_SPACE)
			sendKeys(Keys.BACK_SPACE)
			sendKeys(Keys.BACK_SPACE)
			sendKeys(Keys.BACK_SPACE)
			sendKeys(Keys.BACK_SPACE)
			sendKeys(Keys.BACK_SPACE)
			waitForAngularRequestsToFinish()

			// "100"
			waitUntilTrue({ -> lov.choices == allArticleChoices })
			assert !lov.selectedEntryFromDropdown
			assert !lov.highlightedEntry

			// "10"
			sendKeys(Keys.BACK_SPACE)
			waitForAngularRequestsToFinish()
			waitUntilTrue({ -> lov.choices == allArticleChoices })
			assert !lov.selectedEntryFromDropdown
			assert !lov.highlightedEntry

			// "1"
			sendKeys(Keys.BACK_SPACE)
			waitForAngularRequestsToFinish()
			waitUntilTrue({ -> lov.choices == allArticleChoices })
			assert !lov.selectedEntryFromDropdown
			assert !lov.highlightedEntry

			// ""
			sendKeys(Keys.BACK_SPACE)
			waitForAngularRequestsToFinish()
			waitUntilTrue({ -> lov.choices == allArticleChoices })
			assert !lov.selectedEntryFromDropdown
			assert !lov.highlightedEntry
			waitUntilTrue({ -> lov.inputElement.text == '' })

			// "0"
			sendKeys('0')
			waitForAngularRequestsToFinish()
			waitUntilTrue({ -> lov.choices == allArticleChoices })
			assert !lov.selectedEntryFromDropdown
			assert !lov.highlightedEntry

			// "01"
			sendKeys('1')
			waitForAngularRequestsToFinish()
			waitUntilTrue({ -> lov.choices == ['1001 Nuclos'] })
			assert !lov.selectedEntryFromDropdown
			waitUntilTrue({ -> lov.highlightedEntry == '1001 Nuclos' })

			// ""
			sendKeys(Keys.BACK_SPACE)
			sleep(50)
			sendKeys(Keys.BACK_SPACE)
			waitForAngularRequestsToFinish()
			waitUntilTrue({ -> lov.choices == allArticleChoices })
			assert !lov.selectedEntryFromDropdown
			assert !lov.highlightedEntry
			waitUntilTrue({ -> lov.inputElement.text == '' })
		}

		eo.cancel()
	}

	@Test
	void _18_searchReferenceInMainForm() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		ListOfValues customerLov = ListOfValues.findByAttribute('customer')
		customerLov.searchReference()

		waitUntilTrue({ -> driver.getWindowHandles().size() == 2 })

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		waitUntilTrue({ -> oldWindow != newWindow })

		Searchbar.search('Test-Customer 2')
		Sidebar.selectEntryByText('Test-Customer 2')

		// select first entry in other window
		eo.selectInOtherWindow()

		driver.switchTo().window(oldWindow)

		eo.save()

		waitUntilTrue({ -> eo.getAttribute('customer') == 'Test-Customer 2' })
	}

	@Test
	void _20_editReferenceInMainForm() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()
		Sidebar.refresh()

		Sidebar.selectEntry(2)
		eo.setAttribute('note', 'note changed')

		ListOfValues customerLov = ListOfValues.findByAttribute('customer')
		customerLov.openReference()

		waitForAngularRequestsToFinish()

		editCustomerInOtherWindow:
		{
			waitUntilTrue({ -> driver.getWindowHandles().size() == 2 })

			String oldWindow = driver.windowHandle
			String newWindow = switchToOtherWindow()

			waitUntilTrue({ -> oldWindow != newWindow })

			assert driver.currentUrl.contains('Customer')

			EntityObjectComponent eo2 = EntityObjectComponent.forDetail()
			waitUntilTrue({ -> eo2.getAttribute('name') == 'Test-Customer' })
			eo2.setAttribute('name', 'Test-Customer changed')

			// save and close window
			eo2.save()

			driver.switchTo().window(oldWindow)
		}

		waitUntilTrue({ -> eo.getAttribute('note') == 'note changed' })
		waitUntilTrue({ -> eo.getAttribute('customer') == 'Test-Customer changed' })

		eo.cancel()
	}


	@Test
	void _25_createReferencedEntryInMainForm() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		ListOfValues customerLov = ListOfValues.findByAttribute('customer')
		customerLov.addReference()


		waitUntilTrue({ -> driver.getWindowHandles().size() == 2 })

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		waitUntilTrue({ -> oldWindow != newWindow })

		// create new customer entry
		eo.setAttribute('customerNumber', '2004')
		eo.setAttribute('name', 'Test-Customer 3')
		eo.save()

		driver.switchTo().window(oldWindow)

		waitForAngularRequestsToFinish()
		eo.save()

		// new customer is now selected
		waitUntilTrue({ -> eo.getAttribute('customer') == "Test-Customer 3" })
	}

	// @Test
	//  ... search only in active editor (see NUCLOS-5330, tested by org.nuclos.test.webclient.entityobject.SubformLovTest)
	/*void _30_searchReferenceInSubForm() {

		SubformReferenceCell subformReferenceCell = new SubformReferenceCell(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn, 'category')

		screenshot('subform-category-selected')

		// click search in other window icon
		subformReferenceCell.searchReference()

		waitForAngularRequestsToFinish()

		waitUntilTrue({ -> driver.getWindowHandles().size() == 2 })

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		waitUntilTrue({ -> oldWindow != newWindow })

		screenshot('select-entry-for-subform')

		Searchbar.search('Software')

		// select first entry in other window
		EntityObjectComponent.forDetail().selectInOtherWindow()

		driver.switchTo().window(oldWindow)

		screenshot('is-entry-selected-for-subform')

		waitFor {
			subformReferenceCell.subformReference.text == 'Software'
		}
	}*/

	// @Test
	//  ... add only in active editor (see NUCLOS-5330, tested by org.nuclos.test.webclient.entityobject.SubformLovTest)
	/*void _35_addReferenceInSubForm() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()
		Sidebar.refresh()

		eo.open(TestEntities.EXAMPLE_REST_ORDER)
		SubformReferenceCell subformReferenceCell = new SubformReferenceCell(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn, 'category')

		screenshot('subform-category-selected')

		// click add in other window icon
		subformReferenceCell.addReference()

		waitForAngularRequestsToFinish()

		waitUntilTrue({ -> driver.getWindowHandles().size() == 2 })

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		waitUntilTrue({ -> oldWindow != newWindow })

		screenshot('add-entry-for-subform')

		// create new category entry
		eo.setAttribute('name', 'Test-Category')
		eo.save()

		driver.switchTo().window(oldWindow)

		screenshot('is-entry-added-for-subform')

		waitFor {
			subformReferenceCell.subformReference.text == 'Test-Category'
		}
	}*/

	@Test
	void _40_openReferenceViaLovOverlay() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.refresh()
		SubformReferenceCell subformReferenceCell = new SubformReferenceCell(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn, 'article')

		screenshot('subform-category-selected')

		// click add in other window icon
		subformReferenceCell.openReference()

		waitForAngularRequestsToFinish()

		waitUntilTrue({ -> driver.getWindowHandles().size() == 2 })

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		waitUntilTrue({ -> oldWindow != newWindow })

		screenshot('open-ref-entry-from-subform')

		waitUntilTrue({ -> driver.currentUrl.indexOf("Article") != -1 })
	}

	@Test
	void _55_testReferenceInTextfield() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)

		eo.addNew()

		assert !eo.getText('reference', WebListofvalues)?.trim()
		assert !eo.getText('reference', WebTextfield)?.trim()

		eo.setAttribute('reference', 'nuclos')

		waitUntilTrue({ -> eo.getAttribute('reference', WebListofvalues) == 'nuclos' })
		waitUntilTrue({ -> eo.getAttribute('reference', WebTextfield) == 'nuclos' })

		eo.setAttribute('text', 'Test')
		eo.save()
		assert !eo.dirty

		waitUntilTrue({ -> eo.getAttribute('reference', WebListofvalues) == 'nuclos' })
		waitUntilTrue({ -> eo.getAttribute('reference', WebTextfield) == 'nuclos' })

	}

	@Test
	/* NUCLOS-5935 */
	void _60_testLovClickoutside() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		eo.addNew()

		eo.setAttribute('customer', 'VLPText: Test-Customer 2')
		eo.setAttribute('note', 'Note')

		// assert that customer is still set
		waitUntilTrue({ -> eo.getAttribute('customer') == 'VLPText: Test-Customer 2' })

		eo.setAttribute('orderNumber', 3748293)
		eo.save()
	}

	@Test
	void _65_testDropdownEntrySelection() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		waitUntilTrue({ -> eo.getAttribute('customer') == 'Test-Customer 2' })

		List<String> allCustomers = ['VLPText: Test-Customer changed', 'VLPText: Test-Customer 2', 'VLPText: Test-Customer 3', 'VLPText: Test-Customer 4']

		ListOfValues lov = eo.getLOV('customer')
		lov.open()
		assert lov.choices.containsAll(allCustomers)
		lov.close()

		// FIXME: Should not be dirty here
//		assert !eo.dirty

		testSelectionOfSingleResult:
		{
			lov.search('test*4')
			waitUntilTrue({ -> lov.choices == ['VLPText: Test-Customer 4'] })

			lov.blur()
			waitUntilTrue({ -> lov.textValue == 'VLPText: Test-Customer 4' })
			assert eo.dirty

			eo.save()
			waitUntilTrue({ -> eo.getAttribute('customer') == 'Test-Customer 4' })
		}

		testDeselectionForNoResult:
		{
			lov.search('nonexistentcustomer')
			assert lov.choices.empty
			assert eo.dirty
			lov.blur()
			assert !lov.textValue
			assert eo.dirty

			eo.save()
			assert !eo.getAttribute('customer')
		}

		eo.setAttribute('customer', 'VLPText: Test-Customer 4')
		eo.save()

		testDeselectionForMultipleResults:
		{
			lov.search('Test')
			assert lov.choices.containsAll(allCustomers)
			lov.blur()
			assert !lov.textValue
			assert eo.dirty

			eo.save()
			assert !eo.getAttribute('customer')
		}

		testNotDirtyAfterSelectingSameEntry:
		{
			eo.setAttribute('customer', 'VLPText: Test-Customer 4')
			eo.save()
			assert !eo.dirty

			lov.search('test*4')
			waitUntilTrue({ -> lov.choices == ['VLPText: Test-Customer 4'] })

			lov.blur()
			waitUntilTrue({ -> lov.textValue == 'Test-Customer 4' })
			assert !eo.dirty
		}
	}
}
