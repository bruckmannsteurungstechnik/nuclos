package org.nuclos.test.webclient.entityobject

import javax.ws.rs.core.Response

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.api.businessobject.Flag
import org.nuclos.server.rest.services.helper.RecursiveDependency
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class StatefulSubsubformPermissionTest extends AbstractNuclosTest {

	private static EntityObject<Long> parent
	private static List<EntityObject<Long>> subformData

	static RESTClient testUser = new RESTClient('test', 'test')

	@Test
	void _00_setup() {
		RESTHelper.createUser('test', 'test', ['Example user'], nuclosSession)
		RESTHelper.createUser('readonly', 'readonly', ['Example readonly'], nuclosSession)

		parent = new EntityObject(TestEntities.NUCLET_TEST_SUBFORM_PARENTFORSTATEFULSUBFORM)
		parent.setAttribute('text', 'parent')

		subformData = parent.getDependents(
				TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL,
				'parent'
		)

		3.times { subCount ->
			EntityObject subEo = new EntityObject(TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL)
			subEo.setAttribute('text', subCount.toString())
			subformData << subEo

			// TODO: It's impossible to save stateful subsubforms in stateful subforms...
			List<EntityObject<Long>> subsubform = subEo.getDependents(
					TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORMSTATUSMODEL,
					'subform'
			)
			3.times { subsubCount ->
				EntityObject subsubEo = new EntityObject(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORMSTATUSMODEL)
				subsubEo.setAttribute('text', "$subCount-$subsubCount".toString())
				subsubform << subsubEo
			}

			List<EntityObject<Long>> subsubformStateless = subEo.getDependents(
					TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORMSTATELESS,
					'subform'
			)
			3.times { subsubCount ->
				EntityObject subsubEo = new EntityObject(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORMSTATELESS)
				subsubEo.setAttribute('text', "$subCount-$subsubCount".toString())
				subsubformStateless << subsubEo
			}
		}

		nuclosSession.save(parent)

		subformData = nuclosSession.loadDependents(
				parent,
				TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL,
				TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL.fqn + '_parent'
		)

		// TODO: It's impossible to save stateful subsubforms in stateful subforms...
//		assert nuclosSession.loadDependents(
//				subformData.first(),
//				TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORMSTATUSMODEL,
//				TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORMSTATUSMODEL.fqn + '_subform'
//		).size() == 3

		assert nuclosSession.loadDependents(
				subformData.first(),
				TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORMSTATELESS,
				TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORMSTATELESS.fqn + '_subform'
		).size() == 3
	}

	@Test
	void _10_noPermission() {
		RESTClient readonlyClient = new RESTClient('readonly', 'readonly')
		readonlyClient.login()

		expectErrorStatus(Response.Status.FORBIDDEN) {
			readonlyClient.getEntityObject(
					TestEntities.NUCLET_TEST_SUBFORM_PARENTFORSTATEFULSUBFORM,
					parent.id
			)
		}

		expectErrorStatus(Response.Status.FORBIDDEN) {
			readonlyClient.loadDependents(
					parent,
					TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL,
					TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL.fqn + '_parent'
			)
		}

		expectErrorStatus(Response.Status.FORBIDDEN) {
			readonlyClient.loadDependentsRecursively(
					parent,
					TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL,
					RecursiveDependency.forRoot(parent.id as String).dependency(
							TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL.fqn + '_parent'
					)
			)
		}

		expectErrorStatus(Response.Status.FORBIDDEN) {
			readonlyClient.loadDependentsRecursively(
					parent,
					TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL,
					RecursiveDependency.forRoot(parent.id as String).dependency(
							TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL.fqn + '_parent',
							subformData.first().id.toString()
					)
			)
		}

		expectErrorStatus(Response.Status.FORBIDDEN) {
			readonlyClient.loadDependentsRecursively(
					parent,
					TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL,

					RecursiveDependency.forRoot(parent.id as String).dependency(
							TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL.fqn + '_parent',
							subformData.first().id.toString(),
					).dependency(
							TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORMSTATUSMODEL.fqn + '_subform'
					)
			)
		}
	}

	@Test
	void _20_allPermissions() {
		testUser.login()

		/**
		 * All permissions for Example User.
		 */
		testUser.getEntityObject(
				TestEntities.NUCLET_TEST_SUBFORM_PARENTFORSTATEFULSUBFORM,
				parent.id
		)

		subformData = testUser.loadDependents(
				parent,
				TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL,
				TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL.fqn + '_parent'
		)

		assert testUser.loadDependentsRecursively(
				parent,
				TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL,
				RecursiveDependency.forRoot(parent.id as String).dependency(
						TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL.fqn + '_parent',
						subformData.first().id.toString()
				)
		).first().entityClass == TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL


		// TODO: It's impossible to save stateful subsubforms in stateful subforms...
//		assert testUser.loadDependentsRecursively(
//				parent,
//				TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL,
//				TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL.fqn + '_parent',
//				subformData.first().id.toString(),
//				TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORMSTATUSMODEL.fqn + '_subform'
//		)

		assert testUser.loadDependentsRecursively(
				parent,
				TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL,
				RecursiveDependency.forRoot(parent.id as String).dependency(
						TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL.fqn + '_parent',
						subformData.first().id.toString()
				).dependency(
						TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORMSTATELESS.fqn + '_subform'
				)
		)

		updateSubsubformData(testUser)
	}

	@Test
	void _30_readonlyPermissions() {
		parent.changeState(20)

		// Read allowed
		// TODO: It's impossible to save stateful subsubforms in stateful subforms...
//		assert testUser.loadDependentsRecursively(
//				parent,
//				TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL,
//				TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL.fqn + '_parent',
//				subformData.first().id.toString(),
//				TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORMSTATUSMODEL.fqn + '_subform'
//		)

		assert testUser.loadDependentsRecursively(
				parent,
				TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL,
				RecursiveDependency.forRoot(parent.id as String).dependency(
						TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL.fqn + '_parent',
						subformData.first().id.toString(),
				).dependency(
						TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORMSTATELESS.fqn + '_subform'
				)
		)

		expectErrorStatus(Response.Status.FORBIDDEN) {
			updateSubsubformData(testUser)
		}
	}

	@Test
	void _40_noSubsubformPermissions() {
		parent.changeState(30)

		// Subform read allowed
		testUser.loadDependents(
				parent,
				TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL,
				TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL.fqn + '_parent'
		)

		// Subform record read allowed
		testUser.loadDependentsRecursively(
				parent,
				TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL,
				RecursiveDependency.forRoot(parent.id as String).dependency(
						TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL.fqn + '_parent',
						subformData.first().id.toString()
				)
		)

		// Subsubform read forbidden
		expectErrorStatus(Response.Status.FORBIDDEN) {
			testUser.loadDependentsRecursively(
					parent,
					TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL,
					RecursiveDependency.forRoot(parent.id as String).dependency(
							TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL.fqn + '_parent',
							subformData.first().id.toString(),
					).dependency(
							TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORMSTATUSMODEL.fqn + '_subform'
					)
			)
		}

		// Stateless Subsubform read forbidden
		expectErrorStatus(Response.Status.FORBIDDEN) {
			testUser.loadDependentsRecursively(
					parent,
					TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL,

					RecursiveDependency.forRoot(parent.id as String).dependency(
							TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL.fqn + '_parent',
							subformData.first().id.toString(),
					).dependency(
							TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORMSTATELESS.fqn + '_subform'
					)
			)
		}
	}

	void updateSubsubformData(RESTClient user) {
		parent = user.getEntityObject(parent.entityClass, parent.id)

		subformData = user.loadDependents(
				parent,
				TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL,
				TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL.fqn + '_parent'
		)

		parent.dependents.put(
				TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL.fqn + '_parent',
				subformData
		)

		// TODO: It's impossible to save stateful subsubforms in stateful subforms...
//		stateful:
//		{
//			List<EntityObject<Long>> subsubformData = user.loadDependentsRecursively(
//					parent,
//					TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORMSTATUSMODEL,
//					TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL.fqn + '_parent',
//					subformData.first().id as String,
//					TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORMSTATUSMODEL.fqn + '_subform',
//					subformData.first().id as String
//			)
//
//			subformData.first().dependents.put(
//					TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORMSTATUSMODEL.fqn + '_subform',
//					subsubformData
//			)
//
//			subsubformData.first().setAttribute('text', System.currentTimeMillis() + ' write as user ' + user.username)
//		}

		stateless:
		{
			List<EntityObject<Long>> subsubformData = user.loadDependentsRecursively(
					parent,
					TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORMSTATUSMODEL,
					RecursiveDependency.forRoot(parent.id as String).dependency(
							TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL.fqn + '_parent',
							subformData.first().id as String,
					).dependency(
							TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORMSTATELESS.fqn + '_subform',
							subformData.first().id as String
					)
			)

			subformData.first().dependents.put(
					TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORMSTATELESS.fqn + '_subform',
					subsubformData
			)

			subsubformData.first().setAttribute('text', System.currentTimeMillis() + ' write as user ' + user.loginParams.username)
		}

		// TODO: Should be flagged for UPDATE automatically when dependents are modified
		subformData.first().flag = Flag.UPDATE
		parent.flag = Flag.UPDATE

		user.save(parent)
	}
}


