package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.ListOfValues
import org.nuclos.test.webclient.pageobjects.StateComponent
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.pageobjects.viewconfiguration.SideviewConfiguration
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SubformLayoutrulesTest extends AbstractWebclientTest {
	String customerBoMetaId = TestEntities.EXAMPLE_REST_CUSTOMER.fqn
	String categoryBoMetaId = TestEntities.EXAMPLE_REST_CATEGORY.fqn
	String articleBoMetaId = TestEntities.EXAMPLE_REST_ARTICLE.fqn
	String orderBoMetaId = TestEntities.EXAMPLE_REST_ORDER.fqn
	String orderPositionBoMetaId = TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order'

	Map customer = [customerNumber: 123, name: 'Customer 123']
	def customerId

	Map lebensmittel = [name: 'Lebensmittel']
	def lebensmittelId

	Map eisenwaren = [name: 'Eisenwaren']
	def eisenwarenId

	def orderId
	Map order2 = [orderNumber: '12346', customer: customer.name]

	Map activeArticle = [articleNumber: '2839842', name: 'Test article active', price: 666.66, active: true]
	Map inactiveArticle = [articleNumber: '982323', name: 'Test article inactive', price: 123.45, active: false]


	@Test
	void _01createBos() {

		customerId = RESTHelper.createBo(customerBoMetaId, customer, nuclosSession)['boId']

		lebensmittelId = RESTHelper.createBo(categoryBoMetaId, lebensmittel, nuclosSession)['boId']
		eisenwarenId = RESTHelper.createBo(categoryBoMetaId, eisenwaren, nuclosSession)['boId']

		RESTHelper.createBo(articleBoMetaId, activeArticle, nuclosSession)
		RESTHelper.createBo(articleBoMetaId, inactiveArticle, nuclosSession)

		Map milch = [articleNumber: '4711', name: 'Milch', price: 1.39, active: true, category: [id: lebensmittelId]]
		RESTHelper.createBo(articleBoMetaId, milch, nuclosSession)

		Map nagel = [articleNumber: '348', name: 'Nagel', price: 0.19, active: true, category: [id: eisenwarenId]]
		RESTHelper.createBo(articleBoMetaId, nagel, nuclosSession)

		Map hammer = [articleNumber: '351', name: 'Hammer', price: 14.99, active: true, category: [id: eisenwarenId]]
		RESTHelper.createBo(articleBoMetaId, hammer, nuclosSession)

		Map order = [orderNumber: '12345', customer: [id: customerId]]
		RESTHelper.createBo(orderBoMetaId, order, nuclosSession)
	}

	@Test
	void _02testLayoutRules() {

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		screenshot('after-save')
		Subform subform = eo.getSubform(orderPositionBoMetaId)
		subform.newRow()
		screenshot('after-new-row')

		assert subform.rowCount == 1

		Subform.Row row = subform.getRow(0)

		ListOfValues articleLOV = row.getLOV('article')

		articleLOV.open()
		assert articleLOV.choices.size() == 4 //Three Elements without Empty
		articleLOV.selectEntry('2839842 Test article active')

		assert subform.rowCount == 1


		String price = row.getValue('price')
		assert price == '666.66' || price == '666,66'

		String quantity = row.getValue('quantity')
		assert quantity == '1.00' || quantity == '1,00'

		int nrChoices = row.enterValue('category', 'Eisenwaren')
		assert nrChoices == 3 //Two Elements with Empty

		waitForAngularRequestsToFinish()

		//Reset
		assert !row.getValue('article')

		nrChoices = row.enterValue('article', '348 Nagel')
		assert nrChoices == 2 //Two Elements without Empty

		price = row.getValue('price')
		assert price == '0.19' || price == '0,19'

		// Change category and thus change the VLP

		nrChoices = row.enterValue('category', 'Lebensmittel')
		assert nrChoices == 3 //Two Elements with Empty

		waitForAngularRequestsToFinish()

		//Reset
		assert !row.getValue('article')

		nrChoices = row.enterValue('article', '4711 Milch')
		assert nrChoices == 1 //Only one Element without Empty

		price = row.getValue('price')
		assert price == '1.39' || price == '1,39'

		eo.save()

		// If there is no change, don't set dirty

		row = subform.getRow(0)

		quantity = row.getValue('quantity')
		assert quantity == '1.00' || quantity == '1,00'

		row.enterValue('quantity', '1')

		assert !row.isDirty()
		// assert EntityObject.getSaveButton() == null TODO NUCLOS-5744

	}

	@Test
	void _03_testLayoutRulesAfterCloning() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()
		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		subform.selectAllRows()
		subform.cloneSelectedRows()

		def newRow = subform.getRow(0)
		assert newRow.isNew()

		newRow.enterValue('category', 'Eisenwaren')
		assert newRow.getValue('article').equals('')

		newRow.clickCell('article')
		assert newRow.getLOV('article').choices.stream().filter { item -> item.toString().indexOf('Hammer') != -1 }.count() == 1

		eo.cancel()
	}

	@Test
	void _04_notCloneableColumn() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()
		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		Subform.Row row = subform.getRow(0)

		subform.openViewConfiguration()
		SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_createdAt')
		SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_testNichtKlonen')
		SideviewConfiguration.clickButtonOk()

		row.setChecked('shipped', true)

		eo.save()

		assert row.getValue('shipped', Boolean)

		subform.selectAllRows()
		subform.cloneSelectedRows()

		row = subform.getRow(0)
		assert row.new

		// Attribute "shipped" is not cloneable via layout configuration
		assert !row.getValue('shipped', Boolean)

		// System attributes should not be cloned
		assert row.getValue('createdAt', String).length() == 0

		// test invisible not cloneable attribute
		row.setChecked('testNichtKlonen', true)
		// set value to invisible attribute via update rule
		eo.save()
		subform.selectAllRows()
		subform.cloneSelectedRows()
		eo.save()
		assertNoMessageModel()
	}

	String goDocumentBoMetaId = 'org_nuclos_system_Generalsearchdocument_genericObject'
	String tabGoDocument = 'Documents'
	String tabOrderPosition = 'Position'

	@Test
	void _06testSubformRights() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		assert eo.getSubform(orderPositionBoMetaId).newVisible

		if (false) { //FIXME Tabs are not disappearing until now
			assert !eo.hasTab(tabGoDocument)
		}

		StateComponent.changeStateByNumeral(90)
		StateComponent.confirmStateChange()

		waitForAngularRequestsToFinish()

		assert !eo.getSubform(orderPositionBoMetaId).newVisible

		if (false) { //FIXME Tabs are not disappearing until now
			assert !eo.hasTab(tabGoDocument)
		}

		StateComponent.changeStateByNumeral(95)
		StateComponent.confirmStateChange()

		waitForAngularRequestsToFinish()

		Subform subform = eo.getSubform(orderPositionBoMetaId)
		assert subform.newVisible

		assert eo.hasTab(tabGoDocument)

	}

	@Test
	void _07testTabChangeBehavior() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		//Step A: Enter some value in the subrow

		Subform subform = eo.getSubform(orderPositionBoMetaId)
		Subform.Row row = subform.getRow(0)

		assert !row.isDirty()
		String quantity = row.getValue('quantity')
		assert quantity == '1.00' || quantity == '1,00'

		row.enterValue('quantity', '5')

		screenshot('changedQuantity')

		assert row.isDirty()
		quantity = row.getValue('quantity')
		assert quantity == '5.00' || quantity == '5,00'

		//Step B: Change to another tab and back

		eo.getTab(tabGoDocument).click()
		assert eo.getSubform(goDocumentBoMetaId) != null

		eo.getTab(tabOrderPosition).click()

		subform = eo.getSubform(orderPositionBoMetaId)
		row = subform.getRow(0)

		assert row.isDirty()
		quantity = row.getValue('quantity')
		assert quantity == '5.00' || quantity == '5,00'

		//Step C: Change to another tab and save there

		eo.getTab(tabGoDocument).click()
		assert eo.getSubform(goDocumentBoMetaId) != null

		eo.save()

		screenshot('afterSave')

		eo.getTab(tabOrderPosition).click()

		screenshot('firstTabAfterSave')

		subform = eo.getSubform(orderPositionBoMetaId)
		row = subform.getRow(0)

		assert !row.isDirty()
		quantity = row.getValue('quantity')
		assert quantity == '5.00' || quantity == '5,00'

		//Step D: Enter again another value then change to another tab and cancel there

		row.enterValue('quantity', '7')

		screenshot('changedQuantity2')

		assert row.isDirty()
		quantity = row.getValue('quantity')
		assert quantity == '7.00' || quantity == '7,00'

		eo.getTab(tabGoDocument).click()
		assert eo.getSubform(goDocumentBoMetaId) != null

		eo.cancel()

		screenshot('afterCancel')

		eo.getTab(tabOrderPosition).click()

		screenshot('firstTabAfterCancel')

		subform = eo.getSubform(orderPositionBoMetaId)
		row = subform.getRow(0)

		assert !row.isDirty()
		quantity = row.getValue('quantity')
		assert quantity == '5.00' || quantity == '5,00'

	}

	@Ignore
	@Test
	void _08checkSubformRowCountDisplayInTab() {
		EntityObjectComponent.createEO(TestEntities.EXAMPLE_REST_ORDER, order2)

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)

		screenshot('after-open-customer')

		WebElement we = eo.getTab('tab_' + orderBoMetaId + '_customer')

		assert we != null
		assert we.getText() == 'Orders (2)'

	}

}
