package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.ListOfValues
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.subform.Subform

import groovy.transform.CompileStatic

/**
 * TODO: Don't use the Selenium driver directly, use the integration test API instead
 * TODO: Don't ever use generated IDs
 * TODO: Don't use "sleep(...)"
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SubformLovTest extends AbstractWebclientTest {

	static String cat1Name = 'cat1'
	static String cat2Name = 'cat2'
	static Map cat1 = [name: cat1Name]
	static Map cat2 = [name: cat2Name]
	static String art1Label = '1 One'
	static String art2Label = '2 Two'

	@Test
	void _00_setup() {
		cat1.id = RESTHelper.createBo([
				boMetaId  : TestEntities.EXAMPLE_REST_CATEGORY.fqn,
				attributes: cat1
		], nuclosSession)['boId']
		cat2.id = RESTHelper.createBo([
				boMetaId  : TestEntities.EXAMPLE_REST_CATEGORY.fqn,
				attributes: cat2
		], nuclosSession)['boId']
		RESTHelper.createBo([
				boMetaId  : TestEntities.EXAMPLE_REST_ARTICLE.fqn,
				attributes: [category: [id: cat1.id],
				             articleNumber: 1,
							 name: 'One',
							 price: 1]
		], nuclosSession)
		RESTHelper.createBo([
				boMetaId  : TestEntities.EXAMPLE_REST_ARTICLE.fqn,
				attributes: [category: [id: cat2.id],
				             articleNumber: 2,
				             name: 'Two',
				             price: 2]
		], nuclosSession)
	}

	@Test
	void _01_testLovAddReferenceWithOpenDropdown() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		eo.addNew()
		eo.setAttribute("orderNumber", 1)
		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')

		Subform.Row subformRow = subform.newRow()

		// We need to test the overlay add reference button in the visible dropdown (NUCLOS-7115)
		ListOfValues lov = subformRow.getLOV('article')
		lov.lov.mouseover()
		sleep(2000)
		$('nuc-add-reference-target a').click()

		assert driver.getWindowHandles().size() == 2

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		assert oldWindow != newWindow

		// create a new article in new window
		EntityObjectComponent article = EntityObjectComponent.forDetail()
		article.setAttribute('articleNumber', 2000)
		article.setAttribute('price', 2000)
		article.setAttribute('name', 'Test-Article')
		article.save()

		driver.switchTo().window(oldWindow)
		resizeBrowserWindow()
		waitForAngularRequestsToFinish()

		eo = EntityObjectComponent.forDetail()
		eo.save()

		subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		subformRow = subform.getRow(0);

		assert subformRow.getValue('article') == '2000 Test-Article'
	}

	@Test
	void _02_testLovMagnifyingWithOpenDropdown() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		eo.addNew()
		eo.setAttribute("orderNumber", 2)
		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')

		Subform.Row subformRow = subform.newRow()

		// We need to test the magnifier function (search in new window with VLP) (NUCLOS-5330)
		ListOfValues lov = subformRow.getLOV('article')
		lov.lov.mouseover()
		sleep(2000)
		$('nuc-search-reference-target a').click()

		assert driver.getWindowHandles().size() == 2

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		assert oldWindow != newWindow
		assert Sidebar.listEntryCount == 3

		switchToOtherWindow()
		closeOtherWindows()

		subformRow.enterValue('category', cat1Name)
		subformRow.clickCell('article')

		lov = subformRow.getLOV('article')
		lov.lov.mouseover()
		sleep(2000)
		$('nuc-search-reference-target a').click()

		assert driver.getWindowHandles().size() == 2

		oldWindow = driver.windowHandle
		newWindow = switchToOtherWindow()

		assert oldWindow != newWindow
		assert Sidebar.listEntryCount == 1

	}

	@Test
	void _03_testLovAfterCloning() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		// TODO fix already existing error which causes NUCLOS-7599 to fail
		refresh()

		eo.addNew()
		eo.setAttribute("orderNumber", 3)
		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')

		Subform.Row subformRow = subform.newRow()
		subformRow.enterValue('category', cat1Name)
		subformRow.clickCell('article')
		ListOfValues lov = subformRow.getLOV('article')
		assert lov.open
		assert lov.choices == [art1Label]
		lov.selectEntry(art1Label)

		subformRow = subform.newRow()
		subformRow.enterValue('category', cat2Name)
		subformRow.clickCell('article')
		lov = subformRow.getLOV('article')
		assert lov.open
		assert lov.choices == [art2Label]
		lov.selectEntry(art2Label)

		subform.toggleSelection(1)
		subform.cloneSelectedRows()

		subformRow = subform.getRow(0)
		subformRow.clickCell('article')
		lov = subformRow.getLOV('article')
		assert lov.open
		assert lov.choices == [art1Label]

		eo.save()

		assert subform.getRowCount() == 3
	}

}
