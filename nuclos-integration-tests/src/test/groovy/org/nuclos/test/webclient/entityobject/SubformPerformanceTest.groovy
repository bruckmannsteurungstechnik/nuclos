package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.rest.request.RequestType
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.subform.Subform

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SubformPerformanceTest extends AbstractWebclientTest {

	@Test
	void _00_setup() {
		TestDataHelper.insertTestData(nuclosSession)

	}

	@Test
	void _05_insertSubformEntries() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_SUBFORM_PARENT)

		eo.addNew()
		eo.setAttribute('text', 'Test')

		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent')

		def deltas = []
		10.times {
			countBrowserRequests {
				long startRow = System.currentTimeMillis()
				subform.newRow()
				long deltaRow = System.currentTimeMillis() - startRow
				deltas << deltaRow
			}.with {
				assert it.getRequestCount(RequestType.EO_GENERATION_WITH_DEFAULTS) == 1

				// There might be an additional request for Meta-Data on first insert
				assert it.getRequestCount(RequestType.ALL) <= 2
			}
		}
		println "Row deltas:"
		deltas.each {
			println "$it ms"
		}
		println "Total time: ${deltas.sum()} ms"

		def average = (BigDecimal) deltas.sum() / deltas.size()
		println "Average: $average"

		// Adding a new row should on average not take more than 1 second locally / 2 seconds on Jenkins
		assert average < 2000

		eo.save()

		assert subform.rowCount == 10
	}

	@Test
	void _10_cloneSubformEntries() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent')

		subform.selectAllRows()

		def deltas = []
		9.times {
			countBrowserRequests {
				long startRow = System.currentTimeMillis()
				subform.cloneSelectedRows()
				long deltaRow = System.currentTimeMillis() - startRow
				deltas << deltaRow
			}.with {
				assert it.getRequestCount(RequestType.EO_GENERATION_WITH_DEFAULTS) == 1
				assert it.getRequestCount(RequestType.ALL) == 1
			}
		}
		println "Row deltas:"
		deltas.each {
			println "$it ms"
		}
		println "Total time: ${deltas.sum()} ms"

		def average = (BigDecimal) deltas.sum() / deltas.size()
		println "Average: $average"

		// Cloning 10 rows should on average not take more than 2 seconds locally / 4 seconds on Jenkins
		assert average < 4000

		eo.save()

		assert subform.rowCount > 10
	}

}
