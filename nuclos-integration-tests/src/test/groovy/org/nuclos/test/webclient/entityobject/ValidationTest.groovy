package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.Dialog
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.StateComponent
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.validation.ValidationStatus
import org.openqa.selenium.Keys

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ValidationTest extends AbstractWebclientTest {

	@Test()
	void _00_setup() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTVALIDATION)
		eo.addNew()
	}

	@Test
	void _04_testInitiallyValid() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		// Booleans are initially set to false (not null)
		assert eo.getValidationStatus('requiredboolean') == ValidationStatus.VALID
	}

	@Test
	void _05_testInitiallyMissing() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		assert eo.getValidationStatus('requiredtext') == ValidationStatus.MISSING
		assert eo.getValidationStatus('requireddate') == ValidationStatus.MISSING
		assert eo.getValidationStatus('requiredemail') == ValidationStatus.MISSING
		assert eo.getValidationStatus('requiredinteger') == ValidationStatus.MISSING
		assert eo.getValidationStatus('requireddouble92') == ValidationStatus.MISSING
		assert eo.getValidationStatus('requiredmemo') == ValidationStatus.MISSING
		assert eo.getValidationStatus('requiredreference') == ValidationStatus.MISSING
		assert eo.getValidationStatus('123aeoeuess') == ValidationStatus.MISSING
	}

	@Test
	void _06_testInitiallyValid() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.getValidationStatus('integerminmax') == ValidationStatus.VALID
		eo.getValidationStatus('dateminmax') == ValidationStatus.VALID
	}

	@Test
	void _07_trySave() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.setAttribute('integerminmax', '0')
		eo.save()

		assert Dialog.close()
	}

	@Test
	void _08_testSaveInvalidInput() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.setAttribute('integerminmax', '2') // Min: 5, Max: 2000000000
		eo.setAttribute('dateminmax', '2000-01-01') // Min: 2017-01-01, Max: 2099-12-31
		eo.save()

		assert Dialog.close()

		assert eo.getValidationStatus('integerminmax') == ValidationStatus.INVALID
		assert eo.getValidationStatus('dateminmax') == ValidationStatus.INVALID
	}

	@Test
	void _09_testSaveValidInput() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.setAttribute('integerminmax', '5') // Min: 5, Max: 2000000000
		eo.setAttribute('dateminmax', '2017-01-01') // Min: 2017-01-01, Max: 2099-12-31
		eo.save()

		assert Dialog.close()

		assert eo.getValidationStatus('integerminmax') == ValidationStatus.VALID
		assert eo.getValidationStatus('dateminmax') == ValidationStatus.VALID

	}

	@Test
	void _10_fillRequiredFields() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.setAttribute('requiredtext', 'text')
		eo.setAttribute('requireddate', new Date())
		eo.setAttribute('requiredemail', 'test@example.com')
		eo.setAttribute('requiredinteger', 123)
		eo.setAttribute('requireddouble92', 1.23)
		eo.setAttribute('requiredboolean', true)
		eo.setAttribute('requiredmemo', 'text')
		eo.setAttribute('requiredreference', 'nuclos')
		eo.setAttribute('123aeoeuess', 'text')
	}

	@Test
	void _15_testAllValid() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		assert eo.getValidationStatus('requiredtext') == ValidationStatus.VALID
		assert eo.getValidationStatus('requireddate') == ValidationStatus.VALID
		assert eo.getValidationStatus('requiredemail') == ValidationStatus.VALID
		assert eo.getValidationStatus('requiredinteger') == ValidationStatus.VALID
		assert eo.getValidationStatus('requireddouble92') == ValidationStatus.VALID
		assert eo.getValidationStatus('requiredboolean') == ValidationStatus.VALID
		assert eo.getValidationStatus('requiredmemo') == ValidationStatus.VALID
		assert eo.getValidationStatus('requiredreference') == ValidationStatus.VALID
		assert eo.getValidationStatus('123aeoeuess') == ValidationStatus.VALID
	}

	@Test
	void _20_testInvalidEmail() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.setAttribute('requiredemail', 'a')
		assert eo.getValidationStatus('requiredemail') == ValidationStatus.INVALID

		eo.setAttribute('requiredemail', 'a@a.a')
		assert eo.getValidationStatus('requiredemail') == ValidationStatus.INVALID

		eo.setAttribute('requiredemail', 'test@nuclos.de@')
		assert eo.getValidationStatus('requiredemail') == ValidationStatus.INVALID
	}

	@Test
	void _22_testValidEmail() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.setAttribute('requiredemail', 'test@nuclos.de')
		assert eo.getValidationStatus('requiredemail') == ValidationStatus.VALID
	}

	@Test
	void _24_testInvalidInteger() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.setAttribute('requiredinteger', 'a')
		assert eo.getValidationStatus('requiredinteger') == ValidationStatus.INVALID

		eo.setAttribute('requiredinteger', 'a1')
		assert eo.getValidationStatus('requiredinteger') == ValidationStatus.INVALID
	}

	@Test
	void _26_testValidInteger() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.setAttribute('requiredinteger', '0')
		assert eo.getValidationStatus('requiredinteger') == ValidationStatus.VALID
		assert eo.getAttribute('requiredinteger') == '0'

		eo.setAttribute('requiredinteger', '1')
		assert eo.getValidationStatus('requiredinteger') == ValidationStatus.VALID
		assert eo.getAttribute('requiredinteger') == '1'


		eo.setAttribute('requiredinteger', AbstractWebclientTest.context.locale == Locale.ENGLISH ? '.5' : ',5')
		assert eo.getValidationStatus('requiredinteger') == ValidationStatus.VALID
		assert eo.getAttribute('requiredinteger') == '1'

		eo.setAttribute('requiredinteger', '-1')
		assert eo.getValidationStatus('requiredinteger') == ValidationStatus.VALID
		assert eo.getAttribute('requiredinteger') == '-1'

		eo.setAttribute('requiredinteger', '1test')
		assert eo.getValidationStatus('requiredinteger') == ValidationStatus.VALID
		assert eo.getAttribute('requiredinteger') == '1'

		eo.setAttribute('requiredinteger', '1e10')
		assert eo.getValidationStatus('requiredinteger') == ValidationStatus.VALID
		assert eo.getAttribute('requiredinteger') == '10000000000'

		eo.setAttribute('requiredinteger', AbstractWebclientTest.context.locale == Locale.ENGLISH ? '1.2345' : '1,2345')
		assert eo.getValidationStatus('requiredinteger') == ValidationStatus.VALID
		assert eo.getAttribute('requiredinteger') == '1'
	}

	@Test
	void _28_testInvalidDate() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.enterText('requireddate', 'a')
		assert eo.getValidationStatus('requireddate') == ValidationStatus.INVALID

		eo.enterText('requireddate', '')
		assert eo.getValidationStatus('requireddate') == ValidationStatus.MISSING
	}

	@Test
	void _30_testValidDate() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.setAttribute('requireddate', '2017-03-04')
		assert eo.getValidationStatus('requireddate') == ValidationStatus.VALID
	}

	@Test
	void _50_testSave() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.save()

		assert !eo.dirty
	}

	@Test
	void _60_testRequiredFieldForStateChange() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()
		Sidebar.refresh()

		eo.open(TestEntities.EXAMPLE_REST_ORDER)
		eo.addNew()
		eo.setAttribute('orderNumber', 123)
		eo.save()

		StateComponent.changeStateByNumeral(80)
		StateComponent.confirmStateChange()
		eo.clickButtonOk()
		assert eo.getValidationStatus('customer') == ValidationStatus.MISSING
	}

	@Test
	void _65_testSubformValidation() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order')
		Subform.Row row = subform.newRow()

		// Stop editing
		sendKeys(Keys.ESCAPE)

		assert row.getValidationStatus('article') == ValidationStatus.MISSING
		assert row.getValidationStatus('price') == ValidationStatus.MISSING
		assert [ValidationStatus.VALID, null].contains(row.getValidationStatus('quantity'))

		// row.enterValue('price', 'asdf' + Keys.TAB) // css class is added after blur - tab out of cell
		def priceField = row.getFieldElement('price')
		priceField.click()
		sendKeys('asdf' + Keys.TAB)

		assert row.getValidationStatus('price') == ValidationStatus.INVALID

		row.enterValue('price', '123')
		assert [ValidationStatus.VALID, null].contains(row.getValidationStatus('price'))

	}
}
