package org.nuclos.test.webclient.menu

import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.AbstractPageObject
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
/**
 * tests if left menu toggle state is saved to the workspace
 *
 */
@CompileStatic
@Ignore('TODO: Adjust for Webclient2!')
class ToggleMenuTest extends AbstractWebclientTest {

	WebElement findElementContainingText(String cssSelector, String text) {
		return $$(cssSelector).find { it.text == text }
	}
	
	WebElement getMenuItem(String menuName) {
		waitForAngularRequestsToFinish()
		return findElementContainingText('#dashboard-menu .menu-item', menuName)
	}
	
	WebElement getExampleMenuItem() {
		waitForAngularRequestsToFinish()
		return findElementContainingText('#dashboard-menu .menu-path', 'Example')
	}
	
	@Test
	void _01test() {
		assert $('#logout')
		openStartpage()
		
		screenshot('togglemenuitem-before')
		
		
		
		// toggle menu
		
		boolean isExampleMenuExpanded = getMenuItem('Article') != null

		// TODO: Menut item assertions after refresh always fail in PhantomJS

		if (isExampleMenuExpanded) {
			getExampleMenuItem().click()
			screenshot('togglemenuitem-after-first-toggle')
			assert getMenuItem('Article') == null
			AbstractPageObject.refresh()
			screenshot('togglemenuitem-after-first-toggle-reload')
			// TODO
//			assert getMenuItem('Article') == null
		} else {
			getExampleMenuItem().click()
			screenshot('togglemenuitem-after-first-toggle')
			assert getMenuItem('Article') != null
			AbstractPageObject.refresh()
			screenshot('togglemenuitem-after-first-toggle-reload')
			// TODO
//			assert getMenuItem('Article') != null
		}
		

		// toggle menu again
		
		isExampleMenuExpanded = getMenuItem('Article') != null
		
		if (isExampleMenuExpanded) {
			getExampleMenuItem().click()
			screenshot('togglemenuitem-after-second-toggle')
			assert getMenuItem('Article') == null
			AbstractPageObject.refresh()
			screenshot('togglemenuitem-after-second-toggle-reload')
			// TODO
//			assert getMenuItem('Article') == null
		} else {
			getExampleMenuItem().click()
			screenshot('togglemenuitem-after-second-toggle')
			assert getMenuItem('Article') != null
			AbstractPageObject.refresh()
			screenshot('togglemenuitem-after-second-toggle-reload')
			// TODO
//			assert getMenuItem('Article') != null
		}
	}

}
