package org.nuclos.test.webclient.preference

import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.request.RequestType
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.MenuComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.preference.PreferenceItem
import org.nuclos.test.webclient.pageobjects.preference.PreferenceType
import org.nuclos.test.webclient.pageobjects.preference.Preferences
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.pageobjects.viewconfiguration.SideviewConfiguration
import groovy.transform.CompileStatic
import net.lightbody.bmp.core.har.HarEntry
import net.lightbody.bmp.proxy.CaptureType

/**
 * create, share, customize webclient preferences
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class PreferencesTest extends AbstractWebclientTest {

	String[] prefColumns1 = ['example_rest_Order_orderNumber', 'example_rest_Order_customer', 'example_rest_Order_note']
	String[] prefColumns1Names = ['Order number', 'Customer', 'Note']
	String[] prefColumns2 = ['example_rest_Order_note']
	String[] prefColumns2Names = ['Note']

	@Test
	void _00createTestUser() {
		RESTHelper.createUser('preferencetest', 'preferencetest', ['Example user'], nuclosSession)
	}

	@Test @Ignore
	void _01_openEoWithFlawedSubformPreference() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_SUBFORM_PARENT)
		getBrowserHar([CaptureType.RESPONSE_CONTENT]) {
			eo.addNew()
		}.with {
			HarEntry entry = it.log.entries.find {
				it.request.url.contains('/rest/preferences?boMetaId=nuclet_test_subform_Subform&')
			}

			// Check that the flawed preference was really loaded
			assert entry.response.content.text.contains('untranslateduid123')
		}

		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM.fqn + '_parent')
		assert subform.columnHeaders == ['Text']

		subform.newRow()
		subform.newRow()

		assert subform.rowCount == 2

		eo.cancel()
	}

	@Test
	void _02createPref() {
		// TODO: Make the Test login with the correct user directly
		logout()
		login('preferencetest', 'preferencetest', false)
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		// TODO could be less requests
		countBrowserRequests {
			SideviewConfiguration.newSideviewConfiguration('Sideviewconfig1')
		}.with {
			/// TODO fails on test server
			/// assert it.getCount(RequestCounts.REQUEST_TYPE.PREFERENCE_LOAD) == 1
			/// assert it.getCount(RequestCounts.REQUEST_TYPE.PREFERENCE_SAVE) == 1
		}
		countBrowserRequests {
			prefColumns1.each {
				SideviewConfiguration.selectColumn(it.toString())
			}
		}.with {
			/// assert it.getCount(RequestCounts.REQUEST_TYPE.PREFERENCE_LOAD) == 0
			/// assert it.getCount(RequestCounts.REQUEST_TYPE.PREFERENCE_SAVE) == 0
		}
		countBrowserRequests {
			SideviewConfiguration.saveSideviewConfiguration()
		}.with {
			/// assert it.getCount(RequestCounts.REQUEST_TYPE.PREFERENCE_LOAD) == 0
			/// assert it.getCount(RequestCounts.REQUEST_TYPE.PREFERENCE_SAVE) == 1
		}
		countBrowserRequests {
			SideviewConfiguration.close()
		}.with {
			/// assert it.getCount(RequestCounts.REQUEST_TYPE.PREFERENCE_LOAD) == 3
			/// assert it.getCount(RequestCounts.REQUEST_TYPE.PREFERENCE_SAVE) == 0
		}
	}

	@Test
	void _03sharePref() {

		Sidebar.resizeSidebarComponent(500)

		assert Sidebar.getColumnHeaders().equals(['Order number', 'Customer', 'Note'])

		Preferences.open()

		Preferences.filter('name', 'Sideviewconfig1')
		def pref = getFirstPreferenceItem()
		pref.select()
		assert !pref.shared
		pref = getFirstPreferenceItem()
		Preferences.shareItem(PreferenceType.TABLE, pref.name, 'Example user')
		assert getFirstPreferenceItem().shared

		assertSideviewConfigurationWithOtherUser(prefColumns1Names)

	}

	private void assertSideviewConfigurationWithOtherUser(String[] selectedColumns) {
		logout()
		login('test', 'test', false) // user of "Example user" group

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		SideviewConfiguration.selectSideviewConfiguration('Sideviewconfig1')

		assert Sidebar.getColumnHeaders().equals(selectedColumns)


		logout()
		login('preferencetest', 'preferencetest', false)
	}

	@Test
	void _04customizePref() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		SideviewConfiguration.openColumnConfigurationPanel()
		SideviewConfiguration.selectSideviewConfigurationInModal('Sideviewconfig1')

		SideviewConfiguration.deselectAll()

		prefColumns2.each {
			SideviewConfiguration.selectColumn(it.toString())
		}

		SideviewConfiguration.close()


	}

	@Test
	void _05publishChanges() {
		Preferences.open()
		Preferences.filter('name', 'Sideviewconfig1')
		def pref = getFirstPreferenceItem()
		pref.select()
		pref = getFirstPreferenceItem()

		assert pref.customized
		pref.publishChanges()

		assert !getFirstPreferenceItem().customized

		assertSideviewConfigurationWithOtherUser(prefColumns2Names)
	}

	@Test
	void _06discardCustomizedPrefInSideview() {

		// customize again
		customizePref('Sideviewconfig1')

		assert Sidebar.selectedColumns().findAll() { !it.equals('Status') }.sort().equals(prefColumns1Names.sort())

		// discard changes
		SideviewConfiguration.openColumnConfigurationPanel()
		SideviewConfiguration.selectSideviewConfigurationInModal('Sideviewconfig1')
		SideviewConfiguration.discardChanges()
		assert !$$('#sideviewmenu-selector')[0].getAttribute('selectedIndex').equals('-1')
		SideviewConfiguration.clickButtonOk()

		assert Sidebar.selectedColumns().findAll() { !it.equals('Status') }.sort().equals(prefColumns2Names.sort())
	}

	@Test
	void _06discardCustomizedPrefInManagePrefView() {

		// customize again
		customizePref('Sideviewconfig1')

		// discard changes
		Preferences.open()
		Preferences.filter('name', 'Sideviewconfig1')
		def pref = getFirstPreferenceItem()
		pref.select()
		pref = getFirstPreferenceItem()
		assert pref.customized
		pref.discardChanges()

		assertSideviewConfigurationWithOtherUser(prefColumns2Names)
	}

	@Test
	void _07discardCustomizedPrefInUserMenu() {

		// customize again
		customizePref('Sideviewconfig1')

		// discard changes
		MenuComponent.toggleUserMenu()
		MenuComponent.clickOpenPreferencesResetModal()
		$('#button-reset-allprefs').click()

		assertSideviewConfigurationWithOtherUser(prefColumns2Names)

		// customize again
		customizePref('Sideviewconfig1')

		// discard changes
		discardPrefChanges()

		assertSideviewConfigurationWithOtherUser(prefColumns2Names)
	}

	@Test
	void _08unsharedPref() {
		// unshare pref to run the test as often as you like, without Nuclet import
		Preferences.open()
		Preferences.filter('name', 'Sideviewconfig1')
		def pref = getFirstPreferenceItem()
		pref.select()
		pref.unshareFrom('Example user')
	}

	@Test
	void _09testIfManagePreferencesAreAvailable() {
		MenuComponent.toggleUserMenu()
		// NUCLOS-6620 1A: User with SharePreference privilege are allowed to manage preferences
		assert MenuComponent.getOpenPreferencesManage()
		MenuComponent.toggleUserMenu()

		Preferences.open()
		assert Preferences.getPreferenceItems().size() > 0

		logout()
		RESTHelper.createUser('nopreference', 'nopreference', ['Example readonly'], nuclosSession)
		login('nopreference', 'nopreference', false)

		MenuComponent.toggleUserMenu()
		// NUCLOS-6620 1A: User without SharePreference privilege are not allowed to manage preferences
		assert !MenuComponent.getOpenPreferencesManage()
		MenuComponent.toggleUserMenu()

		Preferences.open()
		assert Preferences.getPreferenceItems().size() == 0

	}

	@Test
	void _10shareEntityLessPref() {
		logout()
		login('nuclos', '', false) // user of "Example user" group

		// create preference for dynamic task list (not yet supported by webclient)
		String input = '{"type": "tasklist-table", "name": "OrderDynTasks", "content": {' +
				'"taskListId": "YKY111j2BzkvqTL1iu58",' +
				'"columns": [' +
					'{' +
						'"name": "Ordernumber",' +
						'"column": "Ordernumber",' +
						'"width": 116,' +
						'"position": 0,' +
						'"sort": {' +
							'"direction": "asc",' +
							'"prio": 1' +
						'}' +
					'},' +
					'{' +
						'"name": "Ordersum",' +
						'"column": "Ordersum",' +
						'"width": 121,' +
						'"position": 1' +
					'}' +
				']' +
				'}}'
		RESTHelper.postJson(RESTHelper.REST_BASE_URL + '/preferences', input, nuclosSession)

		MenuComponent.toggleUserMenu()
		Preferences.open()
		Preferences.filter('name', 'OrderDynTasks')
		def pref = Preferences.preferenceItems.get(0)
		pref.select()
		assert !pref.shared
		pref.shareWith('Example user')
		pref = Preferences.preferenceItems.get(0)
		assert pref.shared
		pref.unshareFrom('Example user')
		pref = Preferences.preferenceItems.get(0)
		assert !pref.shared
	}

	@Test
	void _11_deletePreferences() {
		logout()
		login('preferencetest', 'preferencetest', false)

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		SideviewConfiguration.newSideviewConfiguration('Sideviewconfig2')
		prefColumns1.each {
			SideviewConfiguration.selectColumn(it.toString())
		}
		SideviewConfiguration.saveSideviewConfiguration()
		SideviewConfiguration.close()

		Preferences.open()

		Preferences.filter('name', 'Sideviewconfig2')
		def pref = getFirstPreferenceItem()
		pref.select()
		pref.doNotdelete()

		Preferences.filter('name', 'Sideviewconfig2')
		pref = getFirstPreferenceItem()
		assert pref != null

		pref.select()
		pref.delete()

		Preferences.filter('name', 'Sideviewconfig2')
		pref = getFirstPreferenceItem()
		assert pref == null
	}

	@Test
	void _12_restrictNumberOfRequestsInPreferencesManager() {
		Preferences.open()
		Preferences.filter("","")
		assert Preferences.getPreferenceItems().size() > 1

		countBrowserRequests {
			Preferences.getPreferenceItems()[1].select()
		}.with {
			assert it.getRequestCount(RequestType.PREFERENCE_READ_SHARE) == 1
		}

		duplicateCurrentWindow()
		_02createPref()

		switchToOtherWindow()
		Preferences.open()
		assert Preferences.getPreferenceItems().size() > 0

		countBrowserRequests {
			Preferences.preferenceItems.get(0).select()
		}.with {
			assert it.getRequestCount(RequestType.PREFERENCE_READ_SHARE) == 1
		}
	}

	@Test
	void _13_selectAllPreferencesViaButton() {
		logout()
		login('test', 'test', false) // user of "Example user" group

		Preferences.open()
		Preferences.filter("", "")

		assert Preferences.getSelectedPreferenceItems().isEmpty()

		// first button select all
		$$("#collapseSelection button")[0].click()

		assert !Preferences.getSelectedPreferenceItems().isEmpty()
	}

	@Test
	void _14_selectDeselectAllPreferencesViaCheckbox() {
		logout()
		login('test', 'test', false) // user of "Example user" group

		Preferences.open()
		Preferences.filter("", "")

		assert Preferences.getSelectedPreferenceItems().isEmpty()

		// first button select all
		$("#grid-container > div.card-block > ag-grid-angular > div > div.ag-root-wrapper-body.ag-layout-normal " +
				"> div > div.ag-header > div.ag-header-viewport > div > div:nth-child(1) > div:nth-child(1) > span > " +
				"span.ag-checkbox-unchecked > span").click()

		assert !Preferences.getSelectedPreferenceItems().isEmpty()
	}

	@Test
	void _15_deselectAllPreferencesViaButton() {
		logout()
		login('test', 'test', false) // user of "Example user" group

		Preferences.open()
		Preferences.filter("", "")

		// first button select all
		$$("#collapseSelection button")[0].click()

		assert !Preferences.getSelectedPreferenceItems().isEmpty()

		// third button deselect all
		$$("#collapseSelection button")[2].click()

		assert Preferences.getSelectedPreferenceItems().isEmpty()
	}

	@Test
	void _16_filterCustomizedViaButtonPreferencesViaButton() {
		logout()
		login('test', 'test', false) // user of "Example user" group

		customizePref('Default')

		Preferences.open()
		Preferences.filter("", "")

		assert Preferences.getSelectedPreferenceItems().isEmpty()

		// second button filter all customized
		$$("#collapseSelection button")[1].click()

		assert Preferences.getSelectedPreferenceItems().size() == 1

		discardPrefChanges()
	}

	@Test
	void _17_resetFilterCustomizedViaButtonPreferencesViaButton() {
		logout()
		login('test', 'test', false) // user of "Example user" group

		customizePref('Default')

		Preferences.open()
		Preferences.filter("", "")

		// second button filter all customized
		$$("#collapseSelection button")[1].click()

		assert Preferences.getSelectedPreferenceItems().size() == 1

		// fourth button reset all
		$$("#collapseSelection button")[3].click()

		assert Preferences.getSelectedPreferenceItems().isEmpty()
		assert Preferences.getPreferenceItems().size() > 1
	}

	private void customizePref(String name) {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		SideviewConfiguration.openColumnConfigurationPanel()
		SideviewConfiguration.selectSideviewConfigurationInModal(name)

		SideviewConfiguration.deselectAll()

		prefColumns1.each {
			SideviewConfiguration.selectColumn(it.toString())
		}
		SideviewConfiguration.close()
	}

	private void discardPrefChanges() {
		// discard changes
		MenuComponent.toggleUserMenu()
		MenuComponent.clickOpenPreferencesResetModal()
		$('#button-reset-prefs-for-current-entity-class').click()
	}

	private PreferenceItem getFirstPreferenceItem() {
		Preferences.getPreferenceItems()[0]
	}

}
