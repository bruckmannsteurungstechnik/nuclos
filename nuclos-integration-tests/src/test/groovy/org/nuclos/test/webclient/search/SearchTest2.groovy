package org.nuclos.test.webclient.search

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.rest.request.RequestType
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.Datepicker
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Searchtemplate
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.preference.PreferenceType
import org.nuclos.test.webclient.pageobjects.preference.Preferences
import org.nuclos.test.webclient.pageobjects.search.Searchbar
import org.openqa.selenium.Keys

import groovy.transform.CompileStatic

/**
 * TODO: Merge with org.nuclos.test.webclient.search.SearchTest
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SearchTest2 extends AbstractWebclientTest {
	static String filter1 = 'Filter 1'
	static String filter2 = 'Filter 2'
	static String username = 'SearchTest2'

	static final int COUNT_NUCLET_ORDER_SEARCHFILTER = 3
	static final String CUSTOMER_SEARCHFILTER = 'Customer'
	static final String ORDERPOSITION_SEARCHFILTER = 'Order position'
	static final String SYSTEMFIELDS_SEARCHFILTER = 'Systemfields'

	static final String USER_LOV2_TEST_SEARCHFILTER = 'User-LOV2-Test'

	static void createWord(String word) {
		EntityObject word1 = new EntityObject(TestEntities.EXAMPLE_REST_WORD)
		word1.setAttribute('text', word)
		word1.setAttribute('times', "1")
		nuclosSession.save(word1)
	}

	@Test
	void _00_setup() {
		RESTHelper.createUser(username, 'test', ['Example user'], nuclosSession)

		TestDataHelper.insertTestData(nuclosSession)
		createWord('word1')
		createWord('word2')
		createWord('word3')

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
	}

	@Test
	void _05_useSearchWithoutSearchfilter() {

		countBrowserRequests {
			Searchbar.openSearchEditor()
			Searchbar.clear()
			assert ['[My search]', '[Meine Suche]'].contains(Searchbar.selectedFilter)
		}.with {
			assert it.getRequestCount(RequestType.PREFERENCE_ALL) == 1
			assert it.getRequestCount(RequestType.EO_READ_LIST) == 0 // no searchfilter change -> no read
		}

		countBrowserRequests {
			Searchbar.selectAttribute('Note')
			Searchbar.setCondition(
					TestEntities.EXAMPLE_REST_ORDER,
					new Searchtemplate.SearchTemplateItem(
							name: 'note',
							operator: '=',
							value: 'A'
					)
			)
		}.with {
			assert it.getRequestCount(RequestType.PREFERENCE_ALL) == 4 // 2 changes + 2x GET+PUT
			assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
		}

		assert Sidebar.listEntries.size() == 2
	}

	@Test
	void _06_saveSearchAsSearchfilter() {
		countBrowserRequests {
			Searchbar.openSearchEditor()
			Searchbar.saveAsSearchfilter()
			Searchbar.editName(filter1)
			Searchbar.save()

			assert filter1 == Searchbar.selectedFilter
			assert Searchbar.filters.size() == COUNT_NUCLET_ORDER_SEARCHFILTER + 2
			assert ['[My search]', '[Meine Suche]'].contains(Searchbar.filters.get(0))
		}.with {
			assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
			assert it.getRequestCount(RequestType.PREFERENCE_SAVE) == 3
		}
	}

	@Test
	void _07_removeSearchItem() {
		Searchbar.removeAttribute(TestEntities.EXAMPLE_REST_ORDER.fqn + '_note')
		assert Sidebar.listEntries.size() == 6
	}

	@Test
	void _08_addOtherSearchCondition() {
		Searchbar.selectAttribute('Note')
		Searchbar.setCondition(
				TestEntities.EXAMPLE_REST_ORDER,
				new Searchtemplate.SearchTemplateItem(
						name: 'note',
						operator: '=',
						value: 'B'
				)
		)

		assert Sidebar.listEntries.size() == 4
	}

	@Test
	void _10_createSecondSearchfilter() {
		countBrowserRequests {
			Searchbar.openSearchEditor()
			Searchbar.clear()
			Searchbar.saveAsSearchfilter()

			assert ['Searchfilter 1', 'Suchfilter 1'].contains(Searchbar.selectedFilter)
			assert Searchbar.filters.size() == COUNT_NUCLET_ORDER_SEARCHFILTER + 3

			Searchbar.selectAttribute('Order number')
			Searchbar.setCondition(
				TestEntities.EXAMPLE_REST_ORDER,
				new Searchtemplate.SearchTemplateItem(
						name: 'orderNumber',
						operator: '<',
						value: '10020150'
				)
			)

			assert Sidebar.listEntries.size() == 3

			Searchbar.editName(filter2)
			Searchbar.save()
		}.with {
			assert it.getRequestCount(RequestType.EO_READ_LIST) == 3
		}
	}

	@Test
	void _15_clearSearch() {
		countBrowserRequests {
			Searchbar.openSearchEditor()
			Searchbar.clear()
			assert Sidebar.listEntries.size() == 6
		}.with {
			assert it.getRequestCount(RequestType.ALL) <= 3
			assert it.getRequestCount(RequestType.PREFERENCE_DESELECT) == 0
		}

		Searchbar.selectAttribute('Order number')
		def orderNumberInput = Searchbar.getValueString(TestEntities.EXAMPLE_REST_ORDER, 'orderNumber')
		orderNumberInput.setInput('10020144')
		waitForAngularRequestsToFinish()
		assert Sidebar.listEntries.size() == 1

		refresh()
		assert Sidebar.listEntries.size() == 1

		Searchbar.clear()
		assert Sidebar.listEntries.size() == 6

		refresh()
		assert Sidebar.listEntries.size() == 6
	}

	@Test
	void _20_selectFirstSearchfilter() {
		countBrowserRequests {
			Searchbar.selectSearchfilter(filter1)
		}.with {
			assert it.getRequestCount(RequestType.PREFERENCE_SELECT) == 1
		}
	}

	@Test
	void _21_reload() {
		countBrowserRequests {
			refresh()
		}.with {
			assert it.getRequestCount(RequestType.PREFERENCE_SELECT) == 0
		}

		assert Searchbar.selectedFilter == filter1
	}

	@Test
	void _22_setFavorite() {
		Searchbar.openSearchEditor()

		countBrowserRequests {
			Searchbar.setFavorite('child_care', 20)
			Searchbar.done()
			Searchbar.selectSearchfilter(filter2);
			Searchbar.setFavorite('people_outline', 10)
			Searchbar.done()
		}.with {
			assert it.getRequestCount(RequestType.PREFERENCE_ALL) == 3
			assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
		}
		countBrowserRequests {
			Searchbar.edit()
			Searchbar.save()
			Searchbar.selectSearchfilter(filter1);
			Searchbar.edit()
			Searchbar.save()
		}.with {
			assert it.getRequestCount(RequestType.PREFERENCE_SAVE) == 3
			assert it.getRequestCount(RequestType.PREFERENCE_SELECT) == 1
			assert it.getRequestCount(RequestType.PREFERENCE_READ) == 2
			assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
		}

		assert Searchbar.getFavorites() == ['Filter 2', 'Filter 1']
	}

	@Test
	void _23_selectFavorite() {
		Searchbar.openSearchEditor()
		Searchbar.clear()
		Searchbar.closeSearchEditor()

		countBrowserRequests {
			Searchbar.selectFavorite(filter1);
			assert Sidebar.listEntries.size() == 2
		}.with {
			assert it.getRequestCount(RequestType.PREFERENCE_SELECT) == 1
			assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
			assert it.getRequestCount(RequestType.ALL) == 2
		}
		countBrowserRequests {
			Searchbar.selectFavorite(filter2);
			assert Sidebar.listEntries.size() == 3
		}.with {
			assert it.getRequestCount(RequestType.PREFERENCE_SELECT) == 1
			assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
			assert it.getRequestCount(RequestType.ALL) == 2
		}
		// re-select -> un-select
		countBrowserRequests {
			Searchbar.selectFavorite(filter2);
			assert Sidebar.listEntries.size() == 6
		}.with {
			assert it.getRequestCount(RequestType.PREFERENCE_SELECT) == 1
			assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
			assert it.getRequestCount(RequestType.ALL) == 2
		}
	}

	@Test
	void _24_editItem() {
		Searchbar.openSearchEditor()
		Searchbar.selectSearchfilter(filter2)

		countBrowserRequests {
			Searchbar.setCondition(
					TestEntities.EXAMPLE_REST_ORDER,
					new Searchtemplate.SearchTemplateItem(
							name: 'orderNumber',
							operator: '=',
							value: '10020150'
					)
			)

			assert Sidebar.listEntries.size() == 1
		}.with {
			assert it.getRequestCount(RequestType.PREFERENCE_READ) == 2
			assert it.getRequestCount(RequestType.PREFERENCE_READ) ==
				   it.getRequestCount(RequestType.PREFERENCE_ALL)
			assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
		}
	}

	@Test
	void _25_toggleItem() {
		Searchbar.openSearchEditor()

		countBrowserRequests {
			Searchbar.toggleItemSelection(TestEntities.EXAMPLE_REST_ORDER, 'orderNumber')

			assert Sidebar.listEntries.size() == 6
		}.with {
			assert it.getRequestCount(RequestType.PREFERENCE_READ) == 0
			assert it.getRequestCount(RequestType.PREFERENCE_ALL) == 0
			assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
		}
	}

	@Test
	void _26_undoChanges() {
		Searchbar.openSearchEditor()

		countBrowserRequests {
			Searchbar.edit()
			Searchbar.undo()

			assert Sidebar.listEntries.size() == 3
		}.with {
			assert it.getRequestCount(RequestType.PREFERENCE_SELECT) <= 1
			assert it.getRequestCount(RequestType.PREFERENCE_SAVE) <= 1
			assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
		}
	}

	@Test
	void _27_shareSearchfilter() {
		Preferences.open()
		Preferences.shareItem(PreferenceType.SEARCHTEMPLATE, filter1, 'Example user')
		this.afterPreferencesShare()
	}

	@Test
	void _28_checkSharedOptions() {
		Searchbar.selectFavorite(filter1)
		Searchbar.openSearchEditor()
		Searchbar.edit()
		assert !Searchbar.isDeleteAllowed()
	}

	@Test
	void _29_customize() {
		Searchbar.done()
		Searchbar.toggleItemSelection(TestEntities.EXAMPLE_REST_ORDER, 'note')
		assert Sidebar.listEntries.size() == 6

		Searchbar.edit()
		Searchbar.save()
		refresh()
		assert Searchbar.selectedFilter == filter1
		assert Sidebar.listEntries.size() == 6
	}

	@Test
	void _30_resetCustomization() {
		Searchbar.openSearchEditor()
		Searchbar.edit()
		Searchbar.resetCustomization()
		assert Sidebar.listEntries.size() == 2
	}

	@Test
	void _34_unshareAndDeleteSearchfilter() {
		Preferences.open()
		Preferences.unshareItem(PreferenceType.SEARCHTEMPLATE, filter1, 'Example user')
		this.afterPreferencesShare()

		countBrowserRequests {
			Searchbar.openSearchEditor()
			Searchbar.delete()
		}.with {
			assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
			assert it.getRequestCount(RequestType.PREFERENCE_DELETE) == 1
		}
	}

	@Test
	void _35_deleteLastSearchfilter() {
		Searchbar.selectFavorite(filter2)

		countBrowserRequests {
			Searchbar.openSearchEditor()
			Searchbar.delete()
		}.with {
			assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
			assert it.getRequestCount(RequestType.PREFERENCE_DELETE) == 1
		}
	}

	@Test
	void _40_searchfilterWithSubform() {
		Searchbar.selectSearchfilter(ORDERPOSITION_SEARCHFILTER)

		assert Searchbar.selectedFilter == ORDERPOSITION_SEARCHFILTER
		assert Sidebar.listEntries.size() == 6

		Searchbar.toggleItemSelection(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'quantity')
		assert Sidebar.listEntries.size() == 3

		Searchbar.toggleItemSelection(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'price')
		assert Sidebar.listEntries.size() == 0

		Searchbar.toggleItemSelection(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'quantity')
		assert Sidebar.listEntries.size() == 1
	}

	@Test
	void _41_createSearchfilterWithSubform() {
		Searchbar.clear()
		Searchbar.closeSearchEditor()
		Searchbar.openSearchEditor()
		Searchbar.enableSubformSearch()

		def subformLov = Searchbar.getSubformLov();
		assert subformLov.getChoices().containsAll(['< Order >', 'Order position'])

		subformLov.selectEntry('Order position')
		def attrLov = Searchbar.getAttributeLov()

		assert attrLov.getChoices().containsAll(['Article', 'Category', 'Name', 'Price', 'Quantity'])
		attrLov.selectEntry('Quantity')

		assert Sidebar.listEntries.size() == 6

		def quantityInput = Searchbar.getValueString(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'quantity')
		quantityInput.setInput('224')
		waitForAngularRequestsToFinish()
		assert Sidebar.listEntries.size() == 1
	}

	@Test
	void _43_searchSystemVlp() {
		Searchbar.selectSearchfilter(SYSTEMFIELDS_SEARCHFILTER)

		// automatic filtering for this system fields:
		def processLov = Searchbar.getValueLov(TestEntities.EXAMPLE_REST_ORDER, 'nuclosProcess')
		def stateLov = Searchbar.getValueLov(TestEntities.EXAMPLE_REST_ORDER, 'nuclosState')

		assert processLov.choices.size() == 3
		processLov.closeViaEscape()
		assert stateLov.choices.size() == 5
	}

	@Test
	void _44_searchVlpWithReferenceParam() {
		Searchbar.selectSearchfilter(CUSTOMER_SEARCHFILTER)

		def customerLov = Searchbar.getValueLov(TestEntities.EXAMPLE_REST_ORDER, 'customer')
		def customerAddressLov = Searchbar.getValueLov(TestEntities.EXAMPLE_REST_ORDER, 'customerAddress')

		customerLov.selectEntry('VLPText: Test-Customer')
		assert customerAddressLov.getChoiceElements().size() == 3 // 2 choices + empty
		customerLov.selectEntry('VLPText: Test-Customer 2')
		// more than 1 entry selected in customer, but dependent vlp only supports one id -> empty entry only
		assert customerAddressLov.getChoiceElements().size() == 1

		customerLov.clearMultiSelection()
		customerLov.selectEntry('VLPText: Test-Customer 2')
		assert customerAddressLov.getChoiceElements().size() == 2 // 1 choice + empty
	}

	@Test
	void _45_searchVlpWithNonReferenceParams() {
		EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTRULES)
		Searchbar.selectSearchfilter(USER_LOV2_TEST_SEARCHFILTER)

		def booleanValue = Searchbar.getValueBoolean(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTRULES, 'boolean')
		def dateValue = Searchbar.getValueDatepicker(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTRULES, 'date')
		def userlov2Value = Searchbar.getValueLov(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTRULES, 'userlov2')
		assert booleanValue.isYes() && !booleanValue.isNo()                            // VLP: superuser only
		assert getDateFromPicker(dateValue).equals(newDate(2019,5,1)) // VLP: created at > this date
		assert userlov2Value.getChoices().containsAll('', 'nuclos')

		setDateInPicker(dateValue, newDate(2200,2,2))
		sendKeys(Keys.ESCAPE)
		assert userlov2Value.getChoices().containsAll('')

		Searchbar.edit()
		Searchbar.resetCustomization()
		booleanValue = Searchbar.getValueBoolean(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTRULES, 'boolean')
		userlov2Value = Searchbar.getValueLov(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTRULES, 'userlov2')
		booleanValue.selectNo()
		assert booleanValue.isNo() && !booleanValue.isYes()
		assert userlov2Value.getChoices().containsAll('', 'test', 'SearchTest2')

		userlov2Value.closeViaEscape()

		Searchbar.selectAttribute('Name')
		Searchbar.selectAttribute('Integer')
		// default string value configured is '%' and search is LIKE 'stringParam'
		def stringValue = Searchbar.getValueString(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTRULES, 'name')
		stringValue.setInput('SearchTest2')
		def choices = userlov2Value.getChoices()
		assert choices.containsAll('', 'SearchTest2')
		assert choices.size() == 2

		def integerValue = Searchbar.getValueString(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTRULES, 'integer')
		integerValue.setInput('0')
		choices = userlov2Value.getChoices()
		assert choices.contains('')
		assert choices.size() == 1

		integerValue.setInput('1')
		choices = userlov2Value.getChoices()
		assert choices.containsAll('', 'SearchTest2')
		assert choices.size() == 2

		userlov2Value.closeViaEscape()
		stringValue.setInput(null)
		integerValue.setInput(null)
		choices = userlov2Value.getChoices()
		assert choices.containsAll('', 'test', 'SearchTest2')
	}

	@Test
	void _50_testLongReference() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTDROPDOWNS)
		eo.addNew()
		eo.setAttribute('word', 'word1');
		eo.save()
		eo.addNew()
		eo.setAttribute('word', 'word2')
		eo.save()

		Searchbar.openSearchEditor()
		Searchbar.clear()

		Searchbar.selectAttribute('Word')
		Searchbar.setCondition(
				TestEntities.NUCLET_TEST_OTHER_TESTDROPDOWNS,
				new Searchtemplate.SearchTemplateItem(
						name: 'word',
						operator: '=',
						value: 'word2'
				)
		)

		assert Sidebar.listEntries.size() == 1
	}

	@Test
	void _51_testUidReference() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTDROPDOWNS)
		eo.addNew()
		eo.setAttribute('word', 'word3');
		eo.setAttribute('nucbenutzer', username)
		eo.save()

		Searchbar.openSearchEditor()
		Searchbar.clear()

		Searchbar.selectAttribute('Nuc Benutzer')
		Searchbar.setCondition(
				TestEntities.NUCLET_TEST_OTHER_TESTDROPDOWNS,
				new Searchtemplate.SearchTemplateItem(
						name: 'nucbenutzer',
						operator: '=',
						value: username
				)
		)

		assert Sidebar.listEntries.size() == 1
	}

	private afterPreferencesShare() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		// Why aren't the preferences reloaded? Are they cached?
		refresh()
	}

	private void setDateInPicker(Datepicker datepicker, Date date) {
		datepicker.setInput(context.dateFormat.format(date))
		sendKeys(Keys.ESCAPE)
	}

	private Date getDateFromPicker(Datepicker datepicker) {
		context.dateFormat.parse(datepicker.getInput())
	}

	private Date newDate(int year, int month, int day) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.clear()
		cal.set(Calendar.YEAR, year)
		cal.set(Calendar.MONTH, month - 1)
		cal.set(Calendar.DAY_OF_MONTH, day)
		cal.getTime()
	}
}
