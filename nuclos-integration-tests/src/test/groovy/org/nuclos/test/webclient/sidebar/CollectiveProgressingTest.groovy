package org.nuclos.test.webclient.sidebar

import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class CollectiveProgressingTest extends AbstractWebclientTest {

	@Test
	void _01_setup() {
		TestDataHelper.insertTestData(nuclosSession)
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
	}

	@Test
	void _02_checkCollectiveProgressingOpens(){
		// Click Multi Selection Button
		$("#sideview-statusbar > div.multi-selection-statusbar-button.sideview-statusbar-content > div > a").click()

		assert $("nuc-multi-selection-header").isDisplayed()
		$("nuc-multi-selection-header").click()

		assert $("#collective-processing-container > nuc-collective-processing").isDisplayed()
	}

	@Test
	void _03_checkWeCannotManipulateSingleEntry() {
		$("#backToDetails").click()

		assert $("#newEO").find() == null
		assert $("#saveEO").find() == null
		assert $("#cloneEO").find() == null
		assert $("#deleteEO").find() == null

		$("#backToCollectiveProcessing").click()
	}

	@Test
	void _04_checkStateChangeWorking() {
		$("#collective-processing-container > nuc-collective-processing > div.container-fluid.card-deck.mb-3.mx-auto.text-container > div:nth-child(1) > ul > li:nth-child(1) > a").click()

		assert $("nuc-collective-processing-progress").isDisplayed()

		for (int i = 1; i <= 6; i++) {
			waitUntilTrue({
				$("#collective-processing-container > nuc-collective-processing > nuc-collective-processing-progress" +
						" > div > ag-grid-angular > div > div.ag-root-wrapper-body.ag-layout-normal > div >" +
						" div.ag-body.ag-layout-normal.ag-row-no-animation > div.ag-body-viewport-wrapper > div > div > " +
						"div.ag-row:nth-child(" + i + ")")
						.isDisplayed()})
		}

		$("#closeProgress").click()
		waitForAngularRequestsToFinish()
	}
}
