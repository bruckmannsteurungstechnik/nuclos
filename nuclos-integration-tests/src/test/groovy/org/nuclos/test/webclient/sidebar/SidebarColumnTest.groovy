package org.nuclos.test.webclient.sidebar

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.FileComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.search.Searchbar
import org.nuclos.test.webclient.pageobjects.viewconfiguration.SideviewConfiguration
import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SidebarColumnTest extends AbstractWebclientTest {

	@Test
	void _01setupBos() {
		assert $('#logout')
		TestDataHelper.insertTestData(nuclosSession)
	}


	@Test
	void _02openSideview() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		screenshot('open-sideview')

		SideviewConfiguration.newSideviewConfiguration('Sideviewconfig1')
		SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDER.fqn + '_nuclosStateIcon')
		SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDER.fqn + '_orderNumber')
		SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDER.fqn + '_customer')
		SideviewConfiguration.saveSideviewConfiguration()
		SideviewConfiguration.close()
		Sidebar.resizeSidebarComponent(800)

		assert Sidebar.selectedColumns().size() == 3


		// check if header is shown
		assert Sidebar.selectedColumns().contains('Order number')
		assert Sidebar.selectedColumns().contains('Customer')

		// check if orderNumber of selected entry is also shown in sideview table
		String orderNumberInDetailBlock = eo.getAttribute('orderNumber')
		assert Sidebar.findElementContainingText('.ag-cell', orderNumberInDetailBlock) != null
	}


	@Test
	void _03addColumns() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		assert Sidebar.selectedColumns().size() == 3
		SideviewConfiguration.selectSideviewConfiguration('Sideviewconfig1')
		Sidebar.addColumn(TestEntities.EXAMPLE_REST_ORDER.fqn, 'note')
		Sidebar.addColumn(TestEntities.EXAMPLE_REST_ORDER.fqn, 'primaryKey')

		Sidebar.resizeSidebarComponent(800)

		waitForAngularRequestsToFinish()

		List<String> columns = Sidebar.selectedColumns()
		assert columns.size() == 5
		assert columns.contains('Note')
		assert columns.contains("ID")

		long eoId = eo.id
		assert eoId > 0
		assert Sidebar.findEntryByText("$eo.id")
	}


	@Test
	void _03sortColumns() {

		// sort note column - prio 1 asc
		$$('nuc-sidebar ag-grid-angular .ag-header-row .nuclos-grid-header-text')[3].click()

		// sort order number column - prio 2 desc
		$$('nuc-sidebar ag-grid-angular .ag-header-row .nuclos-grid-header-text')[1].click()
		$$('nuc-sidebar ag-grid-angular .ag-header-row .nuclos-grid-header-text')[1].click()


		assert Sidebar.getValue(1, 1) == "10020148"
		assert Sidebar.getValue(1, 3) == "A"

		assert Sidebar.getValue(2, 1) == "10020154"
		assert Sidebar.getValue(2, 3) == "B"

		assert Sidebar.getValue(3, 1) == "10020150"
		assert Sidebar.getValue(3, 3) == "B"

		assert Sidebar.getValue(4, 1) == "10020144"
		assert Sidebar.getValue(4, 3) == "B"

		assert Sidebar.getValue(5, 1) == "10020140"
		assert Sidebar.getValue(5, 3) == "B"
	}


	@Test
	void _04reorderColumns() {

		assert Sidebar.getValue(0, 1) == "10020158"
		assert Sidebar.getValue(0, 2) == "Test-Customer"
		assert Sidebar.getValue(0, 3) == "A"

		// move "Customer" after "Note" column
		Sidebar.moveColumnRight(TestEntities.EXAMPLE_REST_ORDER.fqn, 'customer')

		assert Sidebar.getValue(0, 1) == "10020158"
		assert Sidebar.getValue(0, 2) == "A"
		assert Sidebar.getValue(0, 3) == "Test-Customer"

		/*
		TODO doesn't work on testserver
		// move "Customer" before "Order number" column
		Sidebar.moveColumnLeft('example_rest_Order', 'customer')
		Sidebar.moveColumnLeft('example_rest_Order', 'customer')

		assert Sidebar.getValue(0, 1) == "Test-Customer"
		assert Sidebar.getValue(0, 2) == "10020158"
		assert Sidebar.getValue(0, 3) == "A"
		*/
	}

	/**
	 * test that sideview list data is loaded when scrolling down
	 */
	@Test
	void _05dynamicLoadTest() {
		TestEntities entityClass = TestEntities.EXAMPLE_REST_CATEGORY
		// add more sideview entries
		def category =
				[
						boMetaId  : entityClass.fqn,
						attributes: [
								'name': ''
						]
				]

		// TODO: Generate test data server side instead of sending 2.000 requests!
		for (int i = 0; i < 2000; i++) {
			category.attributes['name'] = '' + i
			RESTHelper.createBo(category, nuclosSession)
		}
		EntityObjectComponent eo = EntityObjectComponent.open(entityClass)

		waitForAngularRequestsToFinish()

		def loadedEntries = eo.getListEntryCount()

		assert loadedEntries > 0

		// scroll down sideview list
		def nrOfLoadedRows = getRows().size()
		new Actions(driver).moveToElement(getRows()[nrOfLoadedRows - 1]).build().perform();
		waitForAngularRequestsToFinish()

		assert eo.getListEntryCount() > loadedEntries


		// test dynamic load after searching

		Searchbar.search('11')
		loadedEntries = eo.getListEntryCount()

		assert loadedEntries > 0

		// scroll down sideview list
		nrOfLoadedRows = getRows().size()
		new Actions(driver).moveToElement(getRows()[nrOfLoadedRows - 1]).build().perform();
		waitForAngularRequestsToFinish()

		assert eo.getListEntryCount() > loadedEntries
	}


	@Test
	void _06textsearchColumnChangeTest() {

		// test if text search remains after column change

		def searchString = '1000'
		Searchbar.search(searchString)
		waitForAngularRequestsToFinish()

		assert EntityObjectComponent.forDetail().getListEntryCount() == 1

		Sidebar.addColumn(TestEntities.EXAMPLE_REST_CATEGORY.fqn, 'createdAt')


		assert Searchbar.getSearchfilter().getAttribute('value') == searchString
	}


	/**
	 * upload image and check that it is visible in sidebar
	 */
	@Test
	void _07imageAttribute() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE)


		SideviewConfiguration.newSideviewConfiguration('Sideviewconfig1')
		SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ARTICLE.fqn + '_name')
		SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ARTICLE.fqn + '_image')
		SideviewConfiguration.saveSideviewConfiguration()
		SideviewConfiguration.close()
		Sidebar.resizeSidebarComponent(800)

		FileComponent fileComponent = eo.getFileComponent('image')

		assert !Sidebar.isImageVisible(0, TestEntities.EXAMPLE_REST_ARTICLE.fqn + '_image')

		fileComponent.setFile(TestDataHelper.nuclosPngFile)
		eo.save()

		screenshot('image-in-sidebar')

		assert Sidebar.isImageVisible(0, TestEntities.EXAMPLE_REST_ARTICLE.fqn + '_image')

	}

	private List<WebElement> getRows() {
		driver.findElements(By.cssSelector('nuc-sidebar ag-grid-angular .ag-body-container [row-index]'))
	}

}
