package nuclet.test.other; 

import java.sql.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nuclos.api.context.UpdateContext;
import org.nuclos.api.datasource.DatasourceResult;
import org.nuclos.api.provider.DatasourceProvider;
import org.nuclos.api.rule.InsertRule;
import org.nuclos.api.context.InsertContext; 
import org.nuclos.api.annotation.Rule; 
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.UpdateRule;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name="SQLInjectionTestRule", description="...")
public class SQLInjectionTestRule implements InsertRule, UpdateRule {

	public void insert(InsertContext context) throws BusinessException {
		save(context.getBusinessObject(SQLInjectionTest.class));
	}

	@Override
	public void update(final UpdateContext context) throws BusinessException {
		save(context.getBusinessObject(SQLInjectionTest.class));
	}

	private void save(SQLInjectionTest bo) throws BusinessException {
		bo.setResultlistofinteger(false);
		if (bo.getTestlistofinteger()) {
			boolean bResult = false;

			List<Number> listOfInteger = new ArrayList<>();
			listOfInteger.add(4711L); // Test with Long

			Map<String, Object> params = new HashMap<>();
			params.put("pListOfInteger", listOfInteger);
			params.put("pListOfText", new ArrayList<>());
			params.put("pSql", "false");
			DatasourceResult datasourceResult = DatasourceProvider.run(SQLInjectionDS.class, params);

			if (datasourceResult.getRows().size() == 1) {
				// bResult = Integer.valueOf(4711).equals(datasourceResult.getRows().get(0)[1]);
				// Datenquellen liefern ab 2018 standardmäßig Longs (NUCLOS-6200)
				bResult = Long.valueOf(4711).equals(datasourceResult.getRows().get(0)[1]);
			}
			if (!bResult) {
				throw new BusinessException("Test list of integer failed. Expected only '4711' but datasource returns: " + datasourceResult.getRows());
			}

			listOfInteger.clear();
			listOfInteger.add(0); // Test with Integer
			datasourceResult = DatasourceProvider.run(SQLInjectionDS.class, params);

			if (datasourceResult.getRows().size() == 1) {
				// bResult = Integer.valueOf(0).equals(datasourceResult.getRows().get(0)[1]);
				// Datenquellen liefern ab 2018 standardmäßig Longs (NUCLOS-6200)
				bResult = Long.valueOf(0).equals(datasourceResult.getRows().get(0)[1]);
			}
			if (!bResult) {
				throw new BusinessException("Test list of integer failed. Expected only '0' but datasource returns: " + datasourceResult.getRows());
			}

			bo.setResultlistofinteger(true);
		}

		bo.setResultlistofstring(false);
		if (bo.getTestlistofstrings()) {
			boolean bResult = false;

			List<String> listOfStrings = new ArrayList<>();
			listOfStrings.add("4711");

			Map<String, Object> params = new HashMap<>();
			params.put("pListOfInteger", new ArrayList<>());
			params.put("pListOfText", listOfStrings);
			params.put("pSql", "false");
			DatasourceResult datasourceResult = DatasourceProvider.run(SQLInjectionDS.class, params);

			if (datasourceResult.getRows().size() == 1) {
				bResult = "4711".equals(datasourceResult.getRows().get(0)[2]);
			}
			if (!bResult) {
				throw new BusinessException("Test list of strings failed. Expected only '4711' but datasource returns: " + datasourceResult.getRows());
			}

			listOfStrings.clear();
			listOfStrings.add("Armer Ritter");
			datasourceResult = DatasourceProvider.run(SQLInjectionDS.class, params);
			if (datasourceResult.getRows().size() == 1) {
				bResult = "Armer Ritter".equals(datasourceResult.getRows().get(0)[2]);
			}
			if (!bResult) {
				throw new BusinessException("Test list of strings failed. Expected only '4711' but datasource returns: " + datasourceResult.getRows());
			}

			bo.setResultlistofstring(true);
		}

		bo.setResultsqlinreport(false);
		if (bo.getTestsqlinreport()) {
			Map<String, Object> params = new HashMap<>();
			params.put("pListOfInteger", new ArrayList<>());
			params.put("pListOfText", new ArrayList<>());
			params.put("pSql", "true");

			final String DS_EXCEPTION = "org.nuclos.common.querybuilder.NuclosDatasourceException: Value \"true\" is not a valid whitelist entry of datasource parameter \"$pSql\" (SQL type)";
			try {
				DatasourceProvider.run(SQLInjectionDS.class, params);
				throw new BusinessException("Expected datasource exception not thrown: " + DS_EXCEPTION);
			} catch (BusinessException bex) {
				if (bex.getMessage().equals(DS_EXCEPTION)) {
					// ok
					params.put("pSql", "T1.INTARTICLENUMBER > 4000");
					DatasourceResult datasourceResult = DatasourceProvider.run(SQLInjectionDS.class, params);
					boolean bResult = false;
					if (datasourceResult.getRows().size() == 1) {
						bResult = "4711".equals(datasourceResult.getRows().get(0)[2]);
					}
					if (!bResult) {
						throw new BusinessException("Test of sql param in report failed. Expected only '4711' but datasource returns: " + datasourceResult.getRows());
					}

					bo.setResultsqlinreport(true);
				} else {
					throw bex;
				}
			}
		}

		// Test End
	}
}