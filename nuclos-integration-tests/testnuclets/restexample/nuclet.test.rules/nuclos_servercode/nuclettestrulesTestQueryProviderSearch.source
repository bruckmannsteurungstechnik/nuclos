package nuclet.test.rules;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.businessobject.Query;
import org.nuclos.api.context.CustomContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.rule.CustomRule;
import org.nuclos.businessentity.nuclosuser;

import example.rest.Order;

/**
 * @name
 * @description
 * @usage
 * @change
 */
@Rule(name = "TestQueryProviderSearch", description = "TestQueryProviderSearch")
public class TestQueryProviderSearch implements CustomRule {

	public void custom(CustomContext context) throws BusinessException {
		TestQueryProvider bo = context.getBusinessObject(TestQueryProvider.class);

		Order o = new Order();
		o.setOrderNumber(new Random().nextInt(1000000));
		BusinessObjectProvider.insert(o);

		searchViaTimestamp(bo);
		searchViaDate(bo);

		BusinessObjectProvider.delete(o);
	}

	/**
	 * Searches for Orders via Timestamp condition.
	 */
	private void searchViaTimestamp(TestQueryProvider bo) {
		// Search for Orders created within the last hour.
		Query<Order> query = QueryProvider.create(Order.class);
		query.where(Order.CreatedAt.Gte(new Timestamp(System.currentTimeMillis() - 1000 * 60 * 60)));
		List<Order> result = QueryProvider.execute(query);

		// There is at least 1 Order created within the last hour.
		if (result.isEmpty()) {
			bo.setTimestampsearch("Failed: Expected > 0 results, but got " + result.size());
			return;
		}

		// Search for Orders created later than now.
		query = QueryProvider.create(Order.class);
		query.where(Order.CreatedAt.Gte(new Timestamp(System.currentTimeMillis())));
		result = QueryProvider.execute(query);

		// Result should be empty, as there are no Orders that have been created later than now.
		if (!result.isEmpty()) {
			bo.setTimestampsearch("Failed: Expected 0 results, but got " + result.size());
			return;
		}

		bo.setTimestampsearch("ok");
	}

	/**
	 * Searches for Orders via Date condition.
	 */
	private void searchViaDate(TestQueryProvider bo) {
		Query<Order> query = QueryProvider.create(Order.class);
		query.where(Order.CreatedAt.Gte(new Date()));
		List<Order> result = QueryProvider.execute(query);

		if (!result.isEmpty()) {
			bo.setDatesearch("ok");
		} else {
			bo.setDatesearch("Failed: Expected > 0 results, but got " + result.size());
		}
	}
}
