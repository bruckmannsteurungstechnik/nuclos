# nuclos-layout-transformation
This module is used to transform layouts from the old LayoutML schema
to the WebLayout schema of the [new Webclient](http://support.nuclos.de/browse/NUCLOS-5217).

Old layouts can also contain layout rules, thus there is a separate transformer which only extracts the rules.