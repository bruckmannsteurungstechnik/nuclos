package org.nuclos.layout.transformation.rule

import org.nuclos.common.IMetaProvider
import org.nuclos.schema.layout.layoutml.Event
import org.nuclos.schema.layout.layoutml.Layoutml
import org.nuclos.schema.layout.rule.ObjectFactory
import org.nuclos.schema.layout.rule.Rule
import org.nuclos.schema.layout.rule.RuleAction
import org.nuclos.schema.layout.rule.RuleEvent
import org.nuclos.schema.layout.rule.Rules
import org.nuclos.layout.transformation.AbstractLayoutmlTransformer
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import groovy.transform.CompileStatic

/**
 * Transforms an instance of {@link Layoutml} to {@link Rule}.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class LayoutmlToRuleTransformer extends AbstractLayoutmlTransformer<Rules> {
	private static final Logger log = LoggerFactory.getLogger(LayoutmlToRuleTransformer.class);

	protected final ObjectFactory factory

	LayoutmlToRuleTransformer(
			final IMetaProvider metaProvider,
			final Layoutml layoutml
	) {
		super(metaProvider, layoutml)

		factory = new ObjectFactory()
	}


	@Override
	Rules transform() {
		Rules result = factory.createRules()

		layoutml.rules?.rule?.each { org.nuclos.schema.layout.layoutml.Rule it ->
			Rule rule = factory.createRule()

			rule.name = it.name
			rule.event = transformEvent(it.event)
			it.actions?.transferLookedupValueOrClearOrEnable?.each {
				RuleAction action = transformAction(it)
				if (action) {
					rule.actions << action
				}
			}

			result.rules << rule
		}

		return result
	}

	RuleEvent transformEvent(Event event) {
		RuleEvent result = factory.createRuleEvent()

		result.type = event.type
		result.entity = event.entity
		result.sourcecomponent = event.sourcecomponent

		return result
	}

	RuleAction transformAction(Object action) {
		RuleAction result = null

		ActionTransformer<?, ? extends RuleAction> transformer = TransformerRegistry.lookup(action?.class)
		if (transformer) {
			result = transformer.transform(action)
		} else {
			log.warn('Unknown action type {}', action)
		}

		return result
	}
}
