package org.nuclos.layout.transformation.weblayout

import org.nuclos.schema.layout.layoutml.LineBorder
import org.nuclos.schema.layout.layoutml.Panel
import org.nuclos.schema.layout.layoutml.TitledBorder
import org.nuclos.schema.layout.web.WebComponent
import org.nuclos.schema.layout.web.WebContainer
import org.nuclos.schema.layout.web.WebPanel

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
abstract class AbstractPanelTransformer extends ElementTransformer<Panel, WebComponent> {
	AbstractPanelTransformer() {
		super(Panel.class)
	}

	protected WebContainer createPanelOrContainer(Panel panel) {
		WebContainer result
		if (hasTitleOrBorder(panel)) {
			result = factory.createWebPanel()
			(result as WebPanel).title = findTitle(panel)
			addLineBorder(result as WebPanel, panel)
		} else {
			result = factory.createWebContainer()
		}
		result
	}

	private void addLineBorder(final WebPanel webPanel, final Panel panel) {
		LineBorder border = findLineBorder(panel)

		if (border) {
			if (border.thickness) {
				webPanel.borderWidth = border.thickness + 'px'
			}
			if (border.red && border.green && border.blue) {
				try {
					webPanel.borderColor = String.format(
							'#%02X%02X%02X',
							border.red as Integer,
							border.green as Integer,
							border.blue as Integer
					)
				} catch (Exception) {
					// Ignore
				}
			}
		}
	}

	private boolean hasTitleOrBorder(Panel panel) {
		return panel.border.find {
			it.value instanceof TitledBorder || it.value instanceof LineBorder
		}
	}

	/**
	 * Tries to find a titled border and returns its title.
	 *
	 * @param panel
	 */
	private String findTitle(Panel panel) {
		TitledBorder border = panel.border.find {
			it.value instanceof TitledBorder
		}?.value as TitledBorder

		border?.title
	}

	/**
	 * Tries to find a line border.
	 *
	 * @param panel
	 */
	private LineBorder findLineBorder(Panel panel) {
		LineBorder border = panel.border.find {
			it.value instanceof LineBorder
		}?.value as LineBorder

		return border
	}
}
