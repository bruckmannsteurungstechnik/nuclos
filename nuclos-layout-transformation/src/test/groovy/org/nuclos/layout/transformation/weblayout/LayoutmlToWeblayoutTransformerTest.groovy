package org.nuclos.layout.transformation.weblayout

import org.nuclos.schema.layout.web.WebComponent
import org.nuclos.schema.layout.web.WebContainer
import org.nuclos.schema.layout.web.WebGrid
import org.nuclos.schema.layout.web.WebGridCalculated
import org.nuclos.schema.layout.web.WebTab
import org.nuclos.schema.layout.web.WebTabcontainer
import org.nuclos.schema.layout.web.WebTable
import org.nuclos.layout.transformation.AbstractLayoutmlTransformerTest

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
abstract class LayoutmlToWeblayoutTransformerTest extends AbstractLayoutmlTransformerTest {
	protected List<WebComponent> findAllComponents(def componentOrContainer) {
		List<WebComponent> result = []

		if (componentOrContainer instanceof WebComponent) {
			WebComponent component = (WebComponent) componentOrContainer
			result << component
			if (component instanceof WebContainer) {
				WebContainer container = (WebContainer) component
				result << container
				result.addAll(findAllComponents(container.grid))
				result.addAll(findAllComponents(container.table))
				result.addAll(findAllComponents(container.calculated))
				container.components.each {
					result.addAll(findAllComponents(it))
				}
			} else if (component instanceof WebTabcontainer) {
				WebTabcontainer container = (WebTabcontainer) component
				result << container
				container.tabs.each {
					result.addAll(findAllComponents(it))
				}
			}
		} else if (componentOrContainer instanceof WebGrid) {
			WebGrid grid = (WebGrid) componentOrContainer
			grid.rows.each { row ->
				row.cells.each { cell ->
					cell.components.each {
						result.addAll(findAllComponents(it))
					}
				}
			}
		} else if (componentOrContainer instanceof WebTable) {
			WebTable table = (WebTable) componentOrContainer
			table.rows.each { row ->
				row.cells.each { cell ->
					cell.components.each {
						result.addAll(findAllComponents(it))
					}
				}
			}
		} else if (componentOrContainer instanceof WebGridCalculated) {
			WebGridCalculated grid = (WebGridCalculated) componentOrContainer
			grid.cells.each { cell ->
				cell.components.each {
					result.addAll(findAllComponents(it))
				}
			}
		} else if (componentOrContainer instanceof WebTab) {
			WebTab tab = (WebTab) componentOrContainer
			result.addAll(findAllComponents(tab.content))
		} else if (componentOrContainer != null) {
			println "Unknown component: $componentOrContainer"
		}

		return result
	}
}