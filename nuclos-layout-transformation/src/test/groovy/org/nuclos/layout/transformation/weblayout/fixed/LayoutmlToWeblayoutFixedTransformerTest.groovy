package org.nuclos.layout.transformation.weblayout.fixed

import org.junit.Test
import org.nuclos.common2.JaxbMarshalUnmarshalUtil
import org.nuclos.schema.layout.layoutml.Layoutml
import org.nuclos.schema.layout.web.WebButtonChangeState
import org.nuclos.schema.layout.web.WebButtonDummy
import org.nuclos.schema.layout.web.WebButtonExecuteRule
import org.nuclos.schema.layout.web.WebButtonGenerateObject
import org.nuclos.schema.layout.web.WebButtonHyperlink
import org.nuclos.schema.layout.web.WebCheckbox
import org.nuclos.schema.layout.web.WebCombobox
import org.nuclos.schema.layout.web.WebComponent
import org.nuclos.schema.layout.web.WebContainer
import org.nuclos.schema.layout.web.WebDatechooser
import org.nuclos.schema.layout.web.WebFile
import org.nuclos.schema.layout.web.WebLabel
import org.nuclos.schema.layout.web.WebLabelStatic
import org.nuclos.schema.layout.web.WebLayout
import org.nuclos.schema.layout.web.WebListofvalues
import org.nuclos.schema.layout.web.WebMatrix
import org.nuclos.schema.layout.web.WebPanel
import org.nuclos.schema.layout.web.WebSubform
import org.nuclos.schema.layout.web.WebSubformColumn
import org.nuclos.schema.layout.web.WebTabcontainer
import org.nuclos.schema.layout.web.WebTextfield
import org.nuclos.layout.transformation.weblayout.LayoutmlToWeblayoutTransformerTest

import com.fasterxml.jackson.databind.ObjectMapper

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class LayoutmlToWeblayoutFixedTransformerTest extends LayoutmlToWeblayoutTransformerTest {


	@Test
	void testLayoutml2WeblayoutFixed() {
		InputStream xml = this.getClass().getResourceAsStream('../../layout/nuclet_test_other_TestLayoutComponents.xml')

		Layoutml layoutml = JaxbMarshalUnmarshalUtil.unmarshal(xml, Layoutml.class)
		def transformer = new LayoutmlToWeblayoutFixedTransformer(getMetaProvider(), layoutml)
		WebLayout result = transformer.transform()

		assert !result.table.rows.empty

		List<WebComponent> components = findAllComponents(result.table)
		assert components.find { it instanceof WebContainer && !it.opaque }
		assert components.find { it instanceof WebPanel && it.title && it.opaque }
		assert components.find { it instanceof WebTextfield }
		assert components.find { it instanceof WebDatechooser }
		assert components.find { it instanceof WebListofvalues }
		assert components.find { it instanceof WebCombobox }
		assert components.find { it instanceof WebTabcontainer }
		assert components.find { it instanceof WebSubform }
		assert components.find { it instanceof WebFile }
		assert components.find { it instanceof WebButtonDummy && it.label && it.disableDuringEdit }
		assert components.find { it instanceof WebButtonExecuteRule && it.rule && it.label }
		assert components.find { it instanceof WebButtonGenerateObject && it.objectGenerator && it.label }
		assert components.find { it instanceof WebButtonChangeState /*&& it.targetState*/ && it.label }
		assert components.find { it instanceof WebButtonHyperlink && it.hyperlinkField && it.label }
		assert components.find { it instanceof WebCheckbox }

		// There should be static labels with and without font size
		assert components.find { it instanceof WebLabelStatic && it.text && it.fontSize }
		assert components.find { it instanceof WebLabelStatic && it.text && !it.fontSize }

		// There should be labels with and without font size
		assert components.find { it instanceof WebLabel && it.fontSize }
		assert components.find { it instanceof WebLabel && !it.fontSize }

		assert !components.find { it instanceof WebComponent && it.name == 'hiddenText' }
		assert !components.find { it instanceof WebSubformColumn && it.name == 'comment' }
	}

	@Test
	void testWeblayoutFixed2JSON() {
		InputStream xml = this.getClass().getResourceAsStream('../../layout/example_rest_Order.xml')

		Layoutml layoutml = JaxbMarshalUnmarshalUtil.unmarshal(xml, Layoutml.class)
		def transformer = new LayoutmlToWeblayoutFixedTransformer(getMetaProvider(), layoutml)
		WebLayout result = transformer.transform()

		ObjectMapper mapper = getMapper()
		StringWriter stringWriter = new StringWriter()

		mapper.writeValue(stringWriter, result);
		String serializedValue = stringWriter.toString()

		assert serializedValue
	}

	@Test
	void testMatrixLayout() {
		InputStream xml = this.getClass().getResourceAsStream('../../layout/nuclet_test_matrix_Matrix.xml')

		Layoutml layoutml = JaxbMarshalUnmarshalUtil.unmarshal(xml, Layoutml.class)
		def transformer = new LayoutmlToWeblayoutFixedTransformer(getMetaProvider(), layoutml)
		WebLayout result = transformer.transform()

		assert !result.table.rows.empty

		List<WebComponent> components = findAllComponents(result.table)
		assert components.find { it instanceof WebMatrix }

	}
}