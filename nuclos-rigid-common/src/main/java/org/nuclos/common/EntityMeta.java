package org.nuclos.common;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.nuclos.common.collection.Predicate;


public abstract class EntityMeta<PK> implements Serializable {
	
	private static final long serialVersionUID = 8276539549175831030L;
	private static final String ENTITY_LANGUAGE_DB_TABLE_SUFFIX = "_LANG";
	public static final String FIELDS_FOR_EQUALITY_PATTERN = "\\s*[,;]\\s*";
	public static final Integer DATA_LANG_DB_TABLE_NAME_MAX_LENGTH = 30;

	public static final EntityMeta<?> NULL = new EntityMeta() {
		private static final long serialVersionUID = 1L;
		@Override public UID getUID() {return null;}
		@Override public Class<UID> getPkClass() {return null;}
		@Override public String getDbTable() {return "";}
		@Override public String getEntityName() {return "";}
	};

	public enum PROPERTY {
		TASKLIST_DEFINITION,
		TASKLIST_REF_TO_ENTITYMETA_FIELD
	}

	protected final ConcurrentMap<PROPERTY, Serializable> properties = new ConcurrentHashMap<>();
	
	public abstract UID getUID();
	
	public abstract String getDbTable();

	public abstract String getEntityName();

	public String getBusinessObjectClassName() {
		return getEntityName();
	};

	public abstract Class<PK> getPkClass();
	
	public UID getNuclet() {
		return null;
	}
	
	public boolean isUidEntity() {
		return false;
	}
	
	public boolean isSearchable() {
		return true;
	}
	
	public boolean isEditable() {
		return true;
	}

	public boolean isReadonly() {
		return isDatasourceBased() || (getVirtualEntity() != null && getIdFactory() == null && !isWriteProxy());
	}

	public final boolean isWritable() {
		return !isReadonly();
	}
	
	public boolean isCacheable() {
		return false;
	}
	
	public boolean isImportExport() {
		return false;
	}

	public final boolean isDatasourceBased() { return isDynamic() || isChart() || isDynamicTasklist(); }

	/**
	 * TODO: What does "dynamic" mean here exactly?!
	 *  Does it only refer to "dynamic business objects"?
	 *  It returns false for dynamic tasklist entities!
	 */
	public boolean isDynamic() {
    	return false;
    }
	
	public boolean isResultdetailssplitview() {
		return false;
	}
	
	public UID getDataSource() {
		return null;
	}

	public boolean isDynamicTasklist() { return false; }

	public boolean isChart() {
    	return false;
    }
	
	public boolean isFieldValueEntity() {
		return false;
	}
	
	public boolean isLogBookTracking() {
		return false;
	}
	
	public boolean isStateModel() {
		return false;
	}
	
	public boolean isTreeRelation() {
		return false;
	}

	public boolean isTreeGroup() {
		return false;
	}
	
	public boolean isTableMaster() {
		return true;
	}
	
	public boolean isShowSearch() {
		return true;
	}
	
	public boolean isThin() {
		return isUidEntity();
	}
	
	public String getNuclosResource() {
		return null;
	}
	
	public UID getResource() {
		return null;
	}
	
	public String getAccelerator() {
		return null;
	}
	
	public Integer getAcceleratorModifier() {
		return null;
	}
	
	public UID[] getFieldsForEquality() {
		return null;
	}
	
	public String getLocaleResourceIdForLabel() {
		return null;
	}
	
	public String getLocaleResourceIdForMenuPath() {
		return null;
	}
	
	public String getLocaleResourceIdForDescription() {
		return null;
	}

	public String getLocaleResourceIdForTreeView() {
		return null;
	}

	public String getLocaleResourceIdForTreeViewDescription() {
		return null;
	}
	
	public String getSystemIdPrefix() {
		return null;
	}
	
	public String getIdFactory() {
		return null;
	}
	
	public UID[][] getUniqueFieldCombinations() {
		return null;
	}
	
	public UID[][] getLogicalUniqueFieldCombinations() {
		return null;
	}
	
	public UID[][] getIndexFieldCombinations() {
		return null;
	}
	
	public String getMenuShortcut() {
		return null;
	}
	
	public String getReadDelegate() {
		return null;
	}
	
	public String getDataLangRefPath() {
		return null;
	}

	public NuclosScript getRowColorScript() {
		return null;
	}
	
	public String getVirtualEntity() {
		return null;
	}
	
	public UID getCloneGenerator() {
		return null;
	}
	
	public boolean isProxy() {
		return false;
	}

	public String getSystemProxyImplementation() {
		return null;
	}
	
	public boolean isWriteProxy() {
		return false;
	}


	/**
	 * Since Nuclos v4.14 (see NUCLOS-5470)
	 */
	public boolean isGeneric() {
		return false;
	}

	/**
	 * Since Nuclos v4.16
	 */
	public boolean isIntegrationPoint() {
		return false;
	}

	public UID getMandatorLevel() {
		return null;
	}

	/**
	 * Since Nuclos v4.25 (see NUCLOS-6678)
	 */
	public boolean isMandatorUnique() {
		return false;
	}
	
	public final boolean isMandator() {
		return getMandatorLevel() != null;
	}
	
	public String getDataLanguageDBTable() {
		return null;
	}
	
	public String getComment() {
		return null;
	}
	
	public LockMode getLockMode() {
		return null;
	}
	
	public String getOwnerForeignEntityField() {
		return null;
	}
	
	public UnlockMode getUnlockMode() {
		return null;
	}
	
	public boolean isOwner() {
		return LockMode.isOwner(this.getLockMode());
	}

	public Set<EntityContext> getEntityContexts() {
		return Collections.emptySet();
	}
	
	/**
	 * used from datasources
	 */
	public UID getDetailEntity() {
		return null;
	}
	
	/**
	 * used from datasources
	 */
	public UID getParentEntity() {
		return null;
	}
	
	public String getDbSelect() {
		// dbSourceForDML = eMeta.isVirtual() ? eMeta.getVirtualentity() : "T_" + eMeta.getDbEntity().substring(2);
		final String result;
		if (getVirtualEntity() != null) {
			result = getVirtualEntity();
		}
		else if (isDatasourceBased()) {
			/* Oracle don't like it " AS " + */
			result = "(" + getDbTable() + ")";
		}
		else if (getReadDelegate() != null) {
			result = getReadDelegate();
		}
		else if (getDbTable().startsWith("V_")) {
			result = "T_" + getDbTable().substring(2);
		}
		else {
			result = getDbTable();
		}
		return result;
	}
	
	public boolean checkEntityUID(UID entityUID) {
		return (entityUID != null) && entityUID.equals(getUID());
	}
	
	private Collection<FieldMeta<?>> fields = null;
	public Collection<FieldMeta<?>> getFields() {
		if (this.fields == null) {
			Collection<FieldMeta<?>> initFields = new ArrayList<>();
			if (isUidEntity()) {
				initFields.add(SF.PK_UID.getMetaData(this));
			} else {
				initFields.add(SF.PK_ID.getMetaData(this));
			}
			initFields.add(SF.CREATEDBY.getMetaData(this));
			initFields.add(SF.CREATEDAT.getMetaData(this));
			initFields.add(SF.CHANGEDBY.getMetaData(this));
			initFields.add(SF.CHANGEDAT.getMetaData(this));
			try {
				addAllFields(this, initFields, getClass().getDeclaredFields());
				addAllFields(this, initFields, getClass().getFields());
			} catch (Exception e1) {
				throw new RuntimeException(String.format("Receiving all fields from class %s failed: %s", getClass(), e1.getMessage()));
			}
			this.fields = initFields;
		}
		return this.fields;
	}
	
	private static void addAllFields(Object objThis, Collection<FieldMeta<?>> allFields, Field[] fields) throws Exception {
		for (Field f : fields) {
			try {
				f.setAccessible(true);
				Object o = f.get(objThis);
				if (o instanceof FieldMeta<?>) {
					if (!allFields.contains(o)) {
						allFields.add((FieldMeta<?>) o);
					}
				}
			} catch (NullPointerException e) {
				// ignore
			} catch (Exception e) {
				throw e;
			} 
		}
	}
	
	public boolean IsLocalized() {
		boolean retVal = false;
		
		for (FieldMeta fm : getFields()) {
			if (fm.isLocalized() && fm.getCalcFunction() == null) {
				retVal = true;
				break;
			}
		}
		
		return retVal;
	}
	
	/**
	 * Return the field UID within the entity.
	 * 
	 * @param fieldUid 
	 * @throws IllegalArgumentException if fieldUid is not contained in the entity.
	 */
	public FieldMeta<?> getField(final UID fieldUid) {
		final Collection<FieldMeta<?>> fields = getFields();
		final FieldMeta<?> result = RigidUtils.findFirst(fields, 
				new Predicate<FieldMeta<?>>() {
					@Override
					public boolean evaluate(FieldMeta<?> t) {
						return fieldUid.equals(t.getUID());
					}
				});
		if (result == null) {
			throw new IllegalArgumentException("No field UID " + fieldUid + " in entity " + getUID());
		}
		return result;
	}
	
	public boolean hasField(final UID fieldUid) {
		
		boolean retVal = false;
		
		final Collection<FieldMeta<?>> fields = getFields();
		final FieldMeta<?> result = RigidUtils.findFirst(fields, 
				new Predicate<FieldMeta<?>>() {
					@Override
					public boolean evaluate(FieldMeta<?> t) {
						return fieldUid.equals(t.getUID());
					}
				});
		
		if (result != null) {
			retVal = true;
		}
		return retVal;
	}
	
	@SuppressWarnings("unchecked")
	public SF<PK> getPk() {
		if (UID.class.equals(getPkClass())) {
			return (SF<PK>) SF.PK_UID;
		} else {
			return (SF<PK>) SF.PK_ID;
		}
	}

	public void addProperty(PROPERTY prop, Serializable property) {
		properties.put(prop, property);
	}

	public Serializable getProperty(PROPERTY prop) {
		return properties.get(prop);
	}
	
	public static String getEntityLanguageDBTablename(EntityMeta<?> meta) {
		String tablename = meta.getDbTable();

		return getEntityLanguageDBTablename(tablename);
	}

	public static String getEntityLanguageDBTablename(String entitytablename) {
		String tablename = entitytablename + ENTITY_LANGUAGE_DB_TABLE_SUFFIX;
		int length = tablename.length();
		if (tablename.startsWith(NucletConstants.DEFAULT_LOCALIDENTIFIER)) {
			length++;
		}

		if (length > DATA_LANG_DB_TABLE_NAME_MAX_LENGTH) {
			return entitytablename.substring(0, DATA_LANG_DB_TABLE_NAME_MAX_LENGTH - ENTITY_LANGUAGE_DB_TABLE_SUFFIX.length()) + ENTITY_LANGUAGE_DB_TABLE_SUFFIX;
		} else {
			return tablename;
		}
	}
	
	public static String getEntityLanguageName(EntityMeta<?> meta) {
		return meta.getEntityName() + ENTITY_LANGUAGE_DB_TABLE_SUFFIX;
	}
	
	public static boolean isEntityLanguageUID(UID entityUID) {
		return entityUID.getString().endsWith(ENTITY_LANGUAGE_DB_TABLE_SUFFIX);
	}
	
	public static UID getEntityLanguageUID(EntityMeta<?> meta) {
		return new UID(meta.getUID().toString() + ENTITY_LANGUAGE_DB_TABLE_SUFFIX);
	}
	
	public static UID getEntityLanguageUID(UID meta) {
		return new UID(meta.toString() + ENTITY_LANGUAGE_DB_TABLE_SUFFIX);
	}
	
	public static UID getEntityUIDByLanguageUID(UID meta) {
		return new UID(meta.toString().replace(ENTITY_LANGUAGE_DB_TABLE_SUFFIX,  ""));
	}
	
	@Override
	public int hashCode() {
		return 187281 + (getUID() == null ? super.hashCode() : getUID().hashCode());
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that instanceof EntityMeta<?>) {
			return RigidUtils.equal(this.getUID(), (((EntityMeta<?>) that).getUID()));
		} else {
			throw new IllegalArgumentException("Something went wrong: EntityMeta.equal(" + that.getClass().getName() + ")! EntityMeta.this[" + this + "] that[" + that + "]");
		}
	}
	
	@Override
	public String toString() {
		return getEntityName();
	}
	
}
