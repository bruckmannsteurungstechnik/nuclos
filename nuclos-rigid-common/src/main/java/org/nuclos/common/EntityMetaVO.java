package org.nuclos.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;

public class EntityMetaVO<PK> extends EntityMeta<PK> {

	private static final long serialVersionUID = -4459356601136979034L;
	
	private Class<PK> pkClass;
	private UID primaryKey;
	
	private Collection<FieldMeta<?>> fields;
	
	private String dbTable;
	private String entityName;
	private boolean uidEntity;
	
	private UID nuclet;

	private String systemIdPrefix;
	private String menuShortcut;

	private boolean editable;
	private boolean resultDetailsSplit;
	private boolean stateModel;
	private boolean logBookTracking;
	private boolean cacheable;
	private boolean searchable;
	private boolean treeRelation;
	private boolean treeGroup;
	private boolean importExport;
	private boolean fieldValueEntity;
	private boolean dynamic;
	private boolean dynamicTasklist;
	private boolean showSearch;
	private boolean thin;
	private boolean chart;
	private boolean proxy;
	private boolean writeProxy;
	private boolean generic;
	private String comment;
	private String accelerator;
	private Integer acceleratorModifier;
	private UID[] fieldsForEquality;
	private UID resource;
	private String nuclosResource;
	private String localeResourceIdForLabel;
	private String localeResourceIdForDescription;
	private String localeResourceIdForMenuPath;
	private String localeResourceIdForTreeView;
	private String localeResourceIdForTreeViewDescription;

	private String virtualentity;
	private String idFactory;
	
	private String readDelegate;

	private UID[][] uniqueFieldCombinations;
	private UID[][] logicalUniqueFieldCombinations;
	private UID[][] indexFieldCombinations;

	private NuclosScript rowColorScript;
	
	private UID cloneGenerator;
	private UID uidDataSource;
	private UID mandatorLevel;
	private boolean mandatorUnique;
	
	private String dataLangRefPath;
	
	private String dataLanguageDBTable;

	private UID detailEntity;

	private UID parentEntity;
	
	private LockMode lockMode;
	private UnlockMode unlockMode;
	private String ownerForeignEntityField;

	private Set<EntityContext> entityContexts;
	
	public EntityMetaVO(Class<PK> pkClass) {
		this.pkClass = pkClass;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public EntityMetaVO(EntityMeta<?> other, boolean withFields) {
		pkClass = (Class<PK>) other.getPkClass();
		primaryKey = other.getUID();
		
		dbTable = other.getDbTable();
		entityName = other.getEntityName();
		uidEntity = other.isUidEntity();
		
		nuclet = other.getNuclet();

		systemIdPrefix = other.getSystemIdPrefix();
		menuShortcut = other.getMenuShortcut();

		editable = other.isEditable();
		resultDetailsSplit = other.isResultdetailssplitview();
		stateModel = other.isStateModel();
		logBookTracking = other.isLogBookTracking();
		cacheable = other.isCacheable();
		searchable = other.isSearchable();
		treeRelation = other.isTreeRelation();
		treeGroup = other.isTreeGroup();
		importExport = other.isImportExport();
		fieldValueEntity = other.isFieldValueEntity();
		dynamic = other.isDynamic();
		dynamicTasklist = other.isDynamicTasklist();
		showSearch = other.isShowSearch();
		thin = other.isThin();
		dataLangRefPath = other.getDataLangRefPath();
		accelerator = other.getAccelerator();
		acceleratorModifier = other.getAcceleratorModifier();
		fieldsForEquality = other.getFieldsForEquality();
		resource = other.getResource();
		nuclosResource = other.getNuclosResource();
		localeResourceIdForLabel = other.getLocaleResourceIdForLabel();
		localeResourceIdForDescription = other.getLocaleResourceIdForDescription();
		localeResourceIdForMenuPath = other.getLocaleResourceIdForMenuPath();
		localeResourceIdForTreeView = other.getLocaleResourceIdForTreeView();
		localeResourceIdForTreeViewDescription = other.getLocaleResourceIdForTreeViewDescription();
		comment = other.getComment();
		
		virtualentity = other.getVirtualEntity();
		idFactory = other.getIdFactory();
		
		writeProxy=other.isWriteProxy();
		proxy = other.isProxy();
		generic = other.isGeneric();
		
		readDelegate = other.getReadDelegate();

		uniqueFieldCombinations = other.getUniqueFieldCombinations();
		logicalUniqueFieldCombinations = other.getLogicalUniqueFieldCombinations();
		indexFieldCombinations = other.getIndexFieldCombinations();

		rowColorScript = other.getRowColorScript();
		
		cloneGenerator = other.getCloneGenerator();
		uidDataSource = other.getDataSource();
		mandatorLevel = other.getMandatorLevel();
		mandatorUnique = other.isMandatorUnique();
		
		if (withFields && other.getFields() != null) {
			fields = new ArrayList<FieldMeta<?>>();
			for (FieldMeta<?> field : other.getFields()) {
				fields.add(new FieldMetaVO(field));
			}
		}
		
		dataLanguageDBTable = other.getDataLanguageDBTable();
		
		lockMode = other.getLockMode();
		ownerForeignEntityField = other.getOwnerForeignEntityField();
		unlockMode = other.getUnlockMode();

		properties.clear();
		properties.putAll(other.properties);
	}
	
	@Override
	public UID getUID() {
		return primaryKey;
	}
	
	public void setUID(UID primaryKey) {
		this.primaryKey = primaryKey;
	}
	
	@Override
	public Class<PK> getPkClass() {
		return pkClass;
	}

	@Override
	public String getDataLangRefPath() {
		return dataLangRefPath;
	}

	public void setDataLangRefPath(String dataLangRefPath) {
		this.dataLangRefPath = dataLangRefPath;
	}

	@Override
	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	@Override
	public Collection<FieldMeta<?>> getFields() {
		if (fields == null) {
			throw new NuclosFatalException(String.format("Fields in %s not set!", getEntityName()));
		}
		return fields;
	}
	
	public void setFields(Collection<FieldMeta<?>> fields) {
		this.fields = fields;
	}
	
	@Override
	public String getDbTable() {
		return dbTable;
	}
	
	public void setDbTable(String dbTable) {
		this.dbTable = dbTable;
	}

	@Override
	public String getDataLanguageDBTable() {
		return dataLanguageDBTable;
	}
	
	public void setDataLanguageDBTable(String dataLangDBTable) {
		this.dataLanguageDBTable = dataLangDBTable;
	}

	@Override
	public UID getNuclet() {
		return nuclet;
	}

	public void setNuclet(UID nuclet) {
		this.nuclet = nuclet;
	}
	
	@Override
	public String getSystemIdPrefix() {
		return systemIdPrefix;
	}

	public void setSystemIdPrefix(String systemIdPrefix) {
		this.systemIdPrefix = systemIdPrefix;
	}
	
	@Override
	public String getMenuShortcut() {
		return menuShortcut;
	}

	public void setMenuShortcut(String menuShortcut) {
		this.menuShortcut = menuShortcut;
	}

	public void setEditable(Boolean editable) {
		this.editable = Boolean.TRUE.equals(editable);
	}
	
	public void setResultdetailssplitview(Boolean split) {
		this.resultDetailsSplit = (Boolean) ObjectUtils.defaultIfNull(split, Boolean.FALSE);
	}

	public void setStateModel(Boolean stateModel) {
		this.stateModel = Boolean.TRUE.equals(stateModel);
	}

	public void setLogBookTracking(Boolean logBookTracking) {
		this.logBookTracking = Boolean.TRUE.equals(logBookTracking);
	}

	public void setCacheable(Boolean cacheable) {
		this.cacheable = Boolean.TRUE.equals(cacheable);
	}

	public void setSearchable(Boolean searchable) {
		this.searchable = Boolean.TRUE.equals(searchable);
	}

	public void setTreeRelation(Boolean treeRelation) {
		this.treeRelation = Boolean.TRUE.equals(treeRelation);
	}

	public void setTreeGroup(Boolean treeGroup) {
		this.treeGroup = Boolean.TRUE.equals(treeGroup);
	}

	public void setImportExport(Boolean importExport) {
		this.importExport = Boolean.TRUE.equals(importExport);
	}

	public void setFieldValueEntity(Boolean fieldValueEntity) {
		this.fieldValueEntity = Boolean.TRUE.equals(fieldValueEntity);
	}

	public void setAccelerator(String accelerator) {
		this.accelerator = accelerator;
	}

	public void setAcceleratorModifier(Integer acceleratorModifier) {
		this.acceleratorModifier = acceleratorModifier;
	}

	public void setFieldsForEquality(String fieldsForEquality) {
		if (fieldsForEquality != null) {
			fieldsForEquality = fieldsForEquality.trim();
		}
		Set<UID> result = new HashSet<UID>();
		if(fieldsForEquality != null) {
			String[] parsed = fieldsForEquality.split(FIELDS_FOR_EQUALITY_PATTERN);
			for (String p : parsed) {
				result.add(new UID(p));
			}
		}
		this.fieldsForEquality = result.toArray(new UID[] {});
	}
	
	public void setFieldsForEquality(UID[] fieldsForEquality) {
		this.fieldsForEquality = fieldsForEquality;
	}

	public void setResource(UID resourceUID) {
		this.resource = resourceUID;
	}

	public void setNuclosResource(String nuclosResource) {
		this.nuclosResource = nuclosResource;
	}

	public void setLocaleResourceIdForLabel(String localeResourceIdForLabel) {
		this.localeResourceIdForLabel = localeResourceIdForLabel;
	}

	public void setLocaleResourceIdForDescription(
		String localeResourceIdForDescription) {
		this.localeResourceIdForDescription = localeResourceIdForDescription;
	}

	public void setLocaleResourceIdForMenuPath(String localeResourceIdForMenuPath) {
		this.localeResourceIdForMenuPath = localeResourceIdForMenuPath;
	}

	public void setLocaleResourceIdForTreeView(String localeResourceIdForTreeView) {
		this.localeResourceIdForTreeView = localeResourceIdForTreeView;
	}

	public void setLocaleResourceIdForTreeViewDescription(
		String localeResourceIdForTreeViewDescription) {
		this.localeResourceIdForTreeViewDescription = localeResourceIdForTreeViewDescription;
	}
	
	public void setDataSource(UID uidDataSource) {
		this.uidDataSource = uidDataSource;
	}

	@Override
	public boolean isEditable() {
		return Boolean.TRUE.equals(editable);
	}
	
	@Override
	public boolean isResultdetailssplitview() {		
		return Boolean.TRUE.equals(resultDetailsSplit);
	}

	@Override
	public boolean isStateModel() {
		return Boolean.TRUE.equals(stateModel);
	}

	@Override
	public boolean isLogBookTracking() {
		return Boolean.TRUE.equals(logBookTracking);
	}

	@Override
	public boolean isCacheable() {
		return Boolean.TRUE.equals(cacheable);
	}

	@Override
	public boolean isSearchable() {
		return Boolean.TRUE.equals(searchable);
	}

	@Override
	public boolean isTreeRelation() {
		return Boolean.TRUE.equals(treeRelation);
	}

	@Override
	public boolean isTreeGroup() {
		return Boolean.TRUE.equals(treeGroup);
	}

	@Override
	public boolean isImportExport() {
		return Boolean.TRUE.equals(importExport);
	}

	@Override
	public boolean isFieldValueEntity() {
		return Boolean.TRUE.equals(fieldValueEntity);
	}

	@Override
	public String getAccelerator() {
		return accelerator;
	}

	@Override
	public Integer getAcceleratorModifier() {
		return acceleratorModifier;
	}
	
	@Override
	public UID[] getFieldsForEquality() {
		return fieldsForEquality;
	}

	@Override
	public UID getResource() {
		return resource;
	}

	@Override
	public String getNuclosResource() {
		return nuclosResource;
	}

	@Override
	public String getLocaleResourceIdForLabel() {
		return localeResourceIdForLabel;
	}

	@Override
	public String getLocaleResourceIdForDescription() {
		return localeResourceIdForDescription;
	}

	@Override
	public String getLocaleResourceIdForMenuPath() {
		return localeResourceIdForMenuPath;
	}

	@Override
	public String getLocaleResourceIdForTreeView() {
		return localeResourceIdForTreeView;
	}

	@Override
	public String getLocaleResourceIdForTreeViewDescription() {
		return localeResourceIdForTreeViewDescription;
	}

	@Override
	public UID[][] getLogicalUniqueFieldCombinations() {
		return logicalUniqueFieldCombinations;
	}

	public void setLogicalUniqueFieldCombinations(
		UID[][] logicalUniqueFieldCombinations) {
		this.logicalUniqueFieldCombinations = logicalUniqueFieldCombinations;
	}

	@Override
	public UID[][] getUniqueFieldCombinations() {
		return uniqueFieldCombinations;
	}

	public void setUniqueFieldCombinations(
			UID[][] uniqueFieldCombinations) {
		this.uniqueFieldCombinations = uniqueFieldCombinations;
	}

	public UID[][] getIndexFieldCombinations() {
		return indexFieldCombinations;
	}

	public void setIndexFieldCombinations(UID[][] indexFieldCombinations) {
		this.indexFieldCombinations = indexFieldCombinations;
	}

	@Override
	public boolean isDynamic() {
    	return dynamic;
    }
	
	@Override
	public UID getDataSource() {
		return uidDataSource;
	}

	public void setDynamic(boolean dynamic) {
    	this.dynamic = dynamic;
    }
	
	@Override
	public boolean isDynamicTasklist() {
		return dynamicTasklist;
	}

	public void setDynamicTasklist(boolean dynamicTasklist) {
		this.dynamicTasklist = dynamicTasklist;
	}

	@Override
	public boolean isChart() {
		return chart;
	}

	public void setChart(boolean chart) {
		this.chart = chart;
	}
	
	@Override
	public UID getDetailEntity() {
		return this.detailEntity;
	}
	
	public void setDetailEntity(UID detailEntity) {
		this.detailEntity = detailEntity;
	}
	
	@Override
	public UID getParentEntity() {
		return this.parentEntity;
	}
	
	public void setParentEntity(UID parentEntity) {
		this.parentEntity = parentEntity;
	}

	@Override
	public boolean isShowSearch() {
		return showSearch;
	}

	public void setShowSearch(Boolean showSearch) {
		this.showSearch = showSearch != null ? Boolean.TRUE.equals(showSearch) : Boolean.TRUE;
	}
	
	@Override
	public boolean isThin() {
		return thin;
	}
	
	public void setThin(Boolean thin) {
		this.thin = Boolean.TRUE.equals(thin);
	}

	@Override
	public String getVirtualEntity() {
		return virtualentity;
	}

	public void setVirtualEntity(String virtualentity) {
		this.virtualentity = StringUtils.stripToNull(virtualentity);
	}

	public boolean isVirtual() {
		return !StringUtils.isBlank(virtualentity);
	}
	
	@Override
	public String getIdFactory() {
		return idFactory;
	}
	
	public void setIdFactory(String idFactory) {
		this.idFactory = StringUtils.stripToNull(idFactory);
	}
	
	@Override
	public String getReadDelegate() {
		return readDelegate;
	}
	
	public void setReadDelegate(String readDelegate) {
		this.readDelegate = readDelegate;
	}

	@Override
	public NuclosScript getRowColorScript() {
		return rowColorScript;
	}

	public void setRowColorScript(NuclosScript rowColorScript) {
		this.rowColorScript = rowColorScript;
	}
	
	public void setUidEntity(boolean uidEntity) {
		this.uidEntity = uidEntity;
	}
	
	@Override
	public boolean isUidEntity() {
		return this.uidEntity;
	}

	@Override
	public final UID getCloneGenerator() {
		return cloneGenerator;
	}

	public final void setCloneGenerator(UID cloneGenerator) {
		this.cloneGenerator = cloneGenerator;
	}

	@Override
	public final UID getMandatorLevel() {
		return mandatorLevel;
	}

	public final void setMandatorLevel(UID mandatorLevel) {
		this.mandatorLevel = mandatorLevel;
	}

	@Override
	public boolean isMandatorUnique() {
		return mandatorUnique;
	}

	public void setMandatorUnique(final Boolean mandatorUnique) {
		this.mandatorUnique = Boolean.TRUE.equals(mandatorUnique);
	}

	@Override
	public boolean isProxy() {
		return Boolean.TRUE.equals(proxy);
	}

	public void setProxy(Boolean proxy) {
		this.proxy = Boolean.TRUE.equals(proxy);
	}

	@Override
	public boolean isWriteProxy() {
		return Boolean.TRUE.equals(writeProxy);
	}

	public void setWriteProxy(Boolean writeProxy) {
		this.writeProxy = Boolean.TRUE.equals(writeProxy);
	}
	
	@Override
	public boolean isGeneric() {
		return Boolean.TRUE.equals(generic);
	}

	public void setGeneric(Boolean generic) {
		this.generic = Boolean.TRUE.equals(generic);
	}

	public LockMode getLockMode() {
		return lockMode;
	}

	public void setLockMode(String sLockMode) {
		this.lockMode = sLockMode==null?null:LockMode.valueOf(sLockMode);
	}
	
	public void setLockMode(LockMode lockMode) {
		this.lockMode = lockMode;
	}

	public UnlockMode getUnlockMode() {
		return unlockMode;
	}

	public void setUnlockMode(String sUnlockMode) {
		this.unlockMode = sUnlockMode==null?null:UnlockMode.valueOf(sUnlockMode);
	}
	
	public void setUnlockMode(UnlockMode unlockMode) {
		this.unlockMode = unlockMode;
	}

	public String getOwnerForeignEntityField() {
		return ownerForeignEntityField;
	}
	
	public void setOwnerForeignEntityField(String ownerForeignEntityField) {
		this.ownerForeignEntityField = ownerForeignEntityField;
	}

	@Override
	public Set<EntityContext> getEntityContexts() {
		if (entityContexts == null) {
			return super.getEntityContexts();
		}
		return entityContexts;

	}

	public void setEntityContexts(Set<EntityContext> entityContexts) {
		if (entityContexts == null) {
			entityContexts = Collections.emptySet();
		}
		this.entityContexts = Collections.unmodifiableSet(entityContexts);
	}

}
