//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.io.Serializable;


public class Mutable<T> implements Serializable {

	private static final long serialVersionUID = -8435110964473890492L;
	
	/** The mutable value. */
    private T value;

    public Mutable() {
        super();
    }

    public Mutable(T value) {
        super();
        this.value = value;
    }

    /**
     * @return the value, may be null
     */
    public T getValue() {
        return this.value;
    }

    /**
     * @param value  the value to set
     */
    public T setValue(T value) {
        this.value = value;
        return value;
    }

    public boolean equals(Object obj) {
        if (obj instanceof Mutable) {
            Object other = ((Mutable<?>) obj).value;
            return value == other || (value != null && value.equals(other));
        }
        return false;
    }

    public int hashCode() {
        return value == null ? 0 : value.hashCode();
    }

    public String toString() {
        return value == null ? "null" : value.toString();
    }
    
}
