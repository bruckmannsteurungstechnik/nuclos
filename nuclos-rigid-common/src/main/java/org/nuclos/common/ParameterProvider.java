//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.lang.reflect.Field;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Provides system parameters.
 *
 * TODO: Use an enum for static parameters.
 *
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @version 01.00.00
 * @author    <a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 */
public interface ParameterProvider {

	/**
	 * @deprecated Multinuclet: MasterDataVO has no method validate() any more.
	 */
	String KEY_SERVER_VALIDATES_MASTERDATAVALUES = "Server validates masterdata values";

	String KEY_MAX_ROWCOUNT_FOR_SEARCHRESULT_IN_TREE = "Max row count for search result in tree";
	String KEY_MAX_ROWCOUNT_FOR_SEARCHRESULT_IN_TASKLIST = "Max row count for search result in tasklist";

	String KEY_EXCEL_SHEET_NAME = "Excel Sheet Name 4pm Report";

	String KEY_REPORT_MAXROWCOUNT = "Report Max Row Count";

	String KEY_FOCUSSED_ITEM_BACKGROUND_COLOR = "Focussed item background color";
	String KEY_MANDATORY_ITEM_BACKGROUND_COLOR = "Mandatory item background color";
	String KEY_MANDATORY_ADDED_ITEM_BACKGROUND_COLOR = "Mandatory added item background color";

	String KEY_HISTORICAL_STATE_CHANGED_COLOR = "Historical state changed color";
	String KEY_HISTORICAL_STATE_NOT_TRACKED_COLOR = "Historical state not tracked color";

	String KEY_GENERICOBJECT_IMPORT_EXCLUDED_ATTRIBUTES_FOR_UPDATE = "Generic Object Import Excluded Attributes For Update";
	String KEY_GENERICOBJECT_IMPORT_EXCLUDED_ATTRIBUTES_FOR_CREATE = "Generic Object Import Excluded Attributes For Create";

	String KEY_CLIENT_VALIDATION_LAYER_PAINTER_NAME = "Painter name for client validation layer";

	String KEY_DEFAULT_LOCALE = "Default Locale";
	String KEY_DEFAULT_ENCODING = "Default Encoding";
	String SORT_CALCULATED_ATTRIBUTES = "SORT_CALCULATED_ATTRIBUTES";
	String DATA_CHUNK_SIZE = "DATA_CHUNK_SIZE";
	String PAGE_JUMP_LIMIT = "PAGE_JUMP_LIMIT";
	String ESSENTIAL_ENTITY_FIELDS = "ESSENTIAL_ENTITY_FIELDS";
	String QUICKSEARCH_DELAY_TIME = "QUICKSEARCH_DELAY_TIME";
	String NUMBER_MAX_SORT_ATTRIBUTES = "NUMBER_MAX_SORT_ATTRIBUTES";
	String REST_MENU_ENTITIES = "REST_MENU_ENTITIES";
	String KEY_FILECHOOSER_DONT_REMEMBER_PATH = "FILECHOOSER_DONT_REMEMBER_PATH";
	String QUERY_TIMEOUT = "QUERY_TIMEOUT";
	String USE_PK_SUBSELECT = "USE_PK_SUBSELECT";
	String USE_REF_SUBSELECT = "USE_REF_SUBSELECT";
	String COUNT_RESULT_LIST = "COUNT_RESULT_LIST";
	String KEY_ESTIMATE_COUNT = "ESTIMATE_COUNT";

	String KEY_MAINTENANCE_INIT_WAIT_TIME_IN_MINUTES = "MAINTENANCE_INIT_WAIT_TIME_IN_MINUTES";
	/**
	 * JARs to add to the JasperReports compile classpath.
	 * JARs are selected by (fully qualified) classnames, separated by spaces.
	 * <p>
	 * This is needed for JasperReports scriptlets.
	 * </p><p>
	 * Default (and example):
	 * 'net.sf.jasperreports.engine.JasperReport org.nuclos.server.report.api.JRNuclosDataSource'
	 * </p>
	 *
	 * @author Thomas Pasch
	 * @since Nuclos 3.9
	 */
	String JASPER_REPORTS_COMPILE_CPJAR_BYCLASSES
			= "nuclos.jasper.reports.compile.classpath.jars.byclasses";

	/**
	 * @deprecated Get rid of this!
	 */
	String JMS_MESSAGE_ALL_PARAMETERS_ARE_REVALIDATED = "All parameters are revalidated.";

	String KEY_TEMPLATE_USER = "PREFERENCES_TEMPLATE_USER";

	String KEY_DATASOURCE_TABLE_FILTER = "Datasource Table Filter";

	String KEY_SMTP_AUTHENTICATION = "SMTP Authentication";
	String KEY_SMTP_USERNAME = "SMTP Username";
	String KEY_SMTP_PASSWORD = "SMTP Password";
	String KEY_SMTP_SENDER = "SMTP Sender";
	String KEY_SMTP_SERVER = "SMTP Server";
	String KEY_SMTP_PORT = "SMTP Port";
	String KEY_SMTP_SSL = "SMTP SSL";
	String KEY_SMTP_STARTTLS = "SMTP STARTTLS";

	String KEY_POP3_USERNAME = "POP3 Username";
	String KEY_POP3_PASSWORD = "POP3 Password";
	String KEY_POP3_SERVER = "POP3 Server";
	String KEY_POP3_PORT = "POP3 Port";

	String KEY_IMAP_PROTOCOL = "IMAP Protocol";
	String KEY_IMAP_USERNAME = "IMAP Username";
	String KEY_IMAP_PASSWORD = "IMAP Password";
	String KEY_IMAP_SERVER = "IMAP Server";
	String KEY_IMAP_PORT = "IMAP Port";
	String KEY_IMAP_FOLDER_FROM = "IMAP Folder From";
	String KEY_IMAP_FOLDER_TO = "IMAP Folder To";

	String KEY_MAILGET_SSL = "MAILGET SSL";

	String KEY_CIPHER = "server.cryptfield.cipher";

	String KEY_RESPLAN_RESOURCE_LIMIT = "ResPlan Resource Limit";

	String KEY_THUMBAIL_SIZE = "THUMBNAIL_SIZE";

	String KEY_TIMELIMIT_RULE_USER = "Timelimit Rule User";

	String KEY_DRAG_CURSOR_HOLDING_TIME = "DRAG_CURSOR_HOLDING_TIME";

	String KEY_SECURITY_LOCK_DAYS = "SECURITY_LOCK_USER_PERIOD";
	String KEY_SECURITY_LOCK_ATTEMPTS = "SECURITY_LOCK_ATTEMPTS";
	String KEY_SECURITY_PASSWORD_INTERVAL = "SECURITY_PASSWORD_INTERVAL";
	String KEY_SECURITY_PASSWORD_HISTORY_NUMBER = "SECURITY_PASSWORD_HISTORY_NUMBER";
	String KEY_SECURITY_PASSWORD_HISTORY_DAYS = "SECURITY_PASSWORD_HISTORY_DAYS";
	String KEY_SECURITY_PASSWORD_STRENGTH_LENGTH = "SECURITY_PASSWORD_STRENGTH_LENGTH";
	String KEY_SECURITY_PASSWORD_STRENGTH_REGEXP = "SECURITY_PASSWORD_STRENGTH_REGEXP";
	String KEY_SECURITY_PASSWORD_STRENGTH_DESCRIPTION = "SECURITY_PASSWORD_STRENGTH_DESCRIPTION";

	String KEY_SHOW_INTERNAL_TIMESTAMP_WITH_TIME = "SHOW_INTERNAL_TIMESTAMP_WITH_TIME";

	String KEY_DEFAULT_NUCLOS_THEME = "DEFAULT_NUCLOS_THEME";
	String KEY_NUCLOS_FONT_FAMILY = "NUCLOS_FONT_FAMILY";
	String KEY_ONLINE_HELP = "NUCLOS_ONLINE_HELP";
	String KEY_NUCLOS_INSTANCE_NAME = "NUCLOS_INSTANCE_NAME";
	String KEY_LAYOUT_CUSTOM_KEY = "LAYOUT_CUSTOM_KEY";

	String KEY_ACTIVATE_LOGBOOK = "ACTIVATE_LOGBOOK";

	String KEY_ROOTPANE_BACKGROUND_COLOR = "ROOTPANE_BACKGROUND_COLOR";

	String KEY_VLP_LOADINGTHREAD_KEEP_ALIVE = "VLP_LOADING_THREAD_KEEP_ALIVE";
	String KEY_VLP_RESULTCACHE_EXPIRATION = "VLP_RESULT_CACHE_EXPIRATION";

	String KEY_CLIENT_READ_TIMEOUT = "CLIENT_READ_TIMEOUT";

	String KEY_SUPPRESS_USER_NOTIFICATION_EMAIL = "SUPPRESS_USER_NOTIFICATION_EMAIL";

	/**
	 * System default is true.
	 *
	 * @since Nuclos 4.6.0
	 */
	String KEY_DEFAULT_SUBFORM_MULTIEDITING = "DEFAULT_SUBFORM_MULTIEDITING";

	/**
	 * System default is true.
	 *
	 * @since Nuclos 4.9.0
	 */
	String KEY_DEFAULT_FIELD_MULTIEDITING = "DEFAULT_FIELD_MULTIEDITING";

	/**
	 * System default is "512m".
	 */
	String KEY_CLIENT_MAX_HEAP_SIZE = "CLIENT_MAX_HEAP_SIZE";

	/**
	 * Activate cross origin resource sharing for the REST service. Default: false
	 */
	String KEY_REST_ACTIVATE_CORS = "REST_ACTIVATE_CORS";

	/**
	 * Activate User Access
	 */
	String KEY_ANONYMOUS_USER_ACCESS_ENABLED = "ANONYMOUS_USER_ACCESS_ENABLED";

	String KEY_WEBCLIENT_CSS = "WEBCLIENT_CSS";

	/**
	 * Activate self registration and define role name for new users
	 */
	String KEY_ROLE_FOR_SELF_REGISTERED_USERS = "ROLE_FOR_SELF_REGISTERED_USERS";

	/**
	 * Activate self registration and define role name for new users
	 */
	String KEY_ACTIVATION_EMAIL_SUBJECT = "ACTIVATION_EMAIL_SUBJECT";

	/**
	 * Activate self registration and define role name for new users
	 */
	String KEY_ACTIVATION_EMAIL_MESSAGE = "ACTIVATION_EMAIL_MESSAGE";

	/**
	 * Remember username for self registration users
	 */
	String KEY_USERNAME_EMAIL_SUBJECT = "USERNAME_EMAIL_SUBJECT";

	/**
	 * Remember username for self registration users
	 */
	String KEY_USERNAME_EMAIL_MESSAGE = "USERNAME_EMAIL_MESSAGE";

	/**
	 * Reset password for self registration users
	 */
	String KEY_RESET_PW_EMAIL_SUBJECT = "RESET_PW_EMAIL_SUBJECT";

	/**
	 * Reset password for self registration users
	 */
	String KEY_RESET_PW_EMAIL_MESSAGE = "RESET_PW_EMAIL_MESSAGE";

	String EMAIL_SIGNATURE = "EMAIL_SIGNATURE";

	String INITIAL_ENTRY = "INITIAL_ENTRY";

	String LOCALE_OPTIONS = "LOCALE_OPTIONS";

	/**
	 * http://support.nuclos.de/browse/NUCLOS-6200
	 */
	String DEPRECATED_2017_ALLOWED = "DEPRECATED_2017_ALLOWED";

	/**
	 * NUCLOS-6708 Default=On
	 */
	String JOBS_AUTOSTARTING = "JOBS_AUTOSTARTING";

	String KEY_MAINTENANCE_MODE_SURVIVE_RESTART = "KEY_MAINTENANCE_MODE_SURVIVE_RESTART";

	/**
	 * Used within Integrationstests
	 */
	String BUSINESS_TEST_ROLLBACK = "BUSINESS_TEST_ROLLBACK";
	String BUSINESS_TEST_ROLLBACK_BETWEEN_TESTS = "BUSINESS_TEST_ROLLBACK_BETWEEN_TESTS";
	String PARAM_WEBCLIENT_SRC_DIR_FOR_DEVMODE = "WEBCLIENT_SRC_DIR_FOR_DEVMODE";

	/*
	 * NUCLOS-7229 Default=false
	 * If parameter is set to true the contents of textfields where the content is not completely visible are cut
	 * and suffixed with an ellipsis. Furthermore the textfield's content can be conveniently presented and edited
	 * in a popup-textarea, that can be opened by pressing CTRL and clicking into the textfield.
	 */
	String SHOW_OVERLAY_FOR_TOO_SMALL_TEXTFIELDS = "SHOW_OVERLAY_FOR_TOO_SMALL_TEXTFIELDS";

	/*
	 * NUCLOS-7605 Default=0
	 * Pad the content of numeric cells (always right aligned) in result table and subform by this amount of pixels on
	 * the right to provide enhanced readability.
	 */

	String PADDING_RIGHT_NUMERIC_CELLS = "PADDING_RIGHT_NUMERIC_CELLS";

	String USE_OLD_LOCK_STYLE = "USE_OLD_LOCK_STYLE";

	/**
	 * See <a href="http://support.nuclos.de/browse/NUCLOS-7797">NUCLOS-7797</a>
	 */
	String SECURITY_IP_BLOCK_ATTEMPTS = "SECURITY_IP_BLOCK_ATTEMPTS";
	String SECURITY_IP_BLOCK_PERIOD_IN_SECONDS = "SECURITY_IP_BLOCK_PERIOD_IN_SECONDS";
	String SECURITY_IP_BLOCK_PROXY_LIST = "SECURITY_IP_BLOCK_PROXY_LIST";

	String SECURITY_SESSION_TIMEOUT_IN_SECONDS = "SECURITY_SESSION_TIMEOUT_IN_SECONDS";

	String SWAGGER_ACTIVE = "SWAGGER_ACTIVE";

	String MODIFIABLE_READONLY = "MODIFIABLE_READONLY";

	/**
	 * @param sParameterName
	 * @return the value for the parameter with the given name, if any.
	 */
	String getValue(String sParameterName);

	/**
	 * Gets an int value.
	 *
	 * @param sParameterName name of parameter
	 * @param iDefaultValue  default value for parameter if not set
	 * @return the int value from the parameters or the default value.
	 */
	int getIntValue(String sParameterName, int iDefaultValue);

	/**
	 * @param sParameterName
	 * @return the boolean value from the parameters or the default [false] if parameter not set
	 */
	boolean isEnabled(String sParameterName);

	/**
	 * @param sParameterName
	 * @param bDefaultValue  default value for parameter if not set
	 * @return the boolean value from the parameters or the default if parameter not set
	 */
	boolean isEnabled(String sParameterName, boolean bDefaultValue);

	String getJRClassPath();

	List<String> LSTSYSTEMPARAMS = new ArrayList<>();
	static List<String> listSystemParameter() {
		if (LSTSYSTEMPARAMS.isEmpty()) {
			try {
				Field[] fields = ParameterProvider.class.getDeclaredFields();
				for (Field field : fields) {
					Object value = field.get(null);
					if (value instanceof String) {
						LSTSYSTEMPARAMS.add((String)value);
					}
				}
			} catch (ReflectiveOperationException roe) {
				throw new RuntimeException(roe);
			}
			Collections.sort(LSTSYSTEMPARAMS);
		}
		return LSTSYSTEMPARAMS;
	}

	static UID createFixUIDForParam(String param) {
		long seed = ((long)param.hashCode()) | (20012938023L >> 32L);
		SecureRandom random = RigidUtils.getSecureRandom();
		random.setSeed(seed);
		return new UID(UID.generateStringUsingRandom(random, 20));
	}

}    // interface ParameterProvider
