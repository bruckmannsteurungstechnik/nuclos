package org.nuclos.common;

public class RecordGrantMeta extends EntityMeta<Long> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 811680037879951884L;
	private final String sql;
	private final String alias;
	private final boolean bHasCanSomething;
	
	public RecordGrantMeta(String sql, String alias, boolean bHasCanSomething) {
		this.sql = sql;
		this.alias = alias;
		this.bHasCanSomething = bHasCanSomething;
	}

	@Override 
	public UID getUID() {
		return null;
	}
	
	@Override 
	public Class<Long> getPkClass() {
		return Long.class;
	}
	
	
	@Override public String getDbTable() {
		return "(" + sql + ")";
	}
	
	@Override 
	public String getEntityName() {
		return alias;
	}
	
	public boolean hasCanSomething() {
		return bHasCanSomething;
	}

}
