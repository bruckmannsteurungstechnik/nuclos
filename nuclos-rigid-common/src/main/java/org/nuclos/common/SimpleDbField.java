package org.nuclos.common;


public class SimpleDbField<T> implements DbField<T> {

	private final String dbColumn;
	private final Class<T> javaClass;
	
	protected SimpleDbField(String dbColumn, Class<T> javaClass) {
		super();
		this.dbColumn = dbColumn;
		this.javaClass = javaClass;
	}

	@Override
	public String getDbColumn() {
		return dbColumn;
	}

	@Override
	public Class<T> getJavaClass() {
		return javaClass;
	}
	
	public static <T> DbField<T> create(String dbColumn, Class<T> javaClass) {
		return new SimpleDbField<T>(dbColumn, javaClass);
	}

	/**
	 * Return a real reference to foreign entity.
	 * 
	 * @since Nuclos 4.0.19
	 * @author Thomas Pasch
	 */
	public static <T> DbField<T> createRef(String dbColumn, boolean isUid) {
		final DbField<T> result;
		if (isUid) {
			result = (DbField<T>) new SimpleDbField<UID>(dbColumn.replaceFirst("^STRVALUE", "STRUID"), UID.class);
		} else {
			result = (DbField<T>) new SimpleDbField<Long>(dbColumn.replaceFirst("^STRVALUE", "INTID"), Long.class);			
		}
		return result;
	}

	@Override
	public String toString() {
		return String.format("SimpleDbField[dbColumn=%s, javaClass=%s", dbColumn, javaClass);
	}
	
	
	
}
