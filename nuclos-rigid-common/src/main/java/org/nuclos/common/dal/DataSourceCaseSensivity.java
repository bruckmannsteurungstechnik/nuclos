package org.nuclos.common.dal;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;

public class DataSourceCaseSensivity {

	public static final String PRIMARY_KEY = "INTID";
	public static final String REF_ENTITY = "INTID_T_UD_GENERICOBJECT";
	
	private final boolean bIntidCaseInsensitive;
	private final boolean bIntidGenericObjectCaseInsensitive;
	
	public DataSourceCaseSensivity(EntityMeta<?> eMeta) {
		this(getSql(eMeta));
	}
	
	private static String getSql(EntityMeta<?> eMeta) {
		String sql = null;
		if (eMeta != null) {
			sql = eMeta.getDbSelect();
		}
		return sql;
	}
	
	public DataSourceCaseSensivity(String sql) {
		boolean bIntidCaseInsensitive = true;
		boolean bIntidGenericObjectCaseInsensitive = true;
		if (sql != null) {
			String sqlUpper = sql.toUpperCase();
			if (sqlUpper.indexOf(" \"" + PRIMARY_KEY + "\"") != -1) {
				bIntidCaseInsensitive = false;
			}
			if (sqlUpper.indexOf(" \"" + REF_ENTITY + "\"") != -1) {
				bIntidGenericObjectCaseInsensitive = false;
			}
		} 
		
		this.bIntidCaseInsensitive = bIntidCaseInsensitive;
		this.bIntidGenericObjectCaseInsensitive = bIntidGenericObjectCaseInsensitive;
	}

	public boolean isIntidCaseInsensitive() {
		return bIntidCaseInsensitive;
	}

	public boolean isIntidGenericObjectCaseInsensitive() {
		return bIntidGenericObjectCaseInsensitive;
	}
	
	public boolean isCaseSensitive(FieldMeta<?> fMeta) {
		if (fMeta != null) {
			if (PRIMARY_KEY.equals(fMeta.getDbColumn())) {
				return !isIntidCaseInsensitive();
			}
			
			if (REF_ENTITY.equals(fMeta.getDbColumn())) {
				return !isIntidGenericObjectCaseInsensitive();
			}
			
		}
		
		return false;
	}
	
	public static String getFieldWithCorrectCaseAndQuotation(String sField, String sql) {
		
		String sqlUpper = sql.toUpperCase();
		String sFieldUpperWithQuotes = "\"" + sField.toUpperCase() + "\"";
		String sFieldUpperWithBlankAndQuotes = " " + sFieldUpperWithQuotes;
		
		//NUCLOS-4294
		//First is there any quotation match with a blank before, regardless of case?
		if (sqlUpper.contains(sFieldUpperWithBlankAndQuotes)) {
			return getWithQuotation(sField, sql, sFieldUpperWithBlankAndQuotes);
		}
		
		//Second is there any quotation match, regardless of case?
		if (sqlUpper.contains(sFieldUpperWithQuotes)) {
			return getWithQuotation(sField, sql, sFieldUpperWithQuotes);
		}
		
		return sField;
	
	}
	
	private static String getWithQuotation(String sField, String sql, String sFieldUpperWithQuotes) {
		String sFieldLowerWithQuotes = sFieldUpperWithQuotes.toLowerCase();
		
		if (sql.contains(sFieldUpperWithQuotes)) {
			return sFieldUpperWithQuotes.trim();
			
		} else if (sql.contains(sFieldLowerWithQuotes)){
			return sFieldLowerWithQuotes.trim();
			
		}
		
		return "\"" + sField + "\"";
	}

}
