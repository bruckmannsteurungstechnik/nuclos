package org.nuclos.common2;

import java.io.IOException;
import java.io.OutputStream;

public interface BoDataSet {
	
	void write(OutputStream output) throws IOException;
	
}
