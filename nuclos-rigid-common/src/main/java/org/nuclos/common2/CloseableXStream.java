package org.nuclos.common2;

import java.io.Closeable;

import org.apache.commons.pool.impl.GenericObjectPool;

import com.thoughtworks.xstream.XStream;

/**
 * This should be used in a try-with-resources statement,
 * so the underlying XStream will be automatically closed.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class CloseableXStream implements Closeable {
	private final GenericObjectPool<XStream> pool;
	private XStream stream;

	CloseableXStream(final GenericObjectPool<XStream> pool) {
		this.pool = pool;
	}

	public XStream getStream() {
		if (stream == null) {
			try {
				stream = pool.borrowObject();
			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		}

		return stream;
	}

	@Override
	public void close() {
		if (stream != null) {
			try {
				pool.returnObject(stream);
			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		}
	}
}
