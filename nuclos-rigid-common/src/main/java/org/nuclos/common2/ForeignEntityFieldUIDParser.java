//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.dblayer.FieldUIDRefIterator;
import org.nuclos.common.dblayer.IFieldUIDRef;

/**
 * Parser for the expression language used in field references.
 *  
 * @author Maik Stueker
 * @since Nuclos 4.0.00
 * @see org.nuclos.common.FieldMeta#getForeignEntityField()
 */
public class ForeignEntityFieldUIDParser implements Iterable<IFieldUIDRef> {
	
	private static final Logger LOG = Logger.getLogger(ForeignEntityFieldUIDParser.class);
	
	private static final Pattern OLD_SIMPLE_REF_PATTERN = Pattern.compile("\\p{Alpha}[\\p{Alnum}_]*");
	
	public static final Pattern REF_PATTERN = Pattern.compile("uid\\{([a-zA-z0-9\\._-]*)\\}", Pattern.MULTILINE);
	
	private String ffieldname;
	
	public ForeignEntityFieldUIDParser(String foreignEntityField, String lookupEntityField, UID nameFieldIfExist) {		
		String fef = StringUtils.defaultString(foreignEntityField, lookupEntityField);
		if (fef == null) {	
			if (nameFieldIfExist != null) {
				this.ffieldname = String.format("uid{%s}", nameFieldIfExist.getString());
			}
			if (this.ffieldname == null) {
				throw new IllegalArgumentException(String.format("RefField is empty and \"name\" not found"));
			}
			LOG.debug("Null/empty foreignEntityField in expression in " + foreignEntityField 
					+ " is deprecated, use " + this.ffieldname + " instead!");
		}
		else if (OLD_SIMPLE_REF_PATTERN.matcher(fef).matches()) {
			LOG.debug("Old style foreignEntityField expression '" + fef + "' in "
					+ foreignEntityField + " is deprecated, please enclose it: ${" + fef + "}");
			this.ffieldname = "uid{" + fef + "}";
		}
		else {
			this.ffieldname = fef;
		}
	}
	
	public ForeignEntityFieldUIDParser(FieldMeta<?> foreignEntityField, IRigidMetaProvider mdprov) {
		this(foreignEntityField, mdprov, false);
	}
	
	public ForeignEntityFieldUIDParser(FieldMeta<?> foreignEntityField, IRigidMetaProvider mdprov, boolean bSearch) {
		if (foreignEntityField == null || mdprov == null) {
			throw new NullPointerException();
		}
		// this.foreignEntityField = foreignEntityField;
		
		EntityMeta<?> fe = null;
		String fef = null;
		
		if (bSearch && !RigidUtils.isNullOrEmpty(foreignEntityField.getSearchField())) {
			fef = foreignEntityField.getSearchField();
		}
			
		if (fef == null) {
			if (foreignEntityField.getForeignEntity() != null) {
				fe = mdprov.getEntity(foreignEntityField.getForeignEntity());
				fef = foreignEntityField.getForeignEntityField();
			} else if (foreignEntityField.getUnreferencedForeignEntity() != null) {
				fe = mdprov.getEntity(foreignEntityField.getUnreferencedForeignEntity());
				fef = foreignEntityField.getUnreferencedForeignEntityField();
			} else if (foreignEntityField.getLookupEntity() != null) {
				fe = mdprov.getEntity(foreignEntityField.getLookupEntity());
				fef = foreignEntityField.getLookupEntityField();
			}
		}
		if (fef == null) {
			for (FieldMeta<?> fm : fe.getFields()) {
				if ("name".equals(fm.getFieldName())) {
					this.ffieldname = String.format("uid{%s}", fm.getUID().getString());
				}
			}
			if (this.ffieldname == null) {
				throw new IllegalArgumentException(String.format("RefField is empty and \"name\" not found in %s", fe.getUID()));
			}
			LOG.debug("Null/empty foreignEntityField in expression in " + foreignEntityField 
					+ " is deprecated, use " + this.ffieldname + " instead!");
		}
		else if (OLD_SIMPLE_REF_PATTERN.matcher(fef).matches()) {
			LOG.debug("Old style foreignEntityField expression '" + fef + "' in "
					+ foreignEntityField + " is deprecated, please enclose it: ${" + fef + "}");
			this.ffieldname = "uid{" + fef + "}";
		}
		else {
			this.ffieldname = fef;
		}
	}

	@Override
	public Iterator<IFieldUIDRef> iterator() {
		return new FieldUIDRefIterator(REF_PATTERN, ffieldname);
	}
	
	public static boolean isWithinStringifiedReferences(FieldMeta<?> fm, Collection<FieldMeta<?>> collReferencing) {
		
		for (FieldMeta<?> meta : collReferencing) {
			
			if (meta.getForeignEntityField() != null) {
				Iterator<IFieldUIDRef> it = new FieldUIDRefIterator(ForeignEntityFieldUIDParser.REF_PATTERN, meta.getForeignEntityField());
				
				while (it.hasNext()) {
					IFieldUIDRef i = it.next();
					
					if (!i.isConstant() && fm.getUID().equals(i.getUID())) {
						return true;
					}
				}

			}
		}
		
		return false;
	}

	// NUCLOS-6375: Find out which class has the referencing expression, recursively.
	// If it has other than one field, return always String
	public Class<?> getReferencingClass(IRigidMetaProvider mdprov) {
		List<IFieldUIDRef> refFields = new ArrayList<>();
		Iterator<IFieldUIDRef> it = iterator();
		while (it.hasNext()) {
			IFieldUIDRef ref = it.next();
			if (!ref.isConstant()) {
				refFields.add(ref);
			}
		}

		if (refFields.size() != 1) {
			return String.class;
		}

		IFieldUIDRef ref = refFields.get(0);
		if (!ref.isUID()) {
			return String.class;
		}

		FieldMeta<?> fm = mdprov.getEntityField(ref.getUID());
		if (fm.getForeignEntity() != null && fm.getForeignEntityField() != null) {
			// Another reference: Seek recursively!
			ForeignEntityFieldUIDParser parser = new ForeignEntityFieldUIDParser(fm.getForeignEntityField(), null, null);
			return parser.getReferencingClass(mdprov);
		}

		return fm.getJavaClass();
	}

}
