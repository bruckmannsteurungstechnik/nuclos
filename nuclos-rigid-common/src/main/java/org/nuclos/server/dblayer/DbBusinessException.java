//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dblayer;

public class DbBusinessException extends DbException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 765514812079317805L;

	public DbBusinessException(Object pk, String message, Throwable cause) {
		super(pk, message, cause);
	}

	@Override
	public String toString() {
		return getMessage();
	}
	
	public static <T extends Exception> void extractAndThrowDbBusinessExceptionIfAny(T ex) throws T {
		DbBusinessException business = getDbBusinessExceptionIfAny(ex);
		if (business != null) {
			throw business;
		}
		throw ex;
	}

	public static DbBusinessException getDbBusinessExceptionIfAny(Throwable th) {
		if (th instanceof DbBusinessException) {
			return (DbBusinessException) th;
		}
		if (th != null && th.getCause() != null) {
			return getDbBusinessExceptionIfAny(th.getCause());
		}
		return null;
	}
	
}
