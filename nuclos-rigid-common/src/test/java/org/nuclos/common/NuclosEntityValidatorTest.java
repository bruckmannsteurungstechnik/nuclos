package org.nuclos.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.lang.NotImplementedException;
import org.junit.Test;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class NuclosEntityValidatorTest {
	IRigidMetaProvider metaProvider = new TestMetaProvider();
	List<TestImport> imports = Arrays.asList(TestImport.values());

	enum TestImport implements INuclosEntityImport {
		LIST(java.util.List.class),
		SET(java.util.Set.class);

		private final Class<?> cls;

		TestImport(final Class<?> cls) {
			this.cls = cls;
		}

		@Override
		public Class<?> getImportClass() {
			return cls;
		}
	}

	@Test
	public void testEscapeIdentifier() {
		assert NuclosEntityValidator.isValidJavaIdentifier(NuclosEntityValidator.escapeJavaIdentifier("test"));
		assert NuclosEntityValidator.isValidJavaIdentifier(NuclosEntityValidator.escapeJavaIdentifier("_test"));
		assert NuclosEntityValidator.isValidJavaIdentifier(NuclosEntityValidator.escapeJavaIdentifier("123test"));
		assert NuclosEntityValidator.isValidJavaIdentifier(NuclosEntityValidator.escapeJavaIdentifier(" test"));
		assert NuclosEntityValidator.isValidJavaIdentifier(NuclosEntityValidator.escapeJavaIdentifier("äöü"));

		testDoubleEscaping("_test");
		testDoubleEscaping("123test");
		testDoubleEscaping(" test");
		testDoubleEscaping("äöü");

		testInvalidPrefix("123");
		testInvalidPrefix(".invalidPrefix");
		testInvalidPrefix("");
		testInvalidPrefix(null);

		testInvalidIdentifier("");
		testInvalidIdentifier(null);
		testInvalidIdentifier(".");
		testInvalidIdentifier("???");
	}

	@Test
	public void testEscapeIdentifierPart() {
		assert StringUtils.equals(NuclosEntityValidator.escapeJavaIdentifierPart("test"), "test");
		assert StringUtils.equals(NuclosEntityValidator.escapeJavaIdentifierPart("123test"), "123test");
		assert StringUtils.equals(NuclosEntityValidator.escapeJavaIdentifierPart(" test"), "test");
		assert StringUtils.equals(NuclosEntityValidator.escapeJavaIdentifierPart("   1"), "1");

		try {
			NuclosEntityValidator.escapeJavaIdentifierPart("   ");
			assert false : "Expected exception did not occur";
		} catch (Exception ex) {
			// expected
		}
	}

	private void testInvalidIdentifier(final String identifier) {
		try {
			NuclosEntityValidator.escapeJavaIdentifier(identifier);
			assert false : "Expected exception did not occur";
		} catch (IllegalArgumentException ex) {
			// expected
			return;
		}
		assert false : "Expected exception did not occur";
	}

	private void testInvalidPrefix(final String prefix) {
		try {
			NuclosEntityValidator.escapeJavaIdentifier("Test", prefix);
			assert false : "Expected exception did not occur";
		} catch (IllegalArgumentException ex) {
			// expected
			return;
		}
		assert false : "Expected exception did not occur";
	}

	private void testDoubleEscaping(final String name) {
		final String identifier1 = NuclosEntityValidator.escapeJavaIdentifier(name);
		final String identifier2 = NuclosEntityValidator.escapeJavaIdentifier(identifier1);
		assert StringUtils.equals(identifier1, identifier2);
	}

	@Test
	public void testReservedNames() {
		assert NuclosEntityValidator.isReservedName("id");
		assert NuclosEntityValidator.isReservedName("bo");
		assert NuclosEntityValidator.isReservedName("testbo");
		assert NuclosEntityValidator.isReservedName("changedAt");
		assert NuclosEntityValidator.isReservedName("changedBy");
		assert NuclosEntityValidator.isReservedName("createdAt");
		assert NuclosEntityValidator.isReservedName("createdBy");
		assert NuclosEntityValidator.isReservedName("version");
		assert NuclosEntityValidator.isReservedName("nuclosState");
		assert NuclosEntityValidator.isReservedName("nuclosStateNumber");
		assert NuclosEntityValidator.isReservedName("nuclosStateIcon");
		assert NuclosEntityValidator.isReservedName("nuclosSystemId");
		assert NuclosEntityValidator.isReservedName("nuclosProcess");
		assert NuclosEntityValidator.isReservedName("nuclosOrigin");
		assert NuclosEntityValidator.isReservedName("nuclosDeleted");
		assert NuclosEntityValidator.isReservedName("entity");
		assert NuclosEntityValidator.isReservedName("entityId");
		assert NuclosEntityValidator.isReservedName("genericObject");
		assert NuclosEntityValidator.isReservedName("genericObjectId");
		assert NuclosEntityValidator.isReservedName("nuclosMandator");
		assert NuclosEntityValidator.isReservedName("nuclosOwner");
	}

	@Test
	public void testReservedDbColumnsNames() {
		assert NuclosEntityValidator.isReservedDbColumnName("INTID");
		assert NuclosEntityValidator.isReservedDbColumnName("DATCREATED");
		assert NuclosEntityValidator.isReservedDbColumnName("STRCREATED");
		assert NuclosEntityValidator.isReservedDbColumnName("DATCHANGED");
		assert NuclosEntityValidator.isReservedDbColumnName("STRCHANGED");
		assert NuclosEntityValidator.isReservedDbColumnName("INTVERSION");
		assert NuclosEntityValidator.isReservedDbColumnName("STRSYSTEMID");
		assert NuclosEntityValidator.isReservedDbColumnName("INTID_T_MD_PROCESS");
		assert NuclosEntityValidator.isReservedDbColumnName("STRORIGIN");
		assert NuclosEntityValidator.isReservedDbColumnName("BLNNUCLOSDELETED");
		assert NuclosEntityValidator.isReservedDbColumnName("INTID_T_MD_STATE");
		assert NuclosEntityValidator.isReservedDbColumnName("STRVALUE_NUCLOSSTATE");
		assert NuclosEntityValidator.isReservedDbColumnName("INTVALUE_NUCLOSSTATE");
		assert NuclosEntityValidator.isReservedDbColumnName("OBJVALUE_NUCLOSSTATE");
		assert NuclosEntityValidator.isReservedDbColumnName("STRNUCLOSSYSTEMID");
		assert NuclosEntityValidator.isReservedDbColumnName("STRVALUE_NUCLOSPROCESS");
		assert NuclosEntityValidator.isReservedDbColumnName("STRNUCLOSORIGIN");
		assert NuclosEntityValidator.isReservedDbColumnName("STRUID_NUCLOSMANDATOR");
		assert NuclosEntityValidator.isReservedDbColumnName("STRUID_NUCLOSOWNER");
	}

	@Test
	public void testReferenceAttribute() {
		assert !NuclosEntityValidator.isValidReference("b", new A());
		assert !NuclosEntityValidator.isValidReference("B", new A());
		assert !NuclosEntityValidator.isValidReference("_b", new A());
		assert NuclosEntityValidator.isValidReference("b2", new A());
		assert NuclosEntityValidator.isValidReference("bb", new A());
	}

	@Test
	public void testDuplicateEntityName() {
		assert NuclosEntityValidator.isDuplicate("a", A.NUCLET_UID, null, metaProvider.getAllEntities());
		assert NuclosEntityValidator.isDuplicate("b", B.NUCLET_UID, null, metaProvider.getAllEntities());
		assert NuclosEntityValidator.isDuplicate("_b", B.NUCLET_UID, null, metaProvider.getAllEntities());
		assert NuclosEntityValidator.isDuplicate("b.", B.NUCLET_UID, null, metaProvider.getAllEntities());
		assert !NuclosEntityValidator.isDuplicate("a", B.NUCLET_UID, null, metaProvider.getAllEntities());
		assert !NuclosEntityValidator.isDuplicate("b", A.NUCLET_UID, null, metaProvider.getAllEntities());
		assert !NuclosEntityValidator.isDuplicate("_b", A.NUCLET_UID, null, metaProvider.getAllEntities());
		assert !NuclosEntityValidator.isDuplicate("b.", A.NUCLET_UID, null, metaProvider.getAllEntities());
		assert !NuclosEntityValidator.isDuplicate("a", null, null, metaProvider.getAllEntities());
		assert !NuclosEntityValidator.isDuplicate("b", null, null, metaProvider.getAllEntities());
		assert !NuclosEntityValidator.isDuplicate("_b", null, null, metaProvider.getAllEntities());
		assert !NuclosEntityValidator.isDuplicate("b.", null, null, metaProvider.getAllEntities());
		assert !NuclosEntityValidator.isDuplicate("b2", null, null, metaProvider.getAllEntities());
		assert !NuclosEntityValidator.isDuplicate("bb", null, null, metaProvider.getAllEntities());
		assert !NuclosEntityValidator.isDuplicate("b    2", null, null, metaProvider.getAllEntities());
	}

	@Test
	public void testValidEntityName() {
		assert !NuclosEntityValidator.isValidEntityName("a", A.NUCLET_UID, null, metaProvider.getAllEntities(), imports);
		assert !NuclosEntityValidator.isValidEntityName("b", B.NUCLET_UID, null, metaProvider.getAllEntities(), imports);
		assert !NuclosEntityValidator.isValidEntityName("b_", B.NUCLET_UID, null, metaProvider.getAllEntities(), imports);
		assert !NuclosEntityValidator.isValidEntityName("list", null, null, metaProvider.getAllEntities(), imports);
		assert !NuclosEntityValidator.isValidEntityName("set", null, null, metaProvider.getAllEntities(), imports);
		assert !NuclosEntityValidator.isValidEntityName("_set", null, null, metaProvider.getAllEntities(), imports);
		assert !NuclosEntityValidator.isValidEntityName("s e t", null, null, metaProvider.getAllEntities(), imports);

		assert NuclosEntityValidator.isValidEntityName("b2", B.NUCLET_UID, null, metaProvider.getAllEntities(), imports);
		assert NuclosEntityValidator.isValidEntityName("set 2", null, null, metaProvider.getAllEntities(), imports);
		assert NuclosEntityValidator.isValidEntityName("list (xy)", null, null, metaProvider.getAllEntities(), imports);
	}

	@Test
	public void testValidAttributeName() {
		assert NuclosEntityValidator.isValidAttributeName("a", new A(), metaProvider);
		assert NuclosEntityValidator.isValidAttributeName("a2", new A(), metaProvider);
		assert NuclosEntityValidator.isValidAttributeName("123", new A(), metaProvider);

		// No collisions with attribute "B" of A:
		assert NuclosEntityValidator.isValidAttributeName("2b", new A(), metaProvider);
	}

	@Test
	public void testInvalidAttributeName() {
		assert !NuclosEntityValidator.isValidAttributeName("_", new A(), metaProvider);
		assert !NuclosEntityValidator.isValidAttributeName("???", new A(), metaProvider);
		assert !NuclosEntityValidator.isValidAttributeName("", new A(), metaProvider);

		// Collisions with attribute "B" of A:
		assert !NuclosEntityValidator.isValidAttributeName("b", new A(), metaProvider);
		assert !NuclosEntityValidator.isValidAttributeName(" b", new A(), metaProvider);
		assert !NuclosEntityValidator.isValidAttributeName("?b", new A(), metaProvider);
		assert !NuclosEntityValidator.isValidAttributeName("B   ", new A(), metaProvider);
		assert !NuclosEntityValidator.isValidAttributeName("_b", new A(), metaProvider);

		// A is referencing B -> there can be not attribute "a" on B
		assert !NuclosEntityValidator.isValidAttributeName("a", new B(), metaProvider);
		assert !NuclosEntityValidator.isValidAttributeName("_a", new B(), metaProvider);
		assert !NuclosEntityValidator.isValidAttributeName(" a", new B(), metaProvider);
	}

	@Test
	public void testGetGetterName() {
		assert StringUtils.equals("getUnderscore", NuclosEntityValidator.getGetterName(A.Under_score.getFieldName()));
		assert StringUtils.equals("get1234", NuclosEntityValidator.getGetterName(A._1234.getFieldName()));
		assert StringUtils.equals("getRefB", NuclosEntityValidator.getGetterName(A.RefB.getFieldName()));
	}

	@Test
	public void testGetSetterName() {
		assert StringUtils.equals("setUnderscore", NuclosEntityValidator.getSetterName(A.Under_score.getFieldName()));
		assert StringUtils.equals("set1234", NuclosEntityValidator.getSetterName(A._1234.getFieldName()));
		assert StringUtils.equals("setRefB", NuclosEntityValidator.getSetterName(A.RefB.getFieldName()));
	}

	public static final A A = new A();
	public static final B B = new B();

	// @formatter:off
	private static class A extends EntityMeta<Long> {
		private static final long serialVersionUID = 2664374734813644001L;
		public static final UID UID = new UID("A");
		public static final UID NUCLET_UID = new UID("NucletA");
		@Override public UID getUID() { return UID; }
		@Override public String getDbTable() { return null; }
		@Override public String getEntityName() { return "A"; }
		@Override public Class<Long> getPkClass() { return null; }
		@Override public UID getNuclet() { return NUCLET_UID; }

		public static final FieldMeta<String> B = new FieldMeta.Valueable<java.lang.String>() {
			private static final long serialVersionUID = 3819785859369486105L;
			@Override public UID getUID() { return new UID("B_B"); }
			@Override public UID getEntity() { return A.UID; }
			@Override public String getFieldName() { return "B"; }
			@Override public String getDbColumn() { return null; }
			@Override public Class<String> getJavaClass() { return String.class; }
			@Override public UID getForeignEntity() { return null; }
		};

		public static final FieldMeta<String> Under_score = new FieldMeta.Valueable<java.lang.String>() {
			private static final long serialVersionUID = 3819785859369486105L;
			@Override public UID getUID() { return new UID("under_score"); }
			@Override public UID getEntity() { return A.UID; }
			@Override public String getFieldName() { return "under_score"; }
			@Override public String getDbColumn() { return null; }
			@Override public Class<String> getJavaClass() { return String.class; }
			@Override public UID getForeignEntity() { return null; }
		};

		public static final FieldMeta<String> _1234 = new FieldMeta.Valueable<java.lang.String>() {
			private static final long serialVersionUID = 3819785859369486105L;
			@Override public UID getUID() { return new UID("1234"); }
			@Override public UID getEntity() { return A.UID; }
			@Override public String getFieldName() { return "1234"; }
			@Override public String getDbColumn() { return null; }
			@Override public Class<String> getJavaClass() { return String.class; }
			@Override public UID getForeignEntity() { return null; }
		};

		public static final FieldMeta<UID> RefB = new FieldMeta<UID>() {
			private static final long serialVersionUID = 1L;
			@Override public UID getUID() { return new UID("RefB"); }
			@Override public UID getEntity() { return A.UID; }
			@Override public String getFieldName() { return "RefB"; }
			@Override public String getDbColumn() { return null; }
			@Override public Class<UID> getJavaClass() { return UID.class; }
			@Override public UID getForeignEntity() { return new UID("B"); }
		};
	}

	private static class B extends EntityMeta<Long> {
		private static final long serialVersionUID = 2466798037083777839L;
		public static final UID UID = new UID("B");
		public static final UID NUCLET_UID = new UID("NucletB");
		@Override public UID getUID() { return UID; }
		@Override public String getDbTable() { return null; }
		@Override public String getEntityName() { return "B"; }
		@Override public Class<Long> getPkClass() { return null; }
		@Override public UID getNuclet() { return NUCLET_UID; }
	}
	// @formatter:on

	private class TestMetaProvider implements IRigidMetaProvider {
		@Override
		public Collection<EntityMeta<?>> getAllEntities() {
			ArrayList<EntityMeta<?>> result = new ArrayList<>();
			result.addAll(Arrays.asList(new A(), new B()));
			return result;
		}

		@Override
		public <PK> EntityMeta<PK> getEntity(final UID entityUID) {
			throw new NotImplementedException();
		}

		@Override
		public Map<UID, FieldMeta<?>> getAllEntityFieldsByEntity(final UID entityUID) {
			throw new NotImplementedException();
		}

		@Override
		public FieldMeta<?> getEntityField(final UID fieldUID) {
			throw new NotImplementedException();
		}

		@Override
		public EntityMeta<?> getByTablename(final String sTableName) {
			throw new NotImplementedException();
		}

		@Override
		public boolean isNuclosEntity(final UID entityUID) {
			throw new NotImplementedException();
		}
	}
}
