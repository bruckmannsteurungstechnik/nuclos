package org.nuclos.server.autosync;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.appender.FileAppender;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.nuclos.server.autosync.AutoDbSetup.Schema;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.dblayer.util.DbArtifactXmlReader;
import org.nuclos.server.dblayer.util.DbArtifactXmlWriter;

public final class AutoDbSetupUtils {

	private AutoDbSetupUtils() {
	}

	public static Appender createAppender(String filename) {
		if (filename == null)
			return null;
		
		Appender appender = null;
		try {
			File structureChangelogDir = NuclosSystemParameters.getDirectory(NuclosSystemParameters.DATABASE_STRUCTURE_CHANGE_LOG_PATH);
			if (structureChangelogDir != null) {
				String path = structureChangelogDir.getAbsolutePath();
				path = path.substring(0, path.lastIndexOf(File.separatorChar));
				path += File.separatorChar;
				path += filename;
				path += ".log";
				
//				Layout layout = new PatternLayout("%p - %m%n");
				PatternLayout layout = PatternLayout.newBuilder().withPattern("%p - %m%n").build();
				
//				appender = new FileAppender(layout, path, false);
				appender = FileAppender.createAppender(path, null, null, filename, null, null, null, null, layout, null, null, null, null);
			}
		} catch (Exception ex) {
			// ignore
		}
		return appender;
	}

	static byte[] compressSchema(Schema schema) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			GZIPOutputStream gzos = new GZIPOutputStream(baos);
			writeSchema(schema, gzos);
			gzos.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return baos.toByteArray();
	}

	static Schema uncompressSchema(byte[] b) {
		DbArtifactXmlReader reader = new DbArtifactXmlReader();
		try {
			GZIPInputStream gzin = new GZIPInputStream(new ByteArrayInputStream(b));
			reader.read(gzin);
			gzin.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return new Schema(reader.getArtifacts(), reader.getDumps());
	}

	private static void writeSchema(Schema schema, OutputStream os) throws IOException {
		DbArtifactXmlWriter writer = new DbArtifactXmlWriter(os);
		writer.writeArtifacts(schema.getArtifacts());
		writer.writeDumps(schema.getDumps());
		writer.close();
	}
	
}
