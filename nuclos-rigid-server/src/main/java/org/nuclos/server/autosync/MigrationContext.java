//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.package org.nuclos.server.statemodel.ejb3;
package org.nuclos.server.autosync;

import org.nuclos.server.dblayer.PersistentDbAccess;

public class MigrationContext {
	
	enum Type {DB, NUCLET}

	private final boolean nuclet;
	private final boolean db;
	private final IMigrationHelper helper;
	private final PersistentDbAccess dbAccess;
	
	MigrationContext(Type type, IMigrationHelper helper, PersistentDbAccess dbAccess) {
		super();
		this.nuclet = type == Type.NUCLET;
		this.db = type == Type.DB;
		this.helper = helper; 
		this.dbAccess = dbAccess;
	}

	public boolean isNucletMigration() {
		return nuclet;
	}

	public boolean isDbMigration() {
		return db;
	}

	public IMigrationHelper getHelper() {
		return helper;
	}

	public PersistentDbAccess getDbAccess() {
		return dbAccess;
	}
	
}
