//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.autosync;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXB;
import javax.xml.transform.stream.StreamSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Logger;
import org.nuclos.common.DbField;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidE;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SimpleDbField;
import org.nuclos.common.SourceResultHelper;
import org.nuclos.common.SysEntities;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Transformer;
import org.nuclos.server.autosync.AutoDbSetup.Schema;
import org.nuclos.server.datasource.DatasourceMetaVO;
import org.nuclos.server.dblayer.DbAccess;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.DbTuple.DbTupleElement;
import org.nuclos.server.dblayer.PersistentDbAccess;
import org.nuclos.server.dblayer.query.DbCondition;
import org.nuclos.server.dblayer.query.DbDelete;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.query.DbUpdate;
import org.nuclos.server.dblayer.statements.DbBuildableStatement;
import org.nuclos.server.dblayer.statements.DbDeleteStatement;
import org.nuclos.server.dblayer.statements.DbMap;
import org.nuclos.server.dblayer.statements.DbStatement;
import org.nuclos.server.dblayer.structure.DbArtifact;
import org.nuclos.server.dblayer.structure.DbColumn;
import org.nuclos.server.dblayer.structure.DbConstraint;
import org.nuclos.server.dblayer.structure.DbConstraint.AbstractDbForeignKeyConstraint;
import org.nuclos.server.dblayer.structure.DbConstraint.AbstractDbUniqueConstraint;
import org.nuclos.server.dblayer.structure.DbConstraint.DbForeignKeyConstraint;
import org.nuclos.server.dblayer.structure.DbConstraint.DbUnreferencedForeignKeyConstraint;
import org.nuclos.server.dblayer.structure.DbNullable;
import org.nuclos.server.dblayer.structure.DbTable;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

public class SchemaHelper {
	
	private final RigidE._Entity eEntity = new RigidE._Entity(){};
	private final RigidE._Entityfield eEntityField = new RigidE._Entityfield(){};
	
	private Jaxb2Marshaller jaxb2Marshaller;
	
	private Logger log;
	
	private DbAccess _dbAccess;
	private PersistentDbAccess _pdbAccess;
	private DbQueryBuilder builder;
	
	private Map<String, Set<String>> unrefForeignKeys;
	private final Schema schema;
	private final String schemaVersion;
	private Collection<DbTable> dbTables;
	private UpperHashMap<EntityMeta<?>> tableMetas;
	private UpperHashMap<UpperHashMap<DbField<?>>> columnMetas;
	private UpperHashMap<UpperHashSet> nullableColumns;
	private UpperHashMap<UpperHashSet> additionalValidateNotNullColumns;
	// User defined entities
	private Collection<UID> udEntities;
	private Collection<DatasourceMetaVO> dynAndChartEntities;
	private Collection<UID> allEntities;
	
	private boolean grantRemoveEntityFields = false;
	
	public SchemaHelper(Schema schema, String schemaVersion, SysEntities sourceOfUnrefForeignKeys, DbAccess dbAccess) {
		this._dbAccess = dbAccess;
		this.schema = schema;
		this.schemaVersion = schemaVersion;
		init(sourceOfUnrefForeignKeys);
	}
	
	public SchemaHelper(Schema schema, String schemaVersion, SysEntities sourceOfUnrefForeignKeys, PersistentDbAccess dbAccess) {
		this._pdbAccess = dbAccess;
		this.schema = schema;
		this.schemaVersion = schemaVersion;
		init(sourceOfUnrefForeignKeys);
	}
	
	public void setJaxb2Marshaller(Jaxb2Marshaller jaxb2Marshaller) {
		this.jaxb2Marshaller = jaxb2Marshaller;
	}
	
	private void init(SysEntities sourceOfUnrefForeignKeys) {
		this.log = (Logger) LogManager.getLogger(SchemaHelper.class);
		if (_dbAccess != null) {
			this.builder = _dbAccess.getQueryBuilder();
		}
		if (_pdbAccess != null) {
			this.builder = _pdbAccess.getQueryBuilder();
		}
		this.dbTables = getTables(schema);
		this.tableMetas = getTableMetaData(dbTables);
		this.columnMetas = getColumnMetaData(dbTables);
		this.nullableColumns = getNullableColumns(dbTables);
		this.udEntities = getUserDefinedEntities();
		this.dynAndChartEntities = getDynamicAndChartEntities();
		this.allEntities = new ArrayList<UID>(udEntities);
		this.additionalValidateNotNullColumns = new UpperHashMap<>();
		for (EntityMeta<?> eMeta : sourceOfUnrefForeignKeys._getAllEntities()) {
			this.allEntities.add(eMeta.getUID());
			UpperHashSet validateNotNull = new UpperHashSet();
			for (FieldMeta<?> field : eMeta.getFields()) {
				if (field.isValidateNotNull()) {
					validateNotNull.add(field.getDbColumn());
				}
			}
			if (!validateNotNull.isEmpty()) {
				this.additionalValidateNotNullColumns.put(eMeta.getDbTable(), validateNotNull);
			}
		}
		for (DatasourceMetaVO dsMeta : dynAndChartEntities) {
			this.allEntities.add(dsMeta.getEntityUID());
		}

		this.unrefForeignKeys = getUnrefForeignKeys(sourceOfUnrefForeignKeys);
	}
	
	/**
	 * 
	 * @return 
	 * 	true: Löschen von Entity Fields ist erlaubt
	 *  false (default): Ist nicht erlaubt
	 */
	public boolean isGrantRemoveEntityFields() {
		return grantRemoveEntityFields;
	}

	/**
	 * Mit Vorsicht zu genießen!
	 * 
	 * Erlaubt das Löschen von Entity Fields.
	 * Default ist false, und wird beim Validieren eine Exception auslösen falls Entity Fields gelöscht werden müssten.
	 */
	public void grantRemoveEntityFields() {
		this.grantRemoveEntityFields = true;
	}

	public void validate() {
		this.validate("schema-validation-" + schemaVersion);
	}
	
	public void validateNoLogFile() {
		this.validate(null);
	}
	
	public void validate(String filename) {
		Appender appender = null;
		if (filename != null) {
			appender = initLogging(filename);
		}
		
		try {
			int i = 0;
			int last = 0;
			int dirty = 0;
			int waterTread = 0;
			do {
				if (i > 0) {
					log.info("schema validation round " + i + " fixed " + dirty + " issues!");
				}
				i++;
				log.info("schema validation round " + i + "...");
				last = dirty;
				dirty = 0;
				dirty += validateNotNulls();
				dirty += validateUniqueConstraints();
				dirty += validateForeignConstraints();
				if (last != dirty) {
					waterTread = 0;
				} else {
					waterTread++;
				}
			} while (waterTread < 10 && dirty > 0);
			if (dirty > 0) {
				log.warn("schema validation stopped after " + waterTread + " attempts! " + dirty + " unresolved issues left.");
			} else {
				log.info("schema validation finished!");
			}
		} finally {
			if (appender != null) {
				log.removeAppender(appender);
			}
		}
	}
	
	private int validateNotNulls() {
		int dirty = 0;
		for (DbTable dbTable : dbTables) {
			final String table = dbTable.getTableName();
			final EntityMeta<?> meta = tableMetas.get(table);
			final UpperHashMap<DbField<?>> columns = columnMetas.get(table);
			final UpperHashSet validateNotNullColumns = additionalValidateNotNullColumns.get(table);
			List<DbField<?>> columnsToCheck = new ArrayList<>();
			for (String column : columns.keySet()) {
				if (!isNullable(table, column) ||
						validateNotNullColumns != null && validateNotNullColumns.contains(column)) {
					columnsToCheck.add(columns.get(column));
				}
			}
			if (columnsToCheck.size() > 0) {
				int deleted = deleteNullValues(meta, columnsToCheck, "NOT NULLABLE");
				if (deleted > 0) {
					checkDeleteAllowed(table);
					dirty++;
				}
			}
		}
		return dirty;
	}
	
	/**
	 * validates foreign and unreferenced foreign key constraints
	 * @return
	 */
	private int validateForeignConstraints() {
		int dirty = 0;
		for (DbTable dbTable : dbTables) {
			final EntityMeta<?> meta = tableMetas.get(dbTable.getTableName());
			final List<String> allColumns = getDbColumnNames(dbTable);
			for (AbstractDbForeignKeyConstraint constraint : dbTable.getTableArtifacts(AbstractDbForeignKeyConstraint.class)) {
				final List<String> columns = constraint.getColumnNames();
				final String referencedTable = constraint.getReferencedTable().getName();
				final EntityMeta<?> referencedMeta = tableMetas.get(referencedTable);

				final String constraintColumn1 = columns.get(0);
				final String referencedColumn1 = constraint.getReferencedColumnNames().get(0);
				final DbField constraintcol1 = columnMetas.get(dbTable.getTableName()).get(constraintColumn1);
				final DbField referenced1 = columnMetas.get(referencedMeta.getDbTable()).get(referencedColumn1);
				
				if (constraint instanceof DbForeignKeyConstraint && columns.size() == 1) {
					final boolean nullable = isNullable(dbTable.getTableName(), constraintColumn1);
					
					final StringBuilder condition = new StringBuilder();
					condition.append(" NOT EXISTS (SELECT ");
					condition.append(referencedColumn1);
					condition.append(" FROM ");
					condition.append(referencedTable);
					condition.append(" WHERE ");
					condition.append(dbTable.getTableName() + ".");
					condition.append(constraintColumn1);
					condition.append(" = ");
					condition.append(referencedColumn1);
					condition.append(")");
					if (nullable) {
						condition.append(" AND ");
						condition.append(constraintColumn1);
						condition.append(" IS NOT NULL");
					}
					
					// set to null if allowed...
					if (nullable) {
						
						final DbMap values = new DbMap();
						values.putNull(constraintcol1);
						final DbUpdate update = builder.createUpdate(meta, values);
						final DbQuery<?> query = builder.createQuery(referenced1.getJavaClass());
						DbFrom<?> innerExist = query.from(referencedMeta, "t2");
						query.select(innerExist.baseColumn(referenced1));
						
						query.where(builder.equal(update.baseColumn(constraintcol1), innerExist.baseColumn(referenced1)));
						
						DbCondition cond1 = builder.notexists(query);
						DbCondition cond2 = builder.isNotNull(constraintcol1);
						update.where(builder.and(cond1, cond2));
						
						try {
							int updated = executeUpdate(update);
							if (updated > 0) {
								dirty++;
								log.warn(String.format("%-30s update %-30s set %s=NULL where %-40s: %s", constraint.getSimpleName(), meta.getDbTable(), constraintColumn1, condition.toString(), updated));
							}
						} catch(DbException ex) {
							log.error(String.format("%-30s update %-30s set %s=NULL where %-40s: %s", constraint.getSimpleName(), meta.getDbTable(), constraintColumn1, condition.toString(), ex.getMessage()), ex);
						}
						
					} else {
						final DbDelete delete = builder.createDelete(meta);
						final DbQuery<?> query = builder.createQuery(referenced1.getJavaClass());
						
						DbFrom<?> innerExist = query.from(referencedMeta, "t2");
						query.select(innerExist.baseColumn(referenced1));
						query.where(builder.equal(delete.baseColumn(constraintcol1), innerExist.baseColumn(referenced1)));
						
						delete.where(builder.notexists(query));
						
						try {
							int deleted = executeDelete(delete);
							if (deleted > 0) {
								checkDeleteAllowed(meta.getDbTable());
								dirty++;
								log.warn(String.format("%-30s delete from %-30s where %-40s: %s", constraint.getSimpleName(), meta.getDbTable(), condition.toString(), deleted));
							}
						} catch(DbException ex) {
							log.error(String.format("%-30s delete from %-30s where %-40s: %s", constraint.getSimpleName(), meta.getDbTable(), condition.toString(), ex.getMessage()), ex);
						}
					}
					
				} else {
					// unreferenced or refcolumn.count > 1
					boolean allNullable = true;
					
					final DbQuery<DbTuple> query = builder.createQuery(DbTuple.class);
					final DbFrom<?> t = query.from(meta);
					final List<DbExpression<?>> select = new ArrayList<DbExpression<?>>();
					for (String constraintColumn : columns) {
						if (!isNullable(dbTable.getTableName(), constraintColumn)) {
							allNullable = false;
						}
						final DbField column = columnMetas.get(dbTable.getTableName()).get(constraintColumn);
						select.add(t.baseColumn(column));
					}
					
					query.multiselect(select);
					query.distinct(true);

					if (columns.size() == 1) {
						// check only unreferenced values (not in table)
						final DbQuery<?> subquery = builder.createQuery(referenced1.getJavaClass());

						DbFrom<?> whereIn = subquery.from(referencedMeta, "t2");
						subquery.select(whereIn.baseColumn(referenced1));
						query.where(builder.not(builder.in(t.baseColumn(constraintcol1), subquery)));
					}

					final List<DbTuple> rows = executeQuery(query);
					if (!rows.isEmpty()) {
						for (DbTuple tuple: rows) {
							final Map<String, Object> values = new HashMap<String, Object>();
							boolean allNull = true;
							for (int i = 0; i < columns.size(); i++) {
								final String column = columns.get(i);
								final Object value = tuple.get(i);
								values.put(column, value);
								if (value != null) {
									allNull = false;
								}
							}
							
							if (allNullable && allNull) {
								continue;
							}
							
							if (!validateForeignConstraint(constraint, values)) {
								dirty++;
								deleteFrom(meta, tuple, constraint.getSimpleName());
							}
						}
					}
				}
			}
		}
		return dirty;
	}
	
	public static boolean isSystemTable(String tableName) {
		if (tableName == null) {
			return false;
		}
		tableName = tableName.toUpperCase();
		return  tableName.startsWith("T_AD_") ||
				tableName.startsWith("T_MD_") ||
				tableName.startsWith("T_UD_");
	}
	
	/**
	 * do not delete 
	 * - entities, 
	 * - entity fields (could be disabled, default is true)
	 * - or user data!
	 * @param tableName
	 * @throws NuclosFatalException
	 */
	private void checkDeleteAllowed(String tableName) throws NuclosFatalException {
		boolean problem = false;
		tableName = tableName.toUpperCase();
		if (isSystemTable(tableName)) {
			if (eEntity.getDbTable().equals(tableName)) {
				problem = true;
			}
			if (!grantRemoveEntityFields && eEntityField.getDbTable().equals(tableName)) {
				problem = true;
			}
		} else {
			problem = true;
		}
		
		if (problem) {
			throw new NuclosFatalException(String.format("Schema validation problem found: Data of table %s must not be deleted!", tableName));
		}
	}
	
	private boolean validateForeignConstraint(AbstractDbForeignKeyConstraint constraint, Map<String, Object> values) {
		final String referencedTable = constraint.getReferencedTable().getName();
		final EntityMeta<?> referencedMeta = tableMetas.get(referencedTable);
		final List<String> referencedColumns = constraint.getReferencedColumnNames();
		final List<String> originColumns = constraint.getColumnNames();
		
		final DbQuery<Long> query = builder.createQuery(Long.class);
		final DbFrom<?> t = query.from(referencedMeta);
		
		if (constraint instanceof DbUnreferencedForeignKeyConstraint && referencedColumns.size() == 1) {
			String referencedColumn = referencedColumns.get(0);
			Object value = values.get(originColumns.get(0));
			if (value != null && value instanceof String &&
					referencedTable.equalsIgnoreCase(new RigidE._Entityfield(){}.getDbTable()) && 
					"STRUID".equalsIgnoreCase(referencedColumn)) {
				// check SF
				UID fieldUID = new UID(value.toString());
				for (UID entityUID : allEntities) {
					if (SF.isEOField(entityUID, fieldUID)) {
						return true;
					}
				}
			}
			String unrefKey = String.format("%s.%s", constraint.getReferencedTable().getName(), referencedColumn);
			if (unrefForeignKeys.keySet().contains(unrefKey)) {
				if (unrefForeignKeys.get(unrefKey).contains(value)) {
					return true;
				}
			}
		}
		
		for (int i = 0; i < referencedColumns.size(); i++) {
			final String referencedColumn = referencedColumns.get(i);
			final String originColumn = originColumns.get(i);
			final DbField referenced = columnMetas.get(referencedMeta.getDbTable()).get(referencedColumn);
			final Object oValue = values.get(originColumn);
			
			if (oValue == null) {
				query.addToWhereAsAnd(builder.isNull(t.baseColumn(referenced)));
			} else {
				query.addToWhereAsAnd(builder.equalValue(t.baseColumn(referenced), oValue));
			}
		}
		query.select(query.getBuilder().countRows());
		
		final long found = executeQuerySingleResult(query);
		return found > 0l;
	}
	
	/**
	 * validates unique, logical unique and primary keys
	 * @return
	 */
	private int validateUniqueConstraints() {
		int dirty = 0;
		for (DbTable dbTable : dbTables) {
			final EntityMeta<?> meta = tableMetas.get(dbTable.getTableName());
			final List<String> allColumns = getDbColumnNames(dbTable);
			final List<DbConstraint> uniqueConstraints = new ArrayList<DbConstraint>(dbTable.getTableArtifacts(AbstractDbUniqueConstraint.class));
			for (DbConstraint constraint : uniqueConstraints) {
				List<String> columns = constraint.getColumnNames();

				final DbQuery<DbTuple> query = builder.createQuery(DbTuple.class);
				final DbFrom<?> t = query.from(meta);
				final List<DbExpression<?>> select = new ArrayList<DbExpression<?>>();
				query.where(builder.alwaysFalse());
				for (String constraintColumn : columns) {
					final DbField column = columnMetas.get(dbTable.getTableName()).get(constraintColumn);
					select.add(t.baseColumn(column));
					query.addToWhereAsOr(builder.isNotNull(t.baseColumn(column)));
				}
				query.multiselect(select);
				query.groupBy(select);
				query.having(builder.greaterThan(builder.countRows(), builder.literal(1l)));
				
				final List<DbTuple> found = executeQuery(query);
				
				if (!found.isEmpty()) {
					dirty++;
					for (DbTuple tuple: found) {
						deleteFrom(meta, tuple, constraint.getSimpleName());
					}
				}
			}
		}
		return dirty;
	}
	
	private int deleteNullValues(final EntityMeta<?> meta, List<DbField<?>> columns, String reason) {
		final DbDelete delete = builder.createDelete(meta);
		List<DbCondition> columnConditions = new ArrayList<>();
		for (DbField<?> column : columns) {
			columnConditions.add(builder.isNull(column));
		}
		delete.where(builder.or(columnConditions));

		try {
			int deleted = executeDelete(delete);
			if (deleted > 0) {
				checkDeleteAllowed(meta.getDbTable());
				log.warn(String.format("%-30s delete from %-30s where %-40s: %s", reason, meta.getDbTable(), String.format("%s IS NULL", columns), deleted));
			}
			return deleted;
		} catch(DbException ex) {
			log.error(String.format("%-30s delete from %-30s where %-40s: %s", reason, meta.getDbTable(), String.format("%s IS NULL", columns), ex.getMessage()), ex);
			return 1;
		}
	}
	
	private void deleteFrom(final EntityMeta<?> meta, DbTuple tuple, String reason) {
		final DbMap deleteCondition = new DbMap();
		final StringBuilder condition = new StringBuilder();
		for (DbTupleElement<?> element : tuple.getElements()) {
			final Object oValue = tuple.get(element);
			final DbField<?> column = SimpleDbField.create(element.getAlias(), element.getJavaType());
			if (oValue == null) {
				condition.append(String.format("%s IS NULL, ", column.getDbColumn()));
				deleteCondition.putNull(column);
			} else {
				condition.append(String.format("%s = %s, ", column.getDbColumn(), valueToString(oValue)));
				deleteCondition.putUnsafe(column, oValue);
			}
		}
		if (condition.length() > 2) {
			condition.delete(condition.length()-2, condition.length());
		}
		
		DbStatement stmt = new DbDeleteStatement(meta, deleteCondition);
		try {
			int deleted = execute(stmt);
			if (deleted > 0) {
				checkDeleteAllowed(meta.getDbTable());
			}
			log.warn(String.format("%-30s delete from %-30s where %-40s: %s", reason, meta.getDbTable(), condition.toString(), deleted));
		} catch(DbException ex) {
			log.error(String.format("%-30s delete from %-30s where %-40s: %s", reason, meta.getDbTable(), condition.toString(), ex.getMessage()), ex);
		}
	}
	
	private static String valueToString(Object o) {
		if (o == null) {
			return "null";
		}
		try {
			if (o instanceof Double) {
				return new DecimalFormat("#").format(o);
			}
		} catch (Exception ex) {
			// ignore
		}
		return o.toString();
	}
	
	private boolean isNullable(String table, String column) {
		return nullableColumns.get(table).contains(column);
	}
	
	private static boolean hasColumn(DbTable dbTable, String column) {
		return getDbColumnNames(dbTable).contains(column);
	}
	
	private static Collection<DbTable> getTables(Schema schema) {
		List<DbTable> result = new ArrayList<DbTable>();
		for (DbArtifact dba : schema.getArtifacts()) {
			if (dba instanceof DbTable) {
				result.add((DbTable) dba);
			}
		}
		Collections.sort(result, new Comparator<DbTable>() {
			@Override
			public int compare(DbTable o1, DbTable o2) {
				return RigidUtils.compare(o1.getTableName(), o2.getTableName());
			}
		});
		return result;
	}
	
	/**
	 * 
	 * @param dbTable
	 * @return Columns upper case
	 */
	private static List<String> getDbColumnNames(DbTable dbTable) {
		return RigidUtils.transform(dbTable.getTableColumns(), new Transformer<DbColumn, String>() {
			@Override public String transform(DbColumn dbc) {
				return dbc.getColumnName().toUpperCase();
				}
			});
	}
	
	private static UpperHashMap<EntityMeta<?>> getTableMetaData(Collection<DbTable> dbTables) {
		UpperHashMap<EntityMeta<?>> result = new UpperHashMap<EntityMeta<?>>();
		for (final DbTable dbTable : dbTables) {
			final List<String> columnNames = getDbColumnNames(dbTable);
			final EntityMeta<?> tableMeta = new EntityMeta() {
				@Override public UID getUID() {return new UID(dbTable.getTableName());}
				@Override public String getDbTable() {return dbTable.getTableName();}
				@Override public String getEntityName() {return dbTable.getTableName();}
				@Override public Class getPkClass() {return columnNames.contains("INTID") ? Long.class : UID.class;}
			};
			result.put(dbTable.getTableName(), tableMeta);
		}
		return result;
	}
	
	private static UpperHashMap<UpperHashMap<DbField<?>>> getColumnMetaData(Collection<DbTable> dbTables) {
		UpperHashMap<UpperHashMap<DbField<?>>> result = new UpperHashMap<UpperHashMap<DbField<?>>>();
		for (final DbTable dbTable : dbTables) {
			UpperHashMap<DbField<?>> fieldMetas = new UpperHashMap<DbField<?>>();
			for (final DbColumn dbColumn : dbTable.getTableColumns()) {
				fieldMetas.put(dbColumn.getColumnName(), dbColumn);
			}
			result.put(dbTable.getTableName(), fieldMetas);
		}
		return result;
	}
	
	private static UpperHashMap<UpperHashSet> getNullableColumns(Collection<DbTable> dbTables) {
		UpperHashMap<UpperHashSet> result = new UpperHashMap<UpperHashSet>();
		for (final DbTable dbTable : dbTables) {
			UpperHashSet nullables = new UpperHashSet();
			for (final DbColumn dbColumn : dbTable.getTableColumns()) {
				if (dbColumn.getNullable() == DbNullable.NULL) {
					nullables.add(dbColumn.getColumnName());
				}
			}
			result.put(dbTable.getTableName(), nullables);
		}
		return result;
	}
	
	private static class UpperHashSet extends HashSet<String> {
		public boolean contains(String s) {
			return super.contains(s.toUpperCase());
		}
		@Override
		public boolean contains(Object o) {
			throw new IllegalArgumentException("UpperHashSet.contains(Object)");
		}
		@Override
		public boolean add(String s) {
			return super.add(s.toUpperCase());
		}
		public boolean remove(String s) {
			return super.remove(s.toUpperCase());
		}
		@Override
		public boolean remove(Object o) {
			throw new IllegalArgumentException("UpperHashSet.remove(Object)");
		}
	}
	
	private static class UpperHashMap<V> extends HashMap<String, V> {
		public V get(String key) {
			return super.get(((String) key).toUpperCase());
		}
		@Override
		public V get(Object key) {
			throw new IllegalArgumentException("UpperHashMap.get(Object)");
		}
		@Override
		public V put(String key, V value) {
			return super.put(key.toUpperCase(), value);
		}
	};
	
	private Appender initLogging(String filename) {
		Appender appender = AutoDbSetupUtils.createAppender(filename);
		if (appender != null) {
			log.addAppender(appender);
		}
		return appender;
	}
	
	private Map<String, Set<String>> getUnrefForeignKeys(SysEntities sysEntities) {
		Set<String> entityValues = new HashSet<String>();
		Set<String> fieldValues = new HashSet<String>();
		for (EntityMeta<?> entity : sysEntities._getAllEntities()) {
			entityValues.add(entity.getUID().getString());
			for (FieldMeta<?> field : entity.getFields()) {
				fieldValues.add(field.getUID().getString());
			}
		}
		for (DatasourceMetaVO dsMeta : dynAndChartEntities) {
			entityValues.add(dsMeta.getEntityUID().getString());
			for (DatasourceMetaVO.ColumnMeta colMeta : dsMeta.getColumns()) {
				fieldValues.add(colMeta.getFieldUID().getString());
			}
		}
		Map<String, Set<String>> result = new HashMap<String, Set<String>>();
		result.put(String.format("%s.%s", 
				new RigidE._Entity() {}.getDbTable().toUpperCase(),
				new RigidE._Entity() {}.getPk().getDbColumn().toUpperCase()), entityValues);
		result.put(String.format("%s.%s", 
				new RigidE._Entityfield() {}.getDbTable().toUpperCase(),
				new RigidE._Entity() {}.getPk().getDbColumn().toUpperCase()), fieldValues);
		return result;
	}
	
	private Collection<UID> getUserDefinedEntities() {
		final DbQuery<UID> query = builder.createQuery(UID.class);
		final DbFrom<UID> t = query.from(new RigidE._Entity(){});
		query.select(t.basePk());
		return executeQuery(query);
	}
	
	private Collection<DatasourceMetaVO> getDynamicAndChartEntities() {
		Collection<DatasourceMetaVO> result = new ArrayList<DatasourceMetaVO>();
		result.addAll(getDatasourceEntities(RigidE.DYNAMICENTITY, RigidE.DYNAMICENTITY.meta));
		result.addAll(getDatasourceEntities(RigidE.CHART, RigidE.CHART.meta));
		return result;
	}
	
	private Collection<DatasourceMetaVO> getDatasourceEntities(EntityMeta<UID> entity, FieldMeta.Valueable<java.lang.String> metaField) {
		final DbQuery<String> query = builder.createQuery(String.class);
		final DbFrom<UID> t = query.from(entity);
		query.select(t.baseColumn(metaField));
		
		Collection<DatasourceMetaVO> result = new ArrayList<DatasourceMetaVO>();
		for (String sMeta : executeQuery(query)) {
			StreamSource xmlSource = SourceResultHelper.newSource(sMeta);
			DatasourceMetaVO dsMeta = null;
			if (xmlSource != null) {
				try {
					if (jaxb2Marshaller != null) {
						dsMeta = (DatasourceMetaVO) jaxb2Marshaller.unmarshal(xmlSource);
					} else {
						// This is a HACK that probably leaks some memory, see NUCLOS-2919. (tp)
						dsMeta = JAXB.unmarshal(xmlSource, DatasourceMetaVO.class);
					}
				} catch (OutOfMemoryError e) {
					log.error("JAXB unmarshal failed: marshaller=" + jaxb2Marshaller 
							+ " meta=" + sMeta + " xml is:\n" + xmlSource, e);
					throw e;
				}
			}
			if (dsMeta != null) { // after 4.0 migration it is null
				result.add(dsMeta);
			}
		}
		return result;
	}
	
	private int executeUpdate(DbUpdate update) throws DbException {
		if (_dbAccess != null) {
			return _dbAccess.executeUpdate(update);
		} else {
			return _pdbAccess.executeUpdate(update);
		}
	}
	
	private int executeDelete(DbDelete delete) throws DbException {
		if (_dbAccess != null) {
			return _dbAccess.executeDelete(delete);
		} else {
			return _pdbAccess.executeDelete(delete);
		}
	}
	
	private <T> List<T> executeQuery(DbQuery<? extends T> query) throws DbException {
		if (_dbAccess != null) {
			return _dbAccess.executeQuery(query);
		} else {
			return _pdbAccess.executeQuery(query);
		}
	}
	
	private <T> T executeQuerySingleResult(DbQuery<? extends T> query) throws DbException {
		if (_dbAccess != null) {
			return _dbAccess.executeQuerySingleResult(query);
		} else {
			return _pdbAccess.executeQuerySingleResult(query);
		}
	}
	
	private int execute(DbBuildableStatement statement) throws DbException {
		if (_dbAccess != null) {
			return _dbAccess.execute(statement);
		} else {
			return _pdbAccess.execute(statement);
		}
	}
}
