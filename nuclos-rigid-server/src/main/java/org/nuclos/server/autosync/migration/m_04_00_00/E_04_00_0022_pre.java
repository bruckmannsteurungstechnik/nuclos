package org.nuclos.server.autosync.migration.m_04_00_00;

import java.util.Collection;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.dblayer.Schema;
import org.nuclos.common.dblayer.SchemaHelperVersion;


@Schema (version="3.15.0004", helper = SchemaHelperVersion.V_3_X)
class E_04_00_0022_pre extends E_03_15_0004 {

	/**
	 * Entity:         <b>NUCLET</b><br>
	 * DbTable:        <b>T_AD_APPLICATION</b><p>
	 * PrimaryKey:     <b>UID.class</b><br>
	 * UID:            <b>xojr</b>
	 */
	public static final __Nuclet NUCLET = new __Nuclet();
	
	public static class __Nuclet extends _Nuclet {
		/**
		 * Field:    <b>localidentifier</b><br>
		 * DbColumn: <b>STRLOCALIDENTIFIER</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>xojrf</b>
		 */
		public final Localidentifier localidentifier = new Localidentifier();
		public static class Localidentifier extends FieldMeta.Valueable<java.lang.String> {
			public static UID UID = new UID("xojrf");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRLOCALIDENTIFIER";}
			@Override public String getFieldName() {return "localidentifier";}
			@Override public UID getEntity() {return _Nuclet.UID;}
			@Override public Integer getScale() {return 4;}
		}
		/**
		 * Field:    <b>nuclon</b><br>
		 * DbColumn: <b>BLNNUCLON</b><p>
		 * Type:     <b>java.lang.Boolean.class</b><br>
		 * UID:      <b>xojrg</b>
		 */
		public final Nuclon nuclon = new Nuclon();
		public static class Nuclon extends FieldMeta.Valueable<java.lang.Boolean> {
			public static UID UID = new UID("xojrg");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.Boolean> getJavaClass() {return java.lang.Boolean.class;}
			@Override public String getDbColumn() {return "BLNNUCLON";}
			@Override public String getFieldName() {return "nuclon";}
			@Override public UID getEntity() {return _Nuclet.UID;}
		}
		/**
		 * Field:    <b>source</b><br>
		 * DbColumn: <b>BLNSOURCE</b><p>
		 * Type:     <b>java.lang.Boolean.class</b><br>
		 * UID:      <b>xojrh</b>
		 */
		public final Source source = new Source();
		public static class Source extends FieldMeta.Valueable<java.lang.Boolean> {
			public static UID UID = new UID("xojrh");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.Boolean> getJavaClass() {return java.lang.Boolean.class;}
			@Override public String getDbColumn() {return "BLNSOURCE";}
			@Override public String getFieldName() {return "source";}
			@Override public UID getEntity() {return _Nuclet.UID;}
		}
	}
	
	/**
	 * Entity:         <b>RESOURCE</b><br>
	 * DbTable:        <b>T_MD_RESOURCE</b><p>
	 * PrimaryKey:     <b>UID.class</b><br>
	 * UID:            <b>KKZD</b>
	 */
	public static final __Resource RESOURCE = new __Resource();
	
	public static class __Resource extends _Resource {
		/**
		 * Field:    <b>content</b><br>
		 * DbColumn: <b>BLBCONTENT</b><p>
		 * Type:     <b>byte[].class</b><br>
		 * UID:      <b>KKZDf</b>
		 */
		public final Content content = new Content();
		public static class Content extends FieldMeta.Valueable<byte[]> {
			public static UID UID = new UID("KKZDf");
			@Override public UID getUID() {return UID;}
			@Override public Class<byte[]> getJavaClass() {return byte[].class;}
			@Override public String getDbColumn() {return "BLBCONTENT";}
			@Override public String getFieldName() {return "content";}
			@Override public UID getEntity() {return _Resource.UID;}
		}
	}
	
	/**
	 * Entity:         <b>IMPORTATTRIBUTE</b><br>
	 * DbTable:        <b>T_MD_IMPORTATTRIBUTE</b><p>
	 * PrimaryKey:     <b>Long.class</b><br>
	 * UID:            <b>zhxl</b>
	 */
	public static final __Importattribute IMPORTATTRIBUTE = new __Importattribute();
	
	public static class __Importattribute extends _Importattribute {
		/**
		 * Field:    <b>attributeuid</b><br>
		 * DbColumn: <b>STRUID_ATTRIBUTE</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>zhxlbTEMPUID</b>
		 */
		public final Attributeuid attributeuid = new Attributeuid();
		public static class Attributeuid extends FieldMeta.Valueable<java.lang.String> {
			public static UID UID = new UID("zhxlbTEMPUID");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRUID_ATTRIBUTE";}
			@Override public String getFieldName() {return "attribute";}
			@Override public UID getEntity() {return _Importattribute.UID;}
			@Override public UID getUnreferencedForeignEntity() {return _Entityfield.UID;}
			@Override public String getUnreferencedForeignEntityField() {return stringify(_Entityfield.Field.UID);}
			@Override public Integer getScale() {return 128;}
		}
	}
	
	/**
	 * Entity:         <b>IMPORTFEIDENTIFIER</b><br>
	 * DbTable:        <b>T_MD_IMPORTFEIDENTIFIER</b><p>
	 * PrimaryKey:     <b>Long.class</b><br>
	 * UID:            <b>2PfA</b>
	 */
	public static final __Importfeidentifier IMPORTFEIDENTIFIER = new __Importfeidentifier();
	
	public static class __Importfeidentifier extends _Importfeidentifier {
		/**
		 * Field:    <b>attributeuid</b><br>
		 * DbColumn: <b>STRUID_ATTRIBUTE</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>2PfAbTEMPUID</b>
		 */
		public final Attributeuid attributeuid = new Attributeuid();
		public static class Attributeuid extends FieldMeta.Valueable<java.lang.String> {
			public static UID UID = new UID("2PfAbTEMPUID");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRUID_ATTRIBUTE";}
			@Override public String getFieldName() {return "attributeuid";}
			@Override public UID getEntity() {return _Importfeidentifier.UID;}
			@Override public UID getUnreferencedForeignEntity() {return _Entityfield.UID;}
			@Override public String getUnreferencedForeignEntityField() {return stringify(_Entityfield.Field.UID);}
			@Override public Integer getScale() {return 128;}
		}
	}
	
	/**
	 * Entity:         <b>IMPORTIDENTIFIER</b><br>
	 * DbTable:        <b>T_MD_IMPORTIDENTIFIER</b><p>
	 * PrimaryKey:     <b>Long.class</b><br>
	 * UID:            <b>aw54</b>
	 */
	public static final __Importidentifier IMPORTIDENTIFIER = new __Importidentifier();
	
	public static class __Importidentifier extends _Importidentifier {
		/**
		 * Field:    <b>attributeuid</b><br>
		 * DbColumn: <b>STRUID_ATTRIBUTE</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>aw54bTEMPUID</b>
		 */
		public final Attributeuid attributeuid = new Attributeuid();
		public static class Attributeuid extends FieldMeta.Valueable<java.lang.String> {
			public static UID UID = new UID("aw54bTEMPUID");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRUID_ATTRIBUTE";}
			@Override public String getFieldName() {return "attributeuid";}
			@Override public UID getEntity() {return _Importidentifier.UID;}
			@Override public UID getUnreferencedForeignEntity() {return _Entityfield.UID;}
			@Override public String getUnreferencedForeignEntityField() {return stringify(_Entityfield.Field.UID);}
			@Override public Integer getScale() {return 128;}
		}
	}
	
	/**
	 * Entity:         <b>FORM</b><br>
	 * DbTable:        <b>T_UD_REPORT</b><p>
	 * PrimaryKey:     <b>UID.class</b><br>
	 * UID:            <b>2p6B</b>
	 */
	public static final __Form FORM = new __Form();
	
	public static class __Form extends _Form {
		@Override public boolean isTableMaster() {return false;}
	}
	
	/**
	 * Entity:         <b>FORMOUTPUT</b><br>
	 * DbTable:        <b>T_UD_REPORTOUTPUT</b><p>
	 * PrimaryKey:     <b>UID.class</b><br>
	 * UID:            <b>svOY</b>
	 */
	public static final __Formoutput FORMOUTPUT = new __Formoutput();
	
	public static class __Formoutput extends _Formoutput {
		@Override public boolean isTableMaster() {return false;}
	}

	/**
	 * Entity:         <b>FORMUSAGE</b><br>
	 * DbTable:        <b>T_UD_REPORTUSAGE</b><p>
	 * PrimaryKey:     <b>UID.class</b><br>
	 * UID:            <b>XwBq</b>
	 */
	public static final __FormUsage FORMUSAGE = new __FormUsage();
	
	public static class __FormUsage extends _FormUsage {
		@Override public boolean isTableMaster() {return false;}
	}
	
	/**
	 * Entity:         <b>REPORTEXECUTION</b><br>
	 * DbTable:        <b>T_UD_REPORT</b><p>
	 * PrimaryKey:     <b>UID.class</b><br>
	 * UID:            <b>rtPh</b>
	 */
	public static final __ReportExecution REPORTEXECUTION = new __ReportExecution();
	
	public static class __ReportExecution extends _ReportExecution {
		@Override public boolean isTableMaster() {return false;}
	}


	
	private final static E_04_00_0022_pre THIS = new E_04_00_0022_pre();
	
	public static String getSchemaVersion() {
		return THIS._getSchemaVersion();
	}
	
	public static Collection<EntityMeta<?>> getAllEntities() {
		return THIS._getAllEntities();
	}

	public static <PK> EntityMeta<PK> getByUID(UID entityUID) {
		return THIS._getByUID(entityUID);
	}

	public static boolean isNuclosEntity(UID entityUID) {
		return THIS._isNuclosEntity(entityUID);
	}

	public static <PK> EntityMeta<PK> getByName(String entityName) {
		return THIS._getByName(entityName);
	}
	
	public static E_04_00_0022_pre getThis() {
		return THIS;
	}
	
}
