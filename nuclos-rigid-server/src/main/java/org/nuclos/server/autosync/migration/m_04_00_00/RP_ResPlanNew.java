//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.server.autosync.migration.m_04_00_00;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXB;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.nuclos.common.UID;

@SuppressWarnings("serial")
@XmlType
@XmlRootElement(name="resplan")
class RP_ResPlanNew implements Serializable {

	@XmlAttribute(name="version", required=true)
	static final String VERSION = "0.2";

	//Group 1: Single Fields
	private UID resourceEntity;
	private UID resourceSortField;
	
	private String resourceLabelText;
	private String resourceToolTipText;
	private String cornerLabelText;
	private String cornerToolTipText;

	private String defaultViewFrom;
	private String defaultViewUntil;

	private List<RP_LocaleNew> resourceLocales;
	private List<RP_PlanElementNew> planElements;
	
	//Group 2: Entry Entity Fields (1:n to to resource Entity)
	private UID entryEntity;
	private UID referenceField;
	private UID milestoneField;

	private UID dateFromField;
	private UID dateUntilField;
	private String timePeriodsString;
	private UID timeFromField;
	private UID timeUntilField;

	private String entryLabelText;
	private String entryToolTipText;

	//Group 3: Relation Entity Fields (1:n to Resource Entity)
	private UID relationEntity;
	private UID relationFromField;
	private UID relationToField;
	
	private int relationPresentation;
	private int relationFromPresentation;
	private int relationToPresentation;
	private boolean newRelationFromController;
	//missing: Icons and Color

	//Group 4: Scripting
	//Group 4a: Single Scripting Fields
	private boolean scriptingActivated;
	private String scriptingCode;
	private String backgroundPaintMethod;
	private String scriptingResourceCellMethod;
	//Group 4b: Entry Entity Scripting Fields (1:n to Resource Entity)
	private String scriptingEntryCellMethod;
	
	//Group 5: Resourcen
	private List<RP_ResourceNew> resources;

	public RP_ResPlanNew() {
	}

	@XmlElement(name="resourceEntity")
	public UID getResourceEntity() {
		return resourceEntity;
	}

	public void setResourceEntity(UID resourceEntity) {
		this.resourceEntity = resourceEntity;
	}
	
	public void setResourceSortField(UID resourceSortField) {
		this.resourceSortField = resourceSortField;
	}

	public UID getResourceSortField() {
		return resourceSortField;
	}
	
	public RP_PlanElementNew createPlanElementFromOldData(int type) {
		final RP_PlanElementNew elem = new RP_PlanElementNew();
		elem.setType(type);
		if (type == RP_PlanElementNew.ENTRY || type == RP_PlanElementNew.MILESTONE) {
			if (entryEntity == null) return null;
			if (referenceField == null) return null;
			elem.setEntity(entryEntity);
			elem.setPrimaryField(referenceField);
			elem.setDateFromField(dateFromField);
			elem.setDateUntilField(dateUntilField);
			elem.setTimePeriodsString(timePeriodsString);
			elem.setTimeFromField(timeFromField);
			elem.setTimeUntilField(timeUntilField);
			elem.setLabelText(entryLabelText);
			elem.setToolTipText(entryToolTipText);
			elem.setScriptingEntryCellMethod(scriptingEntryCellMethod);
		} else if (type == RP_PlanElementNew.RELATION) {
			if (relationEntity == null) return null;
			if (relationFromField == null) return null;
			if (relationToField == null) return null;
			elem.setEntity(relationEntity);
			elem.setPrimaryField(relationFromField);
			elem.setSecondaryField(relationToField);
			elem.setPresentation(relationPresentation);
			elem.setFromPresentation(relationFromPresentation);
			elem.setToPresentation(relationToPresentation);
			elem.setNewRelationFromController(newRelationFromController);			
		}
		return elem;
	}
	
	//This is a migration-method.
	public List<RP_PlanElementNew> getOrMigratePlanElements() {
		if (planElements == null) {
			planElements = new ArrayList<RP_PlanElementNew>();
			RP_PlanElementNew pElement = createPlanElementFromOldData(RP_PlanElementNew.ENTRY);
			if (pElement != null) {
				planElements.add(pElement);				
			}
			pElement = createPlanElementFromOldData(RP_PlanElementNew.RELATION);
			if (pElement != null) {
				planElements.add(pElement);				
			}
		}
		return planElements;
	}

	public RP_PlanElementNew getFirstPlanElement(int type) {
		RP_PlanElementNew elem = null;
		for (RP_PlanElementNew pe : getOrMigratePlanElements()) {
			if (pe.getType() == type) {
				elem = pe;
				break;
			}
		}
		return elem;
	}
	
	public RP_PlanElementNew getFirstEntry() {
		return getFirstPlanElement(RP_PlanElementNew.ENTRY);
	}
	
	public RP_PlanElementNew getFirstRelation() {
		return getFirstPlanElement(RP_PlanElementNew.RELATION);
	}

	@XmlElement(name="planElements")
	public List<RP_PlanElementNew> getPlanElements() {
		return planElements;
	}

	public void setPlanElements(List<RP_PlanElementNew> entries) {
		this.planElements = entries;
	}

	@XmlElement(name="entryEntity")
	public UID getEntryEntity() {
		return entryEntity;
	}

	public void setEntryEntity(UID entryEntity) {
		this.entryEntity = entryEntity;
	}

	@XmlElement(name="referenceField")
	public UID getReferenceField() {
		return referenceField;
	}

	public void setReferenceField(UID referenceField) {
		this.referenceField = referenceField;
	}

	@XmlElement(name="dateFromField")
	public UID getDateFromField() {
		return dateFromField;
	}

	public void setDateFromField(UID dateFromField) {
		this.dateFromField = dateFromField;
	}

	@XmlElement(name="dateToField")
	public UID getDateUntilField() {
		return dateUntilField;
	}

	public void setDateUntilField(UID dateUntilField) {
		this.dateUntilField = dateUntilField;
	}

	@XmlElement(name="timeFromField")
	public UID getTimeFromField() {
		return timeFromField;
	}

	public void setTimeFromField(UID timeFromField) {
		this.timeFromField = timeFromField;
	}

	@XmlElement(name="timeToField")
	public UID getTimeUntilField() {
		return timeUntilField;
	}

	public void setTimeUntilField(UID timeUntilField) {
		this.timeUntilField = timeUntilField;
	}

	@XmlElement(name="timePerods")
	public String getTimePeriodsString() {
		return timePeriodsString;
	}

	public void setTimePeriodsString(String timePeriodsString) {
		this.timePeriodsString = timePeriodsString;
	}

	@XmlElement(name="resourceLabelText")
	public String getResourceLabelText() {
		return resourceLabelText;
	}

	public void setResourceLabelText(String resourceLabelText) {
		this.resourceLabelText = resourceLabelText;
	}

	@XmlElement(name="resourceToolTipText")
	public String getResourceToolTipText() {
		return resourceToolTipText;
	}

	public void setResourceToolTipText(String resourceToolTipText) {
		this.resourceToolTipText = resourceToolTipText;
	}

	@XmlElement(name="entryLabelText")
	public String getEntryLabelText() {
		return entryLabelText;
	}

	public void setEntryLabelText(String entryLabelText) {
		this.entryLabelText = entryLabelText;
	}

	@XmlElement(name="entryToolTipText")
	public String getEntryToolTipText() {
		return entryToolTipText;
	}

	public void setEntryToolTipText(String entryToolTipText) {
		this.entryToolTipText = entryToolTipText;
	}

	@XmlElement(name="cornerLabelText")
	public String getCornerLabelText() {
		return cornerLabelText;
	}

	public void setCornerLabelText(String cornerLabelText) {
		this.cornerLabelText = cornerLabelText;
	}

	@XmlElement(name="cornerToolTipText")
	public void setCornerToolTipText(String cornerToolTipText) {
		this.cornerToolTipText = cornerToolTipText;
	}

	public String getCornerToolTipText() {
		return cornerToolTipText;
	}

	@XmlElement(name="scripting")
	public boolean isScriptingActivated() {
		return scriptingActivated;
	}

	public void setScriptingActivated(boolean scriptingActivated) {
		this.scriptingActivated = scriptingActivated;
	}

	@XmlElement(name="code")
	public String getScriptingCode() {
		return scriptingCode;
	}

	public void setScriptingCode(String scriptingCode) {
		this.scriptingCode = scriptingCode;
	}

	@XmlElement(name="backgroundPaintMethod")
	public String getScriptingBackgroundPaintMethod() {
		return backgroundPaintMethod;
	}

	public void setScriptingBackgroundPaintMethod(String backgroundPaintCellMethod) {
		this.backgroundPaintMethod = backgroundPaintCellMethod;
	}

	@XmlElement(name="resourceCellMethod")
	public String getScriptingResourceCellMethod() {
		return scriptingResourceCellMethod;
	}

	public void setScriptingResourceCellMethod(String scriptingResourceCellMethod) {
		this.scriptingResourceCellMethod = scriptingResourceCellMethod;
	}

	@XmlElement(name="entryCellMethod")
	public String getScriptingEntryCellMethod() {
		return scriptingEntryCellMethod;
	}

	public void setScriptingEntryCellMethod(String scriptingEntryCellMethod) {
		this.scriptingEntryCellMethod = scriptingEntryCellMethod;
	}
	
	@XmlElement(name="resourceLocales")
	public List<RP_LocaleNew> getResourceLocales() {
		return resourceLocales;
	}

	public void setResourceLocales(List<RP_LocaleNew> resourceLocales) {
		this.resourceLocales = resourceLocales;
	}

	@XmlElement(name="resources")
	public List<RP_ResourceNew> getResources() {
		return resources;
	}

	public void setResources(List<RP_ResourceNew> resources) {
		this.resources = resources;
	}
	
	@XmlElement(name="defaultViewFrom")
	public String getDefaultViewFrom() {
		return defaultViewFrom;
	}

	public void setDefaultViewFrom(String defaultViewFrom) {
		this.defaultViewFrom = defaultViewFrom;
	}

	@XmlElement(name="defaultViewUntil")
	public String getDefaultViewUntil() {
		return defaultViewUntil;
	}

	public void setDefaultViewUntil(String defaultViewUntil) {
		this.defaultViewUntil = defaultViewUntil;
	}

	@XmlElement(name="milestoneField")
	public UID getMilestoneField() {
		return milestoneField;
	}

	public void setMilestoneField(UID milestoneField) {
		this.milestoneField = milestoneField;
	}

	@XmlElement(name="relationEntity")
	public UID getRelationEntity() {
		return relationEntity;
	}

	public void setRelationEntity(UID relationEntity) {
		this.relationEntity = relationEntity;
	}

	@XmlElement(name="relationFromField")
	public UID getRelationFromField() {
		return relationFromField;
	}

	public void setRelationFromField(UID relationFromField) {
		this.relationFromField = relationFromField;
	}

	@XmlElement(name="relationToField")
	public UID getRelationToField() {
		return relationToField;
	}

	public void setRelationToField(UID relationToField) {
		this.relationToField = relationToField;
	}

	@XmlElement(name="relationPresentation")
	public int getRelationPresentation() {
		return relationPresentation;
	}

	public void setRelationPresentation(int relationPresentation) {
		this.relationPresentation = relationPresentation;
	}

	@XmlElement(name="relationFromPresentation")
	public int getRelationFromPresentation() {
		return relationFromPresentation;
	}

	public void setRelationFromPresentation(int relationFromPresentation) {
		this.relationFromPresentation = relationFromPresentation;
	}

	@XmlElement(name="relationToPresentation")
	public int getRelationToPresentation() {
		return relationToPresentation;
	}

	public void setRelationToPresentation(int relationToPresentation) {
		this.relationToPresentation = relationToPresentation;
	}
	
	@XmlElement(name="newRelationFromController")
	public boolean isNewRelationFromController() {
		return newRelationFromController;
	}

	public void setNewRelationFromController(boolean newRelationFromController) {
		this.newRelationFromController = newRelationFromController;
	}

	public byte[] toBytes() {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		JAXB.marshal(this, out);
		return out.toByteArray();
	}

}

