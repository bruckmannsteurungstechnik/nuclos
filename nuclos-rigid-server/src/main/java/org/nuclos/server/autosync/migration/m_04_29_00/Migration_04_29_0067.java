package org.nuclos.server.autosync.migration.m_04_29_00;

import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Logger;
import org.nuclos.common.E_04_19_0017_target;
import org.nuclos.common.SysEntities;
import org.nuclos.common.UID;
import org.nuclos.server.autosync.IMigrationHelper;
import org.nuclos.server.autosync.RigidEO;
import org.nuclos.server.autosync.migration.AbstractMigration;

public class Migration_04_29_0067 extends AbstractMigration {

	private Logger LOG;

	private IMigrationHelper helper;

	public class AlreadyMigratedException extends Exception {

		private static final long serialVersionUID = 1L;

		public AlreadyMigratedException() {
			super("RoleSubform migration done already");
		}

	}

	@Override
	public void migrate() {
		if (getContext().isNucletMigration()) {
			return; // no nuclet migration here
		}

		long start = System.currentTimeMillis();
		LOG = getLogger(Migration_04_29_0067.class);
		LOG.info("Migration started");

		helper = getContext().getHelper();

		migrateResourcesToNews();

		LOG.info("Migration finished in " + new DecimalFormat("0.###").format((System.currentTimeMillis() - start) / 1000d / 60d) + " minutes");
	}

	@Override
	public SysEntities getSourceMeta() {
		return E_04_29_0066.getThis();
	}

	@Override
	public String getSourceStatics() {
		return "2.8";
	}

	@Override
	public SysEntities getTargetMeta() {
		return E_04_29_0067.getThis();
	}

	@Override
	public String getTargetStatics() {
		return "2.8";
	}

	@Override
	public String getNecessaryUntilSchemaVersion() {
		return "4.29.0066";
	}

	@Override
	public String[] getIgnoringSourceSchemaVersions() {
		return new String[]{};
	}

	private void migrateResourcesToNews() {
		LOG.info("Migrating \"LEGAL_DISCLAIMER\" resources to News...");

		// Old style disclaimers: Nuclos resources with name "LEGAL_DISCLAIMER..."
		final List<RigidEO> disclaimerResources = helper.getAll(E_04_29_0066.RESOURCE).stream()
				.filter(res -> res.getValue(E_04_19_0017_target.RESOURCE.name).startsWith("LEGAL_DISCLAIMER"))
				.collect(Collectors.toList());

		// Resulting news
		final List<RigidEO> disclaimerNews = disclaimerResources.stream()
				.map(this::resourceToNews)
				.collect(Collectors.toList());

		disclaimerNews.forEach(news -> {
			helper.insert(E_04_29_0066.NEWS, news);
			LOG.info("Migrated resource resource to News: " + news.getValue(E_04_29_0066.NEWS.name));
		});

		migratePrivacyConsent(disclaimerNews);

		// Delete old resources
		disclaimerResources.forEach(
				res -> helper.delete(E_04_29_0066.RESOURCE, res)
		);
	}

	private RigidEO resourceToNews(final RigidEO res) {
		String name = res.getValue(E_04_19_0017_target.RESOURCE.name);
		String description = res.getValue(E_04_19_0017_target.RESOURCE.description);

		boolean privacyPolicy = StringUtils.equals(description, "Datenschutz");
		UID uid = new UID();

		RigidEO news = new RigidEO(E_04_29_0066.NEWS.getUID(), uid);
		news.setValue(E_04_29_0066.NEWS.name, name);
		news.setValue(E_04_29_0066.NEWS.title, description);
		news.setValue(E_04_29_0066.NEWS.content, new String(res.getValue(E_04_19_0017_target.RESOURCE.content), StandardCharsets.UTF_8));
		news.setValue(E_04_29_0066.NEWS.active, true);
		news.setValue(E_04_29_0066.NEWS.showAtStartup, false);
		news.setValue(E_04_29_0066.NEWS.confirmationRequired, false);
		news.setValue(E_04_29_0066.NEWS.privacyPolicy, privacyPolicy);
		news.setValue(E_04_29_0066.NEWS.revision, 0);

		return news;
	}

	/**
	 * Checks all users for the "privacy consent" flag.
	 * If the flag is set, all disclaimers are confirmed for the respective user.
	 */
	private void migratePrivacyConsent(final List<RigidEO> disclaimers) {
		if (disclaimers.isEmpty()) {
			LOG.info("No disclaimers - skipping migration of privacy consent");
			return;
		}

		LOG.info("Migrating privacy consent...");

		Collection<RigidEO> users = helper.getAll(E_04_29_0066.USER);

		users.forEach(user -> {
			Boolean privacyConsent = user.getValue(E_04_29_0066.USER.privacyConsent);
			if (Boolean.TRUE.equals(privacyConsent)) {
				confirmDisclaimersForUser(disclaimers, user);

				LOG.info("Confirmed disclaimers for user: " + user.getValue(E_04_29_0066.USER.name));
			}
		});
	}

	private void confirmDisclaimersForUser(final List<RigidEO> disclaimers, final RigidEO user) {
		disclaimers.forEach(disclaimer -> {
			UID uid = new UID();

			RigidEO confirmed = new RigidEO(E_04_29_0066.NEWS_CONFIRMED.getUID(), uid);
			confirmed.setForeignUID(E_04_29_0066.NEWS_CONFIRMED.news, (UID) disclaimer.getPrimaryKey());
			confirmed.setForeignUID(E_04_29_0066.NEWS_CONFIRMED.user, (UID) user.getPrimaryKey());

			helper.insert(E_04_29_0066.NEWS_CONFIRMED, confirmed);
		});
	}


}
