//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PreDestroy;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Predicate;
import org.springframework.stereotype.Component;


@Component
public class NuclosClientThreadContextHolder {
	
	private ThreadLocal<StackTraceElement[]> threadLocal = new ThreadLocal<StackTraceElement[]>();
	
	public NuclosClientThreadContextHolder() {
	}
	
	public void setStackTraceElement(StackTraceElement[] element) {		
		threadLocal.set(element);
	}
		
	public StackTraceElement[] getStackTraceElement() {
		return threadLocal.get();
	}
	
	public boolean isSupported() {
		if(threadLocal.get() != null)
			return true;
		return false;
	}
	
	public List<StackTraceElement> getNuclosStackTraceElements() {
		StackTraceElement elments [] = threadLocal.get();
		List<StackTraceElement> lst = new ArrayList<StackTraceElement>();
		CollectionUtils.addAll(lst, elments);
		
		CollectionUtils.filter(lst, new Predicate<StackTraceElement>() {

			@Override
			public boolean evaluate(StackTraceElement object) {
				String name = object.getClassName();
				if(name.indexOf("nuclos") >= 0 || name.indexOf("nuclet") >= 0 || name.indexOf("de.") == 0)
					return true;
				return false;
			}
					
		});
		
		return lst;
	}
	
	public synchronized void clear() {
		threadLocal.remove();
	}

	@PreDestroy
	public synchronized void destroy() {
		if (threadLocal == null) return;
		
		threadLocal.remove();
		threadLocal = null;
	}

}
