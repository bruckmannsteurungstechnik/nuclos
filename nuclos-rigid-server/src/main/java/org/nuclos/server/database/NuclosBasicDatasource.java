//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.database;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.nuclos.server.common.ServerProperties;
import org.springframework.jndi.JndiTemplate;
import org.springframework.util.ResourceUtils;

public class NuclosBasicDatasource extends BasicDataSource {
	private static final Logger LOG = Logger.getLogger(ServerProperties.class);
	
	private static final String DATABASE_ADAPTER = "database.adapter";
	private static final String DATABASE_SCHEMA = "database.schema";
	private static final String DATABASE_HOME = "database.home";
	private static final String DATABASE_MSSQL_ISOLATION = "database.mssql.isolation";
	private static final String ENVIRONMENT_DEVELOPMENT = "environment.development";
	
	private Properties properties;
	
	@Override
	protected synchronized DataSource createDataSource() throws SQLException {
		if (properties == null) {
			properties = ServerProperties.loadProperties(ServerProperties.JNDI_SERVER_PROPERTIES);			
		}
		String sAdapter = properties.getProperty(DATABASE_ADAPTER);
		if ("oracle".equalsIgnoreCase(sAdapter)) {
			setValidationQuery("SELECT 1 FROM DUAL");
			setValidationQueryTimeout(5);
			setTestOnBorrow(true);
		}
		return super.createDataSource();
	}
	
	@PostConstruct
	public void setInitSqlStatements() {
		if (properties == null) {
			properties = ServerProperties.loadProperties(ServerProperties.JNDI_SERVER_PROPERTIES);			
		}
		String sAdapter = properties.getProperty(DATABASE_ADAPTER).toLowerCase();
		String sDBSchema = properties.getProperty(DATABASE_SCHEMA);
		String sMSSQLIsolation = properties.getProperty(DATABASE_MSSQL_ISOLATION);
		String sConnectionInitStatement = loadConnectionInitStatement();
		
		Collection<String> colInitSqls = new ArrayList<String>();
		if (sConnectionInitStatement != null) {
			colInitSqls.add(sConnectionInitStatement);
		} else if ("postgresql".equals(sAdapter)) {
			colInitSqls.add("set search_path to "+ sDBSchema + ",public");
		} else if ("oracle".equals(sAdapter)) {
			colInitSqls.add("alter session set nls_comp='BINARY' nls_sort='BINARY'");
		} else if ("h2".equals(sAdapter)) {
			String url = getUrl() + ":" + getDBFilePath() + ";MODE=PostgreSQL;MV_STORE=FALSE;MVCC=FALSE";
			if (Boolean.parseBoolean(properties.getProperty(ENVIRONMENT_DEVELOPMENT))) {
				url += ";AUTO_SERVER=TRUE";
			}
			setUrl(url);
			colInitSqls.add("SET SCHEMA " + sDBSchema);
		} else if ("mssql".equals(sAdapter) && "snapshot".equals(sMSSQLIsolation)) {
			colInitSqls.add("set transaction isolation level snapshot");
		}
		setConnectionInitSqls(colInitSqls);
	}
	
	public String getDBFilePath() {
		if (properties == null) {
			properties = ServerProperties.loadProperties(ServerProperties.JNDI_SERVER_PROPERTIES);			
		}
		String sDBSchema = properties.getProperty(DATABASE_SCHEMA);
		String sDBPath = properties.getProperty(DATABASE_HOME);
		if (sDBPath == null || sDBPath.isEmpty()) {
			sDBPath = System.getProperty("user.home") + "/.h2";
		}
		File file = new File(sDBPath);
		if (!file.exists()) {
			file.mkdirs();
		}
		
		return sDBPath + "/" + sDBSchema;
	}
	
	private String loadConnectionInitStatement() {
		String sLocation = null;
		try {
			sLocation = new JndiTemplate().lookup("java:comp/env/nuclos-conf-connection-init", String.class);
		} catch (Exception ex) {
			// ignore
		}
		if (sLocation == null) {
			LOG.info("db-connection-init.sql not found. using defaults");
			return null;
		}
		try (InputStream is = new BufferedInputStream(new FileInputStream(ResourceUtils.getFile(sLocation)))) {
			return IOUtils.toString(is, StandardCharsets.UTF_8);
		} catch (Exception e) {
			LOG.warn("Couldnt not load db-connection-init.sql. using defaults");
			return null;
		}
		
	}
}
