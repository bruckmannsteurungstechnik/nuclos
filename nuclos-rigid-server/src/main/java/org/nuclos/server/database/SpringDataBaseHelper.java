//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.database;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.commons.lang.NullArgumentException;
import org.apache.log4j.Logger;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SysEntities;
import org.nuclos.common.UID;
import org.nuclos.common.Version;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.autosync.AutoDbSetup;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.ServerProperties;
import org.nuclos.server.dblayer.DbAccess;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.DbType;
import org.nuclos.server.dblayer.statements.DbBuildableStatement;
import org.nuclos.server.dblayer.structure.DbTableType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;

/**
 * Class containing general static helper functions.<br>
 *
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:martin.weber@novabit.de">martin.weber</a>
 * @version 01.00.00
 */
// @Component("dataBaseHelper")
public class SpringDataBaseHelper {

	private static final Logger LOG = Logger.getLogger(SpringDataBaseHelper.class);

	public static final String DEFAULT_SEQUENCE = "IDFACTORY";
	
	private Map<String, String> config;
	
	private static SpringDataBaseHelper INSTANCE;
	
	//
	
	private DataSource dataSource;

	// 
	
	private DbAccess defaultDbAccess;
	
//	private ApplicationProperties applicationProperties;

	SpringDataBaseHelper() {
		INSTANCE = this;
	}
	
	public static SpringDataBaseHelper getInstance() {
		if (INSTANCE.defaultDbAccess == null) {
			throw new IllegalStateException("too early");
		}
		return INSTANCE;
	}
	
	@Autowired
	@Qualifier("nuclos")
	void setDataSource(DataSource dataSource) {
		try {
			DataSource customDataSource = new JndiDataSourceLookup().getDataSource("jdbc/nuclos-jdbc-resource");

			if (customDataSource != null) {
				Connection con = customDataSource.getConnection();
				if (con != null && con.isValid(5)) {
					LOG.debug("Found custom DataSource: " + customDataSource);
					this.dataSource = customDataSource;
				}
			}
		} catch (Exception e) {
			LOG.debug("Custom DataSource could not be found or used; falling back", e);
			this.dataSource = dataSource;
		}
	}
	
//	@Autowired
//	void setApplicationProperties(ApplicationProperties applicationProperties) {
//		this.applicationProperties = applicationProperties;
//	}

	@PostConstruct
	final void init() {
		try {
			config = new HashMap<String, String>();
			for (Map.Entry<?, ?> p : ServerProperties.loadProperties(ServerProperties.JNDI_SERVER_PROPERTIES).entrySet()) {
				if (p.getKey().toString().startsWith("database."))
					config.put(p.getKey().toString().substring(9), p.getValue().toString());
			}

			if (!config.containsKey(DbAccess.SCHEMA)) {
				throw new NuclosFatalException("Missing database.schema specification");
			}

			File structureChangelogDir = NuclosSystemParameters.getDirectory(NuclosSystemParameters.DATABASE_STRUCTURE_CHANGE_LOG_PATH);
			if (structureChangelogDir != null) {
				String path = structureChangelogDir.getAbsolutePath();
				LOG.info("Set structure change log directory to " + path);
				config.put(DbAccess.STRUCTURE_CHANGELOG_DIR, path);
			}

			// DataSource dataSource = NuclosDataSources.getDefaultDS();
			defaultDbAccess = getDbAccessFor(dataSource, config);

		} catch (Exception ex) {
			LOG.fatal("Error initializing Nuclos datasource/database access", ex);
			throw new NuclosFatalException("Error initializing Nuclos datasource/database access: " + ex, ex);
		}
	}
	
	public void autoDbSetup(SysEntities sysEntities, Version nuclosVersion) {
		try {
			AutoDbSetup autoSetup = new AutoDbSetup(defaultDbAccess, sysEntities, nuclosVersion);
			boolean doAutoSetup = "true".equals(config.get("autosetup"));
	
			LOG.info("Nuclos auto-setup is " + (doAutoSetup ? "enabled" : "disabled"));
			if (doAutoSetup) {
				autoSetup.run();
			}
		} catch (Exception ex) {
			LOG.fatal("Error auto db setup", ex);
			throw new NuclosFatalException("Error AutoDbSetup: " + ex, ex);
		}
	}

	public DbAccess getDbAccess() {
		return defaultDbAccess;
	}

	public DbAccess getDbAccessFor(DataSource dataSource, Map<String, String> config) {
		DbType dbType;
		String typeId = config.get("adapter");
		if (config.containsKey("type"))
			typeId = config.get("type");
		if (typeId != null && !typeId.isEmpty()) {
			dbType = DbType.getFromName(typeId);
			if (dbType == null) {
				throw new NuclosFatalException("Unsupported database type " + typeId);
			}
		} else {
			try {
				dbType = DbType.getFromMetaData(dataSource);
				LOG.info("Auto-determine database type: " + dbType);
			} catch (SQLException ex) {
				throw new NuclosFatalException("Error while determining database vendor and version", ex);
			}
			if (dbType == null) {
				throw new NuclosFatalException("Cannot determine database vendor and version");
			}
		}
		return dbType.createDbAccess(dataSource, config);
	}

	public int execute(DbBuildableStatement command) throws DbException {
		return getDbAccess().execute(command);
	}

	/**
	 * §precondition datasource != null
	 * 
	 * @return a connection from the given datasource. Must be closed by the caller in a finally block.
	 */
	public Connection getConnection(DataSource datasource) {
		if (datasource == null) {
			throw new NullArgumentException("datasource");
		}
		try {
			return datasource.getConnection();
		}
		catch (SQLException ex) {
			throw new CommonFatalException("Connection to datasource could not be initialized.", ex);
		}
	}

	/**
	 * §precondition datasource != null
	 * 
	 * @return a connection from the given datasource. Must be closed by the caller in a finally block.
	 */
	public String getCurrentConnectionInfo() {
		if (dataSource == null) {
			throw new NullArgumentException("datasource");
		}
		Connection conn = null;
		try {
			conn = dataSource.getConnection();
			return conn.toString() + " @schema=" + getDbAccess().getSchemaName();
		}
		catch (SQLException ex) {
			throw new CommonFatalException("Connection to datasource could not be initialized.", ex);
		}
		finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception ex) {
				LOG.error(ex.getMessage(), ex);
			}
		}
	}

	public Long getNextIdAsLong(String sSequenceName) {
		try {
			return getDbAccess().getDbExecutor().getNextId(sSequenceName);
		}
		catch (SQLException e) {
			// Wrap in non-check exception for convenience.
			throw new IllegalStateException(e);
		}
	}
	
	public Long getNextSequentialNumberForEntity(UID entityUid) {
		try {
			return getDbAccess().getDbExecutor().getNextSequentialNumberForEntity(entityUid);
		}
		catch (SQLException e) {
			// Wrap in non-check exception for convenience.
			throw new IllegalStateException(e);
		}
	}
	
	public boolean isTableAvailable(EntityMeta<?> sTable) {
		if (getDbAccess().getTableNames(DbTableType.TABLE).contains(sTable.getDbTable()))
			return true;
		if (getDbAccess().getTableNames(DbTableType.TABLE).contains(sTable.getDbTable().toLowerCase()))
			return true;

		return false;
	}

	public boolean isViewAvailable(EntityMeta<?> sTable) {
		if (getDbAccess().getTableNames(DbTableType.VIEW).contains(sTable.getDbTable()))
			return true;
		if (getDbAccess().getTableNames(DbTableType.VIEW).contains(sTable.getDbTable().toLowerCase()))
			return true;

		return false;
	}

	public boolean isObjectAvailable(EntityMeta<?> sObjectName) {
		return isTableAvailable(sObjectName) ||isViewAvailable(sObjectName);
	}
	
} // class DataBaseHelper
