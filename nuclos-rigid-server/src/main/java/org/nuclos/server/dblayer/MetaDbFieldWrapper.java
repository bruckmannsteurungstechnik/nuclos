//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dblayer;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.RigidE;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.server.autosync.RigidEO;

public class MetaDbFieldWrapper {
	
	private final static RigidE._Entityfield FIELD = new RigidE._Entityfield() {}; 
	
	private final RigidEO rField;
	private final FieldMeta<?> fieldMeta;
	private String alias;
	private boolean bIsNull;
	
	private String overwriteDbColumn = null;
	
	public MetaDbFieldWrapper() {
		this.rField = null;
		this.fieldMeta = null;
	}
	
	public MetaDbFieldWrapper(RigidEO rField) {
		this.rField = rField;
		this.fieldMeta = null;
	}

	public MetaDbFieldWrapper(FieldMeta<?> fieldMeta) {
		this.rField= null;
		this.fieldMeta = fieldMeta;
	}

	public UID getUID() {
		if (fieldMeta != null)
			return fieldMeta.getUID();
		return (UID) rField.getPrimaryKey();
	}

	public boolean isLocalized() {
		boolean retVal = false;
		
		if (fieldMeta != null) {
			retVal = fieldMeta.isLocalized();
		}
		
		return retVal;
	}
	
	public String getFieldName() {
		if (fieldMeta != null)
			return fieldMeta.getFieldName();
		return (String) rField.getValue(FIELD.field);
	}

	public String getDbColumn() {
		if (overwriteDbColumn != null) {
			return overwriteDbColumn;
		}
		if (fieldMeta != null)
			return fieldMeta.getDbColumn();
		return (String) rField.getValue(FIELD.dbfield);
	}
	
	public void overwriteDbColumn(String dbColumn) {
		this.overwriteDbColumn = dbColumn;
	}
	
	public UID getEntity() {
		if (fieldMeta != null)
			return fieldMeta.getEntity();
		return rField.getForeignUID(FIELD.entity);
	}

	public boolean isNullable() {
		if (fieldMeta != null)
			return fieldMeta.isNullable();
		return Boolean.TRUE.equals(rField.getValue(FIELD.nullable));
	}

	public boolean isUnique() {
		if (fieldMeta != null)
			return fieldMeta.isUnique();
		return Boolean.TRUE.equals(rField.getValue(FIELD.unique));
	}

	public boolean isIndexed() {
		if (fieldMeta != null)
			return fieldMeta.isIndexed();
		return Boolean.TRUE.equals(rField.getValue(FIELD.indexed));
	}

	public boolean isOnDeleteCascade() {
		if (fieldMeta != null)
			return fieldMeta.isOnDeleteCascade();
		return Boolean.TRUE.equals(rField.getValue(FIELD.ondeletecascade));
	}

	public boolean isReadonly() {
		if (fieldMeta != null)
			return fieldMeta.isReadonly();
		return Boolean.TRUE.equals(rField.getValue(FIELD.readonly));
	}

	public UID getForeignEntity() {
		if (fieldMeta != null)
			return fieldMeta.getForeignEntity();
		return rField.getForeignUID(FIELD.foreignentity);
	}

	public String getForeignEntityField() {
		if (fieldMeta != null)
			return fieldMeta.getForeignEntityField();
		return (String) rField.getValue(FIELD.foreignentityfield);
	}
	
	public UID getUnreferencedForeignEntity() {
		if (fieldMeta != null)
			return fieldMeta.getUnreferencedForeignEntity();
		return null;
	}

	public String getUnreferencedForeignEntityField() {
		if (fieldMeta != null)
			return fieldMeta.getUnreferencedForeignEntityField();
		return null;
	}

	public String getDataType() {
		if (fieldMeta != null)
			return fieldMeta.getDataType();
		return (String) rField.getValue(FIELD.datatype);
	}

	public Integer getScale() {
		if (fieldMeta != null)
			return fieldMeta.getScale();
		return (Integer) rField.getValue(FIELD.datascale);
	}

	public Integer getPrecision() {
		if (fieldMeta != null)
			return fieldMeta.getPrecision();
		return (Integer) rField.getValue(FIELD.dataprecision);
	}

	public String getCalcFunction() {
		if (fieldMeta != null)
			return fieldMeta.getCalcFunction();
		return (String) rField.getValue(FIELD.calcfunction);
	}
	
	public UID getCalcAttributeDS() {
		if (fieldMeta != null)
			return fieldMeta.getCalcAttributeDS();
		return rField.getForeignUID(FIELD.calcAttributeDS);
	}

	public String getDefaultMandatory() {
		if (fieldMeta != null)
			return fieldMeta.getDefaultMandatory();
		return (String) rField.getValue(FIELD.defaultmandatory);
	}

	public UID getLookupEntity() {
		if (fieldMeta != null)
			return fieldMeta.getLookupEntity();
		return  rField.getForeignUID(FIELD.lookupentity);
	}

	public String getLookupEntityField() {
		if (fieldMeta != null)
			return fieldMeta.getLookupEntityField();
		return (String) rField.getValue(FIELD.lookupentityfield);
	}
	
	public boolean isColumnMaster() {
		if (fieldMeta != null)
			return fieldMeta.isColumnMaster();
		return true;
	}

	public boolean isIntegrationComplete() {
		if (fieldMeta != null)
			return fieldMeta.isIntegrationComplete();
		return true;
	}
	
	public Integer getOrder() {
		if (fieldMeta != null) {
			return fieldMeta.getOrder();
		}
		return (Integer) rField.getValue(FIELD.order);
	}

	public final boolean isCalculated() {
		if (fieldMeta != null) {
			return (fieldMeta.getCalcFunction() != null) || (fieldMeta.getCalcAttributeDS() != null);
		}
		return rField.getValue(FIELD.calcfunction) != null || rField.getValue(FIELD.calcAttributeDS) != null;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(final String alias) {
		this.alias = alias;
	}

	public boolean isNull() {
		return bIsNull;
	}

	public void setIsNull(final boolean bIsNull) {
		this.bIsNull = bIsNull;
	}

	@Override
	public int hashCode() {
		final UID pk = getUID();
		if (pk != null)
			return pk.getString().hashCode();
		return super.hashCode();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that)
			return true;
		if (that instanceof MetaDbFieldWrapper) {
			RigidUtils.equal(((MetaDbFieldWrapper) that).getUID(), getUID());
		}
		return super.equals(that);
	}

	@Override
	public String toString() {
		return String.format("MetaDbFieldWrapper[field=%s, column=%s, uid=%s, entity=%s", getFieldName(), getDbColumn(), getUID(), getEntity());
	}

}
