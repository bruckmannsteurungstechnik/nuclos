//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dblayer.impl;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.JsonObject;
import javax.sql.DataSource;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.IDatabaseConnection;
import org.nuclos.common.JsonUtils;
import org.nuclos.common.NuclosImage;
import org.nuclos.common.NuclosPassword;
import org.nuclos.common.NuclosScript;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.StackTraceParser;
import org.nuclos.common.UID;
import org.nuclos.common.report.ByteArrayCarrier;
import org.nuclos.common2.CloseableXStream;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.XStreamSupport;
import org.nuclos.server.common.MyDataBaseConnection;
import org.nuclos.server.common.NuclosClientThreadContextHolder;
import org.nuclos.server.common.valueobject.DocumentFileBase;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.expression.DbCurrentDate;
import org.nuclos.server.dblayer.expression.DbCurrentDateTime;
import org.nuclos.server.dblayer.expression.DbId;
import org.nuclos.server.dblayer.expression.DbIncrement;
import org.nuclos.server.dblayer.expression.DbNull;
import org.nuclos.server.dblayer.impl.util.JDBCType;
import org.nuclos.server.dblayer.impl.util.PreparedString;
import org.nuclos.server.dblayer.impl.util.RequestInfo;
import org.nuclos.server.dblayer.incubator.DbExecutor;
import org.nuclos.server.dblayer.incubator.SQLInfo;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.thoughtworks.xstream.XStream;

/** This class is an implementation class, only use in exceptional cases! */
public abstract class DataSourceExecutor implements DbExecutor {

	private static ThreadLocal<RequestInfo> REQUEST_INFO = new ThreadLocal<>();

	private static final Logger log = Logger.getLogger(DataSourceExecutor.class);
    private static final Logger SQLLogger = Logger.getLogger("SQLLogger");
	private static final Logger SQLTimer = Logger.getLogger("SQLTimer");
	private static final Logger ClientStackTrace = Logger.getLogger("ClientStackTrace");
	private static final Logger ServerStackTrace = Logger.getLogger("ServerStackTrace");
	private static final Logger SQLUpdate = Logger.getLogger("SQLUpdate");
	private static final Map<Logger, Level> mapLevels = new HashMap<Logger, Level>();
	private static String debugSQL;
	private static Integer minExecutionTimeMS;
	static {
		mapLevels.put(SQLLogger, SQLLogger.getLevel());
		mapLevels.put(SQLTimer, SQLTimer.getLevel());
		mapLevels.put(ClientStackTrace, ClientStackTrace.getLevel());
		mapLevels.put(ServerStackTrace, ServerStackTrace.getLevel());
		mapLevels.put(SQLUpdate, SQLUpdate.getLevel());
	}

	public static void setMinExecutionTimeMS(Integer minExecTimeMS) {
		minExecutionTimeMS = minExecTimeMS;
	}

	public static Integer getMinExecutionTimeMS() {
		return minExecutionTimeMS;
	}

	public static void setDebugSQL(String debugSQL) {
		DataSourceExecutor.debugSQL = debugSQL;
	}

	public static String getDebugSQL() {
		return debugSQL;
	}
	
	private static boolean testForFullDebug(final ConnectionRunner<?> runner) {		
    	//This is a way to get complete debug information for a special SQL-statement. Never skip the first condition for performance
    	if (getDebugSQL() != null && runner.getInfo().toString().contains(getDebugSQL())) {
    		for (Logger logger : mapLevels.keySet()) {
    			logger.setLevel(Level.DEBUG);
    		}
    		return true;
    	}
    	return false;
	}

	public static void initRequestInfo() {
		REQUEST_INFO.set(new RequestInfo(SQLLogger.isDebugEnabled(), SQLTimer.isDebugEnabled()));
	}

	public static RequestInfo getRequestInfo() {
		return REQUEST_INFO.get();
	}

	private static void countSQLs(long executionTimeInMs, SQLInfo info) {
		RequestInfo requestInfo = REQUEST_INFO.get();
		if (requestInfo != null) {
			requestInfo.countSQL(executionTimeInMs, info.isInsertUpdateDelete());
		}
	}
	
    private static long beforeExecution(final ConnectionRunner<?> runner) {
    	final long time1 = System.currentTimeMillis();
    	final boolean fullDebug = testForFullDebug(runner);
    	if (SQLLogger.isDebugEnabled()) {
    		if (!SQLTimer.isDebugEnabled()) {
    			countSQLs(0, runner.getInfo());
			}
    		if (fullDebug || minExecutionTimeMS == null) {
				logStackTrace();
				String msg = runner.getInfo().toString();
				SQLLogger.debug(msg);
			}
		}
    	if (fullDebug) {
    		resetDebugLevels();
    	}
    	return time1;
    }
    
    public static void afterExecution(final ConnectionRunner<?> runner, long time1) {
		if (time1 < 0) {
			if (SQLTimer.isTraceEnabled()) {
				Logger logger = Logger.getLogger("SQLTrace" + (-time1));
				logger.setLevel(Level.ALL);
				logger.trace(runner.getInfo().toString(time1));
			}
			return;
		}
		final boolean bSQLTimerDebug = SQLTimer.isDebugEnabled();
		if (bSQLTimerDebug || SQLUpdate.isDebugEnabled()) {
			SQLInfo sqlInfo = runner.getInfo();
			if (bSQLTimerDebug || sqlInfo.isInsertUpdateDelete()) {
				long diff = System.currentTimeMillis() - time1;
				if (bSQLTimerDebug) {
					countSQLs(diff, sqlInfo);
				}
				if (minExecutionTimeMS == null || diff >= minExecutionTimeMS) {
					String msg = sqlInfo.toString(diff);
					if (bSQLTimerDebug) {
						SQLTimer.debug(msg);
					} else {
						SQLUpdate.debug(msg);
					}
				}
			}
    	}
    }
    
	private static void resetDebugLevels() {
   		for (Logger logger : mapLevels.keySet()) {
			logger.setLevel(mapLevels.get(logger));
		}		
	}

    private static void logStackTrace() {
       if (ServerStackTrace.isDebugEnabled()) {
    	   List<StackTraceElement> lstStack = Arrays.asList(Thread.currentThread().getStackTrace());
    	   logStackTrace(lstStack, ServerStackTrace, "Server Stack Trace:", ServerStackTrace.isTraceEnabled());
       }
       if (ClientStackTrace.isDebugEnabled()) {
    	   List<StackTraceElement> lstStack = getClientStackTrace();    	   
    	   logStackTrace(lstStack, ServerStackTrace, "Client Stack Trace:", ClientStackTrace.isTraceEnabled());
       }
    }

	private static void logStackTrace(List<StackTraceElement> stackTraceElements, Logger logger, String msg, boolean all) {
		final StackTraceParser parser = new StackTraceParser(stackTraceElements);

		StringBuilder sb = new StringBuilder(msg);
		sb.append("\n");

		List<String> stackFrames = all ? parser.getStackFrames() : parser.getShortenedStackFrames();
		stackFrames.forEach(
				frame -> {
					sb.append(frame);
					sb.append("\n");
				}
		);

		logger.debug(sb.toString());
	}
   
    private static List<StackTraceElement> getClientStackTrace() {
    	try {
	        if (!SpringApplicationContextHolder.isSpringReady()) {
	        	return null;
	        }
	        NuclosClientThreadContextHolder holder = SpringApplicationContextHolder.getBean(NuclosClientThreadContextHolder.class);
	 	    if (holder.isSupported()) {
	 		    return holder.getNuclosStackTraceElements();
	 	    }
    	} catch (Exception e) {
    		log.warn(e.getMessage(), e);
    	}
 	    return null;    		
    }
    
	private final DataSource dataSource;
	private final String username; 
	
	public DataSourceExecutor(DataSource dataSource, String username, String password) {
		this.dataSource = dataSource;
		this.username = username;
	}
	
	//This is the method everything to and from the DB comes through
	@Override
	public final <T> T execute(ConnectionRunner<T> runner) throws SQLException {
		long time1 = beforeExecution(runner);
		final Connection conn = getConnection();
		try {
			return runner.perform(conn);
		} finally {
			DataSourceUtils.releaseConnection(conn, dataSource);
			afterExecution(runner, time1);
		}
	}

	@Override
	public final <T> T executeQuery(final String sql, final ResultSetRunner<T> runner) throws SQLException {
		ConnectionRunner<T> connRunner = new ConnectionRunner<T>() {
			@Override
			public T perform(Connection conn) throws SQLException {
				Statement stmt = conn.createStatement();
				try {
					afterExecution(this, -1);
					ResultSet rs = stmt.executeQuery(sql);
					afterExecution(this, -2);
					try {
						return runner.perform(rs);
					} finally {
						afterExecution(this, -3);
						rs.close();
					}
                } catch (SQLException e) {
            		log.error("SQL query failed with " + e.toString() + ":\n\t" + sql);
                	throw e;
				} finally {
					stmt.close();
				}
			}

			@Override
			public SQLInfo getInfo() {
				return new SQLInfo(sql);
			}
		};
		return execute(connRunner);
	}

	@Override
	public final int executeUpdate(final String sql) throws SQLException {
		ConnectionRunner<Integer> connRunner = new ConnectionRunner<Integer>() {
			@Override
			public Integer perform(Connection conn) throws SQLException {
				Statement stmt = conn.createStatement();
				try {
					return stmt.executeUpdate(sql);
                } catch (SQLException e) {
            		log.error("SQL update failed with " + e.toString() + ":\n\t" + sql);
                	throw e;
				} finally {
					stmt.close();
				}
			}

			@Override
			public SQLInfo getInfo() {
				return new SQLInfo(sql);
			}
		};
		return execute(connRunner);
	}
	
	@Override
    public final int prepareStatementParameters(PreparedStatement stmt, Object[] values) throws SQLException {
        return prepareStatementParameters(stmt, 1, Arrays.asList(values));
    }

	@Override
    public final int prepareStatementParameters(PreparedStatement stmt, int index, Iterable<Object> values) throws SQLException {
        java.util.Date now = new java.util.Date();
        for (Object param : values) {
            if (param == null) {
                // Note, db null values have to be wrapped as DbNull object
                throw new IllegalArgumentException("Missing prepared statement parameter #" + index);
            }
            Class<?> javaType = param.getClass();
            if (javaType == DbIncrement.class) {
                // TODO: this behavior matches buildUpdateString but a better handling is required
                continue;
            } else if (javaType == DbId.class) {
                DbId dbId = (DbId) param;
                dbId.setIdValue(getNextId(((DbId) param).getSequenceName()));
                param = dbId.getIdValue();
                javaType = param.getClass();
            } else if (javaType == DbCurrentDate.class) {
                param = RigidUtils.getPureDate(now);
                javaType = param.getClass();
            } else if (javaType == DbCurrentDateTime.class) {
                param = new Timestamp(now.getTime());
                javaType = param.getClass();
            } else if (javaType == DbNull.class) {
                javaType = ((DbNull<?>) param).getJavaType();
                if (javaType == java.util.Date.class) {
                    javaType = java.sql.Date.class;                	
                }
                param = null;
            }
            
        	setStatementParameter(stmt, index++, param, javaType);
		}
        return index;
    }

	public void setStatementParameter(PreparedStatement stmt, int index, Object value, Class<?> javaType)
			throws SQLException {
		if (value == null) {
			stmt.setNull(index, getPreferredSqlTypeFor(javaType));
		} else if (value instanceof String) {
			stmt.setString(index, (String) value);
		} else if (value instanceof NuclosPassword) {
			stmt.setString(index, ((NuclosPassword) value).getValue());
		} else if (value instanceof UID) {
			stmt.setString(index, ((UID) value).getString());
		} else if (value instanceof UID[]) {
			UID[] uids = (UID[]) value;
			if (uids.length == 0) {
				stmt.setNull(index, getPreferredSqlTypeFor(String.class));
			} else {
				StringBuffer stringified = new StringBuffer();
				for (int i = 0; i < uids.length; i++) {
					stringified.append(uids[i].getStringifiedDefinition());
					if (i < uids.length - 1) {
						stringified.append(", ");
					}
				}
				stmt.setString(index, stringified.toString());
			}
		} else if (value instanceof Integer) {
			stmt.setInt(index, (Integer) value);
		} else if (value instanceof Long) {
			stmt.setLong(index, (Long) value);
		} else if (value instanceof Boolean) {
			stmt.setBoolean(index, (Boolean) value);
		} else if (value instanceof Double) {
			stmt.setDouble(index, (Double) value);
		} else if (value instanceof BigDecimal) {
			stmt.setBigDecimal(index, (BigDecimal) value);
		} else if (value instanceof java.sql.Timestamp) {
			stmt.setTimestamp(index, (java.sql.Timestamp) value);
		} else if (value instanceof Date) {
			if (InternalTimestamp.class.isAssignableFrom(javaType)) {
				stmt.setTimestamp(index, new java.sql.Timestamp(((InternalTimestamp) value).getTime()));
			} else {
				stmt.setDate(index, new java.sql.Date(((Date) value).getTime()));
			}
		} else if (value instanceof byte[]) {
			stmt.setBytes(index, (byte[]) value);
		} else if (value instanceof ByteArrayCarrier) {
			stmt.setBytes(index, ((ByteArrayCarrier) value).getData());
		} else if (value instanceof Enum) {
			stmt.setString(index, value.toString());
		} else if (value instanceof NuclosImage) {
			NuclosImage ni = (NuclosImage) value;
			if (ni.getContent() != null) {
				ByteArrayCarrier bac = new ByteArrayCarrier(ni.getContent());
				stmt.setBytes(index, bac.getData());
			} else {
				stmt.setNull(index, getPreferredSqlTypeFor(javaType));
			}
		} else if (value instanceof DocumentFileBase) {
			stmt.setString(index, ((DocumentFileBase) value).getFilename());
		} else if (value instanceof NuclosScript) {
			final XStreamSupport xs = XStreamSupport.getInstance();
			try (CloseableXStream closeable = xs.getCloseableStream()) {
				final XStream xstream = closeable.getStream();
				stmt.setString(index, xstream.toXML(value));
			}
		} else if (value instanceof JsonObject) {
			stmt.setString(index, JsonUtils.objectToString((JsonObject) value));
		} else {
			throw new SQLException("Java type " + javaType + " cannot be mapped to DB type");
		}
	}

	protected Connection getConnection() throws SQLException {
		if (dataSource != null) {
			return DataSourceUtils.getConnection(dataSource);
		}
		return null;
	}
	
	public PreparedStatement getPreparedStatement(PreparedString ps) throws SQLException {
		PreparedStatement stmt = getConnection().prepareStatement(ps.toString());
		prepareStatementParameters(stmt, ps.getParameters());
		return stmt;
	}

	@Override
	public final int getPreferredSqlTypeFor(Class<?> javaType) throws DbException {
		if (javaType == java.util.Date.class) {
			javaType = java.sql.Date.class;
		} else if (InternalTimestamp.class.isAssignableFrom(javaType)) {
			javaType = java.sql.Timestamp.class;
		} else if (javaType == NuclosPassword.class) {
			javaType = java.lang.String.class;
		} else if (javaType == NuclosImage.class) {
			javaType = byte[].class;
		} else if (javaType == NuclosScript.class) {
			javaType = java.lang.String.class;
		} else if (javaType == UID.class) {
			javaType = java.lang.String.class;
		}
		return JDBCType.getJDCBTypesForObjectType(javaType)[0].getSqlType();
	}

	@Override
	public void makePersistent() throws SQLException {
		commit();
	}

	@Override
	public void commit() throws SQLException {
		getConnection().commit();
	}

	protected String getDbAliasForCurrentDate() {
		return "CURRENT_DATE";
	}
	
	@Override
	public Long getNextSequentialNumberForEntity(UID moduleUID) throws SQLException {
		Long iresult = null;
		String sModuleId = "'" + moduleUID.getString() + "'";
		String dbAliasForCurrentDate = getDbAliasForCurrentDate();
		String sql1 = "SELECT COUNT(*) FROM T_AD_MODULE_SEQUENTIALNUMBER WHERE strmodule_uid = " + sModuleId;
		ResultSetRunner<Long> runner = new ResultSetRunner<Long>() {
			@Override
			public Long perform(ResultSet rs) throws SQLException {
				if (rs.next()) {
					return rs.getLong(1);
				}
				return null;
			}
		};
		
		Long iCount = executeQuery(sql1, runner);
		if (iCount == null || iCount.equals(0L)) {
			String sql2 = "INSERT INTO T_AD_MODULE_SEQUENTIALNUMBER (strmodule_uid, datlastreset, intnextval)"
		      + " VALUES (" + sModuleId + ", " + dbAliasForCurrentDate + ", 0)";
			executeUpdate(sql2);
		    iresult = 1L;
		}
		
		String sql3 = "SELECT intnextval, datlastreset, " + dbAliasForCurrentDate
				   + " FROM T_AD_MODULE_SEQUENTIALNUMBER WHERE strmodule_uid = " + sModuleId
				   + " FOR UPDATE";
		ResultSetRunner<Map<Long, Date>> runner3 = new ResultSetRunner<Map<Long, Date>>() {
			@Override
			public Map<Long, Date> perform(ResultSet rs) throws SQLException {
				if (rs.next()) {
					Map<Long, Date> map = new HashMap<Long, Date>();
					map.put(rs.getLong(1), rs.getDate(2));
					map.put(-1L, rs.getDate(3));
					return map;
				}
				return null;
			}
		};
		
		Map<Long, Date> result3 = executeQuery(sql3, runner3);
		if (result3 != null) {
			Date now = result3.remove(-1L);
			iresult = result3.keySet().iterator().next();
			Date dlastreset = result3.get(iresult);
			if (dlastreset == null) {
				// TODO: NUCLOS-5549 Column INTNEXTVAL in T_AD_MODULE_SEQUENTIALNUMBER is still Integer, should be Long
				dlastreset = result3.get(iresult.intValue());
			}
			if (dlastreset.getMonth() != now.getMonth() || dlastreset.getYear() != now.getYear()) {
				String sql4 = "UPDATE T_AD_MODULE_SEQUENTIALNUMBER"
		         + " SET datlastreset = " + dbAliasForCurrentDate;
				executeUpdate(sql4);
				iresult = 1L;
			}
		}
		
		String sql5 = "UPDATE T_AD_MODULE_SEQUENTIALNUMBER"
		         + " SET intnextval = " + (iresult + 1)
		         + " WHERE strmodule_uid = " + sModuleId;
		executeUpdate(sql5);
		return iresult;
	}
	
	protected final Connection getOwnConnection() throws SQLException {
		return dataSource.getConnection();
	}
	
	@Override
	public IDatabaseConnection getDbUnitConnection(String schema, boolean bCreateOwn) throws DatabaseUnitException, SQLException {
		Connection connection = bCreateOwn ? getOwnConnection() : getConnection();
		return new MyDataBaseConnection(connection, schema);
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("ds=").append(dataSource);
		result.append(", user=").append(username);
		result.append("]");
		return result.toString();
	}
}
