package org.nuclos.server.dblayer.impl.h2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.dbunit.dataset.datatype.IDataTypeFactory;
import org.dbunit.ext.h2.H2DataTypeFactory;
import org.nuclos.common.EntityMeta;
import org.nuclos.server.database.NuclosBasicDatasource;
import org.nuclos.server.dblayer.DbType;
import org.nuclos.server.dblayer.IBatch;
import org.nuclos.server.dblayer.IPreparedStringExecutor;
import org.nuclos.server.dblayer.impl.BatchImpl;
import org.nuclos.server.dblayer.impl.SQLUtils2;
import org.nuclos.server.dblayer.impl.StandardPreparedStringExecutor;
import org.nuclos.server.dblayer.impl.postgresql.PostgreSQLDBAccess;
import org.nuclos.server.dblayer.impl.standard.StandardSqlDBAccess;
import org.nuclos.server.dblayer.impl.util.PreparedString;
import org.nuclos.server.dblayer.incubator.DbExecutor;
import org.nuclos.server.dblayer.incubator.SQLInfo;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.structure.DbColumn;
import org.nuclos.server.dblayer.structure.DbColumnType;
import org.nuclos.server.dblayer.structure.DbColumnType.DbGenericType;
import org.nuclos.server.dblayer.structure.DbNullable;
import org.nuclos.server.dblayer.structure.DbSequence;

public class H2DBAccess extends PostgreSQLDBAccess {
	private static final Logger LOG = Logger.getLogger(H2DBAccess.class);
	private static final int DEFAULT_CACHE_SIZE = 128;
	private static final String CACHE = "cache";
	
	@Override
	public void init(DbType type, DataSource dataSource, Map<String, String> config) {
		if (dataSource instanceof NuclosBasicDatasource) {
	
			NuclosBasicDatasource nbds = (NuclosBasicDatasource) dataSource;			
			String zipFilename = nbds.getDBFilePath() + ".zip";
			
			String schema = config.get(SCHEMA);
			String user = nbds.getUsername();
			String pass = nbds.getPassword();
			String url = nbds.getUrl();
			try {
				
				Class.forName(nbds.getDriverClassName());
				Connection conn = DriverManager.getConnection(url);
				
				if (hasSchema(schema, conn)) {
					LOG.info("H2 DATABASE FOUND.");												
				} else {
					
					String sql = "RUNSCRIPT FROM '" + zipFilename + "' COMPRESSION ZIP";
					boolean restored = false;
					try {
						Statement st0 = conn.createStatement();
						st0.executeUpdate(sql);
						if (hasSchema(schema, conn)) {
							restored = true;
							LOG.info("H2 DATABASE RESTORED SUCCESSFULL!");							
						}
					} catch (Exception e) {
						LOG.warn(e.getMessage());
					}
					
					if (!restored) {
						int iCacheSize = DEFAULT_CACHE_SIZE;
						if (config.containsKey(CACHE)) try {
							iCacheSize = Integer.parseInt(config.get(CACHE));
						} catch (NumberFormatException nfe) {
							LOG.warn(nfe.getMessage(), nfe);
						}
						Statement st1 = conn.createStatement();
						st1.execute("CREATE USER IF NOT EXISTS " + user + " PASSWORD '" + pass + "'");
						Statement st2 = conn.createStatement();
						st2.execute("ALTER USER " + user + " ADMIN TRUE");
						Statement st3 = conn.createStatement();
						st3.execute("CREATE SCHEMA IF NOT EXISTS " + schema + " AUTHORIZATION " + user );
						Statement st4 = conn.createStatement();
						st4.execute("SET CACHE_SIZE " + iCacheSize * 1024);
						
						LOG.info("H2 DATABASE SETUP SUCCESSFULL!");
					}
				}
			} catch (Exception sql) {
				LOG.fatal(sql.getMessage(), sql);
			}
			super.init(type, dataSource, config);
			this.executor = new H2Executor(dataSource, config.get(USERNAME), config.get(PASSWORD), zipFilename);
		} 
	}
	
	private static boolean hasSchema(String schema, Connection conn) throws SQLException {
		String sqlSchema = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE UPPER(SCHEMA_NAME) = ?";
		PreparedStatement pst0 = conn.prepareStatement(sqlSchema);
		pst0.setString(1, schema.toUpperCase());
		ResultSet rs = pst0.executeQuery();
		return rs.next();
	}

    @Override
    protected IBatch getSqlForDropColumn(DbColumn column) {
        return BatchImpl.simpleBatch(PreparedString.format("ALTER TABLE %s DROP COLUMN %s",
            getQualifiedName(column.getTable().getName()),
            column.getColumnName()));
    }
    
    private String getAlterColumnSpec(DbColumn column, boolean bNotNull) {
		return String.format("ALTER COLUMN %s %s %s NULL",
				column.getColumnName(),
				getDataType(column.getColumnType()),
				bNotNull ? "NOT" : "");    	
    }

	@Override
	protected IBatch getSqlForRenameColumn(DbColumn column1, DbColumn column2) throws SQLException {
		final PreparedString ps = PreparedString.format("ALTER TABLE %s ALTER COLUMN %s RENAME TO %s",
				getQualifiedName(column2.getTable().getName()),
				column1.getColumnName(), column2.getColumnName());
		return BatchImpl.simpleBatch(ps);
	}

	@Override
	protected IBatch getSqlForAlterTableNotNullColumn(DbColumn column) {
		PreparedString str = PreparedString.format("ALTER TABLE %s %s",
			getQualifiedName(column.getTable().getName()),
			getAlterColumnSpec(column, true));
		
    	return BatchImpl.simpleBatch(str);
    }

	@Override
	protected String getColumnSpecForAlterTableColumn(DbColumn column, DbColumn oldColumn) {
		if(!getDataType(column.getColumnType()).equals(getDataType(oldColumn.getColumnType()))) {
			return getAlterColumnSpec(column, column.getNullable() == DbNullable.NOT_NULL);
			
		} else {			
			if (column.getNullable() != oldColumn.getNullable()) {
				return String.format("ALTER COLUMN %s SET %s NULL",
					column.getColumnName(),
					column.getNullable() == DbNullable.NOT_NULL ? "NOT" : "");
				
			} else {
				return null;
				
			}			
		}
	}

	@Override
    protected IBatch getSqlForCreateSequence(DbSequence sequence) {
        return BatchImpl.simpleBatch(PreparedString.format(
            "CREATE SEQUENCE %s INCREMENT BY 1 START WITH %d",
            getQualifiedName(sequence.getSequenceName()), sequence.getStartWith()));
    }
	
	@Override
    public String getSqlForCastAsString(String x, DbColumnType fromType) {
    	if (fromType.getGenericType() == DbGenericType.DATE
				|| fromType.getGenericType() == DbGenericType.DATETIME) {

    		final String pattern = getDateFormat().toPattern();
    		return String.format("FORMATDATETIME(%s, '%s')", x, pattern);
    	}
		return getSqlForCast(x, new DbColumnType(DbGenericType.VARCHAR, 255));
	}
	
	@Override
    protected IPreparedStringExecutor getPreparedStringExecutor() {
    	return new StandardPreparedStringExecutor(executor);
    	
    }
	
	@Override
	protected Integer getDefaultFetchSize() {
		return null;
	}
	
	@Override
    protected boolean autoCommitToFalseBeforeSelect() {
    	return false;
    }
	
	@Override
	public void shutdown() {
		try {
			getDbExecutor().makePersistent();
			LOG.info("H2 DATABASE STORE SUCCESSFULL!");
			getDbExecutor().execute(new DbExecutor.ConnectionRunner<Void>() {
				@Override
				public Void perform(Connection conn) throws SQLException {
					conn.createStatement().execute("SHUTDOWN");
					return null;
				}
				@Override
				public SQLInfo getInfo() {
					return new SQLInfo("SHUTDOWN");
				}
			});
			LOG.info("H2 DATABASE SHUTDOWN!");
		} catch (Exception sql) {
			LOG.fatal(sql.getMessage(), sql);			
		}
	}
	
	@Override
	public DbQueryBuilder getQueryBuilder() {
		return new H2SQLQueryBuilder(this);
	}

	@Override
	protected IDataTypeFactory getDataTypeFactory() {
		return new H2DataTypeFactory();
	}

	@Override
	public <PK> Long estimateCount(EntityMeta<PK> eMeta, DbQuery<PK> query) {		
		return null;
	}

	@SuppressWarnings("serial")
	static class H2SQLQueryBuilder extends PostgreSQLQueryBuilder {
		
		public H2SQLQueryBuilder(StandardSqlDBAccess dbAccess) {
			super(dbAccess);
		}

		@Override
		public DbExpression<String> convertDateToString(DbExpression<java.util.Date> x, String pattern) {
			return buildExpressionSql(String.class, "FORMATDATETIME(", x, ", '", SQLUtils2.escape(pattern), "')");
		}
	}

}
