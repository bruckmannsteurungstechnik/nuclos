package org.nuclos.server.dblayer.impl.oracle;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.server.dblayer.impl.standard.StandardSqlDBAccess;
import org.nuclos.server.dblayer.impl.standard.StandardSqlDBAccess.StandardQueryBuilder;
import org.nuclos.server.dblayer.impl.util.PreparedStringBuilder;
import org.nuclos.server.dblayer.query.DbColumnExpression;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbOrder;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbSelection;
import org.nuclos.server.dblayer.query.IDbCompoundColumnExpression;
import org.nuclos.server.dblayer.query.IDbReferencedCompoundColumnExpression;

@SuppressWarnings("serial")
public abstract class AbstractOracleQueryBuilder extends StandardQueryBuilder {
	
	private static Logger LOG = Logger.getLogger(AbstractOracleQueryBuilder.class);
	
	public AbstractOracleQueryBuilder(StandardSqlDBAccess dbAccess) {
		super(dbAccess);
	}

	@Override
	public abstract DbExpression<Date> currentDate();
	
	@Override
	public int getInLimit() {
		return 1000;
	}

	protected void prepareOrderByForOracle(PreparedStringBuilder ps, DbQuery<?> query, boolean bWithinRowNumber) {
		//NUCLOS-5202 No "Order By" if there is no sorting
		if (!bWithinRowNumber && query.getOrderList().isEmpty()) {
			return;
		}

        String sep = bWithinRowNumber ? "(ORDER BY " : " ORDER BY ";
		String tableAlias = query.getDbFrom().getAlias();
		String pkColumn = null;
		// If it is a standard query, get the information about the primary column directly from it
		if (query.getDbFrom().getEntity() != null) {
			pkColumn = query.getDbFrom().basePk().getColumn().getDbColumn();
		}

		boolean addPkToOrderList = false;

		for (DbSelection<?>  dbSelection: query.getSelections()) {
			if (dbSelection instanceof DbColumnExpression) {
				String column = ((DbColumnExpression<?>)dbSelection).getColumn().getDbColumn();
				if (RigidUtils.equal(column, pkColumn)) {
					addPkToOrderList = true;
					break;
				}

				// NUCLOS-6567 - If a query is built for VLPs, its dbFrom doesn't contain entity, neither a standard
				// primary key, so look for something that appears be a primary key.
				if (pkColumn == null && ("INTID".equalsIgnoreCase(column) || "STRUID".equalsIgnoreCase(column)
						|| "\"INTID\"".equalsIgnoreCase(column) || "\"STRUID\"".equalsIgnoreCase(column))) {
					pkColumn = column;
					addPkToOrderList = true;
					break;
				}
			}
		}

        for (DbOrder order : query.getOrderList()) {
        	DbExpression<?> expression = order.getExpression();
            boolean bString = expression.getJavaType() == String.class;
            
            String columnExpr = getPreparedString(expression).toString();
            boolean foundExpr = false;
            for (DbSelection<?> sel : query.getSelections()) {
            	if (sel.getSqlColumnExpr().endsWith(columnExpr)) {
            		foundExpr = true;
            		if (!sel.getSqlColumnExpr().equals(columnExpr)) {
            			// NOAINT-465: use origin column expression, necessary for calc attributes.
                		columnExpr = sel.getSqlColumnExpr().substring(0, 
                				sel.getSqlColumnExpr().length()-columnExpr.length()-1);
            		}
            		break;
            	}
            }
            
            if (!foundExpr && LOG.isDebugEnabled()) {
            	LOG.debug("Column selection for \"" + columnExpr + "\" does not exist, using default.");
            }
            
            ps.append(sep);
            if (bString) {
            	ps.append("LOWER(");
            }    
            ps.append(columnExpr);
            if (bString) {
            	ps.append(")");
            }    
            ps.append(order.isAscending() ? " ASC" : " DESC");
            sep = ", ";
            
            //NUCLOS-4968 - Skip additionalPrimary Key Sorting if there is an "addPkToOrderList" at the end.
            if (addPkToOrderList && expression instanceof DbColumnExpression && order.isAscending()) {
            	DbColumnExpression<?> colExpression = (DbColumnExpression<?>)expression;
            	if (RigidUtils.equal(colExpression.getTableAlias(), tableAlias)
            			&& RigidUtils.equal(colExpression.getColumn().getDbColumn(), pkColumn)) {
                	addPkToOrderList = false;                		
            	}
            }

        }    
        
        // add order by PK if PK is in select
        if (addPkToOrderList) {
        	ps.append(sep);
        	ps.append(tableAlias);
        	ps.append(".");
        	ps.append(pkColumn);
        	ps.append(" ASC");
        }
    }

	@Override
	protected void postprocessSelect(PreparedStringBuilder ps, DbQuery<?> query) {
		if (!query.hasOffset()) {
			super.postprocessSelect(ps, query);
		} else {
			ps = ps.append(", ROW_NUMBER() OVER ");
			prepareOrderByForOracle(ps, query, true);
			ps = ps.append(") AS RN");
		}
	}
	
	@Override
	protected void prepareOrderBy(PreparedStringBuilder ps, DbQuery<?> query) {
		if (!query.hasOffset()) {
			prepareOrderByForOracle(ps, query, false);
		}
	}

	@Override
	protected PreparedStringBuilder buildPreparedString(DbQuery<?> query) {
		PreparedStringBuilder ps = super.buildPreparedString(query);
		if (query.hasOffset()) {
			ps.append(") " + SystemFields.BASE_ALIAS + " WHERE");
			ps.appendf(" RN > %d", query.getOffset());
			long offset = query.getOffset();
			
			if (query.hasLimit()) {
				ps.appendf(" AND RN <= %d", offset + query.getLimit());
			}
		}
		return ps;
	}

	  protected final void prepareOutCastColumns(PreparedStringBuilder ps, DbQuery<?> query) {
		  if (!query.hasOffset()) {
		      return;
		  }
		  List<String> columns = translateSqlSelections(query.getSelections());
		  boolean first = true;
		  for (String col : columns) {
		      if (!first) {
		      	ps.append(", ");
		      }
		      first = false;
		      ps.append(col);
		  }
		  ps.append(" ");
	  }

	@Override
	protected final void prepareSelect(PreparedStringBuilder ps, DbQuery<?> query) {
		if (!query.hasOffset()) {
			super.prepareSelect(ps, query);
			
		} else {
			ps.append("SELECT ");
			if (query.isDistinct()) {
				ps.append("DISTINCT ");				
			}
			prepareOutCastColumns(ps, query);
			ps.append("FROM (SELECT ");
		}
	}
	
	
	  @Override
	  protected final List<? extends DbSelection<?>> getSqlSelections(DbQuery<?> query) {
	    List<? extends DbSelection<?>> selections = query.getSelections();
	    if (!query.hasOffset()) {
	    	return selections;
	    }
	    if (query.doSortCalculatedAttributes()) {
	    	return selections;
	    }
	    return filterSqlSelections(selections, false);
	  }

		protected final static List<? extends DbSelection<?>> filterSqlSelections(List<? extends DbSelection<?>> sel, boolean bFunction) {
	     	List<DbSelection<?>> filtered = new ArrayList<DbSelection<?>>();
	        for (Iterator<? extends DbSelection<?>> it = sel.iterator(); it.hasNext();) {
	        final DbSelection<?> s = it.next();
	            if (bFunction == isFunction(s)) {
	            	filtered.add(s);
	            }
	        }
	        return filtered;
	    }
		
		protected final static List<String> translateSqlSelections(List<? extends DbSelection<?>> sel) {
	        List<String> columns = new ArrayList<String>();
		    for (Iterator<? extends DbSelection<?>> it = sel.iterator(); it.hasNext();) {
	            final DbSelection<?> s = it.next();
	            if (isFunction(s)) {
	                columns.add(s.getSqlColumnExpr());
	                continue;
	            }
	            
	            String col = s.getAlias();
	            if (col == null) {
	                if (s instanceof DbColumnExpression) {
	                    DbColumnExpression<?> dbe = (DbColumnExpression<?>) s;
	                    col = dbe.getColumn().getDbColumn();
		            }
	            }
	            
	            if (col == null) {
	            	LOG.error("FATAL: NO NAME FOR COLUMN: " + s);
	                continue;
	            }
	            
	            columns.add(SystemFields.BASE_ALIAS + "." + col);
	        }
	        return columns;
	    }
		
		protected static final boolean isFunction(DbSelection<?> s) {
	        if (s instanceof DbColumnExpression) {
	        	return false;
	        }
	        if (s instanceof IDbCompoundColumnExpression) {
	        	return false;
	        }
	        if (s instanceof IDbReferencedCompoundColumnExpression) {
	        	return false;
	        }
	        return true;
	    }

}
