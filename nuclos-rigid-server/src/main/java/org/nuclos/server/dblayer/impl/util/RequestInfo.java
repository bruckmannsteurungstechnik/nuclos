package org.nuclos.server.dblayer.impl.util;

/**
 * Created by Oliver Brausch on 18.10.17.
 */
public class RequestInfo {
	private int countSQL;
	private int countInsertUpdateDelete = 0;
	private long timeSQL;
	private final long startTime;

	public RequestInfo(boolean sqlDebugger, boolean sqlTimer) {
		this.startTime = System.currentTimeMillis();

		// TODO: Confusing, "magic numbers" anti-pattern. Might be better to split into separate classes.
		this.countSQL = sqlDebugger || sqlTimer ? 0 : -1;
		this.timeSQL = sqlTimer ? 0L : -1L;
	}

	public void countSQL(long executionTimeInMs, boolean insertUpdateDelete) {
		if (countSQL >= 0) {
			countSQL++;
			if (insertUpdateDelete) {
				countInsertUpdateDelete++;
			}
		}
		if (timeSQL >= 0) {
			this.timeSQL += executionTimeInMs;
		}
	}

	public int getCountSQL() {
		return countSQL;
	}

	public int getCountInsertUpdateDelete() {
		return countInsertUpdateDelete;
	}

	public long getTimeSQL() {
		return timeSQL;
	}

	public long getTime() {
		return System.currentTimeMillis() - startTime;
	}

	@Override
	public String toString() {
		return "RequestInfo CountSQL: " + countSQL + " TimeSQL: " + timeSQL + " Time: " + getTime();
	}
}
