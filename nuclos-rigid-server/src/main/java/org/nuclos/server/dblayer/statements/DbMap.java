//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dblayer.statements;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Set;

import org.nuclos.common.DbField;
import org.nuclos.server.dblayer.expression.DbNull;
import org.nuclos.server.dblayer.expression.DbSpecialValue;
import org.nuclos.server.dblayer.structure.DbColumn;

public class DbMap {

	private final LinkedHashMap<DbField<?>, Object> mp;
	
	public DbMap(DbMap cmap) {
		this(cmap.size());
		this.mp.putAll(cmap.mp);
	}
	
	public DbMap() {
		this(10);
	}
	
	public DbMap(int size) {
		this(new LinkedHashMap<DbField<?>, Object>(size));
	}
	
	DbMap(LinkedHashMap<DbField<?>, Object> map) {
		this.mp = map;
	}
	
	public <T> void putUnsafe(DbField<T> field, Object value) {
		this.mp.put(field, value);
	}
	
	public <T> void put(DbField<T> field, DbSpecialValue<T> value) {
		this.mp.put(field, value);
	}

	public <T> void putField(DbField<T> field, DbField<T> value) {
		this.mp.put(field, value);
	}
	
	public <T> void put(DbField<T> field, T value) {
		this.mp.put(field, value);
	}
	
	public void putNull(DbField<?> field) {
		if (field instanceof DbColumn) {
			this.mp.put(field, DbNull.forType(((DbColumn) field).getColumnType()));
			return;
		}
		if (field.getJavaClass() != null) {
			this.mp.put(field, DbNull.forType((field).getJavaClass()));
			return;
		}
		this.mp.put(field, DbNull.NULL);
	}
	
	@SuppressWarnings("unchecked")
	public <T> T get(DbField<T> field) {
		return (T) this.mp.get(field);
	}
	
	public Object getUnsafe(DbField field) {
		return this.mp.get(field);
	}
	
	public Set<DbField<?>> getFields() {
		return this.mp.keySet();
	}

	public int size() {
		return this.mp.size();
	}

	public Collection<Object> values() {
		return this.mp.values();
	}

	public Set<DbField<?>> keySet() {
		return this.mp.keySet();
	}

	public Set<Entry<DbField<?>, Object>> entrySet() {
		return this.mp.entrySet();
	}

	public boolean isEmpty() {
		return this.mp.isEmpty();
	}

	public void remove(DbField<?> field) {
		this.mp.remove(field);
	}

	public void clear() {
		this.mp.clear();
	}

	public int getColumnIndexSelect(DbField fieldToFind) {
		int retVal = -1;
		
		int idx = 0;
		for (DbField field : getFields()) {
			if (field.equals(fieldToFind)) {
				retVal = idx;
				break;
			}

			Object unsafe = getUnsafe(field);
			if (unsafe != null && !(unsafe instanceof DbNull) && unsafe instanceof DbField) {
				idx++;
			}
		}
		
		return retVal;
	}
	
	public Class getColumnTypeSelect(int i) {
		Class retVal = null;
		int idx = 0;
		for (DbField field : getFields()) {
			Object unsafe = getUnsafe(field);
			if (unsafe != null && !(unsafe instanceof DbNull) && unsafe instanceof DbField) {
				if (idx == i) {
					return field.getJavaClass();
				}
				idx++;
			}
		}
		return retVal;
	}

	public static <T> DbMap singelton(DbField<T> field, T value) {
		LinkedHashMap<DbField<?>, Object> map = new LinkedHashMap<>();
		map.put(field, value);
		return new DbMap(map);
	}
	
}
