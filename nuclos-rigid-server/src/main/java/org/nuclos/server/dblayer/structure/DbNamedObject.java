package org.nuclos.server.dblayer.structure;

import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;

public class DbNamedObject {
	
	private final UID uid;
	private final String name;
	
	public DbNamedObject(UID uid, String name) {
		this.uid = uid;
		this.name = name;
	}
	
	public UID getUID() {
		return uid;
	}
	
	public String getName() {
		return name;
	}
	
	public String getEqualsKey() {
		if (uid == null) {
			return getName();
		} else {
			return uid.getStringifiedDefinition();
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj == this)
			return true;
		DbNamedObject k = (DbNamedObject) obj;
		if (k.getUID() != null && uid != null) {
			return uid.equals(k.getUID());
		} else {
			return RigidUtils.equal(k.getName(), name);
		}
	}
	
	@Override
	public int hashCode() {
		return getName().hashCode();
	}
	
	@Override
	public String toString() {
		return getName();
	}
	
}
