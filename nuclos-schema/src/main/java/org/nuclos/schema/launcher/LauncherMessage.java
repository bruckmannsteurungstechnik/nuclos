
package org.nuclos.schema.launcher;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Base type for Launcher messages.
 * 
 * <p>Java class for launcher-message complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="launcher-message"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="client-id" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "launcher-message")
@XmlSeeAlso({
    LauncherLaunchMessage.class,
    LauncherPidMessage.class
})
public class LauncherMessage implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "client-id", required = true)
    protected BigInteger clientId;

    /**
     * Gets the value of the clientId property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getClientId() {
        return clientId;
    }

    /**
     * Sets the value of the clientId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setClientId(BigInteger value) {
        this.clientId = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            BigInteger theClientId;
            theClientId = this.getClientId();
            strategy.appendField(locator, this, "clientId", buffer, theClientId);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof LauncherMessage)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final LauncherMessage that = ((LauncherMessage) object);
        {
            BigInteger lhsClientId;
            lhsClientId = this.getClientId();
            BigInteger rhsClientId;
            rhsClientId = that.getClientId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "clientId", lhsClientId), LocatorUtils.property(thatLocator, "clientId", rhsClientId), lhsClientId, rhsClientId)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            BigInteger theClientId;
            theClientId = this.getClientId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "clientId", theClientId), currentHashCode, theClientId);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof LauncherMessage) {
            final LauncherMessage copy = ((LauncherMessage) draftCopy);
            if (this.clientId!= null) {
                BigInteger sourceClientId;
                sourceClientId = this.getClientId();
                BigInteger copyClientId = ((BigInteger) strategy.copy(LocatorUtils.property(locator, "clientId", sourceClientId), sourceClientId));
                copy.setClientId(copyClientId);
            } else {
                copy.clientId = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new LauncherMessage();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final LauncherMessage.Builder<_B> _other) {
        _other.clientId = this.clientId;
    }

    public<_B >LauncherMessage.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new LauncherMessage.Builder<_B>(_parentBuilder, this, true);
    }

    public LauncherMessage.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static LauncherMessage.Builder<Void> builder() {
        return new LauncherMessage.Builder<Void>(null, null, false);
    }

    public static<_B >LauncherMessage.Builder<_B> copyOf(final LauncherMessage _other) {
        final LauncherMessage.Builder<_B> _newBuilder = new LauncherMessage.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final LauncherMessage.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree clientIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("clientId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(clientIdPropertyTree!= null):((clientIdPropertyTree == null)||(!clientIdPropertyTree.isLeaf())))) {
            _other.clientId = this.clientId;
        }
    }

    public<_B >LauncherMessage.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new LauncherMessage.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public LauncherMessage.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >LauncherMessage.Builder<_B> copyOf(final LauncherMessage _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final LauncherMessage.Builder<_B> _newBuilder = new LauncherMessage.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static LauncherMessage.Builder<Void> copyExcept(final LauncherMessage _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static LauncherMessage.Builder<Void> copyOnly(final LauncherMessage _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final LauncherMessage _storedValue;
        private BigInteger clientId;

        public Builder(final _B _parentBuilder, final LauncherMessage _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.clientId = _other.clientId;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final LauncherMessage _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree clientIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("clientId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(clientIdPropertyTree!= null):((clientIdPropertyTree == null)||(!clientIdPropertyTree.isLeaf())))) {
                        this.clientId = _other.clientId;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends LauncherMessage >_P init(final _P _product) {
            _product.clientId = this.clientId;
            return _product;
        }

        /**
         * Sets the new value of "clientId" (any previous value will be replaced)
         * 
         * @param clientId
         *     New value of the "clientId" property.
         */
        public LauncherMessage.Builder<_B> withClientId(final BigInteger clientId) {
            this.clientId = clientId;
            return this;
        }

        @Override
        public LauncherMessage build() {
            if (_storedValue == null) {
                return this.init(new LauncherMessage());
            } else {
                return ((LauncherMessage) _storedValue);
            }
        }

        public LauncherMessage.Builder<_B> copyOf(final LauncherMessage _other) {
            _other.copyTo(this);
            return this;
        }

        public LauncherMessage.Builder<_B> copyOf(final LauncherMessage.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends LauncherMessage.Selector<LauncherMessage.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static LauncherMessage.Select _root() {
            return new LauncherMessage.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, LauncherMessage.Selector<TRoot, TParent>> clientId = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.clientId!= null) {
                products.put("clientId", this.clientId.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, LauncherMessage.Selector<TRoot, TParent>> clientId() {
            return ((this.clientId == null)?this.clientId = new com.kscs.util.jaxb.Selector<TRoot, LauncherMessage.Selector<TRoot, TParent>>(this._root, this, "clientId"):this.clientId);
        }

    }

}
