
package org.nuclos.schema.launcher;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for launcher-pid-message complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="launcher-pid-message"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:org.nuclos.schema.launcher}launcher-message"&gt;
 *       &lt;attribute name="pid" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "launcher-pid-message")
public class LauncherPidMessage
    extends LauncherMessage
    implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "pid")
    protected BigInteger pid;

    /**
     * Gets the value of the pid property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPid() {
        return pid;
    }

    /**
     * Sets the value of the pid property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPid(BigInteger value) {
        this.pid = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        super.appendFields(locator, buffer, strategy);
        {
            BigInteger thePid;
            thePid = this.getPid();
            strategy.appendField(locator, this, "pid", buffer, thePid);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof LauncherPidMessage)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final LauncherPidMessage that = ((LauncherPidMessage) object);
        {
            BigInteger lhsPid;
            lhsPid = this.getPid();
            BigInteger rhsPid;
            rhsPid = that.getPid();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "pid", lhsPid), LocatorUtils.property(thatLocator, "pid", rhsPid), lhsPid, rhsPid)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = super.hashCode(locator, strategy);
        {
            BigInteger thePid;
            thePid = this.getPid();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "pid", thePid), currentHashCode, thePid);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof LauncherPidMessage) {
            final LauncherPidMessage copy = ((LauncherPidMessage) draftCopy);
            if (this.pid!= null) {
                BigInteger sourcePid;
                sourcePid = this.getPid();
                BigInteger copyPid = ((BigInteger) strategy.copy(LocatorUtils.property(locator, "pid", sourcePid), sourcePid));
                copy.setPid(copyPid);
            } else {
                copy.pid = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new LauncherPidMessage();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final LauncherPidMessage.Builder<_B> _other) {
        super.copyTo(_other);
        _other.pid = this.pid;
    }

    @Override
    public<_B >LauncherPidMessage.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new LauncherPidMessage.Builder<_B>(_parentBuilder, this, true);
    }

    @Override
    public LauncherPidMessage.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static LauncherPidMessage.Builder<Void> builder() {
        return new LauncherPidMessage.Builder<Void>(null, null, false);
    }

    public static<_B >LauncherPidMessage.Builder<_B> copyOf(final LauncherMessage _other) {
        final LauncherPidMessage.Builder<_B> _newBuilder = new LauncherPidMessage.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >LauncherPidMessage.Builder<_B> copyOf(final LauncherPidMessage _other) {
        final LauncherPidMessage.Builder<_B> _newBuilder = new LauncherPidMessage.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final LauncherPidMessage.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        super.copyTo(_other, _propertyTree, _propertyTreeUse);
        final PropertyTree pidPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("pid"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(pidPropertyTree!= null):((pidPropertyTree == null)||(!pidPropertyTree.isLeaf())))) {
            _other.pid = this.pid;
        }
    }

    @Override
    public<_B >LauncherPidMessage.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new LauncherPidMessage.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    @Override
    public LauncherPidMessage.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >LauncherPidMessage.Builder<_B> copyOf(final LauncherMessage _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final LauncherPidMessage.Builder<_B> _newBuilder = new LauncherPidMessage.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >LauncherPidMessage.Builder<_B> copyOf(final LauncherPidMessage _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final LauncherPidMessage.Builder<_B> _newBuilder = new LauncherPidMessage.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static LauncherPidMessage.Builder<Void> copyExcept(final LauncherMessage _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static LauncherPidMessage.Builder<Void> copyExcept(final LauncherPidMessage _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static LauncherPidMessage.Builder<Void> copyOnly(final LauncherMessage _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static LauncherPidMessage.Builder<Void> copyOnly(final LauncherPidMessage _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >
        extends LauncherMessage.Builder<_B>
        implements Buildable
    {

        private BigInteger pid;

        public Builder(final _B _parentBuilder, final LauncherPidMessage _other, final boolean _copy) {
            super(_parentBuilder, _other, _copy);
            if (_other!= null) {
                this.pid = _other.pid;
            }
        }

        public Builder(final _B _parentBuilder, final LauncherPidMessage _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            super(_parentBuilder, _other, _copy, _propertyTree, _propertyTreeUse);
            if (_other!= null) {
                final PropertyTree pidPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("pid"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(pidPropertyTree!= null):((pidPropertyTree == null)||(!pidPropertyTree.isLeaf())))) {
                    this.pid = _other.pid;
                }
            }
        }

        protected<_P extends LauncherPidMessage >_P init(final _P _product) {
            _product.pid = this.pid;
            return super.init(_product);
        }

        /**
         * Sets the new value of "pid" (any previous value will be replaced)
         * 
         * @param pid
         *     New value of the "pid" property.
         */
        public LauncherPidMessage.Builder<_B> withPid(final BigInteger pid) {
            this.pid = pid;
            return this;
        }

        /**
         * Sets the new value of "clientId" (any previous value will be replaced)
         * 
         * @param clientId
         *     New value of the "clientId" property.
         */
        @Override
        public LauncherPidMessage.Builder<_B> withClientId(final BigInteger clientId) {
            super.withClientId(clientId);
            return this;
        }

        @Override
        public LauncherPidMessage build() {
            if (_storedValue == null) {
                return this.init(new LauncherPidMessage());
            } else {
                return ((LauncherPidMessage) _storedValue);
            }
        }

        public LauncherPidMessage.Builder<_B> copyOf(final LauncherPidMessage _other) {
            _other.copyTo(this);
            return this;
        }

        public LauncherPidMessage.Builder<_B> copyOf(final LauncherPidMessage.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends LauncherPidMessage.Selector<LauncherPidMessage.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static LauncherPidMessage.Select _root() {
            return new LauncherPidMessage.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends LauncherMessage.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, LauncherPidMessage.Selector<TRoot, TParent>> pid = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.pid!= null) {
                products.put("pid", this.pid.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, LauncherPidMessage.Selector<TRoot, TParent>> pid() {
            return ((this.pid == null)?this.pid = new com.kscs.util.jaxb.Selector<TRoot, LauncherPidMessage.Selector<TRoot, TParent>>(this._root, this, "pid"):this.pid);
        }

    }

}
