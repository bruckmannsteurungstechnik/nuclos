
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="axis" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
 *             &lt;enumeration value="x"/&gt;
 *             &lt;enumeration value="y"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class Boxlayout implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "axis", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String axis;

    /**
     * Gets the value of the axis property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAxis() {
        return axis;
    }

    /**
     * Sets the value of the axis property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAxis(String value) {
        this.axis = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theAxis;
            theAxis = this.getAxis();
            strategy.appendField(locator, this, "axis", buffer, theAxis);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Boxlayout)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Boxlayout that = ((Boxlayout) object);
        {
            String lhsAxis;
            lhsAxis = this.getAxis();
            String rhsAxis;
            rhsAxis = that.getAxis();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "axis", lhsAxis), LocatorUtils.property(thatLocator, "axis", rhsAxis), lhsAxis, rhsAxis)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theAxis;
            theAxis = this.getAxis();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "axis", theAxis), currentHashCode, theAxis);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Boxlayout) {
            final Boxlayout copy = ((Boxlayout) draftCopy);
            if (this.axis!= null) {
                String sourceAxis;
                sourceAxis = this.getAxis();
                String copyAxis = ((String) strategy.copy(LocatorUtils.property(locator, "axis", sourceAxis), sourceAxis));
                copy.setAxis(copyAxis);
            } else {
                copy.axis = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Boxlayout();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Boxlayout.Builder<_B> _other) {
        _other.axis = this.axis;
    }

    public<_B >Boxlayout.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Boxlayout.Builder<_B>(_parentBuilder, this, true);
    }

    public Boxlayout.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Boxlayout.Builder<Void> builder() {
        return new Boxlayout.Builder<Void>(null, null, false);
    }

    public static<_B >Boxlayout.Builder<_B> copyOf(final Boxlayout _other) {
        final Boxlayout.Builder<_B> _newBuilder = new Boxlayout.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Boxlayout.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree axisPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("axis"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(axisPropertyTree!= null):((axisPropertyTree == null)||(!axisPropertyTree.isLeaf())))) {
            _other.axis = this.axis;
        }
    }

    public<_B >Boxlayout.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Boxlayout.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Boxlayout.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Boxlayout.Builder<_B> copyOf(final Boxlayout _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Boxlayout.Builder<_B> _newBuilder = new Boxlayout.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Boxlayout.Builder<Void> copyExcept(final Boxlayout _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Boxlayout.Builder<Void> copyOnly(final Boxlayout _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Boxlayout _storedValue;
        private String axis;

        public Builder(final _B _parentBuilder, final Boxlayout _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.axis = _other.axis;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Boxlayout _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree axisPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("axis"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(axisPropertyTree!= null):((axisPropertyTree == null)||(!axisPropertyTree.isLeaf())))) {
                        this.axis = _other.axis;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Boxlayout >_P init(final _P _product) {
            _product.axis = this.axis;
            return _product;
        }

        /**
         * Sets the new value of "axis" (any previous value will be replaced)
         * 
         * @param axis
         *     New value of the "axis" property.
         */
        public Boxlayout.Builder<_B> withAxis(final String axis) {
            this.axis = axis;
            return this;
        }

        @Override
        public Boxlayout build() {
            if (_storedValue == null) {
                return this.init(new Boxlayout());
            } else {
                return ((Boxlayout) _storedValue);
            }
        }

        public Boxlayout.Builder<_B> copyOf(final Boxlayout _other) {
            _other.copyTo(this);
            return this;
        }

        public Boxlayout.Builder<_B> copyOf(final Boxlayout.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Boxlayout.Selector<Boxlayout.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Boxlayout.Select _root() {
            return new Boxlayout.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, Boxlayout.Selector<TRoot, TParent>> axis = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.axis!= null) {
                products.put("axis", this.axis.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, Boxlayout.Selector<TRoot, TParent>> axis() {
            return ((this.axis == null)?this.axis = new com.kscs.util.jaxb.Selector<TRoot, Boxlayout.Selector<TRoot, TParent>>(this._root, this, "axis"):this.axis);
        }

    }

}
