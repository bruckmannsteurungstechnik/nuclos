
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}layoutconstraints" minOccurs="0"/&gt;
 *         &lt;group ref="{}borders"/&gt;
 *         &lt;group ref="{}sizes"/&gt;
 *         &lt;element ref="{}font" minOccurs="0"/&gt;
 *         &lt;element ref="{}description" minOccurs="0"/&gt;
 *         &lt;element ref="{}property" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{}translations" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="actioncommand" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="actionkeystroke" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="label" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="tooltip" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="enabled" type="{}boolean" /&gt;
 *       &lt;attribute name="resourceId" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="icon" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="nextfocuscomponent" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="nextfocusfield" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="nextfocusonaction" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="disable-during-edit" type="{}boolean" /&gt;
 *       &lt;attribute name="save-after-rule-execution" type="{}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "layoutconstraints",
    "clearBorder",
    "border",
    "minimumSize",
    "preferredSize",
    "strictSize",
    "font",
    "description",
    "property",
    "translations"
})
@XmlRootElement(name = "button")
public class Button implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "layoutconstraints", type = JAXBElement.class, required = false)
    protected JAXBElement<?> layoutconstraints;
    @XmlElement(name = "clear-border")
    protected ClearBorder clearBorder;
    @XmlElementRef(name = "border", type = JAXBElement.class, required = false)
    protected List<JAXBElement<?>> border;
    @XmlElement(name = "minimum-size")
    protected MinimumSize minimumSize;
    @XmlElement(name = "preferred-size")
    protected PreferredSize preferredSize;
    @XmlElement(name = "strict-size")
    protected StrictSize strictSize;
    protected Font font;
    protected String description;
    protected List<Property> property;
    protected Translations translations;
    @XmlAttribute(name = "name")
    @XmlSchemaType(name = "anySimpleType")
    protected String name;
    @XmlAttribute(name = "actioncommand", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String actioncommand;
    @XmlAttribute(name = "actionkeystroke")
    @XmlSchemaType(name = "anySimpleType")
    protected String actionkeystroke;
    @XmlAttribute(name = "label")
    @XmlSchemaType(name = "anySimpleType")
    protected String label;
    @XmlAttribute(name = "tooltip")
    @XmlSchemaType(name = "anySimpleType")
    protected String tooltip;
    @XmlAttribute(name = "enabled")
    protected Boolean enabled;
    @XmlAttribute(name = "resourceId")
    @XmlSchemaType(name = "anySimpleType")
    protected String resourceId;
    @XmlAttribute(name = "icon")
    @XmlSchemaType(name = "anySimpleType")
    protected String icon;
    @XmlAttribute(name = "nextfocuscomponent")
    @XmlSchemaType(name = "anySimpleType")
    protected String nextfocuscomponent;
    @XmlAttribute(name = "nextfocusfield")
    @XmlSchemaType(name = "anySimpleType")
    protected String nextfocusfield;
    @XmlAttribute(name = "nextfocusonaction")
    @XmlSchemaType(name = "anySimpleType")
    protected String nextfocusonaction;
    @XmlAttribute(name = "disable-during-edit")
    protected Boolean disableDuringEdit;
    @XmlAttribute(name = "save-after-rule-execution")
    protected Boolean saveAfterRuleExecution;

    /**
     * Gets the value of the layoutconstraints property.
     * 
     * @return
     *     possible object is
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     *     
     */
    public JAXBElement<?> getLayoutconstraints() {
        return layoutconstraints;
    }

    /**
     * Sets the value of the layoutconstraints property.
     * 
     * @param value
     *     allowed object is
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     *     
     */
    public void setLayoutconstraints(JAXBElement<?> value) {
        this.layoutconstraints = value;
    }

    /**
     * Gets the value of the clearBorder property.
     * 
     * @return
     *     possible object is
     *     {@link ClearBorder }
     *     
     */
    public ClearBorder getClearBorder() {
        return clearBorder;
    }

    /**
     * Sets the value of the clearBorder property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClearBorder }
     *     
     */
    public void setClearBorder(ClearBorder value) {
        this.clearBorder = value;
    }

    /**
     * Gets the value of the border property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the border property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBorder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     * 
     * 
     */
    public List<JAXBElement<?>> getBorder() {
        if (border == null) {
            border = new ArrayList<JAXBElement<?>>();
        }
        return this.border;
    }

    /**
     * Gets the value of the minimumSize property.
     * 
     * @return
     *     possible object is
     *     {@link MinimumSize }
     *     
     */
    public MinimumSize getMinimumSize() {
        return minimumSize;
    }

    /**
     * Sets the value of the minimumSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link MinimumSize }
     *     
     */
    public void setMinimumSize(MinimumSize value) {
        this.minimumSize = value;
    }

    /**
     * Gets the value of the preferredSize property.
     * 
     * @return
     *     possible object is
     *     {@link PreferredSize }
     *     
     */
    public PreferredSize getPreferredSize() {
        return preferredSize;
    }

    /**
     * Sets the value of the preferredSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link PreferredSize }
     *     
     */
    public void setPreferredSize(PreferredSize value) {
        this.preferredSize = value;
    }

    /**
     * Gets the value of the strictSize property.
     * 
     * @return
     *     possible object is
     *     {@link StrictSize }
     *     
     */
    public StrictSize getStrictSize() {
        return strictSize;
    }

    /**
     * Sets the value of the strictSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link StrictSize }
     *     
     */
    public void setStrictSize(StrictSize value) {
        this.strictSize = value;
    }

    /**
     * Gets the value of the font property.
     * 
     * @return
     *     possible object is
     *     {@link Font }
     *     
     */
    public Font getFont() {
        return font;
    }

    /**
     * Sets the value of the font property.
     * 
     * @param value
     *     allowed object is
     *     {@link Font }
     *     
     */
    public void setFont(Font value) {
        this.font = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the property property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the property property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProperty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Property }
     * 
     * 
     */
    public List<Property> getProperty() {
        if (property == null) {
            property = new ArrayList<Property>();
        }
        return this.property;
    }

    /**
     * Gets the value of the translations property.
     * 
     * @return
     *     possible object is
     *     {@link Translations }
     *     
     */
    public Translations getTranslations() {
        return translations;
    }

    /**
     * Sets the value of the translations property.
     * 
     * @param value
     *     allowed object is
     *     {@link Translations }
     *     
     */
    public void setTranslations(Translations value) {
        this.translations = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the actioncommand property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActioncommand() {
        return actioncommand;
    }

    /**
     * Sets the value of the actioncommand property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActioncommand(String value) {
        this.actioncommand = value;
    }

    /**
     * Gets the value of the actionkeystroke property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionkeystroke() {
        return actionkeystroke;
    }

    /**
     * Sets the value of the actionkeystroke property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionkeystroke(String value) {
        this.actionkeystroke = value;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the tooltip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTooltip() {
        return tooltip;
    }

    /**
     * Sets the value of the tooltip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTooltip(String value) {
        this.tooltip = value;
    }

    /**
     * Gets the value of the enabled property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getEnabled() {
        return enabled;
    }

    /**
     * Sets the value of the enabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnabled(Boolean value) {
        this.enabled = value;
    }

    /**
     * Gets the value of the resourceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceId() {
        return resourceId;
    }

    /**
     * Sets the value of the resourceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceId(String value) {
        this.resourceId = value;
    }

    /**
     * Gets the value of the icon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIcon() {
        return icon;
    }

    /**
     * Sets the value of the icon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIcon(String value) {
        this.icon = value;
    }

    /**
     * Gets the value of the nextfocuscomponent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextfocuscomponent() {
        return nextfocuscomponent;
    }

    /**
     * Sets the value of the nextfocuscomponent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextfocuscomponent(String value) {
        this.nextfocuscomponent = value;
    }

    /**
     * Gets the value of the nextfocusfield property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextfocusfield() {
        return nextfocusfield;
    }

    /**
     * Sets the value of the nextfocusfield property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextfocusfield(String value) {
        this.nextfocusfield = value;
    }

    /**
     * Gets the value of the nextfocusonaction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextfocusonaction() {
        return nextfocusonaction;
    }

    /**
     * Sets the value of the nextfocusonaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextfocusonaction(String value) {
        this.nextfocusonaction = value;
    }

    /**
     * Gets the value of the disableDuringEdit property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDisableDuringEdit() {
        return disableDuringEdit;
    }

    /**
     * Sets the value of the disableDuringEdit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisableDuringEdit(Boolean value) {
        this.disableDuringEdit = value;
    }

    /**
     * Gets the value of the saveAfterRuleExecution property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSaveAfterRuleExecution() {
        return saveAfterRuleExecution;
    }

    /**
     * Sets the value of the saveAfterRuleExecution property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSaveAfterRuleExecution(Boolean value) {
        this.saveAfterRuleExecution = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            JAXBElement<?> theLayoutconstraints;
            theLayoutconstraints = this.getLayoutconstraints();
            strategy.appendField(locator, this, "layoutconstraints", buffer, theLayoutconstraints);
        }
        {
            ClearBorder theClearBorder;
            theClearBorder = this.getClearBorder();
            strategy.appendField(locator, this, "clearBorder", buffer, theClearBorder);
        }
        {
            List<JAXBElement<?>> theBorder;
            theBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            strategy.appendField(locator, this, "border", buffer, theBorder);
        }
        {
            MinimumSize theMinimumSize;
            theMinimumSize = this.getMinimumSize();
            strategy.appendField(locator, this, "minimumSize", buffer, theMinimumSize);
        }
        {
            PreferredSize thePreferredSize;
            thePreferredSize = this.getPreferredSize();
            strategy.appendField(locator, this, "preferredSize", buffer, thePreferredSize);
        }
        {
            StrictSize theStrictSize;
            theStrictSize = this.getStrictSize();
            strategy.appendField(locator, this, "strictSize", buffer, theStrictSize);
        }
        {
            Font theFont;
            theFont = this.getFont();
            strategy.appendField(locator, this, "font", buffer, theFont);
        }
        {
            String theDescription;
            theDescription = this.getDescription();
            strategy.appendField(locator, this, "description", buffer, theDescription);
        }
        {
            List<Property> theProperty;
            theProperty = (((this.property!= null)&&(!this.property.isEmpty()))?this.getProperty():null);
            strategy.appendField(locator, this, "property", buffer, theProperty);
        }
        {
            Translations theTranslations;
            theTranslations = this.getTranslations();
            strategy.appendField(locator, this, "translations", buffer, theTranslations);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String theActioncommand;
            theActioncommand = this.getActioncommand();
            strategy.appendField(locator, this, "actioncommand", buffer, theActioncommand);
        }
        {
            String theActionkeystroke;
            theActionkeystroke = this.getActionkeystroke();
            strategy.appendField(locator, this, "actionkeystroke", buffer, theActionkeystroke);
        }
        {
            String theLabel;
            theLabel = this.getLabel();
            strategy.appendField(locator, this, "label", buffer, theLabel);
        }
        {
            String theTooltip;
            theTooltip = this.getTooltip();
            strategy.appendField(locator, this, "tooltip", buffer, theTooltip);
        }
        {
            Boolean theEnabled;
            theEnabled = this.getEnabled();
            strategy.appendField(locator, this, "enabled", buffer, theEnabled);
        }
        {
            String theResourceId;
            theResourceId = this.getResourceId();
            strategy.appendField(locator, this, "resourceId", buffer, theResourceId);
        }
        {
            String theIcon;
            theIcon = this.getIcon();
            strategy.appendField(locator, this, "icon", buffer, theIcon);
        }
        {
            String theNextfocuscomponent;
            theNextfocuscomponent = this.getNextfocuscomponent();
            strategy.appendField(locator, this, "nextfocuscomponent", buffer, theNextfocuscomponent);
        }
        {
            String theNextfocusfield;
            theNextfocusfield = this.getNextfocusfield();
            strategy.appendField(locator, this, "nextfocusfield", buffer, theNextfocusfield);
        }
        {
            String theNextfocusonaction;
            theNextfocusonaction = this.getNextfocusonaction();
            strategy.appendField(locator, this, "nextfocusonaction", buffer, theNextfocusonaction);
        }
        {
            Boolean theDisableDuringEdit;
            theDisableDuringEdit = this.getDisableDuringEdit();
            strategy.appendField(locator, this, "disableDuringEdit", buffer, theDisableDuringEdit);
        }
        {
            Boolean theSaveAfterRuleExecution;
            theSaveAfterRuleExecution = this.getSaveAfterRuleExecution();
            strategy.appendField(locator, this, "saveAfterRuleExecution", buffer, theSaveAfterRuleExecution);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Button)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Button that = ((Button) object);
        {
            JAXBElement<?> lhsLayoutconstraints;
            lhsLayoutconstraints = this.getLayoutconstraints();
            JAXBElement<?> rhsLayoutconstraints;
            rhsLayoutconstraints = that.getLayoutconstraints();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "layoutconstraints", lhsLayoutconstraints), LocatorUtils.property(thatLocator, "layoutconstraints", rhsLayoutconstraints), lhsLayoutconstraints, rhsLayoutconstraints)) {
                return false;
            }
        }
        {
            ClearBorder lhsClearBorder;
            lhsClearBorder = this.getClearBorder();
            ClearBorder rhsClearBorder;
            rhsClearBorder = that.getClearBorder();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "clearBorder", lhsClearBorder), LocatorUtils.property(thatLocator, "clearBorder", rhsClearBorder), lhsClearBorder, rhsClearBorder)) {
                return false;
            }
        }
        {
            List<JAXBElement<?>> lhsBorder;
            lhsBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            List<JAXBElement<?>> rhsBorder;
            rhsBorder = (((that.border!= null)&&(!that.border.isEmpty()))?that.getBorder():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "border", lhsBorder), LocatorUtils.property(thatLocator, "border", rhsBorder), lhsBorder, rhsBorder)) {
                return false;
            }
        }
        {
            MinimumSize lhsMinimumSize;
            lhsMinimumSize = this.getMinimumSize();
            MinimumSize rhsMinimumSize;
            rhsMinimumSize = that.getMinimumSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "minimumSize", lhsMinimumSize), LocatorUtils.property(thatLocator, "minimumSize", rhsMinimumSize), lhsMinimumSize, rhsMinimumSize)) {
                return false;
            }
        }
        {
            PreferredSize lhsPreferredSize;
            lhsPreferredSize = this.getPreferredSize();
            PreferredSize rhsPreferredSize;
            rhsPreferredSize = that.getPreferredSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "preferredSize", lhsPreferredSize), LocatorUtils.property(thatLocator, "preferredSize", rhsPreferredSize), lhsPreferredSize, rhsPreferredSize)) {
                return false;
            }
        }
        {
            StrictSize lhsStrictSize;
            lhsStrictSize = this.getStrictSize();
            StrictSize rhsStrictSize;
            rhsStrictSize = that.getStrictSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "strictSize", lhsStrictSize), LocatorUtils.property(thatLocator, "strictSize", rhsStrictSize), lhsStrictSize, rhsStrictSize)) {
                return false;
            }
        }
        {
            Font lhsFont;
            lhsFont = this.getFont();
            Font rhsFont;
            rhsFont = that.getFont();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "font", lhsFont), LocatorUtils.property(thatLocator, "font", rhsFont), lhsFont, rhsFont)) {
                return false;
            }
        }
        {
            String lhsDescription;
            lhsDescription = this.getDescription();
            String rhsDescription;
            rhsDescription = that.getDescription();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "description", lhsDescription), LocatorUtils.property(thatLocator, "description", rhsDescription), lhsDescription, rhsDescription)) {
                return false;
            }
        }
        {
            List<Property> lhsProperty;
            lhsProperty = (((this.property!= null)&&(!this.property.isEmpty()))?this.getProperty():null);
            List<Property> rhsProperty;
            rhsProperty = (((that.property!= null)&&(!that.property.isEmpty()))?that.getProperty():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "property", lhsProperty), LocatorUtils.property(thatLocator, "property", rhsProperty), lhsProperty, rhsProperty)) {
                return false;
            }
        }
        {
            Translations lhsTranslations;
            lhsTranslations = this.getTranslations();
            Translations rhsTranslations;
            rhsTranslations = that.getTranslations();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "translations", lhsTranslations), LocatorUtils.property(thatLocator, "translations", rhsTranslations), lhsTranslations, rhsTranslations)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsActioncommand;
            lhsActioncommand = this.getActioncommand();
            String rhsActioncommand;
            rhsActioncommand = that.getActioncommand();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "actioncommand", lhsActioncommand), LocatorUtils.property(thatLocator, "actioncommand", rhsActioncommand), lhsActioncommand, rhsActioncommand)) {
                return false;
            }
        }
        {
            String lhsActionkeystroke;
            lhsActionkeystroke = this.getActionkeystroke();
            String rhsActionkeystroke;
            rhsActionkeystroke = that.getActionkeystroke();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "actionkeystroke", lhsActionkeystroke), LocatorUtils.property(thatLocator, "actionkeystroke", rhsActionkeystroke), lhsActionkeystroke, rhsActionkeystroke)) {
                return false;
            }
        }
        {
            String lhsLabel;
            lhsLabel = this.getLabel();
            String rhsLabel;
            rhsLabel = that.getLabel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "label", lhsLabel), LocatorUtils.property(thatLocator, "label", rhsLabel), lhsLabel, rhsLabel)) {
                return false;
            }
        }
        {
            String lhsTooltip;
            lhsTooltip = this.getTooltip();
            String rhsTooltip;
            rhsTooltip = that.getTooltip();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "tooltip", lhsTooltip), LocatorUtils.property(thatLocator, "tooltip", rhsTooltip), lhsTooltip, rhsTooltip)) {
                return false;
            }
        }
        {
            Boolean lhsEnabled;
            lhsEnabled = this.getEnabled();
            Boolean rhsEnabled;
            rhsEnabled = that.getEnabled();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "enabled", lhsEnabled), LocatorUtils.property(thatLocator, "enabled", rhsEnabled), lhsEnabled, rhsEnabled)) {
                return false;
            }
        }
        {
            String lhsResourceId;
            lhsResourceId = this.getResourceId();
            String rhsResourceId;
            rhsResourceId = that.getResourceId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "resourceId", lhsResourceId), LocatorUtils.property(thatLocator, "resourceId", rhsResourceId), lhsResourceId, rhsResourceId)) {
                return false;
            }
        }
        {
            String lhsIcon;
            lhsIcon = this.getIcon();
            String rhsIcon;
            rhsIcon = that.getIcon();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "icon", lhsIcon), LocatorUtils.property(thatLocator, "icon", rhsIcon), lhsIcon, rhsIcon)) {
                return false;
            }
        }
        {
            String lhsNextfocuscomponent;
            lhsNextfocuscomponent = this.getNextfocuscomponent();
            String rhsNextfocuscomponent;
            rhsNextfocuscomponent = that.getNextfocuscomponent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "nextfocuscomponent", lhsNextfocuscomponent), LocatorUtils.property(thatLocator, "nextfocuscomponent", rhsNextfocuscomponent), lhsNextfocuscomponent, rhsNextfocuscomponent)) {
                return false;
            }
        }
        {
            String lhsNextfocusfield;
            lhsNextfocusfield = this.getNextfocusfield();
            String rhsNextfocusfield;
            rhsNextfocusfield = that.getNextfocusfield();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "nextfocusfield", lhsNextfocusfield), LocatorUtils.property(thatLocator, "nextfocusfield", rhsNextfocusfield), lhsNextfocusfield, rhsNextfocusfield)) {
                return false;
            }
        }
        {
            String lhsNextfocusonaction;
            lhsNextfocusonaction = this.getNextfocusonaction();
            String rhsNextfocusonaction;
            rhsNextfocusonaction = that.getNextfocusonaction();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "nextfocusonaction", lhsNextfocusonaction), LocatorUtils.property(thatLocator, "nextfocusonaction", rhsNextfocusonaction), lhsNextfocusonaction, rhsNextfocusonaction)) {
                return false;
            }
        }
        {
            Boolean lhsDisableDuringEdit;
            lhsDisableDuringEdit = this.getDisableDuringEdit();
            Boolean rhsDisableDuringEdit;
            rhsDisableDuringEdit = that.getDisableDuringEdit();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "disableDuringEdit", lhsDisableDuringEdit), LocatorUtils.property(thatLocator, "disableDuringEdit", rhsDisableDuringEdit), lhsDisableDuringEdit, rhsDisableDuringEdit)) {
                return false;
            }
        }
        {
            Boolean lhsSaveAfterRuleExecution;
            lhsSaveAfterRuleExecution = this.getSaveAfterRuleExecution();
            Boolean rhsSaveAfterRuleExecution;
            rhsSaveAfterRuleExecution = that.getSaveAfterRuleExecution();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "saveAfterRuleExecution", lhsSaveAfterRuleExecution), LocatorUtils.property(thatLocator, "saveAfterRuleExecution", rhsSaveAfterRuleExecution), lhsSaveAfterRuleExecution, rhsSaveAfterRuleExecution)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            JAXBElement<?> theLayoutconstraints;
            theLayoutconstraints = this.getLayoutconstraints();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "layoutconstraints", theLayoutconstraints), currentHashCode, theLayoutconstraints);
        }
        {
            ClearBorder theClearBorder;
            theClearBorder = this.getClearBorder();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "clearBorder", theClearBorder), currentHashCode, theClearBorder);
        }
        {
            List<JAXBElement<?>> theBorder;
            theBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "border", theBorder), currentHashCode, theBorder);
        }
        {
            MinimumSize theMinimumSize;
            theMinimumSize = this.getMinimumSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "minimumSize", theMinimumSize), currentHashCode, theMinimumSize);
        }
        {
            PreferredSize thePreferredSize;
            thePreferredSize = this.getPreferredSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "preferredSize", thePreferredSize), currentHashCode, thePreferredSize);
        }
        {
            StrictSize theStrictSize;
            theStrictSize = this.getStrictSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "strictSize", theStrictSize), currentHashCode, theStrictSize);
        }
        {
            Font theFont;
            theFont = this.getFont();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "font", theFont), currentHashCode, theFont);
        }
        {
            String theDescription;
            theDescription = this.getDescription();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "description", theDescription), currentHashCode, theDescription);
        }
        {
            List<Property> theProperty;
            theProperty = (((this.property!= null)&&(!this.property.isEmpty()))?this.getProperty():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "property", theProperty), currentHashCode, theProperty);
        }
        {
            Translations theTranslations;
            theTranslations = this.getTranslations();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "translations", theTranslations), currentHashCode, theTranslations);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String theActioncommand;
            theActioncommand = this.getActioncommand();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "actioncommand", theActioncommand), currentHashCode, theActioncommand);
        }
        {
            String theActionkeystroke;
            theActionkeystroke = this.getActionkeystroke();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "actionkeystroke", theActionkeystroke), currentHashCode, theActionkeystroke);
        }
        {
            String theLabel;
            theLabel = this.getLabel();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "label", theLabel), currentHashCode, theLabel);
        }
        {
            String theTooltip;
            theTooltip = this.getTooltip();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "tooltip", theTooltip), currentHashCode, theTooltip);
        }
        {
            Boolean theEnabled;
            theEnabled = this.getEnabled();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "enabled", theEnabled), currentHashCode, theEnabled);
        }
        {
            String theResourceId;
            theResourceId = this.getResourceId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "resourceId", theResourceId), currentHashCode, theResourceId);
        }
        {
            String theIcon;
            theIcon = this.getIcon();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "icon", theIcon), currentHashCode, theIcon);
        }
        {
            String theNextfocuscomponent;
            theNextfocuscomponent = this.getNextfocuscomponent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nextfocuscomponent", theNextfocuscomponent), currentHashCode, theNextfocuscomponent);
        }
        {
            String theNextfocusfield;
            theNextfocusfield = this.getNextfocusfield();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nextfocusfield", theNextfocusfield), currentHashCode, theNextfocusfield);
        }
        {
            String theNextfocusonaction;
            theNextfocusonaction = this.getNextfocusonaction();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nextfocusonaction", theNextfocusonaction), currentHashCode, theNextfocusonaction);
        }
        {
            Boolean theDisableDuringEdit;
            theDisableDuringEdit = this.getDisableDuringEdit();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "disableDuringEdit", theDisableDuringEdit), currentHashCode, theDisableDuringEdit);
        }
        {
            Boolean theSaveAfterRuleExecution;
            theSaveAfterRuleExecution = this.getSaveAfterRuleExecution();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "saveAfterRuleExecution", theSaveAfterRuleExecution), currentHashCode, theSaveAfterRuleExecution);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Button) {
            final Button copy = ((Button) draftCopy);
            if (this.layoutconstraints!= null) {
                JAXBElement<?> sourceLayoutconstraints;
                sourceLayoutconstraints = this.getLayoutconstraints();
                @SuppressWarnings("unchecked")
                JAXBElement<?> copyLayoutconstraints = ((JAXBElement<?> ) strategy.copy(LocatorUtils.property(locator, "layoutconstraints", sourceLayoutconstraints), sourceLayoutconstraints));
                copy.setLayoutconstraints(copyLayoutconstraints);
            } else {
                copy.layoutconstraints = null;
            }
            if (this.clearBorder!= null) {
                ClearBorder sourceClearBorder;
                sourceClearBorder = this.getClearBorder();
                ClearBorder copyClearBorder = ((ClearBorder) strategy.copy(LocatorUtils.property(locator, "clearBorder", sourceClearBorder), sourceClearBorder));
                copy.setClearBorder(copyClearBorder);
            } else {
                copy.clearBorder = null;
            }
            if ((this.border!= null)&&(!this.border.isEmpty())) {
                List<JAXBElement<?>> sourceBorder;
                sourceBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
                @SuppressWarnings("unchecked")
                List<JAXBElement<?>> copyBorder = ((List<JAXBElement<?>> ) strategy.copy(LocatorUtils.property(locator, "border", sourceBorder), sourceBorder));
                copy.border = null;
                if (copyBorder!= null) {
                    List<JAXBElement<?>> uniqueBorderl = copy.getBorder();
                    uniqueBorderl.addAll(copyBorder);
                }
            } else {
                copy.border = null;
            }
            if (this.minimumSize!= null) {
                MinimumSize sourceMinimumSize;
                sourceMinimumSize = this.getMinimumSize();
                MinimumSize copyMinimumSize = ((MinimumSize) strategy.copy(LocatorUtils.property(locator, "minimumSize", sourceMinimumSize), sourceMinimumSize));
                copy.setMinimumSize(copyMinimumSize);
            } else {
                copy.minimumSize = null;
            }
            if (this.preferredSize!= null) {
                PreferredSize sourcePreferredSize;
                sourcePreferredSize = this.getPreferredSize();
                PreferredSize copyPreferredSize = ((PreferredSize) strategy.copy(LocatorUtils.property(locator, "preferredSize", sourcePreferredSize), sourcePreferredSize));
                copy.setPreferredSize(copyPreferredSize);
            } else {
                copy.preferredSize = null;
            }
            if (this.strictSize!= null) {
                StrictSize sourceStrictSize;
                sourceStrictSize = this.getStrictSize();
                StrictSize copyStrictSize = ((StrictSize) strategy.copy(LocatorUtils.property(locator, "strictSize", sourceStrictSize), sourceStrictSize));
                copy.setStrictSize(copyStrictSize);
            } else {
                copy.strictSize = null;
            }
            if (this.font!= null) {
                Font sourceFont;
                sourceFont = this.getFont();
                Font copyFont = ((Font) strategy.copy(LocatorUtils.property(locator, "font", sourceFont), sourceFont));
                copy.setFont(copyFont);
            } else {
                copy.font = null;
            }
            if (this.description!= null) {
                String sourceDescription;
                sourceDescription = this.getDescription();
                String copyDescription = ((String) strategy.copy(LocatorUtils.property(locator, "description", sourceDescription), sourceDescription));
                copy.setDescription(copyDescription);
            } else {
                copy.description = null;
            }
            if ((this.property!= null)&&(!this.property.isEmpty())) {
                List<Property> sourceProperty;
                sourceProperty = (((this.property!= null)&&(!this.property.isEmpty()))?this.getProperty():null);
                @SuppressWarnings("unchecked")
                List<Property> copyProperty = ((List<Property> ) strategy.copy(LocatorUtils.property(locator, "property", sourceProperty), sourceProperty));
                copy.property = null;
                if (copyProperty!= null) {
                    List<Property> uniquePropertyl = copy.getProperty();
                    uniquePropertyl.addAll(copyProperty);
                }
            } else {
                copy.property = null;
            }
            if (this.translations!= null) {
                Translations sourceTranslations;
                sourceTranslations = this.getTranslations();
                Translations copyTranslations = ((Translations) strategy.copy(LocatorUtils.property(locator, "translations", sourceTranslations), sourceTranslations));
                copy.setTranslations(copyTranslations);
            } else {
                copy.translations = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.actioncommand!= null) {
                String sourceActioncommand;
                sourceActioncommand = this.getActioncommand();
                String copyActioncommand = ((String) strategy.copy(LocatorUtils.property(locator, "actioncommand", sourceActioncommand), sourceActioncommand));
                copy.setActioncommand(copyActioncommand);
            } else {
                copy.actioncommand = null;
            }
            if (this.actionkeystroke!= null) {
                String sourceActionkeystroke;
                sourceActionkeystroke = this.getActionkeystroke();
                String copyActionkeystroke = ((String) strategy.copy(LocatorUtils.property(locator, "actionkeystroke", sourceActionkeystroke), sourceActionkeystroke));
                copy.setActionkeystroke(copyActionkeystroke);
            } else {
                copy.actionkeystroke = null;
            }
            if (this.label!= null) {
                String sourceLabel;
                sourceLabel = this.getLabel();
                String copyLabel = ((String) strategy.copy(LocatorUtils.property(locator, "label", sourceLabel), sourceLabel));
                copy.setLabel(copyLabel);
            } else {
                copy.label = null;
            }
            if (this.tooltip!= null) {
                String sourceTooltip;
                sourceTooltip = this.getTooltip();
                String copyTooltip = ((String) strategy.copy(LocatorUtils.property(locator, "tooltip", sourceTooltip), sourceTooltip));
                copy.setTooltip(copyTooltip);
            } else {
                copy.tooltip = null;
            }
            if (this.enabled!= null) {
                Boolean sourceEnabled;
                sourceEnabled = this.getEnabled();
                Boolean copyEnabled = ((Boolean) strategy.copy(LocatorUtils.property(locator, "enabled", sourceEnabled), sourceEnabled));
                copy.setEnabled(copyEnabled);
            } else {
                copy.enabled = null;
            }
            if (this.resourceId!= null) {
                String sourceResourceId;
                sourceResourceId = this.getResourceId();
                String copyResourceId = ((String) strategy.copy(LocatorUtils.property(locator, "resourceId", sourceResourceId), sourceResourceId));
                copy.setResourceId(copyResourceId);
            } else {
                copy.resourceId = null;
            }
            if (this.icon!= null) {
                String sourceIcon;
                sourceIcon = this.getIcon();
                String copyIcon = ((String) strategy.copy(LocatorUtils.property(locator, "icon", sourceIcon), sourceIcon));
                copy.setIcon(copyIcon);
            } else {
                copy.icon = null;
            }
            if (this.nextfocuscomponent!= null) {
                String sourceNextfocuscomponent;
                sourceNextfocuscomponent = this.getNextfocuscomponent();
                String copyNextfocuscomponent = ((String) strategy.copy(LocatorUtils.property(locator, "nextfocuscomponent", sourceNextfocuscomponent), sourceNextfocuscomponent));
                copy.setNextfocuscomponent(copyNextfocuscomponent);
            } else {
                copy.nextfocuscomponent = null;
            }
            if (this.nextfocusfield!= null) {
                String sourceNextfocusfield;
                sourceNextfocusfield = this.getNextfocusfield();
                String copyNextfocusfield = ((String) strategy.copy(LocatorUtils.property(locator, "nextfocusfield", sourceNextfocusfield), sourceNextfocusfield));
                copy.setNextfocusfield(copyNextfocusfield);
            } else {
                copy.nextfocusfield = null;
            }
            if (this.nextfocusonaction!= null) {
                String sourceNextfocusonaction;
                sourceNextfocusonaction = this.getNextfocusonaction();
                String copyNextfocusonaction = ((String) strategy.copy(LocatorUtils.property(locator, "nextfocusonaction", sourceNextfocusonaction), sourceNextfocusonaction));
                copy.setNextfocusonaction(copyNextfocusonaction);
            } else {
                copy.nextfocusonaction = null;
            }
            if (this.disableDuringEdit!= null) {
                Boolean sourceDisableDuringEdit;
                sourceDisableDuringEdit = this.getDisableDuringEdit();
                Boolean copyDisableDuringEdit = ((Boolean) strategy.copy(LocatorUtils.property(locator, "disableDuringEdit", sourceDisableDuringEdit), sourceDisableDuringEdit));
                copy.setDisableDuringEdit(copyDisableDuringEdit);
            } else {
                copy.disableDuringEdit = null;
            }
            if (this.saveAfterRuleExecution!= null) {
                Boolean sourceSaveAfterRuleExecution;
                sourceSaveAfterRuleExecution = this.getSaveAfterRuleExecution();
                Boolean copySaveAfterRuleExecution = ((Boolean) strategy.copy(LocatorUtils.property(locator, "saveAfterRuleExecution", sourceSaveAfterRuleExecution), sourceSaveAfterRuleExecution));
                copy.setSaveAfterRuleExecution(copySaveAfterRuleExecution);
            } else {
                copy.saveAfterRuleExecution = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Button();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Button.Builder<_B> _other) {
        _other.layoutconstraints = this.layoutconstraints;
        _other.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.newCopyBuilder(_other));
        if (this.border == null) {
            _other.border = null;
        } else {
            _other.border = new ArrayList<Buildable>();
            for (JAXBElement<?> _item: this.border) {
                _other.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
            }
        }
        _other.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.newCopyBuilder(_other));
        _other.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.newCopyBuilder(_other));
        _other.strictSize = ((this.strictSize == null)?null:this.strictSize.newCopyBuilder(_other));
        _other.font = ((this.font == null)?null:this.font.newCopyBuilder(_other));
        _other.description = this.description;
        if (this.property == null) {
            _other.property = null;
        } else {
            _other.property = new ArrayList<Property.Builder<Button.Builder<_B>>>();
            for (Property _item: this.property) {
                _other.property.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.translations = ((this.translations == null)?null:this.translations.newCopyBuilder(_other));
        _other.name = this.name;
        _other.actioncommand = this.actioncommand;
        _other.actionkeystroke = this.actionkeystroke;
        _other.label = this.label;
        _other.tooltip = this.tooltip;
        _other.enabled = this.enabled;
        _other.resourceId = this.resourceId;
        _other.icon = this.icon;
        _other.nextfocuscomponent = this.nextfocuscomponent;
        _other.nextfocusfield = this.nextfocusfield;
        _other.nextfocusonaction = this.nextfocusonaction;
        _other.disableDuringEdit = this.disableDuringEdit;
        _other.saveAfterRuleExecution = this.saveAfterRuleExecution;
    }

    public<_B >Button.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Button.Builder<_B>(_parentBuilder, this, true);
    }

    public Button.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Button.Builder<Void> builder() {
        return new Button.Builder<Void>(null, null, false);
    }

    public static<_B >Button.Builder<_B> copyOf(final Button _other) {
        final Button.Builder<_B> _newBuilder = new Button.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Button.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree layoutconstraintsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("layoutconstraints"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(layoutconstraintsPropertyTree!= null):((layoutconstraintsPropertyTree == null)||(!layoutconstraintsPropertyTree.isLeaf())))) {
            _other.layoutconstraints = this.layoutconstraints;
        }
        final PropertyTree clearBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("clearBorder"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(clearBorderPropertyTree!= null):((clearBorderPropertyTree == null)||(!clearBorderPropertyTree.isLeaf())))) {
            _other.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.newCopyBuilder(_other, clearBorderPropertyTree, _propertyTreeUse));
        }
        final PropertyTree borderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("border"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(borderPropertyTree!= null):((borderPropertyTree == null)||(!borderPropertyTree.isLeaf())))) {
            if (this.border == null) {
                _other.border = null;
            } else {
                _other.border = new ArrayList<Buildable>();
                for (JAXBElement<?> _item: this.border) {
                    _other.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                }
            }
        }
        final PropertyTree minimumSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minimumSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minimumSizePropertyTree!= null):((minimumSizePropertyTree == null)||(!minimumSizePropertyTree.isLeaf())))) {
            _other.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.newCopyBuilder(_other, minimumSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree preferredSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("preferredSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(preferredSizePropertyTree!= null):((preferredSizePropertyTree == null)||(!preferredSizePropertyTree.isLeaf())))) {
            _other.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.newCopyBuilder(_other, preferredSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree strictSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("strictSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(strictSizePropertyTree!= null):((strictSizePropertyTree == null)||(!strictSizePropertyTree.isLeaf())))) {
            _other.strictSize = ((this.strictSize == null)?null:this.strictSize.newCopyBuilder(_other, strictSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree fontPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("font"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fontPropertyTree!= null):((fontPropertyTree == null)||(!fontPropertyTree.isLeaf())))) {
            _other.font = ((this.font == null)?null:this.font.newCopyBuilder(_other, fontPropertyTree, _propertyTreeUse));
        }
        final PropertyTree descriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("description"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(descriptionPropertyTree!= null):((descriptionPropertyTree == null)||(!descriptionPropertyTree.isLeaf())))) {
            _other.description = this.description;
        }
        final PropertyTree propertyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("property"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(propertyPropertyTree!= null):((propertyPropertyTree == null)||(!propertyPropertyTree.isLeaf())))) {
            if (this.property == null) {
                _other.property = null;
            } else {
                _other.property = new ArrayList<Property.Builder<Button.Builder<_B>>>();
                for (Property _item: this.property) {
                    _other.property.add(((_item == null)?null:_item.newCopyBuilder(_other, propertyPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree translationsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("translations"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(translationsPropertyTree!= null):((translationsPropertyTree == null)||(!translationsPropertyTree.isLeaf())))) {
            _other.translations = ((this.translations == null)?null:this.translations.newCopyBuilder(_other, translationsPropertyTree, _propertyTreeUse));
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree actioncommandPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("actioncommand"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(actioncommandPropertyTree!= null):((actioncommandPropertyTree == null)||(!actioncommandPropertyTree.isLeaf())))) {
            _other.actioncommand = this.actioncommand;
        }
        final PropertyTree actionkeystrokePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("actionkeystroke"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(actionkeystrokePropertyTree!= null):((actionkeystrokePropertyTree == null)||(!actionkeystrokePropertyTree.isLeaf())))) {
            _other.actionkeystroke = this.actionkeystroke;
        }
        final PropertyTree labelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("label"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(labelPropertyTree!= null):((labelPropertyTree == null)||(!labelPropertyTree.isLeaf())))) {
            _other.label = this.label;
        }
        final PropertyTree tooltipPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("tooltip"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(tooltipPropertyTree!= null):((tooltipPropertyTree == null)||(!tooltipPropertyTree.isLeaf())))) {
            _other.tooltip = this.tooltip;
        }
        final PropertyTree enabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("enabled"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(enabledPropertyTree!= null):((enabledPropertyTree == null)||(!enabledPropertyTree.isLeaf())))) {
            _other.enabled = this.enabled;
        }
        final PropertyTree resourceIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resourceId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resourceIdPropertyTree!= null):((resourceIdPropertyTree == null)||(!resourceIdPropertyTree.isLeaf())))) {
            _other.resourceId = this.resourceId;
        }
        final PropertyTree iconPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("icon"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(iconPropertyTree!= null):((iconPropertyTree == null)||(!iconPropertyTree.isLeaf())))) {
            _other.icon = this.icon;
        }
        final PropertyTree nextfocuscomponentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextfocuscomponent"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextfocuscomponentPropertyTree!= null):((nextfocuscomponentPropertyTree == null)||(!nextfocuscomponentPropertyTree.isLeaf())))) {
            _other.nextfocuscomponent = this.nextfocuscomponent;
        }
        final PropertyTree nextfocusfieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextfocusfield"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextfocusfieldPropertyTree!= null):((nextfocusfieldPropertyTree == null)||(!nextfocusfieldPropertyTree.isLeaf())))) {
            _other.nextfocusfield = this.nextfocusfield;
        }
        final PropertyTree nextfocusonactionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextfocusonaction"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextfocusonactionPropertyTree!= null):((nextfocusonactionPropertyTree == null)||(!nextfocusonactionPropertyTree.isLeaf())))) {
            _other.nextfocusonaction = this.nextfocusonaction;
        }
        final PropertyTree disableDuringEditPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("disableDuringEdit"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(disableDuringEditPropertyTree!= null):((disableDuringEditPropertyTree == null)||(!disableDuringEditPropertyTree.isLeaf())))) {
            _other.disableDuringEdit = this.disableDuringEdit;
        }
        final PropertyTree saveAfterRuleExecutionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("saveAfterRuleExecution"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(saveAfterRuleExecutionPropertyTree!= null):((saveAfterRuleExecutionPropertyTree == null)||(!saveAfterRuleExecutionPropertyTree.isLeaf())))) {
            _other.saveAfterRuleExecution = this.saveAfterRuleExecution;
        }
    }

    public<_B >Button.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Button.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Button.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Button.Builder<_B> copyOf(final Button _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Button.Builder<_B> _newBuilder = new Button.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Button.Builder<Void> copyExcept(final Button _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Button.Builder<Void> copyOnly(final Button _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Button _storedValue;
        private JAXBElement<?> layoutconstraints;
        private ClearBorder.Builder<Button.Builder<_B>> clearBorder;
        private List<Buildable> border;
        private MinimumSize.Builder<Button.Builder<_B>> minimumSize;
        private PreferredSize.Builder<Button.Builder<_B>> preferredSize;
        private StrictSize.Builder<Button.Builder<_B>> strictSize;
        private Font.Builder<Button.Builder<_B>> font;
        private String description;
        private List<Property.Builder<Button.Builder<_B>>> property;
        private Translations.Builder<Button.Builder<_B>> translations;
        private String name;
        private String actioncommand;
        private String actionkeystroke;
        private String label;
        private String tooltip;
        private Boolean enabled;
        private String resourceId;
        private String icon;
        private String nextfocuscomponent;
        private String nextfocusfield;
        private String nextfocusonaction;
        private Boolean disableDuringEdit;
        private Boolean saveAfterRuleExecution;

        public Builder(final _B _parentBuilder, final Button _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.layoutconstraints = _other.layoutconstraints;
                    this.clearBorder = ((_other.clearBorder == null)?null:_other.clearBorder.newCopyBuilder(this));
                    if (_other.border == null) {
                        this.border = null;
                    } else {
                        this.border = new ArrayList<Buildable>();
                        for (JAXBElement<?> _item: _other.border) {
                            this.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                        }
                    }
                    this.minimumSize = ((_other.minimumSize == null)?null:_other.minimumSize.newCopyBuilder(this));
                    this.preferredSize = ((_other.preferredSize == null)?null:_other.preferredSize.newCopyBuilder(this));
                    this.strictSize = ((_other.strictSize == null)?null:_other.strictSize.newCopyBuilder(this));
                    this.font = ((_other.font == null)?null:_other.font.newCopyBuilder(this));
                    this.description = _other.description;
                    if (_other.property == null) {
                        this.property = null;
                    } else {
                        this.property = new ArrayList<Property.Builder<Button.Builder<_B>>>();
                        for (Property _item: _other.property) {
                            this.property.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.translations = ((_other.translations == null)?null:_other.translations.newCopyBuilder(this));
                    this.name = _other.name;
                    this.actioncommand = _other.actioncommand;
                    this.actionkeystroke = _other.actionkeystroke;
                    this.label = _other.label;
                    this.tooltip = _other.tooltip;
                    this.enabled = _other.enabled;
                    this.resourceId = _other.resourceId;
                    this.icon = _other.icon;
                    this.nextfocuscomponent = _other.nextfocuscomponent;
                    this.nextfocusfield = _other.nextfocusfield;
                    this.nextfocusonaction = _other.nextfocusonaction;
                    this.disableDuringEdit = _other.disableDuringEdit;
                    this.saveAfterRuleExecution = _other.saveAfterRuleExecution;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Button _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree layoutconstraintsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("layoutconstraints"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(layoutconstraintsPropertyTree!= null):((layoutconstraintsPropertyTree == null)||(!layoutconstraintsPropertyTree.isLeaf())))) {
                        this.layoutconstraints = _other.layoutconstraints;
                    }
                    final PropertyTree clearBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("clearBorder"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(clearBorderPropertyTree!= null):((clearBorderPropertyTree == null)||(!clearBorderPropertyTree.isLeaf())))) {
                        this.clearBorder = ((_other.clearBorder == null)?null:_other.clearBorder.newCopyBuilder(this, clearBorderPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree borderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("border"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(borderPropertyTree!= null):((borderPropertyTree == null)||(!borderPropertyTree.isLeaf())))) {
                        if (_other.border == null) {
                            this.border = null;
                        } else {
                            this.border = new ArrayList<Buildable>();
                            for (JAXBElement<?> _item: _other.border) {
                                this.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                            }
                        }
                    }
                    final PropertyTree minimumSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minimumSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minimumSizePropertyTree!= null):((minimumSizePropertyTree == null)||(!minimumSizePropertyTree.isLeaf())))) {
                        this.minimumSize = ((_other.minimumSize == null)?null:_other.minimumSize.newCopyBuilder(this, minimumSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree preferredSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("preferredSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(preferredSizePropertyTree!= null):((preferredSizePropertyTree == null)||(!preferredSizePropertyTree.isLeaf())))) {
                        this.preferredSize = ((_other.preferredSize == null)?null:_other.preferredSize.newCopyBuilder(this, preferredSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree strictSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("strictSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(strictSizePropertyTree!= null):((strictSizePropertyTree == null)||(!strictSizePropertyTree.isLeaf())))) {
                        this.strictSize = ((_other.strictSize == null)?null:_other.strictSize.newCopyBuilder(this, strictSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree fontPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("font"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fontPropertyTree!= null):((fontPropertyTree == null)||(!fontPropertyTree.isLeaf())))) {
                        this.font = ((_other.font == null)?null:_other.font.newCopyBuilder(this, fontPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree descriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("description"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(descriptionPropertyTree!= null):((descriptionPropertyTree == null)||(!descriptionPropertyTree.isLeaf())))) {
                        this.description = _other.description;
                    }
                    final PropertyTree propertyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("property"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(propertyPropertyTree!= null):((propertyPropertyTree == null)||(!propertyPropertyTree.isLeaf())))) {
                        if (_other.property == null) {
                            this.property = null;
                        } else {
                            this.property = new ArrayList<Property.Builder<Button.Builder<_B>>>();
                            for (Property _item: _other.property) {
                                this.property.add(((_item == null)?null:_item.newCopyBuilder(this, propertyPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree translationsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("translations"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(translationsPropertyTree!= null):((translationsPropertyTree == null)||(!translationsPropertyTree.isLeaf())))) {
                        this.translations = ((_other.translations == null)?null:_other.translations.newCopyBuilder(this, translationsPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree actioncommandPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("actioncommand"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(actioncommandPropertyTree!= null):((actioncommandPropertyTree == null)||(!actioncommandPropertyTree.isLeaf())))) {
                        this.actioncommand = _other.actioncommand;
                    }
                    final PropertyTree actionkeystrokePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("actionkeystroke"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(actionkeystrokePropertyTree!= null):((actionkeystrokePropertyTree == null)||(!actionkeystrokePropertyTree.isLeaf())))) {
                        this.actionkeystroke = _other.actionkeystroke;
                    }
                    final PropertyTree labelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("label"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(labelPropertyTree!= null):((labelPropertyTree == null)||(!labelPropertyTree.isLeaf())))) {
                        this.label = _other.label;
                    }
                    final PropertyTree tooltipPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("tooltip"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(tooltipPropertyTree!= null):((tooltipPropertyTree == null)||(!tooltipPropertyTree.isLeaf())))) {
                        this.tooltip = _other.tooltip;
                    }
                    final PropertyTree enabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("enabled"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(enabledPropertyTree!= null):((enabledPropertyTree == null)||(!enabledPropertyTree.isLeaf())))) {
                        this.enabled = _other.enabled;
                    }
                    final PropertyTree resourceIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resourceId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resourceIdPropertyTree!= null):((resourceIdPropertyTree == null)||(!resourceIdPropertyTree.isLeaf())))) {
                        this.resourceId = _other.resourceId;
                    }
                    final PropertyTree iconPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("icon"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(iconPropertyTree!= null):((iconPropertyTree == null)||(!iconPropertyTree.isLeaf())))) {
                        this.icon = _other.icon;
                    }
                    final PropertyTree nextfocuscomponentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextfocuscomponent"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextfocuscomponentPropertyTree!= null):((nextfocuscomponentPropertyTree == null)||(!nextfocuscomponentPropertyTree.isLeaf())))) {
                        this.nextfocuscomponent = _other.nextfocuscomponent;
                    }
                    final PropertyTree nextfocusfieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextfocusfield"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextfocusfieldPropertyTree!= null):((nextfocusfieldPropertyTree == null)||(!nextfocusfieldPropertyTree.isLeaf())))) {
                        this.nextfocusfield = _other.nextfocusfield;
                    }
                    final PropertyTree nextfocusonactionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextfocusonaction"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextfocusonactionPropertyTree!= null):((nextfocusonactionPropertyTree == null)||(!nextfocusonactionPropertyTree.isLeaf())))) {
                        this.nextfocusonaction = _other.nextfocusonaction;
                    }
                    final PropertyTree disableDuringEditPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("disableDuringEdit"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(disableDuringEditPropertyTree!= null):((disableDuringEditPropertyTree == null)||(!disableDuringEditPropertyTree.isLeaf())))) {
                        this.disableDuringEdit = _other.disableDuringEdit;
                    }
                    final PropertyTree saveAfterRuleExecutionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("saveAfterRuleExecution"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(saveAfterRuleExecutionPropertyTree!= null):((saveAfterRuleExecutionPropertyTree == null)||(!saveAfterRuleExecutionPropertyTree.isLeaf())))) {
                        this.saveAfterRuleExecution = _other.saveAfterRuleExecution;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Button >_P init(final _P _product) {
            _product.layoutconstraints = this.layoutconstraints;
            _product.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.build());
            if (this.border!= null) {
                final List<JAXBElement<?>> border = new ArrayList<JAXBElement<?>>(this.border.size());
                for (Buildable _item: this.border) {
                    border.add(((JAXBElement<?> ) _item.build()));
                }
                _product.border = border;
            }
            _product.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.build());
            _product.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.build());
            _product.strictSize = ((this.strictSize == null)?null:this.strictSize.build());
            _product.font = ((this.font == null)?null:this.font.build());
            _product.description = this.description;
            if (this.property!= null) {
                final List<Property> property = new ArrayList<Property>(this.property.size());
                for (Property.Builder<Button.Builder<_B>> _item: this.property) {
                    property.add(_item.build());
                }
                _product.property = property;
            }
            _product.translations = ((this.translations == null)?null:this.translations.build());
            _product.name = this.name;
            _product.actioncommand = this.actioncommand;
            _product.actionkeystroke = this.actionkeystroke;
            _product.label = this.label;
            _product.tooltip = this.tooltip;
            _product.enabled = this.enabled;
            _product.resourceId = this.resourceId;
            _product.icon = this.icon;
            _product.nextfocuscomponent = this.nextfocuscomponent;
            _product.nextfocusfield = this.nextfocusfield;
            _product.nextfocusonaction = this.nextfocusonaction;
            _product.disableDuringEdit = this.disableDuringEdit;
            _product.saveAfterRuleExecution = this.saveAfterRuleExecution;
            return _product;
        }

        /**
         * Sets the new value of "layoutconstraints" (any previous value will be replaced)
         * 
         * @param layoutconstraints
         *     New value of the "layoutconstraints" property.
         */
        public Button.Builder<_B> withLayoutconstraints(final JAXBElement<?> layoutconstraints) {
            this.layoutconstraints = layoutconstraints;
            return this;
        }

        /**
         * Sets the new value of "clearBorder" (any previous value will be replaced)
         * 
         * @param clearBorder
         *     New value of the "clearBorder" property.
         */
        public Button.Builder<_B> withClearBorder(final ClearBorder clearBorder) {
            this.clearBorder = ((clearBorder == null)?null:new ClearBorder.Builder<Button.Builder<_B>>(this, clearBorder, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "clearBorder" property.
         * Use {@link org.nuclos.schema.layout.layoutml.ClearBorder.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "clearBorder" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.ClearBorder.Builder#end()} to return to the current builder.
         */
        public ClearBorder.Builder<? extends Button.Builder<_B>> withClearBorder() {
            if (this.clearBorder!= null) {
                return this.clearBorder;
            }
            return this.clearBorder = new ClearBorder.Builder<Button.Builder<_B>>(this, null, false);
        }

        /**
         * Adds the given items to the value of "border"
         * 
         * @param border
         *     Items to add to the value of the "border" property
         */
        public Button.Builder<_B> addBorder(final Iterable<? extends JAXBElement<?>> border) {
            if (border!= null) {
                if (this.border == null) {
                    this.border = new ArrayList<Buildable>();
                }
                for (JAXBElement<?> _item: border) {
                    this.border.add(new Buildable.PrimitiveBuildable(_item));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "border" (any previous value will be replaced)
         * 
         * @param border
         *     New value of the "border" property.
         */
        public Button.Builder<_B> withBorder(final Iterable<? extends JAXBElement<?>> border) {
            if (this.border!= null) {
                this.border.clear();
            }
            return addBorder(border);
        }

        /**
         * Adds the given items to the value of "border"
         * 
         * @param border
         *     Items to add to the value of the "border" property
         */
        public Button.Builder<_B> addBorder(JAXBElement<?> ... border) {
            addBorder(Arrays.asList(border));
            return this;
        }

        /**
         * Sets the new value of "border" (any previous value will be replaced)
         * 
         * @param border
         *     New value of the "border" property.
         */
        public Button.Builder<_B> withBorder(JAXBElement<?> ... border) {
            withBorder(Arrays.asList(border));
            return this;
        }

        /**
         * Sets the new value of "minimumSize" (any previous value will be replaced)
         * 
         * @param minimumSize
         *     New value of the "minimumSize" property.
         */
        public Button.Builder<_B> withMinimumSize(final MinimumSize minimumSize) {
            this.minimumSize = ((minimumSize == null)?null:new MinimumSize.Builder<Button.Builder<_B>>(this, minimumSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "minimumSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.MinimumSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "minimumSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.MinimumSize.Builder#end()} to return to the current builder.
         */
        public MinimumSize.Builder<? extends Button.Builder<_B>> withMinimumSize() {
            if (this.minimumSize!= null) {
                return this.minimumSize;
            }
            return this.minimumSize = new MinimumSize.Builder<Button.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "preferredSize" (any previous value will be replaced)
         * 
         * @param preferredSize
         *     New value of the "preferredSize" property.
         */
        public Button.Builder<_B> withPreferredSize(final PreferredSize preferredSize) {
            this.preferredSize = ((preferredSize == null)?null:new PreferredSize.Builder<Button.Builder<_B>>(this, preferredSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "preferredSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.PreferredSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "preferredSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.PreferredSize.Builder#end()} to return to the current builder.
         */
        public PreferredSize.Builder<? extends Button.Builder<_B>> withPreferredSize() {
            if (this.preferredSize!= null) {
                return this.preferredSize;
            }
            return this.preferredSize = new PreferredSize.Builder<Button.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "strictSize" (any previous value will be replaced)
         * 
         * @param strictSize
         *     New value of the "strictSize" property.
         */
        public Button.Builder<_B> withStrictSize(final StrictSize strictSize) {
            this.strictSize = ((strictSize == null)?null:new StrictSize.Builder<Button.Builder<_B>>(this, strictSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "strictSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.StrictSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "strictSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.StrictSize.Builder#end()} to return to the current builder.
         */
        public StrictSize.Builder<? extends Button.Builder<_B>> withStrictSize() {
            if (this.strictSize!= null) {
                return this.strictSize;
            }
            return this.strictSize = new StrictSize.Builder<Button.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "font" (any previous value will be replaced)
         * 
         * @param font
         *     New value of the "font" property.
         */
        public Button.Builder<_B> withFont(final Font font) {
            this.font = ((font == null)?null:new Font.Builder<Button.Builder<_B>>(this, font, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "font" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Font.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "font" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Font.Builder#end()} to return to the current builder.
         */
        public Font.Builder<? extends Button.Builder<_B>> withFont() {
            if (this.font!= null) {
                return this.font;
            }
            return this.font = new Font.Builder<Button.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "description" (any previous value will be replaced)
         * 
         * @param description
         *     New value of the "description" property.
         */
        public Button.Builder<_B> withDescription(final String description) {
            this.description = description;
            return this;
        }

        /**
         * Adds the given items to the value of "property"
         * 
         * @param property
         *     Items to add to the value of the "property" property
         */
        public Button.Builder<_B> addProperty(final Iterable<? extends Property> property) {
            if (property!= null) {
                if (this.property == null) {
                    this.property = new ArrayList<Property.Builder<Button.Builder<_B>>>();
                }
                for (Property _item: property) {
                    this.property.add(new Property.Builder<Button.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "property" (any previous value will be replaced)
         * 
         * @param property
         *     New value of the "property" property.
         */
        public Button.Builder<_B> withProperty(final Iterable<? extends Property> property) {
            if (this.property!= null) {
                this.property.clear();
            }
            return addProperty(property);
        }

        /**
         * Adds the given items to the value of "property"
         * 
         * @param property
         *     Items to add to the value of the "property" property
         */
        public Button.Builder<_B> addProperty(Property... property) {
            addProperty(Arrays.asList(property));
            return this;
        }

        /**
         * Sets the new value of "property" (any previous value will be replaced)
         * 
         * @param property
         *     New value of the "property" property.
         */
        public Button.Builder<_B> withProperty(Property... property) {
            withProperty(Arrays.asList(property));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Property" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Property.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Property" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Property.Builder#end()} to return to the current builder.
         */
        public Property.Builder<? extends Button.Builder<_B>> addProperty() {
            if (this.property == null) {
                this.property = new ArrayList<Property.Builder<Button.Builder<_B>>>();
            }
            final Property.Builder<Button.Builder<_B>> property_Builder = new Property.Builder<Button.Builder<_B>>(this, null, false);
            this.property.add(property_Builder);
            return property_Builder;
        }

        /**
         * Sets the new value of "translations" (any previous value will be replaced)
         * 
         * @param translations
         *     New value of the "translations" property.
         */
        public Button.Builder<_B> withTranslations(final Translations translations) {
            this.translations = ((translations == null)?null:new Translations.Builder<Button.Builder<_B>>(this, translations, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "translations" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Translations.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "translations" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Translations.Builder#end()} to return to the current builder.
         */
        public Translations.Builder<? extends Button.Builder<_B>> withTranslations() {
            if (this.translations!= null) {
                return this.translations;
            }
            return this.translations = new Translations.Builder<Button.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public Button.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "actioncommand" (any previous value will be replaced)
         * 
         * @param actioncommand
         *     New value of the "actioncommand" property.
         */
        public Button.Builder<_B> withActioncommand(final String actioncommand) {
            this.actioncommand = actioncommand;
            return this;
        }

        /**
         * Sets the new value of "actionkeystroke" (any previous value will be replaced)
         * 
         * @param actionkeystroke
         *     New value of the "actionkeystroke" property.
         */
        public Button.Builder<_B> withActionkeystroke(final String actionkeystroke) {
            this.actionkeystroke = actionkeystroke;
            return this;
        }

        /**
         * Sets the new value of "label" (any previous value will be replaced)
         * 
         * @param label
         *     New value of the "label" property.
         */
        public Button.Builder<_B> withLabel(final String label) {
            this.label = label;
            return this;
        }

        /**
         * Sets the new value of "tooltip" (any previous value will be replaced)
         * 
         * @param tooltip
         *     New value of the "tooltip" property.
         */
        public Button.Builder<_B> withTooltip(final String tooltip) {
            this.tooltip = tooltip;
            return this;
        }

        /**
         * Sets the new value of "enabled" (any previous value will be replaced)
         * 
         * @param enabled
         *     New value of the "enabled" property.
         */
        public Button.Builder<_B> withEnabled(final Boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        /**
         * Sets the new value of "resourceId" (any previous value will be replaced)
         * 
         * @param resourceId
         *     New value of the "resourceId" property.
         */
        public Button.Builder<_B> withResourceId(final String resourceId) {
            this.resourceId = resourceId;
            return this;
        }

        /**
         * Sets the new value of "icon" (any previous value will be replaced)
         * 
         * @param icon
         *     New value of the "icon" property.
         */
        public Button.Builder<_B> withIcon(final String icon) {
            this.icon = icon;
            return this;
        }

        /**
         * Sets the new value of "nextfocuscomponent" (any previous value will be replaced)
         * 
         * @param nextfocuscomponent
         *     New value of the "nextfocuscomponent" property.
         */
        public Button.Builder<_B> withNextfocuscomponent(final String nextfocuscomponent) {
            this.nextfocuscomponent = nextfocuscomponent;
            return this;
        }

        /**
         * Sets the new value of "nextfocusfield" (any previous value will be replaced)
         * 
         * @param nextfocusfield
         *     New value of the "nextfocusfield" property.
         */
        public Button.Builder<_B> withNextfocusfield(final String nextfocusfield) {
            this.nextfocusfield = nextfocusfield;
            return this;
        }

        /**
         * Sets the new value of "nextfocusonaction" (any previous value will be replaced)
         * 
         * @param nextfocusonaction
         *     New value of the "nextfocusonaction" property.
         */
        public Button.Builder<_B> withNextfocusonaction(final String nextfocusonaction) {
            this.nextfocusonaction = nextfocusonaction;
            return this;
        }

        /**
         * Sets the new value of "disableDuringEdit" (any previous value will be replaced)
         * 
         * @param disableDuringEdit
         *     New value of the "disableDuringEdit" property.
         */
        public Button.Builder<_B> withDisableDuringEdit(final Boolean disableDuringEdit) {
            this.disableDuringEdit = disableDuringEdit;
            return this;
        }

        /**
         * Sets the new value of "saveAfterRuleExecution" (any previous value will be replaced)
         * 
         * @param saveAfterRuleExecution
         *     New value of the "saveAfterRuleExecution" property.
         */
        public Button.Builder<_B> withSaveAfterRuleExecution(final Boolean saveAfterRuleExecution) {
            this.saveAfterRuleExecution = saveAfterRuleExecution;
            return this;
        }

        @Override
        public Button build() {
            if (_storedValue == null) {
                return this.init(new Button());
            } else {
                return ((Button) _storedValue);
            }
        }

        public Button.Builder<_B> copyOf(final Button _other) {
            _other.copyTo(this);
            return this;
        }

        public Button.Builder<_B> copyOf(final Button.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Button.Selector<Button.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Button.Select _root() {
            return new Button.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> layoutconstraints = null;
        private ClearBorder.Selector<TRoot, Button.Selector<TRoot, TParent>> clearBorder = null;
        private com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> border = null;
        private MinimumSize.Selector<TRoot, Button.Selector<TRoot, TParent>> minimumSize = null;
        private PreferredSize.Selector<TRoot, Button.Selector<TRoot, TParent>> preferredSize = null;
        private StrictSize.Selector<TRoot, Button.Selector<TRoot, TParent>> strictSize = null;
        private Font.Selector<TRoot, Button.Selector<TRoot, TParent>> font = null;
        private com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> description = null;
        private Property.Selector<TRoot, Button.Selector<TRoot, TParent>> property = null;
        private Translations.Selector<TRoot, Button.Selector<TRoot, TParent>> translations = null;
        private com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> actioncommand = null;
        private com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> actionkeystroke = null;
        private com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> label = null;
        private com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> tooltip = null;
        private com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> enabled = null;
        private com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> resourceId = null;
        private com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> icon = null;
        private com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> nextfocuscomponent = null;
        private com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> nextfocusfield = null;
        private com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> nextfocusonaction = null;
        private com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> disableDuringEdit = null;
        private com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> saveAfterRuleExecution = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.layoutconstraints!= null) {
                products.put("layoutconstraints", this.layoutconstraints.init());
            }
            if (this.clearBorder!= null) {
                products.put("clearBorder", this.clearBorder.init());
            }
            if (this.border!= null) {
                products.put("border", this.border.init());
            }
            if (this.minimumSize!= null) {
                products.put("minimumSize", this.minimumSize.init());
            }
            if (this.preferredSize!= null) {
                products.put("preferredSize", this.preferredSize.init());
            }
            if (this.strictSize!= null) {
                products.put("strictSize", this.strictSize.init());
            }
            if (this.font!= null) {
                products.put("font", this.font.init());
            }
            if (this.description!= null) {
                products.put("description", this.description.init());
            }
            if (this.property!= null) {
                products.put("property", this.property.init());
            }
            if (this.translations!= null) {
                products.put("translations", this.translations.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.actioncommand!= null) {
                products.put("actioncommand", this.actioncommand.init());
            }
            if (this.actionkeystroke!= null) {
                products.put("actionkeystroke", this.actionkeystroke.init());
            }
            if (this.label!= null) {
                products.put("label", this.label.init());
            }
            if (this.tooltip!= null) {
                products.put("tooltip", this.tooltip.init());
            }
            if (this.enabled!= null) {
                products.put("enabled", this.enabled.init());
            }
            if (this.resourceId!= null) {
                products.put("resourceId", this.resourceId.init());
            }
            if (this.icon!= null) {
                products.put("icon", this.icon.init());
            }
            if (this.nextfocuscomponent!= null) {
                products.put("nextfocuscomponent", this.nextfocuscomponent.init());
            }
            if (this.nextfocusfield!= null) {
                products.put("nextfocusfield", this.nextfocusfield.init());
            }
            if (this.nextfocusonaction!= null) {
                products.put("nextfocusonaction", this.nextfocusonaction.init());
            }
            if (this.disableDuringEdit!= null) {
                products.put("disableDuringEdit", this.disableDuringEdit.init());
            }
            if (this.saveAfterRuleExecution!= null) {
                products.put("saveAfterRuleExecution", this.saveAfterRuleExecution.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> layoutconstraints() {
            return ((this.layoutconstraints == null)?this.layoutconstraints = new com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>>(this._root, this, "layoutconstraints"):this.layoutconstraints);
        }

        public ClearBorder.Selector<TRoot, Button.Selector<TRoot, TParent>> clearBorder() {
            return ((this.clearBorder == null)?this.clearBorder = new ClearBorder.Selector<TRoot, Button.Selector<TRoot, TParent>>(this._root, this, "clearBorder"):this.clearBorder);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> border() {
            return ((this.border == null)?this.border = new com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>>(this._root, this, "border"):this.border);
        }

        public MinimumSize.Selector<TRoot, Button.Selector<TRoot, TParent>> minimumSize() {
            return ((this.minimumSize == null)?this.minimumSize = new MinimumSize.Selector<TRoot, Button.Selector<TRoot, TParent>>(this._root, this, "minimumSize"):this.minimumSize);
        }

        public PreferredSize.Selector<TRoot, Button.Selector<TRoot, TParent>> preferredSize() {
            return ((this.preferredSize == null)?this.preferredSize = new PreferredSize.Selector<TRoot, Button.Selector<TRoot, TParent>>(this._root, this, "preferredSize"):this.preferredSize);
        }

        public StrictSize.Selector<TRoot, Button.Selector<TRoot, TParent>> strictSize() {
            return ((this.strictSize == null)?this.strictSize = new StrictSize.Selector<TRoot, Button.Selector<TRoot, TParent>>(this._root, this, "strictSize"):this.strictSize);
        }

        public Font.Selector<TRoot, Button.Selector<TRoot, TParent>> font() {
            return ((this.font == null)?this.font = new Font.Selector<TRoot, Button.Selector<TRoot, TParent>>(this._root, this, "font"):this.font);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> description() {
            return ((this.description == null)?this.description = new com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>>(this._root, this, "description"):this.description);
        }

        public Property.Selector<TRoot, Button.Selector<TRoot, TParent>> property() {
            return ((this.property == null)?this.property = new Property.Selector<TRoot, Button.Selector<TRoot, TParent>>(this._root, this, "property"):this.property);
        }

        public Translations.Selector<TRoot, Button.Selector<TRoot, TParent>> translations() {
            return ((this.translations == null)?this.translations = new Translations.Selector<TRoot, Button.Selector<TRoot, TParent>>(this._root, this, "translations"):this.translations);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> actioncommand() {
            return ((this.actioncommand == null)?this.actioncommand = new com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>>(this._root, this, "actioncommand"):this.actioncommand);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> actionkeystroke() {
            return ((this.actionkeystroke == null)?this.actionkeystroke = new com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>>(this._root, this, "actionkeystroke"):this.actionkeystroke);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> label() {
            return ((this.label == null)?this.label = new com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>>(this._root, this, "label"):this.label);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> tooltip() {
            return ((this.tooltip == null)?this.tooltip = new com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>>(this._root, this, "tooltip"):this.tooltip);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> enabled() {
            return ((this.enabled == null)?this.enabled = new com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>>(this._root, this, "enabled"):this.enabled);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> resourceId() {
            return ((this.resourceId == null)?this.resourceId = new com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>>(this._root, this, "resourceId"):this.resourceId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> icon() {
            return ((this.icon == null)?this.icon = new com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>>(this._root, this, "icon"):this.icon);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> nextfocuscomponent() {
            return ((this.nextfocuscomponent == null)?this.nextfocuscomponent = new com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>>(this._root, this, "nextfocuscomponent"):this.nextfocuscomponent);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> nextfocusfield() {
            return ((this.nextfocusfield == null)?this.nextfocusfield = new com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>>(this._root, this, "nextfocusfield"):this.nextfocusfield);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> nextfocusonaction() {
            return ((this.nextfocusonaction == null)?this.nextfocusonaction = new com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>>(this._root, this, "nextfocusonaction"):this.nextfocusonaction);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> disableDuringEdit() {
            return ((this.disableDuringEdit == null)?this.disableDuringEdit = new com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>>(this._root, this, "disableDuringEdit"):this.disableDuringEdit);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>> saveAfterRuleExecution() {
            return ((this.saveAfterRuleExecution == null)?this.saveAfterRuleExecution = new com.kscs.util.jaxb.Selector<TRoot, Button.Selector<TRoot, TParent>>(this._root, this, "saveAfterRuleExecution"):this.saveAfterRuleExecution);
        }

    }

}
