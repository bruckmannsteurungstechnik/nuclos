
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="gap" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="fill-horizontally" type="{}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class Columnlayout implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "gap")
    @XmlSchemaType(name = "anySimpleType")
    protected String gap;
    @XmlAttribute(name = "fill-horizontally")
    protected Boolean fillHorizontally;

    /**
     * Gets the value of the gap property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGap() {
        return gap;
    }

    /**
     * Sets the value of the gap property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGap(String value) {
        this.gap = value;
    }

    /**
     * Gets the value of the fillHorizontally property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getFillHorizontally() {
        return fillHorizontally;
    }

    /**
     * Sets the value of the fillHorizontally property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFillHorizontally(Boolean value) {
        this.fillHorizontally = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theGap;
            theGap = this.getGap();
            strategy.appendField(locator, this, "gap", buffer, theGap);
        }
        {
            Boolean theFillHorizontally;
            theFillHorizontally = this.getFillHorizontally();
            strategy.appendField(locator, this, "fillHorizontally", buffer, theFillHorizontally);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Columnlayout)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Columnlayout that = ((Columnlayout) object);
        {
            String lhsGap;
            lhsGap = this.getGap();
            String rhsGap;
            rhsGap = that.getGap();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "gap", lhsGap), LocatorUtils.property(thatLocator, "gap", rhsGap), lhsGap, rhsGap)) {
                return false;
            }
        }
        {
            Boolean lhsFillHorizontally;
            lhsFillHorizontally = this.getFillHorizontally();
            Boolean rhsFillHorizontally;
            rhsFillHorizontally = that.getFillHorizontally();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fillHorizontally", lhsFillHorizontally), LocatorUtils.property(thatLocator, "fillHorizontally", rhsFillHorizontally), lhsFillHorizontally, rhsFillHorizontally)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theGap;
            theGap = this.getGap();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "gap", theGap), currentHashCode, theGap);
        }
        {
            Boolean theFillHorizontally;
            theFillHorizontally = this.getFillHorizontally();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fillHorizontally", theFillHorizontally), currentHashCode, theFillHorizontally);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Columnlayout) {
            final Columnlayout copy = ((Columnlayout) draftCopy);
            if (this.gap!= null) {
                String sourceGap;
                sourceGap = this.getGap();
                String copyGap = ((String) strategy.copy(LocatorUtils.property(locator, "gap", sourceGap), sourceGap));
                copy.setGap(copyGap);
            } else {
                copy.gap = null;
            }
            if (this.fillHorizontally!= null) {
                Boolean sourceFillHorizontally;
                sourceFillHorizontally = this.getFillHorizontally();
                Boolean copyFillHorizontally = ((Boolean) strategy.copy(LocatorUtils.property(locator, "fillHorizontally", sourceFillHorizontally), sourceFillHorizontally));
                copy.setFillHorizontally(copyFillHorizontally);
            } else {
                copy.fillHorizontally = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Columnlayout();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Columnlayout.Builder<_B> _other) {
        _other.gap = this.gap;
        _other.fillHorizontally = this.fillHorizontally;
    }

    public<_B >Columnlayout.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Columnlayout.Builder<_B>(_parentBuilder, this, true);
    }

    public Columnlayout.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Columnlayout.Builder<Void> builder() {
        return new Columnlayout.Builder<Void>(null, null, false);
    }

    public static<_B >Columnlayout.Builder<_B> copyOf(final Columnlayout _other) {
        final Columnlayout.Builder<_B> _newBuilder = new Columnlayout.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Columnlayout.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree gapPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("gap"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(gapPropertyTree!= null):((gapPropertyTree == null)||(!gapPropertyTree.isLeaf())))) {
            _other.gap = this.gap;
        }
        final PropertyTree fillHorizontallyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fillHorizontally"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fillHorizontallyPropertyTree!= null):((fillHorizontallyPropertyTree == null)||(!fillHorizontallyPropertyTree.isLeaf())))) {
            _other.fillHorizontally = this.fillHorizontally;
        }
    }

    public<_B >Columnlayout.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Columnlayout.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Columnlayout.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Columnlayout.Builder<_B> copyOf(final Columnlayout _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Columnlayout.Builder<_B> _newBuilder = new Columnlayout.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Columnlayout.Builder<Void> copyExcept(final Columnlayout _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Columnlayout.Builder<Void> copyOnly(final Columnlayout _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Columnlayout _storedValue;
        private String gap;
        private Boolean fillHorizontally;

        public Builder(final _B _parentBuilder, final Columnlayout _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.gap = _other.gap;
                    this.fillHorizontally = _other.fillHorizontally;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Columnlayout _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree gapPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("gap"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(gapPropertyTree!= null):((gapPropertyTree == null)||(!gapPropertyTree.isLeaf())))) {
                        this.gap = _other.gap;
                    }
                    final PropertyTree fillHorizontallyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fillHorizontally"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fillHorizontallyPropertyTree!= null):((fillHorizontallyPropertyTree == null)||(!fillHorizontallyPropertyTree.isLeaf())))) {
                        this.fillHorizontally = _other.fillHorizontally;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Columnlayout >_P init(final _P _product) {
            _product.gap = this.gap;
            _product.fillHorizontally = this.fillHorizontally;
            return _product;
        }

        /**
         * Sets the new value of "gap" (any previous value will be replaced)
         * 
         * @param gap
         *     New value of the "gap" property.
         */
        public Columnlayout.Builder<_B> withGap(final String gap) {
            this.gap = gap;
            return this;
        }

        /**
         * Sets the new value of "fillHorizontally" (any previous value will be replaced)
         * 
         * @param fillHorizontally
         *     New value of the "fillHorizontally" property.
         */
        public Columnlayout.Builder<_B> withFillHorizontally(final Boolean fillHorizontally) {
            this.fillHorizontally = fillHorizontally;
            return this;
        }

        @Override
        public Columnlayout build() {
            if (_storedValue == null) {
                return this.init(new Columnlayout());
            } else {
                return ((Columnlayout) _storedValue);
            }
        }

        public Columnlayout.Builder<_B> copyOf(final Columnlayout _other) {
            _other.copyTo(this);
            return this;
        }

        public Columnlayout.Builder<_B> copyOf(final Columnlayout.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Columnlayout.Selector<Columnlayout.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Columnlayout.Select _root() {
            return new Columnlayout.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, Columnlayout.Selector<TRoot, TParent>> gap = null;
        private com.kscs.util.jaxb.Selector<TRoot, Columnlayout.Selector<TRoot, TParent>> fillHorizontally = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.gap!= null) {
                products.put("gap", this.gap.init());
            }
            if (this.fillHorizontally!= null) {
                products.put("fillHorizontally", this.fillHorizontally.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, Columnlayout.Selector<TRoot, TParent>> gap() {
            return ((this.gap == null)?this.gap = new com.kscs.util.jaxb.Selector<TRoot, Columnlayout.Selector<TRoot, TParent>>(this._root, this, "gap"):this.gap);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Columnlayout.Selector<TRoot, TParent>> fillHorizontally() {
            return ((this.fillHorizontally == null)?this.fillHorizontally = new com.kscs.util.jaxb.Selector<TRoot, Columnlayout.Selector<TRoot, TParent>>(this._root, this, "fillHorizontally"):this.fillHorizontally);
        }

    }

}
