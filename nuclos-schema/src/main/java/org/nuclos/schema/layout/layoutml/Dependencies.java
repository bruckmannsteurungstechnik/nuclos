
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}dependency" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dependency"
})
@XmlRootElement(name = "dependencies")
public class Dependencies implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected List<Dependency> dependency;

    /**
     * Gets the value of the dependency property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dependency property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDependency().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Dependency }
     * 
     * 
     */
    public List<Dependency> getDependency() {
        if (dependency == null) {
            dependency = new ArrayList<Dependency>();
        }
        return this.dependency;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<Dependency> theDependency;
            theDependency = (((this.dependency!= null)&&(!this.dependency.isEmpty()))?this.getDependency():null);
            strategy.appendField(locator, this, "dependency", buffer, theDependency);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Dependencies)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Dependencies that = ((Dependencies) object);
        {
            List<Dependency> lhsDependency;
            lhsDependency = (((this.dependency!= null)&&(!this.dependency.isEmpty()))?this.getDependency():null);
            List<Dependency> rhsDependency;
            rhsDependency = (((that.dependency!= null)&&(!that.dependency.isEmpty()))?that.getDependency():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dependency", lhsDependency), LocatorUtils.property(thatLocator, "dependency", rhsDependency), lhsDependency, rhsDependency)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<Dependency> theDependency;
            theDependency = (((this.dependency!= null)&&(!this.dependency.isEmpty()))?this.getDependency():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dependency", theDependency), currentHashCode, theDependency);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Dependencies) {
            final Dependencies copy = ((Dependencies) draftCopy);
            if ((this.dependency!= null)&&(!this.dependency.isEmpty())) {
                List<Dependency> sourceDependency;
                sourceDependency = (((this.dependency!= null)&&(!this.dependency.isEmpty()))?this.getDependency():null);
                @SuppressWarnings("unchecked")
                List<Dependency> copyDependency = ((List<Dependency> ) strategy.copy(LocatorUtils.property(locator, "dependency", sourceDependency), sourceDependency));
                copy.dependency = null;
                if (copyDependency!= null) {
                    List<Dependency> uniqueDependencyl = copy.getDependency();
                    uniqueDependencyl.addAll(copyDependency);
                }
            } else {
                copy.dependency = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Dependencies();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Dependencies.Builder<_B> _other) {
        if (this.dependency == null) {
            _other.dependency = null;
        } else {
            _other.dependency = new ArrayList<Dependency.Builder<Dependencies.Builder<_B>>>();
            for (Dependency _item: this.dependency) {
                _other.dependency.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
    }

    public<_B >Dependencies.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Dependencies.Builder<_B>(_parentBuilder, this, true);
    }

    public Dependencies.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Dependencies.Builder<Void> builder() {
        return new Dependencies.Builder<Void>(null, null, false);
    }

    public static<_B >Dependencies.Builder<_B> copyOf(final Dependencies _other) {
        final Dependencies.Builder<_B> _newBuilder = new Dependencies.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Dependencies.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree dependencyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dependency"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dependencyPropertyTree!= null):((dependencyPropertyTree == null)||(!dependencyPropertyTree.isLeaf())))) {
            if (this.dependency == null) {
                _other.dependency = null;
            } else {
                _other.dependency = new ArrayList<Dependency.Builder<Dependencies.Builder<_B>>>();
                for (Dependency _item: this.dependency) {
                    _other.dependency.add(((_item == null)?null:_item.newCopyBuilder(_other, dependencyPropertyTree, _propertyTreeUse)));
                }
            }
        }
    }

    public<_B >Dependencies.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Dependencies.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Dependencies.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Dependencies.Builder<_B> copyOf(final Dependencies _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Dependencies.Builder<_B> _newBuilder = new Dependencies.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Dependencies.Builder<Void> copyExcept(final Dependencies _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Dependencies.Builder<Void> copyOnly(final Dependencies _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Dependencies _storedValue;
        private List<Dependency.Builder<Dependencies.Builder<_B>>> dependency;

        public Builder(final _B _parentBuilder, final Dependencies _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.dependency == null) {
                        this.dependency = null;
                    } else {
                        this.dependency = new ArrayList<Dependency.Builder<Dependencies.Builder<_B>>>();
                        for (Dependency _item: _other.dependency) {
                            this.dependency.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Dependencies _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree dependencyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dependency"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dependencyPropertyTree!= null):((dependencyPropertyTree == null)||(!dependencyPropertyTree.isLeaf())))) {
                        if (_other.dependency == null) {
                            this.dependency = null;
                        } else {
                            this.dependency = new ArrayList<Dependency.Builder<Dependencies.Builder<_B>>>();
                            for (Dependency _item: _other.dependency) {
                                this.dependency.add(((_item == null)?null:_item.newCopyBuilder(this, dependencyPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Dependencies >_P init(final _P _product) {
            if (this.dependency!= null) {
                final List<Dependency> dependency = new ArrayList<Dependency>(this.dependency.size());
                for (Dependency.Builder<Dependencies.Builder<_B>> _item: this.dependency) {
                    dependency.add(_item.build());
                }
                _product.dependency = dependency;
            }
            return _product;
        }

        /**
         * Adds the given items to the value of "dependency"
         * 
         * @param dependency
         *     Items to add to the value of the "dependency" property
         */
        public Dependencies.Builder<_B> addDependency(final Iterable<? extends Dependency> dependency) {
            if (dependency!= null) {
                if (this.dependency == null) {
                    this.dependency = new ArrayList<Dependency.Builder<Dependencies.Builder<_B>>>();
                }
                for (Dependency _item: dependency) {
                    this.dependency.add(new Dependency.Builder<Dependencies.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "dependency" (any previous value will be replaced)
         * 
         * @param dependency
         *     New value of the "dependency" property.
         */
        public Dependencies.Builder<_B> withDependency(final Iterable<? extends Dependency> dependency) {
            if (this.dependency!= null) {
                this.dependency.clear();
            }
            return addDependency(dependency);
        }

        /**
         * Adds the given items to the value of "dependency"
         * 
         * @param dependency
         *     Items to add to the value of the "dependency" property
         */
        public Dependencies.Builder<_B> addDependency(Dependency... dependency) {
            addDependency(Arrays.asList(dependency));
            return this;
        }

        /**
         * Sets the new value of "dependency" (any previous value will be replaced)
         * 
         * @param dependency
         *     New value of the "dependency" property.
         */
        public Dependencies.Builder<_B> withDependency(Dependency... dependency) {
            withDependency(Arrays.asList(dependency));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Dependency" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Dependency.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Dependency" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Dependency.Builder#end()} to return to the current builder.
         */
        public Dependency.Builder<? extends Dependencies.Builder<_B>> addDependency() {
            if (this.dependency == null) {
                this.dependency = new ArrayList<Dependency.Builder<Dependencies.Builder<_B>>>();
            }
            final Dependency.Builder<Dependencies.Builder<_B>> dependency_Builder = new Dependency.Builder<Dependencies.Builder<_B>>(this, null, false);
            this.dependency.add(dependency_Builder);
            return dependency_Builder;
        }

        @Override
        public Dependencies build() {
            if (_storedValue == null) {
                return this.init(new Dependencies());
            } else {
                return ((Dependencies) _storedValue);
            }
        }

        public Dependencies.Builder<_B> copyOf(final Dependencies _other) {
            _other.copyTo(this);
            return this;
        }

        public Dependencies.Builder<_B> copyOf(final Dependencies.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Dependencies.Selector<Dependencies.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Dependencies.Select _root() {
            return new Dependencies.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private Dependency.Selector<TRoot, Dependencies.Selector<TRoot, TParent>> dependency = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.dependency!= null) {
                products.put("dependency", this.dependency.init());
            }
            return products;
        }

        public Dependency.Selector<TRoot, Dependencies.Selector<TRoot, TParent>> dependency() {
            return ((this.dependency == null)?this.dependency = new Dependency.Selector<TRoot, Dependencies.Selector<TRoot, TParent>>(this._root, this, "dependency"):this.dependency);
        }

    }

}
