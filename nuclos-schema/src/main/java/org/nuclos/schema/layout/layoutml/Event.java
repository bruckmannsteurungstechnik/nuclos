
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="type" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
 *             &lt;enumeration value="lookup"/&gt;
 *             &lt;enumeration value="value-changed"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="entity" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="sourcecomponent" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "event")
public class Event implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "type", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String type;
    @XmlAttribute(name = "entity")
    @XmlSchemaType(name = "anySimpleType")
    protected String entity;
    @XmlAttribute(name = "sourcecomponent", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String sourcecomponent;

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the entity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntity() {
        return entity;
    }

    /**
     * Sets the value of the entity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntity(String value) {
        this.entity = value;
    }

    /**
     * Gets the value of the sourcecomponent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourcecomponent() {
        return sourcecomponent;
    }

    /**
     * Sets the value of the sourcecomponent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourcecomponent(String value) {
        this.sourcecomponent = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theType;
            theType = this.getType();
            strategy.appendField(locator, this, "type", buffer, theType);
        }
        {
            String theEntity;
            theEntity = this.getEntity();
            strategy.appendField(locator, this, "entity", buffer, theEntity);
        }
        {
            String theSourcecomponent;
            theSourcecomponent = this.getSourcecomponent();
            strategy.appendField(locator, this, "sourcecomponent", buffer, theSourcecomponent);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Event)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Event that = ((Event) object);
        {
            String lhsType;
            lhsType = this.getType();
            String rhsType;
            rhsType = that.getType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "type", lhsType), LocatorUtils.property(thatLocator, "type", rhsType), lhsType, rhsType)) {
                return false;
            }
        }
        {
            String lhsEntity;
            lhsEntity = this.getEntity();
            String rhsEntity;
            rhsEntity = that.getEntity();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entity", lhsEntity), LocatorUtils.property(thatLocator, "entity", rhsEntity), lhsEntity, rhsEntity)) {
                return false;
            }
        }
        {
            String lhsSourcecomponent;
            lhsSourcecomponent = this.getSourcecomponent();
            String rhsSourcecomponent;
            rhsSourcecomponent = that.getSourcecomponent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "sourcecomponent", lhsSourcecomponent), LocatorUtils.property(thatLocator, "sourcecomponent", rhsSourcecomponent), lhsSourcecomponent, rhsSourcecomponent)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theType;
            theType = this.getType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "type", theType), currentHashCode, theType);
        }
        {
            String theEntity;
            theEntity = this.getEntity();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entity", theEntity), currentHashCode, theEntity);
        }
        {
            String theSourcecomponent;
            theSourcecomponent = this.getSourcecomponent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "sourcecomponent", theSourcecomponent), currentHashCode, theSourcecomponent);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Event) {
            final Event copy = ((Event) draftCopy);
            if (this.type!= null) {
                String sourceType;
                sourceType = this.getType();
                String copyType = ((String) strategy.copy(LocatorUtils.property(locator, "type", sourceType), sourceType));
                copy.setType(copyType);
            } else {
                copy.type = null;
            }
            if (this.entity!= null) {
                String sourceEntity;
                sourceEntity = this.getEntity();
                String copyEntity = ((String) strategy.copy(LocatorUtils.property(locator, "entity", sourceEntity), sourceEntity));
                copy.setEntity(copyEntity);
            } else {
                copy.entity = null;
            }
            if (this.sourcecomponent!= null) {
                String sourceSourcecomponent;
                sourceSourcecomponent = this.getSourcecomponent();
                String copySourcecomponent = ((String) strategy.copy(LocatorUtils.property(locator, "sourcecomponent", sourceSourcecomponent), sourceSourcecomponent));
                copy.setSourcecomponent(copySourcecomponent);
            } else {
                copy.sourcecomponent = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Event();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Event.Builder<_B> _other) {
        _other.type = this.type;
        _other.entity = this.entity;
        _other.sourcecomponent = this.sourcecomponent;
    }

    public<_B >Event.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Event.Builder<_B>(_parentBuilder, this, true);
    }

    public Event.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Event.Builder<Void> builder() {
        return new Event.Builder<Void>(null, null, false);
    }

    public static<_B >Event.Builder<_B> copyOf(final Event _other) {
        final Event.Builder<_B> _newBuilder = new Event.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Event.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree typePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("type"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(typePropertyTree!= null):((typePropertyTree == null)||(!typePropertyTree.isLeaf())))) {
            _other.type = this.type;
        }
        final PropertyTree entityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entity"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityPropertyTree!= null):((entityPropertyTree == null)||(!entityPropertyTree.isLeaf())))) {
            _other.entity = this.entity;
        }
        final PropertyTree sourcecomponentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("sourcecomponent"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(sourcecomponentPropertyTree!= null):((sourcecomponentPropertyTree == null)||(!sourcecomponentPropertyTree.isLeaf())))) {
            _other.sourcecomponent = this.sourcecomponent;
        }
    }

    public<_B >Event.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Event.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Event.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Event.Builder<_B> copyOf(final Event _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Event.Builder<_B> _newBuilder = new Event.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Event.Builder<Void> copyExcept(final Event _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Event.Builder<Void> copyOnly(final Event _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Event _storedValue;
        private String type;
        private String entity;
        private String sourcecomponent;

        public Builder(final _B _parentBuilder, final Event _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.type = _other.type;
                    this.entity = _other.entity;
                    this.sourcecomponent = _other.sourcecomponent;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Event _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree typePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("type"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(typePropertyTree!= null):((typePropertyTree == null)||(!typePropertyTree.isLeaf())))) {
                        this.type = _other.type;
                    }
                    final PropertyTree entityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entity"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityPropertyTree!= null):((entityPropertyTree == null)||(!entityPropertyTree.isLeaf())))) {
                        this.entity = _other.entity;
                    }
                    final PropertyTree sourcecomponentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("sourcecomponent"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(sourcecomponentPropertyTree!= null):((sourcecomponentPropertyTree == null)||(!sourcecomponentPropertyTree.isLeaf())))) {
                        this.sourcecomponent = _other.sourcecomponent;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Event >_P init(final _P _product) {
            _product.type = this.type;
            _product.entity = this.entity;
            _product.sourcecomponent = this.sourcecomponent;
            return _product;
        }

        /**
         * Sets the new value of "type" (any previous value will be replaced)
         * 
         * @param type
         *     New value of the "type" property.
         */
        public Event.Builder<_B> withType(final String type) {
            this.type = type;
            return this;
        }

        /**
         * Sets the new value of "entity" (any previous value will be replaced)
         * 
         * @param entity
         *     New value of the "entity" property.
         */
        public Event.Builder<_B> withEntity(final String entity) {
            this.entity = entity;
            return this;
        }

        /**
         * Sets the new value of "sourcecomponent" (any previous value will be replaced)
         * 
         * @param sourcecomponent
         *     New value of the "sourcecomponent" property.
         */
        public Event.Builder<_B> withSourcecomponent(final String sourcecomponent) {
            this.sourcecomponent = sourcecomponent;
            return this;
        }

        @Override
        public Event build() {
            if (_storedValue == null) {
                return this.init(new Event());
            } else {
                return ((Event) _storedValue);
            }
        }

        public Event.Builder<_B> copyOf(final Event _other) {
            _other.copyTo(this);
            return this;
        }

        public Event.Builder<_B> copyOf(final Event.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Event.Selector<Event.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Event.Select _root() {
            return new Event.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, Event.Selector<TRoot, TParent>> type = null;
        private com.kscs.util.jaxb.Selector<TRoot, Event.Selector<TRoot, TParent>> entity = null;
        private com.kscs.util.jaxb.Selector<TRoot, Event.Selector<TRoot, TParent>> sourcecomponent = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.type!= null) {
                products.put("type", this.type.init());
            }
            if (this.entity!= null) {
                products.put("entity", this.entity.init());
            }
            if (this.sourcecomponent!= null) {
                products.put("sourcecomponent", this.sourcecomponent.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, Event.Selector<TRoot, TParent>> type() {
            return ((this.type == null)?this.type = new com.kscs.util.jaxb.Selector<TRoot, Event.Selector<TRoot, TParent>>(this._root, this, "type"):this.type);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Event.Selector<TRoot, TParent>> entity() {
            return ((this.entity == null)?this.entity = new com.kscs.util.jaxb.Selector<TRoot, Event.Selector<TRoot, TParent>>(this._root, this, "entity"):this.entity);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Event.Selector<TRoot, TParent>> sourcecomponent() {
            return ((this.sourcecomponent == null)?this.sourcecomponent = new com.kscs.util.jaxb.Selector<TRoot, Event.Selector<TRoot, TParent>>(this._root, this, "sourcecomponent"):this.sourcecomponent);
        }

    }

}
