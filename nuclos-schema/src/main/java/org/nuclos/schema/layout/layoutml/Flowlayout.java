
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="align"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
 *             &lt;enumeration value="left"/&gt;
 *             &lt;enumeration value="right"/&gt;
 *             &lt;enumeration value="leading"/&gt;
 *             &lt;enumeration value="trailing"/&gt;
 *             &lt;enumeration value="center"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="hgap" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="vgap" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class Flowlayout implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "align")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String align;
    @XmlAttribute(name = "hgap")
    @XmlSchemaType(name = "anySimpleType")
    protected String hgap;
    @XmlAttribute(name = "vgap")
    @XmlSchemaType(name = "anySimpleType")
    protected String vgap;

    /**
     * Gets the value of the align property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlign() {
        return align;
    }

    /**
     * Sets the value of the align property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlign(String value) {
        this.align = value;
    }

    /**
     * Gets the value of the hgap property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHgap() {
        return hgap;
    }

    /**
     * Sets the value of the hgap property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHgap(String value) {
        this.hgap = value;
    }

    /**
     * Gets the value of the vgap property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVgap() {
        return vgap;
    }

    /**
     * Sets the value of the vgap property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVgap(String value) {
        this.vgap = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theAlign;
            theAlign = this.getAlign();
            strategy.appendField(locator, this, "align", buffer, theAlign);
        }
        {
            String theHgap;
            theHgap = this.getHgap();
            strategy.appendField(locator, this, "hgap", buffer, theHgap);
        }
        {
            String theVgap;
            theVgap = this.getVgap();
            strategy.appendField(locator, this, "vgap", buffer, theVgap);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Flowlayout)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Flowlayout that = ((Flowlayout) object);
        {
            String lhsAlign;
            lhsAlign = this.getAlign();
            String rhsAlign;
            rhsAlign = that.getAlign();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "align", lhsAlign), LocatorUtils.property(thatLocator, "align", rhsAlign), lhsAlign, rhsAlign)) {
                return false;
            }
        }
        {
            String lhsHgap;
            lhsHgap = this.getHgap();
            String rhsHgap;
            rhsHgap = that.getHgap();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "hgap", lhsHgap), LocatorUtils.property(thatLocator, "hgap", rhsHgap), lhsHgap, rhsHgap)) {
                return false;
            }
        }
        {
            String lhsVgap;
            lhsVgap = this.getVgap();
            String rhsVgap;
            rhsVgap = that.getVgap();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "vgap", lhsVgap), LocatorUtils.property(thatLocator, "vgap", rhsVgap), lhsVgap, rhsVgap)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theAlign;
            theAlign = this.getAlign();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "align", theAlign), currentHashCode, theAlign);
        }
        {
            String theHgap;
            theHgap = this.getHgap();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "hgap", theHgap), currentHashCode, theHgap);
        }
        {
            String theVgap;
            theVgap = this.getVgap();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "vgap", theVgap), currentHashCode, theVgap);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Flowlayout) {
            final Flowlayout copy = ((Flowlayout) draftCopy);
            if (this.align!= null) {
                String sourceAlign;
                sourceAlign = this.getAlign();
                String copyAlign = ((String) strategy.copy(LocatorUtils.property(locator, "align", sourceAlign), sourceAlign));
                copy.setAlign(copyAlign);
            } else {
                copy.align = null;
            }
            if (this.hgap!= null) {
                String sourceHgap;
                sourceHgap = this.getHgap();
                String copyHgap = ((String) strategy.copy(LocatorUtils.property(locator, "hgap", sourceHgap), sourceHgap));
                copy.setHgap(copyHgap);
            } else {
                copy.hgap = null;
            }
            if (this.vgap!= null) {
                String sourceVgap;
                sourceVgap = this.getVgap();
                String copyVgap = ((String) strategy.copy(LocatorUtils.property(locator, "vgap", sourceVgap), sourceVgap));
                copy.setVgap(copyVgap);
            } else {
                copy.vgap = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Flowlayout();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Flowlayout.Builder<_B> _other) {
        _other.align = this.align;
        _other.hgap = this.hgap;
        _other.vgap = this.vgap;
    }

    public<_B >Flowlayout.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Flowlayout.Builder<_B>(_parentBuilder, this, true);
    }

    public Flowlayout.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Flowlayout.Builder<Void> builder() {
        return new Flowlayout.Builder<Void>(null, null, false);
    }

    public static<_B >Flowlayout.Builder<_B> copyOf(final Flowlayout _other) {
        final Flowlayout.Builder<_B> _newBuilder = new Flowlayout.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Flowlayout.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree alignPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("align"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(alignPropertyTree!= null):((alignPropertyTree == null)||(!alignPropertyTree.isLeaf())))) {
            _other.align = this.align;
        }
        final PropertyTree hgapPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("hgap"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(hgapPropertyTree!= null):((hgapPropertyTree == null)||(!hgapPropertyTree.isLeaf())))) {
            _other.hgap = this.hgap;
        }
        final PropertyTree vgapPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("vgap"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(vgapPropertyTree!= null):((vgapPropertyTree == null)||(!vgapPropertyTree.isLeaf())))) {
            _other.vgap = this.vgap;
        }
    }

    public<_B >Flowlayout.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Flowlayout.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Flowlayout.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Flowlayout.Builder<_B> copyOf(final Flowlayout _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Flowlayout.Builder<_B> _newBuilder = new Flowlayout.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Flowlayout.Builder<Void> copyExcept(final Flowlayout _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Flowlayout.Builder<Void> copyOnly(final Flowlayout _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Flowlayout _storedValue;
        private String align;
        private String hgap;
        private String vgap;

        public Builder(final _B _parentBuilder, final Flowlayout _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.align = _other.align;
                    this.hgap = _other.hgap;
                    this.vgap = _other.vgap;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Flowlayout _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree alignPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("align"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(alignPropertyTree!= null):((alignPropertyTree == null)||(!alignPropertyTree.isLeaf())))) {
                        this.align = _other.align;
                    }
                    final PropertyTree hgapPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("hgap"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(hgapPropertyTree!= null):((hgapPropertyTree == null)||(!hgapPropertyTree.isLeaf())))) {
                        this.hgap = _other.hgap;
                    }
                    final PropertyTree vgapPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("vgap"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(vgapPropertyTree!= null):((vgapPropertyTree == null)||(!vgapPropertyTree.isLeaf())))) {
                        this.vgap = _other.vgap;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Flowlayout >_P init(final _P _product) {
            _product.align = this.align;
            _product.hgap = this.hgap;
            _product.vgap = this.vgap;
            return _product;
        }

        /**
         * Sets the new value of "align" (any previous value will be replaced)
         * 
         * @param align
         *     New value of the "align" property.
         */
        public Flowlayout.Builder<_B> withAlign(final String align) {
            this.align = align;
            return this;
        }

        /**
         * Sets the new value of "hgap" (any previous value will be replaced)
         * 
         * @param hgap
         *     New value of the "hgap" property.
         */
        public Flowlayout.Builder<_B> withHgap(final String hgap) {
            this.hgap = hgap;
            return this;
        }

        /**
         * Sets the new value of "vgap" (any previous value will be replaced)
         * 
         * @param vgap
         *     New value of the "vgap" property.
         */
        public Flowlayout.Builder<_B> withVgap(final String vgap) {
            this.vgap = vgap;
            return this;
        }

        @Override
        public Flowlayout build() {
            if (_storedValue == null) {
                return this.init(new Flowlayout());
            } else {
                return ((Flowlayout) _storedValue);
            }
        }

        public Flowlayout.Builder<_B> copyOf(final Flowlayout _other) {
            _other.copyTo(this);
            return this;
        }

        public Flowlayout.Builder<_B> copyOf(final Flowlayout.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Flowlayout.Selector<Flowlayout.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Flowlayout.Select _root() {
            return new Flowlayout.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, Flowlayout.Selector<TRoot, TParent>> align = null;
        private com.kscs.util.jaxb.Selector<TRoot, Flowlayout.Selector<TRoot, TParent>> hgap = null;
        private com.kscs.util.jaxb.Selector<TRoot, Flowlayout.Selector<TRoot, TParent>> vgap = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.align!= null) {
                products.put("align", this.align.init());
            }
            if (this.hgap!= null) {
                products.put("hgap", this.hgap.init());
            }
            if (this.vgap!= null) {
                products.put("vgap", this.vgap.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, Flowlayout.Selector<TRoot, TParent>> align() {
            return ((this.align == null)?this.align = new com.kscs.util.jaxb.Selector<TRoot, Flowlayout.Selector<TRoot, TParent>>(this._root, this, "align"):this.align);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Flowlayout.Selector<TRoot, TParent>> hgap() {
            return ((this.hgap == null)?this.hgap = new com.kscs.util.jaxb.Selector<TRoot, Flowlayout.Selector<TRoot, TParent>>(this._root, this, "hgap"):this.hgap);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Flowlayout.Selector<TRoot, TParent>> vgap() {
            return ((this.vgap == null)?this.vgap = new com.kscs.util.jaxb.Selector<TRoot, Flowlayout.Selector<TRoot, TParent>>(this._root, this, "vgap"):this.vgap);
        }

    }

}
