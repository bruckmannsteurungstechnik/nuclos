
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="gridx" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="gridy" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="gridwidth" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="gridheight" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="weightx" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="weighty" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="anchor"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
 *             &lt;enumeration value="north"/&gt;
 *             &lt;enumeration value="northeast"/&gt;
 *             &lt;enumeration value="east"/&gt;
 *             &lt;enumeration value="southeast"/&gt;
 *             &lt;enumeration value="south"/&gt;
 *             &lt;enumeration value="southwest"/&gt;
 *             &lt;enumeration value="west"/&gt;
 *             &lt;enumeration value="northwest"/&gt;
 *             &lt;enumeration value="center"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="fill"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
 *             &lt;enumeration value="none"/&gt;
 *             &lt;enumeration value="horizontal"/&gt;
 *             &lt;enumeration value="vertical"/&gt;
 *             &lt;enumeration value="both"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="insettop" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="insetleft" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="insetbottom" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="insetright" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="ipadx" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="ipady" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class GridbagConstraints implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "gridx")
    @XmlSchemaType(name = "anySimpleType")
    protected String gridx;
    @XmlAttribute(name = "gridy")
    @XmlSchemaType(name = "anySimpleType")
    protected String gridy;
    @XmlAttribute(name = "gridwidth")
    @XmlSchemaType(name = "anySimpleType")
    protected String gridwidth;
    @XmlAttribute(name = "gridheight")
    @XmlSchemaType(name = "anySimpleType")
    protected String gridheight;
    @XmlAttribute(name = "weightx")
    @XmlSchemaType(name = "anySimpleType")
    protected String weightx;
    @XmlAttribute(name = "weighty")
    @XmlSchemaType(name = "anySimpleType")
    protected String weighty;
    @XmlAttribute(name = "anchor")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String anchor;
    @XmlAttribute(name = "fill")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String fill;
    @XmlAttribute(name = "insettop")
    @XmlSchemaType(name = "anySimpleType")
    protected String insettop;
    @XmlAttribute(name = "insetleft")
    @XmlSchemaType(name = "anySimpleType")
    protected String insetleft;
    @XmlAttribute(name = "insetbottom")
    @XmlSchemaType(name = "anySimpleType")
    protected String insetbottom;
    @XmlAttribute(name = "insetright")
    @XmlSchemaType(name = "anySimpleType")
    protected String insetright;
    @XmlAttribute(name = "ipadx")
    @XmlSchemaType(name = "anySimpleType")
    protected String ipadx;
    @XmlAttribute(name = "ipady")
    @XmlSchemaType(name = "anySimpleType")
    protected String ipady;

    /**
     * Gets the value of the gridx property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGridx() {
        return gridx;
    }

    /**
     * Sets the value of the gridx property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGridx(String value) {
        this.gridx = value;
    }

    /**
     * Gets the value of the gridy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGridy() {
        return gridy;
    }

    /**
     * Sets the value of the gridy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGridy(String value) {
        this.gridy = value;
    }

    /**
     * Gets the value of the gridwidth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGridwidth() {
        return gridwidth;
    }

    /**
     * Sets the value of the gridwidth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGridwidth(String value) {
        this.gridwidth = value;
    }

    /**
     * Gets the value of the gridheight property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGridheight() {
        return gridheight;
    }

    /**
     * Sets the value of the gridheight property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGridheight(String value) {
        this.gridheight = value;
    }

    /**
     * Gets the value of the weightx property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeightx() {
        return weightx;
    }

    /**
     * Sets the value of the weightx property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeightx(String value) {
        this.weightx = value;
    }

    /**
     * Gets the value of the weighty property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeighty() {
        return weighty;
    }

    /**
     * Sets the value of the weighty property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeighty(String value) {
        this.weighty = value;
    }

    /**
     * Gets the value of the anchor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnchor() {
        return anchor;
    }

    /**
     * Sets the value of the anchor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnchor(String value) {
        this.anchor = value;
    }

    /**
     * Gets the value of the fill property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFill() {
        return fill;
    }

    /**
     * Sets the value of the fill property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFill(String value) {
        this.fill = value;
    }

    /**
     * Gets the value of the insettop property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsettop() {
        return insettop;
    }

    /**
     * Sets the value of the insettop property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsettop(String value) {
        this.insettop = value;
    }

    /**
     * Gets the value of the insetleft property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsetleft() {
        return insetleft;
    }

    /**
     * Sets the value of the insetleft property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsetleft(String value) {
        this.insetleft = value;
    }

    /**
     * Gets the value of the insetbottom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsetbottom() {
        return insetbottom;
    }

    /**
     * Sets the value of the insetbottom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsetbottom(String value) {
        this.insetbottom = value;
    }

    /**
     * Gets the value of the insetright property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsetright() {
        return insetright;
    }

    /**
     * Sets the value of the insetright property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsetright(String value) {
        this.insetright = value;
    }

    /**
     * Gets the value of the ipadx property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIpadx() {
        return ipadx;
    }

    /**
     * Sets the value of the ipadx property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIpadx(String value) {
        this.ipadx = value;
    }

    /**
     * Gets the value of the ipady property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIpady() {
        return ipady;
    }

    /**
     * Sets the value of the ipady property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIpady(String value) {
        this.ipady = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theGridx;
            theGridx = this.getGridx();
            strategy.appendField(locator, this, "gridx", buffer, theGridx);
        }
        {
            String theGridy;
            theGridy = this.getGridy();
            strategy.appendField(locator, this, "gridy", buffer, theGridy);
        }
        {
            String theGridwidth;
            theGridwidth = this.getGridwidth();
            strategy.appendField(locator, this, "gridwidth", buffer, theGridwidth);
        }
        {
            String theGridheight;
            theGridheight = this.getGridheight();
            strategy.appendField(locator, this, "gridheight", buffer, theGridheight);
        }
        {
            String theWeightx;
            theWeightx = this.getWeightx();
            strategy.appendField(locator, this, "weightx", buffer, theWeightx);
        }
        {
            String theWeighty;
            theWeighty = this.getWeighty();
            strategy.appendField(locator, this, "weighty", buffer, theWeighty);
        }
        {
            String theAnchor;
            theAnchor = this.getAnchor();
            strategy.appendField(locator, this, "anchor", buffer, theAnchor);
        }
        {
            String theFill;
            theFill = this.getFill();
            strategy.appendField(locator, this, "fill", buffer, theFill);
        }
        {
            String theInsettop;
            theInsettop = this.getInsettop();
            strategy.appendField(locator, this, "insettop", buffer, theInsettop);
        }
        {
            String theInsetleft;
            theInsetleft = this.getInsetleft();
            strategy.appendField(locator, this, "insetleft", buffer, theInsetleft);
        }
        {
            String theInsetbottom;
            theInsetbottom = this.getInsetbottom();
            strategy.appendField(locator, this, "insetbottom", buffer, theInsetbottom);
        }
        {
            String theInsetright;
            theInsetright = this.getInsetright();
            strategy.appendField(locator, this, "insetright", buffer, theInsetright);
        }
        {
            String theIpadx;
            theIpadx = this.getIpadx();
            strategy.appendField(locator, this, "ipadx", buffer, theIpadx);
        }
        {
            String theIpady;
            theIpady = this.getIpady();
            strategy.appendField(locator, this, "ipady", buffer, theIpady);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof GridbagConstraints)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final GridbagConstraints that = ((GridbagConstraints) object);
        {
            String lhsGridx;
            lhsGridx = this.getGridx();
            String rhsGridx;
            rhsGridx = that.getGridx();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "gridx", lhsGridx), LocatorUtils.property(thatLocator, "gridx", rhsGridx), lhsGridx, rhsGridx)) {
                return false;
            }
        }
        {
            String lhsGridy;
            lhsGridy = this.getGridy();
            String rhsGridy;
            rhsGridy = that.getGridy();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "gridy", lhsGridy), LocatorUtils.property(thatLocator, "gridy", rhsGridy), lhsGridy, rhsGridy)) {
                return false;
            }
        }
        {
            String lhsGridwidth;
            lhsGridwidth = this.getGridwidth();
            String rhsGridwidth;
            rhsGridwidth = that.getGridwidth();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "gridwidth", lhsGridwidth), LocatorUtils.property(thatLocator, "gridwidth", rhsGridwidth), lhsGridwidth, rhsGridwidth)) {
                return false;
            }
        }
        {
            String lhsGridheight;
            lhsGridheight = this.getGridheight();
            String rhsGridheight;
            rhsGridheight = that.getGridheight();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "gridheight", lhsGridheight), LocatorUtils.property(thatLocator, "gridheight", rhsGridheight), lhsGridheight, rhsGridheight)) {
                return false;
            }
        }
        {
            String lhsWeightx;
            lhsWeightx = this.getWeightx();
            String rhsWeightx;
            rhsWeightx = that.getWeightx();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "weightx", lhsWeightx), LocatorUtils.property(thatLocator, "weightx", rhsWeightx), lhsWeightx, rhsWeightx)) {
                return false;
            }
        }
        {
            String lhsWeighty;
            lhsWeighty = this.getWeighty();
            String rhsWeighty;
            rhsWeighty = that.getWeighty();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "weighty", lhsWeighty), LocatorUtils.property(thatLocator, "weighty", rhsWeighty), lhsWeighty, rhsWeighty)) {
                return false;
            }
        }
        {
            String lhsAnchor;
            lhsAnchor = this.getAnchor();
            String rhsAnchor;
            rhsAnchor = that.getAnchor();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "anchor", lhsAnchor), LocatorUtils.property(thatLocator, "anchor", rhsAnchor), lhsAnchor, rhsAnchor)) {
                return false;
            }
        }
        {
            String lhsFill;
            lhsFill = this.getFill();
            String rhsFill;
            rhsFill = that.getFill();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fill", lhsFill), LocatorUtils.property(thatLocator, "fill", rhsFill), lhsFill, rhsFill)) {
                return false;
            }
        }
        {
            String lhsInsettop;
            lhsInsettop = this.getInsettop();
            String rhsInsettop;
            rhsInsettop = that.getInsettop();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "insettop", lhsInsettop), LocatorUtils.property(thatLocator, "insettop", rhsInsettop), lhsInsettop, rhsInsettop)) {
                return false;
            }
        }
        {
            String lhsInsetleft;
            lhsInsetleft = this.getInsetleft();
            String rhsInsetleft;
            rhsInsetleft = that.getInsetleft();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "insetleft", lhsInsetleft), LocatorUtils.property(thatLocator, "insetleft", rhsInsetleft), lhsInsetleft, rhsInsetleft)) {
                return false;
            }
        }
        {
            String lhsInsetbottom;
            lhsInsetbottom = this.getInsetbottom();
            String rhsInsetbottom;
            rhsInsetbottom = that.getInsetbottom();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "insetbottom", lhsInsetbottom), LocatorUtils.property(thatLocator, "insetbottom", rhsInsetbottom), lhsInsetbottom, rhsInsetbottom)) {
                return false;
            }
        }
        {
            String lhsInsetright;
            lhsInsetright = this.getInsetright();
            String rhsInsetright;
            rhsInsetright = that.getInsetright();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "insetright", lhsInsetright), LocatorUtils.property(thatLocator, "insetright", rhsInsetright), lhsInsetright, rhsInsetright)) {
                return false;
            }
        }
        {
            String lhsIpadx;
            lhsIpadx = this.getIpadx();
            String rhsIpadx;
            rhsIpadx = that.getIpadx();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ipadx", lhsIpadx), LocatorUtils.property(thatLocator, "ipadx", rhsIpadx), lhsIpadx, rhsIpadx)) {
                return false;
            }
        }
        {
            String lhsIpady;
            lhsIpady = this.getIpady();
            String rhsIpady;
            rhsIpady = that.getIpady();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ipady", lhsIpady), LocatorUtils.property(thatLocator, "ipady", rhsIpady), lhsIpady, rhsIpady)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theGridx;
            theGridx = this.getGridx();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "gridx", theGridx), currentHashCode, theGridx);
        }
        {
            String theGridy;
            theGridy = this.getGridy();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "gridy", theGridy), currentHashCode, theGridy);
        }
        {
            String theGridwidth;
            theGridwidth = this.getGridwidth();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "gridwidth", theGridwidth), currentHashCode, theGridwidth);
        }
        {
            String theGridheight;
            theGridheight = this.getGridheight();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "gridheight", theGridheight), currentHashCode, theGridheight);
        }
        {
            String theWeightx;
            theWeightx = this.getWeightx();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "weightx", theWeightx), currentHashCode, theWeightx);
        }
        {
            String theWeighty;
            theWeighty = this.getWeighty();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "weighty", theWeighty), currentHashCode, theWeighty);
        }
        {
            String theAnchor;
            theAnchor = this.getAnchor();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "anchor", theAnchor), currentHashCode, theAnchor);
        }
        {
            String theFill;
            theFill = this.getFill();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fill", theFill), currentHashCode, theFill);
        }
        {
            String theInsettop;
            theInsettop = this.getInsettop();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "insettop", theInsettop), currentHashCode, theInsettop);
        }
        {
            String theInsetleft;
            theInsetleft = this.getInsetleft();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "insetleft", theInsetleft), currentHashCode, theInsetleft);
        }
        {
            String theInsetbottom;
            theInsetbottom = this.getInsetbottom();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "insetbottom", theInsetbottom), currentHashCode, theInsetbottom);
        }
        {
            String theInsetright;
            theInsetright = this.getInsetright();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "insetright", theInsetright), currentHashCode, theInsetright);
        }
        {
            String theIpadx;
            theIpadx = this.getIpadx();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ipadx", theIpadx), currentHashCode, theIpadx);
        }
        {
            String theIpady;
            theIpady = this.getIpady();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ipady", theIpady), currentHashCode, theIpady);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof GridbagConstraints) {
            final GridbagConstraints copy = ((GridbagConstraints) draftCopy);
            if (this.gridx!= null) {
                String sourceGridx;
                sourceGridx = this.getGridx();
                String copyGridx = ((String) strategy.copy(LocatorUtils.property(locator, "gridx", sourceGridx), sourceGridx));
                copy.setGridx(copyGridx);
            } else {
                copy.gridx = null;
            }
            if (this.gridy!= null) {
                String sourceGridy;
                sourceGridy = this.getGridy();
                String copyGridy = ((String) strategy.copy(LocatorUtils.property(locator, "gridy", sourceGridy), sourceGridy));
                copy.setGridy(copyGridy);
            } else {
                copy.gridy = null;
            }
            if (this.gridwidth!= null) {
                String sourceGridwidth;
                sourceGridwidth = this.getGridwidth();
                String copyGridwidth = ((String) strategy.copy(LocatorUtils.property(locator, "gridwidth", sourceGridwidth), sourceGridwidth));
                copy.setGridwidth(copyGridwidth);
            } else {
                copy.gridwidth = null;
            }
            if (this.gridheight!= null) {
                String sourceGridheight;
                sourceGridheight = this.getGridheight();
                String copyGridheight = ((String) strategy.copy(LocatorUtils.property(locator, "gridheight", sourceGridheight), sourceGridheight));
                copy.setGridheight(copyGridheight);
            } else {
                copy.gridheight = null;
            }
            if (this.weightx!= null) {
                String sourceWeightx;
                sourceWeightx = this.getWeightx();
                String copyWeightx = ((String) strategy.copy(LocatorUtils.property(locator, "weightx", sourceWeightx), sourceWeightx));
                copy.setWeightx(copyWeightx);
            } else {
                copy.weightx = null;
            }
            if (this.weighty!= null) {
                String sourceWeighty;
                sourceWeighty = this.getWeighty();
                String copyWeighty = ((String) strategy.copy(LocatorUtils.property(locator, "weighty", sourceWeighty), sourceWeighty));
                copy.setWeighty(copyWeighty);
            } else {
                copy.weighty = null;
            }
            if (this.anchor!= null) {
                String sourceAnchor;
                sourceAnchor = this.getAnchor();
                String copyAnchor = ((String) strategy.copy(LocatorUtils.property(locator, "anchor", sourceAnchor), sourceAnchor));
                copy.setAnchor(copyAnchor);
            } else {
                copy.anchor = null;
            }
            if (this.fill!= null) {
                String sourceFill;
                sourceFill = this.getFill();
                String copyFill = ((String) strategy.copy(LocatorUtils.property(locator, "fill", sourceFill), sourceFill));
                copy.setFill(copyFill);
            } else {
                copy.fill = null;
            }
            if (this.insettop!= null) {
                String sourceInsettop;
                sourceInsettop = this.getInsettop();
                String copyInsettop = ((String) strategy.copy(LocatorUtils.property(locator, "insettop", sourceInsettop), sourceInsettop));
                copy.setInsettop(copyInsettop);
            } else {
                copy.insettop = null;
            }
            if (this.insetleft!= null) {
                String sourceInsetleft;
                sourceInsetleft = this.getInsetleft();
                String copyInsetleft = ((String) strategy.copy(LocatorUtils.property(locator, "insetleft", sourceInsetleft), sourceInsetleft));
                copy.setInsetleft(copyInsetleft);
            } else {
                copy.insetleft = null;
            }
            if (this.insetbottom!= null) {
                String sourceInsetbottom;
                sourceInsetbottom = this.getInsetbottom();
                String copyInsetbottom = ((String) strategy.copy(LocatorUtils.property(locator, "insetbottom", sourceInsetbottom), sourceInsetbottom));
                copy.setInsetbottom(copyInsetbottom);
            } else {
                copy.insetbottom = null;
            }
            if (this.insetright!= null) {
                String sourceInsetright;
                sourceInsetright = this.getInsetright();
                String copyInsetright = ((String) strategy.copy(LocatorUtils.property(locator, "insetright", sourceInsetright), sourceInsetright));
                copy.setInsetright(copyInsetright);
            } else {
                copy.insetright = null;
            }
            if (this.ipadx!= null) {
                String sourceIpadx;
                sourceIpadx = this.getIpadx();
                String copyIpadx = ((String) strategy.copy(LocatorUtils.property(locator, "ipadx", sourceIpadx), sourceIpadx));
                copy.setIpadx(copyIpadx);
            } else {
                copy.ipadx = null;
            }
            if (this.ipady!= null) {
                String sourceIpady;
                sourceIpady = this.getIpady();
                String copyIpady = ((String) strategy.copy(LocatorUtils.property(locator, "ipady", sourceIpady), sourceIpady));
                copy.setIpady(copyIpady);
            } else {
                copy.ipady = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new GridbagConstraints();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final GridbagConstraints.Builder<_B> _other) {
        _other.gridx = this.gridx;
        _other.gridy = this.gridy;
        _other.gridwidth = this.gridwidth;
        _other.gridheight = this.gridheight;
        _other.weightx = this.weightx;
        _other.weighty = this.weighty;
        _other.anchor = this.anchor;
        _other.fill = this.fill;
        _other.insettop = this.insettop;
        _other.insetleft = this.insetleft;
        _other.insetbottom = this.insetbottom;
        _other.insetright = this.insetright;
        _other.ipadx = this.ipadx;
        _other.ipady = this.ipady;
    }

    public<_B >GridbagConstraints.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new GridbagConstraints.Builder<_B>(_parentBuilder, this, true);
    }

    public GridbagConstraints.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static GridbagConstraints.Builder<Void> builder() {
        return new GridbagConstraints.Builder<Void>(null, null, false);
    }

    public static<_B >GridbagConstraints.Builder<_B> copyOf(final GridbagConstraints _other) {
        final GridbagConstraints.Builder<_B> _newBuilder = new GridbagConstraints.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final GridbagConstraints.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree gridxPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("gridx"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(gridxPropertyTree!= null):((gridxPropertyTree == null)||(!gridxPropertyTree.isLeaf())))) {
            _other.gridx = this.gridx;
        }
        final PropertyTree gridyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("gridy"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(gridyPropertyTree!= null):((gridyPropertyTree == null)||(!gridyPropertyTree.isLeaf())))) {
            _other.gridy = this.gridy;
        }
        final PropertyTree gridwidthPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("gridwidth"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(gridwidthPropertyTree!= null):((gridwidthPropertyTree == null)||(!gridwidthPropertyTree.isLeaf())))) {
            _other.gridwidth = this.gridwidth;
        }
        final PropertyTree gridheightPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("gridheight"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(gridheightPropertyTree!= null):((gridheightPropertyTree == null)||(!gridheightPropertyTree.isLeaf())))) {
            _other.gridheight = this.gridheight;
        }
        final PropertyTree weightxPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("weightx"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(weightxPropertyTree!= null):((weightxPropertyTree == null)||(!weightxPropertyTree.isLeaf())))) {
            _other.weightx = this.weightx;
        }
        final PropertyTree weightyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("weighty"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(weightyPropertyTree!= null):((weightyPropertyTree == null)||(!weightyPropertyTree.isLeaf())))) {
            _other.weighty = this.weighty;
        }
        final PropertyTree anchorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("anchor"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(anchorPropertyTree!= null):((anchorPropertyTree == null)||(!anchorPropertyTree.isLeaf())))) {
            _other.anchor = this.anchor;
        }
        final PropertyTree fillPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fill"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fillPropertyTree!= null):((fillPropertyTree == null)||(!fillPropertyTree.isLeaf())))) {
            _other.fill = this.fill;
        }
        final PropertyTree insettopPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("insettop"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(insettopPropertyTree!= null):((insettopPropertyTree == null)||(!insettopPropertyTree.isLeaf())))) {
            _other.insettop = this.insettop;
        }
        final PropertyTree insetleftPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("insetleft"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(insetleftPropertyTree!= null):((insetleftPropertyTree == null)||(!insetleftPropertyTree.isLeaf())))) {
            _other.insetleft = this.insetleft;
        }
        final PropertyTree insetbottomPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("insetbottom"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(insetbottomPropertyTree!= null):((insetbottomPropertyTree == null)||(!insetbottomPropertyTree.isLeaf())))) {
            _other.insetbottom = this.insetbottom;
        }
        final PropertyTree insetrightPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("insetright"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(insetrightPropertyTree!= null):((insetrightPropertyTree == null)||(!insetrightPropertyTree.isLeaf())))) {
            _other.insetright = this.insetright;
        }
        final PropertyTree ipadxPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("ipadx"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(ipadxPropertyTree!= null):((ipadxPropertyTree == null)||(!ipadxPropertyTree.isLeaf())))) {
            _other.ipadx = this.ipadx;
        }
        final PropertyTree ipadyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("ipady"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(ipadyPropertyTree!= null):((ipadyPropertyTree == null)||(!ipadyPropertyTree.isLeaf())))) {
            _other.ipady = this.ipady;
        }
    }

    public<_B >GridbagConstraints.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new GridbagConstraints.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public GridbagConstraints.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >GridbagConstraints.Builder<_B> copyOf(final GridbagConstraints _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final GridbagConstraints.Builder<_B> _newBuilder = new GridbagConstraints.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static GridbagConstraints.Builder<Void> copyExcept(final GridbagConstraints _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static GridbagConstraints.Builder<Void> copyOnly(final GridbagConstraints _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final GridbagConstraints _storedValue;
        private String gridx;
        private String gridy;
        private String gridwidth;
        private String gridheight;
        private String weightx;
        private String weighty;
        private String anchor;
        private String fill;
        private String insettop;
        private String insetleft;
        private String insetbottom;
        private String insetright;
        private String ipadx;
        private String ipady;

        public Builder(final _B _parentBuilder, final GridbagConstraints _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.gridx = _other.gridx;
                    this.gridy = _other.gridy;
                    this.gridwidth = _other.gridwidth;
                    this.gridheight = _other.gridheight;
                    this.weightx = _other.weightx;
                    this.weighty = _other.weighty;
                    this.anchor = _other.anchor;
                    this.fill = _other.fill;
                    this.insettop = _other.insettop;
                    this.insetleft = _other.insetleft;
                    this.insetbottom = _other.insetbottom;
                    this.insetright = _other.insetright;
                    this.ipadx = _other.ipadx;
                    this.ipady = _other.ipady;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final GridbagConstraints _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree gridxPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("gridx"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(gridxPropertyTree!= null):((gridxPropertyTree == null)||(!gridxPropertyTree.isLeaf())))) {
                        this.gridx = _other.gridx;
                    }
                    final PropertyTree gridyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("gridy"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(gridyPropertyTree!= null):((gridyPropertyTree == null)||(!gridyPropertyTree.isLeaf())))) {
                        this.gridy = _other.gridy;
                    }
                    final PropertyTree gridwidthPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("gridwidth"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(gridwidthPropertyTree!= null):((gridwidthPropertyTree == null)||(!gridwidthPropertyTree.isLeaf())))) {
                        this.gridwidth = _other.gridwidth;
                    }
                    final PropertyTree gridheightPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("gridheight"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(gridheightPropertyTree!= null):((gridheightPropertyTree == null)||(!gridheightPropertyTree.isLeaf())))) {
                        this.gridheight = _other.gridheight;
                    }
                    final PropertyTree weightxPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("weightx"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(weightxPropertyTree!= null):((weightxPropertyTree == null)||(!weightxPropertyTree.isLeaf())))) {
                        this.weightx = _other.weightx;
                    }
                    final PropertyTree weightyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("weighty"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(weightyPropertyTree!= null):((weightyPropertyTree == null)||(!weightyPropertyTree.isLeaf())))) {
                        this.weighty = _other.weighty;
                    }
                    final PropertyTree anchorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("anchor"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(anchorPropertyTree!= null):((anchorPropertyTree == null)||(!anchorPropertyTree.isLeaf())))) {
                        this.anchor = _other.anchor;
                    }
                    final PropertyTree fillPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fill"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fillPropertyTree!= null):((fillPropertyTree == null)||(!fillPropertyTree.isLeaf())))) {
                        this.fill = _other.fill;
                    }
                    final PropertyTree insettopPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("insettop"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(insettopPropertyTree!= null):((insettopPropertyTree == null)||(!insettopPropertyTree.isLeaf())))) {
                        this.insettop = _other.insettop;
                    }
                    final PropertyTree insetleftPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("insetleft"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(insetleftPropertyTree!= null):((insetleftPropertyTree == null)||(!insetleftPropertyTree.isLeaf())))) {
                        this.insetleft = _other.insetleft;
                    }
                    final PropertyTree insetbottomPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("insetbottom"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(insetbottomPropertyTree!= null):((insetbottomPropertyTree == null)||(!insetbottomPropertyTree.isLeaf())))) {
                        this.insetbottom = _other.insetbottom;
                    }
                    final PropertyTree insetrightPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("insetright"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(insetrightPropertyTree!= null):((insetrightPropertyTree == null)||(!insetrightPropertyTree.isLeaf())))) {
                        this.insetright = _other.insetright;
                    }
                    final PropertyTree ipadxPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("ipadx"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(ipadxPropertyTree!= null):((ipadxPropertyTree == null)||(!ipadxPropertyTree.isLeaf())))) {
                        this.ipadx = _other.ipadx;
                    }
                    final PropertyTree ipadyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("ipady"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(ipadyPropertyTree!= null):((ipadyPropertyTree == null)||(!ipadyPropertyTree.isLeaf())))) {
                        this.ipady = _other.ipady;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends GridbagConstraints >_P init(final _P _product) {
            _product.gridx = this.gridx;
            _product.gridy = this.gridy;
            _product.gridwidth = this.gridwidth;
            _product.gridheight = this.gridheight;
            _product.weightx = this.weightx;
            _product.weighty = this.weighty;
            _product.anchor = this.anchor;
            _product.fill = this.fill;
            _product.insettop = this.insettop;
            _product.insetleft = this.insetleft;
            _product.insetbottom = this.insetbottom;
            _product.insetright = this.insetright;
            _product.ipadx = this.ipadx;
            _product.ipady = this.ipady;
            return _product;
        }

        /**
         * Sets the new value of "gridx" (any previous value will be replaced)
         * 
         * @param gridx
         *     New value of the "gridx" property.
         */
        public GridbagConstraints.Builder<_B> withGridx(final String gridx) {
            this.gridx = gridx;
            return this;
        }

        /**
         * Sets the new value of "gridy" (any previous value will be replaced)
         * 
         * @param gridy
         *     New value of the "gridy" property.
         */
        public GridbagConstraints.Builder<_B> withGridy(final String gridy) {
            this.gridy = gridy;
            return this;
        }

        /**
         * Sets the new value of "gridwidth" (any previous value will be replaced)
         * 
         * @param gridwidth
         *     New value of the "gridwidth" property.
         */
        public GridbagConstraints.Builder<_B> withGridwidth(final String gridwidth) {
            this.gridwidth = gridwidth;
            return this;
        }

        /**
         * Sets the new value of "gridheight" (any previous value will be replaced)
         * 
         * @param gridheight
         *     New value of the "gridheight" property.
         */
        public GridbagConstraints.Builder<_B> withGridheight(final String gridheight) {
            this.gridheight = gridheight;
            return this;
        }

        /**
         * Sets the new value of "weightx" (any previous value will be replaced)
         * 
         * @param weightx
         *     New value of the "weightx" property.
         */
        public GridbagConstraints.Builder<_B> withWeightx(final String weightx) {
            this.weightx = weightx;
            return this;
        }

        /**
         * Sets the new value of "weighty" (any previous value will be replaced)
         * 
         * @param weighty
         *     New value of the "weighty" property.
         */
        public GridbagConstraints.Builder<_B> withWeighty(final String weighty) {
            this.weighty = weighty;
            return this;
        }

        /**
         * Sets the new value of "anchor" (any previous value will be replaced)
         * 
         * @param anchor
         *     New value of the "anchor" property.
         */
        public GridbagConstraints.Builder<_B> withAnchor(final String anchor) {
            this.anchor = anchor;
            return this;
        }

        /**
         * Sets the new value of "fill" (any previous value will be replaced)
         * 
         * @param fill
         *     New value of the "fill" property.
         */
        public GridbagConstraints.Builder<_B> withFill(final String fill) {
            this.fill = fill;
            return this;
        }

        /**
         * Sets the new value of "insettop" (any previous value will be replaced)
         * 
         * @param insettop
         *     New value of the "insettop" property.
         */
        public GridbagConstraints.Builder<_B> withInsettop(final String insettop) {
            this.insettop = insettop;
            return this;
        }

        /**
         * Sets the new value of "insetleft" (any previous value will be replaced)
         * 
         * @param insetleft
         *     New value of the "insetleft" property.
         */
        public GridbagConstraints.Builder<_B> withInsetleft(final String insetleft) {
            this.insetleft = insetleft;
            return this;
        }

        /**
         * Sets the new value of "insetbottom" (any previous value will be replaced)
         * 
         * @param insetbottom
         *     New value of the "insetbottom" property.
         */
        public GridbagConstraints.Builder<_B> withInsetbottom(final String insetbottom) {
            this.insetbottom = insetbottom;
            return this;
        }

        /**
         * Sets the new value of "insetright" (any previous value will be replaced)
         * 
         * @param insetright
         *     New value of the "insetright" property.
         */
        public GridbagConstraints.Builder<_B> withInsetright(final String insetright) {
            this.insetright = insetright;
            return this;
        }

        /**
         * Sets the new value of "ipadx" (any previous value will be replaced)
         * 
         * @param ipadx
         *     New value of the "ipadx" property.
         */
        public GridbagConstraints.Builder<_B> withIpadx(final String ipadx) {
            this.ipadx = ipadx;
            return this;
        }

        /**
         * Sets the new value of "ipady" (any previous value will be replaced)
         * 
         * @param ipady
         *     New value of the "ipady" property.
         */
        public GridbagConstraints.Builder<_B> withIpady(final String ipady) {
            this.ipady = ipady;
            return this;
        }

        @Override
        public GridbagConstraints build() {
            if (_storedValue == null) {
                return this.init(new GridbagConstraints());
            } else {
                return ((GridbagConstraints) _storedValue);
            }
        }

        public GridbagConstraints.Builder<_B> copyOf(final GridbagConstraints _other) {
            _other.copyTo(this);
            return this;
        }

        public GridbagConstraints.Builder<_B> copyOf(final GridbagConstraints.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends GridbagConstraints.Selector<GridbagConstraints.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static GridbagConstraints.Select _root() {
            return new GridbagConstraints.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>> gridx = null;
        private com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>> gridy = null;
        private com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>> gridwidth = null;
        private com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>> gridheight = null;
        private com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>> weightx = null;
        private com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>> weighty = null;
        private com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>> anchor = null;
        private com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>> fill = null;
        private com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>> insettop = null;
        private com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>> insetleft = null;
        private com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>> insetbottom = null;
        private com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>> insetright = null;
        private com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>> ipadx = null;
        private com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>> ipady = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.gridx!= null) {
                products.put("gridx", this.gridx.init());
            }
            if (this.gridy!= null) {
                products.put("gridy", this.gridy.init());
            }
            if (this.gridwidth!= null) {
                products.put("gridwidth", this.gridwidth.init());
            }
            if (this.gridheight!= null) {
                products.put("gridheight", this.gridheight.init());
            }
            if (this.weightx!= null) {
                products.put("weightx", this.weightx.init());
            }
            if (this.weighty!= null) {
                products.put("weighty", this.weighty.init());
            }
            if (this.anchor!= null) {
                products.put("anchor", this.anchor.init());
            }
            if (this.fill!= null) {
                products.put("fill", this.fill.init());
            }
            if (this.insettop!= null) {
                products.put("insettop", this.insettop.init());
            }
            if (this.insetleft!= null) {
                products.put("insetleft", this.insetleft.init());
            }
            if (this.insetbottom!= null) {
                products.put("insetbottom", this.insetbottom.init());
            }
            if (this.insetright!= null) {
                products.put("insetright", this.insetright.init());
            }
            if (this.ipadx!= null) {
                products.put("ipadx", this.ipadx.init());
            }
            if (this.ipady!= null) {
                products.put("ipady", this.ipady.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>> gridx() {
            return ((this.gridx == null)?this.gridx = new com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>>(this._root, this, "gridx"):this.gridx);
        }

        public com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>> gridy() {
            return ((this.gridy == null)?this.gridy = new com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>>(this._root, this, "gridy"):this.gridy);
        }

        public com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>> gridwidth() {
            return ((this.gridwidth == null)?this.gridwidth = new com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>>(this._root, this, "gridwidth"):this.gridwidth);
        }

        public com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>> gridheight() {
            return ((this.gridheight == null)?this.gridheight = new com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>>(this._root, this, "gridheight"):this.gridheight);
        }

        public com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>> weightx() {
            return ((this.weightx == null)?this.weightx = new com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>>(this._root, this, "weightx"):this.weightx);
        }

        public com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>> weighty() {
            return ((this.weighty == null)?this.weighty = new com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>>(this._root, this, "weighty"):this.weighty);
        }

        public com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>> anchor() {
            return ((this.anchor == null)?this.anchor = new com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>>(this._root, this, "anchor"):this.anchor);
        }

        public com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>> fill() {
            return ((this.fill == null)?this.fill = new com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>>(this._root, this, "fill"):this.fill);
        }

        public com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>> insettop() {
            return ((this.insettop == null)?this.insettop = new com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>>(this._root, this, "insettop"):this.insettop);
        }

        public com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>> insetleft() {
            return ((this.insetleft == null)?this.insetleft = new com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>>(this._root, this, "insetleft"):this.insetleft);
        }

        public com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>> insetbottom() {
            return ((this.insetbottom == null)?this.insetbottom = new com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>>(this._root, this, "insetbottom"):this.insetbottom);
        }

        public com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>> insetright() {
            return ((this.insetright == null)?this.insetright = new com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>>(this._root, this, "insetright"):this.insetright);
        }

        public com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>> ipadx() {
            return ((this.ipadx == null)?this.ipadx = new com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>>(this._root, this, "ipadx"):this.ipadx);
        }

        public com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>> ipady() {
            return ((this.ipady == null)?this.ipady = new com.kscs.util.jaxb.Selector<TRoot, GridbagConstraints.Selector<TRoot, TParent>>(this._root, this, "ipady"):this.ipady);
        }

    }

}
