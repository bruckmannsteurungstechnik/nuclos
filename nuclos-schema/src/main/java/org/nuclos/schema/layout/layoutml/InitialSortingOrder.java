
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="sorting-order" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
 *             &lt;enumeration value="ascending"/&gt;
 *             &lt;enumeration value="descending"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "initial-sorting-order")
public class InitialSortingOrder implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "name", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String name;
    @XmlAttribute(name = "sorting-order", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String sortingOrder;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the sortingOrder property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSortingOrder() {
        return sortingOrder;
    }

    /**
     * Sets the value of the sortingOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSortingOrder(String value) {
        this.sortingOrder = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String theSortingOrder;
            theSortingOrder = this.getSortingOrder();
            strategy.appendField(locator, this, "sortingOrder", buffer, theSortingOrder);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof InitialSortingOrder)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final InitialSortingOrder that = ((InitialSortingOrder) object);
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsSortingOrder;
            lhsSortingOrder = this.getSortingOrder();
            String rhsSortingOrder;
            rhsSortingOrder = that.getSortingOrder();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "sortingOrder", lhsSortingOrder), LocatorUtils.property(thatLocator, "sortingOrder", rhsSortingOrder), lhsSortingOrder, rhsSortingOrder)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String theSortingOrder;
            theSortingOrder = this.getSortingOrder();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "sortingOrder", theSortingOrder), currentHashCode, theSortingOrder);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof InitialSortingOrder) {
            final InitialSortingOrder copy = ((InitialSortingOrder) draftCopy);
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.sortingOrder!= null) {
                String sourceSortingOrder;
                sourceSortingOrder = this.getSortingOrder();
                String copySortingOrder = ((String) strategy.copy(LocatorUtils.property(locator, "sortingOrder", sourceSortingOrder), sourceSortingOrder));
                copy.setSortingOrder(copySortingOrder);
            } else {
                copy.sortingOrder = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new InitialSortingOrder();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final InitialSortingOrder.Builder<_B> _other) {
        _other.name = this.name;
        _other.sortingOrder = this.sortingOrder;
    }

    public<_B >InitialSortingOrder.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new InitialSortingOrder.Builder<_B>(_parentBuilder, this, true);
    }

    public InitialSortingOrder.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static InitialSortingOrder.Builder<Void> builder() {
        return new InitialSortingOrder.Builder<Void>(null, null, false);
    }

    public static<_B >InitialSortingOrder.Builder<_B> copyOf(final InitialSortingOrder _other) {
        final InitialSortingOrder.Builder<_B> _newBuilder = new InitialSortingOrder.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final InitialSortingOrder.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree sortingOrderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("sortingOrder"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(sortingOrderPropertyTree!= null):((sortingOrderPropertyTree == null)||(!sortingOrderPropertyTree.isLeaf())))) {
            _other.sortingOrder = this.sortingOrder;
        }
    }

    public<_B >InitialSortingOrder.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new InitialSortingOrder.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public InitialSortingOrder.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >InitialSortingOrder.Builder<_B> copyOf(final InitialSortingOrder _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final InitialSortingOrder.Builder<_B> _newBuilder = new InitialSortingOrder.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static InitialSortingOrder.Builder<Void> copyExcept(final InitialSortingOrder _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static InitialSortingOrder.Builder<Void> copyOnly(final InitialSortingOrder _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final InitialSortingOrder _storedValue;
        private String name;
        private String sortingOrder;

        public Builder(final _B _parentBuilder, final InitialSortingOrder _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.name = _other.name;
                    this.sortingOrder = _other.sortingOrder;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final InitialSortingOrder _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree sortingOrderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("sortingOrder"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(sortingOrderPropertyTree!= null):((sortingOrderPropertyTree == null)||(!sortingOrderPropertyTree.isLeaf())))) {
                        this.sortingOrder = _other.sortingOrder;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends InitialSortingOrder >_P init(final _P _product) {
            _product.name = this.name;
            _product.sortingOrder = this.sortingOrder;
            return _product;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public InitialSortingOrder.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "sortingOrder" (any previous value will be replaced)
         * 
         * @param sortingOrder
         *     New value of the "sortingOrder" property.
         */
        public InitialSortingOrder.Builder<_B> withSortingOrder(final String sortingOrder) {
            this.sortingOrder = sortingOrder;
            return this;
        }

        @Override
        public InitialSortingOrder build() {
            if (_storedValue == null) {
                return this.init(new InitialSortingOrder());
            } else {
                return ((InitialSortingOrder) _storedValue);
            }
        }

        public InitialSortingOrder.Builder<_B> copyOf(final InitialSortingOrder _other) {
            _other.copyTo(this);
            return this;
        }

        public InitialSortingOrder.Builder<_B> copyOf(final InitialSortingOrder.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends InitialSortingOrder.Selector<InitialSortingOrder.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static InitialSortingOrder.Select _root() {
            return new InitialSortingOrder.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, InitialSortingOrder.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, InitialSortingOrder.Selector<TRoot, TParent>> sortingOrder = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.sortingOrder!= null) {
                products.put("sortingOrder", this.sortingOrder.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, InitialSortingOrder.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, InitialSortingOrder.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, InitialSortingOrder.Selector<TRoot, TParent>> sortingOrder() {
            return ((this.sortingOrder == null)?this.sortingOrder = new com.kscs.util.jaxb.Selector<TRoot, InitialSortingOrder.Selector<TRoot, TParent>>(this._root, this, "sortingOrder"):this.sortingOrder);
        }

    }

}
