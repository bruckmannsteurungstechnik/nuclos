
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}initial-focus-component" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element ref="{}panel"/&gt;
 *           &lt;element ref="{}splitpane"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "initialFocusComponent",
    "panel",
    "splitpane"
})
@XmlRootElement(name = "layout")
public class Layout implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "initial-focus-component")
    protected InitialFocusComponent initialFocusComponent;
    protected Panel panel;
    protected Splitpane splitpane;

    /**
     * Gets the value of the initialFocusComponent property.
     * 
     * @return
     *     possible object is
     *     {@link InitialFocusComponent }
     *     
     */
    public InitialFocusComponent getInitialFocusComponent() {
        return initialFocusComponent;
    }

    /**
     * Sets the value of the initialFocusComponent property.
     * 
     * @param value
     *     allowed object is
     *     {@link InitialFocusComponent }
     *     
     */
    public void setInitialFocusComponent(InitialFocusComponent value) {
        this.initialFocusComponent = value;
    }

    /**
     * Gets the value of the panel property.
     * 
     * @return
     *     possible object is
     *     {@link Panel }
     *     
     */
    public Panel getPanel() {
        return panel;
    }

    /**
     * Sets the value of the panel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Panel }
     *     
     */
    public void setPanel(Panel value) {
        this.panel = value;
    }

    /**
     * Gets the value of the splitpane property.
     * 
     * @return
     *     possible object is
     *     {@link Splitpane }
     *     
     */
    public Splitpane getSplitpane() {
        return splitpane;
    }

    /**
     * Sets the value of the splitpane property.
     * 
     * @param value
     *     allowed object is
     *     {@link Splitpane }
     *     
     */
    public void setSplitpane(Splitpane value) {
        this.splitpane = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            InitialFocusComponent theInitialFocusComponent;
            theInitialFocusComponent = this.getInitialFocusComponent();
            strategy.appendField(locator, this, "initialFocusComponent", buffer, theInitialFocusComponent);
        }
        {
            Panel thePanel;
            thePanel = this.getPanel();
            strategy.appendField(locator, this, "panel", buffer, thePanel);
        }
        {
            Splitpane theSplitpane;
            theSplitpane = this.getSplitpane();
            strategy.appendField(locator, this, "splitpane", buffer, theSplitpane);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Layout)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Layout that = ((Layout) object);
        {
            InitialFocusComponent lhsInitialFocusComponent;
            lhsInitialFocusComponent = this.getInitialFocusComponent();
            InitialFocusComponent rhsInitialFocusComponent;
            rhsInitialFocusComponent = that.getInitialFocusComponent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "initialFocusComponent", lhsInitialFocusComponent), LocatorUtils.property(thatLocator, "initialFocusComponent", rhsInitialFocusComponent), lhsInitialFocusComponent, rhsInitialFocusComponent)) {
                return false;
            }
        }
        {
            Panel lhsPanel;
            lhsPanel = this.getPanel();
            Panel rhsPanel;
            rhsPanel = that.getPanel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "panel", lhsPanel), LocatorUtils.property(thatLocator, "panel", rhsPanel), lhsPanel, rhsPanel)) {
                return false;
            }
        }
        {
            Splitpane lhsSplitpane;
            lhsSplitpane = this.getSplitpane();
            Splitpane rhsSplitpane;
            rhsSplitpane = that.getSplitpane();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "splitpane", lhsSplitpane), LocatorUtils.property(thatLocator, "splitpane", rhsSplitpane), lhsSplitpane, rhsSplitpane)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            InitialFocusComponent theInitialFocusComponent;
            theInitialFocusComponent = this.getInitialFocusComponent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "initialFocusComponent", theInitialFocusComponent), currentHashCode, theInitialFocusComponent);
        }
        {
            Panel thePanel;
            thePanel = this.getPanel();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "panel", thePanel), currentHashCode, thePanel);
        }
        {
            Splitpane theSplitpane;
            theSplitpane = this.getSplitpane();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "splitpane", theSplitpane), currentHashCode, theSplitpane);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Layout) {
            final Layout copy = ((Layout) draftCopy);
            if (this.initialFocusComponent!= null) {
                InitialFocusComponent sourceInitialFocusComponent;
                sourceInitialFocusComponent = this.getInitialFocusComponent();
                InitialFocusComponent copyInitialFocusComponent = ((InitialFocusComponent) strategy.copy(LocatorUtils.property(locator, "initialFocusComponent", sourceInitialFocusComponent), sourceInitialFocusComponent));
                copy.setInitialFocusComponent(copyInitialFocusComponent);
            } else {
                copy.initialFocusComponent = null;
            }
            if (this.panel!= null) {
                Panel sourcePanel;
                sourcePanel = this.getPanel();
                Panel copyPanel = ((Panel) strategy.copy(LocatorUtils.property(locator, "panel", sourcePanel), sourcePanel));
                copy.setPanel(copyPanel);
            } else {
                copy.panel = null;
            }
            if (this.splitpane!= null) {
                Splitpane sourceSplitpane;
                sourceSplitpane = this.getSplitpane();
                Splitpane copySplitpane = ((Splitpane) strategy.copy(LocatorUtils.property(locator, "splitpane", sourceSplitpane), sourceSplitpane));
                copy.setSplitpane(copySplitpane);
            } else {
                copy.splitpane = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Layout();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Layout.Builder<_B> _other) {
        _other.initialFocusComponent = ((this.initialFocusComponent == null)?null:this.initialFocusComponent.newCopyBuilder(_other));
        _other.panel = ((this.panel == null)?null:this.panel.newCopyBuilder(_other));
        _other.splitpane = ((this.splitpane == null)?null:this.splitpane.newCopyBuilder(_other));
    }

    public<_B >Layout.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Layout.Builder<_B>(_parentBuilder, this, true);
    }

    public Layout.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Layout.Builder<Void> builder() {
        return new Layout.Builder<Void>(null, null, false);
    }

    public static<_B >Layout.Builder<_B> copyOf(final Layout _other) {
        final Layout.Builder<_B> _newBuilder = new Layout.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Layout.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree initialFocusComponentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("initialFocusComponent"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(initialFocusComponentPropertyTree!= null):((initialFocusComponentPropertyTree == null)||(!initialFocusComponentPropertyTree.isLeaf())))) {
            _other.initialFocusComponent = ((this.initialFocusComponent == null)?null:this.initialFocusComponent.newCopyBuilder(_other, initialFocusComponentPropertyTree, _propertyTreeUse));
        }
        final PropertyTree panelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("panel"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(panelPropertyTree!= null):((panelPropertyTree == null)||(!panelPropertyTree.isLeaf())))) {
            _other.panel = ((this.panel == null)?null:this.panel.newCopyBuilder(_other, panelPropertyTree, _propertyTreeUse));
        }
        final PropertyTree splitpanePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("splitpane"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(splitpanePropertyTree!= null):((splitpanePropertyTree == null)||(!splitpanePropertyTree.isLeaf())))) {
            _other.splitpane = ((this.splitpane == null)?null:this.splitpane.newCopyBuilder(_other, splitpanePropertyTree, _propertyTreeUse));
        }
    }

    public<_B >Layout.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Layout.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Layout.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Layout.Builder<_B> copyOf(final Layout _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Layout.Builder<_B> _newBuilder = new Layout.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Layout.Builder<Void> copyExcept(final Layout _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Layout.Builder<Void> copyOnly(final Layout _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Layout _storedValue;
        private InitialFocusComponent.Builder<Layout.Builder<_B>> initialFocusComponent;
        private Panel.Builder<Layout.Builder<_B>> panel;
        private Splitpane.Builder<Layout.Builder<_B>> splitpane;

        public Builder(final _B _parentBuilder, final Layout _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.initialFocusComponent = ((_other.initialFocusComponent == null)?null:_other.initialFocusComponent.newCopyBuilder(this));
                    this.panel = ((_other.panel == null)?null:_other.panel.newCopyBuilder(this));
                    this.splitpane = ((_other.splitpane == null)?null:_other.splitpane.newCopyBuilder(this));
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Layout _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree initialFocusComponentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("initialFocusComponent"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(initialFocusComponentPropertyTree!= null):((initialFocusComponentPropertyTree == null)||(!initialFocusComponentPropertyTree.isLeaf())))) {
                        this.initialFocusComponent = ((_other.initialFocusComponent == null)?null:_other.initialFocusComponent.newCopyBuilder(this, initialFocusComponentPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree panelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("panel"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(panelPropertyTree!= null):((panelPropertyTree == null)||(!panelPropertyTree.isLeaf())))) {
                        this.panel = ((_other.panel == null)?null:_other.panel.newCopyBuilder(this, panelPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree splitpanePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("splitpane"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(splitpanePropertyTree!= null):((splitpanePropertyTree == null)||(!splitpanePropertyTree.isLeaf())))) {
                        this.splitpane = ((_other.splitpane == null)?null:_other.splitpane.newCopyBuilder(this, splitpanePropertyTree, _propertyTreeUse));
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Layout >_P init(final _P _product) {
            _product.initialFocusComponent = ((this.initialFocusComponent == null)?null:this.initialFocusComponent.build());
            _product.panel = ((this.panel == null)?null:this.panel.build());
            _product.splitpane = ((this.splitpane == null)?null:this.splitpane.build());
            return _product;
        }

        /**
         * Sets the new value of "initialFocusComponent" (any previous value will be replaced)
         * 
         * @param initialFocusComponent
         *     New value of the "initialFocusComponent" property.
         */
        public Layout.Builder<_B> withInitialFocusComponent(final InitialFocusComponent initialFocusComponent) {
            this.initialFocusComponent = ((initialFocusComponent == null)?null:new InitialFocusComponent.Builder<Layout.Builder<_B>>(this, initialFocusComponent, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "initialFocusComponent" property.
         * Use {@link org.nuclos.schema.layout.layoutml.InitialFocusComponent.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "initialFocusComponent" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.InitialFocusComponent.Builder#end()} to return to the current builder.
         */
        public InitialFocusComponent.Builder<? extends Layout.Builder<_B>> withInitialFocusComponent() {
            if (this.initialFocusComponent!= null) {
                return this.initialFocusComponent;
            }
            return this.initialFocusComponent = new InitialFocusComponent.Builder<Layout.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "panel" (any previous value will be replaced)
         * 
         * @param panel
         *     New value of the "panel" property.
         */
        public Layout.Builder<_B> withPanel(final Panel panel) {
            this.panel = ((panel == null)?null:new Panel.Builder<Layout.Builder<_B>>(this, panel, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "panel" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Panel.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "panel" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Panel.Builder#end()} to return to the current builder.
         */
        public Panel.Builder<? extends Layout.Builder<_B>> withPanel() {
            if (this.panel!= null) {
                return this.panel;
            }
            return this.panel = new Panel.Builder<Layout.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "splitpane" (any previous value will be replaced)
         * 
         * @param splitpane
         *     New value of the "splitpane" property.
         */
        public Layout.Builder<_B> withSplitpane(final Splitpane splitpane) {
            this.splitpane = ((splitpane == null)?null:new Splitpane.Builder<Layout.Builder<_B>>(this, splitpane, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "splitpane" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Splitpane.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "splitpane" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Splitpane.Builder#end()} to return to the current builder.
         */
        public Splitpane.Builder<? extends Layout.Builder<_B>> withSplitpane() {
            if (this.splitpane!= null) {
                return this.splitpane;
            }
            return this.splitpane = new Splitpane.Builder<Layout.Builder<_B>>(this, null, false);
        }

        @Override
        public Layout build() {
            if (_storedValue == null) {
                return this.init(new Layout());
            } else {
                return ((Layout) _storedValue);
            }
        }

        public Layout.Builder<_B> copyOf(final Layout _other) {
            _other.copyTo(this);
            return this;
        }

        public Layout.Builder<_B> copyOf(final Layout.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Layout.Selector<Layout.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Layout.Select _root() {
            return new Layout.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private InitialFocusComponent.Selector<TRoot, Layout.Selector<TRoot, TParent>> initialFocusComponent = null;
        private Panel.Selector<TRoot, Layout.Selector<TRoot, TParent>> panel = null;
        private Splitpane.Selector<TRoot, Layout.Selector<TRoot, TParent>> splitpane = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.initialFocusComponent!= null) {
                products.put("initialFocusComponent", this.initialFocusComponent.init());
            }
            if (this.panel!= null) {
                products.put("panel", this.panel.init());
            }
            if (this.splitpane!= null) {
                products.put("splitpane", this.splitpane.init());
            }
            return products;
        }

        public InitialFocusComponent.Selector<TRoot, Layout.Selector<TRoot, TParent>> initialFocusComponent() {
            return ((this.initialFocusComponent == null)?this.initialFocusComponent = new InitialFocusComponent.Selector<TRoot, Layout.Selector<TRoot, TParent>>(this._root, this, "initialFocusComponent"):this.initialFocusComponent);
        }

        public Panel.Selector<TRoot, Layout.Selector<TRoot, TParent>> panel() {
            return ((this.panel == null)?this.panel = new Panel.Selector<TRoot, Layout.Selector<TRoot, TParent>>(this._root, this, "panel"):this.panel);
        }

        public Splitpane.Selector<TRoot, Layout.Selector<TRoot, TParent>> splitpane() {
            return ((this.splitpane == null)?this.splitpane = new Splitpane.Selector<TRoot, Layout.Selector<TRoot, TParent>>(this._root, this, "splitpane"):this.splitpane);
        }

    }

}
