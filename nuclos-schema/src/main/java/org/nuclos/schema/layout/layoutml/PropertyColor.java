
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="red" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="green" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="blue" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class PropertyColor implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "name", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String name;
    @XmlAttribute(name = "red")
    @XmlSchemaType(name = "anySimpleType")
    protected String red;
    @XmlAttribute(name = "green")
    @XmlSchemaType(name = "anySimpleType")
    protected String green;
    @XmlAttribute(name = "blue")
    @XmlSchemaType(name = "anySimpleType")
    protected String blue;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the red property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRed() {
        return red;
    }

    /**
     * Sets the value of the red property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRed(String value) {
        this.red = value;
    }

    /**
     * Gets the value of the green property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGreen() {
        return green;
    }

    /**
     * Sets the value of the green property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGreen(String value) {
        this.green = value;
    }

    /**
     * Gets the value of the blue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBlue() {
        return blue;
    }

    /**
     * Sets the value of the blue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBlue(String value) {
        this.blue = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String theRed;
            theRed = this.getRed();
            strategy.appendField(locator, this, "red", buffer, theRed);
        }
        {
            String theGreen;
            theGreen = this.getGreen();
            strategy.appendField(locator, this, "green", buffer, theGreen);
        }
        {
            String theBlue;
            theBlue = this.getBlue();
            strategy.appendField(locator, this, "blue", buffer, theBlue);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof PropertyColor)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final PropertyColor that = ((PropertyColor) object);
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsRed;
            lhsRed = this.getRed();
            String rhsRed;
            rhsRed = that.getRed();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "red", lhsRed), LocatorUtils.property(thatLocator, "red", rhsRed), lhsRed, rhsRed)) {
                return false;
            }
        }
        {
            String lhsGreen;
            lhsGreen = this.getGreen();
            String rhsGreen;
            rhsGreen = that.getGreen();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "green", lhsGreen), LocatorUtils.property(thatLocator, "green", rhsGreen), lhsGreen, rhsGreen)) {
                return false;
            }
        }
        {
            String lhsBlue;
            lhsBlue = this.getBlue();
            String rhsBlue;
            rhsBlue = that.getBlue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "blue", lhsBlue), LocatorUtils.property(thatLocator, "blue", rhsBlue), lhsBlue, rhsBlue)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String theRed;
            theRed = this.getRed();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "red", theRed), currentHashCode, theRed);
        }
        {
            String theGreen;
            theGreen = this.getGreen();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "green", theGreen), currentHashCode, theGreen);
        }
        {
            String theBlue;
            theBlue = this.getBlue();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "blue", theBlue), currentHashCode, theBlue);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof PropertyColor) {
            final PropertyColor copy = ((PropertyColor) draftCopy);
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.red!= null) {
                String sourceRed;
                sourceRed = this.getRed();
                String copyRed = ((String) strategy.copy(LocatorUtils.property(locator, "red", sourceRed), sourceRed));
                copy.setRed(copyRed);
            } else {
                copy.red = null;
            }
            if (this.green!= null) {
                String sourceGreen;
                sourceGreen = this.getGreen();
                String copyGreen = ((String) strategy.copy(LocatorUtils.property(locator, "green", sourceGreen), sourceGreen));
                copy.setGreen(copyGreen);
            } else {
                copy.green = null;
            }
            if (this.blue!= null) {
                String sourceBlue;
                sourceBlue = this.getBlue();
                String copyBlue = ((String) strategy.copy(LocatorUtils.property(locator, "blue", sourceBlue), sourceBlue));
                copy.setBlue(copyBlue);
            } else {
                copy.blue = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new PropertyColor();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final PropertyColor.Builder<_B> _other) {
        _other.name = this.name;
        _other.red = this.red;
        _other.green = this.green;
        _other.blue = this.blue;
    }

    public<_B >PropertyColor.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new PropertyColor.Builder<_B>(_parentBuilder, this, true);
    }

    public PropertyColor.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static PropertyColor.Builder<Void> builder() {
        return new PropertyColor.Builder<Void>(null, null, false);
    }

    public static<_B >PropertyColor.Builder<_B> copyOf(final PropertyColor _other) {
        final PropertyColor.Builder<_B> _newBuilder = new PropertyColor.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final PropertyColor.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree redPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("red"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(redPropertyTree!= null):((redPropertyTree == null)||(!redPropertyTree.isLeaf())))) {
            _other.red = this.red;
        }
        final PropertyTree greenPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("green"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(greenPropertyTree!= null):((greenPropertyTree == null)||(!greenPropertyTree.isLeaf())))) {
            _other.green = this.green;
        }
        final PropertyTree bluePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("blue"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(bluePropertyTree!= null):((bluePropertyTree == null)||(!bluePropertyTree.isLeaf())))) {
            _other.blue = this.blue;
        }
    }

    public<_B >PropertyColor.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new PropertyColor.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public PropertyColor.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >PropertyColor.Builder<_B> copyOf(final PropertyColor _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyColor.Builder<_B> _newBuilder = new PropertyColor.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static PropertyColor.Builder<Void> copyExcept(final PropertyColor _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static PropertyColor.Builder<Void> copyOnly(final PropertyColor _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final PropertyColor _storedValue;
        private String name;
        private String red;
        private String green;
        private String blue;

        public Builder(final _B _parentBuilder, final PropertyColor _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.name = _other.name;
                    this.red = _other.red;
                    this.green = _other.green;
                    this.blue = _other.blue;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final PropertyColor _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree redPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("red"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(redPropertyTree!= null):((redPropertyTree == null)||(!redPropertyTree.isLeaf())))) {
                        this.red = _other.red;
                    }
                    final PropertyTree greenPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("green"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(greenPropertyTree!= null):((greenPropertyTree == null)||(!greenPropertyTree.isLeaf())))) {
                        this.green = _other.green;
                    }
                    final PropertyTree bluePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("blue"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(bluePropertyTree!= null):((bluePropertyTree == null)||(!bluePropertyTree.isLeaf())))) {
                        this.blue = _other.blue;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends PropertyColor >_P init(final _P _product) {
            _product.name = this.name;
            _product.red = this.red;
            _product.green = this.green;
            _product.blue = this.blue;
            return _product;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public PropertyColor.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "red" (any previous value will be replaced)
         * 
         * @param red
         *     New value of the "red" property.
         */
        public PropertyColor.Builder<_B> withRed(final String red) {
            this.red = red;
            return this;
        }

        /**
         * Sets the new value of "green" (any previous value will be replaced)
         * 
         * @param green
         *     New value of the "green" property.
         */
        public PropertyColor.Builder<_B> withGreen(final String green) {
            this.green = green;
            return this;
        }

        /**
         * Sets the new value of "blue" (any previous value will be replaced)
         * 
         * @param blue
         *     New value of the "blue" property.
         */
        public PropertyColor.Builder<_B> withBlue(final String blue) {
            this.blue = blue;
            return this;
        }

        @Override
        public PropertyColor build() {
            if (_storedValue == null) {
                return this.init(new PropertyColor());
            } else {
                return ((PropertyColor) _storedValue);
            }
        }

        public PropertyColor.Builder<_B> copyOf(final PropertyColor _other) {
            _other.copyTo(this);
            return this;
        }

        public PropertyColor.Builder<_B> copyOf(final PropertyColor.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends PropertyColor.Selector<PropertyColor.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static PropertyColor.Select _root() {
            return new PropertyColor.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, PropertyColor.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, PropertyColor.Selector<TRoot, TParent>> red = null;
        private com.kscs.util.jaxb.Selector<TRoot, PropertyColor.Selector<TRoot, TParent>> green = null;
        private com.kscs.util.jaxb.Selector<TRoot, PropertyColor.Selector<TRoot, TParent>> blue = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.red!= null) {
                products.put("red", this.red.init());
            }
            if (this.green!= null) {
                products.put("green", this.green.init());
            }
            if (this.blue!= null) {
                products.put("blue", this.blue.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, PropertyColor.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, PropertyColor.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PropertyColor.Selector<TRoot, TParent>> red() {
            return ((this.red == null)?this.red = new com.kscs.util.jaxb.Selector<TRoot, PropertyColor.Selector<TRoot, TParent>>(this._root, this, "red"):this.red);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PropertyColor.Selector<TRoot, TParent>> green() {
            return ((this.green == null)?this.green = new com.kscs.util.jaxb.Selector<TRoot, PropertyColor.Selector<TRoot, TParent>>(this._root, this, "green"):this.green);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PropertyColor.Selector<TRoot, TParent>> blue() {
            return ((this.blue == null)?this.blue = new com.kscs.util.jaxb.Selector<TRoot, PropertyColor.Selector<TRoot, TParent>>(this._root, this, "blue"):this.blue);
        }

    }

}
