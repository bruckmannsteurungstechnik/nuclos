
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="gap" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="fill-vertically" type="{}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class Rowlayout implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "gap")
    @XmlSchemaType(name = "anySimpleType")
    protected String gap;
    @XmlAttribute(name = "fill-vertically")
    protected Boolean fillVertically;

    /**
     * Gets the value of the gap property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGap() {
        return gap;
    }

    /**
     * Sets the value of the gap property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGap(String value) {
        this.gap = value;
    }

    /**
     * Gets the value of the fillVertically property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getFillVertically() {
        return fillVertically;
    }

    /**
     * Sets the value of the fillVertically property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFillVertically(Boolean value) {
        this.fillVertically = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theGap;
            theGap = this.getGap();
            strategy.appendField(locator, this, "gap", buffer, theGap);
        }
        {
            Boolean theFillVertically;
            theFillVertically = this.getFillVertically();
            strategy.appendField(locator, this, "fillVertically", buffer, theFillVertically);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Rowlayout)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Rowlayout that = ((Rowlayout) object);
        {
            String lhsGap;
            lhsGap = this.getGap();
            String rhsGap;
            rhsGap = that.getGap();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "gap", lhsGap), LocatorUtils.property(thatLocator, "gap", rhsGap), lhsGap, rhsGap)) {
                return false;
            }
        }
        {
            Boolean lhsFillVertically;
            lhsFillVertically = this.getFillVertically();
            Boolean rhsFillVertically;
            rhsFillVertically = that.getFillVertically();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fillVertically", lhsFillVertically), LocatorUtils.property(thatLocator, "fillVertically", rhsFillVertically), lhsFillVertically, rhsFillVertically)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theGap;
            theGap = this.getGap();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "gap", theGap), currentHashCode, theGap);
        }
        {
            Boolean theFillVertically;
            theFillVertically = this.getFillVertically();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fillVertically", theFillVertically), currentHashCode, theFillVertically);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Rowlayout) {
            final Rowlayout copy = ((Rowlayout) draftCopy);
            if (this.gap!= null) {
                String sourceGap;
                sourceGap = this.getGap();
                String copyGap = ((String) strategy.copy(LocatorUtils.property(locator, "gap", sourceGap), sourceGap));
                copy.setGap(copyGap);
            } else {
                copy.gap = null;
            }
            if (this.fillVertically!= null) {
                Boolean sourceFillVertically;
                sourceFillVertically = this.getFillVertically();
                Boolean copyFillVertically = ((Boolean) strategy.copy(LocatorUtils.property(locator, "fillVertically", sourceFillVertically), sourceFillVertically));
                copy.setFillVertically(copyFillVertically);
            } else {
                copy.fillVertically = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Rowlayout();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Rowlayout.Builder<_B> _other) {
        _other.gap = this.gap;
        _other.fillVertically = this.fillVertically;
    }

    public<_B >Rowlayout.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Rowlayout.Builder<_B>(_parentBuilder, this, true);
    }

    public Rowlayout.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Rowlayout.Builder<Void> builder() {
        return new Rowlayout.Builder<Void>(null, null, false);
    }

    public static<_B >Rowlayout.Builder<_B> copyOf(final Rowlayout _other) {
        final Rowlayout.Builder<_B> _newBuilder = new Rowlayout.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Rowlayout.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree gapPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("gap"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(gapPropertyTree!= null):((gapPropertyTree == null)||(!gapPropertyTree.isLeaf())))) {
            _other.gap = this.gap;
        }
        final PropertyTree fillVerticallyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fillVertically"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fillVerticallyPropertyTree!= null):((fillVerticallyPropertyTree == null)||(!fillVerticallyPropertyTree.isLeaf())))) {
            _other.fillVertically = this.fillVertically;
        }
    }

    public<_B >Rowlayout.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Rowlayout.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Rowlayout.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Rowlayout.Builder<_B> copyOf(final Rowlayout _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Rowlayout.Builder<_B> _newBuilder = new Rowlayout.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Rowlayout.Builder<Void> copyExcept(final Rowlayout _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Rowlayout.Builder<Void> copyOnly(final Rowlayout _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Rowlayout _storedValue;
        private String gap;
        private Boolean fillVertically;

        public Builder(final _B _parentBuilder, final Rowlayout _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.gap = _other.gap;
                    this.fillVertically = _other.fillVertically;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Rowlayout _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree gapPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("gap"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(gapPropertyTree!= null):((gapPropertyTree == null)||(!gapPropertyTree.isLeaf())))) {
                        this.gap = _other.gap;
                    }
                    final PropertyTree fillVerticallyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fillVertically"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fillVerticallyPropertyTree!= null):((fillVerticallyPropertyTree == null)||(!fillVerticallyPropertyTree.isLeaf())))) {
                        this.fillVertically = _other.fillVertically;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Rowlayout >_P init(final _P _product) {
            _product.gap = this.gap;
            _product.fillVertically = this.fillVertically;
            return _product;
        }

        /**
         * Sets the new value of "gap" (any previous value will be replaced)
         * 
         * @param gap
         *     New value of the "gap" property.
         */
        public Rowlayout.Builder<_B> withGap(final String gap) {
            this.gap = gap;
            return this;
        }

        /**
         * Sets the new value of "fillVertically" (any previous value will be replaced)
         * 
         * @param fillVertically
         *     New value of the "fillVertically" property.
         */
        public Rowlayout.Builder<_B> withFillVertically(final Boolean fillVertically) {
            this.fillVertically = fillVertically;
            return this;
        }

        @Override
        public Rowlayout build() {
            if (_storedValue == null) {
                return this.init(new Rowlayout());
            } else {
                return ((Rowlayout) _storedValue);
            }
        }

        public Rowlayout.Builder<_B> copyOf(final Rowlayout _other) {
            _other.copyTo(this);
            return this;
        }

        public Rowlayout.Builder<_B> copyOf(final Rowlayout.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Rowlayout.Selector<Rowlayout.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Rowlayout.Select _root() {
            return new Rowlayout.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, Rowlayout.Selector<TRoot, TParent>> gap = null;
        private com.kscs.util.jaxb.Selector<TRoot, Rowlayout.Selector<TRoot, TParent>> fillVertically = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.gap!= null) {
                products.put("gap", this.gap.init());
            }
            if (this.fillVertically!= null) {
                products.put("fillVertically", this.fillVertically.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, Rowlayout.Selector<TRoot, TParent>> gap() {
            return ((this.gap == null)?this.gap = new com.kscs.util.jaxb.Selector<TRoot, Rowlayout.Selector<TRoot, TParent>>(this._root, this, "gap"):this.gap);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Rowlayout.Selector<TRoot, TParent>> fillVertically() {
            return ((this.fillVertically == null)?this.fillVertically = new com.kscs.util.jaxb.Selector<TRoot, Rowlayout.Selector<TRoot, TParent>>(this._root, this, "fillVertically"):this.fillVertically);
        }

    }

}
