
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="position" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
 *             &lt;enumeration value="top"/&gt;
 *             &lt;enumeration value="bottom"/&gt;
 *             &lt;enumeration value="left"/&gt;
 *             &lt;enumeration value="right"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class SplitpaneConstraints implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "position", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String position;

    /**
     * Gets the value of the position property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPosition() {
        return position;
    }

    /**
     * Sets the value of the position property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPosition(String value) {
        this.position = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String thePosition;
            thePosition = this.getPosition();
            strategy.appendField(locator, this, "position", buffer, thePosition);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof SplitpaneConstraints)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final SplitpaneConstraints that = ((SplitpaneConstraints) object);
        {
            String lhsPosition;
            lhsPosition = this.getPosition();
            String rhsPosition;
            rhsPosition = that.getPosition();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "position", lhsPosition), LocatorUtils.property(thatLocator, "position", rhsPosition), lhsPosition, rhsPosition)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String thePosition;
            thePosition = this.getPosition();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "position", thePosition), currentHashCode, thePosition);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof SplitpaneConstraints) {
            final SplitpaneConstraints copy = ((SplitpaneConstraints) draftCopy);
            if (this.position!= null) {
                String sourcePosition;
                sourcePosition = this.getPosition();
                String copyPosition = ((String) strategy.copy(LocatorUtils.property(locator, "position", sourcePosition), sourcePosition));
                copy.setPosition(copyPosition);
            } else {
                copy.position = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new SplitpaneConstraints();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final SplitpaneConstraints.Builder<_B> _other) {
        _other.position = this.position;
    }

    public<_B >SplitpaneConstraints.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new SplitpaneConstraints.Builder<_B>(_parentBuilder, this, true);
    }

    public SplitpaneConstraints.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static SplitpaneConstraints.Builder<Void> builder() {
        return new SplitpaneConstraints.Builder<Void>(null, null, false);
    }

    public static<_B >SplitpaneConstraints.Builder<_B> copyOf(final SplitpaneConstraints _other) {
        final SplitpaneConstraints.Builder<_B> _newBuilder = new SplitpaneConstraints.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final SplitpaneConstraints.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree positionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("position"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(positionPropertyTree!= null):((positionPropertyTree == null)||(!positionPropertyTree.isLeaf())))) {
            _other.position = this.position;
        }
    }

    public<_B >SplitpaneConstraints.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new SplitpaneConstraints.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public SplitpaneConstraints.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >SplitpaneConstraints.Builder<_B> copyOf(final SplitpaneConstraints _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final SplitpaneConstraints.Builder<_B> _newBuilder = new SplitpaneConstraints.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static SplitpaneConstraints.Builder<Void> copyExcept(final SplitpaneConstraints _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static SplitpaneConstraints.Builder<Void> copyOnly(final SplitpaneConstraints _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final SplitpaneConstraints _storedValue;
        private String position;

        public Builder(final _B _parentBuilder, final SplitpaneConstraints _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.position = _other.position;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final SplitpaneConstraints _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree positionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("position"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(positionPropertyTree!= null):((positionPropertyTree == null)||(!positionPropertyTree.isLeaf())))) {
                        this.position = _other.position;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends SplitpaneConstraints >_P init(final _P _product) {
            _product.position = this.position;
            return _product;
        }

        /**
         * Sets the new value of "position" (any previous value will be replaced)
         * 
         * @param position
         *     New value of the "position" property.
         */
        public SplitpaneConstraints.Builder<_B> withPosition(final String position) {
            this.position = position;
            return this;
        }

        @Override
        public SplitpaneConstraints build() {
            if (_storedValue == null) {
                return this.init(new SplitpaneConstraints());
            } else {
                return ((SplitpaneConstraints) _storedValue);
            }
        }

        public SplitpaneConstraints.Builder<_B> copyOf(final SplitpaneConstraints _other) {
            _other.copyTo(this);
            return this;
        }

        public SplitpaneConstraints.Builder<_B> copyOf(final SplitpaneConstraints.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends SplitpaneConstraints.Selector<SplitpaneConstraints.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static SplitpaneConstraints.Select _root() {
            return new SplitpaneConstraints.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, SplitpaneConstraints.Selector<TRoot, TParent>> position = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.position!= null) {
                products.put("position", this.position.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, SplitpaneConstraints.Selector<TRoot, TParent>> position() {
            return ((this.position == null)?this.position = new com.kscs.util.jaxb.Selector<TRoot, SplitpaneConstraints.Selector<TRoot, TParent>>(this._root, this, "position"):this.position);
        }

    }

}
