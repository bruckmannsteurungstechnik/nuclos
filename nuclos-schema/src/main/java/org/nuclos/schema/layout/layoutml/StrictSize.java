
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="width" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="height" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "strict-size")
public class StrictSize implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "width")
    @XmlSchemaType(name = "anySimpleType")
    protected String width;
    @XmlAttribute(name = "height")
    @XmlSchemaType(name = "anySimpleType")
    protected String height;

    /**
     * Gets the value of the width property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWidth() {
        return width;
    }

    /**
     * Sets the value of the width property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWidth(String value) {
        this.width = value;
    }

    /**
     * Gets the value of the height property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHeight() {
        return height;
    }

    /**
     * Sets the value of the height property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHeight(String value) {
        this.height = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theWidth;
            theWidth = this.getWidth();
            strategy.appendField(locator, this, "width", buffer, theWidth);
        }
        {
            String theHeight;
            theHeight = this.getHeight();
            strategy.appendField(locator, this, "height", buffer, theHeight);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof StrictSize)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final StrictSize that = ((StrictSize) object);
        {
            String lhsWidth;
            lhsWidth = this.getWidth();
            String rhsWidth;
            rhsWidth = that.getWidth();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "width", lhsWidth), LocatorUtils.property(thatLocator, "width", rhsWidth), lhsWidth, rhsWidth)) {
                return false;
            }
        }
        {
            String lhsHeight;
            lhsHeight = this.getHeight();
            String rhsHeight;
            rhsHeight = that.getHeight();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "height", lhsHeight), LocatorUtils.property(thatLocator, "height", rhsHeight), lhsHeight, rhsHeight)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theWidth;
            theWidth = this.getWidth();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "width", theWidth), currentHashCode, theWidth);
        }
        {
            String theHeight;
            theHeight = this.getHeight();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "height", theHeight), currentHashCode, theHeight);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof StrictSize) {
            final StrictSize copy = ((StrictSize) draftCopy);
            if (this.width!= null) {
                String sourceWidth;
                sourceWidth = this.getWidth();
                String copyWidth = ((String) strategy.copy(LocatorUtils.property(locator, "width", sourceWidth), sourceWidth));
                copy.setWidth(copyWidth);
            } else {
                copy.width = null;
            }
            if (this.height!= null) {
                String sourceHeight;
                sourceHeight = this.getHeight();
                String copyHeight = ((String) strategy.copy(LocatorUtils.property(locator, "height", sourceHeight), sourceHeight));
                copy.setHeight(copyHeight);
            } else {
                copy.height = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new StrictSize();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final StrictSize.Builder<_B> _other) {
        _other.width = this.width;
        _other.height = this.height;
    }

    public<_B >StrictSize.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new StrictSize.Builder<_B>(_parentBuilder, this, true);
    }

    public StrictSize.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static StrictSize.Builder<Void> builder() {
        return new StrictSize.Builder<Void>(null, null, false);
    }

    public static<_B >StrictSize.Builder<_B> copyOf(final StrictSize _other) {
        final StrictSize.Builder<_B> _newBuilder = new StrictSize.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final StrictSize.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree widthPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("width"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(widthPropertyTree!= null):((widthPropertyTree == null)||(!widthPropertyTree.isLeaf())))) {
            _other.width = this.width;
        }
        final PropertyTree heightPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("height"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(heightPropertyTree!= null):((heightPropertyTree == null)||(!heightPropertyTree.isLeaf())))) {
            _other.height = this.height;
        }
    }

    public<_B >StrictSize.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new StrictSize.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public StrictSize.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >StrictSize.Builder<_B> copyOf(final StrictSize _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final StrictSize.Builder<_B> _newBuilder = new StrictSize.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static StrictSize.Builder<Void> copyExcept(final StrictSize _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static StrictSize.Builder<Void> copyOnly(final StrictSize _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final StrictSize _storedValue;
        private String width;
        private String height;

        public Builder(final _B _parentBuilder, final StrictSize _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.width = _other.width;
                    this.height = _other.height;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final StrictSize _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree widthPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("width"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(widthPropertyTree!= null):((widthPropertyTree == null)||(!widthPropertyTree.isLeaf())))) {
                        this.width = _other.width;
                    }
                    final PropertyTree heightPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("height"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(heightPropertyTree!= null):((heightPropertyTree == null)||(!heightPropertyTree.isLeaf())))) {
                        this.height = _other.height;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends StrictSize >_P init(final _P _product) {
            _product.width = this.width;
            _product.height = this.height;
            return _product;
        }

        /**
         * Sets the new value of "width" (any previous value will be replaced)
         * 
         * @param width
         *     New value of the "width" property.
         */
        public StrictSize.Builder<_B> withWidth(final String width) {
            this.width = width;
            return this;
        }

        /**
         * Sets the new value of "height" (any previous value will be replaced)
         * 
         * @param height
         *     New value of the "height" property.
         */
        public StrictSize.Builder<_B> withHeight(final String height) {
            this.height = height;
            return this;
        }

        @Override
        public StrictSize build() {
            if (_storedValue == null) {
                return this.init(new StrictSize());
            } else {
                return ((StrictSize) _storedValue);
            }
        }

        public StrictSize.Builder<_B> copyOf(final StrictSize _other) {
            _other.copyTo(this);
            return this;
        }

        public StrictSize.Builder<_B> copyOf(final StrictSize.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends StrictSize.Selector<StrictSize.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static StrictSize.Select _root() {
            return new StrictSize.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, StrictSize.Selector<TRoot, TParent>> width = null;
        private com.kscs.util.jaxb.Selector<TRoot, StrictSize.Selector<TRoot, TParent>> height = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.width!= null) {
                products.put("width", this.width.init());
            }
            if (this.height!= null) {
                products.put("height", this.height.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, StrictSize.Selector<TRoot, TParent>> width() {
            return ((this.width == null)?this.width = new com.kscs.util.jaxb.Selector<TRoot, StrictSize.Selector<TRoot, TParent>>(this._root, this, "width"):this.width);
        }

        public com.kscs.util.jaxb.Selector<TRoot, StrictSize.Selector<TRoot, TParent>> height() {
            return ((this.height == null)?this.height = new com.kscs.util.jaxb.Selector<TRoot, StrictSize.Selector<TRoot, TParent>>(this._root, this, "height"):this.height);
        }

    }

}
