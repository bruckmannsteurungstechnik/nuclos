
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}translations" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="title" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="enabled" type="{}boolean" /&gt;
 *       &lt;attribute name="resourceId" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="internalname" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="mnemonic" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "translations"
})
public class TabbedpaneConstraints implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected Translations translations;
    @XmlAttribute(name = "title", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String title;
    @XmlAttribute(name = "enabled")
    protected Boolean enabled;
    @XmlAttribute(name = "resourceId")
    @XmlSchemaType(name = "anySimpleType")
    protected String resourceId;
    @XmlAttribute(name = "internalname")
    @XmlSchemaType(name = "anySimpleType")
    protected String internalname;
    @XmlAttribute(name = "mnemonic")
    @XmlSchemaType(name = "anySimpleType")
    protected String mnemonic;

    /**
     * Gets the value of the translations property.
     * 
     * @return
     *     possible object is
     *     {@link Translations }
     *     
     */
    public Translations getTranslations() {
        return translations;
    }

    /**
     * Sets the value of the translations property.
     * 
     * @param value
     *     allowed object is
     *     {@link Translations }
     *     
     */
    public void setTranslations(Translations value) {
        this.translations = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the enabled property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getEnabled() {
        return enabled;
    }

    /**
     * Sets the value of the enabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnabled(Boolean value) {
        this.enabled = value;
    }

    /**
     * Gets the value of the resourceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceId() {
        return resourceId;
    }

    /**
     * Sets the value of the resourceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceId(String value) {
        this.resourceId = value;
    }

    /**
     * Gets the value of the internalname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInternalname() {
        return internalname;
    }

    /**
     * Sets the value of the internalname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInternalname(String value) {
        this.internalname = value;
    }

    /**
     * Gets the value of the mnemonic property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMnemonic() {
        return mnemonic;
    }

    /**
     * Sets the value of the mnemonic property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMnemonic(String value) {
        this.mnemonic = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            Translations theTranslations;
            theTranslations = this.getTranslations();
            strategy.appendField(locator, this, "translations", buffer, theTranslations);
        }
        {
            String theTitle;
            theTitle = this.getTitle();
            strategy.appendField(locator, this, "title", buffer, theTitle);
        }
        {
            Boolean theEnabled;
            theEnabled = this.getEnabled();
            strategy.appendField(locator, this, "enabled", buffer, theEnabled);
        }
        {
            String theResourceId;
            theResourceId = this.getResourceId();
            strategy.appendField(locator, this, "resourceId", buffer, theResourceId);
        }
        {
            String theInternalname;
            theInternalname = this.getInternalname();
            strategy.appendField(locator, this, "internalname", buffer, theInternalname);
        }
        {
            String theMnemonic;
            theMnemonic = this.getMnemonic();
            strategy.appendField(locator, this, "mnemonic", buffer, theMnemonic);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof TabbedpaneConstraints)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final TabbedpaneConstraints that = ((TabbedpaneConstraints) object);
        {
            Translations lhsTranslations;
            lhsTranslations = this.getTranslations();
            Translations rhsTranslations;
            rhsTranslations = that.getTranslations();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "translations", lhsTranslations), LocatorUtils.property(thatLocator, "translations", rhsTranslations), lhsTranslations, rhsTranslations)) {
                return false;
            }
        }
        {
            String lhsTitle;
            lhsTitle = this.getTitle();
            String rhsTitle;
            rhsTitle = that.getTitle();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "title", lhsTitle), LocatorUtils.property(thatLocator, "title", rhsTitle), lhsTitle, rhsTitle)) {
                return false;
            }
        }
        {
            Boolean lhsEnabled;
            lhsEnabled = this.getEnabled();
            Boolean rhsEnabled;
            rhsEnabled = that.getEnabled();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "enabled", lhsEnabled), LocatorUtils.property(thatLocator, "enabled", rhsEnabled), lhsEnabled, rhsEnabled)) {
                return false;
            }
        }
        {
            String lhsResourceId;
            lhsResourceId = this.getResourceId();
            String rhsResourceId;
            rhsResourceId = that.getResourceId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "resourceId", lhsResourceId), LocatorUtils.property(thatLocator, "resourceId", rhsResourceId), lhsResourceId, rhsResourceId)) {
                return false;
            }
        }
        {
            String lhsInternalname;
            lhsInternalname = this.getInternalname();
            String rhsInternalname;
            rhsInternalname = that.getInternalname();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "internalname", lhsInternalname), LocatorUtils.property(thatLocator, "internalname", rhsInternalname), lhsInternalname, rhsInternalname)) {
                return false;
            }
        }
        {
            String lhsMnemonic;
            lhsMnemonic = this.getMnemonic();
            String rhsMnemonic;
            rhsMnemonic = that.getMnemonic();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "mnemonic", lhsMnemonic), LocatorUtils.property(thatLocator, "mnemonic", rhsMnemonic), lhsMnemonic, rhsMnemonic)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            Translations theTranslations;
            theTranslations = this.getTranslations();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "translations", theTranslations), currentHashCode, theTranslations);
        }
        {
            String theTitle;
            theTitle = this.getTitle();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "title", theTitle), currentHashCode, theTitle);
        }
        {
            Boolean theEnabled;
            theEnabled = this.getEnabled();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "enabled", theEnabled), currentHashCode, theEnabled);
        }
        {
            String theResourceId;
            theResourceId = this.getResourceId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "resourceId", theResourceId), currentHashCode, theResourceId);
        }
        {
            String theInternalname;
            theInternalname = this.getInternalname();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "internalname", theInternalname), currentHashCode, theInternalname);
        }
        {
            String theMnemonic;
            theMnemonic = this.getMnemonic();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "mnemonic", theMnemonic), currentHashCode, theMnemonic);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof TabbedpaneConstraints) {
            final TabbedpaneConstraints copy = ((TabbedpaneConstraints) draftCopy);
            if (this.translations!= null) {
                Translations sourceTranslations;
                sourceTranslations = this.getTranslations();
                Translations copyTranslations = ((Translations) strategy.copy(LocatorUtils.property(locator, "translations", sourceTranslations), sourceTranslations));
                copy.setTranslations(copyTranslations);
            } else {
                copy.translations = null;
            }
            if (this.title!= null) {
                String sourceTitle;
                sourceTitle = this.getTitle();
                String copyTitle = ((String) strategy.copy(LocatorUtils.property(locator, "title", sourceTitle), sourceTitle));
                copy.setTitle(copyTitle);
            } else {
                copy.title = null;
            }
            if (this.enabled!= null) {
                Boolean sourceEnabled;
                sourceEnabled = this.getEnabled();
                Boolean copyEnabled = ((Boolean) strategy.copy(LocatorUtils.property(locator, "enabled", sourceEnabled), sourceEnabled));
                copy.setEnabled(copyEnabled);
            } else {
                copy.enabled = null;
            }
            if (this.resourceId!= null) {
                String sourceResourceId;
                sourceResourceId = this.getResourceId();
                String copyResourceId = ((String) strategy.copy(LocatorUtils.property(locator, "resourceId", sourceResourceId), sourceResourceId));
                copy.setResourceId(copyResourceId);
            } else {
                copy.resourceId = null;
            }
            if (this.internalname!= null) {
                String sourceInternalname;
                sourceInternalname = this.getInternalname();
                String copyInternalname = ((String) strategy.copy(LocatorUtils.property(locator, "internalname", sourceInternalname), sourceInternalname));
                copy.setInternalname(copyInternalname);
            } else {
                copy.internalname = null;
            }
            if (this.mnemonic!= null) {
                String sourceMnemonic;
                sourceMnemonic = this.getMnemonic();
                String copyMnemonic = ((String) strategy.copy(LocatorUtils.property(locator, "mnemonic", sourceMnemonic), sourceMnemonic));
                copy.setMnemonic(copyMnemonic);
            } else {
                copy.mnemonic = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new TabbedpaneConstraints();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final TabbedpaneConstraints.Builder<_B> _other) {
        _other.translations = ((this.translations == null)?null:this.translations.newCopyBuilder(_other));
        _other.title = this.title;
        _other.enabled = this.enabled;
        _other.resourceId = this.resourceId;
        _other.internalname = this.internalname;
        _other.mnemonic = this.mnemonic;
    }

    public<_B >TabbedpaneConstraints.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new TabbedpaneConstraints.Builder<_B>(_parentBuilder, this, true);
    }

    public TabbedpaneConstraints.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static TabbedpaneConstraints.Builder<Void> builder() {
        return new TabbedpaneConstraints.Builder<Void>(null, null, false);
    }

    public static<_B >TabbedpaneConstraints.Builder<_B> copyOf(final TabbedpaneConstraints _other) {
        final TabbedpaneConstraints.Builder<_B> _newBuilder = new TabbedpaneConstraints.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final TabbedpaneConstraints.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree translationsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("translations"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(translationsPropertyTree!= null):((translationsPropertyTree == null)||(!translationsPropertyTree.isLeaf())))) {
            _other.translations = ((this.translations == null)?null:this.translations.newCopyBuilder(_other, translationsPropertyTree, _propertyTreeUse));
        }
        final PropertyTree titlePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("title"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(titlePropertyTree!= null):((titlePropertyTree == null)||(!titlePropertyTree.isLeaf())))) {
            _other.title = this.title;
        }
        final PropertyTree enabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("enabled"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(enabledPropertyTree!= null):((enabledPropertyTree == null)||(!enabledPropertyTree.isLeaf())))) {
            _other.enabled = this.enabled;
        }
        final PropertyTree resourceIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resourceId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resourceIdPropertyTree!= null):((resourceIdPropertyTree == null)||(!resourceIdPropertyTree.isLeaf())))) {
            _other.resourceId = this.resourceId;
        }
        final PropertyTree internalnamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("internalname"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(internalnamePropertyTree!= null):((internalnamePropertyTree == null)||(!internalnamePropertyTree.isLeaf())))) {
            _other.internalname = this.internalname;
        }
        final PropertyTree mnemonicPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("mnemonic"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(mnemonicPropertyTree!= null):((mnemonicPropertyTree == null)||(!mnemonicPropertyTree.isLeaf())))) {
            _other.mnemonic = this.mnemonic;
        }
    }

    public<_B >TabbedpaneConstraints.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new TabbedpaneConstraints.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public TabbedpaneConstraints.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >TabbedpaneConstraints.Builder<_B> copyOf(final TabbedpaneConstraints _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final TabbedpaneConstraints.Builder<_B> _newBuilder = new TabbedpaneConstraints.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static TabbedpaneConstraints.Builder<Void> copyExcept(final TabbedpaneConstraints _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static TabbedpaneConstraints.Builder<Void> copyOnly(final TabbedpaneConstraints _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final TabbedpaneConstraints _storedValue;
        private Translations.Builder<TabbedpaneConstraints.Builder<_B>> translations;
        private String title;
        private Boolean enabled;
        private String resourceId;
        private String internalname;
        private String mnemonic;

        public Builder(final _B _parentBuilder, final TabbedpaneConstraints _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.translations = ((_other.translations == null)?null:_other.translations.newCopyBuilder(this));
                    this.title = _other.title;
                    this.enabled = _other.enabled;
                    this.resourceId = _other.resourceId;
                    this.internalname = _other.internalname;
                    this.mnemonic = _other.mnemonic;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final TabbedpaneConstraints _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree translationsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("translations"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(translationsPropertyTree!= null):((translationsPropertyTree == null)||(!translationsPropertyTree.isLeaf())))) {
                        this.translations = ((_other.translations == null)?null:_other.translations.newCopyBuilder(this, translationsPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree titlePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("title"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(titlePropertyTree!= null):((titlePropertyTree == null)||(!titlePropertyTree.isLeaf())))) {
                        this.title = _other.title;
                    }
                    final PropertyTree enabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("enabled"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(enabledPropertyTree!= null):((enabledPropertyTree == null)||(!enabledPropertyTree.isLeaf())))) {
                        this.enabled = _other.enabled;
                    }
                    final PropertyTree resourceIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resourceId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resourceIdPropertyTree!= null):((resourceIdPropertyTree == null)||(!resourceIdPropertyTree.isLeaf())))) {
                        this.resourceId = _other.resourceId;
                    }
                    final PropertyTree internalnamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("internalname"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(internalnamePropertyTree!= null):((internalnamePropertyTree == null)||(!internalnamePropertyTree.isLeaf())))) {
                        this.internalname = _other.internalname;
                    }
                    final PropertyTree mnemonicPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("mnemonic"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(mnemonicPropertyTree!= null):((mnemonicPropertyTree == null)||(!mnemonicPropertyTree.isLeaf())))) {
                        this.mnemonic = _other.mnemonic;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends TabbedpaneConstraints >_P init(final _P _product) {
            _product.translations = ((this.translations == null)?null:this.translations.build());
            _product.title = this.title;
            _product.enabled = this.enabled;
            _product.resourceId = this.resourceId;
            _product.internalname = this.internalname;
            _product.mnemonic = this.mnemonic;
            return _product;
        }

        /**
         * Sets the new value of "translations" (any previous value will be replaced)
         * 
         * @param translations
         *     New value of the "translations" property.
         */
        public TabbedpaneConstraints.Builder<_B> withTranslations(final Translations translations) {
            this.translations = ((translations == null)?null:new Translations.Builder<TabbedpaneConstraints.Builder<_B>>(this, translations, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "translations" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Translations.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "translations" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Translations.Builder#end()} to return to the current builder.
         */
        public Translations.Builder<? extends TabbedpaneConstraints.Builder<_B>> withTranslations() {
            if (this.translations!= null) {
                return this.translations;
            }
            return this.translations = new Translations.Builder<TabbedpaneConstraints.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "title" (any previous value will be replaced)
         * 
         * @param title
         *     New value of the "title" property.
         */
        public TabbedpaneConstraints.Builder<_B> withTitle(final String title) {
            this.title = title;
            return this;
        }

        /**
         * Sets the new value of "enabled" (any previous value will be replaced)
         * 
         * @param enabled
         *     New value of the "enabled" property.
         */
        public TabbedpaneConstraints.Builder<_B> withEnabled(final Boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        /**
         * Sets the new value of "resourceId" (any previous value will be replaced)
         * 
         * @param resourceId
         *     New value of the "resourceId" property.
         */
        public TabbedpaneConstraints.Builder<_B> withResourceId(final String resourceId) {
            this.resourceId = resourceId;
            return this;
        }

        /**
         * Sets the new value of "internalname" (any previous value will be replaced)
         * 
         * @param internalname
         *     New value of the "internalname" property.
         */
        public TabbedpaneConstraints.Builder<_B> withInternalname(final String internalname) {
            this.internalname = internalname;
            return this;
        }

        /**
         * Sets the new value of "mnemonic" (any previous value will be replaced)
         * 
         * @param mnemonic
         *     New value of the "mnemonic" property.
         */
        public TabbedpaneConstraints.Builder<_B> withMnemonic(final String mnemonic) {
            this.mnemonic = mnemonic;
            return this;
        }

        @Override
        public TabbedpaneConstraints build() {
            if (_storedValue == null) {
                return this.init(new TabbedpaneConstraints());
            } else {
                return ((TabbedpaneConstraints) _storedValue);
            }
        }

        public TabbedpaneConstraints.Builder<_B> copyOf(final TabbedpaneConstraints _other) {
            _other.copyTo(this);
            return this;
        }

        public TabbedpaneConstraints.Builder<_B> copyOf(final TabbedpaneConstraints.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends TabbedpaneConstraints.Selector<TabbedpaneConstraints.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static TabbedpaneConstraints.Select _root() {
            return new TabbedpaneConstraints.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private Translations.Selector<TRoot, TabbedpaneConstraints.Selector<TRoot, TParent>> translations = null;
        private com.kscs.util.jaxb.Selector<TRoot, TabbedpaneConstraints.Selector<TRoot, TParent>> title = null;
        private com.kscs.util.jaxb.Selector<TRoot, TabbedpaneConstraints.Selector<TRoot, TParent>> enabled = null;
        private com.kscs.util.jaxb.Selector<TRoot, TabbedpaneConstraints.Selector<TRoot, TParent>> resourceId = null;
        private com.kscs.util.jaxb.Selector<TRoot, TabbedpaneConstraints.Selector<TRoot, TParent>> internalname = null;
        private com.kscs.util.jaxb.Selector<TRoot, TabbedpaneConstraints.Selector<TRoot, TParent>> mnemonic = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.translations!= null) {
                products.put("translations", this.translations.init());
            }
            if (this.title!= null) {
                products.put("title", this.title.init());
            }
            if (this.enabled!= null) {
                products.put("enabled", this.enabled.init());
            }
            if (this.resourceId!= null) {
                products.put("resourceId", this.resourceId.init());
            }
            if (this.internalname!= null) {
                products.put("internalname", this.internalname.init());
            }
            if (this.mnemonic!= null) {
                products.put("mnemonic", this.mnemonic.init());
            }
            return products;
        }

        public Translations.Selector<TRoot, TabbedpaneConstraints.Selector<TRoot, TParent>> translations() {
            return ((this.translations == null)?this.translations = new Translations.Selector<TRoot, TabbedpaneConstraints.Selector<TRoot, TParent>>(this._root, this, "translations"):this.translations);
        }

        public com.kscs.util.jaxb.Selector<TRoot, TabbedpaneConstraints.Selector<TRoot, TParent>> title() {
            return ((this.title == null)?this.title = new com.kscs.util.jaxb.Selector<TRoot, TabbedpaneConstraints.Selector<TRoot, TParent>>(this._root, this, "title"):this.title);
        }

        public com.kscs.util.jaxb.Selector<TRoot, TabbedpaneConstraints.Selector<TRoot, TParent>> enabled() {
            return ((this.enabled == null)?this.enabled = new com.kscs.util.jaxb.Selector<TRoot, TabbedpaneConstraints.Selector<TRoot, TParent>>(this._root, this, "enabled"):this.enabled);
        }

        public com.kscs.util.jaxb.Selector<TRoot, TabbedpaneConstraints.Selector<TRoot, TParent>> resourceId() {
            return ((this.resourceId == null)?this.resourceId = new com.kscs.util.jaxb.Selector<TRoot, TabbedpaneConstraints.Selector<TRoot, TParent>>(this._root, this, "resourceId"):this.resourceId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, TabbedpaneConstraints.Selector<TRoot, TParent>> internalname() {
            return ((this.internalname == null)?this.internalname = new com.kscs.util.jaxb.Selector<TRoot, TabbedpaneConstraints.Selector<TRoot, TParent>>(this._root, this, "internalname"):this.internalname);
        }

        public com.kscs.util.jaxb.Selector<TRoot, TabbedpaneConstraints.Selector<TRoot, TParent>> mnemonic() {
            return ((this.mnemonic == null)?this.mnemonic = new com.kscs.util.jaxb.Selector<TRoot, TabbedpaneConstraints.Selector<TRoot, TParent>>(this._root, this, "mnemonic"):this.mnemonic);
        }

    }

}
