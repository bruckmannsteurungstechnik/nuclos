
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="lang" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="text" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "translation")
public class Translation implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "lang", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String lang;
    @XmlAttribute(name = "text", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String text;

    /**
     * Gets the value of the lang property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLang() {
        return lang;
    }

    /**
     * Sets the value of the lang property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLang(String value) {
        this.lang = value;
    }

    /**
     * Gets the value of the text property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getText() {
        return text;
    }

    /**
     * Sets the value of the text property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setText(String value) {
        this.text = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theLang;
            theLang = this.getLang();
            strategy.appendField(locator, this, "lang", buffer, theLang);
        }
        {
            String theText;
            theText = this.getText();
            strategy.appendField(locator, this, "text", buffer, theText);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Translation)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Translation that = ((Translation) object);
        {
            String lhsLang;
            lhsLang = this.getLang();
            String rhsLang;
            rhsLang = that.getLang();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "lang", lhsLang), LocatorUtils.property(thatLocator, "lang", rhsLang), lhsLang, rhsLang)) {
                return false;
            }
        }
        {
            String lhsText;
            lhsText = this.getText();
            String rhsText;
            rhsText = that.getText();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "text", lhsText), LocatorUtils.property(thatLocator, "text", rhsText), lhsText, rhsText)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theLang;
            theLang = this.getLang();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "lang", theLang), currentHashCode, theLang);
        }
        {
            String theText;
            theText = this.getText();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "text", theText), currentHashCode, theText);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Translation) {
            final Translation copy = ((Translation) draftCopy);
            if (this.lang!= null) {
                String sourceLang;
                sourceLang = this.getLang();
                String copyLang = ((String) strategy.copy(LocatorUtils.property(locator, "lang", sourceLang), sourceLang));
                copy.setLang(copyLang);
            } else {
                copy.lang = null;
            }
            if (this.text!= null) {
                String sourceText;
                sourceText = this.getText();
                String copyText = ((String) strategy.copy(LocatorUtils.property(locator, "text", sourceText), sourceText));
                copy.setText(copyText);
            } else {
                copy.text = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Translation();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Translation.Builder<_B> _other) {
        _other.lang = this.lang;
        _other.text = this.text;
    }

    public<_B >Translation.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Translation.Builder<_B>(_parentBuilder, this, true);
    }

    public Translation.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Translation.Builder<Void> builder() {
        return new Translation.Builder<Void>(null, null, false);
    }

    public static<_B >Translation.Builder<_B> copyOf(final Translation _other) {
        final Translation.Builder<_B> _newBuilder = new Translation.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Translation.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree langPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("lang"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(langPropertyTree!= null):((langPropertyTree == null)||(!langPropertyTree.isLeaf())))) {
            _other.lang = this.lang;
        }
        final PropertyTree textPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("text"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(textPropertyTree!= null):((textPropertyTree == null)||(!textPropertyTree.isLeaf())))) {
            _other.text = this.text;
        }
    }

    public<_B >Translation.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Translation.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Translation.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Translation.Builder<_B> copyOf(final Translation _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Translation.Builder<_B> _newBuilder = new Translation.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Translation.Builder<Void> copyExcept(final Translation _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Translation.Builder<Void> copyOnly(final Translation _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Translation _storedValue;
        private String lang;
        private String text;

        public Builder(final _B _parentBuilder, final Translation _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.lang = _other.lang;
                    this.text = _other.text;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Translation _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree langPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("lang"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(langPropertyTree!= null):((langPropertyTree == null)||(!langPropertyTree.isLeaf())))) {
                        this.lang = _other.lang;
                    }
                    final PropertyTree textPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("text"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(textPropertyTree!= null):((textPropertyTree == null)||(!textPropertyTree.isLeaf())))) {
                        this.text = _other.text;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Translation >_P init(final _P _product) {
            _product.lang = this.lang;
            _product.text = this.text;
            return _product;
        }

        /**
         * Sets the new value of "lang" (any previous value will be replaced)
         * 
         * @param lang
         *     New value of the "lang" property.
         */
        public Translation.Builder<_B> withLang(final String lang) {
            this.lang = lang;
            return this;
        }

        /**
         * Sets the new value of "text" (any previous value will be replaced)
         * 
         * @param text
         *     New value of the "text" property.
         */
        public Translation.Builder<_B> withText(final String text) {
            this.text = text;
            return this;
        }

        @Override
        public Translation build() {
            if (_storedValue == null) {
                return this.init(new Translation());
            } else {
                return ((Translation) _storedValue);
            }
        }

        public Translation.Builder<_B> copyOf(final Translation _other) {
            _other.copyTo(this);
            return this;
        }

        public Translation.Builder<_B> copyOf(final Translation.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Translation.Selector<Translation.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Translation.Select _root() {
            return new Translation.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, Translation.Selector<TRoot, TParent>> lang = null;
        private com.kscs.util.jaxb.Selector<TRoot, Translation.Selector<TRoot, TParent>> text = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.lang!= null) {
                products.put("lang", this.lang.init());
            }
            if (this.text!= null) {
                products.put("text", this.text.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, Translation.Selector<TRoot, TParent>> lang() {
            return ((this.lang == null)?this.lang = new com.kscs.util.jaxb.Selector<TRoot, Translation.Selector<TRoot, TParent>>(this._root, this, "lang"):this.lang);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Translation.Selector<TRoot, TParent>> text() {
            return ((this.text == null)?this.text = new com.kscs.util.jaxb.Selector<TRoot, Translation.Selector<TRoot, TParent>>(this._root, this, "text"):this.text);
        }

    }

}
