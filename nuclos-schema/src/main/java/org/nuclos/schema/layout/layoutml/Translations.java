
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}translation" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "translation"
})
@XmlRootElement(name = "translations")
public class Translations implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected List<Translation> translation;

    /**
     * Gets the value of the translation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the translation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTranslation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Translation }
     * 
     * 
     */
    public List<Translation> getTranslation() {
        if (translation == null) {
            translation = new ArrayList<Translation>();
        }
        return this.translation;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<Translation> theTranslation;
            theTranslation = (((this.translation!= null)&&(!this.translation.isEmpty()))?this.getTranslation():null);
            strategy.appendField(locator, this, "translation", buffer, theTranslation);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Translations)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Translations that = ((Translations) object);
        {
            List<Translation> lhsTranslation;
            lhsTranslation = (((this.translation!= null)&&(!this.translation.isEmpty()))?this.getTranslation():null);
            List<Translation> rhsTranslation;
            rhsTranslation = (((that.translation!= null)&&(!that.translation.isEmpty()))?that.getTranslation():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "translation", lhsTranslation), LocatorUtils.property(thatLocator, "translation", rhsTranslation), lhsTranslation, rhsTranslation)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<Translation> theTranslation;
            theTranslation = (((this.translation!= null)&&(!this.translation.isEmpty()))?this.getTranslation():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "translation", theTranslation), currentHashCode, theTranslation);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Translations) {
            final Translations copy = ((Translations) draftCopy);
            if ((this.translation!= null)&&(!this.translation.isEmpty())) {
                List<Translation> sourceTranslation;
                sourceTranslation = (((this.translation!= null)&&(!this.translation.isEmpty()))?this.getTranslation():null);
                @SuppressWarnings("unchecked")
                List<Translation> copyTranslation = ((List<Translation> ) strategy.copy(LocatorUtils.property(locator, "translation", sourceTranslation), sourceTranslation));
                copy.translation = null;
                if (copyTranslation!= null) {
                    List<Translation> uniqueTranslationl = copy.getTranslation();
                    uniqueTranslationl.addAll(copyTranslation);
                }
            } else {
                copy.translation = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Translations();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Translations.Builder<_B> _other) {
        if (this.translation == null) {
            _other.translation = null;
        } else {
            _other.translation = new ArrayList<Translation.Builder<Translations.Builder<_B>>>();
            for (Translation _item: this.translation) {
                _other.translation.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
    }

    public<_B >Translations.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Translations.Builder<_B>(_parentBuilder, this, true);
    }

    public Translations.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Translations.Builder<Void> builder() {
        return new Translations.Builder<Void>(null, null, false);
    }

    public static<_B >Translations.Builder<_B> copyOf(final Translations _other) {
        final Translations.Builder<_B> _newBuilder = new Translations.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Translations.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree translationPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("translation"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(translationPropertyTree!= null):((translationPropertyTree == null)||(!translationPropertyTree.isLeaf())))) {
            if (this.translation == null) {
                _other.translation = null;
            } else {
                _other.translation = new ArrayList<Translation.Builder<Translations.Builder<_B>>>();
                for (Translation _item: this.translation) {
                    _other.translation.add(((_item == null)?null:_item.newCopyBuilder(_other, translationPropertyTree, _propertyTreeUse)));
                }
            }
        }
    }

    public<_B >Translations.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Translations.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Translations.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Translations.Builder<_B> copyOf(final Translations _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Translations.Builder<_B> _newBuilder = new Translations.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Translations.Builder<Void> copyExcept(final Translations _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Translations.Builder<Void> copyOnly(final Translations _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Translations _storedValue;
        private List<Translation.Builder<Translations.Builder<_B>>> translation;

        public Builder(final _B _parentBuilder, final Translations _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.translation == null) {
                        this.translation = null;
                    } else {
                        this.translation = new ArrayList<Translation.Builder<Translations.Builder<_B>>>();
                        for (Translation _item: _other.translation) {
                            this.translation.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Translations _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree translationPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("translation"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(translationPropertyTree!= null):((translationPropertyTree == null)||(!translationPropertyTree.isLeaf())))) {
                        if (_other.translation == null) {
                            this.translation = null;
                        } else {
                            this.translation = new ArrayList<Translation.Builder<Translations.Builder<_B>>>();
                            for (Translation _item: _other.translation) {
                                this.translation.add(((_item == null)?null:_item.newCopyBuilder(this, translationPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Translations >_P init(final _P _product) {
            if (this.translation!= null) {
                final List<Translation> translation = new ArrayList<Translation>(this.translation.size());
                for (Translation.Builder<Translations.Builder<_B>> _item: this.translation) {
                    translation.add(_item.build());
                }
                _product.translation = translation;
            }
            return _product;
        }

        /**
         * Adds the given items to the value of "translation"
         * 
         * @param translation
         *     Items to add to the value of the "translation" property
         */
        public Translations.Builder<_B> addTranslation(final Iterable<? extends Translation> translation) {
            if (translation!= null) {
                if (this.translation == null) {
                    this.translation = new ArrayList<Translation.Builder<Translations.Builder<_B>>>();
                }
                for (Translation _item: translation) {
                    this.translation.add(new Translation.Builder<Translations.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "translation" (any previous value will be replaced)
         * 
         * @param translation
         *     New value of the "translation" property.
         */
        public Translations.Builder<_B> withTranslation(final Iterable<? extends Translation> translation) {
            if (this.translation!= null) {
                this.translation.clear();
            }
            return addTranslation(translation);
        }

        /**
         * Adds the given items to the value of "translation"
         * 
         * @param translation
         *     Items to add to the value of the "translation" property
         */
        public Translations.Builder<_B> addTranslation(Translation... translation) {
            addTranslation(Arrays.asList(translation));
            return this;
        }

        /**
         * Sets the new value of "translation" (any previous value will be replaced)
         * 
         * @param translation
         *     New value of the "translation" property.
         */
        public Translations.Builder<_B> withTranslation(Translation... translation) {
            withTranslation(Arrays.asList(translation));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Translation" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Translation.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Translation" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Translation.Builder#end()} to return to the current builder.
         */
        public Translation.Builder<? extends Translations.Builder<_B>> addTranslation() {
            if (this.translation == null) {
                this.translation = new ArrayList<Translation.Builder<Translations.Builder<_B>>>();
            }
            final Translation.Builder<Translations.Builder<_B>> translation_Builder = new Translation.Builder<Translations.Builder<_B>>(this, null, false);
            this.translation.add(translation_Builder);
            return translation_Builder;
        }

        @Override
        public Translations build() {
            if (_storedValue == null) {
                return this.init(new Translations());
            } else {
                return ((Translations) _storedValue);
            }
        }

        public Translations.Builder<_B> copyOf(final Translations _other) {
            _other.copyTo(this);
            return this;
        }

        public Translations.Builder<_B> copyOf(final Translations.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Translations.Selector<Translations.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Translations.Select _root() {
            return new Translations.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private Translation.Selector<TRoot, Translations.Selector<TRoot, TParent>> translation = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.translation!= null) {
                products.put("translation", this.translation.init());
            }
            return products;
        }

        public Translation.Selector<TRoot, Translations.Selector<TRoot, TParent>> translation() {
            return ((this.translation == null)?this.translation = new Translation.Selector<TRoot, Translations.Selector<TRoot, TParent>>(this._root, this, "translation"):this.translation);
        }

    }

}
