
package org.nuclos.schema.layout.rule;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.nuclos.schema.layout.rule package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.nuclos.schema.layout.rule
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Rules }
     * 
     */
    public Rules createRules() {
        return new Rules();
    }

    /**
     * Create an instance of {@link Rule }
     * 
     */
    public Rule createRule() {
        return new Rule();
    }

    /**
     * Create an instance of {@link RuleEvent }
     * 
     */
    public RuleEvent createRuleEvent() {
        return new RuleEvent();
    }

    /**
     * Create an instance of {@link RuleActionTransferLookedupValue }
     * 
     */
    public RuleActionTransferLookedupValue createRuleActionTransferLookedupValue() {
        return new RuleActionTransferLookedupValue();
    }

    /**
     * Create an instance of {@link RuleActionReinitSubform }
     * 
     */
    public RuleActionReinitSubform createRuleActionReinitSubform() {
        return new RuleActionReinitSubform();
    }

    /**
     * Create an instance of {@link RuleActionClear }
     * 
     */
    public RuleActionClear createRuleActionClear() {
        return new RuleActionClear();
    }

    /**
     * Create an instance of {@link RuleActionEnable }
     * 
     */
    public RuleActionEnable createRuleActionEnable() {
        return new RuleActionEnable();
    }

    /**
     * Create an instance of {@link RuleActionRefreshValuelist }
     * 
     */
    public RuleActionRefreshValuelist createRuleActionRefreshValuelist() {
        return new RuleActionRefreshValuelist();
    }

}
