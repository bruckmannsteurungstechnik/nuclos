
package org.nuclos.schema.layout.rule;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Clears a target component.
 * 
 * <p>Java class for rule-action-clear complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rule-action-clear"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:org.nuclos.schema.layout.rule}rule-action"&gt;
 *       &lt;attribute name="entity" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rule-action-clear")
public class RuleActionClear
    extends RuleAction
    implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "entity")
    protected String entity;

    /**
     * Gets the value of the entity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntity() {
        return entity;
    }

    /**
     * Sets the value of the entity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntity(String value) {
        this.entity = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        super.appendFields(locator, buffer, strategy);
        {
            String theEntity;
            theEntity = this.getEntity();
            strategy.appendField(locator, this, "entity", buffer, theEntity);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof RuleActionClear)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final RuleActionClear that = ((RuleActionClear) object);
        {
            String lhsEntity;
            lhsEntity = this.getEntity();
            String rhsEntity;
            rhsEntity = that.getEntity();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entity", lhsEntity), LocatorUtils.property(thatLocator, "entity", rhsEntity), lhsEntity, rhsEntity)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = super.hashCode(locator, strategy);
        {
            String theEntity;
            theEntity = this.getEntity();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entity", theEntity), currentHashCode, theEntity);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof RuleActionClear) {
            final RuleActionClear copy = ((RuleActionClear) draftCopy);
            if (this.entity!= null) {
                String sourceEntity;
                sourceEntity = this.getEntity();
                String copyEntity = ((String) strategy.copy(LocatorUtils.property(locator, "entity", sourceEntity), sourceEntity));
                copy.setEntity(copyEntity);
            } else {
                copy.entity = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new RuleActionClear();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final RuleActionClear.Builder<_B> _other) {
        super.copyTo(_other);
        _other.entity = this.entity;
    }

    @Override
    public<_B >RuleActionClear.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new RuleActionClear.Builder<_B>(_parentBuilder, this, true);
    }

    @Override
    public RuleActionClear.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static RuleActionClear.Builder<Void> builder() {
        return new RuleActionClear.Builder<Void>(null, null, false);
    }

    public static<_B >RuleActionClear.Builder<_B> copyOf(final RuleAction _other) {
        final RuleActionClear.Builder<_B> _newBuilder = new RuleActionClear.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >RuleActionClear.Builder<_B> copyOf(final RuleActionClear _other) {
        final RuleActionClear.Builder<_B> _newBuilder = new RuleActionClear.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final RuleActionClear.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        super.copyTo(_other, _propertyTree, _propertyTreeUse);
        final PropertyTree entityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entity"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityPropertyTree!= null):((entityPropertyTree == null)||(!entityPropertyTree.isLeaf())))) {
            _other.entity = this.entity;
        }
    }

    @Override
    public<_B >RuleActionClear.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new RuleActionClear.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    @Override
    public RuleActionClear.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >RuleActionClear.Builder<_B> copyOf(final RuleAction _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final RuleActionClear.Builder<_B> _newBuilder = new RuleActionClear.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >RuleActionClear.Builder<_B> copyOf(final RuleActionClear _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final RuleActionClear.Builder<_B> _newBuilder = new RuleActionClear.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static RuleActionClear.Builder<Void> copyExcept(final RuleAction _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static RuleActionClear.Builder<Void> copyExcept(final RuleActionClear _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static RuleActionClear.Builder<Void> copyOnly(final RuleAction _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static RuleActionClear.Builder<Void> copyOnly(final RuleActionClear _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >
        extends RuleAction.Builder<_B>
        implements Buildable
    {

        private String entity;

        public Builder(final _B _parentBuilder, final RuleActionClear _other, final boolean _copy) {
            super(_parentBuilder, _other, _copy);
            if (_other!= null) {
                this.entity = _other.entity;
            }
        }

        public Builder(final _B _parentBuilder, final RuleActionClear _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            super(_parentBuilder, _other, _copy, _propertyTree, _propertyTreeUse);
            if (_other!= null) {
                final PropertyTree entityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entity"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityPropertyTree!= null):((entityPropertyTree == null)||(!entityPropertyTree.isLeaf())))) {
                    this.entity = _other.entity;
                }
            }
        }

        protected<_P extends RuleActionClear >_P init(final _P _product) {
            _product.entity = this.entity;
            return super.init(_product);
        }

        /**
         * Sets the new value of "entity" (any previous value will be replaced)
         * 
         * @param entity
         *     New value of the "entity" property.
         */
        public RuleActionClear.Builder<_B> withEntity(final String entity) {
            this.entity = entity;
            return this;
        }

        /**
         * Sets the new value of "targetcomponent" (any previous value will be replaced)
         * 
         * @param targetcomponent
         *     New value of the "targetcomponent" property.
         */
        @Override
        public RuleActionClear.Builder<_B> withTargetcomponent(final String targetcomponent) {
            super.withTargetcomponent(targetcomponent);
            return this;
        }

        @Override
        public RuleActionClear build() {
            if (_storedValue == null) {
                return this.init(new RuleActionClear());
            } else {
                return ((RuleActionClear) _storedValue);
            }
        }

        public RuleActionClear.Builder<_B> copyOf(final RuleActionClear _other) {
            _other.copyTo(this);
            return this;
        }

        public RuleActionClear.Builder<_B> copyOf(final RuleActionClear.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends RuleActionClear.Selector<RuleActionClear.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static RuleActionClear.Select _root() {
            return new RuleActionClear.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends RuleAction.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, RuleActionClear.Selector<TRoot, TParent>> entity = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.entity!= null) {
                products.put("entity", this.entity.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, RuleActionClear.Selector<TRoot, TParent>> entity() {
            return ((this.entity == null)?this.entity = new com.kscs.util.jaxb.Selector<TRoot, RuleActionClear.Selector<TRoot, TParent>>(this._root, this, "entity"):this.entity);
        }

    }

}
