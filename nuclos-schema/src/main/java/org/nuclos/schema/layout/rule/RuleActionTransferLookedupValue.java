
package org.nuclos.schema.layout.rule;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Transfers a value from one field to another component.
 * 
 * <p>Java class for rule-action-transfer-lookedup-value complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rule-action-transfer-lookedup-value"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:org.nuclos.schema.layout.rule}rule-action"&gt;
 *       &lt;attribute name="sourcefield" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rule-action-transfer-lookedup-value")
public class RuleActionTransferLookedupValue
    extends RuleAction
    implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "sourcefield")
    protected String sourcefield;

    /**
     * Gets the value of the sourcefield property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourcefield() {
        return sourcefield;
    }

    /**
     * Sets the value of the sourcefield property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourcefield(String value) {
        this.sourcefield = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        super.appendFields(locator, buffer, strategy);
        {
            String theSourcefield;
            theSourcefield = this.getSourcefield();
            strategy.appendField(locator, this, "sourcefield", buffer, theSourcefield);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof RuleActionTransferLookedupValue)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final RuleActionTransferLookedupValue that = ((RuleActionTransferLookedupValue) object);
        {
            String lhsSourcefield;
            lhsSourcefield = this.getSourcefield();
            String rhsSourcefield;
            rhsSourcefield = that.getSourcefield();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "sourcefield", lhsSourcefield), LocatorUtils.property(thatLocator, "sourcefield", rhsSourcefield), lhsSourcefield, rhsSourcefield)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = super.hashCode(locator, strategy);
        {
            String theSourcefield;
            theSourcefield = this.getSourcefield();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "sourcefield", theSourcefield), currentHashCode, theSourcefield);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof RuleActionTransferLookedupValue) {
            final RuleActionTransferLookedupValue copy = ((RuleActionTransferLookedupValue) draftCopy);
            if (this.sourcefield!= null) {
                String sourceSourcefield;
                sourceSourcefield = this.getSourcefield();
                String copySourcefield = ((String) strategy.copy(LocatorUtils.property(locator, "sourcefield", sourceSourcefield), sourceSourcefield));
                copy.setSourcefield(copySourcefield);
            } else {
                copy.sourcefield = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new RuleActionTransferLookedupValue();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final RuleActionTransferLookedupValue.Builder<_B> _other) {
        super.copyTo(_other);
        _other.sourcefield = this.sourcefield;
    }

    @Override
    public<_B >RuleActionTransferLookedupValue.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new RuleActionTransferLookedupValue.Builder<_B>(_parentBuilder, this, true);
    }

    @Override
    public RuleActionTransferLookedupValue.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static RuleActionTransferLookedupValue.Builder<Void> builder() {
        return new RuleActionTransferLookedupValue.Builder<Void>(null, null, false);
    }

    public static<_B >RuleActionTransferLookedupValue.Builder<_B> copyOf(final RuleAction _other) {
        final RuleActionTransferLookedupValue.Builder<_B> _newBuilder = new RuleActionTransferLookedupValue.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >RuleActionTransferLookedupValue.Builder<_B> copyOf(final RuleActionTransferLookedupValue _other) {
        final RuleActionTransferLookedupValue.Builder<_B> _newBuilder = new RuleActionTransferLookedupValue.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final RuleActionTransferLookedupValue.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        super.copyTo(_other, _propertyTree, _propertyTreeUse);
        final PropertyTree sourcefieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("sourcefield"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(sourcefieldPropertyTree!= null):((sourcefieldPropertyTree == null)||(!sourcefieldPropertyTree.isLeaf())))) {
            _other.sourcefield = this.sourcefield;
        }
    }

    @Override
    public<_B >RuleActionTransferLookedupValue.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new RuleActionTransferLookedupValue.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    @Override
    public RuleActionTransferLookedupValue.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >RuleActionTransferLookedupValue.Builder<_B> copyOf(final RuleAction _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final RuleActionTransferLookedupValue.Builder<_B> _newBuilder = new RuleActionTransferLookedupValue.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >RuleActionTransferLookedupValue.Builder<_B> copyOf(final RuleActionTransferLookedupValue _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final RuleActionTransferLookedupValue.Builder<_B> _newBuilder = new RuleActionTransferLookedupValue.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static RuleActionTransferLookedupValue.Builder<Void> copyExcept(final RuleAction _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static RuleActionTransferLookedupValue.Builder<Void> copyExcept(final RuleActionTransferLookedupValue _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static RuleActionTransferLookedupValue.Builder<Void> copyOnly(final RuleAction _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static RuleActionTransferLookedupValue.Builder<Void> copyOnly(final RuleActionTransferLookedupValue _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >
        extends RuleAction.Builder<_B>
        implements Buildable
    {

        private String sourcefield;

        public Builder(final _B _parentBuilder, final RuleActionTransferLookedupValue _other, final boolean _copy) {
            super(_parentBuilder, _other, _copy);
            if (_other!= null) {
                this.sourcefield = _other.sourcefield;
            }
        }

        public Builder(final _B _parentBuilder, final RuleActionTransferLookedupValue _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            super(_parentBuilder, _other, _copy, _propertyTree, _propertyTreeUse);
            if (_other!= null) {
                final PropertyTree sourcefieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("sourcefield"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(sourcefieldPropertyTree!= null):((sourcefieldPropertyTree == null)||(!sourcefieldPropertyTree.isLeaf())))) {
                    this.sourcefield = _other.sourcefield;
                }
            }
        }

        protected<_P extends RuleActionTransferLookedupValue >_P init(final _P _product) {
            _product.sourcefield = this.sourcefield;
            return super.init(_product);
        }

        /**
         * Sets the new value of "sourcefield" (any previous value will be replaced)
         * 
         * @param sourcefield
         *     New value of the "sourcefield" property.
         */
        public RuleActionTransferLookedupValue.Builder<_B> withSourcefield(final String sourcefield) {
            this.sourcefield = sourcefield;
            return this;
        }

        /**
         * Sets the new value of "targetcomponent" (any previous value will be replaced)
         * 
         * @param targetcomponent
         *     New value of the "targetcomponent" property.
         */
        @Override
        public RuleActionTransferLookedupValue.Builder<_B> withTargetcomponent(final String targetcomponent) {
            super.withTargetcomponent(targetcomponent);
            return this;
        }

        @Override
        public RuleActionTransferLookedupValue build() {
            if (_storedValue == null) {
                return this.init(new RuleActionTransferLookedupValue());
            } else {
                return ((RuleActionTransferLookedupValue) _storedValue);
            }
        }

        public RuleActionTransferLookedupValue.Builder<_B> copyOf(final RuleActionTransferLookedupValue _other) {
            _other.copyTo(this);
            return this;
        }

        public RuleActionTransferLookedupValue.Builder<_B> copyOf(final RuleActionTransferLookedupValue.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends RuleActionTransferLookedupValue.Selector<RuleActionTransferLookedupValue.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static RuleActionTransferLookedupValue.Select _root() {
            return new RuleActionTransferLookedupValue.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends RuleAction.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, RuleActionTransferLookedupValue.Selector<TRoot, TParent>> sourcefield = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.sourcefield!= null) {
                products.put("sourcefield", this.sourcefield.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, RuleActionTransferLookedupValue.Selector<TRoot, TParent>> sourcefield() {
            return ((this.sourcefield == null)?this.sourcefield = new com.kscs.util.jaxb.Selector<TRoot, RuleActionTransferLookedupValue.Selector<TRoot, TParent>>(this._root, this, "sourcefield"):this.sourcefield);
        }

    }

}
