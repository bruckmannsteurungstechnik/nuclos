
package org.nuclos.schema.layout.web;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.nuclos.schema.layout.web package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.nuclos.schema.layout.web
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link WebValuelistProvider }
     * 
     */
    public WebValuelistProvider createWebValuelistProvider() {
        return new WebValuelistProvider();
    }

    /**
     * Create an instance of {@link WebLayout }
     * 
     */
    public WebLayout createWebLayout() {
        return new WebLayout();
    }

    /**
     * Create an instance of {@link WebGrid }
     * 
     */
    public WebGrid createWebGrid() {
        return new WebGrid();
    }

    /**
     * Create an instance of {@link WebTable }
     * 
     */
    public WebTable createWebTable() {
        return new WebTable();
    }

    /**
     * Create an instance of {@link WebGridCalculated }
     * 
     */
    public WebGridCalculated createWebGridCalculated() {
        return new WebGridCalculated();
    }

    /**
     * Create an instance of {@link WebAdvancedProperty }
     * 
     */
    public WebAdvancedProperty createWebAdvancedProperty() {
        return new WebAdvancedProperty();
    }

    /**
     * Create an instance of {@link WebInputComponent }
     * 
     */
    public WebInputComponent createWebInputComponent() {
        return new WebInputComponent();
    }

    /**
     * Create an instance of {@link WebCalcCell }
     * 
     */
    public WebCalcCell createWebCalcCell() {
        return new WebCalcCell();
    }

    /**
     * Create an instance of {@link WebRow }
     * 
     */
    public WebRow createWebRow() {
        return new WebRow();
    }

    /**
     * Create an instance of {@link WebCell }
     * 
     */
    public WebCell createWebCell() {
        return new WebCell();
    }

    /**
     * Create an instance of {@link WebContainer }
     * 
     */
    public WebContainer createWebContainer() {
        return new WebContainer();
    }

    /**
     * Create an instance of {@link WebPanel }
     * 
     */
    public WebPanel createWebPanel() {
        return new WebPanel();
    }

    /**
     * Create an instance of {@link WebTab }
     * 
     */
    public WebTab createWebTab() {
        return new WebTab();
    }

    /**
     * Create an instance of {@link WebTabcontainer }
     * 
     */
    public WebTabcontainer createWebTabcontainer() {
        return new WebTabcontainer();
    }

    /**
     * Create an instance of {@link WebLabel }
     * 
     */
    public WebLabel createWebLabel() {
        return new WebLabel();
    }

    /**
     * Create an instance of {@link WebLabelStatic }
     * 
     */
    public WebLabelStatic createWebLabelStatic() {
        return new WebLabelStatic();
    }

    /**
     * Create an instance of {@link WebTextfield }
     * 
     */
    public WebTextfield createWebTextfield() {
        return new WebTextfield();
    }

    /**
     * Create an instance of {@link WebSeparator }
     * 
     */
    public WebSeparator createWebSeparator() {
        return new WebSeparator();
    }

    /**
     * Create an instance of {@link WebTitledSeparator }
     * 
     */
    public WebTitledSeparator createWebTitledSeparator() {
        return new WebTitledSeparator();
    }

    /**
     * Create an instance of {@link WebPassword }
     * 
     */
    public WebPassword createWebPassword() {
        return new WebPassword();
    }

    /**
     * Create an instance of {@link WebEmail }
     * 
     */
    public WebEmail createWebEmail() {
        return new WebEmail();
    }

    /**
     * Create an instance of {@link WebHyperlink }
     * 
     */
    public WebHyperlink createWebHyperlink() {
        return new WebHyperlink();
    }

    /**
     * Create an instance of {@link WebPhonenumber }
     * 
     */
    public WebPhonenumber createWebPhonenumber() {
        return new WebPhonenumber();
    }

    /**
     * Create an instance of {@link WebTextarea }
     * 
     */
    public WebTextarea createWebTextarea() {
        return new WebTextarea();
    }

    /**
     * Create an instance of {@link WebHtmlEditor }
     * 
     */
    public WebHtmlEditor createWebHtmlEditor() {
        return new WebHtmlEditor();
    }

    /**
     * Create an instance of {@link WebDatechooser }
     * 
     */
    public WebDatechooser createWebDatechooser() {
        return new WebDatechooser();
    }

    /**
     * Create an instance of {@link WebListofvalues }
     * 
     */
    public WebListofvalues createWebListofvalues() {
        return new WebListofvalues();
    }

    /**
     * Create an instance of {@link WebCombobox }
     * 
     */
    public WebCombobox createWebCombobox() {
        return new WebCombobox();
    }

    /**
     * Create an instance of {@link WebFile }
     * 
     */
    public WebFile createWebFile() {
        return new WebFile();
    }

    /**
     * Create an instance of {@link WebButtonChangeState }
     * 
     */
    public WebButtonChangeState createWebButtonChangeState() {
        return new WebButtonChangeState();
    }

    /**
     * Create an instance of {@link WebButtonExecuteRule }
     * 
     */
    public WebButtonExecuteRule createWebButtonExecuteRule() {
        return new WebButtonExecuteRule();
    }

    /**
     * Create an instance of {@link WebButtonGenerateObject }
     * 
     */
    public WebButtonGenerateObject createWebButtonGenerateObject() {
        return new WebButtonGenerateObject();
    }

    /**
     * Create an instance of {@link WebButtonHyperlink }
     * 
     */
    public WebButtonHyperlink createWebButtonHyperlink() {
        return new WebButtonHyperlink();
    }

    /**
     * Create an instance of {@link WebButtonAddon }
     * 
     */
    public WebButtonAddon createWebButtonAddon() {
        return new WebButtonAddon();
    }

    /**
     * Create an instance of {@link WebButtonDummy }
     * 
     */
    public WebButtonDummy createWebButtonDummy() {
        return new WebButtonDummy();
    }

    /**
     * Create an instance of {@link WebCheckbox }
     * 
     */
    public WebCheckbox createWebCheckbox() {
        return new WebCheckbox();
    }

    /**
     * Create an instance of {@link WebMatrix }
     * 
     */
    public WebMatrix createWebMatrix() {
        return new WebMatrix();
    }

    /**
     * Create an instance of {@link WebMatrixColumn }
     * 
     */
    public WebMatrixColumn createWebMatrixColumn() {
        return new WebMatrixColumn();
    }

    /**
     * Create an instance of {@link WebSubform }
     * 
     */
    public WebSubform createWebSubform() {
        return new WebSubform();
    }

    /**
     * Create an instance of {@link WebSubformColumn }
     * 
     */
    public WebSubformColumn createWebSubformColumn() {
        return new WebSubformColumn();
    }

    /**
     * Create an instance of {@link WebOptiongroup }
     * 
     */
    public WebOptiongroup createWebOptiongroup() {
        return new WebOptiongroup();
    }

    /**
     * Create an instance of {@link WebOptions }
     * 
     */
    public WebOptions createWebOptions() {
        return new WebOptions();
    }

    /**
     * Create an instance of {@link WebOption }
     * 
     */
    public WebOption createWebOption() {
        return new WebOption();
    }

    /**
     * Create an instance of {@link WebAddon }
     * 
     */
    public WebAddon createWebAddon() {
        return new WebAddon();
    }

    /**
     * Create an instance of {@link WebColorchooser }
     * 
     */
    public WebColorchooser createWebColorchooser() {
        return new WebColorchooser();
    }

    /**
     * Create an instance of {@link WebValuelistProvider.Parameter }
     * 
     */
    public WebValuelistProvider.Parameter createWebValuelistProviderParameter() {
        return new WebValuelistProvider.Parameter();
    }

}
