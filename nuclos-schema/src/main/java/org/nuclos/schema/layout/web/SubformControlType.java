
package org.nuclos.schema.layout.web;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for subform-control-type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="subform-control-type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="TEXTFIELD"/&gt;
 *     &lt;enumeration value="TEXTAREA"/&gt;
 *     &lt;enumeration value="EMAIL"/&gt;
 *     &lt;enumeration value="HYPERLINK"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "subform-control-type")
@XmlEnum
public enum SubformControlType {

    TEXTFIELD,
    TEXTAREA,
    EMAIL,
    HYPERLINK;

    public String value() {
        return name();
    }

    public static SubformControlType fromValue(String v) {
        return valueOf(v);
    }

}
