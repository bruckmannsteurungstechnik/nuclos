
package org.nuclos.schema.layout.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * An absolute positioned, calculated cell.
 * 
 * <p>Java class for web-calc-cell complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="web-calc-cell"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="components" type="{urn:org.nuclos.schema.layout.web}web-component" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="left" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="top" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="width" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="height" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "web-calc-cell", propOrder = {
    "components"
})
public class WebCalcCell implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected List<WebComponent> components;
    @XmlAttribute(name = "left", required = true)
    protected String left;
    @XmlAttribute(name = "top", required = true)
    protected String top;
    @XmlAttribute(name = "width", required = true)
    protected String width;
    @XmlAttribute(name = "height", required = true)
    protected String height;

    /**
     * Gets the value of the components property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the components property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComponents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WebComponent }
     * 
     * 
     */
    public List<WebComponent> getComponents() {
        if (components == null) {
            components = new ArrayList<WebComponent>();
        }
        return this.components;
    }

    /**
     * Gets the value of the left property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLeft() {
        return left;
    }

    /**
     * Sets the value of the left property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLeft(String value) {
        this.left = value;
    }

    /**
     * Gets the value of the top property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTop() {
        return top;
    }

    /**
     * Sets the value of the top property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTop(String value) {
        this.top = value;
    }

    /**
     * Gets the value of the width property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWidth() {
        return width;
    }

    /**
     * Sets the value of the width property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWidth(String value) {
        this.width = value;
    }

    /**
     * Gets the value of the height property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHeight() {
        return height;
    }

    /**
     * Sets the value of the height property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHeight(String value) {
        this.height = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<WebComponent> theComponents;
            theComponents = (((this.components!= null)&&(!this.components.isEmpty()))?this.getComponents():null);
            strategy.appendField(locator, this, "components", buffer, theComponents);
        }
        {
            String theLeft;
            theLeft = this.getLeft();
            strategy.appendField(locator, this, "left", buffer, theLeft);
        }
        {
            String theTop;
            theTop = this.getTop();
            strategy.appendField(locator, this, "top", buffer, theTop);
        }
        {
            String theWidth;
            theWidth = this.getWidth();
            strategy.appendField(locator, this, "width", buffer, theWidth);
        }
        {
            String theHeight;
            theHeight = this.getHeight();
            strategy.appendField(locator, this, "height", buffer, theHeight);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WebCalcCell)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final WebCalcCell that = ((WebCalcCell) object);
        {
            List<WebComponent> lhsComponents;
            lhsComponents = (((this.components!= null)&&(!this.components.isEmpty()))?this.getComponents():null);
            List<WebComponent> rhsComponents;
            rhsComponents = (((that.components!= null)&&(!that.components.isEmpty()))?that.getComponents():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "components", lhsComponents), LocatorUtils.property(thatLocator, "components", rhsComponents), lhsComponents, rhsComponents)) {
                return false;
            }
        }
        {
            String lhsLeft;
            lhsLeft = this.getLeft();
            String rhsLeft;
            rhsLeft = that.getLeft();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "left", lhsLeft), LocatorUtils.property(thatLocator, "left", rhsLeft), lhsLeft, rhsLeft)) {
                return false;
            }
        }
        {
            String lhsTop;
            lhsTop = this.getTop();
            String rhsTop;
            rhsTop = that.getTop();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "top", lhsTop), LocatorUtils.property(thatLocator, "top", rhsTop), lhsTop, rhsTop)) {
                return false;
            }
        }
        {
            String lhsWidth;
            lhsWidth = this.getWidth();
            String rhsWidth;
            rhsWidth = that.getWidth();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "width", lhsWidth), LocatorUtils.property(thatLocator, "width", rhsWidth), lhsWidth, rhsWidth)) {
                return false;
            }
        }
        {
            String lhsHeight;
            lhsHeight = this.getHeight();
            String rhsHeight;
            rhsHeight = that.getHeight();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "height", lhsHeight), LocatorUtils.property(thatLocator, "height", rhsHeight), lhsHeight, rhsHeight)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<WebComponent> theComponents;
            theComponents = (((this.components!= null)&&(!this.components.isEmpty()))?this.getComponents():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "components", theComponents), currentHashCode, theComponents);
        }
        {
            String theLeft;
            theLeft = this.getLeft();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "left", theLeft), currentHashCode, theLeft);
        }
        {
            String theTop;
            theTop = this.getTop();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "top", theTop), currentHashCode, theTop);
        }
        {
            String theWidth;
            theWidth = this.getWidth();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "width", theWidth), currentHashCode, theWidth);
        }
        {
            String theHeight;
            theHeight = this.getHeight();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "height", theHeight), currentHashCode, theHeight);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof WebCalcCell) {
            final WebCalcCell copy = ((WebCalcCell) draftCopy);
            if ((this.components!= null)&&(!this.components.isEmpty())) {
                List<WebComponent> sourceComponents;
                sourceComponents = (((this.components!= null)&&(!this.components.isEmpty()))?this.getComponents():null);
                @SuppressWarnings("unchecked")
                List<WebComponent> copyComponents = ((List<WebComponent> ) strategy.copy(LocatorUtils.property(locator, "components", sourceComponents), sourceComponents));
                copy.components = null;
                if (copyComponents!= null) {
                    List<WebComponent> uniqueComponentsl = copy.getComponents();
                    uniqueComponentsl.addAll(copyComponents);
                }
            } else {
                copy.components = null;
            }
            if (this.left!= null) {
                String sourceLeft;
                sourceLeft = this.getLeft();
                String copyLeft = ((String) strategy.copy(LocatorUtils.property(locator, "left", sourceLeft), sourceLeft));
                copy.setLeft(copyLeft);
            } else {
                copy.left = null;
            }
            if (this.top!= null) {
                String sourceTop;
                sourceTop = this.getTop();
                String copyTop = ((String) strategy.copy(LocatorUtils.property(locator, "top", sourceTop), sourceTop));
                copy.setTop(copyTop);
            } else {
                copy.top = null;
            }
            if (this.width!= null) {
                String sourceWidth;
                sourceWidth = this.getWidth();
                String copyWidth = ((String) strategy.copy(LocatorUtils.property(locator, "width", sourceWidth), sourceWidth));
                copy.setWidth(copyWidth);
            } else {
                copy.width = null;
            }
            if (this.height!= null) {
                String sourceHeight;
                sourceHeight = this.getHeight();
                String copyHeight = ((String) strategy.copy(LocatorUtils.property(locator, "height", sourceHeight), sourceHeight));
                copy.setHeight(copyHeight);
            } else {
                copy.height = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new WebCalcCell();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebCalcCell.Builder<_B> _other) {
        if (this.components == null) {
            _other.components = null;
        } else {
            _other.components = new ArrayList<WebComponent.Builder<WebCalcCell.Builder<_B>>>();
            for (WebComponent _item: this.components) {
                _other.components.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.left = this.left;
        _other.top = this.top;
        _other.width = this.width;
        _other.height = this.height;
    }

    public<_B >WebCalcCell.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new WebCalcCell.Builder<_B>(_parentBuilder, this, true);
    }

    public WebCalcCell.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static WebCalcCell.Builder<Void> builder() {
        return new WebCalcCell.Builder<Void>(null, null, false);
    }

    public static<_B >WebCalcCell.Builder<_B> copyOf(final WebCalcCell _other) {
        final WebCalcCell.Builder<_B> _newBuilder = new WebCalcCell.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebCalcCell.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree componentsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("components"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(componentsPropertyTree!= null):((componentsPropertyTree == null)||(!componentsPropertyTree.isLeaf())))) {
            if (this.components == null) {
                _other.components = null;
            } else {
                _other.components = new ArrayList<WebComponent.Builder<WebCalcCell.Builder<_B>>>();
                for (WebComponent _item: this.components) {
                    _other.components.add(((_item == null)?null:_item.newCopyBuilder(_other, componentsPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree leftPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("left"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(leftPropertyTree!= null):((leftPropertyTree == null)||(!leftPropertyTree.isLeaf())))) {
            _other.left = this.left;
        }
        final PropertyTree topPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("top"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(topPropertyTree!= null):((topPropertyTree == null)||(!topPropertyTree.isLeaf())))) {
            _other.top = this.top;
        }
        final PropertyTree widthPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("width"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(widthPropertyTree!= null):((widthPropertyTree == null)||(!widthPropertyTree.isLeaf())))) {
            _other.width = this.width;
        }
        final PropertyTree heightPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("height"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(heightPropertyTree!= null):((heightPropertyTree == null)||(!heightPropertyTree.isLeaf())))) {
            _other.height = this.height;
        }
    }

    public<_B >WebCalcCell.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new WebCalcCell.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public WebCalcCell.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >WebCalcCell.Builder<_B> copyOf(final WebCalcCell _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebCalcCell.Builder<_B> _newBuilder = new WebCalcCell.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static WebCalcCell.Builder<Void> copyExcept(final WebCalcCell _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebCalcCell.Builder<Void> copyOnly(final WebCalcCell _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final WebCalcCell _storedValue;
        private List<WebComponent.Builder<WebCalcCell.Builder<_B>>> components;
        private String left;
        private String top;
        private String width;
        private String height;

        public Builder(final _B _parentBuilder, final WebCalcCell _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.components == null) {
                        this.components = null;
                    } else {
                        this.components = new ArrayList<WebComponent.Builder<WebCalcCell.Builder<_B>>>();
                        for (WebComponent _item: _other.components) {
                            this.components.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.left = _other.left;
                    this.top = _other.top;
                    this.width = _other.width;
                    this.height = _other.height;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final WebCalcCell _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree componentsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("components"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(componentsPropertyTree!= null):((componentsPropertyTree == null)||(!componentsPropertyTree.isLeaf())))) {
                        if (_other.components == null) {
                            this.components = null;
                        } else {
                            this.components = new ArrayList<WebComponent.Builder<WebCalcCell.Builder<_B>>>();
                            for (WebComponent _item: _other.components) {
                                this.components.add(((_item == null)?null:_item.newCopyBuilder(this, componentsPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree leftPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("left"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(leftPropertyTree!= null):((leftPropertyTree == null)||(!leftPropertyTree.isLeaf())))) {
                        this.left = _other.left;
                    }
                    final PropertyTree topPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("top"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(topPropertyTree!= null):((topPropertyTree == null)||(!topPropertyTree.isLeaf())))) {
                        this.top = _other.top;
                    }
                    final PropertyTree widthPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("width"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(widthPropertyTree!= null):((widthPropertyTree == null)||(!widthPropertyTree.isLeaf())))) {
                        this.width = _other.width;
                    }
                    final PropertyTree heightPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("height"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(heightPropertyTree!= null):((heightPropertyTree == null)||(!heightPropertyTree.isLeaf())))) {
                        this.height = _other.height;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends WebCalcCell >_P init(final _P _product) {
            if (this.components!= null) {
                final List<WebComponent> components = new ArrayList<WebComponent>(this.components.size());
                for (WebComponent.Builder<WebCalcCell.Builder<_B>> _item: this.components) {
                    components.add(_item.build());
                }
                _product.components = components;
            }
            _product.left = this.left;
            _product.top = this.top;
            _product.width = this.width;
            _product.height = this.height;
            return _product;
        }

        /**
         * Adds the given items to the value of "components"
         * 
         * @param components
         *     Items to add to the value of the "components" property
         */
        public WebCalcCell.Builder<_B> addComponents(final Iterable<? extends WebComponent> components) {
            if (components!= null) {
                if (this.components == null) {
                    this.components = new ArrayList<WebComponent.Builder<WebCalcCell.Builder<_B>>>();
                }
                for (WebComponent _item: components) {
                    this.components.add(new WebComponent.Builder<WebCalcCell.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "components" (any previous value will be replaced)
         * 
         * @param components
         *     New value of the "components" property.
         */
        public WebCalcCell.Builder<_B> withComponents(final Iterable<? extends WebComponent> components) {
            if (this.components!= null) {
                this.components.clear();
            }
            return addComponents(components);
        }

        /**
         * Adds the given items to the value of "components"
         * 
         * @param components
         *     Items to add to the value of the "components" property
         */
        public WebCalcCell.Builder<_B> addComponents(WebComponent... components) {
            addComponents(Arrays.asList(components));
            return this;
        }

        /**
         * Sets the new value of "components" (any previous value will be replaced)
         * 
         * @param components
         *     New value of the "components" property.
         */
        public WebCalcCell.Builder<_B> withComponents(WebComponent... components) {
            withComponents(Arrays.asList(components));
            return this;
        }

        /**
         * Sets the new value of "left" (any previous value will be replaced)
         * 
         * @param left
         *     New value of the "left" property.
         */
        public WebCalcCell.Builder<_B> withLeft(final String left) {
            this.left = left;
            return this;
        }

        /**
         * Sets the new value of "top" (any previous value will be replaced)
         * 
         * @param top
         *     New value of the "top" property.
         */
        public WebCalcCell.Builder<_B> withTop(final String top) {
            this.top = top;
            return this;
        }

        /**
         * Sets the new value of "width" (any previous value will be replaced)
         * 
         * @param width
         *     New value of the "width" property.
         */
        public WebCalcCell.Builder<_B> withWidth(final String width) {
            this.width = width;
            return this;
        }

        /**
         * Sets the new value of "height" (any previous value will be replaced)
         * 
         * @param height
         *     New value of the "height" property.
         */
        public WebCalcCell.Builder<_B> withHeight(final String height) {
            this.height = height;
            return this;
        }

        @Override
        public WebCalcCell build() {
            if (_storedValue == null) {
                return this.init(new WebCalcCell());
            } else {
                return ((WebCalcCell) _storedValue);
            }
        }

        public WebCalcCell.Builder<_B> copyOf(final WebCalcCell _other) {
            _other.copyTo(this);
            return this;
        }

        public WebCalcCell.Builder<_B> copyOf(final WebCalcCell.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends WebCalcCell.Selector<WebCalcCell.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static WebCalcCell.Select _root() {
            return new WebCalcCell.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private WebComponent.Selector<TRoot, WebCalcCell.Selector<TRoot, TParent>> components = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebCalcCell.Selector<TRoot, TParent>> left = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebCalcCell.Selector<TRoot, TParent>> top = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebCalcCell.Selector<TRoot, TParent>> width = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebCalcCell.Selector<TRoot, TParent>> height = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.components!= null) {
                products.put("components", this.components.init());
            }
            if (this.left!= null) {
                products.put("left", this.left.init());
            }
            if (this.top!= null) {
                products.put("top", this.top.init());
            }
            if (this.width!= null) {
                products.put("width", this.width.init());
            }
            if (this.height!= null) {
                products.put("height", this.height.init());
            }
            return products;
        }

        public WebComponent.Selector<TRoot, WebCalcCell.Selector<TRoot, TParent>> components() {
            return ((this.components == null)?this.components = new WebComponent.Selector<TRoot, WebCalcCell.Selector<TRoot, TParent>>(this._root, this, "components"):this.components);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebCalcCell.Selector<TRoot, TParent>> left() {
            return ((this.left == null)?this.left = new com.kscs.util.jaxb.Selector<TRoot, WebCalcCell.Selector<TRoot, TParent>>(this._root, this, "left"):this.left);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebCalcCell.Selector<TRoot, TParent>> top() {
            return ((this.top == null)?this.top = new com.kscs.util.jaxb.Selector<TRoot, WebCalcCell.Selector<TRoot, TParent>>(this._root, this, "top"):this.top);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebCalcCell.Selector<TRoot, TParent>> width() {
            return ((this.width == null)?this.width = new com.kscs.util.jaxb.Selector<TRoot, WebCalcCell.Selector<TRoot, TParent>>(this._root, this, "width"):this.width);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebCalcCell.Selector<TRoot, TParent>> height() {
            return ((this.height == null)?this.height = new com.kscs.util.jaxb.Selector<TRoot, WebCalcCell.Selector<TRoot, TParent>>(this._root, this, "height"):this.height);
        }

    }

}
