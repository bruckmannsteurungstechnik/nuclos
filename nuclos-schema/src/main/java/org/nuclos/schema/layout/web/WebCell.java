
package org.nuclos.schema.layout.web;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Cells of a table or grid.
 * 
 * <p>Java class for web-cell complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="web-cell"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="components" type="{urn:org.nuclos.schema.layout.web}web-component" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="colspan" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&gt;
 *       &lt;attribute name="rowspan" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "web-cell", propOrder = {
    "components"
})
public class WebCell implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected List<WebComponent> components;
    @XmlAttribute(name = "colspan")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger colspan;
    @XmlAttribute(name = "rowspan")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger rowspan;

    /**
     * Gets the value of the components property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the components property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComponents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WebComponent }
     * 
     * 
     */
    public List<WebComponent> getComponents() {
        if (components == null) {
            components = new ArrayList<WebComponent>();
        }
        return this.components;
    }

    /**
     * Gets the value of the colspan property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getColspan() {
        return colspan;
    }

    /**
     * Sets the value of the colspan property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setColspan(BigInteger value) {
        this.colspan = value;
    }

    /**
     * Gets the value of the rowspan property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRowspan() {
        return rowspan;
    }

    /**
     * Sets the value of the rowspan property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRowspan(BigInteger value) {
        this.rowspan = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<WebComponent> theComponents;
            theComponents = (((this.components!= null)&&(!this.components.isEmpty()))?this.getComponents():null);
            strategy.appendField(locator, this, "components", buffer, theComponents);
        }
        {
            BigInteger theColspan;
            theColspan = this.getColspan();
            strategy.appendField(locator, this, "colspan", buffer, theColspan);
        }
        {
            BigInteger theRowspan;
            theRowspan = this.getRowspan();
            strategy.appendField(locator, this, "rowspan", buffer, theRowspan);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WebCell)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final WebCell that = ((WebCell) object);
        {
            List<WebComponent> lhsComponents;
            lhsComponents = (((this.components!= null)&&(!this.components.isEmpty()))?this.getComponents():null);
            List<WebComponent> rhsComponents;
            rhsComponents = (((that.components!= null)&&(!that.components.isEmpty()))?that.getComponents():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "components", lhsComponents), LocatorUtils.property(thatLocator, "components", rhsComponents), lhsComponents, rhsComponents)) {
                return false;
            }
        }
        {
            BigInteger lhsColspan;
            lhsColspan = this.getColspan();
            BigInteger rhsColspan;
            rhsColspan = that.getColspan();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "colspan", lhsColspan), LocatorUtils.property(thatLocator, "colspan", rhsColspan), lhsColspan, rhsColspan)) {
                return false;
            }
        }
        {
            BigInteger lhsRowspan;
            lhsRowspan = this.getRowspan();
            BigInteger rhsRowspan;
            rhsRowspan = that.getRowspan();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "rowspan", lhsRowspan), LocatorUtils.property(thatLocator, "rowspan", rhsRowspan), lhsRowspan, rhsRowspan)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<WebComponent> theComponents;
            theComponents = (((this.components!= null)&&(!this.components.isEmpty()))?this.getComponents():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "components", theComponents), currentHashCode, theComponents);
        }
        {
            BigInteger theColspan;
            theColspan = this.getColspan();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "colspan", theColspan), currentHashCode, theColspan);
        }
        {
            BigInteger theRowspan;
            theRowspan = this.getRowspan();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "rowspan", theRowspan), currentHashCode, theRowspan);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof WebCell) {
            final WebCell copy = ((WebCell) draftCopy);
            if ((this.components!= null)&&(!this.components.isEmpty())) {
                List<WebComponent> sourceComponents;
                sourceComponents = (((this.components!= null)&&(!this.components.isEmpty()))?this.getComponents():null);
                @SuppressWarnings("unchecked")
                List<WebComponent> copyComponents = ((List<WebComponent> ) strategy.copy(LocatorUtils.property(locator, "components", sourceComponents), sourceComponents));
                copy.components = null;
                if (copyComponents!= null) {
                    List<WebComponent> uniqueComponentsl = copy.getComponents();
                    uniqueComponentsl.addAll(copyComponents);
                }
            } else {
                copy.components = null;
            }
            if (this.colspan!= null) {
                BigInteger sourceColspan;
                sourceColspan = this.getColspan();
                BigInteger copyColspan = ((BigInteger) strategy.copy(LocatorUtils.property(locator, "colspan", sourceColspan), sourceColspan));
                copy.setColspan(copyColspan);
            } else {
                copy.colspan = null;
            }
            if (this.rowspan!= null) {
                BigInteger sourceRowspan;
                sourceRowspan = this.getRowspan();
                BigInteger copyRowspan = ((BigInteger) strategy.copy(LocatorUtils.property(locator, "rowspan", sourceRowspan), sourceRowspan));
                copy.setRowspan(copyRowspan);
            } else {
                copy.rowspan = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new WebCell();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebCell.Builder<_B> _other) {
        if (this.components == null) {
            _other.components = null;
        } else {
            _other.components = new ArrayList<WebComponent.Builder<WebCell.Builder<_B>>>();
            for (WebComponent _item: this.components) {
                _other.components.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.colspan = this.colspan;
        _other.rowspan = this.rowspan;
    }

    public<_B >WebCell.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new WebCell.Builder<_B>(_parentBuilder, this, true);
    }

    public WebCell.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static WebCell.Builder<Void> builder() {
        return new WebCell.Builder<Void>(null, null, false);
    }

    public static<_B >WebCell.Builder<_B> copyOf(final WebCell _other) {
        final WebCell.Builder<_B> _newBuilder = new WebCell.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebCell.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree componentsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("components"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(componentsPropertyTree!= null):((componentsPropertyTree == null)||(!componentsPropertyTree.isLeaf())))) {
            if (this.components == null) {
                _other.components = null;
            } else {
                _other.components = new ArrayList<WebComponent.Builder<WebCell.Builder<_B>>>();
                for (WebComponent _item: this.components) {
                    _other.components.add(((_item == null)?null:_item.newCopyBuilder(_other, componentsPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree colspanPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("colspan"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(colspanPropertyTree!= null):((colspanPropertyTree == null)||(!colspanPropertyTree.isLeaf())))) {
            _other.colspan = this.colspan;
        }
        final PropertyTree rowspanPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rowspan"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowspanPropertyTree!= null):((rowspanPropertyTree == null)||(!rowspanPropertyTree.isLeaf())))) {
            _other.rowspan = this.rowspan;
        }
    }

    public<_B >WebCell.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new WebCell.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public WebCell.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >WebCell.Builder<_B> copyOf(final WebCell _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebCell.Builder<_B> _newBuilder = new WebCell.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static WebCell.Builder<Void> copyExcept(final WebCell _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebCell.Builder<Void> copyOnly(final WebCell _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final WebCell _storedValue;
        private List<WebComponent.Builder<WebCell.Builder<_B>>> components;
        private BigInteger colspan;
        private BigInteger rowspan;

        public Builder(final _B _parentBuilder, final WebCell _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.components == null) {
                        this.components = null;
                    } else {
                        this.components = new ArrayList<WebComponent.Builder<WebCell.Builder<_B>>>();
                        for (WebComponent _item: _other.components) {
                            this.components.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.colspan = _other.colspan;
                    this.rowspan = _other.rowspan;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final WebCell _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree componentsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("components"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(componentsPropertyTree!= null):((componentsPropertyTree == null)||(!componentsPropertyTree.isLeaf())))) {
                        if (_other.components == null) {
                            this.components = null;
                        } else {
                            this.components = new ArrayList<WebComponent.Builder<WebCell.Builder<_B>>>();
                            for (WebComponent _item: _other.components) {
                                this.components.add(((_item == null)?null:_item.newCopyBuilder(this, componentsPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree colspanPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("colspan"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(colspanPropertyTree!= null):((colspanPropertyTree == null)||(!colspanPropertyTree.isLeaf())))) {
                        this.colspan = _other.colspan;
                    }
                    final PropertyTree rowspanPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rowspan"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowspanPropertyTree!= null):((rowspanPropertyTree == null)||(!rowspanPropertyTree.isLeaf())))) {
                        this.rowspan = _other.rowspan;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends WebCell >_P init(final _P _product) {
            if (this.components!= null) {
                final List<WebComponent> components = new ArrayList<WebComponent>(this.components.size());
                for (WebComponent.Builder<WebCell.Builder<_B>> _item: this.components) {
                    components.add(_item.build());
                }
                _product.components = components;
            }
            _product.colspan = this.colspan;
            _product.rowspan = this.rowspan;
            return _product;
        }

        /**
         * Adds the given items to the value of "components"
         * 
         * @param components
         *     Items to add to the value of the "components" property
         */
        public WebCell.Builder<_B> addComponents(final Iterable<? extends WebComponent> components) {
            if (components!= null) {
                if (this.components == null) {
                    this.components = new ArrayList<WebComponent.Builder<WebCell.Builder<_B>>>();
                }
                for (WebComponent _item: components) {
                    this.components.add(new WebComponent.Builder<WebCell.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "components" (any previous value will be replaced)
         * 
         * @param components
         *     New value of the "components" property.
         */
        public WebCell.Builder<_B> withComponents(final Iterable<? extends WebComponent> components) {
            if (this.components!= null) {
                this.components.clear();
            }
            return addComponents(components);
        }

        /**
         * Adds the given items to the value of "components"
         * 
         * @param components
         *     Items to add to the value of the "components" property
         */
        public WebCell.Builder<_B> addComponents(WebComponent... components) {
            addComponents(Arrays.asList(components));
            return this;
        }

        /**
         * Sets the new value of "components" (any previous value will be replaced)
         * 
         * @param components
         *     New value of the "components" property.
         */
        public WebCell.Builder<_B> withComponents(WebComponent... components) {
            withComponents(Arrays.asList(components));
            return this;
        }

        /**
         * Sets the new value of "colspan" (any previous value will be replaced)
         * 
         * @param colspan
         *     New value of the "colspan" property.
         */
        public WebCell.Builder<_B> withColspan(final BigInteger colspan) {
            this.colspan = colspan;
            return this;
        }

        /**
         * Sets the new value of "rowspan" (any previous value will be replaced)
         * 
         * @param rowspan
         *     New value of the "rowspan" property.
         */
        public WebCell.Builder<_B> withRowspan(final BigInteger rowspan) {
            this.rowspan = rowspan;
            return this;
        }

        @Override
        public WebCell build() {
            if (_storedValue == null) {
                return this.init(new WebCell());
            } else {
                return ((WebCell) _storedValue);
            }
        }

        public WebCell.Builder<_B> copyOf(final WebCell _other) {
            _other.copyTo(this);
            return this;
        }

        public WebCell.Builder<_B> copyOf(final WebCell.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends WebCell.Selector<WebCell.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static WebCell.Select _root() {
            return new WebCell.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private WebComponent.Selector<TRoot, WebCell.Selector<TRoot, TParent>> components = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebCell.Selector<TRoot, TParent>> colspan = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebCell.Selector<TRoot, TParent>> rowspan = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.components!= null) {
                products.put("components", this.components.init());
            }
            if (this.colspan!= null) {
                products.put("colspan", this.colspan.init());
            }
            if (this.rowspan!= null) {
                products.put("rowspan", this.rowspan.init());
            }
            return products;
        }

        public WebComponent.Selector<TRoot, WebCell.Selector<TRoot, TParent>> components() {
            return ((this.components == null)?this.components = new WebComponent.Selector<TRoot, WebCell.Selector<TRoot, TParent>>(this._root, this, "components"):this.components);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebCell.Selector<TRoot, TParent>> colspan() {
            return ((this.colspan == null)?this.colspan = new com.kscs.util.jaxb.Selector<TRoot, WebCell.Selector<TRoot, TParent>>(this._root, this, "colspan"):this.colspan);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebCell.Selector<TRoot, TParent>> rowspan() {
            return ((this.rowspan == null)?this.rowspan = new com.kscs.util.jaxb.Selector<TRoot, WebCell.Selector<TRoot, TParent>>(this._root, this, "rowspan"):this.rowspan);
        }

    }

}
