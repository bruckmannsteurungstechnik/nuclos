
package org.nuclos.schema.layout.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * A non-responsive table with fixed sizes.
 * 
 * <p>Java class for web-grid complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="web-grid"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="rows" type="{urn:org.nuclos.schema.layout.web}web-row" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "web-grid", propOrder = {
    "rows"
})
public class WebGrid implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected List<WebRow> rows;

    /**
     * Gets the value of the rows property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rows property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRows().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WebRow }
     * 
     * 
     */
    public List<WebRow> getRows() {
        if (rows == null) {
            rows = new ArrayList<WebRow>();
        }
        return this.rows;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<WebRow> theRows;
            theRows = (((this.rows!= null)&&(!this.rows.isEmpty()))?this.getRows():null);
            strategy.appendField(locator, this, "rows", buffer, theRows);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WebGrid)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final WebGrid that = ((WebGrid) object);
        {
            List<WebRow> lhsRows;
            lhsRows = (((this.rows!= null)&&(!this.rows.isEmpty()))?this.getRows():null);
            List<WebRow> rhsRows;
            rhsRows = (((that.rows!= null)&&(!that.rows.isEmpty()))?that.getRows():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "rows", lhsRows), LocatorUtils.property(thatLocator, "rows", rhsRows), lhsRows, rhsRows)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<WebRow> theRows;
            theRows = (((this.rows!= null)&&(!this.rows.isEmpty()))?this.getRows():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "rows", theRows), currentHashCode, theRows);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof WebGrid) {
            final WebGrid copy = ((WebGrid) draftCopy);
            if ((this.rows!= null)&&(!this.rows.isEmpty())) {
                List<WebRow> sourceRows;
                sourceRows = (((this.rows!= null)&&(!this.rows.isEmpty()))?this.getRows():null);
                @SuppressWarnings("unchecked")
                List<WebRow> copyRows = ((List<WebRow> ) strategy.copy(LocatorUtils.property(locator, "rows", sourceRows), sourceRows));
                copy.rows = null;
                if (copyRows!= null) {
                    List<WebRow> uniqueRowsl = copy.getRows();
                    uniqueRowsl.addAll(copyRows);
                }
            } else {
                copy.rows = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new WebGrid();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebGrid.Builder<_B> _other) {
        if (this.rows == null) {
            _other.rows = null;
        } else {
            _other.rows = new ArrayList<WebRow.Builder<WebGrid.Builder<_B>>>();
            for (WebRow _item: this.rows) {
                _other.rows.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
    }

    public<_B >WebGrid.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new WebGrid.Builder<_B>(_parentBuilder, this, true);
    }

    public WebGrid.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static WebGrid.Builder<Void> builder() {
        return new WebGrid.Builder<Void>(null, null, false);
    }

    public static<_B >WebGrid.Builder<_B> copyOf(final WebGrid _other) {
        final WebGrid.Builder<_B> _newBuilder = new WebGrid.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebGrid.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree rowsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rows"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowsPropertyTree!= null):((rowsPropertyTree == null)||(!rowsPropertyTree.isLeaf())))) {
            if (this.rows == null) {
                _other.rows = null;
            } else {
                _other.rows = new ArrayList<WebRow.Builder<WebGrid.Builder<_B>>>();
                for (WebRow _item: this.rows) {
                    _other.rows.add(((_item == null)?null:_item.newCopyBuilder(_other, rowsPropertyTree, _propertyTreeUse)));
                }
            }
        }
    }

    public<_B >WebGrid.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new WebGrid.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public WebGrid.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >WebGrid.Builder<_B> copyOf(final WebGrid _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebGrid.Builder<_B> _newBuilder = new WebGrid.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static WebGrid.Builder<Void> copyExcept(final WebGrid _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebGrid.Builder<Void> copyOnly(final WebGrid _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final WebGrid _storedValue;
        private List<WebRow.Builder<WebGrid.Builder<_B>>> rows;

        public Builder(final _B _parentBuilder, final WebGrid _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.rows == null) {
                        this.rows = null;
                    } else {
                        this.rows = new ArrayList<WebRow.Builder<WebGrid.Builder<_B>>>();
                        for (WebRow _item: _other.rows) {
                            this.rows.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final WebGrid _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree rowsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rows"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowsPropertyTree!= null):((rowsPropertyTree == null)||(!rowsPropertyTree.isLeaf())))) {
                        if (_other.rows == null) {
                            this.rows = null;
                        } else {
                            this.rows = new ArrayList<WebRow.Builder<WebGrid.Builder<_B>>>();
                            for (WebRow _item: _other.rows) {
                                this.rows.add(((_item == null)?null:_item.newCopyBuilder(this, rowsPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends WebGrid >_P init(final _P _product) {
            if (this.rows!= null) {
                final List<WebRow> rows = new ArrayList<WebRow>(this.rows.size());
                for (WebRow.Builder<WebGrid.Builder<_B>> _item: this.rows) {
                    rows.add(_item.build());
                }
                _product.rows = rows;
            }
            return _product;
        }

        /**
         * Adds the given items to the value of "rows"
         * 
         * @param rows
         *     Items to add to the value of the "rows" property
         */
        public WebGrid.Builder<_B> addRows(final Iterable<? extends WebRow> rows) {
            if (rows!= null) {
                if (this.rows == null) {
                    this.rows = new ArrayList<WebRow.Builder<WebGrid.Builder<_B>>>();
                }
                for (WebRow _item: rows) {
                    this.rows.add(new WebRow.Builder<WebGrid.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "rows" (any previous value will be replaced)
         * 
         * @param rows
         *     New value of the "rows" property.
         */
        public WebGrid.Builder<_B> withRows(final Iterable<? extends WebRow> rows) {
            if (this.rows!= null) {
                this.rows.clear();
            }
            return addRows(rows);
        }

        /**
         * Adds the given items to the value of "rows"
         * 
         * @param rows
         *     Items to add to the value of the "rows" property
         */
        public WebGrid.Builder<_B> addRows(WebRow... rows) {
            addRows(Arrays.asList(rows));
            return this;
        }

        /**
         * Sets the new value of "rows" (any previous value will be replaced)
         * 
         * @param rows
         *     New value of the "rows" property.
         */
        public WebGrid.Builder<_B> withRows(WebRow... rows) {
            withRows(Arrays.asList(rows));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Rows" property.
         * Use {@link org.nuclos.schema.layout.web.WebRow.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Rows" property.
         *     Use {@link org.nuclos.schema.layout.web.WebRow.Builder#end()} to return to the current builder.
         */
        public WebRow.Builder<? extends WebGrid.Builder<_B>> addRows() {
            if (this.rows == null) {
                this.rows = new ArrayList<WebRow.Builder<WebGrid.Builder<_B>>>();
            }
            final WebRow.Builder<WebGrid.Builder<_B>> rows_Builder = new WebRow.Builder<WebGrid.Builder<_B>>(this, null, false);
            this.rows.add(rows_Builder);
            return rows_Builder;
        }

        @Override
        public WebGrid build() {
            if (_storedValue == null) {
                return this.init(new WebGrid());
            } else {
                return ((WebGrid) _storedValue);
            }
        }

        public WebGrid.Builder<_B> copyOf(final WebGrid _other) {
            _other.copyTo(this);
            return this;
        }

        public WebGrid.Builder<_B> copyOf(final WebGrid.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends WebGrid.Selector<WebGrid.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static WebGrid.Select _root() {
            return new WebGrid.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private WebRow.Selector<TRoot, WebGrid.Selector<TRoot, TParent>> rows = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.rows!= null) {
                products.put("rows", this.rows.init());
            }
            return products;
        }

        public WebRow.Selector<TRoot, WebGrid.Selector<TRoot, TParent>> rows() {
            return ((this.rows == null)?this.rows = new WebRow.Selector<TRoot, WebGrid.Selector<TRoot, TParent>>(this._root, this, "rows"):this.rows);
        }

    }

}
