
package org.nuclos.schema.layout.web;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="grid" type="{urn:org.nuclos.schema.layout.web}web-grid"/&gt;
 *         &lt;element name="table" type="{urn:org.nuclos.schema.layout.web}web-table"/&gt;
 *         &lt;element name="calculated" type="{urn:org.nuclos.schema.layout.web}web-grid-calculated"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "grid",
    "table",
    "calculated"
})
@XmlRootElement(name = "web-layout")
public class WebLayout implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected WebGrid grid;
    protected WebTable table;
    protected WebGridCalculated calculated;

    /**
     * Gets the value of the grid property.
     * 
     * @return
     *     possible object is
     *     {@link WebGrid }
     *     
     */
    public WebGrid getGrid() {
        return grid;
    }

    /**
     * Sets the value of the grid property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebGrid }
     *     
     */
    public void setGrid(WebGrid value) {
        this.grid = value;
    }

    /**
     * Gets the value of the table property.
     * 
     * @return
     *     possible object is
     *     {@link WebTable }
     *     
     */
    public WebTable getTable() {
        return table;
    }

    /**
     * Sets the value of the table property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebTable }
     *     
     */
    public void setTable(WebTable value) {
        this.table = value;
    }

    /**
     * Gets the value of the calculated property.
     * 
     * @return
     *     possible object is
     *     {@link WebGridCalculated }
     *     
     */
    public WebGridCalculated getCalculated() {
        return calculated;
    }

    /**
     * Sets the value of the calculated property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebGridCalculated }
     *     
     */
    public void setCalculated(WebGridCalculated value) {
        this.calculated = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            WebGrid theGrid;
            theGrid = this.getGrid();
            strategy.appendField(locator, this, "grid", buffer, theGrid);
        }
        {
            WebTable theTable;
            theTable = this.getTable();
            strategy.appendField(locator, this, "table", buffer, theTable);
        }
        {
            WebGridCalculated theCalculated;
            theCalculated = this.getCalculated();
            strategy.appendField(locator, this, "calculated", buffer, theCalculated);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WebLayout)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final WebLayout that = ((WebLayout) object);
        {
            WebGrid lhsGrid;
            lhsGrid = this.getGrid();
            WebGrid rhsGrid;
            rhsGrid = that.getGrid();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "grid", lhsGrid), LocatorUtils.property(thatLocator, "grid", rhsGrid), lhsGrid, rhsGrid)) {
                return false;
            }
        }
        {
            WebTable lhsTable;
            lhsTable = this.getTable();
            WebTable rhsTable;
            rhsTable = that.getTable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "table", lhsTable), LocatorUtils.property(thatLocator, "table", rhsTable), lhsTable, rhsTable)) {
                return false;
            }
        }
        {
            WebGridCalculated lhsCalculated;
            lhsCalculated = this.getCalculated();
            WebGridCalculated rhsCalculated;
            rhsCalculated = that.getCalculated();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "calculated", lhsCalculated), LocatorUtils.property(thatLocator, "calculated", rhsCalculated), lhsCalculated, rhsCalculated)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            WebGrid theGrid;
            theGrid = this.getGrid();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "grid", theGrid), currentHashCode, theGrid);
        }
        {
            WebTable theTable;
            theTable = this.getTable();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "table", theTable), currentHashCode, theTable);
        }
        {
            WebGridCalculated theCalculated;
            theCalculated = this.getCalculated();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "calculated", theCalculated), currentHashCode, theCalculated);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof WebLayout) {
            final WebLayout copy = ((WebLayout) draftCopy);
            if (this.grid!= null) {
                WebGrid sourceGrid;
                sourceGrid = this.getGrid();
                WebGrid copyGrid = ((WebGrid) strategy.copy(LocatorUtils.property(locator, "grid", sourceGrid), sourceGrid));
                copy.setGrid(copyGrid);
            } else {
                copy.grid = null;
            }
            if (this.table!= null) {
                WebTable sourceTable;
                sourceTable = this.getTable();
                WebTable copyTable = ((WebTable) strategy.copy(LocatorUtils.property(locator, "table", sourceTable), sourceTable));
                copy.setTable(copyTable);
            } else {
                copy.table = null;
            }
            if (this.calculated!= null) {
                WebGridCalculated sourceCalculated;
                sourceCalculated = this.getCalculated();
                WebGridCalculated copyCalculated = ((WebGridCalculated) strategy.copy(LocatorUtils.property(locator, "calculated", sourceCalculated), sourceCalculated));
                copy.setCalculated(copyCalculated);
            } else {
                copy.calculated = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new WebLayout();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebLayout.Builder<_B> _other) {
        _other.grid = ((this.grid == null)?null:this.grid.newCopyBuilder(_other));
        _other.table = ((this.table == null)?null:this.table.newCopyBuilder(_other));
        _other.calculated = ((this.calculated == null)?null:this.calculated.newCopyBuilder(_other));
    }

    public<_B >WebLayout.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new WebLayout.Builder<_B>(_parentBuilder, this, true);
    }

    public WebLayout.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static WebLayout.Builder<Void> builder() {
        return new WebLayout.Builder<Void>(null, null, false);
    }

    public static<_B >WebLayout.Builder<_B> copyOf(final WebLayout _other) {
        final WebLayout.Builder<_B> _newBuilder = new WebLayout.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebLayout.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree gridPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("grid"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(gridPropertyTree!= null):((gridPropertyTree == null)||(!gridPropertyTree.isLeaf())))) {
            _other.grid = ((this.grid == null)?null:this.grid.newCopyBuilder(_other, gridPropertyTree, _propertyTreeUse));
        }
        final PropertyTree tablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("table"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(tablePropertyTree!= null):((tablePropertyTree == null)||(!tablePropertyTree.isLeaf())))) {
            _other.table = ((this.table == null)?null:this.table.newCopyBuilder(_other, tablePropertyTree, _propertyTreeUse));
        }
        final PropertyTree calculatedPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("calculated"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(calculatedPropertyTree!= null):((calculatedPropertyTree == null)||(!calculatedPropertyTree.isLeaf())))) {
            _other.calculated = ((this.calculated == null)?null:this.calculated.newCopyBuilder(_other, calculatedPropertyTree, _propertyTreeUse));
        }
    }

    public<_B >WebLayout.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new WebLayout.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public WebLayout.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >WebLayout.Builder<_B> copyOf(final WebLayout _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebLayout.Builder<_B> _newBuilder = new WebLayout.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static WebLayout.Builder<Void> copyExcept(final WebLayout _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebLayout.Builder<Void> copyOnly(final WebLayout _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final WebLayout _storedValue;
        private WebGrid.Builder<WebLayout.Builder<_B>> grid;
        private WebTable.Builder<WebLayout.Builder<_B>> table;
        private WebGridCalculated.Builder<WebLayout.Builder<_B>> calculated;

        public Builder(final _B _parentBuilder, final WebLayout _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.grid = ((_other.grid == null)?null:_other.grid.newCopyBuilder(this));
                    this.table = ((_other.table == null)?null:_other.table.newCopyBuilder(this));
                    this.calculated = ((_other.calculated == null)?null:_other.calculated.newCopyBuilder(this));
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final WebLayout _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree gridPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("grid"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(gridPropertyTree!= null):((gridPropertyTree == null)||(!gridPropertyTree.isLeaf())))) {
                        this.grid = ((_other.grid == null)?null:_other.grid.newCopyBuilder(this, gridPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree tablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("table"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(tablePropertyTree!= null):((tablePropertyTree == null)||(!tablePropertyTree.isLeaf())))) {
                        this.table = ((_other.table == null)?null:_other.table.newCopyBuilder(this, tablePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree calculatedPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("calculated"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(calculatedPropertyTree!= null):((calculatedPropertyTree == null)||(!calculatedPropertyTree.isLeaf())))) {
                        this.calculated = ((_other.calculated == null)?null:_other.calculated.newCopyBuilder(this, calculatedPropertyTree, _propertyTreeUse));
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends WebLayout >_P init(final _P _product) {
            _product.grid = ((this.grid == null)?null:this.grid.build());
            _product.table = ((this.table == null)?null:this.table.build());
            _product.calculated = ((this.calculated == null)?null:this.calculated.build());
            return _product;
        }

        /**
         * Sets the new value of "grid" (any previous value will be replaced)
         * 
         * @param grid
         *     New value of the "grid" property.
         */
        public WebLayout.Builder<_B> withGrid(final WebGrid grid) {
            this.grid = ((grid == null)?null:new WebGrid.Builder<WebLayout.Builder<_B>>(this, grid, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "grid" property.
         * Use {@link org.nuclos.schema.layout.web.WebGrid.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "grid" property.
         *     Use {@link org.nuclos.schema.layout.web.WebGrid.Builder#end()} to return to the current builder.
         */
        public WebGrid.Builder<? extends WebLayout.Builder<_B>> withGrid() {
            if (this.grid!= null) {
                return this.grid;
            }
            return this.grid = new WebGrid.Builder<WebLayout.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "table" (any previous value will be replaced)
         * 
         * @param table
         *     New value of the "table" property.
         */
        public WebLayout.Builder<_B> withTable(final WebTable table) {
            this.table = ((table == null)?null:new WebTable.Builder<WebLayout.Builder<_B>>(this, table, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "table" property.
         * Use {@link org.nuclos.schema.layout.web.WebTable.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "table" property.
         *     Use {@link org.nuclos.schema.layout.web.WebTable.Builder#end()} to return to the current builder.
         */
        public WebTable.Builder<? extends WebLayout.Builder<_B>> withTable() {
            if (this.table!= null) {
                return this.table;
            }
            return this.table = new WebTable.Builder<WebLayout.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "calculated" (any previous value will be replaced)
         * 
         * @param calculated
         *     New value of the "calculated" property.
         */
        public WebLayout.Builder<_B> withCalculated(final WebGridCalculated calculated) {
            this.calculated = ((calculated == null)?null:new WebGridCalculated.Builder<WebLayout.Builder<_B>>(this, calculated, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "calculated" property.
         * Use {@link org.nuclos.schema.layout.web.WebGridCalculated.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "calculated" property.
         *     Use {@link org.nuclos.schema.layout.web.WebGridCalculated.Builder#end()} to return to the current builder.
         */
        public WebGridCalculated.Builder<? extends WebLayout.Builder<_B>> withCalculated() {
            if (this.calculated!= null) {
                return this.calculated;
            }
            return this.calculated = new WebGridCalculated.Builder<WebLayout.Builder<_B>>(this, null, false);
        }

        @Override
        public WebLayout build() {
            if (_storedValue == null) {
                return this.init(new WebLayout());
            } else {
                return ((WebLayout) _storedValue);
            }
        }

        public WebLayout.Builder<_B> copyOf(final WebLayout _other) {
            _other.copyTo(this);
            return this;
        }

        public WebLayout.Builder<_B> copyOf(final WebLayout.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends WebLayout.Selector<WebLayout.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static WebLayout.Select _root() {
            return new WebLayout.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private WebGrid.Selector<TRoot, WebLayout.Selector<TRoot, TParent>> grid = null;
        private WebTable.Selector<TRoot, WebLayout.Selector<TRoot, TParent>> table = null;
        private WebGridCalculated.Selector<TRoot, WebLayout.Selector<TRoot, TParent>> calculated = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.grid!= null) {
                products.put("grid", this.grid.init());
            }
            if (this.table!= null) {
                products.put("table", this.table.init());
            }
            if (this.calculated!= null) {
                products.put("calculated", this.calculated.init());
            }
            return products;
        }

        public WebGrid.Selector<TRoot, WebLayout.Selector<TRoot, TParent>> grid() {
            return ((this.grid == null)?this.grid = new WebGrid.Selector<TRoot, WebLayout.Selector<TRoot, TParent>>(this._root, this, "grid"):this.grid);
        }

        public WebTable.Selector<TRoot, WebLayout.Selector<TRoot, TParent>> table() {
            return ((this.table == null)?this.table = new WebTable.Selector<TRoot, WebLayout.Selector<TRoot, TParent>>(this._root, this, "table"):this.table);
        }

        public WebGridCalculated.Selector<TRoot, WebLayout.Selector<TRoot, TParent>> calculated() {
            return ((this.calculated == null)?this.calculated = new WebGridCalculated.Selector<TRoot, WebLayout.Selector<TRoot, TParent>>(this._root, this, "calculated"):this.calculated);
        }

    }

}
