
package org.nuclos.schema.layout.web;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * A matrix component.
 * 
 * <p>Java class for web-matrix complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="web-matrix"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:org.nuclos.schema.layout.web}web-input-component"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="matrix-columns" type="{urn:org.nuclos.schema.layout.web}web-matrix-column" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="matrix_preferences_field" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entity_x" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entity_y" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entity_matrix" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entity_field_matrix_parent" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entity_field_matrix_x_ref_field" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entity_matrix_value_field" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entity_matrix_number_state" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entity_field_categorie" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entity_field_x" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entity_y_parent_field" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entity_field_y" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entity_x_sorting_fields" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entity_y_sorting_fields" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entity_x_header" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entity_y_header" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entity_matrix_reference_field" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entity_matrix_value_type" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entity_x_vlp_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entity_x_vlp_idfieldname" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entity_x_vlp_fieldname" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entity_x_vlp_reference_param_name" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="cell_input_type" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "web-matrix", propOrder = {
    "matrixColumns"
})
public class WebMatrix
    extends WebInputComponent
    implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "matrix-columns", required = true)
    protected List<WebMatrixColumn> matrixColumns;
    @XmlAttribute(name = "matrix_preferences_field")
    protected String matrixPreferencesField;
    @XmlAttribute(name = "entity_x")
    protected String entityX;
    @XmlAttribute(name = "entity_y")
    protected String entityY;
    @XmlAttribute(name = "entity_matrix")
    protected String entityMatrix;
    @XmlAttribute(name = "entity_field_matrix_parent")
    protected String entityFieldMatrixParent;
    @XmlAttribute(name = "entity_field_matrix_x_ref_field")
    protected String entityFieldMatrixXRefField;
    @XmlAttribute(name = "entity_matrix_value_field")
    protected String entityMatrixValueField;
    @XmlAttribute(name = "entity_matrix_number_state")
    protected String entityMatrixNumberState;
    @XmlAttribute(name = "entity_field_categorie")
    protected String entityFieldCategorie;
    @XmlAttribute(name = "entity_field_x")
    protected String entityFieldX;
    @XmlAttribute(name = "entity_y_parent_field")
    protected String entityYParentField;
    @XmlAttribute(name = "entity_field_y")
    protected String entityFieldY;
    @XmlAttribute(name = "entity_x_sorting_fields")
    protected String entityXSortingFields;
    @XmlAttribute(name = "entity_y_sorting_fields")
    protected String entityYSortingFields;
    @XmlAttribute(name = "entity_x_header")
    protected String entityXHeader;
    @XmlAttribute(name = "entity_y_header")
    protected String entityYHeader;
    @XmlAttribute(name = "entity_matrix_reference_field")
    protected String entityMatrixReferenceField;
    @XmlAttribute(name = "entity_matrix_value_type")
    protected String entityMatrixValueType;
    @XmlAttribute(name = "entity_x_vlp_id")
    protected String entityXVlpId;
    @XmlAttribute(name = "entity_x_vlp_idfieldname")
    protected String entityXVlpIdfieldname;
    @XmlAttribute(name = "entity_x_vlp_fieldname")
    protected String entityXVlpFieldname;
    @XmlAttribute(name = "entity_x_vlp_reference_param_name")
    protected String entityXVlpReferenceParamName;
    @XmlAttribute(name = "cell_input_type")
    protected String cellInputType;

    /**
     * Gets the value of the matrixColumns property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the matrixColumns property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMatrixColumns().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WebMatrixColumn }
     * 
     * 
     */
    public List<WebMatrixColumn> getMatrixColumns() {
        if (matrixColumns == null) {
            matrixColumns = new ArrayList<WebMatrixColumn>();
        }
        return this.matrixColumns;
    }

    /**
     * Gets the value of the matrixPreferencesField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatrixPreferencesField() {
        return matrixPreferencesField;
    }

    /**
     * Sets the value of the matrixPreferencesField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatrixPreferencesField(String value) {
        this.matrixPreferencesField = value;
    }

    /**
     * Gets the value of the entityX property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityX() {
        return entityX;
    }

    /**
     * Sets the value of the entityX property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityX(String value) {
        this.entityX = value;
    }

    /**
     * Gets the value of the entityY property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityY() {
        return entityY;
    }

    /**
     * Sets the value of the entityY property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityY(String value) {
        this.entityY = value;
    }

    /**
     * Gets the value of the entityMatrix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityMatrix() {
        return entityMatrix;
    }

    /**
     * Sets the value of the entityMatrix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityMatrix(String value) {
        this.entityMatrix = value;
    }

    /**
     * Gets the value of the entityFieldMatrixParent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityFieldMatrixParent() {
        return entityFieldMatrixParent;
    }

    /**
     * Sets the value of the entityFieldMatrixParent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityFieldMatrixParent(String value) {
        this.entityFieldMatrixParent = value;
    }

    /**
     * Gets the value of the entityFieldMatrixXRefField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityFieldMatrixXRefField() {
        return entityFieldMatrixXRefField;
    }

    /**
     * Sets the value of the entityFieldMatrixXRefField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityFieldMatrixXRefField(String value) {
        this.entityFieldMatrixXRefField = value;
    }

    /**
     * Gets the value of the entityMatrixValueField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityMatrixValueField() {
        return entityMatrixValueField;
    }

    /**
     * Sets the value of the entityMatrixValueField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityMatrixValueField(String value) {
        this.entityMatrixValueField = value;
    }

    /**
     * Gets the value of the entityMatrixNumberState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityMatrixNumberState() {
        return entityMatrixNumberState;
    }

    /**
     * Sets the value of the entityMatrixNumberState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityMatrixNumberState(String value) {
        this.entityMatrixNumberState = value;
    }

    /**
     * Gets the value of the entityFieldCategorie property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityFieldCategorie() {
        return entityFieldCategorie;
    }

    /**
     * Sets the value of the entityFieldCategorie property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityFieldCategorie(String value) {
        this.entityFieldCategorie = value;
    }

    /**
     * Gets the value of the entityFieldX property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityFieldX() {
        return entityFieldX;
    }

    /**
     * Sets the value of the entityFieldX property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityFieldX(String value) {
        this.entityFieldX = value;
    }

    /**
     * Gets the value of the entityYParentField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityYParentField() {
        return entityYParentField;
    }

    /**
     * Sets the value of the entityYParentField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityYParentField(String value) {
        this.entityYParentField = value;
    }

    /**
     * Gets the value of the entityFieldY property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityFieldY() {
        return entityFieldY;
    }

    /**
     * Sets the value of the entityFieldY property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityFieldY(String value) {
        this.entityFieldY = value;
    }

    /**
     * Gets the value of the entityXSortingFields property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityXSortingFields() {
        return entityXSortingFields;
    }

    /**
     * Sets the value of the entityXSortingFields property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityXSortingFields(String value) {
        this.entityXSortingFields = value;
    }

    /**
     * Gets the value of the entityYSortingFields property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityYSortingFields() {
        return entityYSortingFields;
    }

    /**
     * Sets the value of the entityYSortingFields property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityYSortingFields(String value) {
        this.entityYSortingFields = value;
    }

    /**
     * Gets the value of the entityXHeader property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityXHeader() {
        return entityXHeader;
    }

    /**
     * Sets the value of the entityXHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityXHeader(String value) {
        this.entityXHeader = value;
    }

    /**
     * Gets the value of the entityYHeader property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityYHeader() {
        return entityYHeader;
    }

    /**
     * Sets the value of the entityYHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityYHeader(String value) {
        this.entityYHeader = value;
    }

    /**
     * Gets the value of the entityMatrixReferenceField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityMatrixReferenceField() {
        return entityMatrixReferenceField;
    }

    /**
     * Sets the value of the entityMatrixReferenceField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityMatrixReferenceField(String value) {
        this.entityMatrixReferenceField = value;
    }

    /**
     * Gets the value of the entityMatrixValueType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityMatrixValueType() {
        return entityMatrixValueType;
    }

    /**
     * Sets the value of the entityMatrixValueType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityMatrixValueType(String value) {
        this.entityMatrixValueType = value;
    }

    /**
     * Gets the value of the entityXVlpId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityXVlpId() {
        return entityXVlpId;
    }

    /**
     * Sets the value of the entityXVlpId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityXVlpId(String value) {
        this.entityXVlpId = value;
    }

    /**
     * Gets the value of the entityXVlpIdfieldname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityXVlpIdfieldname() {
        return entityXVlpIdfieldname;
    }

    /**
     * Sets the value of the entityXVlpIdfieldname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityXVlpIdfieldname(String value) {
        this.entityXVlpIdfieldname = value;
    }

    /**
     * Gets the value of the entityXVlpFieldname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityXVlpFieldname() {
        return entityXVlpFieldname;
    }

    /**
     * Sets the value of the entityXVlpFieldname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityXVlpFieldname(String value) {
        this.entityXVlpFieldname = value;
    }

    /**
     * Gets the value of the entityXVlpReferenceParamName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityXVlpReferenceParamName() {
        return entityXVlpReferenceParamName;
    }

    /**
     * Sets the value of the entityXVlpReferenceParamName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityXVlpReferenceParamName(String value) {
        this.entityXVlpReferenceParamName = value;
    }

    /**
     * Gets the value of the cellInputType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCellInputType() {
        return cellInputType;
    }

    /**
     * Sets the value of the cellInputType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCellInputType(String value) {
        this.cellInputType = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        super.appendFields(locator, buffer, strategy);
        {
            List<WebMatrixColumn> theMatrixColumns;
            theMatrixColumns = (((this.matrixColumns!= null)&&(!this.matrixColumns.isEmpty()))?this.getMatrixColumns():null);
            strategy.appendField(locator, this, "matrixColumns", buffer, theMatrixColumns);
        }
        {
            String theMatrixPreferencesField;
            theMatrixPreferencesField = this.getMatrixPreferencesField();
            strategy.appendField(locator, this, "matrixPreferencesField", buffer, theMatrixPreferencesField);
        }
        {
            String theEntityX;
            theEntityX = this.getEntityX();
            strategy.appendField(locator, this, "entityX", buffer, theEntityX);
        }
        {
            String theEntityY;
            theEntityY = this.getEntityY();
            strategy.appendField(locator, this, "entityY", buffer, theEntityY);
        }
        {
            String theEntityMatrix;
            theEntityMatrix = this.getEntityMatrix();
            strategy.appendField(locator, this, "entityMatrix", buffer, theEntityMatrix);
        }
        {
            String theEntityFieldMatrixParent;
            theEntityFieldMatrixParent = this.getEntityFieldMatrixParent();
            strategy.appendField(locator, this, "entityFieldMatrixParent", buffer, theEntityFieldMatrixParent);
        }
        {
            String theEntityFieldMatrixXRefField;
            theEntityFieldMatrixXRefField = this.getEntityFieldMatrixXRefField();
            strategy.appendField(locator, this, "entityFieldMatrixXRefField", buffer, theEntityFieldMatrixXRefField);
        }
        {
            String theEntityMatrixValueField;
            theEntityMatrixValueField = this.getEntityMatrixValueField();
            strategy.appendField(locator, this, "entityMatrixValueField", buffer, theEntityMatrixValueField);
        }
        {
            String theEntityMatrixNumberState;
            theEntityMatrixNumberState = this.getEntityMatrixNumberState();
            strategy.appendField(locator, this, "entityMatrixNumberState", buffer, theEntityMatrixNumberState);
        }
        {
            String theEntityFieldCategorie;
            theEntityFieldCategorie = this.getEntityFieldCategorie();
            strategy.appendField(locator, this, "entityFieldCategorie", buffer, theEntityFieldCategorie);
        }
        {
            String theEntityFieldX;
            theEntityFieldX = this.getEntityFieldX();
            strategy.appendField(locator, this, "entityFieldX", buffer, theEntityFieldX);
        }
        {
            String theEntityYParentField;
            theEntityYParentField = this.getEntityYParentField();
            strategy.appendField(locator, this, "entityYParentField", buffer, theEntityYParentField);
        }
        {
            String theEntityFieldY;
            theEntityFieldY = this.getEntityFieldY();
            strategy.appendField(locator, this, "entityFieldY", buffer, theEntityFieldY);
        }
        {
            String theEntityXSortingFields;
            theEntityXSortingFields = this.getEntityXSortingFields();
            strategy.appendField(locator, this, "entityXSortingFields", buffer, theEntityXSortingFields);
        }
        {
            String theEntityYSortingFields;
            theEntityYSortingFields = this.getEntityYSortingFields();
            strategy.appendField(locator, this, "entityYSortingFields", buffer, theEntityYSortingFields);
        }
        {
            String theEntityXHeader;
            theEntityXHeader = this.getEntityXHeader();
            strategy.appendField(locator, this, "entityXHeader", buffer, theEntityXHeader);
        }
        {
            String theEntityYHeader;
            theEntityYHeader = this.getEntityYHeader();
            strategy.appendField(locator, this, "entityYHeader", buffer, theEntityYHeader);
        }
        {
            String theEntityMatrixReferenceField;
            theEntityMatrixReferenceField = this.getEntityMatrixReferenceField();
            strategy.appendField(locator, this, "entityMatrixReferenceField", buffer, theEntityMatrixReferenceField);
        }
        {
            String theEntityMatrixValueType;
            theEntityMatrixValueType = this.getEntityMatrixValueType();
            strategy.appendField(locator, this, "entityMatrixValueType", buffer, theEntityMatrixValueType);
        }
        {
            String theEntityXVlpId;
            theEntityXVlpId = this.getEntityXVlpId();
            strategy.appendField(locator, this, "entityXVlpId", buffer, theEntityXVlpId);
        }
        {
            String theEntityXVlpIdfieldname;
            theEntityXVlpIdfieldname = this.getEntityXVlpIdfieldname();
            strategy.appendField(locator, this, "entityXVlpIdfieldname", buffer, theEntityXVlpIdfieldname);
        }
        {
            String theEntityXVlpFieldname;
            theEntityXVlpFieldname = this.getEntityXVlpFieldname();
            strategy.appendField(locator, this, "entityXVlpFieldname", buffer, theEntityXVlpFieldname);
        }
        {
            String theEntityXVlpReferenceParamName;
            theEntityXVlpReferenceParamName = this.getEntityXVlpReferenceParamName();
            strategy.appendField(locator, this, "entityXVlpReferenceParamName", buffer, theEntityXVlpReferenceParamName);
        }
        {
            String theCellInputType;
            theCellInputType = this.getCellInputType();
            strategy.appendField(locator, this, "cellInputType", buffer, theCellInputType);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WebMatrix)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final WebMatrix that = ((WebMatrix) object);
        {
            List<WebMatrixColumn> lhsMatrixColumns;
            lhsMatrixColumns = (((this.matrixColumns!= null)&&(!this.matrixColumns.isEmpty()))?this.getMatrixColumns():null);
            List<WebMatrixColumn> rhsMatrixColumns;
            rhsMatrixColumns = (((that.matrixColumns!= null)&&(!that.matrixColumns.isEmpty()))?that.getMatrixColumns():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "matrixColumns", lhsMatrixColumns), LocatorUtils.property(thatLocator, "matrixColumns", rhsMatrixColumns), lhsMatrixColumns, rhsMatrixColumns)) {
                return false;
            }
        }
        {
            String lhsMatrixPreferencesField;
            lhsMatrixPreferencesField = this.getMatrixPreferencesField();
            String rhsMatrixPreferencesField;
            rhsMatrixPreferencesField = that.getMatrixPreferencesField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "matrixPreferencesField", lhsMatrixPreferencesField), LocatorUtils.property(thatLocator, "matrixPreferencesField", rhsMatrixPreferencesField), lhsMatrixPreferencesField, rhsMatrixPreferencesField)) {
                return false;
            }
        }
        {
            String lhsEntityX;
            lhsEntityX = this.getEntityX();
            String rhsEntityX;
            rhsEntityX = that.getEntityX();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityX", lhsEntityX), LocatorUtils.property(thatLocator, "entityX", rhsEntityX), lhsEntityX, rhsEntityX)) {
                return false;
            }
        }
        {
            String lhsEntityY;
            lhsEntityY = this.getEntityY();
            String rhsEntityY;
            rhsEntityY = that.getEntityY();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityY", lhsEntityY), LocatorUtils.property(thatLocator, "entityY", rhsEntityY), lhsEntityY, rhsEntityY)) {
                return false;
            }
        }
        {
            String lhsEntityMatrix;
            lhsEntityMatrix = this.getEntityMatrix();
            String rhsEntityMatrix;
            rhsEntityMatrix = that.getEntityMatrix();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityMatrix", lhsEntityMatrix), LocatorUtils.property(thatLocator, "entityMatrix", rhsEntityMatrix), lhsEntityMatrix, rhsEntityMatrix)) {
                return false;
            }
        }
        {
            String lhsEntityFieldMatrixParent;
            lhsEntityFieldMatrixParent = this.getEntityFieldMatrixParent();
            String rhsEntityFieldMatrixParent;
            rhsEntityFieldMatrixParent = that.getEntityFieldMatrixParent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityFieldMatrixParent", lhsEntityFieldMatrixParent), LocatorUtils.property(thatLocator, "entityFieldMatrixParent", rhsEntityFieldMatrixParent), lhsEntityFieldMatrixParent, rhsEntityFieldMatrixParent)) {
                return false;
            }
        }
        {
            String lhsEntityFieldMatrixXRefField;
            lhsEntityFieldMatrixXRefField = this.getEntityFieldMatrixXRefField();
            String rhsEntityFieldMatrixXRefField;
            rhsEntityFieldMatrixXRefField = that.getEntityFieldMatrixXRefField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityFieldMatrixXRefField", lhsEntityFieldMatrixXRefField), LocatorUtils.property(thatLocator, "entityFieldMatrixXRefField", rhsEntityFieldMatrixXRefField), lhsEntityFieldMatrixXRefField, rhsEntityFieldMatrixXRefField)) {
                return false;
            }
        }
        {
            String lhsEntityMatrixValueField;
            lhsEntityMatrixValueField = this.getEntityMatrixValueField();
            String rhsEntityMatrixValueField;
            rhsEntityMatrixValueField = that.getEntityMatrixValueField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityMatrixValueField", lhsEntityMatrixValueField), LocatorUtils.property(thatLocator, "entityMatrixValueField", rhsEntityMatrixValueField), lhsEntityMatrixValueField, rhsEntityMatrixValueField)) {
                return false;
            }
        }
        {
            String lhsEntityMatrixNumberState;
            lhsEntityMatrixNumberState = this.getEntityMatrixNumberState();
            String rhsEntityMatrixNumberState;
            rhsEntityMatrixNumberState = that.getEntityMatrixNumberState();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityMatrixNumberState", lhsEntityMatrixNumberState), LocatorUtils.property(thatLocator, "entityMatrixNumberState", rhsEntityMatrixNumberState), lhsEntityMatrixNumberState, rhsEntityMatrixNumberState)) {
                return false;
            }
        }
        {
            String lhsEntityFieldCategorie;
            lhsEntityFieldCategorie = this.getEntityFieldCategorie();
            String rhsEntityFieldCategorie;
            rhsEntityFieldCategorie = that.getEntityFieldCategorie();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityFieldCategorie", lhsEntityFieldCategorie), LocatorUtils.property(thatLocator, "entityFieldCategorie", rhsEntityFieldCategorie), lhsEntityFieldCategorie, rhsEntityFieldCategorie)) {
                return false;
            }
        }
        {
            String lhsEntityFieldX;
            lhsEntityFieldX = this.getEntityFieldX();
            String rhsEntityFieldX;
            rhsEntityFieldX = that.getEntityFieldX();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityFieldX", lhsEntityFieldX), LocatorUtils.property(thatLocator, "entityFieldX", rhsEntityFieldX), lhsEntityFieldX, rhsEntityFieldX)) {
                return false;
            }
        }
        {
            String lhsEntityYParentField;
            lhsEntityYParentField = this.getEntityYParentField();
            String rhsEntityYParentField;
            rhsEntityYParentField = that.getEntityYParentField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityYParentField", lhsEntityYParentField), LocatorUtils.property(thatLocator, "entityYParentField", rhsEntityYParentField), lhsEntityYParentField, rhsEntityYParentField)) {
                return false;
            }
        }
        {
            String lhsEntityFieldY;
            lhsEntityFieldY = this.getEntityFieldY();
            String rhsEntityFieldY;
            rhsEntityFieldY = that.getEntityFieldY();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityFieldY", lhsEntityFieldY), LocatorUtils.property(thatLocator, "entityFieldY", rhsEntityFieldY), lhsEntityFieldY, rhsEntityFieldY)) {
                return false;
            }
        }
        {
            String lhsEntityXSortingFields;
            lhsEntityXSortingFields = this.getEntityXSortingFields();
            String rhsEntityXSortingFields;
            rhsEntityXSortingFields = that.getEntityXSortingFields();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityXSortingFields", lhsEntityXSortingFields), LocatorUtils.property(thatLocator, "entityXSortingFields", rhsEntityXSortingFields), lhsEntityXSortingFields, rhsEntityXSortingFields)) {
                return false;
            }
        }
        {
            String lhsEntityYSortingFields;
            lhsEntityYSortingFields = this.getEntityYSortingFields();
            String rhsEntityYSortingFields;
            rhsEntityYSortingFields = that.getEntityYSortingFields();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityYSortingFields", lhsEntityYSortingFields), LocatorUtils.property(thatLocator, "entityYSortingFields", rhsEntityYSortingFields), lhsEntityYSortingFields, rhsEntityYSortingFields)) {
                return false;
            }
        }
        {
            String lhsEntityXHeader;
            lhsEntityXHeader = this.getEntityXHeader();
            String rhsEntityXHeader;
            rhsEntityXHeader = that.getEntityXHeader();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityXHeader", lhsEntityXHeader), LocatorUtils.property(thatLocator, "entityXHeader", rhsEntityXHeader), lhsEntityXHeader, rhsEntityXHeader)) {
                return false;
            }
        }
        {
            String lhsEntityYHeader;
            lhsEntityYHeader = this.getEntityYHeader();
            String rhsEntityYHeader;
            rhsEntityYHeader = that.getEntityYHeader();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityYHeader", lhsEntityYHeader), LocatorUtils.property(thatLocator, "entityYHeader", rhsEntityYHeader), lhsEntityYHeader, rhsEntityYHeader)) {
                return false;
            }
        }
        {
            String lhsEntityMatrixReferenceField;
            lhsEntityMatrixReferenceField = this.getEntityMatrixReferenceField();
            String rhsEntityMatrixReferenceField;
            rhsEntityMatrixReferenceField = that.getEntityMatrixReferenceField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityMatrixReferenceField", lhsEntityMatrixReferenceField), LocatorUtils.property(thatLocator, "entityMatrixReferenceField", rhsEntityMatrixReferenceField), lhsEntityMatrixReferenceField, rhsEntityMatrixReferenceField)) {
                return false;
            }
        }
        {
            String lhsEntityMatrixValueType;
            lhsEntityMatrixValueType = this.getEntityMatrixValueType();
            String rhsEntityMatrixValueType;
            rhsEntityMatrixValueType = that.getEntityMatrixValueType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityMatrixValueType", lhsEntityMatrixValueType), LocatorUtils.property(thatLocator, "entityMatrixValueType", rhsEntityMatrixValueType), lhsEntityMatrixValueType, rhsEntityMatrixValueType)) {
                return false;
            }
        }
        {
            String lhsEntityXVlpId;
            lhsEntityXVlpId = this.getEntityXVlpId();
            String rhsEntityXVlpId;
            rhsEntityXVlpId = that.getEntityXVlpId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityXVlpId", lhsEntityXVlpId), LocatorUtils.property(thatLocator, "entityXVlpId", rhsEntityXVlpId), lhsEntityXVlpId, rhsEntityXVlpId)) {
                return false;
            }
        }
        {
            String lhsEntityXVlpIdfieldname;
            lhsEntityXVlpIdfieldname = this.getEntityXVlpIdfieldname();
            String rhsEntityXVlpIdfieldname;
            rhsEntityXVlpIdfieldname = that.getEntityXVlpIdfieldname();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityXVlpIdfieldname", lhsEntityXVlpIdfieldname), LocatorUtils.property(thatLocator, "entityXVlpIdfieldname", rhsEntityXVlpIdfieldname), lhsEntityXVlpIdfieldname, rhsEntityXVlpIdfieldname)) {
                return false;
            }
        }
        {
            String lhsEntityXVlpFieldname;
            lhsEntityXVlpFieldname = this.getEntityXVlpFieldname();
            String rhsEntityXVlpFieldname;
            rhsEntityXVlpFieldname = that.getEntityXVlpFieldname();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityXVlpFieldname", lhsEntityXVlpFieldname), LocatorUtils.property(thatLocator, "entityXVlpFieldname", rhsEntityXVlpFieldname), lhsEntityXVlpFieldname, rhsEntityXVlpFieldname)) {
                return false;
            }
        }
        {
            String lhsEntityXVlpReferenceParamName;
            lhsEntityXVlpReferenceParamName = this.getEntityXVlpReferenceParamName();
            String rhsEntityXVlpReferenceParamName;
            rhsEntityXVlpReferenceParamName = that.getEntityXVlpReferenceParamName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityXVlpReferenceParamName", lhsEntityXVlpReferenceParamName), LocatorUtils.property(thatLocator, "entityXVlpReferenceParamName", rhsEntityXVlpReferenceParamName), lhsEntityXVlpReferenceParamName, rhsEntityXVlpReferenceParamName)) {
                return false;
            }
        }
        {
            String lhsCellInputType;
            lhsCellInputType = this.getCellInputType();
            String rhsCellInputType;
            rhsCellInputType = that.getCellInputType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "cellInputType", lhsCellInputType), LocatorUtils.property(thatLocator, "cellInputType", rhsCellInputType), lhsCellInputType, rhsCellInputType)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = super.hashCode(locator, strategy);
        {
            List<WebMatrixColumn> theMatrixColumns;
            theMatrixColumns = (((this.matrixColumns!= null)&&(!this.matrixColumns.isEmpty()))?this.getMatrixColumns():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "matrixColumns", theMatrixColumns), currentHashCode, theMatrixColumns);
        }
        {
            String theMatrixPreferencesField;
            theMatrixPreferencesField = this.getMatrixPreferencesField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "matrixPreferencesField", theMatrixPreferencesField), currentHashCode, theMatrixPreferencesField);
        }
        {
            String theEntityX;
            theEntityX = this.getEntityX();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityX", theEntityX), currentHashCode, theEntityX);
        }
        {
            String theEntityY;
            theEntityY = this.getEntityY();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityY", theEntityY), currentHashCode, theEntityY);
        }
        {
            String theEntityMatrix;
            theEntityMatrix = this.getEntityMatrix();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityMatrix", theEntityMatrix), currentHashCode, theEntityMatrix);
        }
        {
            String theEntityFieldMatrixParent;
            theEntityFieldMatrixParent = this.getEntityFieldMatrixParent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityFieldMatrixParent", theEntityFieldMatrixParent), currentHashCode, theEntityFieldMatrixParent);
        }
        {
            String theEntityFieldMatrixXRefField;
            theEntityFieldMatrixXRefField = this.getEntityFieldMatrixXRefField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityFieldMatrixXRefField", theEntityFieldMatrixXRefField), currentHashCode, theEntityFieldMatrixXRefField);
        }
        {
            String theEntityMatrixValueField;
            theEntityMatrixValueField = this.getEntityMatrixValueField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityMatrixValueField", theEntityMatrixValueField), currentHashCode, theEntityMatrixValueField);
        }
        {
            String theEntityMatrixNumberState;
            theEntityMatrixNumberState = this.getEntityMatrixNumberState();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityMatrixNumberState", theEntityMatrixNumberState), currentHashCode, theEntityMatrixNumberState);
        }
        {
            String theEntityFieldCategorie;
            theEntityFieldCategorie = this.getEntityFieldCategorie();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityFieldCategorie", theEntityFieldCategorie), currentHashCode, theEntityFieldCategorie);
        }
        {
            String theEntityFieldX;
            theEntityFieldX = this.getEntityFieldX();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityFieldX", theEntityFieldX), currentHashCode, theEntityFieldX);
        }
        {
            String theEntityYParentField;
            theEntityYParentField = this.getEntityYParentField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityYParentField", theEntityYParentField), currentHashCode, theEntityYParentField);
        }
        {
            String theEntityFieldY;
            theEntityFieldY = this.getEntityFieldY();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityFieldY", theEntityFieldY), currentHashCode, theEntityFieldY);
        }
        {
            String theEntityXSortingFields;
            theEntityXSortingFields = this.getEntityXSortingFields();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityXSortingFields", theEntityXSortingFields), currentHashCode, theEntityXSortingFields);
        }
        {
            String theEntityYSortingFields;
            theEntityYSortingFields = this.getEntityYSortingFields();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityYSortingFields", theEntityYSortingFields), currentHashCode, theEntityYSortingFields);
        }
        {
            String theEntityXHeader;
            theEntityXHeader = this.getEntityXHeader();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityXHeader", theEntityXHeader), currentHashCode, theEntityXHeader);
        }
        {
            String theEntityYHeader;
            theEntityYHeader = this.getEntityYHeader();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityYHeader", theEntityYHeader), currentHashCode, theEntityYHeader);
        }
        {
            String theEntityMatrixReferenceField;
            theEntityMatrixReferenceField = this.getEntityMatrixReferenceField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityMatrixReferenceField", theEntityMatrixReferenceField), currentHashCode, theEntityMatrixReferenceField);
        }
        {
            String theEntityMatrixValueType;
            theEntityMatrixValueType = this.getEntityMatrixValueType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityMatrixValueType", theEntityMatrixValueType), currentHashCode, theEntityMatrixValueType);
        }
        {
            String theEntityXVlpId;
            theEntityXVlpId = this.getEntityXVlpId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityXVlpId", theEntityXVlpId), currentHashCode, theEntityXVlpId);
        }
        {
            String theEntityXVlpIdfieldname;
            theEntityXVlpIdfieldname = this.getEntityXVlpIdfieldname();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityXVlpIdfieldname", theEntityXVlpIdfieldname), currentHashCode, theEntityXVlpIdfieldname);
        }
        {
            String theEntityXVlpFieldname;
            theEntityXVlpFieldname = this.getEntityXVlpFieldname();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityXVlpFieldname", theEntityXVlpFieldname), currentHashCode, theEntityXVlpFieldname);
        }
        {
            String theEntityXVlpReferenceParamName;
            theEntityXVlpReferenceParamName = this.getEntityXVlpReferenceParamName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityXVlpReferenceParamName", theEntityXVlpReferenceParamName), currentHashCode, theEntityXVlpReferenceParamName);
        }
        {
            String theCellInputType;
            theCellInputType = this.getCellInputType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "cellInputType", theCellInputType), currentHashCode, theCellInputType);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof WebMatrix) {
            final WebMatrix copy = ((WebMatrix) draftCopy);
            if ((this.matrixColumns!= null)&&(!this.matrixColumns.isEmpty())) {
                List<WebMatrixColumn> sourceMatrixColumns;
                sourceMatrixColumns = (((this.matrixColumns!= null)&&(!this.matrixColumns.isEmpty()))?this.getMatrixColumns():null);
                @SuppressWarnings("unchecked")
                List<WebMatrixColumn> copyMatrixColumns = ((List<WebMatrixColumn> ) strategy.copy(LocatorUtils.property(locator, "matrixColumns", sourceMatrixColumns), sourceMatrixColumns));
                copy.matrixColumns = null;
                if (copyMatrixColumns!= null) {
                    List<WebMatrixColumn> uniqueMatrixColumnsl = copy.getMatrixColumns();
                    uniqueMatrixColumnsl.addAll(copyMatrixColumns);
                }
            } else {
                copy.matrixColumns = null;
            }
            if (this.matrixPreferencesField!= null) {
                String sourceMatrixPreferencesField;
                sourceMatrixPreferencesField = this.getMatrixPreferencesField();
                String copyMatrixPreferencesField = ((String) strategy.copy(LocatorUtils.property(locator, "matrixPreferencesField", sourceMatrixPreferencesField), sourceMatrixPreferencesField));
                copy.setMatrixPreferencesField(copyMatrixPreferencesField);
            } else {
                copy.matrixPreferencesField = null;
            }
            if (this.entityX!= null) {
                String sourceEntityX;
                sourceEntityX = this.getEntityX();
                String copyEntityX = ((String) strategy.copy(LocatorUtils.property(locator, "entityX", sourceEntityX), sourceEntityX));
                copy.setEntityX(copyEntityX);
            } else {
                copy.entityX = null;
            }
            if (this.entityY!= null) {
                String sourceEntityY;
                sourceEntityY = this.getEntityY();
                String copyEntityY = ((String) strategy.copy(LocatorUtils.property(locator, "entityY", sourceEntityY), sourceEntityY));
                copy.setEntityY(copyEntityY);
            } else {
                copy.entityY = null;
            }
            if (this.entityMatrix!= null) {
                String sourceEntityMatrix;
                sourceEntityMatrix = this.getEntityMatrix();
                String copyEntityMatrix = ((String) strategy.copy(LocatorUtils.property(locator, "entityMatrix", sourceEntityMatrix), sourceEntityMatrix));
                copy.setEntityMatrix(copyEntityMatrix);
            } else {
                copy.entityMatrix = null;
            }
            if (this.entityFieldMatrixParent!= null) {
                String sourceEntityFieldMatrixParent;
                sourceEntityFieldMatrixParent = this.getEntityFieldMatrixParent();
                String copyEntityFieldMatrixParent = ((String) strategy.copy(LocatorUtils.property(locator, "entityFieldMatrixParent", sourceEntityFieldMatrixParent), sourceEntityFieldMatrixParent));
                copy.setEntityFieldMatrixParent(copyEntityFieldMatrixParent);
            } else {
                copy.entityFieldMatrixParent = null;
            }
            if (this.entityFieldMatrixXRefField!= null) {
                String sourceEntityFieldMatrixXRefField;
                sourceEntityFieldMatrixXRefField = this.getEntityFieldMatrixXRefField();
                String copyEntityFieldMatrixXRefField = ((String) strategy.copy(LocatorUtils.property(locator, "entityFieldMatrixXRefField", sourceEntityFieldMatrixXRefField), sourceEntityFieldMatrixXRefField));
                copy.setEntityFieldMatrixXRefField(copyEntityFieldMatrixXRefField);
            } else {
                copy.entityFieldMatrixXRefField = null;
            }
            if (this.entityMatrixValueField!= null) {
                String sourceEntityMatrixValueField;
                sourceEntityMatrixValueField = this.getEntityMatrixValueField();
                String copyEntityMatrixValueField = ((String) strategy.copy(LocatorUtils.property(locator, "entityMatrixValueField", sourceEntityMatrixValueField), sourceEntityMatrixValueField));
                copy.setEntityMatrixValueField(copyEntityMatrixValueField);
            } else {
                copy.entityMatrixValueField = null;
            }
            if (this.entityMatrixNumberState!= null) {
                String sourceEntityMatrixNumberState;
                sourceEntityMatrixNumberState = this.getEntityMatrixNumberState();
                String copyEntityMatrixNumberState = ((String) strategy.copy(LocatorUtils.property(locator, "entityMatrixNumberState", sourceEntityMatrixNumberState), sourceEntityMatrixNumberState));
                copy.setEntityMatrixNumberState(copyEntityMatrixNumberState);
            } else {
                copy.entityMatrixNumberState = null;
            }
            if (this.entityFieldCategorie!= null) {
                String sourceEntityFieldCategorie;
                sourceEntityFieldCategorie = this.getEntityFieldCategorie();
                String copyEntityFieldCategorie = ((String) strategy.copy(LocatorUtils.property(locator, "entityFieldCategorie", sourceEntityFieldCategorie), sourceEntityFieldCategorie));
                copy.setEntityFieldCategorie(copyEntityFieldCategorie);
            } else {
                copy.entityFieldCategorie = null;
            }
            if (this.entityFieldX!= null) {
                String sourceEntityFieldX;
                sourceEntityFieldX = this.getEntityFieldX();
                String copyEntityFieldX = ((String) strategy.copy(LocatorUtils.property(locator, "entityFieldX", sourceEntityFieldX), sourceEntityFieldX));
                copy.setEntityFieldX(copyEntityFieldX);
            } else {
                copy.entityFieldX = null;
            }
            if (this.entityYParentField!= null) {
                String sourceEntityYParentField;
                sourceEntityYParentField = this.getEntityYParentField();
                String copyEntityYParentField = ((String) strategy.copy(LocatorUtils.property(locator, "entityYParentField", sourceEntityYParentField), sourceEntityYParentField));
                copy.setEntityYParentField(copyEntityYParentField);
            } else {
                copy.entityYParentField = null;
            }
            if (this.entityFieldY!= null) {
                String sourceEntityFieldY;
                sourceEntityFieldY = this.getEntityFieldY();
                String copyEntityFieldY = ((String) strategy.copy(LocatorUtils.property(locator, "entityFieldY", sourceEntityFieldY), sourceEntityFieldY));
                copy.setEntityFieldY(copyEntityFieldY);
            } else {
                copy.entityFieldY = null;
            }
            if (this.entityXSortingFields!= null) {
                String sourceEntityXSortingFields;
                sourceEntityXSortingFields = this.getEntityXSortingFields();
                String copyEntityXSortingFields = ((String) strategy.copy(LocatorUtils.property(locator, "entityXSortingFields", sourceEntityXSortingFields), sourceEntityXSortingFields));
                copy.setEntityXSortingFields(copyEntityXSortingFields);
            } else {
                copy.entityXSortingFields = null;
            }
            if (this.entityYSortingFields!= null) {
                String sourceEntityYSortingFields;
                sourceEntityYSortingFields = this.getEntityYSortingFields();
                String copyEntityYSortingFields = ((String) strategy.copy(LocatorUtils.property(locator, "entityYSortingFields", sourceEntityYSortingFields), sourceEntityYSortingFields));
                copy.setEntityYSortingFields(copyEntityYSortingFields);
            } else {
                copy.entityYSortingFields = null;
            }
            if (this.entityXHeader!= null) {
                String sourceEntityXHeader;
                sourceEntityXHeader = this.getEntityXHeader();
                String copyEntityXHeader = ((String) strategy.copy(LocatorUtils.property(locator, "entityXHeader", sourceEntityXHeader), sourceEntityXHeader));
                copy.setEntityXHeader(copyEntityXHeader);
            } else {
                copy.entityXHeader = null;
            }
            if (this.entityYHeader!= null) {
                String sourceEntityYHeader;
                sourceEntityYHeader = this.getEntityYHeader();
                String copyEntityYHeader = ((String) strategy.copy(LocatorUtils.property(locator, "entityYHeader", sourceEntityYHeader), sourceEntityYHeader));
                copy.setEntityYHeader(copyEntityYHeader);
            } else {
                copy.entityYHeader = null;
            }
            if (this.entityMatrixReferenceField!= null) {
                String sourceEntityMatrixReferenceField;
                sourceEntityMatrixReferenceField = this.getEntityMatrixReferenceField();
                String copyEntityMatrixReferenceField = ((String) strategy.copy(LocatorUtils.property(locator, "entityMatrixReferenceField", sourceEntityMatrixReferenceField), sourceEntityMatrixReferenceField));
                copy.setEntityMatrixReferenceField(copyEntityMatrixReferenceField);
            } else {
                copy.entityMatrixReferenceField = null;
            }
            if (this.entityMatrixValueType!= null) {
                String sourceEntityMatrixValueType;
                sourceEntityMatrixValueType = this.getEntityMatrixValueType();
                String copyEntityMatrixValueType = ((String) strategy.copy(LocatorUtils.property(locator, "entityMatrixValueType", sourceEntityMatrixValueType), sourceEntityMatrixValueType));
                copy.setEntityMatrixValueType(copyEntityMatrixValueType);
            } else {
                copy.entityMatrixValueType = null;
            }
            if (this.entityXVlpId!= null) {
                String sourceEntityXVlpId;
                sourceEntityXVlpId = this.getEntityXVlpId();
                String copyEntityXVlpId = ((String) strategy.copy(LocatorUtils.property(locator, "entityXVlpId", sourceEntityXVlpId), sourceEntityXVlpId));
                copy.setEntityXVlpId(copyEntityXVlpId);
            } else {
                copy.entityXVlpId = null;
            }
            if (this.entityXVlpIdfieldname!= null) {
                String sourceEntityXVlpIdfieldname;
                sourceEntityXVlpIdfieldname = this.getEntityXVlpIdfieldname();
                String copyEntityXVlpIdfieldname = ((String) strategy.copy(LocatorUtils.property(locator, "entityXVlpIdfieldname", sourceEntityXVlpIdfieldname), sourceEntityXVlpIdfieldname));
                copy.setEntityXVlpIdfieldname(copyEntityXVlpIdfieldname);
            } else {
                copy.entityXVlpIdfieldname = null;
            }
            if (this.entityXVlpFieldname!= null) {
                String sourceEntityXVlpFieldname;
                sourceEntityXVlpFieldname = this.getEntityXVlpFieldname();
                String copyEntityXVlpFieldname = ((String) strategy.copy(LocatorUtils.property(locator, "entityXVlpFieldname", sourceEntityXVlpFieldname), sourceEntityXVlpFieldname));
                copy.setEntityXVlpFieldname(copyEntityXVlpFieldname);
            } else {
                copy.entityXVlpFieldname = null;
            }
            if (this.entityXVlpReferenceParamName!= null) {
                String sourceEntityXVlpReferenceParamName;
                sourceEntityXVlpReferenceParamName = this.getEntityXVlpReferenceParamName();
                String copyEntityXVlpReferenceParamName = ((String) strategy.copy(LocatorUtils.property(locator, "entityXVlpReferenceParamName", sourceEntityXVlpReferenceParamName), sourceEntityXVlpReferenceParamName));
                copy.setEntityXVlpReferenceParamName(copyEntityXVlpReferenceParamName);
            } else {
                copy.entityXVlpReferenceParamName = null;
            }
            if (this.cellInputType!= null) {
                String sourceCellInputType;
                sourceCellInputType = this.getCellInputType();
                String copyCellInputType = ((String) strategy.copy(LocatorUtils.property(locator, "cellInputType", sourceCellInputType), sourceCellInputType));
                copy.setCellInputType(copyCellInputType);
            } else {
                copy.cellInputType = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new WebMatrix();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebMatrix.Builder<_B> _other) {
        super.copyTo(_other);
        if (this.matrixColumns == null) {
            _other.matrixColumns = null;
        } else {
            _other.matrixColumns = new ArrayList<WebMatrixColumn.Builder<WebMatrix.Builder<_B>>>();
            for (WebMatrixColumn _item: this.matrixColumns) {
                _other.matrixColumns.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.matrixPreferencesField = this.matrixPreferencesField;
        _other.entityX = this.entityX;
        _other.entityY = this.entityY;
        _other.entityMatrix = this.entityMatrix;
        _other.entityFieldMatrixParent = this.entityFieldMatrixParent;
        _other.entityFieldMatrixXRefField = this.entityFieldMatrixXRefField;
        _other.entityMatrixValueField = this.entityMatrixValueField;
        _other.entityMatrixNumberState = this.entityMatrixNumberState;
        _other.entityFieldCategorie = this.entityFieldCategorie;
        _other.entityFieldX = this.entityFieldX;
        _other.entityYParentField = this.entityYParentField;
        _other.entityFieldY = this.entityFieldY;
        _other.entityXSortingFields = this.entityXSortingFields;
        _other.entityYSortingFields = this.entityYSortingFields;
        _other.entityXHeader = this.entityXHeader;
        _other.entityYHeader = this.entityYHeader;
        _other.entityMatrixReferenceField = this.entityMatrixReferenceField;
        _other.entityMatrixValueType = this.entityMatrixValueType;
        _other.entityXVlpId = this.entityXVlpId;
        _other.entityXVlpIdfieldname = this.entityXVlpIdfieldname;
        _other.entityXVlpFieldname = this.entityXVlpFieldname;
        _other.entityXVlpReferenceParamName = this.entityXVlpReferenceParamName;
        _other.cellInputType = this.cellInputType;
    }

    @Override
    public<_B >WebMatrix.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new WebMatrix.Builder<_B>(_parentBuilder, this, true);
    }

    @Override
    public WebMatrix.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static WebMatrix.Builder<Void> builder() {
        return new WebMatrix.Builder<Void>(null, null, false);
    }

    public static<_B >WebMatrix.Builder<_B> copyOf(final WebComponent _other) {
        final WebMatrix.Builder<_B> _newBuilder = new WebMatrix.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >WebMatrix.Builder<_B> copyOf(final WebInputComponent _other) {
        final WebMatrix.Builder<_B> _newBuilder = new WebMatrix.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >WebMatrix.Builder<_B> copyOf(final WebMatrix _other) {
        final WebMatrix.Builder<_B> _newBuilder = new WebMatrix.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebMatrix.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        super.copyTo(_other, _propertyTree, _propertyTreeUse);
        final PropertyTree matrixColumnsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("matrixColumns"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(matrixColumnsPropertyTree!= null):((matrixColumnsPropertyTree == null)||(!matrixColumnsPropertyTree.isLeaf())))) {
            if (this.matrixColumns == null) {
                _other.matrixColumns = null;
            } else {
                _other.matrixColumns = new ArrayList<WebMatrixColumn.Builder<WebMatrix.Builder<_B>>>();
                for (WebMatrixColumn _item: this.matrixColumns) {
                    _other.matrixColumns.add(((_item == null)?null:_item.newCopyBuilder(_other, matrixColumnsPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree matrixPreferencesFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("matrixPreferencesField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(matrixPreferencesFieldPropertyTree!= null):((matrixPreferencesFieldPropertyTree == null)||(!matrixPreferencesFieldPropertyTree.isLeaf())))) {
            _other.matrixPreferencesField = this.matrixPreferencesField;
        }
        final PropertyTree entityXPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityX"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXPropertyTree!= null):((entityXPropertyTree == null)||(!entityXPropertyTree.isLeaf())))) {
            _other.entityX = this.entityX;
        }
        final PropertyTree entityYPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityY"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityYPropertyTree!= null):((entityYPropertyTree == null)||(!entityYPropertyTree.isLeaf())))) {
            _other.entityY = this.entityY;
        }
        final PropertyTree entityMatrixPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMatrix"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMatrixPropertyTree!= null):((entityMatrixPropertyTree == null)||(!entityMatrixPropertyTree.isLeaf())))) {
            _other.entityMatrix = this.entityMatrix;
        }
        final PropertyTree entityFieldMatrixParentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityFieldMatrixParent"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityFieldMatrixParentPropertyTree!= null):((entityFieldMatrixParentPropertyTree == null)||(!entityFieldMatrixParentPropertyTree.isLeaf())))) {
            _other.entityFieldMatrixParent = this.entityFieldMatrixParent;
        }
        final PropertyTree entityFieldMatrixXRefFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityFieldMatrixXRefField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityFieldMatrixXRefFieldPropertyTree!= null):((entityFieldMatrixXRefFieldPropertyTree == null)||(!entityFieldMatrixXRefFieldPropertyTree.isLeaf())))) {
            _other.entityFieldMatrixXRefField = this.entityFieldMatrixXRefField;
        }
        final PropertyTree entityMatrixValueFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMatrixValueField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMatrixValueFieldPropertyTree!= null):((entityMatrixValueFieldPropertyTree == null)||(!entityMatrixValueFieldPropertyTree.isLeaf())))) {
            _other.entityMatrixValueField = this.entityMatrixValueField;
        }
        final PropertyTree entityMatrixNumberStatePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMatrixNumberState"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMatrixNumberStatePropertyTree!= null):((entityMatrixNumberStatePropertyTree == null)||(!entityMatrixNumberStatePropertyTree.isLeaf())))) {
            _other.entityMatrixNumberState = this.entityMatrixNumberState;
        }
        final PropertyTree entityFieldCategoriePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityFieldCategorie"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityFieldCategoriePropertyTree!= null):((entityFieldCategoriePropertyTree == null)||(!entityFieldCategoriePropertyTree.isLeaf())))) {
            _other.entityFieldCategorie = this.entityFieldCategorie;
        }
        final PropertyTree entityFieldXPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityFieldX"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityFieldXPropertyTree!= null):((entityFieldXPropertyTree == null)||(!entityFieldXPropertyTree.isLeaf())))) {
            _other.entityFieldX = this.entityFieldX;
        }
        final PropertyTree entityYParentFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityYParentField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityYParentFieldPropertyTree!= null):((entityYParentFieldPropertyTree == null)||(!entityYParentFieldPropertyTree.isLeaf())))) {
            _other.entityYParentField = this.entityYParentField;
        }
        final PropertyTree entityFieldYPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityFieldY"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityFieldYPropertyTree!= null):((entityFieldYPropertyTree == null)||(!entityFieldYPropertyTree.isLeaf())))) {
            _other.entityFieldY = this.entityFieldY;
        }
        final PropertyTree entityXSortingFieldsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityXSortingFields"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXSortingFieldsPropertyTree!= null):((entityXSortingFieldsPropertyTree == null)||(!entityXSortingFieldsPropertyTree.isLeaf())))) {
            _other.entityXSortingFields = this.entityXSortingFields;
        }
        final PropertyTree entityYSortingFieldsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityYSortingFields"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityYSortingFieldsPropertyTree!= null):((entityYSortingFieldsPropertyTree == null)||(!entityYSortingFieldsPropertyTree.isLeaf())))) {
            _other.entityYSortingFields = this.entityYSortingFields;
        }
        final PropertyTree entityXHeaderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityXHeader"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXHeaderPropertyTree!= null):((entityXHeaderPropertyTree == null)||(!entityXHeaderPropertyTree.isLeaf())))) {
            _other.entityXHeader = this.entityXHeader;
        }
        final PropertyTree entityYHeaderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityYHeader"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityYHeaderPropertyTree!= null):((entityYHeaderPropertyTree == null)||(!entityYHeaderPropertyTree.isLeaf())))) {
            _other.entityYHeader = this.entityYHeader;
        }
        final PropertyTree entityMatrixReferenceFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMatrixReferenceField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMatrixReferenceFieldPropertyTree!= null):((entityMatrixReferenceFieldPropertyTree == null)||(!entityMatrixReferenceFieldPropertyTree.isLeaf())))) {
            _other.entityMatrixReferenceField = this.entityMatrixReferenceField;
        }
        final PropertyTree entityMatrixValueTypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMatrixValueType"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMatrixValueTypePropertyTree!= null):((entityMatrixValueTypePropertyTree == null)||(!entityMatrixValueTypePropertyTree.isLeaf())))) {
            _other.entityMatrixValueType = this.entityMatrixValueType;
        }
        final PropertyTree entityXVlpIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityXVlpId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXVlpIdPropertyTree!= null):((entityXVlpIdPropertyTree == null)||(!entityXVlpIdPropertyTree.isLeaf())))) {
            _other.entityXVlpId = this.entityXVlpId;
        }
        final PropertyTree entityXVlpIdfieldnamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityXVlpIdfieldname"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXVlpIdfieldnamePropertyTree!= null):((entityXVlpIdfieldnamePropertyTree == null)||(!entityXVlpIdfieldnamePropertyTree.isLeaf())))) {
            _other.entityXVlpIdfieldname = this.entityXVlpIdfieldname;
        }
        final PropertyTree entityXVlpFieldnamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityXVlpFieldname"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXVlpFieldnamePropertyTree!= null):((entityXVlpFieldnamePropertyTree == null)||(!entityXVlpFieldnamePropertyTree.isLeaf())))) {
            _other.entityXVlpFieldname = this.entityXVlpFieldname;
        }
        final PropertyTree entityXVlpReferenceParamNamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityXVlpReferenceParamName"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXVlpReferenceParamNamePropertyTree!= null):((entityXVlpReferenceParamNamePropertyTree == null)||(!entityXVlpReferenceParamNamePropertyTree.isLeaf())))) {
            _other.entityXVlpReferenceParamName = this.entityXVlpReferenceParamName;
        }
        final PropertyTree cellInputTypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("cellInputType"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(cellInputTypePropertyTree!= null):((cellInputTypePropertyTree == null)||(!cellInputTypePropertyTree.isLeaf())))) {
            _other.cellInputType = this.cellInputType;
        }
    }

    @Override
    public<_B >WebMatrix.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new WebMatrix.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    @Override
    public WebMatrix.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >WebMatrix.Builder<_B> copyOf(final WebComponent _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebMatrix.Builder<_B> _newBuilder = new WebMatrix.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >WebMatrix.Builder<_B> copyOf(final WebInputComponent _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebMatrix.Builder<_B> _newBuilder = new WebMatrix.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >WebMatrix.Builder<_B> copyOf(final WebMatrix _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebMatrix.Builder<_B> _newBuilder = new WebMatrix.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static WebMatrix.Builder<Void> copyExcept(final WebComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebMatrix.Builder<Void> copyExcept(final WebInputComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebMatrix.Builder<Void> copyExcept(final WebMatrix _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebMatrix.Builder<Void> copyOnly(final WebComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static WebMatrix.Builder<Void> copyOnly(final WebInputComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static WebMatrix.Builder<Void> copyOnly(final WebMatrix _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >
        extends WebInputComponent.Builder<_B>
        implements Buildable
    {

        private List<WebMatrixColumn.Builder<WebMatrix.Builder<_B>>> matrixColumns;
        private String matrixPreferencesField;
        private String entityX;
        private String entityY;
        private String entityMatrix;
        private String entityFieldMatrixParent;
        private String entityFieldMatrixXRefField;
        private String entityMatrixValueField;
        private String entityMatrixNumberState;
        private String entityFieldCategorie;
        private String entityFieldX;
        private String entityYParentField;
        private String entityFieldY;
        private String entityXSortingFields;
        private String entityYSortingFields;
        private String entityXHeader;
        private String entityYHeader;
        private String entityMatrixReferenceField;
        private String entityMatrixValueType;
        private String entityXVlpId;
        private String entityXVlpIdfieldname;
        private String entityXVlpFieldname;
        private String entityXVlpReferenceParamName;
        private String cellInputType;

        public Builder(final _B _parentBuilder, final WebMatrix _other, final boolean _copy) {
            super(_parentBuilder, _other, _copy);
            if (_other!= null) {
                if (_other.matrixColumns == null) {
                    this.matrixColumns = null;
                } else {
                    this.matrixColumns = new ArrayList<WebMatrixColumn.Builder<WebMatrix.Builder<_B>>>();
                    for (WebMatrixColumn _item: _other.matrixColumns) {
                        this.matrixColumns.add(((_item == null)?null:_item.newCopyBuilder(this)));
                    }
                }
                this.matrixPreferencesField = _other.matrixPreferencesField;
                this.entityX = _other.entityX;
                this.entityY = _other.entityY;
                this.entityMatrix = _other.entityMatrix;
                this.entityFieldMatrixParent = _other.entityFieldMatrixParent;
                this.entityFieldMatrixXRefField = _other.entityFieldMatrixXRefField;
                this.entityMatrixValueField = _other.entityMatrixValueField;
                this.entityMatrixNumberState = _other.entityMatrixNumberState;
                this.entityFieldCategorie = _other.entityFieldCategorie;
                this.entityFieldX = _other.entityFieldX;
                this.entityYParentField = _other.entityYParentField;
                this.entityFieldY = _other.entityFieldY;
                this.entityXSortingFields = _other.entityXSortingFields;
                this.entityYSortingFields = _other.entityYSortingFields;
                this.entityXHeader = _other.entityXHeader;
                this.entityYHeader = _other.entityYHeader;
                this.entityMatrixReferenceField = _other.entityMatrixReferenceField;
                this.entityMatrixValueType = _other.entityMatrixValueType;
                this.entityXVlpId = _other.entityXVlpId;
                this.entityXVlpIdfieldname = _other.entityXVlpIdfieldname;
                this.entityXVlpFieldname = _other.entityXVlpFieldname;
                this.entityXVlpReferenceParamName = _other.entityXVlpReferenceParamName;
                this.cellInputType = _other.cellInputType;
            }
        }

        public Builder(final _B _parentBuilder, final WebMatrix _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            super(_parentBuilder, _other, _copy, _propertyTree, _propertyTreeUse);
            if (_other!= null) {
                final PropertyTree matrixColumnsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("matrixColumns"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(matrixColumnsPropertyTree!= null):((matrixColumnsPropertyTree == null)||(!matrixColumnsPropertyTree.isLeaf())))) {
                    if (_other.matrixColumns == null) {
                        this.matrixColumns = null;
                    } else {
                        this.matrixColumns = new ArrayList<WebMatrixColumn.Builder<WebMatrix.Builder<_B>>>();
                        for (WebMatrixColumn _item: _other.matrixColumns) {
                            this.matrixColumns.add(((_item == null)?null:_item.newCopyBuilder(this, matrixColumnsPropertyTree, _propertyTreeUse)));
                        }
                    }
                }
                final PropertyTree matrixPreferencesFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("matrixPreferencesField"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(matrixPreferencesFieldPropertyTree!= null):((matrixPreferencesFieldPropertyTree == null)||(!matrixPreferencesFieldPropertyTree.isLeaf())))) {
                    this.matrixPreferencesField = _other.matrixPreferencesField;
                }
                final PropertyTree entityXPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityX"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXPropertyTree!= null):((entityXPropertyTree == null)||(!entityXPropertyTree.isLeaf())))) {
                    this.entityX = _other.entityX;
                }
                final PropertyTree entityYPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityY"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityYPropertyTree!= null):((entityYPropertyTree == null)||(!entityYPropertyTree.isLeaf())))) {
                    this.entityY = _other.entityY;
                }
                final PropertyTree entityMatrixPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMatrix"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMatrixPropertyTree!= null):((entityMatrixPropertyTree == null)||(!entityMatrixPropertyTree.isLeaf())))) {
                    this.entityMatrix = _other.entityMatrix;
                }
                final PropertyTree entityFieldMatrixParentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityFieldMatrixParent"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityFieldMatrixParentPropertyTree!= null):((entityFieldMatrixParentPropertyTree == null)||(!entityFieldMatrixParentPropertyTree.isLeaf())))) {
                    this.entityFieldMatrixParent = _other.entityFieldMatrixParent;
                }
                final PropertyTree entityFieldMatrixXRefFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityFieldMatrixXRefField"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityFieldMatrixXRefFieldPropertyTree!= null):((entityFieldMatrixXRefFieldPropertyTree == null)||(!entityFieldMatrixXRefFieldPropertyTree.isLeaf())))) {
                    this.entityFieldMatrixXRefField = _other.entityFieldMatrixXRefField;
                }
                final PropertyTree entityMatrixValueFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMatrixValueField"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMatrixValueFieldPropertyTree!= null):((entityMatrixValueFieldPropertyTree == null)||(!entityMatrixValueFieldPropertyTree.isLeaf())))) {
                    this.entityMatrixValueField = _other.entityMatrixValueField;
                }
                final PropertyTree entityMatrixNumberStatePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMatrixNumberState"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMatrixNumberStatePropertyTree!= null):((entityMatrixNumberStatePropertyTree == null)||(!entityMatrixNumberStatePropertyTree.isLeaf())))) {
                    this.entityMatrixNumberState = _other.entityMatrixNumberState;
                }
                final PropertyTree entityFieldCategoriePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityFieldCategorie"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityFieldCategoriePropertyTree!= null):((entityFieldCategoriePropertyTree == null)||(!entityFieldCategoriePropertyTree.isLeaf())))) {
                    this.entityFieldCategorie = _other.entityFieldCategorie;
                }
                final PropertyTree entityFieldXPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityFieldX"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityFieldXPropertyTree!= null):((entityFieldXPropertyTree == null)||(!entityFieldXPropertyTree.isLeaf())))) {
                    this.entityFieldX = _other.entityFieldX;
                }
                final PropertyTree entityYParentFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityYParentField"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityYParentFieldPropertyTree!= null):((entityYParentFieldPropertyTree == null)||(!entityYParentFieldPropertyTree.isLeaf())))) {
                    this.entityYParentField = _other.entityYParentField;
                }
                final PropertyTree entityFieldYPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityFieldY"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityFieldYPropertyTree!= null):((entityFieldYPropertyTree == null)||(!entityFieldYPropertyTree.isLeaf())))) {
                    this.entityFieldY = _other.entityFieldY;
                }
                final PropertyTree entityXSortingFieldsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityXSortingFields"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXSortingFieldsPropertyTree!= null):((entityXSortingFieldsPropertyTree == null)||(!entityXSortingFieldsPropertyTree.isLeaf())))) {
                    this.entityXSortingFields = _other.entityXSortingFields;
                }
                final PropertyTree entityYSortingFieldsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityYSortingFields"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityYSortingFieldsPropertyTree!= null):((entityYSortingFieldsPropertyTree == null)||(!entityYSortingFieldsPropertyTree.isLeaf())))) {
                    this.entityYSortingFields = _other.entityYSortingFields;
                }
                final PropertyTree entityXHeaderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityXHeader"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXHeaderPropertyTree!= null):((entityXHeaderPropertyTree == null)||(!entityXHeaderPropertyTree.isLeaf())))) {
                    this.entityXHeader = _other.entityXHeader;
                }
                final PropertyTree entityYHeaderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityYHeader"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityYHeaderPropertyTree!= null):((entityYHeaderPropertyTree == null)||(!entityYHeaderPropertyTree.isLeaf())))) {
                    this.entityYHeader = _other.entityYHeader;
                }
                final PropertyTree entityMatrixReferenceFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMatrixReferenceField"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMatrixReferenceFieldPropertyTree!= null):((entityMatrixReferenceFieldPropertyTree == null)||(!entityMatrixReferenceFieldPropertyTree.isLeaf())))) {
                    this.entityMatrixReferenceField = _other.entityMatrixReferenceField;
                }
                final PropertyTree entityMatrixValueTypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMatrixValueType"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMatrixValueTypePropertyTree!= null):((entityMatrixValueTypePropertyTree == null)||(!entityMatrixValueTypePropertyTree.isLeaf())))) {
                    this.entityMatrixValueType = _other.entityMatrixValueType;
                }
                final PropertyTree entityXVlpIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityXVlpId"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXVlpIdPropertyTree!= null):((entityXVlpIdPropertyTree == null)||(!entityXVlpIdPropertyTree.isLeaf())))) {
                    this.entityXVlpId = _other.entityXVlpId;
                }
                final PropertyTree entityXVlpIdfieldnamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityXVlpIdfieldname"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXVlpIdfieldnamePropertyTree!= null):((entityXVlpIdfieldnamePropertyTree == null)||(!entityXVlpIdfieldnamePropertyTree.isLeaf())))) {
                    this.entityXVlpIdfieldname = _other.entityXVlpIdfieldname;
                }
                final PropertyTree entityXVlpFieldnamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityXVlpFieldname"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXVlpFieldnamePropertyTree!= null):((entityXVlpFieldnamePropertyTree == null)||(!entityXVlpFieldnamePropertyTree.isLeaf())))) {
                    this.entityXVlpFieldname = _other.entityXVlpFieldname;
                }
                final PropertyTree entityXVlpReferenceParamNamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityXVlpReferenceParamName"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXVlpReferenceParamNamePropertyTree!= null):((entityXVlpReferenceParamNamePropertyTree == null)||(!entityXVlpReferenceParamNamePropertyTree.isLeaf())))) {
                    this.entityXVlpReferenceParamName = _other.entityXVlpReferenceParamName;
                }
                final PropertyTree cellInputTypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("cellInputType"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(cellInputTypePropertyTree!= null):((cellInputTypePropertyTree == null)||(!cellInputTypePropertyTree.isLeaf())))) {
                    this.cellInputType = _other.cellInputType;
                }
            }
        }

        protected<_P extends WebMatrix >_P init(final _P _product) {
            if (this.matrixColumns!= null) {
                final List<WebMatrixColumn> matrixColumns = new ArrayList<WebMatrixColumn>(this.matrixColumns.size());
                for (WebMatrixColumn.Builder<WebMatrix.Builder<_B>> _item: this.matrixColumns) {
                    matrixColumns.add(_item.build());
                }
                _product.matrixColumns = matrixColumns;
            }
            _product.matrixPreferencesField = this.matrixPreferencesField;
            _product.entityX = this.entityX;
            _product.entityY = this.entityY;
            _product.entityMatrix = this.entityMatrix;
            _product.entityFieldMatrixParent = this.entityFieldMatrixParent;
            _product.entityFieldMatrixXRefField = this.entityFieldMatrixXRefField;
            _product.entityMatrixValueField = this.entityMatrixValueField;
            _product.entityMatrixNumberState = this.entityMatrixNumberState;
            _product.entityFieldCategorie = this.entityFieldCategorie;
            _product.entityFieldX = this.entityFieldX;
            _product.entityYParentField = this.entityYParentField;
            _product.entityFieldY = this.entityFieldY;
            _product.entityXSortingFields = this.entityXSortingFields;
            _product.entityYSortingFields = this.entityYSortingFields;
            _product.entityXHeader = this.entityXHeader;
            _product.entityYHeader = this.entityYHeader;
            _product.entityMatrixReferenceField = this.entityMatrixReferenceField;
            _product.entityMatrixValueType = this.entityMatrixValueType;
            _product.entityXVlpId = this.entityXVlpId;
            _product.entityXVlpIdfieldname = this.entityXVlpIdfieldname;
            _product.entityXVlpFieldname = this.entityXVlpFieldname;
            _product.entityXVlpReferenceParamName = this.entityXVlpReferenceParamName;
            _product.cellInputType = this.cellInputType;
            return super.init(_product);
        }

        /**
         * Adds the given items to the value of "matrixColumns"
         * 
         * @param matrixColumns
         *     Items to add to the value of the "matrixColumns" property
         */
        public WebMatrix.Builder<_B> addMatrixColumns(final Iterable<? extends WebMatrixColumn> matrixColumns) {
            if (matrixColumns!= null) {
                if (this.matrixColumns == null) {
                    this.matrixColumns = new ArrayList<WebMatrixColumn.Builder<WebMatrix.Builder<_B>>>();
                }
                for (WebMatrixColumn _item: matrixColumns) {
                    this.matrixColumns.add(new WebMatrixColumn.Builder<WebMatrix.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "matrixColumns" (any previous value will be replaced)
         * 
         * @param matrixColumns
         *     New value of the "matrixColumns" property.
         */
        public WebMatrix.Builder<_B> withMatrixColumns(final Iterable<? extends WebMatrixColumn> matrixColumns) {
            if (this.matrixColumns!= null) {
                this.matrixColumns.clear();
            }
            return addMatrixColumns(matrixColumns);
        }

        /**
         * Adds the given items to the value of "matrixColumns"
         * 
         * @param matrixColumns
         *     Items to add to the value of the "matrixColumns" property
         */
        public WebMatrix.Builder<_B> addMatrixColumns(WebMatrixColumn... matrixColumns) {
            addMatrixColumns(Arrays.asList(matrixColumns));
            return this;
        }

        /**
         * Sets the new value of "matrixColumns" (any previous value will be replaced)
         * 
         * @param matrixColumns
         *     New value of the "matrixColumns" property.
         */
        public WebMatrix.Builder<_B> withMatrixColumns(WebMatrixColumn... matrixColumns) {
            withMatrixColumns(Arrays.asList(matrixColumns));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "MatrixColumns" property.
         * Use {@link org.nuclos.schema.layout.web.WebMatrixColumn.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "MatrixColumns" property.
         *     Use {@link org.nuclos.schema.layout.web.WebMatrixColumn.Builder#end()} to return to the current builder.
         */
        public WebMatrixColumn.Builder<? extends WebMatrix.Builder<_B>> addMatrixColumns() {
            if (this.matrixColumns == null) {
                this.matrixColumns = new ArrayList<WebMatrixColumn.Builder<WebMatrix.Builder<_B>>>();
            }
            final WebMatrixColumn.Builder<WebMatrix.Builder<_B>> matrixColumns_Builder = new WebMatrixColumn.Builder<WebMatrix.Builder<_B>>(this, null, false);
            this.matrixColumns.add(matrixColumns_Builder);
            return matrixColumns_Builder;
        }

        /**
         * Sets the new value of "matrixPreferencesField" (any previous value will be replaced)
         * 
         * @param matrixPreferencesField
         *     New value of the "matrixPreferencesField" property.
         */
        public WebMatrix.Builder<_B> withMatrixPreferencesField(final String matrixPreferencesField) {
            this.matrixPreferencesField = matrixPreferencesField;
            return this;
        }

        /**
         * Sets the new value of "entityX" (any previous value will be replaced)
         * 
         * @param entityX
         *     New value of the "entityX" property.
         */
        public WebMatrix.Builder<_B> withEntityX(final String entityX) {
            this.entityX = entityX;
            return this;
        }

        /**
         * Sets the new value of "entityY" (any previous value will be replaced)
         * 
         * @param entityY
         *     New value of the "entityY" property.
         */
        public WebMatrix.Builder<_B> withEntityY(final String entityY) {
            this.entityY = entityY;
            return this;
        }

        /**
         * Sets the new value of "entityMatrix" (any previous value will be replaced)
         * 
         * @param entityMatrix
         *     New value of the "entityMatrix" property.
         */
        public WebMatrix.Builder<_B> withEntityMatrix(final String entityMatrix) {
            this.entityMatrix = entityMatrix;
            return this;
        }

        /**
         * Sets the new value of "entityFieldMatrixParent" (any previous value will be replaced)
         * 
         * @param entityFieldMatrixParent
         *     New value of the "entityFieldMatrixParent" property.
         */
        public WebMatrix.Builder<_B> withEntityFieldMatrixParent(final String entityFieldMatrixParent) {
            this.entityFieldMatrixParent = entityFieldMatrixParent;
            return this;
        }

        /**
         * Sets the new value of "entityFieldMatrixXRefField" (any previous value will be replaced)
         * 
         * @param entityFieldMatrixXRefField
         *     New value of the "entityFieldMatrixXRefField" property.
         */
        public WebMatrix.Builder<_B> withEntityFieldMatrixXRefField(final String entityFieldMatrixXRefField) {
            this.entityFieldMatrixXRefField = entityFieldMatrixXRefField;
            return this;
        }

        /**
         * Sets the new value of "entityMatrixValueField" (any previous value will be replaced)
         * 
         * @param entityMatrixValueField
         *     New value of the "entityMatrixValueField" property.
         */
        public WebMatrix.Builder<_B> withEntityMatrixValueField(final String entityMatrixValueField) {
            this.entityMatrixValueField = entityMatrixValueField;
            return this;
        }

        /**
         * Sets the new value of "entityMatrixNumberState" (any previous value will be replaced)
         * 
         * @param entityMatrixNumberState
         *     New value of the "entityMatrixNumberState" property.
         */
        public WebMatrix.Builder<_B> withEntityMatrixNumberState(final String entityMatrixNumberState) {
            this.entityMatrixNumberState = entityMatrixNumberState;
            return this;
        }

        /**
         * Sets the new value of "entityFieldCategorie" (any previous value will be replaced)
         * 
         * @param entityFieldCategorie
         *     New value of the "entityFieldCategorie" property.
         */
        public WebMatrix.Builder<_B> withEntityFieldCategorie(final String entityFieldCategorie) {
            this.entityFieldCategorie = entityFieldCategorie;
            return this;
        }

        /**
         * Sets the new value of "entityFieldX" (any previous value will be replaced)
         * 
         * @param entityFieldX
         *     New value of the "entityFieldX" property.
         */
        public WebMatrix.Builder<_B> withEntityFieldX(final String entityFieldX) {
            this.entityFieldX = entityFieldX;
            return this;
        }

        /**
         * Sets the new value of "entityYParentField" (any previous value will be replaced)
         * 
         * @param entityYParentField
         *     New value of the "entityYParentField" property.
         */
        public WebMatrix.Builder<_B> withEntityYParentField(final String entityYParentField) {
            this.entityYParentField = entityYParentField;
            return this;
        }

        /**
         * Sets the new value of "entityFieldY" (any previous value will be replaced)
         * 
         * @param entityFieldY
         *     New value of the "entityFieldY" property.
         */
        public WebMatrix.Builder<_B> withEntityFieldY(final String entityFieldY) {
            this.entityFieldY = entityFieldY;
            return this;
        }

        /**
         * Sets the new value of "entityXSortingFields" (any previous value will be replaced)
         * 
         * @param entityXSortingFields
         *     New value of the "entityXSortingFields" property.
         */
        public WebMatrix.Builder<_B> withEntityXSortingFields(final String entityXSortingFields) {
            this.entityXSortingFields = entityXSortingFields;
            return this;
        }

        /**
         * Sets the new value of "entityYSortingFields" (any previous value will be replaced)
         * 
         * @param entityYSortingFields
         *     New value of the "entityYSortingFields" property.
         */
        public WebMatrix.Builder<_B> withEntityYSortingFields(final String entityYSortingFields) {
            this.entityYSortingFields = entityYSortingFields;
            return this;
        }

        /**
         * Sets the new value of "entityXHeader" (any previous value will be replaced)
         * 
         * @param entityXHeader
         *     New value of the "entityXHeader" property.
         */
        public WebMatrix.Builder<_B> withEntityXHeader(final String entityXHeader) {
            this.entityXHeader = entityXHeader;
            return this;
        }

        /**
         * Sets the new value of "entityYHeader" (any previous value will be replaced)
         * 
         * @param entityYHeader
         *     New value of the "entityYHeader" property.
         */
        public WebMatrix.Builder<_B> withEntityYHeader(final String entityYHeader) {
            this.entityYHeader = entityYHeader;
            return this;
        }

        /**
         * Sets the new value of "entityMatrixReferenceField" (any previous value will be replaced)
         * 
         * @param entityMatrixReferenceField
         *     New value of the "entityMatrixReferenceField" property.
         */
        public WebMatrix.Builder<_B> withEntityMatrixReferenceField(final String entityMatrixReferenceField) {
            this.entityMatrixReferenceField = entityMatrixReferenceField;
            return this;
        }

        /**
         * Sets the new value of "entityMatrixValueType" (any previous value will be replaced)
         * 
         * @param entityMatrixValueType
         *     New value of the "entityMatrixValueType" property.
         */
        public WebMatrix.Builder<_B> withEntityMatrixValueType(final String entityMatrixValueType) {
            this.entityMatrixValueType = entityMatrixValueType;
            return this;
        }

        /**
         * Sets the new value of "entityXVlpId" (any previous value will be replaced)
         * 
         * @param entityXVlpId
         *     New value of the "entityXVlpId" property.
         */
        public WebMatrix.Builder<_B> withEntityXVlpId(final String entityXVlpId) {
            this.entityXVlpId = entityXVlpId;
            return this;
        }

        /**
         * Sets the new value of "entityXVlpIdfieldname" (any previous value will be replaced)
         * 
         * @param entityXVlpIdfieldname
         *     New value of the "entityXVlpIdfieldname" property.
         */
        public WebMatrix.Builder<_B> withEntityXVlpIdfieldname(final String entityXVlpIdfieldname) {
            this.entityXVlpIdfieldname = entityXVlpIdfieldname;
            return this;
        }

        /**
         * Sets the new value of "entityXVlpFieldname" (any previous value will be replaced)
         * 
         * @param entityXVlpFieldname
         *     New value of the "entityXVlpFieldname" property.
         */
        public WebMatrix.Builder<_B> withEntityXVlpFieldname(final String entityXVlpFieldname) {
            this.entityXVlpFieldname = entityXVlpFieldname;
            return this;
        }

        /**
         * Sets the new value of "entityXVlpReferenceParamName" (any previous value will be replaced)
         * 
         * @param entityXVlpReferenceParamName
         *     New value of the "entityXVlpReferenceParamName" property.
         */
        public WebMatrix.Builder<_B> withEntityXVlpReferenceParamName(final String entityXVlpReferenceParamName) {
            this.entityXVlpReferenceParamName = entityXVlpReferenceParamName;
            return this;
        }

        /**
         * Sets the new value of "cellInputType" (any previous value will be replaced)
         * 
         * @param cellInputType
         *     New value of the "cellInputType" property.
         */
        public WebMatrix.Builder<_B> withCellInputType(final String cellInputType) {
            this.cellInputType = cellInputType;
            return this;
        }

        /**
         * Sets the new value of "valuelistProvider" (any previous value will be replaced)
         * 
         * @param valuelistProvider
         *     New value of the "valuelistProvider" property.
         */
        @Override
        public WebMatrix.Builder<_B> withValuelistProvider(final WebValuelistProvider valuelistProvider) {
            super.withValuelistProvider(valuelistProvider);
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "valuelistProvider" property.
         * Use {@link org.nuclos.schema.layout.web.WebValuelistProvider.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "valuelistProvider" property.
         *     Use {@link org.nuclos.schema.layout.web.WebValuelistProvider.Builder#end()} to return to the current builder.
         */
        public WebValuelistProvider.Builder<? extends WebMatrix.Builder<_B>> withValuelistProvider() {
            return ((WebValuelistProvider.Builder<? extends WebMatrix.Builder<_B>> ) super.withValuelistProvider());
        }

        /**
         * Sets the new value of "enabled" (any previous value will be replaced)
         * 
         * @param enabled
         *     New value of the "enabled" property.
         */
        @Override
        public WebMatrix.Builder<_B> withEnabled(final Boolean enabled) {
            super.withEnabled(enabled);
            return this;
        }

        /**
         * Sets the new value of "editable" (any previous value will be replaced)
         * 
         * @param editable
         *     New value of the "editable" property.
         */
        @Override
        public WebMatrix.Builder<_B> withEditable(final Boolean editable) {
            super.withEditable(editable);
            return this;
        }

        /**
         * Sets the new value of "insertable" (any previous value will be replaced)
         * 
         * @param insertable
         *     New value of the "insertable" property.
         */
        @Override
        public WebMatrix.Builder<_B> withInsertable(final Boolean insertable) {
            super.withInsertable(insertable);
            return this;
        }

        /**
         * Sets the new value of "columns" (any previous value will be replaced)
         * 
         * @param columns
         *     New value of the "columns" property.
         */
        @Override
        public WebMatrix.Builder<_B> withColumns(final String columns) {
            super.withColumns(columns);
            return this;
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        @Override
        public WebMatrix.Builder<_B> addAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            super.addAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        @Override
        public WebMatrix.Builder<_B> addAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            super.addAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        @Override
        public WebMatrix.Builder<_B> withAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            super.withAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        @Override
        public WebMatrix.Builder<_B> withAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            super.withAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "advancedProperties" property.
         * Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "advancedProperties" property.
         *     Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         */
        @Override
        public WebAdvancedProperty.Builder<? extends WebMatrix.Builder<_B>> addAdvancedProperties() {
            return ((WebAdvancedProperty.Builder<? extends WebMatrix.Builder<_B>> ) super.addAdvancedProperties());
        }

        /**
         * Sets the new value of "id" (any previous value will be replaced)
         * 
         * @param id
         *     New value of the "id" property.
         */
        @Override
        public WebMatrix.Builder<_B> withId(final String id) {
            super.withId(id);
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        @Override
        public WebMatrix.Builder<_B> withName(final String name) {
            super.withName(name);
            return this;
        }

        /**
         * Sets the new value of "column" (any previous value will be replaced)
         * 
         * @param column
         *     New value of the "column" property.
         */
        @Override
        public WebMatrix.Builder<_B> withColumn(final BigInteger column) {
            super.withColumn(column);
            return this;
        }

        /**
         * Sets the new value of "row" (any previous value will be replaced)
         * 
         * @param row
         *     New value of the "row" property.
         */
        @Override
        public WebMatrix.Builder<_B> withRow(final BigInteger row) {
            super.withRow(row);
            return this;
        }

        /**
         * Sets the new value of "colspan" (any previous value will be replaced)
         * 
         * @param colspan
         *     New value of the "colspan" property.
         */
        @Override
        public WebMatrix.Builder<_B> withColspan(final BigInteger colspan) {
            super.withColspan(colspan);
            return this;
        }

        /**
         * Sets the new value of "rowspan" (any previous value will be replaced)
         * 
         * @param rowspan
         *     New value of the "rowspan" property.
         */
        @Override
        public WebMatrix.Builder<_B> withRowspan(final BigInteger rowspan) {
            super.withRowspan(rowspan);
            return this;
        }

        /**
         * Sets the new value of "fontSize" (any previous value will be replaced)
         * 
         * @param fontSize
         *     New value of the "fontSize" property.
         */
        @Override
        public WebMatrix.Builder<_B> withFontSize(final String fontSize) {
            super.withFontSize(fontSize);
            return this;
        }

        /**
         * Sets the new value of "textColor" (any previous value will be replaced)
         * 
         * @param textColor
         *     New value of the "textColor" property.
         */
        @Override
        public WebMatrix.Builder<_B> withTextColor(final String textColor) {
            super.withTextColor(textColor);
            return this;
        }

        /**
         * Sets the new value of "bold" (any previous value will be replaced)
         * 
         * @param bold
         *     New value of the "bold" property.
         */
        @Override
        public WebMatrix.Builder<_B> withBold(final Boolean bold) {
            super.withBold(bold);
            return this;
        }

        /**
         * Sets the new value of "italic" (any previous value will be replaced)
         * 
         * @param italic
         *     New value of the "italic" property.
         */
        @Override
        public WebMatrix.Builder<_B> withItalic(final Boolean italic) {
            super.withItalic(italic);
            return this;
        }

        /**
         * Sets the new value of "underline" (any previous value will be replaced)
         * 
         * @param underline
         *     New value of the "underline" property.
         */
        @Override
        public WebMatrix.Builder<_B> withUnderline(final Boolean underline) {
            super.withUnderline(underline);
            return this;
        }

        /**
         * Sets the new value of "nextFocusComponent" (any previous value will be replaced)
         * 
         * @param nextFocusComponent
         *     New value of the "nextFocusComponent" property.
         */
        @Override
        public WebMatrix.Builder<_B> withNextFocusComponent(final String nextFocusComponent) {
            super.withNextFocusComponent(nextFocusComponent);
            return this;
        }

        /**
         * Sets the new value of "nextFocusField" (any previous value will be replaced)
         * 
         * @param nextFocusField
         *     New value of the "nextFocusField" property.
         */
        @Override
        public WebMatrix.Builder<_B> withNextFocusField(final String nextFocusField) {
            super.withNextFocusField(nextFocusField);
            return this;
        }

        /**
         * Sets the new value of "alternativeTooltip" (any previous value will be replaced)
         * 
         * @param alternativeTooltip
         *     New value of the "alternativeTooltip" property.
         */
        @Override
        public WebMatrix.Builder<_B> withAlternativeTooltip(final String alternativeTooltip) {
            super.withAlternativeTooltip(alternativeTooltip);
            return this;
        }

        @Override
        public WebMatrix build() {
            if (_storedValue == null) {
                return this.init(new WebMatrix());
            } else {
                return ((WebMatrix) _storedValue);
            }
        }

        public WebMatrix.Builder<_B> copyOf(final WebMatrix _other) {
            _other.copyTo(this);
            return this;
        }

        public WebMatrix.Builder<_B> copyOf(final WebMatrix.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends WebMatrix.Selector<WebMatrix.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static WebMatrix.Select _root() {
            return new WebMatrix.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends WebInputComponent.Selector<TRoot, TParent>
    {

        private WebMatrixColumn.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> matrixColumns = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> matrixPreferencesField = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityX = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityY = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityMatrix = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityFieldMatrixParent = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityFieldMatrixXRefField = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityMatrixValueField = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityMatrixNumberState = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityFieldCategorie = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityFieldX = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityYParentField = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityFieldY = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityXSortingFields = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityYSortingFields = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityXHeader = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityYHeader = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityMatrixReferenceField = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityMatrixValueType = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityXVlpId = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityXVlpIdfieldname = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityXVlpFieldname = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityXVlpReferenceParamName = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> cellInputType = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.matrixColumns!= null) {
                products.put("matrixColumns", this.matrixColumns.init());
            }
            if (this.matrixPreferencesField!= null) {
                products.put("matrixPreferencesField", this.matrixPreferencesField.init());
            }
            if (this.entityX!= null) {
                products.put("entityX", this.entityX.init());
            }
            if (this.entityY!= null) {
                products.put("entityY", this.entityY.init());
            }
            if (this.entityMatrix!= null) {
                products.put("entityMatrix", this.entityMatrix.init());
            }
            if (this.entityFieldMatrixParent!= null) {
                products.put("entityFieldMatrixParent", this.entityFieldMatrixParent.init());
            }
            if (this.entityFieldMatrixXRefField!= null) {
                products.put("entityFieldMatrixXRefField", this.entityFieldMatrixXRefField.init());
            }
            if (this.entityMatrixValueField!= null) {
                products.put("entityMatrixValueField", this.entityMatrixValueField.init());
            }
            if (this.entityMatrixNumberState!= null) {
                products.put("entityMatrixNumberState", this.entityMatrixNumberState.init());
            }
            if (this.entityFieldCategorie!= null) {
                products.put("entityFieldCategorie", this.entityFieldCategorie.init());
            }
            if (this.entityFieldX!= null) {
                products.put("entityFieldX", this.entityFieldX.init());
            }
            if (this.entityYParentField!= null) {
                products.put("entityYParentField", this.entityYParentField.init());
            }
            if (this.entityFieldY!= null) {
                products.put("entityFieldY", this.entityFieldY.init());
            }
            if (this.entityXSortingFields!= null) {
                products.put("entityXSortingFields", this.entityXSortingFields.init());
            }
            if (this.entityYSortingFields!= null) {
                products.put("entityYSortingFields", this.entityYSortingFields.init());
            }
            if (this.entityXHeader!= null) {
                products.put("entityXHeader", this.entityXHeader.init());
            }
            if (this.entityYHeader!= null) {
                products.put("entityYHeader", this.entityYHeader.init());
            }
            if (this.entityMatrixReferenceField!= null) {
                products.put("entityMatrixReferenceField", this.entityMatrixReferenceField.init());
            }
            if (this.entityMatrixValueType!= null) {
                products.put("entityMatrixValueType", this.entityMatrixValueType.init());
            }
            if (this.entityXVlpId!= null) {
                products.put("entityXVlpId", this.entityXVlpId.init());
            }
            if (this.entityXVlpIdfieldname!= null) {
                products.put("entityXVlpIdfieldname", this.entityXVlpIdfieldname.init());
            }
            if (this.entityXVlpFieldname!= null) {
                products.put("entityXVlpFieldname", this.entityXVlpFieldname.init());
            }
            if (this.entityXVlpReferenceParamName!= null) {
                products.put("entityXVlpReferenceParamName", this.entityXVlpReferenceParamName.init());
            }
            if (this.cellInputType!= null) {
                products.put("cellInputType", this.cellInputType.init());
            }
            return products;
        }

        public WebMatrixColumn.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> matrixColumns() {
            return ((this.matrixColumns == null)?this.matrixColumns = new WebMatrixColumn.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>>(this._root, this, "matrixColumns"):this.matrixColumns);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> matrixPreferencesField() {
            return ((this.matrixPreferencesField == null)?this.matrixPreferencesField = new com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>>(this._root, this, "matrixPreferencesField"):this.matrixPreferencesField);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityX() {
            return ((this.entityX == null)?this.entityX = new com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>>(this._root, this, "entityX"):this.entityX);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityY() {
            return ((this.entityY == null)?this.entityY = new com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>>(this._root, this, "entityY"):this.entityY);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityMatrix() {
            return ((this.entityMatrix == null)?this.entityMatrix = new com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>>(this._root, this, "entityMatrix"):this.entityMatrix);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityFieldMatrixParent() {
            return ((this.entityFieldMatrixParent == null)?this.entityFieldMatrixParent = new com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>>(this._root, this, "entityFieldMatrixParent"):this.entityFieldMatrixParent);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityFieldMatrixXRefField() {
            return ((this.entityFieldMatrixXRefField == null)?this.entityFieldMatrixXRefField = new com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>>(this._root, this, "entityFieldMatrixXRefField"):this.entityFieldMatrixXRefField);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityMatrixValueField() {
            return ((this.entityMatrixValueField == null)?this.entityMatrixValueField = new com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>>(this._root, this, "entityMatrixValueField"):this.entityMatrixValueField);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityMatrixNumberState() {
            return ((this.entityMatrixNumberState == null)?this.entityMatrixNumberState = new com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>>(this._root, this, "entityMatrixNumberState"):this.entityMatrixNumberState);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityFieldCategorie() {
            return ((this.entityFieldCategorie == null)?this.entityFieldCategorie = new com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>>(this._root, this, "entityFieldCategorie"):this.entityFieldCategorie);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityFieldX() {
            return ((this.entityFieldX == null)?this.entityFieldX = new com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>>(this._root, this, "entityFieldX"):this.entityFieldX);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityYParentField() {
            return ((this.entityYParentField == null)?this.entityYParentField = new com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>>(this._root, this, "entityYParentField"):this.entityYParentField);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityFieldY() {
            return ((this.entityFieldY == null)?this.entityFieldY = new com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>>(this._root, this, "entityFieldY"):this.entityFieldY);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityXSortingFields() {
            return ((this.entityXSortingFields == null)?this.entityXSortingFields = new com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>>(this._root, this, "entityXSortingFields"):this.entityXSortingFields);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityYSortingFields() {
            return ((this.entityYSortingFields == null)?this.entityYSortingFields = new com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>>(this._root, this, "entityYSortingFields"):this.entityYSortingFields);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityXHeader() {
            return ((this.entityXHeader == null)?this.entityXHeader = new com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>>(this._root, this, "entityXHeader"):this.entityXHeader);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityYHeader() {
            return ((this.entityYHeader == null)?this.entityYHeader = new com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>>(this._root, this, "entityYHeader"):this.entityYHeader);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityMatrixReferenceField() {
            return ((this.entityMatrixReferenceField == null)?this.entityMatrixReferenceField = new com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>>(this._root, this, "entityMatrixReferenceField"):this.entityMatrixReferenceField);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityMatrixValueType() {
            return ((this.entityMatrixValueType == null)?this.entityMatrixValueType = new com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>>(this._root, this, "entityMatrixValueType"):this.entityMatrixValueType);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityXVlpId() {
            return ((this.entityXVlpId == null)?this.entityXVlpId = new com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>>(this._root, this, "entityXVlpId"):this.entityXVlpId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityXVlpIdfieldname() {
            return ((this.entityXVlpIdfieldname == null)?this.entityXVlpIdfieldname = new com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>>(this._root, this, "entityXVlpIdfieldname"):this.entityXVlpIdfieldname);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityXVlpFieldname() {
            return ((this.entityXVlpFieldname == null)?this.entityXVlpFieldname = new com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>>(this._root, this, "entityXVlpFieldname"):this.entityXVlpFieldname);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> entityXVlpReferenceParamName() {
            return ((this.entityXVlpReferenceParamName == null)?this.entityXVlpReferenceParamName = new com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>>(this._root, this, "entityXVlpReferenceParamName"):this.entityXVlpReferenceParamName);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>> cellInputType() {
            return ((this.cellInputType == null)?this.cellInputType = new com.kscs.util.jaxb.Selector<TRoot, WebMatrix.Selector<TRoot, TParent>>(this._root, this, "cellInputType"):this.cellInputType);
        }

    }

}
