
package org.nuclos.schema.layout.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * 
 * 				The columns of a subform component.
 * 				TODO: Which attributes are really needed?
 * 			
 * 
 * <p>Java class for web-subform-column complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="web-subform-column"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="valuelist-provider" type="{urn:org.nuclos.schema.layout.web}web-valuelist-provider" minOccurs="0"/&gt;
 *         &lt;element name="advanced-properties" type="{urn:org.nuclos.schema.layout.web}web-advanced-property" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="label" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="control-type" type="{urn:org.nuclos.schema.layout.web}subform-control-type" /&gt;
 *       &lt;attribute name="control-type-class" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="enabled" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="not-cloneable" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="insertable" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="rows" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="columns" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="resource-id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="width" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="next-focus-component" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="next-focus-field" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="custom-usage-search" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="visible" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "web-subform-column", propOrder = {
    "valuelistProvider",
    "advancedProperties"
})
public class WebSubformColumn implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "valuelist-provider")
    protected WebValuelistProvider valuelistProvider;
    @XmlElement(name = "advanced-properties")
    protected List<WebAdvancedProperty> advancedProperties;
    @XmlAttribute(name = "name")
    protected String name;
    @XmlAttribute(name = "label")
    protected String label;
    @XmlAttribute(name = "control-type")
    protected SubformControlType controlType;
    @XmlAttribute(name = "control-type-class")
    protected String controlTypeClass;
    @XmlAttribute(name = "enabled")
    protected Boolean enabled;
    @XmlAttribute(name = "not-cloneable")
    protected Boolean notCloneable;
    @XmlAttribute(name = "insertable")
    protected Boolean insertable;
    @XmlAttribute(name = "rows")
    protected String rows;
    @XmlAttribute(name = "columns")
    protected String columns;
    @XmlAttribute(name = "resource-id")
    protected String resourceId;
    @XmlAttribute(name = "width")
    protected String width;
    @XmlAttribute(name = "next-focus-component")
    protected String nextFocusComponent;
    @XmlAttribute(name = "next-focus-field")
    protected String nextFocusField;
    @XmlAttribute(name = "custom-usage-search")
    protected String customUsageSearch;
    @XmlAttribute(name = "visible")
    protected Boolean visible;

    /**
     * Gets the value of the valuelistProvider property.
     * 
     * @return
     *     possible object is
     *     {@link WebValuelistProvider }
     *     
     */
    public WebValuelistProvider getValuelistProvider() {
        return valuelistProvider;
    }

    /**
     * Sets the value of the valuelistProvider property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebValuelistProvider }
     *     
     */
    public void setValuelistProvider(WebValuelistProvider value) {
        this.valuelistProvider = value;
    }

    /**
     * Gets the value of the advancedProperties property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the advancedProperties property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdvancedProperties().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WebAdvancedProperty }
     * 
     * 
     */
    public List<WebAdvancedProperty> getAdvancedProperties() {
        if (advancedProperties == null) {
            advancedProperties = new ArrayList<WebAdvancedProperty>();
        }
        return this.advancedProperties;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the controlType property.
     * 
     * @return
     *     possible object is
     *     {@link SubformControlType }
     *     
     */
    public SubformControlType getControlType() {
        return controlType;
    }

    /**
     * Sets the value of the controlType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubformControlType }
     *     
     */
    public void setControlType(SubformControlType value) {
        this.controlType = value;
    }

    /**
     * Gets the value of the controlTypeClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlTypeClass() {
        return controlTypeClass;
    }

    /**
     * Sets the value of the controlTypeClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlTypeClass(String value) {
        this.controlTypeClass = value;
    }

    /**
     * Gets the value of the enabled property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnabled() {
        return enabled;
    }

    /**
     * Sets the value of the enabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnabled(Boolean value) {
        this.enabled = value;
    }

    /**
     * Gets the value of the notCloneable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNotCloneable() {
        return notCloneable;
    }

    /**
     * Sets the value of the notCloneable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNotCloneable(Boolean value) {
        this.notCloneable = value;
    }

    /**
     * Gets the value of the insertable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isInsertable() {
        return insertable;
    }

    /**
     * Sets the value of the insertable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInsertable(Boolean value) {
        this.insertable = value;
    }

    /**
     * Gets the value of the rows property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRows() {
        return rows;
    }

    /**
     * Sets the value of the rows property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRows(String value) {
        this.rows = value;
    }

    /**
     * Gets the value of the columns property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColumns() {
        return columns;
    }

    /**
     * Sets the value of the columns property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColumns(String value) {
        this.columns = value;
    }

    /**
     * Gets the value of the resourceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceId() {
        return resourceId;
    }

    /**
     * Sets the value of the resourceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceId(String value) {
        this.resourceId = value;
    }

    /**
     * Gets the value of the width property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWidth() {
        return width;
    }

    /**
     * Sets the value of the width property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWidth(String value) {
        this.width = value;
    }

    /**
     * Gets the value of the nextFocusComponent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextFocusComponent() {
        return nextFocusComponent;
    }

    /**
     * Sets the value of the nextFocusComponent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextFocusComponent(String value) {
        this.nextFocusComponent = value;
    }

    /**
     * Gets the value of the nextFocusField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextFocusField() {
        return nextFocusField;
    }

    /**
     * Sets the value of the nextFocusField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextFocusField(String value) {
        this.nextFocusField = value;
    }

    /**
     * Gets the value of the customUsageSearch property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomUsageSearch() {
        return customUsageSearch;
    }

    /**
     * Sets the value of the customUsageSearch property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomUsageSearch(String value) {
        this.customUsageSearch = value;
    }

    /**
     * Gets the value of the visible property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVisible() {
        return visible;
    }

    /**
     * Sets the value of the visible property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVisible(Boolean value) {
        this.visible = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            WebValuelistProvider theValuelistProvider;
            theValuelistProvider = this.getValuelistProvider();
            strategy.appendField(locator, this, "valuelistProvider", buffer, theValuelistProvider);
        }
        {
            List<WebAdvancedProperty> theAdvancedProperties;
            theAdvancedProperties = (((this.advancedProperties!= null)&&(!this.advancedProperties.isEmpty()))?this.getAdvancedProperties():null);
            strategy.appendField(locator, this, "advancedProperties", buffer, theAdvancedProperties);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String theLabel;
            theLabel = this.getLabel();
            strategy.appendField(locator, this, "label", buffer, theLabel);
        }
        {
            SubformControlType theControlType;
            theControlType = this.getControlType();
            strategy.appendField(locator, this, "controlType", buffer, theControlType);
        }
        {
            String theControlTypeClass;
            theControlTypeClass = this.getControlTypeClass();
            strategy.appendField(locator, this, "controlTypeClass", buffer, theControlTypeClass);
        }
        {
            Boolean theEnabled;
            theEnabled = this.isEnabled();
            strategy.appendField(locator, this, "enabled", buffer, theEnabled);
        }
        {
            Boolean theNotCloneable;
            theNotCloneable = this.isNotCloneable();
            strategy.appendField(locator, this, "notCloneable", buffer, theNotCloneable);
        }
        {
            Boolean theInsertable;
            theInsertable = this.isInsertable();
            strategy.appendField(locator, this, "insertable", buffer, theInsertable);
        }
        {
            String theRows;
            theRows = this.getRows();
            strategy.appendField(locator, this, "rows", buffer, theRows);
        }
        {
            String theColumns;
            theColumns = this.getColumns();
            strategy.appendField(locator, this, "columns", buffer, theColumns);
        }
        {
            String theResourceId;
            theResourceId = this.getResourceId();
            strategy.appendField(locator, this, "resourceId", buffer, theResourceId);
        }
        {
            String theWidth;
            theWidth = this.getWidth();
            strategy.appendField(locator, this, "width", buffer, theWidth);
        }
        {
            String theNextFocusComponent;
            theNextFocusComponent = this.getNextFocusComponent();
            strategy.appendField(locator, this, "nextFocusComponent", buffer, theNextFocusComponent);
        }
        {
            String theNextFocusField;
            theNextFocusField = this.getNextFocusField();
            strategy.appendField(locator, this, "nextFocusField", buffer, theNextFocusField);
        }
        {
            String theCustomUsageSearch;
            theCustomUsageSearch = this.getCustomUsageSearch();
            strategy.appendField(locator, this, "customUsageSearch", buffer, theCustomUsageSearch);
        }
        {
            Boolean theVisible;
            theVisible = this.isVisible();
            strategy.appendField(locator, this, "visible", buffer, theVisible);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WebSubformColumn)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final WebSubformColumn that = ((WebSubformColumn) object);
        {
            WebValuelistProvider lhsValuelistProvider;
            lhsValuelistProvider = this.getValuelistProvider();
            WebValuelistProvider rhsValuelistProvider;
            rhsValuelistProvider = that.getValuelistProvider();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "valuelistProvider", lhsValuelistProvider), LocatorUtils.property(thatLocator, "valuelistProvider", rhsValuelistProvider), lhsValuelistProvider, rhsValuelistProvider)) {
                return false;
            }
        }
        {
            List<WebAdvancedProperty> lhsAdvancedProperties;
            lhsAdvancedProperties = (((this.advancedProperties!= null)&&(!this.advancedProperties.isEmpty()))?this.getAdvancedProperties():null);
            List<WebAdvancedProperty> rhsAdvancedProperties;
            rhsAdvancedProperties = (((that.advancedProperties!= null)&&(!that.advancedProperties.isEmpty()))?that.getAdvancedProperties():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "advancedProperties", lhsAdvancedProperties), LocatorUtils.property(thatLocator, "advancedProperties", rhsAdvancedProperties), lhsAdvancedProperties, rhsAdvancedProperties)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsLabel;
            lhsLabel = this.getLabel();
            String rhsLabel;
            rhsLabel = that.getLabel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "label", lhsLabel), LocatorUtils.property(thatLocator, "label", rhsLabel), lhsLabel, rhsLabel)) {
                return false;
            }
        }
        {
            SubformControlType lhsControlType;
            lhsControlType = this.getControlType();
            SubformControlType rhsControlType;
            rhsControlType = that.getControlType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "controlType", lhsControlType), LocatorUtils.property(thatLocator, "controlType", rhsControlType), lhsControlType, rhsControlType)) {
                return false;
            }
        }
        {
            String lhsControlTypeClass;
            lhsControlTypeClass = this.getControlTypeClass();
            String rhsControlTypeClass;
            rhsControlTypeClass = that.getControlTypeClass();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "controlTypeClass", lhsControlTypeClass), LocatorUtils.property(thatLocator, "controlTypeClass", rhsControlTypeClass), lhsControlTypeClass, rhsControlTypeClass)) {
                return false;
            }
        }
        {
            Boolean lhsEnabled;
            lhsEnabled = this.isEnabled();
            Boolean rhsEnabled;
            rhsEnabled = that.isEnabled();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "enabled", lhsEnabled), LocatorUtils.property(thatLocator, "enabled", rhsEnabled), lhsEnabled, rhsEnabled)) {
                return false;
            }
        }
        {
            Boolean lhsNotCloneable;
            lhsNotCloneable = this.isNotCloneable();
            Boolean rhsNotCloneable;
            rhsNotCloneable = that.isNotCloneable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "notCloneable", lhsNotCloneable), LocatorUtils.property(thatLocator, "notCloneable", rhsNotCloneable), lhsNotCloneable, rhsNotCloneable)) {
                return false;
            }
        }
        {
            Boolean lhsInsertable;
            lhsInsertable = this.isInsertable();
            Boolean rhsInsertable;
            rhsInsertable = that.isInsertable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "insertable", lhsInsertable), LocatorUtils.property(thatLocator, "insertable", rhsInsertable), lhsInsertable, rhsInsertable)) {
                return false;
            }
        }
        {
            String lhsRows;
            lhsRows = this.getRows();
            String rhsRows;
            rhsRows = that.getRows();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "rows", lhsRows), LocatorUtils.property(thatLocator, "rows", rhsRows), lhsRows, rhsRows)) {
                return false;
            }
        }
        {
            String lhsColumns;
            lhsColumns = this.getColumns();
            String rhsColumns;
            rhsColumns = that.getColumns();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "columns", lhsColumns), LocatorUtils.property(thatLocator, "columns", rhsColumns), lhsColumns, rhsColumns)) {
                return false;
            }
        }
        {
            String lhsResourceId;
            lhsResourceId = this.getResourceId();
            String rhsResourceId;
            rhsResourceId = that.getResourceId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "resourceId", lhsResourceId), LocatorUtils.property(thatLocator, "resourceId", rhsResourceId), lhsResourceId, rhsResourceId)) {
                return false;
            }
        }
        {
            String lhsWidth;
            lhsWidth = this.getWidth();
            String rhsWidth;
            rhsWidth = that.getWidth();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "width", lhsWidth), LocatorUtils.property(thatLocator, "width", rhsWidth), lhsWidth, rhsWidth)) {
                return false;
            }
        }
        {
            String lhsNextFocusComponent;
            lhsNextFocusComponent = this.getNextFocusComponent();
            String rhsNextFocusComponent;
            rhsNextFocusComponent = that.getNextFocusComponent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "nextFocusComponent", lhsNextFocusComponent), LocatorUtils.property(thatLocator, "nextFocusComponent", rhsNextFocusComponent), lhsNextFocusComponent, rhsNextFocusComponent)) {
                return false;
            }
        }
        {
            String lhsNextFocusField;
            lhsNextFocusField = this.getNextFocusField();
            String rhsNextFocusField;
            rhsNextFocusField = that.getNextFocusField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "nextFocusField", lhsNextFocusField), LocatorUtils.property(thatLocator, "nextFocusField", rhsNextFocusField), lhsNextFocusField, rhsNextFocusField)) {
                return false;
            }
        }
        {
            String lhsCustomUsageSearch;
            lhsCustomUsageSearch = this.getCustomUsageSearch();
            String rhsCustomUsageSearch;
            rhsCustomUsageSearch = that.getCustomUsageSearch();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "customUsageSearch", lhsCustomUsageSearch), LocatorUtils.property(thatLocator, "customUsageSearch", rhsCustomUsageSearch), lhsCustomUsageSearch, rhsCustomUsageSearch)) {
                return false;
            }
        }
        {
            Boolean lhsVisible;
            lhsVisible = this.isVisible();
            Boolean rhsVisible;
            rhsVisible = that.isVisible();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "visible", lhsVisible), LocatorUtils.property(thatLocator, "visible", rhsVisible), lhsVisible, rhsVisible)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            WebValuelistProvider theValuelistProvider;
            theValuelistProvider = this.getValuelistProvider();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "valuelistProvider", theValuelistProvider), currentHashCode, theValuelistProvider);
        }
        {
            List<WebAdvancedProperty> theAdvancedProperties;
            theAdvancedProperties = (((this.advancedProperties!= null)&&(!this.advancedProperties.isEmpty()))?this.getAdvancedProperties():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "advancedProperties", theAdvancedProperties), currentHashCode, theAdvancedProperties);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String theLabel;
            theLabel = this.getLabel();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "label", theLabel), currentHashCode, theLabel);
        }
        {
            SubformControlType theControlType;
            theControlType = this.getControlType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "controlType", theControlType), currentHashCode, theControlType);
        }
        {
            String theControlTypeClass;
            theControlTypeClass = this.getControlTypeClass();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "controlTypeClass", theControlTypeClass), currentHashCode, theControlTypeClass);
        }
        {
            Boolean theEnabled;
            theEnabled = this.isEnabled();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "enabled", theEnabled), currentHashCode, theEnabled);
        }
        {
            Boolean theNotCloneable;
            theNotCloneable = this.isNotCloneable();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "notCloneable", theNotCloneable), currentHashCode, theNotCloneable);
        }
        {
            Boolean theInsertable;
            theInsertable = this.isInsertable();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "insertable", theInsertable), currentHashCode, theInsertable);
        }
        {
            String theRows;
            theRows = this.getRows();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "rows", theRows), currentHashCode, theRows);
        }
        {
            String theColumns;
            theColumns = this.getColumns();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "columns", theColumns), currentHashCode, theColumns);
        }
        {
            String theResourceId;
            theResourceId = this.getResourceId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "resourceId", theResourceId), currentHashCode, theResourceId);
        }
        {
            String theWidth;
            theWidth = this.getWidth();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "width", theWidth), currentHashCode, theWidth);
        }
        {
            String theNextFocusComponent;
            theNextFocusComponent = this.getNextFocusComponent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nextFocusComponent", theNextFocusComponent), currentHashCode, theNextFocusComponent);
        }
        {
            String theNextFocusField;
            theNextFocusField = this.getNextFocusField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nextFocusField", theNextFocusField), currentHashCode, theNextFocusField);
        }
        {
            String theCustomUsageSearch;
            theCustomUsageSearch = this.getCustomUsageSearch();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "customUsageSearch", theCustomUsageSearch), currentHashCode, theCustomUsageSearch);
        }
        {
            Boolean theVisible;
            theVisible = this.isVisible();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "visible", theVisible), currentHashCode, theVisible);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof WebSubformColumn) {
            final WebSubformColumn copy = ((WebSubformColumn) draftCopy);
            if (this.valuelistProvider!= null) {
                WebValuelistProvider sourceValuelistProvider;
                sourceValuelistProvider = this.getValuelistProvider();
                WebValuelistProvider copyValuelistProvider = ((WebValuelistProvider) strategy.copy(LocatorUtils.property(locator, "valuelistProvider", sourceValuelistProvider), sourceValuelistProvider));
                copy.setValuelistProvider(copyValuelistProvider);
            } else {
                copy.valuelistProvider = null;
            }
            if ((this.advancedProperties!= null)&&(!this.advancedProperties.isEmpty())) {
                List<WebAdvancedProperty> sourceAdvancedProperties;
                sourceAdvancedProperties = (((this.advancedProperties!= null)&&(!this.advancedProperties.isEmpty()))?this.getAdvancedProperties():null);
                @SuppressWarnings("unchecked")
                List<WebAdvancedProperty> copyAdvancedProperties = ((List<WebAdvancedProperty> ) strategy.copy(LocatorUtils.property(locator, "advancedProperties", sourceAdvancedProperties), sourceAdvancedProperties));
                copy.advancedProperties = null;
                if (copyAdvancedProperties!= null) {
                    List<WebAdvancedProperty> uniqueAdvancedPropertiesl = copy.getAdvancedProperties();
                    uniqueAdvancedPropertiesl.addAll(copyAdvancedProperties);
                }
            } else {
                copy.advancedProperties = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.label!= null) {
                String sourceLabel;
                sourceLabel = this.getLabel();
                String copyLabel = ((String) strategy.copy(LocatorUtils.property(locator, "label", sourceLabel), sourceLabel));
                copy.setLabel(copyLabel);
            } else {
                copy.label = null;
            }
            if (this.controlType!= null) {
                SubformControlType sourceControlType;
                sourceControlType = this.getControlType();
                SubformControlType copyControlType = ((SubformControlType) strategy.copy(LocatorUtils.property(locator, "controlType", sourceControlType), sourceControlType));
                copy.setControlType(copyControlType);
            } else {
                copy.controlType = null;
            }
            if (this.controlTypeClass!= null) {
                String sourceControlTypeClass;
                sourceControlTypeClass = this.getControlTypeClass();
                String copyControlTypeClass = ((String) strategy.copy(LocatorUtils.property(locator, "controlTypeClass", sourceControlTypeClass), sourceControlTypeClass));
                copy.setControlTypeClass(copyControlTypeClass);
            } else {
                copy.controlTypeClass = null;
            }
            if (this.enabled!= null) {
                Boolean sourceEnabled;
                sourceEnabled = this.isEnabled();
                Boolean copyEnabled = ((Boolean) strategy.copy(LocatorUtils.property(locator, "enabled", sourceEnabled), sourceEnabled));
                copy.setEnabled(copyEnabled);
            } else {
                copy.enabled = null;
            }
            if (this.notCloneable!= null) {
                Boolean sourceNotCloneable;
                sourceNotCloneable = this.isNotCloneable();
                Boolean copyNotCloneable = ((Boolean) strategy.copy(LocatorUtils.property(locator, "notCloneable", sourceNotCloneable), sourceNotCloneable));
                copy.setNotCloneable(copyNotCloneable);
            } else {
                copy.notCloneable = null;
            }
            if (this.insertable!= null) {
                Boolean sourceInsertable;
                sourceInsertable = this.isInsertable();
                Boolean copyInsertable = ((Boolean) strategy.copy(LocatorUtils.property(locator, "insertable", sourceInsertable), sourceInsertable));
                copy.setInsertable(copyInsertable);
            } else {
                copy.insertable = null;
            }
            if (this.rows!= null) {
                String sourceRows;
                sourceRows = this.getRows();
                String copyRows = ((String) strategy.copy(LocatorUtils.property(locator, "rows", sourceRows), sourceRows));
                copy.setRows(copyRows);
            } else {
                copy.rows = null;
            }
            if (this.columns!= null) {
                String sourceColumns;
                sourceColumns = this.getColumns();
                String copyColumns = ((String) strategy.copy(LocatorUtils.property(locator, "columns", sourceColumns), sourceColumns));
                copy.setColumns(copyColumns);
            } else {
                copy.columns = null;
            }
            if (this.resourceId!= null) {
                String sourceResourceId;
                sourceResourceId = this.getResourceId();
                String copyResourceId = ((String) strategy.copy(LocatorUtils.property(locator, "resourceId", sourceResourceId), sourceResourceId));
                copy.setResourceId(copyResourceId);
            } else {
                copy.resourceId = null;
            }
            if (this.width!= null) {
                String sourceWidth;
                sourceWidth = this.getWidth();
                String copyWidth = ((String) strategy.copy(LocatorUtils.property(locator, "width", sourceWidth), sourceWidth));
                copy.setWidth(copyWidth);
            } else {
                copy.width = null;
            }
            if (this.nextFocusComponent!= null) {
                String sourceNextFocusComponent;
                sourceNextFocusComponent = this.getNextFocusComponent();
                String copyNextFocusComponent = ((String) strategy.copy(LocatorUtils.property(locator, "nextFocusComponent", sourceNextFocusComponent), sourceNextFocusComponent));
                copy.setNextFocusComponent(copyNextFocusComponent);
            } else {
                copy.nextFocusComponent = null;
            }
            if (this.nextFocusField!= null) {
                String sourceNextFocusField;
                sourceNextFocusField = this.getNextFocusField();
                String copyNextFocusField = ((String) strategy.copy(LocatorUtils.property(locator, "nextFocusField", sourceNextFocusField), sourceNextFocusField));
                copy.setNextFocusField(copyNextFocusField);
            } else {
                copy.nextFocusField = null;
            }
            if (this.customUsageSearch!= null) {
                String sourceCustomUsageSearch;
                sourceCustomUsageSearch = this.getCustomUsageSearch();
                String copyCustomUsageSearch = ((String) strategy.copy(LocatorUtils.property(locator, "customUsageSearch", sourceCustomUsageSearch), sourceCustomUsageSearch));
                copy.setCustomUsageSearch(copyCustomUsageSearch);
            } else {
                copy.customUsageSearch = null;
            }
            if (this.visible!= null) {
                Boolean sourceVisible;
                sourceVisible = this.isVisible();
                Boolean copyVisible = ((Boolean) strategy.copy(LocatorUtils.property(locator, "visible", sourceVisible), sourceVisible));
                copy.setVisible(copyVisible);
            } else {
                copy.visible = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new WebSubformColumn();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebSubformColumn.Builder<_B> _other) {
        _other.valuelistProvider = ((this.valuelistProvider == null)?null:this.valuelistProvider.newCopyBuilder(_other));
        if (this.advancedProperties == null) {
            _other.advancedProperties = null;
        } else {
            _other.advancedProperties = new ArrayList<WebAdvancedProperty.Builder<WebSubformColumn.Builder<_B>>>();
            for (WebAdvancedProperty _item: this.advancedProperties) {
                _other.advancedProperties.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.name = this.name;
        _other.label = this.label;
        _other.controlType = this.controlType;
        _other.controlTypeClass = this.controlTypeClass;
        _other.enabled = this.enabled;
        _other.notCloneable = this.notCloneable;
        _other.insertable = this.insertable;
        _other.rows = this.rows;
        _other.columns = this.columns;
        _other.resourceId = this.resourceId;
        _other.width = this.width;
        _other.nextFocusComponent = this.nextFocusComponent;
        _other.nextFocusField = this.nextFocusField;
        _other.customUsageSearch = this.customUsageSearch;
        _other.visible = this.visible;
    }

    public<_B >WebSubformColumn.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new WebSubformColumn.Builder<_B>(_parentBuilder, this, true);
    }

    public WebSubformColumn.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static WebSubformColumn.Builder<Void> builder() {
        return new WebSubformColumn.Builder<Void>(null, null, false);
    }

    public static<_B >WebSubformColumn.Builder<_B> copyOf(final WebSubformColumn _other) {
        final WebSubformColumn.Builder<_B> _newBuilder = new WebSubformColumn.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebSubformColumn.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree valuelistProviderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("valuelistProvider"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuelistProviderPropertyTree!= null):((valuelistProviderPropertyTree == null)||(!valuelistProviderPropertyTree.isLeaf())))) {
            _other.valuelistProvider = ((this.valuelistProvider == null)?null:this.valuelistProvider.newCopyBuilder(_other, valuelistProviderPropertyTree, _propertyTreeUse));
        }
        final PropertyTree advancedPropertiesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("advancedProperties"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(advancedPropertiesPropertyTree!= null):((advancedPropertiesPropertyTree == null)||(!advancedPropertiesPropertyTree.isLeaf())))) {
            if (this.advancedProperties == null) {
                _other.advancedProperties = null;
            } else {
                _other.advancedProperties = new ArrayList<WebAdvancedProperty.Builder<WebSubformColumn.Builder<_B>>>();
                for (WebAdvancedProperty _item: this.advancedProperties) {
                    _other.advancedProperties.add(((_item == null)?null:_item.newCopyBuilder(_other, advancedPropertiesPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree labelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("label"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(labelPropertyTree!= null):((labelPropertyTree == null)||(!labelPropertyTree.isLeaf())))) {
            _other.label = this.label;
        }
        final PropertyTree controlTypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("controlType"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(controlTypePropertyTree!= null):((controlTypePropertyTree == null)||(!controlTypePropertyTree.isLeaf())))) {
            _other.controlType = this.controlType;
        }
        final PropertyTree controlTypeClassPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("controlTypeClass"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(controlTypeClassPropertyTree!= null):((controlTypeClassPropertyTree == null)||(!controlTypeClassPropertyTree.isLeaf())))) {
            _other.controlTypeClass = this.controlTypeClass;
        }
        final PropertyTree enabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("enabled"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(enabledPropertyTree!= null):((enabledPropertyTree == null)||(!enabledPropertyTree.isLeaf())))) {
            _other.enabled = this.enabled;
        }
        final PropertyTree notCloneablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("notCloneable"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(notCloneablePropertyTree!= null):((notCloneablePropertyTree == null)||(!notCloneablePropertyTree.isLeaf())))) {
            _other.notCloneable = this.notCloneable;
        }
        final PropertyTree insertablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("insertable"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(insertablePropertyTree!= null):((insertablePropertyTree == null)||(!insertablePropertyTree.isLeaf())))) {
            _other.insertable = this.insertable;
        }
        final PropertyTree rowsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rows"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowsPropertyTree!= null):((rowsPropertyTree == null)||(!rowsPropertyTree.isLeaf())))) {
            _other.rows = this.rows;
        }
        final PropertyTree columnsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("columns"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(columnsPropertyTree!= null):((columnsPropertyTree == null)||(!columnsPropertyTree.isLeaf())))) {
            _other.columns = this.columns;
        }
        final PropertyTree resourceIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resourceId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resourceIdPropertyTree!= null):((resourceIdPropertyTree == null)||(!resourceIdPropertyTree.isLeaf())))) {
            _other.resourceId = this.resourceId;
        }
        final PropertyTree widthPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("width"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(widthPropertyTree!= null):((widthPropertyTree == null)||(!widthPropertyTree.isLeaf())))) {
            _other.width = this.width;
        }
        final PropertyTree nextFocusComponentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextFocusComponent"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextFocusComponentPropertyTree!= null):((nextFocusComponentPropertyTree == null)||(!nextFocusComponentPropertyTree.isLeaf())))) {
            _other.nextFocusComponent = this.nextFocusComponent;
        }
        final PropertyTree nextFocusFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextFocusField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextFocusFieldPropertyTree!= null):((nextFocusFieldPropertyTree == null)||(!nextFocusFieldPropertyTree.isLeaf())))) {
            _other.nextFocusField = this.nextFocusField;
        }
        final PropertyTree customUsageSearchPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("customUsageSearch"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(customUsageSearchPropertyTree!= null):((customUsageSearchPropertyTree == null)||(!customUsageSearchPropertyTree.isLeaf())))) {
            _other.customUsageSearch = this.customUsageSearch;
        }
        final PropertyTree visiblePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("visible"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(visiblePropertyTree!= null):((visiblePropertyTree == null)||(!visiblePropertyTree.isLeaf())))) {
            _other.visible = this.visible;
        }
    }

    public<_B >WebSubformColumn.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new WebSubformColumn.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public WebSubformColumn.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >WebSubformColumn.Builder<_B> copyOf(final WebSubformColumn _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebSubformColumn.Builder<_B> _newBuilder = new WebSubformColumn.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static WebSubformColumn.Builder<Void> copyExcept(final WebSubformColumn _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebSubformColumn.Builder<Void> copyOnly(final WebSubformColumn _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final WebSubformColumn _storedValue;
        private WebValuelistProvider.Builder<WebSubformColumn.Builder<_B>> valuelistProvider;
        private List<WebAdvancedProperty.Builder<WebSubformColumn.Builder<_B>>> advancedProperties;
        private String name;
        private String label;
        private SubformControlType controlType;
        private String controlTypeClass;
        private Boolean enabled;
        private Boolean notCloneable;
        private Boolean insertable;
        private String rows;
        private String columns;
        private String resourceId;
        private String width;
        private String nextFocusComponent;
        private String nextFocusField;
        private String customUsageSearch;
        private Boolean visible;

        public Builder(final _B _parentBuilder, final WebSubformColumn _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.valuelistProvider = ((_other.valuelistProvider == null)?null:_other.valuelistProvider.newCopyBuilder(this));
                    if (_other.advancedProperties == null) {
                        this.advancedProperties = null;
                    } else {
                        this.advancedProperties = new ArrayList<WebAdvancedProperty.Builder<WebSubformColumn.Builder<_B>>>();
                        for (WebAdvancedProperty _item: _other.advancedProperties) {
                            this.advancedProperties.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.name = _other.name;
                    this.label = _other.label;
                    this.controlType = _other.controlType;
                    this.controlTypeClass = _other.controlTypeClass;
                    this.enabled = _other.enabled;
                    this.notCloneable = _other.notCloneable;
                    this.insertable = _other.insertable;
                    this.rows = _other.rows;
                    this.columns = _other.columns;
                    this.resourceId = _other.resourceId;
                    this.width = _other.width;
                    this.nextFocusComponent = _other.nextFocusComponent;
                    this.nextFocusField = _other.nextFocusField;
                    this.customUsageSearch = _other.customUsageSearch;
                    this.visible = _other.visible;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final WebSubformColumn _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree valuelistProviderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("valuelistProvider"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuelistProviderPropertyTree!= null):((valuelistProviderPropertyTree == null)||(!valuelistProviderPropertyTree.isLeaf())))) {
                        this.valuelistProvider = ((_other.valuelistProvider == null)?null:_other.valuelistProvider.newCopyBuilder(this, valuelistProviderPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree advancedPropertiesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("advancedProperties"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(advancedPropertiesPropertyTree!= null):((advancedPropertiesPropertyTree == null)||(!advancedPropertiesPropertyTree.isLeaf())))) {
                        if (_other.advancedProperties == null) {
                            this.advancedProperties = null;
                        } else {
                            this.advancedProperties = new ArrayList<WebAdvancedProperty.Builder<WebSubformColumn.Builder<_B>>>();
                            for (WebAdvancedProperty _item: _other.advancedProperties) {
                                this.advancedProperties.add(((_item == null)?null:_item.newCopyBuilder(this, advancedPropertiesPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree labelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("label"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(labelPropertyTree!= null):((labelPropertyTree == null)||(!labelPropertyTree.isLeaf())))) {
                        this.label = _other.label;
                    }
                    final PropertyTree controlTypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("controlType"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(controlTypePropertyTree!= null):((controlTypePropertyTree == null)||(!controlTypePropertyTree.isLeaf())))) {
                        this.controlType = _other.controlType;
                    }
                    final PropertyTree controlTypeClassPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("controlTypeClass"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(controlTypeClassPropertyTree!= null):((controlTypeClassPropertyTree == null)||(!controlTypeClassPropertyTree.isLeaf())))) {
                        this.controlTypeClass = _other.controlTypeClass;
                    }
                    final PropertyTree enabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("enabled"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(enabledPropertyTree!= null):((enabledPropertyTree == null)||(!enabledPropertyTree.isLeaf())))) {
                        this.enabled = _other.enabled;
                    }
                    final PropertyTree notCloneablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("notCloneable"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(notCloneablePropertyTree!= null):((notCloneablePropertyTree == null)||(!notCloneablePropertyTree.isLeaf())))) {
                        this.notCloneable = _other.notCloneable;
                    }
                    final PropertyTree insertablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("insertable"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(insertablePropertyTree!= null):((insertablePropertyTree == null)||(!insertablePropertyTree.isLeaf())))) {
                        this.insertable = _other.insertable;
                    }
                    final PropertyTree rowsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rows"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowsPropertyTree!= null):((rowsPropertyTree == null)||(!rowsPropertyTree.isLeaf())))) {
                        this.rows = _other.rows;
                    }
                    final PropertyTree columnsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("columns"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(columnsPropertyTree!= null):((columnsPropertyTree == null)||(!columnsPropertyTree.isLeaf())))) {
                        this.columns = _other.columns;
                    }
                    final PropertyTree resourceIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resourceId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resourceIdPropertyTree!= null):((resourceIdPropertyTree == null)||(!resourceIdPropertyTree.isLeaf())))) {
                        this.resourceId = _other.resourceId;
                    }
                    final PropertyTree widthPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("width"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(widthPropertyTree!= null):((widthPropertyTree == null)||(!widthPropertyTree.isLeaf())))) {
                        this.width = _other.width;
                    }
                    final PropertyTree nextFocusComponentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextFocusComponent"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextFocusComponentPropertyTree!= null):((nextFocusComponentPropertyTree == null)||(!nextFocusComponentPropertyTree.isLeaf())))) {
                        this.nextFocusComponent = _other.nextFocusComponent;
                    }
                    final PropertyTree nextFocusFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextFocusField"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextFocusFieldPropertyTree!= null):((nextFocusFieldPropertyTree == null)||(!nextFocusFieldPropertyTree.isLeaf())))) {
                        this.nextFocusField = _other.nextFocusField;
                    }
                    final PropertyTree customUsageSearchPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("customUsageSearch"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(customUsageSearchPropertyTree!= null):((customUsageSearchPropertyTree == null)||(!customUsageSearchPropertyTree.isLeaf())))) {
                        this.customUsageSearch = _other.customUsageSearch;
                    }
                    final PropertyTree visiblePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("visible"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(visiblePropertyTree!= null):((visiblePropertyTree == null)||(!visiblePropertyTree.isLeaf())))) {
                        this.visible = _other.visible;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends WebSubformColumn >_P init(final _P _product) {
            _product.valuelistProvider = ((this.valuelistProvider == null)?null:this.valuelistProvider.build());
            if (this.advancedProperties!= null) {
                final List<WebAdvancedProperty> advancedProperties = new ArrayList<WebAdvancedProperty>(this.advancedProperties.size());
                for (WebAdvancedProperty.Builder<WebSubformColumn.Builder<_B>> _item: this.advancedProperties) {
                    advancedProperties.add(_item.build());
                }
                _product.advancedProperties = advancedProperties;
            }
            _product.name = this.name;
            _product.label = this.label;
            _product.controlType = this.controlType;
            _product.controlTypeClass = this.controlTypeClass;
            _product.enabled = this.enabled;
            _product.notCloneable = this.notCloneable;
            _product.insertable = this.insertable;
            _product.rows = this.rows;
            _product.columns = this.columns;
            _product.resourceId = this.resourceId;
            _product.width = this.width;
            _product.nextFocusComponent = this.nextFocusComponent;
            _product.nextFocusField = this.nextFocusField;
            _product.customUsageSearch = this.customUsageSearch;
            _product.visible = this.visible;
            return _product;
        }

        /**
         * Sets the new value of "valuelistProvider" (any previous value will be replaced)
         * 
         * @param valuelistProvider
         *     New value of the "valuelistProvider" property.
         */
        public WebSubformColumn.Builder<_B> withValuelistProvider(final WebValuelistProvider valuelistProvider) {
            this.valuelistProvider = ((valuelistProvider == null)?null:new WebValuelistProvider.Builder<WebSubformColumn.Builder<_B>>(this, valuelistProvider, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "valuelistProvider" property.
         * Use {@link org.nuclos.schema.layout.web.WebValuelistProvider.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "valuelistProvider" property.
         *     Use {@link org.nuclos.schema.layout.web.WebValuelistProvider.Builder#end()} to return to the current builder.
         */
        public WebValuelistProvider.Builder<? extends WebSubformColumn.Builder<_B>> withValuelistProvider() {
            if (this.valuelistProvider!= null) {
                return this.valuelistProvider;
            }
            return this.valuelistProvider = new WebValuelistProvider.Builder<WebSubformColumn.Builder<_B>>(this, null, false);
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        public WebSubformColumn.Builder<_B> addAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            if (advancedProperties!= null) {
                if (this.advancedProperties == null) {
                    this.advancedProperties = new ArrayList<WebAdvancedProperty.Builder<WebSubformColumn.Builder<_B>>>();
                }
                for (WebAdvancedProperty _item: advancedProperties) {
                    this.advancedProperties.add(new WebAdvancedProperty.Builder<WebSubformColumn.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        public WebSubformColumn.Builder<_B> withAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            if (this.advancedProperties!= null) {
                this.advancedProperties.clear();
            }
            return addAdvancedProperties(advancedProperties);
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        public WebSubformColumn.Builder<_B> addAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            addAdvancedProperties(Arrays.asList(advancedProperties));
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        public WebSubformColumn.Builder<_B> withAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            withAdvancedProperties(Arrays.asList(advancedProperties));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "AdvancedProperties" property.
         * Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "AdvancedProperties" property.
         *     Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         */
        public WebAdvancedProperty.Builder<? extends WebSubformColumn.Builder<_B>> addAdvancedProperties() {
            if (this.advancedProperties == null) {
                this.advancedProperties = new ArrayList<WebAdvancedProperty.Builder<WebSubformColumn.Builder<_B>>>();
            }
            final WebAdvancedProperty.Builder<WebSubformColumn.Builder<_B>> advancedProperties_Builder = new WebAdvancedProperty.Builder<WebSubformColumn.Builder<_B>>(this, null, false);
            this.advancedProperties.add(advancedProperties_Builder);
            return advancedProperties_Builder;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public WebSubformColumn.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "label" (any previous value will be replaced)
         * 
         * @param label
         *     New value of the "label" property.
         */
        public WebSubformColumn.Builder<_B> withLabel(final String label) {
            this.label = label;
            return this;
        }

        /**
         * Sets the new value of "controlType" (any previous value will be replaced)
         * 
         * @param controlType
         *     New value of the "controlType" property.
         */
        public WebSubformColumn.Builder<_B> withControlType(final SubformControlType controlType) {
            this.controlType = controlType;
            return this;
        }

        /**
         * Sets the new value of "controlTypeClass" (any previous value will be replaced)
         * 
         * @param controlTypeClass
         *     New value of the "controlTypeClass" property.
         */
        public WebSubformColumn.Builder<_B> withControlTypeClass(final String controlTypeClass) {
            this.controlTypeClass = controlTypeClass;
            return this;
        }

        /**
         * Sets the new value of "enabled" (any previous value will be replaced)
         * 
         * @param enabled
         *     New value of the "enabled" property.
         */
        public WebSubformColumn.Builder<_B> withEnabled(final Boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        /**
         * Sets the new value of "notCloneable" (any previous value will be replaced)
         * 
         * @param notCloneable
         *     New value of the "notCloneable" property.
         */
        public WebSubformColumn.Builder<_B> withNotCloneable(final Boolean notCloneable) {
            this.notCloneable = notCloneable;
            return this;
        }

        /**
         * Sets the new value of "insertable" (any previous value will be replaced)
         * 
         * @param insertable
         *     New value of the "insertable" property.
         */
        public WebSubformColumn.Builder<_B> withInsertable(final Boolean insertable) {
            this.insertable = insertable;
            return this;
        }

        /**
         * Sets the new value of "rows" (any previous value will be replaced)
         * 
         * @param rows
         *     New value of the "rows" property.
         */
        public WebSubformColumn.Builder<_B> withRows(final String rows) {
            this.rows = rows;
            return this;
        }

        /**
         * Sets the new value of "columns" (any previous value will be replaced)
         * 
         * @param columns
         *     New value of the "columns" property.
         */
        public WebSubformColumn.Builder<_B> withColumns(final String columns) {
            this.columns = columns;
            return this;
        }

        /**
         * Sets the new value of "resourceId" (any previous value will be replaced)
         * 
         * @param resourceId
         *     New value of the "resourceId" property.
         */
        public WebSubformColumn.Builder<_B> withResourceId(final String resourceId) {
            this.resourceId = resourceId;
            return this;
        }

        /**
         * Sets the new value of "width" (any previous value will be replaced)
         * 
         * @param width
         *     New value of the "width" property.
         */
        public WebSubformColumn.Builder<_B> withWidth(final String width) {
            this.width = width;
            return this;
        }

        /**
         * Sets the new value of "nextFocusComponent" (any previous value will be replaced)
         * 
         * @param nextFocusComponent
         *     New value of the "nextFocusComponent" property.
         */
        public WebSubformColumn.Builder<_B> withNextFocusComponent(final String nextFocusComponent) {
            this.nextFocusComponent = nextFocusComponent;
            return this;
        }

        /**
         * Sets the new value of "nextFocusField" (any previous value will be replaced)
         * 
         * @param nextFocusField
         *     New value of the "nextFocusField" property.
         */
        public WebSubformColumn.Builder<_B> withNextFocusField(final String nextFocusField) {
            this.nextFocusField = nextFocusField;
            return this;
        }

        /**
         * Sets the new value of "customUsageSearch" (any previous value will be replaced)
         * 
         * @param customUsageSearch
         *     New value of the "customUsageSearch" property.
         */
        public WebSubformColumn.Builder<_B> withCustomUsageSearch(final String customUsageSearch) {
            this.customUsageSearch = customUsageSearch;
            return this;
        }

        /**
         * Sets the new value of "visible" (any previous value will be replaced)
         * 
         * @param visible
         *     New value of the "visible" property.
         */
        public WebSubformColumn.Builder<_B> withVisible(final Boolean visible) {
            this.visible = visible;
            return this;
        }

        @Override
        public WebSubformColumn build() {
            if (_storedValue == null) {
                return this.init(new WebSubformColumn());
            } else {
                return ((WebSubformColumn) _storedValue);
            }
        }

        public WebSubformColumn.Builder<_B> copyOf(final WebSubformColumn _other) {
            _other.copyTo(this);
            return this;
        }

        public WebSubformColumn.Builder<_B> copyOf(final WebSubformColumn.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends WebSubformColumn.Selector<WebSubformColumn.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static WebSubformColumn.Select _root() {
            return new WebSubformColumn.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private WebValuelistProvider.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> valuelistProvider = null;
        private WebAdvancedProperty.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> advancedProperties = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> label = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> controlType = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> controlTypeClass = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> enabled = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> notCloneable = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> insertable = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> rows = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> columns = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> resourceId = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> width = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> nextFocusComponent = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> nextFocusField = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> customUsageSearch = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> visible = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.valuelistProvider!= null) {
                products.put("valuelistProvider", this.valuelistProvider.init());
            }
            if (this.advancedProperties!= null) {
                products.put("advancedProperties", this.advancedProperties.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.label!= null) {
                products.put("label", this.label.init());
            }
            if (this.controlType!= null) {
                products.put("controlType", this.controlType.init());
            }
            if (this.controlTypeClass!= null) {
                products.put("controlTypeClass", this.controlTypeClass.init());
            }
            if (this.enabled!= null) {
                products.put("enabled", this.enabled.init());
            }
            if (this.notCloneable!= null) {
                products.put("notCloneable", this.notCloneable.init());
            }
            if (this.insertable!= null) {
                products.put("insertable", this.insertable.init());
            }
            if (this.rows!= null) {
                products.put("rows", this.rows.init());
            }
            if (this.columns!= null) {
                products.put("columns", this.columns.init());
            }
            if (this.resourceId!= null) {
                products.put("resourceId", this.resourceId.init());
            }
            if (this.width!= null) {
                products.put("width", this.width.init());
            }
            if (this.nextFocusComponent!= null) {
                products.put("nextFocusComponent", this.nextFocusComponent.init());
            }
            if (this.nextFocusField!= null) {
                products.put("nextFocusField", this.nextFocusField.init());
            }
            if (this.customUsageSearch!= null) {
                products.put("customUsageSearch", this.customUsageSearch.init());
            }
            if (this.visible!= null) {
                products.put("visible", this.visible.init());
            }
            return products;
        }

        public WebValuelistProvider.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> valuelistProvider() {
            return ((this.valuelistProvider == null)?this.valuelistProvider = new WebValuelistProvider.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>>(this._root, this, "valuelistProvider"):this.valuelistProvider);
        }

        public WebAdvancedProperty.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> advancedProperties() {
            return ((this.advancedProperties == null)?this.advancedProperties = new WebAdvancedProperty.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>>(this._root, this, "advancedProperties"):this.advancedProperties);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> label() {
            return ((this.label == null)?this.label = new com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>>(this._root, this, "label"):this.label);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> controlType() {
            return ((this.controlType == null)?this.controlType = new com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>>(this._root, this, "controlType"):this.controlType);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> controlTypeClass() {
            return ((this.controlTypeClass == null)?this.controlTypeClass = new com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>>(this._root, this, "controlTypeClass"):this.controlTypeClass);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> enabled() {
            return ((this.enabled == null)?this.enabled = new com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>>(this._root, this, "enabled"):this.enabled);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> notCloneable() {
            return ((this.notCloneable == null)?this.notCloneable = new com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>>(this._root, this, "notCloneable"):this.notCloneable);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> insertable() {
            return ((this.insertable == null)?this.insertable = new com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>>(this._root, this, "insertable"):this.insertable);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> rows() {
            return ((this.rows == null)?this.rows = new com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>>(this._root, this, "rows"):this.rows);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> columns() {
            return ((this.columns == null)?this.columns = new com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>>(this._root, this, "columns"):this.columns);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> resourceId() {
            return ((this.resourceId == null)?this.resourceId = new com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>>(this._root, this, "resourceId"):this.resourceId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> width() {
            return ((this.width == null)?this.width = new com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>>(this._root, this, "width"):this.width);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> nextFocusComponent() {
            return ((this.nextFocusComponent == null)?this.nextFocusComponent = new com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>>(this._root, this, "nextFocusComponent"):this.nextFocusComponent);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> nextFocusField() {
            return ((this.nextFocusField == null)?this.nextFocusField = new com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>>(this._root, this, "nextFocusField"):this.nextFocusField);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> customUsageSearch() {
            return ((this.customUsageSearch == null)?this.customUsageSearch = new com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>>(this._root, this, "customUsageSearch"):this.customUsageSearch);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>> visible() {
            return ((this.visible == null)?this.visible = new com.kscs.util.jaxb.Selector<TRoot, WebSubformColumn.Selector<TRoot, TParent>>(this._root, this, "visible"):this.visible);
        }

    }

}
