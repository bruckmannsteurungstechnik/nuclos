
package org.nuclos.schema.layout.web;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * A tab with a title and content.
 * 
 * <p>Java class for web-tab complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="web-tab"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="content" type="{urn:org.nuclos.schema.layout.web}web-component"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "web-tab", propOrder = {
    "title",
    "content"
})
public class WebTab implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected String title;
    @XmlElement(required = true)
    protected WebComponent content;

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the content property.
     * 
     * @return
     *     possible object is
     *     {@link WebComponent }
     *     
     */
    public WebComponent getContent() {
        return content;
    }

    /**
     * Sets the value of the content property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebComponent }
     *     
     */
    public void setContent(WebComponent value) {
        this.content = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theTitle;
            theTitle = this.getTitle();
            strategy.appendField(locator, this, "title", buffer, theTitle);
        }
        {
            WebComponent theContent;
            theContent = this.getContent();
            strategy.appendField(locator, this, "content", buffer, theContent);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WebTab)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final WebTab that = ((WebTab) object);
        {
            String lhsTitle;
            lhsTitle = this.getTitle();
            String rhsTitle;
            rhsTitle = that.getTitle();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "title", lhsTitle), LocatorUtils.property(thatLocator, "title", rhsTitle), lhsTitle, rhsTitle)) {
                return false;
            }
        }
        {
            WebComponent lhsContent;
            lhsContent = this.getContent();
            WebComponent rhsContent;
            rhsContent = that.getContent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "content", lhsContent), LocatorUtils.property(thatLocator, "content", rhsContent), lhsContent, rhsContent)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theTitle;
            theTitle = this.getTitle();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "title", theTitle), currentHashCode, theTitle);
        }
        {
            WebComponent theContent;
            theContent = this.getContent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "content", theContent), currentHashCode, theContent);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof WebTab) {
            final WebTab copy = ((WebTab) draftCopy);
            if (this.title!= null) {
                String sourceTitle;
                sourceTitle = this.getTitle();
                String copyTitle = ((String) strategy.copy(LocatorUtils.property(locator, "title", sourceTitle), sourceTitle));
                copy.setTitle(copyTitle);
            } else {
                copy.title = null;
            }
            if (this.content!= null) {
                WebComponent sourceContent;
                sourceContent = this.getContent();
                WebComponent copyContent = ((WebComponent) strategy.copy(LocatorUtils.property(locator, "content", sourceContent), sourceContent));
                copy.setContent(copyContent);
            } else {
                copy.content = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new WebTab();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebTab.Builder<_B> _other) {
        _other.title = this.title;
        _other.content = ((this.content == null)?null:this.content.newCopyBuilder(_other));
    }

    public<_B >WebTab.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new WebTab.Builder<_B>(_parentBuilder, this, true);
    }

    public WebTab.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static WebTab.Builder<Void> builder() {
        return new WebTab.Builder<Void>(null, null, false);
    }

    public static<_B >WebTab.Builder<_B> copyOf(final WebTab _other) {
        final WebTab.Builder<_B> _newBuilder = new WebTab.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebTab.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree titlePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("title"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(titlePropertyTree!= null):((titlePropertyTree == null)||(!titlePropertyTree.isLeaf())))) {
            _other.title = this.title;
        }
        final PropertyTree contentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("content"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(contentPropertyTree!= null):((contentPropertyTree == null)||(!contentPropertyTree.isLeaf())))) {
            _other.content = ((this.content == null)?null:this.content.newCopyBuilder(_other, contentPropertyTree, _propertyTreeUse));
        }
    }

    public<_B >WebTab.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new WebTab.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public WebTab.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >WebTab.Builder<_B> copyOf(final WebTab _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebTab.Builder<_B> _newBuilder = new WebTab.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static WebTab.Builder<Void> copyExcept(final WebTab _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebTab.Builder<Void> copyOnly(final WebTab _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final WebTab _storedValue;
        private String title;
        private WebComponent.Builder<WebTab.Builder<_B>> content;

        public Builder(final _B _parentBuilder, final WebTab _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.title = _other.title;
                    this.content = ((_other.content == null)?null:_other.content.newCopyBuilder(this));
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final WebTab _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree titlePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("title"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(titlePropertyTree!= null):((titlePropertyTree == null)||(!titlePropertyTree.isLeaf())))) {
                        this.title = _other.title;
                    }
                    final PropertyTree contentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("content"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(contentPropertyTree!= null):((contentPropertyTree == null)||(!contentPropertyTree.isLeaf())))) {
                        this.content = ((_other.content == null)?null:_other.content.newCopyBuilder(this, contentPropertyTree, _propertyTreeUse));
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends WebTab >_P init(final _P _product) {
            _product.title = this.title;
            _product.content = ((this.content == null)?null:this.content.build());
            return _product;
        }

        /**
         * Sets the new value of "title" (any previous value will be replaced)
         * 
         * @param title
         *     New value of the "title" property.
         */
        public WebTab.Builder<_B> withTitle(final String title) {
            this.title = title;
            return this;
        }

        /**
         * Sets the new value of "content" (any previous value will be replaced)
         * 
         * @param content
         *     New value of the "content" property.
         */
        public WebTab.Builder<_B> withContent(final WebComponent content) {
            this.content = ((content == null)?null:new WebComponent.Builder<WebTab.Builder<_B>>(this, content, false));
            return this;
        }

        @Override
        public WebTab build() {
            if (_storedValue == null) {
                return this.init(new WebTab());
            } else {
                return ((WebTab) _storedValue);
            }
        }

        public WebTab.Builder<_B> copyOf(final WebTab _other) {
            _other.copyTo(this);
            return this;
        }

        public WebTab.Builder<_B> copyOf(final WebTab.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends WebTab.Selector<WebTab.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static WebTab.Select _root() {
            return new WebTab.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, WebTab.Selector<TRoot, TParent>> title = null;
        private WebComponent.Selector<TRoot, WebTab.Selector<TRoot, TParent>> content = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.title!= null) {
                products.put("title", this.title.init());
            }
            if (this.content!= null) {
                products.put("content", this.content.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebTab.Selector<TRoot, TParent>> title() {
            return ((this.title == null)?this.title = new com.kscs.util.jaxb.Selector<TRoot, WebTab.Selector<TRoot, TParent>>(this._root, this, "title"):this.title);
        }

        public WebComponent.Selector<TRoot, WebTab.Selector<TRoot, TParent>> content() {
            return ((this.content == null)?this.content = new WebComponent.Selector<TRoot, WebTab.Selector<TRoot, TParent>>(this._root, this, "content"):this.content);
        }

    }

}
