
package org.nuclos.schema.meta.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for entity-context complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="entity-context"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="dependentEntityClassId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="dependentEntityFieldId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "entity-context")
public class EntityContext implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "dependentEntityClassId", required = true)
    protected String dependentEntityClassId;
    @XmlAttribute(name = "dependentEntityFieldId", required = true)
    protected String dependentEntityFieldId;

    /**
     * Gets the value of the dependentEntityClassId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDependentEntityClassId() {
        return dependentEntityClassId;
    }

    /**
     * Sets the value of the dependentEntityClassId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDependentEntityClassId(String value) {
        this.dependentEntityClassId = value;
    }

    /**
     * Gets the value of the dependentEntityFieldId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDependentEntityFieldId() {
        return dependentEntityFieldId;
    }

    /**
     * Sets the value of the dependentEntityFieldId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDependentEntityFieldId(String value) {
        this.dependentEntityFieldId = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theDependentEntityClassId;
            theDependentEntityClassId = this.getDependentEntityClassId();
            strategy.appendField(locator, this, "dependentEntityClassId", buffer, theDependentEntityClassId);
        }
        {
            String theDependentEntityFieldId;
            theDependentEntityFieldId = this.getDependentEntityFieldId();
            strategy.appendField(locator, this, "dependentEntityFieldId", buffer, theDependentEntityFieldId);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof EntityContext)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final EntityContext that = ((EntityContext) object);
        {
            String lhsDependentEntityClassId;
            lhsDependentEntityClassId = this.getDependentEntityClassId();
            String rhsDependentEntityClassId;
            rhsDependentEntityClassId = that.getDependentEntityClassId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dependentEntityClassId", lhsDependentEntityClassId), LocatorUtils.property(thatLocator, "dependentEntityClassId", rhsDependentEntityClassId), lhsDependentEntityClassId, rhsDependentEntityClassId)) {
                return false;
            }
        }
        {
            String lhsDependentEntityFieldId;
            lhsDependentEntityFieldId = this.getDependentEntityFieldId();
            String rhsDependentEntityFieldId;
            rhsDependentEntityFieldId = that.getDependentEntityFieldId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dependentEntityFieldId", lhsDependentEntityFieldId), LocatorUtils.property(thatLocator, "dependentEntityFieldId", rhsDependentEntityFieldId), lhsDependentEntityFieldId, rhsDependentEntityFieldId)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theDependentEntityClassId;
            theDependentEntityClassId = this.getDependentEntityClassId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dependentEntityClassId", theDependentEntityClassId), currentHashCode, theDependentEntityClassId);
        }
        {
            String theDependentEntityFieldId;
            theDependentEntityFieldId = this.getDependentEntityFieldId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dependentEntityFieldId", theDependentEntityFieldId), currentHashCode, theDependentEntityFieldId);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof EntityContext) {
            final EntityContext copy = ((EntityContext) draftCopy);
            if (this.dependentEntityClassId!= null) {
                String sourceDependentEntityClassId;
                sourceDependentEntityClassId = this.getDependentEntityClassId();
                String copyDependentEntityClassId = ((String) strategy.copy(LocatorUtils.property(locator, "dependentEntityClassId", sourceDependentEntityClassId), sourceDependentEntityClassId));
                copy.setDependentEntityClassId(copyDependentEntityClassId);
            } else {
                copy.dependentEntityClassId = null;
            }
            if (this.dependentEntityFieldId!= null) {
                String sourceDependentEntityFieldId;
                sourceDependentEntityFieldId = this.getDependentEntityFieldId();
                String copyDependentEntityFieldId = ((String) strategy.copy(LocatorUtils.property(locator, "dependentEntityFieldId", sourceDependentEntityFieldId), sourceDependentEntityFieldId));
                copy.setDependentEntityFieldId(copyDependentEntityFieldId);
            } else {
                copy.dependentEntityFieldId = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new EntityContext();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityContext.Builder<_B> _other) {
        _other.dependentEntityClassId = this.dependentEntityClassId;
        _other.dependentEntityFieldId = this.dependentEntityFieldId;
    }

    public<_B >EntityContext.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new EntityContext.Builder<_B>(_parentBuilder, this, true);
    }

    public EntityContext.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static EntityContext.Builder<Void> builder() {
        return new EntityContext.Builder<Void>(null, null, false);
    }

    public static<_B >EntityContext.Builder<_B> copyOf(final EntityContext _other) {
        final EntityContext.Builder<_B> _newBuilder = new EntityContext.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityContext.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree dependentEntityClassIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dependentEntityClassId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dependentEntityClassIdPropertyTree!= null):((dependentEntityClassIdPropertyTree == null)||(!dependentEntityClassIdPropertyTree.isLeaf())))) {
            _other.dependentEntityClassId = this.dependentEntityClassId;
        }
        final PropertyTree dependentEntityFieldIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dependentEntityFieldId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dependentEntityFieldIdPropertyTree!= null):((dependentEntityFieldIdPropertyTree == null)||(!dependentEntityFieldIdPropertyTree.isLeaf())))) {
            _other.dependentEntityFieldId = this.dependentEntityFieldId;
        }
    }

    public<_B >EntityContext.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new EntityContext.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public EntityContext.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >EntityContext.Builder<_B> copyOf(final EntityContext _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final EntityContext.Builder<_B> _newBuilder = new EntityContext.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static EntityContext.Builder<Void> copyExcept(final EntityContext _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static EntityContext.Builder<Void> copyOnly(final EntityContext _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final EntityContext _storedValue;
        private String dependentEntityClassId;
        private String dependentEntityFieldId;

        public Builder(final _B _parentBuilder, final EntityContext _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.dependentEntityClassId = _other.dependentEntityClassId;
                    this.dependentEntityFieldId = _other.dependentEntityFieldId;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final EntityContext _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree dependentEntityClassIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dependentEntityClassId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dependentEntityClassIdPropertyTree!= null):((dependentEntityClassIdPropertyTree == null)||(!dependentEntityClassIdPropertyTree.isLeaf())))) {
                        this.dependentEntityClassId = _other.dependentEntityClassId;
                    }
                    final PropertyTree dependentEntityFieldIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dependentEntityFieldId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dependentEntityFieldIdPropertyTree!= null):((dependentEntityFieldIdPropertyTree == null)||(!dependentEntityFieldIdPropertyTree.isLeaf())))) {
                        this.dependentEntityFieldId = _other.dependentEntityFieldId;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends EntityContext >_P init(final _P _product) {
            _product.dependentEntityClassId = this.dependentEntityClassId;
            _product.dependentEntityFieldId = this.dependentEntityFieldId;
            return _product;
        }

        /**
         * Sets the new value of "dependentEntityClassId" (any previous value will be replaced)
         * 
         * @param dependentEntityClassId
         *     New value of the "dependentEntityClassId" property.
         */
        public EntityContext.Builder<_B> withDependentEntityClassId(final String dependentEntityClassId) {
            this.dependentEntityClassId = dependentEntityClassId;
            return this;
        }

        /**
         * Sets the new value of "dependentEntityFieldId" (any previous value will be replaced)
         * 
         * @param dependentEntityFieldId
         *     New value of the "dependentEntityFieldId" property.
         */
        public EntityContext.Builder<_B> withDependentEntityFieldId(final String dependentEntityFieldId) {
            this.dependentEntityFieldId = dependentEntityFieldId;
            return this;
        }

        @Override
        public EntityContext build() {
            if (_storedValue == null) {
                return this.init(new EntityContext());
            } else {
                return ((EntityContext) _storedValue);
            }
        }

        public EntityContext.Builder<_B> copyOf(final EntityContext _other) {
            _other.copyTo(this);
            return this;
        }

        public EntityContext.Builder<_B> copyOf(final EntityContext.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends EntityContext.Selector<EntityContext.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static EntityContext.Select _root() {
            return new EntityContext.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, EntityContext.Selector<TRoot, TParent>> dependentEntityClassId = null;
        private com.kscs.util.jaxb.Selector<TRoot, EntityContext.Selector<TRoot, TParent>> dependentEntityFieldId = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.dependentEntityClassId!= null) {
                products.put("dependentEntityClassId", this.dependentEntityClassId.init());
            }
            if (this.dependentEntityFieldId!= null) {
                products.put("dependentEntityFieldId", this.dependentEntityFieldId.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityContext.Selector<TRoot, TParent>> dependentEntityClassId() {
            return ((this.dependentEntityClassId == null)?this.dependentEntityClassId = new com.kscs.util.jaxb.Selector<TRoot, EntityContext.Selector<TRoot, TParent>>(this._root, this, "dependentEntityClassId"):this.dependentEntityClassId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityContext.Selector<TRoot, TParent>> dependentEntityFieldId() {
            return ((this.dependentEntityFieldId == null)?this.dependentEntityFieldId = new com.kscs.util.jaxb.Selector<TRoot, EntityContext.Selector<TRoot, TParent>>(this._root, this, "dependentEntityFieldId"):this.dependentEntityFieldId);
        }

    }

}
