
package org.nuclos.schema.meta.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for entity-field-vlp-config complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="entity-field-vlp-config"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="vlpContexts" type="{urn:org.nuclos.schema.meta.entity}entity-field-vlp-context" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="vlpId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="valueField" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="idField" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "entity-field-vlp-config", propOrder = {
    "vlpContexts"
})
public class EntityFieldVlpConfig implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected List<EntityFieldVlpContext> vlpContexts;
    @XmlAttribute(name = "vlpId", required = true)
    protected String vlpId;
    @XmlAttribute(name = "valueField", required = true)
    protected String valueField;
    @XmlAttribute(name = "idField")
    protected String idField;

    /**
     * Gets the value of the vlpContexts property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vlpContexts property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVlpContexts().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EntityFieldVlpContext }
     * 
     * 
     */
    public List<EntityFieldVlpContext> getVlpContexts() {
        if (vlpContexts == null) {
            vlpContexts = new ArrayList<EntityFieldVlpContext>();
        }
        return this.vlpContexts;
    }

    /**
     * Gets the value of the vlpId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVlpId() {
        return vlpId;
    }

    /**
     * Sets the value of the vlpId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVlpId(String value) {
        this.vlpId = value;
    }

    /**
     * Gets the value of the valueField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValueField() {
        return valueField;
    }

    /**
     * Sets the value of the valueField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValueField(String value) {
        this.valueField = value;
    }

    /**
     * Gets the value of the idField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdField() {
        return idField;
    }

    /**
     * Sets the value of the idField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdField(String value) {
        this.idField = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<EntityFieldVlpContext> theVlpContexts;
            theVlpContexts = (((this.vlpContexts!= null)&&(!this.vlpContexts.isEmpty()))?this.getVlpContexts():null);
            strategy.appendField(locator, this, "vlpContexts", buffer, theVlpContexts);
        }
        {
            String theVlpId;
            theVlpId = this.getVlpId();
            strategy.appendField(locator, this, "vlpId", buffer, theVlpId);
        }
        {
            String theValueField;
            theValueField = this.getValueField();
            strategy.appendField(locator, this, "valueField", buffer, theValueField);
        }
        {
            String theIdField;
            theIdField = this.getIdField();
            strategy.appendField(locator, this, "idField", buffer, theIdField);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof EntityFieldVlpConfig)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final EntityFieldVlpConfig that = ((EntityFieldVlpConfig) object);
        {
            List<EntityFieldVlpContext> lhsVlpContexts;
            lhsVlpContexts = (((this.vlpContexts!= null)&&(!this.vlpContexts.isEmpty()))?this.getVlpContexts():null);
            List<EntityFieldVlpContext> rhsVlpContexts;
            rhsVlpContexts = (((that.vlpContexts!= null)&&(!that.vlpContexts.isEmpty()))?that.getVlpContexts():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "vlpContexts", lhsVlpContexts), LocatorUtils.property(thatLocator, "vlpContexts", rhsVlpContexts), lhsVlpContexts, rhsVlpContexts)) {
                return false;
            }
        }
        {
            String lhsVlpId;
            lhsVlpId = this.getVlpId();
            String rhsVlpId;
            rhsVlpId = that.getVlpId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "vlpId", lhsVlpId), LocatorUtils.property(thatLocator, "vlpId", rhsVlpId), lhsVlpId, rhsVlpId)) {
                return false;
            }
        }
        {
            String lhsValueField;
            lhsValueField = this.getValueField();
            String rhsValueField;
            rhsValueField = that.getValueField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "valueField", lhsValueField), LocatorUtils.property(thatLocator, "valueField", rhsValueField), lhsValueField, rhsValueField)) {
                return false;
            }
        }
        {
            String lhsIdField;
            lhsIdField = this.getIdField();
            String rhsIdField;
            rhsIdField = that.getIdField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "idField", lhsIdField), LocatorUtils.property(thatLocator, "idField", rhsIdField), lhsIdField, rhsIdField)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<EntityFieldVlpContext> theVlpContexts;
            theVlpContexts = (((this.vlpContexts!= null)&&(!this.vlpContexts.isEmpty()))?this.getVlpContexts():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "vlpContexts", theVlpContexts), currentHashCode, theVlpContexts);
        }
        {
            String theVlpId;
            theVlpId = this.getVlpId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "vlpId", theVlpId), currentHashCode, theVlpId);
        }
        {
            String theValueField;
            theValueField = this.getValueField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "valueField", theValueField), currentHashCode, theValueField);
        }
        {
            String theIdField;
            theIdField = this.getIdField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "idField", theIdField), currentHashCode, theIdField);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof EntityFieldVlpConfig) {
            final EntityFieldVlpConfig copy = ((EntityFieldVlpConfig) draftCopy);
            if ((this.vlpContexts!= null)&&(!this.vlpContexts.isEmpty())) {
                List<EntityFieldVlpContext> sourceVlpContexts;
                sourceVlpContexts = (((this.vlpContexts!= null)&&(!this.vlpContexts.isEmpty()))?this.getVlpContexts():null);
                @SuppressWarnings("unchecked")
                List<EntityFieldVlpContext> copyVlpContexts = ((List<EntityFieldVlpContext> ) strategy.copy(LocatorUtils.property(locator, "vlpContexts", sourceVlpContexts), sourceVlpContexts));
                copy.vlpContexts = null;
                if (copyVlpContexts!= null) {
                    List<EntityFieldVlpContext> uniqueVlpContextsl = copy.getVlpContexts();
                    uniqueVlpContextsl.addAll(copyVlpContexts);
                }
            } else {
                copy.vlpContexts = null;
            }
            if (this.vlpId!= null) {
                String sourceVlpId;
                sourceVlpId = this.getVlpId();
                String copyVlpId = ((String) strategy.copy(LocatorUtils.property(locator, "vlpId", sourceVlpId), sourceVlpId));
                copy.setVlpId(copyVlpId);
            } else {
                copy.vlpId = null;
            }
            if (this.valueField!= null) {
                String sourceValueField;
                sourceValueField = this.getValueField();
                String copyValueField = ((String) strategy.copy(LocatorUtils.property(locator, "valueField", sourceValueField), sourceValueField));
                copy.setValueField(copyValueField);
            } else {
                copy.valueField = null;
            }
            if (this.idField!= null) {
                String sourceIdField;
                sourceIdField = this.getIdField();
                String copyIdField = ((String) strategy.copy(LocatorUtils.property(locator, "idField", sourceIdField), sourceIdField));
                copy.setIdField(copyIdField);
            } else {
                copy.idField = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new EntityFieldVlpConfig();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityFieldVlpConfig.Builder<_B> _other) {
        if (this.vlpContexts == null) {
            _other.vlpContexts = null;
        } else {
            _other.vlpContexts = new ArrayList<EntityFieldVlpContext.Builder<EntityFieldVlpConfig.Builder<_B>>>();
            for (EntityFieldVlpContext _item: this.vlpContexts) {
                _other.vlpContexts.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.vlpId = this.vlpId;
        _other.valueField = this.valueField;
        _other.idField = this.idField;
    }

    public<_B >EntityFieldVlpConfig.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new EntityFieldVlpConfig.Builder<_B>(_parentBuilder, this, true);
    }

    public EntityFieldVlpConfig.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static EntityFieldVlpConfig.Builder<Void> builder() {
        return new EntityFieldVlpConfig.Builder<Void>(null, null, false);
    }

    public static<_B >EntityFieldVlpConfig.Builder<_B> copyOf(final EntityFieldVlpConfig _other) {
        final EntityFieldVlpConfig.Builder<_B> _newBuilder = new EntityFieldVlpConfig.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityFieldVlpConfig.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree vlpContextsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("vlpContexts"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(vlpContextsPropertyTree!= null):((vlpContextsPropertyTree == null)||(!vlpContextsPropertyTree.isLeaf())))) {
            if (this.vlpContexts == null) {
                _other.vlpContexts = null;
            } else {
                _other.vlpContexts = new ArrayList<EntityFieldVlpContext.Builder<EntityFieldVlpConfig.Builder<_B>>>();
                for (EntityFieldVlpContext _item: this.vlpContexts) {
                    _other.vlpContexts.add(((_item == null)?null:_item.newCopyBuilder(_other, vlpContextsPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree vlpIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("vlpId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(vlpIdPropertyTree!= null):((vlpIdPropertyTree == null)||(!vlpIdPropertyTree.isLeaf())))) {
            _other.vlpId = this.vlpId;
        }
        final PropertyTree valueFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("valueField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valueFieldPropertyTree!= null):((valueFieldPropertyTree == null)||(!valueFieldPropertyTree.isLeaf())))) {
            _other.valueField = this.valueField;
        }
        final PropertyTree idFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("idField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(idFieldPropertyTree!= null):((idFieldPropertyTree == null)||(!idFieldPropertyTree.isLeaf())))) {
            _other.idField = this.idField;
        }
    }

    public<_B >EntityFieldVlpConfig.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new EntityFieldVlpConfig.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public EntityFieldVlpConfig.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >EntityFieldVlpConfig.Builder<_B> copyOf(final EntityFieldVlpConfig _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final EntityFieldVlpConfig.Builder<_B> _newBuilder = new EntityFieldVlpConfig.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static EntityFieldVlpConfig.Builder<Void> copyExcept(final EntityFieldVlpConfig _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static EntityFieldVlpConfig.Builder<Void> copyOnly(final EntityFieldVlpConfig _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final EntityFieldVlpConfig _storedValue;
        private List<EntityFieldVlpContext.Builder<EntityFieldVlpConfig.Builder<_B>>> vlpContexts;
        private String vlpId;
        private String valueField;
        private String idField;

        public Builder(final _B _parentBuilder, final EntityFieldVlpConfig _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.vlpContexts == null) {
                        this.vlpContexts = null;
                    } else {
                        this.vlpContexts = new ArrayList<EntityFieldVlpContext.Builder<EntityFieldVlpConfig.Builder<_B>>>();
                        for (EntityFieldVlpContext _item: _other.vlpContexts) {
                            this.vlpContexts.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.vlpId = _other.vlpId;
                    this.valueField = _other.valueField;
                    this.idField = _other.idField;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final EntityFieldVlpConfig _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree vlpContextsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("vlpContexts"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(vlpContextsPropertyTree!= null):((vlpContextsPropertyTree == null)||(!vlpContextsPropertyTree.isLeaf())))) {
                        if (_other.vlpContexts == null) {
                            this.vlpContexts = null;
                        } else {
                            this.vlpContexts = new ArrayList<EntityFieldVlpContext.Builder<EntityFieldVlpConfig.Builder<_B>>>();
                            for (EntityFieldVlpContext _item: _other.vlpContexts) {
                                this.vlpContexts.add(((_item == null)?null:_item.newCopyBuilder(this, vlpContextsPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree vlpIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("vlpId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(vlpIdPropertyTree!= null):((vlpIdPropertyTree == null)||(!vlpIdPropertyTree.isLeaf())))) {
                        this.vlpId = _other.vlpId;
                    }
                    final PropertyTree valueFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("valueField"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valueFieldPropertyTree!= null):((valueFieldPropertyTree == null)||(!valueFieldPropertyTree.isLeaf())))) {
                        this.valueField = _other.valueField;
                    }
                    final PropertyTree idFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("idField"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(idFieldPropertyTree!= null):((idFieldPropertyTree == null)||(!idFieldPropertyTree.isLeaf())))) {
                        this.idField = _other.idField;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends EntityFieldVlpConfig >_P init(final _P _product) {
            if (this.vlpContexts!= null) {
                final List<EntityFieldVlpContext> vlpContexts = new ArrayList<EntityFieldVlpContext>(this.vlpContexts.size());
                for (EntityFieldVlpContext.Builder<EntityFieldVlpConfig.Builder<_B>> _item: this.vlpContexts) {
                    vlpContexts.add(_item.build());
                }
                _product.vlpContexts = vlpContexts;
            }
            _product.vlpId = this.vlpId;
            _product.valueField = this.valueField;
            _product.idField = this.idField;
            return _product;
        }

        /**
         * Adds the given items to the value of "vlpContexts"
         * 
         * @param vlpContexts
         *     Items to add to the value of the "vlpContexts" property
         */
        public EntityFieldVlpConfig.Builder<_B> addVlpContexts(final Iterable<? extends EntityFieldVlpContext> vlpContexts) {
            if (vlpContexts!= null) {
                if (this.vlpContexts == null) {
                    this.vlpContexts = new ArrayList<EntityFieldVlpContext.Builder<EntityFieldVlpConfig.Builder<_B>>>();
                }
                for (EntityFieldVlpContext _item: vlpContexts) {
                    this.vlpContexts.add(new EntityFieldVlpContext.Builder<EntityFieldVlpConfig.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "vlpContexts" (any previous value will be replaced)
         * 
         * @param vlpContexts
         *     New value of the "vlpContexts" property.
         */
        public EntityFieldVlpConfig.Builder<_B> withVlpContexts(final Iterable<? extends EntityFieldVlpContext> vlpContexts) {
            if (this.vlpContexts!= null) {
                this.vlpContexts.clear();
            }
            return addVlpContexts(vlpContexts);
        }

        /**
         * Adds the given items to the value of "vlpContexts"
         * 
         * @param vlpContexts
         *     Items to add to the value of the "vlpContexts" property
         */
        public EntityFieldVlpConfig.Builder<_B> addVlpContexts(EntityFieldVlpContext... vlpContexts) {
            addVlpContexts(Arrays.asList(vlpContexts));
            return this;
        }

        /**
         * Sets the new value of "vlpContexts" (any previous value will be replaced)
         * 
         * @param vlpContexts
         *     New value of the "vlpContexts" property.
         */
        public EntityFieldVlpConfig.Builder<_B> withVlpContexts(EntityFieldVlpContext... vlpContexts) {
            withVlpContexts(Arrays.asList(vlpContexts));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "VlpContexts" property.
         * Use {@link org.nuclos.schema.meta.entity.EntityFieldVlpContext.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "VlpContexts" property.
         *     Use {@link org.nuclos.schema.meta.entity.EntityFieldVlpContext.Builder#end()} to return to the current builder.
         */
        public EntityFieldVlpContext.Builder<? extends EntityFieldVlpConfig.Builder<_B>> addVlpContexts() {
            if (this.vlpContexts == null) {
                this.vlpContexts = new ArrayList<EntityFieldVlpContext.Builder<EntityFieldVlpConfig.Builder<_B>>>();
            }
            final EntityFieldVlpContext.Builder<EntityFieldVlpConfig.Builder<_B>> vlpContexts_Builder = new EntityFieldVlpContext.Builder<EntityFieldVlpConfig.Builder<_B>>(this, null, false);
            this.vlpContexts.add(vlpContexts_Builder);
            return vlpContexts_Builder;
        }

        /**
         * Sets the new value of "vlpId" (any previous value will be replaced)
         * 
         * @param vlpId
         *     New value of the "vlpId" property.
         */
        public EntityFieldVlpConfig.Builder<_B> withVlpId(final String vlpId) {
            this.vlpId = vlpId;
            return this;
        }

        /**
         * Sets the new value of "valueField" (any previous value will be replaced)
         * 
         * @param valueField
         *     New value of the "valueField" property.
         */
        public EntityFieldVlpConfig.Builder<_B> withValueField(final String valueField) {
            this.valueField = valueField;
            return this;
        }

        /**
         * Sets the new value of "idField" (any previous value will be replaced)
         * 
         * @param idField
         *     New value of the "idField" property.
         */
        public EntityFieldVlpConfig.Builder<_B> withIdField(final String idField) {
            this.idField = idField;
            return this;
        }

        @Override
        public EntityFieldVlpConfig build() {
            if (_storedValue == null) {
                return this.init(new EntityFieldVlpConfig());
            } else {
                return ((EntityFieldVlpConfig) _storedValue);
            }
        }

        public EntityFieldVlpConfig.Builder<_B> copyOf(final EntityFieldVlpConfig _other) {
            _other.copyTo(this);
            return this;
        }

        public EntityFieldVlpConfig.Builder<_B> copyOf(final EntityFieldVlpConfig.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends EntityFieldVlpConfig.Selector<EntityFieldVlpConfig.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static EntityFieldVlpConfig.Select _root() {
            return new EntityFieldVlpConfig.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private EntityFieldVlpContext.Selector<TRoot, EntityFieldVlpConfig.Selector<TRoot, TParent>> vlpContexts = null;
        private com.kscs.util.jaxb.Selector<TRoot, EntityFieldVlpConfig.Selector<TRoot, TParent>> vlpId = null;
        private com.kscs.util.jaxb.Selector<TRoot, EntityFieldVlpConfig.Selector<TRoot, TParent>> valueField = null;
        private com.kscs.util.jaxb.Selector<TRoot, EntityFieldVlpConfig.Selector<TRoot, TParent>> idField = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.vlpContexts!= null) {
                products.put("vlpContexts", this.vlpContexts.init());
            }
            if (this.vlpId!= null) {
                products.put("vlpId", this.vlpId.init());
            }
            if (this.valueField!= null) {
                products.put("valueField", this.valueField.init());
            }
            if (this.idField!= null) {
                products.put("idField", this.idField.init());
            }
            return products;
        }

        public EntityFieldVlpContext.Selector<TRoot, EntityFieldVlpConfig.Selector<TRoot, TParent>> vlpContexts() {
            return ((this.vlpContexts == null)?this.vlpContexts = new EntityFieldVlpContext.Selector<TRoot, EntityFieldVlpConfig.Selector<TRoot, TParent>>(this._root, this, "vlpContexts"):this.vlpContexts);
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityFieldVlpConfig.Selector<TRoot, TParent>> vlpId() {
            return ((this.vlpId == null)?this.vlpId = new com.kscs.util.jaxb.Selector<TRoot, EntityFieldVlpConfig.Selector<TRoot, TParent>>(this._root, this, "vlpId"):this.vlpId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityFieldVlpConfig.Selector<TRoot, TParent>> valueField() {
            return ((this.valueField == null)?this.valueField = new com.kscs.util.jaxb.Selector<TRoot, EntityFieldVlpConfig.Selector<TRoot, TParent>>(this._root, this, "valueField"):this.valueField);
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityFieldVlpConfig.Selector<TRoot, TParent>> idField() {
            return ((this.idField == null)?this.idField = new com.kscs.util.jaxb.Selector<TRoot, EntityFieldVlpConfig.Selector<TRoot, TParent>>(this._root, this, "idField"):this.idField);
        }

    }

}
