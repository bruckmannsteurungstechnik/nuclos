
package org.nuclos.schema.meta.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for entity-field-vlp-context complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="entity-field-vlp-context"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="vlpParams" type="{urn:org.nuclos.schema.meta.entity}entity-field-vlp-param" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="entityClassId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="search" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="input" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "entity-field-vlp-context", propOrder = {
    "vlpParams"
})
public class EntityFieldVlpContext implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected List<EntityFieldVlpParam> vlpParams;
    @XmlAttribute(name = "entityClassId")
    protected String entityClassId;
    @XmlAttribute(name = "search", required = true)
    protected boolean search;
    @XmlAttribute(name = "input", required = true)
    protected boolean input;

    /**
     * Gets the value of the vlpParams property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vlpParams property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVlpParams().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EntityFieldVlpParam }
     * 
     * 
     */
    public List<EntityFieldVlpParam> getVlpParams() {
        if (vlpParams == null) {
            vlpParams = new ArrayList<EntityFieldVlpParam>();
        }
        return this.vlpParams;
    }

    /**
     * Gets the value of the entityClassId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityClassId() {
        return entityClassId;
    }

    /**
     * Sets the value of the entityClassId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityClassId(String value) {
        this.entityClassId = value;
    }

    /**
     * Gets the value of the search property.
     * 
     */
    public boolean isSearch() {
        return search;
    }

    /**
     * Sets the value of the search property.
     * 
     */
    public void setSearch(boolean value) {
        this.search = value;
    }

    /**
     * Gets the value of the input property.
     * 
     */
    public boolean isInput() {
        return input;
    }

    /**
     * Sets the value of the input property.
     * 
     */
    public void setInput(boolean value) {
        this.input = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<EntityFieldVlpParam> theVlpParams;
            theVlpParams = (((this.vlpParams!= null)&&(!this.vlpParams.isEmpty()))?this.getVlpParams():null);
            strategy.appendField(locator, this, "vlpParams", buffer, theVlpParams);
        }
        {
            String theEntityClassId;
            theEntityClassId = this.getEntityClassId();
            strategy.appendField(locator, this, "entityClassId", buffer, theEntityClassId);
        }
        {
            boolean theSearch;
            theSearch = this.isSearch();
            strategy.appendField(locator, this, "search", buffer, theSearch);
        }
        {
            boolean theInput;
            theInput = this.isInput();
            strategy.appendField(locator, this, "input", buffer, theInput);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof EntityFieldVlpContext)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final EntityFieldVlpContext that = ((EntityFieldVlpContext) object);
        {
            List<EntityFieldVlpParam> lhsVlpParams;
            lhsVlpParams = (((this.vlpParams!= null)&&(!this.vlpParams.isEmpty()))?this.getVlpParams():null);
            List<EntityFieldVlpParam> rhsVlpParams;
            rhsVlpParams = (((that.vlpParams!= null)&&(!that.vlpParams.isEmpty()))?that.getVlpParams():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "vlpParams", lhsVlpParams), LocatorUtils.property(thatLocator, "vlpParams", rhsVlpParams), lhsVlpParams, rhsVlpParams)) {
                return false;
            }
        }
        {
            String lhsEntityClassId;
            lhsEntityClassId = this.getEntityClassId();
            String rhsEntityClassId;
            rhsEntityClassId = that.getEntityClassId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityClassId", lhsEntityClassId), LocatorUtils.property(thatLocator, "entityClassId", rhsEntityClassId), lhsEntityClassId, rhsEntityClassId)) {
                return false;
            }
        }
        {
            boolean lhsSearch;
            lhsSearch = this.isSearch();
            boolean rhsSearch;
            rhsSearch = that.isSearch();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "search", lhsSearch), LocatorUtils.property(thatLocator, "search", rhsSearch), lhsSearch, rhsSearch)) {
                return false;
            }
        }
        {
            boolean lhsInput;
            lhsInput = this.isInput();
            boolean rhsInput;
            rhsInput = that.isInput();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "input", lhsInput), LocatorUtils.property(thatLocator, "input", rhsInput), lhsInput, rhsInput)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<EntityFieldVlpParam> theVlpParams;
            theVlpParams = (((this.vlpParams!= null)&&(!this.vlpParams.isEmpty()))?this.getVlpParams():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "vlpParams", theVlpParams), currentHashCode, theVlpParams);
        }
        {
            String theEntityClassId;
            theEntityClassId = this.getEntityClassId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityClassId", theEntityClassId), currentHashCode, theEntityClassId);
        }
        {
            boolean theSearch;
            theSearch = this.isSearch();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "search", theSearch), currentHashCode, theSearch);
        }
        {
            boolean theInput;
            theInput = this.isInput();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "input", theInput), currentHashCode, theInput);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof EntityFieldVlpContext) {
            final EntityFieldVlpContext copy = ((EntityFieldVlpContext) draftCopy);
            if ((this.vlpParams!= null)&&(!this.vlpParams.isEmpty())) {
                List<EntityFieldVlpParam> sourceVlpParams;
                sourceVlpParams = (((this.vlpParams!= null)&&(!this.vlpParams.isEmpty()))?this.getVlpParams():null);
                @SuppressWarnings("unchecked")
                List<EntityFieldVlpParam> copyVlpParams = ((List<EntityFieldVlpParam> ) strategy.copy(LocatorUtils.property(locator, "vlpParams", sourceVlpParams), sourceVlpParams));
                copy.vlpParams = null;
                if (copyVlpParams!= null) {
                    List<EntityFieldVlpParam> uniqueVlpParamsl = copy.getVlpParams();
                    uniqueVlpParamsl.addAll(copyVlpParams);
                }
            } else {
                copy.vlpParams = null;
            }
            if (this.entityClassId!= null) {
                String sourceEntityClassId;
                sourceEntityClassId = this.getEntityClassId();
                String copyEntityClassId = ((String) strategy.copy(LocatorUtils.property(locator, "entityClassId", sourceEntityClassId), sourceEntityClassId));
                copy.setEntityClassId(copyEntityClassId);
            } else {
                copy.entityClassId = null;
            }
            {
                boolean sourceSearch;
                sourceSearch = this.isSearch();
                boolean copySearch = strategy.copy(LocatorUtils.property(locator, "search", sourceSearch), sourceSearch);
                copy.setSearch(copySearch);
            }
            {
                boolean sourceInput;
                sourceInput = this.isInput();
                boolean copyInput = strategy.copy(LocatorUtils.property(locator, "input", sourceInput), sourceInput);
                copy.setInput(copyInput);
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new EntityFieldVlpContext();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityFieldVlpContext.Builder<_B> _other) {
        if (this.vlpParams == null) {
            _other.vlpParams = null;
        } else {
            _other.vlpParams = new ArrayList<EntityFieldVlpParam.Builder<EntityFieldVlpContext.Builder<_B>>>();
            for (EntityFieldVlpParam _item: this.vlpParams) {
                _other.vlpParams.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.entityClassId = this.entityClassId;
        _other.search = this.search;
        _other.input = this.input;
    }

    public<_B >EntityFieldVlpContext.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new EntityFieldVlpContext.Builder<_B>(_parentBuilder, this, true);
    }

    public EntityFieldVlpContext.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static EntityFieldVlpContext.Builder<Void> builder() {
        return new EntityFieldVlpContext.Builder<Void>(null, null, false);
    }

    public static<_B >EntityFieldVlpContext.Builder<_B> copyOf(final EntityFieldVlpContext _other) {
        final EntityFieldVlpContext.Builder<_B> _newBuilder = new EntityFieldVlpContext.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityFieldVlpContext.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree vlpParamsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("vlpParams"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(vlpParamsPropertyTree!= null):((vlpParamsPropertyTree == null)||(!vlpParamsPropertyTree.isLeaf())))) {
            if (this.vlpParams == null) {
                _other.vlpParams = null;
            } else {
                _other.vlpParams = new ArrayList<EntityFieldVlpParam.Builder<EntityFieldVlpContext.Builder<_B>>>();
                for (EntityFieldVlpParam _item: this.vlpParams) {
                    _other.vlpParams.add(((_item == null)?null:_item.newCopyBuilder(_other, vlpParamsPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree entityClassIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityClassId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityClassIdPropertyTree!= null):((entityClassIdPropertyTree == null)||(!entityClassIdPropertyTree.isLeaf())))) {
            _other.entityClassId = this.entityClassId;
        }
        final PropertyTree searchPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("search"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(searchPropertyTree!= null):((searchPropertyTree == null)||(!searchPropertyTree.isLeaf())))) {
            _other.search = this.search;
        }
        final PropertyTree inputPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("input"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(inputPropertyTree!= null):((inputPropertyTree == null)||(!inputPropertyTree.isLeaf())))) {
            _other.input = this.input;
        }
    }

    public<_B >EntityFieldVlpContext.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new EntityFieldVlpContext.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public EntityFieldVlpContext.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >EntityFieldVlpContext.Builder<_B> copyOf(final EntityFieldVlpContext _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final EntityFieldVlpContext.Builder<_B> _newBuilder = new EntityFieldVlpContext.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static EntityFieldVlpContext.Builder<Void> copyExcept(final EntityFieldVlpContext _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static EntityFieldVlpContext.Builder<Void> copyOnly(final EntityFieldVlpContext _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final EntityFieldVlpContext _storedValue;
        private List<EntityFieldVlpParam.Builder<EntityFieldVlpContext.Builder<_B>>> vlpParams;
        private String entityClassId;
        private boolean search;
        private boolean input;

        public Builder(final _B _parentBuilder, final EntityFieldVlpContext _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.vlpParams == null) {
                        this.vlpParams = null;
                    } else {
                        this.vlpParams = new ArrayList<EntityFieldVlpParam.Builder<EntityFieldVlpContext.Builder<_B>>>();
                        for (EntityFieldVlpParam _item: _other.vlpParams) {
                            this.vlpParams.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.entityClassId = _other.entityClassId;
                    this.search = _other.search;
                    this.input = _other.input;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final EntityFieldVlpContext _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree vlpParamsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("vlpParams"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(vlpParamsPropertyTree!= null):((vlpParamsPropertyTree == null)||(!vlpParamsPropertyTree.isLeaf())))) {
                        if (_other.vlpParams == null) {
                            this.vlpParams = null;
                        } else {
                            this.vlpParams = new ArrayList<EntityFieldVlpParam.Builder<EntityFieldVlpContext.Builder<_B>>>();
                            for (EntityFieldVlpParam _item: _other.vlpParams) {
                                this.vlpParams.add(((_item == null)?null:_item.newCopyBuilder(this, vlpParamsPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree entityClassIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityClassId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityClassIdPropertyTree!= null):((entityClassIdPropertyTree == null)||(!entityClassIdPropertyTree.isLeaf())))) {
                        this.entityClassId = _other.entityClassId;
                    }
                    final PropertyTree searchPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("search"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(searchPropertyTree!= null):((searchPropertyTree == null)||(!searchPropertyTree.isLeaf())))) {
                        this.search = _other.search;
                    }
                    final PropertyTree inputPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("input"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(inputPropertyTree!= null):((inputPropertyTree == null)||(!inputPropertyTree.isLeaf())))) {
                        this.input = _other.input;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends EntityFieldVlpContext >_P init(final _P _product) {
            if (this.vlpParams!= null) {
                final List<EntityFieldVlpParam> vlpParams = new ArrayList<EntityFieldVlpParam>(this.vlpParams.size());
                for (EntityFieldVlpParam.Builder<EntityFieldVlpContext.Builder<_B>> _item: this.vlpParams) {
                    vlpParams.add(_item.build());
                }
                _product.vlpParams = vlpParams;
            }
            _product.entityClassId = this.entityClassId;
            _product.search = this.search;
            _product.input = this.input;
            return _product;
        }

        /**
         * Adds the given items to the value of "vlpParams"
         * 
         * @param vlpParams
         *     Items to add to the value of the "vlpParams" property
         */
        public EntityFieldVlpContext.Builder<_B> addVlpParams(final Iterable<? extends EntityFieldVlpParam> vlpParams) {
            if (vlpParams!= null) {
                if (this.vlpParams == null) {
                    this.vlpParams = new ArrayList<EntityFieldVlpParam.Builder<EntityFieldVlpContext.Builder<_B>>>();
                }
                for (EntityFieldVlpParam _item: vlpParams) {
                    this.vlpParams.add(new EntityFieldVlpParam.Builder<EntityFieldVlpContext.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "vlpParams" (any previous value will be replaced)
         * 
         * @param vlpParams
         *     New value of the "vlpParams" property.
         */
        public EntityFieldVlpContext.Builder<_B> withVlpParams(final Iterable<? extends EntityFieldVlpParam> vlpParams) {
            if (this.vlpParams!= null) {
                this.vlpParams.clear();
            }
            return addVlpParams(vlpParams);
        }

        /**
         * Adds the given items to the value of "vlpParams"
         * 
         * @param vlpParams
         *     Items to add to the value of the "vlpParams" property
         */
        public EntityFieldVlpContext.Builder<_B> addVlpParams(EntityFieldVlpParam... vlpParams) {
            addVlpParams(Arrays.asList(vlpParams));
            return this;
        }

        /**
         * Sets the new value of "vlpParams" (any previous value will be replaced)
         * 
         * @param vlpParams
         *     New value of the "vlpParams" property.
         */
        public EntityFieldVlpContext.Builder<_B> withVlpParams(EntityFieldVlpParam... vlpParams) {
            withVlpParams(Arrays.asList(vlpParams));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "VlpParams" property.
         * Use {@link org.nuclos.schema.meta.entity.EntityFieldVlpParam.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "VlpParams" property.
         *     Use {@link org.nuclos.schema.meta.entity.EntityFieldVlpParam.Builder#end()} to return to the current builder.
         */
        public EntityFieldVlpParam.Builder<? extends EntityFieldVlpContext.Builder<_B>> addVlpParams() {
            if (this.vlpParams == null) {
                this.vlpParams = new ArrayList<EntityFieldVlpParam.Builder<EntityFieldVlpContext.Builder<_B>>>();
            }
            final EntityFieldVlpParam.Builder<EntityFieldVlpContext.Builder<_B>> vlpParams_Builder = new EntityFieldVlpParam.Builder<EntityFieldVlpContext.Builder<_B>>(this, null, false);
            this.vlpParams.add(vlpParams_Builder);
            return vlpParams_Builder;
        }

        /**
         * Sets the new value of "entityClassId" (any previous value will be replaced)
         * 
         * @param entityClassId
         *     New value of the "entityClassId" property.
         */
        public EntityFieldVlpContext.Builder<_B> withEntityClassId(final String entityClassId) {
            this.entityClassId = entityClassId;
            return this;
        }

        /**
         * Sets the new value of "search" (any previous value will be replaced)
         * 
         * @param search
         *     New value of the "search" property.
         */
        public EntityFieldVlpContext.Builder<_B> withSearch(final boolean search) {
            this.search = search;
            return this;
        }

        /**
         * Sets the new value of "input" (any previous value will be replaced)
         * 
         * @param input
         *     New value of the "input" property.
         */
        public EntityFieldVlpContext.Builder<_B> withInput(final boolean input) {
            this.input = input;
            return this;
        }

        @Override
        public EntityFieldVlpContext build() {
            if (_storedValue == null) {
                return this.init(new EntityFieldVlpContext());
            } else {
                return ((EntityFieldVlpContext) _storedValue);
            }
        }

        public EntityFieldVlpContext.Builder<_B> copyOf(final EntityFieldVlpContext _other) {
            _other.copyTo(this);
            return this;
        }

        public EntityFieldVlpContext.Builder<_B> copyOf(final EntityFieldVlpContext.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends EntityFieldVlpContext.Selector<EntityFieldVlpContext.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static EntityFieldVlpContext.Select _root() {
            return new EntityFieldVlpContext.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private EntityFieldVlpParam.Selector<TRoot, EntityFieldVlpContext.Selector<TRoot, TParent>> vlpParams = null;
        private com.kscs.util.jaxb.Selector<TRoot, EntityFieldVlpContext.Selector<TRoot, TParent>> entityClassId = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.vlpParams!= null) {
                products.put("vlpParams", this.vlpParams.init());
            }
            if (this.entityClassId!= null) {
                products.put("entityClassId", this.entityClassId.init());
            }
            return products;
        }

        public EntityFieldVlpParam.Selector<TRoot, EntityFieldVlpContext.Selector<TRoot, TParent>> vlpParams() {
            return ((this.vlpParams == null)?this.vlpParams = new EntityFieldVlpParam.Selector<TRoot, EntityFieldVlpContext.Selector<TRoot, TParent>>(this._root, this, "vlpParams"):this.vlpParams);
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityFieldVlpContext.Selector<TRoot, TParent>> entityClassId() {
            return ((this.entityClassId == null)?this.entityClassId = new com.kscs.util.jaxb.Selector<TRoot, EntityFieldVlpContext.Selector<TRoot, TParent>>(this._root, this, "entityClassId"):this.entityClassId);
        }

    }

}
