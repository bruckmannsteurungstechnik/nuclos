
package org.nuclos.schema.meta.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for entity-field-vlp-param complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="entity-field-vlp-param"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="value" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="valueFromEntityFieldId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "entity-field-vlp-param")
public class EntityFieldVlpParam implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "value")
    protected String value;
    @XmlAttribute(name = "valueFromEntityFieldId")
    protected String valueFromEntityFieldId;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the valueFromEntityFieldId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValueFromEntityFieldId() {
        return valueFromEntityFieldId;
    }

    /**
     * Sets the value of the valueFromEntityFieldId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValueFromEntityFieldId(String value) {
        this.valueFromEntityFieldId = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String theValue;
            theValue = this.getValue();
            strategy.appendField(locator, this, "value", buffer, theValue);
        }
        {
            String theValueFromEntityFieldId;
            theValueFromEntityFieldId = this.getValueFromEntityFieldId();
            strategy.appendField(locator, this, "valueFromEntityFieldId", buffer, theValueFromEntityFieldId);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof EntityFieldVlpParam)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final EntityFieldVlpParam that = ((EntityFieldVlpParam) object);
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsValue;
            lhsValue = this.getValue();
            String rhsValue;
            rhsValue = that.getValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "value", lhsValue), LocatorUtils.property(thatLocator, "value", rhsValue), lhsValue, rhsValue)) {
                return false;
            }
        }
        {
            String lhsValueFromEntityFieldId;
            lhsValueFromEntityFieldId = this.getValueFromEntityFieldId();
            String rhsValueFromEntityFieldId;
            rhsValueFromEntityFieldId = that.getValueFromEntityFieldId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "valueFromEntityFieldId", lhsValueFromEntityFieldId), LocatorUtils.property(thatLocator, "valueFromEntityFieldId", rhsValueFromEntityFieldId), lhsValueFromEntityFieldId, rhsValueFromEntityFieldId)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String theValue;
            theValue = this.getValue();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "value", theValue), currentHashCode, theValue);
        }
        {
            String theValueFromEntityFieldId;
            theValueFromEntityFieldId = this.getValueFromEntityFieldId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "valueFromEntityFieldId", theValueFromEntityFieldId), currentHashCode, theValueFromEntityFieldId);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof EntityFieldVlpParam) {
            final EntityFieldVlpParam copy = ((EntityFieldVlpParam) draftCopy);
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.value!= null) {
                String sourceValue;
                sourceValue = this.getValue();
                String copyValue = ((String) strategy.copy(LocatorUtils.property(locator, "value", sourceValue), sourceValue));
                copy.setValue(copyValue);
            } else {
                copy.value = null;
            }
            if (this.valueFromEntityFieldId!= null) {
                String sourceValueFromEntityFieldId;
                sourceValueFromEntityFieldId = this.getValueFromEntityFieldId();
                String copyValueFromEntityFieldId = ((String) strategy.copy(LocatorUtils.property(locator, "valueFromEntityFieldId", sourceValueFromEntityFieldId), sourceValueFromEntityFieldId));
                copy.setValueFromEntityFieldId(copyValueFromEntityFieldId);
            } else {
                copy.valueFromEntityFieldId = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new EntityFieldVlpParam();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityFieldVlpParam.Builder<_B> _other) {
        _other.name = this.name;
        _other.value = this.value;
        _other.valueFromEntityFieldId = this.valueFromEntityFieldId;
    }

    public<_B >EntityFieldVlpParam.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new EntityFieldVlpParam.Builder<_B>(_parentBuilder, this, true);
    }

    public EntityFieldVlpParam.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static EntityFieldVlpParam.Builder<Void> builder() {
        return new EntityFieldVlpParam.Builder<Void>(null, null, false);
    }

    public static<_B >EntityFieldVlpParam.Builder<_B> copyOf(final EntityFieldVlpParam _other) {
        final EntityFieldVlpParam.Builder<_B> _newBuilder = new EntityFieldVlpParam.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityFieldVlpParam.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree valuePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("value"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuePropertyTree!= null):((valuePropertyTree == null)||(!valuePropertyTree.isLeaf())))) {
            _other.value = this.value;
        }
        final PropertyTree valueFromEntityFieldIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("valueFromEntityFieldId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valueFromEntityFieldIdPropertyTree!= null):((valueFromEntityFieldIdPropertyTree == null)||(!valueFromEntityFieldIdPropertyTree.isLeaf())))) {
            _other.valueFromEntityFieldId = this.valueFromEntityFieldId;
        }
    }

    public<_B >EntityFieldVlpParam.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new EntityFieldVlpParam.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public EntityFieldVlpParam.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >EntityFieldVlpParam.Builder<_B> copyOf(final EntityFieldVlpParam _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final EntityFieldVlpParam.Builder<_B> _newBuilder = new EntityFieldVlpParam.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static EntityFieldVlpParam.Builder<Void> copyExcept(final EntityFieldVlpParam _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static EntityFieldVlpParam.Builder<Void> copyOnly(final EntityFieldVlpParam _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final EntityFieldVlpParam _storedValue;
        private String name;
        private String value;
        private String valueFromEntityFieldId;

        public Builder(final _B _parentBuilder, final EntityFieldVlpParam _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.name = _other.name;
                    this.value = _other.value;
                    this.valueFromEntityFieldId = _other.valueFromEntityFieldId;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final EntityFieldVlpParam _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree valuePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("value"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuePropertyTree!= null):((valuePropertyTree == null)||(!valuePropertyTree.isLeaf())))) {
                        this.value = _other.value;
                    }
                    final PropertyTree valueFromEntityFieldIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("valueFromEntityFieldId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valueFromEntityFieldIdPropertyTree!= null):((valueFromEntityFieldIdPropertyTree == null)||(!valueFromEntityFieldIdPropertyTree.isLeaf())))) {
                        this.valueFromEntityFieldId = _other.valueFromEntityFieldId;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends EntityFieldVlpParam >_P init(final _P _product) {
            _product.name = this.name;
            _product.value = this.value;
            _product.valueFromEntityFieldId = this.valueFromEntityFieldId;
            return _product;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public EntityFieldVlpParam.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "value" (any previous value will be replaced)
         * 
         * @param value
         *     New value of the "value" property.
         */
        public EntityFieldVlpParam.Builder<_B> withValue(final String value) {
            this.value = value;
            return this;
        }

        /**
         * Sets the new value of "valueFromEntityFieldId" (any previous value will be replaced)
         * 
         * @param valueFromEntityFieldId
         *     New value of the "valueFromEntityFieldId" property.
         */
        public EntityFieldVlpParam.Builder<_B> withValueFromEntityFieldId(final String valueFromEntityFieldId) {
            this.valueFromEntityFieldId = valueFromEntityFieldId;
            return this;
        }

        @Override
        public EntityFieldVlpParam build() {
            if (_storedValue == null) {
                return this.init(new EntityFieldVlpParam());
            } else {
                return ((EntityFieldVlpParam) _storedValue);
            }
        }

        public EntityFieldVlpParam.Builder<_B> copyOf(final EntityFieldVlpParam _other) {
            _other.copyTo(this);
            return this;
        }

        public EntityFieldVlpParam.Builder<_B> copyOf(final EntityFieldVlpParam.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends EntityFieldVlpParam.Selector<EntityFieldVlpParam.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static EntityFieldVlpParam.Select _root() {
            return new EntityFieldVlpParam.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, EntityFieldVlpParam.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, EntityFieldVlpParam.Selector<TRoot, TParent>> value = null;
        private com.kscs.util.jaxb.Selector<TRoot, EntityFieldVlpParam.Selector<TRoot, TParent>> valueFromEntityFieldId = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.value!= null) {
                products.put("value", this.value.init());
            }
            if (this.valueFromEntityFieldId!= null) {
                products.put("valueFromEntityFieldId", this.valueFromEntityFieldId.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityFieldVlpParam.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, EntityFieldVlpParam.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityFieldVlpParam.Selector<TRoot, TParent>> value() {
            return ((this.value == null)?this.value = new com.kscs.util.jaxb.Selector<TRoot, EntityFieldVlpParam.Selector<TRoot, TParent>>(this._root, this, "value"):this.value);
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityFieldVlpParam.Selector<TRoot, TParent>> valueFromEntityFieldId() {
            return ((this.valueFromEntityFieldId == null)?this.valueFromEntityFieldId = new com.kscs.util.jaxb.Selector<TRoot, EntityFieldVlpParam.Selector<TRoot, TParent>>(this._root, this, "valueFromEntityFieldId"):this.valueFromEntityFieldId);
        }

    }

}
