
package org.nuclos.schema.meta.entity;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.nuclos.schema.meta.entity package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.nuclos.schema.meta.entity
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EntityFieldVlpConfig }
     * 
     */
    public EntityFieldVlpConfig createEntityFieldVlpConfig() {
        return new EntityFieldVlpConfig();
    }

    /**
     * Create an instance of {@link EntityFieldVlpContext }
     * 
     */
    public EntityFieldVlpContext createEntityFieldVlpContext() {
        return new EntityFieldVlpContext();
    }

    /**
     * Create an instance of {@link EntityFieldVlpParam }
     * 
     */
    public EntityFieldVlpParam createEntityFieldVlpParam() {
        return new EntityFieldVlpParam();
    }

    /**
     * Create an instance of {@link EntityContext }
     * 
     */
    public EntityContext createEntityContext() {
        return new EntityContext();
    }

}
