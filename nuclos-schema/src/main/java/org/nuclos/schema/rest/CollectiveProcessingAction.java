
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for collective-processing-action complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="collective-processing-action"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="links" type="{urn:org.nuclos.schema.rest}collective-processing-action-links"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="subName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="description" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="color" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="withoutConfirmation" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "collective-processing-action", propOrder = {
    "links"
})
public class CollectiveProcessingAction implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected CollectiveProcessingActionLinks links;
    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "subName")
    protected String subName;
    @XmlAttribute(name = "description")
    protected String description;
    @XmlAttribute(name = "color")
    protected String color;
    @XmlAttribute(name = "withoutConfirmation", required = true)
    protected boolean withoutConfirmation;

    /**
     * Gets the value of the links property.
     * 
     * @return
     *     possible object is
     *     {@link CollectiveProcessingActionLinks }
     *     
     */
    public CollectiveProcessingActionLinks getLinks() {
        return links;
    }

    /**
     * Sets the value of the links property.
     * 
     * @param value
     *     allowed object is
     *     {@link CollectiveProcessingActionLinks }
     *     
     */
    public void setLinks(CollectiveProcessingActionLinks value) {
        this.links = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the subName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubName() {
        return subName;
    }

    /**
     * Sets the value of the subName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubName(String value) {
        this.subName = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the color property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColor() {
        return color;
    }

    /**
     * Sets the value of the color property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColor(String value) {
        this.color = value;
    }

    /**
     * Gets the value of the withoutConfirmation property.
     * 
     */
    public boolean isWithoutConfirmation() {
        return withoutConfirmation;
    }

    /**
     * Sets the value of the withoutConfirmation property.
     * 
     */
    public void setWithoutConfirmation(boolean value) {
        this.withoutConfirmation = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            CollectiveProcessingActionLinks theLinks;
            theLinks = this.getLinks();
            strategy.appendField(locator, this, "links", buffer, theLinks);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String theSubName;
            theSubName = this.getSubName();
            strategy.appendField(locator, this, "subName", buffer, theSubName);
        }
        {
            String theDescription;
            theDescription = this.getDescription();
            strategy.appendField(locator, this, "description", buffer, theDescription);
        }
        {
            String theColor;
            theColor = this.getColor();
            strategy.appendField(locator, this, "color", buffer, theColor);
        }
        {
            boolean theWithoutConfirmation;
            theWithoutConfirmation = this.isWithoutConfirmation();
            strategy.appendField(locator, this, "withoutConfirmation", buffer, theWithoutConfirmation);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof CollectiveProcessingAction)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final CollectiveProcessingAction that = ((CollectiveProcessingAction) object);
        {
            CollectiveProcessingActionLinks lhsLinks;
            lhsLinks = this.getLinks();
            CollectiveProcessingActionLinks rhsLinks;
            rhsLinks = that.getLinks();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "links", lhsLinks), LocatorUtils.property(thatLocator, "links", rhsLinks), lhsLinks, rhsLinks)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsSubName;
            lhsSubName = this.getSubName();
            String rhsSubName;
            rhsSubName = that.getSubName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "subName", lhsSubName), LocatorUtils.property(thatLocator, "subName", rhsSubName), lhsSubName, rhsSubName)) {
                return false;
            }
        }
        {
            String lhsDescription;
            lhsDescription = this.getDescription();
            String rhsDescription;
            rhsDescription = that.getDescription();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "description", lhsDescription), LocatorUtils.property(thatLocator, "description", rhsDescription), lhsDescription, rhsDescription)) {
                return false;
            }
        }
        {
            String lhsColor;
            lhsColor = this.getColor();
            String rhsColor;
            rhsColor = that.getColor();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "color", lhsColor), LocatorUtils.property(thatLocator, "color", rhsColor), lhsColor, rhsColor)) {
                return false;
            }
        }
        {
            boolean lhsWithoutConfirmation;
            lhsWithoutConfirmation = this.isWithoutConfirmation();
            boolean rhsWithoutConfirmation;
            rhsWithoutConfirmation = that.isWithoutConfirmation();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "withoutConfirmation", lhsWithoutConfirmation), LocatorUtils.property(thatLocator, "withoutConfirmation", rhsWithoutConfirmation), lhsWithoutConfirmation, rhsWithoutConfirmation)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            CollectiveProcessingActionLinks theLinks;
            theLinks = this.getLinks();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "links", theLinks), currentHashCode, theLinks);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String theSubName;
            theSubName = this.getSubName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "subName", theSubName), currentHashCode, theSubName);
        }
        {
            String theDescription;
            theDescription = this.getDescription();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "description", theDescription), currentHashCode, theDescription);
        }
        {
            String theColor;
            theColor = this.getColor();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "color", theColor), currentHashCode, theColor);
        }
        {
            boolean theWithoutConfirmation;
            theWithoutConfirmation = this.isWithoutConfirmation();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "withoutConfirmation", theWithoutConfirmation), currentHashCode, theWithoutConfirmation);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof CollectiveProcessingAction) {
            final CollectiveProcessingAction copy = ((CollectiveProcessingAction) draftCopy);
            if (this.links!= null) {
                CollectiveProcessingActionLinks sourceLinks;
                sourceLinks = this.getLinks();
                CollectiveProcessingActionLinks copyLinks = ((CollectiveProcessingActionLinks) strategy.copy(LocatorUtils.property(locator, "links", sourceLinks), sourceLinks));
                copy.setLinks(copyLinks);
            } else {
                copy.links = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.subName!= null) {
                String sourceSubName;
                sourceSubName = this.getSubName();
                String copySubName = ((String) strategy.copy(LocatorUtils.property(locator, "subName", sourceSubName), sourceSubName));
                copy.setSubName(copySubName);
            } else {
                copy.subName = null;
            }
            if (this.description!= null) {
                String sourceDescription;
                sourceDescription = this.getDescription();
                String copyDescription = ((String) strategy.copy(LocatorUtils.property(locator, "description", sourceDescription), sourceDescription));
                copy.setDescription(copyDescription);
            } else {
                copy.description = null;
            }
            if (this.color!= null) {
                String sourceColor;
                sourceColor = this.getColor();
                String copyColor = ((String) strategy.copy(LocatorUtils.property(locator, "color", sourceColor), sourceColor));
                copy.setColor(copyColor);
            } else {
                copy.color = null;
            }
            {
                boolean sourceWithoutConfirmation;
                sourceWithoutConfirmation = this.isWithoutConfirmation();
                boolean copyWithoutConfirmation = strategy.copy(LocatorUtils.property(locator, "withoutConfirmation", sourceWithoutConfirmation), sourceWithoutConfirmation);
                copy.setWithoutConfirmation(copyWithoutConfirmation);
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new CollectiveProcessingAction();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final CollectiveProcessingAction.Builder<_B> _other) {
        _other.links = ((this.links == null)?null:this.links.newCopyBuilder(_other));
        _other.name = this.name;
        _other.subName = this.subName;
        _other.description = this.description;
        _other.color = this.color;
        _other.withoutConfirmation = this.withoutConfirmation;
    }

    public<_B >CollectiveProcessingAction.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new CollectiveProcessingAction.Builder<_B>(_parentBuilder, this, true);
    }

    public CollectiveProcessingAction.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static CollectiveProcessingAction.Builder<Void> builder() {
        return new CollectiveProcessingAction.Builder<Void>(null, null, false);
    }

    public static<_B >CollectiveProcessingAction.Builder<_B> copyOf(final CollectiveProcessingAction _other) {
        final CollectiveProcessingAction.Builder<_B> _newBuilder = new CollectiveProcessingAction.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final CollectiveProcessingAction.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree linksPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("links"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(linksPropertyTree!= null):((linksPropertyTree == null)||(!linksPropertyTree.isLeaf())))) {
            _other.links = ((this.links == null)?null:this.links.newCopyBuilder(_other, linksPropertyTree, _propertyTreeUse));
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree subNamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("subName"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(subNamePropertyTree!= null):((subNamePropertyTree == null)||(!subNamePropertyTree.isLeaf())))) {
            _other.subName = this.subName;
        }
        final PropertyTree descriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("description"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(descriptionPropertyTree!= null):((descriptionPropertyTree == null)||(!descriptionPropertyTree.isLeaf())))) {
            _other.description = this.description;
        }
        final PropertyTree colorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("color"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(colorPropertyTree!= null):((colorPropertyTree == null)||(!colorPropertyTree.isLeaf())))) {
            _other.color = this.color;
        }
        final PropertyTree withoutConfirmationPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("withoutConfirmation"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(withoutConfirmationPropertyTree!= null):((withoutConfirmationPropertyTree == null)||(!withoutConfirmationPropertyTree.isLeaf())))) {
            _other.withoutConfirmation = this.withoutConfirmation;
        }
    }

    public<_B >CollectiveProcessingAction.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new CollectiveProcessingAction.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public CollectiveProcessingAction.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >CollectiveProcessingAction.Builder<_B> copyOf(final CollectiveProcessingAction _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final CollectiveProcessingAction.Builder<_B> _newBuilder = new CollectiveProcessingAction.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static CollectiveProcessingAction.Builder<Void> copyExcept(final CollectiveProcessingAction _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static CollectiveProcessingAction.Builder<Void> copyOnly(final CollectiveProcessingAction _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final CollectiveProcessingAction _storedValue;
        private CollectiveProcessingActionLinks.Builder<CollectiveProcessingAction.Builder<_B>> links;
        private String name;
        private String subName;
        private String description;
        private String color;
        private boolean withoutConfirmation;

        public Builder(final _B _parentBuilder, final CollectiveProcessingAction _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.links = ((_other.links == null)?null:_other.links.newCopyBuilder(this));
                    this.name = _other.name;
                    this.subName = _other.subName;
                    this.description = _other.description;
                    this.color = _other.color;
                    this.withoutConfirmation = _other.withoutConfirmation;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final CollectiveProcessingAction _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree linksPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("links"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(linksPropertyTree!= null):((linksPropertyTree == null)||(!linksPropertyTree.isLeaf())))) {
                        this.links = ((_other.links == null)?null:_other.links.newCopyBuilder(this, linksPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree subNamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("subName"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(subNamePropertyTree!= null):((subNamePropertyTree == null)||(!subNamePropertyTree.isLeaf())))) {
                        this.subName = _other.subName;
                    }
                    final PropertyTree descriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("description"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(descriptionPropertyTree!= null):((descriptionPropertyTree == null)||(!descriptionPropertyTree.isLeaf())))) {
                        this.description = _other.description;
                    }
                    final PropertyTree colorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("color"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(colorPropertyTree!= null):((colorPropertyTree == null)||(!colorPropertyTree.isLeaf())))) {
                        this.color = _other.color;
                    }
                    final PropertyTree withoutConfirmationPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("withoutConfirmation"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(withoutConfirmationPropertyTree!= null):((withoutConfirmationPropertyTree == null)||(!withoutConfirmationPropertyTree.isLeaf())))) {
                        this.withoutConfirmation = _other.withoutConfirmation;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends CollectiveProcessingAction >_P init(final _P _product) {
            _product.links = ((this.links == null)?null:this.links.build());
            _product.name = this.name;
            _product.subName = this.subName;
            _product.description = this.description;
            _product.color = this.color;
            _product.withoutConfirmation = this.withoutConfirmation;
            return _product;
        }

        /**
         * Sets the new value of "links" (any previous value will be replaced)
         * 
         * @param links
         *     New value of the "links" property.
         */
        public CollectiveProcessingAction.Builder<_B> withLinks(final CollectiveProcessingActionLinks links) {
            this.links = ((links == null)?null:new CollectiveProcessingActionLinks.Builder<CollectiveProcessingAction.Builder<_B>>(this, links, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "links" property.
         * Use {@link org.nuclos.schema.rest.CollectiveProcessingActionLinks.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "links" property.
         *     Use {@link org.nuclos.schema.rest.CollectiveProcessingActionLinks.Builder#end()} to return to the current builder.
         */
        public CollectiveProcessingActionLinks.Builder<? extends CollectiveProcessingAction.Builder<_B>> withLinks() {
            if (this.links!= null) {
                return this.links;
            }
            return this.links = new CollectiveProcessingActionLinks.Builder<CollectiveProcessingAction.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public CollectiveProcessingAction.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "subName" (any previous value will be replaced)
         * 
         * @param subName
         *     New value of the "subName" property.
         */
        public CollectiveProcessingAction.Builder<_B> withSubName(final String subName) {
            this.subName = subName;
            return this;
        }

        /**
         * Sets the new value of "description" (any previous value will be replaced)
         * 
         * @param description
         *     New value of the "description" property.
         */
        public CollectiveProcessingAction.Builder<_B> withDescription(final String description) {
            this.description = description;
            return this;
        }

        /**
         * Sets the new value of "color" (any previous value will be replaced)
         * 
         * @param color
         *     New value of the "color" property.
         */
        public CollectiveProcessingAction.Builder<_B> withColor(final String color) {
            this.color = color;
            return this;
        }

        /**
         * Sets the new value of "withoutConfirmation" (any previous value will be replaced)
         * 
         * @param withoutConfirmation
         *     New value of the "withoutConfirmation" property.
         */
        public CollectiveProcessingAction.Builder<_B> withWithoutConfirmation(final boolean withoutConfirmation) {
            this.withoutConfirmation = withoutConfirmation;
            return this;
        }

        @Override
        public CollectiveProcessingAction build() {
            if (_storedValue == null) {
                return this.init(new CollectiveProcessingAction());
            } else {
                return ((CollectiveProcessingAction) _storedValue);
            }
        }

        public CollectiveProcessingAction.Builder<_B> copyOf(final CollectiveProcessingAction _other) {
            _other.copyTo(this);
            return this;
        }

        public CollectiveProcessingAction.Builder<_B> copyOf(final CollectiveProcessingAction.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends CollectiveProcessingAction.Selector<CollectiveProcessingAction.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static CollectiveProcessingAction.Select _root() {
            return new CollectiveProcessingAction.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private CollectiveProcessingActionLinks.Selector<TRoot, CollectiveProcessingAction.Selector<TRoot, TParent>> links = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectiveProcessingAction.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectiveProcessingAction.Selector<TRoot, TParent>> subName = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectiveProcessingAction.Selector<TRoot, TParent>> description = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectiveProcessingAction.Selector<TRoot, TParent>> color = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.links!= null) {
                products.put("links", this.links.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.subName!= null) {
                products.put("subName", this.subName.init());
            }
            if (this.description!= null) {
                products.put("description", this.description.init());
            }
            if (this.color!= null) {
                products.put("color", this.color.init());
            }
            return products;
        }

        public CollectiveProcessingActionLinks.Selector<TRoot, CollectiveProcessingAction.Selector<TRoot, TParent>> links() {
            return ((this.links == null)?this.links = new CollectiveProcessingActionLinks.Selector<TRoot, CollectiveProcessingAction.Selector<TRoot, TParent>>(this._root, this, "links"):this.links);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectiveProcessingAction.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, CollectiveProcessingAction.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectiveProcessingAction.Selector<TRoot, TParent>> subName() {
            return ((this.subName == null)?this.subName = new com.kscs.util.jaxb.Selector<TRoot, CollectiveProcessingAction.Selector<TRoot, TParent>>(this._root, this, "subName"):this.subName);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectiveProcessingAction.Selector<TRoot, TParent>> description() {
            return ((this.description == null)?this.description = new com.kscs.util.jaxb.Selector<TRoot, CollectiveProcessingAction.Selector<TRoot, TParent>>(this._root, this, "description"):this.description);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectiveProcessingAction.Selector<TRoot, TParent>> color() {
            return ((this.color == null)?this.color = new com.kscs.util.jaxb.Selector<TRoot, CollectiveProcessingAction.Selector<TRoot, TParent>>(this._root, this, "color"):this.color);
        }

    }

}
