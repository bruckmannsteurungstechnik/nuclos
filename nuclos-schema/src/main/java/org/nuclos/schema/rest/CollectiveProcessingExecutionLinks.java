
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for collective-processing-execution-links complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="collective-processing-execution-links"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="progress" type="{urn:org.nuclos.schema.rest}rest-link"/&gt;
 *         &lt;element name="abort" type="{urn:org.nuclos.schema.rest}rest-link"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "collective-processing-execution-links", propOrder = {
    "progress",
    "abort"
})
public class CollectiveProcessingExecutionLinks implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected RestLink progress;
    @XmlElement(required = true)
    protected RestLink abort;

    /**
     * Gets the value of the progress property.
     * 
     * @return
     *     possible object is
     *     {@link RestLink }
     *     
     */
    public RestLink getProgress() {
        return progress;
    }

    /**
     * Sets the value of the progress property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestLink }
     *     
     */
    public void setProgress(RestLink value) {
        this.progress = value;
    }

    /**
     * Gets the value of the abort property.
     * 
     * @return
     *     possible object is
     *     {@link RestLink }
     *     
     */
    public RestLink getAbort() {
        return abort;
    }

    /**
     * Sets the value of the abort property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestLink }
     *     
     */
    public void setAbort(RestLink value) {
        this.abort = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            RestLink theProgress;
            theProgress = this.getProgress();
            strategy.appendField(locator, this, "progress", buffer, theProgress);
        }
        {
            RestLink theAbort;
            theAbort = this.getAbort();
            strategy.appendField(locator, this, "abort", buffer, theAbort);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof CollectiveProcessingExecutionLinks)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final CollectiveProcessingExecutionLinks that = ((CollectiveProcessingExecutionLinks) object);
        {
            RestLink lhsProgress;
            lhsProgress = this.getProgress();
            RestLink rhsProgress;
            rhsProgress = that.getProgress();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "progress", lhsProgress), LocatorUtils.property(thatLocator, "progress", rhsProgress), lhsProgress, rhsProgress)) {
                return false;
            }
        }
        {
            RestLink lhsAbort;
            lhsAbort = this.getAbort();
            RestLink rhsAbort;
            rhsAbort = that.getAbort();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "abort", lhsAbort), LocatorUtils.property(thatLocator, "abort", rhsAbort), lhsAbort, rhsAbort)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            RestLink theProgress;
            theProgress = this.getProgress();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "progress", theProgress), currentHashCode, theProgress);
        }
        {
            RestLink theAbort;
            theAbort = this.getAbort();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "abort", theAbort), currentHashCode, theAbort);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof CollectiveProcessingExecutionLinks) {
            final CollectiveProcessingExecutionLinks copy = ((CollectiveProcessingExecutionLinks) draftCopy);
            if (this.progress!= null) {
                RestLink sourceProgress;
                sourceProgress = this.getProgress();
                RestLink copyProgress = ((RestLink) strategy.copy(LocatorUtils.property(locator, "progress", sourceProgress), sourceProgress));
                copy.setProgress(copyProgress);
            } else {
                copy.progress = null;
            }
            if (this.abort!= null) {
                RestLink sourceAbort;
                sourceAbort = this.getAbort();
                RestLink copyAbort = ((RestLink) strategy.copy(LocatorUtils.property(locator, "abort", sourceAbort), sourceAbort));
                copy.setAbort(copyAbort);
            } else {
                copy.abort = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new CollectiveProcessingExecutionLinks();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final CollectiveProcessingExecutionLinks.Builder<_B> _other) {
        _other.progress = ((this.progress == null)?null:this.progress.newCopyBuilder(_other));
        _other.abort = ((this.abort == null)?null:this.abort.newCopyBuilder(_other));
    }

    public<_B >CollectiveProcessingExecutionLinks.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new CollectiveProcessingExecutionLinks.Builder<_B>(_parentBuilder, this, true);
    }

    public CollectiveProcessingExecutionLinks.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static CollectiveProcessingExecutionLinks.Builder<Void> builder() {
        return new CollectiveProcessingExecutionLinks.Builder<Void>(null, null, false);
    }

    public static<_B >CollectiveProcessingExecutionLinks.Builder<_B> copyOf(final CollectiveProcessingExecutionLinks _other) {
        final CollectiveProcessingExecutionLinks.Builder<_B> _newBuilder = new CollectiveProcessingExecutionLinks.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final CollectiveProcessingExecutionLinks.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree progressPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("progress"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(progressPropertyTree!= null):((progressPropertyTree == null)||(!progressPropertyTree.isLeaf())))) {
            _other.progress = ((this.progress == null)?null:this.progress.newCopyBuilder(_other, progressPropertyTree, _propertyTreeUse));
        }
        final PropertyTree abortPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("abort"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(abortPropertyTree!= null):((abortPropertyTree == null)||(!abortPropertyTree.isLeaf())))) {
            _other.abort = ((this.abort == null)?null:this.abort.newCopyBuilder(_other, abortPropertyTree, _propertyTreeUse));
        }
    }

    public<_B >CollectiveProcessingExecutionLinks.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new CollectiveProcessingExecutionLinks.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public CollectiveProcessingExecutionLinks.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >CollectiveProcessingExecutionLinks.Builder<_B> copyOf(final CollectiveProcessingExecutionLinks _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final CollectiveProcessingExecutionLinks.Builder<_B> _newBuilder = new CollectiveProcessingExecutionLinks.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static CollectiveProcessingExecutionLinks.Builder<Void> copyExcept(final CollectiveProcessingExecutionLinks _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static CollectiveProcessingExecutionLinks.Builder<Void> copyOnly(final CollectiveProcessingExecutionLinks _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final CollectiveProcessingExecutionLinks _storedValue;
        private RestLink.Builder<CollectiveProcessingExecutionLinks.Builder<_B>> progress;
        private RestLink.Builder<CollectiveProcessingExecutionLinks.Builder<_B>> abort;

        public Builder(final _B _parentBuilder, final CollectiveProcessingExecutionLinks _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.progress = ((_other.progress == null)?null:_other.progress.newCopyBuilder(this));
                    this.abort = ((_other.abort == null)?null:_other.abort.newCopyBuilder(this));
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final CollectiveProcessingExecutionLinks _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree progressPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("progress"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(progressPropertyTree!= null):((progressPropertyTree == null)||(!progressPropertyTree.isLeaf())))) {
                        this.progress = ((_other.progress == null)?null:_other.progress.newCopyBuilder(this, progressPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree abortPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("abort"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(abortPropertyTree!= null):((abortPropertyTree == null)||(!abortPropertyTree.isLeaf())))) {
                        this.abort = ((_other.abort == null)?null:_other.abort.newCopyBuilder(this, abortPropertyTree, _propertyTreeUse));
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends CollectiveProcessingExecutionLinks >_P init(final _P _product) {
            _product.progress = ((this.progress == null)?null:this.progress.build());
            _product.abort = ((this.abort == null)?null:this.abort.build());
            return _product;
        }

        /**
         * Sets the new value of "progress" (any previous value will be replaced)
         * 
         * @param progress
         *     New value of the "progress" property.
         */
        public CollectiveProcessingExecutionLinks.Builder<_B> withProgress(final RestLink progress) {
            this.progress = ((progress == null)?null:new RestLink.Builder<CollectiveProcessingExecutionLinks.Builder<_B>>(this, progress, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "progress" property.
         * Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "progress" property.
         *     Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         */
        public RestLink.Builder<? extends CollectiveProcessingExecutionLinks.Builder<_B>> withProgress() {
            if (this.progress!= null) {
                return this.progress;
            }
            return this.progress = new RestLink.Builder<CollectiveProcessingExecutionLinks.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "abort" (any previous value will be replaced)
         * 
         * @param abort
         *     New value of the "abort" property.
         */
        public CollectiveProcessingExecutionLinks.Builder<_B> withAbort(final RestLink abort) {
            this.abort = ((abort == null)?null:new RestLink.Builder<CollectiveProcessingExecutionLinks.Builder<_B>>(this, abort, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "abort" property.
         * Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "abort" property.
         *     Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         */
        public RestLink.Builder<? extends CollectiveProcessingExecutionLinks.Builder<_B>> withAbort() {
            if (this.abort!= null) {
                return this.abort;
            }
            return this.abort = new RestLink.Builder<CollectiveProcessingExecutionLinks.Builder<_B>>(this, null, false);
        }

        @Override
        public CollectiveProcessingExecutionLinks build() {
            if (_storedValue == null) {
                return this.init(new CollectiveProcessingExecutionLinks());
            } else {
                return ((CollectiveProcessingExecutionLinks) _storedValue);
            }
        }

        public CollectiveProcessingExecutionLinks.Builder<_B> copyOf(final CollectiveProcessingExecutionLinks _other) {
            _other.copyTo(this);
            return this;
        }

        public CollectiveProcessingExecutionLinks.Builder<_B> copyOf(final CollectiveProcessingExecutionLinks.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends CollectiveProcessingExecutionLinks.Selector<CollectiveProcessingExecutionLinks.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static CollectiveProcessingExecutionLinks.Select _root() {
            return new CollectiveProcessingExecutionLinks.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private RestLink.Selector<TRoot, CollectiveProcessingExecutionLinks.Selector<TRoot, TParent>> progress = null;
        private RestLink.Selector<TRoot, CollectiveProcessingExecutionLinks.Selector<TRoot, TParent>> abort = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.progress!= null) {
                products.put("progress", this.progress.init());
            }
            if (this.abort!= null) {
                products.put("abort", this.abort.init());
            }
            return products;
        }

        public RestLink.Selector<TRoot, CollectiveProcessingExecutionLinks.Selector<TRoot, TParent>> progress() {
            return ((this.progress == null)?this.progress = new RestLink.Selector<TRoot, CollectiveProcessingExecutionLinks.Selector<TRoot, TParent>>(this._root, this, "progress"):this.progress);
        }

        public RestLink.Selector<TRoot, CollectiveProcessingExecutionLinks.Selector<TRoot, TParent>> abort() {
            return ((this.abort == null)?this.abort = new RestLink.Selector<TRoot, CollectiveProcessingExecutionLinks.Selector<TRoot, TParent>>(this._root, this, "abort"):this.abort);
        }

    }

}
