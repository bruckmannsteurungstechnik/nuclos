
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for collective-processing-object-info complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="collective-processing-object-info"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{urn:org.nuclos.schema.rest}object-id"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="infoRowNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}long" /&gt;
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="inProgress" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="result" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="resultMessage" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="boMetaId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "collective-processing-object-info", propOrder = {
    "id"
})
public class CollectiveProcessingObjectInfo implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected ObjectId id;
    @XmlAttribute(name = "infoRowNumber", required = true)
    protected long infoRowNumber;
    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "inProgress", required = true)
    protected boolean inProgress;
    @XmlAttribute(name = "result")
    protected String result;
    @XmlAttribute(name = "resultMessage")
    protected String resultMessage;
    @XmlAttribute(name = "boMetaId")
    protected String boMetaId;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectId }
     *     
     */
    public ObjectId getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectId }
     *     
     */
    public void setId(ObjectId value) {
        this.id = value;
    }

    /**
     * Gets the value of the infoRowNumber property.
     * 
     */
    public long getInfoRowNumber() {
        return infoRowNumber;
    }

    /**
     * Sets the value of the infoRowNumber property.
     * 
     */
    public void setInfoRowNumber(long value) {
        this.infoRowNumber = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the inProgress property.
     * 
     */
    public boolean isInProgress() {
        return inProgress;
    }

    /**
     * Sets the value of the inProgress property.
     * 
     */
    public void setInProgress(boolean value) {
        this.inProgress = value;
    }

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResult(String value) {
        this.result = value;
    }

    /**
     * Gets the value of the resultMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResultMessage() {
        return resultMessage;
    }

    /**
     * Sets the value of the resultMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResultMessage(String value) {
        this.resultMessage = value;
    }

    /**
     * Gets the value of the boMetaId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoMetaId() {
        return boMetaId;
    }

    /**
     * Sets the value of the boMetaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoMetaId(String value) {
        this.boMetaId = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            ObjectId theId;
            theId = this.getId();
            strategy.appendField(locator, this, "id", buffer, theId);
        }
        {
            long theInfoRowNumber;
            theInfoRowNumber = this.getInfoRowNumber();
            strategy.appendField(locator, this, "infoRowNumber", buffer, theInfoRowNumber);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            boolean theInProgress;
            theInProgress = this.isInProgress();
            strategy.appendField(locator, this, "inProgress", buffer, theInProgress);
        }
        {
            String theResult;
            theResult = this.getResult();
            strategy.appendField(locator, this, "result", buffer, theResult);
        }
        {
            String theResultMessage;
            theResultMessage = this.getResultMessage();
            strategy.appendField(locator, this, "resultMessage", buffer, theResultMessage);
        }
        {
            String theBoMetaId;
            theBoMetaId = this.getBoMetaId();
            strategy.appendField(locator, this, "boMetaId", buffer, theBoMetaId);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof CollectiveProcessingObjectInfo)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final CollectiveProcessingObjectInfo that = ((CollectiveProcessingObjectInfo) object);
        {
            ObjectId lhsId;
            lhsId = this.getId();
            ObjectId rhsId;
            rhsId = that.getId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "id", lhsId), LocatorUtils.property(thatLocator, "id", rhsId), lhsId, rhsId)) {
                return false;
            }
        }
        {
            long lhsInfoRowNumber;
            lhsInfoRowNumber = this.getInfoRowNumber();
            long rhsInfoRowNumber;
            rhsInfoRowNumber = that.getInfoRowNumber();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "infoRowNumber", lhsInfoRowNumber), LocatorUtils.property(thatLocator, "infoRowNumber", rhsInfoRowNumber), lhsInfoRowNumber, rhsInfoRowNumber)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            boolean lhsInProgress;
            lhsInProgress = this.isInProgress();
            boolean rhsInProgress;
            rhsInProgress = that.isInProgress();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "inProgress", lhsInProgress), LocatorUtils.property(thatLocator, "inProgress", rhsInProgress), lhsInProgress, rhsInProgress)) {
                return false;
            }
        }
        {
            String lhsResult;
            lhsResult = this.getResult();
            String rhsResult;
            rhsResult = that.getResult();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "result", lhsResult), LocatorUtils.property(thatLocator, "result", rhsResult), lhsResult, rhsResult)) {
                return false;
            }
        }
        {
            String lhsResultMessage;
            lhsResultMessage = this.getResultMessage();
            String rhsResultMessage;
            rhsResultMessage = that.getResultMessage();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "resultMessage", lhsResultMessage), LocatorUtils.property(thatLocator, "resultMessage", rhsResultMessage), lhsResultMessage, rhsResultMessage)) {
                return false;
            }
        }
        {
            String lhsBoMetaId;
            lhsBoMetaId = this.getBoMetaId();
            String rhsBoMetaId;
            rhsBoMetaId = that.getBoMetaId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "boMetaId", lhsBoMetaId), LocatorUtils.property(thatLocator, "boMetaId", rhsBoMetaId), lhsBoMetaId, rhsBoMetaId)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            ObjectId theId;
            theId = this.getId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "id", theId), currentHashCode, theId);
        }
        {
            long theInfoRowNumber;
            theInfoRowNumber = this.getInfoRowNumber();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "infoRowNumber", theInfoRowNumber), currentHashCode, theInfoRowNumber);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            boolean theInProgress;
            theInProgress = this.isInProgress();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "inProgress", theInProgress), currentHashCode, theInProgress);
        }
        {
            String theResult;
            theResult = this.getResult();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "result", theResult), currentHashCode, theResult);
        }
        {
            String theResultMessage;
            theResultMessage = this.getResultMessage();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "resultMessage", theResultMessage), currentHashCode, theResultMessage);
        }
        {
            String theBoMetaId;
            theBoMetaId = this.getBoMetaId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "boMetaId", theBoMetaId), currentHashCode, theBoMetaId);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof CollectiveProcessingObjectInfo) {
            final CollectiveProcessingObjectInfo copy = ((CollectiveProcessingObjectInfo) draftCopy);
            if (this.id!= null) {
                ObjectId sourceId;
                sourceId = this.getId();
                ObjectId copyId = ((ObjectId) strategy.copy(LocatorUtils.property(locator, "id", sourceId), sourceId));
                copy.setId(copyId);
            } else {
                copy.id = null;
            }
            {
                long sourceInfoRowNumber;
                sourceInfoRowNumber = this.getInfoRowNumber();
                long copyInfoRowNumber = strategy.copy(LocatorUtils.property(locator, "infoRowNumber", sourceInfoRowNumber), sourceInfoRowNumber);
                copy.setInfoRowNumber(copyInfoRowNumber);
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            {
                boolean sourceInProgress;
                sourceInProgress = this.isInProgress();
                boolean copyInProgress = strategy.copy(LocatorUtils.property(locator, "inProgress", sourceInProgress), sourceInProgress);
                copy.setInProgress(copyInProgress);
            }
            if (this.result!= null) {
                String sourceResult;
                sourceResult = this.getResult();
                String copyResult = ((String) strategy.copy(LocatorUtils.property(locator, "result", sourceResult), sourceResult));
                copy.setResult(copyResult);
            } else {
                copy.result = null;
            }
            if (this.resultMessage!= null) {
                String sourceResultMessage;
                sourceResultMessage = this.getResultMessage();
                String copyResultMessage = ((String) strategy.copy(LocatorUtils.property(locator, "resultMessage", sourceResultMessage), sourceResultMessage));
                copy.setResultMessage(copyResultMessage);
            } else {
                copy.resultMessage = null;
            }
            if (this.boMetaId!= null) {
                String sourceBoMetaId;
                sourceBoMetaId = this.getBoMetaId();
                String copyBoMetaId = ((String) strategy.copy(LocatorUtils.property(locator, "boMetaId", sourceBoMetaId), sourceBoMetaId));
                copy.setBoMetaId(copyBoMetaId);
            } else {
                copy.boMetaId = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new CollectiveProcessingObjectInfo();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final CollectiveProcessingObjectInfo.Builder<_B> _other) {
        _other.id = ((this.id == null)?null:this.id.newCopyBuilder(_other));
        _other.infoRowNumber = this.infoRowNumber;
        _other.name = this.name;
        _other.inProgress = this.inProgress;
        _other.result = this.result;
        _other.resultMessage = this.resultMessage;
        _other.boMetaId = this.boMetaId;
    }

    public<_B >CollectiveProcessingObjectInfo.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new CollectiveProcessingObjectInfo.Builder<_B>(_parentBuilder, this, true);
    }

    public CollectiveProcessingObjectInfo.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static CollectiveProcessingObjectInfo.Builder<Void> builder() {
        return new CollectiveProcessingObjectInfo.Builder<Void>(null, null, false);
    }

    public static<_B >CollectiveProcessingObjectInfo.Builder<_B> copyOf(final CollectiveProcessingObjectInfo _other) {
        final CollectiveProcessingObjectInfo.Builder<_B> _newBuilder = new CollectiveProcessingObjectInfo.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final CollectiveProcessingObjectInfo.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree idPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("id"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(idPropertyTree!= null):((idPropertyTree == null)||(!idPropertyTree.isLeaf())))) {
            _other.id = ((this.id == null)?null:this.id.newCopyBuilder(_other, idPropertyTree, _propertyTreeUse));
        }
        final PropertyTree infoRowNumberPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("infoRowNumber"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(infoRowNumberPropertyTree!= null):((infoRowNumberPropertyTree == null)||(!infoRowNumberPropertyTree.isLeaf())))) {
            _other.infoRowNumber = this.infoRowNumber;
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree inProgressPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("inProgress"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(inProgressPropertyTree!= null):((inProgressPropertyTree == null)||(!inProgressPropertyTree.isLeaf())))) {
            _other.inProgress = this.inProgress;
        }
        final PropertyTree resultPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("result"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resultPropertyTree!= null):((resultPropertyTree == null)||(!resultPropertyTree.isLeaf())))) {
            _other.result = this.result;
        }
        final PropertyTree resultMessagePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resultMessage"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resultMessagePropertyTree!= null):((resultMessagePropertyTree == null)||(!resultMessagePropertyTree.isLeaf())))) {
            _other.resultMessage = this.resultMessage;
        }
        final PropertyTree boMetaIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boMetaId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boMetaIdPropertyTree!= null):((boMetaIdPropertyTree == null)||(!boMetaIdPropertyTree.isLeaf())))) {
            _other.boMetaId = this.boMetaId;
        }
    }

    public<_B >CollectiveProcessingObjectInfo.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new CollectiveProcessingObjectInfo.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public CollectiveProcessingObjectInfo.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >CollectiveProcessingObjectInfo.Builder<_B> copyOf(final CollectiveProcessingObjectInfo _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final CollectiveProcessingObjectInfo.Builder<_B> _newBuilder = new CollectiveProcessingObjectInfo.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static CollectiveProcessingObjectInfo.Builder<Void> copyExcept(final CollectiveProcessingObjectInfo _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static CollectiveProcessingObjectInfo.Builder<Void> copyOnly(final CollectiveProcessingObjectInfo _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final CollectiveProcessingObjectInfo _storedValue;
        private ObjectId.Builder<CollectiveProcessingObjectInfo.Builder<_B>> id;
        private long infoRowNumber;
        private String name;
        private boolean inProgress;
        private String result;
        private String resultMessage;
        private String boMetaId;

        public Builder(final _B _parentBuilder, final CollectiveProcessingObjectInfo _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.id = ((_other.id == null)?null:_other.id.newCopyBuilder(this));
                    this.infoRowNumber = _other.infoRowNumber;
                    this.name = _other.name;
                    this.inProgress = _other.inProgress;
                    this.result = _other.result;
                    this.resultMessage = _other.resultMessage;
                    this.boMetaId = _other.boMetaId;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final CollectiveProcessingObjectInfo _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree idPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("id"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(idPropertyTree!= null):((idPropertyTree == null)||(!idPropertyTree.isLeaf())))) {
                        this.id = ((_other.id == null)?null:_other.id.newCopyBuilder(this, idPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree infoRowNumberPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("infoRowNumber"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(infoRowNumberPropertyTree!= null):((infoRowNumberPropertyTree == null)||(!infoRowNumberPropertyTree.isLeaf())))) {
                        this.infoRowNumber = _other.infoRowNumber;
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree inProgressPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("inProgress"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(inProgressPropertyTree!= null):((inProgressPropertyTree == null)||(!inProgressPropertyTree.isLeaf())))) {
                        this.inProgress = _other.inProgress;
                    }
                    final PropertyTree resultPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("result"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resultPropertyTree!= null):((resultPropertyTree == null)||(!resultPropertyTree.isLeaf())))) {
                        this.result = _other.result;
                    }
                    final PropertyTree resultMessagePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resultMessage"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resultMessagePropertyTree!= null):((resultMessagePropertyTree == null)||(!resultMessagePropertyTree.isLeaf())))) {
                        this.resultMessage = _other.resultMessage;
                    }
                    final PropertyTree boMetaIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boMetaId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boMetaIdPropertyTree!= null):((boMetaIdPropertyTree == null)||(!boMetaIdPropertyTree.isLeaf())))) {
                        this.boMetaId = _other.boMetaId;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends CollectiveProcessingObjectInfo >_P init(final _P _product) {
            _product.id = ((this.id == null)?null:this.id.build());
            _product.infoRowNumber = this.infoRowNumber;
            _product.name = this.name;
            _product.inProgress = this.inProgress;
            _product.result = this.result;
            _product.resultMessage = this.resultMessage;
            _product.boMetaId = this.boMetaId;
            return _product;
        }

        /**
         * Sets the new value of "id" (any previous value will be replaced)
         * 
         * @param id
         *     New value of the "id" property.
         */
        public CollectiveProcessingObjectInfo.Builder<_B> withId(final ObjectId id) {
            this.id = ((id == null)?null:new ObjectId.Builder<CollectiveProcessingObjectInfo.Builder<_B>>(this, id, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "id" property.
         * Use {@link org.nuclos.schema.rest.ObjectId.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "id" property.
         *     Use {@link org.nuclos.schema.rest.ObjectId.Builder#end()} to return to the current builder.
         */
        public ObjectId.Builder<? extends CollectiveProcessingObjectInfo.Builder<_B>> withId() {
            if (this.id!= null) {
                return this.id;
            }
            return this.id = new ObjectId.Builder<CollectiveProcessingObjectInfo.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "infoRowNumber" (any previous value will be replaced)
         * 
         * @param infoRowNumber
         *     New value of the "infoRowNumber" property.
         */
        public CollectiveProcessingObjectInfo.Builder<_B> withInfoRowNumber(final long infoRowNumber) {
            this.infoRowNumber = infoRowNumber;
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public CollectiveProcessingObjectInfo.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "inProgress" (any previous value will be replaced)
         * 
         * @param inProgress
         *     New value of the "inProgress" property.
         */
        public CollectiveProcessingObjectInfo.Builder<_B> withInProgress(final boolean inProgress) {
            this.inProgress = inProgress;
            return this;
        }

        /**
         * Sets the new value of "result" (any previous value will be replaced)
         * 
         * @param result
         *     New value of the "result" property.
         */
        public CollectiveProcessingObjectInfo.Builder<_B> withResult(final String result) {
            this.result = result;
            return this;
        }

        /**
         * Sets the new value of "resultMessage" (any previous value will be replaced)
         * 
         * @param resultMessage
         *     New value of the "resultMessage" property.
         */
        public CollectiveProcessingObjectInfo.Builder<_B> withResultMessage(final String resultMessage) {
            this.resultMessage = resultMessage;
            return this;
        }

        /**
         * Sets the new value of "boMetaId" (any previous value will be replaced)
         * 
         * @param boMetaId
         *     New value of the "boMetaId" property.
         */
        public CollectiveProcessingObjectInfo.Builder<_B> withBoMetaId(final String boMetaId) {
            this.boMetaId = boMetaId;
            return this;
        }

        @Override
        public CollectiveProcessingObjectInfo build() {
            if (_storedValue == null) {
                return this.init(new CollectiveProcessingObjectInfo());
            } else {
                return ((CollectiveProcessingObjectInfo) _storedValue);
            }
        }

        public CollectiveProcessingObjectInfo.Builder<_B> copyOf(final CollectiveProcessingObjectInfo _other) {
            _other.copyTo(this);
            return this;
        }

        public CollectiveProcessingObjectInfo.Builder<_B> copyOf(final CollectiveProcessingObjectInfo.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends CollectiveProcessingObjectInfo.Selector<CollectiveProcessingObjectInfo.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static CollectiveProcessingObjectInfo.Select _root() {
            return new CollectiveProcessingObjectInfo.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private ObjectId.Selector<TRoot, CollectiveProcessingObjectInfo.Selector<TRoot, TParent>> id = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectiveProcessingObjectInfo.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectiveProcessingObjectInfo.Selector<TRoot, TParent>> result = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectiveProcessingObjectInfo.Selector<TRoot, TParent>> resultMessage = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectiveProcessingObjectInfo.Selector<TRoot, TParent>> boMetaId = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.id!= null) {
                products.put("id", this.id.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.result!= null) {
                products.put("result", this.result.init());
            }
            if (this.resultMessage!= null) {
                products.put("resultMessage", this.resultMessage.init());
            }
            if (this.boMetaId!= null) {
                products.put("boMetaId", this.boMetaId.init());
            }
            return products;
        }

        public ObjectId.Selector<TRoot, CollectiveProcessingObjectInfo.Selector<TRoot, TParent>> id() {
            return ((this.id == null)?this.id = new ObjectId.Selector<TRoot, CollectiveProcessingObjectInfo.Selector<TRoot, TParent>>(this._root, this, "id"):this.id);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectiveProcessingObjectInfo.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, CollectiveProcessingObjectInfo.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectiveProcessingObjectInfo.Selector<TRoot, TParent>> result() {
            return ((this.result == null)?this.result = new com.kscs.util.jaxb.Selector<TRoot, CollectiveProcessingObjectInfo.Selector<TRoot, TParent>>(this._root, this, "result"):this.result);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectiveProcessingObjectInfo.Selector<TRoot, TParent>> resultMessage() {
            return ((this.resultMessage == null)?this.resultMessage = new com.kscs.util.jaxb.Selector<TRoot, CollectiveProcessingObjectInfo.Selector<TRoot, TParent>>(this._root, this, "resultMessage"):this.resultMessage);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectiveProcessingObjectInfo.Selector<TRoot, TParent>> boMetaId() {
            return ((this.boMetaId == null)?this.boMetaId = new com.kscs.util.jaxb.Selector<TRoot, CollectiveProcessingObjectInfo.Selector<TRoot, TParent>>(this._root, this, "boMetaId"):this.boMetaId);
        }

    }

}
