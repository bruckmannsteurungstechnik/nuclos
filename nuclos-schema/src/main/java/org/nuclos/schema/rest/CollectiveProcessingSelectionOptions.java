
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for collective-processing-selection-options complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="collective-processing-selection-options"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="stateChanges" type="{urn:org.nuclos.schema.rest}collective-processing-action" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "collective-processing-selection-options", propOrder = {
    "stateChanges"
})
public class CollectiveProcessingSelectionOptions implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected List<CollectiveProcessingAction> stateChanges;

    /**
     * Gets the value of the stateChanges property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the stateChanges property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStateChanges().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CollectiveProcessingAction }
     * 
     * 
     */
    public List<CollectiveProcessingAction> getStateChanges() {
        if (stateChanges == null) {
            stateChanges = new ArrayList<CollectiveProcessingAction>();
        }
        return this.stateChanges;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<CollectiveProcessingAction> theStateChanges;
            theStateChanges = (((this.stateChanges!= null)&&(!this.stateChanges.isEmpty()))?this.getStateChanges():null);
            strategy.appendField(locator, this, "stateChanges", buffer, theStateChanges);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof CollectiveProcessingSelectionOptions)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final CollectiveProcessingSelectionOptions that = ((CollectiveProcessingSelectionOptions) object);
        {
            List<CollectiveProcessingAction> lhsStateChanges;
            lhsStateChanges = (((this.stateChanges!= null)&&(!this.stateChanges.isEmpty()))?this.getStateChanges():null);
            List<CollectiveProcessingAction> rhsStateChanges;
            rhsStateChanges = (((that.stateChanges!= null)&&(!that.stateChanges.isEmpty()))?that.getStateChanges():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "stateChanges", lhsStateChanges), LocatorUtils.property(thatLocator, "stateChanges", rhsStateChanges), lhsStateChanges, rhsStateChanges)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<CollectiveProcessingAction> theStateChanges;
            theStateChanges = (((this.stateChanges!= null)&&(!this.stateChanges.isEmpty()))?this.getStateChanges():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "stateChanges", theStateChanges), currentHashCode, theStateChanges);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof CollectiveProcessingSelectionOptions) {
            final CollectiveProcessingSelectionOptions copy = ((CollectiveProcessingSelectionOptions) draftCopy);
            if ((this.stateChanges!= null)&&(!this.stateChanges.isEmpty())) {
                List<CollectiveProcessingAction> sourceStateChanges;
                sourceStateChanges = (((this.stateChanges!= null)&&(!this.stateChanges.isEmpty()))?this.getStateChanges():null);
                @SuppressWarnings("unchecked")
                List<CollectiveProcessingAction> copyStateChanges = ((List<CollectiveProcessingAction> ) strategy.copy(LocatorUtils.property(locator, "stateChanges", sourceStateChanges), sourceStateChanges));
                copy.stateChanges = null;
                if (copyStateChanges!= null) {
                    List<CollectiveProcessingAction> uniqueStateChangesl = copy.getStateChanges();
                    uniqueStateChangesl.addAll(copyStateChanges);
                }
            } else {
                copy.stateChanges = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new CollectiveProcessingSelectionOptions();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final CollectiveProcessingSelectionOptions.Builder<_B> _other) {
        if (this.stateChanges == null) {
            _other.stateChanges = null;
        } else {
            _other.stateChanges = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
            for (CollectiveProcessingAction _item: this.stateChanges) {
                _other.stateChanges.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
    }

    public<_B >CollectiveProcessingSelectionOptions.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new CollectiveProcessingSelectionOptions.Builder<_B>(_parentBuilder, this, true);
    }

    public CollectiveProcessingSelectionOptions.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static CollectiveProcessingSelectionOptions.Builder<Void> builder() {
        return new CollectiveProcessingSelectionOptions.Builder<Void>(null, null, false);
    }

    public static<_B >CollectiveProcessingSelectionOptions.Builder<_B> copyOf(final CollectiveProcessingSelectionOptions _other) {
        final CollectiveProcessingSelectionOptions.Builder<_B> _newBuilder = new CollectiveProcessingSelectionOptions.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final CollectiveProcessingSelectionOptions.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree stateChangesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("stateChanges"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(stateChangesPropertyTree!= null):((stateChangesPropertyTree == null)||(!stateChangesPropertyTree.isLeaf())))) {
            if (this.stateChanges == null) {
                _other.stateChanges = null;
            } else {
                _other.stateChanges = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
                for (CollectiveProcessingAction _item: this.stateChanges) {
                    _other.stateChanges.add(((_item == null)?null:_item.newCopyBuilder(_other, stateChangesPropertyTree, _propertyTreeUse)));
                }
            }
        }
    }

    public<_B >CollectiveProcessingSelectionOptions.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new CollectiveProcessingSelectionOptions.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public CollectiveProcessingSelectionOptions.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >CollectiveProcessingSelectionOptions.Builder<_B> copyOf(final CollectiveProcessingSelectionOptions _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final CollectiveProcessingSelectionOptions.Builder<_B> _newBuilder = new CollectiveProcessingSelectionOptions.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static CollectiveProcessingSelectionOptions.Builder<Void> copyExcept(final CollectiveProcessingSelectionOptions _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static CollectiveProcessingSelectionOptions.Builder<Void> copyOnly(final CollectiveProcessingSelectionOptions _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final CollectiveProcessingSelectionOptions _storedValue;
        private List<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>> stateChanges;

        public Builder(final _B _parentBuilder, final CollectiveProcessingSelectionOptions _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.stateChanges == null) {
                        this.stateChanges = null;
                    } else {
                        this.stateChanges = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
                        for (CollectiveProcessingAction _item: _other.stateChanges) {
                            this.stateChanges.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final CollectiveProcessingSelectionOptions _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree stateChangesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("stateChanges"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(stateChangesPropertyTree!= null):((stateChangesPropertyTree == null)||(!stateChangesPropertyTree.isLeaf())))) {
                        if (_other.stateChanges == null) {
                            this.stateChanges = null;
                        } else {
                            this.stateChanges = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
                            for (CollectiveProcessingAction _item: _other.stateChanges) {
                                this.stateChanges.add(((_item == null)?null:_item.newCopyBuilder(this, stateChangesPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends CollectiveProcessingSelectionOptions >_P init(final _P _product) {
            if (this.stateChanges!= null) {
                final List<CollectiveProcessingAction> stateChanges = new ArrayList<CollectiveProcessingAction>(this.stateChanges.size());
                for (CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>> _item: this.stateChanges) {
                    stateChanges.add(_item.build());
                }
                _product.stateChanges = stateChanges;
            }
            return _product;
        }

        /**
         * Adds the given items to the value of "stateChanges"
         * 
         * @param stateChanges
         *     Items to add to the value of the "stateChanges" property
         */
        public CollectiveProcessingSelectionOptions.Builder<_B> addStateChanges(final Iterable<? extends CollectiveProcessingAction> stateChanges) {
            if (stateChanges!= null) {
                if (this.stateChanges == null) {
                    this.stateChanges = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
                }
                for (CollectiveProcessingAction _item: stateChanges) {
                    this.stateChanges.add(new CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "stateChanges" (any previous value will be replaced)
         * 
         * @param stateChanges
         *     New value of the "stateChanges" property.
         */
        public CollectiveProcessingSelectionOptions.Builder<_B> withStateChanges(final Iterable<? extends CollectiveProcessingAction> stateChanges) {
            if (this.stateChanges!= null) {
                this.stateChanges.clear();
            }
            return addStateChanges(stateChanges);
        }

        /**
         * Adds the given items to the value of "stateChanges"
         * 
         * @param stateChanges
         *     Items to add to the value of the "stateChanges" property
         */
        public CollectiveProcessingSelectionOptions.Builder<_B> addStateChanges(CollectiveProcessingAction... stateChanges) {
            addStateChanges(Arrays.asList(stateChanges));
            return this;
        }

        /**
         * Sets the new value of "stateChanges" (any previous value will be replaced)
         * 
         * @param stateChanges
         *     New value of the "stateChanges" property.
         */
        public CollectiveProcessingSelectionOptions.Builder<_B> withStateChanges(CollectiveProcessingAction... stateChanges) {
            withStateChanges(Arrays.asList(stateChanges));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "StateChanges" property.
         * Use {@link org.nuclos.schema.rest.CollectiveProcessingAction.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "StateChanges" property.
         *     Use {@link org.nuclos.schema.rest.CollectiveProcessingAction.Builder#end()} to return to the current builder.
         */
        public CollectiveProcessingAction.Builder<? extends CollectiveProcessingSelectionOptions.Builder<_B>> addStateChanges() {
            if (this.stateChanges == null) {
                this.stateChanges = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
            }
            final CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>> stateChanges_Builder = new CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>(this, null, false);
            this.stateChanges.add(stateChanges_Builder);
            return stateChanges_Builder;
        }

        @Override
        public CollectiveProcessingSelectionOptions build() {
            if (_storedValue == null) {
                return this.init(new CollectiveProcessingSelectionOptions());
            } else {
                return ((CollectiveProcessingSelectionOptions) _storedValue);
            }
        }

        public CollectiveProcessingSelectionOptions.Builder<_B> copyOf(final CollectiveProcessingSelectionOptions _other) {
            _other.copyTo(this);
            return this;
        }

        public CollectiveProcessingSelectionOptions.Builder<_B> copyOf(final CollectiveProcessingSelectionOptions.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends CollectiveProcessingSelectionOptions.Selector<CollectiveProcessingSelectionOptions.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static CollectiveProcessingSelectionOptions.Select _root() {
            return new CollectiveProcessingSelectionOptions.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private CollectiveProcessingAction.Selector<TRoot, CollectiveProcessingSelectionOptions.Selector<TRoot, TParent>> stateChanges = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.stateChanges!= null) {
                products.put("stateChanges", this.stateChanges.init());
            }
            return products;
        }

        public CollectiveProcessingAction.Selector<TRoot, CollectiveProcessingSelectionOptions.Selector<TRoot, TParent>> stateChanges() {
            return ((this.stateChanges == null)?this.stateChanges = new CollectiveProcessingAction.Selector<TRoot, CollectiveProcessingSelectionOptions.Selector<TRoot, TParent>>(this._root, this, "stateChanges"):this.stateChanges);
        }

    }

}
