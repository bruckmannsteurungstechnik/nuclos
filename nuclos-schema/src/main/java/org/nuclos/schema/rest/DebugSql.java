
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for debug-sql complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="debug-sql"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="debugSQL" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="minExecTime" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "debug-sql")
public class DebugSql implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "debugSQL")
    protected String debugSQL;
    @XmlAttribute(name = "minExecTime")
    protected String minExecTime;

    /**
     * Gets the value of the debugSQL property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebugSQL() {
        return debugSQL;
    }

    /**
     * Sets the value of the debugSQL property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebugSQL(String value) {
        this.debugSQL = value;
    }

    /**
     * Gets the value of the minExecTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMinExecTime() {
        return minExecTime;
    }

    /**
     * Sets the value of the minExecTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMinExecTime(String value) {
        this.minExecTime = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theDebugSQL;
            theDebugSQL = this.getDebugSQL();
            strategy.appendField(locator, this, "debugSQL", buffer, theDebugSQL);
        }
        {
            String theMinExecTime;
            theMinExecTime = this.getMinExecTime();
            strategy.appendField(locator, this, "minExecTime", buffer, theMinExecTime);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof DebugSql)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final DebugSql that = ((DebugSql) object);
        {
            String lhsDebugSQL;
            lhsDebugSQL = this.getDebugSQL();
            String rhsDebugSQL;
            rhsDebugSQL = that.getDebugSQL();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "debugSQL", lhsDebugSQL), LocatorUtils.property(thatLocator, "debugSQL", rhsDebugSQL), lhsDebugSQL, rhsDebugSQL)) {
                return false;
            }
        }
        {
            String lhsMinExecTime;
            lhsMinExecTime = this.getMinExecTime();
            String rhsMinExecTime;
            rhsMinExecTime = that.getMinExecTime();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "minExecTime", lhsMinExecTime), LocatorUtils.property(thatLocator, "minExecTime", rhsMinExecTime), lhsMinExecTime, rhsMinExecTime)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theDebugSQL;
            theDebugSQL = this.getDebugSQL();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "debugSQL", theDebugSQL), currentHashCode, theDebugSQL);
        }
        {
            String theMinExecTime;
            theMinExecTime = this.getMinExecTime();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "minExecTime", theMinExecTime), currentHashCode, theMinExecTime);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof DebugSql) {
            final DebugSql copy = ((DebugSql) draftCopy);
            if (this.debugSQL!= null) {
                String sourceDebugSQL;
                sourceDebugSQL = this.getDebugSQL();
                String copyDebugSQL = ((String) strategy.copy(LocatorUtils.property(locator, "debugSQL", sourceDebugSQL), sourceDebugSQL));
                copy.setDebugSQL(copyDebugSQL);
            } else {
                copy.debugSQL = null;
            }
            if (this.minExecTime!= null) {
                String sourceMinExecTime;
                sourceMinExecTime = this.getMinExecTime();
                String copyMinExecTime = ((String) strategy.copy(LocatorUtils.property(locator, "minExecTime", sourceMinExecTime), sourceMinExecTime));
                copy.setMinExecTime(copyMinExecTime);
            } else {
                copy.minExecTime = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new DebugSql();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final DebugSql.Builder<_B> _other) {
        _other.debugSQL = this.debugSQL;
        _other.minExecTime = this.minExecTime;
    }

    public<_B >DebugSql.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new DebugSql.Builder<_B>(_parentBuilder, this, true);
    }

    public DebugSql.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static DebugSql.Builder<Void> builder() {
        return new DebugSql.Builder<Void>(null, null, false);
    }

    public static<_B >DebugSql.Builder<_B> copyOf(final DebugSql _other) {
        final DebugSql.Builder<_B> _newBuilder = new DebugSql.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final DebugSql.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree debugSQLPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("debugSQL"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(debugSQLPropertyTree!= null):((debugSQLPropertyTree == null)||(!debugSQLPropertyTree.isLeaf())))) {
            _other.debugSQL = this.debugSQL;
        }
        final PropertyTree minExecTimePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minExecTime"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minExecTimePropertyTree!= null):((minExecTimePropertyTree == null)||(!minExecTimePropertyTree.isLeaf())))) {
            _other.minExecTime = this.minExecTime;
        }
    }

    public<_B >DebugSql.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new DebugSql.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public DebugSql.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >DebugSql.Builder<_B> copyOf(final DebugSql _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final DebugSql.Builder<_B> _newBuilder = new DebugSql.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static DebugSql.Builder<Void> copyExcept(final DebugSql _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static DebugSql.Builder<Void> copyOnly(final DebugSql _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final DebugSql _storedValue;
        private String debugSQL;
        private String minExecTime;

        public Builder(final _B _parentBuilder, final DebugSql _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.debugSQL = _other.debugSQL;
                    this.minExecTime = _other.minExecTime;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final DebugSql _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree debugSQLPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("debugSQL"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(debugSQLPropertyTree!= null):((debugSQLPropertyTree == null)||(!debugSQLPropertyTree.isLeaf())))) {
                        this.debugSQL = _other.debugSQL;
                    }
                    final PropertyTree minExecTimePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minExecTime"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minExecTimePropertyTree!= null):((minExecTimePropertyTree == null)||(!minExecTimePropertyTree.isLeaf())))) {
                        this.minExecTime = _other.minExecTime;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends DebugSql >_P init(final _P _product) {
            _product.debugSQL = this.debugSQL;
            _product.minExecTime = this.minExecTime;
            return _product;
        }

        /**
         * Sets the new value of "debugSQL" (any previous value will be replaced)
         * 
         * @param debugSQL
         *     New value of the "debugSQL" property.
         */
        public DebugSql.Builder<_B> withDebugSQL(final String debugSQL) {
            this.debugSQL = debugSQL;
            return this;
        }

        /**
         * Sets the new value of "minExecTime" (any previous value will be replaced)
         * 
         * @param minExecTime
         *     New value of the "minExecTime" property.
         */
        public DebugSql.Builder<_B> withMinExecTime(final String minExecTime) {
            this.minExecTime = minExecTime;
            return this;
        }

        @Override
        public DebugSql build() {
            if (_storedValue == null) {
                return this.init(new DebugSql());
            } else {
                return ((DebugSql) _storedValue);
            }
        }

        public DebugSql.Builder<_B> copyOf(final DebugSql _other) {
            _other.copyTo(this);
            return this;
        }

        public DebugSql.Builder<_B> copyOf(final DebugSql.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends DebugSql.Selector<DebugSql.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static DebugSql.Select _root() {
            return new DebugSql.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, DebugSql.Selector<TRoot, TParent>> debugSQL = null;
        private com.kscs.util.jaxb.Selector<TRoot, DebugSql.Selector<TRoot, TParent>> minExecTime = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.debugSQL!= null) {
                products.put("debugSQL", this.debugSQL.init());
            }
            if (this.minExecTime!= null) {
                products.put("minExecTime", this.minExecTime.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, DebugSql.Selector<TRoot, TParent>> debugSQL() {
            return ((this.debugSQL == null)?this.debugSQL = new com.kscs.util.jaxb.Selector<TRoot, DebugSql.Selector<TRoot, TParent>>(this._root, this, "debugSQL"):this.debugSQL);
        }

        public com.kscs.util.jaxb.Selector<TRoot, DebugSql.Selector<TRoot, TParent>> minExecTime() {
            return ((this.minExecTime == null)?this.minExecTime = new com.kscs.util.jaxb.Selector<TRoot, DebugSql.Selector<TRoot, TParent>>(this._root, this, "minExecTime"):this.minExecTime);
        }

    }

}
