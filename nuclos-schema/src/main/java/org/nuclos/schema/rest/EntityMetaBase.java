
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for entity-meta-base complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="entity-meta-base"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="links" type="{urn:org.nuclos.schema.rest}entity-meta-base-links"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="boMetaId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="createNew" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "entity-meta-base", propOrder = {
    "links"
})
@XmlSeeAlso({
    EntityMetaOverview.class
})
public class EntityMetaBase implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected EntityMetaBaseLinks links;
    @XmlAttribute(name = "boMetaId", required = true)
    protected String boMetaId;
    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "createNew", required = true)
    protected boolean createNew;

    /**
     * Gets the value of the links property.
     * 
     * @return
     *     possible object is
     *     {@link EntityMetaBaseLinks }
     *     
     */
    public EntityMetaBaseLinks getLinks() {
        return links;
    }

    /**
     * Sets the value of the links property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntityMetaBaseLinks }
     *     
     */
    public void setLinks(EntityMetaBaseLinks value) {
        this.links = value;
    }

    /**
     * Gets the value of the boMetaId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoMetaId() {
        return boMetaId;
    }

    /**
     * Sets the value of the boMetaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoMetaId(String value) {
        this.boMetaId = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the createNew property.
     * 
     */
    public boolean isCreateNew() {
        return createNew;
    }

    /**
     * Sets the value of the createNew property.
     * 
     */
    public void setCreateNew(boolean value) {
        this.createNew = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            EntityMetaBaseLinks theLinks;
            theLinks = this.getLinks();
            strategy.appendField(locator, this, "links", buffer, theLinks);
        }
        {
            String theBoMetaId;
            theBoMetaId = this.getBoMetaId();
            strategy.appendField(locator, this, "boMetaId", buffer, theBoMetaId);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            boolean theCreateNew;
            theCreateNew = this.isCreateNew();
            strategy.appendField(locator, this, "createNew", buffer, theCreateNew);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof EntityMetaBase)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final EntityMetaBase that = ((EntityMetaBase) object);
        {
            EntityMetaBaseLinks lhsLinks;
            lhsLinks = this.getLinks();
            EntityMetaBaseLinks rhsLinks;
            rhsLinks = that.getLinks();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "links", lhsLinks), LocatorUtils.property(thatLocator, "links", rhsLinks), lhsLinks, rhsLinks)) {
                return false;
            }
        }
        {
            String lhsBoMetaId;
            lhsBoMetaId = this.getBoMetaId();
            String rhsBoMetaId;
            rhsBoMetaId = that.getBoMetaId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "boMetaId", lhsBoMetaId), LocatorUtils.property(thatLocator, "boMetaId", rhsBoMetaId), lhsBoMetaId, rhsBoMetaId)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            boolean lhsCreateNew;
            lhsCreateNew = this.isCreateNew();
            boolean rhsCreateNew;
            rhsCreateNew = that.isCreateNew();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "createNew", lhsCreateNew), LocatorUtils.property(thatLocator, "createNew", rhsCreateNew), lhsCreateNew, rhsCreateNew)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            EntityMetaBaseLinks theLinks;
            theLinks = this.getLinks();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "links", theLinks), currentHashCode, theLinks);
        }
        {
            String theBoMetaId;
            theBoMetaId = this.getBoMetaId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "boMetaId", theBoMetaId), currentHashCode, theBoMetaId);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            boolean theCreateNew;
            theCreateNew = this.isCreateNew();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "createNew", theCreateNew), currentHashCode, theCreateNew);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof EntityMetaBase) {
            final EntityMetaBase copy = ((EntityMetaBase) draftCopy);
            if (this.links!= null) {
                EntityMetaBaseLinks sourceLinks;
                sourceLinks = this.getLinks();
                EntityMetaBaseLinks copyLinks = ((EntityMetaBaseLinks) strategy.copy(LocatorUtils.property(locator, "links", sourceLinks), sourceLinks));
                copy.setLinks(copyLinks);
            } else {
                copy.links = null;
            }
            if (this.boMetaId!= null) {
                String sourceBoMetaId;
                sourceBoMetaId = this.getBoMetaId();
                String copyBoMetaId = ((String) strategy.copy(LocatorUtils.property(locator, "boMetaId", sourceBoMetaId), sourceBoMetaId));
                copy.setBoMetaId(copyBoMetaId);
            } else {
                copy.boMetaId = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            {
                boolean sourceCreateNew;
                sourceCreateNew = this.isCreateNew();
                boolean copyCreateNew = strategy.copy(LocatorUtils.property(locator, "createNew", sourceCreateNew), sourceCreateNew);
                copy.setCreateNew(copyCreateNew);
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new EntityMetaBase();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityMetaBase.Builder<_B> _other) {
        _other.links = ((this.links == null)?null:this.links.newCopyBuilder(_other));
        _other.boMetaId = this.boMetaId;
        _other.name = this.name;
        _other.createNew = this.createNew;
    }

    public<_B >EntityMetaBase.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new EntityMetaBase.Builder<_B>(_parentBuilder, this, true);
    }

    public EntityMetaBase.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static EntityMetaBase.Builder<Void> builder() {
        return new EntityMetaBase.Builder<Void>(null, null, false);
    }

    public static<_B >EntityMetaBase.Builder<_B> copyOf(final EntityMetaBase _other) {
        final EntityMetaBase.Builder<_B> _newBuilder = new EntityMetaBase.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityMetaBase.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree linksPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("links"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(linksPropertyTree!= null):((linksPropertyTree == null)||(!linksPropertyTree.isLeaf())))) {
            _other.links = ((this.links == null)?null:this.links.newCopyBuilder(_other, linksPropertyTree, _propertyTreeUse));
        }
        final PropertyTree boMetaIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boMetaId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boMetaIdPropertyTree!= null):((boMetaIdPropertyTree == null)||(!boMetaIdPropertyTree.isLeaf())))) {
            _other.boMetaId = this.boMetaId;
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree createNewPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("createNew"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(createNewPropertyTree!= null):((createNewPropertyTree == null)||(!createNewPropertyTree.isLeaf())))) {
            _other.createNew = this.createNew;
        }
    }

    public<_B >EntityMetaBase.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new EntityMetaBase.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public EntityMetaBase.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >EntityMetaBase.Builder<_B> copyOf(final EntityMetaBase _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final EntityMetaBase.Builder<_B> _newBuilder = new EntityMetaBase.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static EntityMetaBase.Builder<Void> copyExcept(final EntityMetaBase _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static EntityMetaBase.Builder<Void> copyOnly(final EntityMetaBase _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final EntityMetaBase _storedValue;
        private EntityMetaBaseLinks.Builder<EntityMetaBase.Builder<_B>> links;
        private String boMetaId;
        private String name;
        private boolean createNew;

        public Builder(final _B _parentBuilder, final EntityMetaBase _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.links = ((_other.links == null)?null:_other.links.newCopyBuilder(this));
                    this.boMetaId = _other.boMetaId;
                    this.name = _other.name;
                    this.createNew = _other.createNew;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final EntityMetaBase _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree linksPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("links"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(linksPropertyTree!= null):((linksPropertyTree == null)||(!linksPropertyTree.isLeaf())))) {
                        this.links = ((_other.links == null)?null:_other.links.newCopyBuilder(this, linksPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree boMetaIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boMetaId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boMetaIdPropertyTree!= null):((boMetaIdPropertyTree == null)||(!boMetaIdPropertyTree.isLeaf())))) {
                        this.boMetaId = _other.boMetaId;
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree createNewPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("createNew"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(createNewPropertyTree!= null):((createNewPropertyTree == null)||(!createNewPropertyTree.isLeaf())))) {
                        this.createNew = _other.createNew;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends EntityMetaBase >_P init(final _P _product) {
            _product.links = ((this.links == null)?null:this.links.build());
            _product.boMetaId = this.boMetaId;
            _product.name = this.name;
            _product.createNew = this.createNew;
            return _product;
        }

        /**
         * Sets the new value of "links" (any previous value will be replaced)
         * 
         * @param links
         *     New value of the "links" property.
         */
        public EntityMetaBase.Builder<_B> withLinks(final EntityMetaBaseLinks links) {
            this.links = ((links == null)?null:new EntityMetaBaseLinks.Builder<EntityMetaBase.Builder<_B>>(this, links, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "links" property.
         * Use {@link org.nuclos.schema.rest.EntityMetaBaseLinks.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "links" property.
         *     Use {@link org.nuclos.schema.rest.EntityMetaBaseLinks.Builder#end()} to return to the current builder.
         */
        public EntityMetaBaseLinks.Builder<? extends EntityMetaBase.Builder<_B>> withLinks() {
            if (this.links!= null) {
                return this.links;
            }
            return this.links = new EntityMetaBaseLinks.Builder<EntityMetaBase.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "boMetaId" (any previous value will be replaced)
         * 
         * @param boMetaId
         *     New value of the "boMetaId" property.
         */
        public EntityMetaBase.Builder<_B> withBoMetaId(final String boMetaId) {
            this.boMetaId = boMetaId;
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public EntityMetaBase.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "createNew" (any previous value will be replaced)
         * 
         * @param createNew
         *     New value of the "createNew" property.
         */
        public EntityMetaBase.Builder<_B> withCreateNew(final boolean createNew) {
            this.createNew = createNew;
            return this;
        }

        @Override
        public EntityMetaBase build() {
            if (_storedValue == null) {
                return this.init(new EntityMetaBase());
            } else {
                return ((EntityMetaBase) _storedValue);
            }
        }

        public EntityMetaBase.Builder<_B> copyOf(final EntityMetaBase _other) {
            _other.copyTo(this);
            return this;
        }

        public EntityMetaBase.Builder<_B> copyOf(final EntityMetaBase.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends EntityMetaBase.Selector<EntityMetaBase.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static EntityMetaBase.Select _root() {
            return new EntityMetaBase.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private EntityMetaBaseLinks.Selector<TRoot, EntityMetaBase.Selector<TRoot, TParent>> links = null;
        private com.kscs.util.jaxb.Selector<TRoot, EntityMetaBase.Selector<TRoot, TParent>> boMetaId = null;
        private com.kscs.util.jaxb.Selector<TRoot, EntityMetaBase.Selector<TRoot, TParent>> name = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.links!= null) {
                products.put("links", this.links.init());
            }
            if (this.boMetaId!= null) {
                products.put("boMetaId", this.boMetaId.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            return products;
        }

        public EntityMetaBaseLinks.Selector<TRoot, EntityMetaBase.Selector<TRoot, TParent>> links() {
            return ((this.links == null)?this.links = new EntityMetaBaseLinks.Selector<TRoot, EntityMetaBase.Selector<TRoot, TParent>>(this._root, this, "links"):this.links);
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityMetaBase.Selector<TRoot, TParent>> boMetaId() {
            return ((this.boMetaId == null)?this.boMetaId = new com.kscs.util.jaxb.Selector<TRoot, EntityMetaBase.Selector<TRoot, TParent>>(this._root, this, "boMetaId"):this.boMetaId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityMetaBase.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, EntityMetaBase.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

    }

}
