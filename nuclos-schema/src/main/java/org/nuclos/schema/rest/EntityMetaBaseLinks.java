
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for entity-meta-base-links complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="entity-meta-base-links"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="boMeta" type="{urn:org.nuclos.schema.rest}rest-link"/&gt;
 *         &lt;element name="bos" type="{urn:org.nuclos.schema.rest}rest-link" minOccurs="0"/&gt;
 *         &lt;element name="resourceicon" type="{urn:org.nuclos.schema.rest}rest-link" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "entity-meta-base-links", propOrder = {
    "boMeta",
    "bos",
    "resourceicon"
})
public class EntityMetaBaseLinks implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected RestLink boMeta;
    protected RestLink bos;
    protected RestLink resourceicon;

    /**
     * Gets the value of the boMeta property.
     * 
     * @return
     *     possible object is
     *     {@link RestLink }
     *     
     */
    public RestLink getBoMeta() {
        return boMeta;
    }

    /**
     * Sets the value of the boMeta property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestLink }
     *     
     */
    public void setBoMeta(RestLink value) {
        this.boMeta = value;
    }

    /**
     * Gets the value of the bos property.
     * 
     * @return
     *     possible object is
     *     {@link RestLink }
     *     
     */
    public RestLink getBos() {
        return bos;
    }

    /**
     * Sets the value of the bos property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestLink }
     *     
     */
    public void setBos(RestLink value) {
        this.bos = value;
    }

    /**
     * Gets the value of the resourceicon property.
     * 
     * @return
     *     possible object is
     *     {@link RestLink }
     *     
     */
    public RestLink getResourceicon() {
        return resourceicon;
    }

    /**
     * Sets the value of the resourceicon property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestLink }
     *     
     */
    public void setResourceicon(RestLink value) {
        this.resourceicon = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            RestLink theBoMeta;
            theBoMeta = this.getBoMeta();
            strategy.appendField(locator, this, "boMeta", buffer, theBoMeta);
        }
        {
            RestLink theBos;
            theBos = this.getBos();
            strategy.appendField(locator, this, "bos", buffer, theBos);
        }
        {
            RestLink theResourceicon;
            theResourceicon = this.getResourceicon();
            strategy.appendField(locator, this, "resourceicon", buffer, theResourceicon);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof EntityMetaBaseLinks)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final EntityMetaBaseLinks that = ((EntityMetaBaseLinks) object);
        {
            RestLink lhsBoMeta;
            lhsBoMeta = this.getBoMeta();
            RestLink rhsBoMeta;
            rhsBoMeta = that.getBoMeta();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "boMeta", lhsBoMeta), LocatorUtils.property(thatLocator, "boMeta", rhsBoMeta), lhsBoMeta, rhsBoMeta)) {
                return false;
            }
        }
        {
            RestLink lhsBos;
            lhsBos = this.getBos();
            RestLink rhsBos;
            rhsBos = that.getBos();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "bos", lhsBos), LocatorUtils.property(thatLocator, "bos", rhsBos), lhsBos, rhsBos)) {
                return false;
            }
        }
        {
            RestLink lhsResourceicon;
            lhsResourceicon = this.getResourceicon();
            RestLink rhsResourceicon;
            rhsResourceicon = that.getResourceicon();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "resourceicon", lhsResourceicon), LocatorUtils.property(thatLocator, "resourceicon", rhsResourceicon), lhsResourceicon, rhsResourceicon)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            RestLink theBoMeta;
            theBoMeta = this.getBoMeta();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "boMeta", theBoMeta), currentHashCode, theBoMeta);
        }
        {
            RestLink theBos;
            theBos = this.getBos();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "bos", theBos), currentHashCode, theBos);
        }
        {
            RestLink theResourceicon;
            theResourceicon = this.getResourceicon();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "resourceicon", theResourceicon), currentHashCode, theResourceicon);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof EntityMetaBaseLinks) {
            final EntityMetaBaseLinks copy = ((EntityMetaBaseLinks) draftCopy);
            if (this.boMeta!= null) {
                RestLink sourceBoMeta;
                sourceBoMeta = this.getBoMeta();
                RestLink copyBoMeta = ((RestLink) strategy.copy(LocatorUtils.property(locator, "boMeta", sourceBoMeta), sourceBoMeta));
                copy.setBoMeta(copyBoMeta);
            } else {
                copy.boMeta = null;
            }
            if (this.bos!= null) {
                RestLink sourceBos;
                sourceBos = this.getBos();
                RestLink copyBos = ((RestLink) strategy.copy(LocatorUtils.property(locator, "bos", sourceBos), sourceBos));
                copy.setBos(copyBos);
            } else {
                copy.bos = null;
            }
            if (this.resourceicon!= null) {
                RestLink sourceResourceicon;
                sourceResourceicon = this.getResourceicon();
                RestLink copyResourceicon = ((RestLink) strategy.copy(LocatorUtils.property(locator, "resourceicon", sourceResourceicon), sourceResourceicon));
                copy.setResourceicon(copyResourceicon);
            } else {
                copy.resourceicon = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new EntityMetaBaseLinks();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityMetaBaseLinks.Builder<_B> _other) {
        _other.boMeta = ((this.boMeta == null)?null:this.boMeta.newCopyBuilder(_other));
        _other.bos = ((this.bos == null)?null:this.bos.newCopyBuilder(_other));
        _other.resourceicon = ((this.resourceicon == null)?null:this.resourceicon.newCopyBuilder(_other));
    }

    public<_B >EntityMetaBaseLinks.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new EntityMetaBaseLinks.Builder<_B>(_parentBuilder, this, true);
    }

    public EntityMetaBaseLinks.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static EntityMetaBaseLinks.Builder<Void> builder() {
        return new EntityMetaBaseLinks.Builder<Void>(null, null, false);
    }

    public static<_B >EntityMetaBaseLinks.Builder<_B> copyOf(final EntityMetaBaseLinks _other) {
        final EntityMetaBaseLinks.Builder<_B> _newBuilder = new EntityMetaBaseLinks.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityMetaBaseLinks.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree boMetaPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boMeta"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boMetaPropertyTree!= null):((boMetaPropertyTree == null)||(!boMetaPropertyTree.isLeaf())))) {
            _other.boMeta = ((this.boMeta == null)?null:this.boMeta.newCopyBuilder(_other, boMetaPropertyTree, _propertyTreeUse));
        }
        final PropertyTree bosPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("bos"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(bosPropertyTree!= null):((bosPropertyTree == null)||(!bosPropertyTree.isLeaf())))) {
            _other.bos = ((this.bos == null)?null:this.bos.newCopyBuilder(_other, bosPropertyTree, _propertyTreeUse));
        }
        final PropertyTree resourceiconPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resourceicon"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resourceiconPropertyTree!= null):((resourceiconPropertyTree == null)||(!resourceiconPropertyTree.isLeaf())))) {
            _other.resourceicon = ((this.resourceicon == null)?null:this.resourceicon.newCopyBuilder(_other, resourceiconPropertyTree, _propertyTreeUse));
        }
    }

    public<_B >EntityMetaBaseLinks.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new EntityMetaBaseLinks.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public EntityMetaBaseLinks.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >EntityMetaBaseLinks.Builder<_B> copyOf(final EntityMetaBaseLinks _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final EntityMetaBaseLinks.Builder<_B> _newBuilder = new EntityMetaBaseLinks.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static EntityMetaBaseLinks.Builder<Void> copyExcept(final EntityMetaBaseLinks _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static EntityMetaBaseLinks.Builder<Void> copyOnly(final EntityMetaBaseLinks _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final EntityMetaBaseLinks _storedValue;
        private RestLink.Builder<EntityMetaBaseLinks.Builder<_B>> boMeta;
        private RestLink.Builder<EntityMetaBaseLinks.Builder<_B>> bos;
        private RestLink.Builder<EntityMetaBaseLinks.Builder<_B>> resourceicon;

        public Builder(final _B _parentBuilder, final EntityMetaBaseLinks _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.boMeta = ((_other.boMeta == null)?null:_other.boMeta.newCopyBuilder(this));
                    this.bos = ((_other.bos == null)?null:_other.bos.newCopyBuilder(this));
                    this.resourceicon = ((_other.resourceicon == null)?null:_other.resourceicon.newCopyBuilder(this));
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final EntityMetaBaseLinks _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree boMetaPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boMeta"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boMetaPropertyTree!= null):((boMetaPropertyTree == null)||(!boMetaPropertyTree.isLeaf())))) {
                        this.boMeta = ((_other.boMeta == null)?null:_other.boMeta.newCopyBuilder(this, boMetaPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree bosPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("bos"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(bosPropertyTree!= null):((bosPropertyTree == null)||(!bosPropertyTree.isLeaf())))) {
                        this.bos = ((_other.bos == null)?null:_other.bos.newCopyBuilder(this, bosPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree resourceiconPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resourceicon"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resourceiconPropertyTree!= null):((resourceiconPropertyTree == null)||(!resourceiconPropertyTree.isLeaf())))) {
                        this.resourceicon = ((_other.resourceicon == null)?null:_other.resourceicon.newCopyBuilder(this, resourceiconPropertyTree, _propertyTreeUse));
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends EntityMetaBaseLinks >_P init(final _P _product) {
            _product.boMeta = ((this.boMeta == null)?null:this.boMeta.build());
            _product.bos = ((this.bos == null)?null:this.bos.build());
            _product.resourceicon = ((this.resourceicon == null)?null:this.resourceicon.build());
            return _product;
        }

        /**
         * Sets the new value of "boMeta" (any previous value will be replaced)
         * 
         * @param boMeta
         *     New value of the "boMeta" property.
         */
        public EntityMetaBaseLinks.Builder<_B> withBoMeta(final RestLink boMeta) {
            this.boMeta = ((boMeta == null)?null:new RestLink.Builder<EntityMetaBaseLinks.Builder<_B>>(this, boMeta, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "boMeta" property.
         * Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "boMeta" property.
         *     Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         */
        public RestLink.Builder<? extends EntityMetaBaseLinks.Builder<_B>> withBoMeta() {
            if (this.boMeta!= null) {
                return this.boMeta;
            }
            return this.boMeta = new RestLink.Builder<EntityMetaBaseLinks.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "bos" (any previous value will be replaced)
         * 
         * @param bos
         *     New value of the "bos" property.
         */
        public EntityMetaBaseLinks.Builder<_B> withBos(final RestLink bos) {
            this.bos = ((bos == null)?null:new RestLink.Builder<EntityMetaBaseLinks.Builder<_B>>(this, bos, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "bos" property.
         * Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "bos" property.
         *     Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         */
        public RestLink.Builder<? extends EntityMetaBaseLinks.Builder<_B>> withBos() {
            if (this.bos!= null) {
                return this.bos;
            }
            return this.bos = new RestLink.Builder<EntityMetaBaseLinks.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "resourceicon" (any previous value will be replaced)
         * 
         * @param resourceicon
         *     New value of the "resourceicon" property.
         */
        public EntityMetaBaseLinks.Builder<_B> withResourceicon(final RestLink resourceicon) {
            this.resourceicon = ((resourceicon == null)?null:new RestLink.Builder<EntityMetaBaseLinks.Builder<_B>>(this, resourceicon, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "resourceicon" property.
         * Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "resourceicon" property.
         *     Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         */
        public RestLink.Builder<? extends EntityMetaBaseLinks.Builder<_B>> withResourceicon() {
            if (this.resourceicon!= null) {
                return this.resourceicon;
            }
            return this.resourceicon = new RestLink.Builder<EntityMetaBaseLinks.Builder<_B>>(this, null, false);
        }

        @Override
        public EntityMetaBaseLinks build() {
            if (_storedValue == null) {
                return this.init(new EntityMetaBaseLinks());
            } else {
                return ((EntityMetaBaseLinks) _storedValue);
            }
        }

        public EntityMetaBaseLinks.Builder<_B> copyOf(final EntityMetaBaseLinks _other) {
            _other.copyTo(this);
            return this;
        }

        public EntityMetaBaseLinks.Builder<_B> copyOf(final EntityMetaBaseLinks.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends EntityMetaBaseLinks.Selector<EntityMetaBaseLinks.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static EntityMetaBaseLinks.Select _root() {
            return new EntityMetaBaseLinks.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private RestLink.Selector<TRoot, EntityMetaBaseLinks.Selector<TRoot, TParent>> boMeta = null;
        private RestLink.Selector<TRoot, EntityMetaBaseLinks.Selector<TRoot, TParent>> bos = null;
        private RestLink.Selector<TRoot, EntityMetaBaseLinks.Selector<TRoot, TParent>> resourceicon = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.boMeta!= null) {
                products.put("boMeta", this.boMeta.init());
            }
            if (this.bos!= null) {
                products.put("bos", this.bos.init());
            }
            if (this.resourceicon!= null) {
                products.put("resourceicon", this.resourceicon.init());
            }
            return products;
        }

        public RestLink.Selector<TRoot, EntityMetaBaseLinks.Selector<TRoot, TParent>> boMeta() {
            return ((this.boMeta == null)?this.boMeta = new RestLink.Selector<TRoot, EntityMetaBaseLinks.Selector<TRoot, TParent>>(this._root, this, "boMeta"):this.boMeta);
        }

        public RestLink.Selector<TRoot, EntityMetaBaseLinks.Selector<TRoot, TParent>> bos() {
            return ((this.bos == null)?this.bos = new RestLink.Selector<TRoot, EntityMetaBaseLinks.Selector<TRoot, TParent>>(this._root, this, "bos"):this.bos);
        }

        public RestLink.Selector<TRoot, EntityMetaBaseLinks.Selector<TRoot, TParent>> resourceicon() {
            return ((this.resourceicon == null)?this.resourceicon = new RestLink.Selector<TRoot, EntityMetaBaseLinks.Selector<TRoot, TParent>>(this._root, this, "resourceicon"):this.resourceicon);
        }

    }

}
