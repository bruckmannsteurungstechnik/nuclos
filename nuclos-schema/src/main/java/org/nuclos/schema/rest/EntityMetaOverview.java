
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for entity-meta-overview complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="entity-meta-overview"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:org.nuclos.schema.rest}entity-meta-base"&gt;
 *       &lt;attribute name="processMetaId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "entity-meta-overview")
@XmlSeeAlso({
    SearchfilterEntityMeta.class
})
public class EntityMetaOverview
    extends EntityMetaBase
    implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "processMetaId")
    protected String processMetaId;

    /**
     * Gets the value of the processMetaId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessMetaId() {
        return processMetaId;
    }

    /**
     * Sets the value of the processMetaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessMetaId(String value) {
        this.processMetaId = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        super.appendFields(locator, buffer, strategy);
        {
            String theProcessMetaId;
            theProcessMetaId = this.getProcessMetaId();
            strategy.appendField(locator, this, "processMetaId", buffer, theProcessMetaId);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof EntityMetaOverview)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final EntityMetaOverview that = ((EntityMetaOverview) object);
        {
            String lhsProcessMetaId;
            lhsProcessMetaId = this.getProcessMetaId();
            String rhsProcessMetaId;
            rhsProcessMetaId = that.getProcessMetaId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "processMetaId", lhsProcessMetaId), LocatorUtils.property(thatLocator, "processMetaId", rhsProcessMetaId), lhsProcessMetaId, rhsProcessMetaId)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = super.hashCode(locator, strategy);
        {
            String theProcessMetaId;
            theProcessMetaId = this.getProcessMetaId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "processMetaId", theProcessMetaId), currentHashCode, theProcessMetaId);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof EntityMetaOverview) {
            final EntityMetaOverview copy = ((EntityMetaOverview) draftCopy);
            if (this.processMetaId!= null) {
                String sourceProcessMetaId;
                sourceProcessMetaId = this.getProcessMetaId();
                String copyProcessMetaId = ((String) strategy.copy(LocatorUtils.property(locator, "processMetaId", sourceProcessMetaId), sourceProcessMetaId));
                copy.setProcessMetaId(copyProcessMetaId);
            } else {
                copy.processMetaId = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new EntityMetaOverview();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityMetaOverview.Builder<_B> _other) {
        super.copyTo(_other);
        _other.processMetaId = this.processMetaId;
    }

    @Override
    public<_B >EntityMetaOverview.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new EntityMetaOverview.Builder<_B>(_parentBuilder, this, true);
    }

    @Override
    public EntityMetaOverview.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static EntityMetaOverview.Builder<Void> builder() {
        return new EntityMetaOverview.Builder<Void>(null, null, false);
    }

    public static<_B >EntityMetaOverview.Builder<_B> copyOf(final EntityMetaBase _other) {
        final EntityMetaOverview.Builder<_B> _newBuilder = new EntityMetaOverview.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >EntityMetaOverview.Builder<_B> copyOf(final EntityMetaOverview _other) {
        final EntityMetaOverview.Builder<_B> _newBuilder = new EntityMetaOverview.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityMetaOverview.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        super.copyTo(_other, _propertyTree, _propertyTreeUse);
        final PropertyTree processMetaIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("processMetaId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(processMetaIdPropertyTree!= null):((processMetaIdPropertyTree == null)||(!processMetaIdPropertyTree.isLeaf())))) {
            _other.processMetaId = this.processMetaId;
        }
    }

    @Override
    public<_B >EntityMetaOverview.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new EntityMetaOverview.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    @Override
    public EntityMetaOverview.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >EntityMetaOverview.Builder<_B> copyOf(final EntityMetaBase _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final EntityMetaOverview.Builder<_B> _newBuilder = new EntityMetaOverview.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >EntityMetaOverview.Builder<_B> copyOf(final EntityMetaOverview _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final EntityMetaOverview.Builder<_B> _newBuilder = new EntityMetaOverview.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static EntityMetaOverview.Builder<Void> copyExcept(final EntityMetaBase _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static EntityMetaOverview.Builder<Void> copyExcept(final EntityMetaOverview _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static EntityMetaOverview.Builder<Void> copyOnly(final EntityMetaBase _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static EntityMetaOverview.Builder<Void> copyOnly(final EntityMetaOverview _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >
        extends EntityMetaBase.Builder<_B>
        implements Buildable
    {

        private String processMetaId;

        public Builder(final _B _parentBuilder, final EntityMetaOverview _other, final boolean _copy) {
            super(_parentBuilder, _other, _copy);
            if (_other!= null) {
                this.processMetaId = _other.processMetaId;
            }
        }

        public Builder(final _B _parentBuilder, final EntityMetaOverview _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            super(_parentBuilder, _other, _copy, _propertyTree, _propertyTreeUse);
            if (_other!= null) {
                final PropertyTree processMetaIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("processMetaId"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(processMetaIdPropertyTree!= null):((processMetaIdPropertyTree == null)||(!processMetaIdPropertyTree.isLeaf())))) {
                    this.processMetaId = _other.processMetaId;
                }
            }
        }

        protected<_P extends EntityMetaOverview >_P init(final _P _product) {
            _product.processMetaId = this.processMetaId;
            return super.init(_product);
        }

        /**
         * Sets the new value of "processMetaId" (any previous value will be replaced)
         * 
         * @param processMetaId
         *     New value of the "processMetaId" property.
         */
        public EntityMetaOverview.Builder<_B> withProcessMetaId(final String processMetaId) {
            this.processMetaId = processMetaId;
            return this;
        }

        /**
         * Sets the new value of "links" (any previous value will be replaced)
         * 
         * @param links
         *     New value of the "links" property.
         */
        @Override
        public EntityMetaOverview.Builder<_B> withLinks(final EntityMetaBaseLinks links) {
            super.withLinks(links);
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "links" property.
         * Use {@link org.nuclos.schema.rest.EntityMetaBaseLinks.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "links" property.
         *     Use {@link org.nuclos.schema.rest.EntityMetaBaseLinks.Builder#end()} to return to the current builder.
         */
        public EntityMetaBaseLinks.Builder<? extends EntityMetaOverview.Builder<_B>> withLinks() {
            return ((EntityMetaBaseLinks.Builder<? extends EntityMetaOverview.Builder<_B>> ) super.withLinks());
        }

        /**
         * Sets the new value of "boMetaId" (any previous value will be replaced)
         * 
         * @param boMetaId
         *     New value of the "boMetaId" property.
         */
        @Override
        public EntityMetaOverview.Builder<_B> withBoMetaId(final String boMetaId) {
            super.withBoMetaId(boMetaId);
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        @Override
        public EntityMetaOverview.Builder<_B> withName(final String name) {
            super.withName(name);
            return this;
        }

        /**
         * Sets the new value of "createNew" (any previous value will be replaced)
         * 
         * @param createNew
         *     New value of the "createNew" property.
         */
        @Override
        public EntityMetaOverview.Builder<_B> withCreateNew(final boolean createNew) {
            super.withCreateNew(createNew);
            return this;
        }

        @Override
        public EntityMetaOverview build() {
            if (_storedValue == null) {
                return this.init(new EntityMetaOverview());
            } else {
                return ((EntityMetaOverview) _storedValue);
            }
        }

        public EntityMetaOverview.Builder<_B> copyOf(final EntityMetaOverview _other) {
            _other.copyTo(this);
            return this;
        }

        public EntityMetaOverview.Builder<_B> copyOf(final EntityMetaOverview.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends EntityMetaOverview.Selector<EntityMetaOverview.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static EntityMetaOverview.Select _root() {
            return new EntityMetaOverview.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends EntityMetaBase.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, EntityMetaOverview.Selector<TRoot, TParent>> processMetaId = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.processMetaId!= null) {
                products.put("processMetaId", this.processMetaId.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityMetaOverview.Selector<TRoot, TParent>> processMetaId() {
            return ((this.processMetaId == null)?this.processMetaId = new com.kscs.util.jaxb.Selector<TRoot, EntityMetaOverview.Selector<TRoot, TParent>>(this._root, this, "processMetaId"):this.processMetaId);
        }

    }

}
