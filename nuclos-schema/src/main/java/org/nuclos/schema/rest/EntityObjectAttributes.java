
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for entity-object-attributes complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="entity-object-attributes"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="primaryKey" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="createdBy" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="createdAt" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="changedBy" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="changedAt" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "entity-object-attributes")
public class EntityObjectAttributes implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "primaryKey")
    protected String primaryKey;
    @XmlAttribute(name = "createdBy")
    protected String createdBy;
    @XmlAttribute(name = "createdAt")
    protected String createdAt;
    @XmlAttribute(name = "changedBy")
    protected String changedBy;
    @XmlAttribute(name = "changedAt")
    protected String changedAt;

    /**
     * Gets the value of the primaryKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryKey() {
        return primaryKey;
    }

    /**
     * Sets the value of the primaryKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryKey(String value) {
        this.primaryKey = value;
    }

    /**
     * Gets the value of the createdBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the value of the createdBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatedBy(String value) {
        this.createdBy = value;
    }

    /**
     * Gets the value of the createdAt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * Sets the value of the createdAt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatedAt(String value) {
        this.createdAt = value;
    }

    /**
     * Gets the value of the changedBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChangedBy() {
        return changedBy;
    }

    /**
     * Sets the value of the changedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChangedBy(String value) {
        this.changedBy = value;
    }

    /**
     * Gets the value of the changedAt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChangedAt() {
        return changedAt;
    }

    /**
     * Sets the value of the changedAt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChangedAt(String value) {
        this.changedAt = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String thePrimaryKey;
            thePrimaryKey = this.getPrimaryKey();
            strategy.appendField(locator, this, "primaryKey", buffer, thePrimaryKey);
        }
        {
            String theCreatedBy;
            theCreatedBy = this.getCreatedBy();
            strategy.appendField(locator, this, "createdBy", buffer, theCreatedBy);
        }
        {
            String theCreatedAt;
            theCreatedAt = this.getCreatedAt();
            strategy.appendField(locator, this, "createdAt", buffer, theCreatedAt);
        }
        {
            String theChangedBy;
            theChangedBy = this.getChangedBy();
            strategy.appendField(locator, this, "changedBy", buffer, theChangedBy);
        }
        {
            String theChangedAt;
            theChangedAt = this.getChangedAt();
            strategy.appendField(locator, this, "changedAt", buffer, theChangedAt);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof EntityObjectAttributes)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final EntityObjectAttributes that = ((EntityObjectAttributes) object);
        {
            String lhsPrimaryKey;
            lhsPrimaryKey = this.getPrimaryKey();
            String rhsPrimaryKey;
            rhsPrimaryKey = that.getPrimaryKey();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "primaryKey", lhsPrimaryKey), LocatorUtils.property(thatLocator, "primaryKey", rhsPrimaryKey), lhsPrimaryKey, rhsPrimaryKey)) {
                return false;
            }
        }
        {
            String lhsCreatedBy;
            lhsCreatedBy = this.getCreatedBy();
            String rhsCreatedBy;
            rhsCreatedBy = that.getCreatedBy();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "createdBy", lhsCreatedBy), LocatorUtils.property(thatLocator, "createdBy", rhsCreatedBy), lhsCreatedBy, rhsCreatedBy)) {
                return false;
            }
        }
        {
            String lhsCreatedAt;
            lhsCreatedAt = this.getCreatedAt();
            String rhsCreatedAt;
            rhsCreatedAt = that.getCreatedAt();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "createdAt", lhsCreatedAt), LocatorUtils.property(thatLocator, "createdAt", rhsCreatedAt), lhsCreatedAt, rhsCreatedAt)) {
                return false;
            }
        }
        {
            String lhsChangedBy;
            lhsChangedBy = this.getChangedBy();
            String rhsChangedBy;
            rhsChangedBy = that.getChangedBy();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "changedBy", lhsChangedBy), LocatorUtils.property(thatLocator, "changedBy", rhsChangedBy), lhsChangedBy, rhsChangedBy)) {
                return false;
            }
        }
        {
            String lhsChangedAt;
            lhsChangedAt = this.getChangedAt();
            String rhsChangedAt;
            rhsChangedAt = that.getChangedAt();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "changedAt", lhsChangedAt), LocatorUtils.property(thatLocator, "changedAt", rhsChangedAt), lhsChangedAt, rhsChangedAt)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String thePrimaryKey;
            thePrimaryKey = this.getPrimaryKey();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "primaryKey", thePrimaryKey), currentHashCode, thePrimaryKey);
        }
        {
            String theCreatedBy;
            theCreatedBy = this.getCreatedBy();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "createdBy", theCreatedBy), currentHashCode, theCreatedBy);
        }
        {
            String theCreatedAt;
            theCreatedAt = this.getCreatedAt();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "createdAt", theCreatedAt), currentHashCode, theCreatedAt);
        }
        {
            String theChangedBy;
            theChangedBy = this.getChangedBy();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "changedBy", theChangedBy), currentHashCode, theChangedBy);
        }
        {
            String theChangedAt;
            theChangedAt = this.getChangedAt();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "changedAt", theChangedAt), currentHashCode, theChangedAt);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof EntityObjectAttributes) {
            final EntityObjectAttributes copy = ((EntityObjectAttributes) draftCopy);
            if (this.primaryKey!= null) {
                String sourcePrimaryKey;
                sourcePrimaryKey = this.getPrimaryKey();
                String copyPrimaryKey = ((String) strategy.copy(LocatorUtils.property(locator, "primaryKey", sourcePrimaryKey), sourcePrimaryKey));
                copy.setPrimaryKey(copyPrimaryKey);
            } else {
                copy.primaryKey = null;
            }
            if (this.createdBy!= null) {
                String sourceCreatedBy;
                sourceCreatedBy = this.getCreatedBy();
                String copyCreatedBy = ((String) strategy.copy(LocatorUtils.property(locator, "createdBy", sourceCreatedBy), sourceCreatedBy));
                copy.setCreatedBy(copyCreatedBy);
            } else {
                copy.createdBy = null;
            }
            if (this.createdAt!= null) {
                String sourceCreatedAt;
                sourceCreatedAt = this.getCreatedAt();
                String copyCreatedAt = ((String) strategy.copy(LocatorUtils.property(locator, "createdAt", sourceCreatedAt), sourceCreatedAt));
                copy.setCreatedAt(copyCreatedAt);
            } else {
                copy.createdAt = null;
            }
            if (this.changedBy!= null) {
                String sourceChangedBy;
                sourceChangedBy = this.getChangedBy();
                String copyChangedBy = ((String) strategy.copy(LocatorUtils.property(locator, "changedBy", sourceChangedBy), sourceChangedBy));
                copy.setChangedBy(copyChangedBy);
            } else {
                copy.changedBy = null;
            }
            if (this.changedAt!= null) {
                String sourceChangedAt;
                sourceChangedAt = this.getChangedAt();
                String copyChangedAt = ((String) strategy.copy(LocatorUtils.property(locator, "changedAt", sourceChangedAt), sourceChangedAt));
                copy.setChangedAt(copyChangedAt);
            } else {
                copy.changedAt = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new EntityObjectAttributes();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityObjectAttributes.Builder<_B> _other) {
        _other.primaryKey = this.primaryKey;
        _other.createdBy = this.createdBy;
        _other.createdAt = this.createdAt;
        _other.changedBy = this.changedBy;
        _other.changedAt = this.changedAt;
    }

    public<_B >EntityObjectAttributes.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new EntityObjectAttributes.Builder<_B>(_parentBuilder, this, true);
    }

    public EntityObjectAttributes.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static EntityObjectAttributes.Builder<Void> builder() {
        return new EntityObjectAttributes.Builder<Void>(null, null, false);
    }

    public static<_B >EntityObjectAttributes.Builder<_B> copyOf(final EntityObjectAttributes _other) {
        final EntityObjectAttributes.Builder<_B> _newBuilder = new EntityObjectAttributes.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityObjectAttributes.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree primaryKeyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("primaryKey"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(primaryKeyPropertyTree!= null):((primaryKeyPropertyTree == null)||(!primaryKeyPropertyTree.isLeaf())))) {
            _other.primaryKey = this.primaryKey;
        }
        final PropertyTree createdByPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("createdBy"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(createdByPropertyTree!= null):((createdByPropertyTree == null)||(!createdByPropertyTree.isLeaf())))) {
            _other.createdBy = this.createdBy;
        }
        final PropertyTree createdAtPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("createdAt"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(createdAtPropertyTree!= null):((createdAtPropertyTree == null)||(!createdAtPropertyTree.isLeaf())))) {
            _other.createdAt = this.createdAt;
        }
        final PropertyTree changedByPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("changedBy"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(changedByPropertyTree!= null):((changedByPropertyTree == null)||(!changedByPropertyTree.isLeaf())))) {
            _other.changedBy = this.changedBy;
        }
        final PropertyTree changedAtPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("changedAt"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(changedAtPropertyTree!= null):((changedAtPropertyTree == null)||(!changedAtPropertyTree.isLeaf())))) {
            _other.changedAt = this.changedAt;
        }
    }

    public<_B >EntityObjectAttributes.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new EntityObjectAttributes.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public EntityObjectAttributes.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >EntityObjectAttributes.Builder<_B> copyOf(final EntityObjectAttributes _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final EntityObjectAttributes.Builder<_B> _newBuilder = new EntityObjectAttributes.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static EntityObjectAttributes.Builder<Void> copyExcept(final EntityObjectAttributes _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static EntityObjectAttributes.Builder<Void> copyOnly(final EntityObjectAttributes _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final EntityObjectAttributes _storedValue;
        private String primaryKey;
        private String createdBy;
        private String createdAt;
        private String changedBy;
        private String changedAt;

        public Builder(final _B _parentBuilder, final EntityObjectAttributes _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.primaryKey = _other.primaryKey;
                    this.createdBy = _other.createdBy;
                    this.createdAt = _other.createdAt;
                    this.changedBy = _other.changedBy;
                    this.changedAt = _other.changedAt;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final EntityObjectAttributes _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree primaryKeyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("primaryKey"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(primaryKeyPropertyTree!= null):((primaryKeyPropertyTree == null)||(!primaryKeyPropertyTree.isLeaf())))) {
                        this.primaryKey = _other.primaryKey;
                    }
                    final PropertyTree createdByPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("createdBy"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(createdByPropertyTree!= null):((createdByPropertyTree == null)||(!createdByPropertyTree.isLeaf())))) {
                        this.createdBy = _other.createdBy;
                    }
                    final PropertyTree createdAtPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("createdAt"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(createdAtPropertyTree!= null):((createdAtPropertyTree == null)||(!createdAtPropertyTree.isLeaf())))) {
                        this.createdAt = _other.createdAt;
                    }
                    final PropertyTree changedByPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("changedBy"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(changedByPropertyTree!= null):((changedByPropertyTree == null)||(!changedByPropertyTree.isLeaf())))) {
                        this.changedBy = _other.changedBy;
                    }
                    final PropertyTree changedAtPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("changedAt"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(changedAtPropertyTree!= null):((changedAtPropertyTree == null)||(!changedAtPropertyTree.isLeaf())))) {
                        this.changedAt = _other.changedAt;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends EntityObjectAttributes >_P init(final _P _product) {
            _product.primaryKey = this.primaryKey;
            _product.createdBy = this.createdBy;
            _product.createdAt = this.createdAt;
            _product.changedBy = this.changedBy;
            _product.changedAt = this.changedAt;
            return _product;
        }

        /**
         * Sets the new value of "primaryKey" (any previous value will be replaced)
         * 
         * @param primaryKey
         *     New value of the "primaryKey" property.
         */
        public EntityObjectAttributes.Builder<_B> withPrimaryKey(final String primaryKey) {
            this.primaryKey = primaryKey;
            return this;
        }

        /**
         * Sets the new value of "createdBy" (any previous value will be replaced)
         * 
         * @param createdBy
         *     New value of the "createdBy" property.
         */
        public EntityObjectAttributes.Builder<_B> withCreatedBy(final String createdBy) {
            this.createdBy = createdBy;
            return this;
        }

        /**
         * Sets the new value of "createdAt" (any previous value will be replaced)
         * 
         * @param createdAt
         *     New value of the "createdAt" property.
         */
        public EntityObjectAttributes.Builder<_B> withCreatedAt(final String createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        /**
         * Sets the new value of "changedBy" (any previous value will be replaced)
         * 
         * @param changedBy
         *     New value of the "changedBy" property.
         */
        public EntityObjectAttributes.Builder<_B> withChangedBy(final String changedBy) {
            this.changedBy = changedBy;
            return this;
        }

        /**
         * Sets the new value of "changedAt" (any previous value will be replaced)
         * 
         * @param changedAt
         *     New value of the "changedAt" property.
         */
        public EntityObjectAttributes.Builder<_B> withChangedAt(final String changedAt) {
            this.changedAt = changedAt;
            return this;
        }

        @Override
        public EntityObjectAttributes build() {
            if (_storedValue == null) {
                return this.init(new EntityObjectAttributes());
            } else {
                return ((EntityObjectAttributes) _storedValue);
            }
        }

        public EntityObjectAttributes.Builder<_B> copyOf(final EntityObjectAttributes _other) {
            _other.copyTo(this);
            return this;
        }

        public EntityObjectAttributes.Builder<_B> copyOf(final EntityObjectAttributes.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends EntityObjectAttributes.Selector<EntityObjectAttributes.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static EntityObjectAttributes.Select _root() {
            return new EntityObjectAttributes.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, EntityObjectAttributes.Selector<TRoot, TParent>> primaryKey = null;
        private com.kscs.util.jaxb.Selector<TRoot, EntityObjectAttributes.Selector<TRoot, TParent>> createdBy = null;
        private com.kscs.util.jaxb.Selector<TRoot, EntityObjectAttributes.Selector<TRoot, TParent>> createdAt = null;
        private com.kscs.util.jaxb.Selector<TRoot, EntityObjectAttributes.Selector<TRoot, TParent>> changedBy = null;
        private com.kscs.util.jaxb.Selector<TRoot, EntityObjectAttributes.Selector<TRoot, TParent>> changedAt = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.primaryKey!= null) {
                products.put("primaryKey", this.primaryKey.init());
            }
            if (this.createdBy!= null) {
                products.put("createdBy", this.createdBy.init());
            }
            if (this.createdAt!= null) {
                products.put("createdAt", this.createdAt.init());
            }
            if (this.changedBy!= null) {
                products.put("changedBy", this.changedBy.init());
            }
            if (this.changedAt!= null) {
                products.put("changedAt", this.changedAt.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityObjectAttributes.Selector<TRoot, TParent>> primaryKey() {
            return ((this.primaryKey == null)?this.primaryKey = new com.kscs.util.jaxb.Selector<TRoot, EntityObjectAttributes.Selector<TRoot, TParent>>(this._root, this, "primaryKey"):this.primaryKey);
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityObjectAttributes.Selector<TRoot, TParent>> createdBy() {
            return ((this.createdBy == null)?this.createdBy = new com.kscs.util.jaxb.Selector<TRoot, EntityObjectAttributes.Selector<TRoot, TParent>>(this._root, this, "createdBy"):this.createdBy);
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityObjectAttributes.Selector<TRoot, TParent>> createdAt() {
            return ((this.createdAt == null)?this.createdAt = new com.kscs.util.jaxb.Selector<TRoot, EntityObjectAttributes.Selector<TRoot, TParent>>(this._root, this, "createdAt"):this.createdAt);
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityObjectAttributes.Selector<TRoot, TParent>> changedBy() {
            return ((this.changedBy == null)?this.changedBy = new com.kscs.util.jaxb.Selector<TRoot, EntityObjectAttributes.Selector<TRoot, TParent>>(this._root, this, "changedBy"):this.changedBy);
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityObjectAttributes.Selector<TRoot, TParent>> changedAt() {
            return ((this.changedAt == null)?this.changedAt = new com.kscs.util.jaxb.Selector<TRoot, EntityObjectAttributes.Selector<TRoot, TParent>>(this._root, this, "changedAt"):this.changedAt);
        }

    }

}
