
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for evaluated-title-expression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="evaluated-title-expression"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="evaluated_expression" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "evaluated-title-expression")
public class EvaluatedTitleExpression implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "evaluated_expression")
    protected String evaluatedExpression;

    /**
     * Gets the value of the evaluatedExpression property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @JsonProperty("evaluated_expression")
    public String getEvaluatedExpression() {
        return evaluatedExpression;
    }

    /**
     * Sets the value of the evaluatedExpression property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEvaluatedExpression(String value) {
        this.evaluatedExpression = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theEvaluatedExpression;
            theEvaluatedExpression = this.getEvaluatedExpression();
            strategy.appendField(locator, this, "evaluatedExpression", buffer, theEvaluatedExpression);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof EvaluatedTitleExpression)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final EvaluatedTitleExpression that = ((EvaluatedTitleExpression) object);
        {
            String lhsEvaluatedExpression;
            lhsEvaluatedExpression = this.getEvaluatedExpression();
            String rhsEvaluatedExpression;
            rhsEvaluatedExpression = that.getEvaluatedExpression();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "evaluatedExpression", lhsEvaluatedExpression), LocatorUtils.property(thatLocator, "evaluatedExpression", rhsEvaluatedExpression), lhsEvaluatedExpression, rhsEvaluatedExpression)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theEvaluatedExpression;
            theEvaluatedExpression = this.getEvaluatedExpression();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "evaluatedExpression", theEvaluatedExpression), currentHashCode, theEvaluatedExpression);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof EvaluatedTitleExpression) {
            final EvaluatedTitleExpression copy = ((EvaluatedTitleExpression) draftCopy);
            if (this.evaluatedExpression!= null) {
                String sourceEvaluatedExpression;
                sourceEvaluatedExpression = this.getEvaluatedExpression();
                String copyEvaluatedExpression = ((String) strategy.copy(LocatorUtils.property(locator, "evaluatedExpression", sourceEvaluatedExpression), sourceEvaluatedExpression));
                copy.setEvaluatedExpression(copyEvaluatedExpression);
            } else {
                copy.evaluatedExpression = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new EvaluatedTitleExpression();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EvaluatedTitleExpression.Builder<_B> _other) {
        _other.evaluatedExpression = this.evaluatedExpression;
    }

    public<_B >EvaluatedTitleExpression.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new EvaluatedTitleExpression.Builder<_B>(_parentBuilder, this, true);
    }

    public EvaluatedTitleExpression.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static EvaluatedTitleExpression.Builder<Void> builder() {
        return new EvaluatedTitleExpression.Builder<Void>(null, null, false);
    }

    public static<_B >EvaluatedTitleExpression.Builder<_B> copyOf(final EvaluatedTitleExpression _other) {
        final EvaluatedTitleExpression.Builder<_B> _newBuilder = new EvaluatedTitleExpression.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EvaluatedTitleExpression.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree evaluatedExpressionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("evaluatedExpression"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(evaluatedExpressionPropertyTree!= null):((evaluatedExpressionPropertyTree == null)||(!evaluatedExpressionPropertyTree.isLeaf())))) {
            _other.evaluatedExpression = this.evaluatedExpression;
        }
    }

    public<_B >EvaluatedTitleExpression.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new EvaluatedTitleExpression.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public EvaluatedTitleExpression.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >EvaluatedTitleExpression.Builder<_B> copyOf(final EvaluatedTitleExpression _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final EvaluatedTitleExpression.Builder<_B> _newBuilder = new EvaluatedTitleExpression.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static EvaluatedTitleExpression.Builder<Void> copyExcept(final EvaluatedTitleExpression _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static EvaluatedTitleExpression.Builder<Void> copyOnly(final EvaluatedTitleExpression _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final EvaluatedTitleExpression _storedValue;
        private String evaluatedExpression;

        public Builder(final _B _parentBuilder, final EvaluatedTitleExpression _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.evaluatedExpression = _other.evaluatedExpression;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final EvaluatedTitleExpression _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree evaluatedExpressionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("evaluatedExpression"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(evaluatedExpressionPropertyTree!= null):((evaluatedExpressionPropertyTree == null)||(!evaluatedExpressionPropertyTree.isLeaf())))) {
                        this.evaluatedExpression = _other.evaluatedExpression;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends EvaluatedTitleExpression >_P init(final _P _product) {
            _product.evaluatedExpression = this.evaluatedExpression;
            return _product;
        }

        /**
         * Sets the new value of "evaluatedExpression" (any previous value will be replaced)
         * 
         * @param evaluatedExpression
         *     New value of the "evaluatedExpression" property.
         */
        public EvaluatedTitleExpression.Builder<_B> withEvaluatedExpression(final String evaluatedExpression) {
            this.evaluatedExpression = evaluatedExpression;
            return this;
        }

        @Override
        public EvaluatedTitleExpression build() {
            if (_storedValue == null) {
                return this.init(new EvaluatedTitleExpression());
            } else {
                return ((EvaluatedTitleExpression) _storedValue);
            }
        }

        public EvaluatedTitleExpression.Builder<_B> copyOf(final EvaluatedTitleExpression _other) {
            _other.copyTo(this);
            return this;
        }

        public EvaluatedTitleExpression.Builder<_B> copyOf(final EvaluatedTitleExpression.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends EvaluatedTitleExpression.Selector<EvaluatedTitleExpression.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static EvaluatedTitleExpression.Select _root() {
            return new EvaluatedTitleExpression.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, EvaluatedTitleExpression.Selector<TRoot, TParent>> evaluatedExpression = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.evaluatedExpression!= null) {
                products.put("evaluatedExpression", this.evaluatedExpression.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, EvaluatedTitleExpression.Selector<TRoot, TParent>> evaluatedExpression() {
            return ((this.evaluatedExpression == null)?this.evaluatedExpression = new com.kscs.util.jaxb.Selector<TRoot, EvaluatedTitleExpression.Selector<TRoot, TParent>>(this._root, this, "evaluatedExpression"):this.evaluatedExpression);
        }

    }

}
