
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for input-required complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="input-required"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="result" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "input-required", propOrder = {
    "result"
})
public class InputRequired implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected Object result;

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setResult(Object value) {
        this.result = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            Object theResult;
            theResult = this.getResult();
            strategy.appendField(locator, this, "result", buffer, theResult);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof InputRequired)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final InputRequired that = ((InputRequired) object);
        {
            Object lhsResult;
            lhsResult = this.getResult();
            Object rhsResult;
            rhsResult = that.getResult();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "result", lhsResult), LocatorUtils.property(thatLocator, "result", rhsResult), lhsResult, rhsResult)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            Object theResult;
            theResult = this.getResult();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "result", theResult), currentHashCode, theResult);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof InputRequired) {
            final InputRequired copy = ((InputRequired) draftCopy);
            if (this.result!= null) {
                Object sourceResult;
                sourceResult = this.getResult();
                Object copyResult = ((Object) strategy.copy(LocatorUtils.property(locator, "result", sourceResult), sourceResult));
                copy.setResult(copyResult);
            } else {
                copy.result = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new InputRequired();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final InputRequired.Builder<_B> _other) {
        _other.result = this.result;
    }

    public<_B >InputRequired.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new InputRequired.Builder<_B>(_parentBuilder, this, true);
    }

    public InputRequired.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static InputRequired.Builder<Void> builder() {
        return new InputRequired.Builder<Void>(null, null, false);
    }

    public static<_B >InputRequired.Builder<_B> copyOf(final InputRequired _other) {
        final InputRequired.Builder<_B> _newBuilder = new InputRequired.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final InputRequired.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree resultPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("result"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resultPropertyTree!= null):((resultPropertyTree == null)||(!resultPropertyTree.isLeaf())))) {
            _other.result = this.result;
        }
    }

    public<_B >InputRequired.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new InputRequired.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public InputRequired.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >InputRequired.Builder<_B> copyOf(final InputRequired _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final InputRequired.Builder<_B> _newBuilder = new InputRequired.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static InputRequired.Builder<Void> copyExcept(final InputRequired _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static InputRequired.Builder<Void> copyOnly(final InputRequired _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final InputRequired _storedValue;
        private Object result;

        public Builder(final _B _parentBuilder, final InputRequired _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.result = _other.result;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final InputRequired _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree resultPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("result"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resultPropertyTree!= null):((resultPropertyTree == null)||(!resultPropertyTree.isLeaf())))) {
                        this.result = _other.result;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends InputRequired >_P init(final _P _product) {
            _product.result = this.result;
            return _product;
        }

        /**
         * Sets the new value of "result" (any previous value will be replaced)
         * 
         * @param result
         *     New value of the "result" property.
         */
        public InputRequired.Builder<_B> withResult(final Object result) {
            this.result = result;
            return this;
        }

        @Override
        public InputRequired build() {
            if (_storedValue == null) {
                return this.init(new InputRequired());
            } else {
                return ((InputRequired) _storedValue);
            }
        }

        public InputRequired.Builder<_B> copyOf(final InputRequired _other) {
            _other.copyTo(this);
            return this;
        }

        public InputRequired.Builder<_B> copyOf(final InputRequired.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends InputRequired.Selector<InputRequired.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static InputRequired.Select _root() {
            return new InputRequired.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, InputRequired.Selector<TRoot, TParent>> result = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.result!= null) {
                products.put("result", this.result.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, InputRequired.Selector<TRoot, TParent>> result() {
            return ((this.result == null)?this.result = new com.kscs.util.jaxb.Selector<TRoot, InputRequired.Selector<TRoot, TParent>>(this._root, this, "result"):this.result);
        }

    }

}
