
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for input-required-context complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="input-required-context"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="inputrequired" type="{urn:org.nuclos.schema.rest}input-required"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "input-required-context", propOrder = {
    "inputrequired"
})
public class InputRequiredContext implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected InputRequired inputrequired;

    /**
     * Gets the value of the inputrequired property.
     * 
     * @return
     *     possible object is
     *     {@link InputRequired }
     *     
     */
    public InputRequired getInputrequired() {
        return inputrequired;
    }

    /**
     * Sets the value of the inputrequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link InputRequired }
     *     
     */
    public void setInputrequired(InputRequired value) {
        this.inputrequired = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            InputRequired theInputrequired;
            theInputrequired = this.getInputrequired();
            strategy.appendField(locator, this, "inputrequired", buffer, theInputrequired);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof InputRequiredContext)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final InputRequiredContext that = ((InputRequiredContext) object);
        {
            InputRequired lhsInputrequired;
            lhsInputrequired = this.getInputrequired();
            InputRequired rhsInputrequired;
            rhsInputrequired = that.getInputrequired();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "inputrequired", lhsInputrequired), LocatorUtils.property(thatLocator, "inputrequired", rhsInputrequired), lhsInputrequired, rhsInputrequired)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            InputRequired theInputrequired;
            theInputrequired = this.getInputrequired();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "inputrequired", theInputrequired), currentHashCode, theInputrequired);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof InputRequiredContext) {
            final InputRequiredContext copy = ((InputRequiredContext) draftCopy);
            if (this.inputrequired!= null) {
                InputRequired sourceInputrequired;
                sourceInputrequired = this.getInputrequired();
                InputRequired copyInputrequired = ((InputRequired) strategy.copy(LocatorUtils.property(locator, "inputrequired", sourceInputrequired), sourceInputrequired));
                copy.setInputrequired(copyInputrequired);
            } else {
                copy.inputrequired = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new InputRequiredContext();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final InputRequiredContext.Builder<_B> _other) {
        _other.inputrequired = ((this.inputrequired == null)?null:this.inputrequired.newCopyBuilder(_other));
    }

    public<_B >InputRequiredContext.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new InputRequiredContext.Builder<_B>(_parentBuilder, this, true);
    }

    public InputRequiredContext.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static InputRequiredContext.Builder<Void> builder() {
        return new InputRequiredContext.Builder<Void>(null, null, false);
    }

    public static<_B >InputRequiredContext.Builder<_B> copyOf(final InputRequiredContext _other) {
        final InputRequiredContext.Builder<_B> _newBuilder = new InputRequiredContext.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final InputRequiredContext.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree inputrequiredPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("inputrequired"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(inputrequiredPropertyTree!= null):((inputrequiredPropertyTree == null)||(!inputrequiredPropertyTree.isLeaf())))) {
            _other.inputrequired = ((this.inputrequired == null)?null:this.inputrequired.newCopyBuilder(_other, inputrequiredPropertyTree, _propertyTreeUse));
        }
    }

    public<_B >InputRequiredContext.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new InputRequiredContext.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public InputRequiredContext.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >InputRequiredContext.Builder<_B> copyOf(final InputRequiredContext _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final InputRequiredContext.Builder<_B> _newBuilder = new InputRequiredContext.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static InputRequiredContext.Builder<Void> copyExcept(final InputRequiredContext _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static InputRequiredContext.Builder<Void> copyOnly(final InputRequiredContext _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final InputRequiredContext _storedValue;
        private InputRequired.Builder<InputRequiredContext.Builder<_B>> inputrequired;

        public Builder(final _B _parentBuilder, final InputRequiredContext _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.inputrequired = ((_other.inputrequired == null)?null:_other.inputrequired.newCopyBuilder(this));
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final InputRequiredContext _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree inputrequiredPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("inputrequired"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(inputrequiredPropertyTree!= null):((inputrequiredPropertyTree == null)||(!inputrequiredPropertyTree.isLeaf())))) {
                        this.inputrequired = ((_other.inputrequired == null)?null:_other.inputrequired.newCopyBuilder(this, inputrequiredPropertyTree, _propertyTreeUse));
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends InputRequiredContext >_P init(final _P _product) {
            _product.inputrequired = ((this.inputrequired == null)?null:this.inputrequired.build());
            return _product;
        }

        /**
         * Sets the new value of "inputrequired" (any previous value will be replaced)
         * 
         * @param inputrequired
         *     New value of the "inputrequired" property.
         */
        public InputRequiredContext.Builder<_B> withInputrequired(final InputRequired inputrequired) {
            this.inputrequired = ((inputrequired == null)?null:new InputRequired.Builder<InputRequiredContext.Builder<_B>>(this, inputrequired, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "inputrequired" property.
         * Use {@link org.nuclos.schema.rest.InputRequired.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "inputrequired" property.
         *     Use {@link org.nuclos.schema.rest.InputRequired.Builder#end()} to return to the current builder.
         */
        public InputRequired.Builder<? extends InputRequiredContext.Builder<_B>> withInputrequired() {
            if (this.inputrequired!= null) {
                return this.inputrequired;
            }
            return this.inputrequired = new InputRequired.Builder<InputRequiredContext.Builder<_B>>(this, null, false);
        }

        @Override
        public InputRequiredContext build() {
            if (_storedValue == null) {
                return this.init(new InputRequiredContext());
            } else {
                return ((InputRequiredContext) _storedValue);
            }
        }

        public InputRequiredContext.Builder<_B> copyOf(final InputRequiredContext _other) {
            _other.copyTo(this);
            return this;
        }

        public InputRequiredContext.Builder<_B> copyOf(final InputRequiredContext.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends InputRequiredContext.Selector<InputRequiredContext.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static InputRequiredContext.Select _root() {
            return new InputRequiredContext.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private InputRequired.Selector<TRoot, InputRequiredContext.Selector<TRoot, TParent>> inputrequired = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.inputrequired!= null) {
                products.put("inputrequired", this.inputrequired.init());
            }
            return products;
        }

        public InputRequired.Selector<TRoot, InputRequiredContext.Selector<TRoot, TParent>> inputrequired() {
            return ((this.inputrequired == null)?this.inputrequired = new InputRequired.Selector<TRoot, InputRequiredContext.Selector<TRoot, TParent>>(this._root, this, "inputrequired"):this.inputrequired);
        }

    }

}
