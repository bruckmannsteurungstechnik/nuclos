
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for job-status complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="job-status"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="status" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "job-status")
public class JobStatus implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "status")
    protected String status;

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theStatus;
            theStatus = this.getStatus();
            strategy.appendField(locator, this, "status", buffer, theStatus);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof JobStatus)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final JobStatus that = ((JobStatus) object);
        {
            String lhsStatus;
            lhsStatus = this.getStatus();
            String rhsStatus;
            rhsStatus = that.getStatus();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "status", lhsStatus), LocatorUtils.property(thatLocator, "status", rhsStatus), lhsStatus, rhsStatus)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theStatus;
            theStatus = this.getStatus();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "status", theStatus), currentHashCode, theStatus);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof JobStatus) {
            final JobStatus copy = ((JobStatus) draftCopy);
            if (this.status!= null) {
                String sourceStatus;
                sourceStatus = this.getStatus();
                String copyStatus = ((String) strategy.copy(LocatorUtils.property(locator, "status", sourceStatus), sourceStatus));
                copy.setStatus(copyStatus);
            } else {
                copy.status = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new JobStatus();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final JobStatus.Builder<_B> _other) {
        _other.status = this.status;
    }

    public<_B >JobStatus.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new JobStatus.Builder<_B>(_parentBuilder, this, true);
    }

    public JobStatus.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static JobStatus.Builder<Void> builder() {
        return new JobStatus.Builder<Void>(null, null, false);
    }

    public static<_B >JobStatus.Builder<_B> copyOf(final JobStatus _other) {
        final JobStatus.Builder<_B> _newBuilder = new JobStatus.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final JobStatus.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree statusPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("status"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(statusPropertyTree!= null):((statusPropertyTree == null)||(!statusPropertyTree.isLeaf())))) {
            _other.status = this.status;
        }
    }

    public<_B >JobStatus.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new JobStatus.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public JobStatus.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >JobStatus.Builder<_B> copyOf(final JobStatus _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final JobStatus.Builder<_B> _newBuilder = new JobStatus.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static JobStatus.Builder<Void> copyExcept(final JobStatus _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static JobStatus.Builder<Void> copyOnly(final JobStatus _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final JobStatus _storedValue;
        private String status;

        public Builder(final _B _parentBuilder, final JobStatus _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.status = _other.status;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final JobStatus _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree statusPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("status"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(statusPropertyTree!= null):((statusPropertyTree == null)||(!statusPropertyTree.isLeaf())))) {
                        this.status = _other.status;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends JobStatus >_P init(final _P _product) {
            _product.status = this.status;
            return _product;
        }

        /**
         * Sets the new value of "status" (any previous value will be replaced)
         * 
         * @param status
         *     New value of the "status" property.
         */
        public JobStatus.Builder<_B> withStatus(final String status) {
            this.status = status;
            return this;
        }

        @Override
        public JobStatus build() {
            if (_storedValue == null) {
                return this.init(new JobStatus());
            } else {
                return ((JobStatus) _storedValue);
            }
        }

        public JobStatus.Builder<_B> copyOf(final JobStatus _other) {
            _other.copyTo(this);
            return this;
        }

        public JobStatus.Builder<_B> copyOf(final JobStatus.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends JobStatus.Selector<JobStatus.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static JobStatus.Select _root() {
            return new JobStatus.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, JobStatus.Selector<TRoot, TParent>> status = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.status!= null) {
                products.put("status", this.status.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, JobStatus.Selector<TRoot, TParent>> status() {
            return ((this.status == null)?this.status = new com.kscs.util.jaxb.Selector<TRoot, JobStatus.Selector<TRoot, TParent>>(this._root, this, "status"):this.status);
        }

    }

}
