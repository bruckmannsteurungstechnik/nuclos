
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for layout-links complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="layout-links"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="layout" type="{urn:org.nuclos.schema.rest}rest-link"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "layout-links", propOrder = {
    "layout"
})
public class LayoutLinks implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected RestLink layout;

    /**
     * Gets the value of the layout property.
     * 
     * @return
     *     possible object is
     *     {@link RestLink }
     *     
     */
    public RestLink getLayout() {
        return layout;
    }

    /**
     * Sets the value of the layout property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestLink }
     *     
     */
    public void setLayout(RestLink value) {
        this.layout = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            RestLink theLayout;
            theLayout = this.getLayout();
            strategy.appendField(locator, this, "layout", buffer, theLayout);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof LayoutLinks)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final LayoutLinks that = ((LayoutLinks) object);
        {
            RestLink lhsLayout;
            lhsLayout = this.getLayout();
            RestLink rhsLayout;
            rhsLayout = that.getLayout();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "layout", lhsLayout), LocatorUtils.property(thatLocator, "layout", rhsLayout), lhsLayout, rhsLayout)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            RestLink theLayout;
            theLayout = this.getLayout();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "layout", theLayout), currentHashCode, theLayout);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof LayoutLinks) {
            final LayoutLinks copy = ((LayoutLinks) draftCopy);
            if (this.layout!= null) {
                RestLink sourceLayout;
                sourceLayout = this.getLayout();
                RestLink copyLayout = ((RestLink) strategy.copy(LocatorUtils.property(locator, "layout", sourceLayout), sourceLayout));
                copy.setLayout(copyLayout);
            } else {
                copy.layout = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new LayoutLinks();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final LayoutLinks.Builder<_B> _other) {
        _other.layout = ((this.layout == null)?null:this.layout.newCopyBuilder(_other));
    }

    public<_B >LayoutLinks.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new LayoutLinks.Builder<_B>(_parentBuilder, this, true);
    }

    public LayoutLinks.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static LayoutLinks.Builder<Void> builder() {
        return new LayoutLinks.Builder<Void>(null, null, false);
    }

    public static<_B >LayoutLinks.Builder<_B> copyOf(final LayoutLinks _other) {
        final LayoutLinks.Builder<_B> _newBuilder = new LayoutLinks.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final LayoutLinks.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree layoutPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("layout"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(layoutPropertyTree!= null):((layoutPropertyTree == null)||(!layoutPropertyTree.isLeaf())))) {
            _other.layout = ((this.layout == null)?null:this.layout.newCopyBuilder(_other, layoutPropertyTree, _propertyTreeUse));
        }
    }

    public<_B >LayoutLinks.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new LayoutLinks.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public LayoutLinks.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >LayoutLinks.Builder<_B> copyOf(final LayoutLinks _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final LayoutLinks.Builder<_B> _newBuilder = new LayoutLinks.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static LayoutLinks.Builder<Void> copyExcept(final LayoutLinks _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static LayoutLinks.Builder<Void> copyOnly(final LayoutLinks _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final LayoutLinks _storedValue;
        private RestLink.Builder<LayoutLinks.Builder<_B>> layout;

        public Builder(final _B _parentBuilder, final LayoutLinks _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.layout = ((_other.layout == null)?null:_other.layout.newCopyBuilder(this));
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final LayoutLinks _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree layoutPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("layout"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(layoutPropertyTree!= null):((layoutPropertyTree == null)||(!layoutPropertyTree.isLeaf())))) {
                        this.layout = ((_other.layout == null)?null:_other.layout.newCopyBuilder(this, layoutPropertyTree, _propertyTreeUse));
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends LayoutLinks >_P init(final _P _product) {
            _product.layout = ((this.layout == null)?null:this.layout.build());
            return _product;
        }

        /**
         * Sets the new value of "layout" (any previous value will be replaced)
         * 
         * @param layout
         *     New value of the "layout" property.
         */
        public LayoutLinks.Builder<_B> withLayout(final RestLink layout) {
            this.layout = ((layout == null)?null:new RestLink.Builder<LayoutLinks.Builder<_B>>(this, layout, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "layout" property.
         * Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "layout" property.
         *     Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         */
        public RestLink.Builder<? extends LayoutLinks.Builder<_B>> withLayout() {
            if (this.layout!= null) {
                return this.layout;
            }
            return this.layout = new RestLink.Builder<LayoutLinks.Builder<_B>>(this, null, false);
        }

        @Override
        public LayoutLinks build() {
            if (_storedValue == null) {
                return this.init(new LayoutLinks());
            } else {
                return ((LayoutLinks) _storedValue);
            }
        }

        public LayoutLinks.Builder<_B> copyOf(final LayoutLinks _other) {
            _other.copyTo(this);
            return this;
        }

        public LayoutLinks.Builder<_B> copyOf(final LayoutLinks.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends LayoutLinks.Selector<LayoutLinks.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static LayoutLinks.Select _root() {
            return new LayoutLinks.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private RestLink.Selector<TRoot, LayoutLinks.Selector<TRoot, TParent>> layout = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.layout!= null) {
                products.put("layout", this.layout.init());
            }
            return products;
        }

        public RestLink.Selector<TRoot, LayoutLinks.Selector<TRoot, TParent>> layout() {
            return ((this.layout == null)?this.layout = new RestLink.Selector<TRoot, LayoutLinks.Selector<TRoot, TParent>>(this._root, this, "layout"):this.layout);
        }

    }

}
