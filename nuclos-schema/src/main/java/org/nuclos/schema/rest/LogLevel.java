
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for log-level complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="log-level"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="level" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "log-level")
public class LogLevel implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "level")
    protected String level;

    /**
     * Gets the value of the level property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLevel() {
        return level;
    }

    /**
     * Sets the value of the level property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLevel(String value) {
        this.level = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theLevel;
            theLevel = this.getLevel();
            strategy.appendField(locator, this, "level", buffer, theLevel);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof LogLevel)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final LogLevel that = ((LogLevel) object);
        {
            String lhsLevel;
            lhsLevel = this.getLevel();
            String rhsLevel;
            rhsLevel = that.getLevel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "level", lhsLevel), LocatorUtils.property(thatLocator, "level", rhsLevel), lhsLevel, rhsLevel)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theLevel;
            theLevel = this.getLevel();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "level", theLevel), currentHashCode, theLevel);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof LogLevel) {
            final LogLevel copy = ((LogLevel) draftCopy);
            if (this.level!= null) {
                String sourceLevel;
                sourceLevel = this.getLevel();
                String copyLevel = ((String) strategy.copy(LocatorUtils.property(locator, "level", sourceLevel), sourceLevel));
                copy.setLevel(copyLevel);
            } else {
                copy.level = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new LogLevel();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final LogLevel.Builder<_B> _other) {
        _other.level = this.level;
    }

    public<_B >LogLevel.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new LogLevel.Builder<_B>(_parentBuilder, this, true);
    }

    public LogLevel.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static LogLevel.Builder<Void> builder() {
        return new LogLevel.Builder<Void>(null, null, false);
    }

    public static<_B >LogLevel.Builder<_B> copyOf(final LogLevel _other) {
        final LogLevel.Builder<_B> _newBuilder = new LogLevel.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final LogLevel.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree levelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("level"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(levelPropertyTree!= null):((levelPropertyTree == null)||(!levelPropertyTree.isLeaf())))) {
            _other.level = this.level;
        }
    }

    public<_B >LogLevel.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new LogLevel.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public LogLevel.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >LogLevel.Builder<_B> copyOf(final LogLevel _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final LogLevel.Builder<_B> _newBuilder = new LogLevel.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static LogLevel.Builder<Void> copyExcept(final LogLevel _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static LogLevel.Builder<Void> copyOnly(final LogLevel _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final LogLevel _storedValue;
        private String level;

        public Builder(final _B _parentBuilder, final LogLevel _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.level = _other.level;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final LogLevel _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree levelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("level"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(levelPropertyTree!= null):((levelPropertyTree == null)||(!levelPropertyTree.isLeaf())))) {
                        this.level = _other.level;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends LogLevel >_P init(final _P _product) {
            _product.level = this.level;
            return _product;
        }

        /**
         * Sets the new value of "level" (any previous value will be replaced)
         * 
         * @param level
         *     New value of the "level" property.
         */
        public LogLevel.Builder<_B> withLevel(final String level) {
            this.level = level;
            return this;
        }

        @Override
        public LogLevel build() {
            if (_storedValue == null) {
                return this.init(new LogLevel());
            } else {
                return ((LogLevel) _storedValue);
            }
        }

        public LogLevel.Builder<_B> copyOf(final LogLevel _other) {
            _other.copyTo(this);
            return this;
        }

        public LogLevel.Builder<_B> copyOf(final LogLevel.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends LogLevel.Selector<LogLevel.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static LogLevel.Select _root() {
            return new LogLevel.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, LogLevel.Selector<TRoot, TParent>> level = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.level!= null) {
                products.put("level", this.level.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, LogLevel.Selector<TRoot, TParent>> level() {
            return ((this.level == null)?this.level = new com.kscs.util.jaxb.Selector<TRoot, LogLevel.Selector<TRoot, TParent>>(this._root, this, "level"):this.level);
        }

    }

}
