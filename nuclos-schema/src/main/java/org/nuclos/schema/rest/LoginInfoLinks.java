
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for login-info-links complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="login-info-links"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="boMetas" type="{urn:org.nuclos.schema.rest}rest-link"/&gt;
 *         &lt;element name="menu" type="{urn:org.nuclos.schema.rest}rest-link"/&gt;
 *         &lt;element name="tasks" type="{urn:org.nuclos.schema.rest}rest-link"/&gt;
 *         &lt;element name="search" type="{urn:org.nuclos.schema.rest}rest-link"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "login-info-links", propOrder = {
    "boMetas",
    "menu",
    "tasks",
    "search"
})
public class LoginInfoLinks implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected RestLink boMetas;
    @XmlElement(required = true)
    protected RestLink menu;
    @XmlElement(required = true)
    protected RestLink tasks;
    @XmlElement(required = true)
    protected RestLink search;

    /**
     * Gets the value of the boMetas property.
     * 
     * @return
     *     possible object is
     *     {@link RestLink }
     *     
     */
    public RestLink getBoMetas() {
        return boMetas;
    }

    /**
     * Sets the value of the boMetas property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestLink }
     *     
     */
    public void setBoMetas(RestLink value) {
        this.boMetas = value;
    }

    /**
     * Gets the value of the menu property.
     * 
     * @return
     *     possible object is
     *     {@link RestLink }
     *     
     */
    public RestLink getMenu() {
        return menu;
    }

    /**
     * Sets the value of the menu property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestLink }
     *     
     */
    public void setMenu(RestLink value) {
        this.menu = value;
    }

    /**
     * Gets the value of the tasks property.
     * 
     * @return
     *     possible object is
     *     {@link RestLink }
     *     
     */
    public RestLink getTasks() {
        return tasks;
    }

    /**
     * Sets the value of the tasks property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestLink }
     *     
     */
    public void setTasks(RestLink value) {
        this.tasks = value;
    }

    /**
     * Gets the value of the search property.
     * 
     * @return
     *     possible object is
     *     {@link RestLink }
     *     
     */
    public RestLink getSearch() {
        return search;
    }

    /**
     * Sets the value of the search property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestLink }
     *     
     */
    public void setSearch(RestLink value) {
        this.search = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            RestLink theBoMetas;
            theBoMetas = this.getBoMetas();
            strategy.appendField(locator, this, "boMetas", buffer, theBoMetas);
        }
        {
            RestLink theMenu;
            theMenu = this.getMenu();
            strategy.appendField(locator, this, "menu", buffer, theMenu);
        }
        {
            RestLink theTasks;
            theTasks = this.getTasks();
            strategy.appendField(locator, this, "tasks", buffer, theTasks);
        }
        {
            RestLink theSearch;
            theSearch = this.getSearch();
            strategy.appendField(locator, this, "search", buffer, theSearch);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof LoginInfoLinks)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final LoginInfoLinks that = ((LoginInfoLinks) object);
        {
            RestLink lhsBoMetas;
            lhsBoMetas = this.getBoMetas();
            RestLink rhsBoMetas;
            rhsBoMetas = that.getBoMetas();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "boMetas", lhsBoMetas), LocatorUtils.property(thatLocator, "boMetas", rhsBoMetas), lhsBoMetas, rhsBoMetas)) {
                return false;
            }
        }
        {
            RestLink lhsMenu;
            lhsMenu = this.getMenu();
            RestLink rhsMenu;
            rhsMenu = that.getMenu();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "menu", lhsMenu), LocatorUtils.property(thatLocator, "menu", rhsMenu), lhsMenu, rhsMenu)) {
                return false;
            }
        }
        {
            RestLink lhsTasks;
            lhsTasks = this.getTasks();
            RestLink rhsTasks;
            rhsTasks = that.getTasks();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "tasks", lhsTasks), LocatorUtils.property(thatLocator, "tasks", rhsTasks), lhsTasks, rhsTasks)) {
                return false;
            }
        }
        {
            RestLink lhsSearch;
            lhsSearch = this.getSearch();
            RestLink rhsSearch;
            rhsSearch = that.getSearch();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "search", lhsSearch), LocatorUtils.property(thatLocator, "search", rhsSearch), lhsSearch, rhsSearch)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            RestLink theBoMetas;
            theBoMetas = this.getBoMetas();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "boMetas", theBoMetas), currentHashCode, theBoMetas);
        }
        {
            RestLink theMenu;
            theMenu = this.getMenu();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "menu", theMenu), currentHashCode, theMenu);
        }
        {
            RestLink theTasks;
            theTasks = this.getTasks();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "tasks", theTasks), currentHashCode, theTasks);
        }
        {
            RestLink theSearch;
            theSearch = this.getSearch();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "search", theSearch), currentHashCode, theSearch);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof LoginInfoLinks) {
            final LoginInfoLinks copy = ((LoginInfoLinks) draftCopy);
            if (this.boMetas!= null) {
                RestLink sourceBoMetas;
                sourceBoMetas = this.getBoMetas();
                RestLink copyBoMetas = ((RestLink) strategy.copy(LocatorUtils.property(locator, "boMetas", sourceBoMetas), sourceBoMetas));
                copy.setBoMetas(copyBoMetas);
            } else {
                copy.boMetas = null;
            }
            if (this.menu!= null) {
                RestLink sourceMenu;
                sourceMenu = this.getMenu();
                RestLink copyMenu = ((RestLink) strategy.copy(LocatorUtils.property(locator, "menu", sourceMenu), sourceMenu));
                copy.setMenu(copyMenu);
            } else {
                copy.menu = null;
            }
            if (this.tasks!= null) {
                RestLink sourceTasks;
                sourceTasks = this.getTasks();
                RestLink copyTasks = ((RestLink) strategy.copy(LocatorUtils.property(locator, "tasks", sourceTasks), sourceTasks));
                copy.setTasks(copyTasks);
            } else {
                copy.tasks = null;
            }
            if (this.search!= null) {
                RestLink sourceSearch;
                sourceSearch = this.getSearch();
                RestLink copySearch = ((RestLink) strategy.copy(LocatorUtils.property(locator, "search", sourceSearch), sourceSearch));
                copy.setSearch(copySearch);
            } else {
                copy.search = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new LoginInfoLinks();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final LoginInfoLinks.Builder<_B> _other) {
        _other.boMetas = ((this.boMetas == null)?null:this.boMetas.newCopyBuilder(_other));
        _other.menu = ((this.menu == null)?null:this.menu.newCopyBuilder(_other));
        _other.tasks = ((this.tasks == null)?null:this.tasks.newCopyBuilder(_other));
        _other.search = ((this.search == null)?null:this.search.newCopyBuilder(_other));
    }

    public<_B >LoginInfoLinks.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new LoginInfoLinks.Builder<_B>(_parentBuilder, this, true);
    }

    public LoginInfoLinks.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static LoginInfoLinks.Builder<Void> builder() {
        return new LoginInfoLinks.Builder<Void>(null, null, false);
    }

    public static<_B >LoginInfoLinks.Builder<_B> copyOf(final LoginInfoLinks _other) {
        final LoginInfoLinks.Builder<_B> _newBuilder = new LoginInfoLinks.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final LoginInfoLinks.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree boMetasPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boMetas"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boMetasPropertyTree!= null):((boMetasPropertyTree == null)||(!boMetasPropertyTree.isLeaf())))) {
            _other.boMetas = ((this.boMetas == null)?null:this.boMetas.newCopyBuilder(_other, boMetasPropertyTree, _propertyTreeUse));
        }
        final PropertyTree menuPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("menu"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(menuPropertyTree!= null):((menuPropertyTree == null)||(!menuPropertyTree.isLeaf())))) {
            _other.menu = ((this.menu == null)?null:this.menu.newCopyBuilder(_other, menuPropertyTree, _propertyTreeUse));
        }
        final PropertyTree tasksPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("tasks"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(tasksPropertyTree!= null):((tasksPropertyTree == null)||(!tasksPropertyTree.isLeaf())))) {
            _other.tasks = ((this.tasks == null)?null:this.tasks.newCopyBuilder(_other, tasksPropertyTree, _propertyTreeUse));
        }
        final PropertyTree searchPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("search"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(searchPropertyTree!= null):((searchPropertyTree == null)||(!searchPropertyTree.isLeaf())))) {
            _other.search = ((this.search == null)?null:this.search.newCopyBuilder(_other, searchPropertyTree, _propertyTreeUse));
        }
    }

    public<_B >LoginInfoLinks.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new LoginInfoLinks.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public LoginInfoLinks.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >LoginInfoLinks.Builder<_B> copyOf(final LoginInfoLinks _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final LoginInfoLinks.Builder<_B> _newBuilder = new LoginInfoLinks.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static LoginInfoLinks.Builder<Void> copyExcept(final LoginInfoLinks _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static LoginInfoLinks.Builder<Void> copyOnly(final LoginInfoLinks _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final LoginInfoLinks _storedValue;
        private RestLink.Builder<LoginInfoLinks.Builder<_B>> boMetas;
        private RestLink.Builder<LoginInfoLinks.Builder<_B>> menu;
        private RestLink.Builder<LoginInfoLinks.Builder<_B>> tasks;
        private RestLink.Builder<LoginInfoLinks.Builder<_B>> search;

        public Builder(final _B _parentBuilder, final LoginInfoLinks _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.boMetas = ((_other.boMetas == null)?null:_other.boMetas.newCopyBuilder(this));
                    this.menu = ((_other.menu == null)?null:_other.menu.newCopyBuilder(this));
                    this.tasks = ((_other.tasks == null)?null:_other.tasks.newCopyBuilder(this));
                    this.search = ((_other.search == null)?null:_other.search.newCopyBuilder(this));
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final LoginInfoLinks _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree boMetasPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boMetas"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boMetasPropertyTree!= null):((boMetasPropertyTree == null)||(!boMetasPropertyTree.isLeaf())))) {
                        this.boMetas = ((_other.boMetas == null)?null:_other.boMetas.newCopyBuilder(this, boMetasPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree menuPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("menu"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(menuPropertyTree!= null):((menuPropertyTree == null)||(!menuPropertyTree.isLeaf())))) {
                        this.menu = ((_other.menu == null)?null:_other.menu.newCopyBuilder(this, menuPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree tasksPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("tasks"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(tasksPropertyTree!= null):((tasksPropertyTree == null)||(!tasksPropertyTree.isLeaf())))) {
                        this.tasks = ((_other.tasks == null)?null:_other.tasks.newCopyBuilder(this, tasksPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree searchPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("search"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(searchPropertyTree!= null):((searchPropertyTree == null)||(!searchPropertyTree.isLeaf())))) {
                        this.search = ((_other.search == null)?null:_other.search.newCopyBuilder(this, searchPropertyTree, _propertyTreeUse));
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends LoginInfoLinks >_P init(final _P _product) {
            _product.boMetas = ((this.boMetas == null)?null:this.boMetas.build());
            _product.menu = ((this.menu == null)?null:this.menu.build());
            _product.tasks = ((this.tasks == null)?null:this.tasks.build());
            _product.search = ((this.search == null)?null:this.search.build());
            return _product;
        }

        /**
         * Sets the new value of "boMetas" (any previous value will be replaced)
         * 
         * @param boMetas
         *     New value of the "boMetas" property.
         */
        public LoginInfoLinks.Builder<_B> withBoMetas(final RestLink boMetas) {
            this.boMetas = ((boMetas == null)?null:new RestLink.Builder<LoginInfoLinks.Builder<_B>>(this, boMetas, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "boMetas" property.
         * Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "boMetas" property.
         *     Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         */
        public RestLink.Builder<? extends LoginInfoLinks.Builder<_B>> withBoMetas() {
            if (this.boMetas!= null) {
                return this.boMetas;
            }
            return this.boMetas = new RestLink.Builder<LoginInfoLinks.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "menu" (any previous value will be replaced)
         * 
         * @param menu
         *     New value of the "menu" property.
         */
        public LoginInfoLinks.Builder<_B> withMenu(final RestLink menu) {
            this.menu = ((menu == null)?null:new RestLink.Builder<LoginInfoLinks.Builder<_B>>(this, menu, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "menu" property.
         * Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "menu" property.
         *     Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         */
        public RestLink.Builder<? extends LoginInfoLinks.Builder<_B>> withMenu() {
            if (this.menu!= null) {
                return this.menu;
            }
            return this.menu = new RestLink.Builder<LoginInfoLinks.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "tasks" (any previous value will be replaced)
         * 
         * @param tasks
         *     New value of the "tasks" property.
         */
        public LoginInfoLinks.Builder<_B> withTasks(final RestLink tasks) {
            this.tasks = ((tasks == null)?null:new RestLink.Builder<LoginInfoLinks.Builder<_B>>(this, tasks, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "tasks" property.
         * Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "tasks" property.
         *     Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         */
        public RestLink.Builder<? extends LoginInfoLinks.Builder<_B>> withTasks() {
            if (this.tasks!= null) {
                return this.tasks;
            }
            return this.tasks = new RestLink.Builder<LoginInfoLinks.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "search" (any previous value will be replaced)
         * 
         * @param search
         *     New value of the "search" property.
         */
        public LoginInfoLinks.Builder<_B> withSearch(final RestLink search) {
            this.search = ((search == null)?null:new RestLink.Builder<LoginInfoLinks.Builder<_B>>(this, search, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "search" property.
         * Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "search" property.
         *     Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         */
        public RestLink.Builder<? extends LoginInfoLinks.Builder<_B>> withSearch() {
            if (this.search!= null) {
                return this.search;
            }
            return this.search = new RestLink.Builder<LoginInfoLinks.Builder<_B>>(this, null, false);
        }

        @Override
        public LoginInfoLinks build() {
            if (_storedValue == null) {
                return this.init(new LoginInfoLinks());
            } else {
                return ((LoginInfoLinks) _storedValue);
            }
        }

        public LoginInfoLinks.Builder<_B> copyOf(final LoginInfoLinks _other) {
            _other.copyTo(this);
            return this;
        }

        public LoginInfoLinks.Builder<_B> copyOf(final LoginInfoLinks.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends LoginInfoLinks.Selector<LoginInfoLinks.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static LoginInfoLinks.Select _root() {
            return new LoginInfoLinks.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private RestLink.Selector<TRoot, LoginInfoLinks.Selector<TRoot, TParent>> boMetas = null;
        private RestLink.Selector<TRoot, LoginInfoLinks.Selector<TRoot, TParent>> menu = null;
        private RestLink.Selector<TRoot, LoginInfoLinks.Selector<TRoot, TParent>> tasks = null;
        private RestLink.Selector<TRoot, LoginInfoLinks.Selector<TRoot, TParent>> search = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.boMetas!= null) {
                products.put("boMetas", this.boMetas.init());
            }
            if (this.menu!= null) {
                products.put("menu", this.menu.init());
            }
            if (this.tasks!= null) {
                products.put("tasks", this.tasks.init());
            }
            if (this.search!= null) {
                products.put("search", this.search.init());
            }
            return products;
        }

        public RestLink.Selector<TRoot, LoginInfoLinks.Selector<TRoot, TParent>> boMetas() {
            return ((this.boMetas == null)?this.boMetas = new RestLink.Selector<TRoot, LoginInfoLinks.Selector<TRoot, TParent>>(this._root, this, "boMetas"):this.boMetas);
        }

        public RestLink.Selector<TRoot, LoginInfoLinks.Selector<TRoot, TParent>> menu() {
            return ((this.menu == null)?this.menu = new RestLink.Selector<TRoot, LoginInfoLinks.Selector<TRoot, TParent>>(this._root, this, "menu"):this.menu);
        }

        public RestLink.Selector<TRoot, LoginInfoLinks.Selector<TRoot, TParent>> tasks() {
            return ((this.tasks == null)?this.tasks = new RestLink.Selector<TRoot, LoginInfoLinks.Selector<TRoot, TParent>>(this._root, this, "tasks"):this.tasks);
        }

        public RestLink.Selector<TRoot, LoginInfoLinks.Selector<TRoot, TParent>> search() {
            return ((this.search == null)?this.search = new RestLink.Selector<TRoot, LoginInfoLinks.Selector<TRoot, TParent>>(this._root, this, "search"):this.search);
        }

    }

}
