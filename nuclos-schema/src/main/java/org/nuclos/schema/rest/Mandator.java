
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for mandator complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mandator"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="links" type="{urn:org.nuclos.schema.rest}mandator-links"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="mandatorId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="mandatorLevelId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="path" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="color" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mandator", propOrder = {
    "links"
})
public class Mandator implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected MandatorLinks links;
    @XmlAttribute(name = "mandatorId", required = true)
    protected String mandatorId;
    @XmlAttribute(name = "mandatorLevelId", required = true)
    protected String mandatorLevelId;
    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "path", required = true)
    protected String path;
    @XmlAttribute(name = "color")
    protected String color;

    /**
     * Gets the value of the links property.
     * 
     * @return
     *     possible object is
     *     {@link MandatorLinks }
     *     
     */
    public MandatorLinks getLinks() {
        return links;
    }

    /**
     * Sets the value of the links property.
     * 
     * @param value
     *     allowed object is
     *     {@link MandatorLinks }
     *     
     */
    public void setLinks(MandatorLinks value) {
        this.links = value;
    }

    /**
     * Gets the value of the mandatorId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMandatorId() {
        return mandatorId;
    }

    /**
     * Sets the value of the mandatorId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMandatorId(String value) {
        this.mandatorId = value;
    }

    /**
     * Gets the value of the mandatorLevelId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMandatorLevelId() {
        return mandatorLevelId;
    }

    /**
     * Sets the value of the mandatorLevelId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMandatorLevelId(String value) {
        this.mandatorLevelId = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the path property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPath() {
        return path;
    }

    /**
     * Sets the value of the path property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPath(String value) {
        this.path = value;
    }

    /**
     * Gets the value of the color property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColor() {
        return color;
    }

    /**
     * Sets the value of the color property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColor(String value) {
        this.color = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            MandatorLinks theLinks;
            theLinks = this.getLinks();
            strategy.appendField(locator, this, "links", buffer, theLinks);
        }
        {
            String theMandatorId;
            theMandatorId = this.getMandatorId();
            strategy.appendField(locator, this, "mandatorId", buffer, theMandatorId);
        }
        {
            String theMandatorLevelId;
            theMandatorLevelId = this.getMandatorLevelId();
            strategy.appendField(locator, this, "mandatorLevelId", buffer, theMandatorLevelId);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String thePath;
            thePath = this.getPath();
            strategy.appendField(locator, this, "path", buffer, thePath);
        }
        {
            String theColor;
            theColor = this.getColor();
            strategy.appendField(locator, this, "color", buffer, theColor);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Mandator)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Mandator that = ((Mandator) object);
        {
            MandatorLinks lhsLinks;
            lhsLinks = this.getLinks();
            MandatorLinks rhsLinks;
            rhsLinks = that.getLinks();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "links", lhsLinks), LocatorUtils.property(thatLocator, "links", rhsLinks), lhsLinks, rhsLinks)) {
                return false;
            }
        }
        {
            String lhsMandatorId;
            lhsMandatorId = this.getMandatorId();
            String rhsMandatorId;
            rhsMandatorId = that.getMandatorId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "mandatorId", lhsMandatorId), LocatorUtils.property(thatLocator, "mandatorId", rhsMandatorId), lhsMandatorId, rhsMandatorId)) {
                return false;
            }
        }
        {
            String lhsMandatorLevelId;
            lhsMandatorLevelId = this.getMandatorLevelId();
            String rhsMandatorLevelId;
            rhsMandatorLevelId = that.getMandatorLevelId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "mandatorLevelId", lhsMandatorLevelId), LocatorUtils.property(thatLocator, "mandatorLevelId", rhsMandatorLevelId), lhsMandatorLevelId, rhsMandatorLevelId)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsPath;
            lhsPath = this.getPath();
            String rhsPath;
            rhsPath = that.getPath();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "path", lhsPath), LocatorUtils.property(thatLocator, "path", rhsPath), lhsPath, rhsPath)) {
                return false;
            }
        }
        {
            String lhsColor;
            lhsColor = this.getColor();
            String rhsColor;
            rhsColor = that.getColor();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "color", lhsColor), LocatorUtils.property(thatLocator, "color", rhsColor), lhsColor, rhsColor)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            MandatorLinks theLinks;
            theLinks = this.getLinks();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "links", theLinks), currentHashCode, theLinks);
        }
        {
            String theMandatorId;
            theMandatorId = this.getMandatorId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "mandatorId", theMandatorId), currentHashCode, theMandatorId);
        }
        {
            String theMandatorLevelId;
            theMandatorLevelId = this.getMandatorLevelId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "mandatorLevelId", theMandatorLevelId), currentHashCode, theMandatorLevelId);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String thePath;
            thePath = this.getPath();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "path", thePath), currentHashCode, thePath);
        }
        {
            String theColor;
            theColor = this.getColor();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "color", theColor), currentHashCode, theColor);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Mandator) {
            final Mandator copy = ((Mandator) draftCopy);
            if (this.links!= null) {
                MandatorLinks sourceLinks;
                sourceLinks = this.getLinks();
                MandatorLinks copyLinks = ((MandatorLinks) strategy.copy(LocatorUtils.property(locator, "links", sourceLinks), sourceLinks));
                copy.setLinks(copyLinks);
            } else {
                copy.links = null;
            }
            if (this.mandatorId!= null) {
                String sourceMandatorId;
                sourceMandatorId = this.getMandatorId();
                String copyMandatorId = ((String) strategy.copy(LocatorUtils.property(locator, "mandatorId", sourceMandatorId), sourceMandatorId));
                copy.setMandatorId(copyMandatorId);
            } else {
                copy.mandatorId = null;
            }
            if (this.mandatorLevelId!= null) {
                String sourceMandatorLevelId;
                sourceMandatorLevelId = this.getMandatorLevelId();
                String copyMandatorLevelId = ((String) strategy.copy(LocatorUtils.property(locator, "mandatorLevelId", sourceMandatorLevelId), sourceMandatorLevelId));
                copy.setMandatorLevelId(copyMandatorLevelId);
            } else {
                copy.mandatorLevelId = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.path!= null) {
                String sourcePath;
                sourcePath = this.getPath();
                String copyPath = ((String) strategy.copy(LocatorUtils.property(locator, "path", sourcePath), sourcePath));
                copy.setPath(copyPath);
            } else {
                copy.path = null;
            }
            if (this.color!= null) {
                String sourceColor;
                sourceColor = this.getColor();
                String copyColor = ((String) strategy.copy(LocatorUtils.property(locator, "color", sourceColor), sourceColor));
                copy.setColor(copyColor);
            } else {
                copy.color = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Mandator();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Mandator.Builder<_B> _other) {
        _other.links = ((this.links == null)?null:this.links.newCopyBuilder(_other));
        _other.mandatorId = this.mandatorId;
        _other.mandatorLevelId = this.mandatorLevelId;
        _other.name = this.name;
        _other.path = this.path;
        _other.color = this.color;
    }

    public<_B >Mandator.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Mandator.Builder<_B>(_parentBuilder, this, true);
    }

    public Mandator.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Mandator.Builder<Void> builder() {
        return new Mandator.Builder<Void>(null, null, false);
    }

    public static<_B >Mandator.Builder<_B> copyOf(final Mandator _other) {
        final Mandator.Builder<_B> _newBuilder = new Mandator.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Mandator.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree linksPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("links"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(linksPropertyTree!= null):((linksPropertyTree == null)||(!linksPropertyTree.isLeaf())))) {
            _other.links = ((this.links == null)?null:this.links.newCopyBuilder(_other, linksPropertyTree, _propertyTreeUse));
        }
        final PropertyTree mandatorIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("mandatorId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(mandatorIdPropertyTree!= null):((mandatorIdPropertyTree == null)||(!mandatorIdPropertyTree.isLeaf())))) {
            _other.mandatorId = this.mandatorId;
        }
        final PropertyTree mandatorLevelIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("mandatorLevelId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(mandatorLevelIdPropertyTree!= null):((mandatorLevelIdPropertyTree == null)||(!mandatorLevelIdPropertyTree.isLeaf())))) {
            _other.mandatorLevelId = this.mandatorLevelId;
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree pathPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("path"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(pathPropertyTree!= null):((pathPropertyTree == null)||(!pathPropertyTree.isLeaf())))) {
            _other.path = this.path;
        }
        final PropertyTree colorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("color"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(colorPropertyTree!= null):((colorPropertyTree == null)||(!colorPropertyTree.isLeaf())))) {
            _other.color = this.color;
        }
    }

    public<_B >Mandator.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Mandator.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Mandator.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Mandator.Builder<_B> copyOf(final Mandator _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Mandator.Builder<_B> _newBuilder = new Mandator.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Mandator.Builder<Void> copyExcept(final Mandator _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Mandator.Builder<Void> copyOnly(final Mandator _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Mandator _storedValue;
        private MandatorLinks.Builder<Mandator.Builder<_B>> links;
        private String mandatorId;
        private String mandatorLevelId;
        private String name;
        private String path;
        private String color;

        public Builder(final _B _parentBuilder, final Mandator _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.links = ((_other.links == null)?null:_other.links.newCopyBuilder(this));
                    this.mandatorId = _other.mandatorId;
                    this.mandatorLevelId = _other.mandatorLevelId;
                    this.name = _other.name;
                    this.path = _other.path;
                    this.color = _other.color;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Mandator _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree linksPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("links"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(linksPropertyTree!= null):((linksPropertyTree == null)||(!linksPropertyTree.isLeaf())))) {
                        this.links = ((_other.links == null)?null:_other.links.newCopyBuilder(this, linksPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree mandatorIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("mandatorId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(mandatorIdPropertyTree!= null):((mandatorIdPropertyTree == null)||(!mandatorIdPropertyTree.isLeaf())))) {
                        this.mandatorId = _other.mandatorId;
                    }
                    final PropertyTree mandatorLevelIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("mandatorLevelId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(mandatorLevelIdPropertyTree!= null):((mandatorLevelIdPropertyTree == null)||(!mandatorLevelIdPropertyTree.isLeaf())))) {
                        this.mandatorLevelId = _other.mandatorLevelId;
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree pathPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("path"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(pathPropertyTree!= null):((pathPropertyTree == null)||(!pathPropertyTree.isLeaf())))) {
                        this.path = _other.path;
                    }
                    final PropertyTree colorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("color"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(colorPropertyTree!= null):((colorPropertyTree == null)||(!colorPropertyTree.isLeaf())))) {
                        this.color = _other.color;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Mandator >_P init(final _P _product) {
            _product.links = ((this.links == null)?null:this.links.build());
            _product.mandatorId = this.mandatorId;
            _product.mandatorLevelId = this.mandatorLevelId;
            _product.name = this.name;
            _product.path = this.path;
            _product.color = this.color;
            return _product;
        }

        /**
         * Sets the new value of "links" (any previous value will be replaced)
         * 
         * @param links
         *     New value of the "links" property.
         */
        public Mandator.Builder<_B> withLinks(final MandatorLinks links) {
            this.links = ((links == null)?null:new MandatorLinks.Builder<Mandator.Builder<_B>>(this, links, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "links" property.
         * Use {@link org.nuclos.schema.rest.MandatorLinks.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "links" property.
         *     Use {@link org.nuclos.schema.rest.MandatorLinks.Builder#end()} to return to the current builder.
         */
        public MandatorLinks.Builder<? extends Mandator.Builder<_B>> withLinks() {
            if (this.links!= null) {
                return this.links;
            }
            return this.links = new MandatorLinks.Builder<Mandator.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "mandatorId" (any previous value will be replaced)
         * 
         * @param mandatorId
         *     New value of the "mandatorId" property.
         */
        public Mandator.Builder<_B> withMandatorId(final String mandatorId) {
            this.mandatorId = mandatorId;
            return this;
        }

        /**
         * Sets the new value of "mandatorLevelId" (any previous value will be replaced)
         * 
         * @param mandatorLevelId
         *     New value of the "mandatorLevelId" property.
         */
        public Mandator.Builder<_B> withMandatorLevelId(final String mandatorLevelId) {
            this.mandatorLevelId = mandatorLevelId;
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public Mandator.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "path" (any previous value will be replaced)
         * 
         * @param path
         *     New value of the "path" property.
         */
        public Mandator.Builder<_B> withPath(final String path) {
            this.path = path;
            return this;
        }

        /**
         * Sets the new value of "color" (any previous value will be replaced)
         * 
         * @param color
         *     New value of the "color" property.
         */
        public Mandator.Builder<_B> withColor(final String color) {
            this.color = color;
            return this;
        }

        @Override
        public Mandator build() {
            if (_storedValue == null) {
                return this.init(new Mandator());
            } else {
                return ((Mandator) _storedValue);
            }
        }

        public Mandator.Builder<_B> copyOf(final Mandator _other) {
            _other.copyTo(this);
            return this;
        }

        public Mandator.Builder<_B> copyOf(final Mandator.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Mandator.Selector<Mandator.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Mandator.Select _root() {
            return new Mandator.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private MandatorLinks.Selector<TRoot, Mandator.Selector<TRoot, TParent>> links = null;
        private com.kscs.util.jaxb.Selector<TRoot, Mandator.Selector<TRoot, TParent>> mandatorId = null;
        private com.kscs.util.jaxb.Selector<TRoot, Mandator.Selector<TRoot, TParent>> mandatorLevelId = null;
        private com.kscs.util.jaxb.Selector<TRoot, Mandator.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, Mandator.Selector<TRoot, TParent>> path = null;
        private com.kscs.util.jaxb.Selector<TRoot, Mandator.Selector<TRoot, TParent>> color = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.links!= null) {
                products.put("links", this.links.init());
            }
            if (this.mandatorId!= null) {
                products.put("mandatorId", this.mandatorId.init());
            }
            if (this.mandatorLevelId!= null) {
                products.put("mandatorLevelId", this.mandatorLevelId.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.path!= null) {
                products.put("path", this.path.init());
            }
            if (this.color!= null) {
                products.put("color", this.color.init());
            }
            return products;
        }

        public MandatorLinks.Selector<TRoot, Mandator.Selector<TRoot, TParent>> links() {
            return ((this.links == null)?this.links = new MandatorLinks.Selector<TRoot, Mandator.Selector<TRoot, TParent>>(this._root, this, "links"):this.links);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Mandator.Selector<TRoot, TParent>> mandatorId() {
            return ((this.mandatorId == null)?this.mandatorId = new com.kscs.util.jaxb.Selector<TRoot, Mandator.Selector<TRoot, TParent>>(this._root, this, "mandatorId"):this.mandatorId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Mandator.Selector<TRoot, TParent>> mandatorLevelId() {
            return ((this.mandatorLevelId == null)?this.mandatorLevelId = new com.kscs.util.jaxb.Selector<TRoot, Mandator.Selector<TRoot, TParent>>(this._root, this, "mandatorLevelId"):this.mandatorLevelId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Mandator.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, Mandator.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Mandator.Selector<TRoot, TParent>> path() {
            return ((this.path == null)?this.path = new com.kscs.util.jaxb.Selector<TRoot, Mandator.Selector<TRoot, TParent>>(this._root, this, "path"):this.path);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Mandator.Selector<TRoot, TParent>> color() {
            return ((this.color == null)?this.color = new com.kscs.util.jaxb.Selector<TRoot, Mandator.Selector<TRoot, TParent>>(this._root, this, "color"):this.color);
        }

    }

}
