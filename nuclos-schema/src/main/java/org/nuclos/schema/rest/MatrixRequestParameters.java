
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for matrix-request-parameters complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="matrix-request-parameters"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="entityMatrix" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entityMatrixValueType" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entityMatrixReferenceField" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entityX" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entityY" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entityFieldMatrixParent" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entityFieldMatrixXRefField" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="cellInputType" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="editable" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="boId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "matrix-request-parameters")
public class MatrixRequestParameters implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "entityMatrix")
    protected String entityMatrix;
    @XmlAttribute(name = "entityMatrixValueType")
    protected String entityMatrixValueType;
    @XmlAttribute(name = "entityMatrixReferenceField")
    protected String entityMatrixReferenceField;
    @XmlAttribute(name = "entityX")
    protected String entityX;
    @XmlAttribute(name = "entityY")
    protected String entityY;
    @XmlAttribute(name = "entityFieldMatrixParent")
    protected String entityFieldMatrixParent;
    @XmlAttribute(name = "entityFieldMatrixXRefField")
    protected String entityFieldMatrixXRefField;
    @XmlAttribute(name = "cellInputType")
    protected String cellInputType;
    @XmlAttribute(name = "editable")
    protected String editable;
    @XmlAttribute(name = "boId")
    protected String boId;

    /**
     * Gets the value of the entityMatrix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityMatrix() {
        return entityMatrix;
    }

    /**
     * Sets the value of the entityMatrix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityMatrix(String value) {
        this.entityMatrix = value;
    }

    /**
     * Gets the value of the entityMatrixValueType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityMatrixValueType() {
        return entityMatrixValueType;
    }

    /**
     * Sets the value of the entityMatrixValueType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityMatrixValueType(String value) {
        this.entityMatrixValueType = value;
    }

    /**
     * Gets the value of the entityMatrixReferenceField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityMatrixReferenceField() {
        return entityMatrixReferenceField;
    }

    /**
     * Sets the value of the entityMatrixReferenceField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityMatrixReferenceField(String value) {
        this.entityMatrixReferenceField = value;
    }

    /**
     * Gets the value of the entityX property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityX() {
        return entityX;
    }

    /**
     * Sets the value of the entityX property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityX(String value) {
        this.entityX = value;
    }

    /**
     * Gets the value of the entityY property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityY() {
        return entityY;
    }

    /**
     * Sets the value of the entityY property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityY(String value) {
        this.entityY = value;
    }

    /**
     * Gets the value of the entityFieldMatrixParent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityFieldMatrixParent() {
        return entityFieldMatrixParent;
    }

    /**
     * Sets the value of the entityFieldMatrixParent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityFieldMatrixParent(String value) {
        this.entityFieldMatrixParent = value;
    }

    /**
     * Gets the value of the entityFieldMatrixXRefField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityFieldMatrixXRefField() {
        return entityFieldMatrixXRefField;
    }

    /**
     * Sets the value of the entityFieldMatrixXRefField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityFieldMatrixXRefField(String value) {
        this.entityFieldMatrixXRefField = value;
    }

    /**
     * Gets the value of the cellInputType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCellInputType() {
        return cellInputType;
    }

    /**
     * Sets the value of the cellInputType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCellInputType(String value) {
        this.cellInputType = value;
    }

    /**
     * Gets the value of the editable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEditable() {
        return editable;
    }

    /**
     * Sets the value of the editable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEditable(String value) {
        this.editable = value;
    }

    /**
     * Gets the value of the boId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoId() {
        return boId;
    }

    /**
     * Sets the value of the boId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoId(String value) {
        this.boId = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theEntityMatrix;
            theEntityMatrix = this.getEntityMatrix();
            strategy.appendField(locator, this, "entityMatrix", buffer, theEntityMatrix);
        }
        {
            String theEntityMatrixValueType;
            theEntityMatrixValueType = this.getEntityMatrixValueType();
            strategy.appendField(locator, this, "entityMatrixValueType", buffer, theEntityMatrixValueType);
        }
        {
            String theEntityMatrixReferenceField;
            theEntityMatrixReferenceField = this.getEntityMatrixReferenceField();
            strategy.appendField(locator, this, "entityMatrixReferenceField", buffer, theEntityMatrixReferenceField);
        }
        {
            String theEntityX;
            theEntityX = this.getEntityX();
            strategy.appendField(locator, this, "entityX", buffer, theEntityX);
        }
        {
            String theEntityY;
            theEntityY = this.getEntityY();
            strategy.appendField(locator, this, "entityY", buffer, theEntityY);
        }
        {
            String theEntityFieldMatrixParent;
            theEntityFieldMatrixParent = this.getEntityFieldMatrixParent();
            strategy.appendField(locator, this, "entityFieldMatrixParent", buffer, theEntityFieldMatrixParent);
        }
        {
            String theEntityFieldMatrixXRefField;
            theEntityFieldMatrixXRefField = this.getEntityFieldMatrixXRefField();
            strategy.appendField(locator, this, "entityFieldMatrixXRefField", buffer, theEntityFieldMatrixXRefField);
        }
        {
            String theCellInputType;
            theCellInputType = this.getCellInputType();
            strategy.appendField(locator, this, "cellInputType", buffer, theCellInputType);
        }
        {
            String theEditable;
            theEditable = this.getEditable();
            strategy.appendField(locator, this, "editable", buffer, theEditable);
        }
        {
            String theBoId;
            theBoId = this.getBoId();
            strategy.appendField(locator, this, "boId", buffer, theBoId);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MatrixRequestParameters)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MatrixRequestParameters that = ((MatrixRequestParameters) object);
        {
            String lhsEntityMatrix;
            lhsEntityMatrix = this.getEntityMatrix();
            String rhsEntityMatrix;
            rhsEntityMatrix = that.getEntityMatrix();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityMatrix", lhsEntityMatrix), LocatorUtils.property(thatLocator, "entityMatrix", rhsEntityMatrix), lhsEntityMatrix, rhsEntityMatrix)) {
                return false;
            }
        }
        {
            String lhsEntityMatrixValueType;
            lhsEntityMatrixValueType = this.getEntityMatrixValueType();
            String rhsEntityMatrixValueType;
            rhsEntityMatrixValueType = that.getEntityMatrixValueType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityMatrixValueType", lhsEntityMatrixValueType), LocatorUtils.property(thatLocator, "entityMatrixValueType", rhsEntityMatrixValueType), lhsEntityMatrixValueType, rhsEntityMatrixValueType)) {
                return false;
            }
        }
        {
            String lhsEntityMatrixReferenceField;
            lhsEntityMatrixReferenceField = this.getEntityMatrixReferenceField();
            String rhsEntityMatrixReferenceField;
            rhsEntityMatrixReferenceField = that.getEntityMatrixReferenceField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityMatrixReferenceField", lhsEntityMatrixReferenceField), LocatorUtils.property(thatLocator, "entityMatrixReferenceField", rhsEntityMatrixReferenceField), lhsEntityMatrixReferenceField, rhsEntityMatrixReferenceField)) {
                return false;
            }
        }
        {
            String lhsEntityX;
            lhsEntityX = this.getEntityX();
            String rhsEntityX;
            rhsEntityX = that.getEntityX();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityX", lhsEntityX), LocatorUtils.property(thatLocator, "entityX", rhsEntityX), lhsEntityX, rhsEntityX)) {
                return false;
            }
        }
        {
            String lhsEntityY;
            lhsEntityY = this.getEntityY();
            String rhsEntityY;
            rhsEntityY = that.getEntityY();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityY", lhsEntityY), LocatorUtils.property(thatLocator, "entityY", rhsEntityY), lhsEntityY, rhsEntityY)) {
                return false;
            }
        }
        {
            String lhsEntityFieldMatrixParent;
            lhsEntityFieldMatrixParent = this.getEntityFieldMatrixParent();
            String rhsEntityFieldMatrixParent;
            rhsEntityFieldMatrixParent = that.getEntityFieldMatrixParent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityFieldMatrixParent", lhsEntityFieldMatrixParent), LocatorUtils.property(thatLocator, "entityFieldMatrixParent", rhsEntityFieldMatrixParent), lhsEntityFieldMatrixParent, rhsEntityFieldMatrixParent)) {
                return false;
            }
        }
        {
            String lhsEntityFieldMatrixXRefField;
            lhsEntityFieldMatrixXRefField = this.getEntityFieldMatrixXRefField();
            String rhsEntityFieldMatrixXRefField;
            rhsEntityFieldMatrixXRefField = that.getEntityFieldMatrixXRefField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityFieldMatrixXRefField", lhsEntityFieldMatrixXRefField), LocatorUtils.property(thatLocator, "entityFieldMatrixXRefField", rhsEntityFieldMatrixXRefField), lhsEntityFieldMatrixXRefField, rhsEntityFieldMatrixXRefField)) {
                return false;
            }
        }
        {
            String lhsCellInputType;
            lhsCellInputType = this.getCellInputType();
            String rhsCellInputType;
            rhsCellInputType = that.getCellInputType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "cellInputType", lhsCellInputType), LocatorUtils.property(thatLocator, "cellInputType", rhsCellInputType), lhsCellInputType, rhsCellInputType)) {
                return false;
            }
        }
        {
            String lhsEditable;
            lhsEditable = this.getEditable();
            String rhsEditable;
            rhsEditable = that.getEditable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "editable", lhsEditable), LocatorUtils.property(thatLocator, "editable", rhsEditable), lhsEditable, rhsEditable)) {
                return false;
            }
        }
        {
            String lhsBoId;
            lhsBoId = this.getBoId();
            String rhsBoId;
            rhsBoId = that.getBoId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "boId", lhsBoId), LocatorUtils.property(thatLocator, "boId", rhsBoId), lhsBoId, rhsBoId)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theEntityMatrix;
            theEntityMatrix = this.getEntityMatrix();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityMatrix", theEntityMatrix), currentHashCode, theEntityMatrix);
        }
        {
            String theEntityMatrixValueType;
            theEntityMatrixValueType = this.getEntityMatrixValueType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityMatrixValueType", theEntityMatrixValueType), currentHashCode, theEntityMatrixValueType);
        }
        {
            String theEntityMatrixReferenceField;
            theEntityMatrixReferenceField = this.getEntityMatrixReferenceField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityMatrixReferenceField", theEntityMatrixReferenceField), currentHashCode, theEntityMatrixReferenceField);
        }
        {
            String theEntityX;
            theEntityX = this.getEntityX();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityX", theEntityX), currentHashCode, theEntityX);
        }
        {
            String theEntityY;
            theEntityY = this.getEntityY();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityY", theEntityY), currentHashCode, theEntityY);
        }
        {
            String theEntityFieldMatrixParent;
            theEntityFieldMatrixParent = this.getEntityFieldMatrixParent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityFieldMatrixParent", theEntityFieldMatrixParent), currentHashCode, theEntityFieldMatrixParent);
        }
        {
            String theEntityFieldMatrixXRefField;
            theEntityFieldMatrixXRefField = this.getEntityFieldMatrixXRefField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityFieldMatrixXRefField", theEntityFieldMatrixXRefField), currentHashCode, theEntityFieldMatrixXRefField);
        }
        {
            String theCellInputType;
            theCellInputType = this.getCellInputType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "cellInputType", theCellInputType), currentHashCode, theCellInputType);
        }
        {
            String theEditable;
            theEditable = this.getEditable();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "editable", theEditable), currentHashCode, theEditable);
        }
        {
            String theBoId;
            theBoId = this.getBoId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "boId", theBoId), currentHashCode, theBoId);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof MatrixRequestParameters) {
            final MatrixRequestParameters copy = ((MatrixRequestParameters) draftCopy);
            if (this.entityMatrix!= null) {
                String sourceEntityMatrix;
                sourceEntityMatrix = this.getEntityMatrix();
                String copyEntityMatrix = ((String) strategy.copy(LocatorUtils.property(locator, "entityMatrix", sourceEntityMatrix), sourceEntityMatrix));
                copy.setEntityMatrix(copyEntityMatrix);
            } else {
                copy.entityMatrix = null;
            }
            if (this.entityMatrixValueType!= null) {
                String sourceEntityMatrixValueType;
                sourceEntityMatrixValueType = this.getEntityMatrixValueType();
                String copyEntityMatrixValueType = ((String) strategy.copy(LocatorUtils.property(locator, "entityMatrixValueType", sourceEntityMatrixValueType), sourceEntityMatrixValueType));
                copy.setEntityMatrixValueType(copyEntityMatrixValueType);
            } else {
                copy.entityMatrixValueType = null;
            }
            if (this.entityMatrixReferenceField!= null) {
                String sourceEntityMatrixReferenceField;
                sourceEntityMatrixReferenceField = this.getEntityMatrixReferenceField();
                String copyEntityMatrixReferenceField = ((String) strategy.copy(LocatorUtils.property(locator, "entityMatrixReferenceField", sourceEntityMatrixReferenceField), sourceEntityMatrixReferenceField));
                copy.setEntityMatrixReferenceField(copyEntityMatrixReferenceField);
            } else {
                copy.entityMatrixReferenceField = null;
            }
            if (this.entityX!= null) {
                String sourceEntityX;
                sourceEntityX = this.getEntityX();
                String copyEntityX = ((String) strategy.copy(LocatorUtils.property(locator, "entityX", sourceEntityX), sourceEntityX));
                copy.setEntityX(copyEntityX);
            } else {
                copy.entityX = null;
            }
            if (this.entityY!= null) {
                String sourceEntityY;
                sourceEntityY = this.getEntityY();
                String copyEntityY = ((String) strategy.copy(LocatorUtils.property(locator, "entityY", sourceEntityY), sourceEntityY));
                copy.setEntityY(copyEntityY);
            } else {
                copy.entityY = null;
            }
            if (this.entityFieldMatrixParent!= null) {
                String sourceEntityFieldMatrixParent;
                sourceEntityFieldMatrixParent = this.getEntityFieldMatrixParent();
                String copyEntityFieldMatrixParent = ((String) strategy.copy(LocatorUtils.property(locator, "entityFieldMatrixParent", sourceEntityFieldMatrixParent), sourceEntityFieldMatrixParent));
                copy.setEntityFieldMatrixParent(copyEntityFieldMatrixParent);
            } else {
                copy.entityFieldMatrixParent = null;
            }
            if (this.entityFieldMatrixXRefField!= null) {
                String sourceEntityFieldMatrixXRefField;
                sourceEntityFieldMatrixXRefField = this.getEntityFieldMatrixXRefField();
                String copyEntityFieldMatrixXRefField = ((String) strategy.copy(LocatorUtils.property(locator, "entityFieldMatrixXRefField", sourceEntityFieldMatrixXRefField), sourceEntityFieldMatrixXRefField));
                copy.setEntityFieldMatrixXRefField(copyEntityFieldMatrixXRefField);
            } else {
                copy.entityFieldMatrixXRefField = null;
            }
            if (this.cellInputType!= null) {
                String sourceCellInputType;
                sourceCellInputType = this.getCellInputType();
                String copyCellInputType = ((String) strategy.copy(LocatorUtils.property(locator, "cellInputType", sourceCellInputType), sourceCellInputType));
                copy.setCellInputType(copyCellInputType);
            } else {
                copy.cellInputType = null;
            }
            if (this.editable!= null) {
                String sourceEditable;
                sourceEditable = this.getEditable();
                String copyEditable = ((String) strategy.copy(LocatorUtils.property(locator, "editable", sourceEditable), sourceEditable));
                copy.setEditable(copyEditable);
            } else {
                copy.editable = null;
            }
            if (this.boId!= null) {
                String sourceBoId;
                sourceBoId = this.getBoId();
                String copyBoId = ((String) strategy.copy(LocatorUtils.property(locator, "boId", sourceBoId), sourceBoId));
                copy.setBoId(copyBoId);
            } else {
                copy.boId = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MatrixRequestParameters();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final MatrixRequestParameters.Builder<_B> _other) {
        _other.entityMatrix = this.entityMatrix;
        _other.entityMatrixValueType = this.entityMatrixValueType;
        _other.entityMatrixReferenceField = this.entityMatrixReferenceField;
        _other.entityX = this.entityX;
        _other.entityY = this.entityY;
        _other.entityFieldMatrixParent = this.entityFieldMatrixParent;
        _other.entityFieldMatrixXRefField = this.entityFieldMatrixXRefField;
        _other.cellInputType = this.cellInputType;
        _other.editable = this.editable;
        _other.boId = this.boId;
    }

    public<_B >MatrixRequestParameters.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new MatrixRequestParameters.Builder<_B>(_parentBuilder, this, true);
    }

    public MatrixRequestParameters.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static MatrixRequestParameters.Builder<Void> builder() {
        return new MatrixRequestParameters.Builder<Void>(null, null, false);
    }

    public static<_B >MatrixRequestParameters.Builder<_B> copyOf(final MatrixRequestParameters _other) {
        final MatrixRequestParameters.Builder<_B> _newBuilder = new MatrixRequestParameters.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final MatrixRequestParameters.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree entityMatrixPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMatrix"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMatrixPropertyTree!= null):((entityMatrixPropertyTree == null)||(!entityMatrixPropertyTree.isLeaf())))) {
            _other.entityMatrix = this.entityMatrix;
        }
        final PropertyTree entityMatrixValueTypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMatrixValueType"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMatrixValueTypePropertyTree!= null):((entityMatrixValueTypePropertyTree == null)||(!entityMatrixValueTypePropertyTree.isLeaf())))) {
            _other.entityMatrixValueType = this.entityMatrixValueType;
        }
        final PropertyTree entityMatrixReferenceFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMatrixReferenceField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMatrixReferenceFieldPropertyTree!= null):((entityMatrixReferenceFieldPropertyTree == null)||(!entityMatrixReferenceFieldPropertyTree.isLeaf())))) {
            _other.entityMatrixReferenceField = this.entityMatrixReferenceField;
        }
        final PropertyTree entityXPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityX"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXPropertyTree!= null):((entityXPropertyTree == null)||(!entityXPropertyTree.isLeaf())))) {
            _other.entityX = this.entityX;
        }
        final PropertyTree entityYPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityY"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityYPropertyTree!= null):((entityYPropertyTree == null)||(!entityYPropertyTree.isLeaf())))) {
            _other.entityY = this.entityY;
        }
        final PropertyTree entityFieldMatrixParentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityFieldMatrixParent"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityFieldMatrixParentPropertyTree!= null):((entityFieldMatrixParentPropertyTree == null)||(!entityFieldMatrixParentPropertyTree.isLeaf())))) {
            _other.entityFieldMatrixParent = this.entityFieldMatrixParent;
        }
        final PropertyTree entityFieldMatrixXRefFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityFieldMatrixXRefField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityFieldMatrixXRefFieldPropertyTree!= null):((entityFieldMatrixXRefFieldPropertyTree == null)||(!entityFieldMatrixXRefFieldPropertyTree.isLeaf())))) {
            _other.entityFieldMatrixXRefField = this.entityFieldMatrixXRefField;
        }
        final PropertyTree cellInputTypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("cellInputType"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(cellInputTypePropertyTree!= null):((cellInputTypePropertyTree == null)||(!cellInputTypePropertyTree.isLeaf())))) {
            _other.cellInputType = this.cellInputType;
        }
        final PropertyTree editablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("editable"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(editablePropertyTree!= null):((editablePropertyTree == null)||(!editablePropertyTree.isLeaf())))) {
            _other.editable = this.editable;
        }
        final PropertyTree boIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boIdPropertyTree!= null):((boIdPropertyTree == null)||(!boIdPropertyTree.isLeaf())))) {
            _other.boId = this.boId;
        }
    }

    public<_B >MatrixRequestParameters.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new MatrixRequestParameters.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public MatrixRequestParameters.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >MatrixRequestParameters.Builder<_B> copyOf(final MatrixRequestParameters _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final MatrixRequestParameters.Builder<_B> _newBuilder = new MatrixRequestParameters.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static MatrixRequestParameters.Builder<Void> copyExcept(final MatrixRequestParameters _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static MatrixRequestParameters.Builder<Void> copyOnly(final MatrixRequestParameters _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final MatrixRequestParameters _storedValue;
        private String entityMatrix;
        private String entityMatrixValueType;
        private String entityMatrixReferenceField;
        private String entityX;
        private String entityY;
        private String entityFieldMatrixParent;
        private String entityFieldMatrixXRefField;
        private String cellInputType;
        private String editable;
        private String boId;

        public Builder(final _B _parentBuilder, final MatrixRequestParameters _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.entityMatrix = _other.entityMatrix;
                    this.entityMatrixValueType = _other.entityMatrixValueType;
                    this.entityMatrixReferenceField = _other.entityMatrixReferenceField;
                    this.entityX = _other.entityX;
                    this.entityY = _other.entityY;
                    this.entityFieldMatrixParent = _other.entityFieldMatrixParent;
                    this.entityFieldMatrixXRefField = _other.entityFieldMatrixXRefField;
                    this.cellInputType = _other.cellInputType;
                    this.editable = _other.editable;
                    this.boId = _other.boId;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final MatrixRequestParameters _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree entityMatrixPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMatrix"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMatrixPropertyTree!= null):((entityMatrixPropertyTree == null)||(!entityMatrixPropertyTree.isLeaf())))) {
                        this.entityMatrix = _other.entityMatrix;
                    }
                    final PropertyTree entityMatrixValueTypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMatrixValueType"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMatrixValueTypePropertyTree!= null):((entityMatrixValueTypePropertyTree == null)||(!entityMatrixValueTypePropertyTree.isLeaf())))) {
                        this.entityMatrixValueType = _other.entityMatrixValueType;
                    }
                    final PropertyTree entityMatrixReferenceFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMatrixReferenceField"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMatrixReferenceFieldPropertyTree!= null):((entityMatrixReferenceFieldPropertyTree == null)||(!entityMatrixReferenceFieldPropertyTree.isLeaf())))) {
                        this.entityMatrixReferenceField = _other.entityMatrixReferenceField;
                    }
                    final PropertyTree entityXPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityX"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityXPropertyTree!= null):((entityXPropertyTree == null)||(!entityXPropertyTree.isLeaf())))) {
                        this.entityX = _other.entityX;
                    }
                    final PropertyTree entityYPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityY"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityYPropertyTree!= null):((entityYPropertyTree == null)||(!entityYPropertyTree.isLeaf())))) {
                        this.entityY = _other.entityY;
                    }
                    final PropertyTree entityFieldMatrixParentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityFieldMatrixParent"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityFieldMatrixParentPropertyTree!= null):((entityFieldMatrixParentPropertyTree == null)||(!entityFieldMatrixParentPropertyTree.isLeaf())))) {
                        this.entityFieldMatrixParent = _other.entityFieldMatrixParent;
                    }
                    final PropertyTree entityFieldMatrixXRefFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityFieldMatrixXRefField"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityFieldMatrixXRefFieldPropertyTree!= null):((entityFieldMatrixXRefFieldPropertyTree == null)||(!entityFieldMatrixXRefFieldPropertyTree.isLeaf())))) {
                        this.entityFieldMatrixXRefField = _other.entityFieldMatrixXRefField;
                    }
                    final PropertyTree cellInputTypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("cellInputType"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(cellInputTypePropertyTree!= null):((cellInputTypePropertyTree == null)||(!cellInputTypePropertyTree.isLeaf())))) {
                        this.cellInputType = _other.cellInputType;
                    }
                    final PropertyTree editablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("editable"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(editablePropertyTree!= null):((editablePropertyTree == null)||(!editablePropertyTree.isLeaf())))) {
                        this.editable = _other.editable;
                    }
                    final PropertyTree boIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boIdPropertyTree!= null):((boIdPropertyTree == null)||(!boIdPropertyTree.isLeaf())))) {
                        this.boId = _other.boId;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends MatrixRequestParameters >_P init(final _P _product) {
            _product.entityMatrix = this.entityMatrix;
            _product.entityMatrixValueType = this.entityMatrixValueType;
            _product.entityMatrixReferenceField = this.entityMatrixReferenceField;
            _product.entityX = this.entityX;
            _product.entityY = this.entityY;
            _product.entityFieldMatrixParent = this.entityFieldMatrixParent;
            _product.entityFieldMatrixXRefField = this.entityFieldMatrixXRefField;
            _product.cellInputType = this.cellInputType;
            _product.editable = this.editable;
            _product.boId = this.boId;
            return _product;
        }

        /**
         * Sets the new value of "entityMatrix" (any previous value will be replaced)
         * 
         * @param entityMatrix
         *     New value of the "entityMatrix" property.
         */
        public MatrixRequestParameters.Builder<_B> withEntityMatrix(final String entityMatrix) {
            this.entityMatrix = entityMatrix;
            return this;
        }

        /**
         * Sets the new value of "entityMatrixValueType" (any previous value will be replaced)
         * 
         * @param entityMatrixValueType
         *     New value of the "entityMatrixValueType" property.
         */
        public MatrixRequestParameters.Builder<_B> withEntityMatrixValueType(final String entityMatrixValueType) {
            this.entityMatrixValueType = entityMatrixValueType;
            return this;
        }

        /**
         * Sets the new value of "entityMatrixReferenceField" (any previous value will be replaced)
         * 
         * @param entityMatrixReferenceField
         *     New value of the "entityMatrixReferenceField" property.
         */
        public MatrixRequestParameters.Builder<_B> withEntityMatrixReferenceField(final String entityMatrixReferenceField) {
            this.entityMatrixReferenceField = entityMatrixReferenceField;
            return this;
        }

        /**
         * Sets the new value of "entityX" (any previous value will be replaced)
         * 
         * @param entityX
         *     New value of the "entityX" property.
         */
        public MatrixRequestParameters.Builder<_B> withEntityX(final String entityX) {
            this.entityX = entityX;
            return this;
        }

        /**
         * Sets the new value of "entityY" (any previous value will be replaced)
         * 
         * @param entityY
         *     New value of the "entityY" property.
         */
        public MatrixRequestParameters.Builder<_B> withEntityY(final String entityY) {
            this.entityY = entityY;
            return this;
        }

        /**
         * Sets the new value of "entityFieldMatrixParent" (any previous value will be replaced)
         * 
         * @param entityFieldMatrixParent
         *     New value of the "entityFieldMatrixParent" property.
         */
        public MatrixRequestParameters.Builder<_B> withEntityFieldMatrixParent(final String entityFieldMatrixParent) {
            this.entityFieldMatrixParent = entityFieldMatrixParent;
            return this;
        }

        /**
         * Sets the new value of "entityFieldMatrixXRefField" (any previous value will be replaced)
         * 
         * @param entityFieldMatrixXRefField
         *     New value of the "entityFieldMatrixXRefField" property.
         */
        public MatrixRequestParameters.Builder<_B> withEntityFieldMatrixXRefField(final String entityFieldMatrixXRefField) {
            this.entityFieldMatrixXRefField = entityFieldMatrixXRefField;
            return this;
        }

        /**
         * Sets the new value of "cellInputType" (any previous value will be replaced)
         * 
         * @param cellInputType
         *     New value of the "cellInputType" property.
         */
        public MatrixRequestParameters.Builder<_B> withCellInputType(final String cellInputType) {
            this.cellInputType = cellInputType;
            return this;
        }

        /**
         * Sets the new value of "editable" (any previous value will be replaced)
         * 
         * @param editable
         *     New value of the "editable" property.
         */
        public MatrixRequestParameters.Builder<_B> withEditable(final String editable) {
            this.editable = editable;
            return this;
        }

        /**
         * Sets the new value of "boId" (any previous value will be replaced)
         * 
         * @param boId
         *     New value of the "boId" property.
         */
        public MatrixRequestParameters.Builder<_B> withBoId(final String boId) {
            this.boId = boId;
            return this;
        }

        @Override
        public MatrixRequestParameters build() {
            if (_storedValue == null) {
                return this.init(new MatrixRequestParameters());
            } else {
                return ((MatrixRequestParameters) _storedValue);
            }
        }

        public MatrixRequestParameters.Builder<_B> copyOf(final MatrixRequestParameters _other) {
            _other.copyTo(this);
            return this;
        }

        public MatrixRequestParameters.Builder<_B> copyOf(final MatrixRequestParameters.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends MatrixRequestParameters.Selector<MatrixRequestParameters.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static MatrixRequestParameters.Select _root() {
            return new MatrixRequestParameters.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>> entityMatrix = null;
        private com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>> entityMatrixValueType = null;
        private com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>> entityMatrixReferenceField = null;
        private com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>> entityX = null;
        private com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>> entityY = null;
        private com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>> entityFieldMatrixParent = null;
        private com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>> entityFieldMatrixXRefField = null;
        private com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>> cellInputType = null;
        private com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>> editable = null;
        private com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>> boId = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.entityMatrix!= null) {
                products.put("entityMatrix", this.entityMatrix.init());
            }
            if (this.entityMatrixValueType!= null) {
                products.put("entityMatrixValueType", this.entityMatrixValueType.init());
            }
            if (this.entityMatrixReferenceField!= null) {
                products.put("entityMatrixReferenceField", this.entityMatrixReferenceField.init());
            }
            if (this.entityX!= null) {
                products.put("entityX", this.entityX.init());
            }
            if (this.entityY!= null) {
                products.put("entityY", this.entityY.init());
            }
            if (this.entityFieldMatrixParent!= null) {
                products.put("entityFieldMatrixParent", this.entityFieldMatrixParent.init());
            }
            if (this.entityFieldMatrixXRefField!= null) {
                products.put("entityFieldMatrixXRefField", this.entityFieldMatrixXRefField.init());
            }
            if (this.cellInputType!= null) {
                products.put("cellInputType", this.cellInputType.init());
            }
            if (this.editable!= null) {
                products.put("editable", this.editable.init());
            }
            if (this.boId!= null) {
                products.put("boId", this.boId.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>> entityMatrix() {
            return ((this.entityMatrix == null)?this.entityMatrix = new com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>>(this._root, this, "entityMatrix"):this.entityMatrix);
        }

        public com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>> entityMatrixValueType() {
            return ((this.entityMatrixValueType == null)?this.entityMatrixValueType = new com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>>(this._root, this, "entityMatrixValueType"):this.entityMatrixValueType);
        }

        public com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>> entityMatrixReferenceField() {
            return ((this.entityMatrixReferenceField == null)?this.entityMatrixReferenceField = new com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>>(this._root, this, "entityMatrixReferenceField"):this.entityMatrixReferenceField);
        }

        public com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>> entityX() {
            return ((this.entityX == null)?this.entityX = new com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>>(this._root, this, "entityX"):this.entityX);
        }

        public com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>> entityY() {
            return ((this.entityY == null)?this.entityY = new com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>>(this._root, this, "entityY"):this.entityY);
        }

        public com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>> entityFieldMatrixParent() {
            return ((this.entityFieldMatrixParent == null)?this.entityFieldMatrixParent = new com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>>(this._root, this, "entityFieldMatrixParent"):this.entityFieldMatrixParent);
        }

        public com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>> entityFieldMatrixXRefField() {
            return ((this.entityFieldMatrixXRefField == null)?this.entityFieldMatrixXRefField = new com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>>(this._root, this, "entityFieldMatrixXRefField"):this.entityFieldMatrixXRefField);
        }

        public com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>> cellInputType() {
            return ((this.cellInputType == null)?this.cellInputType = new com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>>(this._root, this, "cellInputType"):this.cellInputType);
        }

        public com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>> editable() {
            return ((this.editable == null)?this.editable = new com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>>(this._root, this, "editable"):this.editable);
        }

        public com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>> boId() {
            return ((this.boId == null)?this.boId = new com.kscs.util.jaxb.Selector<TRoot, MatrixRequestParameters.Selector<TRoot, TParent>>(this._root, this, "boId"):this.boId);
        }

    }

}
