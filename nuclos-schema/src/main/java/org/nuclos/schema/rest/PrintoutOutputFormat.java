
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for printout-output-format complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="printout-output-format"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="outputFormatId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "printout-output-format")
public class PrintoutOutputFormat implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "name")
    protected String name;
    @XmlAttribute(name = "outputFormatId")
    protected String outputFormatId;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the outputFormatId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutputFormatId() {
        return outputFormatId;
    }

    /**
     * Sets the value of the outputFormatId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutputFormatId(String value) {
        this.outputFormatId = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String theOutputFormatId;
            theOutputFormatId = this.getOutputFormatId();
            strategy.appendField(locator, this, "outputFormatId", buffer, theOutputFormatId);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof PrintoutOutputFormat)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final PrintoutOutputFormat that = ((PrintoutOutputFormat) object);
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsOutputFormatId;
            lhsOutputFormatId = this.getOutputFormatId();
            String rhsOutputFormatId;
            rhsOutputFormatId = that.getOutputFormatId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "outputFormatId", lhsOutputFormatId), LocatorUtils.property(thatLocator, "outputFormatId", rhsOutputFormatId), lhsOutputFormatId, rhsOutputFormatId)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String theOutputFormatId;
            theOutputFormatId = this.getOutputFormatId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "outputFormatId", theOutputFormatId), currentHashCode, theOutputFormatId);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof PrintoutOutputFormat) {
            final PrintoutOutputFormat copy = ((PrintoutOutputFormat) draftCopy);
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.outputFormatId!= null) {
                String sourceOutputFormatId;
                sourceOutputFormatId = this.getOutputFormatId();
                String copyOutputFormatId = ((String) strategy.copy(LocatorUtils.property(locator, "outputFormatId", sourceOutputFormatId), sourceOutputFormatId));
                copy.setOutputFormatId(copyOutputFormatId);
            } else {
                copy.outputFormatId = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new PrintoutOutputFormat();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final PrintoutOutputFormat.Builder<_B> _other) {
        _other.name = this.name;
        _other.outputFormatId = this.outputFormatId;
    }

    public<_B >PrintoutOutputFormat.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new PrintoutOutputFormat.Builder<_B>(_parentBuilder, this, true);
    }

    public PrintoutOutputFormat.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static PrintoutOutputFormat.Builder<Void> builder() {
        return new PrintoutOutputFormat.Builder<Void>(null, null, false);
    }

    public static<_B >PrintoutOutputFormat.Builder<_B> copyOf(final PrintoutOutputFormat _other) {
        final PrintoutOutputFormat.Builder<_B> _newBuilder = new PrintoutOutputFormat.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final PrintoutOutputFormat.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree outputFormatIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("outputFormatId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(outputFormatIdPropertyTree!= null):((outputFormatIdPropertyTree == null)||(!outputFormatIdPropertyTree.isLeaf())))) {
            _other.outputFormatId = this.outputFormatId;
        }
    }

    public<_B >PrintoutOutputFormat.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new PrintoutOutputFormat.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public PrintoutOutputFormat.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >PrintoutOutputFormat.Builder<_B> copyOf(final PrintoutOutputFormat _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PrintoutOutputFormat.Builder<_B> _newBuilder = new PrintoutOutputFormat.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static PrintoutOutputFormat.Builder<Void> copyExcept(final PrintoutOutputFormat _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static PrintoutOutputFormat.Builder<Void> copyOnly(final PrintoutOutputFormat _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final PrintoutOutputFormat _storedValue;
        private String name;
        private String outputFormatId;

        public Builder(final _B _parentBuilder, final PrintoutOutputFormat _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.name = _other.name;
                    this.outputFormatId = _other.outputFormatId;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final PrintoutOutputFormat _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree outputFormatIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("outputFormatId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(outputFormatIdPropertyTree!= null):((outputFormatIdPropertyTree == null)||(!outputFormatIdPropertyTree.isLeaf())))) {
                        this.outputFormatId = _other.outputFormatId;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends PrintoutOutputFormat >_P init(final _P _product) {
            _product.name = this.name;
            _product.outputFormatId = this.outputFormatId;
            return _product;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public PrintoutOutputFormat.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "outputFormatId" (any previous value will be replaced)
         * 
         * @param outputFormatId
         *     New value of the "outputFormatId" property.
         */
        public PrintoutOutputFormat.Builder<_B> withOutputFormatId(final String outputFormatId) {
            this.outputFormatId = outputFormatId;
            return this;
        }

        @Override
        public PrintoutOutputFormat build() {
            if (_storedValue == null) {
                return this.init(new PrintoutOutputFormat());
            } else {
                return ((PrintoutOutputFormat) _storedValue);
            }
        }

        public PrintoutOutputFormat.Builder<_B> copyOf(final PrintoutOutputFormat _other) {
            _other.copyTo(this);
            return this;
        }

        public PrintoutOutputFormat.Builder<_B> copyOf(final PrintoutOutputFormat.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends PrintoutOutputFormat.Selector<PrintoutOutputFormat.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static PrintoutOutputFormat.Select _root() {
            return new PrintoutOutputFormat.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormat.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormat.Selector<TRoot, TParent>> outputFormatId = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.outputFormatId!= null) {
                products.put("outputFormatId", this.outputFormatId.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormat.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormat.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormat.Selector<TRoot, TParent>> outputFormatId() {
            return ((this.outputFormatId == null)?this.outputFormatId = new com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormat.Selector<TRoot, TParent>>(this._root, this, "outputFormatId"):this.outputFormatId);
        }

    }

}
