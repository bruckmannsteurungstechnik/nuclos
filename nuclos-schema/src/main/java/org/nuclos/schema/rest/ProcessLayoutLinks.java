
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for process-layout-links complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="process-layout-links"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="links" type="{urn:org.nuclos.schema.rest}layout-links"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "process-layout-links", propOrder = {
    "links"
})
public class ProcessLayoutLinks implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected LayoutLinks links;

    /**
     * Gets the value of the links property.
     * 
     * @return
     *     possible object is
     *     {@link LayoutLinks }
     *     
     */
    public LayoutLinks getLinks() {
        return links;
    }

    /**
     * Sets the value of the links property.
     * 
     * @param value
     *     allowed object is
     *     {@link LayoutLinks }
     *     
     */
    public void setLinks(LayoutLinks value) {
        this.links = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            LayoutLinks theLinks;
            theLinks = this.getLinks();
            strategy.appendField(locator, this, "links", buffer, theLinks);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ProcessLayoutLinks)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ProcessLayoutLinks that = ((ProcessLayoutLinks) object);
        {
            LayoutLinks lhsLinks;
            lhsLinks = this.getLinks();
            LayoutLinks rhsLinks;
            rhsLinks = that.getLinks();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "links", lhsLinks), LocatorUtils.property(thatLocator, "links", rhsLinks), lhsLinks, rhsLinks)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            LayoutLinks theLinks;
            theLinks = this.getLinks();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "links", theLinks), currentHashCode, theLinks);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof ProcessLayoutLinks) {
            final ProcessLayoutLinks copy = ((ProcessLayoutLinks) draftCopy);
            if (this.links!= null) {
                LayoutLinks sourceLinks;
                sourceLinks = this.getLinks();
                LayoutLinks copyLinks = ((LayoutLinks) strategy.copy(LocatorUtils.property(locator, "links", sourceLinks), sourceLinks));
                copy.setLinks(copyLinks);
            } else {
                copy.links = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ProcessLayoutLinks();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final ProcessLayoutLinks.Builder<_B> _other) {
        _other.links = ((this.links == null)?null:this.links.newCopyBuilder(_other));
    }

    public<_B >ProcessLayoutLinks.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new ProcessLayoutLinks.Builder<_B>(_parentBuilder, this, true);
    }

    public ProcessLayoutLinks.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static ProcessLayoutLinks.Builder<Void> builder() {
        return new ProcessLayoutLinks.Builder<Void>(null, null, false);
    }

    public static<_B >ProcessLayoutLinks.Builder<_B> copyOf(final ProcessLayoutLinks _other) {
        final ProcessLayoutLinks.Builder<_B> _newBuilder = new ProcessLayoutLinks.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final ProcessLayoutLinks.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree linksPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("links"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(linksPropertyTree!= null):((linksPropertyTree == null)||(!linksPropertyTree.isLeaf())))) {
            _other.links = ((this.links == null)?null:this.links.newCopyBuilder(_other, linksPropertyTree, _propertyTreeUse));
        }
    }

    public<_B >ProcessLayoutLinks.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new ProcessLayoutLinks.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public ProcessLayoutLinks.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >ProcessLayoutLinks.Builder<_B> copyOf(final ProcessLayoutLinks _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final ProcessLayoutLinks.Builder<_B> _newBuilder = new ProcessLayoutLinks.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static ProcessLayoutLinks.Builder<Void> copyExcept(final ProcessLayoutLinks _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static ProcessLayoutLinks.Builder<Void> copyOnly(final ProcessLayoutLinks _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final ProcessLayoutLinks _storedValue;
        private LayoutLinks.Builder<ProcessLayoutLinks.Builder<_B>> links;

        public Builder(final _B _parentBuilder, final ProcessLayoutLinks _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.links = ((_other.links == null)?null:_other.links.newCopyBuilder(this));
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final ProcessLayoutLinks _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree linksPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("links"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(linksPropertyTree!= null):((linksPropertyTree == null)||(!linksPropertyTree.isLeaf())))) {
                        this.links = ((_other.links == null)?null:_other.links.newCopyBuilder(this, linksPropertyTree, _propertyTreeUse));
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends ProcessLayoutLinks >_P init(final _P _product) {
            _product.links = ((this.links == null)?null:this.links.build());
            return _product;
        }

        /**
         * Sets the new value of "links" (any previous value will be replaced)
         * 
         * @param links
         *     New value of the "links" property.
         */
        public ProcessLayoutLinks.Builder<_B> withLinks(final LayoutLinks links) {
            this.links = ((links == null)?null:new LayoutLinks.Builder<ProcessLayoutLinks.Builder<_B>>(this, links, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "links" property.
         * Use {@link org.nuclos.schema.rest.LayoutLinks.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "links" property.
         *     Use {@link org.nuclos.schema.rest.LayoutLinks.Builder#end()} to return to the current builder.
         */
        public LayoutLinks.Builder<? extends ProcessLayoutLinks.Builder<_B>> withLinks() {
            if (this.links!= null) {
                return this.links;
            }
            return this.links = new LayoutLinks.Builder<ProcessLayoutLinks.Builder<_B>>(this, null, false);
        }

        @Override
        public ProcessLayoutLinks build() {
            if (_storedValue == null) {
                return this.init(new ProcessLayoutLinks());
            } else {
                return ((ProcessLayoutLinks) _storedValue);
            }
        }

        public ProcessLayoutLinks.Builder<_B> copyOf(final ProcessLayoutLinks _other) {
            _other.copyTo(this);
            return this;
        }

        public ProcessLayoutLinks.Builder<_B> copyOf(final ProcessLayoutLinks.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends ProcessLayoutLinks.Selector<ProcessLayoutLinks.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static ProcessLayoutLinks.Select _root() {
            return new ProcessLayoutLinks.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private LayoutLinks.Selector<TRoot, ProcessLayoutLinks.Selector<TRoot, TParent>> links = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.links!= null) {
                products.put("links", this.links.init());
            }
            return products;
        }

        public LayoutLinks.Selector<TRoot, ProcessLayoutLinks.Selector<TRoot, TParent>> links() {
            return ((this.links == null)?this.links = new LayoutLinks.Selector<TRoot, ProcessLayoutLinks.Selector<TRoot, TParent>>(this._root, this, "links"):this.links);
        }

    }

}
