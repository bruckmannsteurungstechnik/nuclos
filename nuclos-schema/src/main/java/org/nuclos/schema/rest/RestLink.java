
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for rest-link complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rest-link"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="methods" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="href" type="{http://www.w3.org/2001/XMLSchema}anyURI" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rest-link", propOrder = {
    "methods"
})
public class RestLink implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected List<String> methods;
    @XmlAttribute(name = "href")
    @XmlSchemaType(name = "anyURI")
    protected String href;

    /**
     * Gets the value of the methods property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the methods property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMethods().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getMethods() {
        if (methods == null) {
            methods = new ArrayList<String>();
        }
        return this.methods;
    }

    /**
     * Gets the value of the href property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHref() {
        return href;
    }

    /**
     * Sets the value of the href property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHref(String value) {
        this.href = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<String> theMethods;
            theMethods = (((this.methods!= null)&&(!this.methods.isEmpty()))?this.getMethods():null);
            strategy.appendField(locator, this, "methods", buffer, theMethods);
        }
        {
            String theHref;
            theHref = this.getHref();
            strategy.appendField(locator, this, "href", buffer, theHref);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof RestLink)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final RestLink that = ((RestLink) object);
        {
            List<String> lhsMethods;
            lhsMethods = (((this.methods!= null)&&(!this.methods.isEmpty()))?this.getMethods():null);
            List<String> rhsMethods;
            rhsMethods = (((that.methods!= null)&&(!that.methods.isEmpty()))?that.getMethods():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "methods", lhsMethods), LocatorUtils.property(thatLocator, "methods", rhsMethods), lhsMethods, rhsMethods)) {
                return false;
            }
        }
        {
            String lhsHref;
            lhsHref = this.getHref();
            String rhsHref;
            rhsHref = that.getHref();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "href", lhsHref), LocatorUtils.property(thatLocator, "href", rhsHref), lhsHref, rhsHref)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<String> theMethods;
            theMethods = (((this.methods!= null)&&(!this.methods.isEmpty()))?this.getMethods():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "methods", theMethods), currentHashCode, theMethods);
        }
        {
            String theHref;
            theHref = this.getHref();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "href", theHref), currentHashCode, theHref);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof RestLink) {
            final RestLink copy = ((RestLink) draftCopy);
            if ((this.methods!= null)&&(!this.methods.isEmpty())) {
                List<String> sourceMethods;
                sourceMethods = (((this.methods!= null)&&(!this.methods.isEmpty()))?this.getMethods():null);
                @SuppressWarnings("unchecked")
                List<String> copyMethods = ((List<String> ) strategy.copy(LocatorUtils.property(locator, "methods", sourceMethods), sourceMethods));
                copy.methods = null;
                if (copyMethods!= null) {
                    List<String> uniqueMethodsl = copy.getMethods();
                    uniqueMethodsl.addAll(copyMethods);
                }
            } else {
                copy.methods = null;
            }
            if (this.href!= null) {
                String sourceHref;
                sourceHref = this.getHref();
                String copyHref = ((String) strategy.copy(LocatorUtils.property(locator, "href", sourceHref), sourceHref));
                copy.setHref(copyHref);
            } else {
                copy.href = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new RestLink();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final RestLink.Builder<_B> _other) {
        if (this.methods == null) {
            _other.methods = null;
        } else {
            _other.methods = new ArrayList<Buildable>();
            for (String _item: this.methods) {
                _other.methods.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
            }
        }
        _other.href = this.href;
    }

    public<_B >RestLink.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new RestLink.Builder<_B>(_parentBuilder, this, true);
    }

    public RestLink.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static RestLink.Builder<Void> builder() {
        return new RestLink.Builder<Void>(null, null, false);
    }

    public static<_B >RestLink.Builder<_B> copyOf(final RestLink _other) {
        final RestLink.Builder<_B> _newBuilder = new RestLink.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final RestLink.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree methodsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("methods"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(methodsPropertyTree!= null):((methodsPropertyTree == null)||(!methodsPropertyTree.isLeaf())))) {
            if (this.methods == null) {
                _other.methods = null;
            } else {
                _other.methods = new ArrayList<Buildable>();
                for (String _item: this.methods) {
                    _other.methods.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                }
            }
        }
        final PropertyTree hrefPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("href"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(hrefPropertyTree!= null):((hrefPropertyTree == null)||(!hrefPropertyTree.isLeaf())))) {
            _other.href = this.href;
        }
    }

    public<_B >RestLink.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new RestLink.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public RestLink.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >RestLink.Builder<_B> copyOf(final RestLink _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final RestLink.Builder<_B> _newBuilder = new RestLink.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static RestLink.Builder<Void> copyExcept(final RestLink _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static RestLink.Builder<Void> copyOnly(final RestLink _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final RestLink _storedValue;
        private List<Buildable> methods;
        private String href;

        public Builder(final _B _parentBuilder, final RestLink _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.methods == null) {
                        this.methods = null;
                    } else {
                        this.methods = new ArrayList<Buildable>();
                        for (String _item: _other.methods) {
                            this.methods.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                        }
                    }
                    this.href = _other.href;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final RestLink _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree methodsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("methods"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(methodsPropertyTree!= null):((methodsPropertyTree == null)||(!methodsPropertyTree.isLeaf())))) {
                        if (_other.methods == null) {
                            this.methods = null;
                        } else {
                            this.methods = new ArrayList<Buildable>();
                            for (String _item: _other.methods) {
                                this.methods.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                            }
                        }
                    }
                    final PropertyTree hrefPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("href"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(hrefPropertyTree!= null):((hrefPropertyTree == null)||(!hrefPropertyTree.isLeaf())))) {
                        this.href = _other.href;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends RestLink >_P init(final _P _product) {
            if (this.methods!= null) {
                final List<String> methods = new ArrayList<String>(this.methods.size());
                for (Buildable _item: this.methods) {
                    methods.add(((String) _item.build()));
                }
                _product.methods = methods;
            }
            _product.href = this.href;
            return _product;
        }

        /**
         * Adds the given items to the value of "methods"
         * 
         * @param methods
         *     Items to add to the value of the "methods" property
         */
        public RestLink.Builder<_B> addMethods(final Iterable<? extends String> methods) {
            if (methods!= null) {
                if (this.methods == null) {
                    this.methods = new ArrayList<Buildable>();
                }
                for (String _item: methods) {
                    this.methods.add(new Buildable.PrimitiveBuildable(_item));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "methods" (any previous value will be replaced)
         * 
         * @param methods
         *     New value of the "methods" property.
         */
        public RestLink.Builder<_B> withMethods(final Iterable<? extends String> methods) {
            if (this.methods!= null) {
                this.methods.clear();
            }
            return addMethods(methods);
        }

        /**
         * Adds the given items to the value of "methods"
         * 
         * @param methods
         *     Items to add to the value of the "methods" property
         */
        public RestLink.Builder<_B> addMethods(String... methods) {
            addMethods(Arrays.asList(methods));
            return this;
        }

        /**
         * Sets the new value of "methods" (any previous value will be replaced)
         * 
         * @param methods
         *     New value of the "methods" property.
         */
        public RestLink.Builder<_B> withMethods(String... methods) {
            withMethods(Arrays.asList(methods));
            return this;
        }

        /**
         * Sets the new value of "href" (any previous value will be replaced)
         * 
         * @param href
         *     New value of the "href" property.
         */
        public RestLink.Builder<_B> withHref(final String href) {
            this.href = href;
            return this;
        }

        @Override
        public RestLink build() {
            if (_storedValue == null) {
                return this.init(new RestLink());
            } else {
                return ((RestLink) _storedValue);
            }
        }

        public RestLink.Builder<_B> copyOf(final RestLink _other) {
            _other.copyTo(this);
            return this;
        }

        public RestLink.Builder<_B> copyOf(final RestLink.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends RestLink.Selector<RestLink.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static RestLink.Select _root() {
            return new RestLink.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, RestLink.Selector<TRoot, TParent>> methods = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestLink.Selector<TRoot, TParent>> href = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.methods!= null) {
                products.put("methods", this.methods.init());
            }
            if (this.href!= null) {
                products.put("href", this.href.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestLink.Selector<TRoot, TParent>> methods() {
            return ((this.methods == null)?this.methods = new com.kscs.util.jaxb.Selector<TRoot, RestLink.Selector<TRoot, TParent>>(this._root, this, "methods"):this.methods);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestLink.Selector<TRoot, TParent>> href() {
            return ((this.href == null)?this.href = new com.kscs.util.jaxb.Selector<TRoot, RestLink.Selector<TRoot, TParent>>(this._root, this, "href"):this.href);
        }

    }

}
