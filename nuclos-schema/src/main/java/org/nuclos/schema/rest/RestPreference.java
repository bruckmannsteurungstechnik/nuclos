
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for rest-preference complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rest-preference"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="content" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="prefId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="type" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="shared" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="customized" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="app" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="boMetaId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="layoutId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="menuRelevant" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="selected" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="nucletId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rest-preference", propOrder = {
    "content"
})
public class RestPreference implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected Object content;
    @XmlAttribute(name = "prefId", required = true)
    protected String prefId;
    @XmlAttribute(name = "type", required = true)
    protected String type;
    @XmlAttribute(name = "shared", required = true)
    protected boolean shared;
    @XmlAttribute(name = "customized", required = true)
    protected boolean customized;
    @XmlAttribute(name = "app")
    protected String app;
    @XmlAttribute(name = "boMetaId")
    protected String boMetaId;
    @XmlAttribute(name = "layoutId")
    protected String layoutId;
    @XmlAttribute(name = "name")
    protected String name;
    @XmlAttribute(name = "menuRelevant")
    protected String menuRelevant;
    @XmlAttribute(name = "selected")
    protected String selected;
    @XmlAttribute(name = "nucletId")
    protected String nucletId;

    /**
     * Gets the value of the content property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getContent() {
        return content;
    }

    /**
     * Sets the value of the content property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setContent(Object value) {
        this.content = value;
    }

    /**
     * Gets the value of the prefId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrefId() {
        return prefId;
    }

    /**
     * Sets the value of the prefId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrefId(String value) {
        this.prefId = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the shared property.
     * 
     */
    public boolean isShared() {
        return shared;
    }

    /**
     * Sets the value of the shared property.
     * 
     */
    public void setShared(boolean value) {
        this.shared = value;
    }

    /**
     * Gets the value of the customized property.
     * 
     */
    public boolean isCustomized() {
        return customized;
    }

    /**
     * Sets the value of the customized property.
     * 
     */
    public void setCustomized(boolean value) {
        this.customized = value;
    }

    /**
     * Gets the value of the app property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApp() {
        return app;
    }

    /**
     * Sets the value of the app property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApp(String value) {
        this.app = value;
    }

    /**
     * Gets the value of the boMetaId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoMetaId() {
        return boMetaId;
    }

    /**
     * Sets the value of the boMetaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoMetaId(String value) {
        this.boMetaId = value;
    }

    /**
     * Gets the value of the layoutId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLayoutId() {
        return layoutId;
    }

    /**
     * Sets the value of the layoutId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLayoutId(String value) {
        this.layoutId = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the menuRelevant property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMenuRelevant() {
        return menuRelevant;
    }

    /**
     * Sets the value of the menuRelevant property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMenuRelevant(String value) {
        this.menuRelevant = value;
    }

    /**
     * Gets the value of the selected property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSelected() {
        return selected;
    }

    /**
     * Sets the value of the selected property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSelected(String value) {
        this.selected = value;
    }

    /**
     * Gets the value of the nucletId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNucletId() {
        return nucletId;
    }

    /**
     * Sets the value of the nucletId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNucletId(String value) {
        this.nucletId = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            Object theContent;
            theContent = this.getContent();
            strategy.appendField(locator, this, "content", buffer, theContent);
        }
        {
            String thePrefId;
            thePrefId = this.getPrefId();
            strategy.appendField(locator, this, "prefId", buffer, thePrefId);
        }
        {
            String theType;
            theType = this.getType();
            strategy.appendField(locator, this, "type", buffer, theType);
        }
        {
            boolean theShared;
            theShared = this.isShared();
            strategy.appendField(locator, this, "shared", buffer, theShared);
        }
        {
            boolean theCustomized;
            theCustomized = this.isCustomized();
            strategy.appendField(locator, this, "customized", buffer, theCustomized);
        }
        {
            String theApp;
            theApp = this.getApp();
            strategy.appendField(locator, this, "app", buffer, theApp);
        }
        {
            String theBoMetaId;
            theBoMetaId = this.getBoMetaId();
            strategy.appendField(locator, this, "boMetaId", buffer, theBoMetaId);
        }
        {
            String theLayoutId;
            theLayoutId = this.getLayoutId();
            strategy.appendField(locator, this, "layoutId", buffer, theLayoutId);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String theMenuRelevant;
            theMenuRelevant = this.getMenuRelevant();
            strategy.appendField(locator, this, "menuRelevant", buffer, theMenuRelevant);
        }
        {
            String theSelected;
            theSelected = this.getSelected();
            strategy.appendField(locator, this, "selected", buffer, theSelected);
        }
        {
            String theNucletId;
            theNucletId = this.getNucletId();
            strategy.appendField(locator, this, "nucletId", buffer, theNucletId);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof RestPreference)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final RestPreference that = ((RestPreference) object);
        {
            Object lhsContent;
            lhsContent = this.getContent();
            Object rhsContent;
            rhsContent = that.getContent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "content", lhsContent), LocatorUtils.property(thatLocator, "content", rhsContent), lhsContent, rhsContent)) {
                return false;
            }
        }
        {
            String lhsPrefId;
            lhsPrefId = this.getPrefId();
            String rhsPrefId;
            rhsPrefId = that.getPrefId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "prefId", lhsPrefId), LocatorUtils.property(thatLocator, "prefId", rhsPrefId), lhsPrefId, rhsPrefId)) {
                return false;
            }
        }
        {
            String lhsType;
            lhsType = this.getType();
            String rhsType;
            rhsType = that.getType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "type", lhsType), LocatorUtils.property(thatLocator, "type", rhsType), lhsType, rhsType)) {
                return false;
            }
        }
        {
            boolean lhsShared;
            lhsShared = this.isShared();
            boolean rhsShared;
            rhsShared = that.isShared();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "shared", lhsShared), LocatorUtils.property(thatLocator, "shared", rhsShared), lhsShared, rhsShared)) {
                return false;
            }
        }
        {
            boolean lhsCustomized;
            lhsCustomized = this.isCustomized();
            boolean rhsCustomized;
            rhsCustomized = that.isCustomized();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "customized", lhsCustomized), LocatorUtils.property(thatLocator, "customized", rhsCustomized), lhsCustomized, rhsCustomized)) {
                return false;
            }
        }
        {
            String lhsApp;
            lhsApp = this.getApp();
            String rhsApp;
            rhsApp = that.getApp();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "app", lhsApp), LocatorUtils.property(thatLocator, "app", rhsApp), lhsApp, rhsApp)) {
                return false;
            }
        }
        {
            String lhsBoMetaId;
            lhsBoMetaId = this.getBoMetaId();
            String rhsBoMetaId;
            rhsBoMetaId = that.getBoMetaId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "boMetaId", lhsBoMetaId), LocatorUtils.property(thatLocator, "boMetaId", rhsBoMetaId), lhsBoMetaId, rhsBoMetaId)) {
                return false;
            }
        }
        {
            String lhsLayoutId;
            lhsLayoutId = this.getLayoutId();
            String rhsLayoutId;
            rhsLayoutId = that.getLayoutId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "layoutId", lhsLayoutId), LocatorUtils.property(thatLocator, "layoutId", rhsLayoutId), lhsLayoutId, rhsLayoutId)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsMenuRelevant;
            lhsMenuRelevant = this.getMenuRelevant();
            String rhsMenuRelevant;
            rhsMenuRelevant = that.getMenuRelevant();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "menuRelevant", lhsMenuRelevant), LocatorUtils.property(thatLocator, "menuRelevant", rhsMenuRelevant), lhsMenuRelevant, rhsMenuRelevant)) {
                return false;
            }
        }
        {
            String lhsSelected;
            lhsSelected = this.getSelected();
            String rhsSelected;
            rhsSelected = that.getSelected();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "selected", lhsSelected), LocatorUtils.property(thatLocator, "selected", rhsSelected), lhsSelected, rhsSelected)) {
                return false;
            }
        }
        {
            String lhsNucletId;
            lhsNucletId = this.getNucletId();
            String rhsNucletId;
            rhsNucletId = that.getNucletId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "nucletId", lhsNucletId), LocatorUtils.property(thatLocator, "nucletId", rhsNucletId), lhsNucletId, rhsNucletId)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            Object theContent;
            theContent = this.getContent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "content", theContent), currentHashCode, theContent);
        }
        {
            String thePrefId;
            thePrefId = this.getPrefId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "prefId", thePrefId), currentHashCode, thePrefId);
        }
        {
            String theType;
            theType = this.getType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "type", theType), currentHashCode, theType);
        }
        {
            boolean theShared;
            theShared = this.isShared();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "shared", theShared), currentHashCode, theShared);
        }
        {
            boolean theCustomized;
            theCustomized = this.isCustomized();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "customized", theCustomized), currentHashCode, theCustomized);
        }
        {
            String theApp;
            theApp = this.getApp();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "app", theApp), currentHashCode, theApp);
        }
        {
            String theBoMetaId;
            theBoMetaId = this.getBoMetaId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "boMetaId", theBoMetaId), currentHashCode, theBoMetaId);
        }
        {
            String theLayoutId;
            theLayoutId = this.getLayoutId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "layoutId", theLayoutId), currentHashCode, theLayoutId);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String theMenuRelevant;
            theMenuRelevant = this.getMenuRelevant();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "menuRelevant", theMenuRelevant), currentHashCode, theMenuRelevant);
        }
        {
            String theSelected;
            theSelected = this.getSelected();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "selected", theSelected), currentHashCode, theSelected);
        }
        {
            String theNucletId;
            theNucletId = this.getNucletId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nucletId", theNucletId), currentHashCode, theNucletId);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof RestPreference) {
            final RestPreference copy = ((RestPreference) draftCopy);
            if (this.content!= null) {
                Object sourceContent;
                sourceContent = this.getContent();
                Object copyContent = ((Object) strategy.copy(LocatorUtils.property(locator, "content", sourceContent), sourceContent));
                copy.setContent(copyContent);
            } else {
                copy.content = null;
            }
            if (this.prefId!= null) {
                String sourcePrefId;
                sourcePrefId = this.getPrefId();
                String copyPrefId = ((String) strategy.copy(LocatorUtils.property(locator, "prefId", sourcePrefId), sourcePrefId));
                copy.setPrefId(copyPrefId);
            } else {
                copy.prefId = null;
            }
            if (this.type!= null) {
                String sourceType;
                sourceType = this.getType();
                String copyType = ((String) strategy.copy(LocatorUtils.property(locator, "type", sourceType), sourceType));
                copy.setType(copyType);
            } else {
                copy.type = null;
            }
            {
                boolean sourceShared;
                sourceShared = this.isShared();
                boolean copyShared = strategy.copy(LocatorUtils.property(locator, "shared", sourceShared), sourceShared);
                copy.setShared(copyShared);
            }
            {
                boolean sourceCustomized;
                sourceCustomized = this.isCustomized();
                boolean copyCustomized = strategy.copy(LocatorUtils.property(locator, "customized", sourceCustomized), sourceCustomized);
                copy.setCustomized(copyCustomized);
            }
            if (this.app!= null) {
                String sourceApp;
                sourceApp = this.getApp();
                String copyApp = ((String) strategy.copy(LocatorUtils.property(locator, "app", sourceApp), sourceApp));
                copy.setApp(copyApp);
            } else {
                copy.app = null;
            }
            if (this.boMetaId!= null) {
                String sourceBoMetaId;
                sourceBoMetaId = this.getBoMetaId();
                String copyBoMetaId = ((String) strategy.copy(LocatorUtils.property(locator, "boMetaId", sourceBoMetaId), sourceBoMetaId));
                copy.setBoMetaId(copyBoMetaId);
            } else {
                copy.boMetaId = null;
            }
            if (this.layoutId!= null) {
                String sourceLayoutId;
                sourceLayoutId = this.getLayoutId();
                String copyLayoutId = ((String) strategy.copy(LocatorUtils.property(locator, "layoutId", sourceLayoutId), sourceLayoutId));
                copy.setLayoutId(copyLayoutId);
            } else {
                copy.layoutId = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.menuRelevant!= null) {
                String sourceMenuRelevant;
                sourceMenuRelevant = this.getMenuRelevant();
                String copyMenuRelevant = ((String) strategy.copy(LocatorUtils.property(locator, "menuRelevant", sourceMenuRelevant), sourceMenuRelevant));
                copy.setMenuRelevant(copyMenuRelevant);
            } else {
                copy.menuRelevant = null;
            }
            if (this.selected!= null) {
                String sourceSelected;
                sourceSelected = this.getSelected();
                String copySelected = ((String) strategy.copy(LocatorUtils.property(locator, "selected", sourceSelected), sourceSelected));
                copy.setSelected(copySelected);
            } else {
                copy.selected = null;
            }
            if (this.nucletId!= null) {
                String sourceNucletId;
                sourceNucletId = this.getNucletId();
                String copyNucletId = ((String) strategy.copy(LocatorUtils.property(locator, "nucletId", sourceNucletId), sourceNucletId));
                copy.setNucletId(copyNucletId);
            } else {
                copy.nucletId = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new RestPreference();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final RestPreference.Builder<_B> _other) {
        _other.content = this.content;
        _other.prefId = this.prefId;
        _other.type = this.type;
        _other.shared = this.shared;
        _other.customized = this.customized;
        _other.app = this.app;
        _other.boMetaId = this.boMetaId;
        _other.layoutId = this.layoutId;
        _other.name = this.name;
        _other.menuRelevant = this.menuRelevant;
        _other.selected = this.selected;
        _other.nucletId = this.nucletId;
    }

    public<_B >RestPreference.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new RestPreference.Builder<_B>(_parentBuilder, this, true);
    }

    public RestPreference.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static RestPreference.Builder<Void> builder() {
        return new RestPreference.Builder<Void>(null, null, false);
    }

    public static<_B >RestPreference.Builder<_B> copyOf(final RestPreference _other) {
        final RestPreference.Builder<_B> _newBuilder = new RestPreference.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final RestPreference.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree contentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("content"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(contentPropertyTree!= null):((contentPropertyTree == null)||(!contentPropertyTree.isLeaf())))) {
            _other.content = this.content;
        }
        final PropertyTree prefIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("prefId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(prefIdPropertyTree!= null):((prefIdPropertyTree == null)||(!prefIdPropertyTree.isLeaf())))) {
            _other.prefId = this.prefId;
        }
        final PropertyTree typePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("type"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(typePropertyTree!= null):((typePropertyTree == null)||(!typePropertyTree.isLeaf())))) {
            _other.type = this.type;
        }
        final PropertyTree sharedPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("shared"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(sharedPropertyTree!= null):((sharedPropertyTree == null)||(!sharedPropertyTree.isLeaf())))) {
            _other.shared = this.shared;
        }
        final PropertyTree customizedPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("customized"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(customizedPropertyTree!= null):((customizedPropertyTree == null)||(!customizedPropertyTree.isLeaf())))) {
            _other.customized = this.customized;
        }
        final PropertyTree appPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("app"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(appPropertyTree!= null):((appPropertyTree == null)||(!appPropertyTree.isLeaf())))) {
            _other.app = this.app;
        }
        final PropertyTree boMetaIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boMetaId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boMetaIdPropertyTree!= null):((boMetaIdPropertyTree == null)||(!boMetaIdPropertyTree.isLeaf())))) {
            _other.boMetaId = this.boMetaId;
        }
        final PropertyTree layoutIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("layoutId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(layoutIdPropertyTree!= null):((layoutIdPropertyTree == null)||(!layoutIdPropertyTree.isLeaf())))) {
            _other.layoutId = this.layoutId;
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree menuRelevantPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("menuRelevant"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(menuRelevantPropertyTree!= null):((menuRelevantPropertyTree == null)||(!menuRelevantPropertyTree.isLeaf())))) {
            _other.menuRelevant = this.menuRelevant;
        }
        final PropertyTree selectedPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("selected"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(selectedPropertyTree!= null):((selectedPropertyTree == null)||(!selectedPropertyTree.isLeaf())))) {
            _other.selected = this.selected;
        }
        final PropertyTree nucletIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nucletId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nucletIdPropertyTree!= null):((nucletIdPropertyTree == null)||(!nucletIdPropertyTree.isLeaf())))) {
            _other.nucletId = this.nucletId;
        }
    }

    public<_B >RestPreference.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new RestPreference.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public RestPreference.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >RestPreference.Builder<_B> copyOf(final RestPreference _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final RestPreference.Builder<_B> _newBuilder = new RestPreference.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static RestPreference.Builder<Void> copyExcept(final RestPreference _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static RestPreference.Builder<Void> copyOnly(final RestPreference _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final RestPreference _storedValue;
        private Object content;
        private String prefId;
        private String type;
        private boolean shared;
        private boolean customized;
        private String app;
        private String boMetaId;
        private String layoutId;
        private String name;
        private String menuRelevant;
        private String selected;
        private String nucletId;

        public Builder(final _B _parentBuilder, final RestPreference _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.content = _other.content;
                    this.prefId = _other.prefId;
                    this.type = _other.type;
                    this.shared = _other.shared;
                    this.customized = _other.customized;
                    this.app = _other.app;
                    this.boMetaId = _other.boMetaId;
                    this.layoutId = _other.layoutId;
                    this.name = _other.name;
                    this.menuRelevant = _other.menuRelevant;
                    this.selected = _other.selected;
                    this.nucletId = _other.nucletId;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final RestPreference _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree contentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("content"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(contentPropertyTree!= null):((contentPropertyTree == null)||(!contentPropertyTree.isLeaf())))) {
                        this.content = _other.content;
                    }
                    final PropertyTree prefIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("prefId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(prefIdPropertyTree!= null):((prefIdPropertyTree == null)||(!prefIdPropertyTree.isLeaf())))) {
                        this.prefId = _other.prefId;
                    }
                    final PropertyTree typePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("type"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(typePropertyTree!= null):((typePropertyTree == null)||(!typePropertyTree.isLeaf())))) {
                        this.type = _other.type;
                    }
                    final PropertyTree sharedPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("shared"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(sharedPropertyTree!= null):((sharedPropertyTree == null)||(!sharedPropertyTree.isLeaf())))) {
                        this.shared = _other.shared;
                    }
                    final PropertyTree customizedPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("customized"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(customizedPropertyTree!= null):((customizedPropertyTree == null)||(!customizedPropertyTree.isLeaf())))) {
                        this.customized = _other.customized;
                    }
                    final PropertyTree appPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("app"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(appPropertyTree!= null):((appPropertyTree == null)||(!appPropertyTree.isLeaf())))) {
                        this.app = _other.app;
                    }
                    final PropertyTree boMetaIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boMetaId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boMetaIdPropertyTree!= null):((boMetaIdPropertyTree == null)||(!boMetaIdPropertyTree.isLeaf())))) {
                        this.boMetaId = _other.boMetaId;
                    }
                    final PropertyTree layoutIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("layoutId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(layoutIdPropertyTree!= null):((layoutIdPropertyTree == null)||(!layoutIdPropertyTree.isLeaf())))) {
                        this.layoutId = _other.layoutId;
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree menuRelevantPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("menuRelevant"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(menuRelevantPropertyTree!= null):((menuRelevantPropertyTree == null)||(!menuRelevantPropertyTree.isLeaf())))) {
                        this.menuRelevant = _other.menuRelevant;
                    }
                    final PropertyTree selectedPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("selected"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(selectedPropertyTree!= null):((selectedPropertyTree == null)||(!selectedPropertyTree.isLeaf())))) {
                        this.selected = _other.selected;
                    }
                    final PropertyTree nucletIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nucletId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nucletIdPropertyTree!= null):((nucletIdPropertyTree == null)||(!nucletIdPropertyTree.isLeaf())))) {
                        this.nucletId = _other.nucletId;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends RestPreference >_P init(final _P _product) {
            _product.content = this.content;
            _product.prefId = this.prefId;
            _product.type = this.type;
            _product.shared = this.shared;
            _product.customized = this.customized;
            _product.app = this.app;
            _product.boMetaId = this.boMetaId;
            _product.layoutId = this.layoutId;
            _product.name = this.name;
            _product.menuRelevant = this.menuRelevant;
            _product.selected = this.selected;
            _product.nucletId = this.nucletId;
            return _product;
        }

        /**
         * Sets the new value of "content" (any previous value will be replaced)
         * 
         * @param content
         *     New value of the "content" property.
         */
        public RestPreference.Builder<_B> withContent(final Object content) {
            this.content = content;
            return this;
        }

        /**
         * Sets the new value of "prefId" (any previous value will be replaced)
         * 
         * @param prefId
         *     New value of the "prefId" property.
         */
        public RestPreference.Builder<_B> withPrefId(final String prefId) {
            this.prefId = prefId;
            return this;
        }

        /**
         * Sets the new value of "type" (any previous value will be replaced)
         * 
         * @param type
         *     New value of the "type" property.
         */
        public RestPreference.Builder<_B> withType(final String type) {
            this.type = type;
            return this;
        }

        /**
         * Sets the new value of "shared" (any previous value will be replaced)
         * 
         * @param shared
         *     New value of the "shared" property.
         */
        public RestPreference.Builder<_B> withShared(final boolean shared) {
            this.shared = shared;
            return this;
        }

        /**
         * Sets the new value of "customized" (any previous value will be replaced)
         * 
         * @param customized
         *     New value of the "customized" property.
         */
        public RestPreference.Builder<_B> withCustomized(final boolean customized) {
            this.customized = customized;
            return this;
        }

        /**
         * Sets the new value of "app" (any previous value will be replaced)
         * 
         * @param app
         *     New value of the "app" property.
         */
        public RestPreference.Builder<_B> withApp(final String app) {
            this.app = app;
            return this;
        }

        /**
         * Sets the new value of "boMetaId" (any previous value will be replaced)
         * 
         * @param boMetaId
         *     New value of the "boMetaId" property.
         */
        public RestPreference.Builder<_B> withBoMetaId(final String boMetaId) {
            this.boMetaId = boMetaId;
            return this;
        }

        /**
         * Sets the new value of "layoutId" (any previous value will be replaced)
         * 
         * @param layoutId
         *     New value of the "layoutId" property.
         */
        public RestPreference.Builder<_B> withLayoutId(final String layoutId) {
            this.layoutId = layoutId;
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public RestPreference.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "menuRelevant" (any previous value will be replaced)
         * 
         * @param menuRelevant
         *     New value of the "menuRelevant" property.
         */
        public RestPreference.Builder<_B> withMenuRelevant(final String menuRelevant) {
            this.menuRelevant = menuRelevant;
            return this;
        }

        /**
         * Sets the new value of "selected" (any previous value will be replaced)
         * 
         * @param selected
         *     New value of the "selected" property.
         */
        public RestPreference.Builder<_B> withSelected(final String selected) {
            this.selected = selected;
            return this;
        }

        /**
         * Sets the new value of "nucletId" (any previous value will be replaced)
         * 
         * @param nucletId
         *     New value of the "nucletId" property.
         */
        public RestPreference.Builder<_B> withNucletId(final String nucletId) {
            this.nucletId = nucletId;
            return this;
        }

        @Override
        public RestPreference build() {
            if (_storedValue == null) {
                return this.init(new RestPreference());
            } else {
                return ((RestPreference) _storedValue);
            }
        }

        public RestPreference.Builder<_B> copyOf(final RestPreference _other) {
            _other.copyTo(this);
            return this;
        }

        public RestPreference.Builder<_B> copyOf(final RestPreference.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends RestPreference.Selector<RestPreference.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static RestPreference.Select _root() {
            return new RestPreference.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>> content = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>> prefId = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>> type = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>> app = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>> boMetaId = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>> layoutId = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>> menuRelevant = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>> selected = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>> nucletId = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.content!= null) {
                products.put("content", this.content.init());
            }
            if (this.prefId!= null) {
                products.put("prefId", this.prefId.init());
            }
            if (this.type!= null) {
                products.put("type", this.type.init());
            }
            if (this.app!= null) {
                products.put("app", this.app.init());
            }
            if (this.boMetaId!= null) {
                products.put("boMetaId", this.boMetaId.init());
            }
            if (this.layoutId!= null) {
                products.put("layoutId", this.layoutId.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.menuRelevant!= null) {
                products.put("menuRelevant", this.menuRelevant.init());
            }
            if (this.selected!= null) {
                products.put("selected", this.selected.init());
            }
            if (this.nucletId!= null) {
                products.put("nucletId", this.nucletId.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>> content() {
            return ((this.content == null)?this.content = new com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>>(this._root, this, "content"):this.content);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>> prefId() {
            return ((this.prefId == null)?this.prefId = new com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>>(this._root, this, "prefId"):this.prefId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>> type() {
            return ((this.type == null)?this.type = new com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>>(this._root, this, "type"):this.type);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>> app() {
            return ((this.app == null)?this.app = new com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>>(this._root, this, "app"):this.app);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>> boMetaId() {
            return ((this.boMetaId == null)?this.boMetaId = new com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>>(this._root, this, "boMetaId"):this.boMetaId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>> layoutId() {
            return ((this.layoutId == null)?this.layoutId = new com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>>(this._root, this, "layoutId"):this.layoutId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>> menuRelevant() {
            return ((this.menuRelevant == null)?this.menuRelevant = new com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>>(this._root, this, "menuRelevant"):this.menuRelevant);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>> selected() {
            return ((this.selected == null)?this.selected = new com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>>(this._root, this, "selected"):this.selected);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>> nucletId() {
            return ((this.nucletId == null)?this.nucletId = new com.kscs.util.jaxb.Selector<TRoot, RestPreference.Selector<TRoot, TParent>>(this._root, this, "nucletId"):this.nucletId);
        }

    }

}
