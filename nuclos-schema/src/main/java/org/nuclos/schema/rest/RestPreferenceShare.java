
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for rest-preference-share complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rest-preference-share"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="userRoles" type="{urn:org.nuclos.schema.rest}rest-preference-user-role" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rest-preference-share", propOrder = {
    "userRoles"
})
public class RestPreferenceShare implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected List<RestPreferenceUserRole> userRoles;

    /**
     * Gets the value of the userRoles property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the userRoles property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserRoles().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RestPreferenceUserRole }
     * 
     * 
     */
    public List<RestPreferenceUserRole> getUserRoles() {
        if (userRoles == null) {
            userRoles = new ArrayList<RestPreferenceUserRole>();
        }
        return this.userRoles;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<RestPreferenceUserRole> theUserRoles;
            theUserRoles = (((this.userRoles!= null)&&(!this.userRoles.isEmpty()))?this.getUserRoles():null);
            strategy.appendField(locator, this, "userRoles", buffer, theUserRoles);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof RestPreferenceShare)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final RestPreferenceShare that = ((RestPreferenceShare) object);
        {
            List<RestPreferenceUserRole> lhsUserRoles;
            lhsUserRoles = (((this.userRoles!= null)&&(!this.userRoles.isEmpty()))?this.getUserRoles():null);
            List<RestPreferenceUserRole> rhsUserRoles;
            rhsUserRoles = (((that.userRoles!= null)&&(!that.userRoles.isEmpty()))?that.getUserRoles():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "userRoles", lhsUserRoles), LocatorUtils.property(thatLocator, "userRoles", rhsUserRoles), lhsUserRoles, rhsUserRoles)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<RestPreferenceUserRole> theUserRoles;
            theUserRoles = (((this.userRoles!= null)&&(!this.userRoles.isEmpty()))?this.getUserRoles():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "userRoles", theUserRoles), currentHashCode, theUserRoles);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof RestPreferenceShare) {
            final RestPreferenceShare copy = ((RestPreferenceShare) draftCopy);
            if ((this.userRoles!= null)&&(!this.userRoles.isEmpty())) {
                List<RestPreferenceUserRole> sourceUserRoles;
                sourceUserRoles = (((this.userRoles!= null)&&(!this.userRoles.isEmpty()))?this.getUserRoles():null);
                @SuppressWarnings("unchecked")
                List<RestPreferenceUserRole> copyUserRoles = ((List<RestPreferenceUserRole> ) strategy.copy(LocatorUtils.property(locator, "userRoles", sourceUserRoles), sourceUserRoles));
                copy.userRoles = null;
                if (copyUserRoles!= null) {
                    List<RestPreferenceUserRole> uniqueUserRolesl = copy.getUserRoles();
                    uniqueUserRolesl.addAll(copyUserRoles);
                }
            } else {
                copy.userRoles = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new RestPreferenceShare();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final RestPreferenceShare.Builder<_B> _other) {
        if (this.userRoles == null) {
            _other.userRoles = null;
        } else {
            _other.userRoles = new ArrayList<RestPreferenceUserRole.Builder<RestPreferenceShare.Builder<_B>>>();
            for (RestPreferenceUserRole _item: this.userRoles) {
                _other.userRoles.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
    }

    public<_B >RestPreferenceShare.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new RestPreferenceShare.Builder<_B>(_parentBuilder, this, true);
    }

    public RestPreferenceShare.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static RestPreferenceShare.Builder<Void> builder() {
        return new RestPreferenceShare.Builder<Void>(null, null, false);
    }

    public static<_B >RestPreferenceShare.Builder<_B> copyOf(final RestPreferenceShare _other) {
        final RestPreferenceShare.Builder<_B> _newBuilder = new RestPreferenceShare.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final RestPreferenceShare.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree userRolesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("userRoles"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(userRolesPropertyTree!= null):((userRolesPropertyTree == null)||(!userRolesPropertyTree.isLeaf())))) {
            if (this.userRoles == null) {
                _other.userRoles = null;
            } else {
                _other.userRoles = new ArrayList<RestPreferenceUserRole.Builder<RestPreferenceShare.Builder<_B>>>();
                for (RestPreferenceUserRole _item: this.userRoles) {
                    _other.userRoles.add(((_item == null)?null:_item.newCopyBuilder(_other, userRolesPropertyTree, _propertyTreeUse)));
                }
            }
        }
    }

    public<_B >RestPreferenceShare.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new RestPreferenceShare.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public RestPreferenceShare.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >RestPreferenceShare.Builder<_B> copyOf(final RestPreferenceShare _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final RestPreferenceShare.Builder<_B> _newBuilder = new RestPreferenceShare.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static RestPreferenceShare.Builder<Void> copyExcept(final RestPreferenceShare _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static RestPreferenceShare.Builder<Void> copyOnly(final RestPreferenceShare _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final RestPreferenceShare _storedValue;
        private List<RestPreferenceUserRole.Builder<RestPreferenceShare.Builder<_B>>> userRoles;

        public Builder(final _B _parentBuilder, final RestPreferenceShare _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.userRoles == null) {
                        this.userRoles = null;
                    } else {
                        this.userRoles = new ArrayList<RestPreferenceUserRole.Builder<RestPreferenceShare.Builder<_B>>>();
                        for (RestPreferenceUserRole _item: _other.userRoles) {
                            this.userRoles.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final RestPreferenceShare _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree userRolesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("userRoles"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(userRolesPropertyTree!= null):((userRolesPropertyTree == null)||(!userRolesPropertyTree.isLeaf())))) {
                        if (_other.userRoles == null) {
                            this.userRoles = null;
                        } else {
                            this.userRoles = new ArrayList<RestPreferenceUserRole.Builder<RestPreferenceShare.Builder<_B>>>();
                            for (RestPreferenceUserRole _item: _other.userRoles) {
                                this.userRoles.add(((_item == null)?null:_item.newCopyBuilder(this, userRolesPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends RestPreferenceShare >_P init(final _P _product) {
            if (this.userRoles!= null) {
                final List<RestPreferenceUserRole> userRoles = new ArrayList<RestPreferenceUserRole>(this.userRoles.size());
                for (RestPreferenceUserRole.Builder<RestPreferenceShare.Builder<_B>> _item: this.userRoles) {
                    userRoles.add(_item.build());
                }
                _product.userRoles = userRoles;
            }
            return _product;
        }

        /**
         * Adds the given items to the value of "userRoles"
         * 
         * @param userRoles
         *     Items to add to the value of the "userRoles" property
         */
        public RestPreferenceShare.Builder<_B> addUserRoles(final Iterable<? extends RestPreferenceUserRole> userRoles) {
            if (userRoles!= null) {
                if (this.userRoles == null) {
                    this.userRoles = new ArrayList<RestPreferenceUserRole.Builder<RestPreferenceShare.Builder<_B>>>();
                }
                for (RestPreferenceUserRole _item: userRoles) {
                    this.userRoles.add(new RestPreferenceUserRole.Builder<RestPreferenceShare.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "userRoles" (any previous value will be replaced)
         * 
         * @param userRoles
         *     New value of the "userRoles" property.
         */
        public RestPreferenceShare.Builder<_B> withUserRoles(final Iterable<? extends RestPreferenceUserRole> userRoles) {
            if (this.userRoles!= null) {
                this.userRoles.clear();
            }
            return addUserRoles(userRoles);
        }

        /**
         * Adds the given items to the value of "userRoles"
         * 
         * @param userRoles
         *     Items to add to the value of the "userRoles" property
         */
        public RestPreferenceShare.Builder<_B> addUserRoles(RestPreferenceUserRole... userRoles) {
            addUserRoles(Arrays.asList(userRoles));
            return this;
        }

        /**
         * Sets the new value of "userRoles" (any previous value will be replaced)
         * 
         * @param userRoles
         *     New value of the "userRoles" property.
         */
        public RestPreferenceShare.Builder<_B> withUserRoles(RestPreferenceUserRole... userRoles) {
            withUserRoles(Arrays.asList(userRoles));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "UserRoles" property.
         * Use {@link org.nuclos.schema.rest.RestPreferenceUserRole.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "UserRoles" property.
         *     Use {@link org.nuclos.schema.rest.RestPreferenceUserRole.Builder#end()} to return to the current builder.
         */
        public RestPreferenceUserRole.Builder<? extends RestPreferenceShare.Builder<_B>> addUserRoles() {
            if (this.userRoles == null) {
                this.userRoles = new ArrayList<RestPreferenceUserRole.Builder<RestPreferenceShare.Builder<_B>>>();
            }
            final RestPreferenceUserRole.Builder<RestPreferenceShare.Builder<_B>> userRoles_Builder = new RestPreferenceUserRole.Builder<RestPreferenceShare.Builder<_B>>(this, null, false);
            this.userRoles.add(userRoles_Builder);
            return userRoles_Builder;
        }

        @Override
        public RestPreferenceShare build() {
            if (_storedValue == null) {
                return this.init(new RestPreferenceShare());
            } else {
                return ((RestPreferenceShare) _storedValue);
            }
        }

        public RestPreferenceShare.Builder<_B> copyOf(final RestPreferenceShare _other) {
            _other.copyTo(this);
            return this;
        }

        public RestPreferenceShare.Builder<_B> copyOf(final RestPreferenceShare.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends RestPreferenceShare.Selector<RestPreferenceShare.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static RestPreferenceShare.Select _root() {
            return new RestPreferenceShare.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private RestPreferenceUserRole.Selector<TRoot, RestPreferenceShare.Selector<TRoot, TParent>> userRoles = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.userRoles!= null) {
                products.put("userRoles", this.userRoles.init());
            }
            return products;
        }

        public RestPreferenceUserRole.Selector<TRoot, RestPreferenceShare.Selector<TRoot, TParent>> userRoles() {
            return ((this.userRoles == null)?this.userRoles = new RestPreferenceUserRole.Selector<TRoot, RestPreferenceShare.Selector<TRoot, TParent>>(this._root, this, "userRoles"):this.userRoles);
        }

    }

}
