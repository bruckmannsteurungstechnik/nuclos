
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for rest-preference-user-role complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rest-preference-user-role"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="userRoleId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="shared" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rest-preference-user-role")
public class RestPreferenceUserRole implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "userRoleId")
    protected String userRoleId;
    @XmlAttribute(name = "name")
    protected String name;
    @XmlAttribute(name = "shared")
    protected Boolean shared;

    /**
     * Gets the value of the userRoleId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserRoleId() {
        return userRoleId;
    }

    /**
     * Sets the value of the userRoleId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserRoleId(String value) {
        this.userRoleId = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the shared property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isShared() {
        return shared;
    }

    /**
     * Sets the value of the shared property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShared(Boolean value) {
        this.shared = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theUserRoleId;
            theUserRoleId = this.getUserRoleId();
            strategy.appendField(locator, this, "userRoleId", buffer, theUserRoleId);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            Boolean theShared;
            theShared = this.isShared();
            strategy.appendField(locator, this, "shared", buffer, theShared);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof RestPreferenceUserRole)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final RestPreferenceUserRole that = ((RestPreferenceUserRole) object);
        {
            String lhsUserRoleId;
            lhsUserRoleId = this.getUserRoleId();
            String rhsUserRoleId;
            rhsUserRoleId = that.getUserRoleId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "userRoleId", lhsUserRoleId), LocatorUtils.property(thatLocator, "userRoleId", rhsUserRoleId), lhsUserRoleId, rhsUserRoleId)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            Boolean lhsShared;
            lhsShared = this.isShared();
            Boolean rhsShared;
            rhsShared = that.isShared();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "shared", lhsShared), LocatorUtils.property(thatLocator, "shared", rhsShared), lhsShared, rhsShared)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theUserRoleId;
            theUserRoleId = this.getUserRoleId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "userRoleId", theUserRoleId), currentHashCode, theUserRoleId);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            Boolean theShared;
            theShared = this.isShared();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "shared", theShared), currentHashCode, theShared);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof RestPreferenceUserRole) {
            final RestPreferenceUserRole copy = ((RestPreferenceUserRole) draftCopy);
            if (this.userRoleId!= null) {
                String sourceUserRoleId;
                sourceUserRoleId = this.getUserRoleId();
                String copyUserRoleId = ((String) strategy.copy(LocatorUtils.property(locator, "userRoleId", sourceUserRoleId), sourceUserRoleId));
                copy.setUserRoleId(copyUserRoleId);
            } else {
                copy.userRoleId = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.shared!= null) {
                Boolean sourceShared;
                sourceShared = this.isShared();
                Boolean copyShared = ((Boolean) strategy.copy(LocatorUtils.property(locator, "shared", sourceShared), sourceShared));
                copy.setShared(copyShared);
            } else {
                copy.shared = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new RestPreferenceUserRole();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final RestPreferenceUserRole.Builder<_B> _other) {
        _other.userRoleId = this.userRoleId;
        _other.name = this.name;
        _other.shared = this.shared;
    }

    public<_B >RestPreferenceUserRole.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new RestPreferenceUserRole.Builder<_B>(_parentBuilder, this, true);
    }

    public RestPreferenceUserRole.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static RestPreferenceUserRole.Builder<Void> builder() {
        return new RestPreferenceUserRole.Builder<Void>(null, null, false);
    }

    public static<_B >RestPreferenceUserRole.Builder<_B> copyOf(final RestPreferenceUserRole _other) {
        final RestPreferenceUserRole.Builder<_B> _newBuilder = new RestPreferenceUserRole.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final RestPreferenceUserRole.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree userRoleIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("userRoleId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(userRoleIdPropertyTree!= null):((userRoleIdPropertyTree == null)||(!userRoleIdPropertyTree.isLeaf())))) {
            _other.userRoleId = this.userRoleId;
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree sharedPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("shared"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(sharedPropertyTree!= null):((sharedPropertyTree == null)||(!sharedPropertyTree.isLeaf())))) {
            _other.shared = this.shared;
        }
    }

    public<_B >RestPreferenceUserRole.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new RestPreferenceUserRole.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public RestPreferenceUserRole.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >RestPreferenceUserRole.Builder<_B> copyOf(final RestPreferenceUserRole _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final RestPreferenceUserRole.Builder<_B> _newBuilder = new RestPreferenceUserRole.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static RestPreferenceUserRole.Builder<Void> copyExcept(final RestPreferenceUserRole _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static RestPreferenceUserRole.Builder<Void> copyOnly(final RestPreferenceUserRole _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final RestPreferenceUserRole _storedValue;
        private String userRoleId;
        private String name;
        private Boolean shared;

        public Builder(final _B _parentBuilder, final RestPreferenceUserRole _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.userRoleId = _other.userRoleId;
                    this.name = _other.name;
                    this.shared = _other.shared;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final RestPreferenceUserRole _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree userRoleIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("userRoleId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(userRoleIdPropertyTree!= null):((userRoleIdPropertyTree == null)||(!userRoleIdPropertyTree.isLeaf())))) {
                        this.userRoleId = _other.userRoleId;
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree sharedPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("shared"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(sharedPropertyTree!= null):((sharedPropertyTree == null)||(!sharedPropertyTree.isLeaf())))) {
                        this.shared = _other.shared;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends RestPreferenceUserRole >_P init(final _P _product) {
            _product.userRoleId = this.userRoleId;
            _product.name = this.name;
            _product.shared = this.shared;
            return _product;
        }

        /**
         * Sets the new value of "userRoleId" (any previous value will be replaced)
         * 
         * @param userRoleId
         *     New value of the "userRoleId" property.
         */
        public RestPreferenceUserRole.Builder<_B> withUserRoleId(final String userRoleId) {
            this.userRoleId = userRoleId;
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public RestPreferenceUserRole.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "shared" (any previous value will be replaced)
         * 
         * @param shared
         *     New value of the "shared" property.
         */
        public RestPreferenceUserRole.Builder<_B> withShared(final Boolean shared) {
            this.shared = shared;
            return this;
        }

        @Override
        public RestPreferenceUserRole build() {
            if (_storedValue == null) {
                return this.init(new RestPreferenceUserRole());
            } else {
                return ((RestPreferenceUserRole) _storedValue);
            }
        }

        public RestPreferenceUserRole.Builder<_B> copyOf(final RestPreferenceUserRole _other) {
            _other.copyTo(this);
            return this;
        }

        public RestPreferenceUserRole.Builder<_B> copyOf(final RestPreferenceUserRole.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends RestPreferenceUserRole.Selector<RestPreferenceUserRole.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static RestPreferenceUserRole.Select _root() {
            return new RestPreferenceUserRole.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, RestPreferenceUserRole.Selector<TRoot, TParent>> userRoleId = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestPreferenceUserRole.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestPreferenceUserRole.Selector<TRoot, TParent>> shared = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.userRoleId!= null) {
                products.put("userRoleId", this.userRoleId.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.shared!= null) {
                products.put("shared", this.shared.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestPreferenceUserRole.Selector<TRoot, TParent>> userRoleId() {
            return ((this.userRoleId == null)?this.userRoleId = new com.kscs.util.jaxb.Selector<TRoot, RestPreferenceUserRole.Selector<TRoot, TParent>>(this._root, this, "userRoleId"):this.userRoleId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestPreferenceUserRole.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, RestPreferenceUserRole.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestPreferenceUserRole.Selector<TRoot, TParent>> shared() {
            return ((this.shared == null)?this.shared = new com.kscs.util.jaxb.Selector<TRoot, RestPreferenceUserRole.Selector<TRoot, TParent>>(this._root, this, "shared"):this.shared);
        }

    }

}
