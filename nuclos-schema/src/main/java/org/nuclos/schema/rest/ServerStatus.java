
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for server-status complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="server-status"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="ready" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "server-status")
public class ServerStatus implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "ready", required = true)
    protected boolean ready;

    /**
     * Gets the value of the ready property.
     * 
     */
    public boolean isReady() {
        return ready;
    }

    /**
     * Sets the value of the ready property.
     * 
     */
    public void setReady(boolean value) {
        this.ready = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            boolean theReady;
            theReady = this.isReady();
            strategy.appendField(locator, this, "ready", buffer, theReady);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ServerStatus)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ServerStatus that = ((ServerStatus) object);
        {
            boolean lhsReady;
            lhsReady = this.isReady();
            boolean rhsReady;
            rhsReady = that.isReady();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ready", lhsReady), LocatorUtils.property(thatLocator, "ready", rhsReady), lhsReady, rhsReady)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            boolean theReady;
            theReady = this.isReady();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ready", theReady), currentHashCode, theReady);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof ServerStatus) {
            final ServerStatus copy = ((ServerStatus) draftCopy);
            {
                boolean sourceReady;
                sourceReady = this.isReady();
                boolean copyReady = strategy.copy(LocatorUtils.property(locator, "ready", sourceReady), sourceReady);
                copy.setReady(copyReady);
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ServerStatus();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final ServerStatus.Builder<_B> _other) {
        _other.ready = this.ready;
    }

    public<_B >ServerStatus.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new ServerStatus.Builder<_B>(_parentBuilder, this, true);
    }

    public ServerStatus.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static ServerStatus.Builder<Void> builder() {
        return new ServerStatus.Builder<Void>(null, null, false);
    }

    public static<_B >ServerStatus.Builder<_B> copyOf(final ServerStatus _other) {
        final ServerStatus.Builder<_B> _newBuilder = new ServerStatus.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final ServerStatus.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree readyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("ready"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(readyPropertyTree!= null):((readyPropertyTree == null)||(!readyPropertyTree.isLeaf())))) {
            _other.ready = this.ready;
        }
    }

    public<_B >ServerStatus.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new ServerStatus.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public ServerStatus.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >ServerStatus.Builder<_B> copyOf(final ServerStatus _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final ServerStatus.Builder<_B> _newBuilder = new ServerStatus.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static ServerStatus.Builder<Void> copyExcept(final ServerStatus _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static ServerStatus.Builder<Void> copyOnly(final ServerStatus _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final ServerStatus _storedValue;
        private boolean ready;

        public Builder(final _B _parentBuilder, final ServerStatus _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.ready = _other.ready;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final ServerStatus _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree readyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("ready"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(readyPropertyTree!= null):((readyPropertyTree == null)||(!readyPropertyTree.isLeaf())))) {
                        this.ready = _other.ready;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends ServerStatus >_P init(final _P _product) {
            _product.ready = this.ready;
            return _product;
        }

        /**
         * Sets the new value of "ready" (any previous value will be replaced)
         * 
         * @param ready
         *     New value of the "ready" property.
         */
        public ServerStatus.Builder<_B> withReady(final boolean ready) {
            this.ready = ready;
            return this;
        }

        @Override
        public ServerStatus build() {
            if (_storedValue == null) {
                return this.init(new ServerStatus());
            } else {
                return ((ServerStatus) _storedValue);
            }
        }

        public ServerStatus.Builder<_B> copyOf(final ServerStatus _other) {
            _other.copyTo(this);
            return this;
        }

        public ServerStatus.Builder<_B> copyOf(final ServerStatus.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends ServerStatus.Selector<ServerStatus.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static ServerStatus.Select _root() {
            return new ServerStatus.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {


        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            return products;
        }

    }

}
