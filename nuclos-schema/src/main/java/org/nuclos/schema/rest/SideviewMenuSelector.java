
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for sideview-menu-selector complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="sideview-menu-selector"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="links" type="{urn:org.nuclos.schema.rest}sideview-menu-selector-links"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="label" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="boMetaId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="boId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sideview-menu-selector", propOrder = {
    "links"
})
public class SideviewMenuSelector implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected SideviewMenuSelectorLinks links;
    @XmlAttribute(name = "type")
    protected String type;
    @XmlAttribute(name = "label")
    protected String label;
    @XmlAttribute(name = "boMetaId")
    protected String boMetaId;
    @XmlAttribute(name = "boId")
    protected String boId;

    /**
     * Gets the value of the links property.
     * 
     * @return
     *     possible object is
     *     {@link SideviewMenuSelectorLinks }
     *     
     */
    public SideviewMenuSelectorLinks getLinks() {
        return links;
    }

    /**
     * Sets the value of the links property.
     * 
     * @param value
     *     allowed object is
     *     {@link SideviewMenuSelectorLinks }
     *     
     */
    public void setLinks(SideviewMenuSelectorLinks value) {
        this.links = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the boMetaId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoMetaId() {
        return boMetaId;
    }

    /**
     * Sets the value of the boMetaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoMetaId(String value) {
        this.boMetaId = value;
    }

    /**
     * Gets the value of the boId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoId() {
        return boId;
    }

    /**
     * Sets the value of the boId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoId(String value) {
        this.boId = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            SideviewMenuSelectorLinks theLinks;
            theLinks = this.getLinks();
            strategy.appendField(locator, this, "links", buffer, theLinks);
        }
        {
            String theType;
            theType = this.getType();
            strategy.appendField(locator, this, "type", buffer, theType);
        }
        {
            String theLabel;
            theLabel = this.getLabel();
            strategy.appendField(locator, this, "label", buffer, theLabel);
        }
        {
            String theBoMetaId;
            theBoMetaId = this.getBoMetaId();
            strategy.appendField(locator, this, "boMetaId", buffer, theBoMetaId);
        }
        {
            String theBoId;
            theBoId = this.getBoId();
            strategy.appendField(locator, this, "boId", buffer, theBoId);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof SideviewMenuSelector)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final SideviewMenuSelector that = ((SideviewMenuSelector) object);
        {
            SideviewMenuSelectorLinks lhsLinks;
            lhsLinks = this.getLinks();
            SideviewMenuSelectorLinks rhsLinks;
            rhsLinks = that.getLinks();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "links", lhsLinks), LocatorUtils.property(thatLocator, "links", rhsLinks), lhsLinks, rhsLinks)) {
                return false;
            }
        }
        {
            String lhsType;
            lhsType = this.getType();
            String rhsType;
            rhsType = that.getType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "type", lhsType), LocatorUtils.property(thatLocator, "type", rhsType), lhsType, rhsType)) {
                return false;
            }
        }
        {
            String lhsLabel;
            lhsLabel = this.getLabel();
            String rhsLabel;
            rhsLabel = that.getLabel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "label", lhsLabel), LocatorUtils.property(thatLocator, "label", rhsLabel), lhsLabel, rhsLabel)) {
                return false;
            }
        }
        {
            String lhsBoMetaId;
            lhsBoMetaId = this.getBoMetaId();
            String rhsBoMetaId;
            rhsBoMetaId = that.getBoMetaId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "boMetaId", lhsBoMetaId), LocatorUtils.property(thatLocator, "boMetaId", rhsBoMetaId), lhsBoMetaId, rhsBoMetaId)) {
                return false;
            }
        }
        {
            String lhsBoId;
            lhsBoId = this.getBoId();
            String rhsBoId;
            rhsBoId = that.getBoId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "boId", lhsBoId), LocatorUtils.property(thatLocator, "boId", rhsBoId), lhsBoId, rhsBoId)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            SideviewMenuSelectorLinks theLinks;
            theLinks = this.getLinks();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "links", theLinks), currentHashCode, theLinks);
        }
        {
            String theType;
            theType = this.getType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "type", theType), currentHashCode, theType);
        }
        {
            String theLabel;
            theLabel = this.getLabel();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "label", theLabel), currentHashCode, theLabel);
        }
        {
            String theBoMetaId;
            theBoMetaId = this.getBoMetaId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "boMetaId", theBoMetaId), currentHashCode, theBoMetaId);
        }
        {
            String theBoId;
            theBoId = this.getBoId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "boId", theBoId), currentHashCode, theBoId);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof SideviewMenuSelector) {
            final SideviewMenuSelector copy = ((SideviewMenuSelector) draftCopy);
            if (this.links!= null) {
                SideviewMenuSelectorLinks sourceLinks;
                sourceLinks = this.getLinks();
                SideviewMenuSelectorLinks copyLinks = ((SideviewMenuSelectorLinks) strategy.copy(LocatorUtils.property(locator, "links", sourceLinks), sourceLinks));
                copy.setLinks(copyLinks);
            } else {
                copy.links = null;
            }
            if (this.type!= null) {
                String sourceType;
                sourceType = this.getType();
                String copyType = ((String) strategy.copy(LocatorUtils.property(locator, "type", sourceType), sourceType));
                copy.setType(copyType);
            } else {
                copy.type = null;
            }
            if (this.label!= null) {
                String sourceLabel;
                sourceLabel = this.getLabel();
                String copyLabel = ((String) strategy.copy(LocatorUtils.property(locator, "label", sourceLabel), sourceLabel));
                copy.setLabel(copyLabel);
            } else {
                copy.label = null;
            }
            if (this.boMetaId!= null) {
                String sourceBoMetaId;
                sourceBoMetaId = this.getBoMetaId();
                String copyBoMetaId = ((String) strategy.copy(LocatorUtils.property(locator, "boMetaId", sourceBoMetaId), sourceBoMetaId));
                copy.setBoMetaId(copyBoMetaId);
            } else {
                copy.boMetaId = null;
            }
            if (this.boId!= null) {
                String sourceBoId;
                sourceBoId = this.getBoId();
                String copyBoId = ((String) strategy.copy(LocatorUtils.property(locator, "boId", sourceBoId), sourceBoId));
                copy.setBoId(copyBoId);
            } else {
                copy.boId = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new SideviewMenuSelector();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final SideviewMenuSelector.Builder<_B> _other) {
        _other.links = ((this.links == null)?null:this.links.newCopyBuilder(_other));
        _other.type = this.type;
        _other.label = this.label;
        _other.boMetaId = this.boMetaId;
        _other.boId = this.boId;
    }

    public<_B >SideviewMenuSelector.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new SideviewMenuSelector.Builder<_B>(_parentBuilder, this, true);
    }

    public SideviewMenuSelector.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static SideviewMenuSelector.Builder<Void> builder() {
        return new SideviewMenuSelector.Builder<Void>(null, null, false);
    }

    public static<_B >SideviewMenuSelector.Builder<_B> copyOf(final SideviewMenuSelector _other) {
        final SideviewMenuSelector.Builder<_B> _newBuilder = new SideviewMenuSelector.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final SideviewMenuSelector.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree linksPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("links"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(linksPropertyTree!= null):((linksPropertyTree == null)||(!linksPropertyTree.isLeaf())))) {
            _other.links = ((this.links == null)?null:this.links.newCopyBuilder(_other, linksPropertyTree, _propertyTreeUse));
        }
        final PropertyTree typePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("type"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(typePropertyTree!= null):((typePropertyTree == null)||(!typePropertyTree.isLeaf())))) {
            _other.type = this.type;
        }
        final PropertyTree labelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("label"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(labelPropertyTree!= null):((labelPropertyTree == null)||(!labelPropertyTree.isLeaf())))) {
            _other.label = this.label;
        }
        final PropertyTree boMetaIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boMetaId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boMetaIdPropertyTree!= null):((boMetaIdPropertyTree == null)||(!boMetaIdPropertyTree.isLeaf())))) {
            _other.boMetaId = this.boMetaId;
        }
        final PropertyTree boIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boIdPropertyTree!= null):((boIdPropertyTree == null)||(!boIdPropertyTree.isLeaf())))) {
            _other.boId = this.boId;
        }
    }

    public<_B >SideviewMenuSelector.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new SideviewMenuSelector.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public SideviewMenuSelector.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >SideviewMenuSelector.Builder<_B> copyOf(final SideviewMenuSelector _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final SideviewMenuSelector.Builder<_B> _newBuilder = new SideviewMenuSelector.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static SideviewMenuSelector.Builder<Void> copyExcept(final SideviewMenuSelector _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static SideviewMenuSelector.Builder<Void> copyOnly(final SideviewMenuSelector _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final SideviewMenuSelector _storedValue;
        private SideviewMenuSelectorLinks.Builder<SideviewMenuSelector.Builder<_B>> links;
        private String type;
        private String label;
        private String boMetaId;
        private String boId;

        public Builder(final _B _parentBuilder, final SideviewMenuSelector _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.links = ((_other.links == null)?null:_other.links.newCopyBuilder(this));
                    this.type = _other.type;
                    this.label = _other.label;
                    this.boMetaId = _other.boMetaId;
                    this.boId = _other.boId;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final SideviewMenuSelector _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree linksPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("links"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(linksPropertyTree!= null):((linksPropertyTree == null)||(!linksPropertyTree.isLeaf())))) {
                        this.links = ((_other.links == null)?null:_other.links.newCopyBuilder(this, linksPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree typePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("type"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(typePropertyTree!= null):((typePropertyTree == null)||(!typePropertyTree.isLeaf())))) {
                        this.type = _other.type;
                    }
                    final PropertyTree labelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("label"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(labelPropertyTree!= null):((labelPropertyTree == null)||(!labelPropertyTree.isLeaf())))) {
                        this.label = _other.label;
                    }
                    final PropertyTree boMetaIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boMetaId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boMetaIdPropertyTree!= null):((boMetaIdPropertyTree == null)||(!boMetaIdPropertyTree.isLeaf())))) {
                        this.boMetaId = _other.boMetaId;
                    }
                    final PropertyTree boIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boIdPropertyTree!= null):((boIdPropertyTree == null)||(!boIdPropertyTree.isLeaf())))) {
                        this.boId = _other.boId;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends SideviewMenuSelector >_P init(final _P _product) {
            _product.links = ((this.links == null)?null:this.links.build());
            _product.type = this.type;
            _product.label = this.label;
            _product.boMetaId = this.boMetaId;
            _product.boId = this.boId;
            return _product;
        }

        /**
         * Sets the new value of "links" (any previous value will be replaced)
         * 
         * @param links
         *     New value of the "links" property.
         */
        public SideviewMenuSelector.Builder<_B> withLinks(final SideviewMenuSelectorLinks links) {
            this.links = ((links == null)?null:new SideviewMenuSelectorLinks.Builder<SideviewMenuSelector.Builder<_B>>(this, links, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "links" property.
         * Use {@link org.nuclos.schema.rest.SideviewMenuSelectorLinks.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "links" property.
         *     Use {@link org.nuclos.schema.rest.SideviewMenuSelectorLinks.Builder#end()} to return to the current builder.
         */
        public SideviewMenuSelectorLinks.Builder<? extends SideviewMenuSelector.Builder<_B>> withLinks() {
            if (this.links!= null) {
                return this.links;
            }
            return this.links = new SideviewMenuSelectorLinks.Builder<SideviewMenuSelector.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "type" (any previous value will be replaced)
         * 
         * @param type
         *     New value of the "type" property.
         */
        public SideviewMenuSelector.Builder<_B> withType(final String type) {
            this.type = type;
            return this;
        }

        /**
         * Sets the new value of "label" (any previous value will be replaced)
         * 
         * @param label
         *     New value of the "label" property.
         */
        public SideviewMenuSelector.Builder<_B> withLabel(final String label) {
            this.label = label;
            return this;
        }

        /**
         * Sets the new value of "boMetaId" (any previous value will be replaced)
         * 
         * @param boMetaId
         *     New value of the "boMetaId" property.
         */
        public SideviewMenuSelector.Builder<_B> withBoMetaId(final String boMetaId) {
            this.boMetaId = boMetaId;
            return this;
        }

        /**
         * Sets the new value of "boId" (any previous value will be replaced)
         * 
         * @param boId
         *     New value of the "boId" property.
         */
        public SideviewMenuSelector.Builder<_B> withBoId(final String boId) {
            this.boId = boId;
            return this;
        }

        @Override
        public SideviewMenuSelector build() {
            if (_storedValue == null) {
                return this.init(new SideviewMenuSelector());
            } else {
                return ((SideviewMenuSelector) _storedValue);
            }
        }

        public SideviewMenuSelector.Builder<_B> copyOf(final SideviewMenuSelector _other) {
            _other.copyTo(this);
            return this;
        }

        public SideviewMenuSelector.Builder<_B> copyOf(final SideviewMenuSelector.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends SideviewMenuSelector.Selector<SideviewMenuSelector.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static SideviewMenuSelector.Select _root() {
            return new SideviewMenuSelector.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private SideviewMenuSelectorLinks.Selector<TRoot, SideviewMenuSelector.Selector<TRoot, TParent>> links = null;
        private com.kscs.util.jaxb.Selector<TRoot, SideviewMenuSelector.Selector<TRoot, TParent>> type = null;
        private com.kscs.util.jaxb.Selector<TRoot, SideviewMenuSelector.Selector<TRoot, TParent>> label = null;
        private com.kscs.util.jaxb.Selector<TRoot, SideviewMenuSelector.Selector<TRoot, TParent>> boMetaId = null;
        private com.kscs.util.jaxb.Selector<TRoot, SideviewMenuSelector.Selector<TRoot, TParent>> boId = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.links!= null) {
                products.put("links", this.links.init());
            }
            if (this.type!= null) {
                products.put("type", this.type.init());
            }
            if (this.label!= null) {
                products.put("label", this.label.init());
            }
            if (this.boMetaId!= null) {
                products.put("boMetaId", this.boMetaId.init());
            }
            if (this.boId!= null) {
                products.put("boId", this.boId.init());
            }
            return products;
        }

        public SideviewMenuSelectorLinks.Selector<TRoot, SideviewMenuSelector.Selector<TRoot, TParent>> links() {
            return ((this.links == null)?this.links = new SideviewMenuSelectorLinks.Selector<TRoot, SideviewMenuSelector.Selector<TRoot, TParent>>(this._root, this, "links"):this.links);
        }

        public com.kscs.util.jaxb.Selector<TRoot, SideviewMenuSelector.Selector<TRoot, TParent>> type() {
            return ((this.type == null)?this.type = new com.kscs.util.jaxb.Selector<TRoot, SideviewMenuSelector.Selector<TRoot, TParent>>(this._root, this, "type"):this.type);
        }

        public com.kscs.util.jaxb.Selector<TRoot, SideviewMenuSelector.Selector<TRoot, TParent>> label() {
            return ((this.label == null)?this.label = new com.kscs.util.jaxb.Selector<TRoot, SideviewMenuSelector.Selector<TRoot, TParent>>(this._root, this, "label"):this.label);
        }

        public com.kscs.util.jaxb.Selector<TRoot, SideviewMenuSelector.Selector<TRoot, TParent>> boMetaId() {
            return ((this.boMetaId == null)?this.boMetaId = new com.kscs.util.jaxb.Selector<TRoot, SideviewMenuSelector.Selector<TRoot, TParent>>(this._root, this, "boMetaId"):this.boMetaId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, SideviewMenuSelector.Selector<TRoot, TParent>> boId() {
            return ((this.boId == null)?this.boId = new com.kscs.util.jaxb.Selector<TRoot, SideviewMenuSelector.Selector<TRoot, TParent>>(this._root, this, "boId"):this.boId);
        }

    }

}
