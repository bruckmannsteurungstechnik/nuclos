
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for tree-node complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tree-node"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="boMetaId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="boId" type="{http://www.w3.org/2001/XMLSchema}long" /&gt;
 *       &lt;attribute name="node_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="title" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="icon" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tree-node")
public class TreeNode implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "boMetaId")
    protected String boMetaId;
    @XmlAttribute(name = "boId")
    protected Long boId;
    @XmlAttribute(name = "node_id")
    protected String nodeId;
    @XmlAttribute(name = "title")
    protected String title;
    @XmlAttribute(name = "icon")
    protected String icon;

    /**
     * Gets the value of the boMetaId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoMetaId() {
        return boMetaId;
    }

    /**
     * Sets the value of the boMetaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoMetaId(String value) {
        this.boMetaId = value;
    }

    /**
     * Gets the value of the boId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getBoId() {
        return boId;
    }

    /**
     * Sets the value of the boId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setBoId(Long value) {
        this.boId = value;
    }

    /**
     * Gets the value of the nodeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNodeId() {
        return nodeId;
    }

    /**
     * Sets the value of the nodeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNodeId(String value) {
        this.nodeId = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the icon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIcon() {
        return icon;
    }

    /**
     * Sets the value of the icon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIcon(String value) {
        this.icon = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theBoMetaId;
            theBoMetaId = this.getBoMetaId();
            strategy.appendField(locator, this, "boMetaId", buffer, theBoMetaId);
        }
        {
            Long theBoId;
            theBoId = this.getBoId();
            strategy.appendField(locator, this, "boId", buffer, theBoId);
        }
        {
            String theNodeId;
            theNodeId = this.getNodeId();
            strategy.appendField(locator, this, "nodeId", buffer, theNodeId);
        }
        {
            String theTitle;
            theTitle = this.getTitle();
            strategy.appendField(locator, this, "title", buffer, theTitle);
        }
        {
            String theIcon;
            theIcon = this.getIcon();
            strategy.appendField(locator, this, "icon", buffer, theIcon);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof TreeNode)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final TreeNode that = ((TreeNode) object);
        {
            String lhsBoMetaId;
            lhsBoMetaId = this.getBoMetaId();
            String rhsBoMetaId;
            rhsBoMetaId = that.getBoMetaId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "boMetaId", lhsBoMetaId), LocatorUtils.property(thatLocator, "boMetaId", rhsBoMetaId), lhsBoMetaId, rhsBoMetaId)) {
                return false;
            }
        }
        {
            Long lhsBoId;
            lhsBoId = this.getBoId();
            Long rhsBoId;
            rhsBoId = that.getBoId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "boId", lhsBoId), LocatorUtils.property(thatLocator, "boId", rhsBoId), lhsBoId, rhsBoId)) {
                return false;
            }
        }
        {
            String lhsNodeId;
            lhsNodeId = this.getNodeId();
            String rhsNodeId;
            rhsNodeId = that.getNodeId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "nodeId", lhsNodeId), LocatorUtils.property(thatLocator, "nodeId", rhsNodeId), lhsNodeId, rhsNodeId)) {
                return false;
            }
        }
        {
            String lhsTitle;
            lhsTitle = this.getTitle();
            String rhsTitle;
            rhsTitle = that.getTitle();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "title", lhsTitle), LocatorUtils.property(thatLocator, "title", rhsTitle), lhsTitle, rhsTitle)) {
                return false;
            }
        }
        {
            String lhsIcon;
            lhsIcon = this.getIcon();
            String rhsIcon;
            rhsIcon = that.getIcon();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "icon", lhsIcon), LocatorUtils.property(thatLocator, "icon", rhsIcon), lhsIcon, rhsIcon)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theBoMetaId;
            theBoMetaId = this.getBoMetaId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "boMetaId", theBoMetaId), currentHashCode, theBoMetaId);
        }
        {
            Long theBoId;
            theBoId = this.getBoId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "boId", theBoId), currentHashCode, theBoId);
        }
        {
            String theNodeId;
            theNodeId = this.getNodeId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nodeId", theNodeId), currentHashCode, theNodeId);
        }
        {
            String theTitle;
            theTitle = this.getTitle();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "title", theTitle), currentHashCode, theTitle);
        }
        {
            String theIcon;
            theIcon = this.getIcon();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "icon", theIcon), currentHashCode, theIcon);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof TreeNode) {
            final TreeNode copy = ((TreeNode) draftCopy);
            if (this.boMetaId!= null) {
                String sourceBoMetaId;
                sourceBoMetaId = this.getBoMetaId();
                String copyBoMetaId = ((String) strategy.copy(LocatorUtils.property(locator, "boMetaId", sourceBoMetaId), sourceBoMetaId));
                copy.setBoMetaId(copyBoMetaId);
            } else {
                copy.boMetaId = null;
            }
            if (this.boId!= null) {
                Long sourceBoId;
                sourceBoId = this.getBoId();
                Long copyBoId = ((Long) strategy.copy(LocatorUtils.property(locator, "boId", sourceBoId), sourceBoId));
                copy.setBoId(copyBoId);
            } else {
                copy.boId = null;
            }
            if (this.nodeId!= null) {
                String sourceNodeId;
                sourceNodeId = this.getNodeId();
                String copyNodeId = ((String) strategy.copy(LocatorUtils.property(locator, "nodeId", sourceNodeId), sourceNodeId));
                copy.setNodeId(copyNodeId);
            } else {
                copy.nodeId = null;
            }
            if (this.title!= null) {
                String sourceTitle;
                sourceTitle = this.getTitle();
                String copyTitle = ((String) strategy.copy(LocatorUtils.property(locator, "title", sourceTitle), sourceTitle));
                copy.setTitle(copyTitle);
            } else {
                copy.title = null;
            }
            if (this.icon!= null) {
                String sourceIcon;
                sourceIcon = this.getIcon();
                String copyIcon = ((String) strategy.copy(LocatorUtils.property(locator, "icon", sourceIcon), sourceIcon));
                copy.setIcon(copyIcon);
            } else {
                copy.icon = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new TreeNode();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final TreeNode.Builder<_B> _other) {
        _other.boMetaId = this.boMetaId;
        _other.boId = this.boId;
        _other.nodeId = this.nodeId;
        _other.title = this.title;
        _other.icon = this.icon;
    }

    public<_B >TreeNode.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new TreeNode.Builder<_B>(_parentBuilder, this, true);
    }

    public TreeNode.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static TreeNode.Builder<Void> builder() {
        return new TreeNode.Builder<Void>(null, null, false);
    }

    public static<_B >TreeNode.Builder<_B> copyOf(final TreeNode _other) {
        final TreeNode.Builder<_B> _newBuilder = new TreeNode.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final TreeNode.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree boMetaIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boMetaId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boMetaIdPropertyTree!= null):((boMetaIdPropertyTree == null)||(!boMetaIdPropertyTree.isLeaf())))) {
            _other.boMetaId = this.boMetaId;
        }
        final PropertyTree boIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boIdPropertyTree!= null):((boIdPropertyTree == null)||(!boIdPropertyTree.isLeaf())))) {
            _other.boId = this.boId;
        }
        final PropertyTree nodeIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nodeId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nodeIdPropertyTree!= null):((nodeIdPropertyTree == null)||(!nodeIdPropertyTree.isLeaf())))) {
            _other.nodeId = this.nodeId;
        }
        final PropertyTree titlePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("title"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(titlePropertyTree!= null):((titlePropertyTree == null)||(!titlePropertyTree.isLeaf())))) {
            _other.title = this.title;
        }
        final PropertyTree iconPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("icon"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(iconPropertyTree!= null):((iconPropertyTree == null)||(!iconPropertyTree.isLeaf())))) {
            _other.icon = this.icon;
        }
    }

    public<_B >TreeNode.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new TreeNode.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public TreeNode.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >TreeNode.Builder<_B> copyOf(final TreeNode _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final TreeNode.Builder<_B> _newBuilder = new TreeNode.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static TreeNode.Builder<Void> copyExcept(final TreeNode _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static TreeNode.Builder<Void> copyOnly(final TreeNode _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final TreeNode _storedValue;
        private String boMetaId;
        private Long boId;
        private String nodeId;
        private String title;
        private String icon;

        public Builder(final _B _parentBuilder, final TreeNode _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.boMetaId = _other.boMetaId;
                    this.boId = _other.boId;
                    this.nodeId = _other.nodeId;
                    this.title = _other.title;
                    this.icon = _other.icon;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final TreeNode _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree boMetaIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boMetaId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boMetaIdPropertyTree!= null):((boMetaIdPropertyTree == null)||(!boMetaIdPropertyTree.isLeaf())))) {
                        this.boMetaId = _other.boMetaId;
                    }
                    final PropertyTree boIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boIdPropertyTree!= null):((boIdPropertyTree == null)||(!boIdPropertyTree.isLeaf())))) {
                        this.boId = _other.boId;
                    }
                    final PropertyTree nodeIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nodeId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nodeIdPropertyTree!= null):((nodeIdPropertyTree == null)||(!nodeIdPropertyTree.isLeaf())))) {
                        this.nodeId = _other.nodeId;
                    }
                    final PropertyTree titlePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("title"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(titlePropertyTree!= null):((titlePropertyTree == null)||(!titlePropertyTree.isLeaf())))) {
                        this.title = _other.title;
                    }
                    final PropertyTree iconPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("icon"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(iconPropertyTree!= null):((iconPropertyTree == null)||(!iconPropertyTree.isLeaf())))) {
                        this.icon = _other.icon;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends TreeNode >_P init(final _P _product) {
            _product.boMetaId = this.boMetaId;
            _product.boId = this.boId;
            _product.nodeId = this.nodeId;
            _product.title = this.title;
            _product.icon = this.icon;
            return _product;
        }

        /**
         * Sets the new value of "boMetaId" (any previous value will be replaced)
         * 
         * @param boMetaId
         *     New value of the "boMetaId" property.
         */
        public TreeNode.Builder<_B> withBoMetaId(final String boMetaId) {
            this.boMetaId = boMetaId;
            return this;
        }

        /**
         * Sets the new value of "boId" (any previous value will be replaced)
         * 
         * @param boId
         *     New value of the "boId" property.
         */
        public TreeNode.Builder<_B> withBoId(final Long boId) {
            this.boId = boId;
            return this;
        }

        /**
         * Sets the new value of "nodeId" (any previous value will be replaced)
         * 
         * @param nodeId
         *     New value of the "nodeId" property.
         */
        public TreeNode.Builder<_B> withNodeId(final String nodeId) {
            this.nodeId = nodeId;
            return this;
        }

        /**
         * Sets the new value of "title" (any previous value will be replaced)
         * 
         * @param title
         *     New value of the "title" property.
         */
        public TreeNode.Builder<_B> withTitle(final String title) {
            this.title = title;
            return this;
        }

        /**
         * Sets the new value of "icon" (any previous value will be replaced)
         * 
         * @param icon
         *     New value of the "icon" property.
         */
        public TreeNode.Builder<_B> withIcon(final String icon) {
            this.icon = icon;
            return this;
        }

        @Override
        public TreeNode build() {
            if (_storedValue == null) {
                return this.init(new TreeNode());
            } else {
                return ((TreeNode) _storedValue);
            }
        }

        public TreeNode.Builder<_B> copyOf(final TreeNode _other) {
            _other.copyTo(this);
            return this;
        }

        public TreeNode.Builder<_B> copyOf(final TreeNode.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends TreeNode.Selector<TreeNode.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static TreeNode.Select _root() {
            return new TreeNode.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, TreeNode.Selector<TRoot, TParent>> boMetaId = null;
        private com.kscs.util.jaxb.Selector<TRoot, TreeNode.Selector<TRoot, TParent>> boId = null;
        private com.kscs.util.jaxb.Selector<TRoot, TreeNode.Selector<TRoot, TParent>> nodeId = null;
        private com.kscs.util.jaxb.Selector<TRoot, TreeNode.Selector<TRoot, TParent>> title = null;
        private com.kscs.util.jaxb.Selector<TRoot, TreeNode.Selector<TRoot, TParent>> icon = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.boMetaId!= null) {
                products.put("boMetaId", this.boMetaId.init());
            }
            if (this.boId!= null) {
                products.put("boId", this.boId.init());
            }
            if (this.nodeId!= null) {
                products.put("nodeId", this.nodeId.init());
            }
            if (this.title!= null) {
                products.put("title", this.title.init());
            }
            if (this.icon!= null) {
                products.put("icon", this.icon.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, TreeNode.Selector<TRoot, TParent>> boMetaId() {
            return ((this.boMetaId == null)?this.boMetaId = new com.kscs.util.jaxb.Selector<TRoot, TreeNode.Selector<TRoot, TParent>>(this._root, this, "boMetaId"):this.boMetaId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, TreeNode.Selector<TRoot, TParent>> boId() {
            return ((this.boId == null)?this.boId = new com.kscs.util.jaxb.Selector<TRoot, TreeNode.Selector<TRoot, TParent>>(this._root, this, "boId"):this.boId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, TreeNode.Selector<TRoot, TParent>> nodeId() {
            return ((this.nodeId == null)?this.nodeId = new com.kscs.util.jaxb.Selector<TRoot, TreeNode.Selector<TRoot, TParent>>(this._root, this, "nodeId"):this.nodeId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, TreeNode.Selector<TRoot, TParent>> title() {
            return ((this.title == null)?this.title = new com.kscs.util.jaxb.Selector<TRoot, TreeNode.Selector<TRoot, TParent>>(this._root, this, "title"):this.title);
        }

        public com.kscs.util.jaxb.Selector<TRoot, TreeNode.Selector<TRoot, TParent>> icon() {
            return ((this.icon == null)?this.icon = new com.kscs.util.jaxb.Selector<TRoot, TreeNode.Selector<TRoot, TParent>>(this._root, this, "icon"):this.icon);
        }

    }

}
