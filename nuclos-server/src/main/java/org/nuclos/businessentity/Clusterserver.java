//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.UID; 
import org.nuclos.server.nbo.AbstractBusinessObject; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import java.util.Date; 
import java.util.List; 
import java.util.ArrayList; 

/**
 * BusinessObject: nuclos_cluster_server
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_MD_CLUSTER_SERVER
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
public class Clusterserver extends AbstractBusinessObject<org.nuclos.common.UID> implements Modifiable<org.nuclos.common.UID> {
private static final long serialVersionUID = 1L;



/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<org.nuclos.common.UID> Id = 
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "IOTx", "IOTx0", org.nuclos.common.UID.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "IOTx", "IOTx3", java.util.Date.class);


/**
 * Attribute: register
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: DATREGISTER
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> Register = 
	new NumericAttribute<>("Register", "org.nuclos.businessentity", "IOTx", "IOTxc", java.util.Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "IOTx", "IOTx2", java.lang.String.class);


/**
 * Attribute: servername
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: STRSERVERNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Servername = new StringAttribute<>("Servername", "org.nuclos.businessentity", "IOTx", "IOTxb", java.lang.String.class);


/**
 * Attribute: autosetuprunning
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: BLNAUTOSETUPRUNNING
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Autosetuprunning = 
	new Attribute<>("Autosetuprunning", "org.nuclos.businessentity", "IOTx", "IOTxe", java.lang.Boolean.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "IOTx", "IOTx4", java.lang.String.class);


/**
 * Attribute: master
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: BLNMASTER
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Master = 
	new Attribute<>("Master", "org.nuclos.businessentity", "IOTx", "IOTxd", java.lang.Boolean.class);


/**
 * Attribute: servercontext
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: STRSERVERCONTEXT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Servercontext = new StringAttribute<>("Servercontext", "org.nuclos.businessentity", "IOTx", "IOTxg", java.lang.String.class);


/**
 * Attribute: port
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: STRPORT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Port = new StringAttribute<>("Port", "org.nuclos.businessentity", "IOTx", "IOTxf", java.lang.String.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "IOTx", "IOTx1", java.util.Date.class);


/**
 * Attribute: serverip
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: STRSERVERIP
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Serverip = new StringAttribute<>("Serverip", "org.nuclos.businessentity", "IOTx", "IOTxa", java.lang.String.class);


public Clusterserver() {
		super("IOTx");
		setAutosetuprunning(java.lang.Boolean.FALSE);
		setMaster(java.lang.Boolean.FALSE);
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("IOTx");
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
@java.lang.Override
public void setId(org.nuclos.common.UID id) {
		super.setId(id);
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getChangedAt() {
		return getField("IOTx3", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: register
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: DATREGISTER
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getRegister() {
		return getField("IOTxc", java.util.Date.class); 
}


/**
 * Setter-Method for attribute: register
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: DATREGISTER
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setRegister(java.util.Date pRegister) {
		setField("IOTxc", pRegister); 
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getCreatedBy() {
		return getField("IOTx2", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: servername
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: STRSERVERNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.String getServername() {
		return getField("IOTxb", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: servername
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: STRSERVERNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setServername(java.lang.String pServername) {
		setField("IOTxb", pServername); 
}


/**
 * Getter-Method for attribute: autosetuprunning
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: BLNAUTOSETUPRUNNING
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getAutosetuprunning() {
		return getField("IOTxe", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: autosetuprunning
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: BLNAUTOSETUPRUNNING
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setAutosetuprunning(java.lang.Boolean pAutosetuprunning) {
		setField("IOTxe", pAutosetuprunning); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getChangedBy() {
		return getField("IOTx4", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: master
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: BLNMASTER
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getMaster() {
		return getField("IOTxd", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: master
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: BLNMASTER
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setMaster(java.lang.Boolean pMaster) {
		setField("IOTxd", pMaster); 
}


/**
 * Getter-Method for attribute: servercontext
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: STRSERVERCONTEXT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.String getServercontext() {
		return getField("IOTxg", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: servercontext
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: STRSERVERCONTEXT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setServercontext(java.lang.String pServercontext) {
		setField("IOTxg", pServercontext); 
}


/**
 * Getter-Method for attribute: port
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: STRPORT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.String getPort() {
		return getField("IOTxf", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: port
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: STRPORT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setPort(java.lang.String pPort) {
		setField("IOTxf", pPort); 
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getCreatedAt() {
		return getField("IOTx1", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: serverip
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: STRSERVERIP
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.String getServerip() {
		return getField("IOTxa", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: serverip
 *<br>
 *<br>Entity: nuclos_cluster_server
 *<br>DB-Name: STRSERVERIP
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setServerip(java.lang.String pServerip) {
		setField("IOTxa", pServerip); 
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(Clusterserver boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public Clusterserver copy() {
		return super.copy(Clusterserver.class);
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save() throws org.nuclos.api.exception.BusinessException {
		super.save();
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(org.nuclos.common.UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("IOTx"), id);
}
/**
* Static Get by Id
*/
public static Clusterserver get(org.nuclos.common.UID id) {
		return get(Clusterserver.class, id);
}
/**
* Refresh this BO with data from the db layer, interface or similar
*/
public void refresh() throws org.nuclos.api.exception.BusinessException {
	if (this.getId() == null) {
		throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
	}
	super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
}
 }
