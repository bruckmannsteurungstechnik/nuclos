//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.businessentity.facade;

import static org.nuclos.common.ParameterProvider.PARAM_WEBCLIENT_SRC_DIR_FOR_DEVMODE;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.nuclos.api.businessobject.Query;
import org.nuclos.api.businessobject.QueryOperation;
import org.nuclos.api.businessobject.SearchExpression;
import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.ide.utils.WebAddonUtils;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.provider.FileProvider;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.businessentity.Nuclet;
import org.nuclos.businessentity.WebAddon;
import org.nuclos.businessentity.WebAddonFile;
import org.nuclos.common.MutableBoolean;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.ServerParameterProvider;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class WebAddonFacade {

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(WebAddonFacade.class);

	/**
	 * optional param for npm executable
	 * for windows: ﻿"C:\Program Files\nodejs\npm.cmd"
	 */
	private static final String PARAM_WEBCLIENT_PATH_TO_NPM = "WEBCLIENT_PATH_TO_NPM";

	/**
	 * default: $NUCLOS_HOME/data/webaddons-webclient-src/
	 */
	private static final String PARAM_WEBCLIENT_SRC_DIR = "WEBCLIENT_SRC_DIR";

	/**
	 * when running nuclos from IDE set this param to your webclient sources folder
	 * something like:  /..../nuclos/nuclos-webclient2/
	 */

	private static final String WEBCLIENT_DEPLOY_DIR = "webapps/ROOT/webclient";
	private static final String WEBCLIENT_BASE_REF = "/webclient/";

	private static String NPM_VERSION;

	private static final Map<Path, String> addonNameCache = new HashMap<>();

	@Autowired
	private ServerParameterProvider parameterProv;

	private boolean outputAndCompileAllIsRunning = true;

	private static boolean isWindows() {
		return System.getProperty("os.name").toLowerCase().contains("win");
	}

	private String getNpm() {
		final String npmFromParameter = parameterProv.getValue(PARAM_WEBCLIENT_PATH_TO_NPM);
		if (npmFromParameter != null) {
			return npmFromParameter;
		}
		return isWindows() ? "npm.cmd" : "npm";
	}

	/**
	 * addon development mode is activated when system parameter WEBCLIENT_SRC_DIR_FOR_DEVMODE is set to a valid webclient directory
	 * @return
	 */
	public boolean isAddonDevMode() {
		final String webclientSrdDirForDevMode = parameterProv.getValue(PARAM_WEBCLIENT_SRC_DIR_FOR_DEVMODE);
		if (webclientSrdDirForDevMode != null) {
			final boolean srdDirForDevModeConfigured = Files.exists(getWebclientSrcDir());
			if (srdDirForDevModeConfigured && getWebclientSrcDir().resolve("package.json") == null) {
				LOG.error("No valid " + PARAM_WEBCLIENT_SRC_DIR_FOR_DEVMODE + " path defined: " + webclientSrdDirForDevMode);
				return false;
			}
			return srdDirForDevModeConfigured;
		} else {
			return false;
		}
	}

	private Path getWebclientSrcDir() {
		final String webclientSrcDir = parameterProv.getValue(PARAM_WEBCLIENT_SRC_DIR_FOR_DEVMODE);
		if (webclientSrcDir != null) {
			return Paths.get(webclientSrcDir);
		} else {
			final Path genParent = Paths.get(NuclosSystemParameters.getString(NuclosSystemParameters.GENERATOR_OUTPUT_PATH)).getParent();
			final Path webclientSrcDirPath = genParent.resolve("webaddons-webclient-src");
			LOG.info("Webclient source directory is not configured (Systemparameter WEBCLIENT_SRC_DIR). using default: " + webclientSrcDirPath);
			return webclientSrcDirPath;
		}
	}

	/**
	 * root path where the addon sources are stored in filesystem
	 */
	private Path getAddonOutputRootPath() {
		return getWebclientSrcDir().resolve("addons");
	}

	/**
	 * path where the webAddon sources are stored in filesystem
	 */
	private Path getWebAddonOutputPath(WebAddon webAddon) {
		if (isAddonDevMode()) {
			final String webclientSrdDirForDevMode = parameterProv.getValue(PARAM_WEBCLIENT_SRC_DIR_FOR_DEVMODE);
			Path addonSrc = Paths.get(webclientSrdDirForDevMode).resolve("src/addons");
			if  (addonSrc != null) {
				if (!Files.exists(addonSrc)) {
					addonSrc.toFile().mkdirs();
				}
				return addonSrc.resolve(WebAddonUtils.camelCaseToFileName(webAddon.getName()));
			}
		}

		String fileName = WebAddonUtils.camelCaseToFileName(webAddon.getName());

		Path addonSrcPath = getAddonOutputRootPath();
		if  (addonSrcPath != null) {
			return addonSrcPath.resolve(fileName);
		}

		String webAddonDir = "";
		final Nuclet nucletBO = webAddon.getNucletBO();
		if (nucletBO != null) {
			webAddonDir = nucletBO.getPackage() + ".";
		}
		webAddonDir = webAddonDir + fileName;
		return getAddonOutputRootPath().resolve(webAddonDir);
	}


	/**
	 * build webclient with addons on server start
	 */
	@PostConstruct
	final void init() {
		StringBuilder sNpmVersion = new StringBuilder();
		if (runCommandSilent(sNpmVersion, getNpm(), "-version")) {
			NPM_VERSION = sNpmVersion.toString().replace("\n", "").replace("\r", "");;
		}
		SpringApplicationContextHolder.addSpringReadyListener(new SpringApplicationContextHolder.SpringReadyListener() {
			@Override
			public void springIsReady() {
				outputAndCompileAll();
			}
			@Override
			public int getMinReadyState() {
				return 3;
			}
		});
	}

	@Transactional
	public synchronized void outputAndCompileAll() {
		this.outputAndCompileAllIsRunning = true;
		try {
			final List<WebAddon> activeWebAddons = getActiveWebAddons();
			if (activeWebAddons.isEmpty() || !isNodeJsAvailable()) {
				return;
			}
			final Path outputPath = getAddonOutputRootPath();
			try {
				Files.createDirectories(outputPath);
			} catch (IOException ex) {
				LOG.error("Unexpected IO ERROR: {} on {}", ex, outputPath, ex);
				return;
			}

			if (isOutputPathUpToDate()) {
				generateWebAddonModulesRegistry();
				return;
			}

			List<Path> clearProtectedPaths = new ArrayList<>();
			for (WebAddon webAddon : activeWebAddons) {
				outputWebAddon(webAddon, false); // no hash values here, write all at once
				buildWebAddon(webAddon);

				final Path addonOutputPath = getWebAddonOutputPath(webAddon);
				final Path addonLogPath = getWebAddonLogPath(webAddon);
				clearProtectedPaths.add(addonOutputPath);
				clearProtectedPaths.add(addonLogPath);
			}

			// write hash values once
			clearProtectedPaths.add(writeFileHashValuesToOutput());


			// remove inactive addons
			try {
				clearRecursive(outputPath, clearProtectedPaths.toArray(new Path[]{}));
			} catch (IOException ex) {
				LOG.error("Unexpected IO ERROR: {} on {}", ex, outputPath, ex);
			}

			// compile webclient
			buildWebclient(activeWebAddons);
		} finally {
			this.outputAndCompileAllIsRunning = false;
		}
	}

	/**
	 * write webAddon content to files
	 * @param webAddon
	 * @return true if successful
	 */
	public boolean outputWebAddon(WebAddon webAddon) {
		return outputWebAddon(webAddon, true);
	}

	private boolean outputWebAddon(WebAddon webAddon, boolean bWriteHashValues) {
		if (!isNodeJsAvailable()) {
			LOG.error("Node.js is not available.");
			return false;
		}
		final Path addonOutputPath = getWebAddonOutputPath(webAddon);
		final Path addonLogPath = getWebAddonLogPath(webAddon);
		if (webAddon.isUpdate()) {
			// name updated? delete old path
			final WebAddon webAddonDb = QueryProvider.getById(WebAddon.class, webAddon.getId());
			final Path addonOutputPathDb = getWebAddonOutputPath(webAddonDb);
			if (!addonOutputPath.equals(addonOutputPathDb)) {
				try {
					LOG.info("Deleting addons: " + addonOutputPathDb);
					FileUtils.deleteDirectory(addonOutputPathDb.toFile());
				} catch (IOException ex) {
					LOG.error("Unexpected IO ERROR: {} on {}", ex, addonOutputPathDb, ex);
					return false;
				}
			}
		}

		// inactive? remove the addon
		if (!Boolean.TRUE.equals(webAddon.getActive())) {
			// remove only
			LOG.info("Remove inactive addon: " + addonOutputPath.toFile());
			try {
				LOG.info("Deleting addons: " + addonOutputPath);
				FileUtils.deleteDirectory(addonOutputPath.toFile());
				Files.deleteIfExists(addonLogPath);
			} catch (IOException ex) {
				LOG.error("Unexpected IO ERROR: {} on {}", ex, addonOutputPath, ex);
				return false;
			}
			return false;
		}

		// remove directory contents, do not delete node_modules
		final Path nodeModulesDir = addonOutputPath.resolve("node_modules");
		try {
			Files.deleteIfExists(addonLogPath);
		} catch (IOException ex) {
			LOG.error("Unexpected IO ERROR: {} on {}", ex, addonOutputPath, ex);
			return false;
		}

		LOG.info("OUTPUT web addon \"" + addonOutputPath + "\" ...");

		try {
			Files.createDirectories(addonOutputPath);
		} catch (IOException ex) {
			LOG.error("Unexpected IO ERROR: {} on {}", ex, addonOutputPath, ex);
			return false;
		}
		for (WebAddonFile webAddonFile : webAddon.getWebAddonFile()) {
			final NuclosFile file = webAddonFile.getFile();
			if (file == null) {
				continue;
			}
			final String relativePath = webAddonFile.getPath();
			final Path fileOutputPath;
			if (relativePath != null) {
				fileOutputPath = addonOutputPath.resolve(relativePath).resolve(file.getName());
			} else {
				fileOutputPath = addonOutputPath.resolve(file.getName());
			}
			try {
				Files.createDirectories(fileOutputPath.getParent());
				Files.write(fileOutputPath, file.getContent());
			} catch (IOException ex) {
				LOG.error("Unexpected IO ERROR: {} on {}", ex, fileOutputPath, ex);
			}
		}

		if (bWriteHashValues) {
			// update hash values
			writeFileHashValuesToOutput();
		}

		return true;
	}

	public boolean buildWebAddon(final WebAddon webAddon) {
		LOG.info("Building addon '{}'.", webAddon.getName());
		if (isAddonDevMode()) {
			return true;
		}
		if (!isNodeJsAvailable()) {
			LOG.error("Node.js is not available.");
			return false;
		}
		final Path addonOutputPath = getWebAddonOutputPath(webAddon);

		LOG.info("Compile web addon to '{}'.", addonOutputPath);
		addonNameCache.put(addonOutputPath, webAddon.getName());

		StringBuilder log = new StringBuilder();
		try {
			runCommand(log, getWebclientSrcDir(), getNpm(), "run", "install-addon-deps");
			runCommand(log, addonOutputPath, getNpm(), "install");
			if (runCommand(log, addonOutputPath, getNpm(), "run", "build")) {
				log.append("Addon compilation completed successfully");
			}
		} finally {
			Path addonLogPath = getWebAddonLogPath(webAddon);
			try {
				Files.write(addonLogPath, log.toString().getBytes(Charset.forName("UTF-8")));
			} catch (IOException ex) {
				LOG.error("Unexpected IO ERROR while writing logfile: {} on {}", ex, addonLogPath, ex);
			}
		}

		return true;
	}

	/**
	 * installs the addon module into the webclient
	 * @param webAddon
	 * @return
	 */
	private boolean installWebAddon(final WebAddon webAddon) {
		String addonFilename = WebAddonUtils.camelCaseToFileName(webAddon.getName());
		LOG.info("Installing addon: " + addonFilename);
		return runCommand(getWebclientSrcDir(), getNpm(), "install", "addons/" + addonFilename + "/dist/" + addonFilename + "-0.0.1.tgz");
	}

	public synchronized boolean buildWebclient(List<WebAddon> webAddons) {
		LOG.info("Compile webclient.");

		if (isAddonDevMode() || !isNodeJsAvailable()) {
			generateWebAddonModulesRegistry();
			return false;
		}

		final String catalinaBase = System.getProperty("catalina.base");
		final Path webclientDeployPath;
		if (catalinaBase != null) {
			webclientDeployPath = Paths.get(catalinaBase, WEBCLIENT_DEPLOY_DIR);
			if (!webclientDeployPath.toFile().exists()) {
				LOG.info("Creating webclient deploy directory '" + webclientDeployPath.toFile().getAbsolutePath() + "'.");
				webclientDeployPath.toFile().mkdirs();
			}
		} else {
			LOG.error("catalina.base not set");
			return false;
		}

		final Path outputPath = getAddonOutputRootPath();
		final Path webclientSrcDir = getWebclientSrcDir();
		final Path webclientNodeModulesPath = webclientSrcDir.resolve("node_modules");

		final Map<String, String> addonModulesRegistryMap = new HashMap<>();
		try {
			Files.walkFileTree(outputPath, new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
					final Path relativePath = outputPath.relativize(dir);
					if (dir.getFileName().toString().equals("dist") && relativePath.getNameCount() == 2) {
						final String webAddonName = dir.getParent().getFileName().toString();

						// TODO NUCLOS-6717
						// final String addonDeployPath = "nuclos-" + webAddonName;
						final String addonDeployPath = webAddonName;

						final Path webAddonDeployPath = webclientNodeModulesPath.resolve(addonDeployPath);
						FileUtils.copyDirectory(dir.toFile(), webAddonDeployPath.toFile());
						addonModulesRegistryMap.put(addonNameCache.get(dir.getParent()), addonDeployPath);
					}

					return FileVisitResult.CONTINUE;
				}
			});
		} catch (IOException ex) {
			LOG.error("Unexpected IO ERROR: {} on {}", ex, outputPath, ex);
			return false;
		}

		LOG.info("Install webclient dependencies in: " + webclientSrcDir);
		runCommand(webclientSrcDir, getNpm(), "install");

		LOG.info("Installing addon modules in webclient.");
		for (WebAddon webAddon : webAddons) {
			installWebAddon(webAddon);
		}

		webclientSrcDir.resolve("node_modules/@angular/cli/bin/ng").toFile().setExecutable(true);
		LOG.info("Build webclient: " + webclientSrcDir);

		String[] webclientBuildCommand = {"node", "node_modules/@angular/cli/bin/ng", "build",
				/*"--prod", "--aot",*/
				"--base-href=" + WEBCLIENT_BASE_REF};

		if (isWindows()) {
			String content = String.join(" ", webclientBuildCommand);
			final String windowsWebclientBuildFilename = "run-webclient-build.bat";
			try {
				final Path batchFile = Files.write(Paths.get(windowsWebclientBuildFilename), content.getBytes());
				batchFile.toFile().setExecutable(true);
			} catch (IOException e) {
				LOG.error("Unable to create batch file: '{}'.", windowsWebclientBuildFilename , e);
			}
			webclientBuildCommand = new  String[] {windowsWebclientBuildFilename};
		}

		if (runCommand(
				webclientSrcDir,
				webclientBuildCommand
		)) {

			final Path distDir = webclientSrcDir.resolve("dist");
			LOG.info("Move webclient dist from '{}' to '{}'.", distDir, webclientDeployPath);
			try {
				FileUtils.cleanDirectory(webclientDeployPath.toFile());
				webclientDeployPath.toFile().mkdirs();
				Files.move(distDir, webclientDeployPath, StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException ex) {
				LOG.error("Unexpected IO ERROR: {} on {}", ex, webclientDeployPath, ex);
				return false;
			}

			LOG.info("Webclient compilation completed successfully");
			return true;
		} else {
			LOG.error("Error in webclient build.");
		}

		return false;
	}

	/**
	 * @param webAddon
	 * @return true if addon was generated from templates
	 */
	public boolean isWebAddonInitialized(final WebAddon webAddon) {
		final Path addonOutputPath = getWebAddonOutputPath(webAddon);
		return Files.exists(addonOutputPath) && Files.exists(addonOutputPath.resolve("package.json"));
	}

	/**
	 * generate addon from templates
	 * @param webAddon
	 */
	public void initializeWebAddonFiles(final WebAddon webAddon) {
		Path webclientSrc = getWebclientSrcDir();
		if (webclientSrc == null) {
			LOG.error("Webclient source directory is not configured. (Systemparameter WEBCLIENT_SRC_DIR)");
			return;
		}
		final Path addonOutputPath = getWebAddonOutputPath(webAddon);

		LOG.info("Writing addon template to: " + addonOutputPath);

		WebAddonTemplate.initializeWebAddonFiles(webAddon, webclientSrc, addonOutputPath);

		webAddon.setNote(
			"Addon files are created in: " + addonOutputPath + "\n" +
			"Webclient src: " + webclientSrc+ "\n" +
			"Webclient src copied: " + getWebclientSrcDir()+ "\n" +
			""
		);

		// save generated files to db:
		syncWebAddonOutputToObject(webAddon);
	}

	public List<WebAddon> getActiveWebAddons() {
		final Query<WebAddon> query = QueryProvider.create(WebAddon.class);
		SearchExpression<WebAddon> searchExpression = new SearchExpression(WebAddon.Active, Boolean.TRUE, QueryOperation.EQUALS);
		query.where(searchExpression);
		return QueryProvider.execute(query);
	}

	public void generateWebAddonModulesRegistry() {
		// write addon.modules.ts into webclient src in devmode otherwise to webclient output
		final Path addonModulesFile = getWebclientSrcDir().resolve("src/app/addon.modules.ts");
		LOG.info("Generate: " + addonModulesFile);
		WebAddonTemplate.generateWebAddonModulesRegistry(getActiveWebAddons(), isAddonDevMode(), addonModulesFile);
	}

	public void generateWebAddonModulesRegistry(List<WebAddon> addons) {
		// write addon.modules.ts into webclient src in devmode otherwise to webclient output
		final Path addonModulesFile = getWebclientSrcDir().resolve("src/app/addon.modules.ts");
		LOG.info("Generate: " + addonModulesFile);

		Collections.sort(addons, new Comparator<WebAddon>() {
			@Override
			public int compare(WebAddon o1, WebAddon o2) {
				return o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase());
			}
		});

		WebAddonTemplate.generateWebAddonModulesRegistry(addons, isAddonDevMode(), addonModulesFile);
	}

	public void syncActiveWebAddons() {
		boolean bSomethingChanged = false;
		for (WebAddon webAddon : getActiveWebAddons()) {
			try {
				if (syncWebAddonOutputToObject(webAddon)) {
					BusinessObjectProvider.update(webAddon);
					bSomethingChanged = true;
				}
			} catch (BusinessException ex) {
				LOG.error("Unexpected IO ERROR: {} on {}", ex, webAddon, ex);
			}
		}
		if (bSomethingChanged) {
			writeFileHashValuesToOutput();
		}
	}

	/**
	 * save addon files to DB
	 * @param webAddon
	 */
	public boolean syncWebAddonOutputToObject(final WebAddon webAddon) {
		LOG.info("Sync webAddonOutput '{}.", webAddon.getName());
		if (!isNodeJsAvailable()) {
			return false;
		}
		final String[] excludedSubDirectories = new String[]{"dist", "node_modules", ".tmp"};
		final String[] excludedFileTypes = new String[]{".log", ".swp"};
		final Path addonOutputPath = getWebAddonOutputPath(webAddon);
		final List<WebAddonFile> webAddonFileList = webAddon.getWebAddonFile();
		final Set<UID> foundFiles = new HashSet<>();
		final MutableBoolean bSomethingChanged = new MutableBoolean(false);
		try {
			if (!Files.exists(addonOutputPath)) {
				Files.createDirectories(addonOutputPath);
			}
			Files.walkFileTree(addonOutputPath, new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					for (String excluded : excludedFileTypes) {
						if (file.getFileName().toString().toLowerCase().endsWith(excluded.toLowerCase())) {
							return FileVisitResult.CONTINUE;
						}
					}
					final Path relativePath = addonOutputPath.relativize(file.getParent());

					final String fileName = file.getFileName().toString();
					final byte[] bytesFromFile = Files.readAllBytes(file);
					boolean bFound = false;
					String convertedFilePath=relativePath.toString().replaceAll("\\\\","/");

					for (WebAddonFile webAddonFile : webAddonFileList) {
						if (((convertedFilePath.isEmpty() && webAddonFile.getPath() == null) ||
								convertedFilePath.equals(webAddonFile.getPath()))
								&& fileName.equals(webAddonFile.getFile().getName())) {
							bFound = true;
							foundFiles.add(webAddonFile.getId());
							// File found. Check for changes
							if (!Arrays.equals(bytesFromFile, webAddonFile.getFile().getContent())) {
								// Content changed
								try {
									webAddonFile.setFile(FileProvider.newFile(file.toFile()));
									bSomethingChanged.setValue(true);
								} catch (BusinessException ex) {
									LOG.error("Unexpected IO ERROR: {} on {}", ex, file, ex);
								}
							}
							break;
						}
					}

					if (!bFound) {
						// new file
						WebAddonFile webAddonFile = new WebAddonFile();
						if (!convertedFilePath.isEmpty()) {
							webAddonFile.setPath(convertedFilePath);
						}
						try {
							webAddonFile.setFile(FileProvider.newFile(file.toFile()));
						} catch (BusinessException ex) {
							LOG.error("Unexpected IO ERROR: {} on {}", ex, file, ex);
						}
						webAddon.insertWebAddonFile(webAddonFile);
						bSomethingChanged.setValue(true);
					}

					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult preVisitDirectory(final Path dir, final BasicFileAttributes attrs) throws IOException {
					final Path relativePath = addonOutputPath.relativize(dir);
					for (String excluded : excludedSubDirectories) {
						if (relativePath.toString().startsWith(excluded)) {
							return FileVisitResult.SKIP_SUBTREE;
						}
					}
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
					if (exc == null) {
						return FileVisitResult.CONTINUE;
					} else {
						throw exc;
					}
				}
			});

			// Check for deleted files
			for (WebAddonFile webAddonFile : webAddonFileList) {
				if (!foundFiles.contains(webAddonFile.getId())) {
					webAddon.deleteWebAddonFile(webAddonFile);
					bSomethingChanged.setValue(true);
				}
			}

		} catch (IOException ex) {
			LOG.error("Unexpected IO ERROR: {} on {}", ex, addonOutputPath, ex);
		}
		return bSomethingChanged.getValue();
	}

	/**
	 * delete addon source files
	 * @param webAddon
	 */
	public void deleteAddonFiles(WebAddon webAddon) {
		final Path addonOutputPath = getWebAddonOutputPath(webAddon);
		LOG.info("Deleting addon directory: " + addonOutputPath.toString());

		try {
			LOG.info("Deleting: " + addonOutputPath);
			FileUtils.deleteDirectory(new File(addonOutputPath.toString()));
			new File(addonOutputPath.toString() + ".log").delete();
		} catch (IOException ex) {
			LOG.error("Unable to delete addon directory: {} on {}", ex, addonOutputPath, ex);
		}
	}


	public Path getWebAddonLogPath(WebAddon webAddon) {
		final Path addonOutputPath = getWebAddonOutputPath(webAddon);
		Path logFilePath = addonOutputPath.getParent().resolve(addonOutputPath.getFileName().toString() + ".log");
		return logFilePath;
	}

	public static void clearRecursive(final Path pathToClear, final Path...dirsOrFilesToIgnore) throws IOException {
		if (!pathToClear.toFile().exists()) {
			return;
		}
		Files.walkFileTree(pathToClear, new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				if (dirsOrFilesToIgnore != null) {
					for (Path toIgnore : dirsOrFilesToIgnore) {
						if (file.equals(toIgnore)) {
							return FileVisitResult.CONTINUE;
						}
					}
				}
				Files.delete(file);
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
				Files.delete(file);
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult preVisitDirectory(final Path dir, final BasicFileAttributes attrs) throws IOException {
				if (dirsOrFilesToIgnore != null) {
					for (Path toIgnore : dirsOrFilesToIgnore) {
						if (dir.equals(toIgnore)) {
							return FileVisitResult.SKIP_SUBTREE;
						}
					}
				}
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
				if (exc == null) {
					if (!dir.equals(pathToClear)) {
						Files.delete(dir);
					}
					return FileVisitResult.CONTINUE;
				} else {
					throw exc;
				}
			}
		});
	}

	private boolean runCommand(Path workingDir, String...command) {
		return _runCommand(false, null, workingDir, command);
	}

	private boolean runCommand(StringBuilder log, Path workingDir, String...command) {
		return _runCommand(false, log, workingDir, command);
	}

	private boolean runCommandSilent(StringBuilder log, String...command) {
		return _runCommand(true, log, null, command);
	}

	private boolean runCommandSilent(StringBuilder log, Path workingDir, String...command) {
		return _runCommand(true, log, workingDir, command);
	}

	private static boolean _runCommand(boolean bSilent, StringBuilder log, Path workingDir, String...command) {
		final String commandString = String.join(" ", command);
		_log(log, bSilent, "[exec in " + workingDir + "] '" + commandString + "'.");

		int returnValue = 0;
		try {
			ProcessBuilder pb = new ProcessBuilder(command);
			if (workingDir != null) {
				pb.directory(workingDir.toFile());
			}

			Process p = pb.start();

			try (BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
				reader.lines().forEach(l -> {
					_log(log, bSilent, " > " + l);
				});
			} catch (IOException e) {
				LOG.error("Error running '{}'.", commandString, e);
			}

			try (BufferedReader reader = new BufferedReader(new InputStreamReader(p.getErrorStream()))) {
				reader.lines().forEach(l -> {
					_log(log, bSilent, " > " + l);
				});
			} catch (IOException e) {
				LOG.error("Error running '{}'.", commandString, e);
			}

			p.waitFor(4L, TimeUnit.MINUTES); p.destroy();
			returnValue = p.waitFor();
		} catch (Exception ex) {
			if (!bSilent) {
				LOG.error("Unexpected ERROR: {} on {}"+  Arrays.toString(command), ex);
			}
			if (log != null) {
				log.append(String.format("Unexpected ERROR: {} on {}"+  Arrays.toString(command)));
			}
		}
		if (returnValue != 0) {
			LOG.error("Error while running '{}'. Status code '{}'" +  Arrays.toString(command), returnValue);
			return false;
		}
		_log(log, bSilent, "[exec in " + workingDir + "] '" + commandString + "' successfull.");

		return true;
	}

	private static void _log(StringBuilder log, boolean bSilent, String msg) {
		if (bSilent) {
			LOG.debug(msg);
		} else {
			LOG.info(msg);
		}
		if (log != null) {
			log.append(msg + "\n");
		}
	}


	private boolean isNodeJsAvailable() {
		return NPM_VERSION != null;
	}


	/**
	 * check against the file hash values
	 */
	private boolean isOutputPathUpToDate() {
		return getFileHashValuesFromOutput().equals(getFileHashValuesFromDb());
	}

	public Path writeFileHashValuesToOutput() {
		Path outputPath = isAddonDevMode() ? getWebclientSrcDir().resolve("src/addons") : getWebclientSrcDir();
		final Path hashListFilePath = outputPath.resolve("hashlist");
		final File hashListFile = hashListFilePath.toFile();
		LOG.info("Writing hash values to {}", hashListFile);
		try {
			Files.deleteIfExists(hashListFile.toPath());
			final SortedMap<UID, String> sortedFileHashes = new TreeMap<>();
			final Query<WebAddonFile> q = QueryProvider.create(WebAddonFile.class);
			final List<WebAddonFile> webAddonFiles = QueryProvider.execute(q);
			for (WebAddonFile webAddonFile : webAddonFiles) {
				sortedFileHashes.put(webAddonFile.getId(), StringUtils.defaultIfEmpty(webAddonFile.getHash(), "null"));
			}
			final StringBuilder result = new StringBuilder();
			for (UID fileId : sortedFileHashes.keySet()) {
				result.append(fileId.getString() + ":" + sortedFileHashes.get(fileId) + "\n");
			}
			FileUtils.writeStringToFile(hashListFile, result.toString());
		} catch (IOException ex) {
			LOG.error("Unexpected IO ERROR: {} on {}", ex, hashListFile, ex);
		}
		return hashListFilePath;
	}

	private String getFileHashValuesFromDb() {
		final SortedMap<UID, String> sortedFileHashes = new TreeMap<>();
		final Query<WebAddonFile> q = QueryProvider.create(WebAddonFile.class);
		final List<WebAddonFile> webAddonFiles = QueryProvider.execute(q);
		for (WebAddonFile webAddonFile : webAddonFiles) {
			sortedFileHashes.put(webAddonFile.getId(), StringUtils.defaultIfEmpty(webAddonFile.getHash(), "null"));
		}
		final StringBuilder result = new StringBuilder();
		for (UID fileId : sortedFileHashes.keySet()) {
			result.append(fileId.getString() + ":" + sortedFileHashes.get(fileId) + "\n");
		}
		return result.toString();
	}

	private String getFileHashValuesFromOutput() {
		Path outputPath = isAddonDevMode() ? getWebclientSrcDir().resolve("src/addons") : getWebclientSrcDir();
		final File hashListFile = outputPath.resolve("hashlist").toFile();
		try {
			if (hashListFile.exists()) {
				final byte[] fileBytes = Files.readAllBytes(hashListFile.toPath());
				final String result = new String(fileBytes);
				return result;
			}
		} catch (IOException ex) {
			LOG.error("Unexpected IO ERROR: {} on {}", ex, hashListFile, ex);
		}
		return "";
	}

	public boolean isOutputAndCompileAllRunning() {
		return outputAndCompileAllIsRunning;
	}

	/**
	 * removes addon from package.json and node_modules and deletes package-lock.json
	 * @param webAddon
	 */
	public void uninstallAddon(WebAddon webAddon) {
		LOG.info("Uninstalling addon '{}'.", webAddon.getName());
		final String addonFileName = WebAddonUtils.camelCaseToFileName(webAddon.getName());
		final String addonPackaggeText = "\"" + addonFileName + "\":.*$";
		Path packageJson = getWebclientSrcDir().resolve("package.json");
		try (Stream<String> lines = Files.lines(packageJson)) {
			final List<String> replaced = lines
					.map(line-> line.replaceAll(addonPackaggeText, ""))
					.collect(Collectors.toList());
			Files.write(packageJson, replaced);

			Files.deleteIfExists(getWebclientSrcDir().resolve("package-lock.json"));

			FileUtils.forceDelete(getWebclientSrcDir().resolve("node_modules").resolve(addonFileName).toFile());
		} catch (IOException e) {
			LOG.error("Unable to uninstall addon '{}'.", webAddon.getName(), e);
		}
	}

}
