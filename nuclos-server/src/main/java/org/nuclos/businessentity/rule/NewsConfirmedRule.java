//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.businessentity.rule;

import java.util.List;

import org.nuclos.api.User;
import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.InsertContext;
import org.nuclos.api.context.RuleContext;
import org.nuclos.api.context.UpdateContext;
import org.nuclos.api.rule.InsertFinalRule;
import org.nuclos.api.rule.UpdateFinalRule;
import org.nuclos.businessentity.News;
import org.nuclos.businessentity.NewsConfirmed;
import org.nuclos.businessentity.nuclosuser;
import org.nuclos.businessentity.rule.annotation.SystemRuleUsage;
import org.nuclos.common.UID;
import org.nuclos.server.news.NewsService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Rule(
		name = "NewsConfirmedRule",
		description = "")
@SystemRuleUsage(
		boClass = NewsConfirmed.class,
		order = 1
)
@Configurable
@SuppressWarnings("unused") // system internal usage
public class NewsConfirmedRule implements InsertFinalRule, UpdateFinalRule {

	@Autowired
	private NewsService newsService;

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(NewsConfirmedRule.class);

	@Override
	public void insertFinal(final InsertContext context) {
		updatePrivacyConsent(context, context.getBusinessObject(NewsConfirmed.class));
	}

	@Override
	public void updateFinal(final UpdateContext context) {
		updatePrivacyConsent(context, context.getBusinessObject(NewsConfirmed.class));
	}

	private void updatePrivacyConsent(
			final RuleContext context,
			final NewsConfirmed newsConfirmed
	) {
		User user = context.getUser();

		final List<News> unconfirmedPrivacyPolicies = newsService.getUnconfirmedPrivacyPolicies(
				new UID(user.getId().toString())
		);

		nuclosuser nuclosUser = org.nuclos.businessentity.nuclosuser.get(user.getId());

		newsService.updatePrivacyConsentForUser(nuclosUser, unconfirmedPrivacyPolicies.isEmpty());
	}
}
