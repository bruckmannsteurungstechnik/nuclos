//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.businessentity.rule;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.nuclos.api.annotation.Rule;
import org.nuclos.api.businessobject.Query;
import org.nuclos.api.context.CustomContext;
import org.nuclos.api.context.InsertContext;
import org.nuclos.api.context.LogContext;
import org.nuclos.api.context.UpdateContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.notification.Priority;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.rule.CustomRule;
import org.nuclos.api.rule.InsertRule;
import org.nuclos.api.rule.UpdateRule;
import org.nuclos.businessentity.Entity;
import org.nuclos.businessentity.EntityField;
import org.nuclos.businessentity.NucletIntegrationField;
import org.nuclos.businessentity.NucletIntegrationPoint;
import org.nuclos.businessentity.NucletIntegrationProblem;
import org.nuclos.businessentity.facade.NucletIntegrationPointFacade;
import org.nuclos.businessentity.rule.annotation.SystemRuleUsage;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.Mutable;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.NucletFieldMeta;
import org.nuclos.common.NuclosEntityValidator;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.TranslationVO;
import org.nuclos.common.UID;
import org.nuclos.common.transport.vo.EntityMetaTransport;
import org.nuclos.common.transport.vo.FieldMetaTransport;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.ejb3.LocaleFacadeLocal;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.masterdata.ejb3.MetaDataFacadeLocal;
import org.nuclos.server.nbo.NuclosBusinessObjectBuilder;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Rule(
		name = "NucletIntegrationSaveRule",
		description = "")
@SystemRuleUsage(
		boClass = NucletIntegrationPoint.class,
		order = 1
)
@Configurable
@SuppressWarnings("unused") // system internal usage
public class NucletIntegrationSaveRule implements CustomRule, InsertRule, UpdateRule {

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(NucletIntegrationSaveRule.class);

	@Autowired
	private NucletIntegrationPointFacade ipFacade;

	@Autowired
	private MetaProvider metaProv;

	@Autowired
	private MetaDataFacadeLocal metaFacadeLocal;

	@Autowired
	private SpringLocaleDelegate localeDelegate;

	@Autowired
	private LocaleFacadeLocal localeFacade;

	@Override
	public void insert(final InsertContext insertContext) throws BusinessException {
		this.save(insertContext.getBusinessObject(NucletIntegrationPoint.class), null, insertContext);
	}

	@Override
	public void update(final UpdateContext updateContext) throws BusinessException {
		final NucletIntegrationPoint point = updateContext.getBusinessObject(NucletIntegrationPoint.class);
		final NucletIntegrationPoint pointDb = QueryProvider.getById(NucletIntegrationPoint.class, point.getId());
		this.save(point, pointDb, updateContext);
	}

	@Override
	public void custom(CustomContext context) throws BusinessException {
		final NucletIntegrationPoint point = context.getBusinessObject(NucletIntegrationPoint.class);
		final NucletIntegrationPoint pointDb = QueryProvider.getById(NucletIntegrationPoint.class, point.getId());
		if (build(point, pointDb, context, true)) {
			this.save(point, pointDb, context);
			context.setUpdateAfterExecution(true);
		} else {
			context.setUpdateAfterExecution(false);
		}
	}

	/**
	 *
	 * @param point
	 * @param pointDb
	 * @param log
	 * @return
	 * 			true if there are changes or point is new
	 * @throws BusinessException
	 */
	private void save(final NucletIntegrationPoint point, final NucletIntegrationPoint pointDb, LogContext log) throws BusinessException {
		build(point, pointDb, log, false);
	}

	/**
	 * @return true if there are changes or point is new
	 */
	private boolean build(final NucletIntegrationPoint point, final NucletIntegrationPoint pointDb, LogContext log, boolean validateOnly) throws BusinessException {
		final UID targetEntityId = point.getTargetEntityId();
		final List<NucletIntegrationField> nucletIntegrationFieldList = point.getNucletIntegrationField1();

		//needed to compare dependants as well
		point.getNucletIntegrationProblem();
		point.getNucletIntegrationField1();
		point.getNucletIntegrationField2();
		point.getEntityField();
		if (pointDb != null) {
			pointDb.getNucletIntegrationProblem();
			pointDb.getNucletIntegrationField1();
			pointDb.getNucletIntegrationField2();
			pointDb.getEntityField();
		}

		validateBefore(point, pointDb, log);

		// reset all problems
		for (NucletIntegrationProblem problem : point.getNucletIntegrationProblem()) {
			point.deleteNucletIntegrationProblem(problem);
		}

		Collection<NucletIntegrationField> fieldsToCreate = new ArrayList<>();
		for (NucletIntegrationField field : nucletIntegrationFieldList) {
			final UID targetFieldId = field.getTargetFieldId();
			final NucletIntegrationPoint referencingIntegrationPoint = field.getIntegrationPointReferenceFieldBO();
			final Entity referencingEntity = field.getEntityReferenceFieldBO();
			if (targetFieldId == null) {
				continue;
			}
			if (targetEntityId != null && "-1".equals(targetFieldId.getString())) {
				// create new field
				fieldsToCreate.add(field);
				if (!validateOnly) {
					field.setTargetFieldId(new UID());
				}
			} else {
				if (!validateOnly) {
					final FieldMeta<?> targetFieldMeta = metaProv.getEntityField(targetFieldId);
					if (targetEntityId == null || !targetFieldMeta.getEntity().equals(targetEntityId)) {
						// remove target field, it does not belong to target entity or target entity is removed
						field.setTargetFieldId(null);
					} else if (referencingIntegrationPoint != null && referencingIntegrationPoint.getTargetEntityId() == null) {
						// remove target field, referencing integration point has no target entity
						field.setTargetFieldId(null);
					}
					if (referencingIntegrationPoint != null || referencingEntity != null) {
						field.setDatatype(null);
					}
					if (!Double.class.getCanonicalName().equals(field.getDatatype())) {
						field.setDataprecision(null);
					}
					if (!String.class.getCanonicalName().equals(field.getDatatype()) &&
							!Integer.class.getCanonicalName().equals(field.getDatatype()) &&
							!Long.class.getCanonicalName().equals(field.getDatatype()) &&
							!Double.class.getCanonicalName().equals(field.getDatatype())) {
						field.setDatascale(null);
					}
				}
			}
		}

		if (!validateOnly) {
			if (!fieldsToCreate.isEmpty()) {
				createNewFields(fieldsToCreate, point, log);
			}

			if (pointDb != null) {
				ipFacade.deleteIpView(pointDb);
			}
			ipFacade.createIpView(point);
		}

		updateForeignFieldsReferencingThisPoint(point, log, validateOnly);

		validateAfter(point, log);
		point.setProblem(!point.getNucletIntegrationProblem().isEmpty());

		boolean result = true;
		if (pointDb != null) {
			result = !point.compare(pointDb);
		}
		return result;
	}

	private void validateBefore(final NucletIntegrationPoint point, final NucletIntegrationPoint pointDb, LogContext log) throws BusinessException {
		final UID nucletId = point.getNucletId();
		if (NuclosEntityValidator.isDuplicate(
				point.getName(),
				nucletId,
				point.getId(),
				new NuclosBusinessObjectBuilder.BuilderMetaProvider(metaProv).getAllEntities()
		)) {
			throw new BusinessException("NucletIntegrationSaveRule.exception.duplicateName");
		}
		if (pointDb != null) {
			if (!RigidUtils.equal(nucletId, pointDb.getNucletId())) {
				// The client should not have allowed it!
				throw new BusinessException("Change of Nuclet is no longer possible (NUCLOS-6319)");
			}
		}
		for (NucletIntegrationField ipField : point.getNucletIntegrationField1()) {
			final NucletIntegrationPoint refIpBo = ipField.getIntegrationPointReferenceFieldBO();
			if (refIpBo != null && !refIpBo.getNucletId().equals(nucletId)) {
				// The client should not have allowed it!
				throw new BusinessException("Attribute \"" + ipField.getName() + "\" references on an integration point of another nuclet. This is not possible.");
			}
		}
		if (point.getView() != null) {
			point.setView(point.getView().replaceAll("[^A-Za-z0-9_]", "_").toUpperCase());
			Query<NucletIntegrationPoint> qCheckUniqueView = QueryProvider.create(NucletIntegrationPoint.class);
			qCheckUniqueView.where(NucletIntegrationPoint.View.eq(point.getView()));
			qCheckUniqueView.and(NucletIntegrationPoint.NucletId.eq(nucletId));
			if (point.isUpdate()) {
				qCheckUniqueView.and(NucletIntegrationPoint.Id.neq(point.getId()));
			}
			final List<NucletIntegrationPoint> otherPoints = QueryProvider.execute(qCheckUniqueView);
			if (otherPoints.size() > 0) {
				throw new BusinessException("View \"" + point.getView() + "\" is already in use by IP " + otherPoints.get(0).getName());
			}
		}
	}

	private void validateAfter(final NucletIntegrationPoint point, LogContext log) {
		final UID targetEntityId = point.getTargetEntityId();
		final boolean bPointReadonly = Boolean.TRUE.equals(point.getReadonly());
		final boolean bPointStateful = Boolean.TRUE.equals(point.getStateful());
		final List<NucletIntegrationField> nucletIntegrationFieldList = point.getNucletIntegrationField1();

		if (targetEntityId == null) {
			final boolean bOptional = Boolean.TRUE.equals(point.getOptional());
			if (!bOptional) {
				NucletIntegrationProblem problem = new NucletIntegrationProblem();
				problem.setProblem(localeDelegate.getMsg("NucletIntegrationSaveRule.problem.targetEntityNotSet"));
				problem.setSolution(localeDelegate.getMsg("NucletIntegrationSaveRule.solution.targetEntityNotSet"));
				point.insertNucletIntegrationProblem(problem);
			}
		} else {
			// targetEntity != null
			final EntityMeta<Object> entityMeta = metaProv.getEntity(targetEntityId);

			// check readonly
			if (!bPointReadonly && entityMeta.isReadonly()) {
				NucletIntegrationProblem problem = new NucletIntegrationProblem();
				problem.setProblem(localeDelegate.getMsg("NucletIntegrationSaveRule.problem.pointNotReadonly"));
				problem.setSolution(localeDelegate.getMsg("NucletIntegrationSaveRule.solution.pointNotReadonly"));
				point.insertNucletIntegrationProblem(problem);
			}

			// check stateful
			if (bPointStateful && !entityMeta.isStateModel()) {
				NucletIntegrationProblem problem = new NucletIntegrationProblem();
				problem.setProblem(localeDelegate.getMsg("NucletIntegrationSaveRule.problem.targetEntityNotStateful"));
				problem.setSolution(localeDelegate.getMsg("NucletIntegrationSaveRule.solution.targetEntityNotStateful"));
				point.insertNucletIntegrationProblem(problem);
			}

			for (NucletIntegrationField field : nucletIntegrationFieldList) {
				final boolean bOptional = Boolean.TRUE.equals(field.getOptional());
				final boolean bFieldReadonly = Boolean.TRUE.equals(field.getReadonly());
				final NucletIntegrationPoint referencingIntegrationPoint = field.getIntegrationPointReferenceFieldBO();
				final Entity referencingEntity = field.getEntityReferenceFieldBO();

				// check field readonly for readonly ip
				if (!bFieldReadonly && bPointReadonly) {
					NucletIntegrationProblem problem = new NucletIntegrationProblem();
					problem.setProblem(localeDelegate.getMsg("NucletIntegrationSaveRule.problem.fieldNotReadonlyIpReadonly", field.getName()));
					problem.setSolution(localeDelegate.getMsg("NucletIntegrationSaveRule.solution.fieldNotReadonlyIpReadonly"));
					point.insertNucletIntegrationProblem(problem);
				}

				// check field optional
				if (!bOptional && field.getTargetFieldId() == null) {
					NucletIntegrationProblem problem = new NucletIntegrationProblem();
					problem.setProblem(localeDelegate.getMsg("NucletIntegrationSaveRule.problem.notOptional", field.getName()));
					problem.setSolution(localeDelegate.getMsg("NucletIntegrationSaveRule.solution.notOptional"));
					point.insertNucletIntegrationProblem(problem);
				} else if (field.getTargetFieldId() != null) {

					final FieldMeta<?> fieldMeta = metaProv.getEntityField(field.getTargetFieldId());

					// check field readonly
					if (!bFieldReadonly && fieldMeta.isReadonly()) {
						NucletIntegrationProblem problem = new NucletIntegrationProblem();
						problem.setProblem(localeDelegate.getMsg("NucletIntegrationSaveRule.problem.fieldNotReadonly", field.getName()));
						problem.setSolution(localeDelegate.getMsg("NucletIntegrationSaveRule.solution.fieldNotReadonly"));
						point.insertNucletIntegrationProblem(problem);
					}

					// check field data type
					String sDataTypeProblem = null;
					if ((fieldMeta.getForeignEntity() != null && !fieldMeta.isFileDataType() && referencingEntity == null && referencingIntegrationPoint == null)
						|| (fieldMeta.getForeignEntity() == null && !RigidUtils.equal(fieldMeta.getDataType(), field.getDatatype()))) {
						sDataTypeProblem = "fieldDatatypeDoNotMatch";
					} else {
						if (String.class.getCanonicalName().equals(field.getDatatype())) {
							if (field.getDatascale() != null && fieldMeta.getScale() == null) {
								sDataTypeProblem = "fieldDatascaleDoNotMatch";
							} else if (field.getDatascale() == null && fieldMeta.getScale() != null) {
								sDataTypeProblem = "fieldDatascaleDoNotMatch";
							} else if (field.getDatascale() != null && fieldMeta.getScale() != null &&
								field.getDatascale() > fieldMeta.getScale()) {
								sDataTypeProblem = "fieldDatascaleDoNotMatch";
							}
						}
						if (Integer.class.getCanonicalName().equals(field.getDatatype()) ||
								Long.class.getCanonicalName().equals(field.getDatatype()) ||
								Double.class.getCanonicalName().equals(field.getDatatype())) {
							if (field.getDatascale() == null) {
								sDataTypeProblem = "fieldDatascaleDoNotMatch";
							} else if (field.getDatascale() > fieldMeta.getScale()) {
								sDataTypeProblem = "fieldDatascaleDoNotMatch";
							}
						}
						if (Double.class.getCanonicalName().equals(field.getDatatype())) {
							if (field.getDataprecision() == null) {
								sDataTypeProblem = "fieldDataprecisionDoNotMatch";
							} else if (field.getDataprecision() > fieldMeta.getPrecision()) {
								sDataTypeProblem = "fieldDataprecisionDoNotMatch";
							}
						}
					}
					if (sDataTypeProblem != null) {
						NucletIntegrationProblem problem = new NucletIntegrationProblem();
						problem.setProblem(localeDelegate.getMsg("NucletIntegrationSaveRule.problem."+sDataTypeProblem, field.getName()));
						problem.setSolution(localeDelegate.getMsg("NucletIntegrationSaveRule.solution.fieldDatatypeDoNotMatch"));
						point.insertNucletIntegrationProblem(problem);
					}
				}
			}
		}
	}

	private void updateForeignFieldsReferencingThisPoint(final NucletIntegrationPoint point, LogContext log, final boolean validateOnly) throws BusinessException {
		if (point.getId() == null) {
			return;
		}
		final Query<EntityField> q = QueryProvider.create(EntityField.class);
		q.where(EntityField.ForeignIntegrationPointId.eq(point.getId()));
		final List<EntityField> foreignFieldsToPoint = QueryProvider.execute(q);

		if (!foreignFieldsToPoint.isEmpty()) {
			final boolean bOptional = Boolean.TRUE.equals(point.getOptional());
			if (point.getTargetEntityId() == null && !bOptional) {
				NucletIntegrationProblem problem = new NucletIntegrationProblem();
				problem.setProblem(localeDelegate.getMsg("NucletIntegrationSaveRule.problem.foreignFieldsReferencingThisPoint"));
				problem.setSolution(localeDelegate.getMsg("NucletIntegrationSaveRule.solution.foreignFieldsReferencingThisPoint"));
				point.insertNucletIntegrationProblem(problem);
				return;
			} else {
				for (EntityField foreignField : foreignFieldsToPoint) {
					final UID foreignentityId = foreignField.getForeignentityId();
					if (foreignentityId == null || !foreignentityId.equals(point.getTargetEntityId())) {

						if (!validateOnly) {
							// update the reference attribute
							final MetaTransports metaTransports = getMetaTransportsForEntity(foreignField.getEntityId());
							for (FieldMetaTransport fieldTransport : metaTransports.lstFieldTransports) {
								final NucletFieldMeta<?> fieldMeta = fieldTransport.getEntityFieldMeta();
								if (fieldMeta.getUID().equals(foreignField.getId())) {
									fieldMeta.flagUpdate();
									if (point.getTargetEntityId() == null) {
										// optional and not yet integrated point (NUCLOS-6248)
										// create a dummy hidden attribute
										fieldMeta.setForeignEntity(E.DUMMY.getUID());
										fieldMeta.setForeignEntityField(E.stringify(E.DUMMY.name));
									} else {
										fieldMeta.setForeignEntity(point.getTargetEntityId());
										final EntityMeta<Long> foreignEntityMeta = metaProv.<Long>getEntity(fieldMeta.getForeignEntity());
										String sStringifiedReference = localeDelegate.getText(foreignEntityMeta.getLocaleResourceIdForTreeView());
										fieldMeta.setForeignEntityField(sStringifiedReference);
									}
									// NUCLOS-6243
								/*if (!fieldMeta.isNullable()) {
									// Not null is impossible
									fieldTransport.getEntityFieldMeta().setNullable(true);
								}
								if (fieldMeta.isUnique()) {
									// Unique is impossible
									fieldTransport.getEntityFieldMeta().setUnique(false);
								}*/
								}
							}
							updateEntityMeta(metaTransports, log);
						}
					}
				}
			}
		}
	}

	private void createNewFields(
			final Collection<NucletIntegrationField> fieldsToCreate,
			final NucletIntegrationPoint point,
			final LogContext log) throws BusinessException {

		final UID targetEntityId = point.getTargetEntityId();
		final EntityMeta<Long> eMeta = metaProv.<Long>getEntity(targetEntityId);
		final MetaTransports metaTransports = getMetaTransportsForEntity(targetEntityId);

		for (NucletIntegrationField field : fieldsToCreate) {
			metaTransports.order++;
			FieldMetaTransport fieldTransport = new FieldMetaTransport();
			NucletFieldMeta newField = new NucletFieldMeta();
			newField.flagNew();
			newField.setUID(field.getTargetFieldId());
			newField.setEntity(targetEntityId);
			newField.setOrder(metaTransports.order);
			newField.setLocalized(false);
			newField.setNullable(true);

			if (field.getEntityReferenceFieldId() != null) {
				// DEFAULT REFERENCE
				initNewReferenceField(newField, field, field.getEntityReferenceFieldId(), metaTransports.lstFieldTransports);
				fieldTransport.setEntityFieldMeta(newField);

			} else if (field.getIntegrationPointReferenceFieldId() != null) {
				// REF TO INTEGRATION POINT
				final NucletIntegrationPoint referencingIntegrationPoint = field.getIntegrationPointReferenceFieldBO();
				if (referencingIntegrationPoint.getTargetEntityId() == null) {
					NucletIntegrationProblem problem = new NucletIntegrationProblem();
					problem.setProblem(localeDelegate.getMsg("NucletIntegrationSaveRule.problem.pointNotIntegrated", field.getIntegrationPointReferenceFieldBO().getName()));
					problem.setSolution(localeDelegate.getMsg("NucletIntegrationSaveRule.solution.pointNotIntegrated", field.getIntegrationPointReferenceFieldBO().getName()));
					point.insertNucletIntegrationProblem(problem);
					field.setTargetFieldId(null);
				} else {
					initNewReferenceField(newField, field, referencingIntegrationPoint.getTargetEntityId(), metaTransports.lstFieldTransports);
					fieldTransport.setEntityFieldMeta(newField);
				}

			} else if (field.getEntityReferenceFieldId() == null && field.getIntegrationPointReferenceFieldId() == null && field.getDatatype() != null) {
				// DEFAULT DATA
				newField.setDataType(field.getDatatype());
				Mutable<String> fieldName = new Mutable<>(field.getName());
				Mutable<String> dbName;
				if (String.class.getCanonicalName().equals(field.getDatatype())) {
					dbName = new Mutable<>("STR" + field.getName());
					newField.setScale(field.getDatascale());
				} else if (Boolean.class.getCanonicalName().equals(field.getDatatype())) {
					dbName = new Mutable<>("BLN" + field.getName());
					newField.setScale(1);
					newField.setNullable(false);
					newField.setDefaultValue(Boolean.FALSE.toString());
					newField.setDefaultMandatory(Boolean.FALSE.toString());
				} else if (Date.class.getCanonicalName().equals(field.getDatatype())) {
					dbName = new Mutable<>("DAT" + field.getName());
				} else if (Integer.class.getCanonicalName().equals(field.getDatatype())
						|| Long.class.getCanonicalName().equals(field.getDatatype())) {
					dbName = new Mutable<>("INT" + field.getName());
					newField.setScale(field.getDatascale());
				} else if (Double.class.getCanonicalName().equals(field.getDatatype())) {
					dbName = new Mutable<>("DBL" + field.getName());
					newField.setScale(field.getDatascale());
					newField.setPrecision(field.getDataprecision());
				} else if (GenericObjectDocumentFile.class.getCanonicalName().equals(field.getDatatype())) {
					dbName = new Mutable<>("STRUID_STR" + field.getName());
					newField.setForeignEntity(E.DOCUMENTFILE.getUID());
					newField.setForeignEntityField(E.stringify(E.DOCUMENTFILE.filename.getUID()));
					newField.setScale(255);
					newField.setIndexed(true);
				} else {
					throw new BusinessException("Create new field for data type " + field.getDatatype() + " not implemented.");
				}
				checkValidAndAdjustNamesIfNecessary(fieldName, dbName, metaTransports.lstFieldTransports);
				newField.setFieldName(fieldName.getValue());
				newField.setDbColumn(dbName.getValue());
				fieldTransport.setEntityFieldMeta(newField);

			} else {
				throw new BusinessException("Create new field for " + field.getName() + " not implemented.");
			}
			if (fieldTransport.getEntityFieldMeta() != null) {
				fieldTransport.setTranslation(getTranslationsForField(field));
				metaTransports.lstFieldTransports.add(fieldTransport);
			}
		}

		updateEntityMeta(metaTransports, log);
	}

	private void initNewReferenceField(NucletFieldMeta newField, NucletIntegrationField field, UID foreignEntityUID, List<FieldMetaTransport> lstFieldTransports) {
		newField.setDataType(String.class.getCanonicalName());
		newField.setForeignEntity(foreignEntityUID);
		final EntityMeta<Object> foreignEntityMeta = metaProv.getEntity(foreignEntityUID);
		String sStringifiedReference = localeDelegate.getText(foreignEntityMeta.getLocaleResourceIdForTreeView());
		newField.setForeignEntityField(sStringifiedReference);
		newField.setSearchable(true);  //LOV
		newField.setScale(255);
		newField.setIndexed(true);
		Mutable<String> fieldName = new Mutable<>(field.getName());
		Mutable<String> dbName = new Mutable<>("INTID_STR" + field.getName());
		checkValidAndAdjustNamesIfNecessary(fieldName, dbName, lstFieldTransports);
		newField.setFieldName(fieldName.getValue());
		newField.setDbColumn(dbName.getValue());
	}

	private void updateEntityMeta(MetaTransports metaTransports, LogContext log) {
		Map<String, Exception> dbStructureChangeExceptions = new HashMap<>();
		try {
			metaFacadeLocal.createOrModifyEntity(metaTransports.metaTransport, metaTransports.lstFieldTransports,
					true, dbStructureChangeExceptions);
		} catch (Exception e) {
			throw new NuclosFatalException(e);
		}
	}

	private MetaTransports getMetaTransportsForEntity(UID entityId) {
		final EntityMeta<Long> eMeta = metaProv.<Long>getEntity(entityId);
		final EntityMetaTransport metaTransport = new EntityMetaTransport();
		// clone the entity meta for editing
		metaTransport.setEntityMetaVO(new NucletEntityMeta(eMeta, false));
		final MetaTransports result = new MetaTransports(metaTransport, new ArrayList<FieldMetaTransport>());
		for (FieldMeta<?> fMeta : metaProv.getAllEntityFieldsByEntity(entityId).values()) {
			if (SF.isEOField(entityId, fMeta.getUID())) {
				continue;
			}
			FieldMetaTransport fieldTransport = new FieldMetaTransport();
			// clone the field meta for editing
			fieldTransport.setEntityFieldMeta(new NucletFieldMeta<>(fMeta));
			if (fMeta.getOrder() != null && fMeta.getOrder() > result.order) {
				result.order = fMeta.getOrder();
			}
			result.lstFieldTransports.add(fieldTransport);
		}
		return result;
	}

	private List<TranslationVO> getTranslationsForField(NucletIntegrationField field) {
		List<TranslationVO> result = new ArrayList<>();
		for (LocaleInfo li : localeFacade.getAllLocales(false)) {
			Map<String, String> translations = new HashMap<>();
			for (String sLabelField : TranslationVO.LABELS_FIELD) {
				translations.put(sLabelField, field.getName());
			}
			TranslationVO translation = new TranslationVO(li, translations);
			result.add(translation);
		}
		return result;
	}

	private void checkValidAndAdjustNamesIfNecessary(Mutable<String> fieldName, Mutable<String> dbName, List<FieldMetaTransport> lstFieldTransports) {
		dbName.setValue(NuclosEntityValidator.escapeMutatedVowel(dbName.getValue()));
		dbName.setValue(dbName.getValue().replaceAll("[^A-Za-z0-9_]", "_"));
		fieldName.setValue(NuclosEntityValidator.escapeMutatedVowel(fieldName.getValue()));
		fieldName.setValue(fieldName.getValue().replaceAll("[^A-Za-z0-9_]", "_"));
		fieldName.setValue(StringUtils.uncapitalize(fieldName.getValue()));
		boolean valid = true;
		for (FieldMetaTransport fieldTransport : lstFieldTransports) {
			FieldMeta<?> fMeta = fieldTransport.getEntityFieldMeta();
			if (NucletIntegrationPointFacade.adjustNameIfNecessary(fieldName, fMeta.getFieldName(), -1)) {
				valid = false;
			}
			if (NucletIntegrationPointFacade.adjustNameIfNecessary(dbName, fMeta.getDbColumn(), 30)) {
				valid = false;
			}
		}
		if (!valid) {
			//check again
			checkValidAndAdjustNamesIfNecessary(fieldName, dbName, lstFieldTransports);
		}
	}

	private static class MetaTransports {
		private final EntityMetaTransport metaTransport;
		private final List<FieldMetaTransport> lstFieldTransports;
		private int order;
		public MetaTransports(final EntityMetaTransport metaTransport, final List<FieldMetaTransport> lstFieldTransports) {
			this.metaTransport = metaTransport;
			this.lstFieldTransports = lstFieldTransports;
		}
	}

	private static void notifyHigh(LogContext log, String message) {
		if (log instanceof InsertContext) {
			((InsertContext) log).notify(message, Priority.HIGH);
		} else if (log instanceof UpdateContext) {
			((UpdateContext) log).notify(message, Priority.HIGH);
		}
	}

}
