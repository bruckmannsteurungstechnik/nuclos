//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.api.eventsupport;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.nuclos.api.User;
import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.GenericBusinessObject;
import org.nuclos.api.businessobject.facade.Modifiable;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.locale.NuclosLocale;
import org.nuclos.api.notification.Priority;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RuleNotification;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.security.UserVO;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.common.ServerServiceLocator;
import org.nuclos.server.common.ejb3.LocaleFacadeLocal;
import org.nuclos.server.common.ejb3.SecurityFacadeLocal;
import org.nuclos.server.nbo.AbstractBusinessObject;
import org.nuclos.server.nbo.EOBOBridge;
import org.nuclos.server.security.UserFacadeLocal;
import org.nuclos.server.security.UserImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

abstract class AbstractEventObjectWithCache implements NotifiableEventObject {

	private final Logger logger;
	private final Map<String, Object> eventCache;
	private final String ruleClassname;
	private List<RuleNotification> lstRuleNotification;
	private SecurityFacadeLocal SEC_FACADE;
	private LocaleFacadeLocal LO_FACADE;
	private UserFacadeLocal USER_FACADE;
	private UID userDataLocale;
	
	public AbstractEventObjectWithCache(Map<String, Object> eventCache, String pRuleClassname, UID userDataLocale) {
		if (eventCache == null) {
			throw new IllegalArgumentException("cache must not be null");
		}
		this.lstRuleNotification = new ArrayList<RuleNotification>();
		this.eventCache = eventCache;
		this.ruleClassname = pRuleClassname;
		this.userDataLocale = userDataLocale;
		logger = LoggerFactory.getLogger(ruleClassname);
	}
	
	/* (non-Javadoc)
	 * @see org.nuclos.server.api.eventsupport.NotifiableEventObject#notify(java.lang.String, org.nuclos.api.businessobject.BusinessObject, org.nuclos.api.businessobject.BusinessObject)
	 */
	@Override
	public void notify(String message, BusinessObject source, BusinessObject target) {
		this.notify(message, Priority.NORMAL, source, target);
	}
	
	/* (non-Javadoc)
	 * @see org.nuclos.server.api.eventsupport.NotifiableEventObject#notify(java.lang.String, org.nuclos.api.notification.Priority, org.nuclos.api.businessobject.BusinessObject, org.nuclos.api.businessobject.BusinessObject)
	 */
	@Override
	public void notify(String message, Priority prio, BusinessObject source, BusinessObject target) {
		
		if (prio == null)
			throw new IllegalStateException("Priority must not be null.");
		if (message == null)
			throw new IllegalStateException("Message must not be null.");
	
		EntityObjectVO<Long> eoSource = EOBOBridge.getEO((AbstractBusinessObject) source);
		
		UID attSysIdentifier = SF.SYSTEMIDENTIFIER.getUID(eoSource.getDalEntity());
		
		RuleNotification ruleNotification = 
				new RuleNotification(org.nuclos.common.Priority.wrapPriority(prio), message, ruleClassname);

		ruleNotification.setSourceEntityUID(eoSource.getDalEntity());
		ruleNotification.setSourceId(eoSource.getPrimaryKey());
		ruleNotification.setSourceIdentifier(eoSource.getFieldValue(attSysIdentifier, String.class));
		
		if (target != null) {
			EntityObjectVO<Long> eoTarget = EOBOBridge.getEO((AbstractBusinessObject) target);
			
			attSysIdentifier = SF.SYSTEMIDENTIFIER.getUID(eoTarget.getDalEntity());

			ruleNotification.setTargetEntityUID(eoTarget.getDalEntity());
			ruleNotification.setTargetId(eoTarget.getPrimaryKey());
			ruleNotification.setTargetIdentifier(eoTarget.getFieldValue(attSysIdentifier, String.class));
		}
		
		this.lstRuleNotification.add(ruleNotification);
	}

	/* (non-Javadoc)
	 * @see org.nuclos.server.api.eventsupport.NotifiableEventObject#getRuleNotifications()
	 */
	@Override
	public List<RuleNotification> getRuleNotifications() {
		return lstRuleNotification;
	}
	
	protected String getRuleClassname() {
		return this.ruleClassname;
	}
	
	public Object getContextCacheValue(String key) {
		return eventCache.get(key);
	}
	
	public void addContextCacheValue(String key, Object value) {
		if (eventCache.containsKey(key)) {
			throw new IllegalArgumentException(String.format("Event cache contains key %s", key));
		}
		eventCache.put(key, value);
	}
	
	public void removeContextCacheValue(String key) {
		eventCache.remove(key);
	}

	public void log(String message) {
		logger.info(message);
	}
	
	public void logWarn(String message) {
		logger.warn(message);
	}
	
	public void logError(String message, Exception ex) {
		logger.error(message, ex);
	}
	
	private SecurityFacadeLocal getSecurityFacade() {
		if (SEC_FACADE == null) {
			try {
				SEC_FACADE = ServerServiceLocator.getInstance().getFacade(SecurityFacadeLocal.class);;
			}
			catch (RuntimeException ex) {
				throw new NuclosFatalException(ex);
			}
		}
		return SEC_FACADE;
	}

	private LocaleFacadeLocal getLocaleFacade() {
		if (LO_FACADE == null) {
			try {
				LO_FACADE = ServerServiceLocator.getInstance().getFacade(LocaleFacadeLocal.class);;
			}
			catch (RuntimeException ex) {
				throw new NuclosFatalException(ex);
			}
		}
		return LO_FACADE;
	}

	private UserFacadeLocal getUserFacade() {
		if (USER_FACADE == null) {
			try {
				USER_FACADE = ServerServiceLocator.getInstance().getFacade(UserFacadeLocal.class);;
			}
			catch (RuntimeException ex) {
				throw new NuclosFatalException(ex);
			}
		}
		return USER_FACADE;
	}

	public User getUser() {
		User retVal = null;

		try {
			UserVO user = getUserFacade().getByUserName(getSecurityFacade().getUserName());
			retVal = new UserImpl(user.getPrimaryKey(),
					user.getUsername(), user.getLastname(), user.getFirstname(), user.getEmail(),
					user.getLocked(), user.getSuperuser(), user.getLastPasswordChange(), user.getExpirationDate(), user.getPasswordChangeRequired(),
					userDataLocale == null ? null : NuclosLocale.valueOf(userDataLocale.toString().toUpperCase()));
		} catch (CommonBusinessException cbe) {
			// Ignore, just return null if user hasn't been found (Same behaviour like before).
		}
		return retVal;
	}

	public String getLanguage() {
		LocaleInfo logInf = getLocaleFacade().getUserLocale();
		return logInf.getLanguage();
	}

	public <T extends org.nuclos.api.businessobject.facade.thin.Stateful> T getStateful(final BusinessObject busiObject, final Class<T> targetClass) {
		if (busiObject.getClass().equals(targetClass)) {
			return (T) busiObject;
		}
		if (targetClass.equals(org.nuclos.api.businessobject.facade.Stateful.class) && busiObject instanceof org.nuclos.api.businessobject.facade.Stateful) {
			return (T) busiObject;
		}
		if (targetClass.equals(org.nuclos.api.businessobject.facade.thin.Stateful.class) && busiObject instanceof org.nuclos.api.businessobject.facade.thin.Stateful) {
			return (T) busiObject;
		}
		try {
			final Constructor<T> constructor = targetClass.getConstructor();
			final BusinessObject targetBo = (BusinessObject) constructor.newInstance();
			if (targetBo.getEntityUid().equals(busiObject.getEntityUid())) {
				EOBOBridge.setEO((AbstractBusinessObject<Object>) targetBo, EOBOBridge.getEO((AbstractBusinessObject<Object>) busiObject));
				return (T) targetBo;
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new RuntimeException("Stateful transformation of class " + busiObject.getClass().getCanonicalName() +
					" into " + targetClass.getCanonicalName() + " failed: " + e.getMessage());
		}
		throw new RuntimeException("Wrong rule usage! The requested stateful object of class " + targetClass.getCanonicalName() +
				" does not match the context object of type " + busiObject.getClass().getCanonicalName());
	}

	public <T extends BusinessObject> T getBusinessObject(final BusinessObject busiObject, final Class<T> targetClass) {
		if (busiObject.getClass().equals(targetClass)) {
			return (T) busiObject;
		}
		if (targetClass.equals(Modifiable.class) && busiObject instanceof Modifiable) {
			return (T) busiObject;
		}
		try {
			final Constructor<T> constructor = targetClass.getConstructor();
			final T targetBo = constructor.newInstance();
			if (targetBo.getEntityUid().equals(busiObject.getEntityUid())) {
				EOBOBridge.setEO((AbstractBusinessObject<Object>) targetBo, EOBOBridge.getEO((AbstractBusinessObject<Object>) busiObject));
				return targetBo;
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new RuntimeException("BusinessObject transformation of class " + busiObject.getClass().getCanonicalName() +
					" into " + targetClass.getCanonicalName() + " failed: " + e.getMessage());
		}
		throw new RuntimeException("Wrong rule usage! The requested business object of class " + targetClass.getCanonicalName() +
				" does not match the context object of type " + busiObject.getClass().getCanonicalName());
	}

	public <G extends GenericBusinessObject, B extends BusinessObject> G wrapBusinessObject(final B busiObject, final Class<G> genericClass)
			throws BusinessException {
		try {
			final Constructor<G> constructor = genericClass.getConstructor(busiObject.getClass());
			final G gbo = constructor.newInstance(busiObject);
			return gbo;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("BusinessObject of class " + busiObject.getClass().getCanonicalName() + " does not implement the generic type " + genericClass.getCanonicalName());
		}
	}

	public <G extends GenericBusinessObject> G wrapModifiable(final Modifiable modiObject, final Class<G> genericClass)
			throws BusinessException {
		try {
			final Constructor<G> constructor = genericClass.getConstructor(modiObject.getClass());
			final G gbo = constructor.newInstance(modiObject);
			return gbo;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("Modifiable of class " + modiObject.getClass().getCanonicalName() + " does not implement the generic type " + genericClass.getCanonicalName());
		}
	}
	
}
