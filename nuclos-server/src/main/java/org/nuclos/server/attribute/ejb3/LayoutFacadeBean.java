//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.attribute.ejb3;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.GenericObjectMetaDataProvider;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.TrueCondition;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.DependentDataMap.DependentKey;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.layoutml.LayoutMLParser;
import org.nuclos.common2.layoutml.exception.LayoutMLException;
import org.nuclos.server.attribute.valueobject.LayoutVO;
import org.nuclos.server.autosync.XMLEntities;
import org.nuclos.server.common.EventSupportCache;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.genericobject.GenericObjectMetaDataCache;
import org.nuclos.server.genericobject.Modules;
import org.nuclos.server.genericobject.ejb3.GenericObjectFacadeLocal;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.nuclos.server.statemodel.ejb3.StateFacadeLocal;
import org.nuclos.server.transfer.OldXmlExportImportHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.InputSource;

/**
 * Layout facade encapsulating generic object screen layout management.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Transactional(noRollbackFor= {Exception.class})
public class LayoutFacadeBean implements LayoutFacadeLocal, LayoutFacadeRemote {

	public static final Logger LOG = LoggerFactory.getLogger(LayoutFacadeBean.class);

	// Spring injection
	
	@Autowired
	private GenericObjectFacadeLocal genericObjectFacade;
	
	@Autowired
	private MasterDataFacadeLocal masterDataFacade;
	
	@Autowired
	private EventSupportCache esCache;
	
	@Autowired
	private SpringDataBaseHelper dataBaseHelper;
	
	// end of Spring injection
	
	public LayoutFacadeBean() {
	}

	/**
	 * imports the given layouts, adding new and overwriting existing layouts. The other existing layouts are untouched.
	 * Currently, only the layoutml description is imported, not the usages.
	 * @param colllayoutvo
	 */
	@RolesAllowed("UseManagementConsole")
	public void importLayouts(Collection<LayoutVO> colllayoutvo) throws CommonBusinessException {
		for (LayoutVO layoutvo : colllayoutvo) {
			importLayout(E.LAYOUT, layoutvo);
		}
		GenericObjectMetaDataCache.getInstance().revalidate();
		masterDataFacade.notifyClients(E.LAYOUT.getUID());
	}

	@RolesAllowed("Login")
	public <PK> MasterDataVO<PK> create(UID entity, MasterDataVO<PK> mdvo,
		IDependentDataMap mpDependants, String customUsage) throws CommonCreateException,
		CommonPermissionException, NuclosBusinessRuleException {
	
		MasterDataVO<PK> retVal = masterDataFacade.create(mdvo, customUsage);
		
		esCache.invalidate(E.LAYOUT);
		return retVal;
	}
	
	@RolesAllowed("Login")
	public <PK> PK modify(UID entity, MasterDataVO<PK> mdvo,
			IDependentDataMap mpDependants, String customUsage) throws CommonCreateException,
			CommonFinderException, CommonRemoveException,
			CommonStaleVersionException, CommonValidationException,
			CommonPermissionException, NuclosBusinessRuleException {
	 
		 PK modify = masterDataFacade.modify(mdvo, customUsage);
		 
		 esCache.invalidate(E.LAYOUT);
		 return modify;
	 }
	
	private void importLayout(EntityMeta<UID> entityMeta, LayoutVO layoutvo)
			throws CommonCreateException, CommonStaleVersionException, CommonValidationException, CommonPermissionException, NuclosBusinessRuleException {

		final String sLayoutName = layoutvo.getName();
		//final MasterDataMetaVO mdmetavo = 
		final CollectableSearchCondition cond = SearchConditionUtils.newComparison(E.LAYOUT.name, ComparisonOperator.EQUAL, sLayoutName);

		final Collection<MasterDataVO<UID>> collmdvo = masterDataFacade.getMasterData(entityMeta, cond);
		if (collmdvo.isEmpty()) {
			final MasterDataVO<UID> mdvoNew = new MasterDataVO<UID>(entityMeta, true);
			mdvoNew.setFieldValue(E.LAYOUT.name, sLayoutName);
			mdvoNew.setFieldValue(E.LAYOUT.description, layoutvo.getDescription());
			mdvoNew.setFieldValue(E.LAYOUT.layoutML, layoutvo.getLayoutML());
			masterDataFacade.create(mdvoNew, null);
		}
		else {
			final MasterDataVO<UID> mdvo = collmdvo.iterator().next();
			mdvo.setFieldValue(E.LAYOUT.layoutML, layoutvo.getLayoutML());
			try {
				masterDataFacade.modify(mdvo, null);
			}
			catch (CommonFinderException e) {
				throw new CommonFatalException(e);
			}
			catch (CommonRemoveException e) {
				throw new CommonFatalException(e);
			}
		}
	}

	/**
	 * refreshes the module attribute relation table and all generic object views (console function)
	 */
	@RolesAllowed("UseManagementConsole")
	public void refreshAll() {
		GenericObjectMetaDataCache.getInstance().revalidate();
	}

	/**
	 * @return true, if detail layout is available for the given entity name, otherwise false
	 */
	@RolesAllowed("Login")
	public boolean isDetailLayoutAvailable(UsageCriteria usage) {
		return getDetailLayoutIDForUsage(usage) == null ? false : true;
	}

	@Cacheable(value="layoutML", key="#p0")
	public String getLayoutML(UID pk) throws CommonBusinessException {
		return masterDataFacade.get(E.LAYOUT, pk).getFieldValue(E.LAYOUT.layoutML);
	}
	
	@Cacheable(value="allLayoutUidsForEntity", key="#p0")
	public Set<UID> getAllLayoutUidsForEntity(UID entityUid) {
		
		Set<UID> result = new HashSet<UID>();

		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<UID> query = builder.createQuery(UID.class);
		DbFrom t = query.from(E.LAYOUTUSAGE);
		query.select(t.baseColumn(E.LAYOUTUSAGE.layout));
		query.where(builder.equalValue(t.baseColumn(E.LAYOUTUSAGE.entity), entityUid));

		
		result.addAll(dataBaseHelper.getDbAccess().executeQuery(query.distinct(true)));
		return result;
	}
	
	@Cacheable(value="layoutNucletUid", key="#p0")
	public UID getNucletUID(UID pk) throws CommonBusinessException {
		return masterDataFacade.get(E.LAYOUT, pk).getFieldUid(E.LAYOUT.nuclet);
	}

	
	public UID getDetailLayoutIDForUsage(UsageCriteria usage) {
		return getDetailLayoutIDForUsage(usage, false);
	}
	
	/**
	 * @return the detail layout for the given Usage Criteria name if any, otherwise null
	 */
	@Cacheable(value="layoutIDForUsage", key="#p0.hashCode() + (#p1 ? 1 : 0)")
	public UID getDetailLayoutIDForUsage(UsageCriteria usage, boolean bSearchLayout) {
		if (usage == null) {
			throw new CommonFatalException("UsageCriteria may not be null!");
		}
		
		try {
			UID layoutUsageUID = getLayoutUsageUID(usage, bSearchLayout);
			if (layoutUsageUID != null) {
				return getLayoutUsage(layoutUsageUID).getFieldUid(E.LAYOUTUSAGE.layout);
			}
		} catch (CommonBusinessException cbe) {
			LOG.warn(cbe.getMessage(), cbe);
		}
		
		return null;
	}

	private MasterDataVO<UID> getLayoutUsage(UID pk) throws CommonBusinessException {
		return masterDataFacade.get(E.LAYOUTUSAGE, pk);
	}

	private UID getLayoutUsageUID(UsageCriteria usage, boolean bSearchlayout) throws CommonBusinessException {
		Collection<UsageCriteria> ucs = getAllUsageCriteriasFromLayoutUsages();
		
		UsageCriteria usagecriteriaBestMatching = UsageCriteria.getBestMatchingUsageCriteria(ucs, usage);
		UID retVal = usagecriteriaBestMatching != null ? getLayoutUsageUIDForExactUsageCriteria(usagecriteriaBestMatching, bSearchlayout) : null;
		
		if (retVal == null && usage.getStatusUID() == null && Modules.getInstance().isModule(usage.getEntityUID())) {
			UID statusUID = SpringApplicationContextHolder.getBean(StateFacadeLocal.class).getInitialState(usage).getId();
			UsageCriteria usage2 = new UsageCriteria(usage.getEntityUID(), usage.getProcessUID(), statusUID, usage.getCustom());
			
			usagecriteriaBestMatching = UsageCriteria.getBestMatchingUsageCriteria(ucs, usage2);
			if (usagecriteriaBestMatching != null
					// prevent endless loop...
						&& !usagecriteriaBestMatching.equals(usage)) {
				
				retVal = getLayoutUsageUID(usagecriteriaBestMatching, bSearchlayout);
			}
		}
		
		return retVal;
	}
	
	public Collection<UID> getEntitiesAssignedToLayoutId(UID layoutUID) throws CommonBusinessException {
		Set<UID> retVal = new HashSet<UID>();
		Set<UsageCriteria> usages = getAllUsageCriteriasFromLayoutUsages();
		for (UsageCriteria usage : usages) {
			UID entityUID = usage.getEntityUID();
			if (retVal.contains(entityUID)) {
				continue;
			}
			
			if (layoutUID.equals(getDetailLayoutIDForUsage(usage))) {
				retVal.add(entityUID);
			}
		}
		
		return retVal;
	}
		
	@Cacheable(value="usageCriteriasFromLayoutUsage")
	private Set<UsageCriteria> getAllUsageCriteriasFromLayoutUsages() throws CommonBusinessException {
		Set<UsageCriteria> ucs = new HashSet<UsageCriteria>();
		
		Collection<MasterDataVO<UID>> collMD = masterDataFacade.getMasterData(E.LAYOUTUSAGE, TrueCondition.TRUE);
		for (MasterDataVO<UID> mdvo : collMD) {
			ucs.add(UsageCriteria.createUsageCriteriaFromLayoutUsage(mdvo.getEntityObject(), null));
		}
		
		return ucs;
	}
	
	@Cacheable(value="layoutUsageUids", key="#p0.hashCode() + (#p1 ? 1 : 0)")
	private UID getLayoutUsageUIDForExactUsageCriteria(UsageCriteria usage, boolean bSearchLayout) throws CommonBusinessException {
		
		if (E.isNuclosEntity(usage.getEntityUID())) {
			// search in system entities...
			final Collection<MasterDataVO<UID>> layouts = XMLEntities.getSystemObjects(E.LAYOUT.getUID(), TrueCondition.TRUE);
			for (MasterDataVO<UID> layout : layouts) {
				for (EntityObjectVO<UID> layoutusage : layout.getDependents().<UID>getDataPk(E.LAYOUTUSAGE.layout)) {
					if (usage.getEntityUID().equals(layoutusage.getFieldUid(E.LAYOUTUSAGE.entity))) {
						Boolean searchLayout = layoutusage.getFieldValue(E.LAYOUTUSAGE.searchScreen);
						
						if (searchLayout == null) {
							searchLayout = Boolean.FALSE;
						}
						
						if (searchLayout.equals(bSearchLayout)) {
							return layoutusage.getPrimaryKey();							
						}
					}
				}
			}
			return null;
			
		} else {
			CollectableSearchCondition ccEntity = SearchConditionUtils.newUidComparison(E.LAYOUTUSAGE.entity, ComparisonOperator.EQUAL, usage.getEntityUID());
			
			CollectableSearchCondition ccCustom = usage.getCustom() != null ?
					SearchConditionUtils.newComparison(E.LAYOUTUSAGE.custom, ComparisonOperator.EQUAL, usage.getCustom())
					: SearchConditionUtils.newIsNullCondition(E.LAYOUTUSAGE.custom);
					
			CollectableSearchCondition ccProcess = usage.getProcessUID() != null ?
					SearchConditionUtils.newUidComparison(E.LAYOUTUSAGE.process, ComparisonOperator.EQUAL, usage.getProcessUID())
					: SearchConditionUtils.newIsNullCondition(E.LAYOUTUSAGE.process);
					
			CollectableSearchCondition ccState = usage.getStatusUID() != null ?
					SearchConditionUtils.newUidComparison(E.LAYOUTUSAGE.state, ComparisonOperator.EQUAL, usage.getStatusUID())
					: SearchConditionUtils.newIsNullCondition(E.LAYOUTUSAGE.state);
					
			CollectableSearchCondition ccSearchLayouts = 
					SearchConditionUtils.newComparison(E.LAYOUTUSAGE.searchScreen, ComparisonOperator.EQUAL, bSearchLayout);
					
			CollectableSearchCondition ccAll = SearchConditionUtils.and(ccEntity, ccCustom, ccProcess, ccState, ccSearchLayouts);
			Collection<MasterDataVO<UID>> collMD = masterDataFacade.getMasterData(E.LAYOUTUSAGE, ccAll);
			if (collMD.isEmpty()) {
				return null;
			}
			
			return collMD.iterator().next().getPrimaryKey();
		}
	}

	@RolesAllowed("Login")
	public List<LayoutVO> getMasterDataLayoutForNuclet(UID nuclet) {
		List<LayoutVO> retVal = new ArrayList<LayoutVO>();
		
		final List<MasterDataVO<UID>> lstMDLayoutUsage = new ArrayList<MasterDataVO<UID>>(masterDataFacade.getMasterData(E.LAYOUT, null));
		for (MasterDataVO<UID> mdvoUsage : lstMDLayoutUsage) {
			if (nuclet != null && mdvoUsage.getFieldValue(E.LAYOUT.nuclet.getUID()) != null && nuclet.equals(mdvoUsage.getFieldValue(E.LAYOUT.nuclet.getUID(), Integer.class))) {
				retVal.add(MasterDataWrapper.getLayoutVO(mdvoUsage));
			}
			else if (nuclet == null && mdvoUsage.getFieldValue(E.LAYOUT.nuclet.getUID()) == null){
				retVal.add(MasterDataWrapper.getLayoutVO(mdvoUsage));
			}
		}
		
		return retVal;
	}
	
	/**
	 * @param sEntity
	 * @return the layout for the given entity name if any, otherwise null
	 */
	@RolesAllowed("Login")
	public String getMasterDataLayout(UID sEntity, boolean bSearchMode, String customUsage) {
		String sLayoutML = null;
		CollectableSearchCondition csc = customUsage == null ? SearchConditionUtils.newIsNullCondition(E.LAYOUTUSAGE.custom)
				: SearchConditionUtils.newComparison(E.LAYOUTUSAGE.custom, ComparisonOperator.EQUAL, customUsage);
		Collection<MasterDataVO<UID>> collLayouts = masterDataFacade.getMasterData(E.LAYOUTUSAGE, csc);
		if (collLayouts.isEmpty()) {
			collLayouts = masterDataFacade.getMasterData(E.LAYOUTUSAGE, TrueCondition.TRUE);
		}
		final List<MasterDataVO<UID>> lstMDLayoutUsage = new ArrayList<MasterDataVO<UID>>(collLayouts);
		for (MasterDataVO<UID> mdvoUsage : lstMDLayoutUsage) {
			final UID entityUID = mdvoUsage.getFieldUid(E.LAYOUTUSAGE.entity);
			final boolean bSearch = mdvoUsage.getFieldValue(E.LAYOUTUSAGE.searchScreen);
			final UID iLayoutId = mdvoUsage.getFieldUid(E.LAYOUTUSAGE.layout);

			if (entityUID.equals(sEntity) && bSearch == bSearchMode) {
				try {
					MasterDataVO<UID> mdvoLayout = masterDataFacade.get(E.LAYOUT, iLayoutId);							
					sLayoutML = (String) mdvoLayout.getFieldValue(E.LAYOUT.layoutML.getUID(), String.class);
					break;
				}
				catch (CommonFinderException e) {
					throw new NuclosFatalException(e);
				} catch (CommonPermissionException e) {
					throw new NuclosFatalException(e);
				}
			}
		}
		return sLayoutML;
	}

	/**
	 * returns the entity names of the subform entities along with their foreignkey field
	 * and the referenced parent entity name used in the given layout
	 * Note that this works only for genericobject entities
	 * @param layoutUID
	 */
	@RolesAllowed("Login")
	@Cacheable(value="goLayout", key="#p0.getString()")
	public Map<EntityAndField, UID> getSubFormEntityAndParentSubFormEntityNamesByLayoutId(UID layoutUID) {
		String sLayoutML = null;

		try {
			MasterDataVO<UID> mdvoLayout = masterDataFacade.get(E.LAYOUT, layoutUID);
			sLayoutML = (String) mdvoLayout.getFieldValue(E.LAYOUT.layoutML.getUID(), String.class);
		}
		catch (CommonFinderException e) {
			throw new NuclosFatalException(e);
		} catch (CommonPermissionException e) {
			throw new NuclosFatalException(e);
		}

		if (sLayoutML == null) {
			throw new NuclosFatalException(StringUtils.getParameterizedExceptionMessage("layout.facade.exception.1", layoutUID));
				//"Die Eingabemaske mit der Id \"" + iLayoutId + "\" wurde nicht gefunden.");
		}
		try {
			return new LayoutMLParser().getSubFormEntityAndParentSubFormEntities(new InputSource(new StringReader(sLayoutML)));
		} catch (LayoutMLException e) {
			throw new NuclosFatalException(e);
		}
	}

	/**
	 * returns the names of the subform entities along with their foreignkey field
	 * and the referenced parent entity name used in the given entity
	 * 
	 * Caveat: Das Methodenformat und der Kommentar sind ein wenig verwirrend und verschleiern
	 * die internen Zusammenhaenge. Fuer Stammdaten ist eine Objekt-ID ueberhaupt nicht notwendig,
	 * sie wird hier ignoriert. Bei generischen Objekten werden die UsageCritera (Modul- +
	 * Prozess-ID) verwendet, um das "am besten passende" Layout zu ermitteln (s.
	 * GenericObjectMetaDataCache.getBestMatchingLayoutId().
	 * 
	 * @param entity
	 * @param id, id of MasterDataVO or GenericObjectVO
	 * @param forImportOrExport, true if it is used for import- or export-routines
	 * 
	 * @deprecated This method is not very effective for generic objects. Consider to 
	 * 		use {@link #getSubFormEntityAndParentSubFormEntityNamesByGO(UsageCriteria)}
	 * 		or {@link #getSubFormEntityAndParentSubFormEntityNamesMD(UsageCriteria, boolean)}
	 * 		directly.
	 */
	@RolesAllowed("Login")
	public Map<EntityAndField, UID> getSubFormEntityAndParentSubFormEntities(UID entity, Object id, boolean forImportOrExport, String customUsage) {

		Map<EntityAndField, UID> result = new HashMap<EntityAndField, UID>();

		if (Modules.getInstance().isModule(entity)) {
			if (id instanceof Long && (Long)id == -1L) {
				return result;
			}
			try {
				// This is to get the usage criteria which leads directly to the correct Layout;
				final UsageCriteria usage = genericObjectFacade.getGOMeta((Long)id, entity, customUsage);
				
				result = getSubFormEntityAndParentSubFormEntityNamesByGO(usage);
			}
			catch (CommonFinderException e) {
				throw new CommonFatalException(e);
			}
		} else {
			UsageCriteria usage = new UsageCriteria(entity, null, null, customUsage);
			result = getSubFormEntityAndParentSubFormEntityNamesMD(usage, forImportOrExport);
		}

		return result;
	}

	@RolesAllowed("Login")
	public Map<EntityAndField, UID> getSubFormEntityAndParentSubFormEntityNamesMD(UsageCriteria usage, boolean forImportOrExport) {

		UID uidEntity = usage.getEntityUID();
		UID layoutUID = getDetailLayoutIDForUsage(usage);
		String sLayoutML = null;
		if (layoutUID != null) {
			try {
				sLayoutML = getLayoutML(layoutUID);
			} catch (CommonBusinessException cbe) {
				LOG.warn(cbe.getMessage(), cbe);
			}
			
		}
		
		final Map<EntityAndField, UID> result;
		
		if (sLayoutML == null) {
			// special handling for entities with manually build layouts which are not saved in the database
			
			if (E.isNuclosEntity(uidEntity)) {
				result = new HashMap<EntityAndField, UID>();
				
				if(E.STATEMODEL.checkEntityUID(uidEntity)) {
					result.put(new EntityAndField(E.STATEMODELUSAGE, E.STATEMODELUSAGE.statemodel), null);
					result.put(new EntityAndField(E.STATE, E.STATE.model), null);
				}
				else if(E.STATE.checkEntityUID(uidEntity)) {
					result.put(new EntityAndField(E.STATETRANSITION, E.STATETRANSITION.state2), null);
					result.put(new EntityAndField(E.ROLEATTRIBUTEGROUP, E.ROLEATTRIBUTEGROUP.state), null);
					result.put(new EntityAndField(E.ROLESUBFORM, E.ROLESUBFORM.state), null);
				}
				else if(E.STATETRANSITION.checkEntityUID(uidEntity)) {
					result.put(new EntityAndField(E.ROLETRANSITION, E.ROLETRANSITION.transition), null);
					result.put(new EntityAndField(E.SERVERCODETRANSITION, E.SERVERCODETRANSITION.transition), null);
				}
				else if(E.CHART.checkEntityUID(uidEntity)) {
					result.put(new EntityAndField(E.CHARTUSAGE, E.CHARTUSAGE.chart), null);
				}
				else if(E.DATASOURCE.checkEntityUID(uidEntity)) {
					result.put(new EntityAndField(E.DATASOURCEUSAGE, E.DATASOURCEUSAGE.datasource), null);
				}
				else if(E.REPORT.checkEntityUID(uidEntity)) {
					result.put(new EntityAndField(E.REPORTOUTPUT, E.REPORTOUTPUT.parent), null);				
				}
			}
/* Der "tiefe" XML-Export tut's ueberhaupt nicht mehr mit dem "Original"-Code: */
//			else {
//				throw new NuclosFatalException("Die Eingabemaske f\u00fcr die Entit\u00e4t \"" + entityName + "\" fehlt.");
//			}
/* tentativer Fix (10/2009)- TODO: ueberpruefen!!! */
			else {
				//@see NUCLOSINT-1524. there are entities that are not system entities and do not have an layout defined.!
				/*MasterDataMetaVO metaVO =
					MasterDataMetaCache.getInstance().getMetaData(entityName);
				if (!metaVO.isSystemEntity()) {
					throw new NuclosFatalException(
						StringUtils.getParameterizedExceptionMessage("layout.facade.exception.2", entityName));
				}*/
				result = Collections.emptyMap();
			}
		}
		else {
			try {
				final InputSource inSrc = new InputSource(new StringReader(sLayoutML));
				inSrc.setSystemId("entity=" + uidEntity + ":forImportOrExport=" + forImportOrExport);
				if (forImportOrExport) {
					result = getSubFormEntityAndParentSubFormEntityNamesMDforIE(inSrc, uidEntity);
				}
				else {
					result = getSubFormEntityAndParentSubFormEntityNamesMD(inSrc, uidEntity);
				}
			} catch (LayoutMLException e) {
				throw new NuclosFatalException(e);
			}
		}

		return result;
	}

	@Cacheable(value="mdLayout", key="#p1.getString()")
	Map<EntityAndField, UID> getSubFormEntityAndParentSubFormEntityNamesMD(InputSource inSrc, UID entityName) throws LayoutMLException {
		return new LayoutMLParser().getSubFormEntityAndParentSubFormEntities(inSrc);
	}
	
	@Cacheable(value="mdLayoutImportExport", key="#p1.getString()")
	Map<EntityAndField, UID> getSubFormEntityAndParentSubFormEntityNamesMDforIE(InputSource inSrc, UID entityName) throws LayoutMLException {
		
		final Map<EntityAndField, UID> result = new HashMap<EntityAndField, UID>();
		final Map<EntityAndField, UID> subformtree = new LayoutMLParser().getSubFormEntityAndParentSubFormEntities(inSrc);
		for (EntityAndField eafn : subformtree.keySet()) {
			/* Das kann so wohl kaum stimmen, s. XmlExportFacadeBean.exportGOEntity(); tentativer Fix (10/2009): */
			UID uidSubform = eafn.getEntity();
			UID uidParentEntityName = subformtree.get(eafn);
			UID uidForeignKeyFieldName = OldXmlExportImportHelper.getForeignKeyFieldUID(
				(uidParentEntityName == null ? entityName : uidParentEntityName), eafn.getField(), uidSubform);
			result.put(new EntityAndField(uidSubform, uidForeignKeyFieldName), uidParentEntityName/*?!*/);
		}
		return result;
	}

	@RolesAllowed("Login")
	public Map<EntityAndField, UID> getSubFormEntityAndParentSubFormEntityNamesByGO(UsageCriteria usage) throws CommonFinderException{
		final GenericObjectMetaDataProvider lometadataprovider = GenericObjectMetaDataCache.getInstance();
	    final UID iBestMatchingLayoutId = lometadataprovider.getBestMatchingLayout(usage, false);
		return getSubFormEntityAndParentSubFormEntityNamesByLayoutId(iBestMatchingLayoutId);
	}

	/**
	 * returns the entity names of the subform entities along with their foreignkey field
	 * and the referenced parent entity name used in the given layout
	 * Note that this works only for genericobject entities
	 * 
	 * §ejb.interface-method view-type="local"
	 * §ejb.permission role-name="Login"
	 * 
	 * @param layoutUID
	 */
	public Map<EntityAndField, UID> getSubFormEntityAndParentSubFormEntityNamesById(UID layoutUID) {
		String sLayoutML = null;
		try {
			MasterDataVO<UID> mdvoLayout = masterDataFacade.get(E.LAYOUT.getUID(), layoutUID);					
			sLayoutML = (String) mdvoLayout.getFieldValue(E.LAYOUT.layoutML.getUID(), String.class);
		}
		catch (CommonFinderException e) {
			throw new NuclosFatalException(e);
		} catch (CommonPermissionException e) {
			throw new NuclosFatalException(e);
		}

		if (sLayoutML == null) {
			throw new NuclosFatalException(StringUtils.getParameterizedExceptionMessage("layout.facade.exception.1", layoutUID));
		}
		try {
			return new LayoutMLParser().getSubFormEntityAndParentSubFormEntities(new InputSource(new StringReader(sLayoutML)));
		} catch (LayoutMLException e) {
			throw new NuclosFatalException(e);
		}
	}
	
	/*
	 * @ejb.interface-method view-type="local"
	 * @ejb.permission role-name="Login"
	 */
	@Caching(evict = { 
			@CacheEvict(value="webLayoutCalculated", allEntries=true),
			@CacheEvict(value="webLayoutFixed", allEntries=true),
			@CacheEvict(value="webLayoutResponsive", allEntries=true),
			@CacheEvict(value="webLayoutRules", allEntries=true),
			@CacheEvict(value="goLayout", allEntries=true),
			@CacheEvict(value="mdLayout", allEntries=true),
			@CacheEvict(value="mdLayoutImportExport", allEntries=true),
			@CacheEvict(value="layoutML", allEntries=true),
			@CacheEvict(value="layoutIDForUsage", allEntries=true),
			@CacheEvict(value="layoutUsageUids", allEntries=true),
			@CacheEvict(value="usageCriteriasFromLayoutUsage", allEntries=true),
			@CacheEvict(value="layoutNucletUid", allEntries=true),
			@CacheEvict(value="allLayoutUidsForEntity", allEntries=true),
			@CacheEvict(value="dependentKeyBetween", allEntries=true),
			@CacheEvict(value="allLayoutUsageCriterias", allEntries=true)

	})
	public void evictCaches() {
		MetaProvider.getInstance().invalidateWebLayoutCache();
	}

	@Override
	@RolesAllowed("Login")
	public List<LayoutVO> getMasterDataLayout() {
		List<LayoutVO> retVal = new ArrayList<>();
		
		final List<MasterDataVO> lstMDLayoutUsage = new ArrayList<>(masterDataFacade.getMasterData(
				E.LAYOUT, null));
		
		for (MasterDataVO mdvoUsage : lstMDLayoutUsage) {
			retVal.add(MasterDataWrapper.getLayoutVO(mdvoUsage));
		}
		
		return retVal;
	}
	
	/**
	 * get the Dependent Key from Layout,
	 * no support for sub-subforms
	 *
	 * @return null if key is not found or unique
	 */
	@Cacheable(value="dependentKeyBetween", key="#p0.getString() + #p1.getString() + #p2")
	public IDependentKey getDependentKeyBetween(UID master, UID subform, String customUsage) {
		//final UsageCriteria usage = new UsageCriteria(master, null, null, customUsage);
		//return getDependentKeyBetween(usage, subform);

		IDependentKey result = null;

		// find all usage criterias for master entity (NUCLOS-7306)
		Collection<UsageCriteria> allLayoutUsageCriterias = getAllLayoutUsageCriterias(master, customUsage);
		for (UsageCriteria uc : allLayoutUsageCriterias) {
			final IDependentKey dependentKeyBetween = getDependentKeyBetween(uc, subform);
			if (result == null) {
				result = dependentKeyBetween;
			} else if (dependentKeyBetween != null) {
				if (!RigidUtils.equal(result, dependentKeyBetween)) {
					// result is not unique;
					return null;
				}
			}
		}

		if (result == null && customUsage != null) {
			// no result with customUsage... try to search without it
			return getDependentKeyBetween(master, subform, null);
		}

		return result;
	}

	@Cacheable(value="allLayoutUsageCriterias", key="#p0.getString() + #p1")
	public Collection<UsageCriteria> getAllLayoutUsageCriterias(UID entityUID, String customUsage) {
		Collection<UsageCriteria> result = new ArrayList<>();
		CollectableSearchCondition ccEntity = SearchConditionUtils.newUidComparison(E.LAYOUTUSAGE.entity, ComparisonOperator.EQUAL, entityUID);
		CollectableSearchCondition ccCustom = customUsage == null ?
				SearchConditionUtils.newIsNullCondition(E.LAYOUTUSAGE.custom):
				SearchConditionUtils.newComparison(E.LAYOUTUSAGE.custom, ComparisonOperator.EQUAL, customUsage);
		Collection<MasterDataVO<UID>> layoutUsages = masterDataFacade.getMasterData(E.LAYOUTUSAGE, SearchConditionUtils.and(ccEntity, ccCustom));
		for (MasterDataVO<UID> layoutUsage : layoutUsages) {
			result.add(new UsageCriteria(entityUID,
					layoutUsage.getFieldUid(E.LAYOUTUSAGE.process),
					layoutUsage.getFieldUid(E.LAYOUTUSAGE.state),
					customUsage));
		}
		return result;
	}
	
	/**
	 * get the Dependent Key from Layout,
	 * no support for sub-subforms
	 *
	 * @return null if key is not found or unique
	 */
	public IDependentKey getDependentKeyBetween(UsageCriteria usageOfMaster, UID subform) {
		Map<EntityAndField, UID> subFormEntityNames;
		EntityMeta<?> masterMeta = MetaProvider.getInstance().getEntity(usageOfMaster.getEntityUID());
		
		// 1. Search via MetaData: If only one referencing key found, use it.
		
		UID singleReferencingField = null;
		Map<UID, FieldMeta<?>> mpFields = MetaProvider.getInstance().getAllEntityFieldsByEntity(subform);
    	for (UID field : mpFields.keySet()) {
    		FieldMeta<?> efmdv = mpFields.get(field);
    		if (usageOfMaster.getEntityUID().equals(efmdv.getForeignEntity())) {
    			if (singleReferencingField != null) {
    				// more than one found... break
    				singleReferencingField = null;
    				break;
    			} else {
    				singleReferencingField = field;
    			}
    		}
    	}
    	if (singleReferencingField != null) {
			return DependentDataMap.createDependentKey(singleReferencingField);
		}
		
		// 2. If more than one referencing key exists, search via layout.
		
		if (masterMeta.isStateModel()) {
			try {
				subFormEntityNames = getSubFormEntityAndParentSubFormEntityNamesByGO(usageOfMaster);
			} catch (CommonFinderException e) {
				LOG.error(e.getMessage(), e);
				subFormEntityNames = null;
			}
		} else {
			subFormEntityNames = getSubFormEntityAndParentSubFormEntityNamesMD(usageOfMaster, false);
		}
		if (subFormEntityNames != null) {
			for (Entry<EntityAndField, UID> depLayoutInfo : subFormEntityNames.entrySet()) {
				if (depLayoutInfo.getValue() != null) {
					// has parent key -> sub-subform -> not supported
					continue;
				} else {
					EntityAndField entityAndField = depLayoutInfo.getKey();
					if (entityAndField.getEntity().equals(subform)) {
						if (entityAndField.getField() != null) {
							return new DependentKey(entityAndField.getField());
						}
					}
				}
			}
		}
		
		return null;
	}
	
}
