//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.autonumber;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.nuclos.common.DefaultComponentTypes;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.LangUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.SessionUtils;
import org.nuclos.server.dal.processor.jdbc.impl.EOSearchExpressionUnparser;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.i18n.language.data.DataLanguageCache;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * <code>AutoNumberHelper</code>
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:moritz.neuhaeuser@nuclos.de">Moritz Neuhäuser</a>
 */
@Component
public class AutoNumberHelper {


	private MetaProvider metaProvider;
	private SpringDataBaseHelper dataBaseHelper;
	
	@Autowired
	private DataLanguageCache dlCache;
	
	AutoNumberHelper() {
	}

	@Autowired
	final void setMetaProvider(MetaProvider metaProvider) {
		this.metaProvider = metaProvider;
	}

	@Autowired
	final void setDataBaseHelper(SpringDataBaseHelper dataBaseHelper) {
		this.dataBaseHelper = dataBaseHelper;
	}

	/**
	 * find field for autonumber
	 * 
	 * @return null or field meta data
	 */
	public FieldMeta<Integer> findAutoNumberFieldIfAny(final UID entityUID) {
		final Map<UID, FieldMeta<?>> mpEoFieldMetaVO = metaProvider.getAllEntityFieldsByEntity(entityUID);
		FieldMeta<?> metFieldVO = null;
		for (final Entry<UID, FieldMeta<?>> entry: mpEoFieldMetaVO.entrySet()) {
			if (DefaultComponentTypes.AUTONUMBER.equals(entry.getValue().getDefaultComponentType())) {
				metFieldVO = entry.getValue();
				break;
			}

		}
		return (FieldMeta<Integer>) metFieldVO;
	}

	/**
	 * find next autonumber 
	 * @param evo			entityVO
	 * @param metaFieldVO	metaVO for the autonumber field
	 * @return
	 */
	public Integer findNextAutoNumber(final EntityObjectVO<?> evo, final FieldMeta<Integer> metaFieldVO ) throws NuclosBusinessRuleException {
		final UID entityUID = evo.getDalEntity();

		//2. check for parent autonumberfield
		final UID entityParentUID = metaFieldVO.getAutonumberEntity();

		final EntityMeta<Long> metaVO = metaProvider.<Long>getEntity(entityUID);

		final DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		final DbQuery<Integer> query = builder.createQuery(Integer.class);
		final DbFrom<Long> m = query.from(metaVO);
		query.select(builder.max(m.baseColumn(metaFieldVO)));

		if (null != entityParentUID) {
			// use parent entity for autonumbergeneration
			final EntityMeta<?> metaVOParent = metaProvider.getEntity(entityUID);
			for (FieldMeta<?> ref : metaProvider.getAllEntityFieldsByEntity(metaVOParent.getUID()).values()) {
				if (LangUtils.equal(entityParentUID, ref.getForeignEntity())) {
					final Long idParent = evo.getFieldId((FieldMeta<Long>) ref);
					if (idParent == null) {
						throw new NuclosBusinessRuleException(org.nuclos.common2.StringUtils.getParameterizedExceptionMessage("autonumber.exception.reffieldempty", ref.getFieldName()));
					}
					CollectableComparison comp = SearchConditionUtils.newIdComparison(ref, ComparisonOperator.EQUAL, idParent);
					EntityMeta<Long> entityMeta = metaProvider.getEntity(ref.getEntity());
					EOSearchExpressionUnparser eoUnparser = new EOSearchExpressionUnparser(query, entityMeta, 
							(SessionUtils)SpringApplicationContextHolder.getBean("sessionUtils"));
					eoUnparser.setDataLanguageCache(dlCache);
					eoUnparser.unparseSearchCondition(comp);
				}
			}
		}

		Integer autoNumber = (Integer) evo.getFieldValue(metaFieldVO.getUID());

		if (autoNumber == null) {
			final List<Integer> rows = dataBaseHelper.getDbAccess().executeQuery(query);

			Integer lastAutoNumber = 1;
			if (!rows.isEmpty()) {
				lastAutoNumber = rows.iterator().next();
				if (null != lastAutoNumber) {
					++lastAutoNumber;
				} else {
					lastAutoNumber = 1;
				}
			}

			return lastAutoNumber;
		} else {
			return autoNumber;
		}
	}



}
