package org.nuclos.server.businesstest.codegeneration;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.nuclos.common.UID;
import org.nuclos.common2.IOUtils;
import org.nuclos.server.businesstest.codegeneration.source.BusinessTestEntitySource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import groovy.lang.GroovyClassLoader;

/**
 * Compiles the business test class sources.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class BusinessTestClassLoader {
	private static final Logger LOG = LoggerFactory.getLogger(BusinessTestClassLoaderBean.class);

	private final File outputDir;
	private GroovyClassLoader classLoader;
	private Map<UID, BusinessTestEntitySource> sources;

	public BusinessTestClassLoader(final File outputDir) {
		this.outputDir = outputDir;
		classLoader = new GroovyClassLoader();
	}

	/**
	 * Compiles the given Groovy sources and returns the corresponding ClassLoader.
	 * Only recompiles if the sources have changed.
	 */
	public GroovyClassLoader compileSources(final Map<UID, BusinessTestEntitySource> sources) throws IOException {
		if (this.sources != null && this.sources.equals(sources)) {
			return classLoader;
		}

		this.sources = sources;

		classLoader = new GroovyClassLoader();
		final Set<File> classFiles = new HashSet<>();

		if (outputDir.exists()) {
			FileUtils.forceDelete(outputDir);
		}

		for (Map.Entry<UID, BusinessTestEntitySource> classEntry : sources.entrySet()) {
			final BusinessTestEntitySource source = classEntry.getValue();

			final String pathName = source.getPkg().replaceAll("\\.", "/");
			final String fileName = source.getClassName() + ".groovy";
			final File file = new File(outputDir, pathName + "/" + fileName);

			if (!file.getParentFile().exists()) {
				if (!file.getParentFile().mkdirs()) {
					LOG.warn("Could not create parent dir for {}", file);
				}
			}

			IOUtils.writeToTextFile(file, source.getGroovySource(), "UTF-8");
			classFiles.add(file);
		}

		classLoader.addClasspath(outputDir.getPath());

		for (File classFile : classFiles) {
			classLoader.parseClass(classFile);
		}

		return classLoader;
	}
}
