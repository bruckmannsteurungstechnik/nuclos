package org.nuclos.server.businesstest.codegeneration;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.nuclos.common.UID;
import org.nuclos.server.businesstest.codegeneration.source.BusinessTestEntitySource;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import groovy.lang.GroovyClassLoader;

/**
 * Compiles the business test class sources.
 *
 * @author Andreas Lämmlein <andreas@laemm-line.de>
 */
@Component
public class BusinessTestClassLoaderBean {
	private static final Logger LOG = LoggerFactory.getLogger(BusinessTestClassLoaderBean.class);

	private final BusinessTestClassLoader businessTestClassLoader;

	public BusinessTestClassLoaderBean() {
		final File outputDir = NuclosCodegeneratorConstants.BUSINESS_TEST_SRC_MAIN_FOLDER;
		businessTestClassLoader = new BusinessTestClassLoader(outputDir);
	}

	public GroovyClassLoader compileSources(final Map<UID, BusinessTestEntitySource> sources) throws IOException {
		return businessTestClassLoader.compileSources(sources);
	}
}
