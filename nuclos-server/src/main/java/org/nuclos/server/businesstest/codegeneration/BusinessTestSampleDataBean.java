package org.nuclos.server.businesstest.codegeneration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.nuclos.common.FieldMeta;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.businesstest.sampledata.SampleDataGenerator;
import org.nuclos.server.businesstest.sampledata.SampleDataGeneratorFactory;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeBean;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Service
public class BusinessTestSampleDataBean implements IBusinessTestSampleDataProvider {

	@Qualifier("masterDataService")
	private final MasterDataFacadeBean masterDataFacade;

	public BusinessTestSampleDataBean(
			@Qualifier("masterDataService") final MasterDataFacadeBean masterDataFacade
	) {
		this.masterDataFacade = masterDataFacade;
	}

	@Override
	public <T> T getSampleData(final FieldMeta<T> field) {
		return getSampleData(field, 1).iterator().next();
	}

	@Override
	public <T> List<T> getSampleData(final FieldMeta<T> field, final int count) {
		final List<T> result;

		try {
			final SampleDataGenerator<T> generator = SampleDataGeneratorFactory.newDataGeneratorFor(field.getJavaClass());

			if (generator == null) {
				throw new IllegalArgumentException("Not data generator available for field type " + field.getJavaClass());
			}


			final List<MasterDataVO<Object>> data = fetchExistingData(field);
			final Collection<T> values = collectValues(field, data);
			generator.addSampleValues(values);

			result = generateValues(generator, count);

			return result;
		} catch (IllegalAccessException | InstantiationException | CommonPermissionException e) {
			throw new RuntimeException(e);
		}
	}

	private <T> List<MasterDataVO<Object>> fetchExistingData(final FieldMeta<T> field) throws CommonPermissionException {
		ResultParams resultParams = new ResultParams(
				Collections.singletonList(field.getUID()),
				0L,
				99L,
				true
		); // TODO: Maybe this should be configurable
		return masterDataFacade.getMasterDataChunk(field.getEntity(), CollectableSearchExpression.TRUE_SEARCH_EXPR, resultParams);
	}

	private <T> List<T> generateValues(SampleDataGenerator<T> generator, int count) {
		final List<T> result = new ArrayList<>();

		for (int i = 0; i < count; i++) {
			result.add(generator.newValue());
		}

		return result;
	}

	private <T> List<T> collectValues(final FieldMeta<T> field, List<MasterDataVO<Object>> data) {
		final List<T> result = new ArrayList<>();

		for (MasterDataVO<?> mdvo : data) {
			T value = mdvo.getFieldValue(field.getUID(), field.getJavaClass());
			if (value != null) {
				result.add(mdvo.getFieldValue(field.getUID(), field.getJavaClass()));
			}
		}

		return result;
	}
}
