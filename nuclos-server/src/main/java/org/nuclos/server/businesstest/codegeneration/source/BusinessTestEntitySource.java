package org.nuclos.server.businesstest.codegeneration.source;

/**
 * Defines the Groovy source code of a business test entity class.
 *
 * @author Andreas Lämmlein <andreas@laemm-line.de>
 *
 */
public class BusinessTestEntitySource extends AbstractGroovyClassSource {

	public BusinessTestEntitySource(final String pkg, final String className) {
		super(pkg, className);
	}
}
