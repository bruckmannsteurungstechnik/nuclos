package org.nuclos.server.businesstest.codegeneration.source;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.apache.cxf.common.util.StringUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.businesstest.BusinessTestVO;
import org.nuclos.common.file.FileNameMapper;
import org.nuclos.server.businesstest.BusinessTestManagementBean;
import org.nuclos.server.businesstest.NucletVO;
import org.nuclos.server.businesstest.codegeneration.IBusinessTestNucletCache;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Watches the business test script directory and imports the script sources when modified.
 *
 * @author Andreas Lämmlein <andreas@laemm-line.de>
 */
@Component
public class BusinessTestScriptScannerBean {
	private static final Logger LOG = LoggerFactory.getLogger(BusinessTestScriptScannerBean.class);

	private final BusinessTestManagementBean managementBean;
	private final IBusinessTestNucletCache nucletCache;

	private final File outputDir;
	private final Pattern filePattern = Pattern.compile("(?i)^(.+?)\\.groovy$");    // *.groovy

	private boolean enabled = false;

	/**
	 * After the scanner is enabled again, we need to wait for a short cooldown time.
	 * This is because of the asynchronous file scanning: If the scanner is enabled immediately,
	 * it catches file events that happened when the scanner was actually disabled.
	 */
	private Date cooldownUntil = new Date();

	public BusinessTestScriptScannerBean(
			final BusinessTestManagementBean managementBean,
			final IBusinessTestNucletCache nucletCache
	) {
		this.managementBean = managementBean;
		this.nucletCache = nucletCache;
		outputDir = NuclosCodegeneratorConstants.BUSINESS_TEST_SRC_TEST_FOLDER;
	}

	@PostConstruct
	public void init() {

		// Setup monitoring
		try {
			if (!outputDir.exists()) {
				if (!outputDir.mkdirs()) {
					LOG.warn("Could not create output dir");
				}
			}

			final WatchDir watchDir = new WatchDir(outputDir.toPath(), true) {
				@Override
				protected void handleCreation(final Path child) {
					// TODO: Import new scripts?
				}

				@Override
				protected void handleDeletion(final Path child) {
					// TODO: Delete the script? Should only happen if creation works, too
				}

				@Override
				protected void handleModification(final Path child) {
					if (isReady()) {
						updateTestScript(child.toFile());
					}
				}
			};

			final BusinessTestWatchThread watchThread = SpringApplicationContextHolder.getBean(BusinessTestWatchThread.class);
			watchThread.setWatchDir(watchDir);
			watchThread.start();

			setEnabled(true);

			LOG.info("Business test scanner started");
		} catch (IOException e) {
			LOG.error("Failed to watch the business test script directory", e);
		}

	}

	/**
	 * Updates the corresponding test script to the given file.
	 */
	private void updateTestScript(@NotNull File file) {
		BusinessTestVO test = lookup(file);
		if (test == null) {
			LOG.warn("Unknown business test: " + file);
			return;
		}

		try {
			String contents = FileUtils.readFileToString(file, Charsets.UTF_8);
			test.setSource(contents);
			managementBean.modify(test);
			LOG.info("Successfully updated business test: " + test.getName());
		} catch (IOException e) {
			LOG.warn("Unable to read file: " + file, e);
		}
	}

	/**
	 * Looks up the corresponding script to the given file.
	 */
	private BusinessTestVO lookup(@NotNull final File file) {
		final Matcher m = filePattern.matcher(file.getName());
		if (!m.find()) {
			LOG.warn("File {} does not match the test script file pattern {}", file, filePattern);
			return null;
		}

		final String scriptName = FileNameMapper.unescape(m.group(1));

		final BusinessTestVO result;
		if (file.getParentFile().equals(outputDir)) {
			result = managementBean.get(scriptName);
		} else {
			String pkg = "";
			File currentFile = file.getParentFile();
			while (!currentFile.equals(outputDir)) {
				pkg = currentFile.getName() + (StringUtils.isEmpty(pkg) ? "" : "." + pkg);
				currentFile = currentFile.getParentFile();
			}
			NucletVO nuclet = nucletCache.getNucletByPackage(pkg);
			result = managementBean.get(nuclet.getUid(), scriptName);
		}

		return result;
	}

	public synchronized boolean isEnabled() {
		return enabled;
	}

	public synchronized void setEnabled(final boolean enabled) {
		if (!isEnabled()) {
			cooldownUntil = new Date(System.currentTimeMillis() + 1000);
		}
		this.enabled = enabled;
	}

	/**
	 * The source scanner is ready when it is enabled and not on cooldown.
	 */
	public boolean isReady() {
		return isEnabled() && (new Date().after(cooldownUntil));
	}
}
