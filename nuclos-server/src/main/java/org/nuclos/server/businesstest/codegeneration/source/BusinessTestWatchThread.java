package org.nuclos.server.businesstest.codegeneration.source;

import javax.validation.constraints.NotNull;

import org.nuclos.common.ParameterProvider;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.security.NuclosLocalServerSession;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Component
@Scope("prototype")
public class BusinessTestWatchThread extends Thread {

	private final NuclosLocalServerSession serverSession;

	private WatchDir watchDir = null;

	public BusinessTestWatchThread(final NuclosLocalServerSession serverSession) {
		this.serverSession = serverSession;
	}

	void setWatchDir(@NotNull final WatchDir watchDir) {
		this.watchDir = watchDir;
	}

	@Override
	public void run() {
		serverSession.loginAsUser(ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_TIMELIMIT_RULE_USER));
		watchDir.processEvents();
	}
}
