package org.nuclos.server.businesstest.execution;

import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.nuclos.common.E;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.businesstest.codegeneration.BusinessTestClassGenerator;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.ejb3.RestFacadeBean;
import org.nuclos.server.rest.misc.RestSQLParser;
import org.nuclos.server.security.SessionContext;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.statement.Statement;

/**
 * @author Andreas Lämmlein <andreas@laemm-line.de>
 */
@Service
@Lazy
public class BusinessTestRESTFacadeBean {
	private static final Logger LOG = LoggerFactory.getLogger(BusinessTestClassGenerator.class);

	private final RestFacadeBean restFacade;

	private SessionContext sessionContext = null;

	private BusinessTestRESTFacadeBean(final RestFacadeBean restFacade) {
		this.restFacade = restFacade;
	}

	public static BusinessTestRESTFacadeBean getInstance() {
		return SpringApplicationContextHolder.getBean(BusinessTestRESTFacadeBean.class);
	}

	public boolean login(String user, String password) {
		sessionContext = restFacade.webLogin(user, password, Locale.GERMANY, null, generateSessionID());
		return sessionContext.getAuthentication().isAuthenticated();
	}

	public boolean logout() {
		restFacade.webLogout(sessionContext.getJSessionId());
		return !sessionContext.getAuthentication().isAuthenticated();
	}

	public <T extends BusinessTestEO> T get(final UID entityUID, final Class<T> cls, final Long boId) throws CommonPermissionException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
		EntityObjectVO<Long> eo = restFacade.get(entityUID, boId, false);

		if (eo == null) {
			return null;
		}

		return BusinessTestEO.from(eo, cls);
	}

	Collection<EntityObjectVO<Long>> getDependentEOs(final UID dependentUID, final UID dependentFieldUID, final Long parentId) {
		return restFacade.getDependentData(dependentUID, dependentFieldUID, parentId);
	}

	public BusinessTestEO reload(BusinessTestEO bo) throws CommonPermissionException {
		EntityObjectVO<Long> eo = restFacade.get(bo.getEntityUID(), bo.getId(), false);
		bo.setWrappedEO(eo);
		return bo;
	}

	/**
	 * Runs the given query for the given entity and returns a List of objects of the given class.
	 * Limits the results, if limit > 0. Else a default limit of 1000 is used.
	 */
	public <T extends BusinessTestEO> List<T> query(
			final UID entityUID,
			final Class<T> cls,
			final String whereCondition,
			final int limit
	) throws JSQLParserException, IllegalAccessException, InstantiationException, CommonPermissionException {

		final CollectableSearchCondition condition = parseQuery(entityUID, whereCondition);
		final int offset = 0;

		return getChunk(
				entityUID,
				cls,
				new CollectableSearchExpression(condition),
				offset,
				limit
		);
	}

	private CollectableSearchCondition parseQuery(final UID entityUID, final String whereCondition) throws JSQLParserException {
		Statement statement = new CCJSqlParserManager().parse(new StringReader(
				"SELECT * FROM " + Rest.translateUid(E.ENTITY, entityUID) + " WHERE + " + whereCondition));
		RestSQLParser parser = new RestSQLParser();
		statement.accept(parser);
		return parser.getCondition();
	}

	@Transactional(propagation = Propagation.NESTED, noRollbackFor = Throwable.class)
	public boolean save(BusinessTestEO<?> bo, String executeCustomRule) throws CommonBusinessException {
		EntityObjectVO<Long> eo = restFacade.fireCustomEventSupportWithoutRollback(bo.getWrappedEO(), executeCustomRule);
		bo.setWrappedEO(eo);
		return save(bo);
	}

	@Transactional(propagation = Propagation.NESTED, noRollbackFor = Throwable.class)
	public boolean save(BusinessTestEO bo) throws CommonBusinessException {
		EntityObjectVO<Long> eo = bo.getWrappedEO();
		if (eo.isFlagNew()) {
			eo = restFacade.insertWithoutRollback(eo);
		} else {
			eo = restFacade.updateWithoutRollback(eo);
		}
		bo.setWrappedEO(eo);
		return true;
	}

	public boolean changeState(BusinessTestEO bo, int state) throws CommonBusinessException {
		EntityObjectVO<Long> eo = bo.getWrappedEO();
		boolean result = changeState(eo.getDalEntity(), eo.getPrimaryKey(), state);

		eo = restFacade.get(eo.getDalEntity(), eo.getPrimaryKey(), false);
		bo.setWrappedEO(eo);

		return result;
	}

	public boolean changeState(UID entityUID, Long boId, int state) throws CommonBusinessException {
		UID stateId = getStateUID(entityUID, boId, state);
		if (stateId == null) {
			return false;
		}
		restFacade.changeStateWithoutRollback(entityUID, boId, stateId);
		return false;
	}

	public boolean delete(BusinessTestEO bo) throws CommonBusinessException {
		restFacade.deleteWithoutRollback(bo.getEntityUID(), bo.getId(), false);
		return true;
	}

	private String generateSessionID() {
		return "TESTSESSION_" + new SecureRandom().nextInt();
	}

	private UID getStateUID(UID entityUID, Long boId, int stateNumeral) {
		List<StateVO> states = Rest.getSubsequentStates(entityUID, boId);
		for (StateVO state : states) {
			if (state.getNumeral() == stateNumeral) {
				return state.getId();
			}
		}
		return null;
	}

	public <T extends BusinessTestEO> BusinessTestEO<T> executeCustomRule(
			final BusinessTestEO<T> testEO,
			final String customRule
	) throws CommonBusinessException {
		EntityObjectVO entityObjectVO = restFacade.fireCustomEventSupportWithoutRollback(testEO.getWrappedEO(), customRule);
		testEO.setWrappedEO(entityObjectVO);
		return testEO;
	}

	public <T extends BusinessTestEO> List<T> list(
			final UID entityUID,
			final Class<T> cls,
			final int limit
	) throws InstantiationException, IllegalAccessException, CommonPermissionException {
		return getChunk(entityUID, cls, CollectableSearchExpression.TRUE_SEARCH_EXPR, 0, limit);
	}

	/**
	 * Loads a chunk of records.
	 */
	private <T extends BusinessTestEO> List<T> getChunk(
			final UID entityUID,
			final Class<T> cls,
			final CollectableSearchExpression search,
			final int offset,
			final int limit
	) throws IllegalAccessException, InstantiationException, CommonPermissionException {
		ResultParams resultParams = new ResultParams(
				null,
				(long) offset,
				(long) limit,
				true
		);
		Collection<EntityObjectVO<Long>> entityObjects = restFacade.getEntityObjectsChunkWithoutRollback(
				entityUID,
				search,
				resultParams
		);

		final List<T> result = new ArrayList<>();
		for (EntityObjectVO<Long> eo : entityObjects) {
			final T bo = cls.newInstance();
			bo.setWrappedEO(eo);
			result.add(bo);
		}

		return result;
	}
}
