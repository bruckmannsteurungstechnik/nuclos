package org.nuclos.server.businesstest.sampledata;

import java.util.Collection;
import java.util.Random;

/**
 * Generates a new Boolean value, with the probability of the value being true, false or null
 * based on the respective distribution in the input values.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class SampleBooleanDataGenerator extends SampleDataGenerator<Boolean> {
	private final Random random = new Random();

	private int countTrue;
	private int countFalse;
	private int countNull;

	SampleBooleanDataGenerator() {
		super(Boolean.class);
	}

	@Override
	public void addSampleValues(final Collection<Boolean> values) {
		for (Boolean value : values) {
			if (value == null) {
				countNull++;
			} else if (value) {
				countTrue++;
			} else {
				countFalse++;
			}
		}
	}

	@Override
	public Boolean newValue() {
		int total = countTrue + countFalse + countNull;

		if (total <= 0) {
			return null;
		}

		int x = random.nextInt(total) + 1;

		if (x <= countTrue) {
			return true;
		} else if (x <= countTrue + countFalse) {
			return false;
		}

		return null;
	}
}
