//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.cluster;

import java.io.Serializable;
import java.util.Date;

import org.nuclos.common2.LangUtils;

public class ClusterNode implements Serializable {
	
	private String name;
	private String servername;
	private String serverip;
	private String port;
	private String context;
	private boolean master;
	
	private Date datRegistered;
	
	private boolean connected;

	public ClusterNode(String name, String servername, String serverip,	String port, String context, boolean master, Date datRegistered) {
		super();
		this.name = name;
		this.servername = servername;
		this.serverip = serverip;
		this.port = port;
		this.context = context;
		this.master = master;
		this.datRegistered = datRegistered;
		this.connected = false;
	}
	
	public String getJmsUrl() {
		StringBuilder sb = new StringBuilder();
		sb.append("http://");
		sb.append(getServerip());
		sb.append(":");
		sb.append(getPort());
		sb.append(getContext());
		sb.append("/jmsbroker");
		
		return sb.toString();
	}

	public String getName() {
		return name;
	}

	public String getServername() {
		return servername;
	}

	public String getServerip() {
		return serverip;
	}

	public String getPort() {
		return port;
	}

	public String getContext() {
		return context;
	}

	public boolean isMaster() {
		return master;
	}

	public Date getDatRegistered() {
		return datRegistered;
	}

	@Override
	public int hashCode() {		
		return getServername().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof ClusterNode))
			return false;
		ClusterNode that = (ClusterNode)obj;
				
		return LangUtils.equal(that.getServername(), this.getServername());
	}

	public boolean isConnected() {
		return connected;
	}

	public void setConnected(boolean connected) {
		this.connected = connected;
	}

}
