//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.server.cluster;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.TimerTask;

import org.apache.commons.net.echo.EchoTCPClient;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.server.cluster.cache.ClusterNodeCache;
import org.nuclos.server.cluster.jms.ClusterActionFactory;
import org.nuclos.server.cluster.jms.ClusterTopicNotificationReceiver;
import org.nuclos.server.cluster.jms.NuclosClusterAction;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * CLUSTERING
 * class "ping" the other cluster node's
 * if one is not reachable take out of db
 * and look for a new master
 */
public class ClusterTimerTask extends TimerTask {
	
	public static final Logger LOG = LoggerFactory.getLogger(ClusterTimerTask.class);

	@Override
	public void run() {
		ClusterNodeCache nodeCache = SpringApplicationContextHolder.getBean(ClusterNodeCache.class);
		nodeCache.invalidate();

		ClusterTopicNotificationReceiver receiver = SpringApplicationContextHolder.getBean(ClusterTopicNotificationReceiver.class);
		for(ClusterNode node : nodeCache.getNotConnectedNodes()) {
			receiver.topicRegister(node);
		}
		
		checkClusterNodesDB();
	}
	
	private void checkClusterNodesDB() {
		
		for(ClusterNode node : NuclosClusterRegisterHelper.getAllNodes()) {
			
			String sServer = node.getServerip();
			String sPort = node.getPort();
			boolean okay = true;
			try {
				EchoTCPClient client = new EchoTCPClient();
				client.setDefaultPort(Integer.parseInt(sPort));
				client.setDefaultTimeout(3000);
				client.connect(sServer);
				client.disconnect();
				
			} catch (NumberFormatException e) {
				LOG.info("Port is not a number");
			} catch (UnknownHostException e) {
				LOG.info("Server is not reachable!");
				okay = false;
			} catch (IOException e) {
				LOG.info("Server is not reachable!");
				okay = false;
			}
			
			if(!okay) {
				NuclosClusterRegisterHelper.removeClusterServer(node);
				ClusterNodeCache nodeCache = SpringApplicationContextHolder
					.getBean(ClusterNodeCache.class);
				nodeCache.removeNode(node);
				if(node.isMaster()) {
					ClusterNode newMasterNode = nodeCache.getNewMasterNode();					
					if(newMasterNode.getServerip().equals(NuclosClusterRegisterHelper.getServerIp())) {
						NuclosClusterAction action = ClusterActionFactory
							.createClusterAction(NuclosClusterAction.Type.RAISETOMASTER_ACTION, newMasterNode, true);
						action.doAction();
					}
					else {						
						NuclosClusterAction action = ClusterActionFactory
							.createClusterAction(NuclosClusterAction.Type.RAISETOMASTER_ACTION, newMasterNode, false);
						NuclosJMSUtils.sendObjectMessage(action, JMSConstants.TOPICNAME_CLUSTER, null);
					}
				}
				
			}
			
		}
		
	}

}
