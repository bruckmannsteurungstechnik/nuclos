//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.cluster;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.Query;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common.E;
import org.nuclos.common.E._ClusterServer;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SF;
import org.nuclos.common.SimpleDbField;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.server.cluster.jms.ClusterActionFactory;
import org.nuclos.server.cluster.jms.NuclosClusterAction;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.ServerProperties;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbAccess;
import org.nuclos.server.dblayer.DbStatementUtils;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.statements.DbDeleteStatement;
import org.nuclos.server.dblayer.statements.DbInsertStatement;
import org.nuclos.server.dblayer.statements.DbMap;
import org.nuclos.server.dblayer.statements.DbUpdateStatement;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public final class NuclosClusterRegisterHelper {

	private static final Logger LOG = LoggerFactory.getLogger(NuclosClusterRegisterHelper.class);

	private static final String XML_ROOT_TAG = "nuclos";

	private static Map<String, String> config;

	private NuclosClusterRegisterHelper() {
	}

	public static void registerClusterServer(boolean autosetup) {

		try {

			final String serverName = getServerName();
			final String serverIp = getServerIp();
			final String serverPort = getRealHttpPort();

			LOG.info("Register: {}/{}", serverName, serverIp);

			String sContextPath = "";
			if (!autosetup) {
				WebApplicationContext con = (WebApplicationContext) SpringApplicationContextHolder.getApplicationContext();
				sContextPath = con.getServletContext().getContextPath();
			}

			DbAccess dbAccess = SpringDataBaseHelper.getInstance().getDbAccess();

			if (!isAnyServerRegistered()) {
				LOG.debug("No Server found! This is master!");
				DbInsertStatement<?> insert = getInsertStatement(true, autosetup, serverName, serverIp, serverPort, sContextPath);
				dbAccess.execute(insert);
			} else {
				if (isAlreadyRegistered(serverName, serverIp, serverPort)) {
					LOG.debug("Server found! Update this slave!");
					executeUpdateStatement(
							serverName,
							serverIp,
							serverPort,
							dbAccess,
							sContextPath
					);

				} else {
					LOG.debug("Server found! This is slave!");
					DbInsertStatement<?> insert = getInsertStatement(
							false,
							autosetup,
							serverName,
							serverIp,
							serverPort,
							sContextPath
					);
					dbAccess.execute(insert);
				}
			}

		} catch (Exception e) {
			throw new NuclosFatalException(e);
		}

	}

	public static String getServerIp() {
		return StringUtils.defaultIfEmpty(
				getConfig().get("ip"),
				"127.0.0.1"
		);
	}

	public static String getServerName() {
		String hostname;
		try {
			hostname = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			throw new NuclosFatalException(e);
		}

		return hostname;
	}

	public static Collection<ClusterNode> getAllNodes() {
		EntityMeta<UID> clusterMeta = MetaProvider.getInstance().getEntity(_ClusterServer.UID);
		Collection<ClusterNode> col = new ArrayList<>();

		DbAccess dbAccess = SpringDataBaseHelper.getInstance().getDbAccess();
		DbQueryBuilder builder = dbAccess.getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom<UID> from = query.from(clusterMeta);
		List<DbExpression<?>> selections = new ArrayList<>();
		selections.add(from.baseColumn(E.CLUSTERSERVER.serverName));
		selections.add(from.baseColumn(E.CLUSTERSERVER.serverIP));
		selections.add(from.baseColumn(E.CLUSTERSERVER.port));
		selections.add(from.baseColumn(E.CLUSTERSERVER.serverContext));
		selections.add(from.baseColumn(E.CLUSTERSERVER.master));
		selections.add(from.baseColumn(E.CLUSTERSERVER.register));
		query.multiselect(selections);

		List<DbTuple> lstResult = dbAccess.executeQuery(query);
		for (DbTuple dbt : lstResult) {
			ClusterNode node = new ClusterNode((String) dbt.get(0), (String) dbt.get(0), (String) dbt.get(1), (String) dbt.get(2),
					(String) dbt.get(3), (Boolean) dbt.get(4), (Date) dbt.get(5));
			col.add(node);
		}


		return col;
	}

	public static Collection<ClusterNode> getAllOtherNodes() {
		Collection<ClusterNode> col = getAllNodes();
		final String sHost = getServerName();

		return CollectionUtils.select(col, new Predicate<ClusterNode>() {

			@Override
			public boolean evaluate(ClusterNode t) {
				return !t.getServername().equals(sHost);
			}

		});
	}

	private static String getPort() {
		String port = getRealHttpPort();

		if (StringUtils.isBlank(port)) {
			port = getConfiguredHttpPort();
		}

		return port;
	}

	/**
	 * Tries to determine the actual HTTP port via JMX.
	 */
	private static String getRealHttpPort() {
		MBeanServer beanServer = ManagementFactory.getPlatformMBeanServer();

		try {
			Set<ObjectName> objectNames = beanServer.queryNames(
					new ObjectName("*:type=Connector,*"),
					Query.match(Query.attr("protocol"), Query.value("HTTP/1.1"))
			);

			return objectNames.iterator().next().getKeyProperty("port");
		} catch (MalformedObjectNameException e) {
			LOG.warn("Could not determine real HTTP port");
		}

		return null;
	}

	/**
	 * Returns the configured HTTP port (server properties).
	 */
	private static String getConfiguredHttpPort() {
		String port = "8080";

		try {

			Map<String, String> config = new HashMap<>();
			for (Map.Entry<?, ?> p : ServerProperties.loadProperties(ServerProperties.JNDI_SERVER_PROPERTIES).entrySet()) {
				if (p.getKey().toString().startsWith("nuclos.data.database-structure-changes."))
					config.put(p.getKey().toString().substring(39), p.getValue().toString());
			}

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			String sValue = config.get("path");
			int index = sValue.indexOf("logs");
			sValue = sValue.substring(0, index);
			DocumentBuilder documentBuilder = factory.newDocumentBuilder();
			Document document = documentBuilder.parse(new File(sValue + "nuclos.xml"));

			Properties xmlProperties = new Properties();
			Element root = document.getDocumentElement();
			if (!XML_ROOT_TAG.equals(root.getTagName())) {
				throw new IOException("Root element '" + XML_ROOT_TAG + "' required");
			}

			Node rootChild = root.getFirstChild();
			while (rootChild != null) {
				if (rootChild.getNodeType() == Node.ELEMENT_NODE) {
					readElementRecursive(xmlProperties, "", (Element) rootChild);
				}
				rootChild = rootChild.getNextSibling();
			}

			if (xmlProperties.containsKey("server.http.port")) {
				port = xmlProperties.getProperty("server.http.port");
			}

		} catch (FileNotFoundException e) {
			LOG.info("Could not open configuration file nuclos.xml, assuming standard port: {}", port);
			LOG.debug(e.getMessage(), e);
		} catch (ParserConfigurationException | IOException | SAXException e) {
			throw new RuntimeException(e);
		}
		return port;
	}

	private static void readElementRecursive(Properties xmlProperties, String prefix, Element e) {
		if (prefix == null || prefix.isEmpty()) {
			prefix = e.getNodeName();
		} else {
			prefix = prefix + "." + e.getNodeName().replace('-', '.');
		}

		NodeList children = e.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			Node child = children.item(i);
			if (child.getNodeType() == Element.TEXT_NODE) {
				String value = child.getTextContent();
				if (xmlProperties.containsKey(prefix)) {
					String oldValue = xmlProperties.getProperty(prefix);
					value = value + System.getProperty("line.separator") + oldValue;
				}
				xmlProperties.put(prefix, value.trim());
			} else if (child.getNodeType() == Element.ELEMENT_NODE) {
				readElementRecursive(xmlProperties, prefix, (Element) child);
			}
		}
	}

	public static boolean runInClusterMode() {
		return StringUtils.equals(
				getConfig().get("mode"),
				"true"
		);
	}

	private static Map<String, String> getConfig() {
		if (config == null) {
			config = new ConcurrentHashMap<>();
			for (Map.Entry<?, ?> p : ServerProperties.loadProperties(ServerProperties.JNDI_SERVER_PROPERTIES).entrySet()) {
				if (p.getKey().toString().startsWith("cluster.")) {
					config.put(p.getKey().toString().substring(8), p.getValue().toString());
				}
			}
		}

		return config;
	}

	public static boolean isAnyServerRegistered() {
		EntityMeta<UID> clusterMeta = MetaProvider.getInstance().getEntity(_ClusterServer.UID);
		DbAccess dbAccess = SpringDataBaseHelper.getInstance().getDbAccess();

		DbQueryBuilder builder = dbAccess.getQueryBuilder();
		DbQuery<String> query = builder.createQuery(String.class);
		DbFrom<UID> from = query.from(clusterMeta);
		query.select(from.baseColumn(SimpleDbField.create("STRUID", String.class)));

		List<String> lstResult = dbAccess.executeQuery(query);
		return !lstResult.isEmpty();
	}

	public static boolean isAutosetupRunning() {
		EntityMeta<UID> clusterMeta = MetaProvider.getInstance().getEntity(_ClusterServer.UID);
		DbAccess dbAccess = SpringDataBaseHelper.getInstance().getDbAccess();

		DbQueryBuilder builder = dbAccess.getQueryBuilder();
		DbQuery<String> query = builder.createQuery(String.class);
		DbFrom<UID> from = query.from(clusterMeta);
		query.select(from.baseColumn(SimpleDbField.create("STRUID", String.class)));

		query.where(builder.equalValue(from.baseColumn(E.CLUSTERSERVER.autoSetupRunning), Boolean.TRUE));

		List<String> lstMaster = dbAccess.executeQuery(query);
		if (lstMaster.isEmpty())
			return false;

		return true;
	}

	private static void executeUpdateStatement(
			final String serverName,
			final String serverIp,
			final String serverPort,
			final DbAccess dbAccess,
			final String sContext,
			final boolean bMaster
	) {
		EntityMeta<UID> clusterMeta = MetaProvider.getInstance().getEntity(_ClusterServer.UID);

		DbMap mpSetValues = new DbMap();
		mpSetValues.put(E.CLUSTERSERVER.register, new Timestamp(System.currentTimeMillis()));
		mpSetValues.put(E.CLUSTERSERVER.master, bMaster);
		mpSetValues.put(E.CLUSTERSERVER.serverContext, sContext);

		DbMap mpCondition = new DbMap();
		mpCondition.put(E.CLUSTERSERVER.serverName, serverName);
		mpCondition.put(E.CLUSTERSERVER.serverIP, serverIp);
		mpCondition.put(E.CLUSTERSERVER.port, serverPort);

		DbUpdateStatement<?> update = new DbUpdateStatement(clusterMeta, mpSetValues, mpCondition);
		dbAccess.execute(update);
	}

	private static void executeUpdateStatement(
			final String serverName,
			final String serverIp,
			final String serverPort,
			final DbAccess dbAccess,
			final String sContext
	) {
		executeUpdateStatement(
				serverName,
				serverIp,
				serverPort,
				dbAccess,
				sContext,
				Boolean.FALSE
		);
	}

	public static void upliftClusterServerToMaster(
			final String serverName,
			final String serverIp,
			final String serverPort
	) {
		boolean registered = isAlreadyRegistered(serverName, serverIp, serverPort);
		if (!registered) {
			throw new NuclosClusterExecption("Server: " + serverName + " is not registered");
		}

		WebApplicationContext con = (WebApplicationContext) SpringApplicationContextHolder.getApplicationContext();
		String sContextPath = con.getServletContext().getContextPath();

		executeUpdateStatement(
				serverName,
				serverIp,
				serverPort,
				SpringDataBaseHelper.getInstance().getDbAccess(),
				sContextPath,
				true
		);

	}

	private static DbInsertStatement getInsertStatement(boolean master, boolean autosetup, String hostname, String hostip, String port, String sContext) {

		Collection<Object> args = new ArrayList<>(Arrays.asList(
				E.CLUSTERSERVER.autoSetupRunning, autosetup,
				E.CLUSTERSERVER.register, new Timestamp(System.currentTimeMillis()),
				E.CLUSTERSERVER.serverName, hostname,
				E.CLUSTERSERVER.serverIP, hostip,
				E.CLUSTERSERVER.port, port,
				SF.PK_UID, new UID(),
				SF.CREATEDAT, new Timestamp(System.currentTimeMillis()),
				SF.CREATEDBY, "CLUSTER",
				SF.CHANGEDAT, new Timestamp(System.currentTimeMillis()),
				SF.CHANGEDBY, "CLUSTER",
				SF.VERSION, 1
		));
		if (!autosetup) {
			args.add(E.CLUSTERSERVER.serverContext);
			args.add(sContext);
		}
		return DbStatementUtils.insertInto(E.CLUSTERSERVER, E.CLUSTERSERVER.master, master, args.toArray());
	}

	public static boolean iAmSlave() {
		String hostname;
		try {
			hostname = InetAddress.getLocalHost().getHostName();
			String hostip = InetAddress.getLocalHost().getHostAddress();
			return !isMaster(hostname, hostip);
		} catch (UnknownHostException e) {
			throw new NuclosFatalException(e);
		}

	}

	private static boolean isMaster(String sHostName, String sHostIp) {
		EntityMeta<UID> clusterMeta = MetaProvider.getInstance().getEntity(_ClusterServer.UID);
		DbAccess dbAccess = SpringDataBaseHelper.getInstance().getDbAccess();

		DbQueryBuilder builder = dbAccess.getQueryBuilder();
		DbQuery<Boolean> query = builder.createQuery(Boolean.class);
		DbFrom<UID> from = query.from(clusterMeta);
		query.select(from.baseColumn(E.CLUSTERSERVER.master));
		query.where(builder.equalValue(from.baseColumn(E.CLUSTERSERVER.serverIP), sHostIp));
		query.addToWhereAsAnd(builder.equalValue(from.baseColumn(E.CLUSTERSERVER.master), Boolean.TRUE));

		List<Boolean> lstMaster = dbAccess.executeQuery(query);
		if (lstMaster.isEmpty()) {
			return false;
		}

		return lstMaster.iterator().next();

	}

	private static boolean isAlreadyRegistered(
			final String serverName,
			final String serverIp,
			final String serverPort
	) {
		EntityMeta<UID> clusterMeta = MetaProvider.getInstance().getEntity(_ClusterServer.UID);

		DbAccess dbAccess = SpringDataBaseHelper.getInstance().getDbAccess();

		DbQueryBuilder builder = dbAccess.getQueryBuilder();
		DbQuery<String> query = builder.createQuery(String.class);
		DbFrom<UID> from = query.from(clusterMeta);
		query.select(from.baseColumn(SimpleDbField.create("STRUID", String.class)));
		query.where(
				builder.and(
						builder.equalValue(from.baseColumn(E.CLUSTERSERVER.serverName), serverName),
						builder.equalValue(from.baseColumn(E.CLUSTERSERVER.serverIP), serverIp),
						builder.equalValue(from.baseColumn(E.CLUSTERSERVER.port), serverPort)
				)
		);

		return !dbAccess.executeQuery(query).isEmpty();
	}

	public static void deregisterAutosetup() {
		try {
			EntityMeta<UID> clusterMeta = MetaProvider.getInstance().getEntity(_ClusterServer.UID);
			String hostname = InetAddress.getLocalHost().getHostName();
			String hostip = InetAddress.getLocalHost().getHostAddress();

			DbMap mpSetValues = new DbMap();
			mpSetValues.put(E.CLUSTERSERVER.register, new Timestamp(System.currentTimeMillis()));
			mpSetValues.put(E.CLUSTERSERVER.autoSetupRunning, Boolean.FALSE);

			DbMap mpCondition = new DbMap();
			mpCondition.put(E.CLUSTERSERVER.serverName, hostname);
			mpCondition.put(E.CLUSTERSERVER.serverIP, hostip);

			DbAccess dbAccess = SpringDataBaseHelper.getInstance().getDbAccess();
			DbUpdateStatement<?> update = new DbUpdateStatement(clusterMeta, mpSetValues, mpCondition);
			dbAccess.execute(update);
		} catch (UnknownHostException e) {
			throw new NuclosFatalException(e);
		}
	}

	public static String getOwnJmsUrl() {
		StringBuilder sb = new StringBuilder("http://");
		try {
			WebApplicationContext con = (WebApplicationContext) SpringApplicationContextHolder.getApplicationContext();
			String sContextPath = con.getServletContext().getContextPath();
			String hostname = InetAddress.getLocalHost().getHostName();
			String hostip = InetAddress.getLocalHost().getHostAddress();
			String server = hostname;
			if (server == null || server.length() == 0) {
				server = hostip;
			}
			sb.append(server);
			sb.append(":");
			sb.append(getPort());
			sb.append("/");
			sb.append(sContextPath);
			sb.append("/jmsbroker");


		} catch (UnknownHostException e) {
			throw new NuclosFatalException(e);
		}


		return sb.toString();
	}

	public static String getMasterServerUrl() {
		EntityMeta<UID> clusterMeta = MetaProvider.getInstance().getEntity(_ClusterServer.UID);
		StringBuilder sb = new StringBuilder("http://");

		DbAccess dbAccess = SpringDataBaseHelper.getInstance().getDbAccess();
		DbQueryBuilder builder = dbAccess.getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom<UID> from = query.from(clusterMeta);

		List<DbExpression<?>> selections = new ArrayList<>();
		selections.add(from.baseColumn(E.CLUSTERSERVER.serverName));
		selections.add(from.baseColumn(E.CLUSTERSERVER.port));
		selections.add(from.baseColumn(E.CLUSTERSERVER.serverContext));
		selections.add(from.baseColumn(E.CLUSTERSERVER.serverName));
		query.multiselect(selections);
		query.where(builder.equalValue(from.baseColumn(E.CLUSTERSERVER.master), Boolean.TRUE));

		List<DbTuple> lstResult = dbAccess.executeQuery(query);
		DbTuple tuple = lstResult.get(0);

		String server = (String) tuple.get(3);
		if (server == null || server.length() == 0) {
			server = (String) tuple.get(0);
		}

		sb.append(server);
		sb.append(":");
		sb.append(tuple.get(1));
		sb.append("/");
		sb.append(tuple.get(2));
		sb.append("/remoting/");

		return sb.toString();
	}

	public static Collection<String> getServerUrlsForJMS() {
		EntityMeta<UID> clusterMeta = MetaProvider.getInstance().getEntity(_ClusterServer.UID);
		Collection<String> urls = new ArrayList<String>();

		DbAccess dbAccess = SpringDataBaseHelper.getInstance().getDbAccess();
		DbQueryBuilder builder = dbAccess.getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom<UID> from = query.from(clusterMeta);

		List<DbExpression<?>> selections = new ArrayList<>();
		selections.add(from.baseColumn(E.CLUSTERSERVER.serverIP));
		selections.add(from.baseColumn(E.CLUSTERSERVER.port));
		selections.add(from.baseColumn(E.CLUSTERSERVER.serverContext));
		selections.add(from.baseColumn(E.CLUSTERSERVER.serverName));
		query.multiselect(selections);

		try {
			String hostip = InetAddress.getLocalHost().getHostAddress();
			String hostname = InetAddress.getLocalHost().getHostName();

			List<DbTuple> lstResult = dbAccess.executeQuery(query);
			for (DbTuple dbt : lstResult) {
				StringBuilder sb = new StringBuilder("http://");
				String server = (String) dbt.get(3);
				if (server == null || server.length() == 0) {
					server = (String) dbt.get(0);
				}

				sb.append(server);
				sb.append(":");
				sb.append(dbt.get(1));
				sb.append("/");
				sb.append(dbt.get(2));
				sb.append("/jmsbroker");

				if (hostname.equals(dbt.get(0)))
					continue;

				urls.add(sb.toString());
			}

		} catch (UnknownHostException e) {
			// ignore ???
		}


		return urls;
	}

	public static void removeClusterServer(ClusterNode node) {
		EntityMeta<UID> clusterMeta = MetaProvider.getInstance().getEntity(_ClusterServer.UID);
		DbAccess dbAccess = SpringDataBaseHelper.getInstance().getDbAccess();

		DbQueryBuilder builder = dbAccess.getQueryBuilder();
		DbQuery<String> query = builder.createQuery(String.class);
		DbFrom<UID> from = query.from(clusterMeta);
		query.select(from.baseColumn(SimpleDbField.create("STRUID", String.class)));

		query.where(
				builder.and(
						builder.equalValue(from.baseColumn(E.CLUSTERSERVER.serverName), node.getServername()),
						builder.equalValue(from.baseColumn(E.CLUSTERSERVER.serverIP), node.getServerip()),
						builder.equalValue(from.baseColumn(E.CLUSTERSERVER.port), node.getPort())
				)
		);

		List<String> lstNode = dbAccess.executeQuery(query);

		for (String iNode : lstNode) {
			DbMap conditions = new DbMap();
			conditions.put(SimpleDbField.create("STRUID", String.class), iNode);
			DbDeleteStatement<?> delete = new DbDeleteStatement<>(clusterMeta, conditions);
			dbAccess.execute(delete);
		}

	}

	public static void deregisterClusterServer() {
		final String hostname = getServerName();
		final String hostip = getServerIp();
		final String port = getPort();

		LOG.info("Deregister: {}/{}", hostname, hostip);

		final NuclosClusterAction action = ClusterActionFactory.createClusterAction(NuclosClusterAction.Type.SHUTDOWN_ACTION, hostname);
		NuclosJMSUtils.sendObjectMessage(action, JMSConstants.TOPICNAME_CLUSTER, null);

		final DbDeleteStatement<?> delete = DbStatementUtils.deleteFrom(
				E.CLUSTERSERVER,
				E.CLUSTERSERVER.serverIP, hostip,
				E.CLUSTERSERVER.serverName, hostname,
				E.CLUSTERSERVER.port, port
		);
		final DbAccess dbAccess = SpringDataBaseHelper.getInstance().getDbAccess();
		final int del = dbAccess.execute(delete);

		LOG.debug("deleted: {}", del);
	}

}
