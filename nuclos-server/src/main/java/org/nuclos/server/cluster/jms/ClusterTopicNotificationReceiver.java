//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.server.cluster.jms;

import java.util.Collection;
import java.util.Timer;
import java.util.TimerTask;

import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;

import org.apache.activemq.spring.ActiveMQConnectionFactory;
import org.nuclos.common.JMSConstants;
import org.nuclos.server.cluster.ClusterNode;
import org.nuclos.server.cluster.NuclosClusterRegisterHelper;
import org.nuclos.server.cluster.cache.ClusterNodeCache;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class ClusterTopicNotificationReceiver implements InitializingBean, ApplicationContextAware {
	
	private ApplicationContext context;

	@Autowired
	Timer timer;
	
	public static final Logger LOG = LoggerFactory.getLogger(ClusterTopicNotificationReceiver.class);
	
	@Override
	public void setApplicationContext(ApplicationContext context)throws BeansException {
		this.context = context;		
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		final Topic topic = (Topic) context.getBean(JMSConstants.TOPICNAME_CLUSTER_CLIENT);
		
		try {
			ClusterNodeCache nodeCache = this.context.getBean(ClusterNodeCache.class);
			nodeCache.init();
			Collection<ClusterNode> clusterNodes = nodeCache.getNodes();
			
			//If there are no cluster-nodes at all, we can assume there are not Cluster JMS Message to be sent
			if (clusterNodes.isEmpty()) {
				return;
			}
			
			for (ClusterNode node : nodeCache.getNodes()) {
				String sUrl = node.getJmsUrl();
				ActiveMQConnectionFactory jmsFactory = new ActiveMQConnectionFactory();
				jmsFactory.setBrokerURL(sUrl);
				jmsFactory.setTrustAllPackages(true);
				TopicConnection topicConnection = jmsFactory.createTopicConnection();
				topicConnection.start();
				
				TopicSession session = topicConnection
					.createTopicSession(false, TopicSession.AUTO_ACKNOWLEDGE);
				TopicSubscriber subscriber = session.createSubscriber(topic);
				subscriber.setMessageListener(new ClusterMessageListener());
				node.setConnected(true);
			}
			
			TimerTask task = new TimerTask() {				
				@Override
				public void run() {
					String server = NuclosClusterRegisterHelper.getServerName();
					NuclosClusterAction action = ClusterActionFactory
						.createClusterAction(NuclosClusterAction.Type.STARTUP_ACTION,
						                     NuclosClusterRegisterHelper.getOwnJmsUrl(), server);
					NuclosJMSUtils.sendObjectMessage(action, JMSConstants.TOPICNAME_CLUSTER, null);					
				}
			};
			timer.schedule(task, 1000*10);
			
		} catch (Exception e) {
			LOG.warn(e.getMessage(), e);
		}
	}
	
	public void topicRegister(ClusterNode node) {
		try {
			final Topic topic = (Topic) context.getBean(JMSConstants.TOPICNAME_CLUSTER_CLIENT);
			
			String sUrl = node.getJmsUrl();
			ActiveMQConnectionFactory jmsFactory = new ActiveMQConnectionFactory();
			jmsFactory.setBrokerURL(sUrl);
			jmsFactory.setTrustAllPackages(true);
			TopicConnection topicConnection = jmsFactory.createTopicConnection();
			topicConnection.start();
			
			TopicSession session = topicConnection.createTopicSession(false, TopicSession.AUTO_ACKNOWLEDGE);
			TopicSubscriber subscriber = session.createSubscriber(topic);
			subscriber.setMessageListener(new ClusterMessageListener());
			
			node.setConnected(true);		
		}
		catch(Exception e) {
			LOG.warn("Unable to register topic:", e);
		}
	}

}
