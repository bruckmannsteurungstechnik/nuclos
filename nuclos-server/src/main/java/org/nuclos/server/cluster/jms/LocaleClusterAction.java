//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.cluster.jms;

import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.server.common.ejb3.LocaleFacadeBean;


public class LocaleClusterAction implements NuclosClusterAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8961830810975071761L;
	private final boolean notify;
	
	public LocaleClusterAction(boolean notifyClusterCloud) {
		this.notify = notifyClusterCloud;
	}

	@Override
	public void doAction() {		
		LocaleFacadeBean bean = (LocaleFacadeBean)SpringApplicationContextHolder.getBean("localeService");
		bean.flushInternalCaches(notify);		 
	}

	@Override
	public boolean doOnMaster() {
		return false;
	}

}
