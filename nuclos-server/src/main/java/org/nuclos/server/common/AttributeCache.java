//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.nuclos.common.AttributeProvider;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.NuclosAttributeNotFoundException;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.security.Permission;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.attribute.valueobject.AttributeCVO;
import org.nuclos.server.cluster.cache.ClusterCache;
import org.nuclos.server.cluster.jms.ClusterActionFactory;
import org.nuclos.server.cluster.jms.NuclosClusterAction;
import org.nuclos.server.dal.DalSupportForGO;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Singleton class for getting attribute value objects.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @version 00.01.000
 */
public class AttributeCache implements AttributeProvider, ClusterCache {

	private static final Logger LOG = LoggerFactory.getLogger(AttributeCache.class);

	private static AttributeCache INSTANCE;
	
	//

	private final Map<UID, AttributeCVO> mpAttributes 
		= new ConcurrentHashMap<UID, AttributeCVO>();

	private final Map<UID, List<AttributeCVO>> mpAttributesByExternalEntity
		= new ConcurrentHashMap<UID, List<AttributeCVO>>();
	
	private MetaProvider metaProvider;
	
	public static AttributeCache getInstance() {
		return INSTANCE;
	}

	AttributeCache() {
		// validate();
		INSTANCE = this;
	}
	
	@Autowired
	void setMetaProvider(MetaProvider metaProvider) {
		this.metaProvider = metaProvider;
	}

	/**
	 * @deprecated nobody call this method
	 * @param attrcvo
	 */
	public void update(AttributeCVO attrcvo) {
		validate();

		mpAttributes.put(attrcvo.getId(), attrcvo);
		if(attrcvo.getExternalEntity() != null) {
			mpAttributesByExternalEntity.get(attrcvo.getExternalEntity()).add(attrcvo);
		}
	}

	/**
	 * @deprecated nobody call this method
	 * @param attrcvo
	 */
	public void remove(AttributeCVO attrcvo) {
		validate();

		mpAttributes.remove(attrcvo.getId());
		mpAttributesByExternalEntity.get(attrcvo.getExternalEntity()).remove(attrcvo);
	}

	/**
	 * �postcondition result != null
	 */
	@Override
	public AttributeCVO getAttribute(UID attribute) {
		this.validate();

		final AttributeCVO result = mpAttributes.get(attribute);
		if (result == null) {
			throw new NuclosAttributeNotFoundException(attribute);
		}
		assert result != null;
		return result;
	}

	public synchronized boolean contains(UID attribute) {
		validate();
		return mpAttributes.containsKey(attribute);
	}

	@Override
	public FieldMeta getEntityField(UID field) throws NuclosAttributeNotFoundException {
		return metaProvider.getEntityField(field);
	}


	@Override
	public Collection<AttributeCVO> getAttributes() {
		validate();
		return new HashSet<AttributeCVO>(mpAttributes.values());
	}


	private void invalidate() {
		mpAttributes.clear();
		mpAttributesByExternalEntity.clear();
	}

	@PostConstruct
	void validate() {
		if (mpAttributes.isEmpty() || mpAttributesByExternalEntity.isEmpty()) {
			try {
				LOG.debug("START building attribute cache.");

				for (EntityMeta<?> eMeta : metaProvider.getAllEntities()) {
					if (E.isNuclosEntity(eMeta.getUID()) || !eMeta.isStateModel()) {
						continue;
					}
					for (FieldMeta<?> efMeta : metaProvider
							.getAllEntityFieldsByEntity(eMeta.getUID()).values()) {
						Map<UID, Permission> permissions = new HashMap<UID, Permission>();
						// Username "" ???
						//SecurityCache.getInstance().getAttributeGroup("",
						//		eMeta.getUID(),
						//		efMeta.getFieldGroup(), null);
						AttributeCVO attrCVO = DalSupportForGO.getAttributeCVO(efMeta, permissions);

						mpAttributes.put(attrCVO.getId(), attrCVO);

						if (attrCVO.getExternalEntity() != null) {
							if (mpAttributesByExternalEntity.containsKey(attrCVO.getExternalEntity())) {
								mpAttributesByExternalEntity.get(attrCVO.getExternalEntity()).add(attrCVO);
							} else {
								List<AttributeCVO> lstAttributeCVO = new ArrayList<AttributeCVO>();
								lstAttributeCVO.add(attrCVO);
								mpAttributesByExternalEntity.put(attrCVO.getExternalEntity(), lstAttributeCVO);
							}
						}
					}
				}
				LOG.debug("FINISHED building attribute cache.");
			} catch (Exception e) {
				throw new CommonFatalException(e);
			}
		}
	}

	/**
	 * @param sEntity
	 * @return a list of AttributeCVO which contains a reference to another entity
	 */
	public Collection<AttributeCVO> getReferencingAttributes(String sEntity) {
		validate();
		return CollectionUtils.emptyIfNull(mpAttributesByExternalEntity.get(new UID(sEntity)));
	}

	@Override
	public void notifyClusterCloud() {
		NuclosClusterAction action = ClusterActionFactory.createClusterAction(NuclosClusterAction.Type.ATTRIBUTE_ACTION);
		NuclosJMSUtils.sendObjectMessage(action, JMSConstants.TOPICNAME_CLUSTER, null);		
	}

	@Override
	public void registerCache() {
	}

	@Override
	public void deregisterCache() {
	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public void invalidateCache(boolean notifyClients, boolean notifyClusterCloud) {
		invalidate();
		validate();
		if(notifyClusterCloud) {
			notifyClusterCloud();
		}
			
		
	}

}	// class AttributeCache
