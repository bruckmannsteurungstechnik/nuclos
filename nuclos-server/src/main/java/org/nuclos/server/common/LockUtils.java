package org.nuclos.server.common;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LockUtils {

	@Autowired
	private SessionUtils utils;
	
	@Autowired
	private MetaProvider metaProv;
	
	@Autowired
	private NucletDalProvider processorsProv;
	
	public LockUtils() {
	}
	
	public void checkLockedByCurrentUserInternal(UID entity, Object id) throws CommonPermissionException {
		if(!utils.isCalledRemotely()) {
			return;
		}
			
		checkLockedByCurrentUser(entity, id);
	}
	
	public void checkLockedByCurrentUser(UID entity, Object id) throws CommonPermissionException {
		EntityMeta<Object> eMeta = metaProv.getEntity(entity);
		if (eMeta.isOwner()) {
			UID ownerUID = processorsProv.getEntityObjectProcessor(eMeta).getOwner(id);
			if (ownerUID != null) {
				String currentUserName = utils.getCurrentUserName();
				UID userUID = SecurityCache.getInstance().getUserUid(currentUserName);
				if (!LangUtils.equal(userUID, ownerUID)) {
					throw new CommonPermissionException("locked.by.another.user");
				}
			}
		}
	}
	
	public void lock(UID entity, Object id, UID userUID) {
		processorsProv.getEntityObjectProcessor(entity).lock(id, userUID);
	}
	
	public void unlock(UID entity, Object id) {
		processorsProv.getEntityObjectProcessor(entity).unlock(id);
	}
	
}
