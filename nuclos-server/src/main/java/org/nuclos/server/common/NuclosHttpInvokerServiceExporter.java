package org.nuclos.server.common;

import org.nuclos.common.NuclosRemoteInvocationResult;
import org.springframework.remoting.httpinvoker.HttpInvokerServiceExporter;
import org.springframework.remoting.support.RemoteInvocation;
import org.springframework.remoting.support.RemoteInvocationResult;

public class NuclosHttpInvokerServiceExporter extends HttpInvokerServiceExporter {

	@Override
	protected RemoteInvocationResult invokeAndCreateResult(RemoteInvocation invocation, Object targetObject) {
		try {
			Object value = invoke(invocation, targetObject);
			NuclosRemoteInvocationResult result = new NuclosRemoteInvocationResult(value);
			//result.setUserObject("Hello from Server");
			// addd userObject here like above; for future use cases
			return result;
		}
		catch (Throwable ex) {
			return new NuclosRemoteInvocationResult(ex);
		}
	}
	
	
		

}
