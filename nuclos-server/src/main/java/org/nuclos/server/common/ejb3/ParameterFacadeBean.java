//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common.ejb3;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common.LafParameter;
import org.nuclos.common.ParameterProvider;
import org.nuclos.server.common.ServerParameterProvider;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.ImmutableSet;

/**
 * Facade bean for managing parameters.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
public class ParameterFacadeBean extends NuclosFacadeBean implements ParameterFacadeLocal, ParameterFacadeRemote {

	/**
	 * Defines the system parameters that are allowed to be read by any client, no login required!
	 * <p>
	 * TODO: The parameter system should be refactored to clearly separate server- and client-parameters.
	 * This whitelist could then be removed.
	 */
	private static final Set<String> WHITELIST = ImmutableSet.of(
			// TODO: Define constants for these Strings
			"application.settings.client.autologin.allowed",
			"DISPLAY_REFERENCE_FIELDS_ITALIC",
			"SUBFORM_CONFIRMATION_ON_DELETE",

			ParameterProvider.KEY_ACTIVATE_LOGBOOK,
			ParameterProvider.KEY_CLIENT_READ_TIMEOUT,
			ParameterProvider.KEY_CLIENT_VALIDATION_LAYER_PAINTER_NAME,
			ParameterProvider.KEY_DEFAULT_ENCODING,
			ParameterProvider.KEY_DEFAULT_FIELD_MULTIEDITING,
			ParameterProvider.KEY_DEFAULT_LOCALE,
			ParameterProvider.KEY_DEFAULT_SUBFORM_MULTIEDITING,
			ParameterProvider.KEY_FILECHOOSER_DONT_REMEMBER_PATH,
			ParameterProvider.KEY_FOCUSSED_ITEM_BACKGROUND_COLOR,
			ParameterProvider.KEY_GENERICOBJECT_IMPORT_EXCLUDED_ATTRIBUTES_FOR_CREATE,
			ParameterProvider.KEY_GENERICOBJECT_IMPORT_EXCLUDED_ATTRIBUTES_FOR_UPDATE,
			ParameterProvider.KEY_HISTORICAL_STATE_CHANGED_COLOR,
			ParameterProvider.KEY_HISTORICAL_STATE_NOT_TRACKED_COLOR,
			ParameterProvider.KEY_LAYOUT_CUSTOM_KEY,
			ParameterProvider.KEY_MANDATORY_ADDED_ITEM_BACKGROUND_COLOR,
			ParameterProvider.KEY_MANDATORY_ITEM_BACKGROUND_COLOR,
			ParameterProvider.KEY_MAX_ROWCOUNT_FOR_SEARCHRESULT_IN_TASKLIST,
			ParameterProvider.KEY_NUCLOS_INSTANCE_NAME,
			ParameterProvider.KEY_ONLINE_HELP,
			ParameterProvider.KEY_REPORT_MAXROWCOUNT,
			ParameterProvider.KEY_RESPLAN_RESOURCE_LIMIT,
			ParameterProvider.KEY_ROOTPANE_BACKGROUND_COLOR,
			ParameterProvider.KEY_TEMPLATE_USER,
			ParameterProvider.KEY_THUMBAIL_SIZE,
			ParameterProvider.KEY_VLP_LOADINGTHREAD_KEEP_ALIVE,
			ParameterProvider.KEY_VLP_RESULTCACHE_EXPIRATION,
			ParameterProvider.LOCALE_OPTIONS,
			ParameterProvider.NUMBER_MAX_SORT_ATTRIBUTES,
			ParameterProvider.PADDING_RIGHT_NUMERIC_CELLS,
			ParameterProvider.QUICKSEARCH_DELAY_TIME,
			ParameterProvider.SHOW_OVERLAY_FOR_TOO_SMALL_TEXTFIELDS,
			ParameterProvider.SORT_CALCULATED_ATTRIBUTES,
			ParameterProvider.USE_OLD_LOCK_STYLE,
			ParameterProvider.MODIFIABLE_READONLY
	);

	@Autowired
	private ServerParameterProvider serverParameterProvider;

	public ParameterFacadeBean() {
	}

	/**
	 * Returns all public system parameters.
	 *
	 * @return map of parameters with values
	 */
	public Map<String, String> getParameters() {
		final Map<String, String> allParams = serverParameterProvider.getAllParameters();

		return allParams.entrySet().stream()
				.filter(entry -> isPublic(entry.getKey()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}

	/**
	 * Determines if the given system parameter key is public,
	 * i.e. if it is whitelisted or a look-and-feel parameter.
	 */
	private static boolean isPublic(final String parameterKey) {
		return WHITELIST.contains(parameterKey)
				|| StringUtils.startsWith(parameterKey, LafParameter.LAF_PREFIX);
	}
}
