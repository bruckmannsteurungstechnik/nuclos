//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common.ejb3;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.E;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.attribute.DynamicAttributeVO;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.valueobject.TimelimitTaskVO;
import org.nuclos.server.dal.DalSupportForGO;
import org.nuclos.server.genericobject.ejb3.GenericObjectFacadeLocal;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Facade for managing timelimit tasks.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Transactional(noRollbackFor= {Exception.class})
@RolesAllowed("Login")
public class TimelimitTaskFacadeBean extends NuclosFacadeBean implements TimelimitTaskFacadeLocal, TimelimitTaskFacadeRemote {
	
	@Autowired
	private GenericObjectFacadeLocal genericObjectFacade;
	
	@Autowired
	private MasterDataFacadeLocal masterDataFacade;
	
	public TimelimitTaskFacadeBean() {
	}
	
	/**
	 * get all timelimit tasks (or only unfinished tasks)
	 * @param bOnlyUnfinishedTasks get only unfinished tasks
	 * @return collection of task value objects
	 */
	public Collection<TimelimitTaskVO> getTimelimitTasks(boolean bOnlyUnfinishedTasks) {
		return getTimelimitTasksForGenericObject(null, bOnlyUnfinishedTasks);
	}
		
	/**
	 * get all timelimit tasks (or only unfinished tasks) for a generic object
	 * @param goId
	 * @param bOnlyUnfinishedTasks get only unfinished tasks
	 * @return collection of task value objects
	 */
	public Collection<TimelimitTaskVO> getTimelimitTasksForGenericObject(Long goId, boolean bOnlyUnfinishedTasks) {
		final Collection<TimelimitTaskVO> result = new HashSet<TimelimitTaskVO>();

		Collection<MasterDataVO<Long>> mdVOList;

		if (!bOnlyUnfinishedTasks && goId == null) {
			mdVOList = masterDataFacade.getMasterData(E.TIMELIMITTASK, null);
		} else {
			CollectableSearchCondition cond1 = bOnlyUnfinishedTasks ?
					SearchConditionUtils.newIsNullCondition(E.TIMELIMITTASK.completed) : null;
			CollectableSearchCondition cond2 = goId != null ? 
					SearchConditionUtils.newIdComparison(E.TIMELIMITTASK.genericobject, ComparisonOperator.EQUAL, goId) : null;
			CollectableSearchCondition cond = (cond1 != null && cond2 != null) ?
					SearchConditionUtils.and(cond1, cond2) : (cond1 != null ? cond1 : cond2);
			mdVOList = masterDataFacade.getMasterData(E.TIMELIMITTASK, cond);
		}

		for (MasterDataVO<Long> mdVO : mdVOList) {
			try {
				TimelimitTaskVO timelimitTask = getTimelimitTaskVO(mdVO);
				if (timelimitTask != null) {
					result.add(timelimitTask);
				}
			}
			catch(CommonFinderException e) {
				throw new CommonFatalException(e);
			}
		}
		return result;
	}

	/**
	 * create a new TimelimitTask in the database
	 * @return same task as value object
	 */
	public TimelimitTaskVO create(TimelimitTaskVO voTimelimitTask) throws CommonValidationException, NuclosBusinessException {

		//check attribute for validity
		voTimelimitTask.validate();	//throws CommonValidationException

		try {
			MasterDataVO<Long> mdvo = masterDataFacade.create(
					MasterDataWrapper.wrapTimelimitTaskVO(voTimelimitTask), null);
			return getTimelimitTaskVO(masterDataFacade.get(E.TIMELIMITTASK, mdvo.getId()));
		}
		catch (CommonPermissionException ex) {
			throw new NuclosBusinessException(ex);
		}
		catch (CommonFinderException ex) {
			throw new CommonFatalException(ex);
		}
		catch(CommonCreateException ex) {
			throw new NuclosBusinessException(ex);
		}
	}

	/**
	 * modify an existing TimelimitTask in the database
	 * @param voTimelimitTask containing the task data
	 * @return new task id
	 */
	public TimelimitTaskVO modify(TimelimitTaskVO voTimelimitTask) throws CommonFinderException, CommonStaleVersionException, CommonValidationException, NuclosBusinessException {

		//check attribute for validity
		voTimelimitTask.validate();	//throws CommonValidationException

		try {
			masterDataFacade.modify(MasterDataWrapper.wrapTimelimitTaskVO(voTimelimitTask), null);
			return getTimelimitTaskVO(masterDataFacade.get(E.TIMELIMITTASK, voTimelimitTask.getId()));
		}
		catch (CommonPermissionException e) {
			throw new NuclosBusinessException(e);
		}
		catch(CommonRemoveException e) {
			throw new NuclosBusinessException(e);
		}
		catch(CommonCreateException e) {
			throw new NuclosBusinessException(e);
		}
	}

	/**
	 * delete TimelimitTask from database
	 * @param voTimelimitTask containing the task data
	 */
	public void remove(TimelimitTaskVO voTimelimitTask) 
			throws CommonFinderException, CommonRemoveException, 
			CommonStaleVersionException, NuclosBusinessException {
		try {
			masterDataFacade.remove(E.TIMELIMITTASK.getUID(), voTimelimitTask.getPrimaryKey(), false, null);
		}
		catch (CommonPermissionException ex) {
			throw new NuclosBusinessException(ex);
		}
	}
	
	/**
	 * delete all TimelimitTasks for an generic object
	 * @param goId generic object primary key
	 * @throws CommonFinderException
	 * @throws CommonRemoveException
	 * @throws CommonStaleVersionException
	 * @throws NuclosBusinessException
	 */
	public void removeTasksForGenericObject(Long goId) 
			throws CommonFinderException, CommonRemoveException, CommonStaleVersionException, NuclosBusinessException {
		for (TimelimitTaskVO voTimelimitTask : getTimelimitTasksForGenericObject(goId, false)) {
			remove(voTimelimitTask);
		}
	}

	private TimelimitTaskVO getTimelimitTaskVO(MasterDataVO mdVO) throws CommonFinderException {
		
		final Long iGenericObjectId = mdVO.getFieldId(E.TIMELIMITTASK.genericobject);
		
		final EntityObjectVO<Long> eo = DalSupportForGO.getEntityObject(iGenericObjectId);
		if (eo == null) { // object may be null because of mandator
			return null;
		}
		final String sIdentifier = (String) eo.getFieldValue(SF.SYSTEMIDENTIFIER.getUID(eo.getDalEntity()));
		final String sStatus = eo.getFieldValue(SF.STATE);
		final String sProcess = eo.getFieldValue(SF.PROCESS);
		TimelimitTaskVO timelimitTaskVO = MasterDataWrapper.getTimelimitTaskVO(mdVO, sIdentifier, sStatus, sProcess);
		
		return augmentModuleData(timelimitTaskVO);		
	}
	
   private TimelimitTaskVO augmentModuleData(TimelimitTaskVO timelimitTaskVO) throws CommonFinderException {
   	// TODO_AUTOSYNC: A little bit hackish -- but since it will somehow be replaced with the new GO mechanism, 
   	// I leave it this way...
   	if (timelimitTaskVO.getGenericObjectId() != null) {
   		UID moduleId = genericObjectFacade.getModuleContainingGenericObject(timelimitTaskVO.getGenericObjectId());
   		
	   	GenericObjectWithDependantsVO go = CollectionUtils.getFirst(genericObjectFacade.getGenericObjectsMoreNoCheck(
	   		moduleId, Collections.singletonList(timelimitTaskVO.getGenericObjectId()),
	   		Collections.<UID>emptySet(), ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY)));
	   	for (DynamicAttributeVO attr : go.getAttributes()) {
	   		if (SF.SYSTEMIDENTIFIER.checkField(moduleId, attr.getAttributeUID())) {
	   			timelimitTaskVO.setIdentifier((String) attr.getValue());
	   		}
	   		if (SF.STATE.checkField(moduleId, attr.getAttributeUID())) {
	   			timelimitTaskVO.setStatus((String) attr.getValue());
	   		}
	   		if (SF.PROCESS.checkField(moduleId, attr.getAttributeUID())) {
	   			timelimitTaskVO.setProcess((String) attr.getValue());
	   		}
	   	}
	   }
   	return timelimitTaskVO;
   }
}
