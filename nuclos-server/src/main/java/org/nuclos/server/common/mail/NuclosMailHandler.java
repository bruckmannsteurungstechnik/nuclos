//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common.mail;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeUtility;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.nuclos.api.mail.NuclosMail;
import org.nuclos.common.NuclosFile;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.MailReceiveException;
import org.nuclos.server.common.mail.properties.MailConnectionProperties;
import org.nuclos.server.ruleengine.NuclosFatalRuleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.mail.imap.IMAPStore;
import com.sun.mail.pop3.POP3Store;

/**
 *
 */
public class NuclosMailHandler {

	private final static Logger LOG = LoggerFactory.getLogger(NuclosMailHandler.class);

	private final MailConnectionProperties connectionProperties;

	public NuclosMailHandler(final MailConnectionProperties connectionProperties) {
		this.connectionProperties = connectionProperties;
	}

	public MailConnectionProperties getConnectionProperties() {
		return connectionProperties;
	}

	public List<org.nuclos.api.mail.NuclosMail> receiveMails(String folderFromName, boolean deleteAfterReceiving) throws MailReceiveException {
		List<org.nuclos.api.mail.NuclosMail> retVal = new ArrayList<org.nuclos.api.mail.NuclosMail>();
		
		Store store = null;
		
		try {
			final MailConnectionProperties properties = getConnectionProperties();

			if (folderFromName != null) {
				properties.setFolderFrom(folderFromName);
			}

			Session session = Session.getInstance(properties.toProperties(),
				new javax.mail.Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(properties.getUser(), properties.getPassword());
					}
				});

		   session.setDebug(true);
		   store = session.getStore(properties.getProtocol());

		   if (connectionProperties.isSsl()) {
			   if (store instanceof POP3Store) {
				   Field f = POP3Store.class.getDeclaredField("isSSL");
				   f.setAccessible(true);
				   f.set(store, true);
			   } else if (store instanceof IMAPStore) {
				   Field f = IMAPStore.class.getDeclaredField("isSSL");
				   f.setAccessible(true);
				   f.set(store, true);
			   } else {
				   throw new MailReceiveException("Unknown store " + store.getClass().getCanonicalName() + ": Could not set 'isSSL'");
			   }
		   }
		   
		   store.connect();

			Folder folderFrom = store.getFolder(properties.getFolderFrom());
			Folder folderTo = null;
			if (deleteAfterReceiving) {
				folderFrom.open(Folder.READ_WRITE);
				if (properties.getFolderTo() != null) {
					folderTo = store.getFolder(properties.getFolderTo());
				}
			} else {
				folderFrom.open(Folder.READ_ONLY);
			}


		   Message message[] = folderFrom.getMessages();
		   for ( int i = 0; i < message.length; i++ ) {
				Message m = message[i];
				org.nuclos.api.mail.NuclosMail mail = getNuclosMail(m);

				retVal.add(mail);

				if (deleteAfterReceiving) {
					removeMessage(m, folderFrom, folderTo);
				}
			}

		   if (deleteAfterReceiving) {
		   	folderFrom.close(true);
		   } else {
		   	folderFrom.close(false);
		   }

			if (folderTo != null && folderTo.isOpen()) {
				folderTo.close(false);
			}

		   return retVal;
		} catch (Exception e) {
			throw new NuclosFatalRuleException(e);
		} finally {
			if (store != null && store.isConnected()) {
				try {
					store.close();
				} catch (MessagingException e) {
					throw new NuclosFatalRuleException(e);
				}
			}
		}
	}

	/**
	 * Fetches the given Message and creates an instance of NuclosMail from it.
	 *
	 * @param m
	 * @return
	 * @throws MessagingException
	 * @throws IOException
	 */
	private static org.nuclos.api.mail.NuclosMail getNuclosMail(final Message m) throws MessagingException, IOException {
		org.nuclos.api.mail.NuclosMail mail = new org.nuclos.api.mail.NuclosMail();
		LOG.debug("Received mail: From: {}; To: {}; ContentType: {}; Subject: {}; Sent: {}",
		          Arrays.toString(m.getFrom()),
		          Arrays.toString(m.getAllRecipients()),
		          m.getContentType(),
		          m.getSubject(),
		          m.getSentDate());

		mail.setFrom(getAddressString(m.getFrom()));
		mail.addRecipient(getAddressString(m.getRecipients(RecipientType.TO)));
		mail.setReplyTo(getAddressString(m.getReplyTo()));

		mail.setSubject(m.getSubject());
		mail.setReceivedDate(m.getReceivedDate());
		mail.setSentDate(m.getSentDate());

		// TODO: Set the correct mime type and charset of the NuclosMail

		if (m.isMimeType("text/plain")) {
			mail.setMessage((String) m.getContent());
		}
		else {
			if (m.getContent() instanceof Multipart) {
				Multipart mp = (Multipart) m.getContent();

				final String textPlain = getText(mp.getParent(), false);
				final String textHTML = getText(mp.getParent(), true);

				// TODO: Extend NuclosMail by another field for HTML text and set it here
				final String message = StringUtils.isNotBlank(textPlain) ? textPlain : textHTML;
				mail.setMessage(message);

				getAttachments(mp, mail, 0);
			}
			else if (m.getContent() instanceof String) {
				String val = (String) m.getContent();
				mail.setMessage(val);
			}
		}

		return mail;
	}

	/**
	 * Returns the given addresses as a simple String.
	 * Return null, if addresses is null.
	 *
	 * @param addresses
	 * @return
	 */
	private static String getAddressString(Address[] addresses) {
		if (addresses == null) {
			return null;
		} else if (addresses.length == 1 && addresses[0] instanceof InternetAddress) {
			return ((InternetAddress) addresses[0]).getAddress();
		} else {
			return Arrays.toString(addresses);
		}
	}

	/**
	 * Return the primary text content of the message.
	 *
	 * @see <href="http://www.oracle.com/technetwork/java/javamail/faq/index.html#mainbody">JavaMail FAQ</href="http://www.oracle.com/technetwork/java/javamail/faq/index.html#mainbody">
	 */
	private static String getText(Part p, boolean preferHTML) throws
			MessagingException, IOException {
		if (p.isMimeType("text/calendar")) {
			String s = new String(IOUtils.toByteArray((InputStream) p.getContent()), "UTF-8");
			return s;
		} else if (p.isMimeType("text/*")) {
			String s = (String)p.getContent();
			return s;
		}

		if (p.isMimeType("multipart/alternative")) {
			// prefer html text over plain text
			Multipart mp = (Multipart)p.getContent();
			String text = null;
			for (int i = 0; i < mp.getCount(); i++) {
				Part bp = mp.getBodyPart(i);
				if (bp.isMimeType("text/plain")) {
					if (text == null)
						text = getText(bp, preferHTML);

					if (!preferHTML)
						return text;
				} else if (bp.isMimeType("text/html")) {
					String s = getText(bp, preferHTML);

					if (s != null && preferHTML)
						return s;
				} else {
					return getText(bp, preferHTML);
				}
			}
			return text;
		} else if (p.isMimeType("multipart/*")) {
			Multipart mp = (Multipart)p.getContent();
			for (int i = 0; i < mp.getCount(); i++) {
				String s = getText(mp.getBodyPart(i), preferHTML);
				if (s != null)
					return s;
			}
		}

		return null;
	}

	/**
	 * Copies the message to the "to"-Folder (if it is not null) and deletes the message from the "from"-Folder.
	 * 
	 * @param m
	 * @param from
	 * @param to
	 */
	private static void removeMessage(Message m, Folder from, Folder to) {
		try {
			if (to != null) {
				if (!to.exists()) {
					to.create(Folder.HOLDS_MESSAGES);
				}
				from.copyMessages(new Message[]{m}, to);
			}
			m.setFlag(Flags.Flag.DELETED, true);
		} catch (MessagingException e) {
			throw new NuclosFatalRuleException(e);
		}
	}

	/**
	 * Recursively fetches all attachments (normal and inline attachments).
	 *
	 * @param multipart
	 * @param mail
	 * @param iUntitledAttachments
	 * @throws MessagingException
	 * @throws IOException
	 */
	private static void getAttachments(Multipart multipart, NuclosMail mail, Integer iUntitledAttachments) throws MessagingException, IOException {
		for (int i = 0; i < multipart.getCount(); i++) {
			Part part = multipart.getBodyPart(i);
			String disposition = part.getDisposition();
			MimeBodyPart mimePart = (MimeBodyPart) part;
			LOG.debug("Disposition: {}; Part ContentType: {}",
			          disposition, mimePart.getContentType());

			if (part.getContent() instanceof Multipart) {
				LOG.debug("Start child Multipart.");
				Multipart childmultipart = (Multipart) part.getContent();
				getAttachments(childmultipart, mail, iUntitledAttachments);
				LOG.debug("Finished child Multipart.");
			} else if (Part.INLINE.equalsIgnoreCase(disposition)) {
				LOG.debug("Inline attachment: {}", mimePart.getFileName());
				final NuclosFile file = readIntoNuclosFile(mimePart, iUntitledAttachments);
				mail.addInlineAttachment(file);
			} else if (Part.ATTACHMENT.equalsIgnoreCase(disposition)) {
				LOG.debug("Attachment: {}", mimePart.getFileName());
				final NuclosFile file = readIntoNuclosFile(mimePart, iUntitledAttachments);
				mail.addAttachment(file);
			} else if ((part.getContent() instanceof InputStream || part.getContent() instanceof String) 
					&& StringUtils.isNotBlank(part.getFileName())) {
				LOG.debug("Attachment without defined disposition (Content-Type: application/octet-stream): {}", mimePart.getFileName());
				final NuclosFile file = readIntoNuclosFile(mimePart, iUntitledAttachments);
				mail.addAttachment(file);
			} else {
				Object content = part.getContent();
				Object dhContent = part.getDataHandler().getContent();
				LOG.debug("Ignoring unknown multipart {} {} {}", mimePart.getFileName(), content, dhContent);
			}
		}
	}

	private static NuclosFile readIntoNuclosFile(final MimeBodyPart mimePart, Integer iUntitledAttachments) throws IOException, MessagingException {
		String sFileName = mimePart.getFileName();
		if (sFileName != null) {
			sFileName = MimeUtility.decodeText(mimePart.getFileName());
		} else {
			SpringLocaleDelegate localeDelegate = SpringApplicationContextHolder.getBean(SpringLocaleDelegate.class);
			StringBuilder sb = new StringBuilder();
			sb.append(localeDelegate.getResource("NuclosMailHandler.untitledAttachment.filename", "Unbenannte Datei"));
			sb.append(" ");
			sb.append(iUntitledAttachments);
			sFileName =  sb.toString();

			iUntitledAttachments++;
		}
		final byte[] buf = IOUtils.toByteArray(mimePart.getInputStream());
		return new NuclosFile(sFileName, buf);
	}

}