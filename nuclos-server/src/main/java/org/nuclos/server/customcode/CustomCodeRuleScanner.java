package org.nuclos.server.customcode;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.servlet.ServletContext;

import org.nuclos.api.annotation.Rule;
import org.nuclos.common.UID;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.ClassMetadata;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.util.ClassUtils;

//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
public class CustomCodeRuleScanner 
{
	private static final Logger LOG = LoggerFactory.getLogger(CustomCodeRuleScanner.class);

	private ClassLoader cl;

	private ServletContext servletContext;
	
	public CustomCodeRuleScanner(ClassLoader cl, ServletContext ctx)
	{
		this.cl = cl;
		this.servletContext = ctx;
	}

	/**
	 * This method returns all loadable classes that implement one of the RuleInterfaces
	 * 
	 * @param listOfAllowedInterfaces
	 */
	public ConcurrentMap<String, EventSupportSourceVO> getExecutableRulesFromClasspath(List<String> listOfAllowedInterfaces) 
	{
		final ConcurrentMap<String, EventSupportSourceVO> execRuleClasses = new ConcurrentHashMap<String, EventSupportSourceVO>();
	
		final Environment env = new StandardEnvironment();
		final ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver(cl);
		final MetadataReaderFactory metadataReaderFactory = new CachingMetadataReaderFactory(resourcePatternResolver);
	
		// Directories and JARs stored in the Classpath as URLs (e.g. Nuclet.jar)
		final List<File> urls = new ArrayList<File>();
//		for (URL u : cl.getURLs() )
//		{
//			urls.add(new File(u.getFile()));
//		}
		
		// With extensions additional jars might be added to the /WEB-INF/lib/ directory
		// and so to the classpath
		final Iterator<String> iterator = servletContext.getResourcePaths("/WEB-INF/lib/").iterator();

		while (iterator.hasNext())
		{
			String foundJarFile = iterator.next();
			// Only Nuclos-Extensions scanned
			if (foundJarFile != null && foundJarFile.contains("-server-"))
			{
				// Create real path to found jar file for class scanning			
				urls.add(new File (servletContext.getRealPath("/") + foundJarFile));				
			}
		}
		
		for (File u : urls)
		{
			// Search for classes within the current ClassLoader-URLs for loading classes and resources
			ArrayList<File> searchFile = searchClassFilesInDirectory(u, ".class");
			
			for (File curFile :searchFile)
			{
				// Extract filename and filepath to retrieve Resourceinformation 
				String pfad = curFile.getPath().substring(0, curFile.getPath().indexOf(curFile.getName()));
				String converterdResourcePath = 
						ClassUtils.convertClassNameToResourcePath(env.resolveRequiredPlaceholders(pfad));
				
				// get the resource
				Resource resource = resourcePatternResolver.getResource(converterdResourcePath + curFile.getName());
				
				// Get the metainformation of the resource
				MetadataReader metadataReader = null;
				try {
					metadataReader = metadataReaderFactory.getMetadataReader(resource);
				} catch (IOException e) {
					if (!converterdResourcePath.startsWith("1/0/")) {
						LOG.info("not found: {}: {}", resource, e.toString());
					}
				}
				
				if (metadataReader != null && metadataReader.getClassMetadata() != null)
				{
					for (String inter : listOfAllowedInterfaces)
					{
						boolean contains = 
								Arrays.asList(metadataReader.getClassMetadata().getInterfaceNames()).contains(inter);
						
						// If the resource implements one of the ruleInterfaces the resource will
						// be listed as an executable eventsupport
						if (contains)
						{
							ClassMetadata classMetadata = metadataReader.getClassMetadata();
							AnnotationMetadata anmeta = metadataReader.getAnnotationMetadata();
							
							String ruleName = resource.getFilename();
							String ruleDescription = classMetadata.getClassName();
							String ruleClassName = classMetadata.getClassName();
						
							Date ruleClassCompilationDate = null;
							
							try {
								ruleClassCompilationDate = new Date(resource.lastModified());
							} catch (Exception e) {}
							
							String rulePackagePath = ClassUtils.convertResourcePathToClassName(
									env.resolveRequiredPlaceholders(env.resolveRequiredPlaceholders(pfad)));
							
							if (rulePackagePath != null && rulePackagePath.trim().length() > 0 && 
									rulePackagePath.lastIndexOf(".") == rulePackagePath.length() - 1)
								rulePackagePath = rulePackagePath.substring(0, rulePackagePath.length()-1);
							
							if (anmeta.hasAnnotation(Rule.class.getCanonicalName()))
							{
								Map<String, Object> annotationAttributes = anmeta.getAnnotationAttributes(Rule.class.getCanonicalName());
								if (annotationAttributes.containsKey("name"))
								{
									ruleName = (String) annotationAttributes.get("name");
								}
								if (annotationAttributes.containsKey("description"))
								{
									ruleDescription = (String) annotationAttributes.get("description");
								}										
							}

							if (!execRuleClasses.containsKey(ruleName)) {
								
								List<String> newListOfInterfaces = new ArrayList<String>();
								newListOfInterfaces.add(inter);
								NuclosValueObject<UID> nuclosValueObject = 
										new NuclosValueObject<UID>(null, ruleClassCompilationDate, null, ruleClassCompilationDate, null, 1);
								
								EventSupportSourceVO newRule = 
										new EventSupportSourceVO(nuclosValueObject, ruleName, ruleDescription, ruleClassName, 
												newListOfInterfaces, rulePackagePath, ruleClassCompilationDate, null, true );
								execRuleClasses.put(ruleName, newRule);
							}
							else {
								EventSupportSourceVO eventSupportSourceVO = execRuleClasses.get(ruleName);
								eventSupportSourceVO.getInterface().add(inter);
							}
						}
					}	
				}
				
			}	
		}
		return execRuleClasses;
	}
    

	
	/**
	 * This methods scans the given directoy and all subdirectories and returns a list of
	 * all files that correspond with the given filePattern
	 * 
	 * @param dir - Directory oder File to search in
	 * @param filenamePattern - value to search for
	 * @return List of all files that correspond with the filenamePattern within the given directory
	 */
	private ArrayList<File> searchClassFilesInDirectory(File root, String filenamePattern) {

		ArrayList<File> matches = new ArrayList<File> ();
		
		if (root.isFile())
		{
			// Does the found file correpsond with the filepattern
			if (root.getName().endsWith(filenamePattern)) { 
				matches.add(root);
			}
			else if (root.getName().endsWith(".jar"))
			{
				try {
		        	JarFile jarFile = new JarFile(root);
		        	Enumeration<JarEntry> jarEntries = jarFile.entries();
					while(jarEntries.hasMoreElements())
					{
						JarEntry foundElement = jarEntries.nextElement();
						if (foundElement.getName().contains(filenamePattern))
						{
							File myFile = new File(foundElement.getName());
							matches.add(myFile);
						}
					}
				} catch (IOException e) {
					LOG.warn("jar-File '{}' could not be opened for scanning {}",
					         root.getName(), e);
				}
			}
		}
		else if (root.isDirectory())
		{
			// Recursive call for subdirectories
			matches.addAll(searchClassFilesInDirectory(root, filenamePattern)); 
		}
		
		return matches;
	}
	
}
