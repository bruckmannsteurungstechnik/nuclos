package org.nuclos.server.customcode.codegenerator;

import static org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.CCCEJARFILE;
import static org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.CODE_COMPILER_CLASSPATH_EXTENSION;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;

import javax.annotation.PostConstruct;

import org.apache.commons.io.IOUtils;
import org.nuclos.common.NuclosFatalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

@Component
public class GeneratorClasspathComponent implements ApplicationContextAware {
	
	private static final Logger LOG = LoggerFactory.getLogger(GeneratorClasspathComponent.class);
	
	//

	private List<File> expandedGeneratorClassPath;
	
	private List<File> expandedGeneratorClassPathApiOnly;
	
	private ApplicationContext applicationContext;
	
	private final Collection<File> nuclosJars = new ArrayList<File>();
	
	private volatile boolean ccceCreated = false;
	
	GeneratorClasspathComponent() {
	}
	
	@PostConstruct
	void init() {
		// ensure that CCCE.jar exists. (tp)
		try {
			extractCodeCompilationClasspathExtension();
			assert !nuclosJars.isEmpty();
			expandedGeneratorClassPath = getExpandedGeneratorClassPathFactory(false);
			expandedGeneratorClassPathApiOnly = getExpandedGeneratorClassPathFactory(true);
		} catch (IOException e) {
			throw new NuclosFatalException(e);
		}
	}
	
	private List<File> getExpandedGeneratorClassPathFactory(final boolean apiOnly) {
		final List<File> classPath = new ArrayList<File>();
		final Resource r = applicationContext.getResource("WEB-INF/lib/");
		try {
			classPath.addAll(getLibs(r.getFile(), apiOnly));
		}
		catch (IOException e) {
			throw new NuclosFatalException(e);
		}
		if (apiOnly) {
			classPath.add(CCCEJARFILE);
		}
		else {
			classPath.addAll(nuclosJars);
		}
		return classPath;
	}

	/**
	 * Returns the expanded class path for system parameter {@code nuclos.codegenerator.class.path}.
	 * Note: WSDL libraries are not included.
	 */
	public List<File> getExpandedSystemParameterClassPath(boolean apiOnly) {
		if (!CCCEJARFILE.exists()) {
			throw new NuclosFatalException("Code-Compiler-Classpath-Extension removed! Restart Nuclos Server to generate a new one...\n(" + CCCEJARFILE.getAbsolutePath() + ")");
		}
		if (apiOnly) {
			return expandedGeneratorClassPathApiOnly;
		} else {
			return expandedGeneratorClassPath;
		}
	}

	public List<File> getLibs(File folder, boolean apiOnly) {
		final List<File> files = new ArrayList<File>();
		if(!folder.isDirectory()) {
			// just return empty list, compiler will give notice if classes are missing
			return files;
		}
		for(File file : folder.listFiles()) {
			boolean add = true;
			if (apiOnly && file.getName().startsWith("nuclos-") && !file.getName().contains("-api-")) {
				add = false;
			}
			if (add) {
				files.add(file);
			}
		}		
		return files;
	}
	
	private File extractCodeCompilationClasspathExtension() throws IOException {
		final Resource r = applicationContext.getResource("WEB-INF/lib/");
		for(File file : r.getFile().listFiles()) {
			if (file.getName().startsWith("nuclos-") && !file.getName().contains("-api-")) {
				nuclosJars.add(file);
			}
		}
		return extractCodeCompilationClasspathExtension(nuclosJars);
	}
	
	private File extractCodeCompilationClasspathExtension(Collection<File> nuclosJars) throws IOException {
		if (!ccceCreated) {
			if (nuclosJars.isEmpty()) {
				throw new IllegalStateException();
			}
		    final FileOutputStream out = new FileOutputStream(CCCEJARFILE);
		    final JarOutputStream jout = new JarOutputStream(out);
		    
		    try {
			    for (File nuclosJar : nuclosJars) {
					final JarFile nuclosJarFile = new JarFile(nuclosJar);
					for (Class<?> cls : CODE_COMPILER_CLASSPATH_EXTENSION) {
						final String sEntry = cls.getName().replace('.', '/')+".class";
						final JarEntry searchEntry = nuclosJarFile.getJarEntry(sEntry);
						if (searchEntry != null) {
							final JarEntry newEntry = new JarEntry(sEntry);
							newEntry.setTime(searchEntry.getTime());
							jout.putNextEntry(newEntry);
							final InputStream is = nuclosJarFile.getInputStream(searchEntry);
							try {
								IOUtils.copy(is, jout);
							} finally {
								jout.closeEntry();	
								is.close();
							}
						}
					}
			    }
		    } finally {
		    	jout.finish();
		    	out.close();
		    }
		    ccceCreated = true;
		    LOG.info("Code-Compiler-Classpath-Extension (CCCE) extracted from {} stored in {}",
		             nuclosJars, CCCEJARFILE.getAbsolutePath());
		}
		return CCCEJARFILE;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}	
	
}
