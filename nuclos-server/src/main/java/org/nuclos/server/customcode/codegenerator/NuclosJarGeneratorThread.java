package org.nuclos.server.customcode.codegenerator;

import java.util.concurrent.ForkJoinPool;

import org.nuclos.common.JMSConstants;
import org.nuclos.common.Priority;
import org.nuclos.common.RuleNotification;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeLocal;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NuclosJarGeneratorThread extends Thread {

	private static final Logger LOG = LoggerFactory.getLogger(NuclosJarGeneratorThread.class);

	private final EventSupportFacadeLocal eventSupportFacadeLocal;

	private final ForkJoinPool builderThreadPool;

	public NuclosJarGeneratorThread(EventSupportFacadeLocal eventSupportFacadeLocal) {
		this.eventSupportFacadeLocal = eventSupportFacadeLocal;
		setName(getClass().getSimpleName());
		int cores = Runtime.getRuntime().availableProcessors();
		builderThreadPool = new ForkJoinPool(cores);
	}

	@Override
	public void interrupt() {
		super.interrupt();
		builderThreadPool.shutdown();
	}

	@Override
	public boolean isInterrupted() {
		return super.isInterrupted() && builderThreadPool.isShutdown();
	}

	@Override
	public void run() {
		LOG.debug("Nuclos jar generator thread started.");
		long start = System.currentTimeMillis();
		try {
			LOG.debug("Generating Nuclos jars (code generation, compile, jar) ...");
			eventSupportFacadeLocal.createBusinessObjects(builderThreadPool);
			long end = System.currentTimeMillis();
			LOG.debug("Generated Nuclos jars. Took {} s.", (end - start) / 1000L);
		} catch (NuclosCompileException nce) {
			LOG.error("Unable to generate Nuclos jars:", nce);

			StringBuilder msg = new StringBuilder();
			msg.append(
				SpringLocaleDelegate.getInstance().getText("CodeCompilerError.title"));
			if (nce.getErrorMessages() != null) {
				for (NuclosCompileException.ErrorMessage em : nce.getErrorMessages()) {
					msg
						.append("\n")
						.append(em.getSource())
						.append(" (")
						.append(em.getLineNumber())
						.append(",")
						.append(em.getColumnNumber())
						.append("):\n")
						.append(em.getMessage(null));
				}
			}

			RuleNotification rn = new RuleNotification(Priority.HIGH, msg.toString(),
			                                           "Nuclos Server");
			NuclosJMSUtils
				.sendObjectMessage(rn, JMSConstants.TOPICNAME_RULENOTIFICATION, null);

		} catch (InterruptedException e) {
			LOG.debug("Nuclos jar generator was interrupted ...");
		} finally {
			builderThreadPool.shutdown();
		}
	}
}