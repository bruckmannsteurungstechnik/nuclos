//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.customcode.codegenerator;

import java.io.File;
import java.io.IOException;

import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.common.NuclosSystemParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Role;
import org.springframework.stereotype.Component;

/**
 * @author Thomas Pasch
 */
@Component
public class SourceScannerComponent {

	private static final Logger LOG = LoggerFactory.getLogger(SourceScannerComponent.class);

	private SourceScannerTask instance;

	SourceScannerComponent() {
	}
	
	private synchronized SourceScannerTask get() {
		if (instance == null) {
			start();
		}
		return instance;
	}
	
	public GeneratedFile parseFile(File file) throws IOException {
		return get().parseFile(file, true);
	}
	
	public void updateCode(GeneratedFile file) throws CommonBusinessException {
		get().updateCode(file);
	}
	
	public EntityObjectVO createEventSupportRule(GeneratedFile gf) throws IOException, CommonBusinessException {
		return get().createCode(gf);
	}
	
	public synchronized void cancel() {
		if (instance != null) {
			instance.cancel();
			instance = null;
		}
	}
	
	public synchronized void start() {
		this.cancel();
		if (NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_DEVELOPMENT)) {
			instance = new SourceScannerTask();
			instance.run();
		}
	}

	public boolean isRunning() {
		SourceScannerTask scanner = instance;
		if (scanner != null) {
			return scanner.isRunning();
		}
		return false;
	}
	
	public synchronized void runOnce() {
		SourceScannerTask scanner = instance;
		if (scanner == null) {
			scanner = new SourceScannerTask();
		}
		scanner.runOnce();
	}
	
}
