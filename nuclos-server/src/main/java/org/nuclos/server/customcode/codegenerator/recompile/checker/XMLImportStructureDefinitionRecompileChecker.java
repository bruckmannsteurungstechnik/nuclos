package org.nuclos.server.customcode.codegenerator.recompile.checker;

import org.nuclos.common.E;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.CompositeRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.FieldRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.PKRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.UIDFieldRecompileChecker;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.stereotype.Component;

/**
 * Checks if a modification to a xml import must cause a recompile.
 */
@Component
public class XMLImportStructureDefinitionRecompileChecker extends CompositeRecompileChecker {

    public XMLImportStructureDefinitionRecompileChecker() {
        super(new PKRecompileChecker(),
              new FieldRecompileChecker<>(E.XML_IMPORT.name),
              new UIDFieldRecompileChecker(E.XML_IMPORT.nuclet));
    }

    @Override
    public boolean isRecompileRequiredOnInsertOrDelete(final MasterDataVO<?> mdvo) {
        // new or deleted xml imports force a recompile
        return true;
    }

}
