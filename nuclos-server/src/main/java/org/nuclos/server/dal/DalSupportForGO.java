//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SF;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.attribute.DynamicAttributeVO;
import org.nuclos.common.collect.collectable.CollectableFieldFormat;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.report.valueobject.CalcAttributeUtils;
import org.nuclos.common.security.Permission;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.attribute.valueobject.AttributeCVO;
import org.nuclos.server.common.AttributeCache;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.genericobject.ejb3.GenericObjectFacadeLocal;
import org.nuclos.server.genericobject.valueobject.CanonicalAttributeFormat;
import org.nuclos.server.genericobject.valueobject.GenericObjectVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.springframework.context.ApplicationContext;

/**
 * Server-side functions to transform a GenericObjectVO into a EntityObjectVO and vice versa.
 */
public class DalSupportForGO {
	
	private static final ApplicationContext CONTEXT = SpringApplicationContextHolder.getApplicationContext();

	private static final GenericObjectFacadeLocal GO_FACADE = CONTEXT.getBean(GenericObjectFacadeLocal.class);

	private static final MetaProvider META_PROVIDER = CONTEXT.getBean(MetaProvider.class);

	public static IEntityObjectProcessor<Long> getEntityObjectProcesserForGenericObject(Long genericObjectId) {
		try {
			UID entity = GO_FACADE.getModuleContainingGenericObject(genericObjectId);
			return NucletDalProvider.getInstance().getEntityObjectProcessor(entity);
		} catch(Exception e) {
			throw new CommonFatalException(e);
		}
	}

	public static IEntityObjectProcessor<Long> getEntityObjectProcessor(UID module) {
		return NucletDalProvider.getInstance().getEntityObjectProcessor(module);
	}

	public static EntityObjectVO<Long> getEntityObject(Long genericObjectId) {
		return getEntityObjectProcesserForGenericObject(genericObjectId).getByPrimaryKey(genericObjectId);
	}
	
	public static EntityObjectVO getEntityObject(Long genericObjectId, Set<FieldMeta<?>> fields) {
		return getEntityObjectProcesserForGenericObject(genericObjectId).getByPrimaryKey(genericObjectId, fields);
	}

	public static EntityObjectVO<Long> getEntityObjectThin(Long genericObjectId, UID module) {
		IEntityObjectProcessor<Long> processor = getEntityObjectProcessor(module);
		try {
			processor.setThinReadEnabled(true);
			return processor.getByPrimaryKey(genericObjectId);
		} finally {
			processor.setThinReadEnabled(false);
		}
	}

	public static EntityObjectVO<Long> getEntityObject(Long genericObjectId, UID module) {
		return getEntityObjectProcessor(module).getByPrimaryKey(genericObjectId);
	}

	public static GenericObjectVO getGenericObjectThin(Long genericObjectId, UID module) throws CommonFinderException {
		EntityObjectVO<Long> eo = getEntityObjectThin(genericObjectId, module);
		if (eo == null) {
			throw createFinderExceptionForGO(genericObjectId, module);
		}
		return getGenericObjectVO(eo);
	}
	
	public static GenericObjectVO getGenericObject(Long genericObjectId, UID module) throws CommonFinderException {
		EntityObjectVO<Long> eo = module == null ? getEntityObject(genericObjectId) : getEntityObject(genericObjectId, module);
		if (eo == null) {
			throw createFinderExceptionForGO(genericObjectId, module);
		}
		return getGenericObjectVO(eo);
	}

	/**
	 *
	 * @param genericObjectId the PK
	 * @param module, can be null, then T_UD_GENERICOBJECT will be queried
	 * @return CommonFinderException to throw;q
	 */
	private static CommonFinderException createFinderExceptionForGO(Long genericObjectId, UID module) {
		String entity = "<unknown>";
		try {
			if (module == null) {
				module = GO_FACADE.getModuleContainingGenericObject(genericObjectId);
			}
			entity = SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(META_PROVIDER.getEntity(module));
		} catch (Exception e2) {
			// ignore
		}
		String msg = StringUtils.getParameterizedExceptionMessage(
				"common.exception.novabitfinderexception.message", entity, genericObjectId);
		return new CommonFinderException(msg);
	}

	public static Class<?> getClassFromAttribute(UID attribute) {
		return AttributeCache.getInstance().getAttribute(attribute).getJavaClass();
	}

	public static DynamicAttributeVO getDynamicAttributeVO(EntityObjectVO<Long> eo, UID field) {
		return new DynamicAttributeVO(field, eo.getFieldId(field), eo.getFieldUid(field), eo.getFieldValue(field));
	}

	public static boolean isEntityFieldForeign(UID field) {
		 return AttributeCache.getInstance().getAttribute(field).getExternalEntity()!=null;
	}

	public static String convertToCanonicalAttributeValue(UID attribute, Object value) {
		return convertToCanonicalAttributeValue(getClassFromAttribute(attribute), value);
	}

	public static String convertToCanonicalAttributeValue(Class<?> clzz, Object value) {
		return DynamicAttributeVO.getCanonicalFormat(clzz).format(value);
	}

	public static Object convertFromCanonicalAttributeValue(UID attribute, String sCanonicalValue) {
		return convertFromCanonicalAttributeValue(getClassFromAttribute(attribute), sCanonicalValue);
	}

	public static Object convertFromCanonicalAttributeValue(Class<?> clzz, String sCanonicalValue) {
		try {
			return DynamicAttributeVO.getCanonicalFormat(clzz).parse(sCanonicalValue);
		} catch(Exception e) {
			throw new CommonFatalException(e);
		}
	}

	/**
	 * Attention: Update flag is never set.
	 * 
	 * @deprecated Does not honour dependant objects. Consider reimplementation along the lines of
	 * 		wrapGenericObjectVO of client.
	 */
	public static EntityObjectVO<Long> wrapGenericObjectVO(GenericObjectVO go) {
		EntityObjectVO<Long> eo = new EntityObjectVO<Long>(go.getModule());

		eo.setPrimaryKey(go.getId());
		eo.setCreatedBy(go.getCreatedBy());
		eo.setCreatedAt(InternalTimestamp.toInternalTimestamp(go.getCreatedAt()));
		eo.setChangedBy(go.getChangedBy());
		eo.setChangedAt(InternalTimestamp.toInternalTimestamp(go.getChangedAt()));
		eo.setVersion(go.getVersion());

		eo.setDataLanguageMap(go.getDataLanguageMap());
		
		for (DynamicAttributeVO attr : go.getAttributes()) {
			final UID field = attr.getAttributeUID();
			if (attr.isRemoved()) {
				eo.setFieldValue(field, null);
				eo.setFieldId(field, null);
				eo.setFieldUid(field, null);
			} else {
				eo.setFieldValue(field, attr.getValue());
				if (attr.getValueId() != null) {
					eo.setFieldId(field, attr.getValueId());
				}
				if (attr.getValueUid() != null) {
					eo.setFieldUid(field, attr.getValueUid());
				}
			}
		}
		eo.setFieldValue(SF.LOGICALDELETED, go.isDeleted());
		
		eo.setCanWrite(go.getCanWrite());
		eo.setCanStateChange(go.getCanStateChange());
		eo.setCanDelete(go.getCanDelete());

		return eo;
	}

	public static GenericObjectWithDependantsVO getGenericObjectWithDependants(EntityObjectVO eo) {
		return new GenericObjectWithDependantsVO(getGenericObjectVO(eo), eo.getDependents(), eo.getDataLanguageMap());
	}
	
	/**
	 * Attention: Update flag is never set.
	 */
	public static EntityObjectVO<Long> wrapGenericObjectVO(GenericObjectWithDependantsVO go) {
		EntityObjectVO<Long> eo = new EntityObjectVO<Long>(go.getModule());

		eo.setPrimaryKey(go.getId());
		eo.setCreatedBy(go.getCreatedBy());
		eo.setCreatedAt(InternalTimestamp.toInternalTimestamp(go.getCreatedAt()));
		eo.setChangedBy(go.getChangedBy());
		eo.setChangedAt(InternalTimestamp.toInternalTimestamp(go.getChangedAt()));
		eo.setVersion(go.getVersion());

		for (DynamicAttributeVO attr : go.getAttributes()) {
			final UID field = attr.getAttributeUID();
			if (attr.isRemoved()) {
				eo.setFieldValue(field, null);
				eo.setFieldId(field, null);
				eo.setFieldUid(field, null);
			} else {
				eo.setFieldValue(field, attr.getValue());
				if (attr.getValueId() != null) {
					eo.setFieldId(field, attr.getValueId());
				}
				if (attr.getValueUid() != null) {
					eo.setFieldUid(field, attr.getValueUid());
				}
			}
		}
		eo.setFieldValue(SF.LOGICALDELETED, go.isDeleted());
		
		eo.setCanWrite(go.getCanWrite());
		eo.setCanStateChange(go.getCanStateChange());
		eo.setCanDelete(go.getCanDelete());
		
		eo.setDependents(go.getDependents());
		
		eo.setDataLanguageMap(go.getDataLanguageMap());
		
		return eo;
	}

	/**
	 * 
	 */
	public static GenericObjectWithDependantsVO getGenericObjectWithDependantsVO(EntityObjectVO<Long> eo) {
		return new GenericObjectWithDependantsVO(getGenericObjectVO(eo), 
			eo.getDependents() != null ? eo.getDependents() : new DependentDataMap(), eo.getDataLanguageMap());
	}
	
	/**
	 * @deprecated Does not honour dependant objects. Consider reimplementation along the lines of
	 * 		getGenericObjectWithDependantsVO of client.
	 */
	public static GenericObjectVO getGenericObjectVO(EntityObjectVO<Long> eo) {
		final NuclosValueObject<Long> nvo;
		if (eo.getId() != null) {
			nvo = new NuclosValueObject<Long>(eo.getPrimaryKey(), eo.getCreatedAt(), eo.getCreatedBy(), eo.getChangedAt(), eo.getChangedBy(), eo.getVersion());
		}
		else {
			nvo = new NuclosValueObject<Long>();
		}
		Object bLogicalDeleted = eo.getFieldValue(SF.LOGICALDELETED.getUID(eo.getDalEntity()));
		if (bLogicalDeleted == null) bLogicalDeleted = Boolean.FALSE;
		final GenericObjectVO go = new GenericObjectVO(nvo, eo.getDalEntity(), (Boolean) bLogicalDeleted);
		go.setComplete(eo.isComplete());

		Collection<DynamicAttributeVO> attrVOList = new ArrayList<DynamicAttributeVO>();
		
		Set<UID> setFields = new HashSet<UID>();
		setFields.addAll(eo.getFieldValues().keySet());
		setFields.addAll(eo.getFieldIds().keySet());
		setFields.addAll(eo.getFieldUids().keySet());
		
		Map<UID, FieldMeta<?>> mapFields = META_PROVIDER.getAllEntityFieldsByEntity(eo.getDalEntity());

		for (UID field : setFields) {
			if(mapFields.containsKey(field)) {
				attrVOList.add(new DynamicAttributeVO(field, eo.getFieldId(field), eo.getFieldUid(field), eo.getFieldValue(field)));
			} else {
				if (CalcAttributeUtils.isCalcAttributeCustomization(field)) {
					attrVOList.add(new DynamicAttributeVO(field, eo.getFieldId(field), eo.getFieldUid(field), eo.getFieldValue(field)));
				}
			}
		}

		if (!setFields.contains(SF.CREATEDAT.getUID(eo.getDalEntity())))
			attrVOList.add(new DynamicAttributeVO(SF.CREATEDAT.getUID(eo.getDalEntity()), null, null, eo.getCreatedAt()));
		if (!setFields.contains(SF.CREATEDBY.getUID(eo.getDalEntity())))
			attrVOList.add(new DynamicAttributeVO(SF.CREATEDBY.getUID(eo.getDalEntity()), null, null, (Object)eo.getCreatedBy()));
		if (!setFields.contains(SF.CHANGEDAT.getUID(eo.getDalEntity())))
			attrVOList.add(new DynamicAttributeVO(SF.CHANGEDAT.getUID(eo.getDalEntity()), null, null, eo.getChangedAt()));
		if (!setFields.contains(SF.CHANGEDBY.getUID(eo.getDalEntity())))
			attrVOList.add(new DynamicAttributeVO(SF.CHANGEDBY.getUID(eo.getDalEntity()), null, null, (Object)eo.getChangedBy()));
		
		go.setCanWrite(eo.getCanWrite());
		go.setCanStateChange(eo.getCanStateChange());
		go.setCanDelete(eo.getCanDelete());
	
		go.setAttributes(attrVOList);
		
		go.setDataLanguageMap(eo.getDataLanguageMap());
		
		return go;
	}

	/**
	 * Same as {@link #getGenericObjectVO} but without querying the {@link MetaProvider} for fields.
	 * Hence it can be used in a unit test that has no entity/field meta data information available. 
	 * 
	 * @deprecated ONLY FOR JUNIT TESTS. DON'T USE IN REAL LIFE! Does not honour dependant objects. 
	 * 		Consider reimplementation along the lines of getGenericObjectWithDependantsVO of client.
	 * @author Thomas Pasch
	 */
	public static GenericObjectVO getGenericObjectVOForUnitTest(EntityObjectVO<Long> eo) {
		final NuclosValueObject<Long> nvo;
		if (eo.getId() != null) {
			nvo = new NuclosValueObject<Long>(eo.getPrimaryKey(), eo.getCreatedAt(), eo.getCreatedBy(), eo.getChangedAt(), eo.getChangedBy(), eo.getVersion());
		}
		else {
			nvo = new NuclosValueObject<Long>();
		}
		Object bLogicalDeleted = eo.getFieldValue(SF.LOGICALDELETED.getUID(eo.getDalEntity()));
		if (bLogicalDeleted == null) bLogicalDeleted = Boolean.FALSE;
		final GenericObjectVO go = new GenericObjectVO(nvo, eo.getDalEntity(), (Boolean) bLogicalDeleted);
		go.setComplete(eo.isComplete());

		Collection<DynamicAttributeVO> attrVOList = new ArrayList<DynamicAttributeVO>();
		
		Set<UID> setFields = new HashSet<UID>();
		setFields.addAll(eo.getFieldValues().keySet());
		setFields.addAll(eo.getFieldIds().keySet());
		setFields.addAll(eo.getFieldUids().keySet());

		for (UID field : setFields) {
			// if(META_PROVIDER.getAllEntityFieldsByEntity(eo.getDalEntity()).containsKey(field)) {
				attrVOList.add(new DynamicAttributeVO(field, eo.getFieldId(field), eo.getFieldUid(field), eo.getFieldValue(field)));
			// }
		}

		if (!setFields.contains(SF.CREATEDAT.getUID(eo.getDalEntity())))
			attrVOList.add(new DynamicAttributeVO(SF.CREATEDAT.getUID(eo.getDalEntity()), null, null, eo.getCreatedAt()));
		if (!setFields.contains(SF.CREATEDBY.getUID(eo.getDalEntity())))
			attrVOList.add(new DynamicAttributeVO(SF.CREATEDBY.getUID(eo.getDalEntity()), null, null, (Object)eo.getCreatedBy()));
		if (!setFields.contains(SF.CHANGEDAT.getUID(eo.getDalEntity())))
			attrVOList.add(new DynamicAttributeVO(SF.CHANGEDAT.getUID(eo.getDalEntity()), null, null, eo.getChangedAt()));
		if (!setFields.contains(SF.CHANGEDBY.getUID(eo.getDalEntity())))
			attrVOList.add(new DynamicAttributeVO(SF.CHANGEDBY.getUID(eo.getDalEntity()), null, null, (Object)eo.getChangedBy()));
	
		go.setAttributes(attrVOList);
		return go;
	}

	public static AttributeCVO getAttributeCVO(FieldMeta<?> efMeta, Map<UID, Permission> permissions) {
		final Object defaultValue;
		try {
			final Class<?> clazz = Class.forName(efMeta.getDataType());
			final String dVal = efMeta.getDefaultValue();
			final CanonicalAttributeFormat caf = DynamicAttributeVO.getCanonicalFormat(clazz);
			if (StringUtils.isNullOrEmpty(dVal) || clazz.equals(Date.class)) {
				defaultValue = caf.parse(dVal);
			} else {
				final CollectableFieldFormat cff = CollectableFieldFormat.getInstance(clazz);
				final String fInput = efMeta.getFormatInput();
				defaultValue = caf.parse(cff.parse(fInput, dVal).toString());
			}
		}
		catch (ClassNotFoundException ex) {
			throw new NuclosFatalException(StringUtils.getParameterizedExceptionMessage("vofactory.exception.1", efMeta.getDataType()), ex);
		}
		catch (CommonValidationException ex) {
			throw new NuclosFatalException(StringUtils.getParameterizedExceptionMessage("vofactory.exception.2", efMeta.getFieldName(), efMeta.getDefaultValue()), ex);
		}

		final AttributeCVO vo = new AttributeCVO(
			new NuclosValueObject<UID>(
					efMeta.getUID(),
					null,
					null,
					null,
					null,
					null),
			efMeta.getFieldGroup(),
			efMeta.getCalcFunction(),
			efMeta.getCalcAttributeDS(),
			efMeta.getCalcAttributeParamValues(),
			efMeta.isCalcOndemand(),
			efMeta.isCalcAttributeAllowCustomization(),
			efMeta.getFieldName(),
			efMeta.getFallbackLabel(),
			null /** TODO: Description*/,
			efMeta.getDataType(),
			efMeta.getScale(),
			efMeta.getPrecision(),
			efMeta.getFormatInput(),
			efMeta.getFormatOutput(),
			efMeta.isNullable(),
			efMeta.isSearchable(),
			efMeta.isModifiable(),
			efMeta.isInsertable(),
			efMeta.isLogBookTracking(),
			SF.getByField(efMeta.getFieldName()) != null,
			efMeta.isShowMnemonic(),
			(efMeta.getForeignEntity() != null ? efMeta.getDefaultForeignId() : null),
			(efMeta.getForeignEntity() != null ? efMeta.getDefaultForeignUid() : null),
			defaultValue,
			efMeta.getSortorderASC(),
			efMeta.getSortorderDESC(),
			efMeta.getForeignEntity() != null ? efMeta.getForeignEntity() : efMeta.getLookupEntity(),
			efMeta.getForeignEntityField() != null ? efMeta.getForeignEntityField() : efMeta.getLookupEntityField(),
			(permissions == null ? new HashMap<UID, Permission>() : permissions),
			efMeta.getLocaleResourceIdForLabel(),
			efMeta.getLocaleResourceIdForDescription(),
			efMeta.getDefaultComponentType());

		return vo;
	}
}
