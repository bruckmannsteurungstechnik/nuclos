//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor;

import java.text.MessageFormat;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.IDalVO;
import org.nuclos.common.dal.vo.IDalWithFieldsVO;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.dal.processor.jdbc.TableAliasSingleton;
import org.nuclos.server.dblayer.impl.util.PreparedStringBuilder;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.query.DbReferencedCompoundColumnExpression;
import org.nuclos.server.dblayer.query.DbReferencedSubselectExpression;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;

/**
 * Map a database column as a 'stringified' reference. 
 * <p>
 * In the general case, 'stringified' references are compound. Hence it is not 
 * possible to write to the database with this mapping class.
 * This is part of the effort to deprecate all views in Nuclos.
 * </p>
 * @param <T> Java type for the data in this column of the database.
 */
public final class ColumnToRefFieldVOMapping<T extends Object, PK> extends AbstractColumnToVOMapping<T, PK>
	implements IColumnWithMdToVOMapping<T, PK>
{

	private final FieldMeta<T> field;

	/**
	 * Konstruktor für dynamische VO Werte (Die Werte werden in einer "Fields"-Liste gespeichert).
	 * @throws ClassNotFoundException 
	 */
	public ColumnToRefFieldVOMapping(String tableAlias, FieldMeta<T> field) throws ClassNotFoundException {
		super(tableAlias, DbReferencedCompoundColumnExpression.getRefAlias(field), field.getUID(), 
				field.getJavaClass()==UID.class && !field.isFileDataType() ? String.class.getName() : field.getDataType(), 
						field.isReadonly(), field.isDynamic());
		this.field = field;
	}
	
	public static <PK, T> DbExpression<T> getOwnerSubselectColumn(FieldMeta<T> ownerFieldMeta, DbFrom<PK> from, DbQueryBuilder builder) {
		final String idSql = ColumnToFieldIdVOMapping.getOwnerIdColumn(ownerFieldMeta, from, false).getSqlString().toString();
		final String tableAliasForField = TableAliasSingleton.getInstance().getAlias(ownerFieldMeta);
		final PreparedStringBuilder subselect = DbReferencedSubselectExpression.mkSubselect(from, tableAliasForField, ownerFieldMeta, idSql);
		return new DbExpression<T>(builder, ownerFieldMeta.getJavaClass(), subselect);
	}
	
	public static <PK, T> DbExpression<T> getOwnerRefColumn(FieldMeta<T> ownerFieldMeta, DbFrom<PK> from) {
		final String tableAlias = from.getAlias();
		final DbQueryBuilder builder = from.getQuery().getBuilder();
		return getOwnerRefColumn(ownerFieldMeta, tableAlias, builder);
	}
	
	public static <PK, T> DbExpression<T> getOwnerRefColumn(FieldMeta<T> ownerFieldMeta, String tableAlias, DbQueryBuilder builder) {
		final String subsql = MessageFormat.format(ownerFieldMeta.getCalcFunction(), tableAlias, ownerFieldMeta.getEntity().getString());
		return new DbReferencedCompoundColumnExpression<T>(builder, ownerFieldMeta.getJavaClass(), null, new PreparedStringBuilder(subsql));
	}

	@Override
	public DbExpression<T> getDbColumn(DbFrom from) {
		final DbExpression<T> result;
		if (!field.isCalculated()) {
			result = new DbReferencedCompoundColumnExpression<T>(from, field, getJavaClass(), true);
		}
		else {
			if (SF.OWNER.checkField(field.getEntity(), field.getUID())) {
				result = getOwnerRefColumn(field, from);
			} else {
				throw new IllegalStateException("References cannot be calculated attributes");
			}
		}
		return result;
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("col=").append(getColumn());
		result.append(", tableAlias=").append(getTableAlias());
		result.append(", field=").append(field);
		if (getDataType() != null)
			result.append(", type=").append(getDataType().getName());
		result.append("]");
		return result.toString();
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof ColumnToRefFieldVOMapping)) return false;
		final ColumnToRefFieldVOMapping<T, PK> other = (ColumnToRefFieldVOMapping<T, PK>) o;
		return field.equals(other.field);
	}
	
	@Override
	public int hashCode() {
		int result = getColumn().hashCode();
		result += 3 * field.hashCode();
		return result;
	}

	@Override
	public FieldMeta<?> getMeta() {
		return field;
	}

	public UID getUID() {
		return field.getUID();
	}

	@Override
	public Object convertFromDalFieldToDbValue(IDalVO<PK> dal) {
		throw new CommonFatalException("Can't write to ref field " + field + " value " + dal);
	}

	@Override
	public void convertFromDbValueToDalField(IDalVO<PK> result, T o, final FromDbValueConversionParams params) {
		final IDalWithFieldsVO<Object, PK> realDal = (IDalWithFieldsVO<Object, PK>) result;
		try {
			Object value = convertFromDbValue(o, getColumn(), getDataType(), result.getPrimaryKey(), params);
			// Sonderbehandlung Dokumentenanhang
			if (getDataType() == GenericObjectDocumentFile.class) {
				GenericObjectDocumentFile documentFile = (GenericObjectDocumentFile)value;
				if (documentFile != null) {
					documentFile.setDocumentFilePk(realDal.getFieldUid(field.getUID()));
				}
			}
			realDal.setFieldValue(field.getUID(), value);
		} catch (Exception e) {
			throw new CommonFatalException(e);
		}
	}

	@Override
	public boolean constructJoinForStringifiedRefs() {
		return true;
	}

}
