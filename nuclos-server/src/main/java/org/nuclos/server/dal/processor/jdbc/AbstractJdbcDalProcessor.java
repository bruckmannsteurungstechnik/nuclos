//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor.jdbc;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.nuclos.common.CloneUtils;
import org.nuclos.common.DbField;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosLogicalUniqueViolationException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SF;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.JdbcTransformerParams;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.DalCallResult;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.IDalVO;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.common.NuclosUserDetailsContextHolder;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.SessionUtils;
import org.nuclos.server.dal.processor.AbstractDalProcessor;
import org.nuclos.server.dal.processor.ColumnToBeanVOMapping;
import org.nuclos.server.dal.processor.ColumnToLanguageFieldVOMapping;
import org.nuclos.server.dal.processor.ColumnToRefFieldVOMapping;
import org.nuclos.server.dal.processor.FromDbValueConversionParams;
import org.nuclos.server.dal.processor.IColumnToVOMapping;
import org.nuclos.server.dal.processor.IColumnWithMdToVOMapping;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbAccess;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.IBatch;
import org.nuclos.server.dblayer.expression.DbNull;
import org.nuclos.server.dblayer.query.DbCondition;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.query.DbReferencedSubselectExpression;
import org.nuclos.server.dblayer.statements.DbDeleteStatement;
import org.nuclos.server.dblayer.statements.DbInsertStatement;
import org.nuclos.server.dblayer.statements.DbInsertWithSelectStatement;
import org.nuclos.server.dblayer.statements.DbMap;
import org.nuclos.server.dblayer.statements.DbStatement;
import org.nuclos.server.dblayer.statements.DbTableStatement;
import org.nuclos.server.dblayer.statements.DbUpdateStatement;
import org.nuclos.server.masterdata.ejb3.MasterDataRestFqnCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractJdbcDalProcessor<DalVO extends IDalVO<PK>, PK> extends AbstractDalProcessor<DalVO, PK> {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractJdbcDalProcessor.class);

	// Spring injection
	
	protected SpringDataBaseHelper dataBaseHelper;
	
	protected NuclosUserDetailsContextHolder userCtx;

	// end of Spring injection

	// This must be cloned and hence cannot be final.
	protected List<IColumnToVOMapping<?, PK>> allColumns;
	private Set<IColumnToVOMapping<?, PK>> allColumnsAsSet;

	public AbstractJdbcDalProcessor(UID entity, Class<? extends IDalVO<PK>> type, Class<PK> pkType, List<IColumnToVOMapping<?, PK>> allColumns) {
		super(entity, (Class<DalVO>) type, pkType);

		this.allColumns = allColumns;
		this.allColumnsAsSet = new HashSet<>(allColumns);
		checkColumns();
	}
	
	protected SessionUtils utils;
	
	//NOTE THAT AUTOWIRED DOES NOT WORK HERE, IT IS SET MANUALLY
	public void setSessionUtils(SessionUtils utils) {
		this.utils = utils;
	}
	
	@Autowired
	public void setDataBaseHelper(SpringDataBaseHelper dataBaseHelper) {
		this.dataBaseHelper = dataBaseHelper;
	}
	
	private ParameterProvider parameterProvider;
	
	private ParameterProvider getParameterProvider() {
		if (parameterProvider == null) {
			parameterProvider = ServerParameterProvider.getInstance();
		}
		return parameterProvider;
	}
	
	//This is pessimistic, means default = false
	protected final boolean usePrimaryKeySubSelect() {
		ParameterProvider parameterProvider = getParameterProvider();
		if (parameterProvider != null) {
			return "true".equals(parameterProvider.getValue(ParameterProvider.USE_PK_SUBSELECT));
		}
		return false;
	}

	//This is optimistic, means default = true
	protected final boolean useReferenceSubSelect() {
		ParameterProvider parameterProvider = getParameterProvider();
		if (parameterProvider != null) {
			return !"false".equals(parameterProvider.getValue(ParameterProvider.USE_REF_SUBSELECT));
		}
		return true;
	}

	public Object clone() {
		final AbstractJdbcDalProcessor<DalVO, PK> clone;
		try {
			clone = (AbstractJdbcDalProcessor<DalVO, PK>) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new IllegalStateException(e.toString());
		}
		clone.allColumns = CloneUtils.cloneCollection(allColumns);
		clone.allColumnsAsSet = CloneUtils.cloneCollection(allColumnsAsSet);
		return clone;
	}

   private void checkColumns() {
      if (allColumns.size() != allColumnsAsSet.size()) {
    	  LOG.warn("Duplicates in columns, size is "  + allColumns.size() + " but only " + allColumnsAsSet.size() + " unique elements");
    	  assert false;
      }
   }

   protected abstract EntityMeta<PK> getMetaData();

   protected abstract IColumnToVOMapping<PK, PK> getPrimaryKeyColumn();

   protected List<DalVO> getAll() {
      return getAll(allColumns);
   }

	public List<PK> getAllIds() {
		DbQuery<PK> query = createSingleColumnQuery(getPrimaryKeyColumn(), true);
		return dataBaseHelper.getDbAccess().executeQuery(query);
	}

   protected List<DalVO> getAll(final List<IColumnToVOMapping<?, PK>> columns) {
      DbQuery<Object[]> query = createQuery(columns, true, null);
      return dataBaseHelper.getDbAccess().executeQuery(query, createResultTransformer(columns));
   }

	public DalVO getByPrimaryKey(final PK pk, Collection<FieldMeta<?>> fields) {
		return getByPrimaryKey(pk);
	}

   public DalVO getByPrimaryKey(final PK pk) {
      return getByPrimaryKey(allColumns, pk);
   }

   protected DalVO getByPrimaryKey(final List<IColumnToVOMapping<?, PK>> columns, final PK pk) {
      final List<DalVO> result = getByColumn(columns, getPrimaryKeyColumn(), pk);
      if (result.size() == 1)
         return result.get(0);
      if (result.size() == 0)
         return null;
      throw new CommonFatalException("Primary key is not unique!");
   }

   protected <T> List<DalVO> getByColumn(final List<IColumnToVOMapping<?, PK>> columns, IColumnToVOMapping<T, PK> column, final T t) {
      DbQuery<Object[]> query = createQuery(columns, true, null);
      DbFrom<PK> from = (DbFrom<PK>) CollectionUtils.getFirst(query.getRoots());
      query.where(query.getBuilder().equalValue(column.getDbColumn(from), t));
      return dataBaseHelper.getDbAccess().executeQuery(query, createResultTransformer(columns));
   }

   protected List<DalVO> getByPrimaryKeys(final List<IColumnToVOMapping<?, PK>> columns, final List<PK> ids) {
      DbQuery<Object[]> query = createQuery(columns, true, null);
      DbFrom<PK> from = (DbFrom<PK>) CollectionUtils.getFirst(query.getRoots());
      DbExpression<?> pkExpr = getPrimaryKeyColumn().getDbColumn(from);
      Transformer<Object[], DalVO> transformer = createResultTransformer(columns);

      List<DalVO> result = new ArrayList<>(ids.size());
      for (List<PK> idSubList : CollectionUtils.splitEvery(ids, query.getBuilder().getInLimit())) {
         query.where(pkExpr.as(getPkType()).in(idSubList));
         result.addAll(dataBaseHelper.getDbAccess().executeQuery(query, transformer));
      }
      return result;
   }

   /*
    *
    */

	protected Object insertOrUpdate(final DalVO dalVO) {
	   return this.insertOrUpdateImpl(allColumns, dalVO, null);
   }
   protected <S> void insertOrUpdate(final List<IColumnToVOMapping<?, PK>> columns, final DalVO dalVO) {
	   this.insertOrUpdateImpl(columns, dalVO, null);
   }

   //This method can be used by a future "batchInsertOrUpdate" with DalCallResult dcr provided. If null, the first exception is thrown

	protected Object insertOrUpdateImpl(final List<IColumnToVOMapping<?, PK>> columns, DalVO dalVO, final DalCallResult dcr) {
         DbMap columnValueMap = getColumnValuesMap(columns, dalVO);
         DbTableStatement<PK> stmt = null;
         if (dalVO.isFlagNew()) {
            stmt = new DbInsertStatement<>(getMetaData(), columnValueMap);
         } else if (dalVO.isFlagUpdated()) {
            stmt = new DbUpdateStatement<>(getMetaData(), columnValueMap, getPrimaryKeyMap(dalVO.getPrimaryKey()));
         }
         if (stmt != null) {
            try {
               dataBaseHelper.getDbAccess().execute(stmt);
               invalidateFqnCaches();
            } catch (DbException ex) {
            	ex = transformInsertOrUpdateException(ex, dalVO);
            	if (dcr == null) {
            		throw ex;
            	}
            	ex.setPkIfNull(dalVO.getPrimaryKey());
            	ex.setStatementsIfNull(getLogStatements(stmt));
				dcr.addRuntimeException(ex);
            }
         }
         return dalVO.getPrimaryKey();
	   }
    //This method can be used by a future "batchDelete" with DalCallResult dcr provided. If null, the first exception is thrown

	protected void deleteImpl(final Delete<PK> del, final DalCallResult dcr) {
		PK pk = del.getPrimaryKey();
		DbStatement stmt = new DbDeleteStatement<>(getMetaData(), getPrimaryKeyMap(pk));
		try {
			dataBaseHelper.getDbAccess().execute(stmt);
			invalidateFqnCaches();
		} catch(DbException ex) {
			ex = transformDeleteException(ex, pk);
			if (dcr == null) {
				throw ex;
			}
        	ex.setPkIfNull(pk);
        	ex.setStatementsIfNull(getLogStatements(stmt));
			dcr.addRuntimeException(ex);
		}
   }
   protected void delete(final Delete<PK> del) throws DbException {
	   this.deleteImpl(del, null);
   }

   public DalCallResult insertWithSelect(final DbMap fieldsMap, final DbMap fieldConditionMap, EntityMeta<?> metaEntitySource) {

	   if (getMetaData().getVirtualEntity() != null || metaEntitySource.getVirtualEntity() != null) {
		   throw new CommonFatalException("Virtual entities are not allowed in this InsertWithSlect-Statements");
	   }

	   DalCallResult dcr = new DalCallResult();

	   DbTableStatement<PK> stmt = new DbInsertWithSelectStatement<PK>(
			   getMetaData().getDbTable(), metaEntitySource.getDbTable(), fieldsMap, fieldConditionMap);

	   try {
		  dataBaseHelper.getDbAccess().execute(stmt);
		  invalidateFqnCaches();
	   } catch (DbException ex) {
		   dcr.addRuntimeException(ex);
	   }

	   return dcr;
   }

	private boolean doSortCalculatedAttribues() {
		ParameterProvider provider = getParameterProvider();
		if (provider != null) {
			return "true".equalsIgnoreCase(provider.getValue(ParameterProvider.SORT_CALCULATED_ATTRIBUTES));
		}
		return false;
	}

	private Integer getQueryTimeout() {
		ParameterProvider provider = getParameterProvider();
		if (provider != null) {
			String value = provider.getValue(ParameterProvider.QUERY_TIMEOUT);
			if (value != null) {
				try {
					return Integer.parseInt(value);
				} catch (NumberFormatException nfe) {
					LOG.warn(nfe.getMessage(), nfe);
				}
			}
		}
		return null;
	}

	private int getIntiatorId() {
		return LangUtils.hashCode(utils.getCurrentUserName()) ^ getEntityUID().hashCode();
	}

	public void cancelRunningStatements() {
		dataBaseHelper.getDbAccess().cancelRunningStatements(getIntiatorId());
	}

	protected final <T> DbQuery<T> createQueryImpl(Class<T> clazz) {
	   DbQuery<T> query = dataBaseHelper.getDbAccess().getQueryBuilder().createQuery(clazz, doSortCalculatedAttribues());
	   query.setTimeout(getQueryTimeout());
	   query.setInitiatorId(getIntiatorId());
	   return query;
	}

	protected final <S> DbQuery<S> createSingleColumnQuery(IColumnToVOMapping<S, PK> column, boolean overrideDbSourceUseDML) {
		DbQuery<S> query = createQueryImpl(column.getDataType());
		DbFrom<PK> from = query.from(getMetaData(), overrideDbSourceUseDML);
		adjustFrom(from);
		getUserContext();
		query.select(column.getDbColumn(from));
		return query;
	}

	protected DbQuery<Long> createCountQuery(IColumnToVOMapping<PK, PK> column) {
		DbQuery<Long> query = createQueryImpl(Long.class);
		DbFrom<PK> from = query.from(getMetaData());
		adjustFrom(from);

		// Ein Count sollte sich doch nicht durch Joins auf 'zusaetzliche' Uebersetzungen aendern!
		// Datalang moved to EOWithLangSupportProcessor...
		// if (getMetaData().IsLocalized() && MetaProvider.getInstance().hasEntity(NucletEntityMeta.getEntityLanguageUID(getMetaData().getUID()))) {
		//  joinLocalizationTables(null, from, null, false);
		//}

		query.setSimpleCount(true);
		query.select(query.getBuilder().count(column.getDbColumn(from)));
		return query;
    }

    protected DbQuery<Long> createCountWithLimitQuery(IColumnToVOMapping<PK, PK> column, final Long limit, final Map<String, Object> mpParameters) {
   		DbQuery<Long> query = createQueryImpl(Long.class);
   		DbQuery<PK> subQuery = createQueryImpl(getPkType());

   		if (!query.getBuilder().isCountWithLimitSupported()) {
   			return null;
		}

   		DbFrom<PK> subFrom = subQuery.from(getMetaData());
   		adjustFrom(subFrom);
		// Ein Count sollte sich doch nicht durch Joins auf 'zusaetzliche' Uebersetzungen aendern!
		// Datalang moved to EOWithLangSupportProcessor...
		// if (getMetaData().IsLocalized() && MetaProvider.getInstance().hasEntity(NucletEntityMeta.getEntityLanguageUID(getMetaData().getUID()))) {
		//	joinLocalizationTables(null, subFrom, null, false);
		//}

		subQuery.select(column.getDbColumn(subFrom));
		subQuery.limit(limit + 1);

		adjustCountWithLimitSubQuery(subQuery, mpParameters);

		DbFrom<PK> from = query.from(subQuery, "t");

		query.setSimpleCount(true);
		query.select(query.getBuilder().count(column.getDbColumn(from)));
   		return query;
	}

	protected void adjustCountWithLimitSubQuery(final DbQuery<PK> subQuery, final Map<String, Object> mpParameters) {
	}

	protected DbQuery<Object[]> createQuery(List<IColumnToVOMapping<?, PK>> columns, boolean bAllLanguages,
		   Collection<IColumnWithMdToVOMapping<?, PK>> referenceColumnsAsSubselect) {
      DbQuery<Object[]> query = createQueryImpl(Object[].class);
      DbFrom<PK> from = query.from(getMetaData(), false);
      adjustFrom(from);

      List<DbExpression<?>> selections = new ArrayList<>();
      for (IColumnToVOMapping<?, PK> column : columns) {
    	  DbExpression<?> dbExpression = column.getDbColumn(from);

    	  if (referenceColumnsAsSubselect == null || !referenceColumnsAsSubselect.contains(column)) {
        	  selections.add(column.getDbColumn(from));

    	  } else {
    		  final boolean useExprSqlForCond = LangUtils.equal(column.getUID(), SF.OWNER.getUID(getMetaData()));
    		  DbExpression<?> dbSubselectExpression = new DbReferencedSubselectExpression(dbExpression, from, (IColumnWithMdToVOMapping<?, PK>)column, useExprSqlForCond);
    		  selections.add(dbSubselectExpression);
    	  }
      }

      query.multiselect(selections);
      return query;
   }

   protected <S, T> DbMap getColumnValuesMap(List<IColumnToVOMapping<?, PK>> columns, DalVO dalVO) {
      DbMap map = new DbMap();
      for (Entry<IColumnToVOMapping<?, PK>, Object> entry : getColumnValuesMapWithMapping(columns, dalVO).entrySet()) {
    	  DbField<T> dbField = (DbField<T>) entry.getKey();
    	  if ((T) entry.getValue() == null) {
    		  map.put(dbField, new DbNull<T>(dbField.getJavaClass()));
    	  } else {
    		  map.put(dbField, (T) entry.getValue());
    	  }

      }
      return map;
   }

   protected <S> Map<IColumnToVOMapping<?, PK>, Object> getColumnValuesMapWithMapping(List<IColumnToVOMapping<?, PK>> columns, DalVO dalVO) {
		final Map<IColumnToVOMapping<?, PK>, Object> map = new LinkedHashMap<>();
		final boolean bUpdate = !dalVO.isFlagNew() && dalVO.isFlagUpdated();

		for (IColumnToVOMapping<?, PK> column : columns) {
			if (column.isReadonly()) {
				continue;
			}
			// also ignore stringified references
			if (column instanceof ColumnToRefFieldVOMapping) {
				final FieldMeta<?> meta = ((ColumnToRefFieldVOMapping<?, PK>) column).getMeta();
				if (meta.isReadonly()) {
					continue;
				}
				if (meta.getForeignEntity() != null) {
					continue;
				}
				if (meta.getLookupEntity() != null) {
					continue;
				}
				if (meta.getUnreferencedForeignEntity() != null) {
					continue;
				}
			} else if (bUpdate && column instanceof ColumnToBeanVOMapping) {
				// NUCLOS-6031
				String col = column.getColumn();
				if ("INTID".equals(col) || "STRUID".equals(col) || "DATCREATED".equals(col) || "STRCREATED".equals(col)) {
					continue;
				}
			}

			map.put(column, column.convertFromDalFieldToDbValue(dalVO));
		}
		return map;
	}

   private DbMap getPrimaryKeyMap(PK pk) {
	   final DbMap result = new DbMap();
	   result.put(getPrimaryKeyColumn(), pk);
	   return result;
   }

   protected Transformer<Object[], DalVO> createResultTransformer(final JdbcTransformerParams params, List<IColumnToVOMapping<?, PK>> columns) {
	   return getResultTransformer(params, columns.toArray(new IColumnToVOMapping[columns.size()]));
   }

	protected Transformer<Object[], DalVO> createResultTransformer(final List<IColumnToVOMapping<?,PK>> columns) {
		return this.createResultTransformer(new JdbcTransformerParams(), columns);
   }

   protected <S> Transformer<Object[], DalVO> getResultTransformer(final JdbcTransformerParams params, final IColumnToVOMapping<Object, PK>... columns) {
		return new Transformer<Object[], DalVO>() {
			@Override
			public DalVO transform(Object[] result) {
				try {
					final DalVO dalVO = newDalVOInstance();
					for (int i = 0, n = columns.length; i < n; i++) {
						final IColumnToVOMapping<Object, PK> column = columns[i];
						final Object value = result[i];
						if (column instanceof ColumnToLanguageFieldVOMapping) {
							ColumnToLanguageFieldVOMapping col = (ColumnToLanguageFieldVOMapping) column;
							if (col.isForDataMap()) {
								// not here ...
							} else {
								column.convertFromDbValueToDalField(dalVO, value, new FromDbValueConversionParams(params.includeThumbnailsOnly()));
							}
						} else {
							column.convertFromDbValueToDalField(dalVO, value, new FromDbValueConversionParams(params.includeThumbnailsOnly()));
						}
					}

					dalVO.processor(getProcessor());
					setDebugInfo(dalVO);
					return dalVO;
				} catch (Exception e) {
					throw new CommonFatalException(e);
				}
			}
		};
	}

   	protected void setDebugInfo(DalVO dalVO) {
   	}

	protected NuclosLogicalUniqueViolationException checkLogicalUniqueConstraint(
			final Map<IColumnToVOMapping<?, PK>, Object> values, final PK id) {
		DbQuery<Long> query = createQueryImpl(Long.class);
		DbFrom<PK> from = query.from(getMetaData());
		adjustFrom(from);
		query.select(query.getBuilder().countRows());
		List<DbCondition> conditions = new ArrayList<>();

		boolean bFullIsNullCondition = true;
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();

		for (Map.Entry<IColumnToVOMapping<?, PK>, Object> e : values.entrySet()) {
			Object value = e.getValue();
			DbExpression<Object> c = (DbExpression<Object>) e.getKey().getDbColumn(from);
			if (DbNull.isNull(value)) {
				conditions.add(builder.isNull(c));
			} else {
				conditions.add(builder.equalValue(c, value));
				bFullIsNullCondition = false;
			}
		}

		if (bFullIsNullCondition) {
			// If all unique key fields are null, no exception is thrown (Reference: Oracle)
			return null;
		}

		query.where(builder.and(conditions.toArray(new DbCondition[conditions.size()])));
		Long count = dataBaseHelper.getDbAccess().executeQuerySingleResult(query);
		if (count > 1L) {
			return new NuclosLogicalUniqueViolationException("Unique constraint violated in query '"
					+ query + "' with id=" + id + ", number of result is " + count);
		}
		return null;
	}

	private List<String> getLogStatements(DbStatement stmt) {
		List<String> statements = null;
		try {
			final DbAccess dbAccess = dataBaseHelper.getDbAccess();
			final IBatch batch = dbAccess.getBatchFor(stmt);
			statements = dbAccess.getStatementsForLogging(batch);
		} catch (SQLException e) {
			LOG.warn("getLogStatements failed", e);
		}
		return statements;
	}

	public void addToColumns(IColumnToVOMapping<?, PK> column) {
		if (allColumnsAsSet.add(column)) {
			allColumns.add(column);
		}
	}

	public void setAllColumns(List<IColumnToVOMapping<?, PK>> columns) {
		allColumns.clear();
		allColumns.addAll(columns);

		allColumnsAsSet.clear();
		allColumnsAsSet.addAll(columns);

		checkColumns();
	}

	protected DbException transformInsertOrUpdateException(DbException ex, final DalVO dalVO) {
		return ex;
	}

	protected DbException transformDeleteException(DbException ex, final PK pk) {
		return ex;
	}

	protected void invalidateFqnCaches() {
		try {
			SpringApplicationContextHolder.getBean(MasterDataRestFqnCache.class).invalidateCacheForEntity(getEntityUID());
		} catch (Exception ex) {
			LOG.warn("FQN cache invalidation for entity " + getEntityUID() + " faild:" + ex.getMessage());
			LOG.debug("Error: ", ex);
		}
	}

	public NuclosUserDetailsContextHolder getUserContext() {
		return userCtx;
	}

	public void setNuclosUserDetailsContextHolder(NuclosUserDetailsContextHolder userContext) {
		this.userCtx = userContext;
	}

	protected void adjustFrom(DbFrom<PK> from) {}

}
