package org.nuclos.server.dal.processor.jdbc.impl;

import java.util.List;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.NucletEntityContext;
import org.nuclos.common.UID;
import org.nuclos.server.dal.processor.IColumnToVOMapping;
import org.nuclos.server.dal.processor.jdbc.AbstractJdbcDalProcessor;
import org.nuclos.server.dal.processor.nuclet.JdbcEntityContextProcessor;

public class EntityContextProcessor extends AbstractJdbcDalProcessor<NucletEntityContext, UID>
	implements JdbcEntityContextProcessor {

	private final IColumnToVOMapping<UID, UID> uidColumn;

	public EntityContextProcessor(List<IColumnToVOMapping<? extends Object, UID>> allColumns,
								  IColumnToVOMapping<UID, UID> uidColumn) {
		super(E.ENTITYCONTEXT.getUID(), NucletEntityContext.class, UID.class, allColumns);
		this.uidColumn = uidColumn;
	}


	@Override
	protected EntityMeta<UID> getMetaData() {
		return E.ENTITYCONTEXT;
	}

	@Override
	protected IColumnToVOMapping<UID, UID> getPrimaryKeyColumn() {
		return uidColumn;
	}

	@Override
	public List<NucletEntityContext> getAll() {
		return super.getAll();
	}

	@Override
	public List<NucletEntityContext> getByPrimaryKeys(final List<UID> ids) {
		return super.getByPrimaryKeys(allColumns, ids);
	}

}
