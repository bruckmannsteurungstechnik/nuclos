//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor.jdbc.impl;

import java.util.ArrayList;
import java.util.List;

import org.nuclos.common.SF;
import org.nuclos.common.dal.DalCallResult;
import org.nuclos.common.dal.vo.EOGenericObjectVO;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.dal.DalUtils;
import org.nuclos.server.dal.processor.IColumnToVOMapping;
import org.nuclos.server.dal.processor.ProcessorConfiguration;
import org.nuclos.server.dal.processor.jdbc.AbstractJdbcDalProcessor;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.expression.DbIncrement;
import org.nuclos.server.dblayer.statements.DbInsertStatement;
import org.nuclos.server.dblayer.statements.DbMap;
import org.nuclos.server.dblayer.statements.DbTableStatement;
import org.nuclos.server.dblayer.statements.DbUpdateStatement;
import org.nuclos.server.fileimport.ImportStructure;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Role;

/**
 * Implementation for importing objects from files into database.
 * If the importstructure is configured to to updates, an update is tried at first. If no rows where updated, an insert is tried.
 *
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Configurable
public class ImportObjectProcessor<PK> extends EntityObjectProcessor<PK> {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractJdbcDalProcessor.class);

	// private final EntityMeta meta;
	private final ImportStructure importStructure;
	private final List<IColumnToVOMapping<?, PK>> valueColumnsInsert =
		new ArrayList<>();
	private final List<IColumnToVOMapping<?, PK>> valueColumnsUpdate =
		new ArrayList<>();
	private final List<IColumnToVOMapping<?, PK>> conditionColumns =
		new ArrayList<>();

	private int inserted = 0;
	private int updated = 0;

	public ImportObjectProcessor(ProcessorConfiguration<PK> config, ImportStructure structure) {
		super(config);
		this.importStructure = structure;
		for(IColumnToVOMapping<?, PK> column : allColumns) {
			valueColumnsInsert.add(column);
			if(structure.getItems().containsKey(column.getUID())) {
				valueColumnsUpdate.add(column);
			}
			else if (SF.CHANGEDAT.getUID(config.getMetaData().getUID()).equals(column.getUID())) {
				valueColumnsUpdate.add(column);
			}
			else if (SF.CHANGEDBY.getUID(config.getMetaData().getUID()).equals(column.getUID())) {
				valueColumnsUpdate.add(column);
			}

			if(structure.getIdentifiers().contains(column.getUID())) {
				conditionColumns.add(column);
			}
		}
	}

	@Override
	protected PK insertOrUpdateImpl(final List<IColumnToVOMapping<?, PK>> columns, EntityObjectVO<PK> dalVO, final DalCallResult dcr) {
		DbMap columnValueMap = getColumnValuesMap(valueColumnsUpdate, dalVO);
		DbMap columnConditionMap = getColumnValuesMap(conditionColumns, dalVO);

		DbTableStatement<PK> stmt = null;
		try {
			boolean updated = false;
			if (importStructure.isUpdate()) {
				columnValueMap.put(SF.VERSION, DbIncrement.INCREMENT);
				stmt = new DbUpdateStatement<PK>(getMetaData(), columnValueMap, columnConditionMap);
				updated = dataBaseHelper.getDbAccess().execute(stmt) > 0;
				if (updated) {
					this.updated++;
				}
			}

			// NUCLOS-6638 Don't insert anything if there has been an update.
			if (updated || !importStructure.isInsert()) {
				return dalVO.getPrimaryKey();
			}

			dalVO.setPrimaryKey((PK) DalUtils.getNextId());
			columnValueMap = getColumnValuesMap(valueColumnsInsert, dalVO);
			stmt = new DbInsertStatement<PK>(getMetaData(), columnValueMap);
			dataBaseHelper.getDbAccess().execute(stmt);
			inserted++;

			if (eMeta.isStateModel()) {
				EOGenericObjectVO eogo = new EOGenericObjectVO();
				eogo.setPrimaryKey((Long) dalVO.getPrimaryKey());
				eogo.setCreatedBy(dalVO.getCreatedBy());
				eogo.setCreatedAt(dalVO.getCreatedAt());
				eogo.setChangedBy(dalVO.getChangedBy());
				eogo.setChangedAt(dalVO.getChangedAt());
				eogo.setEntityUID(eMeta.getUID());
				eogo.setVersion(1);
				eogo.flagNew();
				NucletDalProvider.getInstance().getEOGenericObjectProcessor().insertOrUpdate(eogo);
			}
		}
		catch(DbException ex) {
			if (dcr == null) {
				throw ex;
			}
        	ex.setPkIfNull(dalVO.getPrimaryKey());
			dcr.addRuntimeException(ex);
		}
		return dalVO.getPrimaryKey();
	}

	public int getInserted() {
		return inserted;
	}

	public int getUpdated() {
		return updated;
	}
	
}
