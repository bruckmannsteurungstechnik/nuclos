//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.nuclos.common.E;
import org.nuclos.common.EntityContext;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.MetaTablesContentFromDb;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.NucletFieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.collection.multimap.MultiListHashMap;
import org.nuclos.common.collection.multimap.MultiListMap;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.dal.DalUtils;
import org.nuclos.server.dal.processor.ProcessorFactorySingleton;
import org.nuclos.server.dal.processor.jdbc.impl.ChartMetaDataProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.DataLanguageMetaDataProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.DynamicMetaDataProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.DynamicTasklistMetaDataProcessor;
import org.nuclos.server.dal.processor.nuclet.IEOGenericObjectProcessor;
import org.nuclos.server.dal.processor.nuclet.IEntityLafParameterProcessor;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.dal.processor.nuclet.IWorkspaceProcessor;
import org.nuclos.server.dal.processor.nuclet.JdbcEntityContextProcessor;
import org.nuclos.server.dal.processor.nuclet.JdbcEntityFieldMetaDataProcessor;
import org.nuclos.server.dal.processor.nuclet.JdbcEntityMetaDataProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Component;

/**
 * TODO Replace with pure Spring solution.
 */
@Component
public class NucletDalProvider {

	private static final Logger LOG = LoggerFactory.getLogger(NucletDalProvider.class);

	private enum ProcessorFactoryType {
		ENTITY_OBJECT,
		DYNAMIC,
		CHART,
		DYNAMICTASKLIST
	}
	
	/**
	 * Singleton der auch in einer MultiThreading-Umgebung Threadsafe ist...
	 */
	private static NucletDalProvider INSTANCE;
	
	// Spring injection

	@Autowired
	private CacheManager cacheManager;

	@Autowired
	private JdbcEntityMetaDataProcessor entityMetaDataProcessor;

	@Autowired
	private JdbcEntityFieldMetaDataProcessor entityFieldMetaDataProcessor;

	@Autowired
	private JdbcEntityContextProcessor entityContextProcessor;

	@Autowired
	private IEntityLafParameterProcessor entityLafParameterProcessor;

	@Autowired
	private IEOGenericObjectProcessor eoGenericObjectProcessor;

	@Autowired
	private IWorkspaceProcessor workspaceProcessor;

	@Autowired
	private DynamicMetaDataProcessor dynMetaDataProcessor;

	@Autowired
	private ChartMetaDataProcessor crtMetaDataProcessor;

	@Autowired
	private DynamicTasklistMetaDataProcessor dynTasklistMetaDataProcessor;

	@Autowired
	private ProcessorFactorySingleton processorFac;

	@Autowired
	private DataLanguageMetaDataProcessor dataLangMetaDataProcessor;

	// end of Spring injection
	
	private ThreadLocal<Stack<Set<UID>>> accessibleMandators = new ThreadLocal<Stack<Set<UID>>>() {
		@Override
		protected Stack<Set<UID>> initialValue() {
			return new Stack<Set<UID>>();
		}
	};
	
	private ThreadLocal<Stack<UID>> accessibleMandatorsOrigin = new ThreadLocal<Stack<UID>>() {
		@Override
		protected Stack<UID> initialValue() {
			return new Stack<UID>();
		}
	};
	
	private ThreadLocal<Stack<UID>> runningJob = new ThreadLocal<Stack<UID>>() {
		@Override
		protected Stack<UID> initialValue() {
			return new Stack<UID>();
		}
	};

	NucletDalProvider() {
		INSTANCE = this;
	}
	
	/**
	 * @deprecated Use Spring injection instead.
	 */
	public static NucletDalProvider getInstance() {
		if (INSTANCE == null || INSTANCE.eoGenericObjectProcessor == null) {
			throw new IllegalStateException("too early");
		}
		return INSTANCE;
	}

	public MetaTablesContentFromDb loadMetaTablesFromDb(boolean forceReload) {
		if (forceReload) {
			this.invalidateMetaTablesContent();
		}
		return getMetaTablesFromDb();
	}

	//@Cacheable annotation does not work during startup. Workaround via cacheManager...
	//@Cacheable(value = "nucletDalProviderMetaTablesContent")
	public MetaTablesContentFromDb getMetaTablesFromDb() {
		MetaTablesContentFromDb result = null;
		Cache.ValueWrapper valueWrapper = cacheManager.getCache("nucletDalProviderMetaTablesContent").get("");
		if (valueWrapper != null) {
			result = (MetaTablesContentFromDb) valueWrapper.get();
		}
		if (result == null) {
			result = createMetaTablesFromDb();
			cacheManager.getCache("nucletDalProviderMetaTablesContent").put("", result);
		}
		return result;
	}

	private MetaTablesContentFromDb createMetaTablesFromDb() {
		List<NucletEntityMeta> nucletEntities = getEntityMetaDataProcessor().getAll();

		MultiListMap<UID, FieldMeta<?>> fieldMetaMap = new MultiListHashMap<>(nucletEntities.size()); // for performance
		for (NucletFieldMeta<?> fieldMeta : getEntityFieldMetaDataProcessor().getAll()) {
			fieldMetaMap.addValue(fieldMeta.getEntity(), fieldMeta);
		}

		Map<UID, NucletEntityMeta> mapEntityMeta = nucletEntities.stream().collect(Collectors.toMap(EntityMeta::getUID, Function.identity()));
		entityContextProcessor.getAll().stream().collect(Collectors.groupingBy(EntityContext::getMainEntity)).forEach((k,v) -> {
			mapEntityMeta.get(k).setEntityContexts(v.stream().map(vo -> (EntityContext)vo).collect(Collectors.toSet()));
		});

		Map<UID, EntityMeta<?>> mpAllEntitiesByUID = new HashMap<>();

		for (NucletEntityMeta eMeta : nucletEntities) {
			Collection<FieldMeta<?>> entityFields = new ArrayList<>(fieldMetaMap.getValues(eMeta.getUID()));
			DalUtils.addStaticFields(entityFields, eMeta);
			eMeta.setFields(entityFields);
			mpAllEntitiesByUID.put(eMeta.getUID(), eMeta);
		}

		List<NucletEntityMeta> dataLanguageEntites = getDataLanguageMetaDataProcessor().getAll(mpAllEntitiesByUID);
		List<NucletEntityMeta> dynamicEntities = getDynamicEntityMetaProcessor().getAll();
		List<NucletEntityMeta> chartEntities = getChartEntityMetaProcessor().getAll();
		List<NucletEntityMeta> dynamicListEntities = getDynamicTasklistEntityMetaProcessor().getAll();

		return new MetaTablesContentFromDb(nucletEntities, dataLanguageEntites, dynamicEntities, chartEntities, dynamicListEntities);
	}

	//@Cacheable annotation does not work during startup. Workaround via cacheManager...
	//@Cacheable(value = "nucletDalProviderAllEntityMetas")
	public Map<UID, Pair<EntityMeta<?>, ProcessorFactoryType>> getAllEntityMetasMap() {
		Map<UID, Pair<EntityMeta<?>, ProcessorFactoryType>> result = null;
		Cache.ValueWrapper valueWrapper = cacheManager.getCache("nucletDalProviderAllEntityMetas").get("");
		if (valueWrapper != null) {
			result = (Map<UID, Pair<EntityMeta<?>, ProcessorFactoryType>>) valueWrapper.get();
		}
		if (result == null) {
			result = createAllEntityMetasMap();
			cacheManager.getCache("nucletDalProviderAllEntityMetas").put("", result);
		}
		return result;
	}

	private Map<UID, Pair<EntityMeta<?>, ProcessorFactoryType>> createAllEntityMetasMap() {
		ConcurrentMap<UID, Pair<EntityMeta<?>, ProcessorFactoryType>> result = new ConcurrentHashMap<>();

		/**
		 * Nuclos Entities
		 */
		E.getAllEntities().parallelStream().forEach(m -> result.put(m.getUID(), new Pair(m, ProcessorFactoryType.ENTITY_OBJECT)));

		MetaTablesContentFromDb metaTablesContent = getMetaTablesFromDb(); //loadMetaTablesFromDb(true);

		/**
		 * Nuclet Entities
		 */
		metaTablesContent.getNucletEntities().parallelStream().filter(m -> !m.isGeneric()).forEach(m -> result.put(m.getUID(), new Pair(m, ProcessorFactoryType.ENTITY_OBJECT)));

		/**
		 * Data Language Entities
		 */
		metaTablesContent.getDataLangEntities().parallelStream().forEach(m -> result.put(m.getUID(), new Pair(m, ProcessorFactoryType.ENTITY_OBJECT)));

		/**
		 * Dynamic Entities
		 */
		metaTablesContent.getDynamicEntities().parallelStream().forEach(m -> result.put(m.getUID(), new Pair(m, ProcessorFactoryType.DYNAMIC)));

		/**
		 * Chart Entities
		 */
		metaTablesContent.getChartEntities().parallelStream().forEach(m -> result.put(m.getUID(), new Pair(m, ProcessorFactoryType.CHART)));

		/**
		 * Dynamic List Entities
		 */
		metaTablesContent.getDynamicListEntities().parallelStream().forEach(m -> result.put(m.getUID(), new Pair(m, ProcessorFactoryType.DYNAMICTASKLIST)));

		return Collections.unmodifiableMap(result);
	}
	
	public JdbcEntityMetaDataProcessor getEntityMetaDataProcessor() {
		return entityMetaDataProcessor;
	}
	
	public JdbcEntityFieldMetaDataProcessor getEntityFieldMetaDataProcessor() {
		return entityFieldMetaDataProcessor;
	}
	
	public IEntityLafParameterProcessor getEntityLafParameterProcessor() {
		return entityLafParameterProcessor;
	}

	public <PK> IEntityObjectProcessor<PK> getEntityObjectProcessor(@NotNull EntityMeta<PK> entity) {
		return (IEntityObjectProcessor<PK>) getEntityObjectProcessor(entity.getUID());
	}

	//@Cacheable annotation does not work during startup. Workaround via cacheManager...
	//@Cacheable(value = "nucletDalProviderEntityObjectProcessor", key = "#entity.getString()")
	public <PK> IEntityObjectProcessor<PK> getEntityObjectProcessor(@NotNull UID entity) {
		IEntityObjectProcessor<PK> result = null;
		Cache.ValueWrapper valueWrapper = cacheManager.getCache("nucletDalProviderEntityObjectProcessor").get(entity.getString());
		if (valueWrapper != null) {
			result = (IEntityObjectProcessor<PK>) valueWrapper.get();
		}
		if (result == null) {
			result = createEntityObjectProcessor(entity);
			cacheManager.getCache("nucletDalProviderEntityObjectProcessor").put(entity.getString(), result);
		}
		return result;
	}

	private <PK> IEntityObjectProcessor<PK> createEntityObjectProcessor(@NotNull UID entity) {
		Pair<EntityMeta<?>, ProcessorFactoryType> metaProcessorFactoryTypePair = getAllEntityMetasMap().get(entity);
		if (metaProcessorFactoryTypePair == null) {
			throw new CommonFatalException(String.format("Entity for UID \"%s\" not found", entity));
		}
		try {
			EntityMeta<?> eMeta = metaProcessorFactoryTypePair.x;
			switch (metaProcessorFactoryTypePair.y) {
				case ENTITY_OBJECT:
					return processorFac.newEntityObjectProcessor(eMeta, eMeta.getFields(), true);
				case DYNAMIC:
					return processorFac.newDynamicEntityObjectProcessor(eMeta, eMeta.getFields());
				case CHART:
					return processorFac.newChartEntityObjectProcessor(eMeta, eMeta.getFields());
				case DYNAMICTASKLIST:
					return processorFac.newDynamicTasklistEntityObjectProcessor(eMeta, eMeta.getFields());
				default:
					return processorFac.newEntityObjectProcessor(eMeta, eMeta.getFields(), true);
			}
		} catch (Exception ex) {
			throw new CommonFatalException(ex);
		}
	}
	
	public IEOGenericObjectProcessor getEOGenericObjectProcessor() {
		return eoGenericObjectProcessor;
	}
	
	public IWorkspaceProcessor getWorkspaceProcessor() {
		return workspaceProcessor;
	}
	
	public DynamicMetaDataProcessor getDynamicEntityMetaProcessor() {
		if (dynMetaDataProcessor == null) {
			// dynMetaDataProcessor = new DynamicMetaDataProcessor();
			throw new IllegalStateException("too early");
		}
	    return dynMetaDataProcessor;
    }

	public ChartMetaDataProcessor getChartEntityMetaProcessor() {
		if (crtMetaDataProcessor == null) {
			// crtMetaDataProcessor = new ChartMetaDataProcessor();
			throw new IllegalStateException("too early");
		}
	    return crtMetaDataProcessor;
    }

	public DynamicTasklistMetaDataProcessor getDynamicTasklistEntityMetaProcessor() {
		if (dynTasklistMetaDataProcessor == null) {
			// dynMetaDataProcessor = new DynamicMetaDataProcessor();
			throw new IllegalStateException("too early");
		}
		return dynTasklistMetaDataProcessor;
	}

	@Caching(evict = {
			@CacheEvict(value = "nucletDalProviderAllEntityMetas", allEntries = true),
			@CacheEvict(value = "nucletDalProviderEntityObjectProcessor", allEntries = true),
			@CacheEvict(value = "nucletDalProviderMetaTablesContent", allEntries = true)
	})
	public void invalidate() {
		LOG.debug("Invalidating NucletDalProvider");
	}

	@Caching(evict = {
			@CacheEvict(value = "nucletDalProviderMetaTablesContent", allEntries = true)
	})
	public void invalidateMetaTablesContent() {
		LOG.debug("Invalidating NucletDalProvider.MetaTablesContent");
	}
	
	/**
	 * only available during rule execution
	 * @return 
	 */
	public Set<UID> getAccessibleMandators() {
		try {
			return accessibleMandators.get().peek();
		} catch (EmptyStackException ese) {
			return null;
		}
	}
	
	public DataLanguageMetaDataProcessor getDataLanguageMetaDataProcessor() {
		return this.dataLangMetaDataProcessor;
	}
	
	/**
	 * only available during rule execution
	 * @return 
	 */
	public UID getAccessibleMandatorsOrigin() {
		try {
			return accessibleMandatorsOrigin.get().peek();
		} catch (EmptyStackException ese) {
			return null;
		}
	}
	
	public boolean isAccessibleMandatorsSet() {
		try {
			return accessibleMandators.get().peek() != null;
		} catch (EmptyStackException ese) {
			return false;
		}
	}
	
	/** 
	 * set from rule engine only
	 * @param mandators
	 * @param mandatorOrigin
	 */
	public void setAccessibleMandators(Set<UID> mandators, UID mandatorOrigin) {
		accessibleMandators.get().push(mandators);
		accessibleMandatorsOrigin.get().push(mandatorOrigin);
	}
	
	/**
	 * called from rule engine only
	 */
	public void removeAccessibleMandators() {
		accessibleMandators.get().pop();
		accessibleMandatorsOrigin.get().pop();
	}
	
	/**
	 * only available during rule execution
	 * @return 
	 */
	public UID getRunningJob() {
		try {
			return runningJob.get().peek();
		} catch (EmptyStackException ese) {
			return null;
		}
	}
	
	public boolean isRunningJobSet() {
		try {
			return runningJob.get().peek() != null;
		} catch (EmptyStackException ese) {
			return false;
		}
	}
	
	/** 
	 * set from rule engine only
	 * @param job
	 */
	public void setRunningJob(UID job) {
		runningJob.get().push(job);
	}
	
	/**
	 * called from rule engine only
	 */
	public void removeRunningJob() {
		runningJob.get().pop();
	}
}
