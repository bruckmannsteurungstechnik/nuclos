//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dblayer.query;

import java.util.Map;

import org.apache.commons.collections15.CollectionUtils;
import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common2.ForeignEntityFieldUIDParser;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.dblayer.impl.util.PreparedStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Column mapping for referenced compound (one-)table expressions. This is used for nuclos 'stringified' 
 * references. The expression uses a CASE construct to return a NULL value if the foreign key is NULL.
 * 
 * @param <T> java type of the expression result, must be String for real compound 
 * 		expressions but maybe different for 'singleton' expressions.
 */
@SuppressWarnings("serial")
public class DbReferencedCompoundColumnExpression<T> extends DbExpression<T> implements IDbReferencedCompoundColumnExpression {
	
	private final static Logger LOG = LoggerFactory.getLogger(DbReferencedCompoundColumnExpression.class);
	
	public DbReferencedCompoundColumnExpression(DbFrom<T> from, FieldMeta<T> field, Class<T> referencedClass, boolean bSetAlias) {
		this(from, field, referencedClass, bSetAlias ? getRefAlias(field) : null, null, null);
	}
	
	public DbReferencedCompoundColumnExpression(DbFrom<T> from, FieldMeta<T> field, Class<T> referencedClass, String sAlias, Map<UID, String> aliasMap, Map<UID, FieldMeta> langFieldsMap) {
		this(from, field, referencedClass, sAlias, aliasMap, langFieldsMap, null);
	}
	
	public DbReferencedCompoundColumnExpression(DbFrom<T> from, FieldMeta<T> field, Class<T> referencedClass, String sAlias, Map<UID, String> aliasMap, Map<UID, FieldMeta> langFieldsMap, PreparedStringBuilder idSql) {
		super(from.getQuery().getBuilder(), referencedClass, sAlias, mkConcat(from, field, aliasMap, langFieldsMap, idSql!=null?idSql.toString():null));
		if (!field.hasAnyForeignEntity()) {
			throw new IllegalArgumentException();
		}
	}
	
	public DbReferencedCompoundColumnExpression(DbQueryBuilder builder, Class<? extends T> javaType, String alias, PreparedStringBuilder idSql) {
		super(builder, javaType, alias, idSql);
	}
	
	static final <T> PreparedStringBuilder mkConcat(DbFrom<T> from, FieldMeta<T> field, Map<UID, String> aliasMap, Map<UID, FieldMeta> fieldsMap, String idSql) {
		final PreparedStringBuilder result = DbCompoundColumnExpression.mkConcat(from, field, false, aliasMap, fieldsMap);
		return mkConcat(result, from.getAlias(), field, idSql);
	}
	
	public static <T> boolean isConcatNecessary(FieldMeta<T> field) {
		final int size = field.getDbColumn().startsWith("INTID_T_") ? 1 : CollectionUtils.size(new ForeignEntityFieldUIDParser(field, MetaProvider.getInstance()).iterator());
		return size > 1;
	}
	
	public static <T> PreparedStringBuilder mkConcat(PreparedStringBuilder result, String tableAlias, FieldMeta<T> field, String idSql) {
		if (isConcatNecessary(field)) {
			if (idSql == null) {
				boolean foreignEntityIsSystem = E.isNuclosEntity(field.getForeignEntity());
				final String foreignkeyColumn = field.getDbColumn().toUpperCase().replaceFirst("^(STRVALUE_|INTVALUE_|OBJVALUE_)", 
					(field.getJavaClass()==UID.class || foreignEntityIsSystem) ? "STRUID_" : "INTID_");
				idSql = DbColumnExpression.mkQualifiedColumnName(tableAlias, foreignkeyColumn, false).toString();
			}
			
			return PreparedStringBuilder.concat("CASE WHEN (", idSql, " IS NOT NULL) THEN ", result, " ELSE NULL END ");
		}
		
		return result;
	}
	
	public static String getRefAlias(FieldMeta<?> field) {
		if (field.getJavaClass() == UID.class && (field.getForeignEntityField() != null || field.getUnreferencedForeignEntityField() != null)) {
			final String column = field.getDbColumn().toUpperCase();
			final String referenceAliasColumn;
			if (column.startsWith("STRUID_")) {
				referenceAliasColumn = field.getDbColumn().toUpperCase().replaceFirst("^STRUID_", "STRVALUE_");
			} else if (column.startsWith("INTID_")) {
				referenceAliasColumn = field.getDbColumn().toUpperCase().replaceFirst("^INTID_", "STRVALUE_");
			} else {
				referenceAliasColumn = null;
			}
			
			if (referenceAliasColumn != null) {
				if (referenceAliasColumn.length() > 30) {
					// for 'STRVALUE_T_MD_VALUELISTPROVIDER'
					LOG.debug("Reference alias column \"{}\" > 30", referenceAliasColumn);
					return referenceAliasColumn.substring(0, 30);
				}
				return referenceAliasColumn;
			} else {
				throw new IllegalArgumentException("RefAlias not possible for field: " + field);
			}
		}
		
		return field.getDbColumn();
	}
	
}
