package org.nuclos.server.dblayer.query;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.dblayer.IFieldUIDRef;
import org.nuclos.common2.ForeignEntityFieldUIDParser;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.dal.processor.IColumnWithMdToVOMapping;
import org.nuclos.server.dal.processor.jdbc.TableAliasSingleton;
import org.nuclos.server.dblayer.DbUtils;
import org.nuclos.server.dblayer.impl.util.PreparedStringBuilder;


@SuppressWarnings("serial")
public class DbReferencedSubselectExpression<T> extends DbReferencedCompoundColumnExpression<T> {
	
	public DbReferencedSubselectExpression(DbExpression<T> dbExpression, DbFrom<T> from, IColumnWithMdToVOMapping<T, ?> column) {
		this(dbExpression, from, column, false);
	}
	
	public DbReferencedSubselectExpression(DbExpression<T> dbExpression, DbFrom<T> from, IColumnWithMdToVOMapping<T, ?> column, boolean useExprSqlForCond) {
		super(dbExpression.getBuilder(), dbExpression.getJavaType(), column.getDbColumn(), mkSubselect(from, column, useExprSqlForCond ? dbExpression.getSqlColumnExpr() : null));
	}
	
	public static <T> PreparedStringBuilder mkSubselect(DbFrom<T> from, IColumnWithMdToVOMapping<T, ?> column, String idSql) {
		final String tableAliasForField = TableAliasSingleton.getInstance().getAlias(column);
		final FieldMeta<?> field = column.getMeta();
		return mkSubselect(from, tableAliasForField, field, idSql);
	}
	
	public static <T> PreparedStringBuilder mkSubselect(DbFrom<T> from, String tableAliasForField, FieldMeta<?> field, String idSql) {
		final MetaProvider mp = MetaProvider.getInstance();
		
		final String tableAliasLeft = from.getAlias();
		
		
		EntityMeta<?> foreignEntityMeta = mp.getEntity(field.getFirstNonNullForeignEntity());

		String dbIdFieldName = DbUtils.getDbIdFieldName(field, foreignEntityMeta.isUidEntity());
		String dbPk = foreignEntityMeta.isUidEntity() ? "STRUID" : "INTID";
		
		//Try to skip Table Aliases, which is only possible for fields from the foreign entity.
		Map<UID, String> aliasMap = new HashMap<UID, String>();
		
		for (IFieldUIDRef ref: new ForeignEntityFieldUIDParser(field, mp, false)) {
			if (!ref.isUID()) {
				continue;
			}
			try
			{
				foreignEntityMeta.getField(ref.getUID()); 
				aliasMap.put(ref.getUID(), null);	
			}catch(IllegalArgumentException e)
			{
				// nothing to do if IllegalArgumentException is caused by foreignEntityMeta.getField() 
			}
			
		}
		
		//Expr:a_kategorie176.STRkategorie, Table:T4PL_KATEGORIE a_kategorie176, Cond:t.INTID_STRKATEGORIE = a_kategorie176.INTID
		//Expr:STRkategorie, Table:T4PL_KATEGORIE, Cond:t.INTID_STRKATEGORIE = INTID (without table alias)
		//=> (SELECT <Expr> FROM <Table> WHERE <Cond>

		PreparedStringBuilder expr = mkConcat(from, (FieldMeta<T>)field, aliasMap, null, idSql);
		
		boolean bUseTableAliasForField = expr.toString().contains(tableAliasForField);

		String tableName = StringUtils.isNotBlank(foreignEntityMeta.getVirtualEntity()) ? foreignEntityMeta.getVirtualEntity() : foreignEntityMeta.getDbTable();
		PreparedStringBuilder table = new PreparedStringBuilder(tableName);
		if (bUseTableAliasForField) {
			table.append(" ").append(tableAliasForField);
		}
		
		PreparedStringBuilder cond = new PreparedStringBuilder(bUseTableAliasForField ? tableAliasForField + "." : "");
		cond.append(dbPk + " = ");
		if (idSql == null) {
			cond.append(tableAliasLeft + "." + dbIdFieldName);
		} else {
			cond.append(idSql);
		}
				
        String s = "(SELECT " + expr + " FROM " + table + " WHERE " + cond + ")";
        
		return new PreparedStringBuilder(s);
	}

}
