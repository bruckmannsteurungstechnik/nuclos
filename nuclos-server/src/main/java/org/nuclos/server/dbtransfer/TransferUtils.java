//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dbtransfer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

import javax.json.JsonObject;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.DalCallResult;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dbtransfer.NucletContentMap;
import org.nuclos.common.dbtransfer.Transfer;
import org.nuclos.common.dbtransfer.TransferEO;
import org.nuclos.common.dbtransfer.TransferOption;
import org.nuclos.common.dbtransfer.TransferTreeNode;
import org.nuclos.common2.LangUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.dbtransfer.content.AbstractNucletContent;
import org.nuclos.server.dbtransfer.content.ActionNucletContent;
import org.nuclos.server.dbtransfer.content.BusinessTestNucletContent;
import org.nuclos.server.dbtransfer.content.CustomComponentNucletContent;
import org.nuclos.server.dbtransfer.content.DbObjectSourceNucletContent;
import org.nuclos.server.dbtransfer.content.DefaultNucletContent;
import org.nuclos.server.dbtransfer.content.EntityContextNucletContent;
import org.nuclos.server.dbtransfer.content.EntityFieldNucletContent;
import org.nuclos.server.dbtransfer.content.EntityGenericFieldMappingNucletContent;
import org.nuclos.server.dbtransfer.content.EntityGenericImplementationNucletContent;
import org.nuclos.server.dbtransfer.content.EntityMenuNucletContent;
import org.nuclos.server.dbtransfer.content.EntityNucletContent;
import org.nuclos.server.dbtransfer.content.EntitySubnodesNucletContent;
import org.nuclos.server.dbtransfer.content.INucletContent;
import org.nuclos.server.dbtransfer.content.ImportNucletContent;
import org.nuclos.server.dbtransfer.content.JobControllerNucletContent;
import org.nuclos.server.dbtransfer.content.LogEntry;
import org.nuclos.server.dbtransfer.content.NucletExtensionNucletContent;
import org.nuclos.server.dbtransfer.content.NucletIntegrationPointFieldNucletContent;
import org.nuclos.server.dbtransfer.content.NucletIntegrationPointNucletContent;
import org.nuclos.server.dbtransfer.content.NucletNucletContent;
import org.nuclos.server.dbtransfer.content.NucletParameterNucletContent;
import org.nuclos.server.dbtransfer.content.PreferenceNucletContent;
import org.nuclos.server.dbtransfer.content.RelationTypeNucletContent;
import org.nuclos.server.dbtransfer.content.ReportOutputNucletContent;
import org.nuclos.server.dbtransfer.content.ResourceNucletContent;
import org.nuclos.server.dbtransfer.content.RolePreferenceNucletContent;
import org.nuclos.server.dbtransfer.content.SearchFilterNucletContent;
import org.nuclos.server.dbtransfer.content.StateModelNucletContent;
import org.nuclos.server.dbtransfer.content.StateNucletContent;
import org.nuclos.server.dbtransfer.content.StateTransitionNucletContent;
import org.nuclos.server.dbtransfer.content.SubReportOutputNucletContent;
import org.nuclos.server.dbtransfer.content.TasklistNucletContent;
import org.nuclos.server.dbtransfer.content.ValidationType;
import org.nuclos.server.dbtransfer.content.ValueListProviderNucletContent;
import org.nuclos.server.dbtransfer.content.WebAddonFileNucletContent;
import org.nuclos.server.dbtransfer.content.WebserviceNucletContent;
import org.nuclos.server.dbtransfer.content.WorkspaceNucletContent;
import org.nuclos.server.dbtransfer.content.XmlImportNucletContent;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;

public class TransferUtils {

	protected static enum Process {CREATE, PREPARE, RUN};
	
	protected static List<INucletContent> getNucletContentInstances(Map<TransferOption, Serializable> transferOptions, Process p) {
		List<INucletContent> contents = new ArrayList<INucletContent>();

		final boolean isDirMode = transferOptions != null && transferOptions.containsKey(TransferOption.IS_DIRECTORY_MODE);

		contents.add(new NucletNucletContent(contents));
		contents.add(new DefaultNucletContent(E.NUCLETDEPENDENCE.nuclet, contents));
		contents.add(new DefaultNucletContent(E.NUCLETRELEASENOTE.nuclet, contents));
		contents.add(new NucletExtensionNucletContent(contents));
		if (p == Process.CREATE) {
			contents.add(new NucletParameterNucletContent(contents, isDirMode));
		}
		contents.add(new NucletIntegrationPointNucletContent(contents));
		contents.add(new NucletIntegrationPointFieldNucletContent(contents));
		contents.add(new DefaultNucletContent(E.SERVERCODEENTITY.integrationPoint, contents));

		contents.add(new ResourceNucletContent(contents));
		contents.add(new RelationTypeNucletContent(contents));
		contents.add(new DefaultNucletContent(E.REPORTGROUP, contents));
		contents.add(new ActionNucletContent(contents));
		contents.add(new DefaultNucletContent(E.DATATYPE, contents));

		contents.add(new EntityNucletContent(contents));
		contents.add(new EntityFieldNucletContent(contents));
		contents.add(new EntityContextNucletContent(contents));
		contents.add(new EntitySubnodesNucletContent(contents));
		contents.add(new DefaultNucletContent(E.ENTITYFIELDGROUP, contents));
		contents.add(new DefaultNucletContent(E.ENTITYRELATION, contents));
		contents.add(new EntityMenuNucletContent(contents));
		contents.add(new DefaultNucletContent(E.ENTITYLAFPARAMETER.entity, contents));
		contents.add(new DefaultNucletContent(E.SERVERCODEENTITY.entity, contents));
		contents.add(new EntityGenericImplementationNucletContent(contents));
		contents.add(new EntityGenericFieldMappingNucletContent(contents));
		contents.add(new DefaultNucletContent(E.WEBADDON_RESULTLIST.entity, contents));
		
		contents.add(new DefaultNucletContent(E.DBOBJECT, contents));
		contents.add(new DbObjectSourceNucletContent(contents));

		contents.add(new DefaultNucletContent(E.PROCESS, contents));
		contents.add(new DefaultNucletContent(E.GENERATION, contents));
		contents.add(new DefaultNucletContent(E.GENERATIONATTRIBUTE.generation, contents));
		contents.add(new DefaultNucletContent(E.GENERATIONSUBENTITY.generation, contents));
		contents.add(new DefaultNucletContent(E.GENERATIONSUBENTITYATTRIBUTE.entity, contents));
		contents.add(new DefaultNucletContent(E.GENERATIONUSAGE.generation, contents));
		contents.add(new DefaultNucletContent(E.SERVERCODEGENERATION.generation, contents));
		
		contents.add(new StateModelNucletContent(contents));
		contents.add(new DefaultNucletContent(E.STATEMODELUSAGE.statemodel, contents));
		contents.add(new StateNucletContent(contents));
		contents.add(new DefaultNucletContent(E.STATEMANDATORYFIELD.state, contents));
		contents.add(new DefaultNucletContent(E.STATEMANDATORYCOLUMN.state, contents));
		contents.add(new StateTransitionNucletContent(contents));
		contents.add(new DefaultNucletContent(E.SERVERCODETRANSITION.transition, contents));

		contents.add(new DefaultNucletContent(E.SERVERCODE, contents));

		contents.add(new ImportNucletContent(contents));
		contents.add(new DefaultNucletContent(E.IMPORTATTRIBUTE.importfield, contents));
		contents.add(new DefaultNucletContent(E.IMPORTIDENTIFIER.importfield, contents));
		contents.add(new DefaultNucletContent(E.IMPORTFEIDENTIFIER.importattribute, contents));

		contents.add(new XmlImportNucletContent(contents));
		contents.add(new DefaultNucletContent(E.XMLIMPORTATTRIBUTE.importfield, contents));
		contents.add(new DefaultNucletContent(E.XMLIMPORTFEIDENTIFIER.importattribute, contents));

		contents.add(new JobControllerNucletContent(contents));
		contents.add(new DefaultNucletContent(E.JOBDBOBJECT.parent, contents));
		contents.add(new DefaultNucletContent(E.SERVERCODEJOB.jobcontroller, contents));
		contents.add(new DefaultNucletContent(E.JOBCONTROLLER_PARAMETERVALUE.jobcontroller, contents));
		
		contents.add(new DefaultNucletContent(E.LAYOUT, contents));
		contents.add(new DefaultNucletContent(E.LAYOUTUSAGE.layout, contents));

		contents.add(new DefaultNucletContent(E.DATASOURCE, contents));
		contents.add(new DefaultNucletContent(E.DATASOURCEUSAGE.datasource, contents));
		contents.add(new ValueListProviderNucletContent(contents));
		contents.add(new DefaultNucletContent(E.VALUELISTPROVIDERUSAGE.valuelistProvider, contents));
		contents.add(new DefaultNucletContent(E.DYNAMICENTITY, contents));
		contents.add(new DefaultNucletContent(E.DYNAMICENTITYUSAGE.dynamicEntity, contents));
		contents.add(new DefaultNucletContent(E.RECORDGRANT, contents));
		contents.add(new DefaultNucletContent(E.RECORDGRANTUSAGE.recordGrant, contents));
		contents.add(new DefaultNucletContent(E.CHART, contents));
		contents.add(new DefaultNucletContent(E.CHARTUSAGE.chart, contents));
		contents.add(new DefaultNucletContent(E.DYNAMICTASKLIST, contents));
		contents.add(new DefaultNucletContent(E.DYNAMICTASKLISTUSAGE.dynamictasklist, contents));
		contents.add(new DefaultNucletContent(E.CALCATTRIBUTE, contents));
		contents.add(new DefaultNucletContent(E.CALCATTRIBUTEUSAGE.calcAttribute, contents));
		
		contents.add(new TasklistNucletContent(contents));
		
		contents.add(new WebserviceNucletContent(contents));
		contents.add(new WorkspaceNucletContent(contents));
		contents.add(new PreferenceNucletContent(contents));
		contents.add(new CustomComponentNucletContent(contents));

		contents.add(new DefaultNucletContent(E.REPORT, contents));
		contents.add(new ReportOutputNucletContent(contents));
		contents.add(new SubReportOutputNucletContent(contents));
		contents.add(new DefaultNucletContent(E.REPORTUSAGE.form, contents));

		contents.add(new DefaultNucletContent(E.ROLE, contents));
		contents.add(new DefaultNucletContent(E.ROLEACTION.role, contents));
		contents.add(new DefaultNucletContent(E.ROLETRANSITION.role, contents));
		contents.add(new DefaultNucletContent(E.ROLEATTRIBUTEGROUP.role, contents));
		contents.add(new DefaultNucletContent(E.ROLEMASTERDATA.role, contents));
		contents.add(new DefaultNucletContent(E.ROLEMODULE.role, contents));
		contents.add(new DefaultNucletContent(E.ROLESUBFORM.role, contents));
		contents.add(new DefaultNucletContent(E.ROLESUBFORMGROUP.rolesubform, contents));
		contents.add(new DefaultNucletContent(E.ROLEREPORT.role, contents));
		contents.add(new DefaultNucletContent(E.ROLEGENERATION.role, contents));
		contents.add(new DefaultNucletContent(E.ROLERECORDGRANT.role, contents));
		contents.add(new DefaultNucletContent(E.ROLEWORKSPACE.role, contents));
		contents.add(new RolePreferenceNucletContent(contents));
		contents.add(new DefaultNucletContent(E.ROLESERVERCODE.role, contents));
		contents.add(new DefaultNucletContent(E.TASKLISTROLE.role, contents));
		contents.add(new DefaultNucletContent(E.SEARCHFILTERROLE.role, contents));

		contents.add(new SearchFilterNucletContent(contents));

		contents.add(new BusinessTestNucletContent(contents));

		contents.add(new DefaultNucletContent(E.WEBADDON, contents));
		contents.add(new WebAddonFileNucletContent(contents));
		contents.add(new DefaultNucletContent(E.WEBADDON_PROPERTY.webAddon, contents));

		return contents;
	}

	public static Collection<FieldMeta<UID>> getUserEntityFields(Collection<FieldMeta<?>> fields) {
		Collection<FieldMeta<UID>> result = new ArrayList<FieldMeta<UID>>();
		for (FieldMeta<?> field : CollectionUtils.select(fields, new UserEntityFieldPredicate())) {
			result.add((FieldMeta<UID>) field);
		}
		return result;
	}
	
	public static FieldMeta<UID> getForeignFieldToNuclet(EntityMeta<UID> entity) {
		if (entity.checkEntityUID(E.NUCLETDEPENDENCE.getUID())) {
			return E.NUCLETDEPENDENCE.nuclet;
		}
		for (FieldMeta<?> efMeta : entity.getFields()) {
			if (E.NUCLET.getUID().equals(efMeta.getForeignEntity())) {
				return (FieldMeta<UID>) efMeta;
			}
		}
		throw new IllegalArgumentException("Entity has no foreign field to nuclet");
	}
	
	public static Collection<FieldMeta<?>> getFieldDependencies(EntityMeta<UID> entity) {
		Collection<FieldMeta<?>> result = new ArrayList<FieldMeta<?>>();
		for (EntityMeta<?> eMeta : MetaProvider.getInstance().getAllEntities()) {
			for (FieldMeta<?> efMeta : MetaProvider.getInstance().getAllEntityFieldsByEntity(eMeta.getUID()).values()) {
				if (entity.getUID().equals(efMeta.getForeignEntity()) ||
					entity.getUID().equals(efMeta.getUnreferencedForeignEntity())) {
					result.add(efMeta);
				}
			}
		}
		return result;
	}

	public static Set<EntityMeta<?>> getForeignEntities(EntityMeta<?> entity) {
		Set<EntityMeta<?>> result = new HashSet<EntityMeta<?>>();
		for (FieldMeta<?> efMeta : MetaProvider.getInstance().getAllEntityFieldsByEntity(entity.getUID()).values()) {
			if (efMeta.getForeignEntity() != null) {
				result.add(MetaProvider.getInstance().getEntity(efMeta.getForeignEntity()));
			}
			if (efMeta.getUnreferencedForeignEntity() != null) {
				result.add(MetaProvider.getInstance().getEntity(efMeta.getUnreferencedForeignEntity()));
			}
		}
		return result;
	}
	
	public static Set<FieldMeta.Valueable<String>> getClobFields(EntityMeta<?> entity) {
		Set<FieldMeta.Valueable<String>> result = new HashSet<FieldMeta.Valueable<String>>();
		for (FieldMeta<?> efMeta : MetaProvider.getInstance().getAllEntityFieldsByEntity(entity.getUID()).values()) {
			if (String.class.getName().equals(efMeta.getDataType()) &&
					efMeta.getScale() == null) {
				result.add((FieldMeta.Valueable<String>) efMeta);
			}
		}
		return result;
	}
	
	public static Set<FieldMeta.Valueable<JsonObject>> getJsonFields(EntityMeta<?> entity) {
		Set<FieldMeta.Valueable<JsonObject>> result = new HashSet<FieldMeta.Valueable<JsonObject>>();
		for (FieldMeta<?> efMeta : MetaProvider.getInstance().getAllEntityFieldsByEntity(entity.getUID()).values()) {
			if (JsonObject.class.equals(efMeta.getJavaClass())) {
				result.add((FieldMeta.Valueable<JsonObject>) efMeta);
			}
		}
		return result;
	}

	public static String getEntityName(FieldMeta<?> efMeta) {
		return MetaProvider.getInstance().getEntity(efMeta.getEntity()).getEntityName();
	}

	public static INucletContent getContentType(List<INucletContent> contentTypes, EntityMeta<UID> entity) {
		for (INucletContent nc : contentTypes) {
			if (nc.getEntity() == entity) {
				return nc;
			}
		}
		throw new NuclosFatalException("No content type for entity \"" + entity.getEntityName() + "\" found");
	}
	
	public static List<INucletContent> getRootContentTypes(List<INucletContent> contentTypes) {
		List<INucletContent> result = new ArrayList<INucletContent>();
		for (INucletContent nc : contentTypes) {
			if (nc.getParentEntity() == null) {
				result.add(nc);
			}
		}
		return result;
	}
	
	public static List<INucletContent> getChildrenContentTypes(List<INucletContent> contentTypes, INucletContent parent) {
		List<INucletContent> result = new ArrayList<INucletContent>();
		for (INucletContent nc : contentTypes) {
			if (nc.getParentEntity() == parent.getEntity()) {
				result.add(nc);
			}
		}
		return result;
	}

	public static boolean validate(INucletContent nc, TransferEO teo, ValidationType validity, NucletContentMap importContentMap, 
			Set<UID> existingNucletUIDs, Map<TransferOption, Serializable> transferOptions, Transfer.Result result, boolean usesDataLanguages) {
		LogEntry log = new LogEntry();
		boolean isValid = nc.validate(teo, validity, importContentMap, existingNucletUIDs, log, transferOptions, usesDataLanguages);
		addLogEntry(log, result);
		return isValid;
	}

	public static boolean insertOrUpdateNcObject(INucletContent nc, DalCallResult dalResult, EntityObjectVO<UID> importEO,
												 boolean nuclon, boolean testMode, Transfer transfer) {
		LogEntry log = new LogEntry();
		boolean result = nc.insertOrUpdateNcObject(dalResult, importEO, testMode, transfer, log);
		addLogEntry(log, transfer.result);
		return result;
	}

	private static void addLogEntry(LogEntry log, Transfer.Result result) {
		if (log.sbWarning.length() > 0) {
			result.addWarning(log.sbWarning);
		}
		if (log.sbNonRepeatedValidationWarning.length() > 0) {
			result.addWarning(log.sbNonRepeatedValidationWarning);
		}
		if (log.sbCritical.length() > 0) {
			result.addCritical(log.sbCritical);
		}
		if (!log.foundReferences.isEmpty()) {
			result.foundReferences.addAll(log.foundReferences);
		}
	}

	public static TransferEO getEntityObjectVO(List<TransferEO> teos, UID uid) {
		List<TransferEO> result = CollectionUtils.select(teos, new TransferEOUidPredicate(uid));
		return result.isEmpty() ? null : result.get(0);
	}
	
	public static Collection<EntityObjectVO<UID>> getEntityObjectVOs(FieldMeta<UID> reffield, Set<UID> refvalues) {
		List<EntityObjectVO<UID>> result = new ArrayList<EntityObjectVO<UID>>();
		if (refvalues != null) {
			for (UID refvalue : refvalues) {
				fillInEntityObjectVOs(result, reffield, refvalue);
			}
		}
		return result;
	}
	
	public static Collection<EntityObjectVO<UID>> getEntityObjectVOs(FieldMeta<UID> reffield, UID refvalue) {
		List<EntityObjectVO<UID>> result = new ArrayList<EntityObjectVO<UID>>();
		return fillInEntityObjectVOs(result, reffield, refvalue);
	}
		
	private static Collection<EntityObjectVO<UID>> fillInEntityObjectVOs(Collection<EntityObjectVO<UID>> result, FieldMeta<UID> reffield, UID refvalue) {
		result.addAll(NucletDalProvider.getInstance().<UID>getEntityObjectProcessor(reffield.getEntity()).getBySearchExpression(
			new CollectableSearchExpression(SearchConditionUtils.newUidComparison(
					reffield,
					ComparisonOperator.EQUAL, 
					refvalue))));
		return result;
	}
	

	public static class NucletDependenceTransformer implements Transformer<EntityObjectVO<UID>, UID> {
		@Override
		public UID transform(EntityObjectVO<UID> i) {
			return i.getFieldUid(E.NUCLETDEPENDENCE.nucletDependence);
		}
	}
	
	public static List<TransferEO> getTransferNcObjects(INucletContent nc, Set<UID> nucletUIDs) {
		return CollectionUtils.transform(nc.getNcObjects(nucletUIDs), new NcToTeoTransformer());
	}
	
	public static EntityObjectVO<UID> getEntityObject(EntityMeta<UID> entity, UID uid) {
		return NucletDalProvider.getInstance().getEntityObjectProcessor(entity).getByPrimaryKey(uid);
	}
	
	private static class NcToTeoTransformer implements Transformer<EntityObjectVO<UID>, TransferEO> {
		
		public NcToTeoTransformer() {}

		@Override
		public TransferEO transform(EntityObjectVO<UID> eo) {
			return new TransferEO(eo);
		}
		
	}

//	private static class IdTransformer implements Transformer<Object, Long> {
//		@Override
//		public Long transform(Object i) {
//			if (i instanceof EntityObjectVO) {
//				return ((EntityObjectVO) i).getId();
//			} else if (i instanceof TransferEO) {
//				return ((TransferEO) i).eo.getId();
//			} else {
//				throw new IllegalArgumentException("Transform into id, class=" + i.getClass().getName());
//			}
//		}
//	}
//
//	private static class EntityObjectFieldValuePredicate implements Predicate<EntityObjectVO> {
//		private final String field;
//		private final String value;
//		public EntityObjectFieldValuePredicate(String field, String value) {
//			this.field = field;
//			this.value = value;
//		}
//		@Override
//		public boolean evaluate(EntityObjectVO t) {
//			return LangUtils.equals(t.getFieldValue(field, String.class), value);
//		}
//	}
//	
//	private static class TransferEOFieldValuePredicate implements Predicate<TransferEO> {
//		private final String field;
//		private final String value;
//		public TransferEOFieldValuePredicate(String field, String value) {
//			this.field = field;
//			this.value = value;
//		}
//		@Override
//		public boolean evaluate(TransferEO t) {
//			return LangUtils.equals(t.eo.getFieldValue(field, String.class), value);
//		}
//	}
	
	private static class TransferEOUidPredicate implements Predicate<TransferEO> {
		private final UID uid;
		public TransferEOUidPredicate(UID uid) {
			this.uid = uid;
		}
		@Override
		public boolean evaluate(TransferEO t) {
			return LangUtils.equal(t.eo.getPrimaryKey(), uid);
		}
	}

//	private static class EntityObjectIdPredicate implements Predicate<EntityObjectVO> {
//		private final Long id;
//		public EntityObjectIdPredicate(Long id) {
//			this.id = id;
//		}
//		@Override
//		public boolean evaluate(EntityObjectVO t) {
//			return LangUtils.equals(t.getId(), id);
//		}
//	}

	private static class UserEntityFieldPredicate implements Predicate<FieldMeta<?>> {
		@Override
		public boolean evaluate(FieldMeta<?> t) {
			if (E.STATEHISTORY.checkEntityUID(t.getEntity())) {
				return true;
			}
			if (E.isNuclosEntity(t.getEntity())) {
				return false;
			}
			return true;
		}
	}
	
	public static boolean existsReference(FieldMeta<UID> efMeta, UID uidReferenceToCheck) {
		try {
			Long count = NucletDalProvider.getInstance().getEntityObjectProcessor(efMeta.getEntity()).count(new CollectableSearchExpression(SearchConditionUtils.newUidComparison(
				efMeta,
				ComparisonOperator.EQUAL, 
				uidReferenceToCheck)));
			if (count > 0)
				return true;
		} catch (Exception e) {}

		return false;
	}
	
	public static FieldMeta<?> getRefToNuclet(EntityMeta<?> entity) {
		for (FieldMeta<?> field: entity.getFields()) {
			final UID fe = field.getForeignEntity();
			if (fe != null && E.NUCLET.checkEntityUID(fe)) {
				return field;
			}
		}
		throw new NuclosFatalException();
	}

	/**
	 * print list of Nuclet Contents to console
	 * @param args
	 */
	public static void main (String[] args) {
		AbstractNucletContent.CHECKS_ENABLED = false;
		Map<TransferOption, Serializable> transferOptions = new HashMap<>();
		transferOptions.put(TransferOption.IS_DIRECTORY_MODE, Boolean.TRUE);
		final List<INucletContent> nucletContentInstances = getNucletContentInstances(transferOptions, Process.CREATE);
		final Map<UID, INucletContent> mapNcByEntityId = new HashMap<>();
		final Map<INucletContent, String> mapTreeDescByNc = new HashMap<>();
		for (INucletContent nc : nucletContentInstances) {
			mapNcByEntityId.put(nc.getEntity().getUID(), nc);
		}
		for (INucletContent nc : nucletContentInstances) {
			mapTreeDescByNc.put(nc, getNucletContentTreeDescription(nc, mapNcByEntityId));
		}
		Collections.sort(nucletContentInstances, new Comparator<INucletContent>() {
			@Override
			public int compare(final INucletContent o1, final INucletContent o2) {
				String s1 = mapTreeDescByNc.get(o1);
				String s2 = mapTreeDescByNc.get(o2);
				return s1.compareToIgnoreCase(s2);
			}
		});
		for (INucletContent nc : nucletContentInstances) {
			printNucletContent(nc, mapTreeDescByNc);
		}
	}

	private static void printNucletContent(INucletContent nc, Map<INucletContent, String> mapTreeDescByNc) {
		StringBuilder s = new StringBuilder();
		s.append(mapTreeDescByNc.get(nc));
		if (!nc.getClass().equals(DefaultNucletContent.class)) {
			String sClassName = nc.getClass().getCanonicalName();
			sClassName = sClassName.substring(sClassName.lastIndexOf(".")+1);
			s.append("\t[class=");
			s.append(sClassName);
			s.append("];");
		}
		System.out.println(s);
	}

	private static String getNucletContentTreeDescription(INucletContent nc, Map<UID, INucletContent> mapNcByEntityId) {
		StringBuilder s = new StringBuilder();
		addParentsToStringBuilder(s, nc, mapNcByEntityId);
		s.append(getEntityName(nc.getEntity()));
		return s.toString();
	}

	private static void addParentsToStringBuilder(StringBuilder s, INucletContent nc, Map<UID, INucletContent> mapNcByEntityId) {
		if (nc.getParentEntity() != null) {
			final INucletContent parentNc = mapNcByEntityId.get(nc.getParentEntity().getUID());
			addParentsToStringBuilder(s, parentNc, mapNcByEntityId);
			s.append(getEntityName(parentNc.getEntity()));
			s.append(" --> ");
		}
	}

	private static String getEntityName(EntityMeta entity) {
		String sEntityName = entity.getEntityName();
		if (sEntityName.startsWith("nuclos_")) {
			sEntityName = sEntityName.substring("nuclos_".length());
		}
		return sEntityName;
	}

	public static TransferTreeNode buildNucletTree(final List<INucletContent> contentTypes, final NucletContentMap nucletContentMap) {
		TransferTreeNode root = new TransferTreeNode();
		INucletContent nucletType = null;
		for (INucletContent contentType : contentTypes) {
			if (contentType.getEntity().equals(E.NUCLET)) {
				nucletType = contentType;
				break;
			}
		}
		final INucletContent nucletTypeFinal = nucletType;
		final int cores = Runtime.getRuntime().availableProcessors();
		final ForkJoinPool threadPool = new ForkJoinPool(cores);
		final List<RecursiveAction> lstActions = new ArrayList<>();
		for (TransferEO nucletEO : nucletContentMap.getValues(E.NUCLET)) {
			RecursiveAction action = new RecursiveAction() {
				@Override
				protected void compute() {
					root.add(buildNucletSubTree(contentTypes, nucletContentMap, nucletTypeFinal, nucletEO, threadPool));
				}
			};
			lstActions.add(action);
			threadPool.execute(action);
		}
		for (RecursiveAction act : lstActions) {
			act.join();
		}
		return root;
	}

	private static TransferTreeNode buildNucletSubTree(final List<INucletContent> contentTypes, final NucletContentMap nucletContentMap,
													   final INucletContent parentType, final TransferEO parent, final ForkJoinPool threadPool) {
		String sTreeLabel = parentType.getIdentifier(parent.eo, nucletContentMap);
		TransferTreeNode subNode = new TransferTreeNode(parent, sTreeLabel);

		final List<RecursiveAction> lstActions = new ArrayList<>();
		for (INucletContent contentType : contentTypes) {
			RecursiveAction action = new RecursiveAction() {
				@Override
				protected void compute() {
					// parent relation
					for (TransferEO teo : nucletContentMap.getValues(contentType.getEntity())) {
						final FieldMeta<UID> fieldToParent = contentType.getFieldToParent();
						if (fieldToParent != null) {
							final UID iParentUID = teo.eo.getFieldUid(fieldToParent);
							if (RigidUtils.equal(iParentUID, parent.getUID()) && !parent.equals(teo)) {
								subNode.add(buildNucletSubTree(contentTypes, nucletContentMap, contentType, teo, threadPool));
							}
						}
					}
					// Nuclet relation
					if (contentType.getEntity().equals(E.NUCLET)) {
						for (INucletContent contentTypeForNucletRelation : contentTypes) {
							if (contentTypeForNucletRelation.getEntity().equals(E.NUCLET)) {
								continue;
							}
							FieldMeta<UID> foreignFieldToNuclet = null;
							try {
								foreignFieldToNuclet = getForeignFieldToNuclet(contentTypeForNucletRelation.getEntity());
							} catch (Exception ex) { /* ignore here */ }
							if (foreignFieldToNuclet != null) {
								for (TransferEO teo : nucletContentMap.getValues(contentTypeForNucletRelation.getEntity())) {
									final UID iParentUID = teo.eo.getFieldUid(foreignFieldToNuclet);
									if (RigidUtils.equal(iParentUID, parent.getUID())) {
										subNode.add(buildNucletSubTree(contentTypes, nucletContentMap, contentTypeForNucletRelation, teo, threadPool));
									}
								}
							}
						}
					}
				}
			};
			lstActions.add(action);
			threadPool.execute(action);
		}
		for (RecursiveAction act : lstActions) {
			act.join();
		}
		return subNode;
	}
}
