//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dbtransfer.content;

import java.util.List;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.dal.DalCallResult;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dbtransfer.Transfer;


public class NucletIntegrationPointFieldNucletContent extends DefaultNucletContent {

	public NucletIntegrationPointFieldNucletContent(List<INucletContent> contentTypes) {
		super(E.NUCLET_INTEGRATION_FIELD.integrationPoint, contentTypes);
	}

	@Override
	public boolean insertOrUpdateNcObject(DalCallResult result, EntityObjectVO<UID> ncObject, boolean testOnly, final Transfer transfer, final LogEntry log) {
		if (!ncObject.isFlagNew()) {
			EntityObjectVO<UID> existing = getEOOriginal(E.NUCLET_INTEGRATION_FIELD.getUID(), ncObject.getPrimaryKey());
			ncObject.setFieldUid(E.NUCLET_INTEGRATION_FIELD.targetField, existing.getFieldUid(E.NUCLET_INTEGRATION_FIELD.targetField));
			ncObject.setVersion(existing.getVersion());
		}
		return super.insertOrUpdateNcObject(result, ncObject, testOnly, transfer, log);
	}

}
