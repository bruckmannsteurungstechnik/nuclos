//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dbtransfer.content;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.dal.DalCallResult;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dbtransfer.NucletContentMap;
import org.nuclos.common.dbtransfer.Transfer;
import org.nuclos.common.dbtransfer.TransferEO;
import org.nuclos.common.dbtransfer.TransferOption;


public class NucletNucletContent extends DefaultNucletContent {

	public NucletNucletContent(List<INucletContent> contentTypes) {
		super(E.NUCLET, contentTypes, true);
	}
	
	@Override
	public boolean insertOrUpdateNcObject(DalCallResult result, EntityObjectVO<UID> ncObject, boolean testOnly, final Transfer transfer, final LogEntry log) {
//		EntityObjectVO<UID> existing = NucletDalProvider.getInstance().getEntityObjectProcessor(E.NUCLET).getByPrimaryKey(ncObject.getPrimaryKey());
		EntityObjectVO<UID> existing = getEO(E.NUCLET.getUID(), ncObject.getPrimaryKey());
		if (existing != null) {
			ncObject.setFieldValue(E.NUCLET.source, existing.getFieldValue(E.NUCLET.source));
		} else {
			ncObject.setFieldValue(E.NUCLET.source, false);
		}
		
		return super.insertOrUpdateNcObject(result, ncObject, testOnly, transfer, log);
	}

	@Override
	public List<EntityObjectVO<UID>> getNcObjects(final Set<UID> nucletUIDs) {
		return CollectionUtils.select(
					super.getNcObjects(nucletUIDs), 
					new Predicate<EntityObjectVO<UID>>() {
						@Override
						public boolean evaluate(EntityObjectVO<UID> t) {
							return nucletUIDs.contains(t.getPrimaryKey());
						}
					});
	}

	@Override
	public boolean validate(TransferEO teo, ValidationType type, NucletContentMap importContentMap,
							Set<UID> existingNucletUIDs, LogEntry validitylog, Map<TransferOption, Serializable> transferOptions, boolean hasDataLanguages) {
		boolean bResult = super.validate(teo, type, importContentMap, existingNucletUIDs, validitylog, transferOptions, hasDataLanguages);
		if (bResult) {
			// Insert is checked before UPDATE, it is enough here
			if (type == ValidationType.INSERT) {
				final String sPackage = teo.eo.getFieldValue(E.NUCLET.packagefield);
				final String sName = teo.eo.getFieldValue(E.NUCLET.name);
				final String sLocalIdentifier = teo.eo.getFieldValue(E.NUCLET.localidentifier);
				Optional<EntityObjectVO<UID>> firstExistingNucletWithSameValue =
						getAllEOs(E.NUCLET.getUID()).stream().filter(eo -> {
							return eo.getFieldValue(E.NUCLET.packagefield).equals(sPackage)
									|| eo.getFieldValue(E.NUCLET.name).equals(sName)
									|| eo.getFieldValue(E.NUCLET.localidentifier).equals(sLocalIdentifier) ;
						}).findFirst();
				if (firstExistingNucletWithSameValue.isPresent() && !firstExistingNucletWithSameValue.get().getPrimaryKey().equals(teo.getUID())) {
					// Value (Pacakge, Name, LocalIdentifier) is in use by other Nuclet!
					validitylog.newCriticalLine("Unique constraint violation found. Package, name and/or local identifier is in use!");
					validitylog.newCriticalLine(".......Existing Nuclet: " + getUniqueNucletValuesPresentation(firstExistingNucletWithSameValue.get()));
					validitylog.newCriticalLine(".......Nuclet to import: " + getUniqueNucletValuesPresentation(teo.eo));
					bResult = false;
				}
			}
		}
		return bResult;
	}

	private String getUniqueNucletValuesPresentation(EntityObjectVO<UID> eo) {
		return String.format("Nuclet[ID=%s, PACKAGE=%s, NAME=%s, LOCAL_IDENTIFIER=%s",
				eo.getPrimaryKey().getString(),
				eo.getFieldValue(E.NUCLET.packagefield),
				eo.getFieldValue(E.NUCLET.name),
				eo.getFieldValue(E.NUCLET.localidentifier));
	}
	
}
