//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dbtransfer.content;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.common.valueobject.DocumentFileBase;
import org.nuclos.server.resource.valueobject.ResourceFile;

public class ResourceNucletContent extends DefaultNucletContent implements IExternalizeBytes {

	public ResourceNucletContent(List<INucletContent> contentTypes) {
		super(E.RESOURCE, contentTypes);
	}

	@Override
	public Map<FieldMeta<?>, Pair<String, byte[]>> externalize(EntityObjectVO<UID> ncObject) {
		DocumentFileBase resfile = ncObject.getFieldValue(E.RESOURCE.file);
		if (resfile != null) {
			String name = resfile.getFilename();
			if (name != null && name.contains(".")) {
				String fileType = name.substring(name.lastIndexOf(".")+1);
				if (fileType != null && fileType.length() > 0) {
					byte[] bytes = ncObject.getFieldValue(E.RESOURCE.content);
					if (bytes != null) {
						Map<FieldMeta<?>, Pair<String, byte[]>> result = new HashMap<FieldMeta<?>, Pair<String, byte[]>>();
						result.put(E.RESOURCE.file, new Pair<String, byte[]>(fileType.toLowerCase(), bytes));
						ncObject.setFieldValue(E.RESOURCE.file, new ResourceFile(resfile.getFilename(), resfile.getDocumentFilePk()));
						return result;
					}
				}
			}
		}
		return null;
	}

	@Override
	public void importBytes(EntityObjectVO<UID> ncObject, FieldMeta<?> efMeta, byte[] bytes) {
		if (E.RESOURCE.file.equals(efMeta)) {
			DocumentFileBase resfile = ncObject.getFieldValue(E.RESOURCE.file);
			if (resfile != null) {
				ncObject.setFieldValue(E.RESOURCE.file, new ResourceFile(resfile.getFilename(), bytes));
			}
		}
	}
	
}
