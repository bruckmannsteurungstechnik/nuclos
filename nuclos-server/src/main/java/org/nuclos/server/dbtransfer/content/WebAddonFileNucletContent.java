//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dbtransfer.content;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.DalCallResult;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dbtransfer.Transfer;
import org.nuclos.server.common.valueobject.DocumentFileBase;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.documentfile.DocumentFileUtils;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;

import com.google.common.io.Files;

public class WebAddonFileNucletContent extends DefaultNucletContent implements IExternalizeBytes {
	
	public WebAddonFileNucletContent(List<INucletContent> contentTypes) {
		super(E.WEBADDON_FILE.webAddon, contentTypes);
	}

	@Override
	public UID getIdentifierField() {
		return E.WEBADDON_FILE.file.getUID();
	}

	@Override
	public EntityObjectVO<UID> readNc(EntityObjectVO<UID> ncObject) {
		loadDocumentFileContent(ncObject, E.WEBADDON_FILE.file.getUID());
		// we do not need the backup here (for the change detection)
		ncObject.removeFieldValue(E.WEBADDON_FILE.content.getUID());
		return ncObject;
	}

	@Override
	public EntityObjectVO<UID> readNcOriginal(final EntityObjectVO<UID> ncObject) {
		loadDocumentFileContent(ncObject, E.WEBADDON_FILE.file.getUID());
		return ncObject;
	}

	@Override
	public void deleteNcObject(DalCallResult result, EntityObjectVO<UID> ncObject, boolean testOnly) {
		if (!testOnly) {
			DocumentFileBase docfile = (DocumentFileBase) NucletDalProvider.getInstance().getEntityObjectProcessor(getEntity()).getByPrimaryKey(ncObject.getPrimaryKey()).getFieldValue(E.WEBADDON_FILE.file.getUID());
			if (docfile != null) {
				DocumentFileUtils.removeDocumentFiles(ncObject);
			}
		}
		super.deleteNcObject(result, ncObject, testOnly);
	}

	@Override
	public boolean insertOrUpdateNcObject(DalCallResult result, EntityObjectVO<UID> ncObject, boolean testOnly, final Transfer transfer, final LogEntry log) {
		if (ncObject.isFlagUpdated()) {
			// remove backup from cache also (for the change detection)
			EntityObjectVO<UID> eoFromCache = getEOOriginal(getEntity().getUID(), ncObject.getPrimaryKey());
			eoFromCache.removeFieldValue(E.WEBADDON_FILE.content.getUID());
		}
		if (!testOnly) {
			Map<UID, UID> existingDocumentFileMap = null;
			if (!ncObject.isFlagNew()) {
				existingDocumentFileMap = DocumentFileUtils.getExistingDocumentFiles(ncObject);
			}
			DocumentFileUtils.storeDocumentFiles(ncObject, existingDocumentFileMap);
		}
		return super.insertOrUpdateNcObject(result, ncObject, testOnly, transfer, log);
	}

	@Override
	public Map<FieldMeta<?>, Pair<String, byte[]>> externalize(EntityObjectVO<UID> ncObject) {
		DocumentFileBase docfile = (DocumentFileBase) ncObject.getFieldValue(E.WEBADDON_FILE.file.getUID());
		if (docfile != null) {
			byte[] bytes = docfile.getContents();
			if (bytes != null) {
				Map<FieldMeta<?>, Pair<String, byte[]>> result = new HashMap<FieldMeta<?>, Pair<String, byte[]>>();
				result.put(E.WEBADDON_FILE.file, new Pair<String, byte[]>(Files.getFileExtension(docfile.getFilename()).toLowerCase(), bytes));
				ncObject.setFieldValue(E.WEBADDON_FILE.file.getUID(), new GenericObjectDocumentFile(docfile.getFilename(), docfile.getDocumentFilePk()));
				return result;
			}
		}
		return null;
	}

	@Override
	public void importBytes(EntityObjectVO<UID> ncObject, FieldMeta<?> efMeta, byte[] bytes) {
		if (E.WEBADDON_FILE.file.equals(efMeta)) {
			DocumentFileBase docfile = (DocumentFileBase) ncObject.getFieldValue(E.WEBADDON_FILE.file.getUID());
			if (docfile != null) {
				ncObject.setFieldValue(E.WEBADDON_FILE.file.getUID(), new GenericObjectDocumentFile(docfile.getFilename(), docfile.getDocumentFilePk(), bytes));
			}
		}
	}
	
}
