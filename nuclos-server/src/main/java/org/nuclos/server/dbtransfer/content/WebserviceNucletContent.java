package org.nuclos.server.dbtransfer.content;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.DalCallResult;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dbtransfer.Transfer;
import org.nuclos.server.common.valueobject.DocumentFileBase;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.documentfile.DocumentFileUtils;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;

public class WebserviceNucletContent extends DefaultNucletContent implements IExternalizeBytes {

	public WebserviceNucletContent(List<INucletContent> contentTypes) {
		super(E.WEBSERVICE, contentTypes);
	}

	@Override
	public EntityObjectVO<UID> readNc(EntityObjectVO<UID> ncObject) {
		loadDocumentFileContent(ncObject, E.WEBSERVICE.wsdlfile.getUID());
		// we do not need the backup here (for the change detection)
		ncObject.removeFieldValue(E.WEBSERVICE.content.getUID());
		return ncObject;
	}

	@Override
	public EntityObjectVO<UID> readNcOriginal(final EntityObjectVO<UID> ncObject) {
		loadDocumentFileContent(ncObject, E.WEBSERVICE.wsdlfile.getUID());
		return ncObject;
	}

	@Override
	public void deleteNcObject(DalCallResult result, EntityObjectVO<UID> ncObject, boolean testOnly) {
		if (!testOnly) {
			DocumentFileBase docfile = (DocumentFileBase) NucletDalProvider.getInstance().getEntityObjectProcessor(getEntity()).getByPrimaryKey(ncObject.getPrimaryKey()).getFieldValue(E.WEBADDON_FILE.file.getUID());
			if (docfile != null) {
				DocumentFileUtils.removeDocumentFiles(ncObject);
			}
		}
		super.deleteNcObject(result, ncObject, testOnly);
	}

	@Override
	public boolean insertOrUpdateNcObject(DalCallResult result, EntityObjectVO<UID> ncObject, boolean testOnly, final Transfer transfer, final LogEntry log) {
		if (ncObject.isFlagUpdated()) {
			// remove backup from cache also (for the change detection)
			EntityObjectVO<UID> eoFromCache = getEOOriginal(getEntity().getUID(), ncObject.getPrimaryKey());
			eoFromCache.removeFieldValue(E.WEBSERVICE.content.getUID());
		}
		if (!testOnly) {
			Map<UID, UID> existingDocumentFileMap = null;
			if (!ncObject.isFlagNew()) {
				existingDocumentFileMap = DocumentFileUtils.getExistingDocumentFiles(ncObject);
			}
			DocumentFileUtils.storeDocumentFiles(ncObject, existingDocumentFileMap);
		}
		return super.insertOrUpdateNcObject(result, ncObject, testOnly, transfer, log);
	}
	
	@Override
	public Map<FieldMeta<?>, Pair<String, byte[]>> externalize(EntityObjectVO<UID> ncObject) {
		DocumentFileBase docfile = (DocumentFileBase) ncObject.getFieldValue(E.WEBSERVICE.wsdlfile.getUID());
		if (docfile != null) {
				String name = docfile.getFilename();
			if (name != null && name.contains(".")) {
				String fileType = name.substring(name.lastIndexOf(".")+1);
				if (fileType != null && fileType.length() > 0) {
					byte[] bytes = docfile.getContents();
					if (bytes != null) {
						Map<FieldMeta<?>, Pair<String, byte[]>> result = new HashMap<FieldMeta<?>, Pair<String, byte[]>>();
						result.put(E.WEBSERVICE.wsdlfile, new Pair<String, byte[]>(fileType.toLowerCase(), bytes));
						ncObject.setFieldValue(E.WEBSERVICE.wsdlfile.getUID(), new GenericObjectDocumentFile(docfile.getFilename(), docfile.getDocumentFilePk()));
						return result;
					}
				}
			}
		}
		return null;
	}

	@Override
	public void importBytes(EntityObjectVO<UID> ncObject, FieldMeta<?> efMeta, byte[] bytes) {
		if (E.WEBSERVICE.wsdlfile.equals(efMeta)) {
			DocumentFileBase docfile = (DocumentFileBase) ncObject.getFieldValue(E.WEBSERVICE.wsdlfile.getUID());
			if (docfile != null) {
				ncObject.setFieldValue(E.WEBSERVICE.wsdlfile.getUID(), new GenericObjectDocumentFile(docfile.getFilename(), docfile.getDocumentFilePk(), bytes));
			}
		}
	}
}
