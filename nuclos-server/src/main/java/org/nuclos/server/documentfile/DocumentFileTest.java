package org.nuclos.server.documentfile;

import java.io.File;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuclos.common.NuclosFile;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.test.IntegrationTest;

public class DocumentFileTest implements IntegrationTest {
	
	private static final Logger LOG = LogManager.getLogger(DocumentFileTest.class);

	@Override
	public Map<String, Object> run(String sTestcase, Map<String, Object> mapParams) {
		switch (sTestcase) {
		
		case "getStoredFiles": getStoredFiles(mapParams); break;
		case "validateNewLink": validateNewLink(mapParams); break;
		case "removeStoredFile": removeStoredFile(mapParams); break;
		case "getAttachmentId": getAttachmentId(mapParams); break;

		default:
			throw new RuntimeException("Unkown documentFile Testcase " + sTestcase);
		}
		
		return mapParams;
	}
	
	private static void getStoredFiles(Map<String, Object> mapParams) {
		File dir = NuclosSystemParameters.getDirectory(NuclosSystemParameters.DOCUMENT_PATH);
		String[] result = dir.list();
		mapParams.put("storedFiles", result==null?new String[]{}:result);
	}
	
	private static void validateNewLink(Map<String, Object> mapParams) {
		Long objectID = (Long) mapParams.get("objectID");
		UID fieldUID = (UID) mapParams.get("fieldUID");
		
		UID documentFileUID = SpringApplicationContextHolder.getBean(DocumentFileFacadeLocal.class).findDocumentFileUID(fieldUID, objectID);
		if (documentFileUID == null) {
			throw new RuntimeException("documentFileUID not found for objectID=" + objectID + " and fieldUID=" + fieldUID);
		}
		mapParams.put("newDocumentFileUID", documentFileUID);
	}
	
	private static void removeStoredFile(Map<String, Object> mapParams) {
		String sFilename = (String) mapParams.get("removeStoredFile");
		File dir = NuclosSystemParameters.getDirectory(NuclosSystemParameters.DOCUMENT_PATH);
		File file = new File(dir, sFilename);
		if (file.exists()) {
			file.delete();
		} else {
			throw new RuntimeException("File " + sFilename + " not found in " + dir);
		}
	}
	
	private static void getAttachmentId(Map<String, Object> mapParams) {
		NuclosFile nFile = (NuclosFile) mapParams.get("attachmentNuclosFile");
		Long attachmentID = nFile.getAttachmentId();
		if (attachmentID == null) {
			throw new RuntimeException("attachmentID of " + nFile + " is not set");
		}
		mapParams.put("attachmentID", attachmentID);
	}

}
