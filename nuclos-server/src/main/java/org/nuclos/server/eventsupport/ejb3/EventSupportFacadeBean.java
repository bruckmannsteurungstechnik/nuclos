package org.nuclos.server.eventsupport.ejb3;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.stream.Collectors;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.NullArgumentException;
import org.apache.xml.serializer.OutputPropertiesFactory;
import org.nuclos.api.User;
import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.Query;
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute;
import org.nuclos.api.businessobject.facade.Modifiable;
import org.nuclos.api.businessobject.facade.thin.Stateful;
import org.nuclos.api.communication.CommunicationPort;
import org.nuclos.api.context.InputRequiredException;
import org.nuclos.api.context.JobContext;
import org.nuclos.api.context.PrintContext;
import org.nuclos.api.context.PrintResult;
import org.nuclos.api.context.RuleContext;
import org.nuclos.api.context.StateChangeContext;
import org.nuclos.api.context.communication.CommunicationContext;
import org.nuclos.api.context.communication.PhoneCallRequestContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.locale.NuclosLocale;
import org.nuclos.api.printout.Printout;
import org.nuclos.api.printout.PrintoutList;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.report.OutputFormat;
import org.nuclos.api.rule.CollectiveProcessingProxy;
import org.nuclos.api.rule.CommunicationRule;
import org.nuclos.api.rule.CustomRule;
import org.nuclos.api.rule.DeleteFinalRule;
import org.nuclos.api.rule.DeleteRule;
import org.nuclos.api.rule.GenerateFinalRule;
import org.nuclos.api.rule.GenerateRule;
import org.nuclos.api.rule.InsertFinalRule;
import org.nuclos.api.rule.InsertRule;
import org.nuclos.api.rule.JobRule;
import org.nuclos.api.rule.PrintFinalRule;
import org.nuclos.api.rule.PrintRule;
import org.nuclos.api.rule.StateChangeFinalRule;
import org.nuclos.api.rule.StateChangeRule;
import org.nuclos.api.rule.TransactionalJobRule;
import org.nuclos.api.rule.UpdateFinalRule;
import org.nuclos.api.rule.UpdateRule;
import org.nuclos.api.statemodel.State;
import org.nuclos.businessentity.NucletExtension;
import org.nuclos.businessentity.rule.annotation.SystemRuleUsage;
import org.nuclos.businessentity.utils.BusinessObjectBuilderForInternalUse;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.EventSupportNotification;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.MandatorVO;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.PropertiesMap;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.RuleNotification;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.printservice.NuclosPrintoutList;
import org.nuclos.common.report.valueobject.DefaultReportOutputVO;
import org.nuclos.common.report.valueobject.DefaultReportVO;
import org.nuclos.common.report.valueobject.OutputFormatTO;
import org.nuclos.common.report.valueobject.PrintoutTO;
import org.nuclos.common.report.valueobject.ReportVO;
import org.nuclos.common.security.UserVO;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.MailException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.common2.exception.NuclosCompileException.ErrorMessage;
import org.nuclos.common2.exception.NuclosExceptions;
import org.nuclos.common2.exception.NuclosRuleCompileException;
import org.nuclos.server.api.eventsupport.CustomEventObjectImpl;
import org.nuclos.server.api.eventsupport.DeleteEventObjectImpl;
import org.nuclos.server.api.eventsupport.GenerateContextImpl;
import org.nuclos.server.api.eventsupport.InsertEventObjectImpl;
import org.nuclos.server.api.eventsupport.NotifiableEventObject;
import org.nuclos.server.api.eventsupport.PrintEventObjectImpl;
import org.nuclos.server.api.eventsupport.PrintFinalEventObjectImpl;
import org.nuclos.server.api.eventsupport.StateChangeContextImpl;
import org.nuclos.server.api.eventsupport.TimelimitEventObjectImpl;
import org.nuclos.server.api.eventsupport.UpdateEventObjectImpl;
import org.nuclos.server.common.EventSupportCache;
import org.nuclos.server.common.GenerationCache;
import org.nuclos.server.common.JobCache;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.ServerServiceLocator;
import org.nuclos.server.common.StateCache;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.communication.NuclosCommunicationBuilder;
import org.nuclos.server.communication.context.impl.PhoneCallRequestContextImpl;
import org.nuclos.server.customcode.CustomCodeManager;
import org.nuclos.server.customcode.codegenerator.GeneratorClasspathComponent;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants;
import org.nuclos.server.customcode.codegenerator.NuclosJarGeneratorManager;
import org.nuclos.server.customcode.codegenerator.NuclosJavaCompilerComponent;
import org.nuclos.server.customcode.codegenerator.NuclosJavaCompilerExceptionCache;
import org.nuclos.server.customcode.codegenerator.SourceScannerComponent;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.eventsupport.valueobject.CommunicationPortVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportCommunicationPortVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportEventVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportGenerationVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportJobVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportTransitionVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportTypeVO;
import org.nuclos.server.eventsupport.valueobject.ProcessVO;
import org.nuclos.server.fileimport.ImportStructureDefinitionBuilder;
import org.nuclos.server.genericobject.ejb3.GeneratorObjectBuilder;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.i18n.language.data.DataLanguageCache;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.nuclos.server.job.ejb3.JobControlFacadeRemote;
import org.nuclos.server.job.valueobject.JobVO;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.nbo.AbstractBusinessObject;
import org.nuclos.server.nbo.AbstractNuclosObjectCompiler;
import org.nuclos.server.nbo.AbstractOutputFormat;
import org.nuclos.server.nbo.EOBOBridge;
import org.nuclos.server.nbo.NuclosBusinessObjectBuilder;
import org.nuclos.server.parameter.NuclosParameterBuilder;
import org.nuclos.server.printservice.printout.PrintoutObjectBuilder;
import org.nuclos.server.printservice.printout.PrintoutWrapper;
import org.nuclos.server.report.ReportDatasourceObjectBuilder;
import org.nuclos.server.report.ReportObjectBuilder;
import org.nuclos.server.report.ReportVOBoResolver;
import org.nuclos.server.report.ejb3.ReportFacadeLocal;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.nuclos.server.ruleengine.NuclosInputRequiredException;
import org.nuclos.server.security.UserFacadeLocal;
import org.nuclos.server.security.UserImpl;
import org.nuclos.server.statemodel.StatemodelObjectBuilder;
import org.nuclos.server.statemodel.ejb3.StateFacadeLocal;
import org.nuclos.server.statemodel.valueobject.StateModelVO;
import org.nuclos.server.statemodel.valueobject.StateTransitionVO;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.nuclos.server.user.UserRoleObjectBuilder;
import org.nuclos.server.web.NucletExtensionLoader;
import org.nuclos.server.webservice.ejb3.WebServiceObjectBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * Facade bean for event support management. <br>
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Transactional(noRollbackFor = { Exception.class })
public class EventSupportFacadeBean extends NuclosFacadeBean implements EventSupportFacadeLocal, EventSupportFacadeRemote {

	private static final Logger LOG = LoggerFactory.getLogger(EventSupportFacadeBean.class);

	// Spring injection
	
	@Autowired
	private NuclosJavaCompilerComponent compiler;
	
	@Autowired
	private CustomCodeManager ccm;

	@Autowired
	private NuclosBusinessObjectBuilder nuclosBusinessObjectBuilder;

	@Autowired
	private MasterDataFacadeLocal masterDataFacade;
	
	@Autowired
	private EventSupportCache esCache;
	
	@Autowired
	private JobCache jobCache;
	
	@Autowired
	private StateCache stateCache;
	
	@Autowired
	private GenerationCache genCache;
	
	@Autowired
	private NuclosJavaCompilerExceptionCache compileCache;
	
	@Autowired
	private ImportStructureDefinitionBuilder isdBuilder;
	
	@Autowired
	private ReportObjectBuilder repObBuilder;
	
	@Autowired
	private PrintoutObjectBuilder printObBuilder;
	
	@Autowired
	private ReportVOBoResolver printBoResolver;
	
	@Autowired
	private StatemodelObjectBuilder stateObBuilder;
	
	@Autowired
	private ReportDatasourceObjectBuilder datasourceObBuilder;
	
	@Autowired
	private GeneratorObjectBuilder genObBuilder;
	
	@Autowired
	private NuclosParameterBuilder paramObBuilder;
	
	@Autowired
	private WebServiceObjectBuilder wsObBuilder;
	
	@Autowired
	private NuclosCommunicationBuilder communicationObBuilder;
	
	@Autowired
	private UserRoleObjectBuilder usergroupObBuilder;
	
	@Autowired
	private SpringDataBaseHelper dataBaseHelper;
	
	@Autowired
	private JobControlFacadeRemote jobControl;
	
	@Autowired
	private DataLanguageCache dataLanguageCache;
	
	@Autowired
	private PrintoutWrapper printoutWrapper;
	
	@Autowired
	private SecurityCache securityCache;

	@Autowired
	private ServerServiceLocator serverServiceLocator;

	@Autowired
	private ServerParameterProvider serverParameterProvider;
	
	@Autowired
	private SourceScannerComponent sourceScanner;

	@Autowired
	private UserFacadeLocal userFacade;

	@Autowired
	private GeneratorClasspathComponent generatorClasspathComponent;

	// end of Spring injection

	private static final String INTERNAL_SYSTEM_RULE_PACKAGE = SystemRuleUsage.class.getPackage().getName().replace(".annotation", "");
	
	public EventSupportFacadeBean() {
	}

	public <T> EntityObjectVO<T> fireStateTransitionEventSupport(UID sourceStateUID, UID targetStateUID, EntityObjectVO<T> loccvoBefore, String eventClassType)
			throws NuclosBusinessRuleException, CommonFinderException, CommonPermissionException, NuclosCompileException {
		EntityObjectVO<T> retVal = loccvoBefore;

		StateFacadeLocal facade = serverServiceLocator.getFacade(StateFacadeLocal.class);

		Map<String, Object> cache = new HashMap<>();

		StateTransitionVO stVO = (sourceStateUID == null) ? facade
				.findStateTransitionByNullAndTargetState(targetStateUID)
				: facade.findStateTransitionBySourceAndTargetState(
						sourceStateUID, targetStateUID);
		StateVO sVOSource = sourceStateUID != null ? stateCache.getStateById(sourceStateUID) : null;
		StateVO sVOTarget = stateCache.getStateById(targetStateUID);

		try {
			if (stVO != null) {
				Collection<EventSupportTransitionVO> eventSupportsByTransitionId = getEventSupportsByTransitionUid(stVO
						.getId());
				eventSupportsByTransitionId = CollectionUtils.sorted(eventSupportsByTransitionId, new Comparator<EventSupportTransitionVO>() {
					@Override
					public int compare(EventSupportTransitionVO o1, EventSupportTransitionVO o2) {
						return o1.getOrder().compareTo(o2.getOrder());
					}
				});
				
				if (eventSupportsByTransitionId.size() > 0) {
					
					try { setMandatorRestriction(retVal);
					
					for (EventSupportTransitionVO estVO : eventSupportsByTransitionId) {
						if (estVO.getEventSupportClassType().equals(
								eventClassType)) {
							// Only execute active rules
							if (isActiveEventSupport(estVO.getEventSupportClass())) {
								if (estVO.getTransition().equals(stVO.getId())) {

									// Instantiate the corresponding JPA file
									AbstractBusinessObject newInstance = createBusinessObject(retVal, estVO.getEventSupportClass());

									if (newInstance == null) {
										throw new NuclosBusinessRuleException(
												"BusinessObject cannot be found");
									}
									if (!(newInstance instanceof Stateful)) {
										throw new NuclosBusinessRuleException(
												"BusinessObject is not of type StateableBusinessObject");
									}
				
									State source = sVOSource != null 
											? new State(sVOSource.getId(), sVOSource.getDescription(Locale.ENGLISH), 
													sVOSource.getStatename(Locale.ENGLISH), sVOSource.getNumeral(), sVOSource.getModelUID()) 
											: null;
									State target = new State(sVOTarget.getId(), sVOTarget.getDescription(Locale.ENGLISH), 
											sVOTarget.getStatename(Locale.ENGLISH), sVOTarget.getNumeral(), sVOTarget.getModelUID());				
									StateChangeContext stObject = new StateChangeContextImpl(
											cache, loccvoBefore.getDalEntity(), (Stateful) newInstance, estVO.getEventSupportClass(),
											target,source, this.dataLanguageCache.getLanguageToUse());
									
									if (StateChangeRule.class.getCanonicalName().equals(estVO.getEventSupportClassType())) {

										StateChangeRule loadedStateChangeSupport = newEventSupportInstance(estVO.getEventSupportClass());
										loadedStateChangeSupport.changeState(stObject);
									} else {
										StateChangeFinalRule loadedStateChangeSupport = newEventSupportInstance(estVO.getEventSupportClass());
										loadedStateChangeSupport.changeStateFinal(stObject);
									}

									retVal = EOBOBridge.getEO(newInstance);
								}
							}
						}
					}
					
					} finally { revertMandatorRestriction(); }
				}

			}
		} catch (org.nuclos.api.exception.PointerException e) {
			throw new org.nuclos.common.PointerException(e.getMessage());
		} catch (BusinessException e) {
			throw new NuclosBusinessRuleException(e);
		}
		return retVal;
	}

	public List<EntityObjectVO<UID>> getAllServerCode() {
		return nucletDalProvider.getEntityObjectProcessor(E.SERVERCODE).getAll();
	}

	public List<EventSupportEventVO> getEventSupportEntitiesByClassname(String classname) {
		final List<EventSupportEventVO> retVal = new ArrayList<>();
		for (EventSupportEventVO eseVO : esCache.getEventSupportEntities()) {
			if (eseVO.getEventSupportClass().equals(classname))
				retVal.add(eseVO);
		}
		return retVal;
	}

	public List<JobVO> getJobsByClassname(String classname) throws CommonFinderException, CommonPermissionException {
		final List<JobVO> retVal = new ArrayList<>();
		for (EventSupportJobVO eseVO : esCache.getEventSupportJobs()) {
			if (eseVO.getEventSupportClass().equals(classname)) {
				retVal.add(jobCache.getJob(eseVO.getJobControllerUID()));
			}	
		}
		return retVal;
	}

	public List<GeneratorActionVO> getGenerationsByClassname(String classname) throws CommonFinderException, CommonPermissionException {
		final List<GeneratorActionVO> retVal = new ArrayList<>();
		for (EventSupportGenerationVO eseGenVO : esCache.getEventSupportGenerations()) {
			if (classname.equals(eseGenVO.getEventSupportClass())) {
				retVal.add(genCache.getGenerationAction(eseGenVO.getGeneration()));
			}
		}
		return retVal;
	}

	public List<StateModelVO> getStateModelsByEventSupportClassname(
			String classname) throws CommonFinderException {
		final List<StateModelVO> retVal = new ArrayList<>();
		for (EventSupportTransitionVO esTransVO : esCache.getEventSupportTransitions()) {
			if (classname.equals(esTransVO.getEventSupportClass())) {
				for (StateVO state : stateCache.getStatesByTransitionId(esTransVO.getTransition())) {
					if (state != null) {
						StateModelVO statemodelById = stateCache.getModelById(state.getModelUID());
						if (!retVal.contains(statemodelById))
							retVal.add(statemodelById);							
					}
				}
			}
		}
		return retVal;
	}

	@Override
	public EventSupportEventVO createEventSupportEvent(
			EventSupportEventVO eseVOToInsert)
			throws CommonPermissionException, CommonValidationException,
			NuclosBusinessRuleException, CommonCreateException {

		this.checkWriteAllowed(E.SERVERCODEENTITY);

		eseVOToInsert.validate();
		eseVOToInsert = MasterDataWrapper.getEventSupportEventVO(masterDataFacade.create( 
				MasterDataWrapper.wrapEventSupportEventVO(eseVOToInsert), null));

		esCache.insertEventSupportEntityCache(eseVOToInsert);
		
		return eseVOToInsert;
	}

	public EventSupportEventVO modifyEventSupportEvent(
			EventSupportEventVO eseVOToUpdate) 
					throws CommonPermissionException, CommonValidationException, CommonFinderException, 
					NuclosBusinessRuleException, CommonCreateException, CommonRemoveException, CommonStaleVersionException, NuclosCompileException {
		
		this.checkWriteAllowed(E.SERVERCODEENTITY.getUID());
		
		eseVOToUpdate.validate();
		
		esCache.removeEventSupportEntityCache(eseVOToUpdate.getEntityUID(), eseVOToUpdate.getId());
		
		UID objId = masterDataFacade.modify(
				MasterDataWrapper.wrapEventSupportEventVO(eseVOToUpdate), null);
		
		eseVOToUpdate = MasterDataWrapper.getEventSupportEventVO(
				masterDataFacade.get(E.SERVERCODEENTITY, objId));
		
		esCache.insertEventSupportEntityCache(eseVOToUpdate);
		
		return eseVOToUpdate;
	}

	public void deleteEventSupportEvent(EventSupportEventVO eseVOToDelete) 
			throws CommonPermissionException, CommonValidationException, NuclosBusinessRuleException, CommonFinderException, 
			CommonRemoveException, CommonStaleVersionException, NuclosCompileException {
		
		this.checkWriteAllowed(E.SERVERCODEENTITY);
		
		eseVOToDelete.validate();			
		masterDataFacade.remove(E.SERVERCODEENTITY.getUID(), eseVOToDelete.getPrimaryKey(), true);
		
		esCache.removeEventSupportEntityCache(RigidUtils.defaultIfNull(eseVOToDelete.getIntegrationPointUID(),eseVOToDelete.getEntityUID()),eseVOToDelete.getId());
	}

	public void deleteEventSupport(EventSupportSourceVO eseVOToDelete)
			throws CommonPermissionException, CommonValidationException,
			NuclosCompileException {

		this.checkWriteAllowed(E.SERVERCODE);
		eseVOToDelete.validate();
		List<EntityObjectVO<UID>> bySearchExpression = nucletDalProvider
				.getEntityObjectProcessor(E.SERVERCODE)
				.getBySearchExpression(
						new CollectableSearchExpression(SearchConditionUtils
								.newComparison(E.SERVERCODE.name,
										ComparisonOperator.EQUAL,
										eseVOToDelete.getClassname())));

		// Due to contraints there can only be one element
		if (bySearchExpression != null && bySearchExpression.size() > 0) {

			try {
				EntityObjectVO<UID> eo = bySearchExpression.get(0);
				masterDataFacade.remove(eo.getDalEntity(), eo.getPrimaryKey(), false);
			} catch (Exception e) {
				throw new NuclosFatalException(e);
			}
			// Remove file from sourcefolder and rebuild jar file		
			// Rule and all its usages are removed from cache;
			esCache.removeEventSupportCache(eseVOToDelete);
			
			this.ccm.getNuclosJavaCompilerComponent().forceCompile();
			this.ccm.getClassLoaderAndCompileIfNeeded();
		}
		else {
			throw new CommonValidationException("Number of found rules for name '" + eseVOToDelete.getClassname() + "' is not one.");
		}
		
	}

	public List<ProcessVO> getProcessesByModuleUid(UID moduleUID) {
		List<ProcessVO> retVal = new ArrayList<>();

		List<EntityObjectVO<UID>> results = nucletDalProvider
				.getEntityObjectProcessor(E.PROCESS)
				.getBySearchExpression(
						new CollectableSearchExpression(SearchConditionUtils
								.newUidComparison(E.PROCESS.module,
										ComparisonOperator.EQUAL, moduleUID)));

		for (EntityObjectVO<UID> eoVO : results) {
			retVal.add(MasterDataWrapper.getProcessVO(eoVO));
		}

		return retVal;
	}

	@Override
	public EventSupportTransitionVO createEventSupportTransition(
			EventSupportTransitionVO eseVOToInsert)
			throws CommonPermissionException, CommonValidationException,
			NuclosBusinessRuleException, CommonCreateException {
		this.checkWriteAllowed(E.SERVERCODETRANSITION);

		eseVOToInsert.validate();
		eseVOToInsert = MasterDataWrapper.getEventSupportTransitionVO(
			masterDataFacade.create( 
				MasterDataWrapper.wrapEventSupportTransitionVO(eseVOToInsert), null));

		esCache.insertEventSupportTransitionCache(eseVOToInsert);
		
		stateCache.invalidateCache(true, true);
		
		return eseVOToInsert;
	}

	@Override
	public EventSupportTransitionVO modifyEventSupportTransition(
		EventSupportTransitionVO eseVOToUpdate, UID oldTransaction) throws CommonPermissionException, CommonValidationException, 
			NuclosBusinessRuleException, CommonCreateException, CommonFinderException, CommonRemoveException, 
			CommonStaleVersionException, NuclosCompileException {
		
		this.checkWriteAllowed(E.SERVERCODETRANSITION);
		eseVOToUpdate.validate();			
		
		esCache.removeEventSupportTransitionCache(oldTransaction, eseVOToUpdate.getId());
		
		final UID id = masterDataFacade.modify(
				MasterDataWrapper.wrapEventSupportTransitionVO(eseVOToUpdate), null);
		
		eseVOToUpdate = MasterDataWrapper.getEventSupportTransitionVO(
				masterDataFacade.get(E.SERVERCODETRANSITION.getUID(), id));
		
		esCache.insertEventSupportTransitionCache(eseVOToUpdate);
		
		stateCache.invalidateCache(true, true);
		
		return eseVOToUpdate;
	}

	@Override
	public void deleteEventSupportTransition(
			EventSupportTransitionVO eseVOToUpdate)
			throws CommonPermissionException, CommonValidationException,
			NuclosBusinessRuleException, CommonFinderException,
			CommonRemoveException, CommonStaleVersionException,
			NuclosCompileException {

		this.checkWriteAllowed(E.SERVERCODETRANSITION);

		eseVOToUpdate.validate();
		masterDataFacade.remove(E.SERVERCODETRANSITION.getUID(), eseVOToUpdate.getPrimaryKey(),
				false, null);
		
		esCache.removeEventSupportTransitionCache(eseVOToUpdate.getTransition(), eseVOToUpdate.getId());
		
		stateCache.invalidateCache(true, true);
	}

	@Override
	public List<EventSupportEventVO> getEventSupportEntities() throws CommonPermissionException {
		return esCache.getEventSupportEntities();
	}

	public EventSupportJobVO createEventSupportJob(
			EventSupportJobVO esjVOToInsert) throws CommonPermissionException,
			CommonValidationException, NuclosBusinessRuleException,
			CommonCreateException {
		this.checkWriteAllowed(E.SERVERCODEJOB);

		esjVOToInsert.validate();

		esjVOToInsert = MasterDataWrapper.getEventSupportJobVO(masterDataFacade.create(
				MasterDataWrapper.wrapEventSupportJobVO(esjVOToInsert), null));

		esCache.insertEventSupportJobCache(esjVOToInsert);
		
		return esjVOToInsert;
	}

	@Override
	public EventSupportJobVO modifyEventSupportJob(EventSupportJobVO esjVOToUpdate) 
			throws CommonPermissionException, CommonValidationException, NuclosBusinessRuleException, 
			CommonCreateException, CommonFinderException, CommonRemoveException, CommonStaleVersionException, NuclosCompileException {
		
		this.checkWriteAllowed(E.SERVERCODEJOB.getUID());
		
		esjVOToUpdate.validate();
		
		esCache.removeEventSupportJobCache(esjVOToUpdate.getJobControllerUID(), esjVOToUpdate.getId());
		
		UID objId = masterDataFacade.modify(
				MasterDataWrapper.wrapEventSupportJobVO(esjVOToUpdate),
				serverParameterProvider.getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
		
		esjVOToUpdate = MasterDataWrapper.getEventSupportJobVO(
				masterDataFacade.get(E.SERVERCODEJOB, objId));
		
		
		esCache.insertEventSupportJobCache(esjVOToUpdate);

		return esjVOToUpdate;
	}


	
	public void deleteEventSupportJob(EventSupportJobVO esjVOToDelete) 
			throws CommonPermissionException, CommonValidationException, NuclosBusinessRuleException, 
			CommonFinderException, CommonRemoveException, CommonStaleVersionException, NuclosCompileException {
		
		this.checkWriteAllowed(E.SERVERCODEJOB.getUID());
		esjVOToDelete.validate();
		masterDataFacade.remove(E.SERVERCODEJOB.getUID(), esjVOToDelete.getPrimaryKey(), false);
		
		esCache.removeEventSupportJobCache(esjVOToDelete.getJobControllerUID(),esjVOToDelete.getId());
		
	}

	@Override
	public List<EventSupportGenerationVO> getEventSupportGenerations() 
			throws CommonPermissionException {
		return this.esCache.getEventSupportGenerations();
	}

	@Override
	public List<EventSupportTransitionVO> getEventSupportTransitions() 
			throws CommonPermissionException {
		return this.esCache.getEventSupportTransitions();
	}
	
	private boolean isActiveEventSupport(String classname) {
		if (classname != null && classname.startsWith("org.nuclos.businessentity.rule")) {
			return true;
		}
		EventSupportSourceVO eventSupportSourceVO = esCache.getEventSupportSourceByClassname(classname);
		if (eventSupportSourceVO == null) {
			String msg = "No EventSupport(Rule) found for class=" + classname + "!";
			throw new NuclosFatalException(msg);
		}
		return eventSupportSourceVO.isActive();
	}

	@Override
	public EventSupportGenerationVO createEventSupportGeneration(
			EventSupportGenerationVO esgVOToInsert)
			throws CommonPermissionException, CommonValidationException,
			NuclosBusinessRuleException, CommonCreateException {
		this.checkWriteAllowed(E.SERVERCODEGENERATION);

		esgVOToInsert.validate();
		esgVOToInsert = MasterDataWrapper.getEventSupportGenerationVO(masterDataFacade.create(
				MasterDataWrapper.wrapEventSupportGenerationVO(esgVOToInsert), null));
		
		esCache.insertEventSupportGenerationCache(esgVOToInsert);
		
		return esgVOToInsert;

	}

	@Override
	public EventSupportGenerationVO modifyEventSupportGeneration(
			EventSupportGenerationVO esgVOToUpdate)
			throws CommonPermissionException, CommonValidationException,
			NuclosBusinessRuleException, CommonCreateException,
			CommonFinderException, CommonRemoveException,
			CommonStaleVersionException, NuclosCompileException {

		this.checkWriteAllowed(E.SERVERCODEGENERATION);

		esgVOToUpdate.validate();			
		
		esCache.removeEventSupportGenerationCache(esgVOToUpdate.getGeneration(), esgVOToUpdate.getId());
		
		UID modify = masterDataFacade.modify(
				MasterDataWrapper.wrapEventSupportGenerationVO(esgVOToUpdate), null);
		
		esgVOToUpdate = MasterDataWrapper.getEventSupportGenerationVO(
				masterDataFacade.get(E.SERVERCODEGENERATION.getUID(), modify));
		
		esCache.insertEventSupportGenerationCache(esgVOToUpdate);
	
		return esgVOToUpdate;
	}

	@Override
	public void deleteEventSupportGeneration(
			EventSupportGenerationVO esgVOToDelete)
			throws CommonPermissionException, CommonValidationException,
			NuclosBusinessRuleException, CommonFinderException,
			CommonRemoveException, CommonStaleVersionException,
			NuclosCompileException {

		this.checkWriteAllowed(E.SERVERCODEGENERATION);

		esgVOToDelete.validate();			
		masterDataFacade.remove(E.SERVERCODEGENERATION.getUID(), esgVOToDelete.getPrimaryKey(), false);
		
		esCache.removeEventSupportGenerationCache(esgVOToDelete.getGeneration(), esgVOToDelete.getId());

	}

	public void forceEventSupportCompilation() throws NuclosCompileException {
		this.ccm.getNuclosJavaCompilerComponent().forceCompile();
		this.ccm.getClassLoaderAndCompileIfNeeded();
		
//		if(NuclosClusterRegisterHelper.runInClusterMode()) {
//			NuclosClusterAction action = ClusterActionFactory.createClusterAction(NuclosClusterAction.RULE_ACTION);
//			NuclosJMSUtils.sendObjectMessage(action, JMSConstants.TOPICNAME_CLUSTER, null);	
//		}
	}

	public List<EventSupportSourceVO> findEventSupportsByUsageAndEntity(String sEventclass, UsageCriteria usagecriteria) {

		List<EventSupportEventVO> lstEventSupportEvents = new ArrayList<>(esCache.getEventSupportsByEntityUID(usagecriteria.getEntityUID(), true));

		//TreeMap<Integer, String> mpcollUsableRuleIds = new TreeMap<Integer, String>();
		//for (EventSupportEventVO e : lstEventSupportEvents) {
		final Iterator<EventSupportEventVO> itEvSupports = lstEventSupportEvents.iterator();
		while (itEvSupports.hasNext()) {
			final EventSupportEventVO e = itEvSupports.next();
			if (!LangUtils.equal(e.getEventSupportClassType(), sEventclass)) {
				itEvSupports.remove();
				continue;
			}
			if (e.getProcessUID() != null && !LangUtils.equal(e.getProcessUID(), usagecriteria.getProcessUID())) {
				itEvSupports.remove();
				continue;
			}
			if (e.getStateUID() != null && !LangUtils.equal(e.getStateUID(), usagecriteria.getStatusUID())) {
				itEvSupports.remove();
				continue;
			}

			// NUCLOS-7014 integration point rule order overwrites default rule with same order :(
			//mpcollUsableRuleIds.put(e.getOrder(), e.getEventSupportClass());
		}

		Collections.sort(lstEventSupportEvents, new Comparator<EventSupportEventVO>() {
			@Override
			public int compare(final EventSupportEventVO o1, final EventSupportEventVO o2) {
				final UID ip1 = o1.getIntegrationPointUID();
				final UID ip2 = o2.getIntegrationPointUID();
				if (ip1 != null && ip2 == null) {
					return 1;
				}
				if (ip1 == null && ip2 != null) {
					return -1;
				}
				final Integer order1 = RigidUtils.defaultIfNull(o1.getOrder(), 0);
				final Integer order2 = RigidUtils.defaultIfNull(o2.getOrder(), 0);
				return order1.compareTo(order2);
			}
		});

		List<EventSupportSourceVO> ruleVOs = new ArrayList<>();

		//for (String esClassName : mpcollUsableRuleIds.values()) {
		for (EventSupportEventVO e : lstEventSupportEvents) {
			EventSupportSourceVO essVO = esCache.getEventSupportSourceByClassname(e.getEventSupportClass());
			if (essVO != null) {
				ruleVOs.add(essVO);
			}
		}
		return ruleVOs;
	}

	private <T> EventSupportCustomResult<T> executeCustomSupportEvent(
			Map<String, Object> cache, String eventSupportClass,
			EntityObjectVO<T> eoVO) throws NuclosBusinessRuleException,
			NuclosCompileException {

		EntityObjectVO<T> retVal = eoVO;

		// Instantiate the corresponding BusinessObject file
		AbstractBusinessObject<T> newInstance = createBusinessObject(retVal, eventSupportClass);

			if (newInstance == null) {
				throw new NuclosBusinessRuleException(
						"BusinessObject cannot be found");
			}
			if (!(newInstance instanceof Modifiable)) {
				throw new NuclosBusinessRuleException(
						"BusinessObject is not of type ModifyableBusinessObject");
			}

			CustomEventObjectImpl ctx = new CustomEventObjectImpl(cache,
				retVal.getDalEntity(), (Modifiable) newInstance,
				eventSupportClass, this.dataLanguageCache.getLanguageToUse());

		try {
			final CustomRule loadedCustomSupport = newEventSupportInstance(eventSupportClass);
			loadedCustomSupport.custom(ctx);

		} catch (InputRequiredException e) {
			throw new NuclosInputRequiredException(e);
		} catch (org.nuclos.api.exception.PointerException e) {
			throw new org.nuclos.common.PointerException(e.getMessage());
		} catch (BusinessException e) {
			if (e.getCause() instanceof MailException) {
				e = new BusinessException(NuclosExceptions.getReasonableMessage(e.getCause()), e.getCause());
			}
			throw new NuclosBusinessRuleException(e);
		}

		retVal = EOBOBridge.getEO(newInstance);

		// check notifications
		if (ctx.getRuleNotifications() != null
				&& ctx.getRuleNotifications().size() > 0) {
			sendMessagesByJMS(ctx.getRuleNotifications());
		}

		EventSupportCustomResult<T> result = new EventSupportCustomResult<>();
		result.setEntityObjectVO(eoVO);
		result.setUpdateAfterExecution(ctx.getUpdateAfterExecution());
		
		return result;
	}

	private String getFullClassnameForEntity(UID pEntityUid) {
		final EntityMeta<Object> eMeta = metaProvider.getEntity(pEntityUid);
		final boolean bForInternalUseOnly = E.isNuclosEntity(eMeta.getUID()) && BusinessObjectBuilderForInternalUse.getEntityMetas().contains(eMeta);
		return getFullClassnameForEntity(pEntityUid, bForInternalUseOnly);
	}

	private String getFullClassnameForEntity(UID pEntityUid, boolean bForSystemRulesOnly) {
		final EntityMeta<Object> eMeta = metaProvider.getEntity(pEntityUid);
		String retVal = esCache.getNucletPackageForEntity(pEntityUid);
		String pEntity = NuclosBusinessObjectBuilder.getNameForFqn(NuclosBusinessObjectBuilder.getFormattedEntityName(eMeta));

		if (retVal == null) {
			retVal = E.isNuclosEntity(eMeta.getUID()) ?
					(bForSystemRulesOnly ? NuclosBusinessObjectBuilder.DEFFAULT_PACKAGE_NUCLOS_INTERNAL_ONLY : NuclosBusinessObjectBuilder.DEFFAULT_PACKAGE_NUCLOS_SYSTEM_FOR_API) :
					NuclosBusinessObjectBuilder.DEFFAULT_PACKAGE_NUCLET;
		}

		retVal += "." + pEntity;
		return retVal;
	}

	private static abstract class SourceBuilderRecursiveAction extends RecursiveAction {
		private final String name;
		private final ForkJoinPool builderThreadPool;
		private final List<InterruptedException> lstExceptions;
		private final List<String> lstErrors;

		public SourceBuilderRecursiveAction startNow(final List<RecursiveAction> lstActions) {
			lstActions.add(this);
			builderThreadPool.execute(this);
			return this;
		}

		protected SourceBuilderRecursiveAction(final String name, final ForkJoinPool builderThreadPool, final List<InterruptedException> lstExceptions, final List<String> lstErrors) {
			this.name = name;
			this.builderThreadPool = builderThreadPool;
			this.lstExceptions = lstExceptions;
			this.lstErrors = lstErrors;
		}

		protected abstract void createObjects() throws InterruptedException, CommonBusinessException;

		@Override
		protected final void compute() {
			try {
				createObjects();
			} catch (InterruptedException e) {
				lstExceptions.add(e);
			} catch (Exception e) {
				lstErrors.add(name);
				LOG.warn("Error creating " + name, e);
			}
		}
	};

	/**
	 * Remote call from Client only
	 */
	public void createBusinessObjects() {
		SpringApplicationContextHolder.getBean(NuclosJarGeneratorManager.class).triggerCancelingOfCurrentGeneratorAndStartNewGenerator("EventSupportFacadeBean.createBusinessObjects");
	}

	/**
	 * Deletes old sources/jars and create new sources + compiles them + creates jars.
	 */
	public void createBusinessObjects(final ForkJoinPool builderThreadPool)
		throws NuclosCompileException, InterruptedException {

		deleteExistingBusinessObjects();

		// write pom.xml for codegenerator
		writeMavenPOM();

		LOG.info("Starting Java Source Builder with " + builderThreadPool.getParallelism() + " Threads...");
		sourceScanner.cancel();

		final Date startCreation = new Date();

		try {
			final List<String> lstErrors = new CopyOnWriteArrayList<>();
			final List<InterruptedException> lstExceptions = new CopyOnWriteArrayList<>();
			final List<RecursiveAction> lstActions = new ArrayList<>();

			final SourceBuilderRecursiveAction boAction = new SourceBuilderRecursiveAction("BusinessObjects", builderThreadPool, lstExceptions, lstErrors) {
				@Override
				protected void createObjects() throws InterruptedException, CommonBusinessException {
					nuclosBusinessObjectBuilder.createObjects(builderThreadPool);
				}
			}.startNow(lstActions);

			new SourceBuilderRecursiveAction("Reports", builderThreadPool, lstExceptions, lstErrors) {
				@Override
				protected void createObjects() throws InterruptedException, CommonBusinessException {
					repObBuilder.createObjects(builderThreadPool);
				}
			}.startNow(lstActions);

			new SourceBuilderRecursiveAction("Printouts", builderThreadPool, lstExceptions, lstErrors) {
				@Override
				protected void createObjects() throws InterruptedException, CommonBusinessException {
					printObBuilder.createObjects(builderThreadPool);
				}
			}.startNow(lstActions);

			new SourceBuilderRecursiveAction("ImportStructureDefinitions", builderThreadPool, lstExceptions, lstErrors) {
				@Override
				protected void createObjects() throws InterruptedException, CommonBusinessException {
					isdBuilder.createObjects(builderThreadPool);
				}
			}.startNow(lstActions);

			new SourceBuilderRecursiveAction("Statemodels", builderThreadPool, lstExceptions, lstErrors) {
				@Override
				protected void createObjects() throws InterruptedException, CommonBusinessException {
					stateObBuilder.createObjects(builderThreadPool);
				}
			}.startNow(lstActions);

			new SourceBuilderRecursiveAction("Datasources", builderThreadPool, lstExceptions, lstErrors) {
				@Override
				protected void createObjects() throws InterruptedException, CommonBusinessException {
					datasourceObBuilder.createObjects(builderThreadPool);
				}
			}.startNow(lstActions);

			new SourceBuilderRecursiveAction("Generations", builderThreadPool, lstExceptions, lstErrors) {
				@Override
				protected void createObjects() throws InterruptedException, CommonBusinessException {
					boAction.join();
					genObBuilder.createObjects(builderThreadPool);
				}
			}.startNow(lstActions);

			new SourceBuilderRecursiveAction("Parameters", builderThreadPool, lstExceptions, lstErrors) {
				@Override
				protected void createObjects() throws InterruptedException, CommonBusinessException {
					paramObBuilder.createObjects(builderThreadPool);
				}
			}.startNow(lstActions);

			new SourceBuilderRecursiveAction("Communications", builderThreadPool, lstExceptions, lstErrors) {
				@Override
				protected void createObjects() throws InterruptedException, CommonBusinessException {
					communicationObBuilder.createObjects(builderThreadPool);
				}
			}.startNow(lstActions);

			new SourceBuilderRecursiveAction("WebServiceObjects", builderThreadPool, lstExceptions, lstErrors) {
				@Override
				protected void createObjects() throws InterruptedException, CommonBusinessException {
					wsObBuilder.createObjects(builderThreadPool);
				}
			}.startNow(lstActions);

			new SourceBuilderRecursiveAction("UserGroups", builderThreadPool, lstExceptions, lstErrors) {
				@Override
				protected void createObjects() throws InterruptedException, CommonBusinessException {
					usergroupObBuilder.createObjects(builderThreadPool);
				}
			}.startNow(lstActions);

			for (RecursiveAction act : lstActions) {
				act.join();
			}

			if (!lstExceptions.isEmpty()) {
				throw lstExceptions.get(0);
			}

			LOG.info("Build rule.jar ...");
			compiler.forceCompile();
			ccm.getClassLoaderAndCompileIfNeeded();

			if (Thread.currentThread().isInterrupted()) {
				LOG.debug("Interrupting Nuclos JAR generator thread ...");
				throw new InterruptedException();
			}

			if (lstErrors.isEmpty()) {
				LOG.info("Finished building all BusinessObjects and Rules in {} seconds.",
				         ((new Date()).getTime() - startCreation.getTime()) / 1000);
			} else {
				LOG.info("Finished building all BusinessObjects and Rules with errors in {} seconds.",
						(((new Date()).getTime() - startCreation.getTime()) / 1000));
				throw new NuclosCompileException("Error building BusinessObjects for " + lstErrors);
			}
		} finally {
			// compilable or not we have to react on the result of this compilation task
			NuclosJMSUtils.sendMessage(EventSupportNotification.COMPILED, JMSConstants.TOPICNAME_EVENTSUPPORTNOTIFICATION);
			NuclosJMSUtils.sendMessage(EventSupportNotification.COMPILED, JMSConstants.TOPICNAME_RULECOMPILATION);
		}
	}

	private void writeMavenPOM() {
		if (!NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_DEVELOPMENT)) {
			return;
		}
		LOG.info("Write pom.xml ...");
		DocumentBuilderFactory icFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder icBuilder;
		try {
			Query<NucletExtension> qNucletExtensionsWithProjectFlag = QueryProvider.create(NucletExtension.class);
			qNucletExtensionsWithProjectFlag.where(NucletExtension.DependencyAddAsProjectToPom.eq(Boolean.TRUE));
			qNucletExtensionsWithProjectFlag.orderBy(NucletExtension.DependencyGroupId, true).and(NucletExtension.DependencyArtifactId, true);
			List<NucletExtension> nucletExtensionsWithProjektFlag = QueryProvider.execute(qNucletExtensionsWithProjectFlag);
			Set<String> nucletExtensionsWithProjectFlagJarNames = nucletExtensionsWithProjektFlag.stream().map(nuEx ->
					String.format("%s-%s.jar", nuEx.getDependencyArtifactId(), nuEx.getDependencyVersion())).collect(Collectors.toSet());

			File pom = NuclosCodegeneratorConstants.GENERATOR_FOLDER.toPath().resolve("pom.xml").toFile();
			icBuilder = icFactory.newDocumentBuilder();
			Document doc = icBuilder.newDocument();
			Element project = doc.createElementNS("http://maven.apache.org/POM/4.0.0", "project");
			project.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			project.setAttribute("xsi:schemaLocation", "http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd");
			doc.appendChild(project);

			// header
			project.appendChild(getTextElement(doc, "modelVersion", "4.0.0"));
			project.appendChild(getTextElement(doc, "groupId", "org.nuclos"));
			project.appendChild(getTextElement(doc, "artifactId", "codegenerator"));
			project.appendChild(getTextElement(doc, "version", "1.0-SNAPSHOT"));

			// dependencies
			Element dependencies = doc.createElement("dependencies");
			project.appendChild(dependencies);
			nucletExtensionsWithProjektFlag.stream().forEach(nuEx -> {
				addDependency(doc, dependencies,
						nuEx.getDependencyGroupId(),
						nuEx.getDependencyArtifactId(),
						nuEx.getDependencyVersion(),
						null,
						"compile",
						null);
			});
			List<File> lstJars = new ArrayList<>(generatorClasspathComponent.getExpandedSystemParameterClassPath(true));
			Path nucletExtensionsServerHomePath = NucletExtensionLoader.getNucletExtensionsServerHomePath();
			if (nucletExtensionsServerHomePath != null && Files.exists(nucletExtensionsServerHomePath)) {
				for (File serverExtensionJar : nucletExtensionsServerHomePath.toFile().listFiles((FilenameFilter) new SuffixFileFilter(".jar"))) {
					lstJars.add(serverExtensionJar);
				}
			}
			File[] sortedJars = lstJars.toArray(new File[]{});
			Arrays.sort(sortedJars, new Comparator<File>() {
				public int compare(File f1, File f2) {
					try {
						return f1.getCanonicalPath().toLowerCase().compareTo(f2.getCanonicalPath().toLowerCase());
					} catch (IOException e) {
						return f1.getName().toLowerCase().compareTo(f2.getName().toLowerCase());
					}
				}});
			Set<String> distinctCheck = new HashSet<>();
			for (File jar : sortedJars) {
				if (nucletExtensionsWithProjectFlagJarNames.contains(jar.getName())) {
					continue;
				}
				addDependency(doc, dependencies, jar, distinctCheck);
			}

			// properties
			Element properties = doc.createElement("properties");
			properties.appendChild(getTextElement(doc, "project.build.sourceEncoding", NuclosCodegeneratorConstants.JAVA_SRC_ENCODING));
			project.appendChild(properties);

			// build
			Element build = doc.createElement("build");
			Element plugins = doc.createElement("plugins");
			Element plugin1 = doc.createElement("plugin");
			Element configuration1 = doc.createElement("configuration");
			project.appendChild(build);
			build.appendChild(plugins);
			plugins.appendChild(plugin1);
			plugin1.appendChild(getTextElement(doc, "groupId", "org.apache.maven.plugins"));
			plugin1.appendChild(getTextElement(doc, "artifactId", "maven-compiler-plugin"));
			plugin1.appendChild(configuration1);
			configuration1.appendChild(getTextElement(doc, "source", "1.8"));
			configuration1.appendChild(getTextElement(doc, "target", "1.8"));
			configuration1.appendChild(getTextElement(doc, "encoding", NuclosCodegeneratorConstants.JAVA_SRC_ENCODING));
			Element plugin2 = doc.createElement("plugin");
			Element executions = doc.createElement("executions");
			Element execution = doc.createElement("execution");
			Element goals = doc.createElement("goals");
			Element configuration2 = doc.createElement("configuration");
			Element sources = doc.createElement("sources");
			plugins.appendChild(plugin2);
			plugin2.appendChild(getTextElement(doc, "groupId", "org.codehaus.mojo"));
			plugin2.appendChild(getTextElement(doc, "artifactId", "build-helper-maven-plugin"));
			plugin2.appendChild(getTextElement(doc, "version", "1.12"));
			plugin2.appendChild(executions);
			executions.appendChild(execution);
			execution.appendChild(getTextElement(doc, "id", "add-source"));
			execution.appendChild(getTextElement(doc, "phase", "generate-sources"));
			execution.appendChild(goals);
			execution.appendChild(configuration2);
			goals.appendChild(getTextElement(doc, "goal", "add-source"));
			configuration2.appendChild(sources);
			sources.appendChild(getTextElement(doc, "source", NuclosCodegeneratorConstants.JAR_SRC_FOLDER.getCanonicalPath()));
			sources.appendChild(getTextElement(doc, "source", NuclosCodegeneratorConstants.BO_SRC_FOLDER.getCanonicalPath()));
			sources.appendChild(getTextElement(doc, "source", NuclosCodegeneratorConstants.STATEMODEL_SRC_FOLDER.getCanonicalPath()));
			sources.appendChild(getTextElement(doc, "source", NuclosCodegeneratorConstants.GENERATION_SRC_FOLDER.getCanonicalPath()));
			sources.appendChild(getTextElement(doc, "source", NuclosCodegeneratorConstants.PRINTOUT_SRC_FOLDER.getCanonicalPath()));
			sources.appendChild(getTextElement(doc, "source", NuclosCodegeneratorConstants.REPORT_SRC_FOLDER.getCanonicalPath()));
			sources.appendChild(getTextElement(doc, "source", NuclosCodegeneratorConstants.DATASOURCEREPORT_SRC_FOLDER.getCanonicalPath()));
			sources.appendChild(getTextElement(doc, "source", NuclosCodegeneratorConstants.PARAMETER_SRC_FOLDER.getCanonicalPath()));
			sources.appendChild(getTextElement(doc, "source", NuclosCodegeneratorConstants.IMPORTSTRUCTUREDEFS_SRC_FOLDER.getCanonicalPath()));
			sources.appendChild(getTextElement(doc, "source", NuclosCodegeneratorConstants.USERROLE_SRC_FOLDER.getCanonicalPath()));
			sources.appendChild(getTextElement(doc, "source", NuclosCodegeneratorConstants.COMMUNICATION_SRC_FOLDER.getCanonicalPath()));
			sources.appendChild(getTextElement(doc, "source", NuclosCodegeneratorConstants.WEBSERVICE_SRC_FOLDER.getCanonicalPath()));

			// write pom.xml
			javax.xml.transform.Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputPropertiesFactory.S_KEY_INDENT_AMOUNT, "4");
			DOMSource source = new DOMSource(doc);
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(pom), NuclosCodegeneratorConstants.JAVA_SRC_ENCODING));
			StreamResult pomResult = new StreamResult(out);
			transformer.transform(source, pomResult);

		} catch (Exception e) {
			LOG.error("pom.xml for codegenerator could not be created: {}", e.getMessage(), e);
		}
	}

	private static void addDependency(Document doc, Element dependencies, File jarFile, final Set<String> distinctCheck) throws IOException {
		if (jarFile.exists() && !distinctCheck.contains(jarFile.getName())) {
			addDependency(doc, dependencies,
					"org.nuclos",
					jarFile.getName().replace(".jar", "").replaceAll("[^a-zA-Z0-9-]+", ""),
					null,
					"jar",
					"system",
					jarFile.getCanonicalPath());
			distinctCheck.add(jarFile.getName());
		}
	}

	private static void addDependency(Document doc, Element dependencies, String groupId, String artifactId, String version, String type, String scope, String systemPath) {
		Element dependency = doc.createElement("dependency");
		dependency.appendChild(getTextElement(doc, "groupId", groupId));
		dependency.appendChild(getTextElement(doc, "artifactId", artifactId));
		dependency.appendChild(getTextElement(doc, "version", RigidUtils.defaultIfNull(version, "0.0")));
		if (type != null) {
			dependency.appendChild(getTextElement(doc, "type", type));
		}
		dependency.appendChild(getTextElement(doc, "scope", scope));
		if (systemPath != null) {
			dependency.appendChild(getTextElement(doc, "systemPath", systemPath));
		}
		dependencies.appendChild(dependency);
	}

	// utility method to create text node
	private static Node getTextElement(Document doc, String name, String value) {
		Element node = doc.createElement(name);
		node.appendChild(doc.createTextNode(value));
		return node;
	}

	private void deleteExistingBusinessObjects()
		throws NuclosCompileException, InterruptedException {

		if (NuclosCodegeneratorConstants.BOJARFILE.exists()) {
			NuclosCodegeneratorConstants.BOJARFILE.delete();
		}
		AbstractNuclosObjectCompiler.removeDirectory(NuclosCodegeneratorConstants.BO_SRC_FOLDER);
		if (Thread.currentThread().isInterrupted()) {
			throw new InterruptedException();
		}

		if (NuclosCodegeneratorConstants.STATEMODELJARFILE.exists()) {
			NuclosCodegeneratorConstants.STATEMODELJARFILE.delete();
		}
		AbstractNuclosObjectCompiler.removeDirectory(NuclosCodegeneratorConstants.STATEMODEL_SRC_FOLDER);
		if (Thread.currentThread().isInterrupted()) {
			throw new InterruptedException();
		}
		
		if (NuclosCodegeneratorConstants.IMPORTSTRUCTUREDEFSJARFILE.exists()) {
			NuclosCodegeneratorConstants.IMPORTSTRUCTUREDEFSJARFILE.delete();
		}
		AbstractNuclosObjectCompiler.removeDirectory(NuclosCodegeneratorConstants.IMPORTSTRUCTUREDEFS_SRC_FOLDER);
		if (Thread.currentThread().isInterrupted()) {
			throw new InterruptedException();
		}

		if (NuclosCodegeneratorConstants.DATASOURCEREPORTJARFILE.exists()) {
			NuclosCodegeneratorConstants.DATASOURCEREPORTJARFILE.delete();
		}
		AbstractNuclosObjectCompiler.removeDirectory(NuclosCodegeneratorConstants.DATASOURCEREPORT_SRC_FOLDER);
		if (Thread.currentThread().isInterrupted()) {
			throw new InterruptedException();
		}
		
		if (NuclosCodegeneratorConstants.PARAMETERJARFILE.exists()) {
			NuclosCodegeneratorConstants.PARAMETERJARFILE.delete();
			AbstractNuclosObjectCompiler.removeDirectory(NuclosCodegeneratorConstants.PARAMETER_SRC_FOLDER);
		}
		if (Thread.currentThread().isInterrupted()) {
			throw new InterruptedException();
		}
		
		if (NuclosCodegeneratorConstants.COMMUNICATIONJARFILE.exists()) {
			NuclosCodegeneratorConstants.COMMUNICATIONJARFILE.delete();
		}
		AbstractNuclosObjectCompiler.removeDirectory(NuclosCodegeneratorConstants.COMMUNICATION_SRC_FOLDER);
		if (Thread.currentThread().isInterrupted()) {
			throw new InterruptedException();
		}

		if (NuclosCodegeneratorConstants.JARFILE.exists()) {
			NuclosCodegeneratorConstants.JARFILE.delete();
		}
		if (Thread.currentThread().isInterrupted()) {
			throw new InterruptedException();
		}

		if (NuclosCodegeneratorConstants.REPORTJARFILE.exists()) {
			NuclosCodegeneratorConstants.REPORTJARFILE.delete();
		}
		AbstractNuclosObjectCompiler.removeDirectory(NuclosCodegeneratorConstants.REPORT_SRC_FOLDER);
		if (Thread.currentThread().isInterrupted()) {
			throw new InterruptedException();
		}
		
		if (NuclosCodegeneratorConstants.WEBSERVICEJARFILE.exists()) {
			NuclosCodegeneratorConstants.WEBSERVICEJARFILE.delete();
		}
		AbstractNuclosObjectCompiler.removeDirectory(NuclosCodegeneratorConstants.WEBSERVICE_SRC_FOLDER);
		if (Thread.currentThread().isInterrupted()) {
			throw new InterruptedException();
		}
		
		if (NuclosCodegeneratorConstants.GENERATIONJARFILE.exists()) {
			NuclosCodegeneratorConstants.GENERATIONJARFILE.delete();
		}
		AbstractNuclosObjectCompiler.removeDirectory(NuclosCodegeneratorConstants.GENERATION_SRC_FOLDER);
		if (Thread.currentThread().isInterrupted()) {
			throw new InterruptedException();
		}
		
		if (NuclosCodegeneratorConstants.PRINTOUTJARFILE.exists()) {
			NuclosCodegeneratorConstants.PRINTOUTJARFILE.delete();
		}
		AbstractNuclosObjectCompiler.removeDirectory(NuclosCodegeneratorConstants.PRINTOUT_SRC_FOLDER);
		if (Thread.currentThread().isInterrupted()) {
			throw new InterruptedException();
		}
		
		if (NuclosCodegeneratorConstants.USERROLEJARFILE.exists()) {
			NuclosCodegeneratorConstants.USERROLEJARFILE.delete();
		}
		AbstractNuclosObjectCompiler.removeDirectory(NuclosCodegeneratorConstants.USERROLE_SRC_FOLDER);
		if (Thread.currentThread().isInterrupted()) {
			throw new InterruptedException();
		}
		
	}

	private <T> EntityObjectVO<T> executeInsertSupportEvent(
			Map<String, Object> cache, String eventSupportClass,
			EntityObjectVO<T> eoVO, boolean isFinal)
			throws NuclosBusinessRuleException, NuclosCompileException {

		eoVO.flagNew();

		// Instantiate the corresponding BusinessObject file
		AbstractBusinessObject<T> newInstance = createBusinessObject(eoVO, eventSupportClass);

		if (newInstance == null) {
			throw new NuclosBusinessRuleException(
					"BusinessObject cannot be found");
		}
		if (!(newInstance instanceof Modifiable)) {
			throw new NuclosBusinessRuleException(
					"BusinessObject is not of type ModifyableBusinessObject");
		}

		InsertEventObjectImpl ctx = new InsertEventObjectImpl(cache,
					eoVO.getDalEntity(), (Modifiable) newInstance,
				eventSupportClass, this.dataLanguageCache.getLanguageToUse());

		try {
			if (isFinal) {
				InsertFinalRule loadedInsertFinalSupport = newEventSupportInstance(eventSupportClass);
				loadedInsertFinalSupport.insertFinal(ctx);
			} else {
				InsertRule loadedInsertSupport = newEventSupportInstance(eventSupportClass);
				loadedInsertSupport.insert(ctx);
			}

			eoVO = EOBOBridge.getEO(newInstance);

		} catch (InputRequiredException e) {
			throw new NuclosInputRequiredException(e);
		} catch (org.nuclos.api.exception.PointerException e) {
			throw new org.nuclos.common.PointerException(e.getMessage());
		} catch (BusinessException e) {
			if (e.getCause() != null && e.getCause() instanceof NuclosInputRequiredException) {
				throw (NuclosInputRequiredException) e.getCause();
			}
			throw new NuclosBusinessRuleException(e);
		}

		// check notifications
		if (ctx.getRuleNotifications() != null
				&& ctx.getRuleNotifications().size() > 0) {
			sendMessagesByJMS(ctx.getRuleNotifications());
		}

		return eoVO;
	}


	private <T> EntityObjectVO<T> executeUpdateSupportEvent(
			Map<String, Object> cache, String eventSupportClass,
			EntityObjectVO<T> eoVO, boolean isFinal, boolean isCollectiveProcessing)
			throws NuclosBusinessRuleException, NuclosCompileException {

		eoVO.flagUpdate();

		// Instantiate the corresponding BusinessObject file
		AbstractBusinessObject<T> newInstance = createBusinessObject(eoVO, eventSupportClass);

		if (newInstance == null) {
			throw new NuclosBusinessRuleException(
					"BusinessObject cannot be found");
		}
		if (!(newInstance instanceof Modifiable)) {
			throw new NuclosBusinessRuleException(
					"BusinessObject is not of type ModifyableBusinessObject");
		}

		UpdateEventObjectImpl ctx = new UpdateEventObjectImpl(cache,
					eoVO.getDalEntity(), (Modifiable) newInstance, isCollectiveProcessing,
					eventSupportClass, this.dataLanguageCache.getLanguageToUse());

		try {
			if (isFinal) {
				UpdateFinalRule loadedUpdateFinalSupport = newEventSupportInstance(eventSupportClass);
				loadedUpdateFinalSupport.updateFinal(ctx);
			} else {
				UpdateRule loadedUpdateSupport = newEventSupportInstance(eventSupportClass);
				loadedUpdateSupport.update(ctx);
			}
						
			eoVO = EOBOBridge.getEO(newInstance);

		} catch (InputRequiredException e) {
			throw new NuclosInputRequiredException(e);
		} catch (org.nuclos.api.exception.PointerException e) {
			throw new org.nuclos.common.PointerException(e.getMessage());
		} catch (BusinessException e) {
			if (e.getCause() != null && e.getCause() instanceof NuclosInputRequiredException) {
				throw (NuclosInputRequiredException) e.getCause();
			}
			throw new NuclosBusinessRuleException(e);
		}

		if (ctx.getRuleNotifications() != null
				&& ctx.getRuleNotifications().size() > 0) {
			sendMessagesByJMS(ctx.getRuleNotifications());
		}

		return eoVO;
	}

	private void executePrintFinalSupportEvent(
			final Map<String, Object> cache,
			final String eventSupportClass, 
			final EntityObjectVO<Long> eoVO,
			boolean isFinal, final UsageCriteria usagecriteria,
			final Collection<? extends PrintResult> printResults) 
					throws NuclosBusinessRuleException, NuclosCompileException {

		// Instantiate the corresponding BusinessObject file for rule execution
		final AbstractBusinessObject<?> newInstance = createBusinessObject(eoVO, eventSupportClass);

		if (newInstance == null) {
			throw new NuclosBusinessRuleException(
					"BusinessObject cannot be found");
		}
		if (!(newInstance instanceof Modifiable)) {
			throw new NuclosBusinessRuleException(
					"BusinessObject is not of type ModifyableBusinessObject");
		}

		if (isFinal) {
				// PrintFinal rule
				EventSupportSourceVO eseVO = esCache.getEventSupportSourceByClassname(eventSupportClass);
			final PrintFinalEventObjectImpl printFinalContext = new PrintFinalEventObjectImpl(
						cache, eoVO.getDalEntity(), (Modifiable) newInstance,
						eventSupportClass, this.dataLanguageCache.getLanguageToUse());
			try {

				// instantiate print rule
				final PrintFinalRule printFinalSupport = newEventSupportInstance(eventSupportClass);

				printFinalContext.getPrintResults().addAll(printResults);

				// execute business rule
				printFinalSupport.printFinal(printFinalContext);

			} catch (InputRequiredException e) {
				throw new NuclosInputRequiredException(e);
			} catch (org.nuclos.api.exception.PointerException e) {
				throw new org.nuclos.common.PointerException(e.getMessage());
			} catch (BusinessException e) {
				throw new NuclosBusinessRuleException(e);
			}

			if (printFinalContext.getRuleNotifications() != null
					&& printFinalContext.getRuleNotifications().size() > 0) {
				sendMessagesByJMS(printFinalContext.getRuleNotifications());
			}
		}
	}
	
	public List<PrintoutTO> findPrintoutTransportObjectsByUsageCriteriaOnly(UsageCriteria usagecriteria) throws NuclosBusinessRuleException {
		List<PrintoutTO> result = transformIntoTransportObjects(getPrintoutListNoRuleExecution(usagecriteria, null));
		return result;
	}
	
	private List<PrintoutTO> transformIntoTransportObjects(PrintoutList printoutlist) {
		final List<PrintoutTO> result = new ArrayList<>();
		
		// Load VOs
		for (final Printout printout : printoutlist) {
			// query ReportVO
			final EntityObjectVO<UID> reportEO = nucletDalProvider.getEntityObjectProcessor(E.REPORT).getByPrimaryKey((UID) printout.getId());
			final ReportVO reportVO = MasterDataWrapper.getReportVO(
				new MasterDataVO<>(reportEO, true),this.getCurrentUserName(),null);
			
			final List<OutputFormat> outputFormats = printout.getOutputFormats();
			if (outputFormats.isEmpty() && reportVO.getOutputType() == ReportVO.OutputType.SINGLE) {
				LOG.debug("remove printout {} due to empty OutputFormat list", printout.getId() );
				continue;
			}
			
			final List<UID> outputFormatIds = new ArrayList<>();
			for (final OutputFormat of : outputFormats) {
				outputFormatIds.add((UID)of.getId());
			}
			
			// query OutputFormat VO
			final List<EntityObjectVO<UID>> reportOutputs = 
					nucletDalProvider.getEntityObjectProcessor(E.REPORTOUTPUT).getByPrimaryKeys(outputFormatIds);
	
			final Map<UID, DefaultReportOutputVO> mapDefaultOutputFormats = new HashMap<>();
			for (final EntityObjectVO<UID> eoOutputVO : reportOutputs) {
				final DefaultReportOutputVO reportOutputVO = MasterDataWrapper.getReportOutputVO(
					new MasterDataVO<>(eoOutputVO, true));
				mapDefaultOutputFormats.put(eoOutputVO.getPrimaryKey(), reportOutputVO);
			}
			
			final PrintoutTO newPrintout = new PrintoutTO(printout.getId(), reportVO.getName(), reportVO.getOutputType(), reportVO.getDatasourceId());
			newPrintout.getOutputFormats().clear();
			for (OutputFormat outputFormat : outputFormats) {
				DefaultReportOutputVO defaultOutputFormat = mapDefaultOutputFormats.get(outputFormat.getId());
				OutputFormatTO wrapTO = printoutWrapper.wrapTO(defaultOutputFormat);
				wrapTO.setProperties(printoutWrapper.wrapTO(outputFormat.getProperties()));
				if (reportVO.getAttachDocument()) {
					// Collective report output type
					wrapTO.setAttachDocument(true);
				}
				newPrintout.getOutputFormats().add(wrapTO);
			}
	
			result.add(newPrintout);
		}
		
		return result;
	}

	private PrintoutList getPrintoutListNoRuleExecution(UsageCriteria usagecriteria, Long pk) throws NuclosBusinessRuleException {
		final PrintoutList printouts = new NuclosPrintoutList();
		
		ReportFacadeLocal reportFacadeLocal = SpringApplicationContextHolder.getBean(ReportFacadeLocal.class);
		// select configured reports using usagecriteria
		for (final DefaultReportVO reportVO : reportFacadeLocal.findReportsByUsage(usagecriteria)) {
			final String classname= printBoResolver.getQualifiedName(reportVO);
			Printout printoutInstance;
			try {
				printoutInstance = (Printout)createInstance(classname);
			} catch (final Exception e) {
				//throw new NuclosBusinessRuleException("could not instantiate " + classname +":" + e.getMessage());
				// NUCLOS-5716
				printoutInstance = new NoRuleClassPrintout(reportVO.getId());
			}
			LOG.debug("attached {} to print context", classname);
			if (pk != null) {
				printoutInstance.setBusinessObjectId(pk);
			}
			printouts.add(printoutInstance);
		}
		
		return printouts;
	}
	
	public boolean existPrinoutEventSupportsForUsageCriteria(UsageCriteria usagecriteria) {
		for (EventSupportSourceVO eseVO : findEventSupportsByUsageAndEntity(PrintRule.class.getCanonicalName(), usagecriteria)) {
			if (isActiveEventSupport(eseVO.getClassname())) {
				return true;
			}
		}
		return false;
	}
	
	private List<PrintoutTO> executePrintSupportEvent(
			final Map<String, Object> cache, 
			final EntityObjectVO<Long> eoVO, boolean isFinal, 
			final UsageCriteria usagecriteria,
			final List<EventSupportSourceVO> rules)
					throws NuclosBusinessRuleException, NuclosCompileException {

		final PrintoutList printouts;

		// Instantiate the corresponding BusinessObject file for rule execution
		final AbstractBusinessObject<?> newInstance = createBusinessObject(eoVO);

		if (newInstance == null) {
			throw new NuclosBusinessRuleException(
					"BusinessObject cannot be found");
		}
		if (!(newInstance instanceof Modifiable)) {
			throw new NuclosBusinessRuleException(
					"BusinessObject is not of type ModifyableBusinessObject");
		}


		RuleContext ctx = null;
		try {
			// PrintFinal rule
			if (isFinal) {
				throw new NotImplementedException("printFinal not implemented yet");
			} else {
				// Print rule
				printouts = getPrintoutListNoRuleExecution(usagecriteria, eoVO.getPrimaryKey());

				PrintContext printContext = null;
				for (final EventSupportSourceVO ruleVO : rules ) {
					final EventSupportSourceVO eseVO = esCache.getEventSupportSourceByClassname(ruleVO.getClassname());
					if (!isActiveEventSupport(eseVO.getClassname())) {
						continue;
					}
					final String eventSupportClass = eseVO.getClassname();
					ctx = new PrintEventObjectImpl(cache,
							eoVO.getDalEntity(), (Modifiable) newInstance,
							eventSupportClass, this.dataLanguageCache.getLanguageToUse());
					// instantiate print rule
					final PrintRule printSupport = newEventSupportInstance(eventSupportClass);

					printContext= (PrintContext)ctx;
					printContext.getPrintoutList().clear();
					printContext.getPrintoutList().addAll(printouts);

					// execute business rule
					printSupport.print(printContext);

					// invalidate effective printout list 
					printouts.clear();
					// publish modified list
					printouts.addAll(printContext.getPrintoutList());
				}
			}

			final List<PrintoutTO> result = transformIntoTransportObjects(printouts);
			return result;
			
		} catch (InputRequiredException e) {
			throw new NuclosInputRequiredException(e);
		} catch (org.nuclos.api.exception.PointerException e) {
			throw new org.nuclos.common.PointerException(e.getMessage());
		} catch (BusinessException e) {
			throw new NuclosBusinessRuleException(e);
		} finally { 
			if (null != ctx && (ctx instanceof NotifiableEventObject)) {
				final NotifiableEventObject eventObject = ((NotifiableEventObject)ctx);
				if (eventObject.getRuleNotifications() != null
						&& eventObject.getRuleNotifications().size() > 0) {
					sendMessagesByJMS(eventObject.getRuleNotifications());
				}
			}
		}
	}

	private <T> T newEventSupportInstance(String rule) throws NuclosRuleCompileException {

		boolean bForSystemRulesOnly = rule.startsWith(INTERNAL_SYSTEM_RULE_PACKAGE);

		final T result;

		try {
			if (bForSystemRulesOnly) {
				result = (T) Class.forName(rule).newInstance();
			} else {
				result = (T) this.ccm.getClassLoaderAndCompileIfNeeded().loadClass(rule).newInstance();
			}
		} catch (NuclosCompileException | InstantiationException | IllegalAccessException |
				ClassNotFoundException e1) {
			throw new NuclosRuleCompileException(e1);
		}

		return result;
	}

	private Class getBusinessObjectClass(String fullClassname, boolean bForSystemRulesOnly) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, NuclosCompileException {
		Class result;
		if (bForSystemRulesOnly) {
			result = Class.forName(fullClassname);
		} else {
			result = getClass(fullClassname);
		}
		return result;
	}

	private <T> AbstractBusinessObject<T> createBusinessObject(EntityObjectVO<T> eo) throws NuclosCompileException {
		return createBusinessObject(eo, false);
	}

	private <T> AbstractBusinessObject<T> createBusinessObject(EntityObjectVO<T> eo, String rule) throws NuclosCompileException {
		boolean bForSystemRulesOnly = rule.startsWith(INTERNAL_SYSTEM_RULE_PACKAGE);
		return createBusinessObject(eo, bForSystemRulesOnly);
	}

	private <T> AbstractBusinessObject<T> createBusinessObject(EntityObjectVO<T> eo, boolean bForSystemRulesOnly) throws NuclosCompileException {

		// Extract full classname for given entity
		String fullClassname = getFullClassnameForEntity(eo.getDalEntity(), bForSystemRulesOnly);

		if (fullClassname == null) {
			throw new NuclosCompileException("Entity unknown");
		}

		AbstractBusinessObject<T> retVal;
		// Try to instantiate the businessObject
		try {
			retVal = (AbstractBusinessObject<T>) getBusinessObjectClass(fullClassname, bForSystemRulesOnly).newInstance();
			EOBOBridge.setEO(retVal, eo);
			
		} catch (Exception e) {
			LOG.error(e.toString(), e);
			throw new NuclosCompileException(e);
		}
		return retVal;
	}

	private Class getClass(String fullClassname) throws NuclosCompileException, ClassNotFoundException, NoSuchMethodException {
		return ccm
				.getClassLoaderAndCompileIfNeeded()
				.loadClass(fullClassname);
	}
	
	private Object createInstance(String fullClassname)
		throws NuclosCompileException, InstantiationException, IllegalAccessException,
		       IllegalArgumentException, InvocationTargetException, NoSuchMethodException,
		       SecurityException, ClassNotFoundException {
		// Try to instantiate the class
		return getClass(fullClassname).getDeclaredConstructor().newInstance();
	}

	private <T> void executeDeleteSupportEvent(Map<String, Object> cache,
			String eventSupportClass, EntityObjectVO<T> eoVO, boolean isFinal,
			boolean isLogical) throws NuclosBusinessRuleException,
			NuclosCompileException {

		// Instantiate the corresponding BusinessObject file
		AbstractBusinessObject<T> newInstance = createBusinessObject(eoVO, eventSupportClass);

		if (newInstance == null) {
			throw new NuclosBusinessRuleException(
					"BusinessObject cannot be found");
		}
		if (!(newInstance instanceof Modifiable)) {
			throw new NuclosBusinessRuleException(
					"BusinessObject is not of type ModifyableBusinessObject");
		}

		DeleteEventObjectImpl ctx = new DeleteEventObjectImpl(cache,
					eoVO.getDalEntity(), (Modifiable) newInstance, isLogical,
					eventSupportClass, this.dataLanguageCache.getLanguageToUse());
		try {
			if (isFinal) {
				DeleteFinalRule loadedDeleteFinalSupport = newEventSupportInstance(eventSupportClass);
				loadedDeleteFinalSupport.deleteFinal(ctx);
			} else {
				DeleteRule loadedDeleteSupport = newEventSupportInstance(eventSupportClass);
				loadedDeleteSupport.delete(ctx);
			}
		} catch (InputRequiredException e) {
			throw new NuclosInputRequiredException(e);
		} catch (org.nuclos.api.exception.PointerException e) {
			throw new org.nuclos.common.PointerException(e.getMessage());
		} catch (BusinessException e) {
			if (e.getCause() != null && e.getCause() instanceof NuclosInputRequiredException) {
				throw (NuclosInputRequiredException) e.getCause();
			}
			throw new NuclosBusinessRuleException(e);
		}

		if (ctx.getRuleNotifications() != null
				&& ctx.getRuleNotifications().size() > 0) {
			sendMessagesByJMS(ctx.getRuleNotifications());
		}
	}

	/**
	 * send the notification messages of all rules in this transaction.
	 */
	private void sendMessagesByJMS(List<RuleNotification> lstRuleNotifications) {
		if (lstRuleNotifications.size() > 0) {
			LOG.info("JMS send rule notifications: {}: {}", lstRuleNotifications, this);
			for (RuleNotification notification : lstRuleNotifications) {
				NuclosJMSUtils.sendObjectMessageAfterCommit(notification,
						JMSConstants.TOPICNAME_RULENOTIFICATION,
						this.getCurrentUserName());
			}
		}
	}

	public <T> EntityObjectVO<T> fireGenerationEventSupport(UID genUid,
			EntityObjectVO<T> target,
			Collection<EntityObjectVO<T>> sources,
			EntityObjectVO paramEoVO, List<String> lstActions,
			PropertiesMap properties, Boolean after)  throws NuclosBusinessRuleException, NuclosCompileException, CommonPermissionException {

		final Map<String, Object> cache = new HashMap<String, Object>();
		List<EventSupportGenerationVO> eventSupportsByGenerationId;
		try {
			eventSupportsByGenerationId = getEventSupportsByGenerationUid(genUid);
			eventSupportsByGenerationId = CollectionUtils.sorted(eventSupportsByGenerationId, new Comparator<EventSupportGenerationVO>() {
				@Override
				public int compare(EventSupportGenerationVO o1, EventSupportGenerationVO o2) {
					return o1.getOrder().compareTo(o2.getOrder());
				}
			});
			
			if (eventSupportsByGenerationId.size() > 0) {

				final Collection<Modifiable> lstEoVOsSource = new HashSet<Modifiable>();
					
				for (EntityObjectVO srcEoVO : sources) {

					// Instantiate the corresponding JPA/BusinessObject file
					final AbstractBusinessObject busiObjSource = createBusinessObject(srcEoVO);

					if (busiObjSource == null) {
						throw new NuclosBusinessRuleException(
								"BusinessObject cannot be found");
					}
					if (!(busiObjSource instanceof Modifiable)) {
						throw new NuclosBusinessRuleException(
								"BusinessObject is not of type ModifyableBusinessObject");
					}
					lstEoVOsSource.add((Modifiable) busiObjSource);
				}

				// Instantiate the corresponding JPA/BusinessObject file
				final AbstractBusinessObject busiObjTarget = createBusinessObject(target);

				if (busiObjTarget == null) {
					throw new NuclosBusinessRuleException(
							"BusinessObject cannot be found");
				}
				if (!(busiObjTarget instanceof Modifiable)) {
					throw new NuclosBusinessRuleException(
							"BusinessObject is not of type ModifyableBusinessObject");
				}
				AbstractBusinessObject busiObjParametert = null;
				if (paramEoVO != null) {

					// Instantiate the corresponding JPA/BusinessObject file
					busiObjParametert = createBusinessObject(paramEoVO);

					if (busiObjParametert == null) {
						throw new NuclosBusinessRuleException(
								"BusinessObject cannot be found");
					}
					if (!(busiObjParametert instanceof Modifiable)) {
						throw new NuclosBusinessRuleException(
								"BusinessObject is not of type ModifyableBusinessObject");
					}
				}
				
				try { setMandatorRestriction(sources.iterator().next());

				// Loop all EventSupports
				for (EventSupportGenerationVO esgVO : eventSupportsByGenerationId) {
					// execute only active rules
					if (isActiveEventSupport(esgVO.getEventSupportClass())) {
						final GenerateContextImpl geObj = new GenerateContextImpl(
								cache, lstEoVOsSource, busiObjTarget,
								busiObjParametert, esgVO.getEventSupportClass(), this.dataLanguageCache.getLanguageToUse());

						if (after && GenerateFinalRule.class.getCanonicalName().equals(esgVO.getEventSupportClassType())) {
							final GenerateFinalRule genSupport = newEventSupportInstance(esgVO.getEventSupportClass());
							genSupport.generateFinal(geObj);
						} else if (!after && GenerateRule.class.getCanonicalName().equals(esgVO.getEventSupportClassType())) {
							final GenerateRule genSupport = newEventSupportInstance(esgVO.getEventSupportClass());
							genSupport.generate(geObj);
						} else {
							// after flag and rule type don't match -> do
							// nothing (tp)
							continue;
						}

						target = EOBOBridge.getEO(busiObjTarget);
						
						// check notifications
						if (geObj.getRuleNotifications() != null
								&& geObj.getRuleNotifications().size() > 0) {
							sendMessagesByJMS(geObj.getRuleNotifications());
						}
					}
				}
				
				} finally { revertMandatorRestriction(); }
			}
		} catch (org.nuclos.api.exception.PointerException e) {
			throw new org.nuclos.common.PointerException(e.getMessage());
		} catch (BusinessException e) {
			throw new NuclosBusinessRuleException( e);
		} catch (NuclosBusinessRuleException e) {
			throw e;
		}
	
		return target;
	}

	public <T> void fireInitMandator(EntityObjectVO<T> eoVO) throws NuclosBusinessRuleException, NuclosCompileException {

		if (eoVO == null) {
			throw new NullArgumentException("EntityObjectVO is null");
		}
		
		if (eoVO.getFieldUid(SF.MANDATOR_UID) != null) {
			// mandator is set already by client
			return;
		}
		
		UID entityUID = eoVO.getDalEntity();
		EntityMeta<?> eMeta = metaProvider.getEntity(entityUID);
		
		if (!eMeta.isMandator() || !securityCache.isMandatorPresent()) {
			return;
		}
		
		final Set<UID> accessibleMandators;
		if (nucletDalProvider.isAccessibleMandatorsSet()) {
			// Ein Aufruf von einer Regel aus...
			accessibleMandators = nucletDalProvider.getAccessibleMandators();
		} else {
			// nur die Anmeldung steht zur Verfügung...
			accessibleMandators = securityCache.getAccessibleMandators(getCurrentMandatorUID());
		}
		
		UID singleMandatorAlreadyFound = null;
		for (UID mandatorUID : accessibleMandators) {
			// Es muss nach Ebene eingeschränkt werden
			MandatorVO mandator = securityCache.getMandator(mandatorUID);
			if (eMeta.getMandatorLevel().equals(mandator.getLevelUID())) {
				if (singleMandatorAlreadyFound != null) {
					// Mehr als ein Mandant für die geforderte Ebene gefunden.
					// In diesem Fall muss eine NuclosMandatorProvider Implementierung den Mandanten liefern...
					singleMandatorAlreadyFound = null;
					break;
				} else {
					singleMandatorAlreadyFound = mandatorUID;
				}
			}
		}
		
		if (singleMandatorAlreadyFound != null) {
			eoVO.setFieldUid(SF.MANDATOR_UID, (org.nuclos.common.UID) singleMandatorAlreadyFound);
			return;
		}

		String mandatorProviderImpl = esCache.getEntityMandatorProviderImpl(eMeta.getUID());
		if (RigidUtils.looksEmpty(mandatorProviderImpl)) {
			throw new NuclosBusinessRuleException("Nuclos mandator provider implementation missing for bo " + esCache.getNucletPackageForEntity(eMeta.getUID()) + "." + eMeta.getEntityName());
		} else {
			try {
				// Instantiate the corresponding Mandator Provider Implementation
				AbstractBusinessObject.NuclosMandatorProvider mandatorProvider = newEventSupportInstance(mandatorProviderImpl);
				
				// Instantiate the corresponding BusinessObject file
				AbstractBusinessObject<T> newInstance = createBusinessObject(eoVO);

				final UID mandatorId = (UID) mandatorProvider.getNuclosMandatorId((Modifiable<Long>) newInstance);
				eoVO.setFieldUid(SF.MANDATOR_UID, mandatorId);
				
			} catch (IllegalArgumentException | SecurityException e) {
				throw new NuclosRuleCompileException(e);
			} catch (Exception e) {
				final Throwable cause = e.getCause();
				if (cause instanceof BusinessException) {
					throw new NuclosBusinessRuleException("fireInitMandator failed: " + e, (BusinessException) cause);
				} else {
					throw new NuclosRuleCompileException(cause);
				}
			}
		}
	}

	public <T> EntityObjectVO<T> fireSaveEventSupport(EntityObjectVO<T> eoVO,
			String sEventSupportType, boolean isCollectiveProcessing) throws NuclosBusinessRuleException,
			NuclosCompileException {

		if (eoVO == null) {
			throw new NullArgumentException("EntityObjectVO is null");
		}
		
		UID entityUID = eoVO.getDalEntity();
		EntityMeta<?> eMeta = metaProvider.getEntity(entityUID);

		UsageCriteria usagecriteria;
		if (!eMeta.isStateModel()) {
			usagecriteria = new UsageCriteria(entityUID, null, null, null);
		} else {
			UID processUID = eoVO.getFieldUid(SF.PROCESS_UID.getUID(entityUID));
			UID stateUID = eoVO.getFieldUid(SF.STATE_UID.getUID(entityUID));
			if (stateUID == null) {
				StateFacadeLocal facade = serverServiceLocator.getFacade(StateFacadeLocal.class);
				stateUID = facade
					.getInitialState(new UsageCriteria(entityUID, processUID, null, null)).getId();
			}
			usagecriteria = new UsageCriteria(entityUID, processUID, stateUID,
					null);
		}

		Collection<EventSupportSourceVO> lstRules = findEventSupportsByUsageAndEntity(
				sEventSupportType, usagecriteria);
		Map<String, Object> cache = new HashMap<>();
		
		try { setMandatorRestriction(eoVO);
	
		for (EventSupportSourceVO rVO : lstRules) {
			EventSupportSourceVO eseVO = esCache.getEventSupportSourceByClassname(rVO.getClassname());
			// Only execute active rules
			if (isActiveEventSupport(eseVO.getClassname())) {
				
				if (InsertRule.class.getCanonicalName().equals(sEventSupportType)) {
					eoVO = executeInsertSupportEvent(cache, 
							eseVO.getClassname(), eoVO, false);
				} else if (InsertFinalRule.class.getCanonicalName()
						.equals(sEventSupportType)) {
					eoVO = executeInsertSupportEvent(cache, 
							eseVO.getClassname(), eoVO, true);
				} else if (UpdateRule.class.getCanonicalName().equals(sEventSupportType)) {
					eoVO = executeUpdateSupportEvent(cache, 
							eseVO.getClassname(), eoVO, false, isCollectiveProcessing);
				} else if (UpdateFinalRule.class.getCanonicalName()
						.equals(sEventSupportType)) {
					eoVO = executeUpdateSupportEvent(cache,
							eseVO.getClassname(), eoVO, true, isCollectiveProcessing);
				}
			}
		}
		
		} finally { revertMandatorRestriction(); }

		return eoVO;
	}

	public <T> void fireDeleteEventSupport(EntityObjectVO<T> eoVO,
			String sEventSupportType, UsageCriteria usagecriteria,
			boolean isLogical) throws NuclosBusinessRuleException,
			NuclosCompileException {

		if (eoVO == null) {
			throw new NullArgumentException("EntityObjectVO is null");
		}

		Collection<EventSupportSourceVO> lstRules = findEventSupportsByUsageAndEntity(
				sEventSupportType, usagecriteria);
		Map<String, Object> cache = new HashMap<>();
		
		try { setMandatorRestriction(eoVO);

		for (EventSupportSourceVO eseVO : lstRules) {
			// execute only active rules
			if (isActiveEventSupport(eseVO.getClassname())) {
				if (DeleteRule.class.getCanonicalName().equals(sEventSupportType)) {
					executeDeleteSupportEvent(cache, eseVO.getClassname(),
							eoVO, false, isLogical);
				} else if (DeleteFinalRule.class.getCanonicalName().equals(sEventSupportType)) {
					executeDeleteSupportEvent(cache, eseVO.getClassname(),
							eoVO, true, isLogical);
				}
			}
		}
		
		} finally { revertMandatorRestriction(); }
	}

	private EventSupportSourceVO getEventSupportByClassname(String classname, boolean compileIfNeeded) {
		EventSupportSourceVO retVal = null;
		for (EventSupportSourceVO esVO : esCache.getExecutableEventSupportFiles()) {
			if (esVO.getClassname().equals(classname)) {
				retVal = esVO;
				break;
			}
		}
		return retVal;
	}

	public void firePrintFinalEventSupport(
			final EntityObjectVO<Long> eoVO,
			final String sEventSupportType, 
			final Collection<? extends PrintResult> colResults) throws NuclosBusinessRuleException,
				NuclosCompileException {

		if (eoVO == null) {
			throw new NullArgumentException("EntityObjectVO is null");
		}

		// obtain business object
		final UID entityUID = eoVO.getDalEntity();
		final EntityMeta<?> eMeta = metaProvider.getEntity(
				entityUID);

		UsageCriteria usagecriteria;
		if (!eMeta.isStateModel()) {
			usagecriteria = new UsageCriteria(entityUID, null, null, null);
		} else {
			UID processUID = eoVO.getFieldUid(SF.PROCESS_UID.getUID(entityUID));
			UID stateUID = eoVO.getFieldUid(SF.STATE_UID.getUID(entityUID));
			if (stateUID == null) {
				StateFacadeLocal facade = serverServiceLocator
						.getFacade(StateFacadeLocal.class);
				stateUID = facade.getInitialState(
						new UsageCriteria(entityUID, processUID, null, null))
						.getId();
			}
			usagecriteria = new UsageCriteria(entityUID, processUID, stateUID,
					null);
		}

		// obtain business rules PrintFinalRule
		final List<EventSupportSourceVO> lstRules = findEventSupportsByUsageAndEntity(
				sEventSupportType, usagecriteria);
		final Map<String, Object> cache = new HashMap<>();

		try {
			setMandatorRestriction(eoVO);
			for (EventSupportSourceVO rVO : lstRules) {
				EventSupportSourceVO eseVO = esCache
						.getEventSupportSourceByClassname(rVO.getClassname());
				// Only execute active rules
				if (isActiveEventSupport(eseVO.getClassname())) {
					executePrintFinalSupportEvent(
							cache, 
							rVO.getClassname(), 
							eoVO, 
							true, 
							usagecriteria,
							colResults);
				}
			}

		} finally {
			revertMandatorRestriction();
		}

	}
	
	public List<PrintoutTO> firePrintEventSupport(EntityObjectVO<Long> eoVO,
			String sEventSupportType) throws NuclosBusinessRuleException,
			NuclosCompileException {
		// result list for printouts, gets modified by rule execution 
		final List<PrintoutTO> result = new ArrayList<>();

		if (eoVO == null) {
			throw new NullArgumentException("EntityObjectVO is null");
		}

		// obtain business object
		final UID entityUID = eoVO.getDalEntity();
		final EntityMeta<?> eMeta = metaProvider.getEntity(entityUID);

		UsageCriteria usagecriteria;
		if (!eMeta.isStateModel()) {
			usagecriteria = new UsageCriteria(entityUID, null, null, null);
		} else {
			UID processUID = eoVO.getFieldUid(SF.PROCESS_UID.getUID(entityUID));
			UID stateUID = eoVO.getFieldUid(SF.STATE_UID.getUID(entityUID));
			if (stateUID == null) {
				StateFacadeLocal facade = serverServiceLocator
						.getFacade(StateFacadeLocal.class);
				stateUID = facade.getInitialState(
						new UsageCriteria(entityUID, processUID, null, null))
						.getId();
			}
			usagecriteria = new UsageCriteria(entityUID, processUID, stateUID,
					null);
		}

		// obtain business rules PrintRule
		final List<EventSupportSourceVO> lstRules = findEventSupportsByUsageAndEntity(
				sEventSupportType, usagecriteria);
		Map<String,Object> cache = new HashMap<>();

		try { setMandatorRestriction(eoVO);
		result.addAll(executePrintSupportEvent(cache, eoVO, false, usagecriteria, lstRules));

		} finally {
			revertMandatorRestriction();
		}

		return result;

	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = { Exception.class })
	public void fireTimelimitEventSupport(UID jobUid, Long sessionId)
			throws CommonPermissionException, NuclosCompileException,NuclosFatalException {
		
		List<EventSupportJobVO> allEventSupportsForJob = getEventSupportsByJobUid(jobUid);
		allEventSupportsForJob = CollectionUtils.sorted(allEventSupportsForJob, new Comparator<EventSupportJobVO>() {
			@Override
			public int compare(EventSupportJobVO o1, EventSupportJobVO o2) {
				return o1.getOrder().compareTo(o2.getOrder());
			}
		});
		
		try {
			setMandatorRestriction(null);
			setJobRestriction(jobUid);
		
			for (EventSupportJobVO esjVO : allEventSupportsForJob) {
				try {
					// only execute active rules
					if (isActiveEventSupport(esjVO.getEventSupportClass())) {
						EventSupportSourceVO source = esCache.getEventSupportSourceByClassname(esjVO.getEventSupportClass());
						TimelimitEventObjectImpl tleObj = new TimelimitEventObjectImpl(sessionId,
								getEventSupportByClassname(
										esjVO.getEventSupportClass(), true));
						if (source.getInterface().contains(TransactionalJobRule.class.getCanonicalName())) {
							TransactionalJobRule genSupport = (TransactionalJobRule) this.ccm
									.getClassLoaderAndCompileIfNeeded()
									.loadClass(esjVO.getEventSupportClass())
									.newInstance();
							
							List<Object> transactionalObjects = genSupport.getTransactionalObjects(tleObj);
							if (transactionalObjects == null) {
								throw new IllegalArgumentException("Transactional objects must not be null!");
							}
							Iterator<Object> itto = transactionalObjects.iterator();
							while (itto.hasNext()) {
								Object to = itto.next();
								tleObj.setTransactionalObject(to);
								tleObj.setLastTransactionalObject(!itto.hasNext());
								try {
									executeTransactionalJobWithObject((JobRule) genSupport, tleObj);
								} catch (BusinessException bex) {
									LOG.error(bex.toString(), bex);
									writeToJobRunMessages(sessionId, bex, to, tleObj.getRulename());
								}
								// release object...
								try {
									itto.remove();
								} catch (Exception ex) {
									// ignore, maybe unmodifiable
								}
							}
						} else {
							JobRule genSupport = (JobRule) this.ccm
								.getClassLoaderAndCompileIfNeeded()
								.loadClass(esjVO.getEventSupportClass())
								.newInstance();
							genSupport.execute(tleObj);
						}
					}
				} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
					LOG.error(e.toString(), e);
					writeToJobRunMessages(sessionId, e, esjVO.getEventSupportClass());
					throw new NuclosCompileException(e);
				} catch(Exception e) {
					writeToJobRunMessages(sessionId, e, esjVO.getEventSupportClass());
					throw new NuclosFatalException(e);
				}
			}
		} finally { 
			revertMandatorRestriction();
			revertJobRestriction();
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = { Exception.class })
	public void executeTransactionalJobWithObject(JobRule jobRule, JobContext context) throws BusinessException {
		if (context.getTransactionalObject() == null) {
			// ignore null objects
			return;
		}
		
		EntityObjectVO<?> eoIfAny = null;
		if (context.getTransactionalObject() instanceof EntityObjectVO) {
			eoIfAny = (EntityObjectVO<?>) context.getTransactionalObject();
		} else if (context.getTransactionalObject() instanceof AbstractBusinessObject) {
			eoIfAny = EOBOBridge.getEO((AbstractBusinessObject<?>) context.getTransactionalObject());
		}
		try { setMandatorRestriction(eoIfAny);
		
		jobRule.execute(context);
		
		} finally { revertMandatorRestriction(); }
	}

	@Override
	public List<EventSupportJobVO> getEventSupportsByJobUid(UID jobUID) throws CommonPermissionException{
		return esCache.getEventSupportsByJobUID(jobUID);
	}

	public List<EventSupportGenerationVO> getEventSupportsByGenerationUid(UID genUID) throws CommonPermissionException{
		return esCache.getEventSupportsByGenerationUID(genUID);
	}
	
	public List<EventSupportTransitionVO> getEventSupportsByTransitionUid(UID transUID) throws CommonPermissionException {
		return esCache.getEventSupportsByTransitionUID(transUID);
	}
		
	public List<EventSupportEventVO> getEventSupportsByEntityUid(UID entUID, boolean bIncludeIntegrationPointsInResult) throws CommonPermissionException {
		return esCache.getEventSupportsByEntityUID(entUID, bIncludeIntegrationPointsInResult);
	}
	
	public List<EventSupportCommunicationPortVO> getEventSupportsByCommunicationPortUid(UID portUID) {
		return esCache.getEventSupportsByCommunicationPortUID(portUID);
	}
	
	public <T> EventSupportCustomResult<T> fireCustomEventSupport(EntityObjectVO<T> eoVO,
			EventSupportSourceVO eseVO, boolean bIgnoreExceptions)
			throws NuclosBusinessRuleException, NuclosCompileException {

		Map<String, Object> cache = new HashMap<>();
		EventSupportCustomResult<T> result = new EventSupportCustomResult<>();

		try { setMandatorRestriction(eoVO);
			
		// only execute active rules
		if (isActiveEventSupport(eseVO.getClassname())) {
			//only execute assigned rules
			UsageCriteria usageCriteria = new UsageCriteria(eoVO.getDalEntity(), eoVO.getFieldUid(SF.PROCESS_UID), eoVO.getFieldUid(SF.STATE_UID), null);
			if (findEventSupportsByUsageAndEntity(CustomRule.class.getCanonicalName(), usageCriteria).contains(eseVO)) {
				result = executeCustomSupportEvent(cache, eseVO.getClassname(), eoVO);
			} else {
				throw new NuclosBusinessRuleException("rule execution not supported with this status and process!");
			}
		}
		
		} finally { revertMandatorRestriction(); }

		return result;
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = { Exception.class })
	public <T> void fireCommunicationEventSupport(CommunicationContext context)
			throws NuclosBusinessRuleException, NuclosCompileException {
		
		BusinessObject<T> bo = null;
			if (context instanceof PhoneCallRequestContextImpl) {
				PhoneCallRequestContextImpl phoneCall = (PhoneCallRequestContextImpl) context;
				EntityObjectVO<T> eo = (EntityObjectVO<T>) phoneCall.getEntityObject();
				if (eo != null && phoneCall.getBusinessObject(BusinessObject.class) == null) {

					// Instantiate the corresponding BusinessObject file
					AbstractBusinessObject<T> newInstance = createBusinessObject(eo);
					phoneCall.setBusinessObject(newInstance);
					phoneCall.setEntityObject(null);
				}
				bo = phoneCall.getBusinessObject(BusinessObject.class);
			}

			validateCommunicationContext(context);

			try { if (bo != null) {setMandatorRestrictionBO((AbstractBusinessObject<?>) bo);}

		final CommunicationPort port = context.getPort();
		List<EventSupportCommunicationPortVO> eventSupportsByCommunicationPortUid = getEventSupportsByCommunicationPortUid((UID)port.getId());
			for (EventSupportCommunicationPortVO evcpVO : eventSupportsByCommunicationPortUid) {
				EventSupportSourceVO eseVO = esCache.getEventSupportSourceByClassname(evcpVO.getEventSupportClass());
				if (eseVO != null) {
					// only execute active rules
					if (isActiveEventSupport(eseVO.getClassname())) {
					executeCommunicationSupportEvent(eseVO.getClassname(), context);
				}
			}
		}

		} finally { if (bo != null) {revertMandatorRestriction();} }
	}

	private void validateCommunicationContext(CommunicationContext context) throws NuclosBusinessRuleException {
		if (context instanceof PhoneCallRequestContext) {
			PhoneCallRequestContext phoneCall = (PhoneCallRequestContext) context;
			if (phoneCall.getBusinessObject(BusinessObject.class) == null) {
				throw new NuclosBusinessRuleException("Business object must not be null");
			}
			if (phoneCall.getNuclosUserAccount() == null) {
				throw new NuclosBusinessRuleException("User communication account must not be null");
			}
			if (phoneCall.getPort() == null) {
				throw new NuclosBusinessRuleException("Communication port must not be null");
			}
		}
	}

	private void executeCommunicationSupportEvent(String eventSupportClass, CommunicationContext context) throws NuclosBusinessRuleException, NuclosCompileException {

		try {
			CommunicationRule loadedCommunicationSupport = newEventSupportInstance(eventSupportClass);
			Class communicationContextClass = loadedCommunicationSupport.communicationContextClass();
			if (communicationContextClass.isAssignableFrom(context.getClass())) {
				loadedCommunicationSupport.communicate(context);
			}

		} catch (InputRequiredException e) {
			throw new NuclosInputRequiredException(e);
		} catch (org.nuclos.api.exception.PointerException e) {
			throw new org.nuclos.common.PointerException(e.getMessage());
		} catch (BusinessException e) {
			throw new NuclosBusinessRuleException(e);
		}
	}

	public List<EventSupportJobVO> getEventSupportJobs()
			throws CommonPermissionException {
		return esCache.getEventSupportJobs();

	}

	/**
	 *
	 * @return only event supports which are not annotated with SystemRuleUsage
	 * @throws NuclosFatalException
	 * @throws CommonPermissionException
	 */
	@Override
	public List<EventSupportSourceVO> getAllEventSupportsRemote()
			throws NuclosFatalException, CommonPermissionException {
		List<EventSupportSourceVO> result = new ArrayList<>();
		for (EventSupportSourceVO vo : this.esCache.getExecutableEventSupportFiles()) {
			if (vo.isSystem()) {
				continue;
			}
			result.add(vo);
		}
		return result;
	}

	@Override
	public List<EventSupportTypeVO> getAllEventSupportTypes()
			throws CommonPermissionException {
		return this.esCache.getEventSupportTypes();
	}

	@Override
	public boolean getUsesEventSupportForEntity(UID entityUID, String eventType) {
		for (EventSupportEventVO eventVO : this.esCache.getEventSupportsByEntityUID(entityUID, true)) {
			if (eventVO.getEventSupportClassType().equals(eventType)) {
				return true;
			}
		}
		return false;
	}

	public Map<String, ErrorMessage> getCompileExceptionMessages() {
		return this.compileCache.getMessages();
	}

	@Override
	public void invalidateCaches(EntityMeta<UID> entity) {
		esCache.invalidate(entity);
		NuclosJMSUtils.sendObjectMessageAfterCompletion(new EventSupportNotification(entity), JMSConstants.TOPICNAME_EVENTSUPPORTNOTIFICATION,
				SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());

	}
	
	@Override
	public void invalidateCaches() {
		esCache.invalidate();
		NuclosJMSUtils.sendObjectMessageAfterCompletion(new EventSupportNotification(E.NUCLET), JMSConstants.TOPICNAME_EVENTSUPPORTNOTIFICATION,
				SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
	}

	@Override
	public EventSupportEventVO modifyEventSupportEvent(
			EventSupportEventVO eseVOToUpdate, UID oldState, UID oldProcess)
			throws CommonPermissionException, CommonValidationException,
			CommonFinderException, NuclosBusinessRuleException,
			CommonCreateException, CommonRemoveException,
			CommonStaleVersionException, NuclosCompileException {
		this.checkWriteAllowed(E.SERVERCODEENTITY);
		
		eseVOToUpdate.validate();
		
		esCache.removeEventSupportEntityCache(eseVOToUpdate.getEntityUID(), eseVOToUpdate.getId());
		
		UID objId = (UID) masterDataFacade.modify(MasterDataWrapper.wrapEventSupportEventVO(eseVOToUpdate), null);
		eseVOToUpdate = MasterDataWrapper.getEventSupportEventVO(masterDataFacade.get(E.SERVERCODEENTITY.getUID(), objId));
		
		esCache.insertEventSupportEntityCache(eseVOToUpdate);
		
		return eseVOToUpdate;
	}

	@Override
	public List<ProcessVO> getProcessesByModule(UID moduleId) {
		List<ProcessVO> retVal = new ArrayList<>();

		Collection<EntityObjectVO<UID>> dependantMasterData = masterDataFacade.getDependantMd4FieldMeta(E.PROCESS.module, moduleId);
		for (EntityObjectVO<UID> mdVO : dependantMasterData) {
			retVal.add(MasterDataWrapper.getProcessVO(mdVO));
		}

		return retVal;
	}
	
	@Override
	public String getEntityProxyInterfaceQualifiedName(UID entityUID) {
		return esCache.getEntityProxyInterfaceQualifiedName(entityUID);
	}

	@Override
	public String getEntityProxyInterfaceQualifiedName(String entity, UID nucletUID) {
		return esCache.getEntityProxyInterfaceQualifiedName(entity, nucletUID);
	}
	
	/**
	 * commit or rollback a writing proxy call.
	 * runs in autonomous transaction!
	 * 
	 * @param entity
	 * @param proxy
	 * @param commit (true==commit; false=rollback)
	 * @throws NuclosCompileException
	 * @throws NuclosBusinessRuleException
	 */
	@Transactional(propagation=Propagation.REQUIRES_NEW, rollbackFor= {Exception.class})
	public void finishProxyCall(UID entity, Object proxy, boolean commit) throws NuclosCompileException, NuclosBusinessRuleException {
		final EntityMeta<?> eMeta = metaProvider.getEntity(entity);
		String proxyImpl = esCache.getEntityProxyImpl(eMeta.getUID());
		
		Class<?> proxyClass;
		try {
			proxyClass = ccm.getClassLoaderAndCompileIfNeeded().loadClass(proxyImpl);
			final Method method = proxyClass.getMethod(commit ? "commit" : "rollback");
			method.invoke(proxy);
		} catch (ClassNotFoundException | IllegalArgumentException | IllegalAccessException |
			SecurityException | NoSuchMethodException e) {
			throw new NuclosRuleCompileException(e);
		} catch (InvocationTargetException e) {
			final Throwable cause = e.getCause();
			if (cause instanceof BusinessException) {
				throw new NuclosBusinessRuleException("finishProxyCall failed: " + e, (BusinessException) cause);
			} else {
				throw new NuclosRuleCompileException(cause);				
			}
		} 
		
	}
	
	private User getUser() {
		User retVal = null;
		
		try {
			UserVO user = userFacade.getByUserName(getCurrentUserName());
			NuclosLocale nuclosLocale = null;
			if (dataLanguageCache != null && dataLanguageCache.getLanguageToUse() != null ){
				nuclosLocale = NuclosLocale.valueOf(dataLanguageCache.getLanguageToUse().toString().toUpperCase());
			}

			retVal = new UserImpl(user.getPrimaryKey(),
					user.getUsername(), user.getLastname(), user.getFirstname(), user.getEmail(),
					user.getLocked(), user.getSuperuser(), user.getLastPasswordChange(), user.getExpirationDate(), user.getPasswordChangeRequired(),
					nuclosLocale);
		} catch (CommonBusinessException cbe) {
			// Ignore, just return null if user hasn't been found (Same behaviour like before).
		}

		return retVal;
	}
	
	/**
	 * executes proxy call in current transaction
	 * 
	 * @param context
	 * @return proxy object (for commit or rollback)
	 * @throws NuclosBusinessRuleException
	 * @throws NuclosCompileException
	 */
	public <PK> Object executeProxyCall(ProxyContext<PK> context)
			throws NuclosCompileException, NuclosBusinessRuleException {

		final EntityMeta<?> eMeta = metaProvider.getEntity(context.getEntity());
		String proxyImpl = E.isNuclosEntity(eMeta.getUID()) ? eMeta.getSystemProxyImplementation() :
				esCache.getEntityProxyImpl(eMeta.getUID());
		if (RigidUtils.looksEmpty(proxyImpl)) {
			throw new NuclosFatalException("Proxy implementation unknown");
		}
		
		// Extract full classname for given entity
		String fullClassname = getFullClassnameForEntity(context.getEntity());
		if (fullClassname == null) {
			throw new NuclosFatalException("Entity unknown");
		}

		boolean bForSystemRulesOnly = proxyImpl.startsWith(INTERNAL_SYSTEM_RULE_PACKAGE);

		try {
			Class boClass = getBusinessObjectClass(fullClassname, bForSystemRulesOnly);

			Object proxy = newEventSupportInstance(proxyImpl);
			Class proxyClass = proxy.getClass();
			Method mSetUser = proxyClass.getMethod("setUser", org.nuclos.api.User.class);
			mSetUser.invoke(proxy, getUser());

			/*Class<? extends AbstractBusinessObject<PK>> boClass = (Class<? extends AbstractBusinessObject<PK>>) this.ccm
					.getClassLoaderAndCompileIfNeeded()
					.loadClass(fullClassname);*/
			Class<?> pkClass = eMeta.isUidEntity() ?
					(bForSystemRulesOnly ? org.nuclos.common.UID.class : org.nuclos.api.UID.class)
					: Long.class;
			
			switch (context.getType()) {
				case GET_BY_ID: {
					Method mGetter = proxyClass.getMethod("getById", pkClass);
					Object proxyResult = mGetter.invoke(proxy, context.getParamId());
					if (proxyResult != null) {
						final EntityObjectVO<PK> eo = EOBOBridge.getEO((AbstractBusinessObject<PK>) proxyResult);
						eo.reset();
						context.setSingleResult(eo);
					} else {
						throw new NuclosFatalException("Proxy result is null");
					}
				} break;
				case GET_BY_IDS: {
					Method mGetter = proxyClass.getMethod("getById", pkClass);
					context.setMultiResult(new ArrayList<EntityObjectVO<PK>>());
					for (Object pk : context.getParamIds()) {
						Object proxyResult = mGetter.invoke(proxy, pk);
						if (proxyResult != null) {
							final EntityObjectVO<PK> eo = EOBOBridge.getEO((AbstractBusinessObject<PK>) proxyResult);
							eo.reset();
							context.getMultiResult().add(eo);
						} else {
							throw new NuclosFatalException("Proxy result is null");
						}
					}
				} break;
				case GET_ALL_IDS: {
					Method mGetter = proxyClass.getMethod("getAllIds");
					Object proxyResult = mGetter.invoke(proxy);
					if (proxyResult != null) {
						context.setIdsResult((List<PK>) proxyResult);
					} else {
						throw new NuclosFatalException("Proxy result is null");
					}
				} break;
				case GET_ALL: {
					Method mGetter = proxyClass.getMethod("getAll");
					Object proxyResult = mGetter.invoke(proxy);
					List<Object> proxyResultList = (List<Object>) proxyResult;
					if (proxyResultList != null) {
						context.setMultiResult(CollectionUtils.transform(proxyResultList, new Transformer<Object, EntityObjectVO<PK>>() {
							@Override
							public EntityObjectVO<PK> transform(Object o) {
								final EntityObjectVO<PK> eo = EOBOBridge.getEO((AbstractBusinessObject<PK>) o);
								eo.reset();
								return eo;
							}
						}));
					} else {
						throw new NuclosFatalException("Proxy result is null");
					}
				} break;
				case GET_BY_FOREIGN_KEY: {
					List<AbstractBusinessObject<PK>> result = null;

					// NUCLOS-5741 First, if collective processing is supported, try this way
					if (CollectiveProcessingProxy.class.isAssignableFrom(proxyClass)) {
						try {
							Method mGetter = proxyClass.getMethod("getForCollectiveProcessing", ForeignKeyAttribute.class, Collection.class);

							String pName = context.getParamForeignMethodName().replaceFirst("getBy", "") + "Id";
							Field f = boClass.getDeclaredField(pName);
							ForeignKeyAttribute<?> fka = (ForeignKeyAttribute<?>)f.get(null);
	;
							result = RigidUtils.uncheckedCast(mGetter.invoke(proxy, fka, context.getParamForeignIds()));
						} catch (ReflectiveOperationException roe) {
							LOG.warn(roe.getMessage(), roe);
						}
					}

					// If collective processing was not supported or did not yield any result, use the standard way
					if (result == null) {
						result = new ArrayList<>();
						Method mGetter = proxyClass.getMethod(context.getParamForeignMethodName(), context.getParamForeignIdClass());
						for (Object fkId : context.getParamForeignIds()) {
							List<AbstractBusinessObject<PK>> proxyResultList = RigidUtils.uncheckedCast(mGetter.invoke(proxy, fkId));
							if (proxyResultList == null) {
								throw new NuclosFatalException("Proxy result is null");
							}
							result.addAll(proxyResultList);
						}
					}
					;
					context.setMultiResult(CollectionUtils.transform(result, new Transformer<AbstractBusinessObject<PK>, EntityObjectVO<PK>>() {
						@Override
						public EntityObjectVO<PK> transform(AbstractBusinessObject<PK> o) {
							final EntityObjectVO<PK> eo = EOBOBridge.getEO(o);
							eo.reset();
							return eo;
						}
					}));
				} break;
				case INSERT_OR_UPDATE: {
					String sMethod;
					if (context.getParamObject().isFlagNew()) {
						sMethod = "insert";
					} else {
						sMethod = "update";
					}
					Method method = proxyClass.getMethod(sMethod, boClass);
					AbstractBusinessObject<PK> param = (AbstractBusinessObject<PK>) boClass.getDeclaredConstructor().newInstance();
					EOBOBridge.setEO(param, context.getParamObject());

					Object proxyResult =method.invoke(proxy, param);
					context.setPKResult(proxyResult);					
				} break;
				case INSERT_OR_UPDATE_BATCH: {
					Method insert = proxyClass.getMethod("insert", boClass);
					Method update = proxyClass.getMethod("update", boClass);
					for (EntityObjectVO<PK> eo : context.getParamObjects()) {
						AbstractBusinessObject<PK> param = (AbstractBusinessObject<PK>) boClass.getDeclaredConstructor().newInstance();
						EOBOBridge.setEO(param, eo);
						try {
							if (eo.isFlagNew()) {
								insert.invoke(proxy, param);
							} else {
								update.invoke(proxy, param);
							}
						} catch (RuntimeException ex) {
							if (!context.isFailAfterBatch()) {
								throw new NuclosRuleCompileException(ex);
							}
							context.getDalCallResult().addRuntimeException(ex);
						}
					}
				} break;
				case DELETE: {
					Method delete = proxyClass.getMethod("delete", pkClass);
					delete.invoke(proxy, context.getParamId());
				} break;
				case DELETE_BATCH: {
					Method delete = proxyClass.getMethod("delete", pkClass);
					for (Delete<PK> del : context.getParamDeletes()) {
						try {
							delete.invoke(proxy, del.getPrimaryKey());
						} catch (RuntimeException ex) {
							if (!context.isFailAfterBatch()) {
								throw new NuclosRuleCompileException(ex);
							}
							context.getDalCallResult().addRuntimeException(ex);
						}
					}
					Method method = proxyClass.getMethod("delete", pkClass);
					method.invoke(proxy, context.getParamId());
				} break;
				default:
					throw new IllegalArgumentException("Type " + context.getType() + " unknown");
			}
			
			return proxy;

		} catch (InvocationTargetException e) {
			final Throwable cause = e.getCause();
			if (cause instanceof BusinessException) {
				throw new NuclosBusinessRuleException((BusinessException) cause);
			}
			throw new NuclosBusinessRuleException(new BusinessException(cause));
		} catch (InstantiationException | IllegalArgumentException | SecurityException |
			NoSuchMethodException | ClassNotFoundException | IllegalAccessException e) {
			throw new NuclosRuleCompileException(e);
		}
	}
	
	private void setMandatorRestrictionBO(AbstractBusinessObject<?> bo) {
		setMandatorRestriction(EOBOBridge.getEO(bo));
	}
	
	private synchronized void setMandatorRestriction(EntityObjectVO<?> eo) {
		if (!securityCache.isMandatorPresent()) {
			return;
		}
		
		if (eo == null || eo.getFieldUid(SF.MANDATOR_UID) == null) {
			Set<UID> accessibleByLogin = securityCache.getAccessibleMandators(getCurrentMandatorUID());
			nucletDalProvider.setAccessibleMandators(accessibleByLogin, getCurrentMandatorUID());
		} else {
			UID mandatorUID = eo.getFieldUid(SF.MANDATOR_UID);

			Set<UID> accessibleByEO = securityCache.getAccessibleMandators(mandatorUID);
			Set<UID> accessibleByLogin = securityCache.getAccessibleMandators(getCurrentMandatorUID());
			Set<UID> accessibleMandators = CollectionUtils.intersection(accessibleByEO, accessibleByLogin);
			
			nucletDalProvider.setAccessibleMandators(accessibleMandators, mandatorUID);
		}
	}
	
	private synchronized void revertMandatorRestriction() {
		if (!securityCache.isMandatorPresent()) {
			return;
		}
		
		nucletDalProvider.removeAccessibleMandators();
	}
	
	private synchronized void setJobRestriction(UID job) {
		nucletDalProvider.setRunningJob(job);
	}
	
	private synchronized void revertJobRestriction() {
		nucletDalProvider.removeRunningJob();
	}
	
	private void writeToJobRunMessages(Long iSessionId, Exception exception, String sRuleName) {
		this.writeToJobRunMessages(iSessionId, exception, null, sRuleName);
	}

	private void writeToJobRunMessages(Long iSessionId, Exception exception, Object transactionalObject, String sRuleName) {
		try {
			String sMessage = "";
			if (transactionalObject != null) {
				sMessage = sMessage + transactionalObject.toString() + ": ";
			}
			sMessage = sMessage + (exception.getMessage() == null ? sMessage.getClass().getCanonicalName() : exception.getMessage());

			jobControl.writeToJobRunMessages(iSessionId, "ERROR", sMessage, sRuleName);
		}
		catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	public List<EventSupportCommunicationPortVO> getEventSupportCommunicationPorts()
			throws CommonPermissionException {
		return this.esCache.getEventSupportCommunicationPorts();
	}

	@Override
	public EventSupportCommunicationPortVO createEventSupportCommunicationPort(
			EventSupportCommunicationPortVO escpVOToInsert)
			throws CommonPermissionException, CommonValidationException,
			NuclosBusinessRuleException, CommonCreateException {
		
		this.checkWriteAllowed(E.SERVERCODECOMMUNICATIONPORT);

		escpVOToInsert.validate();
		escpVOToInsert = MasterDataWrapper.getEventSupportCommunicationPortVO(masterDataFacade.create(
				MasterDataWrapper.wrapEventSupportCommunicationPortVO(escpVOToInsert), null).getEntityObject());
		
		esCache.insertEventSupportCommunicationPortCache(escpVOToInsert);
		
		return escpVOToInsert;

	}

	@Override
	public EventSupportCommunicationPortVO modifyEventSupportCommunicationPort(
			EventSupportCommunicationPortVO escpVOToUpdate)
			throws CommonPermissionException, CommonValidationException,
			NuclosBusinessRuleException, CommonCreateException,
			CommonFinderException, CommonRemoveException,
			CommonStaleVersionException, NuclosCompileException {
		
		this.checkWriteAllowed(E.SERVERCODECOMMUNICATIONPORT);

		escpVOToUpdate.validate();			
		
		esCache.removeEventSupportCommunicationPortCache(escpVOToUpdate.getPort(), escpVOToUpdate.getId());
		
		UID modify = masterDataFacade.modify(
				MasterDataWrapper.wrapEventSupportCommunicationPortVO(escpVOToUpdate), null);
		
		escpVOToUpdate = MasterDataWrapper.getEventSupportCommunicationPortVO(
				masterDataFacade.get(E.SERVERCODECOMMUNICATIONPORT.getUID(), modify).getEntityObject());
		
		esCache.insertEventSupportCommunicationPortCache(escpVOToUpdate);
	
		return escpVOToUpdate;
	}

	@Override
	public void deleteEventSupportCommunicationPort(
			EventSupportCommunicationPortVO escpVOToDelete)
			throws CommonPermissionException, CommonValidationException,
			NuclosBusinessRuleException, CommonFinderException,
			CommonRemoveException, CommonStaleVersionException,
			NuclosCompileException {
		
		this.checkWriteAllowed(E.SERVERCODECOMMUNICATIONPORT);

		escpVOToDelete.validate();			
		masterDataFacade.remove(E.SERVERCODECOMMUNICATIONPORT.getUID(), escpVOToDelete.getPrimaryKey(), false);
		
		esCache.removeEventSupportCommunicationPortCache(escpVOToDelete.getPort(), escpVOToDelete.getId());
		
	}

	@Override
	public List<CommunicationPortVO> getCommunicationPortByClassname(
			String classname) throws CommonFinderException,
			CommonPermissionException {
		final List<CommunicationPortVO> retVal = new ArrayList<>();
		for (EventSupportCommunicationPortVO escpVO : esCache.getEventSupportCommunicationPorts()) {
			if (classname.equals(escpVO.getEventSupportClass())) {
				EntityObjectVO<UID> byPrimaryKey = nucletDalProvider.getEntityObjectProcessor(E.COMMUNICATION_PORT).getByPrimaryKey(escpVO.getPort());
				retVal.add(MasterDataWrapper.getCommunicationPortVO(byPrimaryKey));
			}
		}
		return retVal;
	}
	
	
	@Override
	public boolean isSourceCodeScannerRunning() {
		return sourceScanner.isRunning();
	}
	
	@Override
	public void setSourceCodeScannerActive(boolean activate) throws CommonPermissionException {
		if (!SecurityCache.getInstance().isSuperUser(getCurrentUserName())) {
			throw new CommonPermissionException();
		}
		if (isSourceCodeScannerRunning() && !activate) {
			sourceScanner.cancel();
		} else
		if (!isSourceCodeScannerRunning() && activate) {
			sourceScanner.start();
		}
	}
	
	@Override
	public void readSourcesAndCompile() throws CommonPermissionException {
		if (!SecurityCache.getInstance().isSuperUser(getCurrentUserName())) {
			throw new CommonPermissionException();
		}
		sourceScanner.runOnce();
	}

	private static class NoRuleClassPrintout implements Printout {

		private final UID printoutId;
		private Long boId;
		private List<NoRuleClassOutputFormat> outputFormats;

		public NoRuleClassPrintout(UID printoutId) {
			this.printoutId = printoutId;
		}

		@Override
		public org.nuclos.api.UID getId() {
			return printoutId;
		}

		@Override
		public void setBusinessObjectId(final Long boId) {
			this.boId = boId;
		}

		@Override
		public Long getBusinessObjectId() {
			return boId;
		}

		@Override
		public List<NoRuleClassOutputFormat> getOutputFormats() {
			List<NoRuleClassOutputFormat> result = this.outputFormats;
			if (result == null) {
				result = new ArrayList<>();
				ReportFacadeLocal reportFacadeLocal = SpringApplicationContextHolder.getBean(ReportFacadeLocal.class);
				final Collection<DefaultReportOutputVO> reportOutputs = reportFacadeLocal.getReportOutputs(printoutId);
				for (DefaultReportOutputVO reportOutput : reportOutputs) {
					result.add(new NoRuleClassOutputFormat(reportOutput.getId()));
				}
				this.outputFormats = result;
			}
			return result;
		}
	}

	private static class NoRuleClassOutputFormat extends AbstractOutputFormat {

		public NoRuleClassOutputFormat(final org.nuclos.api.UID id) {
			super(id);
		}

	}
}
