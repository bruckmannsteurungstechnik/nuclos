//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.fileimport;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.fileimport.ImportResult;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.fileimport.NuclosFileImportException;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.StateCache;
import org.nuclos.server.common.ejb3.EntityObjectFacadeLocal;
import org.nuclos.server.common.ejb3.LocaleFacadeLocal;
import org.nuclos.server.dal.processor.ProcessorFactorySingleton;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.genericobject.Modules;
import org.nuclos.server.genericobject.ejb3.GenericObjectFacadeLocal;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.statemodel.ejb3.StateFacadeLocal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * Base class for object imports
 *
 * Provide structure definitions, facades and caches for object import implementations
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Configurable
public abstract class AbstractImport implements Import {
	
	static class ImportStatusImpl implements ImportStatus {
		
		private final ImportResult result;
		private final String message;
		private final ImportProgressNotifier notifier;
		
		ImportStatusImpl(ImportResult result, String message) {
			this(null, result, message);
		}
		
		ImportStatusImpl(ImportProgressNotifier notifier, ImportResult result, String message) {
			this.result = result;
			this.message = message;
			this.notifier = notifier;
		}
		
		@Override
		public void cleanUp() {
			if (this.notifier != null) {
				if (result.equals(ImportResult.ERROR))
					this.notifier.stop(message);
				else
					this.notifier.finish();
			}
		}
		
		@Override
		public String getMessage() {
			return message;
		}
		
		@Override
		public ImportResult getImportResult() {
			return result;
		}
	}

	//TODO MULTINUCLET  
	protected static final UID TMP_UID = new UID("temp");

	private final GenericObjectDocumentFile file;

	private ImportProgressNotifier notifier;
	
	private final ImportContext context;

	private final boolean useClientNotification;
	
	private final ImportLogger logger;

	private final List<? extends ImportStructure> structures;

	private final boolean atomic;

	private List<UID> importedEntities = new ArrayList<UID>();

	private Map<UID, Set<UID>> keyDefinitions = new HashMap<UID, Set<UID>>();

	private Map<UID, Integer> importSettings = new HashMap<UID, Integer>();

	private Map<UID, Map<ImportObjectKey, ImportObject>> data = new HashMap<UID, Map<ImportObjectKey, ImportObject>>();

	private final Map<UID, Map<String, UID>> processCache = new HashMap<UID, Map<String,UID>>();

	private final Map<UID, Map<UID, Map<String, UID>>> stateCache = new HashMap<UID, Map<UID, Map<String, UID>>>();

	private final ScriptEngine engine = new ScriptEngineManager().getEngineByName("groovy");
	
	// Spring injection
	
	@Autowired
	StateFacadeLocal stateFacade;
	
	@Autowired
	MasterDataFacadeLocal masterDataFacade;
	
	@Autowired
	GenericObjectFacadeLocal genericObjectFacade;
	
	@Autowired
	LocaleFacadeLocal localeFacade;
	
	@Autowired
	ProcessorFactorySingleton processorFactorySingleton;
	
	@Autowired
	MetaProvider metaProvider;
	
	@Autowired
	NucletDalProvider nucletDalProvider;
	
	@Autowired
	Modules modules;
	
	@Autowired
	ServerParameterProvider serverParameterProvider;
	
	@Autowired
	EntityObjectFacadeLocal entityObjectFacade;
	
	// end of Spring injection
	
	protected AbstractImport(GenericObjectDocumentFile file, ImportContext context, 
			List<? extends ImportStructure> structures, ImportLogger logger, boolean atomic) {
		
		this.file = file;
		
		if (context == null) {
			this.useClientNotification= false;
			this.context = null;
		}
		else {
			this.context = context;
			this.useClientNotification= true; 
		}
		this.structures = structures;
		this.logger = logger;
		this.atomic = atomic;
	}
	
	@PostConstruct
	void init() {
		LocaleInfo localeInfo = getLocaleInfoFromContext();

		for (final ImportStructure is : structures) {
			if (!importedEntities.contains(is.getEntityUid())) {
				importedEntities.add(is.getEntityUid());
				importSettings.put(is.getEntityUid(), is.getImportSettings());
			}
		}

		for (final ImportStructure definition : this.structures) {
			if (!keyDefinitions.containsKey(definition.getEntityUid())) {
				keyDefinitions.put(definition.getEntityUid(), definition.getIdentifiers());
			}

			if (modules.isModule(definition.getEntityUid())) {
				final UID moduleUid = modules.getModule(definition.getEntityUid()).getUID();

				// cache processes
				final Collection<EntityObjectVO<UID>> processes = masterDataFacade.getDependantMd4FieldMeta(E.PROCESS.module, moduleUid);

				if (processes != null) {
					Map<String, UID> processIdsByName = new HashMap<String, UID>();
					for (final EntityObjectVO<UID> process : processes) {
						processIdsByName.put(process.getFieldValue(E.PROCESS.name), process.getPrimaryKey());
					}
					processCache.put(definition.getEntityUid(), processIdsByName);
				}
				
				// cache states
				if (metaProvider.getEntity(moduleUid).isStateModel()) {
					final Collection<EntityObjectVO<UID>> statemodelUsages = masterDataFacade.getDependantMd4FieldMeta(
							E.STATEMODELUSAGE.nuclos_module, moduleUid);

					StateCache serverStateCache = SpringApplicationContextHolder.getBean(StateCache.class);
					final Map<UID, Map<String, UID>> statesByProcess = new HashMap<UID, Map<String, UID>>();
					for (EntityObjectVO<UID> statemodelUsage : statemodelUsages) {
						Map<String, UID> stateIdsByName = new HashMap<String, UID>();

						final Collection<EntityObjectVO<UID>> states = masterDataFacade.getDependantMd4FieldMeta(
								E.STATE.model, statemodelUsage.getFieldUid(E.STATEMODELUSAGE.statemodel));
						for (final EntityObjectVO<UID> state : states) {
							if (localeInfo != null) {
								try {
									stateIdsByName.put(serverStateCache.getStateById(state.getPrimaryKey()).getStatename(localeInfo.toLocale()), state.getPrimaryKey());
								} catch (CommonFinderException ex) {
									stateIdsByName.put(state.getFieldValue(E.STATE.name), state.getPrimaryKey());
								}
							} else {
								stateIdsByName.put(state.getFieldValue(E.STATE.name), state.getPrimaryKey());
							}
						}
						statesByProcess.put(statemodelUsage.getFieldUid(E.STATEMODELUSAGE.process), stateIdsByName);
					}
					stateCache.put(definition.getEntityUid(), statesByProcess);
				}
			}

			for (final Item item : definition.getItems().values()) {
				if (item.isReferencing()) {
					Set<UID> foreignkey = new HashSet<UID>();

					for (ForeignEntityIdentifier fei : item.getForeignEntityIdentifiers()) {
						foreignkey.add(fei.getFieldUID());
					}

					if (!keyDefinitions.containsKey(item.getForeignEntityUID())) {
						keyDefinitions.put(item.getForeignEntityUID(), foreignkey);
					}
				}
			}
		}

		// setup context
		for (final UID entityUid : keyDefinitions.keySet()) {
			data.put(entityUid, new HashMap<ImportObjectKey, ImportObject>());
		}
	}

	
	@Override
	public boolean isUseClientNotification() {
		return useClientNotification;
	}

	@Override
	public abstract ImportStatus doImport() throws NuclosFileImportException;

	@Override
	public GenericObjectDocumentFile getFile() {
		return file;
	}

	@Override
	public ImportContext getContext() {
		return context;
	}

	public ImportLogger getLogger() {
		return logger;
	}

	@Override
	public List<? extends ImportStructure> getStructures() {
		return structures;
	}

	@Override
	public boolean isAtomic() {
		return atomic;
	}

	@Override
	public List<UID> getImportedEntities() {
		return importedEntities;
	}

	@Override
	public Map<UID, Set<UID>> getKeyDefinitions() {
		return keyDefinitions;
	}

	@Override
	public Map<UID, Integer> getImportSettings() {
		return importSettings;
	}

	@Override
	public Map<UID, Map<ImportObjectKey, ImportObject>> getData() {
		return data;
	}

	public Map<UID, Map<String, UID>> getProcessCache() {
		return processCache;
	}

	public Map<UID, Map<UID, Map<String, UID>>> getStateCache() {
		return stateCache;
	}

	protected void checkInterrupted() throws NuclosFileImportException {
		if (isUseClientNotification() && getContext().isInterrupted()) {
			throw new NuclosFileImportException("import.notification.stopped");
		}
	}

	/**
	 * Add new parsed object to context data
	 *
	 * @param importObject imported object
	 */
	public void putObject(ImportObject importObject) {
		if (importObject.isEmpty()) {
			return;
		}

		if (!getData().get(importObject.getEntityUID()).containsKey(importObject.getKey())) {
			for (final Entry<UID, ImportObject> entry : importObject.getReferences().entrySet()) {
				if (getData().get(entry.getValue().getEntityUID()).containsKey(entry.getValue().getKey())) {
					entry.setValue(getData().get(entry.getValue().getEntityUID()).get(entry.getValue().getKey()));
				}
				else {
					getData().get(entry.getValue().getEntityUID()).put(entry.getValue().getKey(), entry.getValue());
				}
			}
			getData().get(importObject.getEntityUID()).put(importObject.getKey(), importObject);
		}
		else {
			// update references in imported object
			for (final Entry<UID, ImportObject> entry : importObject.getReferences().entrySet()) {
				if (getData().get(entry.getValue().getEntityUID()).containsKey(entry.getValue().getKey())) {
					entry.setValue(getData().get(entry.getValue().getEntityUID()).get(entry.getValue().getKey()));
				}
				else {
					getData().get(entry.getValue().getEntityUID()).put(entry.getValue().getKey(), entry.getValue());
				}
			}
			// merge attributes and references into existing object
			getData().get(importObject.getEntityUID()).get(importObject.getKey()).getAttributes().putAll(importObject.getAttributes());
			getData().get(importObject.getEntityUID()).get(importObject.getKey()).getReferences().putAll(importObject.getReferences());
		}
	}

	/**
	 * Parse line and create new import object
	 *
	 * @param importDefinition import structure definition for imported line
	 * @param asLineValues parsed csv line
	 */
	protected ImportObject getObjectFromCsv(ImportStructure importDefinition, String[] asLineValues, int lineNumber) throws NuclosFileImportException {
		final UID entityUid = importDefinition.getEntityUid();
		final Map<UID, Object> attributes = new HashMap<UID, Object>();
		final Map<UID, ImportObject> references = new HashMap<UID, ImportObject>();

		for (final Item i: importDefinition.getItems().values()) {
			final CsvItem item = (CsvItem) i;
			final UID keyUid = item.getFieldUID();

			if (item.getScript() != null) {
				final Bindings b = engine.createBindings();
				b.put("values", asLineValues);
				b.put("line", lineNumber);
				b.put("log", logger);

		        try {
					Object o = engine.eval(item.getScript().getSource(), b);
					if (item.isReferencing()) {
						attributes.put(keyUid, (Long) o);
					}
					else {
						attributes.put(keyUid, o);
					}
				}
		        catch (ScriptException e) {
		        	throw new NuclosFileImportException(e);
				}
			}
			else {
				if (item.isReferencing()) {
					final ImportObject reference = getReferencedObjectFromCsv(importDefinition, asLineValues, keyUid, lineNumber);
					if (!reference.isEmpty()) {
						// check if reference is already in context data
						ImportObject contextReferenced = getData().get(item.getForeignEntityUID()).get(reference.getKey());
						if (contextReferenced != null) {
							references.put(keyUid, contextReferenced);
						}
						else {
							references.put(keyUid, reference);
						}
					}
				}
				else {
					if (item.getColumn() <= asLineValues.length) {
						Object oValue = item.parse(asLineValues[item.getColumn() - 1]);
						if (oValue == null && item.getCls() == Boolean.class){
							oValue = false;
						}
						attributes.put(item.getFieldUID(), oValue);
					}
					else {
						throw new NuclosFileImportException(StringUtils.getParameterizedExceptionMessage(
								"import.exception.indexoutofbounds", lineNumber, item.getColumn(), asLineValues.length));
					}
				}
			}
		}
		ImportObjectKey key = getKey(importDefinition, attributes, entityUid, references);
		final ImportObject result = new ImportObject(entityUid, key, attributes, lineNumber, references);
		return result;
	}

	/**
	 * Create new referenced import object from parsed line
	 *
	 * @param importDefinition import structure definition for imported line
	 * @param asLineValues parsed csv line
	 * @param referencingFieldUid referencing field name
	 */
	protected ImportObject getReferencedObjectFromCsv(ImportStructure importDefinition, String[] asLineValues, UID referencingFieldUid, int lineNumber) throws NuclosFileImportException {
		final Collection<? extends ForeignEntityIdentifier> feIdentifiers = 
				importDefinition.getItems().get(referencingFieldUid).getForeignEntityIdentifiers();

		UID entityUid;
		try {
			entityUid = feIdentifiers.iterator().next().getEntityUID();
		}
		catch (NoSuchElementException e) {
			String msg = "No foreign entity(s) defined for " + referencingFieldUid;
			logger.error(msg);
			throw new IllegalArgumentException(msg);
		}
		final Map<UID, Object> keyMap = new HashMap<UID, Object>();

		for (ForeignEntityIdentifier fei: feIdentifiers) {
			final CsvForeignEntityIdentifier feIdentifier = (CsvForeignEntityIdentifier) fei;
			final UID keyUid = feIdentifier.getFieldUID();
			if (feIdentifier.getColumn() <= asLineValues.length) {
				final Object oValue = feIdentifier.parse(asLineValues[feIdentifier.getColumn() - 1]);
				keyMap.put(keyUid, oValue);
			}
			else {
				throw new NuclosFileImportException(StringUtils.getParameterizedExceptionMessage(
						"import.exception.indexoutofbounds", lineNumber, feIdentifier.getColumn(), asLineValues.length));
			}
		}
		
		final ImportObject result = new ImportObject(entityUid, getForeignKey(importDefinition, keyMap, importDefinition.getEntityUid(), referencingFieldUid), keyMap, lineNumber);
		return result;
	}

	/**
	 * @param importDefinition
	 * @param entityMap - relative XPath -> value
	 * @param lineNumber
	 * @return
	 * @throws NuclosFileImportException
	 */
	protected ImportObject getObjectFromXml(ImportStructure importDefinition, Map<String, String> entityMap,
			int lineNumber) throws NuclosFileImportException {
		final UID entityUid = importDefinition.getEntityUid();
		final Map<UID, Object> attributes = new HashMap<UID, Object>();
		final Map<UID, ImportObject> references = new HashMap<UID, ImportObject>();

		for (final Item i : importDefinition.getItems().values()) {
			final XmlItem item = (XmlItem) i;
			final UID keyUid = item.getFieldUID();
			if (item.getScript() != null) {
				final Bindings b = engine.createBindings();
				b.put("xpath2value", entityMap);
				b.put("line", lineNumber);
				b.put("log", logger);

				try {
					Object o = engine.eval(item.getScript().getSource(), b);
					if (item.isReferencing()) {
						attributes.put(keyUid, (Long) o);
					}
					else {
						attributes.put(keyUid, o);
					}
				} catch (ScriptException e) {
					throw new NuclosFileImportException(e);
				}
			} else {
				if (item.isReferencing()) {
					final ImportObject reference = getReferencedObjectFromXml(importDefinition, entityMap, keyUid,
							lineNumber);
					if (!reference.isEmpty()) {
						// check if reference is already in context data
						ImportObject contextReferenced = getData().get(item.getForeignEntityUID()).get(
								reference.getKey());
						if (contextReferenced != null) {
							references.put(keyUid, contextReferenced);
						}
						else {
							references.put(keyUid, reference);
						}
					}
				}
				else {
					if (entityMap.containsKey(item.getMatch())) {
						final Object value = item.parse(entityMap.get(item.getMatch()));
						attributes.put(item.getFieldUID(), value);
					} else {
						throw new NuclosFileImportException(StringUtils.getParameterizedExceptionMessage(
								"import.exception.attributenotfound", lineNumber, item.getMatch(), entityMap));
					}
				}
			}
		}
		final ImportObject result = new ImportObject(entityUid, getKey(importDefinition, attributes, entityUid,
				references), attributes, lineNumber, references);
		return result;
	}

	/**
	 * Create new referenced import object from parsed xml (sub-)structure
	 *
	 * @param  importDefinition import structure definition for imported line
	 * @param  entityMap - relative XPath -> value
	 * @param  referencingFieldUid referencing field name
	 * @param  lineNumber of xml file
	 * @return import object similiar to CSV import
	 * @throws NuclosFileImportException
	 */
	protected ImportObject getReferencedObjectFromXml(ImportStructure importDefinition, Map<String,String> entityMap, 
			UID referencingFieldUid, int lineNumber) throws NuclosFileImportException {
		
		final Collection<? extends ForeignEntityIdentifier> feIdentifiers = 
				importDefinition.getItems().get(referencingFieldUid).getForeignEntityIdentifiers();

		UID entityUid;
		try {
			entityUid = feIdentifiers.iterator().next().getEntityUID();
		}
		catch (NoSuchElementException e) {
			String msg = "No foreign entity(s) defined for " + referencingFieldUid;
			logger.error(msg);
			throw new IllegalArgumentException(msg);
		}
		final Map<UID, Object> keyMap = new HashMap<UID, Object>();

		for (ForeignEntityIdentifier fei: feIdentifiers) {
			final XmlForeignEntityIdentifier feIdentifier = (XmlForeignEntityIdentifier) fei;
			final UID keyUid = feIdentifier.getFieldUID();
			
			if (entityMap.containsKey(feIdentifier.getMatch())) {
				final Object value = feIdentifier.parse(entityMap.get(feIdentifier.getMatch()));
				keyMap.put(keyUid, value);
			} else {
				throw new NuclosFileImportException(StringUtils.getParameterizedExceptionMessage(
						"import.exception.attributenotfound", lineNumber, feIdentifier.getMatch(), entityMap));
			}
		}
		
		final ImportObject result = new ImportObject(entityUid, getForeignKey(
				importDefinition, keyMap, importDefinition.getEntityUid(), referencingFieldUid), keyMap, lineNumber);
		return result;
	}

	/**
	 * Create primary key from parsed values
	 *
	 * @param definition parsed attribute values
	 * @param entityUid entity UID
	 * @param references foreign keys
	 */
	protected ImportObjectKey getKey(ImportStructure definition, Map<UID, Object> attributes, UID entityUid, Map<UID, ImportObject> references) {
		final Map<UID, Object> keyMap = new HashMap<UID, Object>();
		for (final UID identifier : definition.getIdentifiers()) {
			if (definition.getItems().get(identifier).isReferencing()) {
				keyMap.put(identifier, references.get(identifier));
			}
			else {
				keyMap.put(identifier, attributes.get(identifier));
			}
		}

		// no identifiers set -> generate random unique id
		if (definition.getIdentifiers().size() == 0) {
			//TODO MULTINUCLET 
			keyMap.put(TMP_UID, UUID.randomUUID());
		}
		return new ImportObjectKey(keyMap);
	}

	/**
	 * Create foreign key by parsed values
	 *
	 * @param attributes values
	 * @param referencingEntityUid referencing entity UID
	 * @param referencingFieldUid referencing field UID
	 */
	protected ImportObjectKey getForeignKey(ImportStructure importDefinition, Map<UID, Object> attributes, UID referencingEntityUid, UID referencingFieldUid) {
		Map<UID, Object> keyMap = new HashMap<UID, Object>();
		Set<? extends ForeignEntityIdentifier> identifiers = 
				importDefinition.getItems().get(referencingFieldUid).getForeignEntityIdentifiers();
		for (ForeignEntityIdentifier entry : identifiers) {
			keyMap.put(entry.getFieldUID(), attributes.get(entry.getFieldUID()));
		}
		return new ImportObjectKey(keyMap);
	}

	protected ImportProgressNotifier getProcessNotifier() {
		return this.notifier;
	}
	
	protected void initProcessNotifiationBar() {
		if (this.isUseClientNotification() && this.notifier == null) {
			this.notifier = new ImportProgressNotifier(this.getContext().getCorrelationId());		
		}
	}
	
	protected void setProcessNotifiationMessage(String msg) {
		if (this.isUseClientNotification() && this.notifier != null) {
			this.notifier.setMessage(msg);			
		}
	}
	
	protected void setNextStep(int nextstep, int percent) {
		if (this.isUseClientNotification() && this.notifier != null) {
			this.notifier.setNextStep(nextstep, percent);			
		}
	}
	
	protected void increment() {
		if (this.isUseClientNotification() && this.notifier != null) {
			this.notifier.increment();			
		}
	}
	
	protected void startProcessNotifiationBar() {
		initProcessNotifiationBar();
		if (this.isUseClientNotification() && this.notifier != null) {
			this.notifier.start();			
		}
	}

	protected LocaleInfo getLocaleInfoFromContext() {
		if (context == null) {
			return null;
		}

		UID localeId = context.getLocaleId();
		LocaleInfo localeInfo = null;
		for (LocaleInfo locale : localeFacade.getAllLocales(false)) {
			if (locale.getLocale().equals(localeId)) {
				localeInfo = locale;
				break;
			}
		}

		return localeInfo;
	}
	
	protected void setDefaultAttributeValues(ImportObject io, ImportStructure definition) throws CommonBusinessException, ParseException {
		Set<UID> definitionFieldUidSet = new HashSet<>(); 
		for (Item item : definition.getItems().values()) {
			definitionFieldUidSet.add(item.getFieldUID());
		}
		EntityObjectVO<?> eo = io.getValueObject();
		EntityMeta<?> eMeta = metaProvider.getEntity(eo.getDalEntity());
		for (FieldMeta<?> fMeta : eMeta.getFields()) {
			if (!definitionFieldUidSet.contains(fMeta.getUID()) &&	// not in structure definition
				!fMeta.isNullable() && !eo.exist(fMeta.getUID())) {	// mandatory and empty
				entityObjectFacade.setDefaultColumnValue(eo, fMeta);
			}
		}
	}
}