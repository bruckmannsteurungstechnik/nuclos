//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.fileimport;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.searchcondition.TrueCondition;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.fileimport.ImportResult;
import org.nuclos.common2.IdUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.fileimport.FileImportResult;
import org.nuclos.common2.fileimport.NuclosFileImportException;
import org.nuclos.server.common.BusinessIDFactory;
import org.nuclos.server.dal.DalUtils;
import org.nuclos.server.dal.processor.jdbc.impl.ImportObjectProcessor;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * <code>DbImport</code> imports object from a CSV file into the database.
 * Does an import "without executing rules".
 *
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Configurable
public class CsvDbImport extends AbstractImport {

	@Autowired
	private SpringLocaleDelegate localeDelegate;

	private static final Logger LOG = LoggerFactory.getLogger(CsvDbImport.class);

	private Map<UID, ImportObjectProcessor> processors = new HashMap<>();

	public CsvDbImport(GenericObjectDocumentFile file, ImportContext context,
	                   List<CsvImportStructure> structures, ImportLogger logger, boolean atomic) {
		super(file, context, structures, logger, atomic);
	}

	@Override
	public ImportStatus doImport() throws NuclosFileImportException {
		final List<FileImportResult> result = new ArrayList<FileImportResult>();

		initProcessNotifiationBar();
		startProcessNotifiationBar();

		int lineCount = 0;
        try {
        	lineCount = ImportUtils.countLines(getFile(), 
        			((CsvImportStructure) getStructures().get(0)).getDelimiter());
        }
        catch(IOException e1) {
        	LOG.info("doImport: {}", e1);
        	LOG.error("Error counting lines for csv import.", e1);
        	setProcessNotifiationMessage("import.notification.progresscalculationerror");
        }

        try {
	        Double step = (100D / (double) getStructures().size());

	        int i = 0;
			for (final ImportStructure is: getStructures()) {
				final CsvImportStructure importstructure = (CsvImportStructure) is;
				checkInterrupted();

				final UID entityUid = importstructure.getEntityUid();

				EntityMeta<?> meta = metaProvider.getEntity(entityUid);

				setProcessNotifiationMessage(StringUtils.getParameterizedExceptionMessage(
						"import.notification.loadinglookups", meta.getEntityName()));
				setNextStep(getLookupCount(importstructure), ((Double)(i * step.intValue() + step / 2)).intValue() );
				loadLookupsToContext(importstructure);

				setProcessNotifiationMessage(StringUtils.getParameterizedExceptionMessage(
						"import.notification.updating", meta.getEntityName()));
				setNextStep(lineCount, ((Double)(i * step.intValue() + step)).intValue() );

				if (!processors.containsKey(entityUid)) {
					processors.put(entityUid, processorFactorySingleton.newImportObjectProcessor(
							metaProvider.getEntity(importstructure.getEntityUid()),
							metaProvider.getAllEntityFieldsByEntity(importstructure.getEntityUid()).values(), 
							(ImportStructure) importstructure));
				}
				ImportObjectProcessor processor = processors.get(entityUid);

				ImportFileLineIterator lines;
				try {
					lines = new ImportFileLineIterator(getFile(), importstructure.getHeaderLineCount(), importstructure.getDelimiter(), importstructure.getEncoding());
				}
				catch(IOException e) {
					throw new NuclosFileImportException(e);
				}

				while (lines.hasNext()) {
					checkInterrupted();
					String[] asLineValues = null;
					try {
						asLineValues = lines.next();
					}
					catch (RuntimeException ex) {
						String message = StringUtils.getParameterizedExceptionMessage("import.exception.4", getFile().getFilename());
						getLogger().error(lines.getCurrentLine(), message, ex);
						if (isAtomic()) {
							throw new NuclosFileImportException(message, ex);
						}
					}

					try {
						ImportObject io = getObjectFromCsv((ImportStructure) importstructure, asLineValues, lines.getCurrentLine());
						io.setValueObject(ImportUtils.getNewObject(entityUid));

						for (final Item item : importstructure.getItems().values()) {
							if (SF.isEOField(item.getEntityUID(), item.getFieldUID())) {
								continue;
							}

							if (item.isPreserve() && io.getValueObject().getFieldValue(item.getFieldUID()) != null) {
								continue;
							}

							if (item.isReferencing()) {
								ImportObject referenced = io.getReferences().get(item.getFieldUID());
								Object valueId = null;
								Object value = null;
								if (referenced != null && referenced.getValueObject() != null) {
									valueId = referenced.getValueObject().getPrimaryKey();
									
									final FieldMeta<?> fieldmeta = metaProvider.getEntityField(item.getFieldUID());
									value = ImportUtils.getReferenceValue(fieldmeta.getForeignEntityField(), referenced.getValueObject());
								} else if (io.getAttributes().containsKey(item.getFieldUID())) {
									valueId = io.getAttributes().get(item.getFieldUID());
								}
								
								io.getValueObject().setFieldValue(item.getFieldUID(), value);
								if (valueId instanceof UID) {
									io.getValueObject().setFieldUid(item.getFieldUID(), (UID)valueId);
								} else {
									io.getValueObject().setFieldId(item.getFieldUID(), IdUtils.toLongId(valueId));
								}								
							} else {
								io.getValueObject().setFieldValue(item.getFieldUID(), io.getAttributes().get(item.getFieldUID()));
							}
						}

						UID process = null;
						
						// Set process if necessary
						if (io.getAttributes().containsKey(SF.PROCESS.getUID(io.getEntityUID()))) {
							final Object prozess = io.getAttributes().get(SF.PROCESS.getUID(io.getEntityUID()));
							if (getProcessCache().get(entityUid).containsKey(prozess)) {
								process = getProcessCache().get(entityUid).get(prozess);
								io.getValueObject().setFieldUid(SF.PROCESS, process);
							} else {
								io.getValueObject().setFieldUid(SF.PROCESS, null);
							}
						}

						// Set initial state if necessary
						if (meta.isStateModel() && !io.getAttributes().containsKey(SF.STATE.getUID(io.getEntityUID()))) {
							final StateVO statevo = stateFacade.getInitialState(new UsageCriteria(meta.getUID(), process, null, null));
							io.getAttributes().put(SF.STATE.getUID(io.getEntityUID()), statevo.getPrimaryKey());
						}

						// Set state if necessary
						if (meta.isStateModel() && io.getAttributes().containsKey(SF.STATE.getUID(io.getEntityUID()))) {
							final Object status = io.getAttributes().get(SF.STATE.getUID(io.getEntityUID()));
							if (getStateCache().containsKey(entityUid) && !getStateCache().get(entityUid).containsKey(process))
								process = null;
							if (getStateCache().containsKey(entityUid) && getStateCache().get(entityUid).containsKey(process) 
									&& getStateCache().get(entityUid).get(process).containsKey(status)) {
								io.getValueObject().setFieldUid(SF.STATE, getStateCache().get(entityUid).get(process).get(status));
							} else {
								//nuclos can't handle generic objects without a state, so such objects should not be imported
								throw new NuclosFileImportException(StringUtils.getParameterizedExceptionMessage(
										"import.exception.statusnotfound", status,
										metaProvider.getEntity(entityUid).getEntityName()));
							}
						}

						// generate system identifier:
						if (meta.isStateModel()) {
							final EntityMeta<?> eMeta = modules.getModule(importstructure.getEntityUid());
							final String sCanonicalValueSystemIdentifier = BusinessIDFactory.generateSystemIdentifier(eMeta);
							io.getValueObject().setFieldValue(SF.SYSTEMIDENTIFIER, sCanonicalValueSystemIdentifier);
						}

						EntityObjectVO<UID> eo = io.getValueObject();
						eo.setFieldValue(SF.LOGICALDELETED, false);
						DalUtils.updateVersionInformation(eo, "import");

						processor.insertOrUpdate(eo);
					}
					catch (Exception ex) {
						getLogger().error(lines.getCurrentLine(), "import.exception.dbimport", ex);
						if (isAtomic()) {
							throw new NuclosFileImportException("import.notification.error");
						}
					}
					finally {
						increment();
					}
				}
				i++;
			}

			for (final Map.Entry<UID, ImportObjectProcessor> processor : processors.entrySet()) {
				result.add(new FileImportResult(processor.getKey(), processor.getValue().getInserted(), processor.getValue().getUpdated(), 0));
			}

			StringBuilder summary = new StringBuilder();
			if (getLogger().getErrorcount() > 0) {
				summary.append(localeDelegate.getMessage("import.logging.errors", "import.logging.errors", getLogger().getErrorcount()) + System.getProperty("line.separator"));
			}
			for (FileImportResult fir : result) {
				summary.append(localeDelegate.getMessage("import.logging.result", "import.logging.result",
						metaProvider.getEntity(fir.getEntity()), fir.getInserted(), fir.getUpdated(), fir.getDeleted()) + System.getProperty("line.separator"));
			}
			
			//this.notifier.finish();

			return new ImportStatusImpl(getProcessNotifier(), getLogger().getErrorcount() > 0 ? ImportResult.INCOMPLETE : ImportResult.OK, summary.toString());
        } catch (NuclosFileImportException ex) {
        	/*if (this.notifier != null) {
				this.notifier.stop(ex.getMessage());
			}*/
        	getLogger().error(ex.getMessage(), ex);
        	return new ImportStatusImpl(getProcessNotifier(), ImportResult.ERROR, ex.getMessage());
        } catch (Exception ex) {
        	/*if (this.notifier != null) {
				this.notifier.stop(ex.getMessage());
			}*/
        	getLogger().error(ex.getMessage(), ex);
        	return new ImportStatusImpl(getProcessNotifier(), ImportResult.ERROR, ex.getMessage());
        }
	}

	private int getLookupCount(CsvImportStructure structure) {
		try {
			int result = 0;
			for (Item item : structure.getItems().values()) {
				if (item.isReferencing()) {
					IEntityObjectProcessor eoProcessor = nucletDalProvider.getEntityObjectProcessor(item.getForeignEntityUID());

					result += eoProcessor.count(new CollectableSearchExpression(TrueCondition.TRUE));
				}
			}
			return result;
		}
		catch (Exception ex) {
			throw new NuclosFatalException(ex);
		}
	}

	private void loadLookupsToContext(CsvImportStructure structure) {
		try {
			for (Item item : structure.getItems().values()) {
				if (item.isReferencing()) {
					IEntityObjectProcessor eoProcessor = nucletDalProvider.getEntityObjectProcessor(item.getForeignEntityUID());

					List<UID> fieldUids = new ArrayList<UID>();
                    for (ForeignEntityIdentifier feid : item.getForeignEntityIdentifiers()) fieldUids.add(feid.getFieldUID());

                    List<EntityObjectVO> objects = eoProcessor.getBySearchExprResultParams(CollectableSearchExpression.TRUE_SEARCH_EXPR, new ResultParams(fieldUids));

					for (EntityObjectVO object : objects) {
						final Map<UID, Object> foreignkey = new HashMap<UID, Object>(1);

						for (ForeignEntityIdentifier feid : item.getForeignEntityIdentifiers()) {
							foreignkey.put(feid.getFieldUID(), object.getFieldValue(feid.getFieldUID()));
						}

						final ImportObject io = new ImportObject(item.getForeignEntityUID(), new ImportObjectKey(foreignkey), foreignkey, -1, new HashMap<UID, ImportObject>());
						io.setValueObject(object);
						putObject(io);
						increment();
					}
				}
			}
		}
		catch (Exception ex) {
			throw new NuclosFatalException(ex);
		}
	}
	
}