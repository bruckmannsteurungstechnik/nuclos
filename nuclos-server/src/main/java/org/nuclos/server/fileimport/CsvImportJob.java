//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.fileimport;

import java.util.Locale;

import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.fileimport.NuclosFileImportException;
import org.nuclos.server.common.ejb3.LocaleFacadeLocal;
import org.nuclos.server.fileimport.ejb3.CsvImportFacadeLocal;
import org.nuclos.server.job.NuclosInterruptableJob;
import org.quartz.InterruptableJob;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.UnableToInterruptJobException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.i18n.LocaleContextHolder;

/**
 * Implementation of a interruptable quartz job that can be started by nuclos.
 *
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
public class CsvImportJob extends NuclosInterruptableJob {

	public CsvImportJob() {
		super(new CsvImportJobImpl());
	}

	@Configurable
	private static class CsvImportJobImpl implements InterruptableJob {

		private static final Logger LOG = LoggerFactory.getLogger(CsvImportJobImpl.class);

		private ImportContext context;

		// Spring injection

		@Autowired
		private CsvImportFacadeLocal importFacade;

		// end of Spring injection

		public CsvImportJobImpl() {
		}

		@Override
		public void execute(JobExecutionContext context) throws JobExecutionException {
			String sId = context.getJobDetail().getKey().getName();
			UID importFileUid = UID.parseUID(sId);
			String correlationId = context.getJobDetail().getJobDataMap().getString("ProcessId");
			UID localeId = UID.parseUID((String) context.getJobDetail().getJobDataMap().get("LocaleId"));
			String username = context.getJobDetail().getJobDataMap().getString("User");

			/* set locale in spring's localeContext to locale of the ImportContext to ensure usage of correct
			 * locale when using SpringLocaleDelegate directly */
			Locale localeBefore = LocaleContextHolder.getLocale();
			LocaleInfo info = null;
			LocaleFacadeLocal localeFacade = SpringApplicationContextHolder.getBean(LocaleFacadeLocal.class);
			for (LocaleInfo i : localeFacade.getAllLocales(true)) {
				if (i.getLocale().equals(localeId)) {
					info = i;
				}
			}
			if (info != null) {
				LocaleContextHolder.setLocale(info.toLocale());
			}

			try {
				this.context = new ImportContext(importFileUid, correlationId, localeId, username);
			} catch (Exception ex) {
				LOG.error("csv object import job could not be started.", ex);
			}
			try {
				importFacade.doImport(this.context);
			} catch (NuclosFileImportException ex) {
				LOG.error("csv import job {} terminated.", sId, ex);
			}
			LocaleContextHolder.setLocale(localeBefore);
		}

		@Override
		public void interrupt() throws UnableToInterruptJobException {
			if (this.context != null) {
				this.context.interrupt();
			} else {
				throw new UnableToInterruptJobException("import.not.running");
			}
		}

	}
}
