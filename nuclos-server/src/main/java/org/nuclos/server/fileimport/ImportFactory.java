//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.fileimport;

import java.util.List;

import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.fileimport.ImportMode;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.fileimport.NuclosFileImportException;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;

public class ImportFactory {

	/**
	 * Create a new import instance by checking the importfile's mode.
	 *
	 * @param importfileId Id of file to import.
	 * @param correlationId CorrelationId to notify clients, if any.
	 * @return Instance of <code>AbstractImport</code>
	 * @throws NuclosFileImportException
	 */
	public static Import newCsvImport(ImportMode mode, GenericObjectDocumentFile file, ImportContext context,
			List<CsvImportStructure> structures, ImportLogger logger, boolean atomic) throws NuclosFileImportException {
		Import instance;
		if (ImportMode.NUCLOSIMPORT.equals(mode)) {
			instance = new CsvNuclosImport(file, context, structures, logger, atomic);
		}
		else if (ImportMode.DBIMPORT.equals(mode)) {
			instance = new CsvDbImport(file, context, structures, logger, atomic);
		}
		else {
			throw new NuclosFatalException(StringUtils.getParameterizedExceptionMessage(
					"import.exception.modenotimplemented", mode.toString()));
		}
		return instance;
	}

	/**
	 * Create a new import instance by checking the importfile's mode.
	 *
	 * @param importfileId Id of file to import.
	 * @param correlationId CorrelationId to notify clients, if any.
	 * @return Instance of <code>AbstractImport</code>
	 * @throws NuclosFileImportException
	 */
	public static Import newXmlImport(ImportMode mode, GenericObjectDocumentFile file, ImportContext context,
			List<XmlImportStructure> structures, ImportLogger logger, boolean atomic) throws NuclosFileImportException {
		Import instance;
		if (ImportMode.NUCLOSIMPORT.equals(mode)) {
			instance = new XmlNuclosImport(file, context, structures, logger, atomic);
		}
		else if (ImportMode.DBIMPORT.equals(mode)) {
			instance = new XmlDbImport(file, context, structures, logger, atomic);
		}
		else {
			throw new NuclosFatalException(StringUtils.getParameterizedExceptionMessage(
					"import.exception.modenotimplemented", mode.toString()));
		}
		return instance;
	}

}
