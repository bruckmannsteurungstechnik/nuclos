package org.nuclos.server.fileimport;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;

import org.nuclos.common.E;
import org.nuclos.common.NuclosEntityValidator;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.common.INucletCache;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorUtils;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.nbo.AbstractNuclosObjectCompiler.NuclosBusinessJavaSource;
import org.nuclos.server.nbo.NuclosObjectBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Matthias Reichert
 * @author Thomas Pasch
 */
@Component
public class ImportStructureDefinitionBuilder extends NuclosObjectBuilder {

	private static final String DEFFAULT_PACKAGE_NUCLET = "org.nuclet.importstructure";
	private static final String DEFFAULT_IMPORT_PREFIX = "ISD";
	private static final String DEFFAULT_IMPORT_POSTFIX = "ISD";

	@Autowired
	private MetaProvider provider;

	@Autowired
	private ImportStructureDefinitionCompiler compiler;

	@Autowired
	private NucletDalProvider nucletDalProvider;


	public void createObjects(final ForkJoinPool builderThreadPool) throws NuclosCompileException, InterruptedException {
		final List<NuclosBusinessJavaSource> retVal = new ArrayList<>();
		createImportStructureDefinitions(retVal);
		createXmlImportStructureDefinitions(retVal);
		if (retVal.size() > 0) {
			compiler.compileSourcesAndJar(builderThreadPool, retVal);
		}
	}

	private void createImportStructureDefinitions(List<NuclosBusinessJavaSource> retVal)
		throws NuclosCompileException, InterruptedException {
		// get all import structure definitions
		final List<EntityObjectVO<UID>> allISDs =
			nucletDalProvider.getEntityObjectProcessor(E.IMPORT).getAll();

		if (allISDs != null) {
			for (EntityObjectVO<UID> eoVO : allISDs) {
				if (Thread.currentThread().isInterrupted()) {
					throw new InterruptedException();
				}
				retVal.add(
					createImportStructureSourceFile(eoVO.getPrimaryKey(),
					                                eoVO.getFieldValue(E.IMPORT.name),
					                                eoVO.getFieldUid(E.IMPORT.nuclet)));
			}
		}
	}

	private void createXmlImportStructureDefinitions(List<NuclosBusinessJavaSource> retVal)
		throws NuclosCompileException, InterruptedException {
		// get all import structure definitions
		final List<EntityObjectVO<UID>>
			allISDs =
			nucletDalProvider.getEntityObjectProcessor(E.XML_IMPORT).getAll();

		if (allISDs != null) {
			for (EntityObjectVO<UID> eoVO : allISDs) {
				if (Thread.currentThread().isInterrupted()) {
					throw new InterruptedException();
				}
				retVal.add(
					createXmlImportStructureSourceFile(eoVO.getPrimaryKey(),
					                                   eoVO.getFieldValue(E.XML_IMPORT.name),
					                                   eoVO.getFieldUid(E.XML_IMPORT.nuclet)));
			}
		}
	}

	private NuclosBusinessJavaSource createImportStructureSourceFile(UID id, String name,
	                                                                 UID nucletUID) {
		final String sPackage = getNucletPackageStatic(nucletUID, provider);
		final String unformatedEntity = NuclosEntityValidator.escapeJavaIdentifier(name, DEFFAULT_IMPORT_PREFIX);
		final String formatEntity =
			unformatedEntity.substring(0, 1).toUpperCase() + unformatedEntity.substring(1)
			+ DEFFAULT_IMPORT_POSTFIX;
		final String qname = sPackage + "." + formatEntity;
		final String filename =
			NuclosCodegeneratorUtils.importStructureDefinitionSource(sPackage, formatEntity)
				.toString();
		final String content = createImportStructureSourceContent(sPackage, id, formatEntity);

		return new NuclosBusinessJavaSource(qname, filename, content, true);
	}

	private String createImportStructureSourceContent(String sPackage, UID id,
	                                                  String formatEntity) {
		StringBuilder s = new StringBuilder();
		s
			.append("package " + sPackage + ";\n\n")

			.append("import org.nuclos.api.objectimport.ImportStructureDefinition;\n")
			.append("import ").append(UID.class.getCanonicalName()).append(";\n")

			.append("public class ").append(formatEntity).append(
			" implements ImportStructureDefinition {\n\n")

			.append("public org.nuclos.api.UID getId() {\n\treturn UID.parseUID(\"")
			.append(id.getString()).append("\");\n}");

		s.append("\n\n}");
		return s.toString();
	}

	public static String getNucletPackageStatic(UID nucletUID, INucletCache nucletCache) {
		String retVal = DEFFAULT_PACKAGE_NUCLET;

		if (nucletUID != null) {
			String sFqn = nucletCache.getFullQualifiedNucletName(nucletUID);
			if (!StringUtils.looksEmpty(sFqn)) {
				retVal = sFqn;
			}
		}

		return retVal;
	}

	private NuclosBusinessJavaSource createXmlImportStructureSourceFile(UID id, String name,
	                                                                    UID nucletUID) {
		final String sPackage = getNucletPackageStatic(nucletUID, provider);
		final String unformatedEntity = NuclosEntityValidator.escapeJavaIdentifier(name, DEFFAULT_IMPORT_PREFIX);
		final String
			formatEntity =
			unformatedEntity.substring(0, 1).toUpperCase() + unformatedEntity.substring(1)
			+ DEFFAULT_IMPORT_POSTFIX;
		final String qname = sPackage + "." + formatEntity;
		final String
			filename =
			NuclosCodegeneratorUtils.importStructureDefinitionSource(sPackage, formatEntity)
				.toString();
		final String content = createXmlImportStructureSourceContent(sPackage, id, formatEntity);

		return new NuclosBusinessJavaSource(qname, filename, content, true);
	}

	private String createXmlImportStructureSourceContent(String sPackage, UID id,
	                                                     String formatEntity) {
		StringBuilder s = new StringBuilder();
		s
			.append("package ").append(sPackage).append(";\n\n")

			.append("import org.nuclos.api.objectimport.XmlImportStructureDefinition;\n")
			.append("import ").append(UID.class.getCanonicalName()).append(";\n")

			.append("public class ").append(formatEntity)
			.append(" implements XmlImportStructureDefinition {\n\n")

			.append("public org.nuclos.api.UID getId() {\n\treturn UID.parseUID(\"")
			.append(id.getString()).append(
			"\");\n}")

			.append("\n\n}");
		return s.toString();
	}

}
