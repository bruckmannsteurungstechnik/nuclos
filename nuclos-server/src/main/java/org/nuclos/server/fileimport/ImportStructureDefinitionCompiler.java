package org.nuclos.server.fileimport;

import org.nuclos.api.ide.valueobject.SourceType;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants;
import org.nuclos.server.nbo.AbstractNuclosObjectCompiler;
import org.springframework.stereotype.Component;

@Component
public class ImportStructureDefinitionCompiler extends AbstractNuclosObjectCompiler {


	public ImportStructureDefinitionCompiler() {
		super(SourceType.IMPORTSTRUCTUREDEFINITIONS,
		      NuclosCodegeneratorConstants.IMPORTSTRUCTUREDEFSJARFILE,
		      NuclosCodegeneratorConstants.IMPORTSTRUCTUREDEFS_SRC_DIR_NAME);
	}

}
