//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.fileimport;

import java.util.Set;

import org.nuclos.common.NuclosScript;
import org.nuclos.common.UID;
import org.nuclos.common2.fileimport.NuclosFileImportException;

interface Item {

	UID getFieldUID();

	UID getEntityUID();

	boolean isPreserve();

	boolean isReferencing();

	UID getForeignEntityUID();

	Set<? extends ForeignEntityIdentifier> getForeignEntityIdentifiers();

	/**
	 * Parse the input String according to the given format/parsestring to the (typed) result object.
	 * <p>
	 * At least the CSV implementation of this delegates to 
	 * {@link org.nuclos.common2.fileimport.parser.CsvFileImportParserFactory}.
	 * 
	 * @see http://wiki.nuclos.de/display/Konfiguration/Import+von+Daten+aus+CSV+Dateien
	 */
	Object parse(String sValue) throws NuclosFileImportException;

	NuclosScript getScript();

	Class<?> getCls();
}
