//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.fileimport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.DalSupportForMD;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.fileimport.ImportResult;
import org.nuclos.common2.IdUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.fileimport.FileImportResult;
import org.nuclos.common2.fileimport.NuclosFileImportException;
import org.nuclos.server.dal.DalSupportForGO;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.genericobject.valueobject.GenericObjectVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * <code>NuclosImport</code> imports object from a CSV file into nuclos by using facades.
 * Does an import "with executing rules".
 *
 * This class reads the file, merges file and database data and finally performs the update of the database.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Configurable
public class XmlNuclosImport extends AbstractImport {

	private static final Logger LOG = LoggerFactory.getLogger(XmlNuclosImport.class);

	/**
	 * Stores all existing object id's of all imported entities to find out the
	 * data that should be deleted.
	 */
	private Map<UID, List<Long>> dataToDelete = new HashMap<UID, List<Long>>();

	public XmlNuclosImport(GenericObjectDocumentFile file, ImportContext context, List<XmlImportStructure> structures, ImportLogger logger, boolean atomic) {
		super(file, context, (List<? extends ImportStructure>) structures, logger, atomic);
	}

	/**
	 * Import file with given filename.
	 */
	@Override
	public ImportStatus doImport() throws NuclosFileImportException {
		try {
			final List<FileImportResult> result = new ArrayList<FileImportResult>();

			initProcessNotifiationBar();
			startProcessNotifiationBar();

			int i = 0;
			setNextStep(getFile().getContents().length * getStructures().size(), 20);

			for (ImportStructure is : getStructures()) {
				final XmlImportStructure importstructure = (XmlImportStructure) is;
				setProcessNotifiationMessage(StringUtils.getParameterizedExceptionMessage(
						"import.notification.parsing", importstructure.getName()));
				
				final Set<String> innerMatch = new HashSet<String>();
				String outerMatch = null;
				for (Item item: importstructure.getItems().values()) {
					final XmlItem it = (XmlItem) item;
					final String im = it.getMatch();
					final Pair<Integer,String> levelAndPath = SimpleXPathUtils.getParentLevelAndPath(im);
					if (levelAndPath == null) {
						innerMatch.add(it.getMatch());
					} else {
						if (outerMatch == null) {
							outerMatch = im;
						} else {
							throw new IllegalStateException("Only ONE outer match is supported, but there is " + 
									im + " and " + outerMatch);
						}
					}
				}

				for (XmlParseObject po: SimpleXPathUtils.xml(getFile().getContents(), importstructure.getMatch(),
						innerMatch, outerMatch)) {
					processBo(importstructure, po.getEntityMap(), po.getLocation().getLineNumber());
					// i = po.getLocation().getCharacterOffset();
				}
			}

			i = 0;
			for (final Map.Entry<UID, Map<ImportObjectKey, ImportObject>> entity : getData().entrySet()) {
				i += entity.getValue().size();
			}
			setNextStep(i, 50);

			merge();

			i = 0;
			for (final UID entityUid : getImportedEntities()) {
				i += getInsertOrUpdateCount(entityUid);
				i += dataToDelete.get(entityUid).size();
			}
			setNextStep(i, 100);

			for (final UID entityUid : getImportedEntities()) {
				setProcessNotifiationMessage(StringUtils.getParameterizedExceptionMessage(
						"import.notification.updating", metaProvider.getEntity(entityUid).getEntityName()));
				setAttributeValues(entityUid);
				result.add(save(entityUid));

			}

			StringBuilder summary = new StringBuilder();
			if (getLogger().getErrorcount() > 0) {
				summary.append(StringUtils.getParameterizedExceptionMessage("import.logging.errors",
						getLogger().getErrorcount()) + System.getProperty("line.separator"));
			}
			for (FileImportResult fir : result) {
				summary.append(StringUtils.getParameterizedExceptionMessage("import.logging.result",
						metaProvider.getEntity(fir.getEntity()).getEntityName(), fir.getInserted(),
						fir.getUpdated(), fir.getDeleted())
						+ System.getProperty("line.separator"));
			}

			//this.notifier.finish();

			return new ImportStatusImpl(getProcessNotifier(), getLogger().getErrorcount() > 0 ? ImportResult.INCOMPLETE
					: ImportResult.OK, summary.toString());
		} catch (NuclosFileImportException ex) {
			/*if (this.notifier != null) {
				this.notifier.stop(ex.getMessage());
			}*/
			getLogger().error(ex.getMessage(), ex);
			return new ImportStatusImpl(getProcessNotifier(), ImportResult.ERROR, ex.getMessage());
		} catch (Exception ex) {
			/*if (this.notifier != null) {
				this.notifier.stop(ex.getMessage());
			}*/
			getLogger().error(ex.getMessage(), ex);
			return new ImportStatusImpl(getProcessNotifier(), ImportResult.ERROR, ex.getMessage());
		}
	}

	private void processBo(ImportStructure importstructure,
			Map<String,String> entityMap, int line) throws NuclosFileImportException {
		
		checkInterrupted();
		try {
			putObject(getObjectFromXml(importstructure, entityMap, line));
		} catch (Exception ex) {
			LOG.error("Unable to put object:", ex);

			getLogger().error(line, "import.exception.read", ex);
			if (isAtomic()) {
				throw new NuclosFileImportException("import.notification.error");
			}
		}
		increment();
	}
	
	private int getInsertOrUpdateCount(final UID entityUid) {
		int i = 0;
		for (ImportObject io : getData().get(entityUid).values()) {
			if (io.getValueObject() != null) {
				if ((getImportSettings().get(entityUid) & CsvImportStructure.INSERT) == CsvImportStructure.INSERT
						&& io.getValueObject().getPrimaryKey() == null) {
					i++;
				}
				else if ((getImportSettings().get(entityUid) & CsvImportStructure.UPDATE) == CsvImportStructure.UPDATE
						&& io.getValueObject().getPrimaryKey() != null) {
					i++;
				}
			}
		}
		return i;
	}

	/**
	 * Merge complete import data with persisted data in memory
	 */
	public void merge() throws NuclosFileImportException {
		final List<UID> merged = new ArrayList<UID>();
		for (final UID entityUid : getImportedEntities()) {
			mergeEntity(entityUid, merged);
		}
	}

	/**
	 * Merge entity with persisted data
	 *
	 * @param entityUid name of entity
	 * @param lstMergedEntityUids tracking already merged entities
	 */
	private void mergeEntity(final UID entityUid, final List<UID> lstMergedEntityUids) throws NuclosFileImportException {
		Map<ImportObjectKey, ImportObject> entity = getData().get(entityUid);
		lstMergedEntityUids.add(entityUid);

		if (getImportedEntities().contains(entityUid)) {
			for (ImportStructure structure : getStructures()) {
				for (Item item : structure.getItems().values()) {
					if (item.isReferencing() && !lstMergedEntityUids.contains(item.getForeignEntityUID())) {
						mergeEntity(item.getForeignEntityUID(), lstMergedEntityUids);
					}
				}
			}
		}

		setProcessNotifiationMessage(StringUtils.getParameterizedExceptionMessage("import.notification.merging",
				metaProvider.getEntity(entityUid).getEntityName()));

		if (getImportedEntities().contains(entityUid)) {
			// do not merge if no primary key combination is defined
			if (getKeyDefinitions().get(entityUid).size() == 0
					&& (getImportSettings().get(entityUid) & CsvImportStructure.INSERT) == CsvImportStructure.INSERT) {
				for (ImportObject io : entity.values()) {
					checkInterrupted();
					io.setValueObject(ImportUtils.getNewObject(entityUid));
					increment();
				}
				this.dataToDelete.put(entityUid, new ArrayList<Long>());
			}
			else {
				// Check import settings
				List<Long> objectsToDelete = new ArrayList<Long>();
				if ((getImportSettings().get(entityUid) & CsvImportStructure.DELETE) == CsvImportStructure.DELETE) {
					objectsToDelete.addAll(ImportUtils.getObjectIdsByEntity(entityUid));
				}

				for (Map.Entry<ImportObjectKey, ImportObject> object : entity.entrySet()) {
					checkInterrupted();
					ImportObject io = object.getValue();
					EntityObjectVO<Long> vo = null;
					try {
						vo = getExistingVO(entityUid, getKeyDefinitions(), io);
					} catch (Exception ex) {
						getLogger().error(io.getLineNumber(), ex.getMessage(), ex);
						if (isAtomic()) {
							throw new NuclosFileImportException("import.notification.error");
						}
					}

					if (vo == null) {
						if ((getImportSettings().get(entityUid) & CsvImportStructure.INSERT) == CsvImportStructure.INSERT) {
							io.setValueObject(ImportUtils.getNewObject(entityUid));
						}
					}
					else {
						io.setValueObject(vo);
						if ((getImportSettings().get(entityUid) & CsvImportStructure.DELETE) == CsvImportStructure.DELETE) {
							objectsToDelete.remove(vo.getPrimaryKey());
						}
					}
					increment();
				}
				this.dataToDelete.put(entityUid, objectsToDelete);
			}
		}
		else {
			for (Map.Entry<ImportObjectKey, ImportObject> object : entity.entrySet()) {
				checkInterrupted();
				try {
					EntityObjectVO<Long> vo = getExistingVO(entityUid, getKeyDefinitions(), object.getValue());
					if (vo == null) {
						throw new NuclosFileImportException(StringUtils.getParameterizedExceptionMessage(
								"import.exception.referencednotfound", object.getValue().toString(),
								metaProvider.getEntity(entityUid).getEntityName()));
					}
					object.getValue().setValueObject(vo);
					increment();
				} catch (NuclosFileImportException ex) {
					getLogger().error(object.getValue().getLineNumber(), ex.getMessage(), ex);
					if (isAtomic()) {
						throw new NuclosFileImportException("import.notification.error");
					}
				}
			}
		}
	}

	/**
	 * Merge imported attribute values into existing or new objects
	 *
	 * @param entityUid of entity to merge
	 */
	private void setAttributeValues(final UID entityUid) throws NuclosFileImportException {
		for (ImportObject io : getData().get(entityUid).values()) {
			checkInterrupted();
			if (io.getValueObject() == null) {
				continue;
			}
			try {
				for (ImportStructure definition : getStructures()) {
					if (definition.getEntityUid().equals(entityUid)) {
						for (Item item : definition.getItems().values()) {
							if (SF.isEOField(definition.getEntityUid(), item.getFieldUID())) {
								continue;
							}

							if (item.isPreserve() && io.getValueObject().getFieldValue(item.getFieldUID()) != null) {
								continue;
							}

							if (item.isReferencing()) {
								ImportObject referenced = io.getReferences().get(item.getFieldUID());
								Object valueId = null;
								Object value = null;
								if (referenced != null) {
									if (referenced.getValueObject() == null
											|| referenced.getValueObject().getPrimaryKey() == null) {
										getLogger().error(
												io.getLineNumber(),
												StringUtils.getParameterizedExceptionMessage(
														"import.exception.referencedobjectnotimported",
														referenced.getKey(),
														metaProvider.getEntity(referenced.getEntityUID()).getEntityName()));
										if (isAtomic()) {
											throw new NuclosFileImportException("import.notification.error");
										}
									} else {
										valueId = referenced.getValueObject().getPrimaryKey();

										final FieldMeta<?> fieldmeta = metaProvider.getEntityField(
												item.getFieldUID());
										value = ImportUtils.getReferenceValue(fieldmeta.getForeignEntityField(),
												referenced.getValueObject());
									}
								} else if (io.getAttributes().containsKey(item.getFieldUID())) {
									valueId = (Long) io.getAttributes().get(item.getFieldUID());
								}

								io.getValueObject().setFieldValue(item.getFieldUID(), value);
								if (valueId instanceof UID) {
									io.getValueObject().setFieldUid(item.getFieldUID(), (UID) valueId);
								} else {
									io.getValueObject().setFieldId(item.getFieldUID(), IdUtils.toLongId(valueId));
								}
							} else {
								io.getValueObject().setFieldValue(item.getFieldUID(),
										io.getAttributes().get(item.getFieldUID()));
							}
						}
						
						setDefaultAttributeValues(io, definition);

						// Set process if necessary
						if (io.getAttributes().containsKey(SF.PROCESS.getUID(io.getEntityUID()))) {
							final Object sProcess = io.getAttributes().get(SF.PROCESS.getUID(io.getEntityUID()));
							if (getProcessCache().get(entityUid).containsKey(sProcess)) {
								final UID processUid = getProcessCache().get(entityUid).get(sProcess);
								io.getValueObject().setFieldUid(SF.PROCESS.getUID(entityUid), processUid);
								io.getValueObject().setFieldValue(SF.PROCESS.getUID(entityUid), sProcess);
							} else {
								io.getValueObject().setFieldUid(SF.PROCESS.getUID(entityUid), null);
							}
						}
					}
				}
			} catch (Exception ex) {
				getLogger().error(io.getLineNumber(), ex.getMessage(), ex);
				if (isAtomic()) {
					throw new NuclosFileImportException("import.notification.error");
				}
			}
		}
	}

	EntityObjectVO<Long> getExistingVO(final UID entityUid, final Map<UID, Set<UID>> definitions,
			final ImportObject io) throws NuclosFileImportException {
		try {
			final CollectableSearchCondition condition = ImportUtils.getSearchCondition(entityUid, definitions, io);

			final List<EntityObjectVO<Long>> foundobjects = nucletDalProvider.<Long> getEntityObjectProcessor(
					entityUid).getBySearchExpression(new CollectableSearchExpression(condition));

			if (foundobjects == null || foundobjects.size() == 0) {
				return null;
			}
			else if (foundobjects.size() > 1) {
				throw new NuclosFileImportException(StringUtils.getParameterizedExceptionMessage(
						"import.exception.duplicatesfound", io,
						metaProvider.getEntity(entityUid).getEntityName()));
			}
			else {
				return foundobjects.get(0);
			}
		} catch (NuclosBusinessException ex) {
			throw new NuclosFileImportException(ex);
		}
	}

	/**
	 * Update database.
	 *
	 * @param entityUid entity to update
	 */
	private FileImportResult save(final UID entityUid) throws NuclosFileImportException {
		int inserted = 0;
		int updated = 0;
		int deleted = 0;

		for (Long id : dataToDelete.get(entityUid)) {
			checkInterrupted();
			try {
				deleteObject(entityUid, id);
				deleted++;
			} catch (Exception ex) {
				String message = StringUtils.getParameterizedExceptionMessage("import.exception.delete", id,
						metaProvider.getEntity(entityUid).getEntityName());
				getLogger().error(message, ex);
				if (isAtomic()) {
					throw new NuclosFileImportException("import.notification.error");
				}
			}
			increment();
		}

		for (ImportObject io : getData().get(entityUid).values()) {
			checkInterrupted();
			if (io.getValueObject() == null) {
				continue;
			}
			if (io.getValueObject().getPrimaryKey() == null
					&& (getImportSettings().get(entityUid) & CsvImportStructure.INSERT) == CsvImportStructure.INSERT) {
				try {
					io.setValueObject(insertObject(entityUid, io.getValueObject()));
					inserted++;
				} catch (Exception ex) {
					String message = StringUtils.getParameterizedExceptionMessage("import.exception.insert",
							io.toString(), metaProvider.getEntity(entityUid).getEntityName());
					getLogger().error(io.getLineNumber(), message, ex);
					if (ex.getCause() != null && ex.getCause() instanceof CommonValidationException) {
						CommonValidationException cve = (CommonValidationException)ex.getCause();
						getLogger().error(io.getLineNumber(), cve.toString(), cve);
					}
					if (isAtomic()) {
						throw new NuclosFileImportException("import.notification.error");
					}
				}
			}
			else if (io.getValueObject().getPrimaryKey() != null
					&& (getImportSettings().get(entityUid) & CsvImportStructure.UPDATE) == CsvImportStructure.UPDATE) {
				try {
					io.setValueObject(updateObject(entityUid, io.getValueObject()));
					updated++;
				} catch (Exception ex) {
					String message = StringUtils.getParameterizedExceptionMessage("import.exception.update",
							io.toString(), metaProvider.getEntity(entityUid).getEntityName());
					getLogger().error(io.getLineNumber(), message, ex);
					if (ex.getCause() != null && ex.getCause() instanceof CommonValidationException) {
						CommonValidationException cve = (CommonValidationException)ex.getCause();
						getLogger().error(io.getLineNumber(), cve.toString(), cve);
					}
					if (isAtomic()) {
						throw new NuclosFileImportException("import.notification.error");
					}
				}
			}
			increment();
		}
		return new FileImportResult(entityUid, inserted, updated, deleted);
	}

	public EntityObjectVO<Long> insertObject(final UID entityUid, final EntityObjectVO vo) throws Exception {
		if (metaProvider.getEntity(entityUid).isStateModel()) {
			GenericObjectVO go = DalSupportForGO.getGenericObjectVO(vo);
			go = genericObjectFacade.create(new GenericObjectWithDependantsVO(go, new DependentDataMap(), null),
					serverParameterProvider.getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
			return DalSupportForGO.wrapGenericObjectVO(go);
		}
		else {
			MasterDataVO<Long> md = DalSupportForMD.wrapEntityObjectVO(vo);
			md = masterDataFacade.create(md,
					serverParameterProvider.getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
			final EntityObjectVO result = md.getEntityObject();
			return result;
		}
	}

	public EntityObjectVO<Long> updateObject(final UID entityUid, final EntityObjectVO vo) throws Exception {
		if (metaProvider.getEntity(entityUid).isStateModel()) {
			GenericObjectVO go = DalSupportForGO.getGenericObjectVO(vo);
			go = genericObjectFacade.modify(new GenericObjectWithDependantsVO(go, new DependentDataMap(), null),
					serverParameterProvider.getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
			return DalSupportForGO.wrapGenericObjectVO(go);
		}
		else {
			MasterDataVO<Long> md = DalSupportForMD.wrapEntityObjectVO(vo);
			md.setPrimaryKey((Long) masterDataFacade.modify(md,
					serverParameterProvider.getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY)));
			final EntityObjectVO<Long> result = md.getEntityObject();
			return result;
		}
	}

	public void deleteObject(final UID entityUid, final Long id) throws Exception {
		if (modules.isModule(entityUid)) {
			genericObjectFacade.remove(entityUid, id, true,
					serverParameterProvider.getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
		}
		else {
			masterDataFacade.remove(entityUid, id, false,
					serverParameterProvider.getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
		}
	}
}
