//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.fileimport.ejb3;

import java.util.List;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.ejb3.LocaleFacadeLocal;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.report.NuclosQuartzJob;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Facade for managing and executing imports.
 * Takes care of executing an import with respect to its transaction settings.
 *
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
abstract class AbstractImportFacadeBean extends NuclosFacadeBean implements ImportFacadeRemote {
	
	private static final Logger LOG = LoggerFactory.getLogger(AbstractImportFacadeBean.class);
	
	// Spring injection
	
	@Autowired
	Scheduler nuclosScheduler;
	
	@Autowired
	MasterDataFacadeLocal masterDataFacade;
	
	@Autowired
	LocaleFacadeLocal localeFacade;
	
	// end of Spring injection
	
	AbstractImportFacadeBean() {
	}
	
	abstract EntityMeta<UID> getImportStructureEntityMeta();
	
	abstract EntityMeta<UID> getImportFileEntityMeta();
	
	abstract void validateImportStructure(MasterDataVO<UID> importStructure) throws CommonValidationException;
	
	abstract void validateFileImport(MasterDataVO<UID> fileImport) throws CommonValidationException;
	
	@Override
	public MasterDataVO<UID> createImportStructure(MasterDataVO<UID> importStructure) throws CommonBusinessException {
		checkWriteAllowed(getImportStructureEntityMeta());
		validateImportStructure(importStructure);

		return masterDataFacade.create(importStructure, null);
	}

	@Override
	public Object modifyImportStructure(MasterDataVO<UID> importStructure) throws CommonBusinessException {
		checkWriteAllowed(getImportStructureEntityMeta());
		validateImportStructure(importStructure);

		return masterDataFacade.modify(importStructure, null);
	}

	@Override
	public void removeImportStructure(MasterDataVO<UID> importStructure) throws CommonBusinessException {
		checkDeleteAllowed(getImportStructureEntityMeta());

		masterDataFacade.remove(importStructure.getEntityObject().getDalEntity(), importStructure.getPrimaryKey(), true, null);
	}

	@Override
	public MasterDataVO<UID> createFileImport(MasterDataVO<UID> fileImport) throws CommonBusinessException {
		checkWriteAllowed(getImportFileEntityMeta());
		validateFileImport(fileImport);

		return masterDataFacade.create(fileImport, null);
	}

	@Override
	public Object modifyFileImport(MasterDataVO<UID> fileImport) throws CommonBusinessException {
		checkWriteAllowed(getImportFileEntityMeta());
		validateFileImport(fileImport);

		if (getImportCorrelationId(fileImport.getPrimaryKey()) == null) {
			return masterDataFacade.modify(fileImport, null);
		}
		else {
			throw new CommonValidationException("import.exception.modify.running");
		}
	}

	@Override
	public void removeFileImport(MasterDataVO<UID> fileImport) throws CommonBusinessException {
		checkDeleteAllowed(getImportFileEntityMeta());

		if (getImportCorrelationId(fileImport.getPrimaryKey()) == null) {
			masterDataFacade.remove(fileImport.getEntityObject().getDalEntity(), fileImport.getPrimaryKey(), true, null);
		}
		else {
			throw new CommonRemoveException("import.exception.remove.running");
		}
	}
	
    String getImportCorrelationId(UID importfileId) {
    	final JobKey jobKey = JobKey.jobKey(importfileId.toString(), NuclosQuartzJob.JOBGROUP_IMPORT);
		try {
	        List<?> executingJobs = nuclosScheduler.getCurrentlyExecutingJobs();
	        for (Object o : executingJobs) {
	        	if (o instanceof JobExecutionContext) {
	        		JobExecutionContext job = (JobExecutionContext) o;
	        		if (job.getJobDetail().getKey().equals(jobKey)) {
	        			return job.getJobDetail().getJobDataMap().getString("ProcessId");
	        		}
	        	}
	        }
        }
        catch(SchedulerException e) {
	        throw new NuclosFatalException("import.exception.getcorrelationid");
        }
	    return null;
    }
    
}
