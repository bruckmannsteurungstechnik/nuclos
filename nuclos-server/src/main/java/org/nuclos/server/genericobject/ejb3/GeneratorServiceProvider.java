package org.nuclos.server.genericobject.ejb3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.generation.Generation;
import org.nuclos.api.service.GenerationService;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.common.ServerServiceLocator;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.genericobject.GeneratorFailedException;
import org.nuclos.server.genericobject.context.GenerationContext;
import org.nuclos.server.genericobject.context.GenerationContextBuilder;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.nbo.AbstractBusinessObject;
import org.nuclos.server.nbo.BusinessObjectQueryProvider;
import org.nuclos.server.nbo.EOBOBridge;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("GeneratorServiceProvider")
public class GeneratorServiceProvider implements GenerationService {

	private BusinessObjectQueryProvider queryProvider;
	
	@Autowired
	public void setBusinessObjectQueryProvider(BusinessObjectQueryProvider provider) {
		this.queryProvider = provider;
	}
	
	public BusinessObjectQueryProvider getQueryProvider() {
		return this.queryProvider;
	}
	
	@Override
	public <S extends BusinessObject, T extends BusinessObject> T execute(S s,
			Class<? extends Generation<S, T>> genClass)
			throws BusinessException {
		
		Generation<S, T> genInstance;
		T retVal = null;
		
		try {
			genInstance = genClass.newInstance();
			
			GeneratorFacadeLocal facade = ServerServiceLocator.getInstance().getFacade(GeneratorFacadeLocal.class);
			
			GeneratorActionVO generatorActionVO = MasterDataWrapper.getGeneratorActionVO(
					new MasterDataVO(NucletDalProvider.getInstance().getEntityObjectProcessor(
							E.GENERATION).getByPrimaryKey( (UID) genInstance.getId()),true),null);
			
			Long newGeneratedObject = (Long) facade.generateGenericObject((Long) s.getId(), null, generatorActionVO, null).getGeneratedObject().getPrimaryKey();
	
			retVal = (T) getQueryProvider().getById(genInstance.getTargetModule(), newGeneratedObject);
		} catch (NuclosBusinessRuleException e) {
			throw e.getCausingBusinessException();
		} catch (GeneratorFailedException e) {
			// needed for http://project.nuclos.de/browse/LIN-116
			final Throwable cause = e.getCause();
			if (cause instanceof BusinessException) {
				throw (BusinessException) cause;
			} else if (cause != null) {
				throw new BusinessException("GenerationProvider: Error while executing generation", cause);
			} else {
				throw new BusinessException("GenerationProvider: Error while executing generation", e);				
			}
		} catch (Exception e) {
			throw new BusinessException("GenerationProvider: Error while executing generation", e);
		}
		
		return retVal;
	}

	@Override
	public <S extends BusinessObject, T extends BusinessObject> List<T> execute(
			List<S> s, Class<? extends Generation<S, T>> genClass)
			throws BusinessException {
		
		Generation<S, T> genInstance;
		List<T> retVal = new ArrayList<T>();
		
		try {
			genInstance = genClass.newInstance();
			
			GeneratorFacadeLocal facade = ServerServiceLocator.getInstance().getFacade(GeneratorFacadeLocal.class);
			
			GeneratorActionVO generatorActionVO = MasterDataWrapper.getGeneratorActionVO(
					new MasterDataVO<UID>(
							NucletDalProvider.getInstance().getEntityObjectProcessor(E.GENERATION).
							 getByPrimaryKey((UID) genInstance.getId()),true),null);
			
			if (generatorActionVO.isGroupAttributes()) {
				
				Collection<Long> lstOfSourceIds = CollectionUtils.transform(s, new Transformer<S, Long>() {
					@Override
					public Long transform(S i) {
						return (Long) i.getId();
					}				
				});
				
				Map<String, Collection<EntityObjectVO<Long>>> groupObjects = facade.groupObjects(lstOfSourceIds, generatorActionVO);
				for (String group : groupObjects.keySet()) {
					GenerationContext<Long> context = new GenerationContextBuilder<>(groupObjects.get(group), generatorActionVO).build();
					GenerationResult generateGenericObject = facade.generateGenericObjects(context);
					if (generateGenericObject != null) {
						EntityObjectVO eoVo = generateGenericObject.getGeneratedObject();
						T t = genInstance.getTargetModule().newInstance();
						EOBOBridge.setEO((AbstractBusinessObject) t, eoVo);
						retVal.add(t);
					}
				}
				
			}
			else {
				// no grouping, so we just execute all sources step by step and add the result to the result list
				for (S curS : s) {
					retVal.add(execute(curS, genClass));
				}
			}
			
		} catch (Exception e) {
			throw new BusinessException("GenerationProvider: Error while executing generation " + genClass.getSimpleName() + " with grouping", e);
		}
		
		return retVal;
	}

}
