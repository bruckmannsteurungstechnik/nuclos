//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.genericobject.ejb3;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;

import org.apache.commons.lang.NullArgumentException;
import org.nuclos.api.rule.DeleteFinalRule;
import org.nuclos.api.rule.DeleteRule;
import org.nuclos.api.rule.InsertFinalRule;
import org.nuclos.api.rule.InsertRule;
import org.nuclos.api.rule.UpdateFinalRule;
import org.nuclos.api.rule.UpdateRule;
import org.nuclos.common.DbField;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.GenericObjectMetaDataVO;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.attribute.DynamicAttributeVO;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.MasterDataToEntityObjectTransformer;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EOGenericObjectVO;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.report.valueobject.CalcAttributeUtils;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.TruncatableCollection;
import org.nuclos.common2.TruncatableCollectionDecorator;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.attribute.valueobject.AttributeCVO;
import org.nuclos.server.autonumber.AutoNumberHelper;
import org.nuclos.server.common.AttributeCache;
import org.nuclos.server.common.BusinessIDFactory;
import org.nuclos.server.common.MandatorUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.dal.DalSupportForGO;
import org.nuclos.server.dal.DalUtils;
import org.nuclos.server.dal.processor.nuclet.IEOChunkableProcessor;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.dal.processor.nuclet.JdbcEntityObjectProcessor;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbBusinessException;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.documentfile.DocumentFileUtils;
import org.nuclos.server.genericobject.GenericObjectMetaDataCache;
import org.nuclos.server.genericobject.GenericObjectProxyList;
import org.nuclos.server.genericobject.Modules;
import org.nuclos.server.genericobject.ProxyList;
import org.nuclos.server.genericobject.ProxyListProvider;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.genericobject.valueobject.GenericObjectRelationVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.genericobject.valueobject.LogbookVO;
import org.nuclos.server.history.ejb3.HistoryFacadeLocal;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeHelper;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.navigation.treenode.GenericObjectTreeNode;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.nuclos.server.statemodel.ejb3.StateFacadeLocal;
import org.nuclos.server.statemodel.valueobject.StateHistoryVO;
import org.nuclos.server.validation.ValidationSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;

/**
 * Facade bean for all generic object management functions.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Transactional(noRollbackFor= {Exception.class})
public class GenericObjectFacadeBean extends NuclosFacadeBean implements GenericObjectFacadeLocal, GenericObjectFacadeRemote, BeanFactoryAware {

	private static final Logger LOG = LoggerFactory.getLogger(GenericObjectFacadeBean.class);

	// BeanFactoryAware
	protected BeanFactory beanFactory;

	@Autowired
	private GenericObjectFacadeHelper genericObjectFacadeHelper;

	/**
	 * No @Autowired, must be lazy.
	 */
	private HistoryFacadeLocal historyFacade;
	
	private MasterDataFacadeLocal masterDataFacade;

	@Autowired
	private MasterDataFacadeHelper masterDataFacadeHelper;

	private AttributeCache attributeCache;

	@Autowired
	private ValidationSupport validationSupport;

	@Autowired
	private AutoNumberHelper autoNumberHelper;

	@Autowired
	private SpringDataBaseHelper dataBaseHelper;
	
	@Autowired
	private MandatorUtils mandatorUtils;

	@Autowired
	private ServerParameterProvider serverParameter;
	
	public GenericObjectFacadeBean() {
	}

	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.beanFactory = beanFactory;
	}
	
	private AttributeCache getAttributeCache() {
		if (attributeCache == null) {
			attributeCache = AttributeCache.getInstance();
		}
		return attributeCache;
	}

	private final MasterDataFacadeLocal getMasterDataFacade() {
		if (masterDataFacade == null) {
			masterDataFacade = beanFactory.getBean(MasterDataFacadeLocal.class);
		}
		return masterDataFacade;
	}

	protected HistoryFacadeLocal getHistoryFacade() {
		if (historyFacade == null) {
			historyFacade = beanFactory.getBean(HistoryFacadeLocal.class);
		}
		return historyFacade;
	}

	@RolesAllowed("Login")
	@Override
	public GenericObjectMetaDataVO getMetaData() {
		return GenericObjectMetaDataCache.getInstance().getMetaDataCVO();
	}

	@RolesAllowed("Login")
	@Override
	public Map<UID, UID> getResourceMap() {
		return GenericObjectMetaDataCache.getInstance().getResourceMap();
	}

	/**
	 * §postcondition result != null
	 * §nucleus.permission mayRead(module)
	 *
	 * @param entity if entity is unknonw (== null), it will be fetched from T_UD_GENERICOBJECT, extra SQL
	 * @param iGenericObjectId primary key
	 * @return the generic object with the given id
	 * @throws CommonFinderException if there is no object with the given id.
	 * @throws CommonPermissionException if the user doesn't have the permission to view the generic object with the given id.
	 */

	@RolesAllowed("Login")
	@Override
	public GenericObjectVO get(UID entity, Long iGenericObjectId) throws CommonFinderException, CommonPermissionException {
		GenericObjectVO go = DalSupportForGO.getGenericObject(iGenericObjectId, entity);
		checkPermission(go, iGenericObjectId);
		return go;
	}

	private void checkPermission(GenericObjectVO govo, Long iGenericObjectId) throws CommonPermissionException {
		checkReadAllowedForModule(govo.getModule());
		grantUtils.checkInternal(govo.getModule(), iGenericObjectId);
		DynamicAttributeVO dynAttrMandator = govo.getAttribute(SF.MANDATOR.getUID(govo.getModule()));
		mandatorUtils.checkReadAllowed(dynAttrMandator==null?null:dynAttrMandator.getValueUid(), metaProvider.getEntity(govo.getModule()));
	}

	private void checkForStaleVersion(GenericObjectVO oldGO, GenericObjectVO newGO) throws CommonStaleVersionException {
		if (oldGO.getVersion() != newGO.getVersion()) {
			EntityObjectVO<Long> eoNew = DalSupportForGO.wrapGenericObjectVO(newGO);
			EntityObjectVO<Long> eoOld = DalSupportForGO.wrapGenericObjectVO(oldGO);
			throw new CommonStaleVersionException(getVersionConflictMessages("generic object", eoNew, eoOld));
		}
	}

	@RolesAllowed("Login")
	@Override
	public GenericObjectWithDependantsVO getWithDependants(UID entity, Long iGenericObjectId, String customUsage)
			throws CommonPermissionException, CommonFinderException {

		GenericObjectVO genericObjectVO = this.get(entity, iGenericObjectId);
		final GenericObjectWithDependantsVO result = 
				new GenericObjectWithDependantsVO(genericObjectVO, new DependentDataMap(), genericObjectVO.getDataLanguageMap());

		final GenericObjectMetaDataCache lometadataprovider = GenericObjectMetaDataCache.getInstance();
		//BMWFDM-182. There have been big performance issues, because read-only virtual subentities have been
		//loaded without need, when changing the state
		//Generally, subform data from such read-only entities are not needed in this method, so they
		//are skipped.
		Set<UID> allStSubEntityNames = lometadataprovider.getSubFormEntityByLayout(
				lometadataprovider.getBestMatchingLayout(result.getUsageCriteria(customUsage), false));
		final Set<UID> stSubEntityNames = new HashSet<>();
		for (UID subEntity : allStSubEntityNames) {
			EntityMeta<?> emdvo = metaProvider.getEntity(subEntity);
			if (!emdvo.isEditable()) continue;
			stSubEntityNames.add(subEntity);
		}

		fillDependents(result, result.getUsageCriteria(customUsage), stSubEntityNames, true, -1);

		assert result.isComplete();
		return result;
	}
	
	/**
	 * @param govo
	 * @param mpDependants
	 * @param bAll
	 * @return reload the dependant data of the genericobject, if bAll is false, only the direct
	 *         dependants (highest hierarchie of subforms) will be reloaded
	 * @throws CommonFinderException if no such object was found.
	 */
	@RolesAllowed("Login")
	public IDependentDataMap reloadDependants(GenericObjectVO govo, IDependentDataMap mpDependants, boolean bAll, String customUsage)
			throws CommonFinderException {

		final Map<EntityAndField, UID> collSubEntities = getLayoutFacade().getSubFormEntityAndParentSubFormEntities(
				govo.getModule(), govo.getId(), false, customUsage);

		if (mpDependants == null) {
			mpDependants = new DependentDataMap();
		}

		for (EntityAndField eafn : collSubEntities.keySet()) {
			// care only about dependant data which are on the highest level
			if (collSubEntities.get(eafn) == null) {
				final UID entity = eafn.getEntity();
				EntityMeta<?> eMeta = metaProvider.getEntity(entity);
				if (!eMeta.isEditable()) {
					continue;
				}
				Collection<EntityObjectVO<Long>> collmdVO = getMasterDataFacade().getDependantMasterData(
						eafn.getField(), govo.getId());

				mpDependants.setData(eafn.getDependentKey(), CollectionUtils.transform(collmdVO,
						new MasterDataToEntityObjectTransformer()));

				if (bAll) {
					for (EntityObjectVO<Long> mdVO : collmdVO) {
						// now read all dependant data of the child subforms
						masterDataFacadeHelper.readAllDependents(
								mdVO.getPrimaryKey(), mdVO.getDependents(), mdVO.isFlagRemoved(),
								entity, collSubEntities);
					}
				}
			}
		}
		return mpDependants;
	}

	/**
	 * gets generic object with dependants vo for a given generic object id (historical, readonly view)
	 * 
	 * §precondition dateHistorical != null
	 * §postcondition result != null
	 * §nucleus.permission mayRead(module)
	 * 
	 * @param iGenericObjectId id of generic object to show historical information for
	 * @param dateHistorical date to show historical information for
	 * @return generic object with dependants vo at the given point in time
	 * @throws CommonFinderException if the given object didn't exist at the given point in time.
	 */
	@RolesAllowed("Login")
	@Override
	public GenericObjectWithDependantsVO getHistorical(Long iGenericObjectId, Date dateHistorical, String customUsage) throws CommonFinderException, CommonPermissionException {
		LOG.debug("Entering getHistorical(Integer iGenericObjectId, Date dateHistorical)");

		if (dateHistorical == null) {
			throw new NullArgumentException("dateHistorical");
		}

		// TODO NUCLOS-6348 add entity as a parameter
		GenericObjectVO genObj = this.get(null, iGenericObjectId);
		GenericObjectWithDependantsVO genericObjectVO = 
				new GenericObjectWithDependantsVO(genObj, reloadDependants(genObj, null, true, customUsage),
						genObj.getDataLanguageMap());
	
		final Calendar calendar = new GregorianCalendar();
		calendar.setTime(dateHistorical);

		// If dateHistorical has no time components, we can assume that the user wants to see the state of the object at the end of that day.
		if (calendar.get(Calendar.HOUR_OF_DAY) + calendar.get(Calendar.MINUTE) + calendar.get(Calendar.SECOND) == 0) {
			calendar.set(Calendar.HOUR_OF_DAY, 23);
			calendar.set(Calendar.MINUTE, 59);
			calendar.set(Calendar.SECOND, 0);
		}
		dateHistorical = calendar.getTime();

		// Check if the object existed at the historical date:
		final Date dateCreatedAt = genericObjectVO.getCreatedAt();
		if (dateHistorical.before(dateCreatedAt)) {
			throw new CommonFinderException("genericobject.facade.exception.1");//"Das Objekt existierte zum angegebenen Zeitpunkt nicht.");
		}

		LOG.debug("Historical Date we want to see  : {}", dateHistorical);
		LOG.debug("Date of creation of record      : {}", dateCreatedAt);

		genericObjectVO.setAttributes(GenericObjectFacadeHelper.getHistoricalAttributes(genericObjectVO, dateHistorical, genericObjectVO.getDependents()).values());

		LOG.debug("Leaving getHistorical(Integer iGenericObjectId, Date dateHistorical)");

		assert genericObjectVO != null;

		return genericObjectVO;
	}

	/**
	 * gets all generic objects that match a given search condition
	 * 
	 * $postcondition result != null
	 * 
	 * @param iModuleId id of module to search for generic objects in
	 * @param clctexpr
	 * @param stRequiredSubEntities
	 * @return list of generic object value objects with specified dependants and without parent objects!
	 * 
	 * @deprecated use with customUsage
	 */
	@Deprecated
	public List<GenericObjectWithDependantsVO> getGenericObjects(UID iModuleId, CollectableSearchExpression clctexpr,
			Set<UID> stRequiredSubEntities) {
		return getGenericObjectsImpl(iModuleId, clctexpr, stRequiredSubEntities, getCurrentUserName(),
				serverParameter.getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY), getCurrentMandatorUID());
	}
	
	/**
	 * gets all generic objects along with its dependents, that match a given search condition
	 * 
	 * §precondition stRequiredSubEntities != null
	 * §todo rename to getGenericObjectProxyList?
	 * 
	 * @param iModuleId id of module to search for generic objects in
	 * @param clctexpr value object containing search expression
	 * @param stRequiredAttributeIds may be <code>null</code>, which means all attributes are required
	 * @param stRequiredSubEntityNames
	 * @return list of generic object value objects
	 */
	@RolesAllowed("Login")
	@Override
	public ProxyList<Long,GenericObjectWithDependantsVO> getGenericObjectsWithDependants(UID iModuleId, CollectableSearchExpression clctexpr,
			Set<UID> stRequiredAttributeIds, Set<UID> stRequiredSubEntityNames, String customUsage) throws CommonPermissionException {
		checkReadAllowed(iModuleId);
		final EntityMeta<?> eMeta = metaProvider.getEntity(iModuleId);
		clctexpr.setSearchCondition(mandatorUtils.append(clctexpr, eMeta));
		final ProxyListProvider plProvider = new ProxyListProvider(serverParameter, metaProvider);
		return new GenericObjectProxyList(iModuleId, clctexpr, stRequiredAttributeIds, stRequiredSubEntityNames, customUsage, plProvider);
	}

	/**
	 * gets all generic objects along with its dependants, that match a given search condition, but
	 * clears the values of all attributes on which the current user has no read permission
	 * NOTE: use only within report mechanism
	 * 
	 * §precondition stRequiredSubEntities != null
	 * §todo rename to getGenericObjectProxyList?
	 * 
	 * @param iModuleId id of module to search for generic objects in
	 * @param clctexpr value object containing search expression
	 * @param stRequiredAttributeIds may be <code>null</code>, which means all attributes are required
	 * @param stRequiredSubEntityNames
	 * @return list of generic object value objects
	 */
	@RolesAllowed("Login")
	@Override
	public Collection<GenericObjectWithDependantsVO> getPrintableGenericObjectsWithDependants(
			UID iModuleId, CollectableSearchExpression clctexpr,
			Set<UID> stRequiredAttributeIds, Set<UID> stRequiredSubEntityNames, String custom) throws CommonPermissionException {
		checkReadAllowed(iModuleId);
		return getPrintableGenericObjectsWithDependantsNoCheck(iModuleId, clctexpr, stRequiredAttributeIds, stRequiredSubEntityNames, custom);
	}
	
	@RolesAllowed("Login")
	public Collection<GenericObjectWithDependantsVO> getPrintableGenericObjectsWithDependantsNoCheck(
			UID iModuleId, CollectableSearchExpression clctexpr,
			Set<UID> stRequiredAttributeIds, Set<UID> stRequiredSubEntityNames, String customUsage) {

		stRequiredAttributeIds.add(SF.STATENUMBER.getUID(iModuleId));
		stRequiredAttributeIds.add(SF.STATE.getUID(iModuleId));
		//stRequiredAttributeIds.add(SF.STATEICON.getUID(iModuleId)); // will be added at client. performance issues...

		ResultParams resultParams = new ResultParams(stRequiredAttributeIds, 0L, null, true);
		Collection<GenericObjectWithDependantsVO> genericObjectsChunk = getGenericObjectsChunkNoCheck(iModuleId,
				stRequiredSubEntityNames, clctexpr, resultParams, customUsage);

		// remove all attribute and subform values on which the current user has no read permission
		for (GenericObjectWithDependantsVO gowdvo : genericObjectsChunk) {
			UID iStatus = gowdvo.getAttribute(SF.STATE.getUID(gowdvo.getModule())).getValueUid();

			if (iStatus != null) {
				for (DynamicAttributeVO dynamicAttributeVO : gowdvo.getAttributes()) {
					UID fieldUID = dynamicAttributeVO.getAttributeUID();
					if (CalcAttributeUtils.isCalcAttributeCustomization(fieldUID)) {
						// check permission for the baseField...
						fieldUID = metaProvider.getEntityField(fieldUID).getCalcBaseFieldUID();
					}

					if (SecurityCache.getInstance().getAttributePermission(gowdvo.getModule(), fieldUID, iStatus) == null) {
						dynamicAttributeVO.setValue(null);
					}
				}

				// TODO NUCLOS-6134, NUCLOS-6140 Centralize logic
				IDependentDataMap dependants = gowdvo.getDependents();
				for (IDependentKey dependentKey : dependants.getKeySet()) {
					Collection<EntityObjectVO<Long>> eoVos = RigidUtils.uncheckedCast(dependants.getData(dependentKey));
					FieldMeta<?> refFieldMeta = metaProvider.getEntityField(dependentKey.getDependentRefFieldUID());
					removeValuesFromForbiddenSubformColumns(iStatus, refFieldMeta.getEntity(), eoVos);
				}
			}
		}

		return genericObjectsChunk;
	}

	/**
	 * gets all generic objects that match a given search condition
	 * 
	 * §precondition stRequiredSubEntities != null
	 * §precondition iMaxRowCount &gt; 0
	 * §postcondition result.size() &lt;= iMaxRowCount
	 * 
	 * @param iModuleId id of module to search for generic objects in
	 * @param clctexpr
	 * @param stRequiredSubEntities
	 * @param iMaxRowCount the maximum number of rows
	 * @return list of generic object value objects
	 * 
	 */
	@RolesAllowed("Login")
	@Override
	public TruncatableCollection<GenericObjectWithDependantsVO> getRestrictedNumberOfGenericObjects(UID iModuleId,
							CollectableSearchExpression clctexpr, Set<UID> stRequiredSubEntities, String customUsage,
							Long iMaxRowCount) throws CommonPermissionException {
		checkReadAllowed(iModuleId);
		return getRestrictedNumberOfGenericObjectsNoCheck(iModuleId, clctexpr, stRequiredSubEntities, customUsage, iMaxRowCount);
	}

	public TruncatableCollection<GenericObjectWithDependantsVO> getRestrictedNumberOfGenericObjectsNoCheck(UID iModuleId,
			CollectableSearchExpression clctexpr, Set<UID> stRequiredSubEntityNames, String customUsage,
			Long iMaxRowCount) {

		if (iMaxRowCount <= 0) {
			throw new IllegalArgumentException("iMaxRowCount == " + iMaxRowCount);
		}

		final EntityMeta<?> eMeta = metaProvider.getEntity(iModuleId);
		clctexpr.setSearchCondition(mandatorUtils.append(clctexpr, eMeta));
		final List<GenericObjectWithDependantsVO> lstResult = new ArrayList<>(Math.min(iMaxRowCount.intValue() + 1, 1000));

		for (EntityObjectVO<?> eo : nucletDalProvider.getEntityObjectProcessor(eMeta).getBySearchExprResultParams(clctexpr, new ResultParams(iMaxRowCount + 1, true))) {
			GenericObjectVO govo = DalSupportForGO.getGenericObjectVO((EntityObjectVO<Long>) eo);
			final GenericObjectWithDependantsVO go = new GenericObjectWithDependantsVO(
					govo, new DependentDataMap(),govo.getDataLanguageMap());
			try {
				if (!stRequiredSubEntityNames.isEmpty()) {
					fillDependants(go, go.getUsageCriteria(customUsage), stRequiredSubEntityNames);
				}
			}
			catch(CommonFinderException e) {
				throw new NuclosFatalException(e);
			}
			lstResult.add(go);
		}

		assert lstResult.size() <= iMaxRowCount + 1;

		final boolean bTruncated = (lstResult.size() == iMaxRowCount + 1);
		if (bTruncated) {
			// remove the last entry:
			lstResult.remove(iMaxRowCount.intValue());
			assert lstResult.size() == iMaxRowCount;
		}

		final Long iTotalRowCount = bTruncated 
				? nucletDalProvider.getEntityObjectProcessor(eMeta).count(clctexpr) 
				: lstResult.size();

		final TruncatableCollection<GenericObjectWithDependantsVO> result =
				new TruncatableCollectionDecorator<>(lstResult, bTruncated, iTotalRowCount.intValue());
		assert result.size() <= iMaxRowCount;
		return result;
	}

	/**
	 * gets the ids of all leased objects that match a given search expression (ordered, when necessary)
	 * @param iModuleId id of module to search for leased objects in
	 * @param cse condition that the leased objects to be found must satisfy
	 * @return List&lt;Long&gt; list of leased object ids
	 */
	public List<Long> getGenericObjectIdsNoCheck(UID iModuleId, CollectableSearchExpression cse) {
		EntityMeta eMeta = metaProvider.getEntity(iModuleId);
		cse.setSearchCondition(mandatorUtils.append(cse, eMeta));
		List<Long> ids = nucletDalProvider.getEntityObjectProcessor(eMeta).getIdsBySearchExpression(cse);
		return ids;
	}

	/**
	 * gets the ids of all generic objects that match a given search condition
	 * 
	 * @param iModuleId id of module to search for generic objects in
	 * @param cse condition that the leased objects to be found must satisfy
	 * @return List&lt;Integer&gt; list of gener
	 */
	@Override
	public List<Long> getGenericObjectIds(UID iModuleId, CollectableSearchExpression cse) throws CommonPermissionException {
		checkReadAllowed(iModuleId);
		return this.getGenericObjectIdsNoCheck(iModuleId, cse);
	}

	/**
	 * gets more leased objects that match a given search condition
	 * 
	 * §precondition stRequiredSubEntities != null
	 * 
	 * @param iModuleId id of module to search for leased objects in
	 * @param stRequiredSubEntityNames
	 * @return list of leased object value objects
	 * 
	 */
	@Override
	public Collection<GenericObjectWithDependantsVO> getGenericObjectsMore(UID iModuleId, List<Long> lstIds,
		   Set<UID> stRequiredSubEntityNames, String customUsage) throws CommonPermissionException {
		checkReadAllowed(iModuleId);
		return getGenericObjectsMoreNoCheck(iModuleId, lstIds, stRequiredSubEntityNames, customUsage);
	}

	public Collection<GenericObjectWithDependantsVO> getGenericObjectsMoreNoCheck(UID iModuleId, List<Long> lstIds,
			Set<UID> stRequiredSubEntityNames, String customUsage) {

		final EntityMeta eMeta = metaProvider.getEntity(iModuleId);

		if (eMeta.isUidEntity()) {
			throw new IllegalArgumentException("UID entities are not allowed here");
		}

		
		IEntityObjectProcessor<Long> entityObjectProcessor = 
				(IEntityObjectProcessor<Long>) nucletDalProvider.getEntityObjectProcessor(eMeta);
		List<EntityObjectVO<Long>> eos = new ArrayList<>();
		int start = 0;
		int end = 0;
		while (start < lstIds.size()) {
			end = start + 5000;
			List<Long> lstPart = new ArrayList<>(lstIds.subList(start, end > lstIds.size() ? lstIds.size() : end));
			eos.addAll(entityObjectProcessor.getByPrimaryKeys(lstPart));
			start = end;
		}
		
		List<GenericObjectWithDependantsVO> result = new ArrayList<>(eos.size());

		for (EntityObjectVO eo : eos) {
			GenericObjectVO genericObjectVO = DalSupportForGO.getGenericObjectVO(eo);
			final GenericObjectWithDependantsVO go = new GenericObjectWithDependantsVO(
					genericObjectVO, new DependentDataMap(), genericObjectVO.getDataLanguageMap());
			try {
				fillDependants(go, go.getUsageCriteria(customUsage), stRequiredSubEntityNames);
			}
			catch(CommonFinderException e) {
				throw new NuclosFatalException(e);
			}
			result.add(go);
		}

		return result;
	}

	@Override
	public Collection<GenericObjectWithDependantsVO> getGenericObjectsChunk(UID iModuleId, Set<UID> stRequiredSubEntityNames,
			CollectableSearchExpression clctexpr, ResultParams resultParams, String customUsage) throws CommonPermissionException {
		checkReadAllowed(iModuleId);
		return getGenericObjectsChunkNoCheck(iModuleId, stRequiredSubEntityNames, clctexpr, resultParams, customUsage);
	}

	private Collection<GenericObjectWithDependantsVO> getGenericObjectsChunkNoCheck(UID iModuleId, Set<UID> stRequiredSubEntityNames,
			CollectableSearchExpression clctexpr, ResultParams resultParams, String customUsage) {

		final EntityMeta<Long> eMeta = metaProvider.getEntity(iModuleId);
		final IEOChunkableProcessor<Long> jeop = (IEOChunkableProcessor<Long>) nucletDalProvider.getEntityObjectProcessor(eMeta);

		final List<EntityObjectVO<Long>> eos = jeop.getChunkBySearchExpressionImpl(clctexpr, resultParams);
		List<GenericObjectWithDependantsVO> result = new ArrayList<GenericObjectWithDependantsVO>(eos.size());

		for (EntityObjectVO<Long> eo : eos) {
			GenericObjectVO genericObjectVO = DalSupportForGO.getGenericObjectVO(eo);
			final GenericObjectWithDependantsVO go = new GenericObjectWithDependantsVO(
					genericObjectVO, new DependentDataMap(), genericObjectVO.getDataLanguageMap());
			try {
				fillDependants(go, go.getUsageCriteria(customUsage), stRequiredSubEntityNames);
			}
			catch(CommonFinderException e) {
				throw new NuclosFatalException(e);
			}
			result.add(go);
		}

		return result;
	}
	
	@Override
	public Long countGenericObjectRows(UID iModuleId, final CollectableSearchExpression clctexpr) throws  CommonPermissionException {
		checkReadAllowed(iModuleId);
		final EntityMeta eMeta = metaProvider.getEntity(iModuleId);
		IEntityObjectProcessor eoProcessor = nucletDalProvider.getEntityObjectProcessor(eMeta);
		return eoProcessor.count(clctexpr);
	}

	@Override
	public Long countGenericObjectRowsWithLimit(UID iModuleId, final CollectableSearchExpression clctexpr, Long limit) throws  CommonPermissionException {
		checkReadAllowed(iModuleId);
		final EntityMeta eMeta = metaProvider.getEntity(iModuleId);
		IEntityObjectProcessor eoProcessor = nucletDalProvider.getEntityObjectProcessor(eMeta);
		return eoProcessor.countWithLimit(clctexpr, limit);
	}

	/**
	 * creates a new generic object, along with its dependants.
	 * 
	 * §precondition gowdvo.getId() == null
	 * §precondition Modules.getInstance().isSubModule(iModuleId.intValue()) --&gt; gowdvo.getParentId() != null
	 * §precondition (gowdvo.getDependants() != null) -&gt; gowdvo.getDependants().dependantsAreNew()
	 * §precondition stRequiredSubEntities != null
	 *
	 * §nucleus.permission mayWrite(module)
	 * 
	 * @param gowdvo containing the generic object data
	 * @param stRequiredSubEntityNames Set&lt;UID&gt;
	 * @return the new generic object, containing the dependants for the specified sub entities.
	 *
	 * @deprecated use with customUsage
	 */
	@RolesAllowed("Login")
	@Deprecated
	@Override
	public GenericObjectWithDependantsVO create(GenericObjectWithDependantsVO gowdvo, Set<UID> stRequiredSubEntityNames) 
			throws NuclosBusinessRuleException, CommonPermissionException, CommonCreateException, CommonFinderException {
		
		return create(gowdvo, stRequiredSubEntityNames, serverParameter.getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
	}
	
	@RolesAllowed("Login")
	@Override
	public GenericObjectWithDependantsVO create(GenericObjectWithDependantsVO gowdvo, Set<UID> stRequiredSubEntityNames, String customUsage)
			throws CommonPermissionException, NuclosBusinessRuleException, CommonCreateException, CommonFinderException {

		if (stRequiredSubEntityNames == null) {
			throw new IllegalArgumentException("stRequiredSubEntities");
		}
		final GenericObjectVO govoCreated = this.create(gowdvo, customUsage);
		
		final GenericObjectWithDependantsVO result = new GenericObjectWithDependantsVO(govoCreated, new DependentDataMap(), govoCreated.getDataLanguageMap());
		fillDependants(result, result.getUsageCriteria(customUsage), stRequiredSubEntityNames);

		return result;
	}

	/**
	 * creates a new generic object, along with its dependants.
	 * 
	 * §precondition gowdvo.getId() == null
	 * §precondition Modules.getInstance().isSubModule(gowdvo.getModuleId()) --&gt; gowdvo.getParentId() != null
	 * §precondition (gowdvo.getDependants() != null) --&gt; gowdvo.getDependants().areAllDependantsNew()
	 *
	 * §nucleus.permission mayWrite(module)
	 * 
	 * @param gowdvo the generic object, along with its dependants.
	 * @return the created object witout dependants.
	 * @throws CommonPermissionException if the current user doesn't have the right to create such an object.
	 * @throws NuclosBusinessRuleException if the object could not be created because a business rule failed.
	 * @throws CommonCreateException if the object could not be created for other reasons, eg. because of a database constraint violation.
	 *
	 * @deprecated use with customUsage
	 */
	@RolesAllowed("Login")
	@Deprecated
	@Override
	public GenericObjectVO create(GenericObjectWithDependantsVO gowdvo)
			throws CommonPermissionException, NuclosBusinessRuleException, CommonCreateException {
		
		return create(gowdvo, serverParameter.getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
	}
	
	
	public GenericObjectVO create(GenericObjectWithDependantsVO gowdvo, String customUsage, boolean checkUser) 
			throws CommonPermissionException, NuclosBusinessRuleException, CommonCreateException {
		
		LOG.debug("Entering create(GenericObjectWithDependantsVO)");
		
		if (gowdvo.getId() != null) {
			throw new IllegalArgumentException("govo.getId() != null");
		}

		final UID entityUid = gowdvo.getModule();
		final EntityMeta<Long> eMeta = metaProvider.getEntity(entityUid);
		
		if (eMeta.isMandator() && SecurityCache.getInstance().isMandatorPresent()) {
			EntityObjectVO<Long> eo = DalSupportForGO.wrapGenericObjectVO(gowdvo);
			try {
				getEventSupportFacade().fireInitMandator(eo);
			} catch (NuclosCompileException e) {
				throw new CommonCreateException("fireInitMandator failed", e);
			}
			mandatorUtils.checkWriteAllowed(eo, eMeta);
			gowdvo = DalSupportForGO.getGenericObjectWithDependantsVO(eo);
		}

		final boolean useEventSupports = getUsesEventSupports(entityUid, InsertRule.class.getCanonicalName());

		if(useEventSupports){
			try {
				gowdvo = fireSaveEvent(InsertRule.class.getCanonicalName(), gowdvo, customUsage, false);
			} catch (NuclosCompileException e) {
				throw new CommonCreateException("fireSaveEvent failed", e);
			}
		}

		final EntityObjectVO<Long> dalVO = DalSupportForGO.wrapGenericObjectVO(gowdvo);
		dalVO.setVersion(null);
		dalVO.setCreatedBy(null);
		dalVO.setCreatedAt(null);
		
		final String user = getCurrentUserName();
		DalUtils.updateVersionInformation(dalVO, user);
		dalVO.setPrimaryKey(DalUtils.getNextId());
		dalVO.flagNew();

		// generate system identifier:
		final String sCanonicalValueSystemIdentifier = BusinessIDFactory.generateSystemIdentifier(eMeta);
		assert !StringUtils.isNullOrEmpty(sCanonicalValueSystemIdentifier);
		dalVO.setFieldValue(SF.SYSTEMIDENTIFIER, sCanonicalValueSystemIdentifier);

		// NUCLOS-1477
		handleAutoNumber(dalVO);
		
		try {
			validationSupport.validate(dalVO, dalVO.getDependents());

			if (MetaProvider.isDataEntity(eMeta, true)) {
				DocumentFileUtils.storeDocumentFiles(dalVO, null);
			}
			
			IEntityObjectProcessor<Long> entityObjectProcessor = 
					(IEntityObjectProcessor<Long>) nucletDalProvider.<Long>getEntityObjectProcessor(dalVO.getDalEntity());
			entityObjectProcessor.insertOrUpdate(dalVO);
			
			final Date sysdate = new Date();
			/**
			 * new "old" generic object record
			 */
			final EOGenericObjectVO eogo = new EOGenericObjectVO();
			eogo.setPrimaryKey(dalVO.getId());
			eogo.setCreatedBy(user);
			eogo.setCreatedAt(InternalTimestamp.toInternalTimestamp(sysdate));
			eogo.setVersion(1);
			eogo.setChangedBy(user);
			eogo.setChangedAt(InternalTimestamp.toInternalTimestamp(sysdate));
			eogo.setEntityUID(entityUid);
			eogo.flagNew();
			
			nucletDalProvider.getEOGenericObjectProcessor().insertOrUpdate(eogo);
		} catch (DbBusinessException ex) {
			throw new NuclosBusinessRuleException(ex.getMessage());
		} catch(Exception e) {
			throw new CommonCreateException(e);
		}

		final Long id = dalVO.getPrimaryKey();
		final GenericObjectWithDependantsVO goVO = DalSupportForGO.getGenericObjectWithDependantsVO(dalVO);

		// get default object group assigned to current user
		if (checkUser && isCalledRemotely()) {
			checkWriteAllowed(gowdvo.getModule());

			// NUCLOS-6195 1)
			if (!securityCache.isNewAllowedForModule(user, gowdvo.getModule(), getCurrentMandatorUID())) {
				throw new CommonPermissionException("Intial Statetransition now allowed for user="
					+ user + " and entity=" + gowdvo.getModule());
			};
		}

		// create dependant records from subforms:
		if (goVO.getDependents() != null) {
			final Map<EntityAndField, UID> collSubEntities
				= getLayoutFacade().getSubFormEntityAndParentSubFormEntities(entityUid, id, false, customUsage);

			try {
				genericObjectFacadeHelper.createDependants(entityUid, id, goVO.getDependents(), collSubEntities, customUsage);
			} catch (DbBusinessException ex) {
				throw new NuclosBusinessRuleException(ex.getMessage());
			} 
		}
				
		gowdvo = new GenericObjectWithDependantsVO(goVO, new DependentDataMap(), goVO.getDataLanguageMap());
				
		GenericObjectWithDependantsVO result;
		if (eMeta.isStateModel()) {
			// create first state entry of generic object:
			try {
				beanFactory.getBean(StateFacadeLocal.class).enterInitialState(gowdvo, customUsage);

				// NUCLEUSINT-928: result was genericObjectFacadeHelper.getValueObject(goVO), see else clause.
				// But that object represents the object before the state change (version 1)
				// while the real object in the database is version 2.  Beside the version,
				// some other attributes may have been changed by state transition, too.
				// TODO: As workaround (because enterInitialState()/StateFacadeBean does not
				// return the changed object), we get a new one via get().
				GenericObjectVO genericObjectVO = get(entityUid, id);
				result = new GenericObjectWithDependantsVO(genericObjectVO, gowdvo.getDependents(), genericObjectVO.getDataLanguageMap());
			} catch (CommonFinderException ex) {
				// This is considered fatal:
				throw new CommonFatalException(ex);
			} catch (NuclosBusinessException ex) {
				// This is considered fatal:
				throw new CommonFatalException(ex.getMessage(), ex);
			}
		} else {
			try {
				GenericObjectVO genericObjectVO = get(entityUid, id);
				result = new GenericObjectWithDependantsVO(
						DalSupportForGO.getGenericObject(id, goVO.getModule()), gowdvo.getDependents(), genericObjectVO.getDataLanguageMap());
			} catch (Exception e) {
				throw new CommonFatalException(e);
			}
		}
		
		final boolean useFinalEventSupports = this.getUsesEventSupports(entityUid, InsertFinalRule.class.getCanonicalName());
		
		if(useFinalEventSupports){
			try {
				fireSaveEvent(InsertFinalRule.class.getCanonicalName(), result, customUsage, false);
				GenericObjectVO genericObjectVO = get(entityUid, id);
				result = new GenericObjectWithDependantsVO(genericObjectVO, new DependentDataMap(), genericObjectVO.getDataLanguageMap());
			} catch (CommonFinderException ex) {
				throw new CommonFatalException(ex);
			} catch (NuclosCompileException e) {
				throw new CommonFatalException(e);
			}
		}

		LOG.debug("Leaving create(GenericObjectWithDependantsVO)");
		return result;
	}
	
	@RolesAllowed("Login")
	@Override
	public GenericObjectVO create(GenericObjectWithDependantsVO gowdvo, String customUsage)
			throws CommonPermissionException, NuclosBusinessRuleException, CommonCreateException {
		return this.create(gowdvo, customUsage, true);
	}
	
	/**
	 * updates an existing generic object in the database and returns the written object along with its dependants.
	 * 
	 * §precondition iModuleId != null
	 * §precondition lowdcvo.getModuleId() == iModuleId
	 * §nucleus.permission mayWrite(module)
	 * 
	 * @param iModuleId module id for plausibility check
	 * @param lowdcvo containing the generic object data
	 * @return same generic object as value object
	 * 
	 * @deprecated use with customUsage
	 */
	@RolesAllowed("Login")
	@Deprecated
	@Override
	public GenericObjectWithDependantsVO modify(UID iModuleId, GenericObjectWithDependantsVO lowdcvo)
			throws CommonCreateException, CommonFinderException, CommonRemoveException,
			CommonPermissionException, CommonStaleVersionException, NuclosBusinessException,
			CommonValidationException {
		
		return modify(iModuleId, lowdcvo, serverParameter.getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
	}
	
	/**
	 * updates an existing generic object in the database and returns the written object along with its dependants.
	 * 
	 * §precondition iModuleId != null
	 * §precondition lowdcvo.getModuleId() == iModuleId
	 * §nucleus.permission mayWrite(module)
	 * 
	 * @param iModuleId module id for plausibility check
	 * @param lowdcvo containing the generic object data
	 * @return same generic object as value object
	 * 
	 * @deprecated use with customUsage
	 */
	@RolesAllowed("Login")
	@Deprecated
	@Override
	public GenericObjectWithDependantsVO modify(UID iModuleId, GenericObjectWithDependantsVO lowdcvo, boolean isCollectiveProcessing)
			throws CommonCreateException, CommonFinderException, CommonRemoveException,
			CommonPermissionException, CommonStaleVersionException, NuclosBusinessException,
			CommonValidationException {
		
		return modify(iModuleId, lowdcvo, serverParameter.getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY), isCollectiveProcessing);
	}
	
	@RolesAllowed("Login")
	@Override
	public GenericObjectWithDependantsVO modify(UID iModuleId, GenericObjectWithDependantsVO lowdcvo, String customUsage, boolean isCollectiveProcessing)
			throws CommonCreateException, CommonFinderException, CommonRemoveException,
			CommonPermissionException, CommonStaleVersionException, NuclosBusinessException,
			CommonValidationException {

		if (iModuleId == null) {
			throw new NullArgumentException("iModuleId");
		}
		if (!lowdcvo.getModule().equals(iModuleId)) {
			throw new IllegalArgumentException("iModuleId");
		}

		final GenericObjectVO govoUpdated = modify(lowdcvo, true, customUsage, isCollectiveProcessing);

		final GenericObjectWithDependantsVO result = new GenericObjectWithDependantsVO(govoUpdated, new DependentDataMap(), govoUpdated.getDataLanguageMap());
		
		return result;
	}
	
	@RolesAllowed("Login")
	@Override
	public GenericObjectWithDependantsVO modify(UID iModuleId, GenericObjectWithDependantsVO lowdcvo, String customUsage)
			throws CommonCreateException, CommonFinderException, CommonRemoveException,
			CommonPermissionException, CommonStaleVersionException, NuclosBusinessException,
			CommonValidationException {
		return modify(iModuleId, lowdcvo, customUsage, false);
	}

	/**
	 * modifies an existing generic object.
	 * 
	 * §nucleus.permission mayWrite(module)
	 * §todo change signature into GenericObjectVO modify(GenericObjectWithDependantsVO lowdcvo, boolean bFireSaveEvent)
	 * 
	 * @param govo containing the generic object data
	 * @param bFireSaveEvent
	 * @return same generic object as value object
	 * @deprecated use with customUsage
	 */
	@RolesAllowed("Login")
	@Deprecated
	// @Override
	public GenericObjectWithDependantsVO modify(GenericObjectWithDependantsVO govo, boolean bFireSaveEvent, boolean isCollectiveProcessing)
			throws CommonPermissionException, CommonStaleVersionException, NuclosBusinessException, CommonValidationException,
			CommonCreateException, CommonFinderException, CommonRemoveException {
		
		return modifyImpl(govo, bFireSaveEvent, true, serverParameter.getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY), isCollectiveProcessing);
	}
	
	@RolesAllowed("Login")
	// @Override
	public GenericObjectWithDependantsVO modify(GenericObjectWithDependantsVO govo, boolean bFireSaveEvent, String customUsage, boolean isCollectiveProcessing)
			throws CommonPermissionException, CommonStaleVersionException,
			NuclosBusinessException, CommonValidationException,
			CommonCreateException, CommonFinderException, CommonRemoveException {
		
		return modifyImpl(govo, bFireSaveEvent, true, customUsage, isCollectiveProcessing);
	}
	
	@RolesAllowed("Login")
	// @Override
	public GenericObjectWithDependantsVO modify(GenericObjectWithDependantsVO govo, boolean bFireSaveEvent, String customUsage)
			throws CommonPermissionException, CommonStaleVersionException,
			NuclosBusinessException, CommonValidationException,
			CommonCreateException, CommonFinderException, CommonRemoveException {
		
		return modifyImpl(govo, bFireSaveEvent, true, customUsage, false);
	}

	@RolesAllowed("Login")
	// @Override
	public GenericObjectWithDependantsVO modify(GenericObjectWithDependantsVO govo, String customUsage, boolean isCollectiveProcessing)
			throws CommonPermissionException, CommonStaleVersionException,
			NuclosBusinessException, CommonValidationException,
			CommonCreateException, CommonFinderException, CommonRemoveException {
		return modifyImpl(govo, true, true, customUsage, isCollectiveProcessing);
	}
	
	@RolesAllowed("Login")
	// @Override
	public GenericObjectWithDependantsVO modify(GenericObjectWithDependantsVO govo, String customUsage)
			throws CommonPermissionException, CommonStaleVersionException,
			NuclosBusinessException, CommonValidationException,
			CommonCreateException, CommonFinderException, CommonRemoveException {
		return modifyImpl(govo, true, true, customUsage, false);
	}
	
	@RolesAllowed("Login")
	// @Override
	public GenericObjectWithDependantsVO modify(GenericObjectWithDependantsVO govo, boolean isCollectiveProcessing)
			throws CommonPermissionException, CommonStaleVersionException,
			NuclosBusinessException, CommonValidationException,
			CommonCreateException, CommonFinderException, CommonRemoveException {
		return modifyImpl(govo, true, true, null, isCollectiveProcessing);
	}
	
	@RolesAllowed("Login")
	// @Override
	public GenericObjectWithDependantsVO modify(GenericObjectWithDependantsVO govo)
			throws CommonPermissionException, CommonStaleVersionException,
			NuclosBusinessException, CommonValidationException,
			CommonCreateException, CommonFinderException, CommonRemoveException {
		return modifyImpl(govo, true, true, null, false);
	}
	
	/**
	 * modifies an existing generic object.
	 * 
	 * §nucleus.permission mayWrite(module)
	 * §todo change signature into GenericObjectVO modify(GenericObjectWithDependantsVO lowdcvo, boolean bFireSaveEvent)
	 * 
	 * @param govo containing the generic object data
	 * @param bFireSaveEvent
	 * @param bCheckPermission
	 * @return same generic object as value object
	 * @deprecated use with customUsage
	 */
	@RolesAllowed("Login")
	@Deprecated
	public GenericObjectWithDependantsVO modify(GenericObjectWithDependantsVO govo, boolean bFireSaveEvent,
												boolean bCheckPermission, boolean isCollectiveProcessing)
			throws CommonPermissionException, CommonStaleVersionException,
			NuclosBusinessException, CommonValidationException,
			CommonCreateException, CommonFinderException, CommonRemoveException {
		
		return modifyImpl(govo, bFireSaveEvent, bCheckPermission,
				serverParameter.getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY), isCollectiveProcessing);
	}

	@RolesAllowed("Login")
	@CacheEvict(value="goMetaFields", key="#p0.id")
	protected GenericObjectWithDependantsVO modifyImpl(GenericObjectWithDependantsVO govo, boolean bFireSaveEvent,
												 boolean bCheckPermission, String customUsage, boolean isCollectiveProcessing)
			throws CommonPermissionException, CommonStaleVersionException, NuclosBusinessException, CommonValidationException,
			CommonCreateException, CommonFinderException, CommonRemoveException {

		// @todo make the handling of mpDependants clean!
		LOG.debug("Entering modify(GenericObjectVO govo, DependantMasterDataMap mpDependants)");

		LOG.debug("Modifying (Start find)");
		final EntityMeta<Object> eMeta = metaProvider.getEntity(govo.getModule());
		final boolean bLogbookTracking = eMeta.isLogBookTracking();
		final EntityObjectVO dbEoVO;
		if (bLogbookTracking) {
			dbEoVO = DalSupportForGO.getEntityObject(govo.getId(), govo.getModule());
		} else {
			dbEoVO = DalSupportForGO.getEntityObjectThin(govo.getId(), govo.getModule());
		}
		 
		GenericObjectWithDependantsVO dbGoVO = DalSupportForGO.getGenericObjectWithDependantsVO(dbEoVO);

		if (bCheckPermission){
			this.checkWriteAllowedForModule(govo.getModule());
			grantUtils.checkWriteInternal(dbEoVO.getDalEntity(), dbEoVO.getPrimaryKey());
			lockUtils.checkLockedByCurrentUserInternal(dbEoVO.getDalEntity(), dbEoVO.getPrimaryKey());
			mandatorUtils.checkWriteAllowedFromDb(govo.getId(), eMeta);
		}
		
		if (eMeta.isMandator()) {
			DynamicAttributeVO mandatorAttribute = govo.getAttribute(SF.MANDATOR.getUID(eMeta));
			UID mandatorUIDFromUpdate = mandatorAttribute.getValueUid();
			UID mandatorUIDFromDb = dbEoVO.getFieldUid(SF.MANDATOR_UID);
			mandatorUtils.checkMandatorChange(mandatorUIDFromDb, mandatorUIDFromUpdate, govo.getId(), eMeta);
		}

		this.checkForStaleVersion(dbGoVO, govo);

		// change the status of leased object now:
		if (!LangUtils.equal(dbGoVO.getStatus(), govo.getStatus())) {
			// E.STATEHISTORY
			DynamicAttributeVO stateDynAttributeVO = govo.getAttribute(SF.STATE.getUID(govo.getModule()));
			StateHistoryVO stateHistoryVO = new StateHistoryVO(govo.getId(), govo.getStatus(), stateDynAttributeVO.getCanonicalValue(getAttributeCache()));
			MasterDataVO<Long> mdvo = MasterDataWrapper.wrapStateHistoryVO(stateHistoryVO);
			getMasterDataFacade().create(mdvo, null);
		}

		final boolean useEventSupports = getUsesEventSupports(govo.getModule(), UpdateRule.class.getCanonicalName());
		
		if (bFireSaveEvent && useEventSupports) {
			LOG.debug("Modifying (Start rules)");
			try {
				govo = fireSaveEvent(UpdateRule.class.getCanonicalName(), new GenericObjectWithDependantsVO(govo, govo.getDependents(), govo.getDataLanguageMap()), customUsage, isCollectiveProcessing);
			} catch (NuclosCompileException e) {
				throw new CommonCreateException("fireSaveEvent failed", e);
			}
		}

		LOG.debug("Modifying (Start change genericobject)");

		EntityObjectVO<Long> eoUpdated = DalSupportForGO.wrapGenericObjectVO(govo);

		validationSupport.validate(eoUpdated, govo.getDependents());

		DalUtils.updateVersionInformation(eoUpdated, getCurrentUserName());
		eoUpdated.flagUpdate();

		Map<UID, UID> existingDocumentFileMap = Collections.emptyMap();
		if (MetaProvider.isDataEntity(eMeta, true)) {
			existingDocumentFileMap = DocumentFileUtils.getExistingDocumentFiles(eoUpdated);
			DocumentFileUtils.storeDocumentFiles(eoUpdated, existingDocumentFileMap);
		}

		try {
			IEntityObjectProcessor<Long> entityObjectProcessor = 
					nucletDalProvider.getEntityObjectProcessor(eoUpdated.getDalEntity());
			entityObjectProcessor.insertOrUpdate(eoUpdated);
		} catch (DbBusinessException ex) {
			throw new NuclosBusinessRuleException(ex.getMessage());
		}
		
		if (!existingDocumentFileMap.isEmpty()) {
			DocumentFileUtils.removeDocumentFiles(eoUpdated, existingDocumentFileMap);
		}
		
		getHistoryFacade().trackChangesToLogbook(dbEoVO, eoUpdated);

		if (govo.getDependents() != null) {
			LOG.debug("Modifying (Start change dependants)");

			// FIXME This doesn't work this way as there is no difference in the model between "set value to null" and "do not change value"
			if (false) {
				// TODO NUCLOS-6134 NUCLOS-6140 Centralize logic
				UID iStatus = govo.getAttribute(SF.STATE.getUID(govo.getModule())).getValueUid();

				IDependentDataMap dependants = govo.getDependents();
				for (IDependentKey dependentKey : dependants.getKeySet()) {
					Collection<EntityObjectVO<Long>> eoVos = RigidUtils.uncheckedCast(dependants.getData(dependentKey));
					FieldMeta<?> refFieldMeta = metaProvider.getEntityField(dependentKey.getDependentRefFieldUID());
					removeValuesFromForbiddenSubformColumns(iStatus, refFieldMeta.getEntity(), eoVos);
				}
			}

			try {
				getMasterDataFacade().modifyDependants(govo.getId(), govo.getDependents(), customUsage);
			} catch (DbBusinessException ex) {
				throw new NuclosBusinessRuleException(ex.getMessage());
			}
				
		}

		LOG.debug("Modifying (Start read)");
		GenericObjectVO result = get(dbGoVO.getModule(), dbGoVO.getId());
		GenericObjectWithDependantsVO go = new GenericObjectWithDependantsVO(result, new DependentDataMap(), result.getDataLanguageMap());
		
		final boolean useFinalEventSupports = getUsesEventSupports(govo.getModule(), UpdateFinalRule.class.getCanonicalName());
		
		if (bFireSaveEvent && useFinalEventSupports) {
			LOG.debug("Modifying (Start rules after save)");
			try {
				boolean tmpComplete = go.isComplete();
				go = this.fireSaveEvent(UpdateFinalRule.class.getCanonicalName(), go, customUsage, isCollectiveProcessing);
				go.setComplete(tmpComplete);
			} catch (NuclosCompileException e) {
				throw new CommonCreateException("fireSaveEvent failed", e);
			}
		}

		LOG.debug("Leaving modify(GenericObjectVO govo, DependantMasterDataMap mpDependants)");
		return go;
	}

	/**
	 * fires a Save event, executing the corresponding business rules.
	 * 
	 * §precondition Modules.getInstance().getUsesRuleEngine(govo.getModuleId().intValue())
	 */
	private GenericObjectWithDependantsVO fireSaveEvent(String event, GenericObjectWithDependantsVO govoDep, String customUsage, boolean isCollectiveProcessing) 
					throws NuclosBusinessRuleException, NuclosCompileException {
		govoDep = DalSupportForGO.getGenericObjectWithDependantsVO(
			getEventSupportFacade().fireSaveEventSupport(DalSupportForGO.wrapGenericObjectVO(govoDep), event, isCollectiveProcessing));
		return govoDep;
	}

	/**
	 * fires a Delete event, executing the corresponding business rules.
	 * 
	 * §precondition Modules.getInstance().getUsesRuleEngine(govo.getModuleId())
	 */
	private void fireDeleteEvent(String event, GenericObjectWithDependantsVO govo, String customUsage, boolean isLogical) 
			throws NuclosBusinessRuleException, NuclosCompileException {
		final UID sEntityName = govo.getModule();
		final UsageCriteria usage = govo.getUsageCriteria(customUsage);
		getEventSupportFacade().fireDeleteEventSupport(
				DalSupportForGO.wrapGenericObjectVO(govo), event, usage, isLogical);
	}

	/**
	 * delete generic object from database
	 * 
	 * §nucleus.permission mayDelete(module, bDeletePhysically)
	 * 
	 * @param id containing the generic object data
	 * @param bDeletePhysically remove from db?
	 * @deprecated use with customUsage
	 */
	@RolesAllowed("Login")
	@Deprecated
	public void remove(UID entity, Long id, boolean bDeletePhysically) 
			throws NuclosBusinessException, CommonFinderException, CommonRemoveException, CommonPermissionException, 
			CommonStaleVersionException, CommonCreateException {
		
		remove(entity, id, bDeletePhysically, ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
	}
	
	@RolesAllowed("Login")
	@CacheEvict(value="goMetaFields", key="#p1")
	public void remove(UID entity, Long id, boolean bDeletePhysically, String customUsage) 
			throws NuclosBusinessException, CommonFinderException, CommonRemoveException, CommonPermissionException, 
			CommonStaleVersionException, CommonCreateException {
		
		LOG.debug(
			"Entering remove(GenericObjectWithDependantsVO gowdvo, boolean bDeletePhysically)");

		if (id == null) {
			throw new NuclosFatalException("genericobject.facade.exception.2");//"Der Datensatz hat eine leere Id.");
		}
		
		final UID iModuleId;

		final GenericObjectVO dbGoVO = DalSupportForGO.getGenericObjectThin(id, entity);
		
		IDependentDataMap mpDependants = new DependentDataMap();
		final GenericObjectWithDependantsVO gowdvo = new GenericObjectWithDependantsVO(dbGoVO, mpDependants, dbGoVO.getDataLanguageMap());
		gowdvo.remove();

		iModuleId = dbGoVO.getModule();
		// String entity = metaProvider.getEntity(IdUtils.toLongId(iModuleId)).getEntity();

		checkDeleteAllowedForModule(iModuleId, dbGoVO.getId(), bDeletePhysically);
		grantUtils.checkDeleteInternal(iModuleId, dbGoVO.getId());
		lockUtils.checkLockedByCurrentUserInternal(iModuleId, dbGoVO.getId());
		mandatorUtils.checkWriteAllowedFromDb(id, MetaProvider.getInstance().getEntity(iModuleId));

		if (!bDeletePhysically) {
			gowdvo.setDeleted(true); // Only set deleted if logical deleted. This could be be useful in rules.
		}

		final Map<EntityAndField, UID> collSubEntities = getLayoutFacade().getSubFormEntityAndParentSubFormEntities(
			dbGoVO.getModule(), dbGoVO.getId(), false, customUsage);

		final boolean useEventSupports = getUsesEventSupports(iModuleId, DeleteRule.class.getCanonicalName());
		
		final UsageCriteria usage = gowdvo.getUsageCriteria(customUsage);
		
		Set<UID> stRequiredSubEntityNames = new HashSet<>();
		for (EntityAndField eafn : getLayoutFacade().getSubFormEntityAndParentSubFormEntityNamesByGO(usage).keySet()) {
			if (metaProvider.getEntity(eafn.getEntity()).isEditable()) {
				stRequiredSubEntityNames.add(eafn.getEntity());
			}
		}
		
		fillDependants(gowdvo, usage, stRequiredSubEntityNames);

		if (useEventSupports) {
			try {
				fireDeleteEvent(DeleteRule.class.getCanonicalName(), gowdvo, customUsage, !bDeletePhysically);
			} catch (NuclosCompileException e) {
				throw new CommonRemoveException("fireDeleteEvent failed: " + e, e);
			}
			
			gowdvo.getDependents().clear();
			fillDependants(gowdvo, usage, stRequiredSubEntityNames);
		}

		if (bDeletePhysically) {
			for (EntityAndField eafn : collSubEntities.keySet()) {
				// care only about subforms which are on the highest level
				if (collSubEntities.get(eafn) == null) {

					// mark all dependant data as removed
					for (EntityObjectVO mdVO : gowdvo.getDependents().getData(eafn.getDependentKey())) {
						mdVO.flagRemove();
						masterDataFacadeHelper.readAllDependents(
								mdVO.getPrimaryKey(), mdVO.getDependents(), mdVO.isFlagRemoved(), eafn.getEntity(), collSubEntities);
					}
				}
			}

			// remove dependant data
			try {
				masterDataFacadeHelper.removeDeletedDependants(gowdvo.getDependents(), customUsage, true);
			} catch (DbBusinessException ex) {
				throw new NuclosBusinessRuleException(ex.getMessage());
			}
			removeDependants(dbGoVO, customUsage);

			genericObjectFacadeHelper.removeDependantComments(dbGoVO.getId());
			genericObjectFacadeHelper.removeDependantDocuments(dbGoVO.getId());
			genericObjectFacadeHelper.removeStateHistoryBelonging(dbGoVO.getId());

			Delete<Long> delete;
			try {
				Long delId = dbGoVO.getId();
				delete = new Delete<>(delId, dbGoVO.getModule());
				DalSupportForGO.getEntityObjectProcessor(dbGoVO.getModule()).delete(delete);
				nucletDalProvider.getEOGenericObjectProcessor().delete(delete);
			} catch (DbBusinessException ex) {
				throw new NuclosBusinessRuleException(ex.getMessage());
			} catch (DbException e) {
				throw new NuclosBusinessException(e);
			}

			EntityObjectVO eoDeleted = DalSupportForGO.wrapGenericObjectVO(dbGoVO);
			if (MetaProvider.isDataEntity(metaProvider.getEntity(iModuleId), true)) {
				DocumentFileUtils.removeDocumentFiles(eoDeleted);
			}
			getHistoryFacade().trackRemoveToLogbook(eoDeleted);
		}
		else {
			EntityObjectVO eoToUpdate = DalSupportForGO.wrapGenericObjectVO(dbGoVO);

			eoToUpdate.setFieldValue(SF.LOGICALDELETED, Boolean.TRUE);
			DalUtils.updateVersionInformation(eoToUpdate, getCurrentUserName());
			eoToUpdate.flagUpdate();
			
			try {
				DalSupportForGO.getEntityObjectProcessor(dbGoVO.getModule()).insertOrUpdate(eoToUpdate);
			} catch (DbBusinessException ex) {
				throw new NuclosBusinessRuleException(ex.getMessage());
			} catch (DbException e) {
				throw new NuclosBusinessException(e);
			}
			
			getHistoryFacade().trackChangesToLogbook(eoToUpdate, eoToUpdate);
		}
		LOG.info("The entry {} (Id: {}) has been deleted.",
			         gowdvo.getSystemIdentifier(), gowdvo.getId());

		final boolean useFinalEventSupports = getUsesEventSupports(iModuleId, DeleteFinalRule.class.getCanonicalName());
		
		if(useFinalEventSupports) {
			try {
				this.fireDeleteEvent(DeleteFinalRule.class.getCanonicalName(), gowdvo, customUsage, !bDeletePhysically);
			} catch (NuclosCompileException e) {
				throw new CommonRemoveException("fireDeleteEvent failed: " + e, e);
			}
		}
		
		LOG.debug("Leaving remove(GenericObjectVO govo, boolean bDeletePhysically)");
	}

	/**
	 * Inserts or remove generic object data based on wether the entity is currently marked as a module.
	 *
	 * @param entityUID
	 * @throws NuclosBusinessRuleException
	 */
	@RolesAllowed("Login")
	public void updateGenericObjectEntries(UID entityUID) throws NuclosBusinessException {
		LOG.debug("Entering updateGenericObjectEntries(String sEntityName)");

		if (Modules.getInstance().isModule(entityUID)) {
			addGenericObjectEntries(entityUID);
		}
		else {
			removeGenericObjectEntries(entityUID);
		}

		LOG.debug("Leaving updateGenericObjectEntries(String sEntityName)");
	}

	/**
	 * See {@link #addGenericObjectEntries(EntityMeta)}
	 *
	 * @param entityUID
	 * @throws NuclosBusinessRuleException
	 */
	public void addGenericObjectEntries(final UID entityUID) throws NuclosBusinessException {
		addGenericObjectEntries(metaProvider.getEntity(entityUID));
	}

	/**
	 * Adds generic object data for the given entity.
	 *
	 * @param entityMeta
	 * @throws NuclosBusinessRuleException
	 */
	public void addGenericObjectEntries(final EntityMeta<?> entityMeta) throws NuclosBusinessException {
		final Collection<MasterDataVO<Long>> mdcvos = masterDataFacadeHelper.<Long>getGenericMasterData(entityMeta.getUID(), CollectableSearchExpression.TRUE_SEARCH_EXPR, true);
		for (MasterDataVO<Long> mdvo : mdcvos) {
			EOGenericObjectVO eoGenericObjectVO = new EOGenericObjectVO();
			eoGenericObjectVO.flagNew();
			eoGenericObjectVO.setPrimaryKey(mdvo.getPrimaryKey());
			eoGenericObjectVO.setEntityUID(entityMeta.getUID());
			DalUtils.updateVersionInformation(eoGenericObjectVO, getCurrentUserName());

			try {
				nucletDalProvider.getEOGenericObjectProcessor().insertOrUpdate(eoGenericObjectVO);
				final IEntityObjectProcessor<Long> entityObjectProcessor = DalSupportForGO.getEntityObjectProcessor(entityMeta.getUID());

				EntityObjectVO dalVO = entityObjectProcessor.getByPrimaryKey(mdvo.getPrimaryKey());

				final String sCanonicalValueSystemIdentifier = BusinessIDFactory.generateSystemIdentifier(entityMeta);
				assert !StringUtils.isNullOrEmpty(sCanonicalValueSystemIdentifier);
				dalVO.setFieldValue(SF.SYSTEMIDENTIFIER, sCanonicalValueSystemIdentifier);

				dalVO.setSystemFieldsOnRequest(true);
				dalVO.flagUpdate();

				entityObjectProcessor.insertOrUpdate(dalVO);
			}
			catch (DbException e) {
				throw new NuclosBusinessException(e);
			}
		}
	}

	/**
	 * Deletes generic object data for the given entity.
	 *
	 * @param entityUID uid of the entity
	 * @throws NuclosBusinessRuleException
	 */
	public void removeGenericObjectEntries(final UID entityUID) {
		genericObjectFacadeHelper.removeDependantComments(entityUID);
		genericObjectFacadeHelper.removeDependantDocuments(entityUID);
		genericObjectFacadeHelper.removeStateHistoryBelonging(entityUID);

		// use plain delete here. @see RSWORGA-66
		final String sDBEntity = metaProvider.getEntity(entityUID).getDbTable();

		String sql = "DELETE FROM T_UD_GO_RELATION WHERE" +
				" INTID_T_UD_GO_1 IN (SELECT INTID FROM " + sDBEntity + ")" +
						"OR INTID_T_UD_GO_2 IN (SELECT INTID FROM " + sDBEntity + ")";
		dataBaseHelper.getDbAccess().executePlainUpdate(sql);

		sql = "DELETE FROM T_UD_GENERICOBJECT WHERE INTID IN (SELECT INTID FROM " + sDBEntity + ")";
		dataBaseHelper.getDbAccess().executePlainUpdate(sql);
	}

	// replaces the ejbRemove() from GenericObjectBean
	private void removeDependants(GenericObjectVO govo, String customUsage) 
			throws CommonFinderException, CommonPermissionException, NuclosBusinessRuleException, 
			CommonStaleVersionException, CommonRemoveException {
		

		// delete all dependant leased object relations:
		for (GenericObjectRelationVO vo : findRelationsByGenericObjectId(govo.getId())) {
			// E.GENERICOBJECTRELATION
			getMasterDataFacade().remove(E.GENERICOBJECTRELATION.getUID(), vo.getPrimaryKey(), false, customUsage);
		}

		if (metaProvider.getEntity(govo.getModule()).isStateModel()) {
			// delete all dependant state history entries:
			StateFacadeLocal stateFacade = beanFactory.getBean(StateFacadeLocal.class);
			for (StateHistoryVO history : stateFacade.findStateHistoryByGenericObjectId(govo.getId()))
			{
				// E.STATEHISTORY
				getMasterDataFacade().remove(E.STATEHISTORY.getUID(), history.getPrimaryKey(), false, customUsage);
			}
		}
	}

	/**
	 * restore GO marked as deleted
	 */
	@Override
	public void restore(UID entity, Long iId, String customUsage)throws CommonBusinessException {
		LOG.debug("Entering restore(GenericObjectWithDependantsVO gowdvo)");

		GenericObjectWithDependantsVO goVO = this.getWithDependants(entity, iId, customUsage);
		goVO.setDeleted(false);
		modify(entity, goVO, customUsage); // NUCLOSINT-890

		LOG.debug("Leaving retstore(GenericObjectVO govo)");
	}

	/**
	 * method to get logbook entries for a generic object
	 * 
	 * §precondition Modules.getInstance().isLogbookTracking(this.getModuleContainingGenericObject(iGenericObjectId))
	 * §nucleus.permission mayRead(module)
	 *
	 * @param iGenericObjectId id of generic object to get logbook entries for
	 * @param iAttributeId		id of attribute to get logbook entries for
	 * @return collection of logbook entry value objects
	 */
	@RolesAllowed("Login")
	@Override
	public Collection<LogbookVO> getLogbook(Long iGenericObjectId, UID iAttributeId)
			throws CommonFinderException, CommonPermissionException {

		final UID iModuleId = getModuleContainingGenericObject(iGenericObjectId);

		if (!Modules.getInstance().isLogbookTracking(iModuleId)) {
			throw new CommonFatalException("genericobject.facade.exception.3");
		}

		checkReadAllowedForModule(iModuleId);

		final Collection<LogbookVO> result = new HashSet<LogbookVO>();

	
		CollectableSearchCondition cond = SearchConditionUtils.newIdComparison(E.GENERICOBJECTLOGBOOK.genericObject, ComparisonOperator.EQUAL,iGenericObjectId);

		if (iAttributeId != null) {
			cond = SearchConditionUtils.and(cond, SearchConditionUtils.newUidComparison(E.GENERICOBJECTLOGBOOK.attributeId, ComparisonOperator.EQUAL, iAttributeId));
		}

		Collection<MasterDataVO<Long>> logbook = getMasterDataFacade().getMasterData(E.GENERICOBJECTLOGBOOK, cond);
   		for (MasterDataVO mdVO : logbook) {
			final UID iAttributeIdLogbook = mdVO.getFieldUid(E.GENERICOBJECTLOGBOOK.attributeId);
			if (iAttributeIdLogbook == null) {
				addLogbookIfPossible(result, mdVO);
			}
			else if (!getAttributeCache().contains(iAttributeIdLogbook)) {
				// This may be the case when there is an old attribute entry in the logbook:
				addLogbookIfPossible(result, mdVO);
			}
			else {
				/** @todo consider attribute group permissions here ! */
				final AttributeCVO attrcvo = getAttributeCache().getAttribute(iAttributeIdLogbook);
				if (attrcvo.isLogbookTracking()/*&& (SecurityCache.getInstance().getAttributegroupsRO(getCurrentUserName()).contains(attrcvo.getAttributegroupId()))*/)
				{
					addLogbookIfPossible(result, mdVO);
				}
			}
		}

      return result;
	}

	private void addLogbookIfPossible(final Collection<LogbookVO> result, MasterDataVO mdVO) {
		try {
			result.add(MasterDataWrapper.getLogbookVO(mdVO));
		}
		catch(Exception e) {
			// logbook entry can't be added; skip value
			LOG.info("addLogbookIfPossible: " + e);
		}
	}

	/**
	 * relates a generic object to another generic object.
	 * 
	 * §precondition isGenericObjectInModule(iModuleIdTarget, iGenericObjectIdTarget)
	 * §precondition Modules.getInstance().isMainModule(iModuleIdTarget.intValue())
	 * §nucleus.permission mayWrite(targetModule)
	 * 
	 * @param iModuleIdTarget module id of target generic object to be related
	 * @param iGenericObjectIdTarget id of target generic object to be related
	 * @param iGenericObjectIdSource id of source generic object to be related to
	 * @param relationType relation type
	 */
	@RolesAllowed("Login")
	@Override
	public void relate(UID iModuleIdTarget, Long iGenericObjectIdTarget,
			Long iGenericObjectIdSource, String relationType)
			throws CommonFinderException, CommonCreateException, CommonPermissionException, NuclosBusinessRuleException {

		relate(iModuleIdTarget, iGenericObjectIdTarget, iGenericObjectIdSource, relationType, null, null, null);
	}

	/**
	 * relates a generic object to another generic object.
	 * 
	 * §precondition isGenericObjectInModule(iModuleIdTarget, iGenericObjectIdTarget)
	 * §precondition Modules.getInstance().isMainModule(iModuleIdTarget.intValue())
	 * §nucleus.permission mayWrite(targetModule)
	 * 
	 * @param iModuleIdTarget module id of target generic object to be related (i.e. of request)
	 * @param iGenericObjectIdTarget id of target generic object to be related (i.e. request)
	 * @param iGenericObjectIdSource id of source generic object to be related to (i.e. demand)
	 * @param relationType relation type
	 * @param dateValidFrom Must be <code>null</code> for system defined relation types.
	 * @param dateValidUntil Must be <code>null</code> for system defined relation types.
	 * @param sDescription Must be <code>null</code> for system defined relation types.
	 */
	@RolesAllowed("Login")
	// @Override
	public void relate(UID iModuleIdTarget, Long iGenericObjectIdTarget,
			Long iGenericObjectIdSource, String relationType, Date dateValidFrom, Date dateValidUntil,
			String sDescription) throws CommonFinderException, CommonCreateException, CommonPermissionException, NuclosBusinessRuleException {

		if (!isGenericObjectInModule(iModuleIdTarget, iGenericObjectIdTarget)) {
			throw new IllegalArgumentException("iModuleIdTarget");
		}

		//this.checkWriteAllowedForModule(iModuleIdTarget, iGenericObjectIdSource); // why do we have do check if write is allowed here. ?? especially we have to have the sourceModuleId here - not the moduleId from the target
		this.checkWriteAllowedForModule(iModuleIdTarget);

		GenericObjectRelationVO relationVO = new GenericObjectRelationVO(
			new NuclosValueObject(),iGenericObjectIdSource, iGenericObjectIdTarget, relationType, dateValidFrom, dateValidUntil, sDescription);
		// E.GENERICOBJECTRELATION
		getMasterDataFacade().create(MasterDataWrapper.wrapGenericObjectRelationVO(relationVO), null);
	}

	/**
	 * removes the relation with the given id.
	 * 
	 * §nucleus.permission mayWrite(targetModule)
	 * 
	 * @param mpGOTreeNodeRelation
	 * @throws CommonRemoveException
	 * @throws CommonFinderException
	 * @throws CommonPermissionException
	 */
	@RolesAllowed("Login")
	@Override
	public void removeRelation(Map<Long, GenericObjectTreeNode> mpGOTreeNodeRelation) throws CommonBusinessException, CommonRemoveException, CommonFinderException {
		for(Long iRelationId : mpGOTreeNodeRelation.keySet()) {
			removeRelation(iRelationId, mpGOTreeNodeRelation.get(iRelationId).getId(), mpGOTreeNodeRelation.get(iRelationId).getEntityUID());
		}
	}

	/**
	 * removes the relation with the given id.
	 * Note that only the relation id is really needed, the other arguments are for security only.
	 * 
	 * §precondition isGenericObjectInModule(iModuleIdTarget, iGenericObjectIdTarget)
	 * §precondition Modules.getInstance().isMainModule(iModuleIdTarget)
	 * §nucleus.permission mayWrite(targetModule)
	 * 
	 * @param iRelationId the id of the relation
	 * @param iGenericObjectIdTarget the id of the target object
	 * @param iModuleIdTarget the module id of the target object
	 * @throws CommonRemoveException
	 * @throws CommonFinderException
	 * @throws CommonPermissionException 
	 * @throws CommonStaleVersionException 
	 * @throws NuclosBusinessRuleException 
	 */
	@RolesAllowed("Login")
	@Override
	public void removeRelation(Long iRelationId, Long iGenericObjectIdTarget, UID iModuleIdTarget)
			throws CommonRemoveException, CommonFinderException, CommonPermissionException, 
			NuclosBusinessRuleException, CommonStaleVersionException {

		if (!isGenericObjectInModule(iModuleIdTarget, iGenericObjectIdTarget)) {
			throw new IllegalArgumentException("iModuleIdTarget");
		}
		this.checkWriteAllowedForModule(iModuleIdTarget);
		MasterDataVO<Long> mdVO = getMasterDataFacade().get(E.GENERICOBJECTRELATION, iRelationId);
		GenericObjectRelationVO relationVO = MasterDataWrapper.getGenericObjectRelationVO(mdVO);
		if (!relationVO.getDestinationGOId().equals(iGenericObjectIdTarget)) {
			throw new IllegalArgumentException("iGenericObjectIdTarget and iRelationId don't match.");
		}
		// E.GENERICOBJECTRELATION
		getMasterDataFacade().remove(E.GENERICOBJECTRELATION.getUID(), mdVO.getPrimaryKey(), false, null);
	}

	/**
	 * unrelates a generic object from another generic object. All relations between these objects with the given
	 * relation type are removed.
	 * 
	 * §precondition isGenericObjectInModule(iModuleIdTarget, iGenericObjectIdTarget)
	 * §precondition Modules.getInstance().isMainModule(iModuleIdTarget.intValue())
	 * §nucleus.permission mayWrite(targetModule)
	 * 
	 * @param iModuleIdTarget module id of target generic object to be unrelated (i.e. request)
	 * @param iGenericObjectIdTarget id of target generic object to be unrelated (i.e. request)
	 * @param iGenericObjectIdSource id of source generic object to be unrelated (i.e. demand)
	 * @param relationType
	 */
	public void unrelate(UID iModuleIdTarget, Long iGenericObjectIdTarget, Long iGenericObjectIdSource, String relationType)
			throws CommonFinderException, NuclosBusinessRuleException, CommonPermissionException, CommonRemoveException {

		if (!isGenericObjectInModule(iModuleIdTarget, iGenericObjectIdTarget)) {
			throw new IllegalArgumentException("iModuleIdTarget");
		}

		this.checkWriteAllowedForModule(iModuleIdTarget);

		try {
			for (GenericObjectRelationVO vo : findRelations(iGenericObjectIdSource, relationType, iGenericObjectIdTarget))
			{
				// E.GENERICOBJECTRELATION
				getMasterDataFacade().remove(E.GENERICOBJECTRELATION.getUID(), vo.getPrimaryKey(), false, null);
			}
		}
		catch (CommonStaleVersionException ex) {
			throw new NuclosFatalException(ex);
		}
	}

	/**
	 * finds relations between two given generic objects.
	 * 
	 * §postcondition result != null
	 * 
	 * @param iGenericObjectIdSource
	 * @param relationType
	 * @param iGenericObjectIdTarget
	 * @return the relation.
	 */
	public Collection<GenericObjectRelationVO> findRelations(Long iGenericObjectIdSource, String relationType, Long iGenericObjectIdTarget) throws CommonFinderException, CommonPermissionException {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<Long> query = builder.createQuery(Long.class);
		DbFrom t = query.from(E.GENERICOBJECTRELATION);
		query.select(t.baseColumn(SF.PK_ID));
		query.where(builder.and(
			builder.equalValue(t.baseColumn(E.GENERICOBJECTRELATION.source), iGenericObjectIdSource),
			builder.equalValue(t.baseColumn(E.GENERICOBJECTRELATION.destination), iGenericObjectIdTarget),
			builder.equalValue(t.baseColumn(E.GENERICOBJECTRELATION.relationType), relationType)));
		return findRelationsImpl(query);
	}

	// @Override
	public Collection<GenericObjectRelationVO> findRelationsByGenericObjectId(Long iGenericObjectId) throws CommonFinderException, CommonPermissionException {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<Long> query = builder.createQuery(Long.class);
		DbFrom t = query.from(E.GENERICOBJECTRELATION);
		query.select(t.baseColumn(SF.PK_ID));
		query.where(builder.or(
			builder.equalValue(t.baseColumn(E.GENERICOBJECTRELATION.source), iGenericObjectId),
			builder.equalValue(t.baseColumn(E.GENERICOBJECTRELATION.destination), iGenericObjectId)));
		return findRelationsImpl(query);
	}

	private Collection<GenericObjectRelationVO> findRelationsImpl(DbQuery<Long> query) throws CommonFinderException, CommonPermissionException {
		
		Collection<GenericObjectRelationVO> result = new ArrayList<>();
		for (Long id : dataBaseHelper.getDbAccess().executeQuery(query.distinct(true))) {
			result.add(MasterDataWrapper.getGenericObjectRelationVO(getMasterDataFacade().get(E.GENERICOBJECTRELATION, id)));
		}
		return result;
	}

	/**
	 * checks if a given generic object belongs to a given module.
	 * @param iModuleId			 id of module to validate generic object for
	 * @param iGenericObjectId id of generic object to check
	 * @return true if in module or false if not
	 */
	// @Override
	public boolean isGenericObjectInModule(UID iModuleId, Long iGenericObjectId) throws CommonFinderException {
		return LangUtils.equal(this.getModuleContainingGenericObject(iGenericObjectId), iModuleId);
	}

	/**
	 * @param iGenericObjectId
	 * @return the id of the module containing the generic object with the given id.
	 * @throws CommonFinderException if there is no generic object with the given id.
	 */
	@RolesAllowed("Login")
	@Override
	public UID getModuleContainingGenericObject(Long iGenericObjectId) throws CommonFinderException {
		final EOGenericObjectVO eogo = nucletDalProvider.getEOGenericObjectProcessor()
				.getByPrimaryKey(iGenericObjectId);
		if (eogo == null) {
			throw new CommonFinderException();
		}
		if (eogo.getEntityUID() == null) {
			throw new CommonFatalException("moduleId is null");
		}
		return eogo.getEntityUID();
	}

	/**
	 * @param iGenericObjectId
	 * @return the id of the state of the generic object with the given id
	 * @throws CommonFinderException
	 */
	@RolesAllowed("Login")
	@Override
	public UID getStateByGenericObject(Long iGenericObjectId) throws CommonFinderException{
		EntityObjectVO eo = DalSupportForGO.getEntityObject(iGenericObjectId);
		return findAttributeByGoAndAttributeId(iGenericObjectId, SF.STATE.getUID(eo.getDalEntity())).getValueUid();
	}

	/**
	 * attaches document to any generic object and default attachment subform
	 * 
	 * §todo restrict permission - check module id! requires the right to modify documents
	 * 
	 * @param mdvoDocument master data value object with fields for table t_ud_go_document, has to be filled
	 * with following fields: comment, createdDate, createdUser, file and genericObjectId.
	 */
	@RolesAllowed("Login")
	public void attachDocumentToObject(MasterDataVO<Long> mdvoDocument) {
		this.attachDocumentToObject(mdvoDocument, E.GENERALSEARCHDOCUMENT.genericObject.getUID());
	}
	
	/**
	 * attaches document to any generic object
	 * 
	 * §todo restrict permission - check module id! requires the right to modify documents
	 * 
	 * @param mdvoDocument master data value object with fields for table of type t_ud_go_document, has to be filled
	 * with following fields: comment, createdDate, createdUser, file and genericObjectId.
	 * @param parentAttribute referencing attribute to parent object
	 */
	@RolesAllowed("Login")
	@Override
	public void attachDocumentToObject(MasterDataVO<Long> mdvoDocument, UID parentAttribute) {
		try {
			IDependentDataMap map = new DependentDataMap();
			IDependentKey dependentKey = DependentDataMap.createDependentKey(parentAttribute);
			map.addData(dependentKey, mdvoDocument.getEntityObject());
			
			final Long iGenericObjectId = mdvoDocument.getFieldId(parentAttribute);
			genericObjectFacadeHelper.createDependants(getModuleContainingGenericObject(iGenericObjectId), iGenericObjectId, map, Collections.EMPTY_MAP, null);
		} catch (CommonCreateException | CommonFinderException ex) {
			throw new NuclosFatalException(ex);
		}
	}

	/**
	 * returns the GenericObjectAttribute matching the given genericObjectId and attributeId
	 * @param genericObjectId
	 * @param attributeId
	 * @return the DynamicAttributeVO matching the given genericObjectId and attributeId
	 */
	// @Override
	public DynamicAttributeVO findAttributeByGoAndAttributeId(Long genericObjectId, UID attributeId) throws CommonFinderException {
		EntityObjectVO eo = DalSupportForGO.getEntityObject(genericObjectId);
		return DalSupportForGO.getDynamicAttributeVO(eo, attributeId);
	}
	
	private boolean getUsesEventSupports(UID entityUID, String sEventSupportType) {
		return getEventSupportFacade().getUsesEventSupportForEntity(entityUID, sEventSupportType);
	}
	
	/**
	 * handle autonumber generation
	 * 
	 * @param evo	entity vo
	 */
	private final void handleAutoNumber(final EntityObjectVO evo) throws NuclosBusinessRuleException {
		// find autonumber field
		// @TODO ignore System entities?
		final FieldMeta<Integer> metaFieldVO = autoNumberHelper.findAutoNumberFieldIfAny(evo.getDalEntity());
		if (null == metaFieldVO) {
			return;
		}
		// next autonumber if autonumberfield exists
		evo.setFieldValue(metaFieldVO.getUID(), autoNumberHelper.findNextAutoNumber(evo, metaFieldVO));
	}
	
	@RolesAllowed("Login")
	@Cacheable(value="goMetaFields", key="#p0")
	public UsageCriteria getGOMeta(Long id, UID entity, String custom) {

		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom t = query.from(metaProvider.getEntity(entity));
		DbField<?> processField = SF.PROCESS_UID.getMetaData(entity);
		DbField<?> stateField = SF.STATE_UID.getMetaData(entity);
		query.multiselect(t.baseColumn(processField), t.baseColumn(stateField));
		query.where(builder.equalValue(t.baseColumn(SF.PK_ID), id));
		DbTuple tuple = dataBaseHelper.getDbAccess().executeQuerySingleResult(query);
		
		UID processUID = (UID) tuple.get(processField);
		UID stateUID = (UID) tuple.get(stateField);
		if (stateUID == null) {
			UsageCriteria usagecriteria = new UsageCriteria(entity, processUID, null, custom);
			stateUID = SpringApplicationContextHolder.getBean(StateFacadeLocal.class).getInitialState(usagecriteria).getId();
		}
		
		return new UsageCriteria(entity, processUID, stateUID, custom);
	}


	/**
	 * gets all generic objects that match a given search condition
	 *
	 * §postcondition result != null
	 *
	 * @param iModuleId id of module to search for generic objects in
	 * @param clctexpr
	 * @param stRequiredSubEntityNames
	 * @return list of generic object value objects with specified dependants and without parent objects!
	 */
	private List<GenericObjectWithDependantsVO> getGenericObjectsImpl(UID iModuleId, CollectableSearchExpression clctexpr,
																 Set<UID> stRequiredSubEntityNames, String username, String customUsage, UID mandator) {

		final List<GenericObjectWithDependantsVO> result = new ArrayList<>();
		clctexpr.setSearchCondition(mandatorUtils.append(clctexpr, metaProvider.getEntity(iModuleId)));

		CollectableSearchExpression clctexpr2 = grantUtils.append(clctexpr, iModuleId);

		for (EntityObjectVO eo : nucletDalProvider.getEntityObjectProcessor(iModuleId).getBySearchExprResultParams(
				clctexpr2, new ResultParams(null, clctexpr.getSortingOrder() != null))) {

			GenericObjectVO genericObjectVO = DalSupportForGO.getGenericObjectVO(eo);
			final GenericObjectWithDependantsVO go = new GenericObjectWithDependantsVO(
					genericObjectVO, new DependentDataMap(), genericObjectVO.getDataLanguageMap());
			try {
				fillDependants(go, go.getUsageCriteria(customUsage), stRequiredSubEntityNames);
			}
			catch(CommonFinderException e) {
				throw new NuclosFatalException(e);
			}
			result.add(go);
		}

		assert result != null;
		return result;
	}

	/**
	 * fills the dependants of <code>lowdcvo</code> with the data from the required sub entities.
	 *
	 * §precondition stRequiredSubEnties != null
	 *
	 */
	private void fillDependants(GenericObjectWithDependantsVO lowdcvo, UsageCriteria usage,
								Set<UID> stRequiredSubEnties) throws CommonFinderException {
		fillDependents(lowdcvo, usage, stRequiredSubEnties, false, -1);
	}

	private void fillDependents(GenericObjectWithDependantsVO lowdcvo, UsageCriteria usage,
								Set<UID> stRequiredSubEnties, boolean loadingSubform, final int dependantsDepth) throws CommonFinderException {

		if (stRequiredSubEnties == null || stRequiredSubEnties.isEmpty()) {
			return;
		}

		UID statusUID = lowdcvo.getAttribute(SF.STATE.getUID(lowdcvo.getModule())).getValueUid();

		final Map<EntityAndField, UID> mapSubEntities =
				getLayoutFacade().getSubFormEntityAndParentSubFormEntityNamesByGO(usage);

		masterDataFacadeHelper.fillDependentsRecursive(mapSubEntities, stRequiredSubEnties, new MasterDataFacadeHelper.IFillDependentPostProcessor() {
			@Override
			public void postProcess(final UID subEntity, final Collection collmdvo) {
				if (!loadingSubform) {
					// NUCLOS-6134, NUCLOS-6140
					removeValuesFromForbiddenSubformColumns(statusUID, subEntity, collmdvo);
				}
			}
		}, getCurrentUserName(), lowdcvo.getId(), lowdcvo.getDependents(), null, dependantsDepth);
	}

}
