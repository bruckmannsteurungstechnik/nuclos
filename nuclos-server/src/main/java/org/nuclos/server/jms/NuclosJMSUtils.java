//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.jms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

/**
 * TODO: This should be a real SINGLETON, i.e. only ONE static method (getInstance()).
 * All other static method should be turned into methods of the SINGLETON.
 */
public class NuclosJMSUtils {
	
	private static final Logger LOG = LoggerFactory.getLogger(NuclosJMSUtils.class);
	
	public static void sendMessageAfterCommit(final String sMessageText, String topic) {
		sendMessageAfterCommit(sMessageText, topic, null);
	}
	
	public static void sendMessage(final String sMessageText, String topic) {
		sendMessage(sMessageText, topic, null);
	}
	
	public static void sendMessageAfterCommit(final String sMessageText, final String topic, final String sReceiver) {
		TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
			
			@Override
			public void afterCommit() {
				LOG.debug("afterCommit: {} JMS send: topic={} receiver={}: {}",
				          this, topic, sReceiver, sMessageText);
				sendMessage(sMessageText, topic, sReceiver);
			}
			
		});
	}
	
	public static void sendMessage(final String sMessageText, final String topic, final String sReceiver) {
		try {
			final JmsTemplate jmsTemplate = getTemplate(topic);
			if (jmsTemplate == null) {
				// spring context is already destroyed
				return;
			}
	    	jmsTemplate.send(new MessageCreator() {
				
				@Override
				public Message createMessage(Session session) throws JMSException {
					Message message = session.createTextMessage(sMessageText);
					if(sReceiver != null)
						message.setJMSCorrelationID(sReceiver);
					logSendMessage(topic, sMessageText, message);
					return message;
				}
			});
    	}
    	catch(Exception ex) {
    		throw new NuclosFatalException(ex);
    	}
	}
	
	public static void sendObjectMessageAfterCommit(final Serializable object, final String topic, final String sReceiver) {
		// TransactionSynchronizationManager-Solution for RichClient
		TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
			
			@Override
			public void afterCommit() {
				LOG.debug("afterCommit: {} JMS object send: topic={} receiver={}: {}",
				          this, topic, sReceiver, object);
				sendObjectMessage(object, topic, sReceiver);
			}
			
		});

		// RequestContextHolder-Solution for RestClient
		final ArrayList<Serializable> commands = getOrCreateRequestCommands();
		commands.add(object);
	}

	private static ArrayList<Serializable> getOrCreateRequestCommands() {
		ArrayList<Serializable> result = new ArrayList<>();

		// RequestContextHolder-Solution for RestClient
		final RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
		if (requestAttributes != null) {
			Object cmd = requestAttributes.getAttribute("CMD", RequestAttributes.SCOPE_REQUEST);
			if (cmd instanceof ArrayList) {
				result = (ArrayList<Serializable>) cmd;
			} else {
				requestAttributes.setAttribute("CMD", result, RequestAttributes.SCOPE_REQUEST);
			}
		} else {
			LOG.warn("Could not add commands - no RequestAttributes");
		}

		return result;
	}

	/**
	 * Commit or Rollback
	 * @param object
	 * @param topic
	 * @param sReceiver
	 */
	public static void sendObjectMessageAfterCompletion(final Serializable object, final String topic, final String sReceiver) {
		TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
			@Override
			public void afterCompletion(int status) {
				LOG.debug("afterCompletion: {} JMS object send: topic={} receiver={}: {}",
				          this, topic, sReceiver, object);
				sendObjectMessage(object, topic, sReceiver);
			}
		});		
	}
	
	public static void sendObjectMessage(final Serializable object, final String topic, final String sReceiver) {
		try {
			final JmsTemplate jmsTemplate = getTemplate(topic);
			if (jmsTemplate == null) {
				// spring context is already destroyed
				return;
			}
	    	jmsTemplate.send(new MessageCreator() {
				
				@Override
				public Message createMessage(Session session) throws JMSException {
					Message message = session.createObjectMessage(object);
					if(sReceiver != null)
						message.setJMSCorrelationID(sReceiver);
					logSendMessage(topic, object, message);
					return message;
				}
			});
    	}
    	catch(Exception ex) {
    		throw new NuclosFatalException(ex);
    	}
	}
	
	public static void sendOnceAfterCommitDelayed(String text, String topic) {
		final JMSOnceAfterCommitSynchronization ts = getJMSOnceAfterCommitSynchronization();
		ts.queue(topic, text);
	}
	
	public static void sendOnceAfterCommitDelayed(Serializable object, String topic) {
		final JMSOnceAfterCommitSynchronization ts = getJMSOnceAfterCommitSynchronization();
		ts.queue(topic, object);
	}
	
	private static JMSOnceAfterCommitSynchronization getJMSOnceAfterCommitSynchronization() {
		final List<TransactionSynchronization> txSyncs = TransactionSynchronizationManager.getSynchronizations();
		for (TransactionSynchronization ts: txSyncs) {
			if (ts instanceof JMSOnceAfterCommitSynchronization) {
				return (JMSOnceAfterCommitSynchronization) ts;
			}
		}
		final JMSOnceAfterCommitSynchronization tsNew = new JMSOnceAfterCommitSynchronization(new JMSSendToGlobalQueue());
		TransactionSynchronizationManager.registerSynchronization(tsNew);
		return tsNew;
	}
	
	private static JmsTemplate getTemplate(String topic) {
		if (SpringApplicationContextHolder.isDestroyed()) {
			return null;
		}
		final ApplicationContext context = SpringApplicationContextHolder.getApplicationContext();
    	final JmsTemplate jmsTemplate = (JmsTemplate)context.getBean(topic);
    	return jmsTemplate;
	}
	
	private static void logSendMessage(String topic, Object body, Message msg) throws JMSException {
		LOG.debug("JMS send to topic '{}' msg={} details={}",
		          topic, body, toString(msg));
	}
	
	private static String toString(Message msg) throws JMSException {
		final StringBuilder result = new StringBuilder();
		result.append("Message[");
		result.append("type=").append(msg.getJMSType());
		result.append(",corrId=").append(msg.getJMSCorrelationID());
		result.append(",msgId=").append(msg.getJMSMessageID());
		result.append("]");
		return result.toString();
	}
	
}
