package org.nuclos.server.job;

import java.util.Properties;

import javax.sql.DataSource;

import org.nuclos.common.NuclosFatalException;
import org.nuclos.server.cluster.NuclosClusterRegisterHelper;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeLocal;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;


public class NuclosSchedulerFactoryBean extends SchedulerFactoryBean {

	private static final Logger LOG = LoggerFactory.getLogger(NuclosSchedulerFactoryBean.class);

	Scheduler scheduler;

	int startupDelay;

	@Autowired
	private EventSupportFacadeLocal eventSupportFacadeLocal;
	
	public NuclosSchedulerFactoryBean() {
	}
	
	@Override
	protected void startScheduler(Scheduler scheduler, int startupDelay) throws SchedulerException {
		
		if(this.scheduler == null) {
			this.scheduler = scheduler;
			this.startupDelay = startupDelay;			
		}
		else {
			super.startScheduler(scheduler, startupDelay);
		}
		
	}
	
	public void startNuclosScheduler(boolean bForce) {
		LOG.info("Starting Quartz");
		try {
			this.startScheduler(scheduler, startupDelay);
		} catch (SchedulerException e) {
			throw new NuclosFatalException(e);
		}
	}
	
	public void startNuclosScheduler() {
		if(NuclosClusterRegisterHelper.runInClusterMode() && NuclosClusterRegisterHelper.iAmSlave()) {
			LOG.info("Server is Slave no Quartz!");
		}
		else {
			LOG.info("Starting Quartz");
			try {
				this.startScheduler(scheduler, startupDelay);
			} catch (SchedulerException e) {
				throw new NuclosFatalException(e);
			}
		}
	}
	
	/**
	 * Attention: This is called as part of @PostConstruct !
	 */
	protected Scheduler createScheduler(SchedulerFactory schedulerFactory, String schedulerName)
			throws SchedulerException {
		
		// Not necessary, jobs start after creating in AutoDbSetupComplete...
//		try {
//			eventSupportFacadeLocal.createBusinessObjects(true);
//		} catch(NuclosCompileException e) {
//			LOG.warn("Compile errors while creating business objects: " + e, e);
//		} catch (Exception e) {
//			LOG.error("NuclosSchedulerFactoryBean: Error creating BusinsessObject: " + e, e);
//		}
		return super.createScheduler(schedulerFactory, schedulerName);
	}

	/**
	 * Ensure to use RAMJobStore
	 */
	@Override
	public void setQuartzProperties(Properties quartzProperties) {
		quartzProperties.remove("org.quartz.jobStore.driverDelegateClass");
		quartzProperties.remove("org.quartz.jobStore.class");
		quartzProperties.remove("org.quartz.jobStore.tablePrefix");
		quartzProperties.remove("org.quartz.jobStore.selectWithLockSQL");
		quartzProperties.remove("org.quartz.jobStore.txIsolationLevelSerializable");
		quartzProperties.remove("org.quartz.jobStore.acquireTriggersWithinLock");
		// only use RAM
		quartzProperties.setProperty("org.quartz.jobStore.class", "org.quartz.simpl.RAMJobStore");
		
		super.setQuartzProperties(quartzProperties);
	}
	
	@Override
	public void setDataSource(DataSource dataSource) {
	}
	
	@Override
	public void setNonTransactionalDataSource(DataSource nonTransactionalDataSource) {
	}
	
}
