//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.job;

import org.nuclos.common.NuclosFatalException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.common.ServerServiceLocator;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeLocal;
import org.nuclos.server.job.valueobject.JobVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Quartz job to execute all rules with event type "Frist".
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Lars.Rueckemann@novabit.de">Lars Rueckemann</a>
 * @version 01.00.00
 */
public class TimelimitJob extends SchedulableJob implements NuclosJob {//NuclosQuartzJob {

	private static final Logger LOG = LoggerFactory.getLogger(TimelimitJob.class);

	@Override
	public String execute(JobVO jobVO, Long iSessionId) throws CommonPermissionException, NuclosCompileException, NuclosFatalException {
		//execute job rules

		final EventSupportFacadeLocal eventSupportFacade = 
				ServerServiceLocator.getInstance().getFacade(EventSupportFacadeLocal.class);
		
		// Execute all eventSupports for this job
		eventSupportFacade.fireTimelimitEventSupport(jobVO.getId(), iSessionId);
		
		return null;
	}

}	// class TimeLimitJob
