package org.nuclos.server.livesearch.ejb3;

import java.io.IOException;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.lucene.store.LockObtainFailedException;
import org.nuclos.common.lucene.CompleteTransaction;
import org.nuclos.common.lucene.IndexStep;
import org.nuclos.common.lucene.IndexVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IndexerHelper {
	private static final Logger LOG = LoggerFactory.getLogger(IndexerHelper.class);

	private final Queue<IndexStep> indexQueue = new LinkedBlockingQueue<IndexStep>(16384);
	private Thread indexThread;
	private NuclosIndexWriter indexWriter;
	
	public void addToQueue(IndexStep indexStep) {
		if (IndexTools.INSTANCE().switchedOff()) {
			return;
		}
					
		if (!indexQueue.offer(indexStep)) {
			//IndexQueue is full, so force one object to be indexed and try again.
			
			if (pollAndWorkOneIndexStep() == EMPTY_QUEUE && !indexQueue.offer(indexStep)) {
				//Something really got wrong. This index step is lost.
				LOG.error("IndexQueue is full and this can't be added:" + indexStep);
				return;
			}
		}

		if (IndexTools.INSTANCE().isSynchronous()) {
			pollAndWorkOneIndexStep();
		}
		else if (indexThread == null || !indexThread.isAlive()) {
			startIndexThread();
		}
		
		LOG.debug("Add:" + indexStep + " and sent into queue:" + indexQueue.size());
		return;
	}
	
	private void startIndexThread() {
		Runnable r = new Runnable() {			
			@Override
			public void run() {
				
				for (;;) {
					try {
						if (IndexTools.INSTANCE().switchedOff()) {
							Thread.sleep(400L);
							continue;
						}
						int result = pollAndWorkOneIndexStep();
						
						if (result == EMPTY_QUEUE || result == LOCKED) {
							Thread.sleep(400L);
							
						} else if ((result & TRANSACTION_COMMIT) != 0) {
							Thread.sleep(400L);
							
							if (indexQueue.isEmpty()) {
								break;								
							}
							
						}
						
					} catch (InterruptedException ie) {
						LOG.warn(ie.getMessage(), ie);
					}
					
				}
			}
		};
		
		indexThread = new Thread(r);
		indexThread.start();
	}
	
	private static final int ERROR = -1;
	private static final int EMPTY_QUEUE = 0;
	private static final int LOCKED = 1;
	private static final int INDEX_DONE = 2;
	private static final int INDEX_SKIPPED = 3;
	private static final int TRANSACTION_COMMIT = 4;
	private static final int TRANSACTION_ROLLBACK = 5;
	
	private int pollAndWorkOneIndexStep() {
		
		if (indexQueue.isEmpty()) {
			return EMPTY_QUEUE;
		}
		
		try {
			if (IndexTools.INSTANCE().switchedOff()) {
				return EMPTY_QUEUE;
			}
		
			//First try if an IndexWriter is available.
			getIndexWriter();
			
			IndexStep is = indexQueue.poll();
			
			if (is instanceof IndexVO) {				
				return indexOneObject((IndexVO<?>)is) ? INDEX_DONE : INDEX_SKIPPED;
				
			} else if (is instanceof CompleteTransaction) {
				return finishTransaction((CompleteTransaction)is) ? TRANSACTION_COMMIT : TRANSACTION_ROLLBACK;
				
			}

		} catch (LockObtainFailedException lofe) {
			LOG.debug(lofe.getMessage());
			return LOCKED;
			
		} catch (IOException io) {
			LOG.error(io.getMessage(), io);
		}
		
		return ERROR;
	}
	
	private boolean finishTransaction(CompleteTransaction ct) throws IOException {
		if (!ct.isSuccess()) {
			
			getIndexWriter().rollback();
			
			LOG.info("Indexer Transaction Rollbacked:" + ct.getTransactId());
			return false;
		}
		
		if (ct.isClose()) {
			getIndexWriter().close();
			
			LOG.debug("Indexer Transaction Closed:" + ct.getTransactId());
			return true;					
		}
		
		getIndexWriter().commit();
		
		LOG.debug("Indexer Transaction Comitted:" + ct.getTransactId());
		return true;		
	}
	
	private boolean indexOneObject(IndexVO<?> vo) throws IOException {
		boolean changed = getIndexWriter().indexVO(vo);
		
		if (changed) {
			LOG.debug("Indexed:" + vo);
		}
		
		return changed;
	}
	
	private NuclosIndexWriter getIndexWriter() throws IOException {
		if (indexWriter == null || !indexWriter.isOpen()) {
			indexWriter = IndexTools.INSTANCE().createWriter();
		}
		
		return indexWriter;
	}
	
}
