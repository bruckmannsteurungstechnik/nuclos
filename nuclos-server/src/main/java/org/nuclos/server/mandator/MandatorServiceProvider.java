package org.nuclos.server.mandator;

import org.nuclos.api.common.NuclosMandator;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.service.MandatorService;
import org.nuclos.common.MandatorVO;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component("mandatorServiceProvider")
public class MandatorServiceProvider implements MandatorService {
    @Autowired
    private MandatorFacadeLocal mandatorFacade;


    @Autowired
    private SpringDataBaseHelper dataBaseHelper;

    public org.nuclos.api.UID insert(String name) throws BusinessException {
        return insertMandator(name).getPrimaryKey();
    }


    @Override
    public org.nuclos.api.UID insert(String name, NuclosMandator parent) throws BusinessException {
        if (name == null)
            throw new BusinessException("name must not be null");
        if (parent == null)
            throw new BusinessException("parentName must not be null");

        UID parentId = (UID) parent.getId();
        MandatorVO parentMandator = null;
        try {
            parentMandator = mandatorFacade.getByUID(parentId);
        } catch (CommonBusinessException e) {
            throw new BusinessException(e);
        }

        try {
            MandatorVO create = mandatorFacade.create(new MandatorVO(name, parentMandator));
            return create.getPrimaryKey();
        } catch (Exception e) {
            throw new BusinessException(e);
        }
    }


    private MandatorVO insertMandator(String name) throws BusinessException {
        if (name == null)
            throw new BusinessException("name must not be null");

        try {
            MandatorVO create = mandatorFacade.create(new MandatorVO(name));
            return create;
        } catch (Exception e) {
            throw new BusinessException(e);
        }
    }
}
