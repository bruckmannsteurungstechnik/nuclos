//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.masterdata;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.StringUtils;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.FieldMeta.Valueable;
import org.nuclos.common.MarshalledValue;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.report.valueobject.CalcAttributeVO;
import org.nuclos.common.report.valueobject.ChartVO;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.DefaultReportOutputVO;
import org.nuclos.common.report.valueobject.DefaultReportOutputVOContext;
import org.nuclos.common.report.valueobject.DefaultReportVO;
import org.nuclos.common.report.valueobject.DynamicEntityVO;
import org.nuclos.common.report.valueobject.DynamicTasklistVO;
import org.nuclos.common.report.valueobject.RecordGrantVO;
import org.nuclos.common.report.valueobject.ReportOutputVOContext;
import org.nuclos.common.report.valueobject.ReportVO.ReportType;
import org.nuclos.common.report.valueobject.SubreportVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common.security.Permission;
import org.nuclos.common.valueobject.EntityRelationshipModelVO;
import org.nuclos.common2.IOUtils;
import org.nuclos.common2.KeyEnum;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.attribute.valueobject.LayoutVO;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerServiceLocator;
import org.nuclos.server.common.ejb3.LocaleFacadeLocal;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.common.valueobject.TimelimitTaskVO;
import org.nuclos.server.customcode.valueobject.CodeVO;
import org.nuclos.server.eventsupport.valueobject.CommunicationPortVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportCommunicationPortVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportEventVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportGenerationVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportJobVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportTransitionVO;
import org.nuclos.server.eventsupport.valueobject.ProcessVO;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.genericobject.valueobject.GeneratorUsageVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectRelationVO;
import org.nuclos.server.genericobject.valueobject.LogbookVO;
import org.nuclos.server.history.HistoryVO;
import org.nuclos.server.job.valueobject.JobVO;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.masterdata.valueobject.RoleTransitionVO;
import org.nuclos.server.resource.ResourceCache;
import org.nuclos.server.rule.client.vo.ClientRuleSourceVO;
import org.nuclos.server.statemodel.valueobject.AttributegroupPermissionVO;
import org.nuclos.server.statemodel.valueobject.MandatoryColumnVO;
import org.nuclos.server.statemodel.valueobject.MandatoryFieldVO;
import org.nuclos.server.statemodel.valueobject.StateGraphVO;
import org.nuclos.server.statemodel.valueobject.StateHistoryVO;
import org.nuclos.server.statemodel.valueobject.StateModelLayout;
import org.nuclos.server.statemodel.valueobject.StateModelVO;
import org.nuclos.server.statemodel.valueobject.StateTransitionVO;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.nuclos.server.statemodel.valueobject.SubformGroupPermissionVO;
import org.nuclos.server.statemodel.valueobject.SubformPermissionVO;

public class MasterDataWrapper {

	public static MasterDataVO<UID> wrapCodeVO(CodeVO vo) {
		MasterDataVO<UID> result = new MasterDataVO<UID>(vo.getEntity().getUID(), vo.getId(), 
				vo.getChangedAt(), vo.getCreatedBy(), vo.getChangedAt(), vo.getChangedBy(), vo.getVersion());
		if (vo.getEntity().equals(E.SERVERCODE)) {
			result.setFieldValue(E.SERVERCODE.name, vo.getName());
			result.setFieldValue(E.SERVERCODE.description, vo.getDescription());
			result.setFieldValue(E.SERVERCODE.source, vo.getSource());
			result.setFieldValue(E.SERVERCODE.active, vo.isActive());
			result.setFieldValue(E.SERVERCODE.debug, vo.isDebug());
			result.setFieldUid(E.SERVERCODE.nuclet, vo.getNuclet());
		} else {
			throw new UnsupportedOperationException();
		}
		
		return result;
	}

	public static ProcessVO getProcessVO(EntityObjectVO<UID> mdVO) {
		
		// Foreiggn Keys
		UID pvoNuclet = mdVO.getFieldUid(E.PROCESS.nuclet);
		UID pvoModule = mdVO.getFieldUid(E.PROCESS.module);
		
		// Dates
		Date pvoDateValidFrom = mdVO.getFieldValue(E.PROCESS.validFrom);
		Date pvoDateValidUntil = mdVO.getFieldValue(E.PROCESS.validUntil);
		
		String sDescription =  	mdVO.getFieldValue(E.PROCESS.description);
		String sName =  	mdVO.getFieldValue(E.PROCESS.name);
		
		ProcessVO pvo = new ProcessVO(new NuclosValueObject<UID>(mdVO.getPrimaryKey(), mdVO.getCreatedAt(), mdVO.getCreatedBy(), mdVO.getChangedAt(), mdVO.getChangedBy(), mdVO.getVersion()),
				pvoNuclet, pvoModule, pvoDateValidUntil, pvoDateValidFrom, sDescription, sName);
		
		return pvo;
	}
	
	public static EventSupportTransitionVO getEventSupportTransitionVO(MasterDataVO<UID> mdVO) {
		
		return getEventSupportTransitionVO(mdVO.getEntityObject());
	}

	public static ClientRuleSourceVO getClientRuleSourceVO(EntityObjectVO<UID> eo) {
		ClientRuleSourceVO newRule = new ClientRuleSourceVO
				(new NuclosValueObject<UID>(eo.getPrimaryKey(), eo.getCreatedAt(), eo.getCreatedBy(), eo.getChangedAt(), eo.getChangedBy(), eo.getVersion()),
					eo.getFieldUid(E.CLIENTCODE.nuclet),
					eo.getFieldValue(E.CLIENTCODE.name),
					eo.getFieldValue(E.CLIENTCODE.description),
					eo.getFieldValue(E.CLIENTCODE.active),
					eo.getFieldValue(E.CLIENTCODE.type),
					eo.getFieldValue(E.CLIENTCODE.source),
					eo.getFieldValue(E.CLIENTCODE.contextsource));
		return newRule;
	}
	
	public static EventSupportTransitionVO getEventSupportTransitionVO(EntityObjectVO<UID> mdVO) {
		
		UID trans = mdVO.getFieldUid(E.SERVERCODETRANSITION.transition);
		String  sSupportClass = mdVO.getFieldValue(E.SERVERCODETRANSITION.classField);
		String  sSupportClassType = mdVO.getFieldValue(E.SERVERCODETRANSITION.type);
		Integer iOrder = mdVO.getFieldValue(E.SERVERCODETRANSITION.order);
		
		return new EventSupportTransitionVO(new NuclosValueObject<UID>(mdVO.getPrimaryKey(), mdVO.getCreatedAt(), mdVO.getCreatedBy(), mdVO.getChangedAt(), mdVO.getChangedBy(), mdVO.getVersion()),
				sSupportClass, sSupportClassType, trans, iOrder);
	}

	public static EventSupportGenerationVO getEventSupportGenerationVO(MasterDataVO<UID> mdVO) {
		String  esgSupClass = mdVO.getFieldValue(E.SERVERCODEGENERATION.classField);
		String  sSupportClassType = mdVO.getFieldValue(E.SERVERCODEGENERATION.type);
		Integer esgOrder = mdVO.getFieldValue(E.SERVERCODEGENERATION.order);
		UID esgGeneration = mdVO.getFieldUid(E.SERVERCODEGENERATION.generation);
		
		EventSupportGenerationVO retVal = new EventSupportGenerationVO(
				new NuclosValueObject<UID>(mdVO.getPrimaryKey(), mdVO.getCreatedAt(), mdVO.getCreatedBy(), mdVO.getChangedAt(), mdVO.getChangedBy(), mdVO.getVersion()),
				esgOrder, esgGeneration,esgSupClass,sSupportClassType);
		
		return retVal;
	}
	
	public static EventSupportEventVO getEventSupportEventVO(MasterDataVO<UID> mdVO) {
		return getEventSupportEventVO(mdVO.getEntityObject());
	}
	
	public static EventSupportEventVO getEventSupportEventVO(EntityObjectVO<UID> mdVO) {

		// Mandatory fields that cannot be null
		final String eseSupClass = mdVO.getFieldValue(E.SERVERCODEENTITY.classField);
		final String eseSupType = mdVO.getFieldValue(E.SERVERCODEENTITY.type);
		final Integer eseOrder = mdVO.getFieldValue(E.SERVERCODEENTITY.order);
	
		final UID eseEntity = mdVO.getFieldUid(E.SERVERCODEENTITY.entity);
		final UID eseIntegrationPoint = mdVO.getFieldUid(E.SERVERCODEENTITY.integrationPoint);
		
		// Fields that can be null
		final UID eseState = mdVO.getFieldUid(E.SERVERCODEENTITY.state);
		final String eseStateName = mdVO.getFieldValue(E.SERVERCODEENTITY.state.getUID(), String.class);
		final UID eseProcess = mdVO.getFieldUid(E.SERVERCODEENTITY.process);
		final String eseProcessName = mdVO.getFieldValue(E.SERVERCODEENTITY.process.getUID(), String.class);
		
		return new EventSupportEventVO(new NuclosValueObject<UID>(mdVO.getPrimaryKey(), 
				mdVO.getCreatedAt(), mdVO.getCreatedBy(), mdVO.getChangedAt(), mdVO.getChangedBy(), mdVO.getVersion()), 
					eseSupClass, eseSupType, eseEntity, eseIntegrationPoint, eseProcess, eseProcessName, eseState, eseStateName, eseOrder);
	}
	
	
	public static EventSupportJobVO getEventSupportJobVO(MasterDataVO<UID> mdVO) {

		return getEventSupportJobVO(mdVO.getEntityObject());
	}
	
	public static EventSupportJobVO getEventSupportJobVO(EntityObjectVO<UID> eoVO) {

		// Mandatory fields that cannot be null
		String eseSupClass = eoVO.getFieldValue(E.SERVERCODEJOB.classField);
		String  sSupportClassType = eoVO.getFieldValue(E.SERVERCODEJOB.type);
		String description = eoVO.getFieldValue(E.SERVERCODEJOB.description);
		Integer eseOrder = eoVO.getFieldValue(E.SERVERCODEJOB.order);
		UID job = eoVO.getFieldUid(E.SERVERCODEJOB.jobcontroller);
		
		EventSupportJobVO esevo = new EventSupportJobVO(
				new NuclosValueObject<UID>(eoVO.getPrimaryKey(), eoVO.getCreatedAt(), eoVO.getCreatedBy(), eoVO.getChangedAt(), eoVO.getChangedBy(), eoVO.getVersion()),
												description,eseSupClass,sSupportClassType, eseOrder, job);
		
		return esevo;
	}
	
	public static CodeVO getServerCodeVO(MasterDataVO<UID> mdVO) {
		UID dalEntity = mdVO.getEntityObject().getDalEntity();
		if (E.SERVERCODE.checkEntityUID(dalEntity)) {
			CodeVO vo = new CodeVO(
					new NuclosValueObject<UID>(mdVO.getPrimaryKey(), mdVO.getCreatedAt(), mdVO.getCreatedBy(), mdVO.getChangedAt(), mdVO.getChangedBy(), mdVO.getVersion()),
					E.SERVERCODE, 
					mdVO.getFieldValue(E.SERVERCODE.name),
					mdVO.getFieldValue(E.SERVERCODE.description),
					mdVO.getFieldValue(E.SERVERCODE.source),
					mdVO.getFieldValue(E.SERVERCODE.active) == null ? false : mdVO.getFieldValue(E.SERVERCODE.active),
					mdVO.getFieldValue(E.SERVERCODE.debug) == null ? false : mdVO.getFieldValue(E.SERVERCODE.debug),
					mdVO.getFieldUid(E.SERVERCODE.nuclet));
			return vo;
		} else {
			throw new UnsupportedOperationException();
		}
	}

	public static EventSupportGenerationVO getEventSupportGenerationVO(EntityObjectVO<UID> mdVO) {
		String  esgSupClass = mdVO.getFieldValue(E.SERVERCODEGENERATION.classField);
		String  sSupportClassType = mdVO.getFieldValue(E.SERVERCODEGENERATION.type);
		Integer esgOrder = mdVO.getFieldValue(E.SERVERCODEGENERATION.order);
		UID esgGeneration = mdVO.getFieldUid(E.SERVERCODEGENERATION.generation);
		
		EventSupportGenerationVO retVal = new EventSupportGenerationVO(
				new NuclosValueObject<UID>(mdVO.getPrimaryKey(), mdVO.getCreatedAt(), mdVO.getCreatedBy(), mdVO.getChangedAt(), mdVO.getChangedBy(), mdVO.getVersion()),
				esgOrder, esgGeneration, esgSupClass, sSupportClassType);
		
		return retVal;
	}
	
	public static EventSupportCommunicationPortVO getEventSupportCommunicationPortVO(EntityObjectVO<UID> mdVO) {
		String  esgSupClass = mdVO.getFieldValue(E.SERVERCODECOMMUNICATIONPORT.classField);
		String  sSupportClassType = mdVO.getFieldValue(E.SERVERCODECOMMUNICATIONPORT.type);
		Integer esgOrder = mdVO.getFieldValue(E.SERVERCODECOMMUNICATIONPORT.order);
		UID esgPort = mdVO.getFieldUid(E.SERVERCODECOMMUNICATIONPORT.communicationport);
		
		EventSupportCommunicationPortVO retVal = new EventSupportCommunicationPortVO(
				new NuclosValueObject<UID>(mdVO.getPrimaryKey(), mdVO.getCreatedAt(), mdVO.getCreatedBy(), mdVO.getChangedAt(), mdVO.getChangedBy(), mdVO.getVersion()),
				esgOrder, esgPort, esgSupClass, sSupportClassType);
		
		return retVal;
	}
	
	public static GeneratorActionVO getGeneratorActionVO(MasterDataVO<UID> mdVO, Collection<GeneratorUsageVO> usages) {
			
		GeneratorActionVO vo = new GeneratorActionVO(
			mdVO.getPrimaryKey(),
			mdVO.getFieldValue(E.GENERATION.name),
			mdVO.getFieldValue(E.GENERATION.label),
			mdVO.getFieldUid(E.GENERATION.buttonIcon) == null ? null : ResourceCache.getInstance().getResource(mdVO.getFieldUid(E.GENERATION.buttonIcon)),
			mdVO.getFieldUid(E.GENERATION.sourceModule),
			mdVO.getFieldUid(E.GENERATION.sourceMandator),
			mdVO.getFieldUid(E.GENERATION.targetModule),
			mdVO.getFieldUid(E.GENERATION.targetProcess),
			mdVO.getFieldUid(E.GENERATION.targetMandator),
			mdVO.getFieldUid(E.GENERATION.parameterEntity),
			mdVO.getFieldUid(E.GENERATION.nuclet),
			usages);
		
		if (mdVO.getFieldValue(E.GENERATION.groupattributes) != null) {
			vo.setGroupAttributes((Boolean)mdVO.getFieldValue(E.GENERATION.groupattributes));			
		}
		
		if (mdVO.getFieldValue(E.GENERATION.showobject) != null) {
			vo.setShowObject((Boolean)mdVO.getFieldValue(E.GENERATION.showobject));			
		} else {
			vo.setShowObject(true);			
		}
		
		if (mdVO.getFieldValue(E.GENERATION.refreshsrcobject) != null) {
			vo.setRefreshSrcObject((Boolean)mdVO.getFieldValue(E.GENERATION.refreshsrcobject));			
		} else {
			vo.setRefreshSrcObject(false);			
		}
		
		if (mdVO.getFieldValue(E.GENERATION.ruleonly) != null) {
			vo.setRuleOnly((Boolean)mdVO.getFieldValue(E.GENERATION.ruleonly));			
		} else {
			vo.setRuleOnly(false);			
		}

		if (mdVO.getFieldValue(E.GENERATION.nonstop) != null) {
			vo.setNonstop((Boolean)mdVO.getFieldValue(E.GENERATION.nonstop));			
		} else {
			vo.setNonstop(false);			
		}

		if (mdVO.getFieldValue(E.GENERATION.createRelation) != null) {
			vo.setCreateRelationBetweenObjects((Boolean)mdVO.getFieldValue(E.GENERATION.createRelation));
		}

		if (mdVO.getFieldValue(E.GENERATION.closeOnException) != null) {
			vo.setCloseOnException((Boolean)mdVO.getFieldValue(E.GENERATION.closeOnException));
		}

		if (mdVO.getFieldValue(E.GENERATION.doNotSave) != null) {
			vo.setDoNotSave((Boolean)mdVO.getFieldValue(E.GENERATION.doNotSave));
		}

		if (mdVO.getFieldValue(E.GENERATION.openInOverlay) != null) {
			vo.setOpenInOverlay((Boolean)mdVO.getFieldValue(E.GENERATION.openInOverlay));
		}
		
		if (mdVO.getFieldValue(E.GENERATION.createParameterRelation) != null) {
			vo.setCreateRelationToParameterObject((Boolean)mdVO.getFieldValue(E.GENERATION.createParameterRelation));			
		}
		
		if(mdVO.getFieldUid(E.GENERATION.parameterValuelist) != null) {
			vo.setValuelistProvider(mdVO.getFieldUid(E.GENERATION.parameterValuelist));			
		}
		
		return vo;
	}

	public static MasterDataVO<UID> wrapGeneratorActionVO(GeneratorActionVO gaVO) {
		MasterDataVO<UID> result = new MasterDataVO<UID>(E.ACTION.getUID(), gaVO.getId(), null, null, null, null, null);
		result.setFieldValue(E.GENERATION.name, gaVO.getName());
		result.setFieldValue(E.GENERATION.label, gaVO.getLabel());
		result.setFieldUid(E.GENERATION.sourceModule, gaVO.getSourceModule());
		result.setFieldUid(E.GENERATION.sourceMandator, gaVO.getSourceMandator());
		result.setFieldUid(E.GENERATION.targetModule, gaVO.getTargetModule());
		result.setFieldUid(E.GENERATION.targetProcess, gaVO.getTargetProcess());
		result.setFieldUid(E.GENERATION.targetMandator, gaVO.getTargetMandator());
		result.setFieldUid(E.GENERATION.parameterEntity, gaVO.getParameterEntity());
		result.setFieldValue(E.GENERATION.createRelation, gaVO.isCreateRelationBetweenObjects());
		result.setFieldValue(E.GENERATION.createParameterRelation, gaVO.isCreateRelationToParameterObject());
		result.setFieldValue(E.GENERATION.closeOnException, gaVO.isCloseOnException());
		result.setFieldValue(E.GENERATION.doNotSave, gaVO.isDoNotSave());
		result.setFieldValue(E.GENERATION.openInOverlay, gaVO.isOpenInOverlay());
				
		return result;
	}

	public static GeneratorUsageVO getGeneratorUsageVO(MasterDataVO<UID> mdVO) {
		return new GeneratorUsageVO(
			mdVO.getFieldUid(E.GENERATIONUSAGE.state),
			mdVO.getFieldUid(E.GENERATIONUSAGE.process));
	}

	
	public static MasterDataVO<UID> wrapEventSupportTransitionVO(EventSupportTransitionVO vo) {
		MasterDataVO<UID> result = new MasterDataVO<UID>(E.SERVERCODETRANSITION.getUID(), vo.getPrimaryKey(), 
				vo.getChangedAt(), vo.getCreatedBy(), vo.getChangedAt(), vo.getChangedBy(), vo.getVersion());
		result.setFieldUid(E.SERVERCODETRANSITION.transition, vo.getTransition());
		result.setFieldValue(E.SERVERCODETRANSITION.classField, vo.getEventSupportClass());
		result.setFieldValue(E.SERVERCODETRANSITION.type, vo.getEventSupportClassType());
		result.setFieldValue(E.SERVERCODETRANSITION.order, vo.getOrder());
		
		return result;
	}
	
	public static MasterDataVO<UID> wrapEventSupportJobVO(EventSupportJobVO vo) {
		MasterDataVO<UID> result = new MasterDataVO<UID>(E.SERVERCODEJOB.getUID(), vo.getPrimaryKey(), 
				vo.getChangedAt(), vo.getCreatedBy(), vo.getChangedAt(), vo.getChangedBy(), vo.getVersion());
		result.setFieldUid(E.SERVERCODEJOB.jobcontroller, vo.getJobControllerUID());
		result.setFieldValue(E.SERVERCODEJOB.classField, vo.getEventSupportClass());
		result.setFieldValue(E.SERVERCODEJOB.type, vo.getEventSupportClassType());
		result.setFieldValue(E.SERVERCODEJOB.order, vo.getOrder());
		result.setFieldValue(E.SERVERCODEJOB.description, vo.getDescription());
		
		return result;
	}
	
	public static MasterDataVO<UID> wrapEventSupportGenerationVO(EventSupportGenerationVO vo) {
		MasterDataVO<UID> result = new MasterDataVO<UID>(E.SERVERCODEGENERATION.getUID(), vo.getPrimaryKey(), 
				vo.getChangedAt(), vo.getCreatedBy(), vo.getChangedAt(), vo.getChangedBy(), vo.getVersion());
		result.setFieldUid(E.SERVERCODEGENERATION.generation, vo.getGeneration());
		result.setFieldValue(E.SERVERCODEGENERATION.classField, vo.getEventSupportClass());
		result.setFieldValue(E.SERVERCODEGENERATION.type, vo.getEventSupportClassType());
		result.setFieldValue(E.SERVERCODEGENERATION.order, vo.getOrder());
		
		return result;
	}
	
	public static MasterDataVO<UID> wrapEventSupportCommunicationPortVO(EventSupportCommunicationPortVO vo) {
		MasterDataVO<UID> result = new MasterDataVO<UID>(E.SERVERCODECOMMUNICATIONPORT.getUID(), vo.getPrimaryKey(), 
				vo.getChangedAt(), vo.getCreatedBy(), vo.getChangedAt(), vo.getChangedBy(), vo.getVersion());
		result.setFieldUid(E.SERVERCODECOMMUNICATIONPORT.communicationport, vo.getPort());
		result.setFieldValue(E.SERVERCODECOMMUNICATIONPORT.classField, vo.getEventSupportClass());
		result.setFieldValue(E.SERVERCODECOMMUNICATIONPORT.type, vo.getEventSupportClassType());
		result.setFieldValue(E.SERVERCODECOMMUNICATIONPORT.order, vo.getOrder());
		
		return result;
	}
	
	public static MasterDataVO<UID> wrapEventSupportEventVO(EventSupportEventVO vo) {
		MasterDataVO<UID> result = new MasterDataVO<UID>(E.SERVERCODEENTITY.getUID(), vo.getPrimaryKey(), 
				vo.getChangedAt(), vo.getCreatedBy(), vo.getChangedAt(), vo.getChangedBy(), vo.getVersion());
		result.setFieldUid(E.SERVERCODEENTITY.state, vo.getStateUID());
		result.setFieldUid(E.SERVERCODEENTITY.entity, vo.getEntityUID());
		result.setFieldUid(E.SERVERCODEENTITY.integrationPoint, vo.getIntegrationPointUID());
		result.setFieldValue(E.SERVERCODEENTITY.order, vo.getOrder());
		result.setFieldUid(E.SERVERCODEENTITY.process, vo.getProcessUID());
		result.setFieldValue(E.SERVERCODEENTITY.classField, vo.getEventSupportClass());
		result.setFieldValue(E.SERVERCODEENTITY.type, vo.getEventSupportClassType());
		
		return result;
	}
	
	public static StateTransitionVO getStateTransitionVOWithoutDependants(MasterDataVO<UID> mdVO) {
		StateTransitionVO vo = new StateTransitionVO(
			mdVO.getPrimaryKey(),
			mdVO.getFieldUid(E.STATETRANSITION.state1),
			mdVO.getFieldUid(E.STATETRANSITION.state2),
			mdVO.getFieldValue(E.STATETRANSITION.description),
			mdVO.getFieldValue(E.STATETRANSITION.automatic),
			mdVO.getFieldValue(E.STATETRANSITION.defaultfield) == null ? false : mdVO.getFieldValue(E.STATETRANSITION.defaultfield),
			mdVO.getFieldValue(E.STATETRANSITION.nonstop) == null ? false : mdVO.getFieldValue(E.STATETRANSITION.nonstop),
			mdVO.getCreatedAt(),
			mdVO.getCreatedBy(),
			mdVO.getChangedAt(),
			mdVO.getChangedBy(),
			mdVO.getVersion());

		vo.setResourceIdForLabel(mdVO.getFieldValue(E.STATETRANSITION.labelres));
		vo.setResourceIdForDescription(mdVO.getFieldValue(E.STATETRANSITION.descriptionres));

		return vo;
	}

	public static MasterDataVO<UID> wrapStateTransitionVO(StateTransitionVO vo) {
		MasterDataVO<UID> result = new MasterDataVO<UID>(E.STATETRANSITION.getUID(), 
				vo.getId(), vo.getChangedAt(), vo.getCreatedBy(), vo.getChangedAt(), vo.getChangedBy(), vo.getVersion());
		result.setFieldUid(E.STATETRANSITION.state1, vo.getStateSourceUID());
		result.setFieldUid(E.STATETRANSITION.state2, vo.getStateTargetUID());
		result.setFieldValue(E.STATETRANSITION.automatic, vo.isAutomatic());
		result.setFieldValue(E.STATETRANSITION.defaultfield, vo.isDefault());
		result.setFieldValue(E.STATETRANSITION.nonstop, vo.isNonstop());
		result.setFieldValue(E.STATETRANSITION.description, vo.getDescription());
		result.setFieldValue(E.STATETRANSITION.labelres, vo.getResourceIdForLabel());
		result.setFieldValue(E.STATETRANSITION.descriptionres, vo.getResourceIdForDescription());

		return result;
	}

	public static JobVO getJobVO(EntityObjectVO<UID> eoVO) {
		return new JobVO(new MasterDataVO<UID>(eoVO, false), eoVO.getDependents());
	}
		
	public static RoleTransitionVO getRoleTransitionVO(MasterDataVO<UID> mdVO) {
		RoleTransitionVO vo = new RoleTransitionVO(
			mdVO.getPrimaryKey(),
			mdVO.getFieldUid(E.ROLETRANSITION.transition),
			mdVO.getFieldUid(E.ROLETRANSITION.role),
			mdVO.getCreatedAt(),
			mdVO.getCreatedBy(),
			mdVO.getChangedAt(),
			mdVO.getChangedBy(),
			mdVO.getVersion());

		return vo;
	}

	public static MasterDataVO<UID> wrapRoleTransitionVO(RoleTransitionVO vo) {
		MasterDataVO<UID> result = new MasterDataVO<UID>(E.ROLETRANSITION.getUID(), vo.getPrimaryKey(), 
				vo.getChangedAt(), vo.getCreatedBy(), vo.getChangedAt(), vo.getChangedBy(), vo.getVersion());
		result.setFieldUid(E.ROLETRANSITION.transition, vo.getTransition());
		result.setFieldUid(E.ROLETRANSITION.role, vo.getRole());
		
		return result;
	}

	public static LayoutVO getLayoutVO(MasterDataVO<UID> mdVO) {
	
		String name = mdVO.getFieldValue(E.LAYOUT.name);
		String description = mdVO.getFieldValue(E.LAYOUT.description);
		String layoutML = mdVO.getFieldValue(E.LAYOUT.layoutML);
		UID nuclet = mdVO.getFieldUid(E.LAYOUT.nuclet);
		
		LayoutVO retVal = new LayoutVO(mdVO.getPrimaryKey() ,name, description, layoutML, nuclet);
		
		return retVal;
	}
	
	public static StateModelVO getStateModelVO(MasterDataVO<UID> mdVO, StateGraphVO stategraphvo) {
		StateModelLayout layout = null;
		try {
			byte b[] = (byte[])mdVO.getFieldValue(E.STATEMODEL.layout);
			Object o = null;

			if (b != null) {
				String xml = new String(b);
				if(xml != null && xml.startsWith("<?xml")) {
					o = xml;
				}
				else {
					o = IOUtils.fromByteArray(b);
				}
			}

			if (o == null) {
				// after migration...
			}
			else if (o instanceof MarshalledValue) {
				// for backwards compatibility:
				layout = (StateModelLayout) ((MarshalledValue) o).get();
			}
			else if (o instanceof StateModelLayout) {
				layout = (StateModelLayout) o;
			}
			else if(o instanceof String) {
				// XML is for future, see StateModelEditor2 
			}
			else {
				throw new NuclosFatalException("Unexpected class: " + o.getClass().getName());
			}
		}
		catch(IOException e) {
			throw new NuclosFatalException("can't read StateModelLayout");
		}
		catch(ClassNotFoundException e) {
			throw new NuclosFatalException("can't read StateModelLayout");
		}

		final StateModelVO vo = new StateModelVO(getBaseVO(mdVO),
			mdVO.getFieldValue(E.STATEMODEL.name),
			mdVO.getFieldValue(E.STATEMODEL.description),
			layout != null ? layout : (stategraphvo == null ? null : StateGraphVO.newLayoutInfo(stategraphvo)),
			mdVO.getFieldUid(E.STATEMODEL.nuclet));

		return vo;
	}

	public static EntityRelationshipModelVO getEntityRelationshipModelVO(MasterDataVO<UID> mdVO) {
		EntityRelationshipModelVO vo = new EntityRelationshipModelVO(getBaseVO(mdVO), mdVO.getFieldValue(E.ENTITYRELATION.name),
			(String)mdVO.getFieldValue(E.ENTITYRELATION.description));

		return vo;
	}

	public static MasterDataVO<UID> wrapStateModelVO(StateModelVO vo) {

		byte[] layoutData = null;

		if (vo.getLayout() == null) {
			throw new NullArgumentException("layout");
		}
		try {
			layoutData = IOUtils.toByteArray(vo.getLayout());
			assert layoutData != null;
			assert layoutData.length > 0;
		}
		catch (IOException ex) {
			throw new NuclosFatalException(ex);
		}

		MasterDataVO<UID> result = new MasterDataVO<UID>(E.STATEMODEL.getUID(), vo.getPrimaryKey(), 
				vo.getChangedAt(), vo.getCreatedBy(), vo.getChangedAt(), vo.getChangedBy(), vo.getVersion());
		result.setFieldValue(E.STATEMODEL.name, vo.getName());
		result.setFieldValue(E.STATEMODEL.description, vo.getDescription());
		result.setFieldValue(E.STATEMODEL.layout, layoutData);
		result.setFieldUid(E.STATEMODEL.nuclet, vo.getNuclet());
		
		return result;
	}

	public static StateHistoryVO getStateHistoryVO(MasterDataVO<Long> mdVO, String sStateName) {
		StateHistoryVO vo = new StateHistoryVO(
			mdVO.getPrimaryKey(),
			mdVO.getFieldId(E.STATEHISTORY.genericObject),
			mdVO.getFieldUid(E.STATEHISTORY.state),
			sStateName,
			mdVO.getCreatedAt(),
			mdVO.getCreatedBy(),
			mdVO.getChangedAt(),
			mdVO.getChangedBy(),
			mdVO.getVersion());

		return vo;
	}

	public static MasterDataVO<Long> wrapStateHistoryVO(StateHistoryVO vo) {
		MasterDataVO<Long> result = new MasterDataVO<Long>(E.STATEHISTORY.getUID(), vo.getPrimaryKey(), 
				vo.getChangedAt(), vo.getCreatedBy(), vo.getChangedAt(), vo.getChangedBy(), vo.getVersion());
		result.setFieldId(E.STATEHISTORY.genericObject, vo.getGenericObjectId());
		result.setFieldUid(E.STATEHISTORY.state, vo.getState());
		result.setFieldValue(E.STATEHISTORY.state.getUID(), vo.getStateName());
				
		return result;
	}

	public static StateVO getStateVO(MasterDataVO<UID> mdVO) {
		StateVO vo = new StateVO(
			getBaseVO(mdVO),
			mdVO.getFieldValue(E.STATE.numeral),
			mdVO.getFieldValue(E.STATE.labelres),
			mdVO.getFieldValue(E.STATE.descriptionres),
			mdVO.getFieldValue(E.STATE.buttonRes),
			mdVO.getFieldValue(E.STATE.icon),
			mdVO.getFieldUid(E.STATE.model));
		vo.setTabbedPaneName(mdVO.getFieldValue(E.STATE.tab));
		vo.setButtonIcon(ResourceCache.getInstance().getResource(mdVO.getFieldUid(E.STATE.buttonIcon)));
		vo.setColor(mdVO.getFieldValue(E.STATE.color));

		return vo;
	}

	public static MasterDataVO<UID> wrapStateVO(StateVO vo) {
		MasterDataVO<UID> result = new MasterDataVO<UID>(E.STATE.getUID(), vo.getPrimaryKey(), 
				vo.getChangedAt(), vo.getCreatedBy(), vo.getChangedAt(), vo.getChangedBy(), vo.getVersion());
		result.setFieldValue(E.STATE.numeral, vo.getNumeral());
		result.setFieldValue(E.STATE.name, vo.getStatename(Locale.ENGLISH));
		result.setFieldValue(E.STATE.description, vo.getDescription(Locale.ENGLISH));
		result.setFieldValue(E.STATE.icon, vo.getIcon());
		result.setFieldUid(E.STATE.model, vo.getModelUID());
		result.setFieldValue(E.STATE.tab, vo.getTabbedPaneName());
		result.setFieldValue(E.STATE.color, vo.getColor());
		//result.setFieldValue(E.STATE.buttonRes, vo.getButtonLabel());
		result.setFieldUid(E.STATE.buttonIcon, vo.getButtonIcon()==null?null:vo.getButtonIcon().getPrimaryKey());
		
		return result;
	}

	public static MandatoryFieldVO getMandatoryFieldVO(MasterDataVO<UID> mdVO) {
		MandatoryFieldVO vo = new MandatoryFieldVO(
			getBaseVO(mdVO),
			mdVO.getFieldUid(E.STATEMANDATORYFIELD.entityfield),
			mdVO.getFieldUid(E.STATEMANDATORYFIELD.state));

		return vo;
	}

	public static MasterDataVO<UID> wrapMandatoryFieldVO(MandatoryFieldVO vo) {
		MasterDataVO<UID> result = new MasterDataVO<UID>(E.STATEMANDATORYFIELD.getUID(), vo.getPrimaryKey(), 
				vo.getChangedAt(), vo.getCreatedBy(), vo.getChangedAt(), vo.getChangedBy(), vo.getVersion());
		result.setFieldUid(E.STATEMANDATORYFIELD.entityfield, vo.getField());
		result.setFieldUid(E.STATEMANDATORYFIELD.state, vo.getState());

		return result;
	}

	public static MandatoryColumnVO getMandatoryColumnVO(MasterDataVO<UID> mdVO) {
		MandatoryColumnVO vo = new MandatoryColumnVO(
			getBaseVO(mdVO),
			mdVO.getFieldUid(E.STATEMANDATORYCOLUMN.entity),
			mdVO.getFieldUid(E.STATEMANDATORYCOLUMN.column),
			mdVO.getFieldUid(E.STATEMANDATORYCOLUMN.state));

		return vo;
	}

	public static MasterDataVO<UID> wrapMandatoryColumnVO(MandatoryColumnVO vo) {
		MasterDataVO<UID> result = new MasterDataVO<UID>(E.STATEMANDATORYCOLUMN.getUID(), vo.getPrimaryKey(), 
				vo.getChangedAt(), vo.getCreatedBy(), vo.getChangedAt(), vo.getChangedBy(), vo.getVersion());
		result.setFieldUid(E.STATEMANDATORYCOLUMN.entity, vo.getEntity());
		result.setFieldUid(E.STATEMANDATORYCOLUMN.column, vo.getColumn());
		result.setFieldUid(E.STATEMANDATORYCOLUMN.state, vo.getState());
		
		return result;
	}

	public static AttributegroupPermissionVO getAttributegroupPermissionVO(MasterDataVO<UID> mdVO) {
		AttributegroupPermissionVO vo = new AttributegroupPermissionVO(
			getBaseVO(mdVO),
			mdVO.getFieldUid(E.ROLEATTRIBUTEGROUP.attributegroup),
			mdVO.getFieldUid(E.ROLEATTRIBUTEGROUP.role),
			mdVO.getFieldUid(E.ROLEATTRIBUTEGROUP.state),
			mdVO.getFieldValue(E.ROLEATTRIBUTEGROUP.readwrite));

		return vo;
	}

	public static MasterDataVO<UID> wrapAttributegroupPermissionVO(AttributegroupPermissionVO vo) {
		MasterDataVO<UID> result = new MasterDataVO<UID>(E.ROLEATTRIBUTEGROUP.getUID(), vo.getPrimaryKey(), 
				vo.getChangedAt(), vo.getCreatedBy(), vo.getChangedAt(), vo.getChangedBy(), vo.getVersion());
		result.setFieldUid(E.ROLEATTRIBUTEGROUP.attributegroup, vo.getAttributegroupUID());
		result.setFieldUid(E.ROLEATTRIBUTEGROUP.role, vo.getRoleUID());
		result.setFieldUid(E.ROLEATTRIBUTEGROUP.state, vo.getStateUID());
		result.setFieldValue(E.ROLEATTRIBUTEGROUP.readwrite, vo.isWritable());
		
		return result;
	}

	public static SubformPermissionVO getSubformPermissionVO(MasterDataVO<UID> mdVO) {
		SubformPermissionVO vo = new SubformPermissionVO(
			getBaseVO(mdVO),
			mdVO.getFieldUid(E.ROLESUBFORM.entity),
			mdVO.getFieldUid(E.ROLESUBFORM.role),
			mdVO.getFieldUid(E.ROLESUBFORM.state),
			mdVO.getFieldValue(E.ROLESUBFORM.create),
			mdVO.getFieldValue(E.ROLESUBFORM.delete),
			null);

		return vo;
	}

	public static MasterDataVO<UID> wrapClientRuleSourceVO(ClientRuleSourceVO vo) {
		
		MasterDataVO<UID> eo = new MasterDataVO<UID>(E.CLIENTCODE.getUID(), vo.getPrimaryKey(), 
						vo.getChangedAt(), vo.getCreatedBy(), vo.getChangedAt(), vo.getChangedBy(), vo.getVersion());
	
		eo.setFieldValue(E.CLIENTCODE.name, vo.getName());
		eo.setFieldValue(E.CLIENTCODE.description, vo.getDescription());
		eo.setFieldValue(E.CLIENTCODE.active, vo.isActive());
		eo.setFieldValue(E.CLIENTCODE.type, vo.getRuleType());
		eo.setFieldValue(E.CLIENTCODE.source, vo.getRuleSource());
		eo.setFieldValue(E.CLIENTCODE.contextsource, vo.getRuleContextSource());
		
		eo.setFieldUid(E.CLIENTCODE.nuclet, vo.getNucletId());
		
		return eo;
	}
	
	public static MasterDataVO<UID> wrapSubformPermissionVO(SubformPermissionVO vo) {
		MasterDataVO<UID> result = new MasterDataVO<UID>(E.ROLESUBFORM.getUID(), vo.getPrimaryKey(), 
				vo.getChangedAt(), vo.getCreatedBy(), vo.getChangedAt(), vo.getChangedBy(), vo.getVersion());
		result.setFieldUid(E.ROLESUBFORM.entity, vo.getSubform());
		result.setFieldUid(E.ROLESUBFORM.role, vo.getRole());
		result.setFieldUid(E.ROLESUBFORM.state, vo.getState());
		result.setFieldValue(E.ROLESUBFORM.create, vo.canCreate());
		result.setFieldValue(E.ROLESUBFORM.delete, vo.canDelete());

		return result;
	}

	public static SubformGroupPermissionVO getSubformColumnPermissionVO(MasterDataVO<UID> mdVO) {
		SubformGroupPermissionVO vo = new SubformGroupPermissionVO(
			getBaseVO(mdVO),
			mdVO.getFieldUid(E.ROLESUBFORMGROUP.rolesubform),
			mdVO.getFieldUid(E.ROLESUBFORMGROUP.group),
			mdVO.getFieldValue(E.ROLESUBFORMGROUP.readwrite));

		return vo;
	}

	public static MasterDataVO<UID> wrapSubformGroupPermissionVO(SubformGroupPermissionVO vo) {
		MasterDataVO<UID> result = new MasterDataVO<UID>(E.ROLESUBFORMGROUP.getUID(), vo.getPrimaryKey(),
				vo.getChangedAt(), vo.getCreatedBy(), vo.getChangedAt(), vo.getChangedBy(), vo.getVersion());
		result.setFieldUid(E.ROLESUBFORMGROUP.rolesubform, vo.getRoleSubform());
		result.setFieldUid(E.ROLESUBFORMGROUP.group, vo.getGroup());
		result.setFieldValue(E.ROLESUBFORMGROUP.readwrite, vo.isWriteable());
		
		return result;
	}
	
	private static <T> FieldMeta.Valueable<T> getDsValueableField(UID dsEntity, FieldMeta.Valueable<T> dsField) {
		return (Valueable<T>) getDsField(dsEntity, dsField);
	}
	@SuppressWarnings("unchecked")
	private static <T> FieldMeta<T> getDsField(UID dsEntity, FieldMeta<T> dsField) {
		if (E.DATASOURCE.checkEntityUID(dsEntity)) {
			if (E.DATASOURCE.name.equals(dsField)) 		return (FieldMeta<T>) E.DATASOURCE.name;
			if (E.DATASOURCE.description.equals(dsField)) 	return (FieldMeta<T>) E.DATASOURCE.description;
			if (E.DATASOURCE.valid.equals(dsField)) 		return (FieldMeta<T>) E.DATASOURCE.valid;
			if (E.DATASOURCE.source.equals(dsField)) 		return (FieldMeta<T>) E.DATASOURCE.source;
			if (E.DATASOURCE.nuclet.equals(dsField)) 		return (FieldMeta<T>) E.DATASOURCE.nuclet;
		} else if (E.DYNAMICENTITY.checkEntityUID(dsEntity)) {
			if (E.DATASOURCE.name.equals(dsField)) 		return (FieldMeta<T>) E.DYNAMICENTITY.name;
			if (E.DATASOURCE.description.equals(dsField)) 	return (FieldMeta<T>) E.DYNAMICENTITY.description;
			if (E.DATASOURCE.valid.equals(dsField)) 		return (FieldMeta<T>) E.DYNAMICENTITY.valid;
			if (E.DATASOURCE.source.equals(dsField)) 		return (FieldMeta<T>) E.DYNAMICENTITY.source;
			if (E.DATASOURCE.nuclet.equals(dsField)) 		return (FieldMeta<T>) E.DYNAMICENTITY.nuclet;
		} else if (E.VALUELISTPROVIDER.checkEntityUID(dsEntity)) {
			if (E.DATASOURCE.name.equals(dsField)) 		return (FieldMeta<T>) E.VALUELISTPROVIDER.name;
			if (E.DATASOURCE.description.equals(dsField)) 	return (FieldMeta<T>) E.VALUELISTPROVIDER.description;
			if (E.VALUELISTPROVIDER.detailsearchdescription.equals(dsField)) 	return (FieldMeta<T>) E.VALUELISTPROVIDER.detailsearchdescription;
			if (E.DATASOURCE.valid.equals(dsField)) 		return (FieldMeta<T>) E.VALUELISTPROVIDER.valid;
			if (E.DATASOURCE.source.equals(dsField)) 		return (FieldMeta<T>) E.VALUELISTPROVIDER.source;
			if (E.DATASOURCE.nuclet.equals(dsField)) 		return (FieldMeta<T>) E.VALUELISTPROVIDER.nuclet;
		} else if (E.RECORDGRANT.checkEntityUID(dsEntity)) {
			if (E.DATASOURCE.name.equals(dsField)) 		return (FieldMeta<T>) E.RECORDGRANT.name;
			if (E.DATASOURCE.description.equals(dsField)) 	return (FieldMeta<T>) E.RECORDGRANT.description;
			if (E.DATASOURCE.valid.equals(dsField)) 		return (FieldMeta<T>) E.RECORDGRANT.valid;
			if (E.DATASOURCE.source.equals(dsField)) 		return (FieldMeta<T>) E.RECORDGRANT.source;
			if (E.DATASOURCE.nuclet.equals(dsField)) 		return (FieldMeta<T>) E.RECORDGRANT.nuclet;
		} else if (E.CHART.checkEntityUID(dsEntity)) {
			if (E.DATASOURCE.name.equals(dsField)) 		return (FieldMeta<T>) E.CHART.name;
			if (E.DATASOURCE.description.equals(dsField)) 	return (FieldMeta<T>) E.CHART.description;
			if (E.DATASOURCE.valid.equals(dsField)) 		return (FieldMeta<T>) E.CHART.valid;
			if (E.DATASOURCE.source.equals(dsField)) 		return (FieldMeta<T>) E.CHART.source;
			if (E.DATASOURCE.nuclet.equals(dsField)) 		return (FieldMeta<T>) E.CHART.nuclet;
		} else if (E.DYNAMICTASKLIST.checkEntityUID(dsEntity)) {
			if (E.DATASOURCE.name.equals(dsField)) 		return (FieldMeta<T>) E.DYNAMICTASKLIST.name;
			if (E.DATASOURCE.description.equals(dsField)) 	return (FieldMeta<T>) E.DYNAMICTASKLIST.description;
			if (E.DATASOURCE.valid.equals(dsField)) 		return (FieldMeta<T>) E.DYNAMICTASKLIST.valid;
			if (E.DATASOURCE.source.equals(dsField)) 		return (FieldMeta<T>) E.DYNAMICTASKLIST.source;
			if (E.DATASOURCE.nuclet.equals(dsField)) 		return (FieldMeta<T>) E.DYNAMICTASKLIST.nuclet;
		} else if (E.CALCATTRIBUTE.checkEntityUID(dsEntity)) {
			if (E.DATASOURCE.name.equals(dsField)) 		return (FieldMeta<T>) E.CALCATTRIBUTE.name;
			if (E.DATASOURCE.description.equals(dsField)) 	return (FieldMeta<T>) E.CALCATTRIBUTE.description;
			if (E.DATASOURCE.valid.equals(dsField)) 		return (FieldMeta<T>) E.CALCATTRIBUTE.valid;
			if (E.DATASOURCE.source.equals(dsField)) 		return (FieldMeta<T>) E.CALCATTRIBUTE.source;
			if (E.DATASOURCE.nuclet.equals(dsField)) 		return (FieldMeta<T>) E.CALCATTRIBUTE.nuclet;
		}
		throw new UnsupportedOperationException();
	}

	public static DatasourceVO getDatasourceVO(MasterDataVO<UID> mdVO, String currentUserName, UID mandator) {

		int permission = DatasourceVO.PERMISSION_NONE;
		if ("INITIAL".equals(currentUserName)) {
			permission = DatasourceVO.PERMISSION_READONLY;
		} else if (SecurityCache.getInstance().getWritableDataSourceUids(currentUserName, mandator).contains(mdVO.getPrimaryKey()))
			permission = DatasourceVO.PERMISSION_READWRITE;
		else if (SecurityCache.getInstance().getReadableDataSourceIds(currentUserName, mandator).contains(mdVO.getPrimaryKey()))
			permission = DatasourceVO.PERMISSION_READONLY;

		UID en = mdVO.getEntityObject().getDalEntity();
		
		DatasourceVO vo = new DatasourceVO(
			getBaseVO(mdVO),
			mdVO.getFieldValue(getDsValueableField(en, E.DATASOURCE.name)),
			mdVO.getFieldValue(getDsValueableField(en, E.DATASOURCE.description)),
			mdVO.getFieldValue(getDsValueableField(en, E.DATASOURCE.valid)),
			mdVO.getFieldValue(getDsValueableField(en, E.DATASOURCE.source)),
			mdVO.getFieldUid(getDsField(en, E.DATASOURCE.nuclet)),
			permission);
		
		vo.setNuclet(mdVO.getFieldValue(getDsField(en, E.DATASOURCE.nuclet).getUID(), String.class));
		if (E.DATASOURCE.checkEntityUID(mdVO.getEntityObject().getDalEntity())) {
			vo.setWithRuleClass(Boolean.TRUE.equals(mdVO.getFieldValue(E.DATASOURCE.withRuleClass)));
		}

		return vo;
	}

	public static MasterDataVO<UID> wrapDatasourceVO(DatasourceVO vo, UID en) {
		MasterDataVO<UID> result = new MasterDataVO<UID>(en, vo.getPrimaryKey(), 
				vo.getChangedAt(), vo.getCreatedBy(), vo.getChangedAt(), vo.getChangedBy(), vo.getVersion());
		result.setFieldValue(getDsValueableField(en, E.DATASOURCE.name), vo.getName());
		result.setFieldValue(getDsValueableField(en, E.DATASOURCE.description), vo.getDescription());
		result.setFieldValue(getDsValueableField(en, E.DATASOURCE.valid), vo.getValid());
		result.setFieldValue(getDsValueableField(en, E.DATASOURCE.source), vo.getSource());
		result.setFieldUid(getDsField(en, E.DATASOURCE.nuclet), vo.getNucletUID());
		result.setFieldValue(E.DATASOURCE.withRuleClass, vo.isWithRuleClass());
		
		if (vo instanceof RecordGrantVO)
			result.setFieldUid(E.RECORDGRANT.entity, ((RecordGrantVO) vo).getEntityUID());
		else if(vo instanceof DynamicEntityVO) {
			result.setFieldUid(E.DYNAMICENTITY.entity, ((DynamicEntityVO) vo).getEntityUID());
			result.setFieldValue(E.DYNAMICENTITY.meta, vo.getMeta());
			result.setFieldValue(E.DYNAMICENTITY.query, vo.getQuery());
		} else if(vo instanceof ChartVO) {
			result.setFieldValue(E.CHART.meta, vo.getMeta());
			result.setFieldValue(E.CHART.query, vo.getQuery());
			result.setFieldUid(E.CHART.detailsEntity, ((ChartVO) vo).getDetailsEntityUID());
			result.setFieldUid(E.CHART.parentEntity, ((ChartVO) vo).getParentEntityUID());
		} else if (vo instanceof DynamicTasklistVO) {
			result.setFieldValue(E.DYNAMICTASKLIST.meta, vo.getMeta());
			result.setFieldValue(E.DYNAMICTASKLIST.query, vo.getQuery());
		}
		
		return result;
	}
	
	public static MasterDataVO<UID> wrapDatasourceVO(ValuelistProviderVO vo, UID en) throws CommonFinderException, CommonPermissionException {
		MasterDataVO<UID> result = wrapDatasourceVO((DatasourceVO)vo, en);
		
		MasterDataFacadeLocal masterDataFacade = ServerServiceLocator.getInstance().getFacade(MasterDataFacadeLocal.class);
		MasterDataVO<UID> mdvoDb = null;
		try {
			mdvoDb = masterDataFacade.get(E.VALUELISTPROVIDER, vo.getId());
		} catch (CommonFinderException ex) {
			mdvoDb = null;
		}
		String resourceId = mdvoDb != null ? mdvoDb.getFieldValue(E.VALUELISTPROVIDER.detailsearchdescription) : null;
		
		LocaleFacadeLocal localeFacade = ServerServiceLocator.getInstance().getFacade(LocaleFacadeLocal.class);
		if (resourceId != null) {
			localeFacade.update(resourceId, LocaleInfo.parseTag(Locale.GERMAN), vo.getDetailsearchdescription(Locale.GERMAN));
			localeFacade.update(resourceId, LocaleInfo.parseTag(Locale.ENGLISH), vo.getDetailsearchdescription(Locale.ENGLISH));
		} else {
			resourceId = localeFacade.insert(null, LocaleInfo.parseTag(Locale.GERMAN), vo.getDetailsearchdescription(Locale.GERMAN));
			localeFacade.insert(resourceId, LocaleInfo.parseTag(Locale.ENGLISH), vo.getDetailsearchdescription(Locale.ENGLISH));
		}
		
		
		result.setFieldValue(getDsValueableField(en, E.VALUELISTPROVIDER.detailsearchdescription), resourceId);
		return result;
	}

	public static ValuelistProviderVO getValuelistProviderVO(MasterDataVO<UID> mdVO) {
		ValuelistProviderVO vo = new ValuelistProviderVO(
			getBaseVO(mdVO),
			mdVO.getFieldValue(E.VALUELISTPROVIDER.name),
			mdVO.getFieldValue(E.VALUELISTPROVIDER.description),
			mdVO.getFieldValue(E.VALUELISTPROVIDER.valid),
			mdVO.getFieldValue(E.VALUELISTPROVIDER.source),
			mdVO.getFieldUid(E.VALUELISTPROVIDER.nuclet));
		
		vo.setNuclet(mdVO.getFieldValue(E.VALUELISTPROVIDER.nuclet.getUID(), String.class));
		vo.setDetailsearchdescriptionResourceId(mdVO.getFieldValue(E.VALUELISTPROVIDER.detailsearchdescription));

		return vo;
	}

	public static RecordGrantVO getRecordGrantVO(MasterDataVO<UID> mdVO) {
		RecordGrantVO vo = new RecordGrantVO(
			getBaseVO(mdVO),
			mdVO.getFieldValue(E.RECORDGRANT.name),
			mdVO.getFieldValue(E.RECORDGRANT.description),
			mdVO.getFieldUid(E.RECORDGRANT.entity),
			mdVO.getFieldValue(E.RECORDGRANT.valid),
			mdVO.getFieldValue(E.RECORDGRANT.source),
			mdVO.getFieldUid(E.RECORDGRANT.nuclet));

		vo.setEntity((String) mdVO.getFieldValue(E.RECORDGRANT.entity.getUID()));
		vo.setNuclet(mdVO.getFieldValue(E.RECORDGRANT.nuclet.getUID(), String.class));
		
		return vo;
	}

	public static ChartVO getChartVO(MasterDataVO<UID> mdVO) {
		ChartVO vo = new ChartVO(
			getBaseVO(mdVO),
			mdVO.getFieldValue(E.CHART.name),
			mdVO.getFieldValue(E.CHART.description),
			mdVO.getFieldUid(E.CHART.detailsEntity),
			mdVO.getFieldUid(E.CHART.parentEntity),
			mdVO.getFieldValue(E.CHART.valid),
			mdVO.getFieldValue(E.CHART.source),
			mdVO.getFieldUid(E.CHART.nuclet));
		
		vo.setDetailsEntity(mdVO.getFieldValue(E.CHART.detailsEntity.getUID(), String.class));
		vo.setParentEntity(mdVO.getFieldValue(E.CHART.parentEntity.getUID(), String.class));
		vo.setNuclet(mdVO.getFieldValue(E.CHART.nuclet.getUID(), String.class));
		vo.setMeta(mdVO.getFieldValue(E.CHART.meta));
		vo.setQuery(mdVO.getFieldValue(E.CHART.query));
		
		return vo;
	}

	public static DynamicTasklistVO getDynamicTasklistVO(MasterDataVO<UID> mdVO) {
		DynamicTasklistVO vo = new DynamicTasklistVO(
			getBaseVO(mdVO),
			mdVO.getFieldValue(E.DYNAMICTASKLIST.name),
			mdVO.getFieldValue(E.DYNAMICTASKLIST.description),
			mdVO.getFieldValue(E.DYNAMICTASKLIST.valid),
			mdVO.getFieldValue(E.DYNAMICTASKLIST.source),
			mdVO.getFieldUid(E.DYNAMICTASKLIST.nuclet));
		
		vo.setNuclet(mdVO.getFieldValue(E.DYNAMICTASKLIST.nuclet.getUID(), String.class));
		vo.setMeta(mdVO.getFieldValue(E.DYNAMICTASKLIST.meta));
		vo.setQuery(mdVO.getFieldValue(E.DYNAMICTASKLIST.query));

		return vo;
	}
	
	public static CalcAttributeVO getCalcAttributeVO(MasterDataVO<UID> mdVO) {
		CalcAttributeVO vo = new CalcAttributeVO(
			getBaseVO(mdVO),
			mdVO.getFieldValue(E.CALCATTRIBUTE.name),
			mdVO.getFieldValue(E.CALCATTRIBUTE.description),
			mdVO.getFieldValue(E.CALCATTRIBUTE.valid),
			mdVO.getFieldValue(E.CALCATTRIBUTE.source),
			mdVO.getFieldUid(E.CALCATTRIBUTE.nuclet));
		
		vo.setNuclet(mdVO.getFieldValue(E.CALCATTRIBUTE.nuclet.getUID(), String.class));

		return vo;
	}

	public static MasterDataVO<UID> wrapValuelistProviderVO(ValuelistProviderVO vo) {
		MasterDataVO<UID> result = new MasterDataVO<UID>(E.VALUELISTPROVIDER.getUID(), vo.getPrimaryKey(), 
				vo.getChangedAt(), vo.getCreatedBy(), vo.getChangedAt(), vo.getChangedBy(), vo.getVersion());
		result.setFieldValue(E.VALUELISTPROVIDER.name, vo.getName());
		result.setFieldValue(E.VALUELISTPROVIDER.description, vo.getDescription());
		//result.setFieldValue(E.VALUELISTPROVIDER.detailsearchdescription, vo.getDetailsearchdescription());
		result.setFieldValue(E.VALUELISTPROVIDER.valid, vo.getValid());
		result.setFieldValue(E.VALUELISTPROVIDER.source, vo.getSource());
		result.setFieldUid(E.VALUELISTPROVIDER.nuclet, vo.getNucletUID());
		
		return result;
	}

	public static DynamicEntityVO getDynamicEntityVO(MasterDataVO<UID> mdVO) {
		DynamicEntityVO vo = new DynamicEntityVO(
			getBaseVO(mdVO),
			mdVO.getFieldValue(E.DYNAMICENTITY.name),
			mdVO.getFieldValue(E.DYNAMICENTITY.description),
			mdVO.getFieldUid(E.DYNAMICENTITY.entity),
			mdVO.getFieldValue(E.DYNAMICENTITY.valid),
			mdVO.getFieldValue(E.DYNAMICENTITY.source),
			mdVO.getFieldUid(E.DYNAMICENTITY.nuclet));

		vo.setEntity(mdVO.getFieldValue(E.DYNAMICENTITY.entity.getUID(), String.class));
		vo.setNuclet(mdVO.getFieldValue(E.DYNAMICENTITY.nuclet.getUID(), String.class));
		vo.setMeta(mdVO.getFieldValue(E.DYNAMICENTITY.meta));
		vo.setQuery(mdVO.getFieldValue(E.DYNAMICENTITY.query));
		
		return vo;
	}

	public static TimelimitTaskVO getTimelimitTaskVO(MasterDataVO<Long> mdVO, String sIdentifier, String sStatus, String sProcess) {
		TimelimitTaskVO vo = new TimelimitTaskVO(
			mdVO.getPrimaryKey(),
			mdVO.getFieldValue(E.TIMELIMITTASK.description),
			mdVO.getFieldValue(E.TIMELIMITTASK.expired),
			mdVO.getFieldValue(E.TIMELIMITTASK.completed),
			mdVO.getFieldId(E.TIMELIMITTASK.genericobject),
			sIdentifier,
			sStatus,
			sProcess,
			mdVO.getCreatedAt(),
			mdVO.getCreatedBy(),
			mdVO.getChangedAt(),
			mdVO.getChangedBy(),
			mdVO.getVersion());

		return vo;
	}

	public static MasterDataVO<Long> wrapTimelimitTaskVO(TimelimitTaskVO vo) {
		MasterDataVO<Long> result = new MasterDataVO<Long>(E.TIMELIMITTASK.getUID(), vo.getPrimaryKey(), 
				vo.getChangedAt(), vo.getCreatedBy(), vo.getChangedAt(), vo.getChangedBy(), vo.getVersion());
		result.setFieldId(E.TIMELIMITTASK.genericobject, vo.getGenericObjectId());
		result.setFieldValue(E.TIMELIMITTASK.expired, vo.getExpired());
		result.setFieldValue(E.TIMELIMITTASK.completed, vo.getCompleted());
		result.setFieldValue(E.TIMELIMITTASK.description, vo.getDescription());
		
		return result;
	}

	public static DefaultReportOutputVO getReportOutputVO(MasterDataVO<UID> mdVO) {
		UID dalEntity = mdVO.getEntityObject().getDalEntity();
		if (E.FORMOUTPUT.checkEntityUID(dalEntity)) {
			final ReportOutputVOContext context = createOutputFormatContext(mdVO, E.FORMOUTPUT);
			final DefaultReportOutputVO vo = new DefaultReportOutputVO(context);
			return vo;
		} else if (E.REPORTOUTPUT.checkEntityUID(dalEntity)) {
			final ReportOutputVOContext context = createOutputFormatContext(mdVO, E.REPORTOUTPUT);
			final DefaultReportOutputVO vo = new DefaultReportOutputVO(context);
			return vo;
		} else {
			throw new UnsupportedOperationException();
		}
	}
	
	/**
	 * create {@link ReportOutputVOContext} from {@link MasterDataVO}
	 * 
	 * @param mdVO	{@link MasterDataVO}
	 * @param meta	{@link EntityMeta} i.e. E.REPORTOUTPUT or E.FORMOUTPUT
	 * @return {@link ReportOutputVOContext}
	 */
	public static ReportOutputVOContext createOutputFormatContext(final MasterDataVO<UID> mdVO, final EntityMeta<UID> meta) {
		return new DefaultReportOutputVOContext(mdVO, meta);

	}

	public static MasterDataVO<UID> wrapReportOutputVO(EntityMeta<?> entity, DefaultReportOutputVO vo) {
		boolean isReport = entity.equals(E.REPORT); // otherwise FORM
		MasterDataVO<UID> result = new MasterDataVO<UID>(isReport ? E.REPORTOUTPUT.getUID() : E.FORMOUTPUT.getUID(), vo.getPrimaryKey(), 
				vo.getChangedAt(), vo.getCreatedBy(), vo.getChangedAt(), vo.getChangedBy(), vo.getVersion());
		if (isReport) {
			result.setFieldUid(E.REPORTOUTPUT.parent, vo.getReportUID());
			result.setFieldValue(E.REPORTOUTPUT.format, KeyEnum.Utils.unwrap(vo.getFormat()));
			result.setFieldValue(E.REPORTOUTPUT.destination, KeyEnum.Utils.unwrap(vo.getDestination()));
			result.setFieldValue(E.REPORTOUTPUT.parameter, vo.getParameter());
			result.setFieldValue(E.REPORTOUTPUT.sourceFile, vo.getSourceFile());
			result.setFieldValue(E.REPORTOUTPUT.filename, vo.getFilename());
			result.setFieldValue(E.REPORTOUTPUT.reportCLS, vo.getReportCLS());
			result.setFieldValue(E.REPORTOUTPUT.sourceFileContent, vo.getSourceFileContent());
			result.setFieldUid(E.REPORTOUTPUT.datasource, vo.getDatasourceUID());
			result.setFieldValue(E.REPORTOUTPUT.sheetname, vo.getSheetname());
			result.setFieldValue(E.REPORTOUTPUT.description, vo.getDescription());
			result.setFieldValue(E.REPORTOUTPUT.locale, vo.getLocale());
			result.setFieldValue(E.REPORTOUTPUT.copies, vo.getPrintProperties().getCopies());
			result.setFieldValue(E.REPORTOUTPUT.attach, vo.getAttachDocument());
			result.setFieldValue(E.REPORTOUTPUT.isDuplex, vo.getPrintProperties().isDuplex());
			result.setFieldUid(E.REPORTOUTPUT.tray, (UID)vo.getPrintProperties().getTrayId());
			result.setFieldUid(E.REPORTOUTPUT.printservice, ((UID)vo.getPrintProperties().getPrintServiceId()));
			result.setFieldValue(E.REPORTOUTPUT.isMandatory, vo.getIsMandatory());
			result.setFieldUid(E.REPORTOUTPUT.user, (vo.getUserId() != null) ? UID.parseUID(vo.getUserId().toString()) : null);
			result.setFieldUid(E.REPORTOUTPUT.role, (vo.getRoleId() != null) ? UID.parseUID(vo.getRoleId().toString()) : null);
			result.setFieldValue(E.REPORTOUTPUT.custom, vo.getCustomParameter());
			
			
		} else {
			result.setFieldUid(E.FORMOUTPUT.parent, vo.getReportUID());
			result.setFieldValue(E.FORMOUTPUT.format, KeyEnum.Utils.unwrap(vo.getFormat()));
			result.setFieldValue(E.FORMOUTPUT.destination, KeyEnum.Utils.unwrap(vo.getDestination()));
			result.setFieldValue(E.FORMOUTPUT.parameter, vo.getParameter());
			result.setFieldValue(E.FORMOUTPUT.sourceFile, vo.getSourceFile());
			result.setFieldValue(E.FORMOUTPUT.filename, vo.getFilename());
			result.setFieldValue(E.FORMOUTPUT.reportCLS, vo.getReportCLS());
			result.setFieldValue(E.FORMOUTPUT.sourceFileContent, vo.getSourceFileContent());
			result.setFieldUid(E.FORMOUTPUT.datasource, vo.getDatasourceUID());
			result.setFieldValue(E.FORMOUTPUT.sheetname, vo.getSheetname());
			result.setFieldValue(E.FORMOUTPUT.description, vo.getDescription());
			result.setFieldValue(E.FORMOUTPUT.locale, vo.getLocale());
			result.setFieldValue(E.FORMOUTPUT.copies, vo.getPrintProperties().getCopies());
			result.setFieldValue(E.FORMOUTPUT.attach, vo.getAttachDocument());
			result.setFieldValue(E.FORMOUTPUT.isDuplex, vo.getPrintProperties().isDuplex());
			result.setFieldUid(E.FORMOUTPUT.tray, (UID)vo.getPrintProperties().getTrayId());
			result.setFieldUid(E.FORMOUTPUT.printservice, ((UID)vo.getPrintProperties().getPrintServiceId()));
			result.setFieldValue(E.FORMOUTPUT.isMandatory, vo.getIsMandatory());
			result.setFieldUid(E.FORMOUTPUT.user, (vo.getUserId() != null) ? UID.parseUID(vo.getUserId().toString()) : null);
			result.setFieldUid(E.FORMOUTPUT.role, (vo.getRoleId() != null) ? UID.parseUID(vo.getRoleId().toString()) : null);
			result.setFieldValue(E.FORMOUTPUT.custom, vo.getCustomParameter());
		}
		return result;
	}

	public static SubreportVO getSubreportVO(MasterDataVO<UID> mdVO) {
		UID dalEntity = mdVO.getEntityObject().getDalEntity();
		if (E.SUBREPORT.checkEntityUID(dalEntity)) {
			SubreportVO vo = new SubreportVO(new NuclosValueObject<UID>(mdVO.getPrimaryKey(), mdVO.getCreatedAt(), mdVO.getCreatedBy(), mdVO.getChangedAt(), mdVO.getChangedBy(), mdVO.getVersion()),
					mdVO.getFieldUid(E.SUBREPORT.reportoutput),
					mdVO.getFieldValue(E.SUBREPORT.parametername),
					mdVO.getFieldValue(E.SUBREPORT.sourcefilename),
					mdVO.getFieldValue(E.SUBREPORT.sourcefileContent),
					mdVO.getFieldValue(E.SUBREPORT.reportCLS));
			return vo;
		} else if (E.SUBFORM.checkEntityUID(dalEntity)) {
			SubreportVO vo = new SubreportVO(new NuclosValueObject<UID>(mdVO.getPrimaryKey(), mdVO.getCreatedAt(), mdVO.getCreatedBy(), mdVO.getChangedAt(), mdVO.getChangedBy(), mdVO.getVersion()),
					mdVO.getFieldUid(E.SUBFORM.formoutput),
					mdVO.getFieldValue(E.SUBFORM.parametername),
					mdVO.getFieldValue(E.SUBFORM.sourcefilename),
					mdVO.getFieldValue(E.SUBFORM.sourcefileContent),
					mdVO.getFieldValue(E.SUBFORM.reportCLS));
			return vo;
		} else {
			throw new UnsupportedOperationException();
		}
	}

	public static MasterDataVO<UID> wrapSubreportVO(EntityMeta<?> entity, SubreportVO vo) {
		boolean isReport = entity.equals(E.REPORT); // otherwise FORM
		MasterDataVO<UID> result = new MasterDataVO<UID>(isReport ? E.SUBREPORT.getUID() : E.SUBFORM.getUID(), vo.getPrimaryKey(), 
				vo.getChangedAt(), vo.getCreatedBy(), vo.getChangedAt(), vo.getChangedBy(), vo.getVersion());
		if (isReport) {
			result.setFieldUid(E.SUBREPORT.reportoutput, vo.getReportOutputUID());
			result.setFieldValue(E.SUBREPORT.parametername, vo.getParameter());
			result.setFieldValue(E.SUBREPORT.sourcefilename, vo.getSourcefileName());
			result.setFieldValue(E.SUBREPORT.sourcefileContent, vo.getSourcefileContent());
			result.setFieldValue(E.SUBREPORT.reportCLS, vo.getReportCLS());
		} else {
			result.setFieldUid(E.SUBFORM.formoutput, vo.getReportOutputUID());
			result.setFieldValue(E.SUBFORM.parametername, vo.getParameter());
			result.setFieldValue(E.SUBFORM.sourcefilename, vo.getSourcefileName());
			result.setFieldValue(E.SUBFORM.sourcefileContent, vo.getSourcefileContent());
			result.setFieldValue(E.SUBFORM.reportCLS, vo.getReportCLS());
		}
		return result;
	}

	public static DefaultReportVO getReportVO(MasterDataVO<UID> mdVO, String currentUserName, UID mandator) {
		boolean isReport = E.REPORT.checkEntityUID(mdVO.getEntityObject().getDalEntity()); // otherwise FORM
		if (isReport) {
			DefaultReportVO vo = new DefaultReportVO(
					new NuclosValueObject<UID>(mdVO.getPrimaryKey(), mdVO.getCreatedAt(), mdVO.getCreatedBy(), mdVO.getChangedAt(), mdVO.getChangedBy(), mdVO.getVersion()),
					KeyEnum.Utils.findEnum(ReportType.class, mdVO.getFieldValue(E.REPORT.type)),
					mdVO.getFieldValue(E.REPORT.name),
					mdVO.getFieldValue(E.REPORT.description),
					mdVO.getFieldUid(E.REPORT.datasource),
					mdVO.getFieldValue(E.REPORT.parameter),
					mdVO.getFieldValue(E.REPORT.filename),
					mdVO.getFieldValue(E.REPORT.outputtype),
					mdVO.getFieldValue(E.REPORT.attach),
					getReportPermission(mdVO.getPrimaryKey(), currentUserName, mandator));
			return vo;
		} else {
			DefaultReportVO vo = new DefaultReportVO(
					new NuclosValueObject<UID>(mdVO.getPrimaryKey(), mdVO.getCreatedAt(), mdVO.getCreatedBy(), mdVO.getChangedAt(), mdVO.getChangedBy(), mdVO.getVersion()),
					KeyEnum.Utils.findEnum(ReportType.class, mdVO.getFieldValue(E.FORM.type)),
					mdVO.getFieldValue(E.FORM.name),
					mdVO.getFieldValue(E.FORM.description),
					mdVO.getFieldUid(E.FORM.datasource),
					mdVO.getFieldValue(E.FORM.parameter),
					mdVO.getFieldValue(E.FORM.filename),
					mdVO.getFieldValue(E.FORM.outputtype),
					mdVO.getFieldValue(E.FORM.attach),
					getReportPermission(mdVO.getPrimaryKey(), currentUserName, mandator));
			return vo;
		}
	}

	private static Permission getReportPermission(UID report, String currentUserName, UID mandator) {
		final Permission result;

		// Note that we don't use Permission.getPermission() here for performance reasons.
		// @todo use Permission.getPermission() after refactoring getWritableReportIds()/getReadableReportIds()
		if (SecurityCache.getInstance().getWritableReportIds(currentUserName, mandator).contains(report)) {
			result = Permission.READWRITE;
		}
		else if (CollectionUtils.concatAll(SecurityCache.getInstance().getReadableReports(currentUserName, mandator).values()).contains(report)) {
			result = Permission.READONLY;
		}
		else {
			result = Permission.NONE;
		}
		return result;
	}

	public static MasterDataVO<Long> wrapGenericObjectRelationVO(GenericObjectRelationVO vo) {
		MasterDataVO<Long> result = new MasterDataVO<Long>(E.GENERICOBJECTRELATION.getUID(), vo.getPrimaryKey(), 
				vo.getChangedAt(), vo.getCreatedBy(), vo.getChangedAt(), vo.getChangedBy(), vo.getVersion());
		result.setFieldId(E.GENERICOBJECTRELATION.source, vo.getSourceGOId());
		result.setFieldId(E.GENERICOBJECTRELATION.destination, vo.getDestinationGOId());
		result.setFieldValue(E.GENERICOBJECTRELATION.relationType, vo.getRelationType());
		result.setFieldValue(E.GENERICOBJECTRELATION.validFrom, vo.getValidFrom());
		result.setFieldValue(E.GENERICOBJECTRELATION.validUntil, vo.getValidUntil());
		result.setFieldValue(E.GENERICOBJECTRELATION.description, vo.getDescription());

		return result;
	}

	public static GenericObjectRelationVO getGenericObjectRelationVO(MasterDataVO<Long> mdVO) {
		GenericObjectRelationVO vo = new GenericObjectRelationVO(
			new NuclosValueObject<Long>(mdVO.getPrimaryKey(), mdVO.getCreatedAt(), mdVO.getCreatedBy(), mdVO.getChangedAt(), mdVO.getChangedBy(), mdVO.getVersion()),
			mdVO.getFieldId(E.GENERICOBJECTRELATION.source),
			mdVO.getFieldId(E.GENERICOBJECTRELATION.destination),
			mdVO.getFieldValue(E.GENERICOBJECTRELATION.relationType),
			mdVO.getFieldValue(E.GENERICOBJECTRELATION.validFrom),
			mdVO.getFieldValue(E.GENERICOBJECTRELATION.validUntil),
			mdVO.getFieldValue(E.GENERICOBJECTRELATION.description));

		return vo;
	}

	private static <PK> NuclosValueObject<PK> getBaseVO(MasterDataVO<PK> mdVO) {
		return new NuclosValueObject<PK>(
			mdVO.getPrimaryKey(),
			mdVO.getCreatedAt(),
			mdVO.getCreatedBy(),
			mdVO.getChangedAt(),
			mdVO.getChangedBy(),
			mdVO.getVersion());
	}

	public static LogbookVO getLogbookVO(MasterDataVO<Long> mdVO) {
		return new LogbookVO(
			new NuclosValueObject<Long>(
				mdVO.getPrimaryKey(),
				mdVO.getCreatedAt(),
				mdVO.getCreatedBy(),
				mdVO.getChangedAt(),
				mdVO.getChangedBy(),
				mdVO.getVersion()),
			mdVO.getFieldId(E.GENERICOBJECTLOGBOOK.genericObject),
			mdVO.getFieldUid(E.GENERICOBJECTLOGBOOK.attributeId),
			mdVO.getFieldUid(E.GENERICOBJECTLOGBOOK.masterdataId),
			mdVO.getFieldUid(E.GENERICOBJECTLOGBOOK.entityfieldId),
			mdVO.getFieldValue(E.GENERICOBJECTLOGBOOK.externalid),
			mdVO.getFieldValue(E.GENERICOBJECTLOGBOOK.action),
			mdVO.getFieldValue(E.GENERICOBJECTLOGBOOK.oldattributevalue),
			mdVO.getFieldValue(E.GENERICOBJECTLOGBOOK.oldexternalvalue),
			mdVO.getFieldValue(E.GENERICOBJECTLOGBOOK.oldvalue),
			mdVO.getFieldValue(E.GENERICOBJECTLOGBOOK.newattributevalue),
			mdVO.getFieldValue(E.GENERICOBJECTLOGBOOK.newexternalvalue),
			mdVO.getFieldValue(E.GENERICOBJECTLOGBOOK.newvalue));
	}

	public static MasterDataVO<Long> wrapHistoryVO(HistoryVO vo) {
		MasterDataVO<Long> result = new MasterDataVO<Long>(E.HISTORY.getUID(), vo.getPrimaryKey(), 
				vo.getChangedAt(), vo.getCreatedBy(), vo.getChangedAt(), vo.getChangedBy(), vo.getVersion());
		result.setFieldValue(E.HISTORY.entityobjectid, vo.getObjectId());
		result.setFieldValue(E.HISTORY.entityobjectuid, vo.getObjectUid());
		result.setFieldUid(E.HISTORY.entity, vo.getEntity());
		result.setFieldUid(E.HISTORY.entityfield, vo.getEntityField());
		result.setFieldUid(E.HISTORY.systemfield, vo.getSystemField());
		result.setFieldValue(E.HISTORY.savepoint, vo.getSavePoint());
		result.setFieldValue(E.HISTORY.username, vo.getUsername());
		result.setFieldValue(E.HISTORY.validuntil, vo.getValidUntil());
		result.setFieldValue(E.HISTORY.value_string, vo.getValueString());
		result.setFieldValue(E.HISTORY.value_byte, (byte[])vo.getValueByte());
		result.setFieldValue(E.HISTORY.value_refid, vo.getValueRefId());
		result.setFieldValue(E.HISTORY.value_refuid, vo.getValueRefUid());
		
		return result;
	}

	public static CommunicationPortVO getCommunicationPortVO(
			EntityObjectVO<UID> mdVO) {
		CommunicationPortVO result = new CommunicationPortVO(new NuclosValueObject<UID>(mdVO.getPrimaryKey(), mdVO.getCreatedAt(), mdVO.getCreatedBy(), mdVO.getChangedAt(), mdVO.getChangedBy(), mdVO.getVersion()),
				mdVO.getFieldValue(E.COMMUNICATION_PORT.portName),
				mdVO.getFieldValue(E.COMMUNICATION_PORT.description));
		return result;
	}
}
