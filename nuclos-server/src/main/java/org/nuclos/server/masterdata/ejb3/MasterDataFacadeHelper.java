//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.masterdata.ejb3;

import java.io.File;
import java.sql.ResultSet;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.ObjectUtils;
import org.nuclos.api.businessobject.Query;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.service.MessageContextService;
import org.nuclos.businessentity.NucletIntegrationPoint;
import org.nuclos.businessentity.facade.NucletIntegrationPointFacade;
import org.nuclos.common.DbField;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SimpleDbField;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableDbCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.collect.collectable.searchcondition.TrueCondition;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.MasterDataToEntityObjectTransformer;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.DalSupportForMD;
import org.nuclos.common.dal.DataSourceCaseSensivity;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.common.dblayer.DbObjectMessage;
import org.nuclos.common.entityobject.CollectableEOEntityField;
import org.nuclos.common.querybuilder.NuclosDatasourceException;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.transport.GzipList;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.IdUtils;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.TruncatableCollection;
import org.nuclos.common2.TruncatableCollectionDecorator;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.attribute.ejb3.AttributeFacadeLocal;
import org.nuclos.server.attribute.ejb3.LayoutFacadeLocal;
import org.nuclos.server.autosync.XMLEntities;
import org.nuclos.server.common.MandatorUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.RecordGrantUtils;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.dal.DalSupportForGO;
import org.nuclos.server.dal.DalUtils;
import org.nuclos.server.dal.processor.nuclet.IEOChunkableProcessor;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.dal.processor.nuclet.IPreferenceProcessor;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbAccess;
import org.nuclos.server.dblayer.DbBusinessException;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.DbObjectHelper;
import org.nuclos.server.dblayer.DbObjectHelper.DbObject;
import org.nuclos.server.dblayer.DbObjectHelper.DbObjectType;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.DbType;
import org.nuclos.server.dblayer.impl.util.PreparedStringBuilder;
import org.nuclos.server.dblayer.query.DbColumnExpression;
import org.nuclos.server.dblayer.query.DbCondition;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.query.DbSelection;
import org.nuclos.server.dblayer.statements.DbPlainStatement;
import org.nuclos.server.dblayer.statements.DbStatement;
import org.nuclos.server.dblayer.statements.DbStructureChange;
import org.nuclos.server.documentfile.DocumentFileUtils;
import org.nuclos.server.genericobject.ejb3.GenericObjectFacadeLocal;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.genericobject.valueobject.GenericObjectVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.history.ejb3.HistoryFacadeLocal;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.report.ejb3.DatasourceFacadeLocal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * Helper class for the MasterDataFacade.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:christoph.radig@novabit.de">christoph.radig</a>
 * @version 01.00.00
 */
@Component
public class MasterDataFacadeHelper {
	
	private static final Logger LOG = LoggerFactory.getLogger(MasterDataFacadeHelper.class);

	static final Long MAXROWS = 100L;

	public static enum RoleDependant {

		ROLE_ACTION(E.ROLEACTION, "22292", E.ACTION.action),
		ROLE_MODULE(E.ROLEMODULE, "22293", E.ROLEMODULE.module),
		ROLE_MASTERDATA(E.ROLEMASTERDATA, "22294", E.ROLEMASTERDATA.entity),
		ROLE_USER(E.ROLEUSER, "22290", E.ROLEUSER.user),
		ROLE_REPORT(E.ROLEREPORT, "22295", E.ROLEREPORT.report),
		ROLE_GENERATION(E.ROLEGENERATION, "22296", E.ROLEGENERATION.generation),
		ROLE_RECORDGRANT(E.ROLERECORDGRANT, "22297", E.RECORDGRANTUSAGE.recordGrant);

		private final EntityMeta<UID> entity;
		private final String resourceId;
		private final FieldMeta<?> entityFieldName;

		RoleDependant(EntityMeta<UID> entity, String resourceId, FieldMeta<?> entityFieldName) {
			this.entity = entity;
			this.resourceId = resourceId;
			this.entityFieldName = entityFieldName;
		}

		public EntityMeta<UID> getEntity() {
			return entity;
		}

		public String getResourceId() {
			return resourceId;
		}

		public FieldMeta<?> getEntityFieldName() {
			return entityFieldName;
		}

		public static RoleDependant getByEntityName(UID entityUid) {
			for (RoleDependant u : RoleDependant.class.getEnumConstants()) {
				if (u.getEntity().checkEntityUID(entityUid)) {
					return u;
				}
			}
			return null;
		}
	}
	
	//
	
	private HistoryFacadeLocal historyFacade;
		
	private AttributeFacadeLocal attributeFacade;

	private RecordGrantUtils grantUtils;
	
	private MetaProvider metaProvider;
	
	private SpringDataBaseHelper dataBaseHelper;
	
	private NucletDalProvider nucletDalProvider;
	
	private DatasourceFacadeLocal datasourceFacade;
	
	private MessageContextService messageService;

	@Autowired
	private MandatorUtils mandatorUtils;
	
	public MasterDataFacadeHelper() {
	}
	
	private DatasourceFacadeLocal getDatasourceFacade() {
		if (this.datasourceFacade == null)
			this.datasourceFacade = SpringApplicationContextHolder.getBean(DatasourceFacadeLocal.class);
		return this.datasourceFacade;
	}
	
	private HistoryFacadeLocal getHistoryFacade() {
		if (this.historyFacade == null)
			this.historyFacade = SpringApplicationContextHolder.getBean(HistoryFacadeLocal.class);
		return this.historyFacade;
	}

	@Autowired
	void setRecordGrantUtils(RecordGrantUtils grantUtils) {
		this.grantUtils = grantUtils;
	}
	
	@Autowired
	void setMasterDataMetaCache(MetaProvider masterDataMetaCache) {
		this.metaProvider = masterDataMetaCache;
	}
	
	@Autowired
	void setDataBaseHelper(SpringDataBaseHelper dataBaseHelper) {
		this.dataBaseHelper = dataBaseHelper;
	}
	
	@Autowired
	void setNucletDalProvider(NucletDalProvider nucletDalProvider) {
		this.nucletDalProvider = nucletDalProvider;
	}
	
	@Autowired
	void setMessageContextService(MessageContextService messageService) {
		this.messageService = messageService;
	}
	
	private void appendMandator(CollectableSearchExpression cse, EntityMeta<?> entity) {
		cse.setSearchCondition(mandatorUtils.append(cse, entity));
	}

	public void notifyClients(UID cachedEntityUid) {
		if (cachedEntityUid == null) {
			throw new NullArgumentException("sCachedEntityName");
		}
		if (!E.isNuclosEntity(cachedEntityUid)) {
			// NUCLOS-6825 as non-system-entities are not cached there is no need to notify anyone.
			return;
		}
		LOG.info("JMS send: notify clients that master data changed: {}", this);
		NuclosJMSUtils.sendOnceAfterCommitDelayed(cachedEntityUid, JMSConstants.TOPICNAME_MASTERDATACACHE);
	}

	public <PK> MasterDataVO<PK> getMasterDataCVOById(final EntityMeta<?> mdmetavo, final PK oId, boolean checkReadAllowed) throws CommonFinderException, CommonPermissionException {
		return getMasterDataCVOByIdImpl(mdmetavo, oId, checkReadAllowed, false);
	}
	
	public <PK> MasterDataVO<PK> getMasterDataCVOThinById(final EntityMeta<?> mdmetavo, final PK oId, boolean checkReadAllowed) throws CommonFinderException, CommonPermissionException {
		return getMasterDataCVOByIdImpl(mdmetavo, oId, checkReadAllowed, true);
	}

	public <PK> void checkReadAllowed(final EntityMeta<?> mdmetavo, final EntityObjectVO<PK> eo) throws CommonPermissionException {
		mandatorUtils.checkReadAllowed(eo, mdmetavo);
		grantUtils.checkInternal(mdmetavo.getUID(), eo.getPrimaryKey());
	}
	
	private <PK> MasterDataVO<PK> getMasterDataCVOByIdImpl(final EntityMeta<?> mdmetavo, final PK oId, boolean checkReadAllowed, boolean thin) throws CommonFinderException, CommonPermissionException {
		MasterDataVO<?> mdVO = XMLEntities.getSystemObjectById(mdmetavo.getUID(), oId);
		if (mdVO != null) {
			return (MasterDataVO<PK>) mdVO;
		}

		IEntityObjectProcessor<PK> eoProcessor = (IEntityObjectProcessor<PK>) nucletDalProvider.getEntityObjectProcessor(mdmetavo);
		if (thin) {
			eoProcessor.setThinReadEnabled(true);
		}
		EntityObjectVO<PK> eoResult = null;
		try {
			eoResult = eoProcessor.getByPrimaryKey(oId);
		} finally {
			if (thin) {
				eoProcessor.setThinReadEnabled(false);
			}
		}

		if (eoResult == null) {
			throw new CommonFinderException(StringUtils.getParameterizedExceptionMessage(
					"common.exception.novabitfinderexception.message", SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(mdmetavo), oId));
		}

		if (checkReadAllowed) {
			this.checkReadAllowed(mdmetavo, eoResult);
		}
		
		return DalSupportForMD.wrapEntityObjectVO(eoResult);
	}

	public <PK> void fillDependentsRecursive(Map<EntityAndField, UID> mapSubEntities,
											 Set<UID> stRequiredSubEnties,
											 IFillDependentPostProcessor postProcessor,
											 String currentUserName,
											 PK parentId,
											 IDependentDataMap dependentDataMapToFill,
											 UID parentSubform, final int dependantsDepth) {
		if (dependantsDepth != 0) {
			for (EntityAndField eafn : mapSubEntities.keySet()) {
				final UID currentParentSubform = mapSubEntities.get(eafn);
				UID subEntity = eafn.getEntity();

				if (RigidUtils.equal(currentParentSubform, parentSubform)
						&& stRequiredSubEnties != null && stRequiredSubEnties.contains(subEntity)) {

					UID sForeignKeyField = eafn.getField();
					final Collection<EntityObjectVO<PK>> collmdvo = getDependantMasterDataWithLimit(
							sForeignKeyField, null, currentUserName, null, null, parentId);

					final Collection<EntityObjectVO<PK>> collmdvoNotNull = CollectionUtils.emptyIfNull(collmdvo);

					if (postProcessor != null) {
						postProcessor.postProcess(subEntity, collmdvoNotNull);
					}

					dependentDataMapToFill.addAllData(eafn.getDependentKey(), collmdvoNotNull);

					// fill recursively
					for (EntityObjectVO<PK> vo : collmdvoNotNull) {
						fillDependentsRecursive(mapSubEntities, stRequiredSubEnties, postProcessor, currentUserName,
								vo.getPrimaryKey(), vo.getDependents(), subEntity, dependantsDepth != -1 ? dependantsDepth - 1 : dependantsDepth);
					}
				}
			}
		}
	}

	public interface IFillDependentPostProcessor<PK> {
		void postProcess(UID subEntity, Collection<EntityObjectVO<PK>> collmdvo);
	}

	/**
	 * gets the dependant master data records for the given entity, using the given foreign key field and given id(s) as foreign key(s).
	 * 
	 * §precondition oRelatedIds != null and not an empty list
	 * §todo restrict permissions by entity name
	 *  @param foreignKeyField UID of the field relating to the foreign entity
	 * @param filterCondition
	 * @param relatedIds id(s) by which sEntityName and sParentEntity are related
	 */
	public <PK, F> Collection<EntityObjectVO<PK>> getDependantMasterDataWithLimit(UID foreignKeyField, CollectableSearchCondition filterCondition, String username, Map<String, Object> mpParams, Integer limit, F... relatedIds) {
		F[] pks = relatedIds;
		if (pks == null || pks.length == 0) {
			throw new NullArgumentException("oRelatedId");			
		}

		final FieldMeta<?> fieldMeta = metaProvider.getEntityField(foreignKeyField);
		final EntityMeta<?> mdmetavo = metaProvider.getEntity(fieldMeta.getEntity());
		
		//Anonymous fetch (typically by Lucene) is not suitable for Proxy-Entities
		if (NuclosSystemParameters.ANONYMOUS_LOGIN.equals(username) && mdmetavo.isProxy()) {
			return Collections.emptyList();
		}
		
		Collection<MasterDataVO<PK>> result;
		if (mdmetavo.isChart() || mdmetavo.isDynamic()) {
			result = getChartsOrDynamicDependents(mdmetavo, mpParams, pks);
		}
		else {
			result = getDependantMasterDataByBean(foreignKeyField, filterCondition, pks, limit);
		}

		Collection<EntityObjectVO<PK>> colEntityObject = CollectionUtils.transform(result, new MasterDataToEntityObjectTransformer<PK>());
		return colEntityObject;
	}
	
	public <PK, F> Collection<EntityObjectVO<PK>> getDependantMasterData(UID foreignKeyField, String username, Map<String, Object> mpParams, F... relatedIds) {
		return getDependantMasterDataWithLimit(foreignKeyField, null, username, mpParams, null, relatedIds);
	}
	
	private <PK, F> Collection<MasterDataVO<PK>> getDependantMasterDataByBean(UID sForeignKeyField, CollectableSearchCondition filterCondition, F[] pks, Integer limit) {
		final FieldMeta<?> fieldMeta = metaProvider.getEntityField(sForeignKeyField);
		UID entityUid = fieldMeta.getEntity();
		final FieldMeta<?> efDeleted = SF.LOGICALDELETED.getMetaData(entityUid);
		
		CollectableSearchCondition baseCond = getSearchConditionForObjectOrList(fieldMeta, Arrays.asList(pks));

		if (efDeleted != null && metaProvider.getEntity(entityUid).getFields().contains(efDeleted)) {
			final CollectableEntityField clctEOEFdeleted = new CollectableEOEntityField(
					SF.LOGICALDELETED.getMetaData(entityUid));
			final CollectableSearchCondition condSearchDeleted = new CollectableComparison(
					clctEOEFdeleted, ComparisonOperator.EQUAL, new CollectableValueField(false));
			baseCond = SearchConditionUtils.and(baseCond, condSearchDeleted);
		}
		
		if (SecurityCache.getInstance().isMandatorPresent()) {
			baseCond = mandatorUtils.append(baseCond, metaProvider.getEntity(entityUid));
		}

		CompositeCollectableSearchCondition cond = new CompositeCollectableSearchCondition(LogicalOperator.AND, baseCond);
		if (filterCondition != null) {
			if (filterCondition instanceof CompositeCollectableSearchCondition) {
				if (((CompositeCollectableSearchCondition) filterCondition).getOperandCount() > 0) {
					cond.addOperand(filterCondition);
				}
			}
		}
		
		return getGenericMasterDataImpl(entityUid, new CollectableSearchExpression(cond), true, limit);
	}
	
	private <PK, F> Collection<MasterDataVO<PK>> getChartsOrDynamicDependents(EntityMeta<?> mdmetavo, Map<String, Object> mpParams, F[] pks) {
		Collection<MasterDataVO<PK>> result = new ArrayList<>();
		try {
			for (F pk : pks) {
				if (mdmetavo.isChart()) {
					final int maxRows = 100000;		
					final DatasourceVO datasourceVO = getDatasourceFacade().getChart(
							new UID(mdmetavo.getUID().getString().substring(E.CHART.getUID().getString().length() + 1)));
					
					final Map<String, Object> mpTempParams = mpParams != null ? new HashMap<>(mpParams) : new HashMap<String, Object>();
					mpTempParams.put("genericObject", pk);
					Collection<MasterDataVO<PK>> r1 = getDependantMasterDataForDatasource(mdmetavo, datasourceVO, mpTempParams, maxRows);
					result.addAll(r1);
					
				} else if (mdmetavo.isDynamic()) {					
					// @see NUCLOS-654
					DataSourceCaseSensivity datasourceCaseSensivity = new DataSourceCaseSensivity(mdmetavo);
					Collection<MasterDataVO<PK>> r1 = getDependantMasterDataForDatasource(pk, mdmetavo, datasourceCaseSensivity);
					result.addAll(r1);
					
				}
			}
		} catch (Exception e) {
			LOG.warn("getDependantMasterDataForDatasource failed for datasource {}", mdmetavo.getEntityName(), e);
		}
		
		return result;
	}
	
	private static CollectableSearchCondition getSearchConditionForObjectOrList(FieldMeta<?> fieldMeta, Object obj) {
		if (obj instanceof Collection) {
			Collection<?> lstObj = (Collection<?>)obj;
			if(lstObj.isEmpty()) {
				return TrueCondition.TRUE;
			}
			Iterator<?> it = lstObj.iterator();
			CollectableSearchCondition[] csc = new CollectableSearchCondition[lstObj.size()];
			int i = 0;
			while (it.hasNext()) {
				csc[i++] = getIDComparison(fieldMeta, it.next());
			}
			return SearchConditionUtils.or(csc);
		} else {
			return getIDComparison(fieldMeta, obj);
		}
	}
	
	private static CollectableSearchCondition getIDComparison(FieldMeta<?> fieldMeta, Object id) {
		if (id instanceof UID) {
			return SearchConditionUtils.newUidComparison(fieldMeta, ComparisonOperator.EQUAL, (UID) id);
		} else {
			return SearchConditionUtils.newIdComparison(fieldMeta, ComparisonOperator.EQUAL, IdUtils.toLongId(id));
		}
	}

	/*
	 * Building SQL Statements here (outside the DBLayer) is wrong.
	 * Furthermore this method doesn't support paging, nor sorting, nor reducing the number of fields
	 * Use EntityObjectProcessor instead!
	 */
	@Deprecated
	private <PK, F> Collection<MasterDataVO<PK>> getDependantMasterDataForDatasource(
			F oRelatedId, final EntityMeta<?> mdmetavo, DataSourceCaseSensivity datasourceCaseSensivity) {
		
		final List<FieldMeta<?>> lstFields = new ArrayList<>(mdmetavo.getFields());
		DbField<?> pkDbField = mdmetavo.getPk();

		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom<PK> t = (DbFrom<PK>) query.from(mdmetavo);
		String username = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
		t.setUsername(username);
		
		DbColumnExpression<F> goColumn = null;
		List<DbSelection<?>> selection = new ArrayList<>();
		selection.add(datasourceCaseSensivity.isIntidCaseInsensitive() 
				? (DbColumnExpression<PK>)t.baseColumn(pkDbField)
				: (DbColumnExpression<PK>)t.baseColumnSensitiveOrInsensitive(pkDbField, true, false));
		Iterator<FieldMeta<?>> itFieldMetas = lstFields.iterator();
		while (itFieldMetas.hasNext()) {
			FieldMeta<?> field = itFieldMetas.next();
			SimpleDbField<?> f = (SimpleDbField<?>) SimpleDbField.create(field.getDbColumn(), field.getJavaClass());
			if (mdmetavo.getPk().getDbColumn().equalsIgnoreCase(field.getDbColumn())) {
				// Old style: INTID part of fields in meta, not used any more...
				LOG.warn("INTID is part of fields in meta " + mdmetavo.getEntityName());
				itFieldMetas.remove();
			} else if (E.GENERICOBJECT.checkEntityUID(field.getForeignEntity())) {
				goColumn = datasourceCaseSensivity.isIntidGenericObjectCaseInsensitive() 
						? (DbColumnExpression<F>)t.baseColumn(f)
						: (DbColumnExpression<F>)t.baseColumnSensitiveOrInsensitive(f, true, false);
				selection.add(goColumn);
			} else {
				selection.add(t.baseColumnSensitiveOrInsensitive(f, true, true));
			}
		}
		query.multiselect(selection);
		query.where(builder.equalValue(goColumn, oRelatedId));

		return dataBaseHelper.getDbAccess().executeQuery(query, (DbTuple tuple) -> {
			final MasterDataVO<PK> mdvo = new MasterDataVO<>(mdmetavo, false);
			mdvo.setPrimaryKey((PK)tuple.get(0));
			int i = 1;
			for (FieldMeta<?> field: lstFields) {
				Object value = tuple.get(i);
				if (E.GENERICOBJECT.checkEntityUID(field.getForeignEntity())) {
					mdvo.setFieldId(field.getUID(), Long.parseLong(value.toString()));
				} else {
					mdvo.setFieldValue(field.getUID(), value);
				}
				++i;
			}
			return mdvo;
		});
	}
	
	private <PK, F> Collection<MasterDataVO<PK>> getDependantMasterDataForDatasource(
			final EntityMeta<?> mdmetavo, final DatasourceVO datasourceVO, final Map<String, Object> mpParams, final int maxRows)
					throws NuclosDatasourceException {
		
		final String sql = getDatasourceFacade().createSQL(datasourceVO, mpParams);
		//TODO: This is an unlimited Fetch. Limit!
		return dataBaseHelper.getDbAccess().executePlainQuery(sql, maxRows, (ResultSet result) -> {
			final Collection<MasterDataVO<PK>> values = new ArrayList<>();

			int counter = 0;
			while (result.next()) {
				if (maxRows >= 0 && counter++ >= maxRows) {
					break;
				}

				final MasterDataVO<PK> mdvo = new MasterDataVO<PK>(mdmetavo, false);

				int i = 1;
				for (FieldMeta<?> field : mdmetavo.getFields()) {
					Object value = result.getObject(i);
					if (mdmetavo.getPk().getDbColumn().equalsIgnoreCase(field.getDbColumn())) {
						mdvo.setPrimaryKey((PK)value);
						mdvo.setFieldValue(field.getUID(), value);
					} else if (E.GENERICOBJECT.checkEntityUID(field.getForeignEntity())) {
						mdvo.setFieldId(field.getUID(), Long.parseLong(value.toString()));
					} else {
						mdvo.setFieldValue(field.getUID(), value);
					}
					++i;
				}
				values.add(mdvo);
			}
			return values;
		});
	}
	
	/**
	 * Called after an entity was changed - that is, a row was inserted, updated or deleted.
	 * @param mdmetavo the entity that was changed.
	 */
	private void entityChanged(EntityMeta<?> mdmetavo) {
		UID entityUID = mdmetavo.getUID();

		//if (E.ROLEUSER.checkEntityUID(entityUID)) {
			// Rights are reloaded the next time the user logs in - we don't need to do anything here
			// @todo To make these changes visible immediately, however, we could notify the client and change the roles dynamically in the server.
			// But can we do that in a J2EE conformant way?
			//NucleusSecurityProxy.invalidateMethodRightsForUser(mdvoChanged.getField("user", String.class));
		//} else if (E.ROLEACTION.checkEntityUID(entityUID)) {
			//NucleusSecurityProxy.invalidateMethodRightsForAllUsers();
		//}

		if (mdmetavo.isCacheable()) {
			this.notifyClients(mdmetavo.getUID());
		} else {
			if (E.SERVERCODE.checkEntityUID(entityUID))
				this.notifyClients(mdmetavo.getUID());	
		}
	}
	
	/**
	 * performs a stale version check.
	 * @param mdvo
	 * @throws CommonStaleVersionException
	 */
	<PK> MasterDataVO<PK> checkForStaleVersion(EntityMeta<PK> mdMetaVO, MasterDataVO<PK> mdvo, MasterDataVO<PK> mdvoThinFromDB)
			throws CommonStaleVersionException, CommonPermissionException, CommonFinderException {
		if (mdvo.getEntityObject().isSkipLoadingFromDB()) return mdvo;

		if (mdvoThinFromDB == null) {
			mdvoThinFromDB = getMasterDataCVOThinById(mdMetaVO, mdvo.getPrimaryKey(), true);
		}
		if (mdvo.getVersion() != mdvoThinFromDB.getVersion()) {
			List<String> msgs = metaProvider.getVersionConflictMessages("master data", mdvo.getEntityObject(), mdvoThinFromDB.getEntityObject());
			throw new CommonStaleVersionException(msgs);
		}
		if (mdvo.isSystemRecord()) {
			throw new CommonPermissionException();
		}

		return mdvoThinFromDB;
	}

	<PK> void checkInvariantFields(EntityMeta<?> mdMetaVO, MasterDataVO<PK> mdvo, MasterDataVO<PK> mdvoInDataBase) throws CommonValidationException {
		for (FieldMeta<?> mdMetaFieldVO : mdMetaVO.getFields()) {
			if (!mdMetaFieldVO.isInvariant()) {
				continue;
			}
			final UID fieldName = mdMetaFieldVO.getUID();
			final boolean bValid;
			if (mdMetaFieldVO.getForeignEntity() != null || mdMetaFieldVO.getUnreferencedForeignEntity() != null) {
				EntityMeta<Object> eMeta = metaProvider.getEntity((UID) ObjectUtils.defaultIfNull(mdMetaFieldVO.getUnreferencedForeignEntity(), mdMetaFieldVO.getForeignEntity()));
				if (eMeta.isUidEntity()) {
					bValid = ObjectUtils.equals(mdvo.getFieldUid(fieldName), mdvoInDataBase.getFieldUid(fieldName));
				} else {
					bValid = ObjectUtils.equals(mdvo.getFieldId(fieldName), mdvoInDataBase.getFieldId(fieldName));
				}
			} else {
				bValid = ObjectUtils.equals(mdvo.getFieldValue(fieldName), mdvoInDataBase.getFieldValue(fieldName));
			}
			if (!bValid) {
				throw new CommonValidationException(MessageFormat.format(
						"Field \"{0}\" cannot be changed because it is declared as invariant", fieldName));
			}
		}
	}

	/**
	 * removes a single masterdata row.
	 * 
	 * §precondition sEntityName != null
	 */
	<PK> void removeSingleRow(final MasterDataVO<PK> mdvo, String customUsage)
			throws CommonFinderException, CommonRemoveException, CommonStaleVersionException, CommonPermissionException {

		final PK pk = mdvo.getPrimaryKey();
		if (pk == null) {
			throw new NuclosFatalException("mdhelper.error.invalid.id");//"Der Datensatz hat eine leere Id.");
		}

		final UID entityUid = mdvo.getEntityObject().getDalEntity();
		final EntityMeta<PK> mdmetavo = metaProvider.getEntity(entityUid);

		final MasterDataVO<?> mdvoInDB = mdvo; //checkForStaleVersion(mdmetavo, mdvo);
		
		boolean removed = false;
		if (E.isNuclosEntity(entityUid)) {
			if (E.DBSOURCE.checkEntityUID(entityUid)) {
				try {
		            updateDbObject((EntityObjectVO<UID>) mdvo.getEntityObject(), null, -1);
		            removed = true;
	            }
	            catch(NuclosBusinessException e) {
		            throw new CommonRemoveException("updateDbObject failed: " + e, e);
	            }
			} else if (E.DBOBJECT.checkEntityUID(entityUid)) {
				CollectableSearchCondition cond = SearchConditionUtils.newUidComparison(E.DBSOURCE.dbobject, ComparisonOperator.EQUAL, (UID) mdvo.getPrimaryKey());
				for (EntityObjectVO<?> source : nucletDalProvider.getEntityObjectProcessor(E.DBSOURCE).getBySearchExpression(
					grantUtils.append(new CollectableSearchExpression(cond), entityUid))) {
						removeSingleRow(DalSupportForMD.wrapEntityObjectVO(source), customUsage);
				}
			} else if (E.VALUELISTPROVIDER.checkEntityUID(entityUid)) {
				LayoutFacadeLocal layoutFacadeLocal = SpringApplicationContextHolder.getBean(LayoutFacadeLocal.class);
				HashSet<UID> layoutUIDs = new HashSet<>();
				for (UID uid : metaProvider.getAllEntityUids()) {
					layoutUIDs.addAll(layoutFacadeLocal.getAllLayoutUidsForEntity(uid));
				}

				int usages = 0;
				for (UID layoutUID : layoutUIDs) {
					try {
						if (layoutFacadeLocal.getLayoutML(layoutUID).contains(entityUid.getString())) {
							usages++;
						}
					} catch (CommonBusinessException e) {
						throw new CommonRemoveException(e.getMessage(), e);
					}
				}
				if (usages > 0) {
					throw new CommonRemoveException(StringUtils.getParameterizedExceptionMessage("masterdata.error.delete.usage.exists", mdvo.getFieldValue(E.VALUELISTPROVIDER.name), usages));
				}
			}
		}

		try {
			if (!removed) { 
				nucletDalProvider.getEntityObjectProcessor(mdmetavo).delete(new Delete<>(pk, entityUid));
			}
		} catch (DbBusinessException ex) {
			throw ex;
		} catch (CommonFatalException ex) {
			throw new CommonRemoveException("dblayer failed: " + ex, ex);
		}
		
		// remove preferences
		if (E.ROLEPREFERENCE.checkEntityUID(entityUid)) {
			UID prefUID = mdvo.getFieldUid(E.ROLEPREFERENCE.preference);
			IPreferenceProcessor prefsProc = SpringApplicationContextHolder.getBean(IPreferenceProcessor.class);
			prefsProc.batchDeleteCustomizations(prefUID, false);
		} else if (E.ROLEUSER.checkEntityUID(entityUid)) {
			UID userUID = mdvo.getFieldUid(E.ROLEUSER.user);
			IPreferenceProcessor prefsProc = SpringApplicationContextHolder.getBean(IPreferenceProcessor.class);
			prefsProc.batchDeleteCustomizationsForUser(userUID);
		}

		if (MetaProvider.isDataEntity(mdmetavo, true)) {
			DocumentFileUtils.removeDocumentFiles(mdvo.getEntityObject());
		}

		getHistoryFacade().trackRemoveToLogbookIfPossible(mdvoInDB.getEntityObject());
		entityChanged(mdmetavo);
	}

	/**
	 * modifies a single masterdata row.
	 * @param entityUid
	 * @param mdvo
	 * @param sUserName
	 * @return
	 * @throws CommonStaleVersionException
	 * @throws CommonValidationException
	 */
	<PK> PK modifySingleRow(UID entityUid, MasterDataVO<PK> mdvo, MasterDataVO<PK> mdvoThinFromDB, String sUserName, String customUsage)
			throws CommonCreateException, CommonFinderException, CommonStaleVersionException, CommonValidationException, CommonPermissionException {

		final EntityMeta<PK> mdmetavo = metaProvider.getEntity(entityUid);
		mdvoThinFromDB = checkForStaleVersion(mdmetavo, mdvo, mdvoThinFromDB);
		checkInvariantFields(mdmetavo, mdvo, mdvoThinFromDB);
		validateUniqueConstraintWithJson(mdmetavo, mdvo);

		if(E.USER.getUID().equals(entityUid)
			&& sUserName.equalsIgnoreCase(mdvoThinFromDB.getFieldValue(E.USER.username))
			&& !mdvoThinFromDB.getFieldValue(E.USER.username).equalsIgnoreCase(mdvo.getFieldValue(E.USER.username))) {
				throw new CommonPermissionException("masterdata.error.change.own.user.name");
		}

		IEntityObjectProcessor<PK> eoProcessor = nucletDalProvider.getEntityObjectProcessor(mdmetavo);
		EntityObjectVO<PK> eoVO = mdvo.getEntityObject();
		DalUtils.updateVersionInformation(eoVO, sUserName);
		eoVO.flagUpdate();

		boolean updated = false;
		EntityObjectVO<PK> eoOld = null;
		if (mdmetavo.isLogBookTracking()) {
			// we need stringified values for the log book
			eoOld = getMasterDataCVOById(mdmetavo, eoVO.getPrimaryKey(), false).getEntityObject();
		}

		if (E.DBSOURCE.checkEntityUID(entityUid)) {
			try {
				updateDbObject((EntityObjectVO<UID>) mdvoThinFromDB.getEntityObject(), (EntityObjectVO<UID>) eoVO, 0);
				updated = true;
			} catch (NuclosBusinessException e) {
				throw new CommonCreateException(e.getMessage(), e);
			}
		}

		Map<UID, UID> existingDocumentFileMap = Collections.emptyMap();
		if (MetaProvider.isDataEntity(mdmetavo, true)) {
			existingDocumentFileMap = DocumentFileUtils.getExistingDocumentFiles(eoVO);
			DocumentFileUtils.storeDocumentFiles(eoVO, existingDocumentFileMap);
		}

		try {
			if (!updated) {
				eoProcessor.insertOrUpdate(eoVO);
			}
		} catch (DbBusinessException ex) {
			throw ex;
		} catch (DbException e) {
			throw new CommonCreateException(e.getMessage(), e);
		}

		if (!existingDocumentFileMap.isEmpty()) {
			DocumentFileUtils.removeDocumentFiles(eoVO, existingDocumentFileMap);
		}

		if (mdmetavo.isLogBookTracking()) {
			if (eoOld == null) {
				throw new NuclosFatalException("EO for LogBookTracking is null.");
			}
			getHistoryFacade().trackChangesToLogbookIfPossible(eoOld, eoVO);
		}
		entityChanged(mdmetavo);
		return mdvo.getPrimaryKey();
	}

	/**
	 * creates a single masterdata row.
	 * 
	 * §precondition mdvo.getId() == null
	 * 
	 * @return the new id of the created row
	 */
	<PK> PK createSingleRow(MasterDataVO<PK> mdvoToCreate, String sUserName, PK intid) throws
		CommonCreateException, CommonValidationException {
		if (mdvoToCreate.getPrimaryKey() != null) {
			throw new IllegalArgumentException("mdvoToCreate.getId()");
		}
		final UID entityUid = mdvoToCreate.getEntityObject().getDalEntity();
		final EntityMeta<?> mdmetavo = metaProvider.getEntity(entityUid);

		validateUniqueConstraintWithJson(mdmetavo, mdvoToCreate);

		// @todo optimize: use idfactory.nextval for insert
		
		final boolean isInsertWoId = mdvoToCreate.getEntityObject().isbInsertWoId();
		 PK result;
		if (intid != null || isInsertWoId) {
			result = intid;
		} else {
			if (mdvoToCreate.getPrimaryKey() != null) {
				result = mdvoToCreate.getPrimaryKey();
			} else {
				if (mdmetavo.getPkClass() == UID.class) {
					result = (PK) new UID();
				} else {
					final String idFactory = mdmetavo.getIdFactory();
					if (idFactory == null) {
						result = (PK) dataBaseHelper.getNextIdAsLong(SpringDataBaseHelper.DEFAULT_SEQUENCE);
					} else {
						result = (PK) dataBaseHelper.getDbAccess().executeFunction(idFactory, Long.class);
					}
				}
			}
		}
		if (!isInsertWoId) {
			mdvoToCreate.setPrimaryKey(result);
		}

		final IEntityObjectProcessor<PK> eoProcessor 
			= (IEntityObjectProcessor<PK>) nucletDalProvider.getEntityObjectProcessor(mdmetavo);

		final EntityObjectVO<PK> eoVO = mdvoToCreate.getEntityObject();
		eoVO.setCreatedBy(null);
		eoVO.setCreatedAt(null);
		if (mdmetavo.isUidEntity()) {
			eoVO.removeFieldValue(SF.IMPORTVERSION.getUID(entityUid));
			eoVO.removeFieldValue(SF.ORIGINUID.getUID(entityUid));
		}
		DalUtils.updateVersionInformation(eoVO, sUserName);
		eoVO.flagNew();

		boolean created = false;

		if (E.DBSOURCE.checkEntityUID(entityUid)) {
			try {
				updateDbObject(null, (EntityObjectVO<UID>) eoVO, 1);
				created = true;
			} 
			catch (NuclosBusinessException e) {
				throw new CommonCreateException(e.getMessage(), e);
			}
		}

		if (MetaProvider.isDataEntity(mdmetavo, true)) {
			DocumentFileUtils.storeDocumentFiles(eoVO, null);
		}

		try {
			if (!created) {
				PK tmpResult = (PK) eoProcessor.insertOrUpdate(eoVO);
				if (metaProvider.getEntity(entityUid).isWriteProxy()) {
					result = tmpResult;
				}
			}
		} catch (DbBusinessException ex) {
			throw ex;
		} catch (DbException e) {
			throw new CommonCreateException(e.toString(), e);
		}

		entityChanged(mdmetavo);

		if (isInsertWoId) {
			return null;
		}
		return result;
	}

	private void validateUniqueConstraintWithJson(EntityMeta<?> mdmetavo, MasterDataVO<?> mdvoToCreate) throws CommonValidationException {
		if (!XMLEntities.hasSystemData(mdmetavo.getUID())) {
			return;
		}
		CompositeCollectableSearchCondition cond = new CompositeCollectableSearchCondition(LogicalOperator.AND);
		for (FieldMeta<?> field : mdmetavo.getFields()) {
			if (field.isUnique()) {
				final UID fieldUid = field.getUID();
				if (mdvoToCreate.getFieldValue(fieldUid) != null) {
					if (field.getForeignEntity() != null) {
						Object id = mdvoToCreate.getFieldId(fieldUid);
						if (id == null) {
							id = mdvoToCreate.getFieldUid(fieldUid);
						}
						cond.addOperand(SearchConditionUtils.newPkComparison(
								field, ComparisonOperator.EQUAL, id));
					} else {
						cond.addOperand(SearchConditionUtils.newComparison(
								field, ComparisonOperator.EQUAL, 
								mdvoToCreate.getFieldValue(field.getUID())));
					}
				} else {
					cond.addOperand(SearchConditionUtils.newIsNullCondition(field));
				}
			}
		}

		if (cond.getOperandCount() > 0) {
			final Collection<MasterDataVO<UID>> systemObjects = XMLEntities.getSystemObjects(mdmetavo.getUID(), cond);
			if (!systemObjects.isEmpty()) {
				throw new CommonValidationException("nuclos.validation.systementity.unique");
			}
		}
	}

	/**
	 * removes the given dependants.
	 * @param mpDependants
	 * @throws CommonFinderException
	 * @throws CommonRemoveException
	 * @throws CommonStaleVersionException
	 */
	public void removeDeletedDependants(IDependentDataMap mpDependants, String customUsage, boolean deleteAll)
			throws CommonFinderException, CommonRemoveException, CommonStaleVersionException, CommonPermissionException {
		
		for (IDependentKey dependentKey : mpDependants.getKeySet()) {
			FieldMeta<?> refFieldMeta = metaProvider.getEntityField(dependentKey.getDependentRefFieldUID());
			EntityMeta<?> eMeta = metaProvider.getEntity(refFieldMeta.getEntity());
			if (!eMeta.isDynamic()) {
				for (EntityObjectVO<?> mdvoDependant : mpDependants.getData(dependentKey)) {
					if (!eMeta.isEditable() && !mdvoDependant.hasDirtySubformData()) {
						continue;
					}
					removeDeletedDependants(mdvoDependant.getDependents(), customUsage, mdvoDependant.isFlagRemoved());
					if ((mdvoDependant.isFlagRemoved() || deleteAll) && mdvoDependant.getPrimaryKey() != null) {
						if (eMeta.isStateModel()) {
							try {
								GenericObjectFacadeLocal goLocal = SpringApplicationContextHolder.getBean(GenericObjectFacadeLocal.class);
								goLocal.remove(mdvoDependant.getDalEntity(), (Long)mdvoDependant.getPrimaryKey(), true, customUsage);
							} catch (CommonFinderException e) {
								//ignore this, because if it cannot be found an item seems to be removed already
							} catch (CommonCreateException e) {
								throw new NuclosFatalException(e);
							}
							catch (NuclosBusinessException e) {
								throw new NuclosFatalException(e);
							}
						} else {
							// remove the row:
							MasterDataVO<?> voDependant = DalSupportForMD.wrapEntityObjectVO(mdvoDependant);
							removeSingleRow(voDependant, customUsage);
						}
					}
				}
			}
		}
	}

	/**
	 * creates/modifies the given dependants.
	 * @param mpDependants
	 * @param sUserName
	 * @param bValidate
	 * @throws CommonCreateException
	 * @throws CommonValidationException
	 * @throws CommonFinderException
	 * @throws CommonStaleVersionException
	 */
	<PK> void createOrModifyDependants(IDependentDataMap mpDependants, String sUserName, 
			boolean bValidate, PK iParentId, String customUsage)
			throws CommonCreateException, CommonValidationException, CommonFinderException, 
			CommonStaleVersionException, CommonPermissionException {

		for (IDependentKey dependentKey : mpDependants.getKeySet()) {
			for (EntityObjectVO<?> mdvoDependant : mpDependants.getData(dependentKey)) {
				final EntityMeta<?> eMeta = MetaProvider.getInstance().getEntity(mdvoDependant.getDalEntity());
				if (!eMeta.isEditable() && !mdvoDependant.hasDirtySubformData()) {
					continue;
				}
				
				// create/modify the row:
				PK intid = null;
				
				final UID sForeignIdFieldUid = dependentKey.getDependentRefFieldUID();
				if (iParentId instanceof UID) {
					mdvoDependant.setFieldUid(sForeignIdFieldUid, (UID) iParentId);
				} else {
					mdvoDependant.setFieldId(sForeignIdFieldUid, IdUtils.toLongId(iParentId));
				}
				
				if (eMeta.isStateModel()) {
					try {
						final EntityObjectVO<Long> dep = (EntityObjectVO<Long>) mdvoDependant;
						final GenericObjectVO govo = DalSupportForGO.getGenericObjectVO(dep);
						final GenericObjectFacadeLocal goLocal = SpringApplicationContextHolder.getBean(GenericObjectFacadeLocal.class);
						final IDependentDataMap deps = mdvoDependant.getDependents();
						if(mdvoDependant.isFlagNew()) {
							goLocal.create(new GenericObjectWithDependantsVO(govo, deps, govo.getDataLanguageMap()), customUsage);
						}
						else if(mdvoDependant.getPrimaryKey() != null && mdvoDependant.isFlagRemoved()) {
							// do not remove dependents here! 
							// see org.nuclos.server.masterdata.ejb3.MasterDataFacadeHelper.removeDeletedDependants(IDependentDataMap, String, boolean)
							// goLocal.remove(govo.getModule(), govo.getPrimaryKey(), true, customUsage);
						}
						else if (mdvoDependant.getPrimaryKey() != null && (mdvoDependant.isFlagUpdated() || deps.getPendingChanges())) {
							goLocal.modify(new GenericObjectWithDependantsVO(govo, deps, govo.getDataLanguageMap()), false, customUsage);
						}
					}
					catch (NuclosBusinessException e) {
						throw new NuclosFatalException(e);
					}
					catch (CommonRemoveException e) {
						throw new NuclosFatalException(e);
					}
				} else {
					final EntityObjectVO<PK> dep = (EntityObjectVO<PK>) mdvoDependant;
					final MasterDataVO<PK> voDependant = (MasterDataVO<PK>) DalSupportForMD.wrapEntityObjectVO(mdvoDependant);
					PK id = createOrModify(voDependant, dependentKey, sUserName, bValidate, intid, customUsage);
					dep.setPrimaryKey(id);
				}
			}
		}
	}

	/**
	 * creates the given dependant row, if it is new or updates it, if it has changed.
	 * @param mdvoDependant
	 * @param sUserName
	 * @param bValidate
	 * @throws CommonCreateException
	 * @throws CommonValidationException
	 * @throws CommonStaleVersionException
	 */
	private <PK> PK createOrModify(MasterDataVO<PK> mdvoDependant, IDependentKey dependentKeyForValidation,
			String sUserName, boolean bValidate, PK intid, String customUsage)
			throws CommonCreateException, CommonValidationException, CommonFinderException, 
			CommonStaleVersionException, CommonPermissionException {

		final EntityMeta<?> meta = metaProvider.getEntity(mdvoDependant.getEntityObject().getDalEntity());
		
		Object id = mdvoDependant.getFieldId(dependentKeyForValidation.getDependentRefFieldUID());
		if (id == null) {
			id = mdvoDependant.getFieldUid(dependentKeyForValidation.getDependentRefFieldUID());
		}
		
		if (id != null && !meta.isDynamic()
				&& !mdvoDependant.isRemoved()) {
			PK iReferenceId;
	
			if (mdvoDependant.getEntityObject().isFlagUnchanged()){ 
				if (mdvoDependant.getPrimaryKey() == null) { // must be an removed unchanged object. @see NUCLOS-2468
					return null;
				}
				iReferenceId = mdvoDependant.getPrimaryKey();
			} else {
				if (mdvoDependant.getPrimaryKey() == null) {
					// work on sDependantEntityUid
					iReferenceId = createSingleRow(mdvoDependant, sUserName, intid);
				} else {
					iReferenceId = mdvoDependant.getPrimaryKey();
					if (meta.isEditable() && mdvoDependant.isChanged()) {
						this.modifySingleRow(meta.getUID(), mdvoDependant, null, sUserName, customUsage);
					} else {
						LOG.debug("Dependant row {} has not changed. Will not be updated.",
						          mdvoDependant.getId());
					}
				}	
			}
			for (IDependentKey dependentKey : mdvoDependant.getDependents().getKeySet()) {
				for (EntityObjectVO<?> mdvo : mdvoDependant.getDependents().getData(dependentKey)) {
					if(mdvo.getFieldIds().get(dependentKey.getDependentRefFieldUID()) == null) //@see NUCLOS-1113
					if (iReferenceId instanceof UID) {
						mdvo.setFieldUid(dependentKey.getDependentRefFieldUID(), (UID) iReferenceId);
					} else {
						mdvo.setFieldId(dependentKey.getDependentRefFieldUID(), IdUtils.toLongId(iReferenceId));
					}
				}
			}
			createOrModifyDependants(mdvoDependant.getDependents(), sUserName, bValidate, iReferenceId, customUsage);
			return iReferenceId;
		} else {
			return null;
		}
	}

	/**
	 * gets master data records for a given entity and search condition (generic mechanism)
	 * 
	 * §postcondition result != null
	 * 
	 * @param entityUid name of the entity to get master data records for
	 * @param search search condition value object
	 * @return TruncatableCollection&lt;MasterDataVO&gt; collection of master data value objects
	 * 
	 */
	public <PK>TruncatableCollection<MasterDataVO<PK>> getGenericMasterData(UID entityUid, final CollectableSearchExpression search, final boolean bAll, Integer limit) {
		return getGenericMasterDataImpl(entityUid, search, bAll, limit);
	}
	
	public <PK>TruncatableCollection<MasterDataVO<PK>> getGenericMasterData(UID entityUid, final CollectableSearchExpression search, final boolean bAll) {
		return getGenericMasterData(entityUid, search, bAll, null);
	}

	private <PK>TruncatableCollection<MasterDataVO<PK>> getGenericMasterDataImpl(UID entityUid, final CollectableSearchExpression clctexpr, final boolean bAll, Integer limit) {
		IEntityObjectProcessor<PK> eoProcessor = nucletDalProvider.getEntityObjectProcessor(entityUid);
		appendMandator(clctexpr, metaProvider.getEntity(entityUid));
		final boolean bSort = CollectionUtils.isNonEmpty(clctexpr.getSortingOrder());
		Long lLimit = limit != null && !E.isNuclosEntity(entityUid) && limit > 0 ? (long) (limit + 1) : null;
		final List<EntityObjectVO<PK>> eoResult = eoProcessor.getBySearchExprResultParams(
				grantUtils.append(clctexpr, entityUid), new ResultParams(bAll ? lLimit : new Long(MAXROWS + 1), bSort));

		boolean truncated = false;
		int recordCount = eoResult.size();
		if (!bAll && recordCount >= MAXROWS) {
			eoResult.subList(MAXROWS.intValue(), recordCount).clear();
			truncated = true;
		}

		List<MasterDataVO<PK>> result = CollectionUtils.transform(eoResult, (EntityObjectVO<PK> eo) -> {
			MasterDataVO<PK> mdvo = DalSupportForMD.wrapEntityObjectVO(eo);
			mdvo.getEntityObject().reset();
			return mdvo;
		});
		result = new GzipList<>(result);

		final Collection<MasterDataVO<UID>> systemObjects = XMLEntities.getSystemObjects(entityUid, clctexpr.getSearchCondition());
		if (!systemObjects.isEmpty()) {
			recordCount += systemObjects.size();
			
			Collection<MasterDataVO<PK>> sys = RigidUtils.uncheckedCast(systemObjects);
			result.addAll(sys);
		}

		return new TruncatableCollectionDecorator<>(result, truncated, recordCount);
	}
	
	public <PK> MasterDataVO<PK> getLastMasterDataCVOByMasterDataVO(MasterDataVO<PK> mdvo) 
			throws CommonFinderException {
		Map<UID, Object> fields = mdvo.getFieldValues();
		List<CollectableSearchCondition> lstCsc = new ArrayList<>();
		for (UID field : fields.keySet()) {
			Object value = fields.get(field);
			if (value == null) continue;
			if (value instanceof String) {
				String s = (String) value;
				if (s.isEmpty()) continue;
			}
			CollectableSearchCondition csc =
					org.nuclos.common.SearchConditionUtils.newComparison(field, ComparisonOperator.EQUAL, value);
			lstCsc.add(csc);
		}
		CollectableSearchCondition[] cscs = new CollectableSearchCondition[lstCsc.size()];
		for (int i = 0; i < cscs.length; i++) {
			cscs[i] = lstCsc.get(i);
		}
		CollectableSearchExpression clctexpr = new CollectableSearchExpression(
				SearchConditionUtils.and(cscs));
		clctexpr.setIncludingSystemData(false);
		ResultParams resultParams = new ResultParams(null, 0L, 1000L, true);
		List<MasterDataVO<PK>> lstResult = getMasterDataChunk(mdvo.getEntityObject().getDalEntity(), clctexpr, resultParams);
		if (lstResult == null || lstResult.size() == 0) {
			throw new CommonFinderException();
		}
		MasterDataVO<PK> result = null;
		for (MasterDataVO<PK> m : lstResult) {
			if (result == null) {
				result = m;
			} else {
				int compareval = LangUtils.compare(m.getCreatedAt(), result.getCreatedAt());
				if (compareval > 0) {
					result = m;
				} else if (compareval == 0 && LangUtils.compare(m.getPrimaryKey(), result.getPrimaryKey()) > 0) {
					result = m;	
				}
			}
		}
		return result;
	}

	public <PK> List<MasterDataVO<PK>> getMasterDataChunk(UID entityUid, final CollectableSearchExpression clctexpr, ResultParams resultParams) {
		List<MasterDataVO<PK>> systemObjects = null;
		if (clctexpr.isIncludingSystemData()) {
			systemObjects = new ArrayList<>();
			List<MasterDataVO<UID>> allSystemObjects = XMLEntities.getSystemObjects(entityUid, clctexpr.getSearchCondition());
			Long istart = resultParams.getOffset() != null ? resultParams.getOffset() : 0L;
			Long iend = resultParams.getLimit() != null ? resultParams.getLimit() - istart + 1 : -1;
			for (int i = istart.intValue(); i <= iend; i++) {
				if (allSystemObjects.size() > i) {
					systemObjects.add((MasterDataVO<PK>) allSystemObjects.get(i));
				}
			}
			int eoOffset;
			if (!systemObjects.isEmpty()) {
				eoOffset = systemObjects.size();
			} else {
				eoOffset = allSystemObjects.size();
			}
		}
		
		List<MasterDataVO<PK>> result = null;
		if (resultParams.getLimit() == null || resultParams.getLimit() >= 0) {
			IEOChunkableProcessor<PK> eoProcessor = (IEOChunkableProcessor<PK>) nucletDalProvider.getEntityObjectProcessor(entityUid);
			List<EntityObjectVO<PK>> eoResult = eoProcessor.getChunkBySearchExpressionImpl(clctexpr, resultParams);
			result = CollectionUtils.transform(eoResult, (EntityObjectVO<PK> eo) -> DalSupportForMD.wrapEntityObjectVO(eo));
		} else {
			result = new ArrayList<>();
		}
		
		if (systemObjects != null) {
			result.addAll(0, systemObjects);
		}
		return result;	
	}
	
	public Long countMasterDataRows(UID entityUid, final CollectableSearchExpression clctexpr) {
		IEntityObjectProcessor eoProcessor = nucletDalProvider.getEntityObjectProcessor(entityUid);
		int countSystemObjects = 0;
		if (clctexpr != null && clctexpr.isIncludingSystemData()) {
			Collection<?> systemObjects = XMLEntities.getSystemObjectIds(entityUid, clctexpr.getSearchCondition());
			countSystemObjects = systemObjects.size();
		}
		return eoProcessor.count(clctexpr)+countSystemObjects;
	}

	public Long countMasterDataRowsWithLimit(UID entityUid, final CollectableSearchExpression clctexpr, Long limit) {
		IEntityObjectProcessor eoProcessor = nucletDalProvider.getEntityObjectProcessor(entityUid);
		int countSystemObjects = 0;
		if (clctexpr != null && clctexpr.isIncludingSystemData()) {
			Collection<?> systemObjects = XMLEntities.getSystemObjectIds(entityUid, clctexpr.getSearchCondition());
			countSystemObjects = systemObjects.size();
		}
		return eoProcessor.countWithLimit(clctexpr, limit)+countSystemObjects;
	}


	/**
	 * deletes the file with the given id
	 * @param iFileId
	 */
	public static void remove(Object iFileId, String sFilename, File dir) {
		if (dir.isDirectory()) {
			for (String sFileName : dir.list()) {
				if (sFileName.startsWith(iFileId + "." + (sFilename != null ? sFilename : ""))) {
					final File file = new File(dir.getAbsolutePath() + File.separator + sFileName);
					if (file.exists())
						file.delete();
				}
			}
		}
	}

	public void validateRoleDependants(IDependentDataMap mpDependants) throws CommonValidationException {

		for (IDependentKey dependentKey : mpDependants.getKeySet()) {
			FieldMeta<?> refFieldMeta = metaProvider.getEntityField(dependentKey.getDependentRefFieldUID());
			RoleDependant dependant = RoleDependant.getByEntityName(refFieldMeta.getEntity());
			if (dependant != null) {
				List<UID> names = CollectionUtils.transform(
						mpDependants.getData(dependentKey),
						(EntityObjectVO<?> eovo) -> {
							UID uid = eovo.getFieldUid(dependant.getEntityFieldName().getUID());
							return uid;
						}
				);

				for (Object name : names) {
					if (name != null && Collections.frequency(names, name) > 1) {
						throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage(
							"role.error.validation.dependant", name, dependant.getEntity().toString()));
					}
				}
			}
		}
	}

	/**
	 * 
	 * @param oldSource old E.DBSOURCE object
	 * @param newSource new E.DBSOURCE object
	 * @param createModifyDelete
	 * @throws NuclosBusinessException
	 */
	private void updateDbObject(EntityObjectVO<UID> oldSource, EntityObjectVO<UID> newSource, int createModifyDelete) throws NuclosBusinessException {
		if (oldSource == null && newSource == null) {
			throw new NuclosFatalException("oldSource and newSource must not be null.");
		} else if (oldSource != null && newSource != null 
				&& !oldSource.getFieldUid(E.DBSOURCE.dbobject).equals(newSource.getFieldUid(E.DBSOURCE.dbobject))) {
			throw new NuclosFatalException("oldSource and newSource not from same object.");
		} else if (oldSource != null && newSource != null 
				&& !oldSource.getFieldValue(E.DBSOURCE.dbtype).equals(newSource.getFieldValue(E.DBSOURCE.dbtype))) {
			throw new NuclosFatalException("Dbtype of oldSource and dbtype of newSource have to be equal.");
		}

		final String dbtype = (oldSource != null) 
				? oldSource.getFieldValue(E.DBSOURCE.dbtype) 
				: newSource.getFieldValue(E.DBSOURCE.dbtype);

		final DbAccess dbAccess = dataBaseHelper.getDbAccess();

		if (!dbAccess.getDbType().equals(DbType.getFromName(dbtype))) {
			switch (createModifyDelete) {
				case -1:
					nucletDalProvider.getEntityObjectProcessor(E.DBSOURCE).delete(new Delete(oldSource.getPrimaryKey()));
					break;
				case 0:
				case 1:
					nucletDalProvider.getEntityObjectProcessor(E.DBSOURCE).insertOrUpdate(newSource);
					StringBuffer warnings = new StringBuffer("Update of DB Object from type '" + dbtype + "' has no effect to database. Current connected database type is '" + dbAccess.getDbType().name() + "'.");
					messageService.sendMessage(new DbObjectMessage("Mit Fehlern", "Datenbank Aktualisierung", true, Collections.EMPTY_LIST, warnings));
			}

			return;
		}

		// NUCLOS-7316
		if (createModifyDelete >= 0) {
			final NucletIntegrationPointFacade ipFacade = SpringApplicationContextHolder.getBean(NucletIntegrationPointFacade.class);
			final String dbSource = newSource.getFieldValue(E.DBSOURCE.source).toUpperCase();
			for (String ipView : ipFacade.getAllViewNames()) {
				if (dbSource.indexOf(ipView.toUpperCase()) != -1) {
					throw new NuclosBusinessException("masterdata.error.dbobject.do.not.use.integration.view");
				}
			}
		}

		final DbObjectHelper dboHelper = new DbObjectHelper(dbAccess);

		boolean isUsedAsCalculatedAttribute = false;

		final UID objectUid = oldSource != null
				? oldSource.getFieldUid(E.DBSOURCE.dbobject) 
				: newSource.getFieldUid(E.DBSOURCE.dbobject);
		final EntityObjectVO<UID> dbObject = nucletDalProvider.getEntityObjectProcessor(E.DBOBJECT).getByPrimaryKey(objectUid);
		if (dbObject == null)
			throw new NuclosFatalException("Database object with uid \"" + objectUid + "\" does not exist");

		final String dbObjectName = dbObject.getFieldValue(E.DBOBJECT.name);
		DbObjectType type = DbObjectType.getByName(dbObject.getFieldValue(E.DBOBJECT.dbobjecttype));
		switch (type) {
		case FUNCTION:
			/**
			 * look if function is used as calculated attribute
			 */
			isUsedAsCalculatedAttribute = DbObjectHelper.isUsedAsCalculatedAttribute(dbObjectName, metaProvider);
			break;
		case VIEW:
			/**
			 * look if view is replacing an entity object view
			 */
			dboHelper.getEntityMetaForView(dbObjectName, metaProvider);
		}

		/**
		 * check before any DML is executed,
		 * otherwise oracle commits the transaction and it doesn't matter if throw an exception ot not.
		 */
		if (newSource == null || !newSource.getFieldValue(E.DBSOURCE.active)) {
			if (isUsedAsCalculatedAttribute) {
				/**
				 * if in use no deactivation/delete allowed
				 */
				throw new NuclosBusinessException("masterdata.error.dbobject.isinuse.calcattr");
			}
		}

		List<String> script = new ArrayList<>();
		StringBuffer warnings = new StringBuffer();
		Map<DbObject, Pair<DbPlainStatement, DbStatement>> oldDbObjects = dboHelper.getAllDbObjects(null);
		switch (createModifyDelete) {
			case -1:
				nucletDalProvider.getEntityObjectProcessor(E.DBSOURCE).delete(new Delete<>(oldSource.getPrimaryKey()));
				break;
			case 0:
			case 1:
				nucletDalProvider.getEntityObjectProcessor(E.DBSOURCE).insertOrUpdate(newSource);
				break;
		}
//		schemaHelper = new MetaDbHelper(E.getSchemaHelperVersion(), dbAccess, MetaProvider.getInstance());
//		schema = schemaHelper.getSchema();
		//** create dbobjects
		//Map<DbObject, Pair<DbPlainStatement, DbStatement>> newDbObjects = dboHelper.getAllDbObjects(null);
		final String name = dbObject.getFieldValue(E.DBOBJECT.name);
		Map<DbObject, Pair<DbPlainStatement, DbStatement>> dependantObjects = new HashMap<>();

		for (Map.Entry<DbObject, Pair<DbPlainStatement, DbStatement>> compDbObject : dboHelper.getAllDbObjects(null).entrySet()) {
			if (compDbObject.getValue().getX().getSql().contains(name)) {
				dependantObjects.put(compDbObject.getKey(), compDbObject.getValue());
			}
		}

		NucletIntegrationPointFacade integrationPointFacade = SpringApplicationContextHolder.getBean(NucletIntegrationPointFacade.class);
		Query<NucletIntegrationPoint> qIp = QueryProvider.create(NucletIntegrationPoint.class);
		Collection<NucletIntegrationPoint> ips = QueryProvider.execute(qIp);

		ips.forEach(point -> integrationPointFacade.deleteIpView(point));

		DbObjectHelper.updateDbObjects(dbAccess, dependantObjects, oldDbObjects,
				DbStructureChange.Type.DROP, true, script, new StringBuffer()); // just log warnings to dev:null
		
		DbObjectHelper.updateDbObjects(dbAccess, dependantObjects, oldDbObjects,
				DbStructureChange.Type.CREATE, true, script, warnings);
		ips.forEach(point -> integrationPointFacade.createIpView(point));
		
		//** create entity views
//		DbObjectHelper.updateViews(dbAccess, schema.values(), MetaProvider.getInstance(),
//				DbStructureChange.Type.CREATE, true, script,  warnings);
		
		messageService.sendMessage(new DbObjectMessage(
				warnings.length() == 0 ? "Successful" : "With errors", 
				"Database Update", true, script, warnings));
	}
	
	public <PK> IDependentDataMap readAllDependents(PK iId, IDependentDataMap mpDependants, Boolean bRemoved,
			UID sParentEntity,
			Map<EntityAndField, UID> mpEntityAndParentEntityName) {
			Collection<EntityObjectVO<PK>> collmdvo = Collections.emptyList();

			if (mpDependants == null) {
				mpDependants = new DependentDataMap();
			}
			
			// last subform in hierarchie found
			if(mpEntityAndParentEntityName.containsValue(sParentEntity)) {
				for(EntityAndField eafn : mpEntityAndParentEntityName.keySet()) {
					// first subform in hierarchie found or
					// child subfrom found
					final UID entity = eafn.getEntity();
					EntityMeta<PK> eMeta = metaProvider.getEntity(entity);
					if (!eMeta.isEditable()) {
						continue;
					}
					if ((mpEntityAndParentEntityName.get(eafn) == null && sParentEntity == null)
						|| (mpEntityAndParentEntityName.get(eafn) != null 
						&& mpEntityAndParentEntityName.get(eafn).equals(sParentEntity))) {
						if(!mpDependants.getData(eafn.getDependentKey()).isEmpty()) {
							collmdvo = CollectionUtils.emptyIfNull(mpDependants.<PK>getDataPk(eafn.getDependentKey()));
						} else {
							if (iId != null) {
								String username = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
								Collection<EntityObjectVO<PK>> col = getDependantMasterData(
										eafn.getField(),
										username,
										null, iId);

								collmdvo = CollectionUtils.emptyIfNull(col);
								// mpDependants.addAllData(entity, collmdvo);
								for (EntityObjectVO<?> dep: collmdvo) {
									mpDependants.addData(eafn.getDependentKey(), dep);
								}
							}
						}

						for (EntityObjectVO<?> dmdvo : collmdvo) {
							if (bRemoved) {
								dmdvo.flagRemove();
							}
							dmdvo.setDependents(readAllDependents(
								dmdvo.getPrimaryKey(), dmdvo.getDependents(),
								dmdvo.isFlagRemoved(), eafn.getEntity(),
								mpEntityAndParentEntityName));
						}
					}
				}
			}
			return mpDependants;
	}

	<PK, FK> List<EntityObjectVO<PK>> readDependenciesForMultiRecords(Collection<FK> lstIds, FieldMeta<FK> fmRefField) {
		if (lstIds.isEmpty()) {
			return Collections.emptyList();
		}

		final String pkColumn = fmRefField.getJavaClass() == UID.class ? "STRUID_":"INTID_";

		IEntityObjectProcessor<?> eoProcessor = nucletDalProvider.getEntityObjectProcessor(fmRefField.getEntity());
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();

		final String foreignkeyColumn = fmRefField.getDbColumn().toUpperCase().replaceFirst(
				"^(STRVALUE_|INTVALUE_|OBJVALUE_)", pkColumn);

		PreparedStringBuilder ps = new PreparedStringBuilder(foreignkeyColumn);
		DbExpression<FK> expression = new DbExpression<>(builder, fmRefField.getJavaClass(), ps);

		DbCondition cond = builder.in(SystemFields.BASE_ALIAS, expression, lstIds);
		CollectableDbCondition cdbc = new CollectableDbCondition(cond);
		CollectableSearchExpression clctexpr = new CollectableSearchExpression(cdbc);

		return RigidUtils.uncheckedCast(eoProcessor.getBySearchExpression(clctexpr));
	}


	/**
     * Reads sub-form data for multiple foreign-keys at once. Much faster and less resource consuming than iterating through
     * each data row. Specifically interesting for loading sub-sub-form data.
     * 
     * @param lstIds - List of foreign keys
     * @param subform - sub-form BO
     * @return A map of the foreign keys holding a collection of the complete sub-form data.
     */

    <PK, FK> Map<FK, Collection<EntityObjectVO<PK>>> readMultiSubFormData(Collection<FK> lstIds, IDependentKey subform) {
		if (lstIds.isEmpty()) {
			return Collections.emptyMap();
		}

		final FieldMeta<FK> fmRefField = RigidUtils.uncheckedCast(metaProvider.getEntityField(subform.getDependentRefFieldUID()));
		final EntityMeta<Object> entityMetaRef = metaProvider.getEntity(RigidUtils.defaultIfNull(fmRefField.getUnreferencedForeignEntity(), fmRefField.getForeignEntity()));
		List<EntityObjectVO<PK>> lstEOs = readDependenciesForMultiRecords(lstIds, fmRefField);

		Map<FK, Collection<EntityObjectVO<PK>>> mpRet = new HashMap<>();
		for (EntityObjectVO<PK> eo : lstEOs) {
			FK fk;
			if (entityMetaRef.isUidEntity()) {
				fk = (FK)eo.getFieldUid(fmRefField.getUID());
			} else {
				fk = (FK)eo.getFieldId(fmRefField.getUID());
			}
			mpRet.putIfAbsent(fk, new ArrayList<>());
			mpRet.get(fk).add(eo);
		}
    	return mpRet;

    }
    
}	// class MasterDataFacadeHelper
