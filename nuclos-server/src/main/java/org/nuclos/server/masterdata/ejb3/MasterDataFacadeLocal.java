//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.masterdata.ejb3;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.EntityTreeViewVO;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;

// @Local
public interface MasterDataFacadeLocal extends CommonMasterDataFacade {

	/**
	 * gets the ids of all masterdata objects that match a given search expression (ordered, when necessary)
	 *
	 * @param cse condition that the masterdata objects to be found must satisfy
	 * @return List&lt;Integer&gt; list of masterdata ids
	 */
	@RolesAllowed("Login")
	<PK> List<PK> getMasterDataIdsNoCheck(UID entity, CollectableSearchExpression cse);

	/**
	 * gets the ids of all masterdata objects
	 *
	 * @return List&lt;PK&gt; list of masterdata ids
	 */
	@RolesAllowed("Login")
	<PK> List<PK> getMasterDataIds(UID entity);

	/**
	 * gets the dependant master data records for the given entity, using the given foreign key field and the given id as foreign key.
	 * <p>
	 * §precondition oRelatedId != null
	 * §todo restrict permissions by entity name
	 *
	 * @param sEntityName      name of the entity to get all dependant master data records for
	 * @param sForeignKeyField name of the field relating to the foreign entity
	 * @param oRelatedId       id by which sEntityName and sParentEntity are related
	 */
	@RolesAllowed("Login")
	<PK, F> Collection<EntityObjectVO<PK>> getDependantMasterData(UID sForeignKeyField, F oRelatedId);

	<PK, F> Collection<EntityObjectVO<PK>> getDependantMd4FieldMeta(FieldMeta<?> foreignKeyField, F oRelatedId);

	@RolesAllowed("Login")
	<PK> Collection<EntityTreeViewVO> getDependentSubnodes(UID sForeignKeyField, Object oRelatedId);

	@RolesAllowed("Login")
	<PK> MasterDataVO<PK> get(EntityMeta<PK> entity, PK pk)
			throws CommonFinderException, CommonPermissionException;

	@RolesAllowed("Login")
	<PK> MasterDataVO<PK> get(UID entity, PK pk)
			throws CommonFinderException, CommonPermissionException;

	/**
	 * @return the version of the given masterdata id.
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	@RolesAllowed("Login")
	Integer getVersion(UID entity, Object oId)
			throws CommonFinderException, CommonPermissionException;

	/**
	 * create a new master data record
	 * <p>
	 * §precondition sEntityName != null
	 * §precondition mdvo.getId() == null
	 * §precondition (mpDependants != null) --&gt; mpDependants.areAllDependantsNew()
	 * §nucleus.permission checkWriteAllowed(sEntityName)
	 *
	 * @param mdvo the master data record to be created
	 * @return master data value object containing the newly created record
	 */
	@RolesAllowed("Login")
	<PK> MasterDataVO<PK> create(MasterDataVO<PK> mdvo, String customUsage) throws CommonCreateException,
			CommonPermissionException, NuclosBusinessRuleException;

	/**
	 * modifies an existing master data record.
	 * <p>
	 * §precondition sEntityName != null
	 * §nucleus.permission checkWriteAllowed(sEntityName)
	 *
	 * @param mdvo the master data record
	 * @return id of the modified master data record
	 */
	@RolesAllowed("Login")
	@Deprecated
	<PK> PK modify(MasterDataVO<PK> mdvo) throws CommonCreateException,
			CommonFinderException, CommonRemoveException,
			CommonStaleVersionException, CommonValidationException,
			CommonPermissionException, NuclosBusinessRuleException;

	/**
	 * modifies an existing master data record.
	 * <p>
	 * §precondition sEntityName != null
	 * §nucleus.permission checkWriteAllowed(sEntityName)
	 *
	 * @param mdvo the master data record
	 * @return id of the modified master data record
	 */
	@RolesAllowed("Login")
	@Deprecated
	<PK> PK modify(MasterDataVO<PK> mdvo, boolean isCollectiveProcessing) throws CommonCreateException,
			CommonFinderException, CommonRemoveException,
			CommonStaleVersionException, CommonValidationException,
			CommonPermissionException, NuclosBusinessRuleException;

	/**
	 * modifies an existing master data record.
	 * <p>
	 * §precondition sEntityName != null
	 * §nucleus.permission checkWriteAllowed(sEntityName)
	 *
	 * @param mdvo the master data record
	 * @return id of the modified master data record
	 */
	@RolesAllowed("Login")
	<PK> PK modify(MasterDataVO<PK> mdvo, String customUsage, boolean isCollectiveProcessing) throws CommonCreateException,
			CommonFinderException, CommonRemoveException,
			CommonStaleVersionException, CommonValidationException,
			CommonPermissionException, NuclosBusinessRuleException;

	/**
	 * modifies an existing master data record.
	 * <p>
	 * §precondition sEntityName != null
	 * §nucleus.permission checkWriteAllowed(sEntityName)
	 *
	 * @param mdvo the master data record
	 * @return id of the modified master data record
	 */
	@RolesAllowed("Login")
	<PK> PK modify(MasterDataVO<PK> mdvo, String customUsage) throws CommonCreateException,
			CommonFinderException, CommonRemoveException,
			CommonStaleVersionException, CommonValidationException,
			CommonPermissionException, NuclosBusinessRuleException;

	/**
	 * method to delete an existing master data record
	 * <p>
	 * §precondition sEntityName != null
	 * §nucleus.permission checkDeleteAllowed(sEntityName)
	 *
	 * @param pk              primary key of the master data record
	 * @param bRemoveDependants remove all dependants if true, else remove only given (single) mdvo record
	 *                          this is helpful for entities which have no layout
	 */
	@RolesAllowed("Login")
	<PK> void remove(UID entity, PK pk, boolean bRemoveDependants) throws CommonFinderException,
			CommonRemoveException, CommonStaleVersionException,
			CommonPermissionException, NuclosBusinessRuleException;

	/**
	 * method to delete an existing master data record
	 * <p>
	 * §precondition sEntityName != null
	 * §nucleus.permission checkDeleteAllowed(sEntityName)
	 *
	 * @param pk              primary key of the master data record
	 * @param bRemoveDependants remove all dependants if true, else remove only given (single) mdvo record
	 *                          this is helpful for entities which have no layout
	 */
	@RolesAllowed("Login")
	<PK> void remove(UID entity, PK pk,
					 boolean bRemoveDependants, String customUsage) throws CommonFinderException,
			CommonRemoveException, CommonStaleVersionException,
			CommonPermissionException, NuclosBusinessRuleException;

	/**
	 * creates the given dependants (local use only).
	 * <p>
	 * §precondition mpDependants != null
	 */
	void createDependants(Object id, IDependentDataMap dependants, String customUsage)
			throws CommonCreateException, CommonPermissionException;

	/**
	 * modifies the given dependants (local use only).
	 * <p>
	 * §precondition mpDependants != null
	 */
	void modifyDependants(Object id, IDependentDataMap dependants, String customUsage)
			throws CommonCreateException, CommonFinderException, CommonPermissionException,
			CommonRemoveException, CommonStaleVersionException;

	/**
	 * notifies clients that the contents of an entity has changed.
	 * <p>
	 * §precondition sCachedEntityName != null
	 *
	 * @param entity UID of the cached entity.
	 */
	void notifyClients(UID entity);

	<PK> MasterDataVO<PK> getWithDependants(
			UID entity, PK iId, String customUsage) throws CommonFinderException,
			NuclosBusinessException, CommonPermissionException;

	/**
	 * @param cond search condition
	 * @return the masterdata objects for the given entityname and search condition.
	 */
	<PK> Collection<MasterDataVO<PK>> getWithDependantsByCondition(
			UID entity, CollectableSearchCondition cond, String customUsage);

	Set<UID> getRolesHierarchyForUser(String user);

	@RolesAllowed("Login")
	<PK> Collection<MasterDataVO<PK>> getMasterData(
			EntityMeta<PK> entity, CollectableSearchCondition cond);

	void evictAllProcessesFromCache();

	<PK> void fillDependentsForSubformColumns(
			List<EntityObjectVO<PK>> eos,
			Collection<UID> fields,
			UID entity,
			String customUsage
	);

	<PK, FK> List<EntityObjectVO<PK>> readDependenciesForMultiRecords(Collection<FK> lstIds, UID reffield);

}
