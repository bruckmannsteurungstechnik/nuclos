package org.nuclos.server.masterdata.ejb3;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.apache.commons.collections15.BidiMap;
import org.apache.commons.collections15.bidimap.DualHashBidiMap;
import org.apache.commons.collections15.bidimap.UnmodifiableBidiMap;
import org.apache.commons.lang.NotImplementedException;
import org.nuclos.cache.IFqnCache;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosEntityValidator;
import org.nuclos.common.UID;
import org.nuclos.server.attribute.ejb3.LayoutObjectBuilder;
import org.nuclos.server.common.INucletCache;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.genericobject.ejb3.GeneratorObjectBuilder;
import org.nuclos.server.job.JobObjectBuilder;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.nbo.NuclosBusinessObjectBuilder;
import org.nuclos.server.printservice.printout.PrintoutObjectBuilder;
import org.nuclos.server.printservice.printout.PrintoutObjectBuilder.OutputFormatVisitor;
import org.nuclos.server.report.ChartDatasourceObjectBuilder;
import org.nuclos.server.report.ReportDatasourceObjectBuilder;
import org.nuclos.server.statemodel.StatemodelObjectBuilder;
import org.nuclos.server.user.UserRoleObjectBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

@Component
public class MasterDataRestFqnCache implements INucletCache, IFqnCache {
	
	private static final Set<UID> FQN_ENABLED = new HashSet<UID>();
	static {
		FQN_ENABLED.add(E.NUCLET.getUID());
		FQN_ENABLED.add(E.ENTITY.getUID());
		FQN_ENABLED.add(E.ENTITYFIELD.getUID());
		FQN_ENABLED.add(E.PROCESS.getUID());
		FQN_ENABLED.add(E.STATEMODEL.getUID());
		FQN_ENABLED.add(E.STATE.getUID());
		FQN_ENABLED.add(E.LAYOUT.getUID());
		FQN_ENABLED.add(E.GENERATION.getUID());
		FQN_ENABLED.add(E.REPORT.getUID());
		FQN_ENABLED.add(E.REPORTOUTPUT.getUID());
		FQN_ENABLED.add(E.DATASOURCE.getUID());
		FQN_ENABLED.add(E.CHART.getUID());
		FQN_ENABLED.add(E.JOBCONTROLLER.getUID());
		FQN_ENABLED.add(E.ROLE.getUID());
	}
	
	@Autowired
	private MasterDataFacadeLocal mdfacade;
	
	@Autowired
	private MetaProvider metaprovider;
	
	@Override
	public String getFullQualifiedNucletName(UID nucletUID) {
		String result = getRestFqnUidMap(E.NUCLET).get(nucletUID);
		if (result != null) {
			result = validateFqn(result);
		}
		return result;
	}
	
	@Override
	public UID translateFqn(EntityMeta<?> eMeta, String fqn) {
		if (fqn == null) {
			throw new IllegalArgumentException("fqn must not be null");
		}
		BidiMap<UID, String> bidiMap = eMeta==null?null:getRestFqnUidMap(eMeta);
		final UID result;
		if (bidiMap == null) {
			// not enabled or eMeta is null
			result = UID.parseUID(fqn);
		} else {
			result = bidiMap.getKey(fqn);
		}
		if (result == null) {
			// special case like calc attribute...
			return UID.parseUID(fqn);
		}
		return result;
	}

	/**
	 * Tries to guess the EntityMeta and translate the given UID.
	 *
	 * TODO: If the cache was not splitted by entity, we could use it directly.
	 *
	 * @param uid
	 * @return
	 */
	@Override
	public String translateUid(
			@NotNull final UID uid
	) {
		for (UID enabledEntityUID: FQN_ENABLED) {
			EntityMeta<Object> entityMeta = E.getByUID(enabledEntityUID);
			String result = translateUid(entityMeta, uid, null);
			if (result != null) {
				return result;
			}
		}
		return uid.toString();
	}

	/**
	 * Tries to translate the given UID for the given EntityMeta.
	 * Returns the stringified UID if the translation was not successful.
	 *
	 * @param eMeta
	 * @param uid
	 * @return
	 */
	@Override
	public String translateUid(
			final EntityMeta<?> eMeta,
			final UID uid
	) {
		if (uid == null) {
			throw new IllegalArgumentException("uid must not be null");
		}

		final String defaultValue = uid.toString();
		return translateUid(eMeta, uid, defaultValue);
	}

	/**
	 * Tries to translate the given UID for the given EntityMeta.
	 * Returns the given default value if the translation was not successful.
	 *
	 * @param eMeta
	 * @param uid
	 * @param defaultValue
	 * @return
	 */
	private String translateUid(
			final EntityMeta<?> eMeta,
			final UID uid,
			final String defaultValue
	) {
		String result = null;
		BidiMap<UID, String> bidiMap = eMeta == null ? null : getRestFqnUidMap(eMeta);
		if (bidiMap != null) {
			result = bidiMap.get(uid);
		}

		if (result == null) {
			// special case like calc attribute...
			result = defaultValue;
		}

		return result;
	}

	@CacheEvict(value="masterDataRestFqnCache", allEntries=true)
	private void evictCompleteCache() {
	}
	
	@CacheEvict(value="masterDataRestFqnCache", key="#p0")
	private void evictCacheForEntity(UID entityUID) {
	}
	
	@Override
	public void invalidateCacheForEntity(UID entityUID) {
		if (!FQN_ENABLED.contains(entityUID)) {
			return;
		}
		if (E.NUCLET.checkEntityUID(entityUID)) {
			evictCompleteCache();
			return;
		}
		if (E.ENTITY.checkEntityUID(entityUID)) {
			invalidateCacheForEntity(E.ENTITYFIELD.getUID());
			invalidateCacheForEntity(E.PROCESS.getUID());
		} else if (E.STATEMODEL.checkEntityUID(entityUID)) {
			invalidateCacheForEntity(E.STATE.getUID());
		} else if (E.REPORT.checkEntityUID(entityUID)) {
			invalidateCacheForEntity(E.REPORTOUTPUT.getUID());
		} else if (E.FORM.checkEntityUID(entityUID)) {
			invalidateCacheForEntity(E.REPORT.getUID());
		}  
		evictCacheForEntity(entityUID);
	}

	/**
	 * TODO: Do a proper generic lookup here instead of cumbersome if-else switches over all entities!
	 * <p>
	 * TODO: Why do we have separate Maps per entity?
	 * The point of UIDs is to have globally unique IDs, so 1 Map should be enough.
	 * The only use of separate maps seems to be cache invalidation for specific entities.
	 *
	 * @param eMeta
	 * @return
	 */
	@Cacheable(value="masterDataRestFqnCache", key="#p0.getUID()")
	private BidiMap<UID, String> getRestFqnUidMap(EntityMeta<?> eMeta) {
		if (!FQN_ENABLED.contains(eMeta.getUID())) {
			return null;
		}
		if (!eMeta.isUidEntity()) {
			throw new IllegalArgumentException("Entity " + eMeta.getEntityName() + " has to be a UID entity");
		}
		final BidiMap<UID, String> result = new DualHashBidiMap<UID, String>();
		
		// use special case for entity and entityfield. provider contains more entities, like dynamic.
		if (E.ENTITY.equals(eMeta)) {
			for (EntityMeta<?> entity : metaprovider.getAllEntities()) {
				String fqn = NuclosBusinessObjectBuilder.getNucletPackageStatic(entity.getUID(), 
						entity.getNuclet(), this, false) +
						"_" + NuclosBusinessObjectBuilder.getNameForFqn(NuclosEntityValidator.getFormattedEntityNameForSystemEntity(entity.getBusinessObjectClassName()));
				fqn = validateFqn(fqn);
				result.put(entity.getUID(), fqn);
			}
		} else if (E.ENTITYFIELD.equals(eMeta)) {
			for (EntityMeta<?> entity : metaprovider.getAllEntities()) {
				for (FieldMeta<?> field : metaprovider.getAllEntityFieldsByEntity(entity.getUID()).values()) {
					BidiMap<UID, String> entityMap = getRestFqnUidMap(E.ENTITY);
					String fqnEntity = entityMap.get(entity.getUID()); 
					String fqn = fqnEntity + "_" + NuclosBusinessObjectBuilder.getFieldNameForFqn(field);
					fqn = validateFqn(fqn);
					result.put(field.getUID(), fqn);
				}
			}
		} 
		
		// special case for report output formats.
		else if (E.REPORTOUTPUT.equals(eMeta)) {
			BidiMap<UID, String> reportMap = getRestFqnUidMap(E.REPORT);
			for (final UID reportUID : reportMap.keySet()) {
				final String fqnReport = reportMap.get(reportUID); 
				PrintoutObjectBuilder.visitOutputFormats(reportUID, new OutputFormatVisitor() {
					@Override
					public void visitOutputFormat(UID outputFormatUID, String fullyQualifiedName) {
						String fqn = fqnReport + "_" + fullyQualifiedName;
						fqn = validateFqn(fqn);
						result.put(outputFormatUID, fqn);
					}
				});
			}
		} 
		
		// default masterdata way for all other entities, including the XML Entities (very important)
		else {
			for (MasterDataVO<?> mdvo : mdfacade.getMasterData(eMeta, null)) {
				final UID pk = (UID) mdvo.getPrimaryKey();
				final String fqn;
				if (E.NUCLET.equals(eMeta)) {
					fqn = mdvo.getFieldValue(E.NUCLET.packagefield);
				} else if (E.PROCESS.equals(eMeta)) {
					BidiMap<UID, String> entityMap = getRestFqnUidMap(E.ENTITY);
					String fqnEntity = entityMap.get(mdvo.getFieldUid(E.PROCESS.module)); 
					fqn = fqnEntity + "_" + NuclosBusinessObjectBuilder.getProcessNameForFqn(mdvo.getFieldValue(E.PROCESS.name));
				} else if (E.STATEMODEL.equals(eMeta)) {
					fqn = StatemodelObjectBuilder.getNucletPackageStatic(mdvo.getFieldUid(E.STATEMODEL.nuclet), this) +
							"_" + StatemodelObjectBuilder.getNameForFqn(mdvo.getFieldValue(E.STATEMODEL.name));
				} else if (E.STATE.equals(eMeta)) {
					BidiMap<UID, String> statemodelMap = getRestFqnUidMap(E.STATEMODEL);
					String fqnStatemodel = statemodelMap.get(mdvo.getFieldUid(E.STATE.model)); 
					fqn = fqnStatemodel + "_" + StatemodelObjectBuilder.getStateNameForFqn(mdvo.getFieldValue(E.STATE.numeral));
				} else if (E.LAYOUT.equals(eMeta)) {
					fqn = LayoutObjectBuilder.getNucletPackageStatic(mdvo.getFieldUid(E.LAYOUT.nuclet), this) +
							"_" + LayoutObjectBuilder.getNameForFqn(mdvo.getFieldValue(E.LAYOUT.name));
				} else if (E.GENERATION.equals(eMeta)) {
					fqn = GeneratorObjectBuilder.getNucletPackageStatic(mdvo.getFieldUid(E.GENERATION.nuclet), this) +
							"_" + GeneratorObjectBuilder.getNameForFqn(mdvo.getFieldValue(E.GENERATION.name));	
				} else if (E.REPORT.equals(eMeta)) {
					fqn = PrintoutObjectBuilder.getNucletPackageStatic(mdvo.getFieldUid(E.REPORT.nuclet), this) +
							"_" + PrintoutObjectBuilder.getNameForFqn(mdvo.getFieldValue(E.REPORT.name));
				} else if (E.DATASOURCE.equals(eMeta)) {
					fqn = ReportDatasourceObjectBuilder.getNucletPackageStatic(mdvo.getFieldUid(E.DATASOURCE.nuclet), this) +
							"_" + ReportDatasourceObjectBuilder.getNameForFqn(mdvo.getFieldValue(E.DATASOURCE.name));
				} else if (E.CHART.equals(eMeta)) {
					fqn = ChartDatasourceObjectBuilder.getNucletPackageStatic(mdvo.getFieldUid(E.CHART.nuclet), this) +
							"_" + ChartDatasourceObjectBuilder.getNameForFqn(mdvo.getFieldValue(E.CHART.name));
				} else if (E.JOBCONTROLLER.equals(eMeta)) {
					fqn = JobObjectBuilder.getNucletPackageStatic(mdvo.getFieldUid(E.JOBCONTROLLER.nuclet), this) +
							"_" + JobObjectBuilder.getNameForFqn(mdvo.getFieldValue(E.JOBCONTROLLER.name));
				} else if (E.ROLE.equals(eMeta)) {
					fqn = UserRoleObjectBuilder.getNucletPackageStatic(mdvo.getFieldUid(E.ROLE.nuclet), this) + 
							"_" + UserRoleObjectBuilder.getNameForFqn(mdvo.getFieldValue(E.ROLE.name));
				} else {
					throw new NotImplementedException("FQN to UID transformation for entity " + eMeta.getEntityName() + " not implemented yet");
				}
				String validatedFqn = validateFqn(fqn);
				result.put(pk, validatedFqn);
			}
		}
		return UnmodifiableBidiMap.decorate(result);
	}

	/**
	 * TODO: A simple string replacement is no validation!
	 *
	 * @param sName
	 * @return
	 */
	private static String validateFqn(String sName) {
		return sName.replace('.', '_');
	}
}
