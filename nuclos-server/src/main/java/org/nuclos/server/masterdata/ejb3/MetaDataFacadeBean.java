//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.masterdata.ejb3;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.sql.DataSource;

import org.nuclos.api.businessobject.Query;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.businessentity.EntityField;
import org.nuclos.businessentity.NucletIntegrationField;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.EntityMetaVO;
import org.nuclos.common.EntityTreeViewVO;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.FieldMetaVO;
import org.nuclos.common.LafParameterMap;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.NucletFieldMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SimpleDbField;
import org.nuclos.common.StaticMetaDataProvider;
import org.nuclos.common.TranslationVO;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIdCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.dal.DalCallResult;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.metadata.TreeMetaProvider;
import org.nuclos.common.transport.vo.EntityMetaTransport;
import org.nuclos.common.transport.vo.FieldMetaTransport;
import org.nuclos.common.valueobject.EntityRelationshipModelVO;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.common.DatasourceServerUtils;
import org.nuclos.server.common.EventSupportCache;
import org.nuclos.server.common.LocaleUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.ServerServiceLocator;
import org.nuclos.server.common.SessionUtils;
import org.nuclos.server.common.ejb3.EntityObjectFacadeLocal;
import org.nuclos.server.common.ejb3.LocaleFacadeLocal;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.customcode.codegenerator.NuclosJarGeneratorManager;
import org.nuclos.server.customcode.codegenerator.recompile.checker.bo.BORecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.txsync.CompileGeneratedCodeTransactionSynchronization;
import org.nuclos.server.dal.DalUtils;
import org.nuclos.server.dal.processor.ProcessorFactorySingleton;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbAccess;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.DbObjectHelper.DbObjectType;
import org.nuclos.server.dblayer.DbStatementUtils;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.DbUtils;
import org.nuclos.server.dblayer.IBatch;
import org.nuclos.server.dblayer.MetaDbHelper;
import org.nuclos.server.dblayer.MetaDbProvider;
import org.nuclos.server.dblayer.expression.DbIncrement;
import org.nuclos.server.dblayer.expression.DbNull;
import org.nuclos.server.dblayer.impl.SchemaUtils;
import org.nuclos.server.dblayer.query.DbColumnExpression;
import org.nuclos.server.dblayer.query.DbCondition;
import org.nuclos.server.dblayer.query.DbDelete;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.query.DbSelection;
import org.nuclos.server.dblayer.statements.DbDeleteStatement;
import org.nuclos.server.dblayer.statements.DbInsertStatement;
import org.nuclos.server.dblayer.statements.DbMap;
import org.nuclos.server.dblayer.statements.DbStructureChange;
import org.nuclos.server.dblayer.statements.DbUpdateStatement;
import org.nuclos.server.dblayer.structure.DbColumn;
import org.nuclos.server.dblayer.structure.DbColumnType;
import org.nuclos.server.dblayer.structure.DbTable;
import org.nuclos.server.dblayer.structure.DbTableType;
import org.nuclos.server.genericobject.ejb3.GenericObjectFacadeLocal;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.i18n.language.data.DataLanguageFacadeBean;
import org.nuclos.server.i18n.language.data.DataLanguageServerUtils;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.nuclos.server.validation.DefaultSchemaValidation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.transaction.support.TransactionSynchronizationManager;

/**
* Facade bean for all meta data management functions (server side).
* <p>
* Uses the MetaProvider as implementation.
* </p>
* <br>
* <br>Created by Novabit Informationssysteme GmbH
* <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
*/
@Transactional(noRollbackFor= {Exception.class})
public class MetaDataFacadeBean extends NuclosFacadeBean implements MetaDataFacadeLocal, MetaDataFacadeRemote {

	private static final Logger LOG = LoggerFactory.getLogger(MetaDataFacadeBean.class);

	private ProcessorFactorySingleton processorFactory;

	private SpringDataBaseHelper dataBaseHelper;

	@Autowired
	private DatasourceServerUtils datasourceServerUtils;

	@Autowired
	private	SessionUtils sessionUtils;

	private DataSource dataSource;

	private GenericObjectFacadeLocal genericObjectFacade;

	private MasterDataFacadeLocal masterDataFacade;

	@Autowired
	private EntityObjectFacadeLocal entityObjectFacadeLocal;

	private LocaleFacadeLocal localeFacade;

	@Autowired
	private TreeMetaProvider treeProvider;

	@Autowired
	private EventSupportCache esCache;

	@Autowired
	private DataLanguageFacadeBean dataLanguageService;

	@Autowired
	private NuclosJarGeneratorManager nuclosJarGeneratorManager;

	private ThreadLocal<CompileGeneratedCodeTransactionSynchronization> transactionSyncs =
		new ThreadLocal<>();

	public MetaDataFacadeBean() {
	}

	@Autowired
	void setDataBaseHelper(SpringDataBaseHelper dataBaseHelper) {
		this.dataBaseHelper = dataBaseHelper;
	}

	@Autowired
	@Qualifier("nuclos")
	void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public ProcessorFactorySingleton getProcessorFactory() {
		return processorFactory;
	}

	@Autowired
	void setProcessorFactory(ProcessorFactorySingleton processorFactory) {
		this.processorFactory = processorFactory;
	}

	@Autowired
	final void setGenericObjectFacade(GenericObjectFacadeLocal genericObjectFacade) {
		this.genericObjectFacade = genericObjectFacade;
	}

	private final GenericObjectFacadeLocal getGenericObjectFacade() {
		return genericObjectFacade;
	}

	@Autowired
	final void setMasterDataFacade(MasterDataFacadeLocal masterDataFacade) {
		this.masterDataFacade = masterDataFacade;
	}

	private final LocaleFacadeLocal getLocaleFacade() {
		return localeFacade;
	}

	@Autowired
	final void setLocaleFacade(LocaleFacadeLocal localeFacade) {
		this.localeFacade = localeFacade;
	}


	public DataLanguageFacadeBean getDataLanguageService() {
		return dataLanguageService;
	}

	@Override
    @RolesAllowed("Login")
	public Collection<EntityMeta<?>> getAllEntities() {
		return MetaProvider.getInstance().getAllEntities();
	}

	@Override
    @RolesAllowed("Login")
	public Map<UID, FieldMeta<?>> getAllEntityFieldsByEntity(UID entity) {
		return MetaProvider.getInstance().getAllEntityFieldsByEntity(entity);
	}

	@Override
    @RolesAllowed("Login")
	public 	Map<UID, Map<UID, FieldMeta<?>>> getAllEntityFieldsByEntitiesGz(List<UID> entities) {
		return MetaProvider.getInstance().getAllEntityFieldsByEntitiesGz(entities);
	}

	@Override
    @RolesAllowed("Login")
	public Collection<EntityMeta<?>> getNucletEntities() {
		return MetaProvider.getInstance().getAllEntities();
	}

	@Override
    @RolesAllowed("Login")
	public List<EntityObjectVO<UID>> getNuclets() {
		return MetaProvider.getInstance().getNuclets();
	}

	@Override
	public Set<UID> getImplementingEntities(final UID genericEntityUID) {
		return MetaProvider.getInstance().getImplementingEntities(genericEntityUID);
	}

	@Deprecated
	@Override
    public Object modifyEntityMetaData(UID entityUid, List<FieldMetaTransport> toFields) {

		final EntityMeta<?> metaVO = MetaProvider.getInstance().getEntity(entityUid);

		final List<FieldMeta<?>> lstFields = new ArrayList<FieldMeta<?>>();

		for(FieldMetaTransport to : toFields) {
			if(!to.getEntityFieldMeta().isFlagRemoved()) {
				lstFields.add(to.getEntityFieldMeta());
			}
		}

		final EntityMeta<?> voIst = MetaProvider.getInstance().getEntity(metaVO.getUID());

		final MetaDbHelper dbHelperIst = new MetaDbHelper(E.getSchemaHelperVersion(), dataBaseHelper.getDbAccess(), MetaProvider.getInstance(), null, null);

		final DbTable tableIst = dbHelperIst.getDbTable(metaVO);

		final StaticMetaDataProvider staticMetaData = new StaticMetaDataProvider(E.getThis());

		final EntityMeta<?> originMeta = MetaProvider.getInstance().getEntity(metaVO.getUID());
		staticMetaData.addClone(originMeta, false);

		final Collection<FieldMeta<?>> colFields = originMeta.getFields();
		if (colFields != null) {
			for(FieldMeta<?> vo : colFields) {
				boolean addField = true;
				for(FieldMetaTransport to : toFields) {
					if(to.getEntityFieldMeta().getFieldName().equals(vo.getFieldName())) {
						if(to.getEntityFieldMeta().isFlagRemoved()){
							addField = false;
						}
					}
					if(to.getEntityFieldMeta().getPrimaryKey() != null) {
						if(to.getEntityFieldMeta().getPrimaryKey().equals(vo.getUID())) {
							addField = false;
						}
					}

				}
				if(addField) {
					FieldMetaVO<?> clone = staticMetaData.addClone(vo);
					if(vo.getForeignEntity() != null) {
						clone.setReadonly(false);
						staticMetaData.addClone(MetaProvider.getInstance().getEntity(vo.getForeignEntity()), true);
					}
				}
			}
		}
		for(FieldMetaTransport to : toFields) {
			final NucletFieldMeta<?> vo = to.getEntityFieldMeta();
			if(vo.getForeignEntity() != null) {
				if(!vo.isFlagRemoved()) {
					vo.setReadonly(false);
					staticMetaData.addClone(MetaProvider.getInstance().getEntity(vo.getForeignEntity()), true);
				}
			}
			if(!vo.isFlagRemoved()) {
				staticMetaData.addClone(vo);
			}
		}

		final MetaDbHelper dbHelperSoll = new MetaDbHelper(E.getSchemaHelperVersion(), dataBaseHelper.getDbAccess(), staticMetaData, null, null);

		final DbTable tableSoll = dbHelperSoll.getDbTable(metaVO);

		List<DbStructureChange> lstStructureChanges = null;
		if(voIst.getUID() != null) {
			lstStructureChanges = SchemaUtils.modify(tableIst, tableSoll, false);
		}
		else {
			lstStructureChanges = SchemaUtils.create(tableSoll);
		}

		for(DbStructureChange ds : lstStructureChanges) {
			dataBaseHelper.getDbAccess().execute(ds);
		}

		for(FieldMetaTransport metaFieldTO : toFields) {
			final NucletFieldMeta<?> metaFieldVO = metaFieldTO.getEntityFieldMeta();
			if(metaFieldVO.getUID() == null) {
				metaFieldVO.setPrimaryKey(new UID());
				metaFieldVO.setEntity(voIst.getUID());
				metaFieldVO.flagNew();
			}
			else {
				if(!metaFieldVO.isFlagRemoved()) {
					metaFieldVO.flagUpdate();
					metaFieldVO.setEntity(voIst.getUID());
				}
			}
			if(metaFieldVO.isFlagRemoved()) {
				final DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();

				// delete generic implementing fields before field itself, no delete-on-cascade cause of MS-SQL Problems (see NUCLOS-7121)
				DbDelete delete = builder.createDelete(E.ENTITY_GENERIC_FIELDMAPPING);
				delete.where(builder.equalValue(delete.baseColumn(E.ENTITY_GENERIC_FIELDMAPPING.genericField), metaFieldVO.getPrimaryKey()));
				dataBaseHelper.getDbAccess().executeDelete(delete);

				NucletDalProvider.getInstance().getEntityFieldMetaDataProcessor().delete(new Delete(metaFieldVO.getPrimaryKey()));
			}
			else {
				DalUtils.updateVersionInformation(metaFieldVO, getCurrentUserName());
				NucletDalProvider.getInstance().getEntityFieldMetaDataProcessor().insertOrUpdate(metaFieldVO);
				createResourceIdForEntityField(metaFieldTO, metaFieldTO.getEntityFieldMeta().getPrimaryKey());
			}
		}
		MetaProvider.getInstance().revalidate(true, false);
		return null;
	}

	private void createResourceIdForEntity(EntityMetaTransport mdvo, UID uid) {
		if (mdvo.getTranslation() == null) {
			return;
		}

		Map<String, FieldMeta<String>> mp = new HashMap<String, FieldMeta<String>>();

		mp.put(TranslationVO.LABELS_ENTITY[0], E.ENTITY.localeresourcel);
		mp.put(TranslationVO.LABELS_ENTITY[1], E.ENTITY.localeresourcem);
		mp.put(TranslationVO.LABELS_ENTITY[2], E.ENTITY.localeresourcetw);
		mp.put(TranslationVO.LABELS_ENTITY[3], E.ENTITY.localeresourcett);

		final Locale currentLocale = SpringLocaleDelegate.getInstance().getLocale();

		final Map<String, String> mapOldValues = new HashMap<>();

		for(String key : mp.keySet()) {
			String sResId = null;
			Collection<LocaleInfo> colLocaleInfo = getLocaleFacade().getAllLocales(false);
			for(LocaleInfo li : colLocaleInfo) {
				for(TranslationVO vo : mdvo.getTranslation()) {
					if(vo.getLanguage().equals(li.getLanguage())) {
						final String valueNew = vo.getLabels().get(key);
						if(valueNew != null && valueNew.length() > 0) {
							if(sResId == null)
								sResId = getResourceIdFromMetaDataVO(mdvo.getEntityMetaVO(), mp.get(key));
							if (sResId != null) {
								final String valueOld = getLocaleFacade().getResourceById(li, sResId);
								if (valueOld != null && !valueOld.equals(valueNew) &&
										currentLocale.getLanguage().equalsIgnoreCase(li.getLanguage())) {
									// NUCLOS-6294 Update References on integration points also
									updateIntegrationPointReferenceStringifiedValues(uid, valueOld, valueNew);
								}
							}
							sResId = getLocaleFacade().setResourceForLocale(sResId, li, valueNew);
							LocaleUtils.setResourceIdForField(E.ENTITY, uid, mp.get(key), sResId);
							break;
						}
					}
				}
			}
		}
		getLocaleFacade().flushInternalCaches(true);
	}

	private void updateIntegrationPointReferenceStringifiedValues(UID foreignEntityUID, String valueOld, String valueNew) {
		final Query<EntityField> qEFields = QueryProvider.create(EntityField.class);
		qEFields.where(EntityField.ForeignentityId.eq(foreignEntityUID));
		qEFields.and(EntityField.Foreignentityfield.eq(valueOld));
		final List<EntityField> foundFields = QueryProvider.execute(qEFields);
		for (EntityField ef : foundFields) {
			boolean bUpdate = ef.getForeignIntegrationPointId() != null;
			if (!bUpdate) {
				// check for integrated fields also (NUCLOS-6294)
				final Query<NucletIntegrationField> qIFields = QueryProvider.create(NucletIntegrationField.class);
				qIFields.where(NucletIntegrationField.TargetFieldId.eq(ef.getId()));
				qEFields.and(NucletIntegrationField.IntegrationPointReferenceFieldId.notNull());
				bUpdate = !QueryProvider.execute(qIFields).isEmpty();
			}
			if (bUpdate) {
				ef.setForeignentityfield(valueNew);
				try {
					BusinessObjectProvider.update(ef);
				} catch (BusinessException e) {
					LOG.error("Error during update of stringified value for integration point references: " + e.getMessage(), e);
				}
			}
		}
	}

	private void createResourceIdForEntityField(FieldMetaTransport mdvo, UID uid) {
		if(mdvo.getTranslation() == null) {
			return;
		}

		Map<String, FieldMeta<String>> mp = new HashMap<String, FieldMeta<String>>();

		mp.put(TranslationVO.LABELS_FIELD[0], E.ENTITYFIELD.localeresourcel);
		mp.put(TranslationVO.LABELS_FIELD[1], E.ENTITYFIELD.localeresourced);

		for(String key : mp.keySet()) {
			String sResId = null;
			Collection<LocaleInfo> colLocaleInfo = getLocaleFacade().getAllLocales(false);
			for(LocaleInfo li : colLocaleInfo) {
				for(TranslationVO vo : mdvo.getTranslation()) {
					if(vo.getLanguage().equals(li.getLanguage())) {
						if(vo.getLabels().get(key) != null && vo.getLabels().get(key).length() > 0) {
							if(sResId == null)
								sResId = getResourceIdFromMetaDataVO(mdvo.getEntityFieldMeta(), mp.get(key));
							sResId = getLocaleFacade().setResourceForLocale(sResId, li, vo.getLabels().get(key));
							LocaleUtils.setResourceIdForField(E.ENTITYFIELD, uid, mp.get(key), sResId);
							break;
						}
					}
				}
			}
		}
		getLocaleFacade().flushInternalCaches(true);
	}

	private static String getResourceIdFromMetaDataVO(FieldMeta<?> metavo, FieldMeta<String> resField) {
		if (resField.equals(E.ENTITYFIELD.localeresourcel)) {
			return metavo.getLocaleResourceIdForLabel();
		} else if (resField.equals(E.ENTITYFIELD.localeresourced)) {
			return metavo.getLocaleResourceIdForDescription();
		}
		return null;
	}

	private static String getResourceIdFromMetaDataVO(EntityMeta<?> metavo, FieldMeta<String> resField) {
		if (resField.equals(E.ENTITY.localeresourcel)) {
			return metavo.getLocaleResourceIdForLabel();
		} else if (resField.equals(E.ENTITY.localeresourcem)) {
			return metavo.getLocaleResourceIdForMenuPath();
		} else if (resField.equals(E.ENTITY.localeresourced)) {
			return metavo.getLocaleResourceIdForDescription();
		} else if (resField.equals(E.ENTITY.localeresourcetw)) {
			return metavo.getLocaleResourceIdForTreeView();
		} else if (resField.equals(E.ENTITY.localeresourcett)) {
			return metavo.getLocaleResourceIdForTreeViewDescription();
		}
		return null;
	}

	@RolesAllowed("Login")
	@Override
	public boolean hasEntityImportStructure(UID entityUID) throws CommonBusinessException {
		final DbQuery<DbTuple> query = dataBaseHelper.getDbAccess().getQueryBuilder().createTupleQuery();
		final DbFrom<?> from = query.from(E.IMPORT);

		final List<DbSelection<?>> columns = new ArrayList<DbSelection<?>>();
		columns.add(from.baseColumn(E.IMPORT.getPk()).alias("INTID"));
		columns.add(from.baseColumn(E.IMPORT.entity).alias("INTID_T_AD_MASTERDATA"));

		query.multiselect(columns);
		query.where(dataBaseHelper.getDbAccess().getQueryBuilder().equalValue(from.baseColumn(E.IMPORT.entity),	entityUID));

		List<DbTuple> count = dataBaseHelper.getDbAccess().executeQuery(query);

		return count.size() > 0;
	}

	@Override
	@RolesAllowed("Login")
	public boolean hasEntityWorkflow(UID entityUID) throws CommonBusinessException {
		final DbQuery<DbTuple> query = dataBaseHelper.getDbAccess().getQueryBuilder().createTupleQuery();
		final DbFrom<?> from = query.from(E.GENERATION);

		List<DbSelection<?>> columns = new ArrayList<DbSelection<?>>();
		columns.add(from.baseColumn(E.GENERATION.getPk()).alias("INTID"));
		columns.add(from.baseColumn(E.GENERATION.targetModule).alias("INTID_T_MD_MODULE_TARGET"));
		columns.add(from.baseColumn(E.GENERATION.sourceModule).alias("INTID_T_MD_MODULE_SOURCE"));

		query.multiselect(columns);

		DbCondition cond1 = dataBaseHelper.getDbAccess().getQueryBuilder().equalValue(from.baseColumn(E.GENERATION.targetModule), entityUID);
		DbCondition cond2 = dataBaseHelper.getDbAccess().getQueryBuilder().equalValue(from.baseColumn(E.GENERATION.sourceModule), entityUID);

		query.where(dataBaseHelper.getDbAccess().getQueryBuilder().or(cond1, cond2));

		List<DbTuple> count = dataBaseHelper.getDbAccess().executeQuery(query);

		return count.size() > 0;
	}

	@Override
    public void removeEntity(UID entityUid, boolean dropLayout) throws CommonBusinessException {
		final EntityMeta<?> voEntity;
		if (MetaProvider.getInstance().getEntity(entityUid).isProxy()) {
			voEntity = MetaProvider.getInstance().getEntity(entityUid);
		} else {
			voEntity = clearAndDropTableWithNewTransaction(entityUid);
		}
		try {
			DefaultSchemaValidation.removeSysContraints("Remove entity " + voEntity.getEntityName());
			removeEntityWithNewTransaction(voEntity, dropLayout);
			DefaultSchemaValidation.validate(true, jaxb2Marshaller, true);
			triggerRecompile();
		} catch (Exception e) {
			LOG.error("remove sys constraints before entity remove failed: {}", e.getMessage(), e);
		} finally {
			try {
				DefaultSchemaValidation.createSysConstraints("Remove entity " + voEntity.getEntityName());
			} catch (Exception e) {
				LOG.error("recreate sys constraints after entity remove failed: {}", e.getMessage(), e);
				throw new NuclosFatalException("recreate sys constraints after entity remove failed: " + e.getMessage(), e);
			} finally {
				revalidateCachesInNewTransaction();
			}
		}
	}

	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
	private <PK> EntityMeta<PK> clearAndDropTableWithNewTransaction(UID entityUid) {
		EntityMeta<PK> voEntity = MetaProvider.getInstance().getEntity(entityUid);

		// check if we have to delete language tables first
		if (metaProvider.hasEntity(NucletEntityMeta.getEntityLanguageUID(voEntity.getUID()))) {
			nucletDalProvider.getDataLanguageMetaDataProcessor().remove(
					metaProvider.getEntity(NucletEntityMeta.getEntityLanguageUID(voEntity.getUID())));
			if (voEntity instanceof NucletEntityMeta) {
				for (FieldMeta fm : voEntity.getFields()) {
					if (fm instanceof NucletFieldMeta) {
						NucletFieldMeta nfm = (NucletFieldMeta) fm;
						if (fm.isLocalized()) {
							nfm.setLocalized(false);
							nfm.flagUpdate();
							nucletDalProvider.getEntityFieldMetaDataProcessor().insertOrUpdate(nfm);
						}
					}
				}
				NucletEntityMeta nem = (NucletEntityMeta) voEntity;
				nem.setDataLangRefPath(null);
				nem.setDataLanguageDBTable(null);
				nem.flagUpdate();
				nucletDalProvider.getEntityMetaDataProcessor().insertOrUpdate(nem);
				metaProvider.revalidate(true, false);
				voEntity = MetaProvider.getInstance().getEntity(entityUid);
			}

		}


		if (voEntity.getVirtualEntity() == null) {
			if(hasEntityRows(voEntity.getUID())) {
				if(voEntity.isStateModel()) {
					GenericObjectFacadeLocal local = ServerServiceLocator.getInstance().getFacade(GenericObjectFacadeLocal.class);
					for(Long iId : local.getGenericObjectIdsNoCheck(voEntity.getUID(), new CollectableSearchExpression())) {
						Set<UID> setNames = new HashSet<UID>();
						try {
							local.remove(voEntity.getUID(), iId, true, ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
						}
						catch(CommonBusinessException e) {
							throw new NuclosFatalException(e);
						}
					}
				} else {
					final MasterDataFacadeLocal local = ServerServiceLocator.getInstance().getFacade(MasterDataFacadeLocal.class);
					List<Long> lstIds = local.getMasterDataIds(voEntity.getUID());
					for(Long id : lstIds) {
						try {
							local.remove(voEntity.getUID(), id, false, ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
						}
						catch(NuclosBusinessRuleException e) {
							throw new NuclosFatalException(e);
						}
						catch(CommonBusinessException e) {
							throw new NuclosFatalException(e);
						}
					}
				}
			}
		}


		final MetaDbHelper helper = new MetaDbHelper(E.getSchemaHelperVersion(), MetaProvider.getInstance(), null, null);
		final DbTable table = helper.getDbTable(voEntity);

		List<DbStructureChange> lstChanges = SchemaUtils.drop(table);
		for(DbStructureChange db : lstChanges) {
			try {
				dataBaseHelper.getDbAccess().execute(db);
			} catch (DbException ex) {
				// Missing constraints... log only
				LOG.error("Error during drop of table \"" + table.getTableName() + "\": " + ex.getMessage(), ex);
			}
		}

		return voEntity;
	}

	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
	private void removeEntityWithNewTransaction(EntityMeta<?> voEntity, boolean dropLayout) {
		final DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();

		// delete layouts
		if(dropLayout) {
			// query layouts
			DbQuery<DbTuple> query = builder.createTupleQuery();
			DbFrom<UID> from = query.from(E.LAYOUTUSAGE);
			List<DbSelection<?>> columns = new ArrayList<DbSelection<?>>();

			columns.add(from.baseColumn(E.LAYOUTUSAGE.entity).alias("STRENTITY"));
			columns.add(from.baseColumn(E.LAYOUTUSAGE.getPk()).alias("INTID"));
			columns.add(from.baseColumn(E.LAYOUTUSAGE.layout).alias("INTID_T_MD_LAYOUT"));
			query.multiselect(columns);
			query.where(builder.equalValue(from.baseColumn(E.LAYOUTUSAGE.entity),	voEntity.getUID()));

			List<UID> lstDeleteIds = new ArrayList<UID>();
			List<DbTuple> usages = dataBaseHelper.getDbAccess().executeQuery(query);

			for(DbTuple tuple : usages) {
			   UID idLayout = tuple.get("INTID_T_MD_LAYOUT", UID.class);
			   lstDeleteIds.add(idLayout);
			}

			for(UID idLayout : lstDeleteIds) {
				DbMap mpDelLayout = new DbMap();
				mpDelLayout.put(E.LAYOUT.getPk(), idLayout);
				DbDeleteStatement<UID> delLayout = new DbDeleteStatement<UID>(E.LAYOUT, mpDelLayout);
				dataBaseHelper.getDbAccess().execute(delLayout);
			}
		}

		// delete generic implementions before entity itself, no delete-on-cascade cause of MS-SQL Problems (see NUCLOS-7121)
		DbDelete delete = builder.createDelete(E.ENTITY_GENERIC_IMPLEMENTATION);
		delete.where(builder.equalValue(delete.baseColumn(E.ENTITY_GENERIC_IMPLEMENTATION.genericEntity), voEntity.getUID()));
		dataBaseHelper.getDbAccess().executeDelete(delete);

		// delete entity
		NucletDalProvider.getInstance().getEntityMetaDataProcessor().delete(new Delete(voEntity.getUID()));
	}

	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
	private void revalidateCachesInNewTransaction() {
		MetaProvider.getInstance().revalidate(true, true);
	}

	@Override
    @RolesAllowed("Login")
	public boolean hasEntityRows(UID entityUid) {
		final EntityMeta<?> voEntity = MetaProvider.getInstance().getEntity(entityUid);

		if (voEntity.isProxy() || voEntity.isGeneric()) {
			return false;
		}

		final DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		final DbQuery<?> query;

		if (voEntity.isUidEntity()) {
			query = builder.createQuery(UID.class);
			final DbFrom t = query.from(voEntity);
			query.select(t.baseColumn(SF.PK_UID)).limit(1l);
		} else {
			query = builder.createQuery(Long.class);
			final DbFrom t = query.from(voEntity);
			query.select(t.baseColumn(SF.PK_ID)).limit(1l);
		}

		return dataBaseHelper.getDbAccess().executeQuery(query).stream().count() > 0;
	}

	public void setMandatorLevel(UID entityUID, UID mandatorLevelUID, UID initialMandatorUID) throws NuclosBusinessException {
		NucletEntityMeta eMeta = nucletDalProvider.getEntityMetaDataProcessor().getByPrimaryKey(entityUID);
		eMeta.setMandatorLevel(mandatorLevelUID);
		EntityMetaTransport eMetaTransport = new EntityMetaTransport();
		eMetaTransport.setEntityMetaVO(eMeta);
		eMetaTransport.setMandatorInitialFill(initialMandatorUID);
		List<FieldMetaTransport> toFields = new ArrayList<FieldMetaTransport>();
		for (NucletFieldMeta<?> fMeta : nucletDalProvider.getEntityFieldMetaDataProcessor().getByParent(entityUID)) {
			FieldMetaTransport fMetaTransport = new FieldMetaTransport();
			fMetaTransport.setEntityFieldMeta(fMeta);
			toFields.add(fMetaTransport);
		}

		this.createOrModifyEntity(eMetaTransport, toFields, true, null);
	}

	@Override
    public String createOrModifyEntity(
		final EntityMetaTransport updatedTOEntity,
		final List<FieldMetaTransport> toFields,
		final boolean bRollBackOnStructureChangeExceptions,
		final Map<String, Exception> structureChangeExceptions)
		throws NuclosBusinessException {

		// TODO: Why not separate into a 'create' and 'modify' method?
		NucletEntityMeta updatedMDEntity = updatedTOEntity.getEntityMetaVO();
		UID entityUID = updatedMDEntity.getUID();

		boolean isStateModelAdded = false;
		boolean isStateModelRemoved = false;

		// determine isNew & old document path
		NucletEntityMeta oldEntityMeta = null;
		boolean isNew = false;
		if(entityUID != null && metaProvider.hasEntity(entityUID)) {
			oldEntityMeta = new NucletEntityMeta(metaProvider.getEntity(entityUID), true);

			isStateModelAdded = updatedMDEntity.isStateModel() && !oldEntityMeta.isStateModel();
			isStateModelRemoved = !updatedMDEntity.isStateModel() && oldEntityMeta.isStateModel();
		} else {
			isNew = true;
		}

		String resultMessage = null;

		final StaticMetaDataProvider staticMetaData = new StaticMetaDataProvider(E.getThis());
		staticMetaData.addClone(updatedMDEntity, false);

		final List<NucletFieldMeta<?>> fieldMetas = new ArrayList<>();
		final List<FieldMetaTransport> toNonSystemFields = new ArrayList<>();

		final List<FieldMeta<?>> systemFields = new ArrayList<>();
		DalUtils.addStaticFields(systemFields, updatedMDEntity);

		List<NucletFieldMeta> localizedFields = new ArrayList<>();
		for(FieldMetaTransport toField : toFields) {
			NucletFieldMeta<?> fieldMeta = toField.getEntityFieldMeta();

			if (fieldMeta.isLocalized() && fieldMeta.getCalcFunction() == null
			    && !fieldMeta.isFlagRemoved()) {
				localizedFields.add(fieldMeta);
			}

			boolean alteredSystemField = false;
			if (fieldMeta.getPrimaryKey() != null && entityUID != null &&
					SF.isEOField(entityUID, fieldMeta.getPrimaryKey())) {
				//So we've got an altered System Field here. Extra-Wurscht
				alteredSystemField = true;
			} else {
				toNonSystemFields.add(toField);
			}

			if(!fieldMeta.isFlagRemoved()) {
				fieldMetas.add(fieldMeta);
				if (alteredSystemField) {
					continue;
				}


				FieldMetaVO fieldMetaClone = staticMetaData.addClone(fieldMeta);

				if(fieldMeta.getForeignEntity() != null) {
					super.validateEntityFieldDefaults(fieldMeta,fieldMetaClone,null,dataBaseHelper,datasourceServerUtils,sessionUtils,null);
				}

				if(entityUID.equals(fieldMeta.getForeignEntity()) || entityUID.equals(fieldMeta.getLookupEntity())) {
					continue;
					// NUCLOSINT-697
				}
				if(fieldMeta.getForeignEntity() != null) {
					staticMetaData.addClone(metaProvider.getEntity(fieldMeta.getForeignEntity()), true);
				}
				if(fieldMeta.getLookupEntity() != null) {
					staticMetaData.addClone(metaProvider.getEntity(fieldMeta.getLookupEntity()), true);
				}
			}
		}

		for(FieldMeta<?> voSystemField : systemFields) {
			staticMetaData.addClone(voSystemField);
			if(voSystemField.getForeignEntity() != null) {
				staticMetaData.addClone(metaProvider.getEntity(voSystemField.getForeignEntity()),
				                        true);
			}
		}

		final MetaDbHelper dbHelperIst = new MetaDbHelper(E.getSchemaHelperVersion(), dataBaseHelper.getDbAccess(), metaProvider, null, null);

		UID mandatorFieldUID = SF.MANDATOR.getUID(entityUID);
		final MetaDbHelper dbHelperSoll = new MetaDbHelper(E.getSchemaHelperVersion(), dataBaseHelper.getDbAccess(), staticMetaData, null, null);
		if (!isNew && !updatedMDEntity.isVirtual() && !updatedMDEntity.isProxy() && !updatedMDEntity.isGeneric() &&
				updatedMDEntity.isMandator() && !oldEntityMeta.isMandator()) {
			if (updatedTOEntity.getMandatorInitialFill() == null) {
				DbQueryBuilder qb = dataBaseHelper.getDbAccess().getQueryBuilder();
				DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
				DbQuery<Long> query = builder.createQuery(Long.class);
				DbFrom t = query.from(updatedMDEntity);
				query.select(builder.countRows());
				Long count = dataBaseHelper.getDbAccess().executeQuerySingleResult(query);
				if (count > 0) {
					throw new IllegalArgumentException(
						"Mandator-dependency could not be enabled: "
					    + "Data exist in table but no mandator initial fill is set!");
				}
			}
			dbHelperSoll.setOverwriteNullable(mandatorFieldUID, true);
		}

		// Statemodel removed -> generic object updates need to be performed BEFORE the corresponding system fields are removed
		if (isStateModelRemoved) {
			genericObjectFacade.removeGenericObjectEntries(entityUID);
			// delete processes
			IEntityObjectProcessor<UID> processProc = nucletDalProvider.getEntityObjectProcessor(E.PROCESS);
			processProc.getIdsBySearchExpression(new CollectableSearchExpression(
					SearchConditionUtils.newUidComparison(E.PROCESS.module, ComparisonOperator.EQUAL, entityUID))).forEach(
							pk -> {
								this.deleteReferencingObjects(E.STATEMODELUSAGE.process, pk);
								this.deleteReferencingObjects(E.LAYOUTUSAGE.process, pk);
								this.deleteReferencingObjects(E.SERVERCODEENTITY.process, pk);
								this.deleteReferencingObjects(E.GENERATIONUSAGE.process, pk);
								this.deleteReferencingObjects(E.ENTITYMENU.process, pk);
								this.deleteReferencingObjects(E.REPORTUSAGE.process, pk);
								processProc.delete(new Delete<>(pk));
							}
			);
		}

		// execute changes on table (SQL)
		final DbTable tableIst = dbHelperIst.getDbTable(isNew ? updatedMDEntity : metaProvider.getEntity(updatedMDEntity.getUID()));
		final DbTable tableSoll = dbHelperSoll.getDbTable(updatedMDEntity);
		List<DbStructureChange> lstStructureChanges;
		if(!isNew) {
			lstStructureChanges = SchemaUtils.modify(tableIst, tableSoll, false);
		}
		else {
			lstStructureChanges = SchemaUtils.create(tableSoll);
		}

		final List<DbStructureChange> dbChangesOkay = new ArrayList<>();
		final List<DbStructureChange> dbChangesNotOkay = new ArrayList<>();

		boolean dbChangeOkay = true;
		for(DbStructureChange ds : lstStructureChanges) {
			try {
				dataBaseHelper.getDbAccess().execute(ds);
				dbChangesOkay.add(ds);
			} catch(DbException e) {
				LOG.info("createOrModifyEntity failed: {}", e);
				dbChangeOkay = false;
				dbChangesNotOkay.add(ds);
				if (structureChangeExceptions != null) {
					final StringBuilder sb = new StringBuilder();
					appendSqlStatements(dataBaseHelper.getDbAccess(), ds, sb);
					structureChangeExceptions.put(sb.toString(), e);
				}
			}
		}

		// SQL: mandator update
		if (updatedTOEntity.getMandatorInitialFill() != null) {
			final DbMap condition = new DbMap();
			final DbMap update = new DbMap();
			update.put(SF.MANDATOR_UID, updatedTOEntity.getMandatorInitialFill());
			condition.putNull(SF.MANDATOR_UID);
			dataBaseHelper.getDbAccess().execute(
				new DbUpdateStatement<UID>(updatedMDEntity.getDbTable(), update, condition));
		}
		// SQL: mandator:
		if (dbHelperSoll.isOverwriteNullable(mandatorFieldUID)) {
			dbHelperSoll.removeOverwriteNullable(mandatorFieldUID);
			final DbTable tableSollWithoutNullable = dbHelperSoll.getDbTable(updatedMDEntity);
			lstStructureChanges = SchemaUtils.modify(tableSoll, tableSollWithoutNullable, false);
			for(DbStructureChange ds : lstStructureChanges) {
				try {
					dataBaseHelper.getDbAccess().execute(ds);
					dbChangesOkay.add(ds);
				}
				catch(DbException e) {
					LOG.info("createOrModifyEntity failed: {}", e);
					dbChangeOkay = false;
					dbChangesNotOkay.add(ds);
				}
			}
		}

		// Error handling
		if(!dbChangeOkay && bRollBackOnStructureChangeExceptions) {
			final StringBuilder sb = new StringBuilder();
			final DbAccess dbAccess = dataBaseHelper.getDbAccess();
			if(!isNew) {
				// entity was updated:
				rollBackDBChanges(updatedTOEntity, toNonSystemFields);
				sb
					.append("Entit\u00e4t ")
					.append(updatedMDEntity.getEntityName())
					.append(" konnte nicht ver\u00e4ndert werden.\n")
					.append("Grund:\n");
				appendSqlStatements(dbAccess, dbChangesNotOkay.get(0), sb);
			} else {
				// entity is new:
				final MetaDbHelper helper = new MetaDbHelper(E.getSchemaHelperVersion(), metaProvider, null, null);
				final DbTable table = helper.getDbTable(updatedMDEntity);

				final List<DbStructureChange> tableSQLChanges = SchemaUtils.drop(table);
				for(DbStructureChange db : tableSQLChanges) {
					try {
						dataBaseHelper.getDbAccess().execute(db);
					}
					catch(DbException e)  {
						// ignore
						LOG.info("createOrModifyEntity: {}", e);
					}
				}


				List<String> statementsForLogging = Collections.emptyList();
				try {
					final IBatch batch = dbAccess.getBatchFor(dbChangesNotOkay.get(0));
					statementsForLogging = dbAccess.getStatementsForLogging(batch);
				} catch (SQLException e) {
					sb
						.append("Failed to get SQL statement (")
						.append(dbChangesNotOkay.get(0))
						.append("): ")
						.append(e);
				}
				sb
					.append("Entit\u00e4t ")
					.append(updatedMDEntity.getEntityName())
					.append(" konnte nicht angelegt werden.\n")
					.append("Grund:\n")
					.append(statementsForLogging.get(0));
			}
			return sb.toString();
		}


		// insert entity meta + field meta:
		try {
			final String currentUserName = getCurrentUserName();
			DalUtils.updateVersionInformation(updatedMDEntity, currentUserName);

			// we want a foreign key on this table
			staticMetaData.addClone(E.DATA_LANGUAGE, true);
			final MetaDbProvider updatedProvider = new MetaDbProvider(staticMetaData, null, null);

			if(!isNew) {
				updatedMDEntity.flagUpdate();

				nucletDalProvider.getEntityMetaDataProcessor().insertOrUpdate(updatedMDEntity);

				//TODO: Optimize. This fires each time a coupe of updates on T_MD_ENTITY and T_MD_LOCALERESOURCE, even nothing was changed
				createResourceIdForEntity(updatedTOEntity, entityUID);
				setSystemValuesAndParent(fieldMetas, updatedMDEntity);

				insertOrUpdateEntityFieldMetaData(toFields);

				for(FieldMetaTransport toField : toFields) {
					NucletFieldMeta<?> nfm = toField.getEntityFieldMeta();

					if (nfm.isFlagUpdated() || nfm.isFlagNew()) {
						createResourceIdForEntityField(toField, nfm.getPrimaryKey());
					}
				}

				// If one field is using localization we need to create/update the language table
				if (!localizedFields.isEmpty()) {
					UID dataLangUID = NucletEntityMeta.getEntityLanguageUID(
						entityUID);
					final Boolean isNewDL = !MetaProvider.getInstance().hasEntity(dataLangUID);

					// create or modify language table
					final EntityMeta newDLEntityMeta = createOrUpdateEntityLanguage(updatedMDEntity, toFields,
							SF.PK_ID.getMetaData(updatedMDEntity), updatedProvider);

					final List<FieldMeta<?>> transFields = new ArrayList<>();
					for (FieldMetaTransport trans : toFields) {
						if (!trans.getEntityFieldMeta().isFlagRemoved()) {
							transFields.add(trans.getEntityFieldMeta());
						}
					}

					metaProvider.revalidate(true, false);
					final NucletEntityMeta metaEntity = updatedMDEntity;
					Thread t = new Thread(new Runnable() {
						@Override
						public void run() {
							// Fill language table with defaults from primary language
							getDataLanguageService().fillLocalizedDataIntoLanguageTable(isNewDL, metaEntity, transFields, newDLEntityMeta);
						}
					});
					t.start();


				} else {
					// otherwise we have to remove an existing language table is existing
					EntityMeta<Object> entity = null;
					try {
						entity = metaProvider.getEntity(NucletEntityMeta.getEntityLanguageUID(updatedMDEntity));
					} catch (Exception e) {}
					if (entity != null) {
						nucletDalProvider.getDataLanguageMetaDataProcessor().remove(
								metaProvider.getEntity(NucletEntityMeta.getEntityLanguageUID(updatedMDEntity)));
						nucletDalProvider.invalidate();
						metaProvider.revalidate(true, false);
					}
				}
			} else {
				// entity is new:
				updatedMDEntity.flagNew();

				nucletDalProvider.getEntityMetaDataProcessor().insertOrUpdate(updatedMDEntity);

				createResourceIdForEntity(updatedTOEntity, entityUID);
				setSystemValuesAndParent(fieldMetas, updatedMDEntity);
				insertOrUpdateEntityFieldMetaData(toFields);
				for(FieldMetaTransport toField : toFields) {
					createResourceIdForEntityField(toField, toField.getEntityFieldMeta().getPrimaryKey());
				}

				// If one field is using localization we need to create a new language table
				if (!localizedFields.isEmpty()) {
					final EntityMeta newLangEntity = createOrUpdateEntityLanguage(updatedMDEntity, toFields, SF.PK_ID.getMetaData(entityUID), updatedProvider);

					final List<FieldMeta<?>> transFields = new ArrayList<>();
					for (FieldMetaTransport trans : toFields) {
						if (!trans.getEntityFieldMeta().isFlagRemoved()) {
							transFields.add(trans.getEntityFieldMeta());
						}
					}
					metaProvider.revalidate(true, false);
					final NucletEntityMeta metaEntity = updatedMDEntity;
					Thread t = new Thread(new Runnable() {
						@Override
						public void run() {
							// Fill language table with defaults from primary language
							getDataLanguageService().fillLocalizedDataIntoLanguageTable(true, metaEntity, transFields, newLangEntity);
						}
					});
					t.start();
				} else {
					metaProvider.revalidate(true, false);
				}

			}

			// update contexts:
			if (updatedTOEntity.getContexts() != null) {
				for (EntityObjectVO<UID> context : updatedTOEntity.getContexts()) {
					if (context.isFlagNew()) {
						context.setFieldUid(E.ENTITYCONTEXT.mainEntity, entityUID);
						entityObjectFacadeLocal.insert(context);
					} else if (context.isFlagUpdated()) {
						context.setFieldUid(E.ENTITYCONTEXT.mainEntity, entityUID);
						entityObjectFacadeLocal.update(context, false);
					} else if (context.getPrimaryKey() != null && context.isFlagRemoved()) {
						if (context.getFieldUid(E.ENTITYCONTEXT.mainEntity) != null) {
							entityObjectFacadeLocal.delete(E.ENTITYCONTEXT.UID, context.getPrimaryKey(), false, false);
						}
					}
				}
			}

			// update processes:
			if (updatedTOEntity.getProcesses() != null) {
				for (EntityObjectVO<UID> process : updatedTOEntity.getProcesses()) {
					process.setFieldUid(E.PROCESS.module, entityUID);
					if (process.isFlagNew()) {
						entityObjectFacadeLocal.insert(process);
					} else if (process.isFlagUpdated()) {
						entityObjectFacadeLocal.update(process, false);
					} else if (process.getPrimaryKey() != null && process.isFlagRemoved()) {
						entityObjectFacadeLocal.delete(E.PROCESS.UID, process.getPrimaryKey(), false, false);
					}
				}
			}

			// update menus:
			if (updatedTOEntity.getMenus() != null) {
				final IEntityObjectProcessor<UID> menuProcessor = nucletDalProvider.getEntityObjectProcessor(E.ENTITYMENU);
				for (EntityObjectVO<UID> menu : updatedTOEntity.getMenus().keySet()) {
					if (menu.isFlagNew() || menu.isFlagUpdated()) {
						if (menu.getPrimaryKey() == null || menu.isFlagNew()) {
							menu.flagNew();
							menu.setPrimaryKey(new UID());
							DalUtils.updateVersionInformation(menu, currentUserName);
						}
						menu.setFieldUid(E.ENTITYMENU.entity, entityUID);


						final Map<String, String> locales = updatedTOEntity.getMenus().get(menu);

						String resourceId = menu.getFieldValue(E.ENTITYMENU.menupath);
						for (LocaleInfo li : getLocaleFacade().getAllLocales(false)) {
							resourceId = getLocaleFacade().setResourceForLocale(resourceId, li, locales.get(li.getTag()));
						}
						menu.setFieldValue(E.ENTITYMENU.menupath, resourceId);
						menuProcessor.insertOrUpdate(menu);

					}
					else if (menu.getPrimaryKey() != null && menu.isFlagRemoved()) {
						menuProcessor.delete(new Delete<>(menu.getPrimaryKey()));
						getLocaleFacade().deleteResource(menu.getFieldValue(E.ENTITYMENU.menupath));
					}
				}
			}

			// update trees:
			if (updatedTOEntity.getTreeView() != null) {
				for(EntityTreeViewVO voTreeView : updatedTOEntity.getTreeView()) {
					// recursive update
					updateEntityTreeView(voTreeView, null, currentUserName);
				}
			}

			// Statemodel added -> generic object updates need to be performed AFTER the corresponding system fields are added
			if (isStateModelAdded) {
				// Rebuild the EO processors, because the processor for this EO does not know about the new system fields yet
				// TODO: Don't rebuild all processors, but only the processor responsible for this EO
				nucletDalProvider.invalidate();

				genericObjectFacade.addGenericObjectEntries(entityUID);
			}

			updatedMDEntity.setFields(getFieldMetas(toFields));

			if (BORecompileChecker.isRecompileRequired(oldEntityMeta, updatedMDEntity)) {
				dataBaseHelper.getDbAccess().getDbExecutor().commit();
				metaProvider.revalidate(true, false);
				triggerRecompile();
			}

		} catch (CommonFatalException e) {
			throw e;
		} catch (Exception e) {
			throw new CommonFatalException(e);
		}

		return resultMessage;
	}

	private static void appendSqlStatements(DbAccess dbAccess, DbStructureChange ds, StringBuilder sb) {
		try {
			final IBatch batch = dbAccess.getBatchFor(ds);
			for (String s : dbAccess.getStatementsForLogging(batch)) {
				sb.append(s).append(" ");
			}
		} catch (SQLException e) {
			sb.append("Failed to get SQL statement (")
					.append(ds)
					.append("): ")
					.append(e);
		}
	}

	private List<FieldMeta<?>> getFieldMetas(Collection<FieldMetaTransport> toFields) {
		List<FieldMeta<?>> result = new ArrayList<FieldMeta<?>>();

		for (FieldMetaTransport fmt : toFields) {
			result.add(fmt.getEntityFieldMeta());
		}

		return result;
	}

	/**
	 * Causes a (async) recompilation after the tx commits successfully.
	 */
	private void triggerRecompile() {
		CompileGeneratedCodeTransactionSynchronization transactionSync = transactionSyncs.get();
		if (transactionSync == null) {
			transactionSync =
				new CompileGeneratedCodeTransactionSynchronization(nuclosJarGeneratorManager);
			transactionSyncs.set(transactionSync);
		}
		transactionSync.increaseCallCount();
		TransactionSynchronizationManager.registerSynchronization(transactionSync);
	}


	private EntityMeta createOrUpdateEntityLanguage(NucletEntityMeta updatedEntityMeta, List<FieldMetaTransport> fields, FieldMeta<?> entityFieldPK, MetaDbProvider updatedProvider) throws NuclosBusinessException {

		UID dataLangUID = NucletEntityMeta.getEntityLanguageUID(updatedEntityMeta);
		NucletEntityMeta entityLanguageMeta = null;

		if (MetaProvider.getInstance().hasEntity(dataLangUID)) {
			// Language data table already exists
			EntityMeta existingEntityMeta = MetaProvider.getInstance().getEntity(updatedEntityMeta.getUID());
			EntityMeta metaTemp = MetaProvider.getInstance().getEntity(dataLangUID);
			entityLanguageMeta = (NucletEntityMeta) metaTemp;

			// First if the db-table name of the origin table has changed we must update the
			// lang data table as well			
			if (updatedEntityMeta.isFlagUpdated() && !existingEntityMeta.getDbTable().equals(updatedEntityMeta.getDbTable())) {
				// insert new data lang table name into model
				entityLanguageMeta.setDbTable(NucletEntityMeta.getEntityLanguageDBTablename(updatedEntityMeta));
				// validate and adjust new name if necessary
				nucletDalProvider.getDataLanguageMetaDataProcessor().validateDBTableName(entityLanguageMeta);
				// store data lang table name in origin entity meta model and update entry in db

				updatedEntityMeta.setDataLanguageDBTable(entityLanguageMeta.getDbTable());
				updatedEntityMeta.flagUpdate();
				nucletDalProvider.getEntityMetaDataProcessor().insertOrUpdate(updatedEntityMeta);
				entityLanguageMeta.flagUpdate();
			}
			// adjust fields
			updateStateOfFields(entityLanguageMeta, updatedEntityMeta, fields);

			// update entity language table
			Map<UID, FieldMeta<?>> lstFields = new HashMap<UID, FieldMeta<?>>();
			for (FieldMetaTransport field : fields) {
				lstFields.put(field.getEntityFieldMeta().getUID(), field.getEntityFieldMeta());
			}
			nucletDalProvider.getDataLanguageMetaDataProcessor().modify(entityLanguageMeta, lstFields);
		} else {

			List<FieldMeta<?>> lstFields = new ArrayList<FieldMeta<?>> ();
			for (FieldMetaTransport field : fields) {
				lstFields.add(field.getEntityFieldMeta());
			}
			((NucletEntityMeta)updatedEntityMeta).setFields(lstFields);
			// language Entity does not exist yet
			entityLanguageMeta = DataLanguageServerUtils.createEntityLanguageMeta(updatedEntityMeta, entityFieldPK);
			// validate and adjust new name if necessary
			nucletDalProvider.getDataLanguageMetaDataProcessor().validateDBTableName(entityLanguageMeta);
			// store data lang table name in origin entity meta model and update entry in db
			updatedEntityMeta = nucletDalProvider.getEntityMetaDataProcessor().getByPrimaryKey(updatedEntityMeta.getUID());
			updatedEntityMeta.setDataLanguageDBTable(entityLanguageMeta.getDbTable());
			updatedEntityMeta.flagUpdate();
			nucletDalProvider.getEntityMetaDataProcessor().insertOrUpdate(updatedEntityMeta);

			// create new entity language table because it does not exist yet
			nucletDalProvider.getDataLanguageMetaDataProcessor().create(entityLanguageMeta, updatedProvider);
		}

		return entityLanguageMeta;
	}

	private void updateStateOfFields(NucletEntityMeta dataLangEntityMeta,
			EntityMetaVO updatedEntityMeta, List<FieldMetaTransport> fields) {

		for (FieldMetaTransport field : fields) {

			UID curDLFieldUID = DataLanguageServerUtils.extractFieldUID(field.getEntityFieldMeta().getUID());
			UID curDLFlaggedFieldUID = DataLanguageServerUtils.extractFlaggedFieldFromLangField(curDLFieldUID);

			NucletFieldMeta<?> curFieldMetaUpdatedEntity = field.getEntityFieldMeta();

			if (curFieldMetaUpdatedEntity.getCalcFunction() == null) {
				if (curFieldMetaUpdatedEntity.isFlagNew()) {
					if (curFieldMetaUpdatedEntity.isLocalized()) {
						if (dataLangEntityMeta.hasField(curDLFieldUID)) {
							throw new CommonFatalException("Data language field " + dataLangEntityMeta.getField(curDLFieldUID).getFieldName() + " already exists.");
						}
						List<FieldMeta<?>> lstFields =
								new ArrayList<FieldMeta<?>> (dataLangEntityMeta.getFields());
						lstFields.add(
								DataLanguageServerUtils.extractFieldValue(curFieldMetaUpdatedEntity, updatedEntityMeta));
						lstFields.add(
								DataLanguageServerUtils.extractFlaggedFieldValue(curFieldMetaUpdatedEntity, updatedEntityMeta));

						dataLangEntityMeta.setFields(lstFields);
						dataLangEntityMeta.flagUpdate();
					}
				}
				else if (curFieldMetaUpdatedEntity.isFlagUpdated()) {

					NucletFieldMeta existFieldMeta = (NucletFieldMeta) MetaProvider.getInstance().getEntityField(curFieldMetaUpdatedEntity.getUID());

					if (curFieldMetaUpdatedEntity.isLocalized()) {
						if (!existFieldMeta.isLocalized()) {
							// the updated field is now localized; we have to create a new field in the data lang table
							List<FieldMeta<?>> lstFields =
									new ArrayList<FieldMeta<?>> (dataLangEntityMeta.getFields());
							NucletFieldMeta newDataLangField =
									DataLanguageServerUtils.extractFieldValue(curFieldMetaUpdatedEntity, updatedEntityMeta);

							// set new valid db column and store it in the entity field meta info
							newDataLangField.setDbColumn(NucletFieldMeta.getLanguageFieldDBColumName(curFieldMetaUpdatedEntity));
							nucletDalProvider.getDataLanguageMetaDataProcessor().validateDBColumnName(dataLangEntityMeta.getDbTable(), newDataLangField);
							curFieldMetaUpdatedEntity.setDataLangDBColumn(newDataLangField.getDbColumn());
							nucletDalProvider.getEntityFieldMetaDataProcessor().insertOrUpdate(curFieldMetaUpdatedEntity);

							NucletFieldMeta newDataLangFlaggedField =
									DataLanguageServerUtils.extractFlaggedFieldValue(curFieldMetaUpdatedEntity, updatedEntityMeta);

							lstFields.add(newDataLangField);
							lstFields.add(newDataLangFlaggedField);

							dataLangEntityMeta.setFields(lstFields);
							dataLangEntityMeta.flagUpdate();
						} else {
							NucletFieldMeta existDatLangField = (NucletFieldMeta) dataLangEntityMeta.getField(curDLFieldUID);
							NucletFieldMeta existDatLangFlaggedField = (NucletFieldMeta) dataLangEntityMeta.getField(curDLFlaggedFieldUID);

							// If new updated field has a different column name that does not match to the 
							// existing data lang column name then we have to update the data lang column
							if (!curFieldMetaUpdatedEntity.getDbColumn().equals(existDatLangField.getDbColumn())) {
								// insert new data lang column name into model
								existDatLangField.setDbColumn(NucletFieldMeta.getLanguageFieldDBColumName(curFieldMetaUpdatedEntity));
								existDatLangFlaggedField.setDbColumn(NucletFieldMeta.getLanguageFieldDBColumName(curFieldMetaUpdatedEntity, true));
								// validate and adjust new name if necessary
								nucletDalProvider.getDataLanguageMetaDataProcessor().validateDBColumnName(dataLangEntityMeta.getDbTable(), existDatLangField);
								nucletDalProvider.getDataLanguageMetaDataProcessor().validateDBColumnName(dataLangEntityMeta.getDbTable(), existDatLangFlaggedField);

								// store data lang column name in origin field meta model and update field in db
								curFieldMetaUpdatedEntity.setDataLangDBColumn(existDatLangField.getDbColumn());
								nucletDalProvider.getEntityFieldMetaDataProcessor().insertOrUpdate(curFieldMetaUpdatedEntity);
								existDatLangField.flagUpdate();
								existDatLangFlaggedField.flagUpdate();
							}
						}
					} else {
						if (existFieldMeta.isLocalized()) {
							NucletFieldMeta existDatLangField = (NucletFieldMeta) dataLangEntityMeta.getField(curDLFieldUID);
							NucletFieldMeta existDatLangFlaggedField = (NucletFieldMeta) dataLangEntityMeta.getField(curDLFlaggedFieldUID);
							existDatLangField.flagRemove();
							existDatLangFlaggedField.flagRemove();
						}
					}
				} else if(curFieldMetaUpdatedEntity.isFlagRemoved()) {
					if (dataLangEntityMeta.hasField(curDLFieldUID)) {
						NucletFieldMeta existDatLangField = (NucletFieldMeta) dataLangEntityMeta.getField(curDLFieldUID);
						NucletFieldMeta existDatLangFlaggedField = (NucletFieldMeta) dataLangEntityMeta.getField(curDLFlaggedFieldUID);
						existDatLangField.flagRemove();
						existDatLangFlaggedField.flagRemove();
					}
				}
			}
		}
	}

	private void updateEntityTreeView(final EntityTreeViewVO voTreeView, final UID uidParent, final String user) {

		final DbMap m = new DbMap();
		// EntityTreeViewVO specific fields
		m.put(E.ENTITYSUBNODES.field, voTreeView.getField());
		m.put(E.ENTITYSUBNODES.entity, voTreeView.getEntity());
		m.put(E.ENTITYSUBNODES.originentityid, voTreeView.getOriginEntity());
		// if(voTreeView.getFoldername() != null)
		m.put(E.ENTITYSUBNODES.foldername, voTreeView.getFoldername());
		m.put(E.ENTITYSUBNODES.active, voTreeView.isActive());
		m.put(E.ENTITYSUBNODES.sortOrder, voTreeView.getSortOrder());

		m.put(E.ENTITYSUBNODES.inheritSubNodes, voTreeView.getIsInheritNodes());
		m.put(E.ENTITYSUBNODES.fieldRoot, voTreeView.getFieldRoot());
		m.put(E.ENTITYSUBNODES.parentSubNode, uidParent);
		m.put(E.ENTITYSUBNODES.node, voTreeView.getNode());
		m.put(E.ENTITYSUBNODES.nodeTooltip, voTreeView.getNodeTooltip());
		m.put(E.ENTITYSUBNODES.groupBy, voTreeView.getGroupBy());
		m.put(E.ENTITYSUBNODES.isShowEntityFolder, voTreeView.getIsShowEntityFolder());

		UID intidNode = null;
		voTreeView.makeConsistent();

		if (voTreeView.isFlagNew()) {
			intidNode = new UID();
			assert intidNode != null;
			// Standard nuclos fields
			m.put(E.ENTITYSUBNODES.getPk(), intidNode);
			m.put(SF.CREATEDAT,new InternalTimestamp(System.currentTimeMillis()));
			m.put(SF.CREATEDBY, user);
			m.put(SF.CHANGEDAT, new InternalTimestamp(System.currentTimeMillis()));
			m.put(SF.CHANGEDBY, user);
			m.put(SF.VERSION, 1);

			dataBaseHelper.getDbAccess().execute(new DbInsertStatement<UID>(E.ENTITYSUBNODES.getDbTable(), DbNull.escapeNull(m)));
		} else {
			intidNode = voTreeView.getPrimaryKey();
			assert intidNode != null;
			DbMap conditionMap = new DbMap();
			conditionMap.put(E.ENTITYSUBNODES.getPk(), intidNode);
			if (voTreeView.isFlagUpdated()) {
				m.put(E.ENTITYSUBNODES.getPk(), intidNode);
				m.put(SF.CHANGEDAT, new InternalTimestamp(System.currentTimeMillis()));
				m.put(SF.CHANGEDBY, user);
				m.put(SF.VERSION, DbIncrement.INCREMENT);
				try {
					EntityObjectVO<UID> originNode = nucletDalProvider.getEntityObjectProcessor(E.ENTITYSUBNODES).getByPrimaryKey(intidNode);
					if (RigidUtils.equal(originNode.getPrimaryKey(), intidNode) &&
						RigidUtils.equal(originNode.getFieldUid(E.ENTITYSUBNODES.field), voTreeView.getField()) &&
						RigidUtils.equal(originNode.getFieldUid(E.ENTITYSUBNODES.entity), voTreeView.getEntity()) &&
						RigidUtils.equal(originNode.getFieldUid(E.ENTITYSUBNODES.originentityid), voTreeView.getOriginEntity()) &&
						RigidUtils.equal(originNode.getFieldValue(E.ENTITYSUBNODES.foldername), voTreeView.getFoldername()) &&
						RigidUtils.equal(originNode.getFieldValue(E.ENTITYSUBNODES.active), voTreeView.isActive()) &&
						RigidUtils.equal(originNode.getFieldValue(E.ENTITYSUBNODES.sortOrder), voTreeView.getSortOrder()) &&
						RigidUtils.equal(originNode.getFieldValue(E.ENTITYSUBNODES.inheritSubNodes), voTreeView.getIsInheritNodes()) &&
						RigidUtils.equal(originNode.getFieldUid(E.ENTITYSUBNODES.fieldRoot), voTreeView.getFieldRoot()) &&
						RigidUtils.equal(originNode.getFieldUid(E.ENTITYSUBNODES.parentSubNode), uidParent) &&
						RigidUtils.equal(originNode.getFieldValue(E.ENTITYSUBNODES.node), voTreeView.getNode()) &&
						RigidUtils.equal(originNode.getFieldValue(E.ENTITYSUBNODES.nodeTooltip), voTreeView.getNodeTooltip()) &&
						RigidUtils.equal(originNode.getFieldValue(E.ENTITYSUBNODES.isShowEntityFolder), voTreeView.getIsShowEntityFolder())) {
						//for nuclet export / import
						m.put(SF.VERSION, originNode.getVersion());
					}
				} catch (Exception ex) {
					LOG.warn("Error during field equality check for subnode with UID {} Error: {}",
					         intidNode, ex.getMessage());
				}
				dataBaseHelper.getDbAccess().execute(new DbUpdateStatement<UID>(E.ENTITYSUBNODES.getDbTable(), DbNull.escapeNull(m), conditionMap));
			} else if (voTreeView.isFlagRemoved()) {
				if (null != intidNode) {
					final List<EntityTreeViewVO> lstChildren = new ArrayList<>();

					// "delete on cascade" not supported by mssql, therefore simulate it here
					buildReverseChildNodeOrder(voTreeView, lstChildren);

					// delete dependent child nodes
					for (final EntityTreeViewVO etvVO : lstChildren) {
						// flag as removed
						etvVO.flagRemove();
						if (null != etvVO.getPrimaryKey()) {
							conditionMap.put(E.ENTITYSUBNODES.getPk(), etvVO.getPrimaryKey());
							dataBaseHelper.getDbAccess().execute(new DbDeleteStatement(E.ENTITYSUBNODES.getDbTable(), conditionMap));
						}

					}
					// delete node
					conditionMap.put(E.ENTITYSUBNODES.getPk(), intidNode);
					dataBaseHelper.getDbAccess().execute(new DbDeleteStatement(E.ENTITYSUBNODES.getDbTable(), conditionMap));
				}
			}
		}
		for (final EntityTreeViewVO childVO : voTreeView.getChildNodes()) {
			updateEntityTreeView(childVO, intidNode, user);
		}
		// invalidate caches
		treeProvider.invalidate();
	}



	private void buildReverseChildNodeOrder(final EntityTreeViewVO voTreeView, final List<EntityTreeViewVO> lstChildren) {
		for (final EntityTreeViewVO etvVO : voTreeView.getChildNodes()) {
			if (!etvVO.getChildNodes().isEmpty()) {
				buildReverseChildNodeOrder(etvVO, lstChildren);

			}
			lstChildren.add(etvVO);
		}

	}

	private void rollBackDBChanges(EntityMetaTransport updatedTOEntity,
		List<FieldMetaTransport> toFields) {
		NucletEntityMeta updatedMDEntity = updatedTOEntity.getEntityMetaVO();

		MetaDbHelper dbHelperIst = new MetaDbHelper(E.getSchemaHelperVersion(), dataBaseHelper.getDbAccess(), MetaProvider.getInstance(), null, null);
		DbTable tableIst = dbHelperIst.getDbTable(updatedMDEntity);

		StaticMetaDataProvider staticMetaData = new StaticMetaDataProvider(E.getThis());
		staticMetaData.addClone(updatedMDEntity, false);
		List<FieldMeta<?>> lstFields = new ArrayList<>();

		Collection<FieldMeta<?>> lstSystemFields = new ArrayList<>();
		DalUtils.addStaticFields(lstSystemFields, updatedMDEntity);
		for(FieldMetaTransport to : toFields) {
			lstFields.add(to.getEntityFieldMeta());
			staticMetaData.addClone(to.getEntityFieldMeta());
			if(to.getEntityFieldMeta().getForeignEntity() != null) {
				staticMetaData.addClone(MetaProvider.getInstance().getEntity(to.getEntityFieldMeta().getForeignEntity()), true);
			}
		}
		for(FieldMeta<?> voSystemField : lstSystemFields) {
			staticMetaData.addClone(voSystemField);
			if(voSystemField.getForeignEntity() != null) {
				staticMetaData.addClone(MetaProvider.getInstance().getEntity(voSystemField.getForeignEntity()), true);
			}
		}
		MetaDbHelper dbHelperSoll = new MetaDbHelper(E.getSchemaHelperVersion(), dataBaseHelper.getDbAccess(), staticMetaData, null, null);
		DbTable tableSoll = dbHelperSoll.getDbTable(updatedMDEntity);

		List<DbStructureChange> lstStructureChanges = null;

		if(updatedMDEntity.getUID() != null) {
			lstStructureChanges = SchemaUtils.modify(tableSoll, tableIst, false);
		}
		else {
			lstStructureChanges = new ArrayList<>();
		}

		for(DbStructureChange ds : lstStructureChanges) {
			try {
				dataBaseHelper.getDbAccess().execute(ds);
			}
			catch(DbException e) {
				// ignore
				LOG.info("rollBackDBChanges: {}", e);
			}
		}
	}

	private void setSystemValuesAndParent(List<NucletFieldMeta<?>> lstFields, EntityMeta<?> voParent) {
		for(NucletFieldMeta<?> voField : lstFields) {
			voField.setEntity(voParent.getUID());
			DalUtils.updateVersionInformation(voField,getCurrentUserName());
		}
	}

	private DalCallResult insertOrUpdateEntityFieldMetaData(List<FieldMetaTransport> lstFields) {
		// first remove fields
		for(FieldMetaTransport vo : lstFields) {
			NucletFieldMeta<?> v = vo.getEntityFieldMeta();
			DalUtils.updateVersionInformation(v, getCurrentUserName());
			if(v.isFlagRemoved() && v.getPrimaryKey() != null) {
				// NUCLOSINT-714: remove dependants generation attributes
				dataBaseHelper.getDbAccess().execute(DbStatementUtils.deleteFrom(E.GENERATIONATTRIBUTE,	E.GENERATIONATTRIBUTE.attributeSource, v.getPrimaryKey()));
				dataBaseHelper.getDbAccess().execute(DbStatementUtils.deleteFrom(E.GENERATIONATTRIBUTE, E.GENERATIONATTRIBUTE.attributeTarget, v.getPrimaryKey()));
				dataBaseHelper.getDbAccess().execute(DbStatementUtils.deleteFrom(E.GENERATIONSUBENTITYATTRIBUTE, E.GENERATIONSUBENTITYATTRIBUTE.subentityAttributeSource, v.getPrimaryKey()));
				dataBaseHelper.getDbAccess().execute(DbStatementUtils.deleteFrom(E.GENERATIONSUBENTITYATTRIBUTE, E.GENERATIONSUBENTITYATTRIBUTE.subentityAttributeTarget, v.getPrimaryKey()));
				dataBaseHelper.getDbAccess().execute(DbStatementUtils.deleteFrom(E.IMPORTATTRIBUTE, E.IMPORTATTRIBUTE.attribute, v.getPrimaryKey()));
				dataBaseHelper.getDbAccess().execute(DbStatementUtils.deleteFrom(E.IMPORTIDENTIFIER, E.IMPORTIDENTIFIER.attribute, v.getPrimaryKey()));
				NucletDalProvider.getInstance().getEntityFieldMetaDataProcessor().delete(
					new Delete<>(v.getPrimaryKey()));
			}
			else
				continue;
		}

		for(FieldMetaTransport vo : lstFields) {
			NucletFieldMeta<?> v = vo.getEntityFieldMeta();
			DalUtils.updateVersionInformation(v, getCurrentUserName());
			if (!v.isFlagNew() && !SF.isEOField(v.getEntity(), v.getUID())) {
				try {
					NucletFieldMeta<?> originV = NucletDalProvider.getInstance().getEntityFieldMetaDataProcessor().getByPrimaryKey(v.getUID());
					if (v.equalAll(originV)) {
						// for nuclet export / import
						v.setVersion(originV.getVersion());
					}
				} catch (Exception ex) {
					LOG.warn("Error during field equality check for field with UID {} Error: {}",
					         v.getUID(), ex.getMessage());
				}
			}
			if(v.isFlagRemoved()) {
				continue;
			}
			else {
				NucletDalProvider.getInstance().getEntityFieldMetaDataProcessor().insertOrUpdate(v);
			}
		}

		return null;
	}

	@Override
    public List<String> getDBTables() {
		return new ArrayList<String>(dataBaseHelper.getDbAccess().getTableNames(DbTableType.TABLE));
	}

	/**
	 * @return Script (with results if selected)
	 */
	@Override
    @RolesAllowed("Login")
	public Map<String, MasterDataVO<?>> getColumnsFromTable(String sTable) {
		Map<String, MasterDataVO<?>> mp = new HashMap<String, MasterDataVO<?>>();
		DbTable table = dataBaseHelper.getDbAccess().getTableMetaData(sTable);
		for (DbColumn column : table.getTableColumns()) {
			MasterDataVO<UID> vo = new MasterDataVO<UID>(E.DATATYPE, false);
			DbColumnType type = column.getColumnType();
			String javatyp = "java.lang.String";
			String name = "Text";
			int scale = 0, precision = 0;
			boolean blnAddColumn = true;
			if (type.getGenericType() != null) {
				javatyp = type.getGenericType().getPreferredJavaType().getName();
				switch (type.getGenericType()) {
				case NUMERIC:
					name = "Kommazahl";
					scale = (type.getPrecision() != null ? type.getPrecision() : 0);
					precision = (type.getScale() != null ? type.getScale() : 0);
					// Nuclos maps integer to number(x,0), hence we map these back to Integer
					if (precision == 0) {
						name = "Ganzzahl";
						javatyp = scale <= 9 ? "java.lang.Integer" : "java.lang.Long";
					}
					break;
				case BOOLEAN:
					name = "Boolean";
					break;
				case VARCHAR:
					name = "Text";
					scale = (type.getLength() != null ? type.getLength() : 0);
					break;
				case DATE:
				case DATETIME:
					name = "Datum";
					break;
				default:
					// Column type nuclos don't supported
					blnAddColumn = false;
					break;
				}
			}
			if(blnAddColumn) {
				vo.setFieldValue(E.DATATYPE.name, name);
				vo.setFieldValue(E.DATATYPE.javatyp, javatyp);
				vo.setFieldValue(E.DATATYPE.scale, scale);
				vo.setFieldValue(E.DATATYPE.precision, precision);

				mp.put(column.getColumnName(), vo);
			}
		}
		return mp;
	}

	/**
	 * @return Script (with results if selected)
	 */
	@Override
    @RolesAllowed("Login")
	public List<String> getTablesFromSchema(String url, String user, String password, String schema) {
		List<String> lstTables = new ArrayList<String>();
		Connection connect = null;
		try {
			connect = DriverManager.getConnection(url, user, password);
			DatabaseMetaData dbmeta = connect.getMetaData();
			ResultSet rsTables = dbmeta.getTables(null, schema.toUpperCase(), "%", new String [] {"TABLE"});
			while(rsTables.next()) {
				lstTables.add(rsTables.getString("TABLE_NAME"));
			}
			rsTables.close();
		}
		catch(SQLException e) {
			throw new CommonFatalException(e);
		}
		finally {
			if(connect != null)
				try {
					connect.close();
				}
				catch(SQLException e) {
					// do noting here
					LOG.info("getTablesFromSchema: {}", e);
				}
		}

		return lstTables;
	}

	/**
	 * @return Script (with results if selected)
	 */
	@Override
    @RolesAllowed("Login")
	public List<MasterDataVO<UID>> transformTable(String url, String user, String password, String schema, String table) {

		List<MasterDataVO<UID>> lstFields = new ArrayList<MasterDataVO<UID>>();

		Connection connect = null;
		try {
			connect = DriverManager.getConnection(url, user, password);
			DatabaseMetaData dbmeta = connect.getMetaData();
			ResultSet rsCols = dbmeta.getColumns(null, schema.toUpperCase(), table, "%");
			while(rsCols.next()) {
				String colName = rsCols.getString("COLUMN_NAME");
				int colsize = rsCols.getInt("COLUMN_SIZE");
				int postsize = rsCols.getInt("DECIMAL_DIGITS");
				int columsType = rsCols.getInt("DATA_TYPE");
				String sJavaType = getBestJavaType(columsType);
				if(postsize > 0)
					sJavaType = "java.lang.Double";

				MasterDataVO<UID> mdFieldVO = new MasterDataVO<UID>(E.ENTITYFIELD, true);
				mdFieldVO.setFieldValue(E.ENTITYFIELD.datascale, colsize);
				mdFieldVO.setFieldValue(E.ENTITYFIELD.localeresourcel, org.apache.commons.lang.StringUtils.capitalize(colName.toLowerCase()));
				mdFieldVO.setFieldValue(E.ENTITYFIELD.nullable, Boolean.TRUE);
				mdFieldVO.setFieldValue(E.ENTITYFIELD.dataprecision, postsize);
				mdFieldVO.setFieldValue(E.ENTITYFIELD.dbfield, colName.toLowerCase());
				mdFieldVO.setFieldValue(E.ENTITYFIELD.localeresourced, org.apache.commons.lang.StringUtils.capitalize(colName.toLowerCase()));
				mdFieldVO.setFieldValue(E.ENTITYFIELD.field, colName.toLowerCase());
				mdFieldVO.setFieldValue(E.ENTITYFIELD.datatype, sJavaType);
				lstFields.add(mdFieldVO);
			}
			rsCols.close();
		}
		catch(Exception e) {
			LOG.info("transformTable: {}", e, e);
		}
		finally {
			try {
				if (connect != null) {
					connect.close();
				}
			}
			catch(Exception e) {
				LOG.info("transformTable: {}", e, e);
			}
		}
		return lstFields;
	}

	/**
	 * @return Script (with results if selected)
	 */
	@Override
    @RolesAllowed("Login")
	public EntityMeta<?> transferTable(String url, String user, String password, String schema, String table, UID entityUID) {

		EntityMeta<?> metaNew = null;

		Connection connect = null;
		try {
			List<MasterDataVO<UID>> tableFields = transformTable(url, user, password, schema, table);

			connect = DriverManager.getConnection(url, user, password);

			metaNew = MetaProvider.getInstance().getEntity(entityUID);

			String sqlSelect = "select * from " + schema + "." + table;
			Statement stmt = connect.createStatement();
			ResultSet rsSelect =  stmt.executeQuery(sqlSelect);
			while(rsSelect.next()) {
				List<Object> lstValues = new ArrayList<Object>();
				for(MasterDataVO<UID> field : tableFields) {
					lstValues.add(rsSelect.getObject(field.getFieldValue(E.ENTITYFIELD.dbfield)));
				}

				StringBuffer sb = new StringBuffer();
				sb.append("insert into " + metaNew.getDbTable());
				sb.append(" values(?");
				for(int i = 0; i < lstValues.size(); i++) {
					sb.append(",?");
				}
				sb.append(",?,?,?,?,?)");

				int col = 1;
				PreparedStatement pst = dataSource.getConnection().prepareStatement(sb.toString());
				pst.setLong(col++, dataBaseHelper.getNextIdAsLong(SpringDataBaseHelper.DEFAULT_SEQUENCE));
				for(Object object : lstValues) {
					pst.setObject(col++, object);
				}
				pst.setDate(col++, new java.sql.Date(System.currentTimeMillis()));
				pst.setString(col++, "Wizard");
				pst.setDate(col++, new java.sql.Date(System.currentTimeMillis()));
				pst.setString(col++, "Wizard");
				pst.setInt(col++, 1);

				pst.executeUpdate();
				pst.close();

			}
			rsSelect.close();
			stmt.close();

		}
		catch(SQLException e) {
			LOG.info("transferTable: {}", e, e);
		}
		finally {
			if(connect != null)
				try {
					connect.close();
				}
				catch(SQLException e) {
					// do noting here
					LOG.info("transferTable: {}", e);
				}
		}
		return metaNew;
	}

	private String getBestJavaType(int colType) {
		String sType = "java.lang.String";
		switch(colType) {
		case Types.VARCHAR:
			return sType;
		case Types.CHAR:
			return sType;
		case Types.NCHAR:
			return sType;
		case Types.NVARCHAR:
			return sType;
		case Types.LONGNVARCHAR:
			return sType;
		case Types.LONGVARCHAR:
			return sType;
		case Types.LONGVARBINARY:
			return sType;
		case Types.NUMERIC:
			return "java.lang.Integer";
		case Types.DECIMAL:
			return "java.lang.Double";
		case Types.BOOLEAN:
			return "java.lang.Integer";
		case Types.DATE:
			return "java.util.Date";
		case Types.TIME:
			return "java.util.Date";
		case Types.TIMESTAMP:
			return "java.util.Date";

		default:

			return sType;
		}
	}

	@Override
    @RolesAllowed("Login")
	public EntityRelationshipModelVO getEntityRelationshipModelVO(MasterDataVO<UID> vo) {
		return MasterDataWrapper.getEntityRelationshipModelVO(vo);
	}

	/**
	 * force to change internal entity name
	 */
	@Override
    @RolesAllowed("Login")
	public boolean isChangeDatabaseColumnToNotNullableAllowed(UID field) {
		FieldMeta<?> fieldMeta = metaProvider.getEntityField(field);
		EntityMeta<?> entityMeta = metaProvider.getEntity(fieldMeta.getEntity());

		try {
			// @TODO GOREF: maybe this should be delegated to the (JDBC)-EntityObjectProcessor ?
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<Long> query = builder.createQuery(Long.class);
			DbFrom t = query.from(entityMeta);
			DbColumnExpression<?> c = t.baseColumn(fieldMeta);
			query.select(builder.countRows());
			query.where(c.isNull());

			Long count = dataBaseHelper.getDbAccess().executeQuerySingleResult(query);
			return count == 0L;
		}
		catch(Exception e) {
			LOG.info("isChangeDatabaseColumnToNotNullableAllowed: {}", e);
			return false;
		}
	}

	/**
	 * force to change internal entity name
	 */
	@Override
    @RolesAllowed("Login")
	public boolean isChangeDatabaseColumnToUniqueAllowed(UID field) {
		FieldMeta<?> fieldMeta = metaProvider.getEntityField(field);
		EntityMeta<?> entityMeta = metaProvider.getEntity(fieldMeta.getEntity());

		List<FieldMeta<?>> uniqueFields = new ArrayList<FieldMeta<?>>(Collections.singleton(fieldMeta));
		for (FieldMeta<?> fm : entityMeta.getFields()) {
			if (fm.isUnique() && !uniqueFields.contains(fm)) {
				uniqueFields.add(fm);
			}
		}

		try {
			// @TODO GOREF: maybe this should be delegated to the (JDBC)-EntityObjectProcessor ?
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<Long> query = builder.createQuery(Long.class);
			DbFrom<?> t = query.from(entityMeta);
			query.select(builder.countRows());

			List<DbExpression<?>> lstColumns = new ArrayList<DbExpression<?>>();

			for (FieldMeta<?> fm : uniqueFields) {
				String sColumn;
				// We don't need a join to the referenced field here, instead we simply need th
				// (raw) field name in the database. (tp)
				if (fm.getForeignEntity() != null && fm.getForeignEntityField() != null) {
					sColumn = MetaDbHelper.getDbRefColumn(metaProvider.getEntity(fm.getForeignEntity()), fm);
				}
				else {
					sColumn = fm.getDbColumn();
				}

				DbColumnExpression<?> c = t.baseColumn(SimpleDbField.create(sColumn, DbUtils.getDbType(fm.getDataType())));
				lstColumns.add(c);
			}

			query.groupBy(lstColumns);
			query.having(builder.greaterThan(builder.countRows(), builder.literal(1L)));
			query.limit(2L);

			List<Long> result = dataBaseHelper.getDbAccess().executeQuery(query);
			return result.isEmpty();
		}
		catch(Exception e) {
			LOG.error("isChangeDatabaseColumnToUniqueAllowed: {}", e, e);
			return false;
		}
	}

	@Override
	@RolesAllowed("Login")
	public Collection<MasterDataVO<UID>> hasEntityFieldInImportStructure(UID field) {
		FieldMeta<?> fieldMeta = metaProvider.getEntityField(field);

		CollectableComparison comp = SearchConditionUtils.newUidComparison(E.IMPORT.entity, ComparisonOperator.EQUAL, fieldMeta.getEntity());
		Collection<MasterDataVO<UID>> colImportStructure = masterDataFacade.getMasterData(E.IMPORT, comp);
		for(MasterDataVO<UID> voImportStructure : colImportStructure) {

			CollectableComparison compAttribute1 = SearchConditionUtils.newUidComparison(E.IMPORTATTRIBUTE.importfield, ComparisonOperator.EQUAL, voImportStructure.getPrimaryKey());
			CollectableComparison compAttribute2 = SearchConditionUtils.newUidComparison(E.IMPORTATTRIBUTE.attribute, ComparisonOperator.EQUAL, field);
			CompositeCollectableSearchCondition search = SearchConditionUtils.and(compAttribute1, compAttribute2);
			Collection<MasterDataVO<UID>> colImportAttribute = masterDataFacade.getMasterData(E.IMPORTATTRIBUTE, search);
			if(colImportAttribute.size() > 0)
				return colImportStructure;
		}

		return new ArrayList<MasterDataVO<UID>>();
	}

	@Override
	@RolesAllowed("Login")
	public boolean hasEntityLayout(UID entity) {
		DbQuery<DbTuple> query = dataBaseHelper.getDbAccess().getQueryBuilder().createTupleQuery();
		DbFrom from = query.from(E.LAYOUTUSAGE);
		List<DbSelection<?>> columns = new ArrayList<DbSelection<?>>();

		columns.add(from.baseColumn(E.LAYOUTUSAGE.getPk()).alias("INTID"));
		columns.add(from.baseColumn(E.LAYOUTUSAGE.entity).alias("STRENTITY"));
		query.multiselect(columns);
		query.where(dataBaseHelper.getDbAccess().getQueryBuilder().equalValue(from.baseColumn(E.LAYOUTUSAGE.entity), entity));

		List<DbTuple> count = dataBaseHelper.getDbAccess().executeQuery(query);

		return count.size() > 0;
	}


    @RolesAllowed("Login")
	public EntityMeta<?> getEntityMeta(UID entityUID) {
		return metaProvider.getEntity(entityUID);
	}

	@Override
	@RolesAllowed("Login")
    public void invalidateServerMetadata() {
		metaProvider.revalidate(true, true);
    }

	@Override
	@RolesAllowed("Login")
	public List<String> getVirtualEntities() {
		List<String> result = new ArrayList<String>();
		result.addAll(dataBaseHelper.getDbAccess().getTableNames(DbTableType.TABLE));
		result.addAll(dataBaseHelper.getDbAccess().getTableNames(DbTableType.VIEW));

		CollectableSearchCondition cond = SearchConditionUtils.newComparison(E.DBOBJECT.dbobjecttype, ComparisonOperator.EQUAL, DbObjectType.VIEW.getName());
		List<EntityObjectVO<UID>> dbObjectViews = nucletDalProvider.getEntityObjectProcessor(E.DBOBJECT).getBySearchExpression(new CollectableSearchExpression(cond));

		Iterator<String> it = result.iterator();
		while (it.hasNext()) {
			String sResult = it.next();
			boolean isVirtual = false;
			for (EntityObjectVO<UID> dbObjectView : dbObjectViews) {
				if (sResult.equalsIgnoreCase(dbObjectView.getFieldValue(E.DBOBJECT.name))) {
					isVirtual = true;
					break;
				}
			}
			if (!isVirtual) {
				it.remove();
			}
		}

		return result;
	}

	@Override
	@RolesAllowed("Login")
	public List<String> getPossibleIdFactories() {
		return metaProvider.getPossibleIdFactories();
	}

	@Override
	@RolesAllowed("Login")
	public List<FieldMeta<?>> getVirtualEntityFields(String virtualentity) {
		List<FieldMeta<?>> result = new ArrayList<FieldMeta<?>>();
		DbTable tableMetaData = dataBaseHelper.getDbAccess().getTableMetaData(virtualentity);

		for (DbColumn column : tableMetaData.getTableColumns()) {
			if (SF.MANDATOR_UID.getDbColumn().equalsIgnoreCase(column.getColumnName())) {
				// ignore mandator uid column
				continue;
			}
			NucletFieldMeta<?> field = DalUtils.getFieldMeta(column);
			field.setDbColumn(column.getColumnName().toUpperCase());
			field.setFieldName(field.getFieldName().toLowerCase());
			field.setFallbackLabel(field.getFieldName());
			result.add(field);
		}
		return result;
	}

	@Override
	public void tryVirtualEntitySelect(EntityMeta<?> virtualentity) throws NuclosBusinessException {
		IEntityObjectProcessor<?> processor = getProcessorFactory().newEntityObjectProcessor(virtualentity, new ArrayList<FieldMeta<?>>(), true);
		try {
			processor.getBySearchExpression(new CollectableSearchExpression(new CollectableIdCondition(0L)));
		}
		catch (Exception ex) {
			LOG.error("Unable to try virtual entity select: ", ex);
			throw new NuclosBusinessException(StringUtils.getParameterizedExceptionMessage("MetaDataFacade.tryVirtualEntitySelect.error", ex.getMessage()));
		}
	}

	@Override
	public void tryRemoveProcess(EntityObjectVO<?> process) throws NuclosBusinessException {
		try {
			NucletDalProvider.getInstance().getEntityObjectProcessor(E.PROCESS).delete(new Delete(process.getId()));
		}
		catch (DbException e) {
			throw new NuclosBusinessException("tryRemoveProcess failed", e);
		}
		TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
	}

	@Override
	public List<EntityObjectVO<UID>> getEntityMenus() {
		return NucletDalProvider.getInstance().getEntityObjectProcessor(E.ENTITYMENU).getAll();
	}

	@Override
	public Collection<EntityMeta<?>> getSystemMetaData() {
		return E.getAllEntities();
	}

	@Override
	public Map<UID, LafParameterMap> getLafParameters() {
		return MetaProvider.getInstance().getAllLafParameters();
	}

	@Override
	public <PK> EntityMeta<PK> getEntity(UID entityUID) {
		return (EntityMeta<PK>) MetaProvider.getInstance().getEntities(entityUID);
	}

	@Override
	public FieldMeta<?> getEntityField(UID fieldUID) {
		return MetaProvider.getInstance().getEntityField(fieldUID);
	}

	@Override
	public EntityMeta<?> getByTablename(String sTableName) {
		return MetaProvider.getInstance().getByTablename(sTableName);
	}

	@Override
	public boolean isNuclosEntity(UID entityUID) {
		return MetaProvider.getInstance().isNuclosEntity(entityUID);
	}

	@Override
	public FieldMeta<?> getCalcAttributeCustomization(UID fieldUID, String paramValues) {
		return MetaProvider.getInstance().getCalcAttributeCustomization(fieldUID, paramValues);
	}

	@Override
	public List<EntityObjectVO<UID>> getImplementingEntityDetails(final UID genericEntityUID) {
		return MetaProvider.getInstance().getImplementingEntityDetails(genericEntityUID);
	}

	@Override
	public List<EntityObjectVO<UID>> getImplementingFieldMapping(final UID genericImplementationUID) {
		return MetaProvider.getInstance().getImplementingFieldMapping(genericImplementationUID);
	}

	@Override
	public Collection<EntityMeta<?>> getAllLanguageEntities() {
		return MetaProvider.getInstance().getAllLanguageEntities();
	}

	private void deleteReferencingObjects(FieldMeta<UID> refField, UID processId) {
		IEntityObjectProcessor<UID> proc = nucletDalProvider.getEntityObjectProcessor(refField.getEntity());
		proc.getIdsBySearchExpression(new CollectableSearchExpression(
				SearchConditionUtils.newUidComparison(refField, ComparisonOperator.EQUAL, processId)
		)).forEach(subpk -> proc.delete(new Delete<>(subpk)));
	}
}
