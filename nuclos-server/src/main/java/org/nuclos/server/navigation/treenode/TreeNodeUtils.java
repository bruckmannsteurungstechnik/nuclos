//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.navigation.treenode;

import java.util.Map;

import org.nuclos.api.NuclosImage;
import org.nuclos.common.EntityTreeViewVO;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.metadata.TreeMetaProvider;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.statemodel.valueobject.StateVO;
/**
 * TreeNodeUtils
 *
 * @author Moritz Neuhäuser &lt;moritz.neuhaeuser@nuclos.de&gt;
 */
public class TreeNodeUtils {

	/**
	 * tree node identifier
	 *
	 * @param uidNode			node id for {@link EntityTreeViewVO}
	 * @param uidEntity			entity id
	 * @param treeProvider		tree meta data provider
	 * @param localeDelegate	locale delegate {@link SpringLocaleDelegate}
	 * @param metaProvider		meta provider {@link MetaProvider}
	 * @return node identifier
	 */
	public static String getIdentifier(
			final UID uidNode,
			final UID uidEntity,
			final Map<UID, Object> fieldValues,
			TreeMetaProvider treeProvider,
			SpringLocaleDelegate localeDelegate,
			MetaProvider metaProvider,
			StateVO state,
			UID language
	) {
		String identifier = "";

		if(state != null) {
			UID stateIconUID = SF.STATEICON.getUID(uidEntity);
			NuclosImage icon = state.getIcon();
			fieldValues.put(stateIconUID, icon);
		}

		try {
			EntityTreeViewVO node = null;
			if (null != uidNode) {
				// read label from node configuration
				node = treeProvider.getNode(uidNode);
			}

			// get node identifier from node configuration
			if (null != node) {
					String strNode = node.getNode();
					if (null != strNode) {
						identifier = localeDelegate.getTreeViewLabel(fieldValues, uidEntity, strNode, metaProvider, language);
					}
			}

			// fallback1: get identifier as before
			if (org.nuclos.common2.StringUtils.looksEmpty(identifier)) {
				identifier = localeDelegate.getTreeViewLabel(fieldValues, uidEntity, metaProvider, language);
			}

			// fallback2: get entity name
			if (org.nuclos.common2.StringUtils.looksEmpty(identifier) && null != node) {
				identifier = metaProvider.getEntity(node.getEntity()).getEntityName();
			}
		} catch(CommonPermissionException ex) {
			// ignore
		}
		return identifier;
	}

	/**
	 * tree node description
	 *
	 * @param uidNode			node id for {@link EntityTreeViewVO}
	 * @param uidEntity			entity id
	 * @param treeProvider		tree meta data provider
	 * @param localeDelegate	locale delegate {@link SpringLocaleDelegate}
	 * @param metaProvider		meta provider {@link MetaProvider}
	 * @return node description
	 */
	public static String getDescription(
			final UID uidNode,
			final UID uidEntity,
			final Map<UID, Object> fieldValues,
			TreeMetaProvider treeProvider,
			SpringLocaleDelegate localeDelegate,
			MetaProvider metaProvider,
			UID language
	) {
		String result = "";
		if (null != uidNode) {
			result = treeProvider.getNodeDescription(uidNode, fieldValues, language);
		}
		// fallback
		if (StringUtils.looksEmpty(result)) {
			result = localeDelegate.getTreeViewDescription(fieldValues, uidEntity, metaProvider, language);
		}
		return result;
	}
}
