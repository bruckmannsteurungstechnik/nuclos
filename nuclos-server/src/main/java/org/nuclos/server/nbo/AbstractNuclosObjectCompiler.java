package org.nuclos.server.nbo;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.tools.Diagnostic;
import javax.tools.DiagnosticListener;
import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject;
import javax.tools.SimpleJavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;

import org.apache.commons.io.FileUtils;
import org.nuclos.api.ide.valueobject.ISourceItem;
import org.nuclos.api.ide.valueobject.SourceType;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.common2.exception.NuclosCompileException.ErrorMessage;
import org.nuclos.common2.security.MessageDigestOutputStream;
import org.nuclos.server.customcode.codegenerator.CodeGenerator.JavaSourceAsString;
import org.nuclos.server.customcode.codegenerator.GeneratorClasspathComponent;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants;
import org.nuclos.server.customcode.codegenerator.NuclosJavaCompilerComponent;
import org.nuclos.server.customcode.codegenerator.NuclosJavaCompilerExceptionCache;
import org.nuclos.server.eventsupport.ejb3.SourceCache;
import org.nuclos.server.eventsupport.ejb3.SourceInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractNuclosObjectCompiler {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractNuclosObjectCompiler.class);

	@Autowired
	protected NuclosJavaCompilerComponent nuclosJavaCompilerComponent;

	@Autowired
	protected GeneratorClasspathComponent generatorClasspathComponent;

	@Autowired
	protected NuclosJavaCompilerExceptionCache compilerExceptionCache;

	@Autowired
	private SourceCache sourceCache;

	private Locale locale = null;

	/**
	 * The path of the generated jar file.
	 */
	private final File jarFile;

	/**
	 * The type of source.
	 */
	private final SourceType sourceType;

	/**
	 * The name of the base directory.
	 */
	private final String sourceDirectoryName;

	/**
	 * The directory of the source files. Ends with {@link #sourceDirectoryName}.
	 */
	private File sourceDirectory;

	/**
	 * The directory where the source files are build. Ends with {@link #sourceDirectoryName}.
	 */
	private File sourceBuildDirectory;

	private JavaCompiler javac;

	private StandardJavaFileManager stdFileManager;

	private CodeGeneratorDiagnosticListener diagnosticListener;


	/**
	 * @param sourceType          The type of the source code.
	 * @param jarFile             The path of the jar file to generate.
	 * @param sourceDirectoryName The name of the subdirectory for build and sources.
	 *                            Must not be null or empty.
	 */
	public AbstractNuclosObjectCompiler(
			SourceType sourceType,
			File jarFile,
			String sourceDirectoryName
	) {
		this.sourceType = sourceType;
		this.sourceDirectoryName = sourceDirectoryName;
		this.jarFile = jarFile;
	}

	/**
	 * Initializes the compiler that works in the given subdirectory of the build directory and
	 * saves the source files in the given subdirectory of the source directory.
	 */
	@PostConstruct
	public void init() {
		sourceBuildDirectory = ensureBuildOutputPath();
		sourceDirectory = ensureSourceOutputPath();

		javac = NuclosJavaCompilerComponent.getJavaCompilerTool();
		if (javac == null) {
			throw new NuclosFatalException("No registered system Java compiler found");
		}
		this.diagnosticListener = new CodeGeneratorDiagnosticListener(locale);
		this.stdFileManager = javac.getStandardFileManager(diagnosticListener, locale, null);

		try {
			// add libs from tomcat
			String catalinaHome = (String) System.getProperties().get("catalina.home");
			List<File> classpath = new ArrayList<>();
			if (catalinaHome != null) {
				catalinaHome = catalinaHome + File.separator + "lib";
				classpath.addAll(getLibs(new File(catalinaHome)));
			}
			classpath.addAll(generatorClasspathComponent.getExpandedSystemParameterClassPath(false));
			// TODO: Why add both JARs and build output directories?
			// We will test it, with classes only ...

			classpath.add(new File(nuclosJavaCompilerComponent.getBuildOutputPath(),
					NuclosCodegeneratorConstants.SRC_DIR_NAME));
			classpath.add(new File(nuclosJavaCompilerComponent.getBuildOutputPath(),
					NuclosCodegeneratorConstants.BO_SRC_DIR_NAME));
			classpath.add(new File(nuclosJavaCompilerComponent.getBuildOutputPath(),
					NuclosCodegeneratorConstants.DATASOURCEREPORT_SRC_DIR_NAME));
			classpath.add(new File(nuclosJavaCompilerComponent.getBuildOutputPath(),
					NuclosCodegeneratorConstants.GENERATION_SRC_DIR_NAME));
			classpath.add(new File(nuclosJavaCompilerComponent.getBuildOutputPath(),
					NuclosCodegeneratorConstants.STATEMODEL_SRC_DIR_NAME));
			classpath.add(new File(nuclosJavaCompilerComponent.getBuildOutputPath(),
					NuclosCodegeneratorConstants.REPORT_SRC_DIR_NAME));
			classpath.add(new File(nuclosJavaCompilerComponent.getBuildOutputPath(),
					NuclosCodegeneratorConstants.PRINTOUT_SRC_DIR_NAME));
			classpath.add(new File(nuclosJavaCompilerComponent.getBuildOutputPath(),
					NuclosCodegeneratorConstants.IMPORTSTRUCTUREDEFS_SRC_DIR_NAME));

			classpath.add(new File(nuclosJavaCompilerComponent.getBuildOutputPath(),
					NuclosCodegeneratorConstants.PARAMETER_SRC_DIR_NAME));
			classpath.add(new File(nuclosJavaCompilerComponent.getBuildOutputPath(),
					NuclosCodegeneratorConstants.COMMUNICATION_SRC_DIR_NAME));
			classpath.add(new File(nuclosJavaCompilerComponent.getBuildOutputPath(),
					NuclosCodegeneratorConstants.USERROLE_SRC_DIR_NAME));
			classpath.add(new File(nuclosJavaCompilerComponent.getBuildOutputPath(),
					NuclosCodegeneratorConstants.WEBSERVICE_SRC_DIR_NAME));

			//classpath.add(NuclosCodegeneratorConstants.JARFILE);
			//classpath.add(NuclosCodegeneratorConstants.BOJARFILE);
			//classpath.add(NuclosCodegeneratorConstants.STATEMODELJARFILE);
			//classpath.add(NuclosCodegeneratorConstants.DATASOURCEREPORTJARFILE);
			//classpath.add(NuclosCodegeneratorConstants.IMPORTSTRUCTUREDEFSJARFILE);
			//classpath.add(NuclosCodegeneratorConstants.GENERATIONJARFILE);
			//classpath.add(NuclosCodegeneratorConstants.REPORTJARFILE);
			//classpath.add(NuclosCodegeneratorConstants.WEBSERVICEJARFILE);
			//classpath.add(NuclosCodegeneratorConstants.PRINTOUTJARFILE);
			//classpath.add(NuclosCodegeneratorConstants.PARAMETERJARFILE);
			//classpath.add(NuclosCodegeneratorConstants.COMMUNICATIONJARFILE);
			//classpath.add(NuclosCodegeneratorConstants.USERROLEJARFILE);
			stdFileManager.setLocation(StandardLocation.CLASS_PATH, new ArrayList<>(classpath));

			stdFileManager.setLocation(StandardLocation.SOURCE_OUTPUT,
					Collections.singleton(sourceDirectory));

			stdFileManager.setLocation(StandardLocation.CLASS_OUTPUT,
					Collections.singleton(sourceBuildDirectory));

		} catch (IOException e) {
			throw new NuclosFatalException(e);
		}
	}

	@PreDestroy
	public void close() {
		try {
			this.stdFileManager.close();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * Compiles the given sources and creates a jar.
	 *
	 *
	 * @param builderThreadPool
	 * @param javaSources The sources to compile.
	 * @throws NuclosCompileException On compilation errors.
	 */
	public void compileSourcesAndJar(final ForkJoinPool builderThreadPool, List<NuclosBusinessJavaSource> javaSources)
			throws NuclosCompileException, InterruptedException {

		cleanupBuildDirectory();
		cleanupSourceDirectory();
		compile(builderThreadPool, javaSources);
		jar();
	}

	private void compile(final ForkJoinPool builderThreadPool, List<NuclosBusinessJavaSource> javaSources)
			throws NuclosCompileException, InterruptedException {

		LOG.debug("Compiler Classpath: {}", stdFileManager.getLocation(StandardLocation.CLASS_PATH));

		if (javaSources.size() > 0) {
			try {
				stdFileManager.flush();
				// Everything went fine, so return a list of fullqualified classname

				final List<IOException> exceptions = new CopyOnWriteArrayList<>();
				final List<RecursiveAction> actions = new ArrayList<>();
				for (NuclosBusinessJavaSource javaSource : javaSources) {
					RecursiveAction action = new RecursiveAction() {
						@Override
						protected void compute() {
							File currentSourceFile = new File(javaSource.getName());
							if (!currentSourceFile.getParentFile().exists()) {
								currentSourceFile.getParentFile().mkdirs();
							}

							if (!currentSourceFile.exists() || javaSource.isEdited()) {
								MessageDigestOutputStream mdos = null;
								BufferedWriter out = null;
								try {
									currentSourceFile.createNewFile();
									mdos = new MessageDigestOutputStream(new FileOutputStream(currentSourceFile),
											SourceCache.DIGEST);
									out = new BufferedWriter(new OutputStreamWriter(
											mdos, NuclosCodegeneratorConstants.JAVA_SRC_ENCODING));
									out.write(javaSource.getCharContent(true).toString());
								} catch (IOException e) {
									exceptions.add(e);
								} finally {
									if (out != null) {
										try {
											out.close();
										} catch (IOException e) {
											exceptions.add(e);
										}
									}
									if (mdos != null) {
										final String qname = javaSource.getFQName();
										final String newHash = mdos.digestAsBase64();
										final SourceInfo info = new SourceInfo(qname, sourceType, null, null);
										final ISourceItem item = sourceCache.getOrCreateSourceItem(info);
										item.setHashValue(newHash);
										sourceCache.addOrUpdate(item, info);
										sourceCache.updateHashValue(qname, newHash);
									}
								}
							}
						}
					};
					builderThreadPool.execute(action);
					actions.add(action);

				}

				for (RecursiveAction action : actions) {
					action.join();
				}
				if (!exceptions.isEmpty()) {
					throw exceptions.get(0);
				}

			} catch (IOException e) {
				throw new NuclosCompileException(e);
			}
		}

		List<String> options = new ArrayList<>();
		options.add("-g");
		options.add("-encoding");
		options.add(NuclosCodegeneratorConstants.JAVA_SRC_ENCODING);

		boolean success = false;
		List<ErrorMessage> errors = null;

		CompilationTask task =
				javac.getTask(null, stdFileManager, diagnosticListener, options, null, javaSources);

		success = task.call();
		errors = diagnosticListener.clearErrors();

		this.compilerExceptionCache.clear();

		if (!success) {
			LOG.info("Compile failed with {} errors:", errors.size());
			for (ErrorMessage em : errors) {
				LOG.info("Error message: {}", em);
			}

			// Store exceptions in cache for later fixing
			compilerExceptionCache.add(errors);
		throw new NuclosCompileException(errors);
		}
	}


	private void jar() throws InterruptedException {
		Manifest manifest = new Manifest();
		manifest.getMainAttributes().put(Attributes.Name.MANIFEST_VERSION, "1.0");
		JarOutputStream target;
		try {
			target = new JarOutputStream(new FileOutputStream(jarFile), manifest);
			add(sourceBuildDirectory, sourceBuildDirectory, target);
			target.close();
		} catch (InterruptedException e) {
			throw e;
		} catch (Exception e) {

			LOG.error(e.getMessage(), e);
		}
	}

	private List<File> getLibs(File folder) {
		List<File> files = new ArrayList<>();
		if (!folder.isDirectory()) {
			// just return empty list, compiler will give notice if classes are missing
			return files;
		}

		File[] fileListing = folder.listFiles();
		if (fileListing != null) {
			Collections.addAll(files, fileListing);
		}

		return files;
	}

	private void add(File source, File sourceBase, JarOutputStream target)
			throws IOException, InterruptedException {
		BufferedInputStream in = null;
		final String basename = sourceBase.getPath().replace("\\", "/");

		try {
			if (source.isDirectory()) {
				for (File nestedFile : source.listFiles()) {
					if (Thread.currentThread().isInterrupted()) {
						throw new InterruptedException();
					}
					add(nestedFile, sourceBase, target);
				}
			} else {
				final JarEntry entry =
						new JarEntry(source.getPath().replace("\\", "/").replace(basename + "/", ""));
				entry.setTime(source.lastModified());
				target.putNextEntry(entry);
				in = new BufferedInputStream(new FileInputStream(source));

				byte[] buffer = new byte[1024];
				while (true) {
					int count = in.read(buffer);
					if (count == -1) {
						break;
					}
					target.write(buffer, 0, count);
				}
				target.closeEntry();
			}
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}


	private void cleanupSourceDirectory() {
		// cleanup source directory:
		final File srcDir = this.sourceDirectory;
		if (srcDir.exists()) {
			AbstractNuclosObjectCompiler.removeDirectory(srcDir);
		}
		srcDir.mkdirs();
	}

	private void cleanupBuildDirectory() {
		AbstractNuclosObjectCompiler.removeDirectory(sourceBuildDirectory);
		sourceBuildDirectory.mkdirs();
	}

	private File ensureBuildOutputPath() {
		File dir = new File(nuclosJavaCompilerComponent.getBuildOutputPath(), sourceDirectoryName);
		if (!dir.exists()) {
			if (dir.mkdirs()) {
				LOG.debug("Created build directory {}", dir);
			}
		}
		return dir;
	}

	private File ensureSourceOutputPath() {
		File dir =
				new File(nuclosJavaCompilerComponent.getCodeGeneratorOutputPath(), sourceDirectoryName);
		if (!dir.exists()) {
			if (dir.mkdirs()) {
				LOG.debug("Created source directory {}", dir);
			}
		}
		return dir;
	}

	public static void removeDirectory(File directoryToDelete) {
		try {
			FileUtils.deleteDirectory(directoryToDelete);
		} catch (IOException e) {
			LOG.debug("Unable to delete directory {}", directoryToDelete, e);
		}
	}

	public static class NuclosBusinessJavaSource extends SimpleJavaFileObject {

		private final String qualifiedName;
		private final String content;
		private final boolean edited;

		public NuclosBusinessJavaSource(String qualifiedName, String pFileString, String pContent,
										boolean pEdited) {
			super(new File(pFileString).toURI(), Kind.SOURCE);
			this.qualifiedName = qualifiedName;
			this.content = pContent;
			this.edited = pEdited;
		}

		public String getFQName() {
			return qualifiedName;
		}

		@Override
		public CharSequence getCharContent(boolean ignoreEncodingErrors) {
			return content;
		}

		public boolean isEdited() {
			return this.edited;
		}
	}


	/**
	 * A diagnostic listener which collects the error messages.  It recognizes
	 * <code>GeneratedJavaFileObject</code>s and automatically adjust the line and position
	 * offsets.
	 */
	static class CodeGeneratorDiagnosticListener implements DiagnosticListener<JavaFileObject> {


		private final Locale locale;


		private final List<NuclosCompileException.ErrorMessage> errors;

		CodeGeneratorDiagnosticListener(Locale locale) {
			this.locale = locale;
			this.errors = new ArrayList<>();
		}

		@Override
		public synchronized void report(Diagnostic<? extends JavaFileObject> diag) {
			if (diag.getKind() == Diagnostic.Kind.ERROR) {
				JavaFileObject source = diag.getSource();

				String packageName = null;
				try {
					String content = source.getCharContent(true).toString();
					if (content != null && content.contains("package")) {
						packageName =
								content.substring(content.indexOf("package") + 7, content.indexOf(";"))
										.trim();
					}
				} catch (Exception e) {
					// nothing can be done.
				}
				String message = getMessageWithoutPath(diag);
				if (message == null || message.isEmpty()) {
					message = "Unknown error";
				}
				long dl = 0, dp = 0; // line and position deltas

				if (source instanceof JavaSourceAsString) {
					long line = diag.getLineNumber();
					if (line != Diagnostic.NOPOS
							&& source.getKind() == JavaFileObject.Kind.SOURCE) {
						if (message.startsWith(line + ":")) {
							message = message.substring((line + ":").length());
						}
					}
				}

				if (source != null) {
					String sourcename = source.getName(); // physical or symbolic source name
					UID entity = null;

					if (source instanceof JavaSourceAsString) {
						JavaSourceAsString jas = (JavaSourceAsString) source;
						sourcename = jas.getLabel();
						entity = jas.getEntityUid();
					}

					errors.add(
							new ErrorMessage(
									diag.getKind(),
									sourcename,
									message,
									entity,
									packageName,
									diag.getLineNumber(),
									diag.getColumnNumber(),
									diag.getPosition(),
									diag.getStartPosition(),
									diag.getEndPosition()
							)
					);
				}
			}
		}

		public synchronized List<NuclosCompileException.ErrorMessage> clearErrors() {
			List<NuclosCompileException.ErrorMessage> result = new ArrayList<>(errors);
			errors.clear();
			return result;
		}

		private String getMessageWithoutPath(Diagnostic<? extends JavaFileObject> diag) {
			String message = diag.getMessage(locale);
			JavaFileObject source = diag.getSource();
			if (source != null && message != null) {
				String path = source.toUri().getPath();
				if (path != null && message.startsWith(path + ":")) {
					message = message.substring(path.length() + 1);
				}
				String lineNumber = "" + diag.getLineNumber();
				if (message.startsWith(diag.getLineNumber() + ":")) {
					message = message.substring(lineNumber.length() + 1);
				}
				message = message.trim();
			}
			return message;
		}

		private static long shift(long pos, long delta) {
			if (pos != Diagnostic.NOPOS) {
				pos -= delta;
				if (pos < 0) {
					pos = 0;
				}
			}
			return pos;
		}

	}


}
