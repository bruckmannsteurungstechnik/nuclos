//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.nbo;

import java.io.Serializable;

import org.nuclos.api.UID;
import org.nuclos.api.print.PrintProperties;
import org.nuclos.api.report.OutputFormat;
import org.nuclos.common.E;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * {@link AbstractOutputFormat} is  the nuclos default implementation for {@link OutputFormat}
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public abstract class AbstractOutputFormat implements OutputFormat, Serializable {

	private final UID id;
	private final ThreadLocal<PrintProperties> printProperties = new ThreadLocal<PrintProperties>(); 
	
	public AbstractOutputFormat(final UID id) {
		this.id = id;
	}


	@Override
	public UID getId() {
		return this.id;
	}


	@Override
	public PrintProperties getProperties() {
		PrintProperties result = printProperties.get();
		if (result == null) {
			result = new PrintProperties();
			
			// TODO cache outputFormats
			final EntityObjectVO<org.nuclos.common.UID> reportOutput = 
					NucletDalProvider.getInstance().getEntityObjectProcessor(E.REPORTOUTPUT).getByPrimaryKey((org.nuclos.common.UID) getId());
			final ReportOutputVO reportOutputVO = MasterDataWrapper.getReportOutputVO(new MasterDataVO<org.nuclos.common.UID>(reportOutput, true));
			PrintProperties defaultProperties = reportOutputVO.getProperties();
			result.setPrintServiceId(defaultProperties.getPrintServiceId());
			result.setTrayId(defaultProperties.getTrayId());
			result.setCopies(defaultProperties.getCopies());
			result.setDuplex(defaultProperties.isDuplex());
			
			printProperties.set(result);
		}
		assert printProperties.get() != null;
		assert result != null;
		return result;
	}
	

}
