//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.nbo;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.nuclos.api.UID;
import org.nuclos.api.printout.Printout;
import org.nuclos.api.report.OutputFormat;
import org.nuclos.server.report.ejb3.ReportFacadeLocal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javassist.Modifier;

/**
 * {@link AbstractPrintout} is  the nuclos default implementation for {@link Printout}
 * 
 * <pre>
 * {@code
 * package org.nuclet.printout;
 * 
 * import org.nuclos.api.printout.Printout;
 * import org.nuclos.common.UID;
 * import org.nuclos.api.report.OutputFormat;
 * import org.nuclos.server.nbo.AbstractPrintout;
 * import java.io.Serializable;
 * import org.nuclos.server.nbo.AbstractOutputFormat;
 * 
 * public class FormularLieferscheinPO extends AbstractPrintout implements Serializable {
 * 
 *     public static final OutputFormat CSV_Erstellung = new AbstractOutputFormat(UID.parseUID("TbbmD96JIXWMal9solWd")){};
 *     public static final OutputFormat PDF_Erstellung_LS = new AbstractOutputFormat(UID.parseUID("BJImQBDVwY4H0rumovh5")){};
 * 
 * 
 *     public org.nuclos.api.UID getId() {
 *         return UID.parseUID("xV7job3gGm0b7st6wiLO");
 *     }
 * }
 * </pre>
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public abstract class AbstractPrintout implements Printout {

	private static Logger LOG = LoggerFactory.getLogger(AbstractPrintout.class);
	private Long idBo;
	private List<OutputFormat> outputFormats;

	@Autowired
	private ReportFacadeLocal facadeReport;
	

	@Override
	public void setBusinessObjectId(Long idBo) {
		this.idBo = idBo;
	}

	@Override
	public Long getBusinessObjectId() {
		return idBo;
	}

	@Override
	public synchronized List<OutputFormat> getOutputFormats() {
		if (null == this.outputFormats) {
			try {
				this.outputFormats = loadConfiguredOutputFormats();
			} catch (final IllegalArgumentException e) {
				LOG.error(e.getMessage(), e);
			} catch (final IllegalAccessException e) {
				LOG.error(e.getMessage(), e);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("initialized {0} configured Ouptutformats: {}", outputFormats.size(), StringUtils.join(outputFormats, ","));
			}
		}
		return outputFormats;
	}

	/**
	 * load static declared {@link OutputFormat}
	 * 
	 * @return list of {@link OutputFormat}
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	private final List<OutputFormat> loadConfiguredOutputFormats() throws IllegalArgumentException, IllegalAccessException {
		final Class<? extends AbstractPrintout> clazz = this.getClass();
		final Map<UID, String> mapNames = new HashMap<UID, String>();
		final List<OutputFormat> result = new ArrayList<OutputFormat>();
		for (final Field field : clazz.getFields()) {
			// check for static fields (OutputFormats are static for compatibility reasons)
			if (!Modifier.isStatic(field.getModifiers())) {
				continue;
			}
			// check if field type is OutputFormat
			if (!OutputFormat.class.equals(field.getType())) {
				continue;
			}
			
			//of.add((OutputFormat)field.get(null));
			// create new instance, otherwise the print properties would always be the same object
			OutputFormat of = new AbstractOutputFormat(((OutputFormat)field.get(null)).getId()){};
			result.add(of);
			mapNames.put(of.getId(), field.getName());
		}
		Collections.sort(result, new Comparator<OutputFormat>() {
			@Override
			public int compare(OutputFormat of1, OutputFormat of2) {
				final String name1 = mapNames.get(of1.getId());
				final String name2 = mapNames.get(of2.getId());
				if (name2.length()>name1.length() && name2.startsWith(name1)) {
					return 1;
				}
				if (name1.length()>name2.length() && name1.startsWith(name2)) {
					return -1;
				}
				return name1.compareToIgnoreCase(name2);
			}
		});
		return result;
	}
}
