//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.nbo;

import java.util.concurrent.ForkJoinPool;

import org.nuclos.common.UID;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.common.INucletCache;

public abstract class NuclosObjectBuilder {

	protected abstract void createObjects(final ForkJoinPool builderThreadPool) throws CommonBusinessException, InterruptedException;
	
	protected static String formatMethodName(String name) {
		return name.substring(0, 1).toUpperCase() + name.substring(1);
	}

	protected String formatStringValues(String value, String defaultValue) {
		
		if (defaultValue == null)
			defaultValue = "";
		
		String retVal = StringUtils.defaultIfNull(value, defaultValue);
		
		retVal = retVal.replace("\n", "");
		retVal = retVal.replace("\\", "");
		retVal = retVal.replace("\"", "");
		
		return retVal;
	}
	
	public static String getNucletPackageStatic(UID nucletUID, INucletCache nucletCache, String defaultPackage) {
		String retVal = defaultPackage;

		if (nucletUID != null) {
			String sFqn = nucletCache.getFullQualifiedNucletName(nucletUID);
			if (!StringUtils.looksEmpty(sFqn)) {
				retVal = sFqn;
			}
		}

		return retVal;
	}
}
