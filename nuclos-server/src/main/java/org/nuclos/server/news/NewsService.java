package org.nuclos.server.news;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.nuclos.api.businessobject.Query;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.businessentity.News;
import org.nuclos.businessentity.NewsConfirmed;
import org.nuclos.businessentity.NewsViewed;
import org.nuclos.businessentity.nuclosuser;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.query.DbUpdate;
import org.nuclos.server.dblayer.statements.DbMap;
import org.nuclos.server.rest.ejb3.Rest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Service
public class NewsService {

	private static final Logger LOG = LoggerFactory.getLogger(NewsService.class);

	private final SpringDataBaseHelper dataBaseHelper;

	public NewsService(final SpringDataBaseHelper dataBaseHelper) {
		this.dataBaseHelper = dataBaseHelper;
	}

	public List<News> getCurrentNews() {
		Query<News> query = currentlyActiveNews();

		return QueryProvider.execute(query);
	}

	public List<News> getNewsConfirmationRequired() {
		Query<News> query = currentlyActiveNews()
				.and(News.ConfirmationRequired.eq(true));

		return QueryProvider.execute(query);
	}

	/**
	 * @return The base Query for currently active and valid News.
	 */
	private Query<News> currentlyActiveNews() {
		Date now = new Date();

		return QueryProvider.create(News.class)
				.where(News.Active.eq(true))
				.and(News.ValidFrom.isNull().or(News.ValidFrom.Lte(now)))
				.and(News.ValidUntil.isNull().or(News.ValidUntil.Gte(now)))
				.orderBy(News.Name, true)
				.orderBy(News.Revision, false);
	}

	public List<News> getUnconfirmedNewsForCurrentUser() {
		return getUnconfirmedNews(Rest.facade().getCurrentUserId());
	}

	/**
	 * @return The unconfirmed News that are flagged as privacy policy, for the given user.
	 */
	public List<News> getUnconfirmedPrivacyPolicies(final UID userId) {
		final Query<News> query = currentlyActiveNews().and(News.PrivacyPolicy.eq(true));

		return getUnconfirmedNewsByQueryAndUser(
				query,
				userId
		);
	}

	/**
	 * @return The unconfirmed News that require confirmation for the given user.
	 */
	public List<News> getUnconfirmedNews(final UID userId) {
		Query<News> query = currentlyActiveNews()
				.and(News.ConfirmationRequired.eq(true));

		return getUnconfirmedNewsByQueryAndUser(query, userId);
	}

	/**
	 * @return The unconfirmed News that require confirmation for the given user.
	 */
	public List<News> getUnconfirmedNewsByQueryAndUser(
			final Query<News> query,
			final UID userId
	) {
		List<News> list = QueryProvider.execute(query);

		// TODO: Ugly workaround for missing "NOT EXISTS" method in Query API. See NUCLOS-7529
		Query<NewsConfirmed> newsConfirmedQuery = QueryProvider.create(NewsConfirmed.class)
				.where(NewsConfirmed.UserId.eq(userId));
		List<NewsConfirmed> confirmedNews = QueryProvider.execute(newsConfirmedQuery);

		return list.stream().filter(
				// TODO: Ugly workaround for missing "NOT EXISTS" method in Query API. See NUCLOS-7529
				news -> confirmedNews.stream().noneMatch(
						confirmed -> confirmed.getNewsId().equals(news.getId())
				)
		).collect(Collectors.toList());
	}

	public List<News> getUnreadNewsForCurrentUser() {
		Query<News> query = currentlyActiveNews()
				.and(News.ShowAtStartup.eq(true))
				// News which require confirmation are displayed separately
				.and(News.ConfirmationRequired.eq(false));

		List<News> list = QueryProvider.execute(query);

		// TODO: Ugly workaround for missing "NOT EXISTS" method in Query API. See NUCLOS-7529
		Query<NewsViewed> newsViewedQuery = QueryProvider.create(NewsViewed.class)
				.where(NewsViewed.UserId.eq(Rest.facade().getCurrentUserId()));
		List<NewsViewed> viewedNews = QueryProvider.execute(newsViewedQuery);

		return list.stream().filter(
				// TODO: Ugly workaround for missing "NOT EXISTS" method in Query API. See NUCLOS-7529
				news -> viewedNews.stream().noneMatch(
						viewed -> viewed.getNewsId().equals(news.getId())
				)
		).collect(Collectors.toList());
	}

	public void newsConfirmed(final UID newsId) throws BusinessException {
		NewsConfirmed confirmed = new NewsConfirmed();
		confirmed.setNewsId(newsId);
		confirmed.setUserId(Rest.facade().getCurrentUserId());

		confirmed.save();
	}

	public void newsViewed(final UID newsId) throws BusinessException {
		NewsViewed viewed = new NewsViewed();
		viewed.setNewsId(newsId);
		viewed.setUserId(Rest.facade().getCurrentUserId());

		viewed.save();
	}

	/**
	 * Sets the privacy consent flags of all users back to false.
	 */
	public void deletePrivacyConsents() {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();

		DbMap dbMap = new DbMap();
		dbMap.put(E.USER.privacyConsentAccepted, false);

		DbUpdate query = builder.createUpdate(E.USER, dbMap);
		int affectedRows = dataBaseHelper.getDbAccess().executeUpdate(query);

		LOG.info("Update privacy consent flag for {} users", affectedRows);
	}


	/**
	 * Updates the privacy consent flag of every user.
	 */
	public void updatePrivacyConsent() {
		// TODO: This could be done in one single query on the DB.
		Query<nuclosuser> query = QueryProvider.create(nuclosuser.class)
				// Exclude current user who is editing the News
				.where(nuclosuser.Id.neq(Rest.facade().getCurrentUserId()));

		for (nuclosuser user: QueryProvider.execute(query)) {
			List<News> unconfirmedPrivacyPolicies = getUnconfirmedPrivacyPolicies(user.getPrimaryKey());

			Boolean privacyConsent = unconfirmedPrivacyPolicies.isEmpty();
			if (!privacyConsent.equals(user.getPrivacyconsent())) {
				updatePrivacyConsentForUser(user, privacyConsent);
			}
		}
	}

	/**
	 * Updates the privacy consent of the given nuclosuser via a DB query,
	 * because an update via BusinessObjectProvider causes problems through
	 * automatic cache invalidations and logouts.
	 */
	public void updatePrivacyConsentForUser(final nuclosuser user, final Boolean privacyConsent) {
		try {
			// TODO: Do not execute plain SQL, but there seems to be no other options now...
			//  DbUpdate with PK-based WHERE-conditions is not possible?!
			dataBaseHelper.getDbAccess().executePlainUpdate("UPDATE T_MD_USER SET BLNPRIVACYCONSENT = " + (privacyConsent ? 1 : 0) + " WHERE STRUID = '" + user.getId() + "'");
			LOG.debug("Updated privacy consent of user {}", user.getUsername());
		} catch (Exception e) {
			LOG.error("Could not update privacy consent of user {}", user.getUsername(), e);
		}
	}
}
