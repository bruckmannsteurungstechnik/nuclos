//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.printservice;

import org.nuclos.server.printservice.PrintJob.Mode;
import org.nuclos.server.printservice.exception.PrintSpoolerJobException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link PrintJobWorker} wraps execution of {@link PrintJob}
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public class PrintJobWorker implements Runnable {

	final Logger LOG = LoggerFactory.getLogger(PrintJobWorker.class);
	
	private final PrintJob job;

	public PrintJobWorker(final PrintJob job) {
		this.job = job;
		job.setMode(Mode.SCHEDULED);
	}

	@Override
	public void run() {
		try {
			job.setMode(Mode.RUNNING);
			
			job.execute();
			job.setMode(Mode.DONE);
		} catch (final PrintSpoolerJobException ex) {
			job.setMode(Mode.INTERRUPTED);
		}
		
	}

	@Override
	public String toString() {
		return "PrintJobWorker [job=" + job + "]";
	}
	
	
	
}
