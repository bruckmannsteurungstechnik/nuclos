//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.printservice;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.printservice.PrintServiceLocator;
import org.nuclos.common.report.valueobject.PrintServiceTO;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Component;

/**
 * {@link PrintServiceRepository} provides print services
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
@Component
public class PrintServiceRepository {
	
	private final static Logger LOG = LoggerFactory.getLogger(PrintServiceRepository.class);
	@Autowired
	private NucletDalProvider nucletDalProvider;
	
	/**
	 * provide print service by id
	 * 
	 * @param idPrintservice print service id
	 * 
	 * @return {@link PrintServiceTO}
	 */
	public PrintServiceTO printService(final UID idPrintservice) {
		return printServicesMap().get(idPrintservice);
	}
	
	public PrintServiceTO.Tray tray(final UID idTray) {
		return traysMap().get(idTray);
	}

	/**
	 * provides available print service
	 * 
	 * @return list of {@link PrintServiceTO}
	 */
	public List<PrintServiceTO> printServices() {
		final List<PrintServiceTO> result = new ArrayList<PrintServiceTO>(printServicesMap().values());
		Collections.sort(result);
		return result;
	}
	
	@Cacheable(value="printServiceRepositoryByName", key="#p0")
	public PrintServiceTO printServiceByName(String name) {
		for (PrintServiceTO printService : printServicesMap().values()) {
			if (printService.getName().equals(name)) {
				return printService;
			}
		}
		return null;
	}
	
	@Cacheable(value="printServiceRepository")
	private Map<UID, PrintServiceTO> printServicesMap() {	
		if (LOG.isDebugEnabled()) {
			LOG.debug("lookup printservices");
		}
		final Map<UID, PrintServiceTO> result = new HashMap<UID, PrintServiceTO>();
		final List<EntityObjectVO<UID>> allPrintservices = nucletDalProvider
				.getEntityObjectProcessor(E.PRINTSERVICE)
				.getAll();
		

		for (final EntityObjectVO<UID> eo : allPrintservices) {
			UID idDefaultTray = eo.getFieldUid(E.PRINTSERVICE.defaultTray);
			PrintServiceTO.Tray defaultTray = null;
			if (idDefaultTray != null) {
				defaultTray = traysMap().get(idDefaultTray);
			}
			PrintServiceTO printService = new PrintServiceTO(eo.getPrimaryKey(), 
					eo.getFieldValue(E.PRINTSERVICE.name), 
					eo.getFieldValue(E.PRINTSERVICE.description),
					eo.getFieldValue(E.PRINTSERVICE.useNativePdfSupport),
					defaultTray); 
			result.put(eo.getPrimaryKey(), printService);
		}

		for (PrintServiceTO.Tray tray : traysMap().values()) {
			PrintServiceTO printService = result.get(tray.getPrintServiceId());
			printService.addTray(tray);
		}
		
		if (LOG.isDebugEnabled()) {
			LOG.debug("printservices: {}", result.size());
		}

		return result;
	}
	
	@Cacheable(value="printServiceRepositoryTray")
	private Map<UID, PrintServiceTO.Tray> traysMap() {	
		final Map<UID, PrintServiceTO.Tray> result = new HashMap<UID, PrintServiceTO.Tray>();
		final List<EntityObjectVO<UID>> allTrays = nucletDalProvider
				.getEntityObjectProcessor(E.PRINTSERVICE_TRAY)
				.getAll();
		for (EntityObjectVO<UID> eo : allTrays) {
			result.put(eo.getPrimaryKey(), transformToTray(eo));
		}
		return result;
	}
	
	private static PrintServiceTO.Tray transformToTray(EntityObjectVO<UID> eo) {
		return new PrintServiceTO.Tray(eo.getPrimaryKey(), 
				eo.getFieldUid(E.PRINTSERVICE_TRAY.printService),
				eo.getFieldValue(E.PRINTSERVICE_TRAY.number), 
				eo.getFieldValue(E.PRINTSERVICE_TRAY.description));
	}
	
	@Caching(evict= {
			@CacheEvict(value="printServiceRepository", allEntries=true),
			@CacheEvict(value="printServiceRepositoryTray", allEntries=true),
			@CacheEvict(value="printServiceRepositoryByName", allEntries=true)
	})
	public void evictCaches() {}
	
	public javax.print.PrintService lookupDefaultPrintService() {
		return lookupPrintService(null);
	}
	
	public javax.print.PrintService lookupPrintService(final UID idPrintservice) {
		String printerName = null;
		if (idPrintservice != null) {
			printerName = printService(idPrintservice).getName();
		}
		return PrintServiceLocator.lookupPrintServiceByPrinterName(printerName);
	}

}
