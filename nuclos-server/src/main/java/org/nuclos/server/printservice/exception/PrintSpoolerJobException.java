//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.printservice.exception;

import org.nuclos.server.printservice.PrintJob;

/**
 * {@link Exception} for {@link PrintJob} actions
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public class PrintSpoolerJobException extends Exception {

	public PrintSpoolerJobException() {
	}

	public PrintSpoolerJobException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public PrintSpoolerJobException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public PrintSpoolerJobException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/*public PrintSpoolerJobException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}*/

}
