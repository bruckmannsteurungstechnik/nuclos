//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.report;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.SpringApplicationContextHolder.SpringReadyListener;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.collection.PredicateUtils;
import org.nuclos.common.database.query.definition.Column;
import org.nuclos.common.database.query.definition.Constraint;
import org.nuclos.common.database.query.definition.ConstraintEmumerationType;
import org.nuclos.common.database.query.definition.DataType;
import org.nuclos.common.database.query.definition.QueryTable;
import org.nuclos.common.database.query.definition.ReferentialContraint;
import org.nuclos.common.database.query.definition.Schema;
import org.nuclos.common.database.query.definition.Table;
import org.nuclos.common.report.valueobject.CalcAttributeVO;
import org.nuclos.common.report.valueobject.ChartVO;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.DynamicEntityVO;
import org.nuclos.common.report.valueobject.DynamicTasklistVO;
import org.nuclos.common.report.valueobject.RecordGrantVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.XMLUtils;
import org.nuclos.server.cluster.cache.ClusterCache;
import org.nuclos.server.cluster.jms.ClusterActionFactory;
import org.nuclos.server.cluster.jms.NuclosClusterAction;
import org.nuclos.server.common.DatasourceCache;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.DbNotNullableException;
import org.nuclos.server.dblayer.MetaDbHelper;
import org.nuclos.server.dblayer.structure.AbstractDbArtifactVisitor;
import org.nuclos.server.dblayer.structure.DbArtifact;
import org.nuclos.server.dblayer.structure.DbColumn;
import org.nuclos.server.dblayer.structure.DbColumnType;
import org.nuclos.server.dblayer.structure.DbConstraint;
import org.nuclos.server.dblayer.structure.DbConstraint.DbForeignKeyConstraint;
import org.nuclos.server.dblayer.structure.DbConstraint.DbPrimaryKeyConstraint;
import org.nuclos.server.dblayer.structure.DbConstraint.DbUniqueConstraint;
import org.nuclos.server.dblayer.structure.DbNullable;
import org.nuclos.server.dblayer.structure.DbTable;
import org.nuclos.server.dblayer.structure.DbTableType;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedOperationParameters;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.ext.LexicalHandler;

/**
 * Caches the database schema used for reports and forms.<br>
 *
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Boris.Sander@novabit.de">Boris Sander</a>
 * @author	<a href="mailto:Rostislav.Maksymovskyi@novabit.de">Rostislav Maksymovskyi</a>
 * @version 02.00.00
 */
public class SchemaCache implements ClusterCache{
	
   private static final Logger LOG = LoggerFactory.getLogger(SchemaCache.class);

   private static SchemaCache INSTANCE;
   
   //

   private Schema currentSchema = null;
   
   private ServerParameterProvider serverParameterProvider;
   
   private MetaProvider metaProvider;
   
   private SpringDataBaseHelper dataBaseHelper;
   
   private DatasourceCache datasourceCache;
   
   private static final Map<String, Table> mpTableColumns = new ConcurrentHashMap<String, Table>();
   private static Map<String, NamedTableConstraintCacheEntry> mpTableConstraints = null;
   
   public static class NamedTableConstraintCacheEntry {
	   
	   public static final int TYPE_FOREIGN_KEY = 1;
	   public static final int TYPE_UNIQUE_KEY = 2;
//	   public static final int TYPE_NOTNULL = 3;
	   
	   private final String constraintName;
	   private final EntityMeta<?> eMeta;
	   private final List<FieldMeta<?>> fMetas;
	   private final int iType;
	
	   public NamedTableConstraintCacheEntry(String constraintName, EntityMeta<?> eMeta, List<FieldMeta<?>> fMetas, int iType) {
		   super();
		   this.constraintName = constraintName.toUpperCase();
		   this.eMeta = eMeta;
		   this.fMetas = Collections.unmodifiableList(fMetas);
		   this.iType = iType;
	   }
	   public String getConstraintName() {
		   return constraintName;
	   }
	   public EntityMeta<?> getEntityMeta() {
		   return eMeta;
	   }
	   public List<FieldMeta<?>> getFieldMetas() {
		   return fMetas;
	   }
	   public int getType() {
		   return iType;
	   }
	   
	   public static String createNotNullName(String sTableName, String sColumnName) {
		   return "NOTNULL_" + sTableName.toUpperCase() + "." + sColumnName.toUpperCase();
	   }
   }

	SchemaCache() {
		INSTANCE = this;
	}

	@PostConstruct
	final synchronized void init() {
		this.currentSchema = getSchemaFromDB();
		
		// MBeanAgent.invokeCacheMethodAsMBean(INSTANCE.getClass(), "initTableColumnsMap", null, null);
		initTableColumnsMap();
	}

   @Autowired
   void setServerParameterProvider(ServerParameterProvider serverParameterProvider) {
	   this.serverParameterProvider = serverParameterProvider;
   }
   
   @Autowired
   void setMetaProvider(MetaProvider metaProvider) {
	   this.metaProvider = metaProvider;
   }
   
   @Autowired
   void setDataBaseHelper(SpringDataBaseHelper dataBaseHelper) {
	   this.dataBaseHelper = dataBaseHelper;
   }
   
   @Autowired
   void setDatasourceCache(DatasourceCache datasourceCache) {
	   this.datasourceCache = datasourceCache;
   }

   public static SchemaCache getInstance() {
      return INSTANCE;
   }

   //@see NUCLOS-1196 public synchronized Schema getCurrentSchema() {
   public Schema getCurrentSchema() {
	   if (currentSchema == null) {
		   throw new IllegalArgumentException("too early");
	   }
	   return currentSchema;
	}

	private Schema getSchemaFromDB() {
   	// TODO: wie getTablesAndViewsFromDB ...
		//log.info("Initializing SchemaCache");

   	Predicate<String> predicate = null;
   	String filter = serverParameterProvider.getValue(ParameterProvider.KEY_DATASOURCE_TABLE_FILTER);
   	if (filter != null) {
   		predicate = PredicateUtils.wildcardFilterList(filter);
   	}

   	Schema result = getTablesAndViewsFromDB(predicate);

      // @TODO GOREF: "Dynamic tables": For legacy modules their v_ud_go_xxx view was added
      // and marked as "dynamic"
      // DataBaseHelper.getDynamicTables().addToSchema(result);

   	addToSchema(result);

      //log.info("Finished initializing SchemaCache.");
      return result;
   }

   public Schema getCompleteCurrentSchema() {
      return getTablesAndViewsFromDB(null);
   }

	private Schema getTablesAndViewsFromDB(Predicate<String> namePredicate) {
      Schema schema = new Schema();

		//TODO MULTINUCLET tableName to UID?
      for (DbTableType tableType : DbTableType.TABLE_AND_VIEW) {
      	for (String tableName : dataBaseHelper.getDbAccess().getTableNames(tableType)) {
      		if (namePredicate != null && !namePredicate.evaluate(tableName))
      			continue;
	         Table table = new Table(schema.getTimestamp(), tableName);
	         table.setType(tableType.toString());
	         table.setEntityUID(getEntityUID(tableName));
	         schema.addTable(table);
      	}
      }

      return schema;
   }

	private UID getEntityUID(String tableName) {
		for (EntityMeta eMeta : metaProvider.getAllEntities()) {
			if (StringUtils.isNullOrEmpty(eMeta.getDbTable())) {
				LOG.warn("STRDBENTITY is NULL for " + eMeta);
				continue;
			}
			if(eMeta.getDbTable().substring(1).equalsIgnoreCase(tableName.substring(1))) {
				// ignore T or V
				return eMeta.getUID();
			}
		}
		return null;
	}

	@ManagedOperation
	@ManagedOperationParameters
	public void invalidate() {
		invalidate(false, true);
	}

   //@see NUCLOS-1196 public synchronized void invalidate(boolean schemaOnly) {
   public void invalidate(boolean schemaOnly, boolean notifyClusterCloud) {
      LOG.debug("Invalidating SchemaCache");
      currentSchema = getSchemaFromDB();
      if (!schemaOnly) {
	      mpTableColumns.clear();
	      
		  // MBeanAgent.invokeCacheMethodAsMBean(INSTANCE.getClass(), "initTableColumnsMap", null, null);
	      initTableColumnsMap();
      }
      if(notifyClusterCloud)
    	  notifyClusterCloud();
      mpTableConstraints = null;
   }
   
	@ManagedAttribute(description="get the size (number of tables) of mpTableColumns")
	public int getNumberOfTablesInCache() {
		return mpTableColumns.size();
	}

	protected static final String SYSTEMID = "http://www.novabit.de/technologies/querybuilder/querybuildermodel.dtd";
	protected static final String RESOURCE_PATH = "org/nuclos/common/querybuilder/querybuildermodel.dtd";
	protected static final EntityResolver RESOLVER = XMLUtils.newClasspathEntityResolver(SYSTEMID, RESOURCE_PATH, false);

	private class XMLContentHandler implements ContentHandler, LexicalHandler {
		
		private XMLContentHandler() {
		}
		
		@Override
		public void characters(char[] ac, int start, int length) {
		}

		@Override
		public void endDocument() {
		}

		@Override
		public void endElement(String namespaceURI, String localName, String qName) {
		}

		@Override
		public void endPrefixMapping(String prefix) {
		}

		@Override
		public void ignorableWhitespace(char[] ac, int start, int length) {
		}

		@Override
		public void processingInstruction(String target, String data) {
		}

		@Override
		public void skippedEntity(String name) {
		}

		@Override
		public void startDocument() {
		}

		@Override
		public void startPrefixMapping(String prefix, String uri) {
		}

		@Override
		public void setDocumentLocator(Locator loc) {
		}

		@Override
		public void startElement(String namespaceURI, String localName, String qName, Attributes atts) {
			// only process tables
			if (qName.equals("table")) {
				final String sEntity = atts.getValue("entity");
				final Table tableSchema = getCurrentSchema().getTable(sEntity);
				if (tableSchema != null) {
					final Table table = (Table) tableSchema.clone();
					fillTableColumnsAndConstraints(table);
				}
			}
		}

		@Override
		public void endCDATA() throws SAXException {
		}

		@Override
		public void endDTD() throws SAXException {
		}

		@Override
		public void startCDATA() throws SAXException {
		}

		@Override
		public void comment(char[] ch, int start, int length) throws SAXException {
		}

		@Override
		public void endEntity(String name) throws SAXException {
		}

		@Override
		public void startEntity(String name) throws SAXException {
		}

		@Override
		public void startDTD(String name, String publicId, String systemId) throws SAXException {
		}
	}

	@ManagedOperation
	@ManagedOperationParameters
	public void initTableColumnsMap() {
		// invoke this method only via MBean Server. As we do this, running a new Thread is allowed here.
		SpringApplicationContextHolder.addSpringReadyListener(new SpringReadyListener() {
			@Override
			public int getMinReadyState() {
				return 3;
			}
			@Override
			public void springIsReady() {
				final Thread t = new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							Thread.sleep(3000);
						} catch (InterruptedException e) {
							// ignore
						}
						buildTableColumnsMap();
					}
				}, "SchemaCache.initTableColumnsMap");
				t.setDaemon(true);
				t.setPriority(Thread.MIN_PRIORITY);
				t.start();
			}
		});
	}

   private Map<String, Table> buildTableColumnsMap() {
		try {
			for (DatasourceVO dsvo : datasourceCache.getAll()) {
				final XMLContentHandler xmlContentHandler = new XMLContentHandler();
				XMLUtils.parse(dsvo.getSource(), xmlContentHandler, xmlContentHandler, RESOLVER, false);
			}
		}
		catch (SAXException e) {
			throw new NuclosFatalException(e);
		}
	   return mpTableColumns;
   }

   // ***** static **********************************************************************

   public synchronized void fillTableColumnsAndConstraints(final Table table) {
	   final Table t;
	   if (mpTableColumns.containsKey(table.getName())) {
		   t = mpTableColumns.get(table.getName());
	   } else {
		   t = (Table)table.clone();
		   t.getColumns().clear();
		   t.getConstraints().clear();
		   getConstraints(t, getColumns(t));
		   mpTableColumns.put(t.getName(), t);
	   }
	   table.getColumns().clear();
	   table.getConstraints().clear();
	   table.getColumns().addAll(t.getColumns());
	   table.getConstraints().addAll(t.getConstraints());
   }

   public DbTable getColumns(final Table table) {
      if (table.isQuery()) {
         new QueryTable().setAllQueryColumns(table);
         return null;
      }
	 return readColumnsIntoTable(table);
   }

   synchronized void getConstraints(final Table table, final DbTable tableMetaData) {
   	if (tableMetaData != null) {
   		DbArtifact.acceptAll(tableMetaData.getTableArtifacts(DbConstraint.class), new AbstractDbArtifactVisitor<Void>() {
   			@Override
   			public Void visitPrimaryKeyConstraint(DbPrimaryKeyConstraint constraint) {
   				registerConstraint(constraint, new Constraint(table, constraint.getConstraintName(), ConstraintEmumerationType.PRIMARY_KEY));
   				return null;
   			}
   			@Override
   			public Void visitForeignKeyConstraint(DbForeignKeyConstraint constraint) throws DbException {
               final String sReferentialOwner = null; // TODO_AUTOSYNC: add schema to db artifacts
               registerConstraint(constraint, new ReferentialContraint(table, constraint.getConstraintName(),
               	sReferentialOwner, constraint.getReferencedConstraintName(), ConstraintEmumerationType.FOREIGN_KEY));
   				return null;
   			}
   			// No Indices (NUCLOS-3226)
//   			@Override
//   			public Void visitUniqueConstraint(DbUniqueConstraint constraint) {
//   				registerConstraint(constraint, new Constraint(table, constraint.getConstraintName(), ConstraintEmumerationType.UNIQUE));
//   				return null;
//   			}
   			private void registerConstraint(DbConstraint dbConstraint, Constraint constraint) {
   				for (String columnName : dbConstraint.getColumnNames()) {
   					Column column = table.getColumn(columnName);
   					if (column == null) {
   						throw new NuclosFatalException("SchemaCache does not find column " + columnName + " for constraint " + constraint.getName());
   					}
   					constraint.addColumn(column);
   				}
   				table.addConstraint(constraint);
   			}
   		});
      }
   }

   private DbTable readColumnsIntoTable(final Table table) {
   	// TODO_AUTOSYNC: Other schema

	if (!table.getColumns().isEmpty()) {
		// no need to read columns from db
		return null;
	}
	   
   	DbTable dbTable = dataBaseHelper.getDbAccess().getTableMetaData(table.getName());
   	for (DbColumn dbColumn : dbTable.getTableArtifacts(DbColumn.class)) {
   		final String name = dbColumn.getColumnName();
   		final DbColumnType columnType = dbColumn.getColumnType();
   		DataType type = null;
   		if (columnType.getGenericType() != null) {
   			switch (columnType.getGenericType()) {
   			case NUMERIC:
   				type = DataType.NUMERIC;
   				if (columnType.getPrecision() != null) { // could be null (view)
	   				if (columnType.getPrecision() <= 9 && columnType.getScale() == 0)
	   					type = DataType.INTEGER;
   				}
   				break;
   			case DATE:
   				type = DataType.DATE;
   				break;
   			case DATETIME:
   				type = DataType.TIMESTAMP;
   				break;
   			}
   		}
         final Column column = new Column(table, name,
         	type != null ? type : DataType.VARCHAR,
         	columnType.getLength() != null ? columnType.getLength() : 0,
         	columnType.getPrecision() != null ? columnType.getPrecision() : -1,
         	columnType.getScale() != null ? columnType.getScale() : -1,
      		dbColumn.getNullable() == DbNullable.NULL);
         column.setComment(dbColumn.getComment());
         table.addColumn(column);
   	}
   	return dbTable;
   }

   private void addToSchema(Schema schema) {
		//DatasourceLocalHome datasourceHome = (DatasourceLocalHome) ServerServiceLocator.getInstance().getLocalHome(DatasourceLocalHome.JNDI_NAME);
		//try {
			//for (Iterator i = datasourceHome.findAll().iterator(); i.hasNext();) {
			//@todo is it the right way to ignore user permissions here?
			for (DatasourceVO  voDatasource : datasourceCache.getAllDatasources()) {
				//DatasourceLocal datasource = (DatasourceLocal) i.next();
				//DatasourceVO voDatasource = datasource.getValueObject();
//				if (voDatasource.getPermission() != DatasourceVO.PERMISSION_NONE) {
				String sName = voDatasource.getName();
				String sViewName = null;
				String sComment = voDatasource.getDescription();
				Table table = new Table(schema.getTimestamp(), sName, sViewName);
				table.setQuery(true);
				table.setType(QueryTable.QUERY_TYPE_REPORT);
				table.setDatasourceVO(voDatasource);
				table.setComment(sComment);
				schema.addTable(table);
//				}
			}
	  /*}
		catch (FinderException e) {
			throw new NuclosFatalException(e);
		}*/
			for (DynamicEntityVO dynamicEntityVO : datasourceCache.getAllDynamicEntities()) {
				String sName = dynamicEntityVO.getName();
				String sViewName = null;
				String sComment = dynamicEntityVO.getDescription();
				Table table = new Table(schema.getTimestamp(), sName, sViewName);
				table.setQuery(true);
				table.setType(QueryTable.QUERY_TYPE_DYNAMIC_ENTITY);
				table.setDatasourceVO(dynamicEntityVO);
				table.setComment(sComment);
				schema.addTable(table);
			}

			for (ValuelistProviderVO valuelistProviderVO : datasourceCache.getAllValuelistProvider(false)) {
				String sName = valuelistProviderVO.getName();
				String sViewName = null;
				String sComment = valuelistProviderVO.getDescription();
				Table table = new Table(schema.getTimestamp(), sName, sViewName);
				table.setQuery(true);
				table.setType(QueryTable.QUERY_TYPE_VALUELIST_PROVIDER);
				table.setDatasourceVO(valuelistProviderVO);
				table.setComment(sComment);
				schema.addTable(table);
			}

			for (RecordGrantVO recordGrantVO : datasourceCache.getAllRecordGrant()) {
				String sName = recordGrantVO.getName();
				String sViewName = null;
				String sComment = recordGrantVO.getDescription();
				Table table = new Table(schema.getTimestamp(), sName, sViewName);
				table.setQuery(true);
				table.setType(QueryTable.QUERY_TYPE_RECORDGRANT);
				table.setDatasourceVO(recordGrantVO);
				table.setComment(sComment);
				schema.addTable(table);
			}

			for (DynamicTasklistVO dynamicTasklistVO : datasourceCache.getAllDynamicTasklists()) {
				String sName = dynamicTasklistVO.getName();
				String sViewName = null;
				String sComment = dynamicTasklistVO.getDescription();
				Table table = new Table(schema.getTimestamp(), sName, sViewName);
				table.setQuery(true);
				table.setType(QueryTable.QUERY_TYPE_DYNAMIC_TASK);
				table.setDatasourceVO(dynamicTasklistVO);
				table.setComment(sComment);
				schema.addTable(table);
			}


			for (ChartVO chartVO : datasourceCache.getAllCharts()) {
				String sName = chartVO.getName();
				String sViewName = null;
				String sComment = chartVO.getDescription();
				Table table = new Table(schema.getTimestamp(), sName, sViewName);
				table.setQuery(true);
				table.setType(QueryTable.QUERY_TYPE_CHART);
				table.setDatasourceVO(chartVO);
				table.setComment(sComment);
				schema.addTable(table);
			}
			
			for (CalcAttributeVO calcVO : datasourceCache.getAllCalcAttributes()) {
				String sName = calcVO.getName();
				String sViewName = null;
				String sComment = calcVO.getDescription();
				Table table = new Table(schema.getTimestamp(), sName, sViewName);
				table.setQuery(true);
				table.setType(QueryTable.QUERY_TYPE_CALC_ATTRIBUTE);
				table.setDatasourceVO(calcVO);
				table.setComment(sComment);
				schema.addTable(table);
			}

	}

	@Override
	public void invalidateCache(boolean notifyClients, boolean notifyClusterCloud) {
		this.invalidate(false, false);
		if(notifyClusterCloud) {
			notifyClusterCloud();
		}
	}

	@Override
	public void notifyClusterCloud() {
		NuclosClusterAction action = ClusterActionFactory.createClusterAction(NuclosClusterAction.Type.SCHEMA_ACTION);
		NuclosJMSUtils.sendObjectMessage(action, JMSConstants.TOPICNAME_CLUSTER, null);	
	}

	@Override
	public void registerCache() {
	}

	@Override
	public void deregisterCache() {
	}

	@Override
	public String getName() {
		return null;
	}
   
   public NamedTableConstraintCacheEntry getTableConstraintFromDbExceptionIfAny(DbException ex) {
	   try {
		   Map<String, NamedTableConstraintCacheEntry> mpConstraints = getTableConstraintMap();
		   if (ex instanceof DbNotNullableException) {
			   String constraintName = NamedTableConstraintCacheEntry.createNotNullName(((DbNotNullableException) ex).getTableName(), ((DbNotNullableException) ex).getColumnName());
			   return mpConstraints.get(constraintName);
		   }
		   
		   String errorMessage = ex.getSqlCause().getMessage().toUpperCase();
		   for (String constraintName : mpConstraints.keySet()) {
			   if (errorMessage.contains(constraintName)) {
				   return mpConstraints.get(constraintName);
			   }
		   }
	   } catch (Exception e) {
		   LOG.error(e.getMessage(), e);
	   }
	   
	   return null;
   }
   
   private Map<String, NamedTableConstraintCacheEntry> getTableConstraintMap() {
	   Map<String, NamedTableConstraintCacheEntry> result = mpTableConstraints;
	   if (result == null) {
		   result = new HashMap<>();
		   MetaDbHelper metaDbHelper = new MetaDbHelper(E.getSchemaHelperVersion(), metaProvider, null, null);
		   for (EntityMeta<?> eMeta : metaProvider.getAllEntities()) {
			   try {
				   DbTable table = metaDbHelper.getDbTable(eMeta);
				   addCacheEntries(eMeta, table, result);
			   } catch (Exception ex) {
				   LOG.error(ex.getMessage(), ex);
			   }
		   } 
		   mpTableConstraints = result;
	   }
	   return result;
   }
   
   private void addCacheEntries(EntityMeta<?> eMeta, DbTable table, Map<String, NamedTableConstraintCacheEntry> result) {
	   if (table == null) {
		   return;
	   }
	   for (DbForeignKeyConstraint constraint : table.getTableArtifacts(DbForeignKeyConstraint.class)) {
		   addCacheEntryIfNotNull(createCacheEntry(eMeta, table, constraint), result);
	   }
	   for (DbUniqueConstraint constraint : table.getTableArtifacts(DbUniqueConstraint.class)) {
		   addCacheEntryIfNotNull(createCacheEntry(eMeta, table, constraint), result);
	   }
//	   for (DbColumn col : table.getTableColumns()) {
//		   try {
//			   if (col.getNullable() == DbNullable.NOT_NULL) {
//				   String constraintName = NamedTableConstraintCacheEntry.createNotNullName(table.getTableName(), col.getColumnName());
//				   List<FieldMeta<?>> fields = new ArrayList<FieldMeta<?>>();
//				   fields.add(metaProvider.getEntityField(col.getUID()));
//				   NamedTableConstraintCacheEntry notNullConstraint = new NamedTableConstraintCacheEntry(constraintName, eMeta, fields, NamedTableConstraintCacheEntry.TYPE_NOTNULL);
//				   result.put(notNullConstraint.getConstraintName(), notNullConstraint);
//			   }
//		   } catch (Exception ex) {
//			   LOG.error(ex.getMessage(), ex);
//		   }
//	   }
   }
   
   private static void addCacheEntryIfNotNull(NamedTableConstraintCacheEntry cacheEntry, Map<String, NamedTableConstraintCacheEntry> result) {
	   if (cacheEntry != null) {
		   result.put(cacheEntry.getConstraintName(), cacheEntry);
	   }
   }
   
   private NamedTableConstraintCacheEntry createCacheEntry(EntityMeta<?> eMeta, DbTable table, DbConstraint constraint) {
	   try {
		   List<FieldMeta<?>> fields = new ArrayList<>();
		   
		   for (String colName : constraint.getColumnNames()) {
			   for (DbColumn col : table.getTableColumns()) {
				   if (LangUtils.equal(col.getColumnName(), colName)) {
					   FieldMeta<?> fMeta = metaProvider.getEntityField(col.getUID());
					   fields.add(fMeta);
				   }
			   }
		   }
		   
		   int iType = 0;
		   if (constraint instanceof DbForeignKeyConstraint) {
			   iType = NamedTableConstraintCacheEntry.TYPE_FOREIGN_KEY;
		   } else if (constraint instanceof DbUniqueConstraint) {
			   iType = NamedTableConstraintCacheEntry.TYPE_UNIQUE_KEY;
		   } else {
			   throw new IllegalArgumentException("Unsupported class " + constraint.getClass().getCanonicalName());
		   }
		   
		   return new NamedTableConstraintCacheEntry(constraint.getConstraintName(), eMeta, fields, iType);
	   } catch (Exception ex) {
		   LOG.error(ex.getMessage(), ex);
		   return null;
	   }
   }
   
}	// class SchemaCache
