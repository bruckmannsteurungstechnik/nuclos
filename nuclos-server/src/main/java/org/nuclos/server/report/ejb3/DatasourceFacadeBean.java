//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.report.ejb3;

import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;

import org.apache.commons.lang.BooleanUtils;
import org.nuclos.common.Actions;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.dal.DataSourceCaseSensivity;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.database.query.definition.Schema;
import org.nuclos.common.database.query.definition.Table;
import org.nuclos.common.dblayer.ResultSetMetaDataTransformer;
import org.nuclos.common.querybuilder.DatasourceUtils;
import org.nuclos.common.querybuilder.DatasourceXMLParser;
import org.nuclos.common.querybuilder.NuclosDatasourceException;
import org.nuclos.common.report.NuclosReportException;
import org.nuclos.common.report.ejb3.DatasourceFacadeRemote;
import org.nuclos.common.report.valueobject.CalcAttributeVO;
import org.nuclos.common.report.valueobject.ChartVO;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.DynamicEntityVO;
import org.nuclos.common.report.valueobject.DynamicTasklistVO;
import org.nuclos.common.report.valueobject.RecordGrantVO;
import org.nuclos.common.report.valueobject.ResultColumnVO;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common.valuelistprovider.VLPQuery;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoteException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.common.DatasourceCache;
import org.nuclos.server.common.DatasourceServerUtils;
import org.nuclos.server.common.MandatorUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.ServerServiceLocator;
import org.nuclos.server.common.SessionUtils;
import org.nuclos.server.common.ejb3.LocaleFacadeLocal;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.customcode.CustomCodeManager;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.datasource.DatasourceMetaParser;
import org.nuclos.server.datasource.DatasourceMetaVO;
import org.nuclos.server.datasource.DatasourceMetaVO.ColumnMeta;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.incubator.DbExecutor.LimitedResultSetRunner;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.report.ReportDatasourceObjectBuilder;
import org.nuclos.server.report.SchemaCache;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Datasource facade encapsulating datasource management. <br>
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Transactional(noRollbackFor = { Exception.class })
public class DatasourceFacadeBean extends NuclosFacadeBean implements DatasourceFacadeLocal, DatasourceFacadeRemote {

	private static final Logger LOG = LoggerFactory.getLogger(DatasourceFacadeBean.class);
	
	// Spring injection
	
	@Autowired
	private SpringDataBaseHelper dataBaseHelper;

	@Autowired
	private MandatorUtils mandatorUtils;
		
	private ReportDatasourceObjectBuilder builder;
	
	@Autowired
	private DatasourceServerUtils utils;

	@Autowired
	private MasterDataFacadeLocal masterDataFacade;

	@Autowired
	private CustomCodeManager ccm;
	
	@Autowired
	private SessionUtils sessionUtils;

	@Autowired
	private DatasourceMetaParser dsMetaParser;

	@Autowired
	private LocaleFacadeLocal localeFacade;
	
	// end of Spring injection
	
	private static enum DataSourceType {
		DYNAMICENTITY(E.DYNAMICENTITY, E.DYNAMICENTITYUSAGE, E.DYNAMICENTITYUSAGE.dynamicEntity.getUID(), E.DYNAMICENTITYUSAGE.dynamicEntityUsed.getUID()), 
		VALUELISTPROVIDER(E.VALUELISTPROVIDER, E.VALUELISTPROVIDERUSAGE, E.VALUELISTPROVIDERUSAGE.valuelistProvider.getUID(), E.VALUELISTPROVIDERUSAGE.valuelistProviderUsed.getUID()), 
		RECORDGRANT(E.RECORDGRANT, E.RECORDGRANTUSAGE, E.RECORDGRANTUSAGE.recordGrant.getUID(), E.RECORDGRANTUSAGE.recordGrantUsed.getUID()), 
		DATASOURCE(E.DATASOURCE, E.DATASOURCEUSAGE, E.DATASOURCEUSAGE.datasource.getUID(), E.DATASOURCEUSAGE.datasourceUsed.getUID()), 
		CHART(E.CHART, E.CHARTUSAGE, E.CHARTUSAGE.chart.getUID(), E.CHARTUSAGE.chartUsed.getUID()), 
		DYNAMICTASKLIST(E.DYNAMICTASKLIST, E.DYNAMICTASKLISTUSAGE, E.DYNAMICTASKLISTUSAGE.dynamictasklist.getUID(), E.DYNAMICTASKLISTUSAGE.dynamictasklistUsed.getUID()),
		CALCATTRIBUTE(E.CALCATTRIBUTE, E.CALCATTRIBUTEUSAGE, E.CALCATTRIBUTEUSAGE.calcAttribute.getUID(), E.CALCATTRIBUTEUSAGE.calcAttributeUsed.getUID()),;

		final EntityMeta<UID> entity;
		final EntityMeta<UID> entityUsage;
		final UID fieldEntityUID;
		final UID fieldEntityUsedUID;

		private DataSourceType(final EntityMeta<UID> entity, final EntityMeta<UID> entityUsage, final UID fieldEntityUID, final UID fieldEntityUsedUID) {
			this.entity = entity;
			this.entityUsage = entityUsage;
			this.fieldEntityUID = fieldEntityUID;
			this.fieldEntityUsedUID = fieldEntityUsedUID;
		}

		public DatasourceVO wrap(MasterDataVO<UID> deVO, final String sUser, UID mandator) {
			switch (this) {
			case DYNAMICENTITY:
				return MasterDataWrapper.getDynamicEntityVO(deVO);
			case VALUELISTPROVIDER:
				return MasterDataWrapper.getValuelistProviderVO(deVO);
			case RECORDGRANT:
				return MasterDataWrapper.getRecordGrantVO(deVO);
			case DYNAMICTASKLIST:
				return MasterDataWrapper.getDynamicTasklistVO(deVO);
			case CHART:
					return MasterDataWrapper.getChartVO(deVO);
			case CALCATTRIBUTE:
				return MasterDataWrapper.getCalcAttributeVO(deVO);
			default:
				return MasterDataWrapper.getDatasourceVO(deVO, sUser, mandator);
			}
		}

		public MasterDataVO<UID> unwrap(final DatasourceVO dsVO, final UID entityUID) throws CommonFinderException, CommonPermissionException {
			if (dsVO instanceof ValuelistProviderVO) {
				return MasterDataWrapper.wrapDatasourceVO((ValuelistProviderVO)dsVO, entityUID);
			}
			return MasterDataWrapper.wrapDatasourceVO(dsVO, entityUID);
		}

		public static DataSourceType getFromDatasourceVO(final DatasourceVO datasourceVO) {
			if (datasourceVO instanceof DynamicEntityVO) {
				return DataSourceType.DYNAMICENTITY;
			}
			else if (datasourceVO instanceof ValuelistProviderVO) {
				return DataSourceType.VALUELISTPROVIDER;
			}
			else if (datasourceVO instanceof RecordGrantVO) {
				return DataSourceType.RECORDGRANT;
			}
			else if (datasourceVO instanceof DynamicTasklistVO) {
				return DataSourceType.DYNAMICTASKLIST;
			}
			else if (datasourceVO instanceof ChartVO) {
				return DataSourceType.CHART;
			}
			else if (datasourceVO instanceof CalcAttributeVO) {
				return DataSourceType.CALCATTRIBUTE;
			}
			else {
				return DataSourceType.DATASOURCE;
			}
		}
	}

	public DatasourceFacadeBean() {
	}

	private final MasterDataFacadeLocal getMasterDataFacade() {
		return masterDataFacade;
	}
	
	private final ReportDatasourceObjectBuilder getReportDatasourceObjectBuilder() {
		return this.builder;
	}

	/**
	 * get all datasources
	 *
	 * @return set of datasources
	 * @throws CommonPermissionException
	 */
	@Override
	public Collection<DatasourceVO> getDatasources() throws CommonPermissionException {
		this.checkReadAllowed(E.REPORT, E.DATASOURCE);
		return DatasourceCache.getInstance().getAllDatasources(getCurrentUserName(), getCurrentMandatorUID());
	}
	
	@Override
	public DatasourceVO getDatasourceByName(String sDataSource) throws CommonBusinessException {
		try {
			for (DatasourceVO datasourceVO : getDatasources()) {
				if (datasourceVO.getName().equals(sDataSource))
					return datasourceVO;
			}
			return null;
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * get all datasources
	 *
	 * @return set of datasources
	 */
	@Override
	public Collection<DatasourceVO> getDatasourcesForCurrentUser() {
		return DatasourceCache.getInstance().getDatasourcesByCreator(getCurrentUserName(), getCurrentMandatorUID());
	}

	/**
	 * get datasource value object
	 *
	 * @param dataSourceUID
	 *            UID of datasource
	 * @return datasource value object
	 */
	@Override
	@RolesAllowed("Login")
	public DatasourceVO get(final UID dataSourceUID) throws CommonFinderException, CommonPermissionException {
		 return DatasourceCache.getInstance().getDatasourcesByUser(dataSourceUID, getCurrentUserName(), getCurrentMandatorUID());

		/*
		 * todo how should we handle permissions here? - It is necessary, that
		 * // all users have the right to execute inline datasources if
		 * (result.getPermission() == DatasourceVO.PERMISSION_NONE) { throw new
		 * CommonPermissionException(); } return result;
		 */
	}

	/**
	 * //TODO MULTINUCLET use get(UID)
	 * get datasource value object
	 *
	 * @param sDatasourceName
	 *            name of datasource
	 * @return datasource value object
	@RolesAllowed("Login")
	public DatasourceVO get(final String sDatasourceName) throws CommonFinderException, CommonPermissionException {
		return DatasourceCache.getInstance().getDatasourceByName(sDatasourceName);
	}
	 */

	/**
	 * get valuelist provider value object
	 *
	 * @param valuelistProviderUID
	 *            UID of valuelist provider
	 * @return valuelist provider value object
	 */
	@Override
	@RolesAllowed("Login")
	public ValuelistProviderVO getValuelistProvider(final UID valuelistProviderUID) throws CommonFinderException, CommonPermissionException {
		ValuelistProviderVO vlpvo = utils.getValuelistProvider(valuelistProviderUID);
		setLocaleResouces(vlpvo);
		return vlpvo;
	}

	/**
	 * get dynamic entity value object
	 *
	 * @param dynamicEntityUID
	 *            UID of dynamic entity 
	 * @return dynamic entity value object
	 */
	@Override
	@RolesAllowed("Login")
	public DynamicEntityVO getDynamicEntity(final UID dynamicEntityUID) throws CommonFinderException, CommonPermissionException {
		return DatasourceCache.getInstance().getDynamicEntity(dynamicEntityUID);
	}

	/**
	 * get chart value object
	 * 
	 * @param chartUID
	 *            UID of chart
	 * @return chart value object
	 */
	@Override
	@RolesAllowed("Login")
	public ChartVO getChart(final UID chartUID) throws CommonFinderException, CommonPermissionException {
		return DatasourceCache.getInstance().getChart(chartUID);
	}

	/**
	 * get all charts
	 */
	@Override
	@RolesAllowed("Login")
	public Collection<ChartVO> getCharts() {
		// this.checkReadAllowed(ENTITY_NAME_CHARTENTITY);
		return DatasourceCache.getInstance().getAllCharts();
	}
	
	@Override
	@RolesAllowed("Login")
	public CalcAttributeVO getCalcAttribute(final UID calcAttributeUID) throws CommonFinderException, CommonPermissionException {
		return DatasourceCache.getInstance().getCalcAttribute(calcAttributeUID);
	}
	
	@Override
	@RolesAllowed("Login")
	public Collection<CalcAttributeVO> getCalcAttributes() throws CommonPermissionException {
		// this.checkReadAllowed(ENTITY_NAME_CHARTENTITY);
		return DatasourceCache.getInstance().getAllCalcAttributes();
	}

	/**
	 * get a Datasource by UID regardless of permisssions
	 */
	@RolesAllowed("Login")
	public DatasourceVO getDatasource(final UID dataSourceUID) throws CommonFinderException, CommonPermissionException {
		return DatasourceCache.getInstance().get(dataSourceUID);
	}

	private boolean isEditSQLAllowed() {
		return SecurityCache.getInstance().isActionAllowed(getCurrentUserName(), Actions.ACTION_EDIT_DATASOURCE_SQL, getCurrentMandatorUID());
	}

	private void checkWriteAllowed(final DatasourceVO datasourcevo) throws CommonPermissionException {
		final boolean bEditSQLAllowed = isEditSQLAllowed();
		try {
			final DatasourceXMLParser.Result parseresult = utils.parseDatasourceXML(datasourcevo.getSource());
			if(!parseresult.isModelUsed() && !bEditSQLAllowed) {
				throw new CommonPermissionException("edit.datasource.sql.not.allowed");
			}
			for (DatasourceParameterVO parameterVO : parseresult.getLstParameters()) {
				if ("SQL".equals(parameterVO.getDatatype()) && !bEditSQLAllowed) {
					throw new CommonPermissionException("edit.datasource.sql.param.not.allowed");
				}
			}
		} catch (NuclosDatasourceException e) {
			// Usually no problem here: The validation takes place before the check
			throw new NuclosFatalException(e);
		}
	}

	/**
	 * create new datasource
	 *
	 * @param datasourcevo
	 *            value object
	 * @return new datasource
	 * @throws CommonFinderException 
	 */
	@Override
	public DatasourceVO create(final DatasourceVO datasourcevo, final IDependentDataMap dependants, final List<UID> lstUsedDataSourceUIDs) throws CommonCreateException, CommonValidationException, NuclosBusinessRuleException, CommonPermissionException, CommonFinderException {
		final DataSourceType type = DataSourceType.getFromDatasourceVO(datasourcevo);
		EntityMeta<UID> entity = type.entity;
		this.checkWriteAllowed(entity);

		datasourcevo.setPrimaryKey(new UID());

		datasourcevo.validate();
		updateValidFlag(datasourcevo);
		this.checkWriteAllowed(datasourcevo);

		if (E.RECORDGRANT.equals(entity)) {
			final RecordGrantVO rg = (RecordGrantVO) datasourcevo;
			try {
				if (rg.getValid()) {
					String sqlSelect = createSQL(rg);
					DatasourceUtils.validateRecordGrantSQL(rg.getEntityUID(), sqlSelect, getColumns(sqlSelect));
				}
			}
			catch (NuclosDatasourceException e1) {
				throw new CommonFatalException(e1);
			}
		}

		if (E.DYNAMICENTITY.equals(entity)) {
			processChangingDynamicEntity((DynamicEntityVO) datasourcevo, null);
		}

		if (E.CHART.equals(entity)) {
			processChangingChartEntity((ChartVO) datasourcevo, null);
		}

		if (E.DYNAMICTASKLIST.equals(entity)) {
			processChangingDynamicTasklist((DynamicTasklistVO) datasourcevo, null);
		}

		final MasterDataVO<UID> mdVO = getMasterDataFacade().create(type.unwrap(datasourcevo, entity.getUID()), null);
		getMasterDataFacade().notifyClients(entity.getUID());

		final DatasourceVO dbDataSourceVO = MasterDataWrapper.getDatasourceVO(mdVO, getCurrentUserName(), getCurrentMandatorUID());

		try {
			replaceUsedDatasourceList(dbDataSourceVO, lstUsedDataSourceUIDs);
		}
		catch (CommonFinderException e) {
			throw new CommonFatalException(e);
		}
		catch (CommonRemoveException e) {
			throw new CommonFatalException(e);
		}
		catch (CommonStaleVersionException e) {
			throw new CommonFatalException(e);
		}
		invalidateCaches(type);
		return setLocaleResouces(type.wrap(mdVO, getCurrentUserName(), getCurrentMandatorUID()));
	}

	/**
	 * modify an existing datasource without usages
	 *
	 * @param datasourcevo
	 * @throws CommonFinderException
	 * @throws CommonPermissionException
	 * @throws CommonStaleVersionException
	 * @throws CommonValidationException
	 */
	@Override
	public void modify(DatasourceVO datasourcevo) throws CommonFinderException, CommonPermissionException, CommonStaleVersionException, CommonValidationException, NuclosBusinessRuleException {
		modify(datasourcevo, null, null, false);
	}

	/**
	 * modify an existing datasource
	 *
	 * @param datasourcevo
	 *            value object
	 * @return modified datasource
	 */
	@Override
	public DatasourceVO modify(final DatasourceVO datasourcevo, final IDependentDataMap dependants, final List<UID> lstUsedDataSourceUIDs) throws CommonFinderException, CommonPermissionException, CommonStaleVersionException, CommonValidationException,
			NuclosBusinessRuleException, CommonRemoteException {

		return modify(datasourcevo, dependants, lstUsedDataSourceUIDs, true);
	}

	private DatasourceVO modify(final DatasourceVO datasourcevo, final IDependentDataMap dependants, final List<UID> lstUsedDataSourceUIDs, boolean modifyUsedDatasources) throws CommonFinderException, CommonPermissionException,
			CommonStaleVersionException, CommonValidationException, NuclosBusinessRuleException, CommonRemoteException {
		final DataSourceType type = DataSourceType.getFromDatasourceVO(datasourcevo);
		final EntityMeta<UID> entity = type.entity;
		this.checkWriteAllowed(entity);

		datasourcevo.validate();
		
		// validate used data sources first to avoid mistaken java.lang.StackOverflowError: alreadyProcessed
		if (lstUsedDataSourceUIDs != null && !lstUsedDataSourceUIDs.isEmpty()) {
			for(UID uid : lstUsedDataSourceUIDs) {
				DatasourceVO usedDS = DatasourceCache.getInstance().get(uid);
				try {
					validateSqlFromXML(usedDS);
				} catch (NuclosDatasourceException ex) {
					throw new CommonRemoteException(ex);
				}
			}
		}
		
		if (E.DYNAMICENTITY.equals(entity)) {
			try { //validation before updateValidFlag for better validation messages
				String sqlSelect = createSQL(datasourcevo);
				DatasourceUtils.validateDynEntitySQL(sqlSelect, null, dataBaseHelper.getDbAccess().getTableAliasing());
			} catch (CommonValidationException ex) {
				throw ex;
			} catch (Exception ex) {
				// ignore here
			}
		}
		
		updateValidFlag(datasourcevo);
		this.checkWriteAllowed(datasourcevo);

		try {
			validateUniqueConstraint(datasourcevo);

			final String sUser = getCurrentUserName();
			MasterDataVO<UID> dsAsMd = getMasterDataFacade().get(entity, datasourcevo.getPrimaryKey());

			if (E.DATASOURCE.equals(entity)) {
				if (DatasourceCache.getInstance().getPermission(dsAsMd.getPrimaryKey(), sUser, getCurrentMandatorUID()) != DatasourceVO.PERMISSION_READWRITE) {
					throw new CommonPermissionException();
				}
			}
			
			if (E.RECORDGRANT.equals(entity)) {
				RecordGrantVO rg = (RecordGrantVO) datasourcevo;
				try {
					if (rg.getValid()) {
						final String sqlSelect = createSQL(rg);
						DatasourceUtils.validateRecordGrantSQL(rg.getEntityUID(), sqlSelect, getColumns(sqlSelect));
					}
				}
				catch (NuclosDatasourceException e1) {
					throw new CommonFatalException(e1);
				}
			}

			if (E.DYNAMICENTITY.equals(entity)) {
				processChangingDynamicEntity((DynamicEntityVO) datasourcevo, (DynamicEntityVO) type.wrap(dsAsMd, sUser, getCurrentMandatorUID()));
			}

			if (E.CHART.equals(entity)) {
				processChangingChartEntity((ChartVO) datasourcevo, (ChartVO) type.wrap(dsAsMd, sUser, getCurrentMandatorUID()));
			}

			if (E.DYNAMICTASKLIST.equals(entity)) {
				processChangingDynamicTasklist((DynamicTasklistVO) datasourcevo, (DynamicTasklistVO) type.wrap(dsAsMd, sUser, getCurrentMandatorUID()));
			}

			MasterDataVO<UID> unwrapped = type.unwrap(datasourcevo, entity.getUID());

			if (dsAsMd.getVersion() != datasourcevo.getVersion()) {
				throw new CommonStaleVersionException(getVersionConflictMessages("datasource", unwrapped.getEntityObject(), dsAsMd.getEntityObject()));
			}
			getMasterDataFacade().modify(unwrapped, null);

			if (modifyUsedDatasources) {
				// store the list of used datasources
				replaceUsedDatasourceList(datasourcevo, lstUsedDataSourceUIDs);
			}

			invalidateCaches(type);

			dsAsMd = getMasterDataFacade().get(entity, datasourcevo.getPrimaryKey());
			return setLocaleResouces(type.wrap(dsAsMd, sUser, getCurrentMandatorUID()));
		}
		catch (CommonRemoveException e) {
			throw new NuclosFatalException(e);
		}
		catch (CommonCreateException e) {
			throw new NuclosFatalException(e);
		}
		catch (CommonFatalException e) {
			throw new NuclosFatalException(e);
		}
	}

	/**
	 *
	 * @param newDEVO
	 * @param oldDEVO
	 * @throws CommonValidationException
	 *             (only if validate is true)
	 */
	private void processChangingDynamicEntity(DynamicEntityVO newDEVO, DynamicEntityVO oldDEVO) throws CommonValidationException {
		String sqlSelect;
		if (newDEVO != null) {
			try {
				sqlSelect = createSQL(newDEVO);
				newDEVO.setQuery(sqlSelect);
				LOG.debug("validate dynamic entity sql <SQL>{}</SQL>", sqlSelect);

				final DatasourceMetaVO dsmetaOld = dsMetaParser.parse(oldDEVO);
				final DatasourceMetaTransformer transformer = new DatasourceMetaTransformer(newDEVO, dsmetaOld, E.DYNAMICENTITY);
				final DatasourceMetaVO dsmetaNew = DatasourceUtils.validateDynEntitySQL(sqlSelect, transformer, dataBaseHelper.getDbAccess().getTableAliasing());
				if (dsmetaNew != null) {
					newDEVO.setMeta(dsmetaNew.toString());
				}
			}
			catch (NuclosDatasourceException e) {
				throw new CommonFatalException(e);
			}
		}
	}

	/**
	 * 
	 * @param newDEVO
	 * @param oldDEVO
	 * @throws CommonValidationException
	 *             (only if validate is true)
	 */
	private void processChangingChartEntity(ChartVO newDEVO, ChartVO oldDEVO) throws CommonValidationException {
		String sqlSelect;
		if (newDEVO != null) {
			try {
				LOG.debug("validate chart entity name \"{}\"", newDEVO.getName());
				sqlSelect = createSQL(newDEVO);
				newDEVO.setQuery(sqlSelect);
				LOG.debug("validate chart entity sql <SQL>{}</SQL>",  sqlSelect);

				final DatasourceMetaVO dsmetaOld = dsMetaParser.parse(oldDEVO);
				final DatasourceMetaTransformer transformer = new DatasourceMetaTransformer(newDEVO, dsmetaOld, E.CHART);
				DatasourceMetaVO dsmetaNew;
				if (newDEVO.getParentEntityUID() != null) {
					dsmetaNew = DatasourceUtils.validateDynEntitySQL(sqlSelect, transformer, dataBaseHelper.getDbAccess().getTableAliasing());
				} else {
					dsmetaNew = DatasourceUtils.validateChartEntitySQL(sqlSelect, 
							getColumns(createSQL(newDEVO, getTestParameters(newDEVO.getSource()))), transformer);
					dsmetaNew = DatasourceUtils.validateChartEntitySQLWithParameters(
							createSQL(newDEVO, getTestParameters(newDEVO.getSource())), transformer);
				}

				if (dsmetaNew != null) {
					newDEVO.setMeta(dsmetaNew.toString());
				}
			}
			catch (NuclosDatasourceException e) {
				throw new CommonFatalException(e);
			}
		}
	}

	/**
	 *
	 * @param newDTVO
	 * @param oldDTVO
	 * @throws CommonValidationException
	 *             (only if validate is true)
	 */
	private void processChangingDynamicTasklist(DynamicTasklistVO newDTVO, DynamicTasklistVO oldDTVO) throws CommonValidationException {
		String sqlSelect;
		if (newDTVO != null) {
			try {
				sqlSelect = createSQL(newDTVO);
				newDTVO.setQuery(sqlSelect);
				LOG.debug("validate dynamic task sql <SQL>{}</SQL>", sqlSelect);

				final DatasourceMetaVO dsmetaOld = dsMetaParser.parse(oldDTVO);
				final DatasourceMetaTransformer transformer = new DatasourceMetaTransformer(newDTVO, dsmetaOld, E.DYNAMICTASKLIST);
				DatasourceMetaVO dsmetaNew = null;
				try {
					dsmetaNew = DatasourceUtils.validateDynTasklistSQL(sqlSelect, transformer, dataBaseHelper.getDbAccess().getTableAliasing());
				} catch (Exception e) {
					LOG.warn("Metadata creation for SQL SELECT {} failed:\n{}", sqlSelect, e);
					throw e;
				}
				if (dsmetaNew != null) {
					newDTVO.setMeta(dsmetaNew.toString());
				}
			}
			catch (NuclosDatasourceException e) {
				throw new CommonFatalException(e);
			}
		}
	}

	private void validateUniqueConstraint(DatasourceVO datasourcevo) throws CommonValidationException {
		try {
			DatasourceVO foundDatasourceVO = this.getDatasource(datasourcevo.getPrimaryKey());
			if (foundDatasourceVO != null && !datasourcevo.getId().equals(foundDatasourceVO.getId())) {
				throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage("validation.unique.constraint", "Name", "Data source"));
			}
		}
		catch (CommonFinderException e) {
			// No element found -> validation O.K.
		}
		catch (CommonPermissionException e) {
			// No element found -> validation O.K.
		}
	}

	/**
	 * @param datasourceVO
	 * @param referencedDataSourceUIDs
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 * @throws CommonStaleVersionException
	 * @throws CommonRemoveException
	 * @throws NuclosBusinessRuleException
	 * @throws CommonCreateException
	 * @throws NuclosCompileException 
	 */
	private void replaceUsedDatasourceList(final DatasourceVO datasourceVO, final List<UID> referencedDataSourceUIDs) 
			throws CommonFinderException, CommonPermissionException, NuclosBusinessRuleException, CommonRemoveException,
			CommonStaleVersionException, CommonCreateException {
		final DataSourceType datasourceType = DataSourceType.getFromDatasourceVO(datasourceVO);
		

		// 1. remove all entries for this id:
		CollectableComparison condUsage = SearchConditionUtils.newKeyComparison(datasourceType.fieldEntityUID, ComparisonOperator.EQUAL, datasourceVO.getPrimaryKey());
		List<UID> lstIds = getMasterDataFacade().getMasterDataIdsNoCheck(datasourceType.entityUsage.getUID(), new CollectableSearchExpression(condUsage));
		for (UID id : lstIds) {
			getMasterDataFacade().remove(datasourceType.entityUsage.getUID(), id, false, null);
		}

		// 2. insert the new entries:
		for (final UID dataSourceUID : referencedDataSourceUIDs) {
			EntityObjectVO<UID> vo = new EntityObjectVO<UID>(datasourceType.entityUsage);
			MasterDataVO<UID> newEntryVO = new MasterDataVO<UID>(vo, true);
			newEntryVO.setFieldUid(datasourceType.fieldEntityUID, datasourceVO.getPrimaryKey());
			newEntryVO.setFieldUid(datasourceType.fieldEntityUsedUID, dataSourceUID);

			getMasterDataFacade().create(newEntryVO, null);
		}
	}

	/**
	 * get a list of DatasourceVO which uses the datasource with the given UID
	 *
	 * ONLY for datasources, NOT for dynamic entities and valuelist provider
	 *
	 * @param dataSourceUID data source UID
	 * @see #getUsagesForDatasource(DatasourceVO)
	 */
	@Override
	@RolesAllowed("Login")
	public List<DatasourceVO> getUsagesForDatasource(final UID dataSourceUID) throws CommonFinderException, CommonPermissionException {
		return getUsagesForDatasource(get(dataSourceUID));
	}

	/**
	 * get a list of DatasourceVO which uses the datasource
	 *
	 * @param datasourceVO
	 *            could also be an instance of <code>DynamicEntityVO</code> or
	 *            <code>ValuelistProviderVO</code>
	 * @return
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	@Override
	@RolesAllowed("Login")
	public List<DatasourceVO> getUsagesForDatasource(final DatasourceVO datasourceVO) throws CommonFinderException, CommonPermissionException {
		final DataSourceType datasourceType = DataSourceType.getFromDatasourceVO(datasourceVO);

		final List<DatasourceVO> result = new ArrayList<DatasourceVO>();
		
		CollectableComparison cond = SearchConditionUtils.newKeyComparison(datasourceType.fieldEntityUsedUID, ComparisonOperator.EQUAL, datasourceVO.getPrimaryKey());

		//CollectableComparison cond = SearchConditionUtils.newMDReferenceComparison(MasterDataMetaCache.getInstance().getMetaData(datasourceType.entityUsage), datasourceType.fieldEntityUsedUID, datasourceVO.getPrimaryKey());

		for (MasterDataVO<UID> usageVO : getMasterDataFacade().getMasterData(datasourceType.entityUsage, cond)) {
			MasterDataVO<UID> deVO = getMasterDataFacade().get(datasourceType.entity, usageVO.getFieldUid(datasourceType.fieldEntityUsedUID));
			if (!datasourceVO.getId().equals(deVO.getPrimaryKey())) {
				result.add(setLocaleResouces(datasourceType.wrap(deVO, getCurrentUserName(), getCurrentMandatorUID())));
			}
		}

		return result;
	}

	/**
	 * get a list of DatasourceCVO which are used by the datasource with the
	 * given UID
	 *
	 * @param dataSourceUID data source UID
	 * @return
	 * @throws CommonFinderException
	 * @throws CommonPermissionException
	 */
	@Override
	public List<DatasourceVO> getUsingByForDatasource(final UID dataSourceUID) throws CommonFinderException, CommonPermissionException {

		final DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		final DbQuery<DbTuple> query = builder.createTupleQuery();
		final DbFrom<UID> t = query.from(E.DATASOURCEUSAGE);
		query.multiselect(t.baseColumn(SF.PK_UID), t.baseColumn(E.DATASOURCEUSAGE.datasource), t.baseColumn(E.DATASOURCEUSAGE.datasourceUsed));
		query.where(builder.equalValue(t.baseColumn(E.DATASOURCEUSAGE.datasource), dataSourceUID));

		final List<DatasourceVO> result = new ArrayList<DatasourceVO>();
		// TODO MULTINUCLET use DbTuple<UID>
		for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query)) {
			try {
				final UID dataSourceUsedUID = tuple.get(2, UID.class);
				final MasterDataVO<UID> mdVO = getMasterDataFacade().get(E.DATASOURCE, dataSourceUsedUID);
				result.add(MasterDataWrapper.getDatasourceVO(mdVO, getCurrentUserName(), getCurrentMandatorUID()));
			}
			catch (CommonFinderException ex) {
				throw new NuclosFatalException(ex);
			}
			catch (CommonPermissionException ex) {
				throw new NuclosFatalException(ex);
			}
		}
		return result;
	}

	/**
	 * delete an existing datasource
	 *
	 * @param datasourcevo
	 *            value object
	 */
	@Override
	public void remove(DatasourceVO datasourcevo) 
			throws CommonFinderException, CommonRemoveException, CommonPermissionException, 
			CommonStaleVersionException, NuclosBusinessRuleException {
		
		DataSourceType type = DataSourceType.getFromDatasourceVO(datasourcevo);
		EntityMeta<UID> entity = type.entity;
		this.checkDeleteAllowed(entity);

		MasterDataVO<UID> dsAsMd = getMasterDataFacade().get(entity, datasourcevo.getId());

		if (E.DATASOURCE.equals(entity)) {
			if (DatasourceCache.getInstance().getPermission(dsAsMd.getPrimaryKey(), getCurrentUserName(), getCurrentMandatorUID()) != DatasourceVO.PERMISSION_READWRITE) {
				throw new CommonPermissionException();
			}
		}

		if (dsAsMd.getVersion() != datasourcevo.getVersion()) {
			throw new CommonStaleVersionException(dsAsMd.getEntityObject(), datasourcevo.getVersion());
		}

		getMasterDataFacade().remove(entity.getUID(), datasourcevo.getPrimaryKey(), false, null);
		getMasterDataFacade().notifyClients(entity.getUID());

		try {
			replaceUsedDatasourceList(datasourcevo, Collections.<UID> emptyList());
		}
		catch (CommonCreateException e) {
			throw new CommonFatalException(e);
		}

		invalidateCaches(type);
	}

	/**
	 * @param datasourcevo
	 */
	private void updateValidFlag(final DatasourceVO datasourcevo) throws CommonValidationException {
		try {
			this.validateSqlFromXML(datasourcevo);
			datasourcevo.setValid(Boolean.TRUE);
		}
		catch (CommonValidationException e) {
			//LOG.info("updateValidFlag: " + e);
			datasourcevo.setValid(Boolean.FALSE);
			throw e;
		}
		catch (NuclosDatasourceException e) {
			//LOG.info("updateValidFlag: " + e);
			datasourcevo.setValid(Boolean.FALSE);
			throw new NuclosFatalException(e);
		}
	}

	/**
	 * Retrieve the parameters a datasource accepts.
	 *
	 * @param sDatasourceXML
	 * @return
	 * @throws NuclosFatalException
	 * @throws NuclosDatasourceException
	 */
	@Override
	@RolesAllowed("Login")
	public List<DatasourceParameterVO> getParametersFromXML(String sDatasourceXML) throws NuclosFatalException, NuclosDatasourceException {
		return utils.getParameters(sDatasourceXML);
	}

	/**
	 * Retrieve the parameters a datasource accepts.
	 *
	 * @param dataSourceUID
	 * @return
	 * @throws NuclosFatalException
	 * @throws NuclosDatasourceException
	 */
	@Override
	@RolesAllowed("Login")
	public List<DatasourceParameterVO> getParameters(final UID dataSourceUID) throws NuclosFatalException, NuclosDatasourceException {
		return utils.getParameters(dataSourceUID);
	}
	
	@Override
	@RolesAllowed("Login")
	public List<DatasourceParameterVO> getCalcAttributeParameters(final UID calcAttributeUID) throws NuclosFatalException, NuclosDatasourceException {
		return utils.getCalcAttributeParameters(calcAttributeUID);
	}

	/**
	 * validate the given DatasourceXML
	 *
	 * @param sDatasourceXML
	 */
	@Override
	@RolesAllowed("Login")
	public void validateSqlFromXML(DatasourceVO datasourcevo) throws CommonValidationException, NuclosDatasourceException {
		String sSql = this.createSQL(datasourcevo, this.getTestParameters(datasourcevo.getSource()));
		if (datasourcevo instanceof DynamicEntityVO || datasourcevo instanceof ChartVO) {
			final String refCol;
			if (new DataSourceCaseSensivity(sSql).isIntidGenericObjectCaseInsensitive()) {
				refCol = DataSourceCaseSensivity.REF_ENTITY;
			} else {
				refCol = "\"" + DataSourceCaseSensivity.REF_ENTITY + "\"";
			}
			sSql = "SELECT * FROM ("+sSql+") " + dataBaseHelper.getDbAccess().getTableAliasing() + " validateSqlFromXML WHERE validateSqlFromXML." + refCol + " = 123";
		}
		this.validateSql(sSql, null);
	}

	/**
	 * validate the given SQL
	 *
	 * @param sql
	 */
	@Override
	@RolesAllowed("Login")
	public <T> T validateSql(String sql, ResultSetMetaDataTransformer<T> transformer) throws CommonValidationException, NuclosDatasourceException {
		try {
			return dataBaseHelper.getDbAccess().checkSyntax(sql, transformer);
		}
		catch (DbException e) {
			throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage("datasource.error.invalid.statement", e.getMessage()), e);// "Die Abfrage ist ung\u00ef\u00bf\u00bdltig.\n"
		}
	}

	/**
	 * get sql string for datasource definition
	 *
	 * @param dataSourceUID
	 *            id of datasource
	 * @return string containing sql
	 */
	@Override
	public String createSQL(final UID dataSourceUID, final Map<String, Object> mpParams) throws NuclosDatasourceException {
		return utils.createSQL(dataSourceUID, mpParams, null, null);
	}
	
	public String createSQL(final UID dataSourceUID, final Map<String, Object> mpParams, UID mandatorUID, UID language) throws NuclosDatasourceException {
		return utils.createSQL(dataSourceUID, mpParams, mandatorUID, language);
	}

	/**
	 * get sql string for datasource definition without parameter definition
	 *
	 * @param sDatasourceXML
	 *            xml of datasource
	 * @return string containing sql
	 */
	@Override
	@RolesAllowed("Login")
	public String createSQL(final DatasourceVO datasourcevo) throws NuclosDatasourceException {
		return utils.createSQL(datasourcevo);
	}

	@RolesAllowed("Login")
	public String createSQL(final DatasourceVO datasourcevo, final Map<String, Object> mpParams) throws NuclosDatasourceException {
		return utils.createSQL(datasourcevo, mpParams, null, null);
	}
	
	public String createSQL(final DatasourceVO datasourcevo, final Map<String, Object> mpParams, UID mandatorUID, UID language) throws NuclosDatasourceException {
		return utils.createSQL(datasourcevo, mpParams, mandatorUID, language);
	}

	/**
	 * get sql string for report execution. check that this method is only
	 * called by the local interface as there is no authorization applied.
	 *
	 * @return string containing sql
	 */
	public String createSQLForReportExecution(final UID dataSourceUID, final Map<String, Object> mpParams, UID language) throws NuclosDatasourceException {
		if (isCalledRemotely()) {
			// just to ensure it won't be used in remote interface
			throw new NuclosFatalException("Invalid remote call");
		}

		return utils.createSQLForReportExecution(dataSourceUID, mpParams, language);
	}

	/**
	 * //TODO MULTINUCLET 
	 * 
	 * set test values for every parameter for sysntax check
	 *
	 * @param sDatasourceXML
	 * @return Map<String sName, String sValue>
	 */
	private Map<String, Object> getTestParameters(final String sDatasourceXML) {
		final Map<String, Object> result = new HashMap<String, Object>();

		final List<DatasourceParameterVO> lstParams;
		try {
			lstParams = this.getParametersFromXML(sDatasourceXML);
		}
		catch (NuclosDatasourceException e) {
			// No parameters defined?
			return result;
		}

		for (final DatasourceParameterVO paramvo : lstParams) {
			final String sValue = DatasourceUtils.getTestValueForParameter(paramvo, getBooleanRepresentationInSQL(Boolean.FALSE));
			result.put(paramvo.getParameter(), sValue);
			if (paramvo.getValueListProvider() != null) {
				result.put(paramvo.getParameter() + "Id", "123434");
			}
		}
		result.put("username", getCurrentUserName());

		return result;
	}

	/**
	 * invalidate datasource cache
	 */
	@Override
	@RolesAllowed("Login")
	public void invalidateCache() {
		DatasourceCache.getInstance().invalidateCache(true, true);
	}

	/**
	 * get all DynamicEntities
	 */
	@Override
	@RolesAllowed("Login")
	public Collection<DynamicEntityVO> getDynamicEntities() {
		// this.checkReadAllowed(ENTITY_NAME_DYNAMICENTITY);
		return DatasourceCache.getInstance().getAllDynamicEntities();
	}


	/**
	 * get all ValuelistProvider
	 *
	 * @return set of ValuelistProviderVO
	 * @throws CommonPermissionException
	 */
	@Override
	@RolesAllowed("Login")
	public Collection<ValuelistProviderVO> getValuelistProvider() throws CommonPermissionException {
		// this.checkReadAllowed(ENTITY_NAME_VALUELISTPROVIDER);
		return DatasourceCache.getInstance().getAllValuelistProvider(true);
	}


	/**
	 * get all RecordGrant
	 *
	 * @return set of RecordGrantVO
	 * @throws CommonPermissionException
	 */
	@Override
	@RolesAllowed("Login")
	public Collection<RecordGrantVO> getRecordGrant() throws CommonPermissionException {
		// this.checkReadAllowed(ENTITY_NAME_VALUELISTPROVIDER);
		return DatasourceCache.getInstance().getAllRecordGrant();
	}

	/**
	 * get RecordGrant value object
	 *
	 * @param recordGrantUID
	 *            primary key of RecordGrant
	 * @return RecordGrantVO
	 */
	@Override
	@RolesAllowed("Login")
	public RecordGrantVO getRecordGrant(final UID recordGrantUID) throws CommonPermissionException {
		// this.checkReadAllowed(ENTITY_NAME_VALUELISTPROVIDER);
		return DatasourceCache.getInstance().getRecordGrant(recordGrantUID);
	}

	/**
	 * get a datasource result by datasource id
	 * used by a client
	 */
	@Override
	public ResultVO executeQuery(final UID dataSourceUID, final Map<String, Object> mpParams, Integer iMaxRowCount, UID languageUID) throws NuclosDatasourceException, CommonFinderException {
		return executeQuery(dataSourceUID, mpParams, iMaxRowCount, languageUID, null);
	}

	/**
	 * get a datasource result by datasource id
	 * used by server
	 */
	public ResultVO executeQuery(final UID dataSourceUID, final Map<String, Object> mpParams, Integer iMaxRowCount, UID languageUID, UID mandatorUID) throws NuclosDatasourceException, CommonFinderException {
		final ResultVO result;
		try {
			if (iMaxRowCount == null && sessionUtils.isCalledRemotely()) {
				iMaxRowCount = LimitedResultSetRunner.MAXFETCHSIZE;
			}
			final DatasourceFacadeLocal facade = ServerServiceLocator.getInstance().getFacade(DatasourceFacadeLocal.class);
			result = executeQueryForDataSourceWithoutPermissionCheck(facade.getDatasource(dataSourceUID), mpParams, iMaxRowCount, languageUID, mandatorUID);
		}
		catch (CommonFinderException ex) {
			throw new NuclosFatalException(ex);
		}
		catch (CommonPermissionException ex) {
			throw new NuclosFatalException(ex);
		}

		return result;
	}

	@Override
	public List<CollectableValueIdField> executeQueryForVLP(VLPQuery query) throws CommonBusinessException {
		Long maxRows = 250000l;
		if (query.getMaxRowCount() != null && query.getMaxRowCount() < 250000) {
			maxRows = query.getMaxRowCount().longValue();
		}
		List<CollectableValueIdField> result = getReferenceList(dataBaseHelper, utils, sessionUtils,
				query.getEntityFieldUID() != null ? metaProvider.getEntityField(query.getEntityFieldUID()) : null,
				StringUtils.defaultIfNull(query.getQuickSearchInput(), ""),
				query.getValuelistProviderUid(),
				query.getQueryParams(),
				query.getDefaultField(),
				maxRows,
				query.getMandatorUID(),
				!StringUtils.looksEmpty(query.getQuickSearchInput()));
		return result;
	}

	/**
	 * gets a datasource result by datasource xml
	 *
	 * @param sDatasourceXML
	 *            datasource id
	 * @param mpParams
	 *            parameters
	 * @param iMaxRowCount
	 * @return report/form filled with data
	 */
	@Override
	public ResultVO executeQueryForDataSource(DatasourceVO datasourcevo, Map<String, Object> mpParams, UID dsEntity, Integer iMaxRowCount, UID languageUID) throws CommonBusinessException, NuclosDatasourceException {
		boolean bCanWriteToDataSource = securityCache.isWriteAllowedForMasterData(getCurrentUserName(), dsEntity, getCurrentMandatorUID());
		if (!bCanWriteToDataSource) {
			throw new CommonPermissionException("No permission to execute Datasource-XML. Cannot write to " + dsEntity);
		}
		
		return executeQueryForDataSourceWithoutPermissionCheck(datasourcevo, mpParams, iMaxRowCount, languageUID, getCurrentMandatorUID());
	}

	private ResultVO executeQueryForDataSourceWithoutPermissionCheck(DatasourceVO datasourcevo, Map<String, Object> mpParams, Integer iMaxRowCount, UID languageUID, UID mandatorUID) throws CommonFinderException, NuclosDatasourceException {
		final String sQuery = createSQL(datasourcevo, mpParams, mandatorUID, languageUID);

		return executePlainQueryAsResultVO(sQuery, iMaxRowCount);
	}
	
	private ResultVO executePlainQueryAsResultVO(String sql, Integer iMaxRowCount) {
		return utils.executePlainQueryAsResultVO(sql, iMaxRowCount);
	}
	
	private void checkReadPermissionForDSEntity(UID dsEntity)  throws CommonPermissionException {
		boolean bCanReadToDataSource = securityCache.isReadAllowedForMasterData(getCurrentUserName(), dsEntity, getCurrentMandatorUID());
		if (!bCanReadToDataSource) {
			throw new CommonPermissionException("No permission to read SchemaData. Cannot read " + dsEntity);
		}
		
	}
 
	@Override
	public Schema getSchemaTables(UID dsEntity) throws CommonPermissionException {
		checkReadPermissionForDSEntity(dsEntity);
		
		return SchemaCache.getInstance().getCurrentSchema();
	}

	@Override
	public Table getSchemaColumns(Table table, UID dsEntity)  throws CommonPermissionException {
		checkReadPermissionForDSEntity(dsEntity);
		
		SchemaCache.getInstance().fillTableColumnsAndConstraints(table);
		return table;
	}

	@Override
	public String createSQLOriginalParameter(DatasourceVO datasourcevo) throws NuclosDatasourceException {
		return utils.createSQLOriginalParameter(datasourcevo);
	}

	@Override
	@RolesAllowed("Login")
	public DynamicTasklistVO getDynamicTasklist(final UID dynamicTaskListUID) throws CommonPermissionException {
		return DatasourceCache.getInstance().getDynamicTasklist(dynamicTaskListUID);
	}

	private void invalidateCaches(final DataSourceType type) {
		DatasourceCache.getInstance().invalidateCache(true, true);
		SchemaCache.getInstance().invalidate(true, true);
		switch (type) {
		case DATASOURCE:
			SecurityCache.getInstance().invalidateCache(true, true);
			break;
		case CHART:
		case DYNAMICENTITY:
			// already done by MasterDataFacadeHelper.invalidateCaches(UID, MasterDataVO<?>)
			// MetaProvider.getInstance().revalidate(true, false);
			break;
		}
	}

	@Override
	public Collection<DynamicTasklistVO> getDynamicTasklists() throws CommonPermissionException {
		return DatasourceCache.getInstance().getAllDynamicTasklists();
	}
	 //TODO MULTINUCLET Set<String> attributes?
	@Override
	public Set<String> getDynamicTasklistAttributes(final UID dynamicTaskListUID) throws CommonPermissionException, NuclosDatasourceException {
		checkReadAllowed(E.TASKLIST);
		final DynamicTasklistVO dtl = DatasourceCache.getInstance().getDynamicTasklist(dynamicTaskListUID);

		final String sQuery = createSQL(dtl, new HashMap<String, Object>());

		final ResultVO result = executePlainQueryAsResultVO(sQuery, 1);
		//TODO MULTINUCLET attributes? 
		Set<String> attributes = new HashSet<String>();
		for (ResultColumnVO col : result.getColumns()) {
			attributes.add(col.getColumnLabel());
		}
		return attributes;
	}

	@Override
	public ResultVO getDynamicTasklistData(final UID dynamicTaskListUID) throws CommonPermissionException, NuclosDatasourceException {
		if (!SecurityCache.getInstance().isSuperUser(getCurrentUserName()) && !SecurityCache.getInstance().getDynamicTasklistDatasources(getCurrentUserName(), getCurrentMandatorUID()).contains(dynamicTaskListUID)) {
			throw new CommonPermissionException("Permission denied for dynamic task list with id " + dynamicTaskListUID);
		}

		final DynamicTasklistVO dtl = DatasourceCache.getInstance().getDynamicTasklist(dynamicTaskListUID);
		String sQuery = createSQL(dtl, new HashMap<String, Object>());
		return executePlainQueryAsResultVO(sQuery, null);
	}

	@Override
	public CollectableField getDefaultValue(final UID dataSourceUID, final String valuefield, String idfield, String defaultfield, Map<String, Object> params, UID baseEntityUID, UID mandatorUID, UID languageUID) throws CommonBusinessException {
		final DatasourceVO dsvo = getValuelistProvider(dataSourceUID);
		final List<DatasourceParameterVO> collParameters = getParametersFromXML(dsvo.getSource());
		final Map<String, Object> queryParams = new HashMap<String, Object>(params);

		for (final DatasourceParameterVO dpvo : collParameters) {
			if (queryParams.get(dpvo.getParameter()) == null) {
				queryParams.put(dpvo.getParameter(), null);
			}
		}

		final List<CollectableValueIdField> result = executeQueryForVLP(
				new VLPQuery(dataSourceUID)
						.setQueryParams(queryParams)
						.setNameField(valuefield)
						.setIdFieldName(idfield)
						.setDefaultField(defaultfield)
						.setBaseEntityUID(baseEntityUID)
						.setMandatorUID(mandatorUID)
						.setLanguageUID(languageUID)
		);

		for (CollectableValueIdField field : result) {
			if (field.isSelected()) {
				if (idfield != null) {
					return field;
				} else {
					return new CollectableValueField(field.getValue());
				}
			}
		}
		return idfield != null ? CollectableValueIdField.NULL : CollectableValueField.NULL;
	}
	
	/**
	 * boolean representation from object
	 * 
	 * @param obj	object
	 * @return	
	 */
	private Boolean booleanFromObject(final Object obj) {
		if (null == obj) {
			return Boolean.FALSE;
		}
		
		final Object value = obj;
		
		Boolean isDefault = null;
		if (value instanceof String) {
			isDefault = BooleanUtils.toBooleanObject((String)value);
		}
		if (value instanceof Integer) {
			isDefault = BooleanUtils.toBooleanObject((Integer)value);
		}
		if (value instanceof Boolean) {
			isDefault = (Boolean)value;
		} 
		
		if (null == isDefault) {
			throw new IllegalArgumentException("no boolean representation for unknown class " + obj.getClass());
		}
		
		return isDefault;
	}
	
	@Override
	public List<String> getColumnsFromVLP(UID valuelistProviderUid, Class clazz) throws CommonBusinessException {
		ValuelistProviderVO vlpVO = getValuelistProvider(valuelistProviderUid);
		if (vlpVO == null) {
			throw new CommonFinderException("Could Not Find VLP with UID=" + valuelistProviderUid);
		}

		return getColumns(createSQL(vlpVO, getTestParameters(vlpVO.getSource())), clazz);
	}
	
	@Override
	public List<String> getColumns(String sql) {
		return getColumns(sql, null);
	}
	
	public List<String> getColumns(String sql, Class clazz) {
		List<String> result = new ArrayList<String>();
		try {
			if (StringUtils.isNullOrEmpty(sql))
				return result;
			
			ResultVO resultVO = executePlainQueryAsResultVO(sql, 0);
			for (ResultColumnVO column : resultVO.getColumns()) {
				if (clazz == null || clazz.isAssignableFrom(column.getColumnClass()))
				result.add(column.getColumnLabel());
			}
			
			// fallback 
			// beware. we cannot respect the column clazz here.
			if (result.isEmpty() && clazz == null) {
				result.addAll(DatasourceUtils.getColumns(sql));
			}
			return result;
		} catch (Exception e) {
			// fallback
			// beware. we cannot respect the column clazz here.
			if (result.isEmpty() && clazz == null) {
				result.addAll(DatasourceUtils.getColumns(sql));
			}
		}
		return result;
	}

	public static class DatasourceMetaTransformer implements ResultSetMetaDataTransformer<DatasourceMetaVO> {
		
		private final DatasourceVO dsvo;
		
		private final DatasourceMetaVO current;
		
		private final EntityMeta<UID> datasourceMeta;

		public DatasourceMetaTransformer(DatasourceVO dsvo, DatasourceMetaVO current, EntityMeta<UID> datasourceMeta) {
			super();
			this.dsvo = dsvo;
			this.current = current;
			this.datasourceMeta = datasourceMeta;
		}

		public static boolean checkEntityUID(UID entityUID, EntityMeta<UID> datasourceMeta) {
			final String sTest = entityUID.getString();
			return sTest.startsWith(getEntityPrefix(datasourceMeta));
		}

		private static String getEntityPrefix(EntityMeta<UID> datasourceMeta) {
			return datasourceMeta.getUID().getString() + "-";
		}

		private static String getEntityFieldPrefix(EntityMeta<UID> datasourceMeta) {
			return datasourceMeta.getUID().getString() + "f-";
		}

		@Override
		public DatasourceMetaVO transform(ResultSetMetaData rsmeta) {
			if (rsmeta == null) {
				return null;
			}
			
			DatasourceMetaVO dsmeta = new DatasourceMetaVO();
			
			try {
				dsmeta.setEntityUID(new UID(getEntityPrefix(datasourceMeta)+dsvo.getId().getString()));
				
				List<ColumnMeta> columns = new ArrayList<>();
				for (int i = 1; i <= rsmeta.getColumnCount(); i++) {
					String columnname = rsmeta.getColumnLabel(i);
					
					boolean forceTimestampFormat = false;
					final String tsHint = "_as_timestamp";
					if (columnname.toLowerCase().endsWith(tsHint)) {
						forceTimestampFormat = true;
					}
					
					UID fieldUID = null;
					if (current != null && current.getColumns() != null) {
						for (ColumnMeta existingcol : current.getColumns()) {
							if (columnname.equals(existingcol.getColumnName())) {
								fieldUID = existingcol.getFieldUID();
							}
						}
					}
					if (fieldUID == null) {
						fieldUID = new UID(getEntityFieldPrefix(datasourceMeta)+new UID().getString());
					}
					
					Integer scale = rsmeta.getScale(i);
					Integer precision = rsmeta.getPrecision(i);
					String classname = rsmeta.getColumnClassName(i);
					Class<?> cls = Class.forName(classname);
					if (Number.class.isAssignableFrom(cls)) {
						if (LangUtils.defaultIfNull(scale, new Integer(0)).intValue() > 0) {
							classname = Double.class.getName();
						} else {
							if (columnname.toUpperCase().startsWith("BLN") && LangUtils.defaultIfNull(precision, new Integer(0)).intValue() == 1) {
								classname = Boolean.class.getName();
								scale = null;
							} else {
								classname = Integer.class.getName();
							}
						}
						if (precision == null) {
							precision = 9;
						}
					} else if (cls == java.sql.Date.class) {
						cls = Date.class;
						classname = cls.getName();
					}
					if (forceTimestampFormat && Date.class.isAssignableFrom(Class.forName(classname))) {
						classname = InternalTimestamp.class.getName();
					}
					Boolean readonly = rsmeta.isReadOnly(i);
					
					ColumnMeta colmeta = new ColumnMeta();
					colmeta.setFieldUID(fieldUID);
					colmeta.setColumnName(columnname);
					colmeta.setJavaType(classname);
					colmeta.setReadOnly(readonly);
					// switch scale <-> precision for nuclos
					colmeta.setPrecision(scale);
					colmeta.setScale(precision);
					columns.add(colmeta);
				}
				
				dsmeta.setColumns(columns);
			} catch (Exception ex) {
				LOG.error("Unable to generate meta data for datasource {} uid={}: {}",
				          dsvo.getName(), dsvo.getId(), ex.getMessage(), ex);
			}
			
			return dsmeta;
		}
		
	}
	
	public int cacheExpiration() {
		return ServerParameterProvider.getInstance().getIntValue(ParameterProvider.KEY_VLP_RESULTCACHE_EXPIRATION, 4000);
	}

	@Override
	public Set<UID> getAccessibleMandators(UID mandatorUID) {
		return mandatorUtils.getAccessibleMandators(mandatorUID);
	}

	@Override
	public boolean isMandator(UID baseEntityUID) {
		return baseEntityUID != null ? MetaProvider.getInstance().getEntity(baseEntityUID).isMandator() : false;
	}

	@Override
	public String getBaseTable(UID baseEntityUID) {
		return MetaProvider.getInstance().getEntity(baseEntityUID).getDbSelect();
	}
	
	@Override
	public UID getReportLanguageToUse(UID report, UID language, Long pk) throws NuclosReportException {
		
		Collection<MasterDataVO<UID>> lstModules = masterDataFacade.getMasterData(E.REPORTUSAGE,
				SearchConditionUtils.newUidComparison(E.REPORTUSAGE.form, ComparisonOperator.EQUAL, report));
		
		return this.utils.getReportLanguageToUse(lstModules, language, pk);
	}
	
	public Object getBooleanRepresentationInSQL(Boolean b) {
		return dataBaseHelper.getDbAccess().getBooleanRepresentationInSQL(b);
	}

	private DatasourceVO setLocaleResouces(DatasourceVO dsVO) {
		if (dsVO instanceof ValuelistProviderVO) {
			ValuelistProviderVO vlp = (ValuelistProviderVO) dsVO;
			String resId = vlp.getDetailsearchdescriptionResourceId();
			if (resId != null) {
				String sDetailsearchdescriptionDe = localeFacade.getResourceById(LocaleInfo.parseTag(Locale.GERMAN), resId);
				String sDetailsearchdescriptionEn = localeFacade.getResourceById(LocaleInfo.parseTag(Locale.ENGLISH), resId);
				vlp.setDetailsearchdescription(sDetailsearchdescriptionDe, Locale.GERMAN);
				vlp.setDetailsearchdescription(sDetailsearchdescriptionEn, Locale.ENGLISH);
			}
		}
		return dsVO;
	}
}
