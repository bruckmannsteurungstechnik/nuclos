//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.report.ejb3;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;

import org.nuclos.common.E;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.TrueCondition;
import org.nuclos.common.report.ejb3.IJobKey;
import org.nuclos.common.report.ejb3.JobKeyImpl;
import org.nuclos.common.report.ejb3.SchedulerControlFacadeRemote;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonSchedulerException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.job.SchedulableJob;
import org.nuclos.server.job.ejb3.JobControlFacadeBean;
import org.nuclos.server.job.ejb3.JobControlFacadeLocal;
import org.nuclos.server.job.valueobject.JobVO;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.nuclos.server.security.NuclosLocalServerSession;
import org.quartz.CronExpression;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.matchers.GroupMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
* <tt>SchedulerControlFacadeBean</tt> provides functionality to control quartz job execution services.
* All asynchronous tasks (i.e. tasks that do not return a result to the user) should be executed as a job.
*
* Currently known types are:
* <ul>
* 	<li>Jobs</li>
* 	<li>Imports</li>
* </ul>
*
* <br>
* <br>Created by Novabit Informationssysteme GmbH
* <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
*/
@Transactional(noRollbackFor= {Exception.class})
@RolesAllowed("UseManagementConsole")
public class SchedulerControlFacadeBean extends NuclosFacadeBean implements SchedulerControlFacadeLocal, SchedulerControlFacadeRemote {
	
	private static final Logger LOG = LoggerFactory.getLogger(SchedulerControlFacadeBean.class);
	
	private static final GroupMatcher<JobKey> MATCH_JK_IN_DEFAULT_GROUP = GroupMatcher.jobGroupEquals(Scheduler.DEFAULT_GROUP);

	// Spring injection
	
	@Autowired
	private Scheduler nuclosScheduler;
	
	@Autowired
	private MasterDataFacadeLocal masterDataFacade;
	
	// end of Spring injection
	
	public SchedulerControlFacadeBean() {
	}
	
	@PostConstruct
	void init() {
		try {
			NuclosLocalServerSession.getInstance().loginAsUser(ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_TIMELIMIT_RULE_USER));
			Collection<MasterDataVO<UID>> collMD = masterDataFacade.getMasterData(E.JOBCONTROLLER, TrueCondition.TRUE);

			for (final MasterDataVO<UID> md : collMD) {
				final JobVO job = new JobVO(md, md.getDependents());
				if (JobControlFacadeBean.migrateToCron(job)) {
					// NUCLOS-2306
					if (!JobControlFacadeLocal.NOT_ACTIVATED.equals(job.getLastState())) {
						masterDataFacade.modify(job.toMasterDataVO(), null);
					}
				}
				if (!JobControlFacadeLocal.NOT_ACTIVATED.equals(job.getLastState())) {
					scheduleJob(job);
				}
			}
		} catch (CommonValidationException | CommonCreateException | CommonFinderException | CommonRemoveException | CommonStaleVersionException | CommonPermissionException | NuclosBusinessRuleException | CommonSchedulerException e) {
			throw new NuclosFatalException(e);
		}
	}
	
	public JobDetail addJob(JobVO job) throws CommonSchedulerException {
		final JobKey jk = JobKey.jobKey(job.getName(), Scheduler.DEFAULT_GROUP);
		if (exists(jk)) {
			try {
				JobDetail jd = nuclosScheduler.getJobDetail(jk);
				if (!job.getId().equals(jd.getJobDataMap().getIntValue(job.getName()))) {
					throw new CommonSchedulerException("scheduler.error.alreadyscheduled");
				}
			} catch (SchedulerException e) {
				throw new NuclosFatalException("");
			}
		}
		final JobDataMap jdm = new JobDataMap();
		jdm.put(job.getName(), job.getId());
		final JobBuilder jb = JobBuilder.newJob(SchedulableJob.class).withIdentity(jk).storeDurably().setJobData(jdm);
		final JobDetail jobDetail = jb.build();

		try {
			nuclosScheduler.addJob(jobDetail, true);
		} catch (SchedulerException e) {
			throw new NuclosFatalException("addJob failed: " + e);
		}
		LOG.debug(getSchedulerSummary());
		return jobDetail;
	}
	
	@Override
	public void deleteJob(IJobKey jk) throws CommonSchedulerException {
		final JobKey jobKey = JobKey.jobKey(jk.getName(), jk.getGroup());
		deleteJob(jobKey);
	}

	public void deleteJob(JobKey jobKey) throws CommonSchedulerException {
		if (exists(jobKey)) {
			try {
				if (nuclosScheduler.deleteJob(jobKey)) {
					LOG.debug("Deleted job: {}", jobKey);
				}
				else {
					LOG.warn("Failed to delete job: {}", jobKey);
					throw new CommonSchedulerException("scheduler.error.delete");
				}
			}
			catch (SchedulerException ex) {
				LOG.error("Unable to delete job key:", ex);
				throw new NuclosFatalException("scheduler.error.delete");
			}
			LOG.debug(getSchedulerSummary());
		}
	}

	/**
	 * schedules the Job at the given time
	 * @param jobVO
	 * @return
	 * @throws CommonSchedulerException 
	 */
	public Trigger scheduleJob(JobVO jobVO) throws CommonSchedulerException {
		final JobKey jk = JobKey.jobKey(jobVO.getName(), Scheduler.DEFAULT_GROUP);
		final TriggerKey tk = TriggerKey.triggerKey(jobVO.getName(), Scheduler.DEFAULT_GROUP);
		final List<? extends Trigger> triggers = getTriggers(jk);
		try {
			if (isRunning(triggers)) {
				throw new SchedulerException("scheduler.error.running");
			}
			if (isScheduled(triggers)) {
				// remove existing triggers
				for (Trigger t : nuclosScheduler.getTriggersOfJob(jk)) {
					nuclosScheduler.unscheduleJob(t.getKey());
				}
			}
		} catch (SchedulerException ex) {
			throw new CommonSchedulerException("scheduler.error.reschedule");
		}

		if (!exists(jk)) {
			addJob(jobVO);
		}

		final int iHour = Integer.parseInt(jobVO.getStarttime().substring(0, 2));
		final int iMinute = Integer.parseInt(jobVO.getStarttime().substring(3));
		final Calendar startDate = Calendar.getInstance();
		final Date now = startDate.getTime();
		startDate.setTime(jobVO.getStartdate());
		startDate.set(Calendar.HOUR_OF_DAY, iHour);
		startDate.set(Calendar.MINUTE, iMinute);

		final CronExpression ce;
		try {
			ce = new CronExpression(jobVO.getCronExpression());
		} catch (ParseException ex) {
			LOG.warn("scheduleJob '{}' failed: wrong cron expression: {}", jobVO.getName(), ex, ex);
			throw new NuclosFatalException("scheduler.error.scheduling");
		}
		// NUCLOS-2298
		final CronScheduleBuilder sb = CronScheduleBuilder.cronSchedule(ce)
				.withMisfireHandlingInstructionDoNothing();
		final TriggerBuilder<CronTrigger> tb = 
				TriggerBuilder.newTrigger().withIdentity(tk).withSchedule(sb).forJob(jk);
		final Date start = startDate.getTime();
		if (now.before(start)) {
			tb.startAt(start);
		}

		final Trigger result = tb.build();
		try {
			nuclosScheduler.scheduleJob(result);
		} catch (SchedulerException ex) {
			LOG.warn("scheduleJob '{}' failed: ", jobVO.getName() , ex);
			throw new NuclosFatalException("scheduler.error.scheduling");
		}
		LOG.info("Successfully scheduled Job '{}'. Job will start at {}",
		         jobVO.getName(), result.getNextFireTime());
		LOG.debug(getSchedulerSummary());
		return result;
	}
	
	@Override
	public void unscheduleJob(IJobKey jk) throws CommonSchedulerException {
		final JobKey jobKey = JobKey.jobKey(jk.getName(), jk.getGroup());
		unscheduleJob(jobKey);
	}

	public void unscheduleJob(JobVO jobVO) throws CommonSchedulerException {
		final JobKey jobKey = JobKey.jobKey(jobVO.getName(), Scheduler.DEFAULT_GROUP);
		unscheduleJob(jobKey);
	}

	public void unscheduleJob(JobKey jobKey) throws CommonSchedulerException {
		if (!isScheduled(jobKey)) {
			throw new CommonSchedulerException("scheduler.error.notscheduled");
		}
		try {
			List<? extends Trigger> triggers = nuclosScheduler.getTriggersOfJob(jobKey);
			if (triggers != null && !triggers.isEmpty()) {
				final TriggerKey tk = TriggerKey.triggerKey(jobKey.getName(), jobKey.getGroup());
				for (Trigger t : triggers) {
					if (!nuclosScheduler.unscheduleJob(tk)) {
						throw new SchedulerException("scheduler.error.unscheduling");
					}
				}
			}
		}
		catch (SchedulerException ex) {
			throw new NuclosFatalException("scheduler.error.unscheduling");
		}
		LOG.info(getSchedulerSummary());
	}

	/**
	 * trigger immediate job execution
	 * @throws CommonSchedulerException 
	 */
	public void triggerJob(JobVO jobVO) throws CommonSchedulerException {
		final JobKey jk = JobKey.jobKey(jobVO.getName(), Scheduler.DEFAULT_GROUP);
		final List<? extends Trigger> triggers = getTriggers(jk);
		try {
			if (isRunning(triggers)) {
				throw new SchedulerException("scheduler.error.running");
			}
		} catch (SchedulerException ex) {
			throw new CommonSchedulerException("scheduler.error.reschedule");
		}
		if (!exists(jk)) {
			addJob(jobVO);
		}
		try {
			LOG.info("Scheduling job {}.{} immediately",
			         Scheduler.DEFAULT_GROUP, jk.getName());
			nuclosScheduler.triggerJob(jk);
		} catch (SchedulerException e) {
			throw new CommonSchedulerException("scheduler.error.trigger.immediate", e);
		}
	}

	private List<? extends Trigger> getTriggers(JobKey jobKey) {
		for (JobKey job : _getJobKeys()) {
			if (job.equals(jobKey)) {
				try {
					List<? extends Trigger> triggers = nuclosScheduler.getTriggersOfJob(jobKey);
					if (triggers != null && !triggers.isEmpty()) {
						return triggers;
					}
				}
				catch (SchedulerException e) {
					throw new NuclosFatalException("Quartz failed", e);
				}
			}
		}
		return null;
	}
	
	private static boolean isScheduled(List<? extends Trigger> triggers) {
		return triggers != null && !triggers.isEmpty();
	}
	
	private boolean isRunning(List<? extends Trigger> triggers) throws SchedulerException {
		if (triggers == null) return false;
		final List<JobExecutionContext> jcs = nuclosScheduler.getCurrentlyExecutingJobs();
		final Set<JobKey> running = new HashSet<JobKey>();
		for (JobExecutionContext j: jcs) {
			running.add(j.getJobDetail().getKey());
		}
		for (Trigger t: triggers) {
			final JobKey jk = JobKey.jobKey(t.getKey().getName(), t.getKey().getGroup());
			if (running.contains(jk)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isScheduled(IJobKey jk) {
		final JobKey jobKey = JobKey.jobKey(jk.getName(), jk.getGroup());
		final List<? extends Trigger> triggers = getTriggers(jobKey);
		return isScheduled(triggers);
	}

	public boolean isScheduled(JobKey jobKey) {
		final List<? extends Trigger> triggers = getTriggers(jobKey);
		return isScheduled(triggers);
	}

	@Override
	public Set<IJobKey> getJobKeys() {
		final Set<JobKey> l = _getJobKeys();
		final Set<IJobKey> result = new HashSet<IJobKey>(l.size());
		for (JobKey jk: l) {
			result.add(new JobKeyImpl(new UID(jk.getName()), jk.getGroup()));
		}
		return result;
	}
	
	/**
	 * @return the names of all scheduled jobs.
	 */
	public Set<JobKey> _getJobKeys() {
		try {
			return nuclosScheduler.getJobKeys(MATCH_JK_IN_DEFAULT_GROUP);
		}
		catch (SchedulerException ex) {
			LOG.error("Unable to get job key", ex);
			return Collections.emptySet();
		}
	}

	private boolean exists(JobKey jobKey) {
		for (JobKey job : _getJobKeys()) {
			if (job.equals(jobKey)) {
				return true;
			}
		}
		return false;
	}

	public String getSchedulerSummary() {
		try {
			StringBuilder sb = new StringBuilder();
			sb.append("Nuclos Scheduler Summary:");
			for (JobKey job : nuclosScheduler.getJobKeys(MATCH_JK_IN_DEFAULT_GROUP)) {
				sb.append("\n  " + job + "(" + nuclosScheduler.getJobDetail(job) + "):");
				List <? extends Trigger> triggers = nuclosScheduler.getTriggersOfJob(job);
				if (triggers == null || triggers.isEmpty()) {
					sb.append(" not scheduled;");
				}
				else {
					for (Trigger t : triggers) {
						sb.append("\n    " + t.toString());
					}
				}
			}
			return sb.toString();
		}
		catch (SchedulerException ex) {
			return ex.toString();
		}
	}

}
