package org.nuclos.server.rest;

import java.security.Principal;

import javax.annotation.Priority;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;

import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.server.maintenance.MaintenanceFacadeLocal;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * blocks requests if server is in maintenance mode (except for the super user who initiated the maintenance mode)
 */
@Configurable
@Priority(500) // Set priority so low that filter is hit very early in chain
public class MaintenanceModeRequestFilter implements ContainerRequestFilter {

	@Override
	public void filter(final ContainerRequestContext containerRequestContext) {
		boolean abort = false;

		final MaintenanceFacadeLocal maintenanceFacade = getMaintenanceFacade();
		if (maintenanceFacade == null) {
			abort = true;
		} else {
			final Principal userPrincipal = containerRequestContext.getSecurityContext().getUserPrincipal();

			if (userPrincipal != null) {
				String username = userPrincipal.getName();
				if (maintenanceFacade.blockUserLogin(username)) {
					abort = true;
				}
			}
		}

		if (abort) {
			containerRequestContext.abortWith(Response.status(Response.Status.SERVICE_UNAVAILABLE).build());
		}
	}

	private MaintenanceFacadeLocal getMaintenanceFacade() {
		if (SpringApplicationContextHolder.isNuclosReady()) {
			return SpringApplicationContextHolder.getBean(MaintenanceFacadeLocal.class);
		}

		return null;
	}
}
