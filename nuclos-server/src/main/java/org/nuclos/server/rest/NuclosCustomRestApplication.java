//Copyright (C) 2019  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest;

import java.security.Principal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.process.internal.RequestScoped;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.server.spi.Container;
import org.glassfish.jersey.server.spi.ContainerLifecycleListener;
import org.nuclos.api.context.CustomRestContext;
import org.nuclos.api.rule.CustomRestRule;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common2.LangUtils;
import org.nuclos.server.common.EventSupportCache;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.customcode.CustomCodeManager;
import org.nuclos.server.customcode.ejb3.CodeFacadeBean;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.services.helper.CustomRestContextFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ApplicationPath("/rest/custom/")
public class NuclosCustomRestApplication extends NuclosRestApplication {

	private final static Logger LOG = LoggerFactory.getLogger(NuclosCustomRestApplication.class);
	private static List<Container> containers = new CopyOnWriteArrayList<>();

	static {
		// reload container configuration after CustomRestRule code has changed
		CodeFacadeBean.addAdListener(
				codeVO -> {
					LOG.debug("Rule compiled: {}", codeVO.getName());
					if (codeVO.getSource().indexOf(CustomRestRule.class.getSimpleName()) != -1) {
						reload();
					}
				}
		);

		SpringApplicationContextHolder.addSpringReadyListener(new SpringApplicationContextHolder.SpringReadyListener() {
			@Override
			public void springIsReady() {
				reload();
			}

			@Override
			public int getMinReadyState() {
				return 2;
			}
		});
	}

	public static void reload() {
		LOG.info("Reloading Containers {}...", containers);
		NuclosCustomRestApplication resourceConfig = new NuclosCustomRestApplication();
		resourceConfig.registerCustomRestRules();
		containers.forEach(it -> it.reload(resourceConfig));
		LOG.info("Reload done for Containers {}", containers);
	}


	private void registerCustomRestRules() {
		try {
			Set<String> paths = new HashSet<>();

			// TODO also check system REST paths (like '/rest/version')
			final List<Class> customRules = getCustomRestRules();
			for (Class customRestRule : customRules) {
				final List<String> currentClassPathConfigs = restPaths(customRestRule);
				for (String path : currentClassPathConfigs) {
					if (paths.contains(path)) {
						throw new RuntimeException("The path configuration '" + path
								+ "' in '" + customRestRule + "' conflicts with another REST path.");
					} else {
						LOG.debug("Adding path: {}", path);
						paths.add(path);
					}
					LOG.debug("Registering custom rest rule: {}", customRestRule.getName());
					register(customRestRule);
				}
			}

		} catch (Exception e) {
			LOG.error("Unable to register CustomRestRules.", e);
		}
	}


	private List<String> restPaths(Class cl) {
		String pathForClass = cl.getAnnotation(Path.class) != null ? (((Path) cl.getAnnotation(Path.class)).value() + "/") : "";
		final Stream<List<String>> listStream = Stream.of(GET.class, POST.class, PUT.class, PATCH.class, DELETE.class)
				.map(httpMethod -> Arrays.stream(cl.getMethods())
				.filter(m -> m.getAnnotation(Path.class) != null)
				.map(m -> (
						m.getAnnotation(Path.class) != null
								? (pathForClass + m.getAnnotation(Path.class).value() + " [" + httpMethod.getSimpleName() + "]")
								: (pathForClass.isEmpty() ? null : (pathForClass + " [" + httpMethod.getSimpleName() + "]"))))
				.filter(Objects::nonNull)
				.collect(Collectors.toList()));
		return listStream.flatMap(List::stream).collect(Collectors.toList());
	}

	private List<Class> getCustomRestRules() throws Exception {

		EventSupportCache esCache = SpringApplicationContextHolder.getBean(EventSupportCache.class);
		CustomCodeManager customCodeManager = SpringApplicationContextHolder.getBean(CustomCodeManager.class);

		final ClassLoader classLoader = customCodeManager.getClassLoaderAndCompileIfNeeded();
		return esCache.getExecutableEventSupportFiles()
				.stream()
				.map(eventSupportSourceVO -> eventSupportSourceVO.getClassname())
				.map(className -> {
					try {
						return classLoader.loadClass(className).newInstance();
					} catch (ClassNotFoundException e) {
						// ignore inactive rules
					} catch (InstantiationException | IllegalAccessException | NoClassDefFoundError e) {
						LOG.error("Unable to load CustomRestRule '{}'.", e, className);
					}
					return null;
				})
				.filter(ruleInstance -> ruleInstance != null && ruleInstance instanceof CustomRestRule)
				.map(customRestRuleInstance -> customRestRuleInstance.getClass())
				.collect(Collectors.toList())
				;
	}


	public NuclosCustomRestApplication() {

		// Disable WADL generation for the REST service
		property(ServerProperties.WADL_FEATURE_DISABLE, "true");

		// inject CustomRestContext into CustomRestRules
		register(new AbstractBinder() {
			@Override
			protected void configure() {
				bindFactory(CustomRestContextFactoryHk2.class)
						.to(CustomRestContext.class)
						.in(RequestScoped.class);
			}
		});

		// container reference is needed for reloading ServletContext after CustomRestRules are changed
		register(new ContainerLifecycleListener() {

			@Override
			public void onStartup(final Container container) {
				LOG.info("Container startup: {}", container);

				// Might be called for multiple containers, at least on Glassfish.
				NuclosCustomRestApplication.containers.add(container);
			}

			@Override
			public void onReload(final Container container) {
				LOG.info("Container reload: {}", container);
			}

			@Override
			public void onShutdown(final Container container) {
				LOG.info("Container shutdown: {}", container);

				NuclosCustomRestApplication.containers.remove(container);
			}
		});


		// Registering JacksonFeature is necessary for Glassfish deployment
		register(JacksonFeature.class);

		final String sAdditionalServicesClassName = LangUtils.defaultIfNull(
				ApplicationProperties.getInstance().getAdditionalRestServices(), AdditionalRestServices.class.getName()
		);

		if (SpringApplicationContextHolder.isSpringReady()) {
			registerCustomRestRules();
		}

		register(CustomRestServiceAuthenticationFilter.class);

		// REGISTER SINGLETON
		registerInstances(
				new CacheHeaderFilter(),
				new CrossOriginResourceSharingFilter(),
				new SessionValidationRequestFilter(),
				new MaintenanceModeRequestFilter(),
				new ProfilingFilter(),
				new SwaggerRequestFilter()
		);
	}

	/**
	 * verify access permissions of CustomRestServices
	 */
	public static class CustomRestServiceAuthenticationFilter implements ContainerRequestFilter {

		@Context
		private ResourceInfo resourceInfo;

		@Override
		public void filter(final ContainerRequestContext requestContext) {

			final Principal userPrincipal = requestContext.getSecurityContext().getUserPrincipal();
			if (userPrincipal != null) {
				final Map<UID, String> allowedCustomRestRules = SecurityCache.getInstance().getAllowedCustomRestRules(userPrincipal.getName(), null);
				if (CustomRestRule.class.isAssignableFrom(resourceInfo.getResourceClass())) {
					if (!allowedCustomRestRules.containsValue(resourceInfo.getResourceClass().getName())) {
						throw new NuclosWebException(Response.Status.UNAUTHORIZED);
					}
				}
			}
		}

	}

	public static class CustomRestContextFactoryHk2 implements org.glassfish.hk2.api.Factory<CustomRestContext> {

		private final HttpServletRequest request;

		@Inject
		public CustomRestContextFactoryHk2(HttpServletRequest request) {
			this.request = request;
		}

		@Override
		public CustomRestContext provide() {
			final String userName = request.getUserPrincipal() != null ? request.getUserPrincipal().getName() : null;
			return CustomRestContextFactory.createCustomRestContext(userName);
		}

		@Override
		public void dispose(final CustomRestContext instance) {
		}
	}

}
