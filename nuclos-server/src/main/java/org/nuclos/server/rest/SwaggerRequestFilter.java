package org.nuclos.server.rest;

import javax.annotation.Nullable;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.services.helper.AbstractSessionIdLocator;
import org.springframework.beans.factory.annotation.Configurable;

import io.swagger.v3.jaxrs2.integration.resources.BaseOpenApiResource;

@Configurable
public class SwaggerRequestFilter extends AbstractSessionIdLocator implements ContainerRequestFilter {

	@Context
	private ResourceInfo resourceInfo;

	@Override
	public void filter(ContainerRequestContext creq) {
		if (isSwaggerRequest() && !isSwaggerActive()) {
			throw new NuclosWebException(Response.Status.NO_CONTENT);
		}
	}

	@Nullable
	private ParameterProvider getParameterProvider() {
		// TODO: ParameterProvider should be injected, but injection currently can cause
		//  exceptions during server startup.
		if (SpringApplicationContextHolder.isNuclosReady()) {
			return SpringApplicationContextHolder.getBean(ParameterProvider.class);
		}

		return null;
	}

	private boolean isSwaggerActive() {
		final ParameterProvider parameterProvider = getParameterProvider();

		if (parameterProvider == null) {
			// Assume that Swagger is disabled until we can really check the parameter
			return false;
		}

		return parameterProvider.isEnabled(
				ParameterProvider.SWAGGER_ACTIVE,
				true
		);
	}

	private boolean isSwaggerRequest() {
		return BaseOpenApiResource.class.isAssignableFrom(resourceInfo.getResourceClass());
	}
}
