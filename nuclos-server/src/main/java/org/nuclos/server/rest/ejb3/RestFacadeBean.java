//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.ejb3;


import static org.nuclos.server.rest.services.helper.WebContext.DEFAULT_RESULT_LIMIT;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.security.auth.login.LoginException;
import javax.ws.rs.core.Response;

import org.nuclos.api.context.SpringInputContext;
import org.nuclos.api.context.communication.RequestContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.common.Actions;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.CommonConsole;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.LoginResult;
import org.nuclos.common.MandatorVO;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosConstants;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosFile;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UnlockMode;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.WorkspaceVO;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldFormat;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.ValueListProviderType;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.livesearch.ejb3.LiveSearchFacadeRemote;
import org.nuclos.common.lucene.LuceneSearchResult;
import org.nuclos.common.querybuilder.NuclosDatasourceException;
import org.nuclos.common.report.NuclosReportException;
import org.nuclos.common.report.ejb3.ReportFacadeRemote;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.DefaultReportVO;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.nuclos.common.report.valueobject.ResultColumnVO;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common.security.EntityPermission;
import org.nuclos.common.security.RemoteAuthenticationManager;
import org.nuclos.common.security.SubformPermission;
import org.nuclos.common.tasklist.TasklistDefinition;
import org.nuclos.common.valuelistprovider.VLPQuery;
import org.nuclos.common.valuelistprovider.VLPUtils;
import org.nuclos.common2.BoDataSet;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.common2.exception.NuclosExceptions;
import org.nuclos.common2.layoutml.WebValueListProvider;
import org.nuclos.common2.resource.ResourceResolver;
import org.nuclos.common2.searchfilter.EntitySearchFilter2;
import org.nuclos.server.attribute.ejb3.LayoutFacadeLocal;
import org.nuclos.server.autosync.AutoDbSetup;
import org.nuclos.server.common.DatasourceServerUtils;
import org.nuclos.server.common.EventSupportCache;
import org.nuclos.server.common.GenerationCache;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.ModulePermission;
import org.nuclos.server.common.ModulePermissions;
import org.nuclos.server.common.NuclosRemoteContextHolder;
import org.nuclos.server.common.NuclosUserDetailsContextHolder;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.ejb3.EntityObjectFacadeLocal;
import org.nuclos.server.common.ejb3.EntityObjectFacadeRemote;
import org.nuclos.server.common.ejb3.PreferencesFacadeLocal;
import org.nuclos.server.common.ejb3.SecurityFacadeLocal;
import org.nuclos.server.dal.DalSupportForGO;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbAccess;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.query.DbColumnExpression;
import org.nuclos.server.dblayer.query.DbCondition;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dbtransfer.TransferFacadeLocal;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeLocal;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.genericobject.GeneratorFailedException;
import org.nuclos.server.genericobject.context.GenerationContext;
import org.nuclos.server.genericobject.context.GenerationContextBuilder;
import org.nuclos.server.genericobject.ejb3.GenerationResult;
import org.nuclos.server.genericobject.ejb3.GeneratorFacadeRemote;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.genericobject.valueobject.GeneratorVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.i18n.language.data.DataLanguageCache;
import org.nuclos.server.i18n.language.data.DataLanguageVO;
import org.nuclos.server.maintenance.MaintenanceFacadeLocal;
import org.nuclos.server.masterdata.ejb3.MasterDataRestFqnCache;
import org.nuclos.server.navigation.ejb3.TreeNodeFacadeRemote;
import org.nuclos.server.navigation.treenode.AbstractTreeNode;
import org.nuclos.server.navigation.treenode.DefaultMasterDataTreeNode;
import org.nuclos.server.navigation.treenode.DefaultMasterDataTreeNodeParameters;
import org.nuclos.server.navigation.treenode.SimpleTreeNode;
import org.nuclos.server.parameter.NuclosParameterProvider;
import org.nuclos.server.report.ejb3.DatasourceFacadeLocal;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.misc.RestPrintoutFileDownloadCache;
import org.nuclos.server.rest.services.rvo.FilterJ;
import org.nuclos.server.rest.services.rvo.LayoutInfo;
import org.nuclos.server.rest.services.rvo.NucletInfo;
import org.nuclos.server.rest.services.rvo.RValueObject;
import org.nuclos.server.rest.services.rvo.ResultListExportRVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.nuclos.server.searchfilter.ejb3.SearchFilterFacadeLocal;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;
import org.nuclos.server.security.NuclosAuthenticationProvider;
import org.nuclos.server.security.SessionContext;
import org.nuclos.server.security.UserDetailsService;
import org.nuclos.server.statemodel.ejb3.StateFacadeLocal;
import org.nuclos.server.statemodel.ejb3.StateFacadeRemote;
import org.nuclos.server.statemodel.valueobject.StateTransitionVO;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.nuclos.server.tasklist.TasklistFacadeLocal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.Resource;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

/**
 * Server implementation of the RestFacadeLocale interface.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 *
 * @author Oliver Brausch
 * @since Nuclos 4.0
 */
@Transactional(propagation=Propagation.SUPPORTS)
@Component
@DependsOn("clientPreferences")
@RolesAllowed("Login")
@Lazy
public class RestFacadeBean implements RestFacadeLocal {

	private static final Logger LOG = LoggerFactory.getLogger(RestFacadeBean.class);

	private static final String TEMP = "temp_";

	@Autowired
	private SecurityCache securityCache;

	@Autowired
	private GeneratorFacadeRemote generatorFacade;

	@Autowired
	private LiveSearchFacadeRemote liveSearchFacade;

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private NuclosAuthenticationProvider nuclosAuthenticationProvider;

	@Autowired
	private PreferencesFacadeLocal preferencesFacade;

	@Autowired
	private SearchFilterFacadeLocal searchFilter;

	@Autowired
	private ResourceResolver resourceResolver;

	@Autowired
	private LayoutFacadeLocal layoutFacade;

	@Autowired
	private MetaProvider metaProvider;

	@Autowired
	private NuclosRemoteContextHolder remoteContext;

	@Autowired
	private EntityObjectFacadeRemote eoFacade;

	@Autowired
	private EntityObjectFacadeLocal eoFacadeLocal;

	@Autowired
	private StateFacadeRemote stateFacadeRemote;

	@Autowired
	private StateFacadeLocal stateFacadeLocal;

	@Autowired
	private NucletDalProvider nucletDalProvider;

	@Autowired
	private DatasourceFacadeLocal dsFacade;

	@Autowired
	private DatasourceServerUtils dssUtils;

	@Autowired
	private SpringDataBaseHelper dataBaseHelper;

	@Autowired
	private EventSupportCache esCache;

	@Autowired
	private EventSupportFacadeLocal esFacade;

	@Autowired
	private TransferFacadeLocal transferFacade;

	@Autowired
	private TreeNodeFacadeRemote treeNodeFacadeRemote;

	@Autowired
	private ServerParameterProvider serverParameter;

	@Autowired
	private NuclosParameterProvider nuclosParameterProvider;

	@Autowired
	private MasterDataRestFqnCache fqnCache;

	@Autowired
	private ReportFacadeRemote reportFacade;

	@Autowired
	private GenerationCache genCache;

	@Autowired
	private SpringInputContext inputContext;

	@Autowired
	private RemoteAuthenticationManager remoteAuthenticationManager;

	@Autowired
	private SecurityFacadeLocal securityFacade;

	@Autowired
	private NuclosUserDetailsContextHolder userCtx;

	@Autowired
	private TasklistFacadeLocal tasklistFacade;

	@Autowired
	private DataLanguageCache dataLanguageCache;

	@Autowired
	private MaintenanceFacadeLocal maintenanceFacade;

	@Transactional(noRollbackFor = {Exception.class})
	public SessionContext webLogin(
			String login,
			String password,
			Locale locale,
			String datalanguage,
			String jSessionId
	) {

		String username = null;

		if (maintenanceFacade.blockUserLogin(login)) {
			throw new NuclosWebException(Response.Status.SERVICE_UNAVAILABLE);
		}

		try {
			Authentication auth = new UsernamePasswordAuthenticationToken(login, password);

			// TODO: Authentication and logging of the attempt should happen in one call
			remoteAuthenticationManager.attemptAuthentication(login, password);
			auth = nuclosAuthenticationProvider.authenticate(auth);

			// login with email -> get login from authentication
			username = auth.getName();

			final List<MandatorVO> mandators = getMandators(username);
			final boolean restApiAllowed = isRestApiAllowed(username, mandators);

			if (!restApiAllowed) {
				throw new NuclosWebException(Response.Status.FORBIDDEN);
			}

			if (maintenanceFacade.blockUserLogin(username)) {
				throw new NuclosWebException(Response.Status.SERVICE_UNAVAILABLE);
			}

			LoginResult loginResult = securityFacade.login();
			assert StringUtils.equals(loginResult.getjSessionId(), jSessionId);
			SessionContext webSessionContext = securityCache.setSessionContext(
					auth,
					locale,
					jSessionId,
					loginResult.getSessionId()
			);

			webSessionContext.setMandators(mandators);
			SecurityContextHolder.getContext().setAuthentication(auth);
			LocaleContextHolder.setLocale(webSessionContext.getLocale());
			UID datalanguageUID = null;
			if (datalanguage != null) {
				datalanguage = datalanguage.toLowerCase();
				for (DataLanguageVO dataLanguageVO : dataLanguageCache.getDataLanguages()) {
					if (datalanguage.startsWith(dataLanguageVO.getLanguage().toLowerCase()) &&
							datalanguage.endsWith(dataLanguageVO.getCountry().toLowerCase())) {
						datalanguageUID = dataLanguageVO.getPrimaryKey();
						break;
					}
				}
			}
			final UID primaryDataLanguage = getPrimaryDataLanguage();
			if (primaryDataLanguage != null && datalanguageUID == null) {
				datalanguageUID = primaryDataLanguage;
			}
			webSessionContext.setDataLanguageUID(datalanguageUID);

			return webSessionContext;
		} catch (Exception e) {
			LOG.warn(e.getMessage(), e);
			if (username != null) {
				securityCache.removeSessionContextsForUser(username, null);
			}
			webLogout(null);
			throw e;
		}
	}

	private List<MandatorVO> getMandators(final String username) {
		List<MandatorVO> mandators = null;
		if (securityCache.isMandatorPresent()) {
			mandators = new ArrayList<>(securityCache.getAssignedMandators(username));
			Iterator<MandatorVO> it = mandators.iterator();
			while (it.hasNext()) {
				MandatorVO mandatorVO = it.next();
				if (!securityCache.getAllowedActions(username, mandatorVO.getUID()).contains(Actions.ACTION_RESTAPI)) {
					it.remove();
				}
			}
		}
		return mandators;
	}

	public boolean isRestApiAllowed(final String username) {
		List<MandatorVO> mandators = getMandators(username);

		return isRestApiAllowed(username, mandators);
	}

	@Override
	public Set<UsageCriteria> getUsageCriteriaBySearchExpression(final UID entity, final CollectableSearchExpression clctexpr) {
		return nucletDalProvider.getEntityObjectProcessor(entity).getUsageCriteriaBySearchExpression(clctexpr);
	}

	@Override
	public StateVO getState(final UID stateUID) throws CommonFinderException {
		return stateFacadeLocal.getState(stateUID);
	}

	private boolean isRestApiAllowed(
			final String username,
			final List<MandatorVO> mandators
	) {
		final boolean result;

		// TODO: Mandators auto-activate REST-API rights?!
		if (mandators == null || mandators.isEmpty()) {
			result = securityCache.getAllowedActions(username, null).contains(Actions.ACTION_RESTAPI);
		} else {
			result = true;
		}

		return result;
	}

	@Transactional(noRollbackFor= {Exception.class})
	public void chooseMandator(UID mandatorUID) throws CommonPermissionException {
		List<MandatorVO> assignedMandators = securityCache.getAssignedMandators(getCurrentUserName());
		for (MandatorVO mandatorVO : assignedMandators) {
			if (mandatorVO.getUID().equals(mandatorUID)) {
				if (securityCache.getAllowedActions(getCurrentUserName(), mandatorUID).contains(Actions.ACTION_RESTAPI)) {
					userCtx.setMandatorUID(mandatorUID);
					return;
				}
			}
		}
		throw new CommonPermissionException("Mandator not assigned");
	}

	@Transactional(noRollbackFor= {Exception.class})
	public void changePassword(String username, String password, String newPassword, String jSessionId) throws NuclosWebException {
		String currentUser = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
		if (!currentUser.equals("anonymousUser") && !securityCache.getAllowedActions(currentUser, null).contains(Actions.ACTION_RESTAPI)) {
			throw new NuclosWebException(Response.Status.PRECONDITION_FAILED);
		}

		try {
			remoteAuthenticationManager.changePassword(username, password, newPassword, getCustomUsage());
			securityCache.setAuthentication2NullForUser(username, jSessionId);
			securityCache.removeSessionContextsForUser(username, jSessionId);
		} catch (NuclosWebException nwe) {
			if (nwe.getResponse().getStatus() == 423) {
				//User locked from now on
				//log out to apply
				securityCache.setAuthentication2NullForUser(currentUser, null);
				securityCache.removeSessionContextsForUser(currentUser, null);
				SecurityContextHolder.clearContext();
			}
			throw nwe;
		} catch (BadCredentialsException | CommonPermissionException bce) {
			throw new NuclosWebException(Response.Status.FORBIDDEN);
		} catch (CommonBusinessException cbe) {
			throw new NuclosWebException(Response.Status.NOT_ACCEPTABLE);
		}
	}

	public SessionContext getSessionContext(String webSessionID) {
		if (webSessionID == null) {
			return null;
		}
		SessionContext webSessionContext = securityCache.getSessionContext(webSessionID);
		return webSessionContext;
	}

	public SessionContext validateSessionAuthenticationAndInitUserContext(String webSessionID) {
		SessionContext webSessionContext = getSessionContext(webSessionID);
		if (webSessionContext == null) {
			return null;
		}
		Authentication auth = webSessionContext.getAuthentication();
		if (auth == null) {
			try {
				UserDetails userDetails = userDetailsService.loadUserByUsername(webSessionContext.getUsername());
				Authentication nAuth = new UsernamePasswordAuthenticationToken(userDetails.getUsername(), userDetails.getPassword());
				auth = nuclosAuthenticationProvider.authenticate(nAuth);
				webSessionContext.setAuthentication(auth);
			} catch (Exception e) {
				LOG.warn(e.getMessage(), e);
			}
		}
		if (auth != null) {
			SecurityContextHolder.getContext().setAuthentication(auth);
			LocaleContextHolder.setLocale(webSessionContext.getLocale());
			userCtx.setMandatorUID(webSessionContext.getMandatorUID());
			userCtx.setDataLocal(webSessionContext.getDataLanguageUID());
			return webSessionContext;
		}
		webLogout(webSessionContext.getJSessionId());
		return null;
	}

	@Transactional(noRollbackFor= {Exception.class})
	public void webLogout(String webSessionID) {
		try {
			securityFacade.logout(webSessionID);
		} catch (LoginException e) {
			LOG.warn("Logout failed", e);
		}
		SecurityContextHolder.getContext().setAuthentication(null);
		if (webSessionID != null) securityCache.removeSessionContext(webSessionID);
	}

	public EntitySearchFilter2 getSearchFilterByPk(UID pk) throws CommonBusinessException {
		SearchFilterVO vo = searchFilter.getSearchFilterByPk(pk);
		return vo != null ? searchFilter.createEntitySearchFilterFromVO(vo) : null;
	}


	public List<SearchFilterVO> getSearchFilters() throws CommonBusinessException {
		List<SearchFilterVO> searchFilters = new ArrayList<SearchFilterVO>(searchFilter.getAllSearchFilterByCurrentUser());
		Collections.sort(searchFilters);
		return searchFilters;
	}


	public Resource getResource(UID uid) throws IOException {
		return resourceResolver.resolveDbResourceIcon(uid);
	}

	public WorkspaceVO getDefaultWorkspace() throws CommonBusinessException {
		return preferencesFacade.getDefaultWorkspace(getCurrentUserName());
	}

	public EntityPermission getEntityPermission(UID entity) {
		EntityMeta<Object> eMeta = metaProvider.getEntity(entity);
		boolean statemodel = eMeta.isStateModel();
		String user = getCurrentUserName();
		UID mandatorUID = getCurrentMandatorUID();
		try {
			securityCache.checkMandatorLoginPermission(user, mandatorUID);
		} catch (CommonPermissionException e) {
			throw new NuclosFatalException(e);
		}
		boolean readAllowed = securityCache.isReadAllowedForEntity(user, entity, mandatorUID);
		boolean writeAllowed = eMeta.isEditable() &&
				(statemodel ? securityCache.isWriteAllowedForModule(user, entity, mandatorUID) : securityCache.isWriteAllowedForMasterData(user, entity, mandatorUID));

		boolean updateAllowed = writeAllowed;
		boolean insertAllowed = writeAllowed;
		boolean deleteAllowed = writeAllowed;

		//With state-model the initial transition must be allowed, too
		if (insertAllowed && statemodel) {
			insertAllowed = securityCache.isNewAllowedForModule(user, entity, mandatorUID);
		}

		if (deleteAllowed) {
			if (statemodel) {
				deleteAllowed = securityCache.isDeleteAllowedForModule(user, entity, true, mandatorUID);
			} else {
				// refs NUCLOS-5027
				deleteAllowed = securityCache.isDeleteAllowedForMasterData(user, entity, mandatorUID);
			}
		}

		return new EntityPermission(readAllowed, updateAllowed, insertAllowed, deleteAllowed);
	}

	@Override
	public boolean isDatasourceAllowed(final UID datasourceUID) {
		return securityCache.getReadableDataSourceIds(getCurrentUserName(), getCurrentMandatorUID()).contains(datasourceUID);
	}

	public boolean isWriteAllowedForGO(UID entity) {
		return securityCache.isWriteAllowedForModule(getCurrentUserName(), entity, getCurrentMandatorUID());
	}

	public boolean isDeleteAllowedForGO(UID entity, boolean bPhysically) {
		return securityCache.isDeleteAllowedForModule(getCurrentUserName(), entity, bPhysically, getCurrentMandatorUID());
	}

	public Collection<FieldMeta<?>> getAllFieldsByEntity(UID entity, boolean onlyReferenceFields) {
		Collection<FieldMeta<?>> result = new HashSet<FieldMeta<?>>();
		Map<UID, FieldMeta<?>> mapFields = metaProvider.getAllEntityFieldsByEntity(entity);
		for (FieldMeta<?> fmvo : mapFields.values()) {
			if (onlyReferenceFields && fmvo.getForeignEntity() == null) {
				continue;
			}
			result.add(fmvo);
		}
		return result;
	}

	public boolean isSuperUser(){
		return securityCache.isSuperUser(getCurrentUserName());
	}

	public boolean isReadAllowedForField(FieldMeta<?> fm, UsageCriteria usage) {
		return isReadOrWriteAllowedForField(fm, usage, false);
	}

	public boolean isWriteAllowedForField(FieldMeta<?> fm, UsageCriteria usage) {
		return isReadOrWriteAllowedForField(fm, usage, true);
	}

	private boolean isReadOrWriteAllowedForField(FieldMeta<?> fm, UsageCriteria usage, boolean bWrite) {

		if (SF.CHANGEDAT.checkField(fm.getFieldName()) ||
				SF.CHANGEDBY.checkField(fm.getFieldName()) ||
				SF.CREATEDAT.checkField(fm.getFieldName()) ||
				SF.CREATEDBY.checkField(fm.getFieldName())
				) {
			return bWrite ? false : true;
		}

		return securityCache.isReadOrWriteAllowedForField(fm, usage, bWrite, getCurrentUserName(), getCurrentMandatorUID());
	}

	public boolean isReadAllowedForSubform(UID subform, UsageCriteria usage) {
		return isReadOrWriteAllowedForSubform(subform, usage, false);
	}

	public boolean isWriteAllowedForSubform(UID subform, UsageCriteria usage) {
		return isReadOrWriteAllowedForSubform(subform, usage, true);
	}

	private boolean isReadOrWriteAllowedForSubform(UID subform, UsageCriteria usage, boolean bWrite) {
		return securityCache.isReadOrWriteAllowedForSubform(subform, usage, bWrite,
				getCurrentUserName(), getCurrentMandatorUID());
	}

	public SubformPermission getSubformPermission(UID subform, UsageCriteria usage) {
		return securityCache.getSubformPermission(subform, usage, getCurrentUserName(), getCurrentMandatorUID());
	}

	@Override
	public boolean isUnlockAllowed(UID entity) {
		final EntityMeta<Object> eMeta = metaProvider.getEntity(entity);
		if (eMeta.isOwner()) {
			if (eMeta.getUnlockMode() == UnlockMode.ALL_USERS_MANUALLY || isSuperUser()) {
				return true;
			}
		}
		return false;
	}

	public <PK> void unlock(UID entity, PK pk) throws CommonPermissionException {
		eoFacade.unlock(entity, pk);
	}

	public Long countEntityObjectRows(UID entity, CollectableSearchExpression clctexpr) throws CommonPermissionException {
		remoteContext.setRemotly(true);
		return eoFacade.countEntityObjectRows(entity, clctexpr);
	}

	public <PK> Collection<EntityObjectVO<PK>> getEntityObjectsChunk(UID entity, CollectableSearchExpression clctexpr, ResultParams resultParams)
		throws CommonPermissionException {
		remoteContext.setRemotly(true);
		return eoFacade.getEntityObjectsChunk(entity, clctexpr, resultParams, getCustomUsage());
	}

	@Override
	public <PK> Collection<PK> getEntityObjectIds(UID entity, CollectableSearchExpression clctexpr)
			throws CommonPermissionException {
		remoteContext.setRemotly(true);
		return eoFacade.getEntityObjectIds(entity, clctexpr);
	}

	@Transactional(noRollbackFor = Exception.class)
	public <PK> Collection<EntityObjectVO<PK>> getEntityObjectsChunkWithoutRollback(
			UID entity,
			CollectableSearchExpression clctexpr,
			ResultParams resultParams
	) throws CommonPermissionException {
		return eoFacade.getEntityObjectsChunk(entity, clctexpr, resultParams, getCustomUsage());
	}

	public UID getInitialStateId(UID entity) {
		UsageCriteria usagecriteria = new UsageCriteria(entity, null, null, getCustomUsage());
		return stateFacadeLocal.getInitialState(usagecriteria).getId();
	}

	@Override
	public <PK> EntityObjectVO<PK> getThin(UID entity, PK pk, boolean remotely) throws CommonPermissionException {
		IEntityObjectProcessor<PK> processor = null;
		try {
			processor = nucletDalProvider.getEntityObjectProcessor(entity);
			processor.setThinReadEnabled(true);
		} catch (Exception ex) {
			LOG.warn("No processor for entity " + entity.getString());
		}
		try {
			return get(entity, pk, remotely);
		} finally {
			if (processor != null) {
				processor.setThinReadEnabled(false);
			}
		}
	}

	@Override
	public <PK> EntityObjectVO<PK> get(UID entity, PK pk, boolean remotely) throws CommonPermissionException {
		if (remotely) {
			remoteContext.setRemotly(true);
			return eoFacade.get(entity, pk);
		} else {
			return eoFacadeLocal.get(entity, pk);
		}
	}

	public <PK> EntityObjectVO<PK> getDataForField(PK pk, FieldMeta<?> fm) {
		EntityMeta<PK> em = metaProvider.getEntity(fm.getEntity());
		Set<FieldMeta<?>> fields = new HashSet<FieldMeta<?>>();
		fields.add(fm);
		final IEntityObjectProcessor<PK> eop = nucletDalProvider.getEntityObjectProcessor(em);
		final boolean ignoreRecordGrantsAndOthers = eop.getIgnoreRecordGrantsAndOthers();
		try {
			eop.setIgnoreRecordGrantsAndOthers(true);
			return eop.getByPrimaryKey(pk, fields);
		} finally {
			eop.setIgnoreRecordGrantsAndOthers(ignoreRecordGrantsAndOthers);
		}
	}

	public List<CollectableField> getReferenceList(
			UID field,
			String search,
			Long lMaxRowCount,
			WebValueListProvider vlp,
			String pk,
			UID mandatorUID
	) throws CommonBusinessException {
		FieldMeta<?> efMeta = metaProvider.getEntityField(field);

		if (vlp != null && ValuelistProviderVO.Type.NAMED.getType().equals(vlp.getType())) {
			List<CollectableField> result = getVLPData(
					field,
					vlp,
					pk,
					search,
					DEFAULT_RESULT_LIMIT,
					false,
					mandatorUID
			);

			if (!StringUtils.looksEmpty(search)) {
				Iterator<CollectableField> it = result.iterator();
				while ( it.hasNext()) {
					CollectableField clct = it.next();
					String sValue = (String) clct.getValue();
					if (!sValue.toUpperCase().contains(search.toUpperCase())) {
						it.remove();
					}
				}
			}
			return result;
		}

		// Empty values in params produces invalid SQL. 
		// -> we replace "" with null.
		Map<String, Object> vlpParams = new HashMap<String, Object>();
		if (vlp != null && vlp.getParameters() != null) {
			for (String key : vlp.getParameters().keySet()) {
				Object value = vlp.getParameters().get(key);
				if ("".equals(value)) {
					value = null;
				}
				vlpParams.put(key, value);
			}
		}

		mandatorUID = getMandatorForReferenceList(mandatorUID);

		List<CollectableValueIdField> result = eoFacadeLocal.getReferenceList(
				efMeta,
				search,
				vlp == null ? null : vlp.getValue(),
				vlp == null ? null : vlpParams,
				null,
				lMaxRowCount,
				mandatorUID
		);

		return RigidUtils.uncheckedCast(result);
	}

	public <T> List<CollectableField> getValueList(FieldMeta<T> field, List<String> sSearch, FieldMeta<Long> referencingField, Long boId) {
		final EntityMeta<Long> entity = metaProvider.<Long>getEntity(field.getEntity());
		final boolean isStringField = field.getJavaClass().equals(String.class);

		final DbAccess dbAccess = SpringDataBaseHelper.getInstance().getDbAccess();
		final DbQueryBuilder queryBuilder = dbAccess.getQueryBuilder();
		final DbQuery<DbTuple> q = queryBuilder.createTupleQuery();
		final DbFrom<Long> from = q.from(entity);
		final DbColumnExpression<T> column;
		final DbColumnExpression<T> columnLowerForGroupBy;
		if ((entity.isDynamic() || entity.isChart()) && (field.isDynamic())) {
			boolean bCaseSensitive = DbFrom.useCaseSensitiveConcerningUdGenericObject(entity.getDbTable(), field.getDbColumn());
			column = from.baseColumnSensitiveOrInsensitive(field, bCaseSensitive, false);
			columnLowerForGroupBy = (DbColumnExpression<T>) from.baseColumnSensitiveOrInsensitive(field, bCaseSensitive, false).alias("nuclosLowerColumnForGroupBy");
		} else {
			column = from.baseColumn(field);
			columnLowerForGroupBy = (DbColumnExpression<T>) from.baseColumn(field).alias("nuclosLowerColumnForGroupBy");
		}
		final List selections = new ArrayList();
		selections.add(column);
		if (isStringField) {
			/* Um Postgres Fehler zu verhindern...
			 * FEHLER: bei SELECT DISTINCT müssen ORDER-BY-Ausdrücke in der Select-Liste erscheinen
			 * SQL Status:42P10
			 * Zeichen:2034
			 */
			selections.add(queryBuilder.lower((DbExpression<String>) columnLowerForGroupBy));
		}
		final DbQuery<DbTuple> select = q.multiselect(selections).distinct(true);
		if (referencingField != null) {
			q.where(queryBuilder.equalValue(from.baseColumn(referencingField), boId));
		}
		if (sSearch != null && sSearch.size() > 0 && !StringUtils.looksEmpty(sSearch.get(0))) {
			final DbCondition condition;
			if (isStringField) {
				condition = queryBuilder.like((DbExpression<String>) column, sSearch.get(0).replace('*', '%'));
			} else {
				condition = queryBuilder.equalValue(column, field.getJavaClass().cast(sSearch.get(0)));
			}
			if (referencingField != null) {
				q.addToWhereAsAnd(condition);
			} else {
				q.where(condition);
			}
		}
		if (entity.isMandator() && getCurrentMandatorUID() != null) {
			q.addToWhereAsAnd(queryBuilder.equalValue(from.baseColumn(SF.MANDATOR_UID), getCurrentMandatorUID()));
		}

		q.groupBy(column);
		q.orderBy(queryBuilder.asc(column));
		q.limit(1000L);
		final List<CollectableField> result = dbAccess.executeQuery(q, new Transformer<DbTuple, CollectableField>(){
			@Override
			public CollectableField transform(DbTuple tuple) {
				return new CollectableValueField(tuple.get(0));
			}});

		return result;
	}

	public List<CollectableField> getVLPData(
			UID field,
			WebValueListProvider vlp,
			String pk,
			String quickSearchInput,
			Integer maxRowCount,
			boolean bDefault,
			UID mandatorUID
	) throws CommonBusinessException {

		Map<String, Object> queryParams = new HashMap<>();
		Map<String, Object> params = vlp.getParameters();

		String sNameFieldName = null;
		String sIdFieldName = null;

		UID sessionDataLanguageUID = RigidUtils.defaultIfNull(getCurrentDataLanguage(), getPrimaryDataLanguage());
		queryParams.put("locale", sessionDataLanguageUID);

		for (String key : params.keySet()) {
			Object sValue = params.get(key);

			if (key.equals(ValuelistProviderVO.DATASOURCE_NAMEFIELD)) {
				sNameFieldName = VLPUtils.getNameField(sValue, queryParams);

			} else if(key.equals(ValuelistProviderVO.DATASOURCE_IDFIELD)) {
				sIdFieldName = VLPUtils.getIdField(sValue, queryParams, vlp.getValue(), dsFacade);

			} else if (sValue instanceof String && UID.isStringifiedUID((String)sValue)) {
				queryParams.put(key, UID.parseUID((String)sValue));

			} else {
				queryParams.put(key, sValue);

			}
		}

		if (ValuelistProviderVO.Type.NAMED.getType().equals(vlp.getType())) {
			// TODO
			EntityMeta<?> TODO = null;
			String vlpName = translateUid(TODO, vlp.getValue());
			UID entityUID = metaProvider.getEntityField(field).getEntity();
			if (ValueListProviderType.STATUS.getType().equals(vlpName)) {

				Collection<StateVO> states;
				if (params.get("processId") != null) {
					UID processId = translateFqn(E.PROCESS, (String)params.get("processId"));
					UsageCriteria uc = new UsageCriteria(entityUID, processId, null, null);
					UID stateModelId = stateFacadeLocal.getStateModelId(uc);
					states = stateFacadeLocal.getStatesByModel(stateModelId);
				} else {
					states = stateFacadeLocal.getStatesByModule(entityUID);
				}
				List<CollectableField> result = new ArrayList<>();
				for (StateVO state : states) {
					result.add(
							new CollectableValueIdField(state.getId(), state.getNumeral() + " - " + state.getStatename(getCurrentLocale()))
					);
				}

				Collections.sort(result);
				return result;

			} else if (ValueListProviderType.PROCESS.getType().equals(vlpName)) {

				List<CollectableField> result = getAllowedProcessesByEntity(getCurrentUserName(), entityUID, pk);
				Collections.sort(result);
				return result;

			} else if (ValueListProviderType.MANDATOR.getType().equals(vlpName)) {

				UID levelUID = metaProvider.getEntity(entityUID).getMandatorLevel();

				List<CollectableField> result = new ArrayList<>();
				for (MandatorVO mandator : securityCache.getMandatorsByLevel(levelUID)) {
					result.add(new CollectableValueIdField(mandator.getUID(), mandator.toString()));
				}
				Collections.sort(result);
				return result;

			} else if (ValueListProviderType.USER_COMMUNICATION_ACCOUNT.getType().equals(vlpName)) {

				Class<? extends RequestContext<?>> requestContextClass = null;
				if (params.get("RequestContext") != null) {
					try {
						requestContextClass = (Class<? extends RequestContext<?>>) Class.forName("org.nuclos.api.context.communication." + params.get("RequestContext"));
					} catch (ClassNotFoundException cnfe) {
						LOG.warn(cnfe.getMessage(), cnfe);
					}
				}

				UID userUID = null;
				if (params.get("selfId") != null) {
					userUID = (UID) params.get("selfId");
				}

				return securityFacade.getUserCommunicationAccounts(userUID, requestContextClass);
			} else if (ValueListProviderType.SYSTEM_PARAMETER.getType().equals(vlpName)) {
				return CollectionUtils.transform(ParameterProvider.listSystemParameter(), CollectableValueField::new);
			}

			throw new UnsupportedOperationException("Named VLP " + vlp.getValue() + " not supported yet!");
		}

		mandatorUID = getMandatorForReferenceList(mandatorUID);

		List<CollectableValueIdField> result = dsFacade.executeQueryForVLP(
				new VLPQuery(vlp.getValue())
						.setQueryParams(queryParams)
						.setIdFieldName(sIdFieldName)
						.setMandatorUID(mandatorUID)
						.setNameField(sNameFieldName)
						.setEntityFieldUID(field)
						.setQuickSearchInput(quickSearchInput)
						.setMaxRowCount(maxRowCount)
		);
		return RigidUtils.uncheckedCast(result);
	}

	private UID getMandatorForReferenceList(UID mandatorFromClient) throws CommonPermissionException {
		if (mandatorFromClient != null) {
			securityCache.checkMandatorLoginPermission(getCurrentUserName(), mandatorFromClient);
			return mandatorFromClient;
		} else {
			return getCurrentMandatorUID();
		}
	}

	/**
	 *
	 * @param user String username
	 * @param entityUID UID entity
	 * @param spk String representation of primary key of the genericc object, e.g. spk = "4000392".
	 * 		 spk = "-1"/null means the allowed processes for a new bo are requested
	 * 		 spk = "0" means the check for write permission is only on module permissions, not for a specific generic object.
	 *
	 * @return List of allowed processed
	 */
	private List<CollectableField> getAllowedProcessesByEntity(String user, UID entityUID, String spk) {
		List<CollectableValueIdField> lstProcesses = eoFacadeLocal.getProcessByEntity(entityUID, false);

		ModulePermissions permissions = securityCache.getModulePermissions(user, getCurrentMandatorUID());

		Collection<UID> allowedProcesses;

		Long pk = null;
		if (spk != null && !spk.isEmpty()) {
			try {
				pk = Long.parseLong(spk);
			} catch (NumberFormatException nfe) {
				LOG.debug(nfe.getMessage(), nfe);
			}
		}

		if (pk == null || pk < 0) {
			allowedProcesses = permissions.getNewAllowedProcessesByModuleUid().get(entityUID);

		} else {

			boolean bAllowedToUpdate = pk == 0 ? getEntityPermission(entityUID).isUpdateAllowed()
					: ModulePermission.includesWriting(permissions.getMaxPermissionForGenericObject(entityUID));

			if (!bAllowedToUpdate) {
				return Collections.EMPTY_LIST;
			}

			allowedProcesses = new HashSet<UID>();
			for (CollectableField process :lstProcesses) {
				allowedProcesses.add((UID)process.getValueId());
			}
		}

		if (allowedProcesses == null || allowedProcesses.isEmpty()) {
			return Collections.EMPTY_LIST;
		}

		List<CollectableField> result = new ArrayList<>();

		for (CollectableField processField : lstProcesses) {
			if (allowedProcesses.contains(processField.getValueId())) {
			    result.add(new CollectableValueIdField(translateUid(E.PROCESS, (UID)processField.getValueId()), processField.getValue()));
			}
		}

		return result;
	}

	public boolean doesLayoutChangeWithProcess(Collection<UID> entities) {
		String user = getCurrentUserName();

		for (UID entityUID : entities) {

			Collection<StateVO> collStates = stateFacadeLocal.getStatesByModule(entityUID);

			//First check if there are layout changes for a new BO:
			List<CollectableField> lstProcessesNew = getAllowedProcessesByEntity(user, entityUID, null);
			if (doesLayoutChangeForThoseProcesses(lstProcessesNew, collStates, entityUID)) {
				return true;
			}

			//Second check if there are layout changed for an existing BO:
			List<CollectableField> lstProcessesExisting = getAllowedProcessesByEntity(user, entityUID, "0");
			if (doesLayoutChangeForThoseProcesses(lstProcessesExisting, collStates, entityUID)) {
				return true;
			}

		}

		return false;
	}

	private boolean doesLayoutChangeForThoseProcesses(List<CollectableField> lstProcesses, Collection<StateVO> collStates, UID entityUID) {
		String sCustom = getCustomUsage();
		Set<UID> layoutUIDs = new HashSet<UID>();

		for (StateVO state : collStates) {
			UID statusUID = state.getId();

			for (CollectableField cf : lstProcesses) {

				if (cf instanceof CollectableValueIdField) {
					String processFQN = (String) ((CollectableValueIdField)cf).getValueId();
					UID processUID = translateFqn(E.PROCESS, processFQN);

					UsageCriteria testUsage = new UsageCriteria(entityUID, processUID, statusUID, sCustom);
					layoutUIDs.add(layoutFacade.getDetailLayoutIDForUsage(testUsage));

					// consider also layouts without process
					testUsage = new UsageCriteria(entityUID, null, statusUID, sCustom);
					layoutUIDs.add(layoutFacade.getDetailLayoutIDForUsage(testUsage));

					if (layoutUIDs.size() >= 2) {
						return true;
					}

				}
			}
		}

		return false;
	}

	public List<DatasourceParameterVO> getParameterListForDataSource(UID entity) throws NuclosDatasourceException {
		return dssUtils.getParameters(entity);
	}

	public String getParamRestMenuEntities() {
		return serverParameter.getValue(ParameterProvider.REST_MENU_ENTITIES);
	}

	public String getInitialEntityEntry() {
		String user = getCurrentUserName();
		String entries = serverParameter.getValue(ParameterProvider.INITIAL_ENTRY);
		if (!StringUtils.looksEmpty(entries)) {
			for (String entry : entries.split(",")) {
				if (entry.startsWith(user + ":")) {
					return entry.substring(user.length() + 1);
				}
			}
		}

		return null;
	}

	@Transactional(rollbackFor= {Throwable.class})
	public <PK> EntityObjectVO<PK> insert(EntityObjectVO<PK> eoVO) throws CommonBusinessException {
		return doInsert(eoVO);
	}

	@Transactional(noRollbackFor = Exception.class)
	public <PK> EntityObjectVO<PK> insertWithoutRollback(EntityObjectVO<PK> eoVO) throws CommonBusinessException {
		return doInsert(eoVO);
	}

	private <PK> EntityObjectVO<PK> doInsert(EntityObjectVO<PK> eoVO) throws CommonBusinessException {
		convertNumbers(eoVO);
		String gpoId = eoVO.getFieldValue(RValueObject.TEMPORARY_ID_FIELD, String.class);
		remoteContext.setRemotly(true);
		EntityObjectVO<PK> result = eoFacade.insert(eoVO);
		if (gpoId != null) {
			evictTemporaryObject(gpoId);
		}
		return result;
	}

	@Transactional(rollbackFor= {Throwable.class})
	public <PK> EntityObjectVO<PK> update(EntityObjectVO<PK> eoVO) throws CommonBusinessException {
		return doUpdate(eoVO);
	}

	@Transactional(noRollbackFor = Exception.class)
	public <PK> EntityObjectVO<PK> updateWithoutRollback(EntityObjectVO<PK> eoVO) throws CommonBusinessException {
		return doUpdate(eoVO);
	}


	private <PK> EntityObjectVO<PK> doUpdate(EntityObjectVO<PK> eoVO) throws CommonBusinessException {
		return doUpdate(eoVO, true);
	}

	private <PK> EntityObjectVO<PK> doUpdate(EntityObjectVO<PK> eoVO, boolean remotely) throws CommonBusinessException {
		convertNumbers(eoVO);

		// handle state change
		UID stateChangedAction = eoVO.getFieldUid(SF.STATE_CHANGED_ACTION);
		boolean bStateChange = stateChangedAction != null;

		remoteContext.setRemotly(remotely);
		if (bStateChange) {
			GenericObjectWithDependantsVO gowdvo = DalSupportForGO.getGenericObjectWithDependantsVO((EntityObjectVO<Long>) eoVO);
        	stateFacadeRemote.changeStateAndModifyByUser(eoVO.getDalEntity(), gowdvo, stateChangedAction, getCustomUsage());
        } else {
        	eoFacade.update(eoVO, false);
        }

		return eoFacade.get(eoVO.getDalEntity(), eoVO.getPrimaryKey());
	}

	@Transactional(rollbackFor= {Throwable.class})
	public <PK> void delete(UID entity, PK pk, boolean logicalDeletion) throws CommonBusinessException {
		doDelete(entity, pk, logicalDeletion);
	}

	@Transactional(noRollbackFor = Exception.class)
	public <PK> void deleteWithoutRollback(UID entity, PK pk, boolean logicalDeletion) throws CommonBusinessException {
		doDelete(entity, pk, logicalDeletion);
	}

	private <PK> void doDelete(UID entity, PK pk, boolean logicalDeletion) throws CommonBusinessException {
		remoteContext.setRemotly(true);
		eoFacade.delete(entity, pk, logicalDeletion, false);
	}

	@Override
	public <PK> EntityObjectVO<PK> clone(final EntityObjectVO<PK> eoVO, final UID layout, final boolean fetchFromDb) throws CommonBusinessException {
		EntityObjectVO<PK> clonedEo = eoFacade.clone(eoVO, layout, fetchFromDb);

		prepareNewTemporaryObject(clonedEo, getCurrentUserName());
		return clonedEo;
	}

	public Collection<StateVO> getSubsequentStates(UsageCriteria usagecriteria) throws CommonBusinessException {
		//remoteContext.setRemotly(true);
		// setRemotly not working on local interfaces!
		return stateFacadeLocal.getSubsequentStates(usagecriteria, false, usagecriteria.getStatusUID());
	}

	@Override
	public Collection<StateTransitionVO> getStateTransitions(final UsageCriteria usageCriteria) {
		if (usageCriteria.getStatusUID() == null) {
			return Collections.emptyList();
		}
		return stateFacadeLocal.findStateTransitionBySourceStateNonAutomatic(usageCriteria.getStatusUID());
	}

	@Override
	public UID getTargetStateId(final UID stateTransitionUID) throws CommonFinderException {
		return stateFacadeLocal.getTargetStateId(stateTransitionUID);
	}

	@Transactional(rollbackFor= {Throwable.class})
	public void changeState(UID entity, Long pk, UID newState) throws CommonBusinessException {
		doChangeState(entity, pk, newState);
	}

	@Transactional(noRollbackFor = Exception.class)
	public void changeStateWithoutRollback(UID entity, Long pk, UID newState) throws CommonBusinessException {
		doChangeState(entity, pk, newState);
	}

	private void doChangeState(UID entity, Long pk, UID newState) throws CommonBusinessException {
		remoteContext.setRemotly(true);
		stateFacadeRemote.changeStateByUser(entity, pk, newState, getCustomUsage());
	}

	public <PK> Collection<EntityObjectVO<PK>> getSubformData(UID subform, UID field, PK relatedPK, CollectableSearchExpression filterExpr, ResultParams resultParams) {
		remoteContext.setRemotly(true);

		final CompositeCollectableSearchCondition cond = new CompositeCollectableSearchCondition(LogicalOperator.AND);
		final CollectableSearchCondition cond1 = SearchConditionUtils.newPkComparison(field, ComparisonOperator.EQUAL, relatedPK);
		cond.addOperand(cond1);

		if (filterExpr.getSearchCondition() != null) {
			cond.addOperand(filterExpr.getSearchCondition());
		}
		filterExpr.setSearchCondition(cond);

		return nucletDalProvider.<PK>getEntityObjectProcessor(subform).getBySearchExprResultParams(filterExpr, resultParams);
	}

	public <PK> Collection<EntityObjectVO<PK>> getDependentData(UID subform, UID field, PK relatedPK) {
		return eoFacadeLocal.getDependentEntityObjectsNoCheck(subform, field, relatedPK);
	}

	public List<LuceneSearchResult> luceneIndexSearch(String searchString) {
		remoteContext.setRemotly(true);
		return liveSearchFacade.nuclosIndexSearch(searchString);
	}

	public String getFieldGroupName(UID groupID) {
		if (groupID == null) {
			return null;
		}
		return eoFacadeLocal.getFieldGroupName(groupID);
	}

	public String getDbVersion() {
		AutoDbSetup autoSetup = new AutoDbSetup(dataBaseHelper.getDbAccess(), E.getThis(), ApplicationProperties.getInstance().getNuclosVersion());
		Pair<String, Date> res = autoSetup.determineLastVersion();
		if (res == null) {
			throw new CommonFatalException("Could not determine last DB Version");
		}
		return res.x;
	}

	public boolean isActionAllowed(String action) {
		return securityCache.getAllowedActions(getCurrentUserName(), getCurrentMandatorUID()).contains(action);
	}

	@Override
	public Collection<UID> getAllowedStateTransitions() {
		return securityCache.getTransitionUids(getCurrentUserName(), getCurrentMandatorUID());
	}

	@Transactional(rollbackFor= {Throwable.class})
	public <PK> EntityObjectVO<PK> fireCustomEventSupport(EntityObjectVO<PK> eoVO, String rule) throws NuclosCompileException, CommonBusinessException {
		return doFireCustomEventSupport(eoVO, rule, true);
	}

	@Transactional(noRollbackFor = Exception.class)
	public <PK> EntityObjectVO<PK> fireCustomEventSupportWithoutRollback(EntityObjectVO<PK> eoVO, String rule) throws NuclosCompileException, CommonBusinessException {
		return doFireCustomEventSupport(eoVO, rule, false);
	}

	private <PK> EntityObjectVO<PK> doFireCustomEventSupport(EntityObjectVO<PK> eoVO, String rule, boolean checkPermissions) throws NuclosCompileException, CommonBusinessException {
		convertNumbers(eoVO);

		EventSupportSourceVO eseVO = esCache.getEventSupportSourceByClassname(rule);
		if (eseVO == null) {
			throw new CommonFinderException("There is no such custom rule '" + rule + "'");
		}

		// NUCLOS-5995 Use this instead of esFacade, automatically solves NUCLOS-5994
		EntityObjectVO<PK> eo = eoFacade.executeBusinessRules(Collections.singletonList(eseVO), eoVO, getCustomUsage(), false);

		// TODO: This is done to ensure the same behavior as the rich client. Check if this is correct
		if (eo.getPrimaryKey() != null) {
			eo = get(eo.getDalEntity(), eo.getPrimaryKey(), false);
		}

		return eo;
	}

	public Collection<UID> getEntitiesAssignedToLayoutId(UID layoutUID) throws CommonBusinessException {
		if (layoutUID == null) {
			return Collections.EMPTY_SET;
		}

		return layoutFacade.getEntitiesAssignedToLayoutId(layoutUID);
	}

	public String getLayoutML(UID pk) throws CommonBusinessException {
		if(pk == null) {
			return null;
		}
		return layoutFacade.getLayoutML(pk);
	}

	public LayoutInfo getLayoutInfo(UID layoutUID) throws CommonPermissionException {
		EntityObjectVO eo = eoFacade.get(E.LAYOUT.getUID(), layoutUID);
		return new LayoutInfo(eo);
	}

	public Set<UID> getLayoutUIDsForEntity(UID entityUID) {
		return layoutFacade.getAllLayoutUidsForEntity(entityUID);
	}

	/**
	 * Fetches thin Layout EOs and wraps them in LayoutInfo objects.
	 *
	 * @param entityUID
	 * @return
	 * @throws CommonBusinessException
	 */
	public List<LayoutInfo> getLayoutInfosForEntity(UID entityUID) {
		final List<LayoutInfo> result = new ArrayList<>();

		final ArrayList<UID> layoutUIDs = new ArrayList<>();
		layoutUIDs.addAll(layoutFacade.getAllLayoutUidsForEntity(entityUID));

		Collection<UID> fields = Arrays.<UID>asList(
				E.LAYOUT.name.getUID(),
				E.LAYOUT.description.getUID(),
				E.LAYOUT.nuclet.getUID()
		);
		ResultParams resultParams = new ResultParams(fields, 0L, 1000L, true);
		final Collection<EntityObjectVO<UID>> data = eoFacadeLocal.getEntityObjectsChunkNoCheck(
				E.LAYOUT.getUID(),
				new CollectableSearchExpression(
						SearchConditionUtils.newPkInCondition(
								E.LAYOUT,
								layoutUIDs
						)
				),
				resultParams,
				null
		);

		for (EntityObjectVO<UID> eo: data) {
			final LayoutInfo layoutInfo = new LayoutInfo(eo);

			result.add(layoutInfo);
		}

		return result;
	}

	public boolean isLayoutRestrictionsMustIgnoreGroovyRules(UID pk) {
		if(pk == null) {
			return false;
		}
		try {
			UID nucletUID = layoutFacade.getNucletUID(pk);
			String ignoreGroovyRules = getNucletParameterByName(nucletUID, NuclosParameterProvider.WEBCLIENT_RESTRICTIONS_MUST_IGNORE_GROOVY_RULES);
			if ("true".equals(ignoreGroovyRules)) {
				return true;
			}
			return false;
		} catch (CommonBusinessException e) {
			return false;
		}
	}

	public UID getDetailLayoutIDForUsage(UsageCriteria usage, boolean bSearchLayout) {
		return layoutFacade.getDetailLayoutIDForUsage(usage, bSearchLayout);
	}

	public <PK> UsageCriteria getUsageCriteriaForPK(PK pk, UID entity) throws CommonBusinessException {
		return eoFacadeLocal.getUsageCriteriaForPK(pk, entity, getCustomUsage());
	}

	@Transactional(noRollbackFor= {Exception.class})
	public BoDataSet importOrExportDatabase(InputStream is)  throws CommonPermissionException {
		return transferFacade.importOrExportDatabase(is);
	}

	private <PK> void convertNumbers(EntityObjectVO<PK> eo) {
		Map<UID,Object> mapValues = eo.getFieldValues();
		for (UID field : mapValues.keySet()) {

			Object value = mapValues.get(field);
			if (value == null) continue;
			if (value instanceof Long) {
				Long lValue = (Long)value;
				FieldMeta<?> fm = metaProvider.getEntityField(field);
				if (fm.getDataType().equals("java.lang.Integer")) {
					eo.setFieldValue(field, lValue.intValue());
				}
			} else if (value instanceof BigDecimal) {
				BigDecimal bdValue = (BigDecimal) value;
				FieldMeta<?> fm = metaProvider.getEntityField(field);
				if (fm.getDataType().equals("java.lang.Double")) {
					eo.setFieldValue(field, bdValue.doubleValue());
				}
			}
		}
		IDependentDataMap map = eo.getDependents();
		Collection<List<EntityObjectVO<?>>> depeos = map.getRoDataMap().values();
		for (List<EntityObjectVO<?>> lstDepeo : depeos) {
			for (EntityObjectVO<?> depeo : lstDepeo) {
				convertNumbers(depeo);
			}
		}
	}

	//TODO: This here is the place where a special CustomUsage for RestService and thus Webclient could be defined. So the user has the
	//chance to define a special WebLayout-Usage.
	public String getCustomUsage() {
		return NuclosConstants.WEB_LAYOUT_CUSTOM_USAGE;
	}

	final Map<String, AbstractTreeNode<?>> PROTOTYPE_CACHE = new HashMap<String, AbstractTreeNode<?>>();

	private void buildNextLevel(AbstractTreeNode treeNode, JsonArrayBuilder result) {
		List<AbstractTreeNode> subnodes = treeNode.getSubNodes();
		if(subnodes != null) {
			for(AbstractTreeNode subnode : subnodes) {

				UID cacheKey = new UID();
				// TODO
				EntityMeta<?> TODO = null;
				PROTOTYPE_CACHE.put(translateUid(TODO, cacheKey), subnode);

				JsonObjectBuilder json = Json.createObjectBuilder();
				json.add("node_id", translateUid(TODO, cacheKey));
				json.add("boId", subnode.getId().toString());
				json.add("title", subnode.getLabel());

				try {
					if(subnode.getEntityUID() != null) {
						EntityMeta<UID> em = metaProvider.getEntity(subnode.getEntityUID());
						if (em.getNuclosResource() != null) {
							json.add("icon", em.getNuclosResource());
						}
						json.add("boMetaId", fqnCache.translateUid(E.ENTITY, subnode.getEntityUID()));
					}
				} catch(Exception e) {
					e.printStackTrace();
				}

				result.add(json);
			}
		}
	}


    public JsonArrayBuilder appendExpandedSubnotes(SimpleTreeNode tree, int deep){
        JsonArrayBuilder result = Json.createArrayBuilder();

        List<SimpleTreeNode> subnodes = tree.getSubNodes();
        if(subnodes != null) {
            deep++;
            for (SimpleTreeNode subnode : subnodes) {
                JsonObjectBuilder json = Json.createObjectBuilder();
                json.add("node_id", translateUid(null, new UID()));
                json.add("boId", subnode.getId().toString());
                json.add("title", subnode.getLabel());
                EntityMeta<UID> em = metaProvider.getEntity(subnode.getEntityUID());
                json.add("icon", em.getNuclosResource());
                try {
                    if(subnode.getEntityUID() != null) {
                        json.add("boMetaId", fqnCache.translateUid(E.ENTITY, subnode.getEntityUID()));
                    }
                } catch(Exception e) {
                    e.printStackTrace();
                }
                json.add("subNodes", appendExpandedSubnotes(subnode, deep));
                result.add(json);
            }
        }
        return result;
    }

	public JsonArrayBuilder getTreeWithSearch(String boMetaId, String boId, String search) throws CommonBusinessException {
		remoteContext.setRemotly(true);

	    DefaultMasterDataTreeNodeParameters params = new DefaultMasterDataTreeNodeParameters(Rest.translateFqn(E.ENTITY, boMetaId), new Long(boId), null, null,  "label", "description");
        DefaultMasterDataTreeNode treeNode = new DefaultMasterDataTreeNode<UID>(params);

        SimpleTreeNode expandedTreeNode = treeNodeFacadeRemote.getExpandedTreeWithSearch(treeNode, search);

	    return appendExpandedSubnotes(expandedTreeNode,0);
	}




    public JsonArrayBuilder getSubtree(String nodeId) {
		remoteContext.setRemotly(true);

		AbstractTreeNode<?> treeNode = PROTOTYPE_CACHE.get(nodeId);

		try {
			if (treeNode != null) {
				JsonArrayBuilder array = Json.createArrayBuilder();
				buildNextLevel(treeNode, array);
				return array;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public JsonArrayBuilder getTree(String boMetaId, String boId) {
		remoteContext.setRemotly(true);
		try {
			DefaultMasterDataTreeNodeParameters params = new DefaultMasterDataTreeNodeParameters(Rest.translateFqn(E.ENTITY, boMetaId), new Long(boId), null, null,  "label", "description");
			DefaultMasterDataTreeNode treeNode = new DefaultMasterDataTreeNode<UID>(params);

			JsonArrayBuilder array = Json.createArrayBuilder();
			buildNextLevel(treeNode, array);
			return array;

		} catch (Exception e) {
			throw NuclosWebException.wrap(e);
		}
	}

	public String translateUid(EntityMeta<?> eMeta, UID uid) {
		return fqnCache.translateUid(eMeta, uid);
	}

	public UID translateFqn(EntityMeta<?> eMeta, String fqn) {
		return fqnCache.translateFqn(eMeta, fqn);
	}

	public boolean hasPrintouts(UsageCriteria usagecriteria, Object boId) {
		Collection<DefaultReportVO> findReportsByUsage = reportFacade.findReportsByUsage(usagecriteria);
		if (findReportsByUsage == null || findReportsByUsage.isEmpty()) {
			return false;
		}
		return true;
	}

	public String getNucletParameterByName(UID nucletUID, String parameter) {
		return nuclosParameterProvider.getNucletParameterByName(nucletUID, parameter);
	}

	public <PK> EntityObjectVO<PK> getBoWithDefaultValues(UID entityUID) {
		EntityObjectVO<PK> eo = new EntityObjectVO<>(entityUID);
		eoFacadeLocal.setDefaultValues(eo);
		return eo;
	}

	@Transactional(rollbackFor= {Throwable.class})
	public BoGenerationResult generateBo(
			UID entityUID,
			Map<Long, Integer> eoIdsAndVersions,
			UID generatorUID
	) throws CommonBusinessException {
		return doGenerateBo(entityUID, eoIdsAndVersions, generatorUID);
	}

	@Transactional(noRollbackFor = Exception.class)
	public BoGenerationResult generateBoWithoutRollback(
			UID entityUID,
			Map<Long, Integer> eoIdsAndVersions,
			UID generatorUID
	) throws CommonBusinessException {
		return doGenerateBo(entityUID, eoIdsAndVersions, generatorUID);
	}

	private BoGenerationResult doGenerateBo(
			UID entityUID,
			Map<Long, Integer> boIdsToVersion,
			UID generatorUID
	) throws CommonBusinessException {
		//TODO NOAINT-629: parameterObjectId must no be null for Generations with ParameterEntity.
		remoteContext.setRemotly(true);
		final Long parameterObjectId = null;
		final boolean isCloningAction = false;

		GeneratorActionVO generatorAction = genCache.getGenerationAction(generatorUID);
		EntityObjectVO<Long> eoTarget;
		try {
			List<EntityObjectVO<Long>> eoSources = new ArrayList<EntityObjectVO<Long>>();
			for (Long boId : boIdsToVersion.keySet()) {
				Integer expectedVersion = boIdsToVersion.get(boId);
				EntityObjectVO<Long> eo = eoFacade.get(entityUID, boId);
				if (eo.getVersion() > expectedVersion) {
					throw new CommonStaleVersionException(eo, expectedVersion);
				}
				eoSources.add(eo);
			}

			GenerationContext<Long> context = new GenerationContextBuilder(eoSources, generatorAction)
					.parameterObjectId(parameterObjectId)
					.customUsage(getCustomUsage())
					.cloning(isCloningAction)
					.build();
			GenerationResult<Long> genResult = generatorFacade.generateGenericObjects(context);

			eoTarget = genResult.getGeneratedObject();

			if (generatorAction.isDoNotSave()) {
				prepareNewTemporaryObject(eoTarget, getCurrentUserName());
			}
			
			return new BoGenerationResult(generatorAction, eoTarget, null);

		} catch (GeneratorFailedException gfex) {
			// NUCLOS-6214
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();

			eoTarget = gfex.getGenerationResult().getGeneratedObject();
			String temporaryId = prepareNewTemporaryObject(eoTarget, getCurrentUserName());

			String error = null;
			if (gfex.getCause() instanceof BusinessException) {
				error = gfex.getGenerationResult().getError();
			}

			if (error == null) {
				CommonValidationException cve = NuclosExceptions.getCause(gfex, CommonValidationException.class);
				if (cve != null) {
					//TODO: Using Rest.java at this point is bad coding because of cycles.
					error = Rest.getFullMessage(cve, entityUID);
					if (cve.getClass() != CommonValidationException.class) {
						if (cve.getCause() instanceof NuclosBusinessRuleException) {
							error = (cve.getCause()).getLocalizedMessage();

						} else if (cve.getCause() instanceof CommonValidationException) {
							error = Rest.getFullMessage((CommonValidationException)cve.getCause(), entityUID);

						} else if (cve.getCause() != null && cve.getCause().getCause() instanceof CommonValidationException) {
							error = ((CommonValidationException)cve.getCause().getCause()).getFullMessage();

						}
					}
				}
			}

			if (generatorAction.isCloseOnException()) {
				throw gfex;
			}
			
			return new BoGenerationResult(generatorAction, eoTarget, error);
		}
	}

	@CachePut(value="restTemporaryObjects", key="#p0")
	private <PK> EntityObjectVO<PK> cacheTemporaryObject(String temporaryId, EntityObjectVO<PK> gpo) {
		return gpo;
	}

	@Cacheable(value="restTemporaryObjects", key="#p0")
	public EntityObjectVO<Long> getTemporaryObject(String temporaryId) throws CommonBusinessException {
		throw new NuclosBusinessException("Temporary object ID " + temporaryId + " does not exist any more. Try again.");
	}

	@CacheEvict(value="restTemporaryObjects", key="#p0")
	private void evictTemporaryObject(String temporaryId) {
	}

	public boolean isTemporaryId(String id) {
		return id != null && id.startsWith(TEMP);
	}

	@Override
	public String getTempId(String id) {
		return id.substring(TEMP.length());
	}

	private String prepareNewTemporaryObject(EntityObjectVO<?> eovo, String user) {
		String id = TEMP+getNextId();
		eovo.flagNew();
		eovo.setCreatedBy(user);
		eovo.setFieldValue(RValueObject.TEMPORARY_ID_FIELD, id);
		IDependentDataMap dependents = eovo.getDependents();
		if (dependents != null) {
			for (IDependentKey dependentKey : dependents.getKeySet()) {
				for (EntityObjectVO<?> dep : dependents.getData(dependentKey)) {
					prepareNewTemporaryObject(dep, user);
				}
			}
		} else {
			dependents = new DependentDataMap();
			eovo.setDependents(dependents);
		}
		eovo.setFieldValue(RValueObject.TEMPORARY_DEPENDENTS_ALREADY_LOADED_FIELD, new HashSet<IDependentKey>());

		cacheTemporaryObject(id, eovo);

		return id;
	}

	private Long getNextId() {
		return dataBaseHelper.getNextIdAsLong(SpringDataBaseHelper.DEFAULT_SEQUENCE);
	}

	public List<GeneratorActionVO> getGenerations(UsageCriteria usage) {
		UID mandator = getCurrentMandatorUID();
		List<GeneratorActionVO> generatorActions = new ArrayList<GeneratorActionVO>(genCache.getGeneratorActions());
		if (generatorActions == null) {
			return null;
		}

		Collection<UID> assignedGenerations = SecurityCache.getInstance().getAssignedGenerations(getCurrentUserName(), mandator);
		if (assignedGenerations == null) {
			return null;
		}

		GeneratorVO generator = new GeneratorVO(generatorActions);
		generatorActions = generator.getGeneratorActions(
				usage.getEntityUID(),
				usage.getStatusUID(),
				usage.getProcessUID(),
				mandator);

		if (generatorActions.isEmpty()) {
			return null;
		}

		return generatorActions;
	}

	public void updateInputContext(Map<String, Serializable> context) {
		inputContext.setSupported(true);
		if (context != null) {
			inputContext.set(context);
		}
	}

	public void clearInputContext() {
		inputContext.clear();
	}

	public List<EventSupportSourceVO> findEventSupportsByUsageAndEntity(String sEventclass, UsageCriteria usagecriteria) {
		return esFacade.findEventSupportsByUsageAndEntity(sEventclass, usagecriteria);
	}

	@Override
	public ValuelistProviderVO getValuelistProvider(UID vlpUID) throws CommonFinderException, CommonPermissionException {
		return dsFacade.getValuelistProvider(vlpUID);
	}

	@Override
	public Map<String, Object> getDatasourceParameters(String dsXML, Map<String, String> datasourceParams) throws CommonBusinessException, ClassNotFoundException {
		Map<String, Object> datasourceParamsWithObjectValues = new HashMap<>();

		final List<DatasourceParameterVO> lstParameters = dsFacade.getParametersFromXML(dsXML);
		for (DatasourceParameterVO dspvo: lstParameters) {
			final String datatype = dspvo.getDatatype();
			final String paramName = dspvo.getParameter();
			if (datasourceParams.containsKey(paramName)) {
				final String sValue = datasourceParams.get(paramName);
				if (String.class.getCanonicalName().equals(datatype)) {
					datasourceParamsWithObjectValues.put(paramName, sValue);
				} else if (Boolean.class.getCanonicalName().equals(datatype)) {
					datasourceParamsWithObjectValues.put(paramName, RigidUtils.parseBoolean(sValue));
				} else {
					if (sValue != null) {
						if (Integer.class.getCanonicalName().equals(datatype)) {
							datasourceParamsWithObjectValues.put(paramName, Integer.parseInt(sValue));
						} else if (Double.class.getCanonicalName().equals(datatype)) {
							datasourceParamsWithObjectValues.put(paramName, Double.parseDouble(sValue));
						} else if (Long.class.getCanonicalName().equals(datatype)) {
							datasourceParamsWithObjectValues.put(paramName, Long.parseLong(sValue));
						} else if (BigDecimal.class.getCanonicalName().equals(datatype)) {
							datasourceParamsWithObjectValues.put(paramName, BigDecimal.valueOf(Double.parseDouble(sValue)));
						} else if (Date.class.getCanonicalName().equals(datatype)) {
							// ISO-format 'yyyy-mm-dd', matches JDBC escape syntax
							try {
								Date date = new SimpleDateFormat("yyyy-MM-dd").parse(sValue);
								datasourceParamsWithObjectValues.put(paramName, date);
							} catch (ParseException e) {
								throw new NuclosFatalException("Date value " + sValue + " is not parseable with yyyy-MM-dd: " + e.getMessage());
							}
						} else if (InternalTimestamp.class.getCanonicalName().equals(datatype)) {
							try {
								// TODO: Use ISO-8601 format
								Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(sValue);
								datasourceParamsWithObjectValues.put(paramName, date);
							} catch (ParseException e) {
								throw new NuclosFatalException("InternalTimestamp value " + sValue + " is not parseable with yyyy-MM-dd HH:mm:ss: " + e.getMessage());
							}
						} else {
							datasourceParamsWithObjectValues.put(paramName, CollectableFieldFormat.getInstance(
									Class.forName(datatype))
									.parse(null, sValue));
						}
					} else { // sValue == null
						datasourceParamsWithObjectValues.put(paramName, null);
					}
				}
			} else { // missing parameter
				datasourceParamsWithObjectValues.put(paramName, null);
			}

			// fill in mandatory values...
			if (datasourceParamsWithObjectValues.get(paramName) == null) {
				if (Integer.class.getCanonicalName().equals(datatype)) {
					// NUCLOS-5010 (VLPs erzeugen unterschiedliche SQLs für Webclient, RichclientComboBox und RichClientLOV)
					// Abwärtskompatibilität: Integer Parameter müssen wenn leer mit 0 befüllt sein. Das stellen wir hier sicher...
					datasourceParamsWithObjectValues.put(paramName, 0);
				} else if (Boolean.class.getCanonicalName().equals(datatype)) {
					if ("searchmode".equals(paramName)) {
						datasourceParamsWithObjectValues.put(paramName, Boolean.FALSE);
					}
				}
			}
		}

		return datasourceParamsWithObjectValues;
	}

	@Override
	public JsonArray executeDatasource(UID datasourceUID, Map<String, String> datasourceParams, Integer iMaxRowCount) throws CommonBusinessException, ClassNotFoundException {
		final DatasourceVO datasource = dsFacade.getDatasource(datasourceUID);
		Map<String, Object> datasourceParamsWithObjectValues = getDatasourceParameters(datasource.getSource(), datasourceParams);

		final ResultVO resultVO = dsFacade.executeQuery(datasourceUID, datasourceParamsWithObjectValues, iMaxRowCount, null, getCurrentMandatorUID());
		final JsonArrayBuilder resultJson = Json.createArrayBuilder();
		final List<ResultColumnVO> columns = resultVO.getColumns();
		for (Object[] row : resultVO.getRows()) {
			final JsonObjectBuilder jsonObject = Json.createObjectBuilder();
			for (int i = 0; i < row.length; i++) {
				if (columns.size() > i) {
					final ResultColumnVO resultColumnVO = columns.get(i);
					final String columnLabel = resultColumnVO.getColumnLabel();
					final String columnClassName = resultColumnVO.getColumnClassName();
					final Object value = row[i];
					if (value != null) {
						// support for native json formats
						if (Boolean.class.getCanonicalName().equals(columnClassName)) {
							jsonObject.add(columnLabel, RigidUtils.parseBoolean(value));
						} else if (value instanceof Number) {
							Number nValue = ((Number) value);
							if (Integer.class.getCanonicalName().equals(columnClassName)) {
								jsonObject.add(columnLabel, nValue.intValue());
							} else if (Double.class.getCanonicalName().equals(columnClassName)) {
								jsonObject.add(columnLabel, nValue.doubleValue());
							} else if (Long.class.getCanonicalName().equals(columnClassName)) {
								jsonObject.add(columnLabel, nValue.longValue());
							} else if (BigDecimal.class.getCanonicalName().equals(columnClassName) &&
									value instanceof BigDecimal) {
								jsonObject.add(columnLabel, (BigDecimal)nValue);
							} else {
								jsonObject.add(columnLabel, resultColumnVO.format(value));
							}
						} else {
							if (Date.class.getCanonicalName().equals(columnClassName)) {
								// RESTful Service, we want a useful ISO-format 'yyyy-mm-dd' instead of 'Java' DateFormat.MEDIUM
								String sValue = new SimpleDateFormat("yyyy-MM-dd").format(value);
								jsonObject.add(columnLabel, sValue);
							} else if (InternalTimestamp.class.getCanonicalName().equals(columnClassName)) {
								// TODO: Use ISO-8601 format
								String sValue = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(value);
								jsonObject.add(columnLabel, sValue);
							} else {
								jsonObject.add(columnLabel, resultColumnVO.format(value));
							}
						}
					}
				}
			}
			resultJson.add(jsonObject);
		}
		return resultJson.build();
	}

	@Override
	public String executeManagementConsole(String sCommand, String sArguments) throws Exception {
		sArguments = sArguments==null? null: URLDecoder.decode(sArguments, "UTF-8");

		if (sCommand != null && !sCommand.startsWith("-")) {
			sCommand = "-"+sCommand;
		}

	    final PrintStream stdout = System.out;
	    final PrintStream stderr = System.err;

	    final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			final PrintStream ps = new PrintStream(baos);
			System.setOut(ps);
			System.setErr(ps);
		}
		catch (Exception e) {
			LOG.error("System.setOut/Err failed", e);
		}

		try {
			new CommonConsole().parseAndInvoke(sCommand, sArguments);
			System.out.flush();
		}
		catch (Exception ex) {
			LOG.error("parseAndInvoke failed", ex);
		}
		finally {
			// reset to standard output
			try {
				System.setOut(stdout);
				System.setErr(stderr);
			} catch (SecurityException e) {
				// ignore here
			}
		}

		return baos.toString();
	}

	@Override
	public Collection<TasklistDefinition> getDynamicTaskLists() {
		return tasklistFacade.getUsersTasklists();
	}

	@Override
	public List<NucletInfo> getNucletInfos() {
    	List<NucletInfo> result = new ArrayList<>();
    	for (EntityObjectVO<UID> nucletEo : metaProvider.getNuclets()) {
    		result.add(new NucletInfo(nucletEo));
		}
		Collections.sort(result, new Comparator<NucletInfo>() {
			@Override
			public int compare(final NucletInfo info1, final NucletInfo info2) {
				int result = RigidUtils.compareComparables(info1.getPackage(), info2.getPackage());
				if (result == 0) {
					RigidUtils.compareComparables(info1.getName(), info2.getName());
				}
				return result;
			}
		});
		return result;
	}

	@Override
	public Collection<DataLanguageVO> getDataLanguages() {
		return dataLanguageCache.getDataLanguages();
	}

	@Override
	public UID getPrimaryDataLanguage() {
		return (UID) dataLanguageCache.getNuclosPrimaryDataLanguage();
	}

	@Override
	public UID getCurrentDataLanguage() {
		return userCtx.getDataLocal();
	}

	public UID getCurrentMandatorUID() {
		return userCtx.getMandatorUID();
	}

	public Locale getCurrentLocale() {
		return LocaleContextHolder.getLocale();
	}

	public String getCurrentUserName() {
		return SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
	}

	public UID getCurrentUserId() {
		return securityCache.getUserUid(getCurrentUserName());
	}

	public ResultListExportRVO.OutputFile exportBoList(
			String boMetaId,
			String format,
			boolean pageOrientationLandscape,
			boolean isColumnScaled, Collection<FieldMeta<?>> fields,
			CollectableSearchExpression clctexpr,
			FilterJ filter
	) throws NuclosReportException, IOException {
		remoteContext.setRemotly(true);

		final UID entityUid = Rest.translateFqn(E.ENTITY, boMetaId);
		final List<CollectableEntityField> lstclctefweSelected = new ArrayList<>();
		List<Integer> selectedFieldWidth = new ArrayList<>();
		ReportOutputVO.Format outputFormat = ReportOutputVO.Format.CSV;
		if ("xls".equals(format)) {
			outputFormat = ReportOutputVO.Format.XLS;
		} else if ("xlsx".equals(format)) {
			outputFormat = ReportOutputVO.Format.XLSX;
		} else if ("pdf".equals(format)) {
			outputFormat = ReportOutputVO.Format.PDF;
		}
		String customUsage = null;
		ReportOutputVO.PageOrientation orientation = pageOrientationLandscape ? ReportOutputVO.PageOrientation.LANDSCAPE : ReportOutputVO.PageOrientation.PORTRAIT;


		if (filter.getAttributes() != null) {
			for (String attribute : filter.getAttributes().split(",")) {
				for (FieldMeta<?> fm : fields) {
					if (
							!fm.isHidden() &&
									!fm.getDbColumn().equalsIgnoreCase(SF.PK_ID.getDbColumn()) &&
									Rest.translateFqn(E.ENTITYFIELD, attribute).equals(fm.getUID())
							) {
						lstclctefweSelected.add(SearchConditionUtils.newEntityField(fm));
					}
				}

				selectedFieldWidth.add(200);
			}
		} else {
			for (FieldMeta<?> fm : fields) {
				if (fm.isHidden()
						|| fm.getUID().equals(SF.CREATEDBY.getUID(fm.getEntity()))
						|| fm.getUID().equals(SF.CREATEDAT.getUID(fm.getEntity()))
						|| fm.getUID().equals(SF.CHANGEDBY.getUID(fm.getEntity()))
						|| fm.getUID().equals(SF.CHANGEDAT.getUID(fm.getEntity()))
						|| fm.getUID().equals(SF.VERSION.getUID(fm.getEntity()))
						|| fm.getDbColumn().equalsIgnoreCase(SF.PK_ID.getDbColumn())
						) {
					continue;
				}
				lstclctefweSelected.add(SearchConditionUtils.newEntityField(fm));
				selectedFieldWidth.add(200);
			}
		}

		NuclosFile file = reportFacade.prepareSearchResult(
				clctexpr,
				lstclctefweSelected,
				selectedFieldWidth,
				entityUid,
				outputFormat,
				customUsage,
				orientation,
				isColumnScaled
		);

		RestPrintoutFileDownloadCache downloadCache = SpringApplicationContextHolder.getBean(RestPrintoutFileDownloadCache.class);

		String boId = null;
		String fileId = downloadCache.cacheFile(boMetaId, boId, format, file);
		RestPrintoutFileDownloadCache.CacheResult cached = downloadCache.get(boMetaId, boId, format, fileId);

		return new ResultListExportRVO.OutputFile(boMetaId, cached.fileName, format, fileId);
	}
}
