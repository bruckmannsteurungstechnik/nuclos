package org.nuclos.server.rest.misc;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonValue;

import org.apache.commons.codec.binary.Base64;
import org.nuclos.common.NuclosFile;
import org.nuclos.common.NuclosImage;
import org.nuclos.common.NuclosPassword;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.server.common.valueobject.DocumentFileBase;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.services.BoDocumentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RDataType {

	private static final Logger LOG = LoggerFactory.getLogger(RDataType.class);
	
	private final String type;
	private final Integer precision;
	private final boolean isPassword;

	public RDataType(String dataType, Integer precision) {
		this.type = Rest.simpleDataType(dataType);
		this.precision = precision;
		this.isPassword = NuclosPassword.class.getCanonicalName().equals(dataType);
	}
	
	public boolean isReadOnly() {
		if (isString()) return false;
		if (isDate()) return false;
		if (isImage()) return false;
		if (isNumber()) return false;
		if (isBoolean()) return false;
		if (isDocument()) return false;
		if (isImage()) return false;
		return true;
	}
	
	public String getExportType() {
		if (isDecimal()) return "Decimal";
		if (isNumber()) return "Number";
		return type;
	}
	
	public boolean isNumber() {
		if (isIntNumber()) return true;
		if (isDecimal()) return true;
		return false;
	}
	
	private boolean isDecimal() {
		if ("Double".equals(type)) return true;
		if ("Decimal".equals(type)) return true;
		if ("BigDecimal".equals(type)) return true;
		if ("Float".equals(type)) return true;
		return false;
	}
	
	private boolean isIntNumber() {
		if ("Long".equals(type)) return true;
		if ("Integer".equals(type)) return true;
		if ("Short".equals(type)) return true;
		if ("Byte".equals(type)) return true;
		if ("BigInteger".equals(type)) return true;
		return false;
	}
	
	private boolean isString() {
		if ("String".equals(type)) return true;
		return false;
	}
	
	public final boolean isBoolean() {
		if ("Boolean".equals(type)) return true;
		return false;
	}
	
	public final boolean isImage() {
		if ("Image".equals(type)) return true;
		if (NuclosImage.class.getName().equals(type)) return true;
		return false;
	}

	public final boolean isDocument() {
		if ("Document".equals(type)) return true;
		if (GenericObjectDocumentFile.class.getName().equals(type)) return true;
		return false;
	}

	private boolean isDate() {
		if ("Date".equals(type)) return true;
		return false;
	}

	private boolean isInternalTimestamp() {
		if ("InternalTimestamp".equals(type)) return true;
		return false;
	}

	private boolean isNonString() {
		if (isNumber()) return true;
		if (isBoolean()) return true;
		return false;
	}
	
	private Integer getPrecision() {
		return precision;
	}

	public final Object getValue(JsonObject obj, String key) throws SkipValueException {
		if (key == null) {
			throw new IllegalArgumentException("key must not be null");
		}
		
		if (obj.get(key) == null || obj.isNull(key)) {
			return null;
		}
		
		if (isNonString()) try {
			
			if (isIntNumber()) try {
				return obj.getJsonNumber(key).longValue();
			} catch (ClassCastException cce) {
				return Long.parseLong(obj.getString(key));
			}
			
			if (isDecimal()) try {
				return obj.getJsonNumber(key).bigDecimalValue().setScale(getPrecision(), RoundingMode.HALF_UP);
			} catch (ClassCastException cce) {
				return new BigDecimal(obj.getString(key)).setScale(getPrecision(), RoundingMode.HALF_UP);
			}
			
			if (isBoolean()) try {
				return obj.getBoolean(key);
			} catch (ClassCastException cce) {
				return Boolean.parseBoolean(obj.getString(key));
			}
			
		} catch (Exception iae) {			
			LOG.warn("Unable to get value:", iae);
		}
		
		if (isImage()) {
			// HINT: Image-file has no name
			JsonValue jsonValue = obj.get(key);
			if (jsonValue instanceof JsonObject) {
				JsonObject image = obj.getJsonObject(key);
				if (image.containsKey("uploadToken")) {
					String uploadToken = image.getString("uploadToken");
					NuclosFile nuclosFile = BoDocumentService.getUploadedFile(uploadToken, true);
					return new NuclosImage(nuclosFile.getName(), nuclosFile.getContent());
				}
				if (image.containsKey("data")) {
					byte[] bytes = Base64.decodeBase64(image.getString("data"));
					return new NuclosImage(image.getString("name"), bytes);				
				}
				if (image.isEmpty()) {
					// remove image
					return null;
				}
			}
			throw new SkipValueException("Skipping Image Field=" + key + " with Value=" + jsonValue);
			
		} else if (isDocument()) {
			JsonValue jsonValue = obj.get(key);
			if (jsonValue instanceof JsonObject) {
				JsonObject document = (JsonObject) jsonValue;
				if (document.containsKey("uploadToken")) {
					String uploadToken = document.getString("uploadToken");
					NuclosFile nuclosFile = BoDocumentService.getUploadedFile(uploadToken, true);
					return new GenericObjectDocumentFile(nuclosFile.getName(), DocumentFileBase.newFileUID(), nuclosFile.getContent()); 
				}
				if (document.containsKey("data")) {
					String fileName = document.getString("name");
					byte[] bytes = Base64.decodeBase64(document.getString("data"));
					return new GenericObjectDocumentFile(fileName, DocumentFileBase.newFileUID(), bytes);
				}
				if (document.isEmpty()) {
					// remove doc
					return null;
				}
			}
			throw new SkipValueException("Skipping Document Field=" + key + " with Value=" + jsonValue);
		}
		
		String s;
		try {
			s = obj.getString(key);
			
		} catch (ClassCastException cce) {
			Object value = obj.get(key);
			
			if (value instanceof JsonObject && ((JsonObject)value).containsKey("name")) {
				JsonValue jsonValue = ((JsonObject)value).get("name");
				if (jsonValue instanceof JsonString) {
					s = ((JsonString)jsonValue).getString();
				} else {
					s = null;
				}
				
			} else {
				s = value.toString();			
			}
		}
		
		if (isPassword) {
			return new NuclosPassword(s);
		}
		
		if (s != null && s.isEmpty()) {
			s = null;
		}
		
		if (s != null && isDate()) try {
			Date date;
			try {
				date = new SimpleDateFormat("yyyy-MM-dd").parse(s);
			} catch (ParseException pe1) {
				try {
					date = new SimpleDateFormat("MM/dd/yyyy").parse(s);
				} catch (ParseException pe2) {
					date = new SimpleDateFormat("dd.MM.yyyy").parse(s);				
				}
			}
			
			return date;
		} catch (ParseException pe) {
			LOG.warn("Unable to get value:", pe);
		}
		
		if (isInternalTimestamp()) try {
			// TODO: Use ISO-8601 format
			Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(s);
			return InternalTimestamp.toInternalTimestamp(date);
		} catch (ParseException pe) {
			LOG.warn("Unable to get value:", pe);
		}
		
		return s;
	}
	
}
