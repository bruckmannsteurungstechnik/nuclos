package org.nuclos.server.rest.services;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.NuclosFile;
import org.nuclos.common.UID;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.misc.SessionEntityContext;
import org.nuclos.server.rest.services.helper.DataServiceHelper;
import org.nuclos.server.rest.services.helper.RestServiceInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationManager;

@Path("/boDocuments")
@Produces(MediaType.APPLICATION_JSON)
public class BoDocumentService extends DataServiceHelper {
	
	private static File uploadPath;
	
	@POST
	@Path("/upload") 
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@RestServiceInfo(identifier="boDocumentUpload", isFinalized=true, description="Upload a file, returns a token for use in attribute value and BO update later.")
	@Produces(MediaType.TEXT_PLAIN)
	public String boDocumentUpload(@FormDataParam("file") InputStream is, @FormDataParam("file") FormDataContentDisposition formData) {
		try {
			byte[] fileContents = IOUtils.toByteArray(is);
			String fileName = formData.getFileName();
		
			UID token = new UID();
			String uploadBaseFileName = getUploadPath().getCanonicalPath() + File.separatorChar + token;
		
			Writer out = null;
			try {
				File file = new File(uploadBaseFileName + ".meta");
				file.deleteOnExit();
				out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
			    out.write(fileName);
			    out.write("\n");
			    out.write("Uploader: " + getUser());
			} finally {
				if (out != null) {
					out.close();
				}
			}
			
			FileOutputStream fop = null;
			try {
				File file = new File(uploadBaseFileName + ".data");
				file.deleteOnExit();
				fop = new FileOutputStream(file);
				fop.write(fileContents);
				fop.flush();
			} finally {
				if (fop != null) {
					fop.close();
				}
			}
			
			return token.getString();
			
		} catch (IOException ex) {
			throw new NuclosWebException(Status.INTERNAL_SERVER_ERROR, ex.getMessage());
		}
		
	}
	
	public static NuclosFile getUploadedFile(String token, boolean deleteFile) {
		try {
			BoDocumentTransactionSynchronization transactionSync = new BoDocumentTransactionSynchronization();
			String uploadBaseFileName = getUploadPath().getCanonicalPath() + File.separatorChar + token;
			String fileName = null;
			byte[] fileContents = null;

			BufferedReader reader = null;
			try {
				File file = new File(uploadBaseFileName + ".meta");
				reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), Charset.forName("UTF-8")));
				fileName = reader.readLine();
				if (deleteFile) {
					transactionSync.filesToDelete.add(file); //... instead of file.delete();
				}
			} finally {
				if (reader != null) {
					reader.close();
				}
			}

			FileInputStream fis = null;
			try {
				File file = new File(uploadBaseFileName + ".data");
				fis = new FileInputStream(file);
				fileContents = new byte[(int) file.length()];
				fis.read(fileContents);
				if (deleteFile) {
					transactionSync.filesToDelete.add(file); //... instead of file.delete();
				}
			} finally {
				if (fis != null) {
					fis.close();
				}
			}

			if (deleteFile) {
				TransactionSynchronizationManager.registerSynchronization(transactionSync);
			}
			return new NuclosFile(fileName, fileContents);

		} catch (IOException ex) {
			throw new IllegalArgumentException("File value IOException: " + ex.getMessage());
		}
	}
	
	public static File getUploadPath() {
		if (uploadPath != null) {
			return uploadPath;
		}

		File uploadPathFromParameters = NuclosSystemParameters.getDirectory(NuclosSystemParameters.DOCUMENT_UPLOAD_PATH);
		if (uploadPathFromParameters != null) {
			uploadPath = uploadPathFromParameters;
			return uploadPath;
		}

	 	// TODO: Refactor - the REST service should not be concerned about directory creation.
		File defaultUploadPath = createDefaultUploadPath();
		if (defaultUploadPath != null) {
			uploadPath = defaultUploadPath;
			return uploadPath;
		}

		throw new NuclosWebException(
				Status.INTERNAL_SERVER_ERROR,
				NuclosSystemParameters.DOCUMENT_UPLOAD_PATH + " is not set in server.properties and default directory could not be created"
		);
	}

	/**
	 * TODO: Define default path in central place.
	 * TODO: Move to appropriate place in server.
	 */
	private static File createDefaultUploadPath() {
		try {
			File documentPath = NuclosSystemParameters.getDirectory(NuclosSystemParameters.DOCUMENT_PATH);
			if (documentPath != null) {
				File documentParent = documentPath.getParentFile();
				File defaultUploadPath = new File(documentParent, "documents-upload");
				defaultUploadPath.mkdir();
				return defaultUploadPath;
			}
		} catch (Exception e) {
		}

		return null;
	}

	@GET
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Path("/{boMetaId}/{boId}/documents/{docAttrId}")
	@RestServiceInfo(identifier="boDocumentValue", isFinalized=true, description="File of BO attribute")
	public Response boDocumentValue(@PathParam("boMetaId") String boMetaId, @PathParam("boId") String boId, @PathParam("docAttrId") String docAttrId) {
		
		NuclosFile nf = byteAttributeValueWithValidation(boMetaId, boId, docAttrId);
		return fileResponse(nf);
	}

	@GET
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Path("/{boMetaId}/{boId}/subBos/{refAttrId}/{subBoId}/documents/{docAttrId}")
	@RestServiceInfo(identifier="dependence_DocumentValue", isFinalized=true, description="Image file of SubBO attribute")
	public Response boDocumentValue(
			@PathParam("boMetaId") String boMetaId,
			@PathParam("boId") String boId,
			@PathParam("refAttrId") String refAttrId,
			@PathParam("subBoId") String subBoId,
			@PathParam("docAttrId") String docAttrId) {

		checkReadAllowed(boMetaId, boId);
		String subBoMetaId = getSubBoMetaId(refAttrId);
		UID subBoUid = Rest.translateFqn(E.ENTITY, subBoMetaId);
		EntityMeta<?> eMeta = Rest.getEntity(subBoUid);

		NuclosFile nf = byteAttributeValue(eMeta, subBoId, docAttrId, false);
		return fileResponse(nf);
	}

	private Response fileResponse(final NuclosFile nf) {
		if (nf != null) {
			ResponseBuilder responseBuilder = Response.ok(nf.getContent()).type(URLConnection.guessContentTypeFromName(nf.getName()));
			responseBuilder.header("Content-Disposition", "attachment; filename=\"" + nf.getName() + "\"");
			return responseBuilder.build();
		} else {
			return Response.noContent().build();
		}
	}

	private SessionEntityContext checkReadAllowed(String boMetaId, String boId) {
		SessionEntityContext info = checkPermission(E.ENTITY, boMetaId, boId);
		try {
			Rest.facade().get(
					info.getEntity(),
					info.getMasterMeta().isUidEntity() ? UID.parseUID(boId) : new Long(boId),
					true
			);
		} catch (Exception ex) {
			throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITY, boMetaId));
		}
		return info;
	}
	
	private static class BoDocumentTransactionSynchronization implements TransactionSynchronization {
		
		private static final Logger LOG = LoggerFactory.getLogger(
			BoDocumentTransactionSynchronization.class);
		
		private final List<File> filesToDelete = new ArrayList<>();

		@Override
		public void suspend() {
		}

		@Override
		public void resume() {
		}

		@Override
		public void flush() {
		}

		@Override
		public void beforeCommit(boolean readOnly) {
		}

		@Override
		public void beforeCompletion() {
		}

		@Override
		public void afterCommit() {
			for (File file : filesToDelete) {
				try {
					file.delete();
				} catch (Exception ex) {
					LOG.error(ex.getMessage(), ex);
				}
			}
		}

		@Override
		public void afterCompletion(int status) {
		}
		
	}
	
}
