package org.nuclos.server.rest.services;

import java.util.List;

import javax.json.JsonValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.nuclos.schema.rest.EntityMetaBase;
import org.nuclos.server.rest.services.helper.MetaDataServiceHelper;
import org.nuclos.server.rest.services.helper.RestServiceInfo;
import org.nuclos.server.rest.services.rvo.LayoutInfo;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Path("/boMetas")
@Produces(MediaType.APPLICATION_JSON)
public class BoMetaService<T> extends MetaDataServiceHelper {
	
	@GET
	@Path("/{boMetaId}")
	@RestServiceInfo(identifier="boMeta", isFinalized=true, description="Get the meta information about one Businessobject-meta")
	@Operation(
			responses = @ApiResponse(
					content = @Content(
							schema = @Schema(
									implementation = EntityMetaBase.class
							)
					)
			)
	)
	public JsonValue bometaSelf(@PathParam("boMetaId") String boMetaId) {
		return getBOMetaSelf(boMetaId).build();
	}
	
	@GET
	@Path("/{boMetaId}/subBos/{refAttrId}")
	@RestServiceInfo(identifier="referencemeta_self", isFinalized=true, description="Get the meta information about one Reference")
	@Operation(
			responses = @ApiResponse(
					content = @Content(
							schema = @Schema(
									implementation = EntityMetaBase.class
							)
					)
			)
	)
	public JsonValue dependencemetaSelf(@PathParam("boMetaId") String boMetaId, @PathParam("refAttrId") String refAttrId) {
		return getDependenceMetaSelf(boMetaId, refAttrId).build();
	}

	@GET
	@Path("/{boMetaId}/layouts")
	@RestServiceInfo(identifier="entity layouts", description="Available layouts for for the given entity")
	public List<LayoutInfo> layoutInfosForEntity(@PathParam("boMetaId") String boMetaId) {
		return getLayoutInfosForEntity(boMetaId);
	}
}
