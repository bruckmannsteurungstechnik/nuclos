package org.nuclos.server.rest.services;

import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.nuclos.api.common.NuclosFile;
import org.nuclos.common.E;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.printservice.PrintExecutionContext;
import org.nuclos.common.report.ejb3.ReportFacadeRemote;
import org.nuclos.common.report.valueobject.DefaultReportOutputVO;
import org.nuclos.common.report.valueobject.DefaultReportVO;
import org.nuclos.common.report.valueobject.OutputFormatTO;
import org.nuclos.common.report.valueobject.PrintResultTO;
import org.nuclos.common.report.valueobject.PrintoutTO;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.nuclos.common.report.valueobject.ReportVO;
import org.nuclos.schema.rest.PrintoutList;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.printservice.PrintServiceFacadeLocal;
import org.nuclos.server.printservice.printout.PrintoutServiceProvider;
import org.nuclos.server.printservice.printout.PrintoutWrapper;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.misc.RestPrintoutFileDownloadCache;
import org.nuclos.server.rest.misc.RestPrintoutFileDownloadCache.CacheResult;
import org.nuclos.server.rest.misc.SessionEntityContext;
import org.nuclos.server.rest.services.helper.DataServiceHelper;
import org.nuclos.server.rest.services.helper.RestServiceInfo;
import org.nuclos.server.rest.services.rvo.OutputFormatRVO;
import org.nuclos.server.rest.services.rvo.PrintoutRVO;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Path("/boPrintouts")
@Produces(MediaType.APPLICATION_JSON)
public class BoPrintoutService extends DataServiceHelper {
	
	@GET
	@Path("/{boMetaId}/{boId}")
	@RestServiceInfo(identifier="boPrintoutList", isFinalized=true, description="List of all executeable Printouts. Send the Printouts back to execute")
	@Operation(
			responses = @ApiResponse(
					content = @Content(
							array = @ArraySchema(
									schema = @Schema(
											implementation = PrintoutList.class
									)
							)
					)
			)
	)
 	public JsonArray boPrintoutList(@PathParam("boMetaId") String boMetaId, @PathParam("boId") String boId) {
		SessionEntityContext info =  checkPermission(E.ENTITY, boMetaId, boId);
		
		try {
			UsageCriteria usage = Rest.facade().getUsageCriteriaForPK(Long.parseLong(boId), info.getEntity());
			
			ReportFacadeRemote reportFacadeRemote = SpringApplicationContextHolder.getBean(ReportFacadeRemote.class);
			Collection<DefaultReportVO> reportsByUsage = reportFacadeRemote.findReportsByUsage(usage);
			
			JsonArrayBuilder jsonarray = Json.createArrayBuilder();
			
			for (DefaultReportVO reportvo : reportsByUsage) {
				PrintoutRVO printout = new PrintoutRVO(boId, reportvo);
				if (reportvo.getOutputType() == ReportVO.OutputType.SINGLE) {
					for (ReportOutputVO reportoutputvo : reportFacadeRemote.getReportOutputs(reportvo.getId())) {
						String translatedUid = Rest.translateUid(E.REPORTOUTPUT, (UID)reportoutputvo.getId());
						
						if (translatedUid != null) {
							// not all output-formats are runnable in server.
							// see also org.nuclos.server.report.PrintoutObjectBuilder.visitOutputFormats(UID, OutputFormatVisitor)
							OutputFormatRVO outputformat = new OutputFormatRVO(reportoutputvo);
							printout.addOutputFormat(outputformat);
							
						}
					}
				}
				jsonarray.add(printout.getJSONObjectBuilder());				
			}
			
			return jsonarray.build();
			
		} catch (Exception ex) {
			throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITY, boMetaId));
		}
	}
	
	@POST
	@Path("/{boMetaId}/{boId}")
	@RestServiceInfo(identifier="boPrintoutExecution", isFinalized=true, description="Execute the posted Printouts, returns the generated file list")
	@Consumes({MediaType.APPLICATION_JSON})
	@Operation(
			responses = @ApiResponse(
					content = @Content(
							array = @ArraySchema(
									schema = @Schema(
											implementation = PrintoutList.class
									)
							)
					)
			)
	)
	public JsonArray boPrintoutExecution(@PathParam("boMetaId") String boMetaId, @PathParam("boId") String boId, JsonArray printoutListData) {
		SessionEntityContext info = checkPermission(E.ENTITY, boMetaId, boId);
		
		try {
			Long primaryKey = Long.parseLong(boId);
			UsageCriteria usage = Rest.facade().getUsageCriteriaForPK(primaryKey, info.getEntity());

			ReportFacadeRemote reportFacadeRemote = SpringApplicationContextHolder.getBean(ReportFacadeRemote.class);
			PrintoutServiceProvider printoutProvider = SpringApplicationContextHolder.getBean(PrintoutServiceProvider.class);
			RestPrintoutFileDownloadCache downloadCache = SpringApplicationContextHolder.getBean(RestPrintoutFileDownloadCache.class);
			PrintServiceFacadeLocal printFacade = SpringApplicationContextHolder.getBean(PrintServiceFacadeLocal.class);
			PrintoutWrapper printoutWrapper = SpringApplicationContextHolder.getBean(PrintoutWrapper.class);

			final List<PrintoutRVO> result = new ArrayList<PrintoutRVO>();
			final List<PrintoutTO> printouts = new ArrayList<PrintoutTO>(); 
			
			for (int i = 0; i < printoutListData.size(); i++) {
				final JsonObject printoutJson = printoutListData.getJsonObject(i);
				final String printoutFqn = printoutJson.getString("printoutId");
				final UID printoutUid = Rest.translateFqn(E.REPORT, printoutFqn);
				
				// query ReportVO
				final EntityObjectVO<UID> reportEO = NucletDalProvider.getInstance().getEntityObjectProcessor(E.REPORT).getByPrimaryKey(printoutUid);
				final UID mandator = Rest.facade().getCurrentMandatorUID();
				final DefaultReportVO reportVO = MasterDataWrapper.getReportVO(new MasterDataVO<>(reportEO, true),this.getUser(),mandator);
				final PrintoutRVO printoutResult = new PrintoutRVO(boId, reportVO);
				final PrintoutTO printout = new PrintoutTO(printoutUid, reportVO.getName(), reportVO.getOutputType(), reportVO.getDatasourceId());
				printout.getOutputFormats().clear();
				printout.setBusinessObjectId(primaryKey);
				
				boolean add = false;
				if (reportVO.getOutputType() == ReportVO.OutputType.SINGLE) {
					if (printoutJson.containsKey("outputFormats")) {
						JsonArray outputformatListJson = printoutJson.getJsonArray("outputFormats");
						for (int j = 0; j < outputformatListJson.size(); j++) {
							add = true;
							JsonObject outputformatJson = outputformatListJson.getJsonObject(j);
							OutputFormatRVO outputFormat = OutputFormatRVO.getOutputFormat(outputformatJson, reportFacadeRemote);
							DefaultReportOutputVO reportOutputFormat = reportFacadeRemote.getReportOutput(outputFormat.getUID());
							OutputFormatTO wrapTO = printoutWrapper.wrapTO(reportOutputFormat);
							// TODO printout webclient
							// wrapTO.setProperties(...);
							wrapTO.getDatasourceParams().putAll(outputFormat.getParams());
							if (reportVO.getAttachDocument()) {
								// Collective report output type
								wrapTO.setAttachDocument(true);
							}
							printoutResult.addOutputFormat(outputFormat);
							printout.getOutputFormats().add(wrapTO);
						}
					}
				} else {
					for (ReportOutputVO reportoutputvo : reportFacadeRemote.getReportOutputs(printoutUid)) {
						add = true;
						printoutResult.addOutputFormat(new OutputFormatRVO(reportoutputvo));
						printout.getOutputFormats().add(printoutWrapper.wrapTO(reportoutputvo));
					}
				}
				
				if (add) {
					result.add(printoutResult);
					printouts.add(printout);
				}
			}
			
			final UID attachmentSubEntityUID = null; // Used from client extensions only
			final UID[] attachmentSubEntityAttributeUIDs = null; // Used from client extensions only
			final PrintExecutionContext context = new PrintExecutionContext(usage, primaryKey, attachmentSubEntityUID, attachmentSubEntityAttributeUIDs);
			// executes print final rules...
			final List<PrintResultTO> executedPrintouts = printFacade.executePrintout(printouts, context);
			
			final JsonArrayBuilder jsonarray = Json.createArrayBuilder();
			
			for (PrintoutRVO printoutResult : result) {
				for (OutputFormatRVO outputFormatResult : printoutResult.getOutputFormats()) {
					for (PrintResultTO printed : executedPrintouts) {
						String outputFormatFqn = Rest.translateUid(E.REPORTOUTPUT, printed.getOutputFormat().getId());
						if (RigidUtils.equal(outputFormatFqn, outputFormatResult.getId())) {
							NuclosFile file = printed.getOutput();
							String fileId = downloadCache.cacheFile(boMetaId, boId, outputFormatResult.getId(), file);
							
							OutputFormatRVO.OutputFile outputFileResult = new OutputFormatRVO.OutputFile(boMetaId, boId, file.getName(), fileId, BoPrintoutService.this);
							outputFormatResult.setFile(outputFileResult);
							break;
						}						
					}
				}				
				jsonarray.add(printoutResult.getJSONObjectBuilder());
			}
			
			return jsonarray.build();
			
		} catch (Exception ex) {
			throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITY, boMetaId));
		}
	}
	
	@GET
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Path("/{boMetaId}/{boId}/{outputFormatId}/files/{fileId}")
	@RestServiceInfo(identifier="boPrintoutFile", isFinalized=true, description="Get the Printout file")
	public Response boPrintoutFile(@PathParam("boMetaId") String boMetaId, @PathParam("boId") String boId, @PathParam("outputFormatId") String outputFormatId, @PathParam("fileId") String fileId) {
		checkPermission(E.ENTITY, boMetaId, boId);
		
		RestPrintoutFileDownloadCache downloadCache = SpringApplicationContextHolder.getBean(RestPrintoutFileDownloadCache.class);
		try {
			CacheResult cached = downloadCache.get(boMetaId, boId, outputFormatId, fileId);
			
			if (cached != null) {
				ResponseBuilder responseBuilder = Response.ok(cached.content).type(URLConnection.guessContentTypeFromName(cached.fileName));
				responseBuilder.header("Content-Disposition", "attachment; filename=\"" + cached.fileName + "\"");
				return responseBuilder.build();
				
			} 				
			
			return Response.noContent().build();

		} catch (Exception ex) {
			throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITY, boMetaId));
		}
	}
	
}
