package org.nuclos.server.rest.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.schema.rest.CollectiveProcessingAction;
import org.nuclos.schema.rest.CollectiveProcessingActionLinks;
import org.nuclos.schema.rest.CollectiveProcessingExecution;
import org.nuclos.schema.rest.CollectiveProcessingExecutionLinks;
import org.nuclos.schema.rest.CollectiveProcessingProgressInfo;
import org.nuclos.schema.rest.CollectiveProcessingSelectionOptions;
import org.nuclos.schema.rest.QueryContext;
import org.nuclos.schema.rest.RestLink;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.misc.SessionEntityContext;
import org.nuclos.server.rest.services.helper.CollectiveProcessingExecutionEntry;
import org.nuclos.server.rest.services.helper.CollectiveProcessingExecutionService;
import org.nuclos.server.rest.services.helper.DataServiceHelper;
import org.nuclos.server.rest.services.helper.RestServiceInfo;
import org.nuclos.server.rest.services.rvo.FilterJ;
import org.nuclos.server.statemodel.valueobject.StateTransitionVO;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;

@Path("/collectiveProcessing")
@Produces(MediaType.APPLICATION_JSON)
public class CollectiveProcessingService extends DataServiceHelper {

	private final ConcurrentMap<UID, CollectiveProcessingExecutionEntry> executions = new ConcurrentHashMap<>();

	@Autowired
	private CollectiveProcessingExecutionService executionService;

	@POST
	@Path("/{boMetaId}")
	@RestServiceInfo(identifier = "selectionOptions", description = "Selection options for a possible collection processing")
	@Consumes({MediaType.APPLICATION_JSON})
	public CollectiveProcessingSelectionOptions selectionOptions(
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					schema = @Schema(
							implementation = QueryContext.class
					)
			) JsonObject queryContext
	) {
		final SessionEntityContext info = checkPermission(E.ENTITY, boMetaId);
		final CollectiveProcessingSelectionOptions.Builder result = CollectiveProcessingSelectionOptions.builder();

		final FilterJ filter = getFilterJ(queryContext, -1L);
		checkFilter(filter);
		final List<FieldMeta<?>> fields = Rest.getFieldsFromQueryAttributesAndSystem(getBOMeta().getUID(), filter.getAttributes());

		try {
			final CollectableSearchExpression clctexpr = filter.getSearchExpression(info.getEntity(), fields);
			final Set<UsageCriteria> usageCriterias = Rest.facade().getUsageCriteriaBySearchExpression(info.getEntity(), clctexpr);

			result.addStateChanges(getStateChangeActionsForCurrentUser(boMetaId, usageCriterias));
		} catch (CommonBusinessException e) {
			throw new NuclosWebException(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
		}
		return result.build();
	}

	private List<CollectiveProcessingAction> getStateChangeActionsForCurrentUser(String boMetaId, Set<UsageCriteria> usageCriterias) {
		final SortedMap<Integer, CollectiveProcessingAction> sortedMap = new TreeMap<>();

		final Set<UID> stateIds = usageCriterias.stream().filter(uc -> uc.getStatusUID() != null)
				.map(uc -> uc.getStatusUID()).collect(Collectors.toSet());
		// Change state support only for the same source state
		if (stateIds.size() == 1) {
			final Locale locale = getLocale();
			final Set<UID> allowed = new HashSet<>(Rest.facade().getAllowedStateTransitions());
			final Collection<StateTransitionVO> transitions = Rest.facade().getStateTransitions(
					new UsageCriteria(null, null, stateIds.iterator().next(), null))
					.stream().filter(vo -> allowed.contains(vo.getId())).collect(Collectors.toList());
			transitions.stream().forEach(transition -> {
				try {
					final StateVO state = Rest.facade().getState(transition.getStateTargetUID());
					String label = StringUtils.defaultIfNull(transition.getLabel(locale), state.getButtonLabel(locale));
					if (StringUtils.looksEmpty(label)) {
						label = state.getStatename(locale);
					}
					String description = StringUtils.defaultIfNull(transition.getDescription(locale), state.getDescription(locale));
					sortedMap.put(state.getNumeral(),
							CollectiveProcessingAction.builder()
							.withName(label)
							.withSubName(state.getNumeral().toString())
							.withColor(state.getColor())
							.withDescription(description)
							.withWithoutConfirmation(transition.isNonstop())
							.withLinks(CollectiveProcessingActionLinks.builder()
									.withExecute(
										RestLink.builder()
											.withMethods("POST")
											.withHref(getRestURI("collectiveProcessingStateChange",
													boMetaId,
													Rest.translateUid(E.STATETRANSITION, transition.getId())))
											.build())
									.build())
							.build());
				} catch (CommonFinderException e) {
					throw new NuclosFatalException(e);
				}
			});
		}

		return new ArrayList<>(sortedMap.values());
	}

	@POST
	@Path("/{boMetaId}/stateChange/{transitionId}")
	@RestServiceInfo(identifier="collectiveProcessingStateChange", description="Change status of selected BOs")
	@Consumes({MediaType.APPLICATION_JSON})
	public CollectiveProcessingExecution collectiveProcessingStateChange(
			@PathParam("boMetaId") String boMetaId,
			@PathParam("transitionId") String transitionId,
			@Parameter(
					schema = @Schema(
							implementation = QueryContext.class
					)
			) JsonObject queryContext
	) {
		final SessionEntityContext info = checkPermission(E.ENTITY, boMetaId);

		final UID transitionUID = Rest.facade().translateFqn(E.STATETRANSITION, transitionId);
		final FilterJ filter = getFilterJ(queryContext, -1L);
		final List<FieldMeta<?>> fields = Rest.getFieldsFromQueryAttributesAndSystem(getBOMeta().getUID(), filter.getAttributes());
		checkFilter(filter);

		final String executionId = executionService.newExecutionId();
		final CollectiveProcessingExecutionEntry cacheEntry = executionService.createCacheEntry(executionId, getUser());
		CollectiveProcessingExecution result = CollectiveProcessingExecution.builder()
				.withLinks(CollectiveProcessingExecutionLinks.builder()
						.withProgress(
								RestLink.builder()
										.withMethods("GET")
										.withHref(getRestURI("collectiveProcessingProgress", executionId))
										.build())
                        .withAbort(
                                RestLink.builder()
                                        .withMethods("GET")
                                        .withHref(getRestURI("collectiveProcessingAbort", executionId))
                                        .build())
                        .build())
				.build();

		try {
			final CollectableSearchExpression clctexpr = filter.getSearchExpression(info.getEntity(), fields);
			executionService.executeStateChanges(cacheEntry, clctexpr, info.getEntity(), transitionUID);
		} catch (CommonBusinessException e) {
			throw new NuclosWebException(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
		}

		return result;
	}

	@GET
	@Path("/progress/{executionId}")
	@RestServiceInfo(identifier="collectiveProcessingProgress", description="Progress information of the current collective processing")
	@Consumes({MediaType.APPLICATION_JSON})
	public CollectiveProcessingProgressInfo progress(@PathParam("executionId") String executionId) {
		try {
			CollectiveProcessingExecutionEntry cacheEntry = executionService.getCacheEntry(executionId, getUser());
			CollectiveProcessingExecutionEntry.ProgressInfoSinceLastRequest cachedResult = cacheEntry.getProgressInfoSinceLastRequest();
			CollectiveProcessingProgressInfo result = CollectiveProcessingProgressInfo.builder()
					.withPercent(cachedResult.getProgressInPercent())
					.addObjectInfos(cachedResult.getObjectInfos()).build();
			if (result.getPercent() >= 100l) {
				// clear the cache
				executionService.evictCache(executionId);
			}
			return result;
		} catch (CommonPermissionException e) {
			throw new NuclosWebException(Response.Status.FORBIDDEN, e.getMessage());
		} catch (CommonFinderException e) {
			throw new NuclosWebException(Response.Status.NOT_FOUND, e.getMessage());
		}
	}

    @GET
    @Path("/abort/{executionId}")
    @RestServiceInfo(identifier="collectiveProcessingAbort", description="Abort current collective processing")
    @Consumes({MediaType.APPLICATION_JSON})
    public Response abort(@PathParam("executionId") String executionId) {
        try {
            CollectiveProcessingExecutionEntry cacheEntry = executionService.getCacheEntry(executionId, getUser());
            cacheEntry.abort();

            return Response.status(Response.Status.NO_CONTENT).build();
        } catch (CommonPermissionException e) {
            throw new NuclosWebException(Response.Status.FORBIDDEN, e.getMessage());
        } catch (CommonFinderException e) {
            throw new NuclosWebException(Response.Status.NOT_FOUND, e.getMessage());
        }
    }

	private void checkFilter(FilterJ filter) {
		if (!filter.hasMultiSelectionCondition()) {
			throw new NuclosWebException(Response.Status.NOT_ACCEPTABLE, "QueryContext is missing the " + FilterJ.KEY_MULTI_SELECTION_CONDITION);
		}
	}

}
