package org.nuclos.server.rest.services;

import java.io.InputStream;

import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.nuclos.server.rest.services.helper.CustomRestServiceHelper;
import org.nuclos.server.security.UserFacadeLocal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * executes CustomRestRule methods
 * @see org.nuclos.api.rule.CustomRestRule
 * @deprecated use custom REST services with @Path annotations
 */
@Deprecated
@Path("/execute")
@Produces(MediaType.APPLICATION_JSON)
public class CustomRestService extends CustomRestServiceHelper {

	@Autowired
	UserFacadeLocal userFacade;

	/**
	 * @see org.nuclos.api.rule.CustomRestRule
	 *
	 * @param className
	 * @param methodName
	 * @return
	 * @throws IllegalAccessException
	 */
	@Transactional(propagation=Propagation.REQUIRES_NEW, rollbackFor= {Exception.class})
	@GET
	@Path("/{className}/{methodName}")
	@Consumes({MediaType.APPLICATION_JSON})
	public Response executeMethod(
			@PathParam("className") String className,
			@PathParam("methodName") String methodName
	) throws IllegalAccessException {
		return invokeRestRuleMethod(className, methodName, GET.class, null, String.class);
	}


	@Transactional(propagation=Propagation.REQUIRES_NEW, rollbackFor= {Exception.class})
	@POST
	@Path("/{className}/{methodName}")
	@Consumes({MediaType.APPLICATION_JSON})
	public Response executeMethodPostJson(
			final @PathParam("className") String className,
			final @PathParam("methodName") String methodName,
			final JsonObject data
	) throws IllegalAccessException {
		return invokeRestRuleMethod(className, methodName, POST.class, data, String.class);
	}

	@PUT
	@Path("/{className}/{methodName}")
	@Consumes({MediaType.APPLICATION_JSON})
	public Response executeMethodPut(
			final @PathParam("className") String className,
			final @PathParam("methodName") String methodName,
			final JsonObject data
	) throws IllegalAccessException {
		return invokeRestRuleMethod(className, methodName, PUT.class, data, String.class);
	}

	/**
	 * TODO: DELETE methods should not have a request body,
	 *  remove the "data" parameter!
	 */
	@DELETE
	@Path("/{className}/{methodName}")
	@Consumes({MediaType.APPLICATION_JSON})
	public Response executeMethodDelete(
			final @PathParam("className") String className,
			final @PathParam("methodName") String methodName,
			final JsonObject data
	) throws IllegalAccessException {
		return invokeRestRuleMethod(className, methodName, DELETE.class, data, String.class);
	}


	@Transactional(propagation=Propagation.REQUIRES_NEW, rollbackFor= {Exception.class})
	@GET
	@Path("/image/{className}/{methodName}")
	@Produces("image/png")
	@Consumes({MediaType.APPLICATION_JSON})
	public Object executeMethodImage(
			@PathParam("className") String className,
			@PathParam("methodName") String methodName
	) throws IllegalAccessException {
		return invokeRestRuleMethod(className, methodName, GET.class, null, Object.class);
	}


	@Transactional(propagation=Propagation.REQUIRES_NEW, rollbackFor= {Exception.class})
	@POST
	@Path("/upload/{className}/{methodName}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response executeMethodPostUploadJson(
			final @PathParam("className") String className,
			final @PathParam("methodName") String methodName,
			final @FormDataParam("file") InputStream is,
			final @FormDataParam("file") FormDataContentDisposition formData
	) throws IllegalAccessException {
		return invokeRestRuleMethod(className, methodName, POST.class, null, is, formData, String.class);
	}


	@Transactional(propagation=Propagation.REQUIRES_NEW, rollbackFor= {Exception.class})
	@GET
	@Path("/download/{className}/{methodName}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Consumes({MediaType.APPLICATION_JSON})
	public Response executeMethodDownload(
			@PathParam("className") String className,
			@PathParam("methodName") String methodName
	) throws IllegalAccessException {
		return invokeRestRuleMethod(className, methodName, GET.class, null, Object.class);
	}
}
