//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.services;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.services.helper.RestServiceInfo;
import org.nuclos.server.rest.services.helper.WebContext;
import org.nuclos.server.rest.services.rvo.NucletInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/nuclets")
@Produces(MediaType.APPLICATION_JSON)
public class NucletService extends WebContext {

	private static final Logger LOG = LoggerFactory.getLogger(NucletService.class);

	@GET
	@RestServiceInfo(identifier = "nuclets", description = "Returns all Nuclets.")
	public List<NucletInfo> nuclets() throws CommonPermissionException {
		if (!Rest.facade().isSuperUser()) {
			throw new CommonPermissionException("Access not allowed. Only for super user!");
		}
		return Rest.facade().getNucletInfos();
	}

}
