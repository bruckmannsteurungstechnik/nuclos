package org.nuclos.server.rest.services.helper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Response;

import org.nuclos.common.E;
import org.nuclos.common.Mutable;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.schema.rest.CollectiveProcessingExecution;
import org.nuclos.schema.rest.CollectiveProcessingObjectInfo;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.NuclosUserDetailsContextHolder;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class CollectiveProcessingExecutionService {

	private static final Logger LOG = LoggerFactory.getLogger(CollectiveProcessingExecutionService.class);

	@Autowired
	private CacheManager cacheManager;

	@Autowired
	private MetaProvider metaProv;

	@Autowired
	private SpringLocaleDelegate localeDelegate;

	@Autowired
	private NucletDalProvider dalProvider;

	@Autowired
	private NuclosUserDetailsContextHolder userDetails;

	@PostConstruct
	final void init() {
		Timer timer = new Timer(true);
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				evictForgottenCacheEntries();
			}
		}, 1000 * 60 * 60 * 24);
	}

	//private final ThreadPoolExecutor executor = Executors.newCachedThreadPool();
	private final ThreadPoolExecutor executor = new ThreadPoolExecutor(0, Integer.MAX_VALUE,
			60L, TimeUnit.SECONDS,
			new SynchronousQueue<Runnable>()) {
		@Override
		protected void beforeExecute(final Thread t, final Runnable r) {
			super.beforeExecute(t, ((ThreadLocalePropagationRunnable)r).initThreadLocales());
		}
	};

	@Transactional(propagation= Propagation.REQUIRES_NEW, rollbackFor= {Exception.class})
	public void executeStateChangeTransactional(UID entityUID, Object id, UID transitionUID) throws CommonBusinessException {
		UID stateUID = Rest.facade().getTargetStateId(transitionUID);
		Rest.facade().changeState(entityUID, (Long) id, stateUID);
	}

	public void executeStateChanges(
			final CollectiveProcessingExecutionEntry cacheEntry,
			final CollectableSearchExpression clctexpr,
			final UID entityUID,
			final UID transitionUID) {
		executor.execute(new ThreadLocalePropagationRunnable() {
			@Override
			public void run() {
				try {
					final Collection<Object> ids = Rest.facade().getEntityObjectIds(entityUID, clctexpr);
					cacheEntry.setTotalCount(ids.size());
					final Mutable<Long> infoRowNumber = new Mutable<>(0l);
                    for (Object id : ids) {
                        final String eoTitle = getEoTitle(entityUID, id);
                        final String entityFQN = Rest.translateUid(E.ENTITY, entityUID);
                        final Long rowNumber = infoRowNumber.setValue(infoRowNumber.getValue() + 1l);
                        final CollectiveProcessingObjectInfo oInfo = CollectiveProcessingExecutionEntry.newObjectInfo(entityFQN, id, eoTitle, rowNumber);
                        oInfo.setInProgress(true);
                        cacheEntry.storeObjectInfo(oInfo);

                        if (cacheEntry.isAborted()) {
                            oInfo.setResult("Not ok");
                            oInfo.setResultMessage("Aborted by User."); // internationalize ?
                            oInfo.setInProgress(false);
                            cacheEntry.storeObjectInfo(oInfo);
                            cacheEntry.setTotalCount(1);

                            break;
                        }

                        try {
                            executeStateChangeTransactional(entityUID, id, transitionUID);
                            oInfo.setResult("Ok");
                        } catch (CommonBusinessException e) {
                            LOG.error("CollectiveProcessingExecution exception: executionId=" + cacheEntry.getExecutionId() +
                                            ", row=" + rowNumber +
                                            ", exception=" + e.getMessage(),
                                    e);
                            oInfo.setResult("Not ok");
                            oInfo.setResultMessage(e.getMessage());
                        } finally {
                            oInfo.setInProgress(false);
                            cacheEntry.storeObjectInfo(oInfo);
                        }
                    }
                } catch (CommonBusinessException e) {
					throw new NuclosWebException(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
				}
			}
		});
	}

    private String getEoTitle(UID entityUID, Object oId) {
		final EntityObjectVO<?> eo = dalProvider.getEntityObjectProcessor(metaProv.getEntity(entityUID)).getByPrimaryKey(oId);
		return localeDelegate.getTreeViewLabel(eo, metaProv, userDetails.getDataLocal());
	}

	public String newExecutionId() {
		return new UID().getString();
	}

	public CollectiveProcessingExecutionEntry createCacheEntry(@NotNull String executionId, @NotNull String user) {
		checkExecutionParameter(executionId, user);
		final CollectiveProcessingExecutionEntry result = getCacheEntry(executionId);
		if (result.getUser() != null) {
			throw new NuclosFatalException("Cache entry exist already: executionId=" + executionId + ", userInCache=" + result.getUser() + ", userCreate=" + user);
		}
		result.setUser(user);
		return result;
	}

	public CollectiveProcessingExecutionEntry getCacheEntry(@NotNull String executionId, @NotNull String user) throws CommonPermissionException, CommonFinderException {
		checkExecutionParameter(executionId, user);
		final CollectiveProcessingExecutionEntry result = getCacheEntry(executionId);
		if (result.getUser() == null) {
			evictCache(executionId);
			throw new CommonFinderException("Execution with id " + executionId + " does not exist!");
		}
		if (!RigidUtils.equal(result.getUser(), user)) {
			throw new CommonPermissionException("User " + user + " is not authorized to access this information!");
		}
		return result;
	}

	private void checkExecutionParameter(@NotNull final String executionId, @NotNull final String user) {
		if (executionId == null) {
			throw new IllegalArgumentException("executionId must not be null");
		}
		if (user == null) {
			throw new IllegalArgumentException("user must not be null");
		}
	}

	private void evictForgottenCacheEntries() {
		Collection<Object> collectiveProcessingExecutionEntries = new ArrayList(
				((ConcurrentMapCache) cacheManager.getCache("collectiveProcessingExecutionCache")).getNativeCache().values()
		);
		collectiveProcessingExecutionEntries.stream().forEach(object -> {
			CollectiveProcessingExecutionEntry cacheEntry = (CollectiveProcessingExecutionEntry) object;
			if (cacheEntry.getLastUpdate() + (1000 * 60 * 60 * 12) < System.currentTimeMillis()) {
				// 12h no update? Dead client or client is not watching the progress
				LOG.warn("Removing progress cache entry for execution " + cacheEntry.getExecutionId() + ". " +
						"It's been more than 12 hours since the last update, and the information was not requested by any client");
				this.evictCache(cacheEntry.getExecutionId());
			}
		});
	}

	@Cacheable(value="collectiveProcessingExecutionCache", key="#p0")
	public CollectiveProcessingExecutionEntry getCacheEntry(@NotNull String executionId) {
		return new CollectiveProcessingExecutionEntry(executionId);
	}

	@Caching(evict = {
			@CacheEvict(value = "collectiveProcessingExecutionCache", key = "#p0")
	})
	public void evictCache(@NotNull String executionId) {
	}

	private abstract class ThreadLocalePropagationRunnable implements Runnable {
		private final SecurityContext securityContext;
		private final NuclosUserDetailsContextHolder.AllValues userDetailsValues;
		public ThreadLocalePropagationRunnable() {
			this.securityContext = SecurityContextHolder.getContext();
			this.userDetailsValues = userDetails.getAllValues();
		}

		public ThreadLocalePropagationRunnable initThreadLocales() {
			SecurityContextHolder.setContext(this.securityContext);
			userDetails.setAllValues(this.userDetailsValues);
			return this;
		}
	}

}
