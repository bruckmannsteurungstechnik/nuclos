package org.nuclos.server.rest.services.helper;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class RecursiveDependency {
	private final RecursiveDependency parent;
	private final String referenceAttributeFqn;
	private final String recordId;

	public static RecursiveDependency forRoot(final String recordId) {
		return new RecursiveDependency(
				null,
				null,
				recordId
		);
	}

	private RecursiveDependency(
			final RecursiveDependency parent,
			final String referenceAttributeFqn,
			final String recordId
	) {
		this.parent = parent;
		this.referenceAttributeFqn = referenceAttributeFqn;
		this.recordId = recordId;
	}

	public RecursiveDependency dependency(
			final String referenceAttributeFqn
	) {
		return dependency(referenceAttributeFqn, null);
	}

	public RecursiveDependency dependency(
			final String referenceAttributeFqn,
			final String recordId
	) {
		return new RecursiveDependency(
				this,
				referenceAttributeFqn,
				recordId
		);
	}

	public RecursiveDependency getParent() {
		return parent;
	}

	public String getReferenceAttributeFqn() {
		return referenceAttributeFqn;
	}

	public String getRecordId() {
		return recordId;
	}

	public String getRootRecordId() {
		if (parent != null) {
			return parent.getRootRecordId();
		}

		return recordId;
	}

	/**
	 * Creates the sub-path for the recursive depency, as used for the REST service:
	 * ../dependency/recursive/<sub-Bo 1>/<record ID 1>/<sub-Bo 2>/<record ID 2>/...
	 */
	public String toUrlPath() {
		final StringBuilder builder = new StringBuilder();

		return toUrlPath(builder);
	}

	private String toUrlPath(final StringBuilder builder) {
		// reference attribute is null for the root
		if (referenceAttributeFqn == null) {
			return builder.toString();
		}

		if (recordId != null) {
			builder.insert(0, recordId);
			builder.insert(0, "/");
		}

		builder.insert(0, referenceAttributeFqn);
		builder.insert(0, "/");

		return parent.toUrlPath(builder);
	}
}
