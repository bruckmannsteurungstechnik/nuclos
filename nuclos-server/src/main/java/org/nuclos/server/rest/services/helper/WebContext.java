package org.nuclos.server.rest.services.helper;

import java.io.Serializable;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common.E;
import org.nuclos.common.E._Generalsearchdocument;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.DataLanguageMap;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.preferences.IPreferencesProvider;
import org.nuclos.common.security.SubformPermission;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.layout.LayoutInfo;
import org.nuclos.common2.layoutml.AbstractWebDependents;
import org.nuclos.common2.layoutml.LayoutMLParser;
import org.nuclos.common2.layoutml.NuclosRuleAction;
import org.nuclos.common2.layoutml.NuclosRuleEvent;
import org.nuclos.common2.layoutml.WebSubform;
import org.nuclos.common2.layoutml.exception.LayoutMLException;
import org.nuclos.schema.rest.LoginParams;
import org.nuclos.schema.rest.RestLink;
import org.nuclos.server.attribute.ejb3.LayoutFacadeLocal;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.StateCache;
import org.nuclos.server.dal.DalSupportForGO;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.i18n.language.data.DataLanguageUtils;
import org.nuclos.server.i18n.language.data.DataLanguageVO;
import org.nuclos.server.nbo.NuclosBusinessObjectBuilder;
import org.nuclos.server.rest.NuclosRestApplication;
import org.nuclos.server.rest.ejb3.IRValueObject.JsonBuilderConfiguration;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.IWebContext;
import org.nuclos.server.rest.misc.IWebLayout;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.misc.RDataType;
import org.nuclos.server.rest.misc.RestLinks;
import org.nuclos.server.rest.misc.SessionEntityContext;
import org.nuclos.server.rest.misc.SkipValueException;
import org.nuclos.server.rest.services.rvo.JsonFactory;
import org.nuclos.server.rest.services.rvo.LayoutAdditions;
import org.nuclos.server.rest.services.rvo.RValueObject;
import org.nuclos.server.rest.services.rvo.UsageProperties;
import org.nuclos.server.rest.services.rvo.WebLayout;
import org.nuclos.server.security.SessionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Lazy;
import org.xml.sax.InputSource;

@Configurable
public class WebContext extends AbstractSessionIdLocator implements IWebContext {

	protected static final Logger LOG = LoggerFactory.getLogger(WebContext.class);

	@Lazy
	@Autowired
	protected JsonFactory jsonFactory;

	@Lazy
	@Autowired
	protected MetaProvider metaProvider;

	@Lazy
	@Autowired
	protected ParameterProvider parameterProvider;

	@Autowired
	protected IPreferencesProvider preferencesProvider;

	@Lazy
	@Autowired
	protected StateCache stateCache;

	public static final int DEFAULT_RESULT_LIMIT = 10000;

	@Override
	public final Set<String> getParameterKeys() {
		return super.getQueryParameterKeys();
	}

	@Override
	public final String getFirstParameter(String key) {
		List<String> lstString = getQueryParameters(key);

		if (lstString != null && !lstString.isEmpty()) {
			String firstString = lstString.get(0);
			return firstString;
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getFirstParameter(String key, Class<T> clazz) {
		List<String> lstString = getQueryParameters(key);

		if (lstString != null && !lstString.isEmpty()) {
			String firstString = lstString.get(0);
			if (clazz == String.class) {
				return (T)firstString;
			}

			if (clazz == Integer.class) {
				return (T)Integer.valueOf(firstString);
			}

			if (clazz == Long.class) {
				return (T)Long.valueOf(firstString);
			}

			if (clazz == Boolean.class) {
				return (T)Boolean.valueOf(firstString);
			}
		}

		return null;
	}

 	// NUCLOS-6311 5) Cache Annotation/Reflection Information
 	private static final Map<String, String> mpPathsForService = new HashMap<>();
 	private static String getPathForService(String serviceid) {
		if (!mpPathsForService.containsKey(serviceid)) {
			mpPathsForService.put(serviceid, NuclosRestApplication.getPathForService(serviceid));
		}
		return mpPathsForService.get(serviceid);
	}

	/**
	 * TODO: serviceid should be an enum.
	 * TODO: Make sure a service for every serviceid really exists.
	 */
	@Override
	public String getRestURI(String serviceid, Object... tags) {
 		String spath = getPathForService(serviceid);
 		if (spath == null) {
 			return null;
		}

		for (Object tag : tags) {
			if (tag != null) {
				spath = spath.replaceFirst("\\{[^\\}]*\\}", tag.toString());
			}
		}

		return restURL(spath);
	}

	private EntityMeta<?> boMeta;
	@Override
	public EntityMeta<?> getBOMeta() {
		if (boMeta == null) {
			throw new NuclosWebException(Response.Status.FORBIDDEN);
		}
		return boMeta;
	}

	protected void setBOMeta(EntityMeta<?> m) {
		this.boMeta = m;
	}

	private FieldMeta<?> refFieldMeta;
	@Override
	public FieldMeta<?> getRefFieldMeta() {
		return refFieldMeta;
	}

	private UsageCriteria usage;
	private UsageCriteria getUsage() {
		if (usage == null) {
			usage = Rest.getDefaultUsageCriteria(getBOMeta().getUID());
		}
		return usage;
	}

	@Override
	public IWebLayout getWebLayout() {
		return getWebLayout(null, false);
	}

	@Override
	public IWebLayout getWebLayout(UsageCriteria paramUC) {
		return getWebLayout(paramUC, false);
	}

	@Override
	public IWebLayout getWebLayout(UsageCriteria paramUC, boolean bSearchLayout) {
		try {
			UsageCriteria uc;
			if (paramUC == null) {
				uc = getUsage();

			//FIXME: This fall-back is awkward, should never happen
			} else if (paramUC.getStatusUID() == null && usage != null && usage.getStatusUID() != null && usage.getEntityUID().equals(paramUC.getEntityUID())) {
				UID processUid = usage.getProcessUID() != null ? usage.getProcessUID() : paramUC.getProcessUID();

				uc = new UsageCriteria(paramUC.getEntityUID(), processUid, usage.getStatusUID(), paramUC.getCustom());

			} else {
				uc = paramUC;
			}
			return getWebLayout(Rest.facade().getDetailLayoutIDForUsage(uc, bSearchLayout));

		} catch (CommonBusinessException cbe) {
			throw new NuclosWebException(cbe, paramUC.getEntityUID());
		}
	}

	private Map<UID, WebLayout> mpWebLayouts = new HashMap<UID, WebLayout>();

	//FIXME: This method is very expensive and eats lots of ressources
	// TODO: Get rid of this completely - use new Webclient layout if necessary at all
	@Override
	public WebLayout getWebLayout(UID layoutUID) throws CommonBusinessException {

		if (!mpWebLayouts.containsKey(layoutUID)) {
			String sLayoutML = Rest.facade().getLayoutML(layoutUID);
			boolean restrictionMustIgnoreGroovyRules = Rest.facade().isLayoutRestrictionsMustIgnoreGroovyRules(layoutUID);

			Collection<UID> entities = Rest.facade().getEntitiesAssignedToLayoutId(layoutUID);
			boolean doesLayoutChangesWithProcess = Rest.facade().doesLayoutChangeWithProcess(entities);

			LayoutInfo layoutInfo = new LayoutInfo(layoutUID, sLayoutML, restrictionMustIgnoreGroovyRules, entities, doesLayoutChangesWithProcess);
			mpWebLayouts.put(layoutUID, new WebLayout(layoutInfo));

		}

		return mpWebLayouts.get(layoutUID);
	}

	protected JsonObjectBuilder getLayout(String sLayoutId) {
		try {
			UID layoutUID = Rest.translateFqn(E.LAYOUT, sLayoutId);
			WebLayout webLayout = getWebLayout(layoutUID);

			boolean allowed = false;

			for (UID entity : webLayout.getLayoutInfo().getEntities()) {
				if (isReadAllowedForEntity(entity)) {
					allowed = true;
					break;
				}
			}

			if (!allowed) {
				throw new NuclosWebException(Response.Status.FORBIDDEN);
			}

			return webLayout.getFlattenedAndTabIndexedLayout();
		} catch (CommonBusinessException e) {
			throw new NuclosWebException(e, Rest.translateFqn(E.LAYOUT, sLayoutId));
		}
	}

	/**
	 * Gets a JSON builder containing all weblayouts for the given entity.
	 */
	protected List<org.nuclos.server.rest.services.rvo.LayoutInfo> getLayoutInfosForEntity(final String boMetaId) {
		UID entityUID = Rest.translateFqn(E.ENTITY, boMetaId);
		List<org.nuclos.server.rest.services.rvo.LayoutInfo> result = Rest.facade().getLayoutInfosForEntity(entityUID);

		return result;
	}

	@Override
	public List<LayoutAdditions> getComponentsFromLayoutByContext(UsageCriteria usage, boolean onlyVisibleComponents) {
		final UID subBoUID;
		if (usage.getEntityUID().equals(getBOMeta().getUID())) {
			subBoUID = null;
		} else {
			subBoUID = usage.getEntityUID();
		}
		return getComponentsFromLayout(getUsage(), subBoUID, onlyVisibleComponents);
	}

	@Override
	public List<LayoutAdditions> getComponentsFromLayout(UsageCriteria baseUsage, UID subBoUID, boolean onlyVisibleComponents) {
		// Fields from Layout of the Master-Detail View

		if (subBoUID == null) {
			//TODO: Performance: 200ms
			IWebLayout w = getWebLayout(baseUsage);
			//TODO: Performance: 100ms
			List<LayoutAdditions> ret = w.getComponents(usage.getEntityUID(), null, onlyVisibleComponents);
			return ret;
		}

		//Fields from Subform.
		IWebLayout layout = getWebLayout(baseUsage);
		WebSubform subform = layout.getWebSubform(subBoUID);

		//TODO: Security breach... Must check if user is allowed to get those fields
		//TODO: only if this is a matrix or a chart datasource
		if (subform == null) {
			final Collection<FieldMeta<?>> collFm = Rest.facade().getAllFieldsByEntity(subBoUID, false);
			List<FieldMeta<?>> sortedFm = new ArrayList<FieldMeta<?>>(collFm);
			Collections.sort(sortedFm);

			final List<LayoutAdditions> lstFields = new ArrayList<LayoutAdditions>();

			for (FieldMeta<?> fm : sortedFm) {
				LayoutAdditions fmExtra = new LayoutAdditions(fm, false, false, new ArrayList<NuclosRuleEvent>(), null, null, null, null, null);
				lstFields.add(fmExtra);
			}

			//TODO: This list of LayoutAdditions is not complete. Most important. RuleEvents are missing. There are
			//necessary later in order to find out which fields are layout-ml targets (see NUCLOS-4420)
			return lstFields;
		}

		return layout.getComponents(subform.getEntity(), subform.getForeignkeyfield(), onlyVisibleComponents);
	}

	protected SessionContext login(
			final LoginParams params,
			final String jSessionId
	) {
		Locale locale = parseLocale(params.getLocale());

		SessionContext session = Rest.facade().webLogin(
				params.getUsername(),
				StringUtils.defaultIfEmpty(params.getPassword(), ""),
				locale,
				params.getDatalanguage(),
				jSessionId
		);
		if (session == null) {
			throw new NuclosWebException(Response.Status.UNAUTHORIZED);
		}
		return session;
	}

	protected void setLocale(final String localeString) {
		SessionContext session = getSessionContext();
		Locale locale = parseLocale(localeString);
		session.setLocale(locale);
	}

	protected Locale parseLocale(final String sLocale) {
		Locale locale;
		if (sLocale == null || sLocale.length() < 2) {
			locale = Locale.GERMAN;
		} else if (sLocale.length() < 5) {
			locale = new Locale(sLocale.substring(0, 2));
		} else {
			locale = new Locale(sLocale.substring(0, 2), sLocale.substring(3, 5));
		}
		return locale;
	}

	/**
	 * Throws an exception with status code FORBIDDEN if the current user
	 * is not a superuser.
	 */
	protected final void checkSuperUser() {
		if (!Rest.facade().isSuperUser()) {
			throw new NuclosWebException(Status.FORBIDDEN);
		}
	}

	/**
	 * Throws an exception with status code FORBIDDEN if the current user
	 * is not the given user.
	 */
	protected final void checkUser(final String username) {
		if (!StringUtils.equalsIgnoreCase(getSessionContext().getUsername(), username)) {
			throw new NuclosWebException(Status.FORBIDDEN);
		}
	}

	public final SessionContext getSessionContext() {
		SessionContext result = Rest.facade().getSessionContext(getSessionId());
		if (result == null) {
			throw new IllegalStateException("Unable to determine session. (Verify JSESSIONID cookie.)");
		}
		return result;
	}

	protected UID getPkForUserIfNull(UID fk, UID foreignEntity) {
		if (fk == null && E.USER.getUID().equals(foreignEntity)) {
			return SecurityCache.getInstance().getUserUid(getUser());
		}
		return fk;
	}

	@Override
	public String getUser() {
		return Rest.facade().getCurrentUserName();
	}

	public UID getMandatorUID() {
		return Rest.facade().getCurrentMandatorUID();
	}

	@Override
	public String getRemoteAddr() {
		if (this.httpRequest != null) {
			return this.httpRequest.getRemoteAddr();
		}

		return null;
	}

	@Override
	public Locale getLocale() {
		return Rest.facade().getCurrentLocale();
	}

	protected boolean isReadAllowedForEntity(UID entity) {
		return Rest.getEntityPermission(entity).isReadAllowed();
	}

	protected SessionEntityContext checkPermission(JsonObject json) {
		UID entity = getEntityFromJson(json);
		SessionEntityContext vInfo = checkPermission(E.ENTITY, Rest.translateUid(E.ENTITY, entity));
		if (!isReadAllowedForEntity(entity)) {
			throw new NuclosWebException(Response.Status.FORBIDDEN);
		}
		return vInfo;
	}

	protected SessionEntityContext checkPermission(EntityMeta<?> eMeta, String bocOrField) {
		return checkPermission(eMeta, bocOrField, null, true);
	}

	protected SessionEntityContext checkPermission(EntityMeta<?> eMeta, String bocOrField, String pk) {
		return checkPermission(eMeta, bocOrField, pk, true);
	}

	protected SessionEntityContext checkPermission(EntityMeta<?> eMeta, String bocOrField, String pk, boolean checkDirectEntityPermission) {
		UID master = Rest.translateFqn(eMeta, bocOrField);
		if (master == null) {
			throw new NuclosWebException(Status.NOT_FOUND, "BO-Class: " + bocOrField + " not found!");
		}

		this.boMeta = null;
		//First test, if it is the UID of the entity, if not, it should be a UID of a field.
		try {
			boMeta = Rest.getEntity(master);

		} catch (CommonFatalException cfe) {
			try {
				master = Rest.getEntityField(master).getEntity();
				boMeta = Rest.getEntity(master);

			} catch (CommonFatalException cfe2) {
				throw new NuclosWebException(Status.NOT_FOUND, "BO-Class: " + bocOrField + " not found!");
			}
		}

		if (boMeta.isDynamicTasklist()) {
			final Serializable tlDef = boMeta.getProperty(EntityMeta.PROPERTY.TASKLIST_DEFINITION);
			if (tlDef == null) {
				throw new NuclosWebException(Response.Status.FORBIDDEN);
			}
			if (!Rest.facade().getDynamicTaskLists().contains(tlDef)) {
				throw new NuclosWebException(Response.Status.FORBIDDEN);
			}
			usage = Rest.getDefaultUsageCriteria(master);
			return new SessionEntityContext(boMeta, usage);
		}

		if (checkDirectEntityPermission && !isReadAllowedForEntity(master)) {
			throw new NuclosWebException(Response.Status.FORBIDDEN);
		}

		if (pk != null) {
			try {
				if (!Rest.facade().isTemporaryId(pk)) {
					if (boMeta.isUidEntity()) {
						usage = Rest.facade().getUsageCriteriaForPK(
								Rest.translateFqn(boMeta, !Rest.facade().isTemporaryId(pk) ? pk : Rest.facade().getTempId(pk)),
								master);

					} else {
						usage = Rest.facade().getUsageCriteriaForPK(
								Long.parseLong(!Rest.facade().isTemporaryId(pk) ? pk : Rest.facade().getTempId(pk)),
								master);

					}
				} else {
					EntityObjectVO<Long> eovo = Rest.facade().getTemporaryObject(pk);
					usage = new UsageCriteria(boMeta.getUID(), eovo.getFieldUid(SF.PROCESS_UID), eovo.getFieldUid(SF.STATE_UID), Rest.facade().getCustomUsage());
				}
			} catch (CommonBusinessException cbe) {
				throw new NuclosWebException(Response.Status.EXPECTATION_FAILED, "Entry with ID " + pk + " not found in BO " + master + "!");
			}
		} else {
			usage = Rest.getDefaultUsageCriteria(master);
		}

		return new SessionEntityContext(boMeta, usage);
	}

	protected FieldMeta<?> checkPermissionForFieldGet(String field, FieldMeta<?> fm, UsageCriteria usage,
			String sourcefield, String pksource) throws CommonBusinessException {

		UID fieldUid = fm.getUID();

		//First check direct right access over Layout
		if (isReadAllowedForEntity(fm.getEntity())) {
			checkPermission(E.ENTITYFIELD, field);
			IWebLayout webLayout = getWebLayout(usage);
			if (webLayout != null) {
				for (LayoutAdditions frvo : webLayout.getComponents(usage.getEntityUID(), null, true)) {
					if (frvo.getUID().equals(fieldUid)) {
						return fm;
					}
				}
			}

			if (Rest.isReadAllowedForField(fm, usage)) {
				return fm;
			}
		}

		//Obviously there is no direct access, so check the layoutML Rules
		if (sourcefield != null ) {
			FieldMeta<?> fmSource = Rest.getEntityField(Rest.translateFqn(E.ENTITYFIELD, sourcefield));
			EntityMeta<?> eMetaSource = Rest.getEntity(fmSource.getEntity());
			UsageCriteria usageSource;

			if (pksource == null || "null".equals(pksource)) {
				// TODO check process id 
				UID processUid = null;
				UID stateUid = eMetaSource.isStateModel() ? Rest.facade().getInitialStateId(eMetaSource.getUID()) : null;

				usageSource = new UsageCriteria(
						eMetaSource.getUID(),
						processUid,
						stateUid,
						Rest.facade().getCustomUsage()
					);
			} else if (eMetaSource.isUidEntity()) {
				usageSource = Rest.facade().getUsageCriteriaForPK(Rest.translateFqn(eMetaSource, pksource), eMetaSource.getUID());

			} else {
				usageSource = Rest.facade().getUsageCriteriaForPK(Long.parseLong(pksource), eMetaSource.getUID());

			}

			try {
				boMeta = Rest.getEntity(fm.getEntity());
				IWebLayout webLayout = getWebLayout(usageSource);

				String sLayoutML = webLayout.getLayoutInfo().getLayoutML();
				if (sLayoutML != null) {

					Map<UID, List<NuclosRuleEvent>> ruleEvents =
							new LayoutMLParser().getRuleEvents(new InputSource(new StringReader((sLayoutML))));

					for (List<NuclosRuleEvent> lstRules : ruleEvents.values()) {
						for (NuclosRuleEvent event :lstRules) {
							if (event.getType().equals("lookup")) {
								for (NuclosRuleAction action : event.getRuleActions()) {
									if (fieldUid.getString().equals(action.getParameter())) {
										return fm;
									}
								}
							}
						}
					}
				} else {
					//TODO: Security issue. If there is no layout other checks should be done.
					//TODO: In that case the layout ml rules of the sub-form should be checked.
					return fm;
				}

			} catch (LayoutMLException lmle) {
				boMeta = null;
				throw new NuclosWebException(lmle, usage.getEntityUID());

			}

			//Handle layoutml-Rules within Subform. Thus find master layout first
			Set<UID> foreignEntities = new HashSet<UID>();
			Map<UID, FieldMeta<?>> mpFields = MetaProvider.getInstance().getAllEntityFieldsByEntity(eMetaSource.getUID());

			for (UID fmUid : mpFields.keySet()) {
				FieldMeta<?> fm2 = mpFields.get(fmUid);
				if (fm2.getForeignEntity() != null && isReadAllowedForEntity(fm2.getForeignEntity())) {
					foreignEntities.add(fm2.getForeignEntity());
				}
			}

			for (UID fe : foreignEntities) {
				IWebLayout layout = getWebLayout(Rest.getDefaultUsageCriteria(fe));
				if (layout == null) {
					continue;
				}

				Map<UID, AbstractWebDependents> mpSublists = layout.getVisibleSubLists();
				if (mpSublists.isEmpty()) {
					continue;
				}

				for (UID uid : mpSublists.keySet()) {
					if (mpSublists.get(uid) instanceof WebSubform) {
						WebSubform subform = (WebSubform) mpSublists.get(uid);

						if (subform.getEntity().equals(eMetaSource.getUID())) {

							List<LayoutAdditions> lstColumns = layout.getComponents(subform.getUID(), subform.getForeignkeyfield(), true);

							for (LayoutAdditions col : lstColumns) {
								for (NuclosRuleEvent event :col.getRuleEvents()) {

									if (event.getType().equals("lookup")) {
										for (NuclosRuleAction action : event.getRuleActions()) {
											if (fieldUid.getString().equals(action.getParameter())) {
												return fm;
											}
										}
									}

								}
							}

						}
					}
				}

			}

		}

		boMeta = null;
		throw new NuclosWebException(Response.Status.FORBIDDEN);
	}

	UID checkPermissionForReferenceField(UID field) {
		FieldMeta<?> fm = Rest.getEntityField(field);
		if (fm.getForeignEntity() == null) {
			throw new NuclosWebException(Response.Status.FORBIDDEN);
		}
		// set boMeta, otherwise throws forbidden exception
		boMeta = Rest.getEntity(fm.getForeignEntity());
		UID entity = fm.getEntity();
		Map<UID, FieldMeta<?>> mpFields = Rest.getAllEntityFieldsByEntityIncludingVersion(entity);

		if (isReadAllowedForEntity(entity)) {
			checkPermission(E.ENTITY, Rest.translateUid(E.ENTITY, entity));

			for(FieldMeta<?> f : mpFields.values()) {
				if (f.getUID().equals(field)) {
					refFieldMeta = fm;
					return field;
				}
			}
		}

		for (FieldMeta<?> reff : mpFields.values()) {
			UID foreignEntity = reff.getForeignEntity();
			if (foreignEntity == null) {
				continue;
			}
			if (isReadAllowedForEntity(foreignEntity)) {
				LayoutFacadeLocal layoutFacadeLocal = SpringApplicationContextHolder.getBean(LayoutFacadeLocal.class);
				for (UID uid : layoutFacadeLocal.getAllLayoutUidsForEntity(foreignEntity)) {
					try {
						IWebLayout wl = getWebLayout(uid);
						if (wl != null) {
							if (wl.getWebSubform(entity) != null) {
								refFieldMeta = fm;
								return field;
							}
						}
					} catch (CommonBusinessException cbe) {
						throw new NuclosWebException(cbe, foreignEntity);
					}
				}

			}
		}
		throw new NuclosWebException(Response.Status.FORBIDDEN);
	}

	protected SessionEntityContext checkPermissionForSubform(
			final EntityMeta<?> parentEntityMeta,
			final RecursiveDependency dependency
	) {
		SessionEntityContext info = checkPermission(
				E.ENTITY,
				Rest.translateUid(E.ENTITY, parentEntityMeta.getUID()),
				dependency.getRootRecordId()
		);

		if (Rest.facade().isTemporaryId(dependency.getRootRecordId())) {
			try {
				EntityObjectVO<Long> temporaryObject = Rest.facade().getTemporaryObject(dependency.getRootRecordId());
				if (!getUser().equals(temporaryObject.getCreatedBy())) {
					throw new NuclosWebException(Response.Status.FORBIDDEN);
				}
			} catch (CommonBusinessException e) {
				throw new NuclosWebException(Response.Status.NOT_FOUND, e.getMessage());
			}
		}

		List<FieldMeta<?>> referenceAttributes = getReferenceAttributes(dependency);
		for (FieldMeta<?> fieldMeta: referenceAttributes) {
			SubformPermission permission = SecurityCache.getInstance().getSubformPermission(
					fieldMeta.getEntity(),
					getUsage(),
					getUser(),
					getMandatorUID()
			);

			if (permission == null || !permission.includesReading()) {
				throw new NuclosWebException(Status.FORBIDDEN);
			}
		}

		// Reference field on the deepest level
		FieldMeta<?> fieldMeta = referenceAttributes.get(0);

		return new SessionEntityContext(info, fieldMeta);
	}

	private static List<FieldMeta<?>> getReferenceAttributes(RecursiveDependency dependency) {
		final List<FieldMeta<?>> result = new ArrayList<>();

		while (dependency.getReferenceAttributeFqn() != null) {
			UID fieldUid = Rest.translateFqn(E.ENTITYFIELD, dependency.getReferenceAttributeFqn());
			FieldMeta<?> fieldMeta = Rest.getEntityField(fieldUid);
			result.add(fieldMeta);
			dependency = dependency.getParent();
		}

		return result;
	}

	protected SessionEntityContext checkPermissionForSubform(EntityMeta<?> parentEntityMeta, FieldMeta<?> fm, String fk, boolean checkDirectEntityPermission) {

		UID master = parentEntityMeta.getUID();
		if (E.GENERICOBJECT.getUID().equals(master) && fk != null) {
			EntityObjectVO govo = DalSupportForGO.getEntityObject(Long.valueOf(fk), new HashSet<>());
			master = govo.getDalEntity();
		}

		SessionEntityContext info = checkPermission(E.ENTITY, Rest.translateUid(E.ENTITY, parentEntityMeta.getUID()), fk);

		if (Rest.facade().isTemporaryId(fk)) {
			try {
				EntityObjectVO<Long> temporaryObject = Rest.facade().getTemporaryObject(fk);
				if (!getUser().equals(temporaryObject.getCreatedBy())) {
					throw new NuclosWebException(Response.Status.FORBIDDEN);
				}
			} catch (CommonBusinessException e) {
				throw new NuclosWebException(Response.Status.NOT_FOUND, e.getMessage());
			}

		} else {

			if (checkDirectEntityPermission && (!isReadAllowedForEntity(master) || !isReadAllowedForEntity(fm.getEntity()))) {
				EntityMeta<?> eMeta = Rest.getEntity(fm.getEntity());

				// NUCLOS-5983 1)
				if (parentEntityMeta.isStateModel()) {
					UID mandator = Rest.facade().getCurrentMandatorUID();
					Map<UID, SubformPermission> subPermissions = SecurityCache.getInstance().getSubForm(getUser(), eMeta.getUID(), mandator);
					SubformPermission subPermission = subPermissions.get(info.getUsage().getStatusUID());
					if (subPermission == null || !subPermission.includesReading()) {
						throw new NuclosWebException(Response.Status.FORBIDDEN);
					}
				}

				if (!eMeta.isChart() && !eMeta.isDynamic()) {
					// chart and dynamic datasources are not defined in layout

					boolean anyValidLayout = false;
					// notice: do not check with getForeignEntity(). In case of parent virtual bo, the reference is on the 'base' table, 
					// but the subform could also be used inside a layout of the virtual bo!
					if (isReadAllowedForEntity(master)) {
						UsageCriteria uc;
						try {
							long longFk = Long.parseLong(fk);
							uc = Rest.getUsageCriteria(master, longFk);
						} catch (NumberFormatException ex) {
							uc = Rest.getDefaultUsageCriteria(master);
						}
						IWebLayout wl = getWebLayout(uc);
						if (wl != null && (wl.getWebSubform(fm.getEntity()) != null || wl.hasChart(fm.getEntity()))) {
							anyValidLayout = true;
						}
					}

					if (!anyValidLayout) {
						throw new NuclosWebException(Response.Status.FORBIDDEN);
					}
				}
			}
		}
		refFieldMeta = fm;
		return new SessionEntityContext(info, fm);
	}

	protected SessionEntityContext checkPermissionForReference(UID master, String masterBoId, UID reffieldUid) {
		SessionEntityContext info = checkPermission(E.ENTITY, Rest.translateUid(E.ENTITY, master));

		FieldMeta<?> fm = Rest.getEntityField(reffieldUid);
		if (master.equals(fm.getForeignEntity())) {
			refFieldMeta = fm;
			return new SessionEntityContext(info, fm);
		} else {
			// master != foreign entity
			// test security for sub BO in virtual BO (sub BO in meta is referencing on "real" BO, but is also used in "virtual")
			EntityMeta<Object> entityMeta = Rest.getEntity(master);
			Object masterPK = entityMeta.isUidEntity() ? new UID(masterBoId) : new Long(masterBoId);
			CollectableSearchExpression clctexpr = new CollectableSearchExpression(
					SearchConditionUtils.newPkComparison(entityMeta.getPk().getUID(entityMeta), ComparisonOperator.EQUAL, masterPK));
			if (NucletDalProvider.getInstance().getEntityObjectProcessor(master).count(clctexpr) > 0) {
				refFieldMeta = fm;
				return new SessionEntityContext(info, fm);
			}
		}

		//If there was no direct reference to the master BO, then it may be a GenericObjectDocument-Subform which references
		//via GenericObject
		if (E.GENERICOBJECT.getUID().equals(fm.getForeignEntity())) {
			refFieldMeta = fm;
			return new SessionEntityContext(info, fm);
		}

		throw new NuclosWebException(Response.Status.FORBIDDEN);
	}

	protected static UID getEntityFromJson(JsonObject json) {
		if (!json.containsKey("boMetaId")) {
			throw new NuclosWebException(Response.Status.BAD_REQUEST);
		}
    	return Rest.translateFqn(E.ENTITY, json.getString("boMetaId"));
	}

	private boolean isInteger(String o) {
		try {
			Integer.parseInt(o);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}


	/**
	 * in case there is user input which can be processed in nuclos rules (InputRequiredException)
	 * this puts the input variables into inputContext
	 */
	protected void updateInputContext(JsonObject data) {
		Map<String, Serializable> context = null;

		JsonObject inputrequired = (JsonObject) data.get("inputrequired");

		if (inputrequired != null) {
			JsonObject result = (JsonObject) inputrequired.get("result");

			if (result != null) {
				context = new HashMap<String, Serializable>();

				for (String key : result.keySet()) {
					String value = result.get(key).toString();

					if (isInteger(value)) {
						context.put(key, Integer.parseInt(value));
					} else if (value instanceof String) {
						context.put(key, result.getString(key));
					} else {
						context.put(key, value);
					}
				}
			}
		}
		Rest.facade().updateInputContext(context);
	}

	protected void clearInputContext() {
		Rest.facade().clearInputContext();
	}

	protected <PK> JsonObjectBuilder insertUpdateDelete(EntityObjectVO<PK> eo, NuclosMethod method, SessionEntityContext info) throws CommonBusinessException {

     	if (eo.isFlagNew()) {
            eo = Rest.facade().insert(eo);
    	} else if (eo.isFlagUpdated()) {
            eo = Rest.facade().update(eo);
    	} else if (eo.isFlagRemoved()) {
            Rest.facade().delete(eo.getDalEntity(), eo.getPrimaryKey(), true);
    	}

        if (eo != null) {
			JsonObjectBuilder json = getJsonObjectBuilder(eo);
            return json;
        }
    	return null;
	}

	protected <PK> JsonObjectBuilder getJsonObjectBuilder(final EntityObjectVO<PK> eo) throws CommonBusinessException {
		usage = Rest.getUsageCriteriaFromEO(eo);
		UsageProperties up = new UsageProperties(usage, this, metaProvider, preferencesProvider, parameterProvider);
		final UID sessionDataLanguageUID = getSessionContext().getDataLanguageUID();
		RValueObject<PK> rvobj = new RValueObject<>(eo, JsonBuilderConfiguration.getFull(), up, null, sessionDataLanguageUID, jsonFactory, metaProvider, stateCache);
		return rvobj.getJSONObjectBuilder();
	}

	protected <PK> EntityObjectVO<PK> prepareEOForModification(JsonObject json, NuclosMethod method, SessionEntityContext info) throws CommonBusinessException {
		UID jsonEntityUID = getEntityFromJson(json);
		EntityMeta<?> eMeta = Rest.getEntity(jsonEntityUID);
		return prepareEOForModification(json, method, eMeta, null, null, null, info);
	}

	protected <PK> EntityObjectVO<PK> prepareEOForModification(JsonObject json, NuclosMethod method, EntityMeta<?> eMeta, IDependentKey subBoDependentKey, String baseId, UsageCriteria baseUsage, SessionEntityContext info) throws CommonBusinessException {
		if (eMeta == null) {
			throw new IllegalArgumentException("eMeta must not be null");
		}

		EntityObjectVO<PK> entityObjectVOFromJson = getEOFromJson(json, method, eMeta, subBoDependentKey, baseId);
		if (baseId == null) {
			baseId = entityObjectVOFromJson.getPKStringRepresentation();
			if (baseId == null) {
				baseId = entityObjectVOFromJson.getFieldValue(RValueObject.TEMPORARY_ID_FIELD, String.class);
			}
		}

		JsonObject attrJson = null;
		if (!entityObjectVOFromJson.isFlagRemoved() && json.containsKey("attributes")) {
			attrJson = json.getJsonObject("attributes");
		}
		UsageCriteria parentUsage = Rest.getUsageCriteriaFromJsonAttrAndEO(attrJson, entityObjectVOFromJson);

		if (baseUsage == null) {
			baseUsage = parentUsage;
		}

    	if (json.containsKey("subBos")) {
    		JsonObject subBos = json.getJsonObject("subBos");
    		final IDependentDataMap result = new DependentDataMap();
    		if (json.containsKey(RValueObject.TEMPORARY_ID_JSON_KEY)) {
    			@SuppressWarnings("unchecked")
				Set<IDependentKey> alreadyLoaded = (Set<IDependentKey>) entityObjectVOFromJson.getFieldValue(RValueObject.TEMPORARY_DEPENDENTS_ALREADY_LOADED_FIELD);
    			for (IDependentKey dependentKey : entityObjectVOFromJson.getDependents().getKeySet()) {
    				if (!alreadyLoaded.contains(dependentKey)) {
    					// Unterformular wurde vom WebClient noch nicht abgeholt und wird daher komplett vom temporaeren Objekt uebernommen:
    					result.addAllData(dependentKey, entityObjectVOFromJson.getDependents().getDataPk(dependentKey));
    				}
    			}
    		}

    		boolean hasChangedSubBos = NuclosMethod.INSERT.equals(method);

    		for (String sbKey : subBos.keySet()) {
    			NuclosMethod subMethod;
    			if ("insert".equals(sbKey)) {
    				subMethod = NuclosMethod.INSERT;
    			} else if ("update".equals(sbKey)) {
    				subMethod = NuclosMethod.UPDATE;
    			} else if ("delete".equals(sbKey)) {
    				subMethod = NuclosMethod.DELETE;
    			} else {
    				continue;
				}

				JsonObject subBoMetas = subBos.getJsonObject(sbKey);
				for (String refAttrId : subBoMetas.keySet()) {
					JsonArray subBoList = subBoMetas.getJsonArray(refAttrId);
					if (subBoList.isEmpty()) {
						continue;
					}

					UID refFieldUID = Rest.translateFqn(E.ENTITYFIELD, refAttrId);
					FieldMeta<?> refFieldMeta = Rest.getEntityField(refFieldUID);
					UID subEntityUID = refFieldMeta.getEntity();

					// -NUCLOS-5926 5) Do not check permissions against baseUsage, check against parentUsage, which is different in sub-suboforms.-
					// NUCLOS-7689 	Must check against baseUsage, because sub-subform is included in the layout of the base entity and rights are configured
					// 				directly in the status model of the base entity, too.
					if (!Rest.isWriteAllowedForSubform(subEntityUID, baseUsage)) {
						String msg = "No permission for User " + getUser() + " to write into subform " + Rest.translateUid(E.ENTITY, subEntityUID);
						throw new CommonPermissionException(msg);
					}

					// NUCLOS-6134
					SubformPermission subformPermission = Rest.facade().getSubformPermission(subEntityUID, baseUsage);
					if (subMethod == NuclosMethod.DELETE) {
						if (subformPermission == null || !subformPermission.canDelete()) {
							String msg = "No permission for User " + getUser() + " to delete from subform " + Rest.translateUid(E.ENTITY, subEntityUID);
							throw new CommonPermissionException(msg);
						}
					} else if (subMethod == NuclosMethod.INSERT) {
						if (subformPermission == null || !subformPermission.canCreate()) {
							String msg = "No permission for User " + getUser() + " to create in subform " + Rest.translateUid(E.ENTITY, subEntityUID);
							throw new CommonPermissionException(msg);
						}
					}

					IDependentKey dependentKey = DependentDataMap.createDependentKey(refFieldMeta);
					EntityMeta<?> subEntityMeta = Rest.getEntity(subEntityUID);
					for (int i = 0; i < subBoList.size(); i++) {
						final EntityObjectVO<?> subEo;
						if (subMethod == NuclosMethod.DELETE) {
							Object id;
							if (subEntityMeta.isUidEntity()) {
								id = new UID(subBoList.getString(i));
							} else {
								id = subBoList.getJsonNumber(i).longValue();
							}
							subEo = Rest.facade().get(subEntityUID, id, false);
							subEo.flagRemove();
						} else {
							JsonObject subBo = subBoList.getJsonObject(i);
							subEo = prepareEOForModification(subBo, subMethod, subEntityMeta, dependentKey, baseId, baseUsage, null);
							if (subMethod == NuclosMethod.INSERT) {
								subEo.flagNew();
							} else {
								subEo.flagUpdate();
							}
						}
						if (subEo != null) {
							result.addData(refFieldMeta, subEo);
							hasChangedSubBos = true;
						}
					}
    			}
    		}

    		if (hasChangedSubBos) {
    			entityObjectVOFromJson.setDependents(result);
    		}
    	}
    	if (entityObjectVOFromJson.isFlagRemoved() || !json.containsKey("attributes")) {
    		return entityObjectVOFromJson;
    	}

    	return parseJsonValuesIntoEO(json.getJsonObject("attributes"), entityObjectVOFromJson, eMeta, baseUsage);
    }

	private LayoutAdditions getProcessFromList(List<LayoutAdditions> lstAdditions) {
    	for (LayoutAdditions layoutadds : lstAdditions) {
    		FieldMeta<?> fm = layoutadds.getFieldMeta();

    		if (SF.PROCESS.checkField(fm.getEntity(), fm.getUID()) ) {
    			return layoutadds;
    		}
    	}
    	return null;
	}

	private <PK> EntityObjectVO<PK> parseJsonValuesIntoEO(JsonObject attrJson, EntityObjectVO<PK> eo, EntityMeta<?> eMeta, UsageCriteria baseUsage) throws CommonPermissionException {

		// eoUsage is the same as baseUsage for initial recursive call, then subbo usage

		//NUCLOS-4209, NUCLOS-4228
    	UsageCriteria eoUsage = Rest.getUsageCriteriaFromJsonAttrAndEO(attrJson, eo);
    	UID subBoUID = null;
    	if (!baseUsage.getEntityUID().equals(eoUsage.getEntityUID())) {
    		subBoUID = eoUsage.getEntityUID();
		}

    	EntityMeta<?> eMetaOfEo = Rest.getEntity(eo.getDalEntity());
    	// set mandator directly (no field in layout!)
    	if (eMetaOfEo.isMandator()) {
    		final String sMandatorFieldName = SF.MANDATOR.getFieldName();
    		if (attrJson.containsKey(sMandatorFieldName) && !attrJson.isNull(sMandatorFieldName)) {
    			JsonObject jsonFK = attrJson.getJsonObject(sMandatorFieldName);
				if (!jsonFK.isNull("id")) {
					UID mandatorUID = UID.parseUID(jsonFK.getString("id"));
					SecurityCache.getInstance().checkMandatorLoginPermission(getUser(), mandatorUID);
					eo.setFieldUid(SF.MANDATOR_UID, mandatorUID);
					eo.setFieldValue(SF.MANDATOR, SecurityCache.getInstance().getMandator(mandatorUID).getName());
				}
    		}
    	}

    	//FIXME: This part is not consistent with the security. Target Components of Value Changing LayoutML Rules
		//may be writeable even though the user does not have the rights for it. See also comment below
    	Set<UID> targetsOfValueChangingEvents = new HashSet<UID>();
		List<LayoutAdditions> lstAdditions = getComponentsFromLayout(baseUsage, subBoUID,false);

		for (LayoutAdditions layoutadds : lstAdditions) {
    		targetsOfValueChangingEvents.addAll(layoutadds.getTargetsOfValueChangingEvents());
    	}

    	for (FieldMeta<?> fm : eMetaOfEo.getFields()) {

			if (SF.STATE.checkField(fm.getEntity(), fm.getUID()) || fm.isPrimaryKey()) {
				continue;
			}

    		boolean updateField = fm.isModifiable() && (parameterProvider.isEnabled(ParameterProvider.MODIFIABLE_READONLY) || !fm.isReadonly());
    		if (updateField) {
    			if (!Rest.isReadAllowedForField(fm, eoUsage) || !Rest.isWriteAllowedForField(fm, eoUsage)) {
    				updateField = false;
    			} else if (subBoUID != null) {
					// TODO: NUCLOS-6140 Centralize
					// NUCLOS-6134
					SubformPermission subformPermission = Rest.facade().getSubformPermission(subBoUID, baseUsage);
					if (subformPermission == null || !subformPermission.getGroupPermission(fm.getFieldGroup()).includesWriting()) {
						updateField = false;;
					}
				}
    		}

    		// NUCLOS-6195 4 - This has always been some kind of hack, but for Boolean Field there should be a default value from server
    		if (!updateField && !fm.isNullable() && eo.isFlagNew() && fm.getJavaClass() != Boolean.class) {
    			updateField = true;
    		}

    		//TODO: NUCLOS-4420 A "readonly" field can be set by layout-rules. The Server cannot know, if it actually was set
    		//by such rule, but cannot deny writing it, as the client has the power of the rules.

    		if (!updateField && targetsOfValueChangingEvents.contains(fm.getUID())) {
    			updateField = true;
    		}

    		// allow update of Documents
    		UID field = fm.getUID();
    		if(!updateField && _Generalsearchdocument.DocumentFile.UID.equals(field)) {
    			updateField = true;
    		}

    		String fieldName = NuclosBusinessObjectBuilder.getFieldNameForFqn(fm);
    		if (updateField && attrJson.containsKey(fieldName)) {
    			try {
	    			Object content = new RDataType(fm.getDataType(), fm.getPrecision()).getValue(attrJson, fieldName);

	    			eo.setFieldValue(field, content);

	    			if (fm.isFileDataType()) {
	    				if (content != null) {
	    					GenericObjectDocumentFile godf = (GenericObjectDocumentFile)content;
	    					eo.setFieldUid(field, (UID)godf.getDocumentFilePk());
	    				} else {
	    					eo.setFieldUid(field, null);
	    				}

	    			} else if (fm.getForeignEntity() != null) {
	    				boolean bNullKey = !attrJson.containsKey(fieldName) || attrJson.isNull(fieldName);
	    				UID foreignEntity = fm.getForeignEntity();
	    				boolean bUidEntity = Rest.getEntity(foreignEntity).isUidEntity();

	    				if (bNullKey) {
	        				if (bUidEntity) {
								UID fk = getPkForUserIfNull(null, foreignEntity);
								eo.setFieldUid(field, fk);
	    					} else {
	    						eo.setFieldId(field, null);
	    					}
	        				eo.setFieldValue(field, null);

	    				} else {
							JsonObject jsonFK = attrJson.getJsonObject(fieldName);
							if (jsonFK.isNull("id")) {
								eo.removeFieldId(field);
								eo.removeFieldUid(field);
								eo.removeFieldValue(field);
							} else {
		        				if (bUidEntity) {
		        					EntityMeta<Object> entityMeta = E.getByUID(foreignEntity);
		        					UID fk = Rest.translateFqn(entityMeta, jsonFK.getJsonString("id").getString());
		        					eo.setFieldUid(field, fk);
		    					} else {
		    						Long fk = jsonFK.getJsonNumber("id").longValue();
		    	    				eo.setFieldId(field, fk);
		    					}

								String name = null;
								if (jsonFK.containsKey("name") && !jsonFK.isNull("name")) {
									name = jsonFK.getJsonString("name").getString();
								}

								eo.setFieldValue(field, name);
							}
	    				}
	    			}
    			} catch (SkipValueException sve) {
    				LOG.debug("Unable to parse:", sve);
    			}
    		}
    	}

    	if (eo.getPrimaryKey() != null) {
    		UID oldStateUid = eo.getFieldUid(SF.STATE_UID);
    		if (oldStateUid != null) {
    			String stateAttributeName = SF.STATE.getFieldName();
				boolean bNullKey = !attrJson.containsKey(stateAttributeName) || attrJson.isNull(stateAttributeName);
				if (!bNullKey) {
					JsonObject jsonFK = attrJson.getJsonObject(stateAttributeName);
					if (!jsonFK.isNull("id")) {
    					UID newStateUid = Rest.translateFqn(E.STATE, jsonFK.getJsonString("id").getString());
						if (!oldStateUid.equals(newStateUid)) {
	    					eo.setFieldUid(SF.STATE_CHANGED_ACTION, newStateUid);
						}
					}
				}
    		}
    	}

    	if (eo.getPrimaryKey() == null && attrJson.containsKey(RValueObject.TEMPORARY_ID_JSON_KEY)) {
    		String gpoId = attrJson.getString(RValueObject.TEMPORARY_ID_JSON_KEY);
    		eo.setFieldValue(RValueObject.TEMPORARY_ID_FIELD, gpoId);
    	}

		if (eMeta.getDataLanguageDBTable() != null) {
			// set the current data language

			if (eo.getDataLanguageMap() == null) {
				eo.setDataLanguageMap(new DataLanguageMap());
			}

			final UID primaryDataLanguage = Rest.facade().getPrimaryDataLanguage();
			final UID sessionDataLanguage = getSessionContext().getDataLanguageUID();

			// set new values
			for (FieldMeta fieldMeta : eMeta.getFields()) {
				if (fieldMeta.isLocalized()) {
					final String fieldValue = eo.getFieldValue(fieldMeta.getUID(), String.class);
					DataLanguageUtils.setLocalizedValueAndValidateMap(
							eo.getDataLanguageMap(), (Long)eo.getPrimaryKey(), eMeta,
							sessionDataLanguage, primaryDataLanguage,
							sessionDataLanguage, fieldMeta.getUID(),
							CollectionUtils.transform(Rest.facade().getDataLanguages(), new Transformer<DataLanguageVO, UID>() {
								@Override
								public UID transform(DataLanguageVO i) {
									return i.getPrimaryKey();
								}
							}),
							fieldValue);
				}
			}
		}

    	return eo;
	}

	private static <PK> EntityObjectVO<PK> getEOFromJson(JsonObject json, NuclosMethod method, EntityMeta<?> eMeta, IDependentKey subBoDependentKey, String baseId) throws CommonBusinessException {
		boolean delete = false;
		boolean create = false;
		boolean update = false;

		if (method != null) {
			switch (method) {
			case INSERT:
				create = true;
				break;
			case UPDATE:
				update = true;
				break;
			case DELETE:
				delete = true;
				break;
			default:
				// do nothind
			}
		}

    	UID entity = eMeta.getUID();
    	EntityObjectVO<PK> eo = null;
    	if (create) {
    		if (json.containsKey(RValueObject.TEMPORARY_ID_JSON_KEY)) {
    			String tempId = json.getString(RValueObject.TEMPORARY_ID_JSON_KEY);
    			if (baseId == null) {
    				eo = (EntityObjectVO<PK>) Rest.facade().getTemporaryObject(tempId);
    			} else {
    				EntityObjectVO<PK> baseEo = (EntityObjectVO<PK>) Rest.facade().getTemporaryObject(baseId);
    				eo = findSubBoInTemporaryObject(baseEo, subBoDependentKey, tempId);
    			}
    		}
    		if (eo == null) {
    			eo = Rest.facade().getBoWithDefaultValues(entity);
    		}
    		if (eMeta.isStateModel()) {
    			eo.setFieldValue(SF.LOGICALDELETED, false);
    		}
    		eo.flagNew();
    	} else {

			Integer optimisticRecordVersion = null;

			if (json.containsKey("version")) {
				JsonNumber number = json.getJsonNumber("version");
				if (number != null) {
					optimisticRecordVersion = number.intValue();
				}
			}

    		if (json.containsKey("boId") && !json.isNull("boId")) {
	    		PK pk;
	    		if (eMeta.isUidEntity()) {
	    			pk = (PK)Rest.translateFqn(eMeta, json.getJsonString("boId").getString());
	    		} else {
	        		pk = (PK)(Long)json.getJsonNumber("boId").longValue();
	    		}
	    		eo = Rest.facade().getThin(entity, pk, false);
	    		if (eo == null) {
					throw new CommonFinderException("Entity=" + entity+ " Pk=" + pk);
				}

		        if (delete) {
		        	eo.flagRemove();
		        } else if (update) {
		        	eo.flagUpdate();
		        }
    		} else {
    			eo = new EntityObjectVO<PK>(entity);
    		}

			/*
			Set the expected version only if it was sent by the client.
			I.e. there is no version check, if the client did not provide the version.

			This is currently necessary for the matrix component, see NUCLOS-5867.
			*/
			if (optimisticRecordVersion != null) {
				eo.setVersion(optimisticRecordVersion);
			}

    	}

    	return eo;
	}

	private static <PK> EntityObjectVO<PK> findSubBoInTemporaryObject(EntityObjectVO<PK> temporaryObject, IDependentKey subDependentKey, String subTemporaryId) {
		for (IDependentKey dependentKey : temporaryObject.getDependents().getKeySet()) {
			Collection<EntityObjectVO<PK>> dataPk = temporaryObject.getDependents().getDataPk(dependentKey);
			for (EntityObjectVO<PK> data : dataPk) {
				String temporaryId = data.getFieldValue(RValueObject.TEMPORARY_ID_FIELD, String.class);
				if (dependentKey.equals(subDependentKey) && subTemporaryId.equals(temporaryId)) {
					return data;
				}
				EntityObjectVO<PK> deepSearch = findSubBoInTemporaryObject(data, subDependentKey, subTemporaryId);
				if (deepSearch != null) {
					return deepSearch;
				}
			}
		}
		return null;
	}

	protected RestLink buildRestLink(final String serviceId, final RestLinks.Verbs verbs, final Object... params) {
		final String href = getRestURI(serviceId, params);

		return RestLink.builder()
				.withHref(href)
				.withMethods(verbs.toArray())
				.build();
	}
}
