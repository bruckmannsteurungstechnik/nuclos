package org.nuclos.server.rest.services.rvo;

import java.util.List;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;
import org.nuclos.server.rest.ejb3.Rest;

public abstract class AbstractJsonRVO<PK> implements JsonRVO {
	
	private final EntityMeta<?> eMeta;
	
	public AbstractJsonRVO(EntityMeta<?> eMeta) {
		super();
		this.eMeta = eMeta;
	}

	protected EntityMeta<?> getEntityMeta() {
		return this.eMeta;
	}

	protected Object getPKForJson(PK pk) {
		if (pk instanceof UID) {
			return Rest.translateUid(eMeta, (UID)pk);
		}
		return pk;
	}
	
	protected static void addArray(JsonObjectBuilder json, String key, List<JsonRVO> lstJsonRvo) {
		if (lstJsonRvo != null) {
			JsonArrayBuilder array2 = Json.createArrayBuilder();
			for (JsonRVO jrvo : lstJsonRvo) {
				array2.add(jrvo.getJSONObjectBuilder());
			}
			
			json.add(key, array2);			
		}
	}
	
	protected static void addJsonObjectArray(JsonObjectBuilder json, String key, List<JsonObjectBuilder> lstJsonRvo) {
		if (lstJsonRvo != null) {
			JsonArrayBuilder array2 = Json.createArrayBuilder();
			for (JsonObjectBuilder jrvo : lstJsonRvo) {
				array2.add(jrvo);
			}
			
			json.add(key, array2);			
		}
	}
}
