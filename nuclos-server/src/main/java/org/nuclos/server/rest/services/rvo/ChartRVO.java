package org.nuclos.server.rest.services.rvo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.IChart;
import org.nuclos.common.querybuilder.NuclosDatasourceException;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common2.layoutml.WebChart;
import org.nuclos.server.rest.ejb3.Rest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class ChartRVO extends AbstractJsonRVO<UID> implements IChart {

	@Autowired
	JsonFactory jsonFactory;
	
	private final static Logger LOG = LoggerFactory.getLogger(ChartRVO.class);
	
	private final UID entityUID;
	private final UID foreignKeyField;
	private final String sLabel;
	
	private final Map<String, String> props;
	
	public ChartRVO(WebChart chart, String label0) {
		super(null);
		this.entityUID = chart.getEntity();
		this.foreignKeyField = chart.getForeignkeyfield();
		
		if (label0 == null || label0.isEmpty()) {
			label0 = Rest.getLabelFromMetaDataVO(Rest.getEntity(chart.getUID()));
		}
		this.sLabel = label0;

		this.props = new HashMap<>();
		Map<String, String> rawProps = chart.getProperties();
		for (String key : rawProps.keySet()) {
			String value = rawProps.get(key);
			if (value == null || value.isEmpty()) {
				continue;
			}
			if (UID.isStringifiedUID(value)) {
				// TODO
				EntityMeta<?> TODO = null;
				value = Rest.translateUid(TODO, UID.parseUID(value));
			}
			this.props.put(key, value);
		}		
		
	}
	
	@Override
	public JsonObjectBuilder getJSONObjectBuilder() {
		JsonObjectBuilder json = Json.createObjectBuilder();
		String sEntityUID = Rest.translateUid(E.ENTITY, entityUID);
		String sReffield = Rest.translateUid(E.ENTITYFIELD, foreignKeyField);
		
		json.add("uid", sEntityUID);
		json.add("name", sLabel);
		json.add("reffield", sReffield);
		json.add("type", props.get(PROPERTY_TYPE));
		json.add("readonly", true);
		JsonObjectBuilder jprops = Json.createObjectBuilder();
		for (String key : props.keySet()) {
			jprops.add(key, props.get(key));
		}
		
		json.add("properties", jprops);
		
		List<JsonObjectBuilder> lstJson = new ArrayList<>();
		
		try {
			List<DatasourceParameterVO> lstParams0 = Rest.facade().getParameterListForDataSource(entityUID);
			for (DatasourceParameterVO dpvo : lstParams0) {
				lstJson.add(jsonFactory.buildJsonObject(dpvo, null));
			}
		} catch (NuclosDatasourceException e) {
			LOG.warn("Exception during parameter determination: {}", e.getMessage());
			LOG.debug(e.getMessage(), e);
		}

		if (!lstJson.isEmpty()) {
			addJsonObjectArray(json, "fields", lstJson);			
		}
		
		return json;
	}
	
}