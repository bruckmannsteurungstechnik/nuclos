package org.nuclos.server.rest.services.rvo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import org.nuclos.common.DefaultComponentTypes;
import org.nuclos.common.E;
import org.nuclos.common.EntityContext;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.LafParameter;
import org.nuclos.common.NuclosImage;
import org.nuclos.common.NuclosPassword;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.lucene.LuceneSearchResult;
import org.nuclos.common.preferences.IPreferencesProvider;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.DatasourceParameterValuelistproviderVO;
import org.nuclos.common2.DateTime;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.layoutml.AbstractWebComponent;
import org.nuclos.common2.layoutml.IHasTableLayout;
import org.nuclos.common2.layoutml.IWebComponent;
import org.nuclos.common2.layoutml.IWebConstraints;
import org.nuclos.common2.layoutml.IWebContainer;
import org.nuclos.common2.layoutml.NuclosRuleAction;
import org.nuclos.common2.layoutml.NuclosRuleAction.NuclosRuleTransferLookedUpValueAction;
import org.nuclos.common2.layoutml.NuclosRuleEvent;
import org.nuclos.common2.layoutml.WebChart;
import org.nuclos.common2.layoutml.WebCollectableComponent;
import org.nuclos.common2.layoutml.WebComponentSizes;
import org.nuclos.common2.layoutml.WebComponentSizes.WDimension;
import org.nuclos.common2.layoutml.WebMatrix;
import org.nuclos.common2.layoutml.WebRoot;
import org.nuclos.common2.layoutml.WebSplitpaneConstraints;
import org.nuclos.common2.layoutml.WebStaticComponent;
import org.nuclos.common2.layoutml.WebSubform;
import org.nuclos.common2.layoutml.WebTableLayout;
import org.nuclos.common2.layoutml.WebTableLayoutConstraints;
import org.nuclos.common2.layoutml.WebValueListProvider;
import org.nuclos.schema.meta.entity.EntityFieldVlpConfig;
import org.nuclos.schema.meta.entity.EntityFieldVlpContext;
import org.nuclos.schema.meta.entity.EntityFieldVlpParam;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.StateCache;
import org.nuclos.server.nbo.NuclosBusinessObjectBuilder;
import org.nuclos.server.rest.ejb3.BoGenerationResult;
import org.nuclos.server.rest.ejb3.IRValueObject.JsonBuilderConfiguration;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.ejb3.Rest.LabelPattern;
import org.nuclos.server.rest.misc.BoMetaOverview;
import org.nuclos.server.rest.misc.IMenuEntry;
import org.nuclos.server.rest.misc.IWebContext;
import org.nuclos.server.rest.misc.IWebLayout;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.misc.RDataType;
import org.nuclos.server.rest.misc.ReferenceFieldMeta;
import org.nuclos.server.rest.misc.RestLinks;
import org.nuclos.server.rest.misc.RestLinks.Verbs;
import org.nuclos.server.rest.misc.TableLayoutConstants;
import org.nuclos.server.rest.misc.TaskOverview;
import org.nuclos.server.rest.services.helper.ColumnInfo;
import org.nuclos.server.rest.services.helper.TableViewLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component
/**
 * @deprecated Use Jackson mapping!
 */
@Deprecated
public class JsonFactory {

	@Autowired
	protected MetaProvider metaProvider;

	@Autowired
	protected ParameterProvider parameterProvider;

	@Autowired
	private IPreferencesProvider preferencesProvider;

	@Autowired
	private StateCache stateCache;

	public JsonFactory() {
	}
	
	public static JsonValue buildJsonValue(Object obj) {
		
		if (obj == null) {
			return JsonValue.NULL;
			
		} else if (obj instanceof Boolean) {
			return (Boolean)obj ? JsonValue.TRUE : JsonValue.FALSE;
			
		} else if (obj instanceof Number) {
			return new NuclosJsonNumber((Number)obj);
			
		} else if (obj instanceof String) {
			return new NuclosJsonString((String)obj);
			
		} else if (obj instanceof UID) {
			return new NuclosJsonString(((UID)obj).getString());
			
		} else if (obj instanceof InternalTimestamp) {
			return new NuclosJsonString(((InternalTimestamp)obj).toLocaleString());
		} else if (obj instanceof DateTime) {
			return new NuclosJsonNumber(((DateTime)obj).getTime());
		}
		
		return null;

	}

	/**
	 * TODO: This method violates all kinds of design principles!
	 * - Too big, too complex
	 * - Too many responsibilities, adds different kinds of data to lots of different entities
	 * TODO: Refactor and split appropriately!
	 *
	 * @deprecated Use Jackson mapping!
	 */
	@Deprecated
	public JsonObjectBuilder buildJsonObject(Object obj, IWebContext context) {
		
		JsonObjectBuilder json = Json.createObjectBuilder();
		
		if (obj instanceof IMenuEntry) {
			
			IMenuEntry entry = (IMenuEntry) obj;
			
			String boMetaId = Rest.translateUid(E.ENTITY, entry.getUID());
			String processMetaId = Rest.translateUid(E.PROCESS, entry.getProcessUid());
			
			json.add("boMetaId", boMetaId)
				.add("name", entry.getLabel());
			
			if (entry instanceof TaskOverview) {
				TaskOverview taskOverview = (TaskOverview) entry;
				if (taskOverview.getDynamicEntityField() != null) {
					// TODO: This "UID" contains really a field name...
//					FieldMeta<?> entityField = MetaProvider.getInstance().getEntityField(taskOverview.getDynamicEntityField());
					json.add("dynamicEntityFieldName", taskOverview.getDynamicEntityField().toString());
				}
				if (taskOverview.getTaskUID() != null) {
					json.add("taskMetaId", Rest.translateUid(E.TASKLIST, taskOverview.getTaskUID()));
				}
				if (taskOverview.getTaskEntity() != null) {
					json.add("taskEntity", Rest.translateUid(E.ENTITY, taskOverview.getTaskEntity()));
				}
				if (taskOverview.getSearchFilterId() != null) {
					// TODO: Should be "searchFilterId"
					json.add("searchfilter", Rest.translateUid(E.SEARCHFILTER, taskOverview.getSearchFilterId()));
				}
			} else if (entry instanceof BoMetaOverview) {
				if (!StringUtils.isNullOrEmpty(processMetaId)) {
					json.add("processMetaId", processMetaId);
				}
				json.add("createNew", entry.isCreateNew());
			}
			
			RestLinks restLinks = new RestLinks(json);
			restLinks.addLink("boMeta", Verbs.GET, boMetaId);
			
			if (entry.canReadBO()) {
				
				Verbs verbs = entry.canCreateBO() ? Verbs.GET_POST : Verbs.GET;
				
				restLinks.addLink("bos", verbs, boMetaId);
				if (entry.asMenuTree()) {
					restLinks.addLink("resourceicon", Verbs.GET, entry.getIcon());
				}
			}
						
			restLinks.buildJson(context);
						
		} else if (obj instanceof NuclosImage) {
			
			json.add("TODO", "implement links for image");
			
		} else if (obj instanceof FieldMeta) {
			
			FieldMeta<?> fm = (FieldMeta<?>) obj;
			
			boolean bAutoNumber = DefaultComponentTypes.AUTONUMBER.equals(fm.getDefaultComponentType());
			
			json.add("boAttrId", Rest.translateUid(E.ENTITYFIELD, fm.getUID()))

				// boAttrName must not be used for accessing attributes - use boAttrId instead
				.add("boAttrName", fm.getFieldName())

				// TODO should be renamed to "label"
				.add("name", Rest.getLabelFromMetaFieldDataVO(fm))

				.add("type", Rest.simpleDataType(fm.getDataType()))
				.add("readonly", 
						(!parameterProvider.isEnabled(ParameterProvider.MODIFIABLE_READONLY) && fm.isReadonly())
						|| !fm.isModifiable()

						// TODO: fm.isSystemField() ?!
						|| fm.isPrimaryKey()
						|| fm.getUID().equals(SF.CREATEDBY.getUID(fm.getEntity()))
						|| fm.getUID().equals(SF.CREATEDAT.getUID(fm.getEntity()))
						|| fm.getUID().equals(SF.CHANGEDBY.getUID(fm.getEntity()))
						|| fm.getUID().equals(SF.CHANGEDAT.getUID(fm.getEntity()))
						)
				.add("unique", fm.isUnique())
				.add("nullable", fm.isNullable())
				.add("hidden", fm.isHidden())
				.add("reference", fm.getForeignEntity() != null)
				.add("calculated", fm.isCalculated())
				.add("order", (fm.getOrder() != null ? "" + fm.getOrder() : ""));
			
			if (fm.getDefaultComponentType() != null) {
				json.add("defcomptype", fm.getDefaultComponentType());
			} else if (NuclosPassword.class.equals(fm.getJavaClass())) {
				json.add("defcomptype", "Password");
			}
			
			if (SF.isEOField(fm.getEntity(), fm.getUID())) {
				json.add("system", true);
			}
			
			if (fm.getForeignEntity() != null) {
				json.add("referencingBoMetaId", Rest.translateUid(E.ENTITY, fm.getForeignEntity()));
				json.add("referenceWithUid", metaProvider.getEntity(fm.getForeignEntity()).isUidEntity());
			}
			
			if (fm.getScale() != null) {
				json.add("scale", fm.getScale());
			}
			
			if (fm.getPrecision() != null) {
				json.add("precision", fm.getPrecision());
			}

			if (fm.getValuelistProvider() != null) {
				EntityFieldVlpConfig vlpConfig = new EntityFieldVlpConfig();
				final String sConfigJson = fm.getValuelistProviderConfig();
				final ObjectMapper mapper = new ObjectMapper();
				if (sConfigJson != null) {
					try {
						vlpConfig = mapper.readValue(sConfigJson, EntityFieldVlpConfig.class);
					} catch (Exception e) {
						throw new NuclosWebException(e, E.ENTITYFIELD.getUID());
					}
				}
				vlpConfig.setVlpId(Rest.translateUid(E.VALUELISTPROVIDER, fm.getValuelistProvider()));
				vlpConfig.getVlpContexts().stream().forEach(vlpContext -> {
					if (vlpContext.getEntityClassId() != null) {
						vlpContext.setEntityClassId(Rest.translateUid(E.ENTITY, UID.parseUID(vlpContext.getEntityClassId())));
					}
					vlpContext.getVlpParams().stream().forEach(vlpParam -> {
						if (vlpParam.getValueFromEntityFieldId() != null) {
							vlpParam.setValueFromEntityFieldId(Rest.translateUid(E.ENTITYFIELD, UID.parseUID(vlpParam.getValueFromEntityFieldId())));
						}
					});
				});
				json.add("vlpConfig", buildJsonObject(vlpConfig, context));
			}

		} else if (obj instanceof EntityMeta || obj instanceof ReferenceFieldMeta) {
			
			final EntityMeta<?> em;
			if (obj instanceof EntityMeta) {
				em = (EntityMeta<?>) obj;
				
			} else {
				ReferenceFieldMeta rfm = (ReferenceFieldMeta) obj;
				json.add("refAttrId", Rest.translateUid(E.ENTITYFIELD, rfm.getReffield().getUID()));
				
				UID detailEntity = rfm.getDepMeta().getDetailEntity();
				if (detailEntity != null) {
					json.add("detailBoMetaId", Rest.translateUid(E.ENTITY, detailEntity));
				}
				
				em = Rest.getEntity(rfm.getReffield().getEntity());				
			}
			
			String boMetaId = Rest.translateUid(E.ENTITY, em.getUID());

			String titlePattern = Rest.getLabelPatternFromMetaDataVO(em, LabelPattern.TITLE);
			String infoPattern = Rest.getLabelPatternFromMetaDataVO(em, LabelPattern.INFO);
			if (titlePattern.equals(infoPattern)) {
				infoPattern = "";
			}
			
			json.add("boMetaId", boMetaId)
				.add("name", Rest.getLabelFromMetaDataVO(em))
				.add("titlePattern", titlePattern)
				.add("infoPattern", infoPattern);

			if (!em.getEntityContexts().isEmpty()) {
				json.add("entityContexts", buildJsonArray(em.getEntityContexts().stream().collect(Collectors.toList()), context));
			}
			
			if (em.isStateModel()) {
				json.add("stateModel", true);
			}
			
			if (em.getDetailEntity() != null) {
				json.add("detailBoMetaId", Rest.translateUid(E.ENTITY, em.getDetailEntity()));
			}
			
			if (em.getMandatorLevel() != null) {
				json.add("mandatorLevelId", em.getMandatorLevel().getString());
			}
			
			UsageCriteria ucForNew = new UsageCriteria(em.getUID(), null, null, Rest.facade().getCustomUsage());
			IWebLayout webLayout = context.getWebLayout(ucForNew);
			
			RestLinks restLinks = new RestLinks(json);
			restLinks.addLinkHref("self", "boMeta", Verbs.GET, boMetaId);
			restLinks.addLinkHref("defaultGeneration", "boWithDefaultValues", Verbs.GET, boMetaId);
			
			if(webLayout.getLayoutInfo().getLayoutUID() != null) {
				restLinks.addLinkHref("defaultLayout", "weblayoutCalculated", Verbs.GET, Rest.translateUid(E.LAYOUT, webLayout.getLayoutInfo().getLayoutUID()));
			}
			
			
			IWebLayout searchWebLayout = context.getWebLayout(ucForNew, true);
			if (searchWebLayout == null) {
				searchWebLayout = webLayout;
			}
			if(searchWebLayout.getLayoutInfo().getLayoutUID() != null) {
				restLinks.addLinkHref("defaultSearchLayout", "weblayoutCalculated", Verbs.GET, Rest.translateUid(E.LAYOUT, searchWebLayout.getLayoutInfo().getLayoutUID()));
			}

			restLinks.buildJson(context);

			// add Look & Feel parameters
			JsonObjectBuilder lafParameter = Json.createObjectBuilder();
			if (hasLafParameter(lafParameter, em,
					LafParameter.nuclos_LAF_Empty_Result_List_If_No_Search_Condition,
					LafParameter.nuclos_LAF_Webclient_Popup,
					LafParameter.nuclos_LAF_Webclient_Subform_Show_Selection_Column)) {
				json.add("lafParameter", lafParameter);
			}
			
			JsonObjectBuilder jsonAttr = Json.createObjectBuilder();
			
			List<FieldMeta<?>> lstFields = new ArrayList<FieldMeta<?>>(em.getFields());
			Collections.sort(lstFields);
			for (FieldMeta<?> fm : lstFields) {
				String jsonPropertyName = NuclosBusinessObjectBuilder.getFieldNameForFqn(fm);
				jsonAttr.add(jsonPropertyName, buildJsonObject(fm, context));
			}
			
			json.add("attributes", jsonAttr);
			
		} else if (obj instanceof EntityContext) {
			EntityContext ec = (EntityContext) obj;
			json.add("dependentEntityClassId", Rest.translateUid(E.ENTITY, ec.getDependentEntity()));
			json.add("dependentEntityFieldId", Rest.translateUid(E.ENTITYFIELD, ec.getDependentEntityField()));

		} else if (obj instanceof ColumnInfo) {
			
			ColumnInfo info = (ColumnInfo)obj;
			
			json.add("uid", Rest.translateUid(E.ENTITYFIELD, info.getFieldMeta().getUID()))
				.add("fieldName", info.getFieldMeta().getFieldName()) // for debugging //Not needed anymore when using Full Qualified Name
				.add("displayName", info.getLabel())
				.add("width", info.getColumnWidth())
				.add("type", info.getType());
			
			if (info.getSortColumn() != null) {
				json.add("sort", info.getSortColumn());
				if(info.getSortColumnOrder() != null) {
					json.add("sortOrder", info.getSortColumnOrder());
				}
			}
			
		} else if (obj instanceof StateRVO) {
			
			StateRVO state = (StateRVO)obj;
			String stateId = Rest.translateUid(E.STATE, state.getStateVO().getId());
					
			json.add("nuclosStateId", stateId)
				.add("name", state.getStateVO().getStatename(context.getLocale()))
				.add("number", state.getStateVO().getNumeral())
				.add("nonstop", state.isNonStop());
			
			String buttonLabel = state.getStateVO().getButtonLabel(context.getLocale());
			if (buttonLabel != null) {
				json.add("buttonLabel", state.getStateVO().getButtonLabel(context.getLocale()));				
			}
			
			RestLinks links = new RestLinks(json);
			links.addLinkHref("change", "boStateChange", Verbs.GET, state.getBoMetaId(), state.getBoId(), stateId);
			links.addLink("stateIcon", Verbs.GET, stateId);
			links.buildJson(context);

		} else if (obj instanceof WebValueListProvider) {
			
			WebValueListProvider vlp = (WebValueListProvider)obj;
			
			JsonObjectBuilder json2 = Json.createObjectBuilder();
			for (String key : vlp.getParameters().keySet()) {
				if (vlp.getParameters().get(key) instanceof String) {
					json2.add(key, (String)vlp.getParameters().get(key));
				}
			}
			// TODO
			EntityMeta<?> TODO = null;
			json.add("type", vlp.getType())
				.add("value", Rest.translateUid(TODO, vlp.getValue()))			
				.add("params", json2);
			
		} else if (obj instanceof NuclosRuleEvent) {
			
			NuclosRuleEvent ruleEvent = (NuclosRuleEvent)obj;
			
			json.add("type", ruleEvent.getType())
				.add("sourcecomponent", Rest.translateUid(E.ENTITYFIELD, ruleEvent.getSourceComponent()));
			if (ruleEvent.getSubform() != null) {
				json.add("subform",  Rest.translateUid(E.ENTITY, ruleEvent.getSubform()));
				
			}
			
			json.add("actions", buildJsonArray(ruleEvent.getRuleActions(), context));
			
		} else if (obj instanceof NuclosRuleAction) {
			
			NuclosRuleAction ruleAction = (NuclosRuleAction)obj;
			
			json.add("type", ruleAction.getActionType());
			if (ruleAction.getEntity() != null) {
				json.add("entity", Rest.translateUid(E.ENTITY, ruleAction.getEntity()));
				
			}
			
			String targetUID = Rest.translateUid(E.ENTITYFIELD, ruleAction.getTargetComponent());
			if (targetUID != null) {
				json.add("targetcomponent", targetUID);				
			}
			
			String sParameter = ruleAction.getParameter();
			if (sParameter != null) {
				if (obj instanceof NuclosRuleTransferLookedUpValueAction) {
					json.add("parameter", Rest.translateUid(E.ENTITYFIELD, new UID(sParameter)));
				} else {
					json.add("parameter", sParameter);
				}
			}
			
		} else if (obj instanceof CollectableValueIdField) {
			
			CollectableValueIdField cvif = (CollectableValueIdField) obj;
			
			//NUCLOS-3173 - Any commit labeled NUCLOS-3171 before was actually for NUCLOS-3173
			Object value = cvif.getValueId();
			
			if (value instanceof UID) {
				FieldMeta<?> referencingField = context.getRefFieldMeta();
				if (referencingField != null) {
					EntityMeta<?> referencedEntity = Rest.getEntity(referencingField.getForeignEntity());
					value = Rest.translatePK(referencedEntity, cvif.getValueId());
					
				}
			}
			
			json.add("id", buildJsonValue(value))
				.add("name", buildJsonValue(cvif.getValue()));
			
		} else if (obj instanceof CollectableValueField) {
			
			CollectableValueField cvf = (CollectableValueField) obj;
			
			JsonValue value = buildJsonValue(cvf.getValue());
			json.add("name", value);
			
		} else if (obj instanceof WebTableLayout) {
			
			WebTableLayout webTableLayout = (WebTableLayout)obj;
			
			json.add("columns", buildJsonArray(webTableLayout.getColumns(), context))
				.add("rows", buildJsonArray(webTableLayout.getRows(), context));
			
		} else if (obj instanceof TableViewLayout) {
			
			TableViewLayout tableViewLayout = (TableViewLayout)obj;
			
			json.add("tableWidth", tableViewLayout.getTableWidth())
				.add("columns", buildJsonArray(tableViewLayout.getColumns(), context));
			
		} else if (obj instanceof WDimension) {
			
			WDimension dim = (WDimension)obj;
			
			json.add("width", dim.getWidth());
			json.add("height", dim.getHeight());
			
		} else if (obj instanceof WebComponentSizes) {
			
			WebComponentSizes sizes = (WebComponentSizes)obj;
			
			WDimension wd = sizes.getWDimension(IWebConstraints.MINIMUM_SIZE);
			if (wd != null) {
				json.add("minimun", buildJsonObject(wd, context));
			}
			
			wd = sizes.getWDimension(IWebConstraints.PREFERED_SIZE);
			if (wd != null) {
				json.add("preferred", buildJsonObject(wd, context));
			}
			
			wd = sizes.getWDimension(IWebConstraints.STRICT_SIZE);
			if (wd != null) {
				json.add("strict", buildJsonObject(wd, context));
			}
			
		} else if (obj instanceof WebSplitpaneConstraints) {
			
			WebSplitpaneConstraints constraints = (WebSplitpaneConstraints)obj;
			
			json.add("position", constraints.getPosition());

		} else if (obj instanceof WebTableLayoutConstraints) {
			
			WebTableLayoutConstraints constraints = (WebTableLayoutConstraints)obj;
			
			json.add("column", constraints.getColumn1());
			if (constraints.getColumn2() > constraints.getColumn1()) {
				json.add("colspan", constraints.getColumn2() - constraints.getColumn1() + 1);		
			}
				
			json.add("row", constraints.getRow1());
			if (constraints.getRow2() > constraints.getRow1()) {
				json.add("rowspan", constraints.getRow2() - constraints.getRow1() + 1);			
			}
			
			switch (constraints.gethAlign()) {
				case TableLayoutConstants.LEFT:
					json.add("align", "left");
					break;
				case TableLayoutConstants.CENTER:
					json.add("align", "center");
					break;
				case TableLayoutConstants.RIGHT:
					json.add("align", "right");
			}

			switch (constraints.getvAlign()) {
				case TableLayoutConstants.TOP:
					json.add("valign", "top");
					break;
				case TableLayoutConstants.CENTER:
					json.add("valign", "middle");
					break;
				case TableLayoutConstants.BOTTOM:
					json.add("valign", "bottom");
			}
			
			if (constraints.getSizes() != null) {
				json.add("size", buildJsonObject(constraints.getSizes(), context));
			}
			
		} else if (obj instanceof BoGenerationResult) {
			
			BoGenerationResult genResult = (BoGenerationResult) obj;
			EntityObjectVO<?> eo = genResult.getGeneratedEo();
			
			json.add("complete", eo.getPrimaryKey() != null);
			if (genResult.getBusinessError() != null) {
				json.add("businessError", genResult.getBusinessError());
			}
			json.add("showGenerated", genResult.getGeneratorAction().isShowObject());
			json.add("openInOverlay", genResult.getGeneratorAction().isOpenInOverlay());
			json.add("refreshSource", genResult.getGeneratorAction().isRefreshSrcObject());
			json.add("closeOnException", genResult.getGeneratorAction().isCloseOnException());


			try {
				UsageProperties up = new UsageProperties(Rest.getUsageCriteriaFromEO(eo), context, metaProvider, preferencesProvider, parameterProvider);
				@SuppressWarnings({ "rawtypes", "unchecked" })
				final UID sessionDataLanguageUID = RigidUtils.defaultIfNull(Rest.facade().getCurrentDataLanguage(), Rest.facade().getPrimaryDataLanguage());
				RValueObject<?> rvobj = new RValueObject(eo, JsonBuilderConfiguration.getFull(), up, null, sessionDataLanguageUID, this, metaProvider, stateCache);
				json.add("bo", rvobj.getJSONObjectBuilder());
			} catch (CommonBusinessException e) {
				throw new NuclosWebException(e, eo.getDalEntity());
			}
			
		} else if (obj instanceof DatasourceParameterVO) {
			
			DatasourceParameterVO dpVO = (DatasourceParameterVO) obj;
			
			RDataType rdatatype = new RDataType(dpVO.getDatatype(), 2);
			
			json.add("parameter", dpVO.getParameter())
				.add("name", dpVO.getLabel())
				.add("type", rdatatype.getExportType())
				.add("nullable", !dpVO.getMandatory());
			DatasourceParameterValuelistproviderVO vlp = dpVO.getValueListProvider();
			
			if (vlp != null) {
				WebValueListProvider provider = new WebValueListProvider(vlp.getType().name(), vlp.getValue());
				for (String key : vlp.getParameters().keySet()) {
					provider.addParameter(key, vlp.getParameters().get(key));
				}
				json.add("vlp", buildJsonObject(provider, null));
			}
			
		} else if (obj instanceof LuceneSearchResult) {
			
			LuceneSearchResult lsr = (LuceneSearchResult)obj;

			if (lsr.getPk() instanceof Long) {
				json.add("pk", (Long)lsr.getPk());
			} else if (lsr.getPk() instanceof UID) {
				json.add("pk", ((UID)lsr.getPk()).getString());
			}

			json.add("uid", Rest.translateUid(E.ENTITY, lsr.getEntity()))
				// TODO add result data as JSON object instead of HTML
				.add("text", lsr.getFormattedResultText(MetaProvider.getInstance(), SpringLocaleDelegate.getInstance()));
			
		} else if (obj instanceof EntityFieldVlpConfig) {
			EntityFieldVlpConfig vlpConfig = (EntityFieldVlpConfig) obj;
			json.add("vlpId", vlpConfig.getVlpId());
			json.add("valueField", vlpConfig.getValueField());
			if (vlpConfig.getIdField() != null) {
				json.add("idField", vlpConfig.getIdField());
			}
			if (!vlpConfig.getVlpContexts().isEmpty()) {
				json.add("vlpContexts", buildJsonArray(vlpConfig.getVlpContexts(), context));
			}

		} else if (obj instanceof EntityFieldVlpContext) {
			EntityFieldVlpContext vlpContext = (EntityFieldVlpContext) obj;
			if (vlpContext.getEntityClassId() != null) {
				json.add("entityClassId", vlpContext.getEntityClassId());
				// We want to use the automatically generated typescript class
				// json.add("boMetaId", vlpContext.getEntityId());
			}
			json.add("search", vlpContext.isSearch());
			json.add("input", vlpContext.isInput());
			if (!vlpContext.getVlpParams().isEmpty()) {
				json.add("vlpParams", buildJsonArray(vlpContext.getVlpParams(), context));
			}

		} else if (obj instanceof EntityFieldVlpParam) {
			EntityFieldVlpParam vlpParam = (EntityFieldVlpParam) obj;
			json.add("name", vlpParam.getName());
			if (vlpParam.getValue() != null) {
				json.add("value", vlpParam.getValue());
			}
			if (vlpParam.getValueFromEntityFieldId() != null) {
				json.add("valueFromEntityFieldId", vlpParam.getValueFromEntityFieldId());
				// We want to use the automatically generated typescript class
				// json.add("valueFromBoAttrId", vlpParam.getValueFromEntityFieldId());
			}

		}
		
		return json;
		
	}
	
	public static JsonArrayBuilder buildJsonArrayOfString(String[] array) {
		JsonArrayBuilder json = Json.createArrayBuilder();
		
		for (String s : array) {
			json.add(buildJsonValue(s));
			
		}
		return json;
	}
	
	public JsonArrayBuilder buildJsonArray(List<?> lst, IWebContext context) {

		JsonArrayBuilder json = Json.createArrayBuilder();
		
		for (Object o : lst) {
			JsonValue jsonValue = buildJsonValue(o);
			if (jsonValue != null) {
				json.add(jsonValue);
				
			} else {
				json.add(buildJsonObject(o, context));
				
			}
			
		}
		return json;
	}
	
	public JsonObjectBuilder buildJsonForWebComponents(IWebComponent webComponent, IWebLayout webLayout) {
		JsonObjectBuilder json = Json.createObjectBuilder();
		
		if (webComponent instanceof IWebContainer) {
			
			IWebContainer webContainer = (IWebContainer) webComponent;
			return getJSONObjectBuilderForContainer(webContainer, webLayout);
			
		} else if (webComponent instanceof WebCollectableComponent) {
			
			WebCollectableComponent webCollectableComponent = (WebCollectableComponent) webComponent;
			
			if (webCollectableComponent.isJustLabel()) {
				if (webCollectableComponent.isVisible()) {
					FieldMeta<?> fieldMeta = Rest.getEntityField(webCollectableComponent.getUID());
					
					json.add("type", "LABEL")
						.add("text", Rest.getLabelFromMetaFieldDataVO(fieldMeta))
						.add("name", fieldMeta.getFieldName())
						.add("cstr", buildJsonObject(webCollectableComponent.getConstraints(), null));					
				}

			} else {
				LayoutAdditions layoutAdditions = webLayout.getLayoutAdditionsForWebComponenet(webCollectableComponent);
				if (layoutAdditions != null) {
					return getJSONObjectBuilderForLayoutAdditions(layoutAdditions);
				}
			}
			
			if (webCollectableComponent.getBackground() != null) {
				json.add("background", webCollectableComponent.getBackground());
			}
			
		} else if (webComponent instanceof WebStaticComponent) {
			
			WebStaticComponent comp = (WebStaticComponent) webComponent;

			Map<String, String> mpProperties = comp.getProperties();
			for (String key : mpProperties.keySet()) {
				String value = mpProperties.get(key);
				
				if (key != null && value != null) {
					if (UID.isStringifiedUID(value)) {
						EntityMeta<?> emBase = null;
						if ("targetState".equals(key)) {
							emBase = E.STATE;
						} else if ("icon".equals(key)) {
							emBase = E.RESOURCE;
						} else if ("generatortoexecute".equals(key)) {
							emBase = E.GENERATION;
						}
						value = Rest.translateUid(emBase, UID.parseUID(value));
					} 
					
					json.add(key, value);
				}
			}
			
			json.add("cstr", buildJsonObject(comp.getConstraints(), null));
			
			if (comp.isButton() && (!comp.isEnabled() || !comp.isActionSupportedAndCompleteForWeb())) {
				json.add("disabled", true);
			}

			if (comp.getTabIndex() != null) {
				json.add("tabindex", comp.getTabIndex());
			}
			
		} else if (webComponent instanceof WebSubform) {
			
			return getJSONObjectBuilderForSubform((WebSubform)webComponent, webLayout);
			
		} else if (webComponent instanceof WebChart) {
			WebChart chart = (WebChart)webComponent;
			
			return chart.isVisible() ? new ChartRVO(chart, null).getJSONObjectBuilder() : null;
			
		} else if (webComponent instanceof WebMatrix) {
			WebMatrix webMatrix = (WebMatrix)webComponent;
			
			json.add("type", "MATRIX")
				.add("name", webMatrix.getName());
			
			//TODO: Handle all those UIDs
			json.add("reffield",  Rest.translateUid(E.ENTITYFIELD, webMatrix.getEntityFieldMatrixParent())) // reffield wird wegen subform handling benötigt, ist das ok so?
				.add("entity_matrix", Rest.translateUid(E.ENTITY, webMatrix.getEntityMatrix()));

			json.add("entity_x", Rest.translateUid(E.ENTITY, webMatrix.getEntityX()))
				.add("entity_y", Rest.translateUid(E.ENTITY, webMatrix.getEntityY()));

			// TODO
			EntityMeta<?> TODO = null;
			
			json.add("entity_matrix_value_type", Rest.translateUid(TODO, webMatrix.getEntityMatrixValueType()))
				.add("cell_input_type", webMatrix.getCellInputType() != null ? webMatrix.getCellInputType() : "checkicon")
				.add("entity_field_matrix_x_refField",  Rest.translateUid(E.ENTITYFIELD, webMatrix.getEntityFieldMatrixXRefField()))
				.add("entity_matrix_value_field",  Rest.translateUid(E.ENTITYFIELD, webMatrix.getEntityMatrixValueField()))
				.add("entity_field_matrix_parent",  Rest.translateUid(E.ENTITYFIELD, webMatrix.getEntityFieldMatrixParent()));
			
			json.add("entity_y_parent_field",  Rest.translateUid(E.ENTITYFIELD, webMatrix.getEntityYParentField()))
				.add("entity_matrix_number_state",  webMatrix.getEntityMatrixNumberState() != null ? webMatrix.getEntityMatrixNumberState() : 3)
				.add("entity_x_sorting_fields", stringValue(webMatrix.getEntityXSortingFields()))
				.add("entity_y_sorting_fields", stringValue(webMatrix.getEntityYSortingFields()));  

			json.add("entity_x_header", stringValue(webMatrix.getEntityXHeader()))
				.add("entity_y_header", stringValue(webMatrix.getEntityYHeader())); 
			
			json.add("entity_matrix_reference_field", Rest.translateUid(E.ENTITYFIELD, webMatrix.getEntityMatrixReferenceField()))
				.add("entity_x_vlp_id", Rest.translateUid(E.VALUELISTPROVIDER, webMatrix.getEntityXVlpId()))
				.add("entity_x_vlp_idfieldname", stringValue(webMatrix.getEntityXVlpIdfieldname()))
				.add("entity_x_vlp_fieldname", stringValue(webMatrix.getEntityXVlpFieldname()))
				.add("entity_x_vlp_reference_param_name", stringValue(webMatrix.getEntityXVlpReferenceParamName()));

			
			json.add("cstr", buildJsonObject(webMatrix.getConstraints(), null/*context*/));

			String translatedWebMatrixEntityUID = Rest.translateUid(E.ENTITY, webMatrix.getEntity());
			if (translatedWebMatrixEntityUID != null) {
				json.add("entity", translatedWebMatrixEntityUID);  
			}

		}
		
		return json;
	}
	
	public JsonObjectBuilder getJSONObjectBuilderForContainer(IWebContainer container, IWebLayout webLayout) {
		JsonObjectBuilder json = Json.createObjectBuilder();
		
		if (container instanceof WebRoot) {
			json.add("layoutId", Rest.translateUid(E.LAYOUT, ((WebRoot)container).getLayoutUID()));
		}
		
		String title = container.getTitle();
		if (title != null && !title.isEmpty()) {
			json.add("title", title);			
		}
		
		json.add("type", container.getType());
		
		if (container.getConstraints() != null) {
			json.add("cstr", buildJsonObject(container.getConstraints(), null));
			
		}
		
		if (container instanceof IHasTableLayout) {
			IHasTableLayout htl = (IHasTableLayout)container;
			
			if (htl.getBorderType() != null) {
				json.add("border", htl.getBorderType());
			}
			
			if (htl.getBackground() != null) {
				json.add("background", htl.getBackground());
			}
			
			WebTableLayout layout = htl.getWebTableLayout();
			if (layout != null) {
				json.add("layout", buildJsonObject(layout, null));
			}
		}

		JsonArrayBuilder array2 = Json.createArrayBuilder();
		
		for (AbstractWebComponent component : container.getComponents()) {
			final JsonObjectBuilder jsonObjectBuilder = buildJsonForWebComponents(component, webLayout);
						
			if (jsonObjectBuilder != null) {
				array2.add(jsonObjectBuilder);
			}
		}
		json.add("components", array2);
		
		Map<String, String> props = container.getProperties();
		if (props != null && !props.isEmpty()) {
			boolean addedOnce = false;
			
			JsonObjectBuilder jprops = Json.createObjectBuilder();
			for (String key : props.keySet()) {
				if (props.get(key) != null) {
					jprops.add(key, props.get(key));
					addedOnce = true;
				}
			}
			
			if (addedOnce) {
				json.add("properties", jprops);				
			}
		}
		
		return json;
	}
	
	public JsonObjectBuilder getJSONObjectBuilderForLayoutAdditions(LayoutAdditions layoutAdditions) {
		
		JsonObjectBuilder json = Json.createObjectBuilder();
		
		json.add("uid", Rest.translateUid(E.ENTITYFIELD, layoutAdditions.getUID()));
		json.add("fieldname", layoutAdditions.getFieldName());
		json.add("name", layoutAdditions.getLabel());
		json.add("readonly", layoutAdditions.isDisabled());
		
		if (layoutAdditions.isInvisible()) {
			json.add("invisible", true);
		}
		
		if (layoutAdditions.isMemo()) {
			json.add("memo", layoutAdditions.isMemo());
		}
		
		if (layoutAdditions.getBackground() != null) {
			json.add("background", layoutAdditions.getBackground());
		}
		
		IWebConstraints constraints = layoutAdditions.getConstraints();
		if (constraints == null) {
			json.add("cstr", JsonValue.NULL);
			
		} else {
			json.add("cstr", buildJsonObject(constraints, null));
			
		}
		
		ExtendedLayoutProperties properties = (ExtendedLayoutProperties)layoutAdditions.getExtendedLayoutProperties();
		if (properties != null && properties.size() != 0) {
			JsonObjectBuilder jsonProperties = Json.createObjectBuilder();
			for(String key : properties.keySet()) {
				jsonProperties.add(key, properties.get(key));
			}
			json.add("properties", jsonProperties);
		}
		
		Integer tabIndex = layoutAdditions.getTabIndex();
		if (tabIndex != null) {
			json.add("tabindex", tabIndex);
		}
		
		List<NuclosRuleEvent> ruleEvents = layoutAdditions.getRuleEvents();
		if (ruleEvents != null) {
			json.add("mlrules", buildJsonArray(ruleEvents, null));
		}
		
		if (layoutAdditions.getValueListProvider() != null) {
			json.add("vlp", buildJsonObject(layoutAdditions.getValueListProvider(), null));
		}
		
		return json;
	}
	
	private static final String SUBFORM = "SUBFORM";
	
	public JsonObjectBuilder getJSONObjectBuilderForSubform(WebSubform subform, IWebLayout webLayout) {
		if (!subform.isVisible()) {
			return null;
		}
		
		//Sub-Subforms not supported
		if (subform.getParentsubform() != null) {
			return null;
		}
		
		EntityMeta<?> subformMeta = Rest.getEntity(subform.getUID());
		
		boolean readonly = !subform.isEnabled();
		if (!subformMeta.isEditable()) {
			readonly = true;
		}

		if (subformMeta.isDynamic()) {
			readonly = true;
		}
		
		String label0 = subform.getContainer().getTitle();
		if (label0 == null || label0.isEmpty()) {
			label0 = Rest.getLabelFromMetaDataVO(subformMeta);
		}
		
		String subFormFqn = Rest.translateUid(E.ENTITY, subform.getEntity());
		String forKeyFqn = Rest.translateUid(E.ENTITYFIELD, subform.getForeignkeyfield());

		JsonObjectBuilder json = Json.createObjectBuilder();
		
		json.add("uid", subFormFqn);
		json.add("name", label0);
		json.add("reffield", forKeyFqn);
		json.add("type", SUBFORM);
		json.add("readonly", readonly);
		json.add("cstr", buildJsonObject(subform.getConstraints(), null));
		
		if (subform.isNotClonable()) {
			json.add("notclonable", true);
		}
		
		if (subform.isDynamicCellHeights()) {
			json.add("dynamiccellheights", true);
		}
		
		if (subform.isIgnoreSubLayout()) {
			json.add("ignoresublayout", true);
		}
		
		Map<String, String> mpProperties = subform.getProperties();
		for (String key : mpProperties.keySet()) {
			String value = mpProperties.get(key);
			if (key != null && value != null) {
				json.add(key, value);
			}
		}

		List<LayoutAdditions> lstColumns = webLayout.getComponents(subform.getUID(), subform.getForeignkeyfield(), true);

		JsonArrayBuilder jsonarray = Json.createArrayBuilder();
		for (LayoutAdditions jrvo : lstColumns) {
			jsonarray.add(getJSONObjectBuilderForLayoutAdditions(jrvo));
		}
		
		json.add("columns", jsonarray);
		
		return json;
	}

	private static boolean hasLafParameter(JsonObjectBuilder jsonObjectBuilder, EntityMeta<?> em, LafParameter...parameters) {
		boolean result = false;
		for (LafParameter parameter : parameters) {
			if (addLafParameter(jsonObjectBuilder, em, parameter)) {
				result = true;
			}
		}
		return result;
	}

	private static boolean addLafParameter(JsonObjectBuilder jsonObjectBuilder, EntityMeta<?> em, LafParameter parameter) {
		ServerParameterProvider paramProv = ServerParameterProvider.getInstance();
		IMetaProvider metaProv = MetaProvider.getInstance();
		Object parameterValue = parameter.getValue(em.getUID(), null, paramProv, metaProv);
		if (parameterValue != null) {
			if (parameterValue instanceof String) {
				jsonObjectBuilder.add(parameter.getName(), (String) parameterValue);
			} else if (parameterValue instanceof Boolean) {
				jsonObjectBuilder.add(parameter.getName(), (Boolean) parameterValue);
			} else {
				throw new IllegalArgumentException("Laf-parameter of type " + parameterValue.getClass().getCanonicalName() + " not supported yet");
			}
			return true;
		}
		return false;
	}

	
	private static String stringValue(Object value) {
		return value != null ? value.toString() : "";
	}
	
}
