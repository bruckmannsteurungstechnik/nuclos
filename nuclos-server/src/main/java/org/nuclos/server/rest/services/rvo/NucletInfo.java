//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.services.rvo;

import java.io.Serializable;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.rest.ejb3.Rest;

/**
 * Wrapper class for Nuclet EOs to access just some basic infos.
 *
 */
public class NucletInfo implements Serializable {
	private final EntityObjectVO<UID> wrappedEO;

	public NucletInfo(final EntityObjectVO<UID> wrappedEO) {
		this.wrappedEO = wrappedEO;
	}

	public String getId() {
		return Rest.translateUid(E.NUCLET, wrappedEO.getPrimaryKey());
	}

	public String getName() {
		return wrappedEO.getFieldValue(E.NUCLET.name);
	}

	public String getPackage() {
		return wrappedEO.getFieldValue(E.NUCLET.packagefield);
	}

	public String getLocalIdentifier() {
		return wrappedEO.getFieldValue(E.NUCLET.localidentifier);
	}

	public Boolean getSource() {
		return wrappedEO.getFieldValue(E.NUCLET.source);
	}

	public String getDescription() {
		return wrappedEO.getFieldValue(E.NUCLET.description);
	}

	public String getVersion() {
		return String.valueOf(wrappedEO.getFieldValue(E.NUCLET.nucletVersion));
	}

}
