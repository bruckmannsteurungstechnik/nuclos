package org.nuclos.server.rest.services.rvo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.preferences.IPreferencesProvider;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.StateCache;
import org.nuclos.server.rest.ejb3.IRValueObject.JsonBuilderConfiguration;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.IWebContext;
import org.nuclos.server.rest.services.rvo.RValueObject.BoLinksFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class ResultRVO<PK> extends AbstractJsonRVO<PK> {

	@Autowired
	private JsonFactory jsonFactory;

	@Autowired
	private MetaProvider metaProvider;

	@Autowired
	private ParameterProvider parameterProvider;

	@Autowired
	private IPreferencesProvider preferencesProvider;

	@Autowired
	private StateCache stateCache;

	private Boolean all;
	private Integer total;
	private String boType;
	private List<JsonRVO> bos;

	public ResultRVO() {
		super(null);
	}

	public ResultRVO fill(Collection<EntityObjectVO<PK>> collEo,
						  Boolean all,
						  Integer total,
						  JsonBuilderConfiguration jsonConfig,
						  IWebContext webContext,
						  UID sessionDataLanguageUID) throws CommonBusinessException {
		return this.fill(collEo,
				all,
				total,
				jsonConfig,
				webContext,
				sessionDataLanguageUID,
				null,
				null);
	}

	public ResultRVO fill(Collection<EntityObjectVO<PK>> collEo,
					 Boolean all,
					 Integer total,
					 JsonBuilderConfiguration jsonConfig,
					 IWebContext webContext,
					 UID sessionDataLanguageUID,
					 String boType,
					 BoLinksFactory boLinksFactory) throws CommonBusinessException {
		this.total = total;
		this.all = all;
		this.boType = boType;
		this.bos = new ArrayList<>();
		Map<UsageCriteria, UsageProperties> mpUsageProperties = new HashMap<UsageCriteria, UsageProperties>();

		for (EntityObjectVO<PK> eo : collEo) {
			UsageCriteria uc = Rest.getUsageCriteriaFromEO(eo);
			if (!mpUsageProperties.containsKey(uc)) {
				mpUsageProperties.put(uc, new UsageProperties(uc, webContext, metaProvider, preferencesProvider, parameterProvider));
			}
			bos.add(new RValueObject<>(eo, jsonConfig, mpUsageProperties.get(uc), boLinksFactory, sessionDataLanguageUID, jsonFactory, metaProvider, stateCache));
		}
		return this;
	}

	@Override
	public JsonObjectBuilder getJSONObjectBuilder() {
		JsonObjectBuilder json = Json.createObjectBuilder();
		if (all != null) {
			json.add("all", all);
		}
		if (total != null) {
			json.add("total", total);
		}
		if (boType != null) {
			json.add("bo_type", boType);
		}

		addArray(json, "bos", bos);
		return json;
	}

	private static <PK> Collection<EntityObjectVO<PK>> collEo(EntityObjectVO<PK> eo) {
		Collection<EntityObjectVO<PK>> lstEO = new ArrayList<EntityObjectVO<PK>>();
		lstEO.add(eo);
		return lstEO;
	}
}
