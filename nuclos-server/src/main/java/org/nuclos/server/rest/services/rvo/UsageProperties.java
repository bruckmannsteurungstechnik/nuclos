package org.nuclos.server.rest.services.rvo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.preferences.IPreferencesProvider;
import org.nuclos.common.preferences.PerspectivePreferences;
import org.nuclos.common.security.EntityPermission;
import org.nuclos.common.security.SubformPermission;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.layoutml.AbstractWebDependents;
import org.nuclos.common2.layoutml.WebStaticComponent;
import org.nuclos.common2.layoutml.WebSubform;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.nbo.NuclosBusinessObjectBuilder;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.IWebContext;
import org.nuclos.server.rest.misc.IWebLayout;
import org.nuclos.server.rest.misc.RestLinks;
import org.nuclos.server.statemodel.valueobject.StateVO;

/**
 * Created by Oliver Brausch on 10.01.19.
 */
//Note: UsageProperties only contains Properties that are dependent of the Usage Criteria, not of the Data-Row itself, or the primary key.

public class UsageProperties {

	final UsageCriteria usage;
	final IWebContext webContext;
	final String sTranslatedEntity;

	private final List<StateVO> lstStates;

	IWebLayout webLayout;
	Map<UID, AbstractWebDependents> visibleSubBos = new HashMap<>();

	final boolean bHasStateModel;
	final boolean isInsertAllowed;
	final boolean isUpdateAllowed;
	final boolean isDeleteAllowed;
	private final MetaProvider metaProvider;
	private final IPreferencesProvider preferencesProvider;
	private final ParameterProvider parameterProvider;

	public UsageProperties(UsageCriteria usage, IWebContext webContext, MetaProvider metaProvider, final IPreferencesProvider preferencesProvider, final ParameterProvider parameterProvider) throws CommonBusinessException {
		this.metaProvider = metaProvider;
		this.parameterProvider = parameterProvider;
		this.preferencesProvider = preferencesProvider;

		this.usage = usage;
		this.webContext = webContext;
		this.sTranslatedEntity = Rest.translateUid(E.ENTITY, usage.getEntityUID());

		this.webLayout = webContext.getWebLayout(usage);
		webLayout.getVisibleSubLists().entrySet().forEach(sublistEntry -> this.visibleSubBos.put(sublistEntry.getKey(), sublistEntry.getValue()));
		List<PerspectivePreferences> lstPerspectivePreferences = preferencesProvider.getPerspectivePreferences("nuclos", usage.getEntityUID());

		for(PerspectivePreferences preferences : lstPerspectivePreferences) {
			webContext.getWebLayout(preferences.getLayoutId()).getVisibleSubLists().entrySet().forEach(sublistEntry -> this.visibleSubBos.put(sublistEntry.getKey(), sublistEntry.getValue()));
		}

		this.bHasStateModel = Rest.hasStateModel(usage.getEntityUID());

		EntityPermission entityPermission = Rest.getEntityPermission(usage.getEntityUID());
		this.isInsertAllowed = entityPermission.isInsertAllowed() && webLayout != null && webLayout.getLayoutInfo().getLayoutUID() != null;
		this.isDeleteAllowed = entityPermission.isDeleteAllowed();
		this.isUpdateAllowed = entityPermission.isUpdateAllowed();

		if (bHasStateModel) {
			this.lstStates = Rest.getSubsequentStates(usage);
		} else {
			this.lstStates = new ArrayList<>();
		}
	}

	private final Map<String, RValueObject.AttributesAndRestrictions> mpAttributesAndRestrictions = new HashMap<>();

	RValueObject.AttributesAndRestrictions getAttributesAndRestrictions(String queryAttributes) {
		if (!mpAttributesAndRestrictions.containsKey(queryAttributes)) {
			Collection<FieldMeta<?>> fields = Rest.getEntity(usage.getEntityUID()).getFields();

			List<FieldMeta<?>> lstAttributesReadAllowed = new ArrayList<>();

			for (FieldMeta<?> fm : fields) {
				// NUCLOS-5242 Check all fields for permission (any but nuclosRowColor)
				if (Rest.isReadAllowedForField(fm, usage) || fm.isNuclosRowColor()) {
					lstAttributesReadAllowed.add(fm);
				}
			}

			JsonObjectBuilder restrictions = Json.createObjectBuilder();
			boolean hasRestrictions = false;

			for (FieldMeta<?> fm : fields) {
				String jsonPropertyName = NuclosBusinessObjectBuilder.getFieldNameForFqn(fm);
				String srestrict = null;

				if (!lstAttributesReadAllowed.contains(fm)) {
					srestrict = "hidden";

				} else {
					boolean bWrite = isUpdateAllowed && Rest.isWriteAllowedForField(fm, usage) && fm.isModifiable() && (parameterProvider.isEnabled(ParameterProvider.MODIFIABLE_READONLY) || !fm.isReadonly());

					if (!bWrite) {
						if (SF.STATE.checkField(jsonPropertyName)) {
							if (lstStates.isEmpty()) {
								srestrict = "readonly";
							}

						} else {
							srestrict = "readonly";
						}
					}

				}

				if (srestrict != null) {
					restrictions.add(jsonPropertyName, srestrict);
					hasRestrictions = true;
				}

			}

			if (bHasStateModel) {
				List<WebStaticComponent> lstButtons = webLayout.getEnabledButtons();

				for (WebStaticComponent button : lstButtons) {

					if (!Rest.isButtonEnabled(button, usage, webContext.getUser())) {
						restrictions.add(Rest.translateUid(E.ACTION, button.getUID()), "disabled");
					}
				}
			}

			mpAttributesAndRestrictions.put(queryAttributes,
					new RValueObject.AttributesAndRestrictions(lstAttributesReadAllowed, hasRestrictions ? restrictions.build() : null));
		}

		return mpAttributesAndRestrictions.get(queryAttributes);
	}

	private List<StateVO> lstStatesSorted;

	List<StateVO> getStatesSorted() {
		if (bHasStateModel) {

			if (lstStatesSorted != null) {
				return lstStatesSorted;
			}

			lstStatesSorted = lstStates;

			lstStatesSorted.sort((o1, o2) -> o1.getNumeral().compareTo(o2.getNumeral()));
		}

		return lstStatesSorted;
	}

	private List<GeneratorActionRVO> lstGenerationsSorted;

	List<GeneratorActionRVO> getGenerationsSorted() {
		if (lstGenerationsSorted != null) {
			return lstGenerationsSorted;
		}

		lstGenerationsSorted = new ArrayList<>();

		List<GeneratorActionVO> lstGenerations = Rest.facade().getGenerations(usage);

		if (lstGenerations != null) {

			lstGenerations.sort((o1, o2) -> StringUtils.compareIgnoreCase(StringUtils.defaultIfNull(o1.getLabel(), o1.getName()),
					StringUtils.defaultIfNull(o2.getLabel(), o2.getName())));

			for (GeneratorActionVO generation : lstGenerations) {
				lstGenerationsSorted.add(new GeneratorActionRVO(generation, sTranslatedEntity));
			}

		}

		return lstGenerationsSorted;
	}

	JsonObjectBuilder getSubBos(Object pk, Boolean bWriteAllowedForPk) {
		JsonObjectBuilder subBos = Json.createObjectBuilder();
		boolean hasSubBos = false;

		for (UID refkey : visibleSubBos.keySet()) {
			AbstractWebDependents webSub = visibleSubBos.get(refkey);

			UID subform = Rest.getEntityField(refkey).getEntity();
			String sRefKey = Rest.translateUid(E.ENTITYFIELD, refkey);

			if (Rest.isReadAllowedForSubform(subform, usage)) {
				hasSubBos = true;
				if (bWriteAllowedForPk == null) {
					bWriteAllowedForPk = isWriteAllowed();
				}
				JsonObjectBuilder subBo = getSubBo(sRefKey, sTranslatedEntity, webSub.getServiceIdentifier(), pk, webContext, subform, usage, bWriteAllowedForPk);
				subBos.add(sRefKey, subBo);
			}
		}

		for (FieldMeta<?> subRefMeta : metaProvider.getAllReferencingFields(usage.getEntityUID())) {
			EntityMeta<Object> subMeta = metaProvider.getEntity(subRefMeta.getEntity());
			if (subMeta.isChart()) {
				// chart datasources are not defined in layout
				EntityMeta<?> parentMeta = metaProvider.getEntity(usage.getEntityUID());
				boolean add = false;
				if (parentMeta.isStateModel()) {
					UID mandator = Rest.facade().getCurrentMandatorUID();
					Map<UID, SubformPermission> subPermissions = SecurityCache.getInstance().getSubForm(webContext.getUser(), subMeta.getUID(), mandator);
					SubformPermission subPermission = subPermissions.get(usage.getStatusUID());
					if (subPermission != null && subPermission.includesReading()) {
						add = true;
					}
				} else {
					add = true;
				}
				if (add) {
					hasSubBos = true;
					String sRefKey = Rest.translateUid(E.ENTITYFIELD, subRefMeta.getUID());
					if (bWriteAllowedForPk == null) {
						bWriteAllowedForPk = isWriteAllowed();
					}
					JsonObjectBuilder subBo = getSubBo(sRefKey, sTranslatedEntity, WebSubform.SERVICE_IDENTIFIER, pk, webContext, subMeta.getUID(), usage, bWriteAllowedForPk);
					subBos.add(sRefKey, subBo);
				}
			}
		}

		return hasSubBos ? subBos : null;
	}

	boolean isWriteAllowed() {
		if (!isUpdateAllowed) {
			return false;
		}

		if (!bHasStateModel) {
			return true;
		}

		return Rest.facade().isWriteAllowedForGO(usage.getEntityUID());
	}

	boolean isDeleteAllowed() {
		if (!isDeleteAllowed) {
			return false;
		}

		if (!bHasStateModel) {
			return true;
		}

		return Rest.facade().isDeleteAllowedForGO(usage.getEntityUID(), true);
	}

	public IWebLayout getWebLayout() {
		return webLayout;
	}

	public void setWebLayout(IWebLayout webLayout) {
		this.webLayout = webLayout;
	}

	private JsonObjectBuilder getSubBo(String sRefKey, String sTranslatedEntity, String serviceIdentifier,
											  Object pk, IWebContext webContext, UID subform, UsageCriteria usage, boolean isWriteAllowed) {
		JsonObjectBuilder subBo = Json.createObjectBuilder();

		RestLinks subBoLinks = new RestLinks(subBo);
		subBoLinks.addLinkHref("boMeta", "referencemeta_self", RestLinks.Verbs.GET, sTranslatedEntity, sRefKey);
		if (pk != null) {
			subBoLinks.addLinkHref("bos", serviceIdentifier, RestLinks.Verbs.GET, sTranslatedEntity, pk, sRefKey);
		}

		subBoLinks.buildJson(webContext);

		boolean bWrite = isWriteAllowed && Rest.isWriteAllowedForSubform(subform, usage);
		if (!bWrite) {
			subBo.add("restriction", "readonly");
		}
		if (usage.getStatusUID() != null) {

			// NUCLOS-6134
			SubformPermission subformPermission = Rest.facade().getSubformPermission(subform, usage);
			if (subformPermission != null) {

				if (bWrite) {
					boolean noCreate = !subformPermission.canCreate();
					boolean noDelete = !subformPermission.canDelete();

					if (noCreate && noDelete) {
						subBo.add("restriction", "nocreate,nodelete");
					} else if (noCreate) {
						subBo.add("restriction", "nocreate");
					} else if (noDelete) {
						subBo.add("restriction", "nodelete");
					}
				}

				Collection<UID> readWriteAllowedGroups = subformPermission.getReadWriteAllowedGroups();

				if (readWriteAllowedGroups != null) {
					Collection<UID> attrNotWrite = metaProvider.getAllFieldsByEntityFilteredByGroups(subform, readWriteAllowedGroups, false, false);
					Collection<UID> attrNotRO = Collections.emptySet();

					Collection<UID> readOnlyAllowedGroups = subformPermission.getReadOnlyAllowedGroups();
					if (readOnlyAllowedGroups != null) {
						attrNotRO = metaProvider.getAllFieldsByEntityFilteredByGroups(subform, readOnlyAllowedGroups, false, false);
					}

					if (!attrNotWrite.isEmpty()) {
						JsonObjectBuilder job = Json.createObjectBuilder();
						for (UID attr : attrNotWrite) {
							String restr = attrNotRO.contains(attr) ? "hidden" : "ro";
							job.add(Rest.translateUid(E.ENTITYFIELD, attr), restr);
						}
						subBo.add("readonlyattributes", job);
					}
				}

			}

		}

		return subBo;
	}


}
