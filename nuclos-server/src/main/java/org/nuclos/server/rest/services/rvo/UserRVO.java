//Copyright (C) 2016  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.services.rvo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.security.UserVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserRVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private boolean removed;
	private String activationCode;
	private Date changedAt;
	private String changedBy;
	private UID communicationAccountPhone;
	private Date createdAt;
	private String createdBy;
	private String email;
	private String email2;
	private Date expirationDate;
	private String firstname;
	private UID id;
	private String lastname;
	private Boolean locked;
	private String username;
	private String newPassword;
	private Boolean notifyUser;
	private Date lastPasswordChange;
	private UID primaryKey;
	private Boolean passwordChangeRequired;
	private Boolean setPassword;
	private Boolean superuser;
	private int version;
	private Boolean privacyConsentAccepted;
	private Boolean loginWithEmailAllowed;
	
	private List<String> roles = new ArrayList<String>();

	public UserRVO() {
	}

	public UserRVO(UserVO userVO) {
		removed = userVO.isRemoved();
		activationCode = userVO.getActivationCode();
		changedAt = userVO.getChangedAt();
		changedBy = userVO.getChangedBy();
		communicationAccountPhone = userVO.getCommunicationAccountPhone();
		createdAt = userVO.getCreatedAt();
		createdBy = userVO.getCreatedBy();
		email = userVO.getEmail();
		expirationDate = userVO.getExpirationDate();
		firstname = userVO.getFirstname();
		id = userVO.getId();
		lastname = userVO.getLastname();
		locked = userVO.getLocked();
		username = userVO.getUsername();
		newPassword = userVO.getNewPassword();
		notifyUser = userVO.getNotifyUser();
		lastPasswordChange = userVO.getLastPasswordChange();
		primaryKey = userVO.getPrimaryKey();
		passwordChangeRequired = userVO.getPasswordChangeRequired();
		setPassword = userVO.getSetPassword();
		superuser = userVO.getSuperuser();
		version = userVO.getVersion();
		privacyConsentAccepted = userVO.isPrivacyConsentAccepted();
		loginWithEmailAllowed = userVO.isLoginWithEmailAllowed();
	}
	
	@JsonIgnore
	public UserVO toUserVO(){
		UserVO vo = new UserVO(toMasterDataVO());
		vo.setNewPassword(newPassword);
		return vo;
	}
	
	@JsonIgnore
	public MasterDataVO<UID> toMasterDataVO(){
		MasterDataVO<UID> mdvo = new MasterDataVO<UID>(E.USER.getUID(), getId(), 
				getCreatedAt(), getCreatedBy(), getChangedAt(), getChangedBy(), getVersion(), null, null, null, false);
		mdvo.getEntityObject().setComplete(true);
		mdvo.setFieldValue(E.USER.username, username);
		mdvo.setFieldValue(E.USER.email, email);
		mdvo.setFieldValue(E.USER.lastname, lastname);
		mdvo.setFieldValue(E.USER.firstname, firstname);
		mdvo.setFieldValue(E.USER.superuser, superuser);
		mdvo.setFieldValue(E.USER.locked, locked);
		mdvo.setFieldValue(E.USER.lastPasswordChange, lastPasswordChange);
		mdvo.setFieldValue(E.USER.expirationDate, expirationDate);
		mdvo.setFieldValue(E.USER.passwordChangeRequired, passwordChangeRequired);
		mdvo.setFieldUid(E.USER.communicationAccountPhone, communicationAccountPhone);
		mdvo.setFieldValue(E.USER.activationCode, activationCode);
		mdvo.setFieldValue(E.USER.privacyConsentAccepted, Boolean.TRUE.equals(this.privacyConsentAccepted));
		mdvo.setFieldValue(E.USER.loginWithEmailAllowed, Boolean.TRUE.equals(this.loginWithEmailAllowed));
		return mdvo;
	}

	public boolean isRemoved() {
		return removed;
	}

	public void setRemoved(boolean removed) {
		this.removed = removed;
	}

	public String getActivationCode() {
		return activationCode;
	}

	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}

	public Date getChangedAt() {
		return changedAt;
	}

	public void setChangedAt(Date changedAt) {
		this.changedAt = changedAt;
	}

	public String getChangedBy() {
		return changedBy;
	}

	public void setChangedBy(String changedBy) {
		this.changedBy = changedBy;
	}

	public UID getCommunicationAccountPhone() {
		return communicationAccountPhone;
	}

	public void setCommunicationAccountPhone(UID communicationAccountPhone) {
		this.communicationAccountPhone = communicationAccountPhone;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public UID getId() {
		return id;
	}

	public void setId(UID id) {
		this.id = id;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Boolean getLocked() {
		return locked;
	}

	public void setLocked(Boolean locked) {
		this.locked = locked;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public Boolean getNotifyUser() {
		return notifyUser;
	}

	public void setNotifyUser(Boolean notifyUser) {
		this.notifyUser = notifyUser;
	}

	public Date getLastPasswordChange() {
		return lastPasswordChange;
	}

	public void setLastPasswordChange(Date lastPasswordChange) {
		this.lastPasswordChange = lastPasswordChange;
	}

	public UID getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(UID primaryKey) {
		this.primaryKey = primaryKey;
	}

	public Boolean getPasswordChangeRequired() {
		return passwordChangeRequired;
	}

	public void setPasswordChangeRequired(Boolean passwordChangeRequired) {
		this.passwordChangeRequired = passwordChangeRequired;
	}

	public Boolean getSetPassword() {
		return setPassword;
	}

	public void setSetPassword(Boolean setPassword) {
		this.setPassword = setPassword;
	}

	public Boolean getSuperuser() {
		return superuser;
	}

	public void setSuperuser(Boolean superuser) {
		this.superuser = superuser;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public Boolean isPrivacyConsentAccepted() {
		return Boolean.TRUE.equals(privacyConsentAccepted);
	}

	public void setPrivacyConsentAcceptedt(Boolean privacyConsentAccepted) {
		this.privacyConsentAccepted = privacyConsentAccepted;
	}

	public String getEmail2() {
		return email2;
	}
}
