//Copyright (C) 2016  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.services.rvo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nuclos.common.E;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.common.ejb3.EntityObjectFacadeLocal;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WebAddonUsageRVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<WebAddonRVO> resultlists = new ArrayList<>();

	private Map<UID, String> propertyNameCache = new HashMap<>();

	public WebAddonUsageRVO() {
		this(false);
	}

	public WebAddonUsageRVO(boolean bLoadFromDb) {
		if (bLoadFromDb) {
			CollectableSearchExpression searchexpr = new CollectableSearchExpression(SearchConditionUtils.newComparison(E.WEBADDON.resultlist, ComparisonOperator.EQUAL, Boolean.TRUE));
			EntityObjectFacadeLocal efacade = SpringApplicationContextHolder.getBean(EntityObjectFacadeLocal.class);
			for (EntityObjectVO eo : efacade.getEntityObjectsChunkNoCheck(E.WEBADDON.getUID(), searchexpr, ResultParams.DEFAULT_RESULT_PARAMS, null)) {
				final WebAddonRVO webAddonRVO = new WebAddonRVO(eo, propertyNameCache);
				if (eo.getFieldValue(E.WEBADDON.active.getUID()).equals(Boolean.TRUE)) {
					resultlists.add(webAddonRVO);
				}
			}
		}
	}

	public List<WebAddonRVO> getResultlists() {
		return resultlists;
	}

	public void addResultlist(WebAddonRVO webAddon) {
		resultlists.add(webAddon);
	}
}
