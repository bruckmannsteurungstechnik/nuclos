//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.security.UserVO;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.NuclosBadCredentialsException;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.rcp.RemoteAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * Implementation for Spring's <code>RemoteAuthenticationManager</code> that
 * does not convert all exceptions to RemoteAuthenticationException.<br>
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Transactional(propagation=Propagation.NOT_SUPPORTED, noRollbackFor= {Exception.class})
public class RemoteAuthenticationManager implements org.nuclos.common.security.RemoteAuthenticationManager, InitializingBean {

	private static final Logger LOG = LoggerFactory.getLogger(RemoteAuthenticationManager.class);

	private AuthenticationManager authenticationManager;

	private UserDetailsService userDetailsService;

	private TemporaryBlockService blockService;

	private Map<String, Integer> failedPasswordChangeAttempts = new HashMap<>();

	public void afterPropertiesSet() {
		Assert.notNull(this.authenticationManager, "authenticationManager is required");
	}

	@Override
	public Collection<? extends GrantedAuthority> attemptAuthentication(
			final String username,
			final String password
	) throws RemoteAuthenticationException {
		UsernamePasswordAuthenticationToken request = new UsernamePasswordAuthenticationToken(username, password);

		if (blockService.isBlocked()) {
			throw new NuclosWebException(
					// TODO: Update to javaee-api 8 and use {@link org.nuclos.server.rest.misc.NuclosWebException.NuclosWebException(javax.ws.rs.core.Response.Status)} with Status.TOO_MANY_REQUESTS here
					Response.status(429).entity("Too Many Requests").type(MediaType.TEXT_PLAIN).build()
			);
		}

		boolean triedUnknownCredentials = true;
		Authentication auth;
		try {
			auth = authenticationManager.authenticate(request);
			userDetailsService.logAttempt(username, auth.isAuthenticated());
			final Collection<? extends GrantedAuthority> result = auth.getAuthorities();
			if (auth.isAuthenticated()) {
				triedUnknownCredentials = false;
				blockService.deleteAttempts();
				LOG.info("user {} logged in successfully", username);
			}
			return result;
		} catch (UsernameNotFoundException ex) {
			throw ex;
		} catch (CredentialsExpiredException ex) {
			// Credentials are correct, but expired - do not block the user for this
			triedUnknownCredentials = false;
			userDetailsService.logAttempt(username, true);
			throw ex;
		} catch (AuthenticationException ex) {
			userDetailsService.logAttempt(username, false);
			return Collections.emptyList();
		} finally {
			if (triedUnknownCredentials) {
				blockService.logAttempt();
			}
		}
	}

	@Override
	public void changePassword(String username, String oldpassword, final String newpassword, String customUsage) throws AuthenticationException, CommonBusinessException {
		if (blockService.isBlocked())
			throw new NuclosWebException(Response.status(429).entity("Too Many Requests").type(MediaType.TEXT_PLAIN).build());
		final UserDetails ud = userDetailsService.loadUserByUsername(username);

		String currentUser = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
		if (currentUser.equals("anonymousUser")) { //used for pw-reset
			if (ud.isCredentialsNonExpired()) {
				throw new CommonPermissionException();
			}
		}


		boolean authenticated = false;
		if (StringUtils.isNullOrEmpty(ud.getPassword()) && StringUtils.isNullOrEmpty(oldpassword)) {
			authenticated = true;
		}

		final String sPasswordFromUser = StringUtils.encryptPw(username, oldpassword);
		if (!authenticated && sPasswordFromUser.equals(ud.getPassword())) {
			authenticated = true;
		}

		// http://support.novabit.de/browse/ACC-228
		final SecurityContext context = SecurityContextHolder.getContext();
		final Authentication preAuth = context.getAuthentication();
		if (!authenticated && preAuth != null) {
			final Collection<? extends GrantedAuthority> granted = preAuth.getAuthorities();
			if (granted != null && !granted.isEmpty()) {
				final Collection<GrantedAuthority> required = new ArrayList<GrantedAuthority>();
				required.add(new SimpleGrantedAuthority("Login"));
				
				required.removeAll(granted);
				if (required.isEmpty() && ud.getUsername().equalsIgnoreCase(username) && sPasswordFromUser.equals(ud.getPassword())) {
					authenticated = true;
				}
			}
		}
		
		ArrayList<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("Login"));

		if (preAuth != null) {
			UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(preAuth.getPrincipal(), preAuth.getCredentials(), authorities);
			context.setAuthentication(auth);
			currentUser = preAuth.getPrincipal().toString();
		} else {
			UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(ud.getUsername(), ud.getPassword(), authorities);
			context.setAuthentication(auth);
			currentUser = ud.getUsername();
		}

		if (authenticated) {
			failedPasswordChangeAttempts.remove(currentUser);
			blockService.deleteAttempts();
			SpringApplicationContextHolder.getBean(UserFacadeLocal.class).setPassword(ud.getUsername(), newpassword, customUsage);
		}
		else {
			if (currentUser.equals(username)) {
				userDetailsService.logAttempt(ud.getUsername(), authenticated);
			} else {
				if (failedPasswordChangeAttempts.containsKey(currentUser)) {
					failedPasswordChangeAttempts.put(currentUser, failedPasswordChangeAttempts.get(currentUser) + 1);
				} else {
					failedPasswordChangeAttempts.put(currentUser, 1);
				}

				blockService.logAttempt();

				String sMaxattempts = blockService.getParameterProvider().getValue(ParameterProvider.KEY_SECURITY_LOCK_ATTEMPTS);
				if (!StringUtils.isNullOrEmpty(sMaxattempts)) {
					int maxattempts = 0;
					try {
						maxattempts = Integer.parseInt(sMaxattempts);
					}
					catch (NumberFormatException ex) {
						LOG.error("Cannot parse parameter value for key {}", ParameterProvider.KEY_SECURITY_LOCK_ATTEMPTS, ex);
						return;
					}

					if (maxattempts > 0 && failedPasswordChangeAttempts.get(currentUser) > maxattempts) {
						UserFacadeBean bean = SpringApplicationContextHolder.getBean(UserFacadeBean.class);
						UserVO uvo = bean.getByUserName(currentUser);
						uvo.setLocked(true);
						bean.modify(uvo, null, null);
						throw new NuclosWebException(Response.status(423).entity("Locked").type(MediaType.TEXT_PLAIN).build());
					}
				}
			}
			throw new NuclosBadCredentialsException("invalid.login.exception");
		}
	}

	public AuthenticationManager getAuthenticationManager() {
		return authenticationManager;
	}

	public void setAuthenticationManager(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}

	public void setUserDetailsService(UserDetailsService userDetailsService) {
		this.userDetailsService = userDetailsService;
	}

	public void setBlockService(TemporaryBlockService blockService) {
		this.blockService = blockService;
	}
}
