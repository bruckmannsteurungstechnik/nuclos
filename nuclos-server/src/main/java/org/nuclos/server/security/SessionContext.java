package org.nuclos.server.security;

import java.util.List;
import java.util.Locale;

import org.nuclos.common.MandatorVO;
import org.nuclos.common.UID;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;

public class SessionContext {

	private Authentication authentication;
	private Locale locale;
	private final String jSessionId;
	private final Long id;
	private List<MandatorVO> mandators;
	private UID mandatorUID;
	private UID dataLanguageUID;

	/**
	 * @param id The DB-ID of this session (from t_ad_session)
	 * @param jSessionId The container session ID
	 */
	public SessionContext(
			final long id,
			final String jSessionId,
			final Authentication authentication,
			final Locale locale
	) {
		this.id = id;
		this.jSessionId = jSessionId;
		this.authentication = authentication;
		this.locale = locale;
	}

	public Long getId() {
		return id;
	}

	public String getJSessionId() {
		return jSessionId;
	}

	public String getUsername() {
		if (authentication != null && authentication.getDetails() instanceof User) {
			return ((User)authentication.getDetails()).getUsername();
		}
		return "<unknown>";
	}
	
	public Locale getLocale() {
		return locale;
	}
	
	public Authentication getAuthentication() {
		return authentication;
	}
	
	public void setAuthentication(Authentication authentication) {
		this.authentication = authentication;
	}
	
	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public List<MandatorVO> getMandators() {
		return mandators;
	}

	public void setMandators(List<MandatorVO> mandators) {
		this.mandators = mandators;
	}

	public UID getMandatorUID() {
		return mandatorUID;
	}

	public void setMandatorUID(UID mandatorUID) {
		this.mandatorUID = mandatorUID;
	}

	public void setDataLanguageUID(final UID dataLanguageUID) {
		this.dataLanguageUID = dataLanguageUID;
	}

	public UID getDataLanguageUID() {
		return dataLanguageUID;
	}
}
