package org.nuclos.server.security;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.nuclos.api.businessobject.Query;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.businessentity.Loginattempt;
import org.nuclos.common.E;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.common.ejb3.EntityObjectFacadeLocal;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.nuclos.server.web.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * Logs failed login attempts by IP.
 * Blocks an IP after too many failed attempts.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 * @see <a href="http://support.nuclos.de/browse/NUCLOS-7797">NUCLOS-7797</a>
 */
@Service
public class TemporaryBlockService {

	private static final Logger LOG = LoggerFactory.getLogger(TemporaryBlockService.class);

	private final Request request;
	private final ParameterProvider parameterProvider;
	private final EntityObjectFacadeLocal eoFacade;

	public TemporaryBlockService(
			final Request request,
			final ParameterProvider parameterProvider,
			final EntityObjectFacadeLocal eoFacade
	) {
		this.request = request;
		this.parameterProvider = parameterProvider;
		this.eoFacade = eoFacade;
	}

	/**
	 * IP blocking will be only active if there is a valid configuration.
	 */
	public boolean isEnabled() {
		return getMaxAttempts() > 0 && getPeriod() > 0;
	}

	private int getMaxAttempts() {
		return parameterProvider.getIntValue(ParameterProvider.SECURITY_IP_BLOCK_ATTEMPTS, -1);
	}

	private int getPeriod() {
		return parameterProvider.getIntValue(ParameterProvider.SECURITY_IP_BLOCK_PERIOD_IN_SECONDS, -1);
	}

	private Set<String> getProxyList() {
		final String param = parameterProvider.getValue(ParameterProvider.SECURITY_IP_BLOCK_PROXY_LIST);

		if (StringUtils.isBlank(param)) {
			return Collections.emptySet();
		}

		final String[] proxies = StringUtils.split(param, ",;\n ");

		return Sets.newHashSet(proxies);
	}

	private String getRemoteAddr() {
		@Nonnull final HttpServletRequest httpRequest = request.getHttpRequest();
		@Nonnull final String remoteAddr = httpRequest.getRemoteAddr();

		if (isProxy(remoteAddr)) {
			return getForwardedForIp(httpRequest);
		}

		return remoteAddr;
	}

	private boolean isProxy(final String remoteAddr) {
		return getProxyList().contains(remoteAddr);
	}

	/**
	 * Uses the X-Forwarded-For header to find the IP of the client.
	 * All proxy IPs are skipped, the first unknown IP is returned.
	 *
	 * @return The first unknown IP, or null if no unknown IP is found or the X-Forwarded-For
	 *         header is not set.
	 */
	private String getForwardedForIp(final HttpServletRequest httpRequest) {
		final String forwardedFor = httpRequest.getHeader("X-Forwarded-For");

		if (StringUtils.isNotBlank(forwardedFor)) {
			// Reverse list of IPs: Immediate Proxy -> intermedia proxies -> client
			final List<String> ips = Lists.reverse(
					Arrays.stream(forwardedFor.split(","))
							.map(StringUtils::trim)
							.collect(Collectors.toList())
			);

			// Look for the "closes" IP that is not a whitelisted proxy
			for (String ip: ips) {
				if (!isProxy(ip)) {
					return ip;
				}
			}
		}

		return null;
	}

	/**
	 * Checks if the remote IP is blocked because of too many failed login attempts.
	 */
	public boolean isBlocked() {
		if (!isEnabled()) {
			return false;
		}

		final String remoteAddr = getRemoteAddr();

		// If we don't have a remote addr, the request probably came from a whitelisted proxy
		// that did not forward the client IP via X-Forwarded-For.
		if (StringUtils.isBlank(remoteAddr)) {
			LOG.warn("Could not determine remote address");
			return false;
		}

		int attempts = getAttempts();
		boolean blocked = attempts >= getMaxAttempts();

		if (blocked) {
			LOG.warn("{}. failed login attempt with blocked IP: {}", attempts, getRemoteAddr());
		}

		return blocked;
	}

	/**
	 * Logs a new failed login attempt from the current remote IP.
	 */
	public void logAttempt() {
		if (!isEnabled()) {
			return;
		}

		String addr = getRemoteAddr();
		if (addr == null) {
			LOG.info("getRemoteAddr() equal null");
			return;
		}

		final EntityObjectVO<Object> eo = new EntityObjectVO<>(E.LOGIN_ATTEMPT);
		eo.setFieldValue(E.LOGIN_ATTEMPT.ip, addr);

		try {
			eoFacade.insert(eo);
			LOG.warn("{}. failed login attempt with IP: {}", getAttempts(), addr);
		} catch (CommonCreateException | CommonPermissionException | NuclosBusinessRuleException e) {
			LOG.error("Could not log failed login attempt", e);
		}
	}

	/**
	 * Deletes all login attempts of the remote IP.
	 */
	public void deleteAttempts() {
		if (getRemoteAddr() != null) {
			Query<Loginattempt> query = QueryProvider.create(Loginattempt.class)
					.where(Loginattempt.Ip.eq(getRemoteAddr()));

			List<Loginattempt> attempts = QueryProvider.execute(query);
			try {
				if (attempts.size() > 0) {
					BusinessObjectProvider.deleteAll(attempts);
					LOG.info(
							"Deleted {} previously failed login attempts for IP {}",
							attempts.size(),
							getRemoteAddr()
					);
				}
			} catch (BusinessException e) {
				LOG.error("Could not delete failed login attempts", e);
			}
		}
	}

	private int getAttempts() {
		// TODO: Only COUNT the records instead of really fetching them - cannot be done via the Rule API yet :(
		Query<Loginattempt> query = QueryProvider.create(Loginattempt.class)
				.where(Loginattempt.Ip.eq(getRemoteAddr()))
				.and(Loginattempt.CreatedAt.Gte(new Timestamp(System.currentTimeMillis() - getPeriod() * 1000)));
		return QueryProvider.execute(query).size();
	}

	public ParameterProvider getParameterProvider() {
		return parameterProvider;
	}
}
