//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.spring;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.FatalBeanException;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessorAdapter;
import org.springframework.beans.factory.config.RuntimeBeanReference;
import org.springframework.beans.factory.support.AutowireCandidateQualifier;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.core.Ordered;
import org.springframework.core.PriorityOrdered;
import org.springframework.stereotype.Component;

@Component
public class FacadeLocalProxyBeanFactoryPostProcessor extends InstantiationAwareBeanPostProcessorAdapter 
	implements BeanFactoryPostProcessor, PriorityOrdered {
	
	private static final Logger LOG = LoggerFactory.getLogger(
		FacadeLocalProxyBeanFactoryPostProcessor.class);
	
	private int order = Ordered.LOWEST_PRECEDENCE;  // default: same as non-Ordered
	
	//
	
	private ConfigurableListableBeanFactory beanFactory;
	
	private BeanDefinitionRegistry registry;
	
	private Map<String,Class<?>> beanName2Class = new HashMap<String, Class<?>>();

	FacadeLocalProxyBeanFactoryPostProcessor() {
	}

	/**
	 * Set the order value of this object for sorting purposes.
	 * @see PriorityOrdered
	 */
	public void setOrder(int order) {
		this.order = order;
	}

	@Override
	public int getOrder() {
		return this.order;
	}

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		this.beanFactory = beanFactory;
		registry = (BeanDefinitionRegistry) beanFactory;
		final Set<String> beanDefinitionNames =
			new HashSet<>(Arrays.asList(beanFactory.getBeanDefinitionNames()));
		for (String bn: beanFactory.getBeanDefinitionNames()) {
			final BeanDefinition bd = beanFactory.getBeanDefinition(bn);
			if (bd.isAbstract()) {
				continue;
			}
			String bclass = bd.getBeanClassName();
			if (bclass == null) {
				continue;
			}
			// support for Springockito
			if (bclass.startsWith("org.kubek2k.springockito.core.internal.") && bclass.endsWith("MockFactorySpringockito")) {
				final String mockClass = (String) bd.getPropertyValues().get("mockClass");
				LOG.info("Found springockito mock {} : {}", bn, bd);
				if (mockClass != null) {
					LOG.info("real bean type for {}: {} (instead of {})",
					         bn, mockClass, bclass);
					bclass = mockClass;
				}
			}
			if (bclass.startsWith("org.nuclos.server.spring.")) {
				LOG.debug("Found bean definition {}: {}", bn, bd);
				LOG.debug("bean type for {}: {}", bn, beanFactory.getType(bn));
			}
			if (bclass.startsWith("org.nuclos.server.") && bclass.endsWith("FacadeBean")) {
				if (!bd.isSingleton()) {
					throw new FatalBeanException("facade bean " + bn + " is not defined as singleton");
				}
				LOG.debug("Found bean definition {}: {}", bn, bd);
				final String facadeLocalInterface = mkLocalInterfaceName(bclass);
				
				final Class<?> facadeLocal;
				try {
					facadeLocal = beanFactory.getBeanClassLoader().loadClass(facadeLocalInterface);
				}
				catch (ClassNotFoundException e) {
					// no local interface -> ignore and continue
					LOG.warn("No local interface for facade bean: {}", bn);
					continue;
					// throw new FatalBeanException("Failed on interface " + facadeLocalInterface, e);
				}
				final MutablePropertyValues props = new MutablePropertyValues();
				// final ConstructorArgumentValues props = new ConstructorArgumentValues();
				addPropertyValue(props, "facadeLocalInterface", facadeLocal, "java.lang.Class");
				addPropertyRef(props, "facadeBean", bn);
				addPropertyRef(props, "nuclosRemoteContextHolder", "nuclosRemoteContextHolder");

				final GenericBeanDefinition beanDefinition = new GenericBeanDefinition();
				beanDefinition.setBeanClassName("org.nuclos.server.spring.FacadeLocalProxyFactoryBean");
				final AutowireCandidateQualifier qualifier = mkQualifier(facadeLocal);
				if (qualifier != null) {
					// Tried very hard to get this working with qualifier - but at present... (tp)
					beanDefinition.addQualifier(qualifier);
					beanDefinition.setPrimary(true);
				}
				beanDefinition.setPropertyValues(props);
				
				final String beanName = mkBeanName(facadeLocalInterface);
				if (beanDefinitionNames.contains(beanName)) {
					LOG.warn("local interface {} already defined in spring context as {}, "
					         + "only adding qualifier",
					         facadeLocalInterface, beanName);
					// but set the qualifier here
					((GenericBeanDefinition) bd).addQualifier(qualifier);
					continue;
				}

				registry.registerBeanDefinition(beanName, beanDefinition);
				beanFactory.registerDependentBean(beanName, bn);
				beanName2Class.put(beanName, facadeLocal);
				
				LOG.debug("registered bean definition {}: {}", beanName, beanDefinition);
				/*
				LOG.info("contains bean definition for "  + beanName + ": " + beanFactory.containsBeanDefinition(beanName));
				LOG.info("contains bean for "  + beanName + ": " + beanFactory.containsBean(beanName));
				final Object bean = beanFactory.getSingleton(beanName);
				LOG.info("bean for "  + beanName + ": " + beanFactory.getSingleton(beanName));
				 */
			}
		}
		LOG.debug("END");
	}
	
	private void addPropertyValue(MutablePropertyValues props, String property, Object value, String type) {
		props.addPropertyValue(property, value);
	}
	
	private void addPropertyRef(MutablePropertyValues props, String property, String refBeanName) {
		props.addPropertyValue(property, new RuntimeBeanReference(refBeanName));
	}
	
	private String mkLocalInterfaceName(String facadeBean) {
		final int len = facadeBean.length();
		return facadeBean.substring(0, len - 4) + "Local";
	}
	
	private AutowireCandidateQualifier mkQualifier(Class<?> local) {
		final String name = local.getSimpleName();
		final String qualifierName = name.substring(0, 1).toLowerCase() + name.substring(1, name.length() - 5);
		/*
		final Class<?>[] interfaces = local.getInterfaces();
		Class<?> common = null;
		for (Class<?> i: interfaces) {
			if (i.getSimpleName().startsWith("Common")) {
				common = i;
				break;
			}
		}
		if (common == null) {
			return null;
		}
		 */
		LOG.debug("qualifier for {} is -> {}", local, qualifierName);
		return new AutowireCandidateQualifier(Qualifier.class, qualifierName);
	}

	private String mkBeanName(String classname) {
		final int len = classname.lastIndexOf('.');
		return classname.substring(len + 1, len + 2).toLowerCase() + classname.substring(len + 2);
	}
	
	//
	
	@Override
	public Class<?> predictBeanType(Class<?> beanClass, String beanName) throws BeansException {
		Class<?> result = beanName2Class.get(beanName);
		/*
		if (result != null || interestedInBeanName(beanName)) {
			LOG.info("predictBeanType for " + beanName + ": " + beanClass + " -> " + result);
		}
		 */
		return result;
	}

	private boolean interestedInBeanName(String beanName) {
		return beanName.endsWith("Local") || beanName.startsWith("org.nuclos.server.spring.");
	}
	
}
