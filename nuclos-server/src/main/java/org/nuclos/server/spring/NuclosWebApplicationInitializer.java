package org.nuclos.server.spring;

import javax.servlet.Filter;
import javax.servlet.FilterRegistration;
import javax.servlet.Registration;
import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

import org.nuclos.server.rest.NuclosCustomRestApplication;
import org.nuclos.server.rest.NuclosRestApplication;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.filter.RequestContextFilter;

/**
 * This file replaces the old web.xml.
 * As of the Servlet 3.0 specification it is now possible to initialize WebApps programmatically.
 * <p>
 * See http://support.nuclos.de/browse/NUCLOS-5322
 * <p>
 * Extension developers may extend this class and add mappings to Nuclos Filters and Servlets,
 * or use a completely independent WebApplicationInitializer.
 * A web.xml can also still be used.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class NuclosWebApplicationInitializer implements WebApplicationInitializer {

	public static final int DEFAULT_SESSION_TIMEOUT = 60 * 60 * 24 * 7;    // 7 days

	private static final String CONTEXT_CONFIG_LOCATION = "contextConfigLocation";
	private static final String CONTEXT_INITIALIZER_CLASSES = "contextInitializerClasses";

	@Override
	public void onStartup(ServletContext servletContext) {

		servletContext.getSessionCookieConfig().setMaxAge(DEFAULT_SESSION_TIMEOUT);
		servletContext.setInitParameter(CONTEXT_CONFIG_LOCATION, "classpath*:META-INF/nuclos/nuclos-beans-startup.xml");

		addFilterClientLifecycle(servletContext);
		addFilterSpringSecurity(servletContext);
		addFilterSpringRequestContext(servletContext);

		addServletJnlp(servletContext);
		addServletJarDownload(servletContext);
		addServletRemoting(servletContext);
		addServletJmsBroker(servletContext);
		addServletCxf(servletContext);
		addServletSpringWS(servletContext);
		addServletReports(servletContext);
		addServletRest(servletContext);
		addServletCustomRest(servletContext);
		addServletSiDownload(servletContext);
		addServletApiDownload(servletContext);
		addServletAtmosphere(servletContext);
		addServletLauncherDownload(servletContext);
	}

	/**
	 * Returns the filter that matches the given filterName.
	 * If no such filter exists, it is registered first.
	 */
	protected FilterRegistration getFilter(
			final ServletContext servletContext,
			final String filterName,
			final Class<? extends Filter> filterClass
	) {
		FilterRegistration filter = servletContext.getFilterRegistration(filterName);

		if (filter == null) {
			filter = servletContext.addFilter(
					filterName,
					filterClass
			);
		}

		return filter;
	}

	/**
	 * See {@link #getFilter(ServletContext, String, Class)}
	 * This method additionally sets the "asyncSupported" parameter.
	 */
	protected FilterRegistration getFilter(
			final ServletContext servletContext,
			final String filterName,
			final Class<? extends Filter> filterClass,
			final boolean asyncSupported
	) {
		FilterRegistration filter = getFilter(servletContext, filterName, filterClass);

		if (filter instanceof FilterRegistration.Dynamic) {
			((FilterRegistration.Dynamic) filter).setAsyncSupported(asyncSupported);
		}

		return filter;
	}

	protected FilterRegistration addFilterSpringRequestContext(final ServletContext servletContext) {
		FilterRegistration filter = getFilter(
				servletContext,
				"springRequestContextFilter",
				RequestContextFilter.class,
				true
		);

		filter.addMappingForUrlPatterns(null, true, "/rest/*");

		return filter;
	}

	protected FilterRegistration addFilterSpringSecurity(final ServletContext servletContext) {
		FilterRegistration filter = getFilter(
				servletContext,
				"springSecurityFilterChain",
				org.springframework.web.filter.DelegatingFilterProxy.class,
				true
		);

		filter.addMappingForUrlPatterns(null, true, "/*");

		return filter;
	}

	protected FilterRegistration addFilterClientLifecycle(final ServletContext servletContext) {
		FilterRegistration filter = getFilter(
				servletContext,
				"clientLifecycleFilter",
				org.springframework.web.filter.DelegatingFilterProxy.class
		);

		filter.addMappingForUrlPatterns(null, true, "/app/*");

		return filter;
	}

	/**
	 * Returns the servlet that matches the simple name of the given servlet class.
	 * If no such servlet exists, it is registered first.
	 */
	protected ServletRegistration getServlet(
			final ServletContext servletContext,
			final String servletName,
			final Class<? extends Servlet> servletClass
	) {
		javax.servlet.ServletRegistration servlet = servletContext.getServletRegistration(servletName);

		if (servlet == null) {
			ServletRegistration.Dynamic dynamicServlet = servletContext.addServlet(
					servletName,
					servletClass
			);
			servlet = dynamicServlet;
		}

		return servlet;
	}

	/**
	 * See {@link #getServlet(ServletContext, String, Class)}
	 * This method additionally sets the "loadOnStartup" parameter.
	 */
	protected ServletRegistration getServlet(
			final ServletContext servletContext,
			final String servletName,
			final Class<? extends Servlet> servletClass,
			final int loadOnStartup
	) {
		javax.servlet.ServletRegistration servlet = getServlet(servletContext, servletName, servletClass);

		if (servlet instanceof ServletRegistration.Dynamic) {
			((ServletRegistration.Dynamic) servlet).setLoadOnStartup(loadOnStartup);
		}

		return servlet;
	}

	/**
	 * See {@link #getServlet(ServletContext, String, Class)}
	 * This method additionally sets the "loadOnStartup" and "asyncSupported" parameters.
	 */
	protected ServletRegistration getServlet(
			final ServletContext servletContext,
			final String servletName,
			final Class<? extends Servlet> servletClass,
			final int loadOnStartup,
			final boolean asyncSupported
	) {
		javax.servlet.ServletRegistration servlet = getServlet(servletContext, servletName, servletClass, loadOnStartup);

		if (servlet instanceof Registration.Dynamic) {
			((Registration.Dynamic) servlet).setAsyncSupported(asyncSupported);
		}

		return servlet;
	}

	/**
	 * Adds the Atmosphere servlet which handles websocket connections and maps it to "/websocket/*".
	 */
	protected ServletRegistration addServletAtmosphere(final ServletContext servletContext) {
		ServletRegistration servlet = getServlet(
				servletContext,
				org.atmosphere.cpr.AtmosphereServlet.class.getSimpleName(),
				org.atmosphere.cpr.AtmosphereServlet.class
		);

		servlet.addMapping("/websocket/*");

		return servlet;
	}

	/**
	 * Adds the JarDownloadServlet which handles .jar downloads (Web Start).
	 */
	protected ServletRegistration addServletJarDownload(final ServletContext servletContext) {
		ServletRegistration servlet = getServlet(
				servletContext,
				org.nuclos.server.jnlp.JarDownloadServlet.class.getSimpleName(),
				org.nuclos.server.jnlp.JarDownloadServlet.class
		);

		servlet.addMapping("*.jar");

		return servlet;
	}

	protected ServletRegistration addServletApiDownload(final ServletContext servletContext) {
		ServletRegistration servlet = getServlet(
				servletContext,
				"apidownload",
				org.nuclos.server.web.ApiDownloadServlet.class,
				-8
		);

		servlet.addMapping("/apidownload/*");

		return servlet;
	}

	protected ServletRegistration addServletLauncherDownload(final ServletContext servletContext) {
		ServletRegistration servlet = getServlet(
				servletContext,
				"launcherdownload",
				org.nuclos.server.web.LauncherDownloadServlet.class,
				10
		);

		servlet.addMapping("/launcherdownload/*");

		return servlet;
	}

	protected ServletRegistration addServletSiDownload(final ServletContext servletContext) {
		ServletRegistration servlet = getServlet(
				servletContext,
				"sidownload",
				org.nuclos.server.web.SourceItemDownloadServlet.class,
				7
		);

		servlet.addMapping("/sidownload/*");

		return servlet;
	}

	/**
	 * Adds a jersey servlet for the REST service (/rest/*).
	 */
	protected ServletRegistration addServletRest(final ServletContext servletContext) {
		ServletRegistration servlet = getServlet(
				servletContext,
				"JAX-RS REST Servlet",
				org.glassfish.jersey.servlet.ServletContainer.class,
				6,
				true
		);

		servlet.setInitParameter("javax.ws.rs.Application", NuclosRestApplication.class.getCanonicalName());
		servlet.addMapping("/rest/*");

		return servlet;
	}

	protected ServletRegistration addServletCustomRest(final ServletContext servletContext) {
		ServletRegistration customRestServlet = getServlet(
				servletContext,
				"JAX-RS Custom REST Servlet",
				org.glassfish.jersey.servlet.ServletContainer.class,
				7,
				true
		);
		customRestServlet.setInitParameter("javax.ws.rs.Application", NuclosCustomRestApplication.class.getCanonicalName());
		customRestServlet.addMapping("/rest/custom/*");

		return customRestServlet;
	}

	protected ServletRegistration addServletReports(final ServletContext servletContext) {
		ServletRegistration servlet = getServlet(
				servletContext,
				"reports",
				org.springframework.web.servlet.DispatcherServlet.class,
				-5
		);

		servlet.setInitParameter(CONTEXT_CONFIG_LOCATION, "classpath*:META-INF/nuclos/*-reports.xml");
		servlet.setInitParameter(
				CONTEXT_INITIALIZER_CLASSES,
				org.nuclos.server.spring.DispatcherApplicationContextInitializer.class.getName()
		);
		servlet.addMapping("/reports/*");

		return servlet;
	}

	protected ServletRegistration addServletSpringWS(final ServletContext servletContext) {
		ServletRegistration servlet = getServlet(
				servletContext,
				"spring-ws",
				org.springframework.ws.transport.http.MessageDispatcherServlet.class,
				-4
		);

		servlet.setInitParameter(CONTEXT_CONFIG_LOCATION, "classpath*:META-INF/nuclos/*-ws.xml");
		servlet.setInitParameter("transformWsdlLocations", "true");
		servlet.setInitParameter(
				CONTEXT_INITIALIZER_CLASSES,
				org.nuclos.server.spring.DispatcherApplicationContextInitializer.class.getName()
		);
		servlet.addMapping("/springws/*");

		return servlet;
	}

	protected ServletRegistration addServletCxf(final ServletContext servletContext) {
		ServletRegistration servlet = getServlet(
				servletContext,
				"CXFServlet",
				org.apache.cxf.transport.servlet.CXFServlet.class
		);

		servlet.addMapping("/ws/*");

		return servlet;
	}

	protected ServletRegistration addServletJmsBroker(final ServletContext servletContext) {
		ServletRegistration servlet = getServlet(
				servletContext,
				"jmsbroker",
				org.nuclos.server.web.activemq.NuclosJMSBrokerTunnelServlet.class,
				-2
		);

		servlet.addMapping("/jmsbroker");

		return servlet;
	}

	protected ServletRegistration addServletRemoting(final ServletContext servletContext) {
		ServletRegistration servlet = getServlet(
				servletContext,
				"remoting",
				org.springframework.web.servlet.DispatcherServlet.class,
				-1
		);

		servlet.setInitParameter(CONTEXT_CONFIG_LOCATION, "classpath*:META-INF/nuclos/*-remoting.xml");
		servlet.setInitParameter(
				CONTEXT_INITIALIZER_CLASSES,
				"org.nuclos.server.spring.DispatcherApplicationContextInitializer"
		);
		servlet.addMapping("/remoting/*");

		return servlet;
	}

	/**
	 * Adds the JnlpServlet which handles .jnlp downloads (Web Start).
	 */
	protected ServletRegistration addServletJnlp(final ServletContext servletContext) {
		ServletRegistration servlet = getServlet(
				servletContext,
				"JnlpServlet",
				org.nuclos.server.jnlp.JnlpServlet.class
		);

		servlet.addMapping("*.jnlp");

		return servlet;
	}
}
