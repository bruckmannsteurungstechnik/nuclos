package org.nuclos.server.statemodel;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ForkJoinPool;

import org.nuclos.api.businessobject.facade.thin.Stateful;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.statemodel.State;
import org.nuclos.common.E;
import org.nuclos.common.NuclosEntityValidator;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.common.INucletCache;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.StateCache;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.nbo.AbstractNuclosObjectCompiler.NuclosBusinessJavaSource;
import org.nuclos.server.nbo.NuclosObjectBuilder;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

@Component("statemodelObjectBuilder")
@DependsOn("stateCache")
public class StatemodelObjectBuilder extends NuclosObjectBuilder{

	private static final Logger LOG = LoggerFactory.getLogger(StatemodelObjectBuilder.class);

	private MasterDataFacadeLocal masterDataFacade;
	private StatemodelObjectCompiler compiler;
	
	private static final String DEPENDENT_PREFIX = "_";
	private static final String DEFFAULT_ENTITY_PREFIX = "SM";
	private static final String DEFFAULT_ENTITY_POSTFIX = "SM";
	private static final String DEFFAULT_PACKAGE_NUCLET = "org.nuclet.statemodel";


	@Autowired
	final void setMasterDataFacade(MasterDataFacadeLocal masterDataFacade) {
		this.masterDataFacade = masterDataFacade;
	}
	
	@Autowired
	final void setStatemodelObjectCompiler(StatemodelObjectCompiler comp) {
		this.compiler = comp;
	}

	public StatemodelObjectCompiler getStatemodelObjectCompiler() {
		return this.compiler;
	}
	
	private final MasterDataFacadeLocal getMasterDataFacade() {
		return masterDataFacade;
	}
	
	@Autowired
	private MetaProvider provider;

	@Autowired
	private NucletDalProvider nucletDalProvider;

	@Autowired
	private StateCache stateCache;

	public StatemodelObjectBuilder() {}
	
	/**
	 * This method extracts a list of all user and system entities for which Nuclos Business Objects (NBOs)
	 * must be created. Elements in the list are compiled and stored in the Classpath
	 * 
	 * @throws CommonBusinessException
	 * @param builderThreadPool
	 */
	public void createObjects(final ForkJoinPool builderThreadPool) throws CommonBusinessException, InterruptedException {
		// get all statemodels
		List<EntityObjectVO<UID>> allModels = nucletDalProvider
			.getEntityObjectProcessor(E.STATEMODEL).getAll();
		List<NuclosBusinessJavaSource> busiJavaSource = new ArrayList<>();
		
		if (allModels != null && allModels.size() > 0) {
			// get all state for a statemodell and create class
			for (final EntityObjectVO<UID> eoVO : allModels) {
				if (Thread.currentThread().isInterrupted()) {
					throw new InterruptedException();
				}
				busiJavaSource.add(createJavaSourceFile(eoVO.getPrimaryKey(),
				                                        eoVO.getFieldValue(E.STATEMODEL.name),
				                                        eoVO.getFieldUid(E.STATEMODEL.nuclet)));
			}
			
			getStatemodelObjectCompiler().compileSourcesAndJar(builderThreadPool, busiJavaSource);
		}
	}

	private NuclosBusinessJavaSource createJavaSourceFile(
		UID statemodelUID,
		String statemodelName,
		UID nucletUID)
		throws InterruptedException {

		final String sPackage = getNucletPackageStatic(nucletUID, provider);
		final String formatEntity = getNameForFqn(statemodelName);
		final String qname = sPackage + "." + formatEntity;
		// final String filesname = NuclosCodegeneratorUtils.statemodelSource(sPackage, formatEntity).toString();
		final String filesname = NuclosCodegeneratorConstants.STATEMODEL_SRC_FOLDER.getAbsolutePath()
				+ File.separatorChar + sPackage.replace(".", File.separator)
				+ File.separatorChar + formatEntity + ".java";

		final String content = createJavaSourceContent(sPackage, formatEntity, statemodelUID);
		
		return new NuclosBusinessJavaSource(qname, filesname, content, true);
	}
	
	public static String getNameForFqn(String sName) {
		return NuclosEntityValidator.escapeJavaIdentifier(sName, DEFFAULT_ENTITY_PREFIX) + DEFFAULT_ENTITY_POSTFIX;
	}
	
	public static String getStateNameForFqn(Integer iNumeral) {
		return "State_" + iNumeral;
	}

	private String createJavaSourceContent(String pkgName, String className, UID statemodelUID)
		throws InterruptedException {
		StringBuilder s = new StringBuilder();
		s.append("package " + pkgName + ";\n\n");
		
		s.append("import " + UID.class.getCanonicalName() + ";\n");
		s.append("import " + State.class.getCanonicalName() + ";\n");
		s.append("import " + BusinessException.class.getCanonicalName() + ";\n");
		s.append("import " + Stateful.class.getCanonicalName() + ";\n\n");
		
		s.append("public class " + className + " {\n");
		
		s.append(createModelStates(statemodelUID));
		
		s.append("\n}");
		return s.toString();
	}

	private String createModelStates(UID statemodelUID) throws InterruptedException {
		
		final StringBuilder s = new StringBuilder();
		
		Collection<StateVO> statesByModel = stateCache.getStatesByModelId(statemodelUID);
		
		for (StateVO stateVO : statesByModel) {
			if (Thread.currentThread().isInterrupted()) {
				throw new InterruptedException();
			}
			String description = formatStringValues(stateVO.getDescription(Locale.ENGLISH), stateVO.getStatename(Locale.ENGLISH));
			String statename = formatStringValues(stateVO.getStatename(Locale.ENGLISH), "");
			
			s.append("\npublic static State " + getStateNameForFqn(stateVO.getNumeral()) + " = \n"
                                 + "\tnew State(UID.parseUID(\"" + stateVO.getId().getString() + "\"), \"" +  description + "\", \"" + statename + "\", " + stateVO.getNumeral() + ", UID.parseUID(\"" + stateVO.getModelUID().getString() + "\"));");
		}
		
		s.append("\n\n");
		s.append(createGetNumeralMethod(statesByModel));
		
		return s.toString();
	}
	
	private String createGetNumeralMethod(Collection<StateVO> states) {
		final StringBuilder s = new StringBuilder();
		
		s.append("\n  public static Integer getNumeral(org.nuclos.api.UID stateId) throws BusinessException {");
		s.append("\n    if (stateId == null) {");
		s.append("\n      throw new IllegalArgumentException(\"stateId must not be null\");");
		s.append("\n    }");
		s.append("\n");
		/*
		if (stateId.equals(State_10.getId())) {
			return State_10.getNumeral();
		}
		*/
		for (StateVO stateVO : states) {			
			final String sStateClass = getStateNameForFqn(stateVO.getNumeral());
			s.append("\n    if (stateId.equals(").append(sStateClass).append(".getId())) {");
			s.append("\n      return ").append(sStateClass).append(".getNumeral();");
			s.append("\n    }");
		}
		s.append("\n    throw new BusinessException(\"Status with id \" + stateId + \" does not exist in this status model\");");
		s.append("\n  }");
		s.append("\n");
		
		s.append("\n  public static Integer getNumeral(Stateful statefulBusinessObject) throws BusinessException {");
		s.append("\n    return getNumeral(statefulBusinessObject.getNuclosStateId());");
		s.append("\n  }");
		s.append("\n");
		
		return s.toString();
	}
	
	public static String getNucletPackageStatic(UID nucletUID, INucletCache nucletCache) {
		String retVal = DEFFAULT_PACKAGE_NUCLET;

		if (nucletUID != null) {
			String sFqn = nucletCache.getFullQualifiedNucletName(nucletUID);
			if (!StringUtils.looksEmpty(sFqn)) {
				retVal = sFqn;
			}
		}

		return retVal;
	}

}
