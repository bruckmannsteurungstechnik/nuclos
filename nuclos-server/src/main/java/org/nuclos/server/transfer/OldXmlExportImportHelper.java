//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.transfer;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.ZipException;
import java.util.zip.ZipOutputStream;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.common.MetaProvider;

/**
 * Helper class for XML Export and Import. <br>
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * 
 * @author <a href="mailto:fabian.kastlunger@novabit.de">fabian.kastlunger</a>
 * @version 01.00.00
 */
public class OldXmlExportImportHelper {

	public static final String EXPIMP_TYPE_EXPORT = "Export";
	public static final String EXPIMP_TYPE_IMPORT = "Import";

	public static final String EXPIMP_MESSAGE_LEVEL_INFO = "INFO";
	public static final String EXPIMP_MESSAGE_LEVEL_WARNING = "WARNING";
	public static final String EXPIMP_MESSAGE_LEVEL_ERROR = "ERROR";

	public static final String EXPIMP_ACTION_INSERT = "INSERT";
	public static final String EXPIMP_ACTION_UPDATE = "UPDATE";
	public static final String EXPIMP_ACTION_DELETE = "DELETE";
	public static final String EXPIMP_ACTION_READ = "READ";

	/**
	 * deletes all Files and Subfolders in a directory
	 */
	public static boolean deleteDir(File dir) {
		throw new NotImplementedException();
	}

	/**
	 * Copys a document file from one directory to another
	 */
	public static void copyDocumentFile(File documentDir, File destDir,
			String filename, Object entityId, Object newEntityId)
			throws IOException {
		throw new NotImplementedException();
	}

	public static void writeObjectToFile(Object oObject, File destDir,
			String filename) throws IOException {
		throw new NotImplementedException();
	}

	public static Object readObjectFromFile(File destDir, String filename)
			throws IOException, ClassNotFoundException {
		throw new NotImplementedException();
	}

	/**
	 * coverts a File into novabit container file (serializeable)
	 * 
	 * @param path
	 * @param filename
	 * @return
	 * @throws IOException
	 */
	public static org.nuclos.common2.File createFile(String path,
			String filename) throws IOException {
		throw new NotImplementedException();
	}

	/**
	 * creates azip file from an directory with all subfolders
	 * 
	 * @param dir
	 * @param zipFileName
	 * @throws IOException
	 */
	public static void createZipFile(String dir, String zipFileName)
			throws IOException {
		throw new NotImplementedException();
	}

	/**
	 * zips a directory
	 */
	private static void zipDir(String dir2zip, ZipOutputStream zipOut,
			String zipFileName) throws IOException {
		throw new NotImplementedException();
	}

	/**
	 * extracts a zip file to the given dir
	 */
	public static void extractZipArchive(File archive, File destDir)
			throws ZipException, IOException {
		throw new NotImplementedException();
	}

	private static File buildDirectoryHierarchyFor(String entryName,
			File destDir) {
		throw new NotImplementedException();
	}

	static public File zipFolder(File srcFolder, String destZipFile)
			throws Exception {
		throw new NotImplementedException();
	}

	static private void addFileToZip(String path, File srcFile,
			ZipOutputStream zip, String destZipFile) throws Exception {
		throw new NotImplementedException();
	}

	static private void addFolderToZip(String path, File srcFolder,
			ZipOutputStream zip, String destZipFile) throws Exception {
		throw new NotImplementedException();
	}

	/**
	 * §postcondition result != null
	 * 
	 * @return the name of the foreign key field referencing the parent entity
	 */
	public static final UID getForeignKeyFieldUID(final UID parentEntityUid,
			UID foreignKeyFieldToParentUid, final UID childEntityUid) {
		// Use the field referencing the parent entity from the subform, if any:
		UID resultUid = foreignKeyFieldToParentUid;

		if (resultUid == null) {
			// Default: Find the field referencing the parent entity from the
			// meta data.
			// If more than one field applies, throw an exception:
			
			final Map<UID, FieldMeta<?>> stFieldNames = MetaProvider.getInstance().getAllEntityFieldsByEntity(childEntityUid);

			for (final Entry<UID, FieldMeta<?>> entry : stFieldNames.entrySet()) {
				final FieldMeta<?> meta = entry.getValue();
				final UID fieldUid= entry.getKey();
				
				if (parentEntityUid.equals(meta.getForeignEntity())) {
					if (resultUid == null) {
						// this is the foreign key field:
						resultUid = fieldUid;
					} else {
						final String sMessage = "Das Unterformular f\u00fcr die Entit\u00e4t \""
								+ childEntityUid
								+ "\" enth\u00e4lt mehr als ein Fremdschl\u00fcsselfeld, das die \u00fcbergeordnete Entit\u00e4t \""
								+ parentEntityUid
								+ "\" referenziert:\n"
								+ "\t"
								+ resultUid
								+ "\n"
								+ "\t"
								+ meta.getFieldName()
								+ "\n"
								+ "Bitte geben Sie das Feld im Layout explizit an.";
						throw new CommonFatalException(sMessage);
					}
				}
			}
		}

		if (resultUid == null) {
			throw new CommonFatalException(
					"Das Unterformular f\u00fcr die Entit\u00e4t \""
							+ childEntityUid
							+ "\" enth\u00e4lt kein Fremdschl\u00fcsselfeld, das die \u00fcbergeordnete Entit\u00e4t \""
							+ parentEntityUid
							+ "\" referenziert."
							+ "\nBitte geben Sie das Feld im Layout explizit an.");
		}
		assert resultUid != null;
		return resultUid;
	}
}
