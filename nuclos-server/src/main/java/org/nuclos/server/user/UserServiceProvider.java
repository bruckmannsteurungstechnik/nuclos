package org.nuclos.server.user;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Collection;
import java.util.Date;

import org.nuclos.api.common.NuclosMandator;
import org.nuclos.api.common.NuclosRole;
import org.nuclos.api.common.NuclosUser;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.service.UserService;
import org.nuclos.common.E;
import org.nuclos.common.MandatorVO;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.security.UserVO;
import org.nuclos.common.valueobject.MandatorUserVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.server.mandator.MandatorFacadeLocal;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.nbo.AbstractBusinessObject;
import org.nuclos.server.nbo.EOBOBridge;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.nuclos.server.security.UserFacadeLocal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("userServiceProvider")
public class UserServiceProvider implements UserService {

	@Autowired
	private MasterDataFacadeLocal mdFacade;
	@Autowired
	private UserFacadeLocal userFacade;
	@Autowired
	private MandatorFacadeLocal mandatorFacade;

	@Override
	public org.nuclos.api.UID insert(final NuclosUser user) throws BusinessException {
		return insert(user, null);
	}

	@Override
	public org.nuclos.api.UID insert(final NuclosUser user, final String password) throws BusinessException {
		return insert(user, password, true);
	}

	@Override
	public org.nuclos.api.UID insert(org.nuclos.api.common.NuclosUser user, String password, boolean sendNotification) throws BusinessException {
		if (user == null) {
			throw new IllegalArgumentException("user must not be null");
		}
		if (user.getUsername() == null) {
			// set a random username
			user.setUsername(UID.generateString(10));
		}
		if (password == null) {
			password = createUserPassword();
		}

		UserVO userVO = toUserVO(user, password, sendNotification);

		validateFields(userVO, false);

		try {
			UserVO create = userFacade.create(userVO, null);
			return create.getPrimaryKey();
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void update(org.nuclos.api.common.NuclosUser user) throws BusinessException {
		if (user == null) {
			throw new IllegalArgumentException("user must not be null");
		}

		UserVO userVO = toUserVO(user, null, false);

		validateFields(userVO, false);

		try {
			userFacade.modify(userVO, null, null);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void resetPassword(final NuclosUser user, String password, final boolean sendNotification) throws BusinessException {
		if (user == null) {
			throw new IllegalArgumentException("user must not be null");
		}
		if (password == null) {
			password = createUserPassword();
		}

		UserVO userVO = toUserVO(user, password, sendNotification);

		validateFields(userVO, false);

		try {
			userFacade.modify(userVO, null, null);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public org.nuclos.api.UID insert(String username, String firstname, String lastname, String email, Boolean passwordChangeRequired) throws BusinessException {
		return insert(username, firstname, lastname, email, createUserPassword(), passwordChangeRequired);
	}
	
	@Override
	public org.nuclos.api.UID insert(String username, String firstname, String lastname, String email, String password, Boolean passwordChangeRequired) throws BusinessException {

		org.nuclos.api.UID retVal = null;

		EntityObjectVO<org.nuclos.common.UID> eoUser = new EntityObjectVO<org.nuclos.common.UID>(E.USER);
		UserVO userVO = new UserVO(new MasterDataVO<org.nuclos.common.UID>(eoUser));

		userVO.setLocked(Boolean.FALSE);
		userVO.setUsername(username);
		userVO.setEmail(email);
		userVO.setFirstname(firstname);
		userVO.setLastname(lastname);
		userVO.setPasswordChangeRequired(passwordChangeRequired);
		userVO.setLoginWithEmailAllowed(false);
		// for backward compatibility always true
		userVO.setNotifyUser(true);
		userVO.setNewPassword(password);
		userVO.setSetPassword(password != null);
		userVO.setSuperuser(Boolean.FALSE);
		userVO.setPrivacyConsentAccepted(Boolean.FALSE);

		validateFields(userVO, true);

		try {
			UserVO create = userFacade.create(userVO, null);
			retVal = create.getPrimaryKey();
			
		} catch (Exception e) {
			throw new BusinessException(e);
		}
		
		return retVal;
	}

	private static void validateFields(UserVO user, boolean bMailIsMandatory) throws BusinessException {
		if (user.getUsername() == null)
			throw new BusinessException("username must not be null");
		if (user.getFirstname() == null)
			throw new BusinessException("firstname must not be null");
		if (user.getLastname() == null)
			throw new BusinessException("lastname must not be null");
		if (bMailIsMandatory && user.getEmail() == null)
			throw new BusinessException("email must not be null");
		if (user.getPasswordChangeRequired() == null)
			throw new BusinessException("passwordChangeRequired must not be null");
	}
	
	private static String createUserPassword() {
		SecureRandom random = new SecureRandom();
		return new BigInteger(130, random).toString(32).replaceAll("", "").substring(1, 8);
	}

	private static UserVO toUserVO(NuclosUser userBO, String newPassword, Boolean sendNotification) {
		if (userBO instanceof AbstractBusinessObject) {
			AbstractBusinessObject abstractUser = (AbstractBusinessObject) userBO;

			EntityObjectVO<UID> eoUser = EOBOBridge.getEO(abstractUser);
			UserVO userVO = new UserVO(eoUser);

			userVO.setNewPassword(newPassword);
			userVO.setSetPassword(newPassword != null);
			userVO.setNotifyUser(sendNotification);

			return userVO;
		} else {
			throw new IllegalArgumentException("NuclosUser is not based on an AbstractBusinessObject");
		}
	}
	
	@Override
	public void revokeRole(Class<? extends NuclosRole> role, NuclosUser user) throws BusinessException {
		if (user == null) {
			throw new BusinessException("user must not be null");
		}
		if (role == null) {
			throw new BusinessException("userrole must not be null");
		}

		try {
			UID roleId = (UID) role.newInstance().getId();
			UID userId = (UID) user.getId();
			this.revokeRole(roleId, userId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void revokeRole(final NuclosRole role, final NuclosUser user) throws BusinessException {
		if (user == null) {
			throw new BusinessException("user must not be null");
		}
		if (role == null) {
			throw new BusinessException("userrole must not be null");
		}

		try {
			UID roleId = (UID) role.getId();
			UID userId = (UID) user.getId();
			this.revokeRole(roleId, userId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	private void revokeRole(UID roleId, UID userId) throws BusinessException,
			CommonStaleVersionException, NuclosBusinessRuleException, CommonRemoveException, CommonPermissionException, CommonFinderException {
		Collection<MasterDataVO<UID>> results = this.mdFacade.getMasterData(E.ROLEUSER,
				SearchConditionUtils.and(SearchConditionUtils.newComparison(E.ROLEUSER.role, ComparisonOperator.EQUAL, roleId),
						SearchConditionUtils.newComparison(E.ROLEUSER.user, ComparisonOperator.EQUAL, userId)));
		if (results.size() == 0) {
			throw new BusinessException("User does not have this role");
		}
		if (results.size() > 1) {
			throw new BusinessException("Incorrect number of elements found for this combination");
		}
		MasterDataVO<UID> mdvo = results.iterator().next();
		this.mdFacade.remove(mdvo.getEntityObject().getDalEntity(), mdvo.getPrimaryKey(), false);
	}

	@Override
	public org.nuclos.api.UID grantRole(Class<? extends NuclosRole> role, NuclosUser user) throws BusinessException  {
		if (user == null) {
			throw new BusinessException("user must not be null");
		}
		if (role == null) {
			throw new BusinessException("userrole must not be null");
		}

		try {
			UID roleId = (UID) role.newInstance().getId();
			UID userId = (UID) user.getId();
			return this.grantRole(roleId, userId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public org.nuclos.api.UID grantRole(final NuclosRole role, final NuclosUser user) throws BusinessException {
		if (user == null) {
			throw new BusinessException("user must not be null");
		}
		if (role == null) {
			throw new BusinessException("userrole must not be null");
		}

		try {
			UID roleId = (UID) role.getId();
			UID userId = (UID) user.getId();
			return this.grantRole(roleId, userId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	private org.nuclos.api.UID grantRole(final UID roleId, final UID userId) throws BusinessException, CommonPermissionException, NuclosBusinessRuleException, CommonCreateException {
		MasterDataVO<UID> create = null;

		EntityObjectVO<UID> newRoleUser = new EntityObjectVO<UID>(E.ROLEUSER);
		newRoleUser.setFieldUid(E.ROLEUSER.user, userId);
		newRoleUser.setFieldUid(E.ROLEUSER.role, roleId);
		create = mdFacade.create(new MasterDataVO<UID> (newRoleUser), null);

		if (create == null) {
			throw new BusinessException("user cannot be assigned to this role");
		}
		return create.getPrimaryKey();
	}

	@Override
	public void expire(NuclosUser user, Date date) throws BusinessException {
		try {
			UserVO userVO = userFacade.getByUID((UID) user.getId());
			userVO.setExpirationDate(date);
			userFacade.modify(userVO, null, null);
		} catch (CommonBusinessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public org.nuclos.api.UID grantMandator(NuclosMandator mandator,NuclosUser user) throws BusinessException {
		if (mandator == null || user == null)
			throw new BusinessException("Mandant und User müssen übergeben werden");
		MandatorVO mandatorVO = null;
		UserVO userVO = null;
		try {
			mandatorVO = mandatorFacade.getByUID((UID) mandator.getId());
			userVO = userFacade.getByUID((UID) user.getId());
			return userFacade.grantMandator(new MandatorUserVO(mandatorVO, userVO));
		} catch (CommonBusinessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public boolean isMandatorGranted(NuclosMandator mandator,NuclosUser user) throws BusinessException {
		MandatorVO mandatorVO = null;
		UserVO userVO = null;
		try {
			mandatorVO = mandatorFacade.getByUID((UID) mandator.getId());
			userVO = userFacade.getByUID((UID) user.getId());
			return userFacade.isMandatorGranted(userVO, mandatorVO);
		} catch (CommonBusinessException e) {
			throw new BusinessException(e);
		}

	}

	@Override
	public void revokeMandator(NuclosMandator mandator, NuclosUser user) throws BusinessException {
		MandatorVO mandatorVO = null;
		UserVO userVO = null;
		try {
			mandatorVO = mandatorFacade.getByUID((UID) mandator.getId());
			userVO = userFacade.getByUID((UID) user.getId());
			userFacade.revokeMandator(userVO,mandatorVO	);
		} catch (CommonBusinessException e) {
			throw new BusinessException(e);
		}
	}

}
