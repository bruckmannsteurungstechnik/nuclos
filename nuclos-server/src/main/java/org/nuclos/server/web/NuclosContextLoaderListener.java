//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.server.web;


import java.io.FileNotFoundException;
import java.sql.Driver;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.annotation.WebListener;

import org.apache.activemq.broker.TransportConnector;
import org.apache.activemq.thread.Scheduler;
import org.apache.activemq.xbean.XBeanBrokerService;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Appender;
import org.apache.log4j.Category;
import org.apache.log4j.Level;
import org.apache.log4j.spi.HierarchyEventListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configurator;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.E;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.server.cluster.NuclosClusterRegisterHelper;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.ejb3.SecurityFacadeBean;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.nuclos.server.report.ejb3.JRJavaxToolsCompiler;
import org.nuclos.server.spring.AutoDbSetupComplete;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.listener.SimpleMessageListenerContainer;
import org.springframework.jndi.JndiTemplate;
import org.springframework.util.Log4jConfigurer;
import org.springframework.util.ResourceUtils;
import org.springframework.util.SystemPropertyUtils;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.Log4jWebConfigurer;
import org.springframework.web.util.WebUtils;

//import com.sun.tools.javac.jvm.Target;
//import org.nuclos.server.cluster.NuclosClusterRegisterHelper;
//import org.nuclos.server.cluster.NuclosClusterRegisterHelper;

/**
 * <p>
 * There are still issues with closing down ActiveMQ:
 * <ul>
 *   <li>https://issues.apache.org/jira/browse/AMQ-3451</li>
 * </ul>
 * <p>
 * There are still issues with ThreadLocal variables. Better avoid and/or clean them.
 */
@WebListener
public class NuclosContextLoaderListener extends ContextLoaderListener {

	public static final String JNDI_LOG4J_PATH = "java:comp/env/nuclos-conf-log4j";
	
	private Logger log;

	@Override
	public void contextInitialized(ServletContextEvent event) {
		// set system properties
		// from ReportFacadeBean
		System.setProperty("jasper.reports.compile.keep.java.file", 
				NuclosSystemParameters.getString(NuclosSystemParameters.JASPER_REPORTS_COMPILE_KEEP_JAVA_FILE));
		System.setProperty("jasper.reports.compile.temp", 
				NuclosSystemParameters.getString(NuclosSystemParameters.JASPER_REPORTS_COMPILE_TMP));
		System.setProperty("jasper.reports.compiler.class", 
				JRJavaxToolsCompiler.class.getName());
		// @see NUCLOS-2831
		// probably a non-op, as jdt is NOT used (see http://wiki.nuclos.de/display/Konfiguration/Nuclos+Java+Compiler) (tp)
		// removed @see NUCLOS-4230
		// System.setProperty("org.eclipse.jdt.core.compiler.source", Target.JDK1_6.name);
		// System.setProperty("org.eclipse.jdt.core.compiler.compliance", Target.JDK1_6.name);
		// System.setProperty("org.eclipse.jdt.core.compiler.codegen.TargetPlatform", Target.JDK1_6.name);
		
		initLogging(event.getServletContext());

		super.contextInitialized(event);
		
		this.log = LoggerFactory.getLogger(this.getClass());
		final WebApplicationContext wac = getCurrentWebApplicationContext();
		
		// final Timer timer = (Timer) SpringApplicationContextHolder.getBean("timer");
		final Timer timer = (Timer) wac.getBean("timer");
		
		// startup
		final TimerTask startup = new TimerTask() {

			@Override
			public void run() {
				try {
					wac.getBean(SpringDataBaseHelper.class).autoDbSetup(E.getThis(), ApplicationProperties.getInstance().getNuclosVersion());
					NucletExtensionLoader.updateAndLoadServerExtensions();
					wac.getBean(AutoDbSetupComplete.class).afterAutoDbSetup();
					
					final TimerTask heartbeat = new TimerTask() {
						private int i = 0;
						
						@Override
						public void run() {
							try {
								NuclosJMSUtils.sendMessage(Integer.toString(++i), JMSConstants.TOPICNAME_HEARTBEAT);
							} catch (Exception e) {
								log.error("Send of heart beat in timer task failed: {}", e, e);
							}
						}
					};
					
					//In development modes repeating server heart-beats each few minutes are spared. They only disturb the 
					//developing process and have no use when not in production.
					
					if (NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_DEVELOPMENT)) {
						//On development mode, the heart-beat is spared for now.
						//When there is need for a repeating heart beat, too, it should be a way longer interval.
						
						//timer.schedule(heartbeat, JMSConstants.JMS_HEARTBEAT_INTERVAL, JMSConstants.JMS_HEARTBEAT_MUCHLONGER_INTERVAL);
						
					} else {
						timer.schedule(heartbeat, JMSConstants.JMS_HEARTBEAT_INTERVAL, JMSConstants.JMS_HEARTBEAT_INTERVAL);
						
					}

				} catch (Exception e) {
					log.error("startup of heart beat server thread in timer task failed: {}", e, e);
				}
			}
			
		};

		timer.schedule(startup, 2000);					

	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		_contextDestroyed(event);
		
		System.gc();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// Ignore
			// Ok! (tp)
			// e.printStackTrace();
			log.warn("In contextDestroyed sleep failed: {}", e, e);
		}
		System.gc();
	}
	
	private void _contextDestroyed(ServletContextEvent event) {

		// FIXME: The preDestroy method should be called automatically by Spring.
		try {
			SecurityFacadeBean securityFacadeBean = SpringApplicationContextHolder.getBean(SecurityFacadeBean.class);
			securityFacadeBean.preDestroy();
		} catch (Exception e) {
			log.error("SecurityFacade preDestroy failed", e);
		}

		//The next call throws an exception when there is no cluster which was not caught (!) and thus stopping the complete shutdown,
		//hanging the server forever.
		
		if (NuclosClusterRegisterHelper.runInClusterMode()) {
			try {
				NuclosClusterRegisterHelper.deregisterClusterServer();			
			} catch (NuclosFatalException nfe) {
				log.error(nfe.getMessage(), nfe);
			}
		}
		
		// We perform some additional clean-up actions to free
		// all system classes from Nuclos references.
		// Any such reference will lead to a memory leak after
		// undeploying/stopping the server because it prevents
		// Java from releasing and garbage-collecting the app.
		SpringDataBaseHelper.getInstance().getDbAccess().shutdown();		

		final ClassLoader cl = Thread.currentThread().getContextClassLoader(); // used for checking

		// Get some beans before we destroy the context
		final XBeanBrokerService activeMQBroker = (XBeanBrokerService) SpringApplicationContextHolder.getBean("broker");
		final org.quartz.Scheduler quartz = (org.quartz.Scheduler) SpringApplicationContextHolder.getBean("nuclosScheduler");
		final Scheduler activeMQScheduler = activeMQBroker.getScheduler();
		final SimpleMessageListenerContainer listenerContainer = (SimpleMessageListenerContainer) 
				SpringApplicationContextHolder.getBean("listener.masterdataCache");

		// Let Spring perform its default clean-ups...
		super.contextDestroyed(event);

		// Shutdown ActiveMQ thread and broker
		try {
			if (checkClass(cl, listenerContainer)) {
				listenerContainer.stop();
				listenerContainer.destroy();
				log.info("Shutdown Jms listener container: done");
			}
		} catch (Exception ex) {
			log.warn("Unable to shutdown JMS listener container", ex);
		}
		try {
			if (checkClass(cl, activeMQScheduler)) {
				activeMQScheduler.stop();
				activeMQScheduler.shutdown();
				log.info("Shutdown MQ scheduler: done");
			}
		} catch (Exception ex) {
			log.warn("Unable to shutdown MQ scheduler", ex);
		}
		try {
			if (checkClass(cl, activeMQBroker)) {
				List<TransportConnector> tcs = activeMQBroker.getTransportConnectors();
				for (TransportConnector tc: tcs) {
					tc.stop();
				}
				activeMQBroker.getTaskRunnerFactory().shutdown();
				
				activeMQBroker.stop();
				activeMQBroker.waitUntilStopped();
				activeMQBroker.destroy();
				log.info("Shutdown MQ broker: done");
			}
		} catch (Exception ex) {
			log.warn("Unable to shutdown MQ broker", ex);
		}
		try {
			if (checkClass(cl, quartz)) {
				quartz.shutdown(true);
				log.info("Shutdown Quartz/SchedulerFactoryBean: done");
			}
		} catch (Exception ex) {
			log.warn("Unable to shutdown quartz", ex);
		}

		// Unregister all JDBC driver registered by this webapp
		// See also DBCP-332: possible memory leak during Tomcat context reloads
		Enumeration<Driver> drivers = DriverManager.getDrivers();
		List<Driver> webAppDrivers = new ArrayList<Driver>();
		while (drivers.hasMoreElements()) {
			Driver driver = drivers.nextElement();
			if (driver.getClass().getClassLoader() == cl)
				webAppDrivers.add(driver);
		}
		for (Driver driver : webAppDrivers) {
			try {
				DriverManager.deregisterDriver(driver);
				log.info("Deregister db driver: " + driver);
			} catch (Exception ex) {
				log.warn("Unable to deregister driver {}", driver, ex);
			}
			log.info("Deregister db driver: done");
		}

		shutdownLogging(event.getServletContext());
		// TODO: there are still some "open" references...
	}
	
	private boolean checkClass(ClassLoader cl, Object o) {
		if (o == null) return false;
		final Class<?> clazz = o.getClass();
		final String classname = clazz.getName();
		Class<?> clclazz;
		try {
			clclazz = cl.loadClass(classname);
		} catch (ClassNotFoundException e) {
			return false;
		}
		return clazz.equals(clclazz);
	}

	/**
	 * Initialize log4j logging from JNDI configuration (or default paths).
	 * @param servletContext the current ServletContext
	 * @see Log4jWebConfigurer#initLogging
	 */
	private static void initLogging(ServletContext servletContext) {
		String location = null;
		try {
			JndiTemplate template = new JndiTemplate();
			location = template.lookup(JNDI_LOG4J_PATH, String.class);
		}
		catch (NamingException ex) {
			// Ok! (tp)
			System.out.println(JNDI_LOG4J_PATH + " not found.");
		}

		if (location == null) {
			location = "WEB-INF/log4j2.xml";
		}

		if (location != null) {
			// Perform actual log4j initialization; else rely on log4j's default initialization.
			try {
				// Return a URL (e.g. "classpath:" or "file:") as-is;
				// consider a plain file path as relative to the web application root directory.
				if (!ResourceUtils.isUrl(location)) {
					// Resolve system property placeholders before resolving real path.
					location = SystemPropertyUtils.resolvePlaceholders(location);
					location = WebUtils.getRealPath(servletContext, location);
				}

				// Write log message to server log.
				servletContext.log("Initializing log4j from [" + location + "]");
				
				//System.setProperty("log4j.configurationFile", location);
				// ...is too late here...
				Configurator.initialize(null, location);
			}
			catch (FileNotFoundException ex) {
				throw new IllegalArgumentException("Invalid 'log4jConfigLocation' parameter: " + ex.getMessage());
			}
		}
	}

	/**
	 * Shut down log4j.
	 * @param servletContext the current ServletContext
	 * @see Log4jWebConfigurer#shutdownLogging
	 */
	private void shutdownLogging(ServletContext servletContext) {
		servletContext.log("Shutting down log4j (servletContext)");
		log.info("Shutting down log4j");
		try {
			Log4jConfigurer.shutdownLogging();
		}
		catch (Exception ex) {
			servletContext.log("log4j shutdown exception", ex);
		}
		log = null;
		servletContext.log("Shutdown logging: done");
	}


}
