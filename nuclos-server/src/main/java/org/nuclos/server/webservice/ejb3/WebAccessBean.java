//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.server.webservice.ejb3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIdCondition;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Factories;
import org.nuclos.common.collection.LazyInitMapWrapper;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.security.Permission;
import org.nuclos.server.common.MasterDataPermission;
import org.nuclos.server.common.MasterDataPermissions;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.ModulePermission;
import org.nuclos.server.common.ModulePermissions;
import org.nuclos.server.common.ejb3.SecurityFacadeLocal;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.genericobject.ejb3.GenericObjectFacadeLocal;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.springframework.beans.factory.annotation.Autowired;

//Note2self: @EndpointConfig(configName="Standard WSSecurity Endpoint")
//config in jboss-4.2.2.GA/server/nuclos/deploy/jbossws.sar/META-INF
//Must include JBOSS_HOME/server/default/deploy/jbossws.sar/jbossws-core.jar in classpath for build

// @Stateless
// @Local(WebAccessWS.class)
public class WebAccessBean implements WebAccessWS {
	
	private SecurityFacadeLocal securityFacade;
//	private RuleEngineFacadeLocal ruleEngineFacade;
	private GenericObjectFacadeLocal genericObjectFacade;
	
	@Autowired
	public void setSecurityFacade(SecurityFacadeLocal securityFacade) {
		this.securityFacade = securityFacade;
	}

//	@Autowired
//	public void setRuleEngineFacade(RuleEngineFacadeLocal ruleEngineFacade) {
//		this.ruleEngineFacade = ruleEngineFacade;
//	}

	@Autowired
	public void setGenericObjectFacade(GenericObjectFacadeLocal genericObjectFacade) {
		this.genericObjectFacade = genericObjectFacade;
	}

	@RolesAllowed("Login")
	@Override
	public ArrayList<String> listEntities() {
		MasterDataPermissions masterDataPermissions = securityFacade.getMasterDataPermissions();
		ModulePermissions modulePermissions = securityFacade.getModulePermissions();

		ArrayList<String> res = new ArrayList<String>();
		for(EntityMeta e : MetaProvider.getInstance().getAllEntities())
			if(canReadEntity(e.getUID(), masterDataPermissions, modulePermissions))
				res.add(e.getEntityName());
		
		Collections.sort(res);
		return res;
	}
	
	private boolean canReadEntity(final UID entityUid, final MasterDataPermissions masterDataPermissions, final ModulePermissions modulePermissions) {
		MasterDataPermission m = masterDataPermissions.get(entityUid);
		if(m != null && MasterDataPermission.includesReading(m))
			return true;
		
		ModulePermission g = modulePermissions.getPermissionsByModuleUid().get(new Pair<UID, UID>(entityUid, null));
		if(g != null && ModulePermission.includesReading(g))
			return true;
		
		return false;
	}
	
	
	
	@Override
    public ArrayList<Long> list(final UID entityUid) {
		final IEntityObjectProcessor<Object> proc = NucletDalProvider.getInstance().getEntityObjectProcessor(entityUid);
		final List<EntityObjectVO<Object>> dbRes = proc.getBySearchExprResultParams(new CollectableSearchExpression(null), new ResultParams(null, true));
		
		List<Long> res = CollectionUtils.transform(dbRes, new Transformer<EntityObjectVO<?>, Long>() {

			@Override
			public Long transform(EntityObjectVO<?> i) {
				return (Long) i.getId();
			}

			
		});
		
		return new ArrayList<Long>(res);
    }

	@Override
    public ArrayList<String> read(final UID entityUid, Long id) {
		IEntityObjectProcessor proc = NucletDalProvider.getInstance().getEntityObjectProcessor(entityUid);
		List<EntityObjectVO> dbRes =
			proc.getBySearchExprResultParams(new CollectableSearchExpression(new CollectableIdCondition(id)), new ResultParams(null, true));
		if(dbRes.isEmpty())
			return null;
		
		return entityObjectToString(dbRes.get(0));
    }
		
	private ArrayList<String> entityObjectToString(EntityObjectVO<Long> obj) {
		Map<UID, Permission> attributePermissions;

		final UID stateId = obj.getFieldUid(SF.STATE_UID);
		if(stateId != null)
			attributePermissions = new LazyInitMapWrapper<UID, Permission>(
				securityFacade.getAttributePermissionsByEntity(obj.getDalEntity(), stateId),
				Factories.constFactory(Permission.NONE));
		else
			attributePermissions = new LazyInitMapWrapper<UID, Permission>(
				new HashMap<UID, Permission>(),
				Factories.constFactory(Permission.READONLY));
		
		final ArrayList<String> res = new ArrayList<String>();

		for(final Map.Entry<UID, Object> e : obj.getFieldValues().entrySet())
			if(attributePermissions.get(e.getKey()).includesReading())
				res.add(e.getKey() + "=" + (e.getValue() != null ? e.getValue().toString() : ""));
		
		return res;
	}
	
	
	@Override
    public void executeBusinessRule(final UID entityUid, Object id, final UID ruleUid) {
//		try {
//	        RuleVO rule = ruleEngineFacade.get(ruleUid);
//	        
//	        RuleObjectContainerCVO ruleContainer;
//	        EntityMeta meta = MetaProvider.getInstance().getEntity(entityUid);
//	        if(meta.isStateModel())
//	        	ruleContainer = genericObjectFacade.getRuleObjectContainerCVO(Event.INTERFACE, (Long) id, null);
//	        else
//	        	ruleContainer = ServerServiceLocator.getInstance().getFacade(MasterDataFacadeLocal.class).getRuleObjectContainerCVO(Event.INTERFACE, entityUid, id, null);
//	        
//	        ruleEngineFacade.executeBusinessRules(
//	        	CollectionUtils.asList(rule),
//	        	ruleContainer,
//	        	false, null);
//        }
//        catch(NuclosBusinessRuleException e) {
//        	throw new CommonFatalException(e);
//        }
//        catch(CommonFinderException e) {
//        	throw new CommonFatalException(e);
//        }
//        catch(CommonPermissionException e) {
//        	throw new CommonFatalException(e);
//        }
//        catch(NuclosBusinessException e) {
//        	throw new CommonFatalException(e);
//        }
	}
}
