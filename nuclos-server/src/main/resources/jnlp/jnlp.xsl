<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" />

	<xsl:param name="codebase" />
	<xsl:param name="href" />
	<xsl:param name="jnlp.packEnabled" />
	<xsl:param name="jnlp.concurrentDownloads" />
	<xsl:param name="server" />
	<xsl:param name="singleinstance" />
	<xsl:param name="nuclos.version" />
	<!--xsl:param name="extensions" /-->
	<xsl:param name="version" />
	<!--xsl:param name="extension-version" /-->
	<xsl:param name="max-heap-size" />
	<xsl:param name="customerIcon" />
	<xsl:param name="splashScreen" />

	<xsl:template match="/">
		<!-- JNLP File for webstart client -->
		<jnlp spec="7.0+" codebase="{$codebase}" href="{$href}" version="{$version}">
			<information>
				<title>Nuclos</title>
				<vendor>Novabit Informationssysteme GmbH</vendor>
				<homepage href="http://www.nuclos.de" />
				<description>Nuclos Webstart Client</description>
				<xsl:if test="$customerIcon='true'">
					<icon href="customer-icon.gif" />
				</xsl:if>
				<xsl:if test="$splashScreen='true'">
					<icon href="splash-screen.gif" kind="splash" />
				</xsl:if>
				<!-- 
					Don't enable offline allowed. As with java 1.6.0_33, it seems that there is no 
					caching update even with <update/>. (tp)
				-->
				<!-- offline-allowed/ -->
			</information>
			<security>
				<all-permissions />
			</security>
			<update check="always" policy="always"/>
			<resources>
				<property name="sun.java2d.xrender" value="false"/>
				<!--
					-XX:+HeapDumpOnOutOfMemoryError
					will only be used by jse7 because it is not mention at
					http://docs.oracle.com/javase/6/docs/technotes/guides/javaws/developersguide/syntax.html
					
					See also:
					http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=6664424
				-->
				<java version="1.8+" initial-heap-size="256m" max-heap-size="{$max-heap-size}"
					java-vm-args="
--add-opens=java.base/java.lang.reflect=ALL-UNNAMED
--add-opens=java.base/java.lang=ALL-UNNAMED
--add-opens=java.base/java.text=ALL-UNNAMED
--add-opens=java.base/java.util=ALL-UNNAMED
--add-opens=java.desktop/com.sun.java.swing.plaf.windows.resources=ALL-UNNAMED
--add-opens=java.desktop/com.sun.swing.internal.plaf.basic.resources=ALL-UNNAMED
--add-opens=java.desktop/java.awt.font=ALL-UNNAMED
-ea
-verbose:gc
-XX:+CMSClassUnloadingEnabled
-XX:+HeapDumpOnOutOfMemoryError
-XX:+UseG1GC
-XX:+UseStringDeduplication
-XX:+UseThreadPriorities
"/>
				<xsl:for-each select="jnlp/themes/theme">
					<extension name="theme-{@name}" href="extensions/themes/theme-{@name}.jnlp" version="{@version}" />
				</xsl:for-each>
				<xsl:for-each select="jnlp/extensions/extension">
					<extension name="extension-{@name}" href="extensions/extension-{@name}.jnlp" version="{@version}" />
				</xsl:for-each>
				<!--xsl:if test="$extensions='true'"-->
					<!--extension name="extension" href="extensions/extension.jnlp" version="{$extension-version}" /-->
				<!--/xsl:if-->
				<xsl:for-each select="jnlp/jars/jar">
					<jar href="{text()}" version="{@version}" download="{@download}" main="{@main}" />
				</xsl:for-each>
				<!-- Attention: java from 7u41 onwards is VERY picky about accepting properties.
					 That's why we introduced client.properties! (tp) -->
				<property name="jnlp.versionEnabled" value="true"/>
				<property name="jnlp.packEnabled" value="{$jnlp.packEnabled}" />
				<property name="jnlp.concurrentDownloads" value="{$jnlp.concurrentDownloads}" />
				<property name="server" value="{$server}" />
				<property name="log4j2.url" value="{$codebase}/log4j2.xml" />
				<property name="nuclos.client.singleinstance" value="{$singleinstance}" />
				<property name="java.util.prefs.PreferencesFactory" value="org.nuclos.common.preferences.NuclosPreferencesFactory" />
			</resources>
			<application-desc main-class="org.nuclos.client.main.Nuclos">
				<xsl:for-each select="jnlp/arguments/argument">
					<argument>
						<xsl:value-of select="." />
					</argument>
				</xsl:for-each>
			</application-desc>
		</jnlp>
	</xsl:template>
</xsl:stylesheet>
