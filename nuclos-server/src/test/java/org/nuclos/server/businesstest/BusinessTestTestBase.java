package org.nuclos.server.businesstest;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.NotImplementedException;
import org.junit.BeforeClass;
import org.nuclos.api.rule.CustomRule;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.businesstest.codegeneration.BusinessTestClassGenerator;
import org.nuclos.server.businesstest.codegeneration.BusinessTestClassLoader;
import org.nuclos.server.businesstest.codegeneration.BusinessTestGenerationContext;
import org.nuclos.server.businesstest.codegeneration.source.BusinessTestEntitySource;
import org.nuclos.server.eventsupport.valueobject.EventSupportEventVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.statemodel.valueobject.StateVO;

import groovy.lang.GroovyClassLoader;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class BusinessTestTestBase {
	protected static Map<UID, NucletVO> nuclets = new HashMap<>();

	@BeforeClass
	public static void setup() {
		final NucletVO nucletA = new NucletVO(new UID("NucletA"), "nuclet.a");
		final NucletVO nucletB = new NucletVO(new UID("NucletB"), "nuclet.b");

		nuclets.put(nucletA.getUid(), nucletA);
		nuclets.put(nucletB.getUid(), nucletB);
	}

	protected BusinessTestGenerationContext getContext() {
		final Collection<EntityMeta<?>> entities = getEntities();
		EventSupportEventVO customRule = new EventSupportEventVO("test.TestClassName", CustomRule.class.getCanonicalName(), A.UID, null, null, null, null, null, 1);
		final List<EventSupportEventVO> eventSupportEntities = Arrays.asList(customRule);
		final List<BusinessTestGenerationContext.Process> processes = Arrays.asList(
				new BusinessTestGenerationContext.Process(new UID("Aktion1"), "Aktion 1", new UID("A")),
				new BusinessTestGenerationContext.Process(new UID("Aktion2"), "Aktion 2", new UID("A"))
		);

		return new BusinessTestGenerationContext(entities, eventSupportEntities, processes) {
			@Override
			protected NucletVO getNuclet(final UID nucletUID) {
				return nuclets.get(nucletUID);
			}

			@Override
			public String getEntityMetaID(final UID entityUID) {
				if (A.UID.equals(entityUID)) {
					return A.META_ID;
				} else if (B.UID.equals(entityUID)) {
					return B.META_ID;
				}
				throw new NotImplementedException("Missing entity UID: " + entityUID);
			}

			@Override
			public Collection<StateVO> getEntityStates(final UID entityUID) {
				// TODO: Implement this and test generation with states
				throw new NotImplementedException();
			}
		};
	}

	protected Collection<EntityMeta<?>> getEntities() {
		Collection<EntityMeta<?>> result = new ArrayList<>();

		result.add(new A());
		result.add(new B());

		result.add(E.USER);

		return result;
	}

	protected Map<UID, BusinessTestEntitySource> generateClasses() throws CommonFinderException, CommonPermissionException {
		final BusinessTestGenerationContext context = getContext();
		final BusinessTestClassGenerator generator = new BusinessTestClassGenerator(context);
		final Map<UID, BusinessTestEntitySource> entitySources = generator.generateClasses();

		return entitySources;
	}

	protected GroovyClassLoader loadEntityClasses() throws IOException, CommonFinderException, CommonPermissionException {
		Map<UID, BusinessTestEntitySource> entitySources = generateClasses();

		assert !entitySources.isEmpty();
		assert entitySources.size() == getContext().getBusinessEntities().size();

		final File outputDir = Files.createTempDirectory("BusinessTestClassGeneratorTest").toFile();
		final BusinessTestClassLoader loader = new BusinessTestClassLoader(outputDir);
		final GroovyClassLoader groovyClassLoader = loader.compileSources(entitySources);

		assert groovyClassLoader.getLoadedClasses().length > 0;
		assert groovyClassLoader.getLoadedClasses().length == entitySources.size();

		return groovyClassLoader;
	}

	// @formatter:off
	public static class A extends EntityMeta<Long> {
		public static final UID UID = new UID("A");
		public static String META_ID = "nuclet_a_A";
		@Override public UID getUID() { return UID; }
		@Override public String getDbTable() { return null; }
		@Override public String getEntityName() { return "A"; }
		@Override public Class<Long> getPkClass() { return null; }
		@Override public UID getNuclet() { return new UID("NucletA"); }

		public static final FieldMeta<String> Name = new FieldMeta.Valueable<String>() {
			@Override public UID getUID() { return new UID("A_Name"); }
			@Override public UID getEntity() { return A.UID; }
			@Override public String getFieldName() { return "Name"; }
			@Override public String getDbColumn() { return null; }
			@Override public Class<String> getJavaClass() { return String.class; }
			@Override public UID getForeignEntity() { return null; }
			@Override public boolean isNullable() { return false; }
		};

		public static final FieldMeta<Integer> IntegerField = new FieldMeta.Valueable<Integer>() {
			@Override public UID getUID() { return new UID("A_IntegerField"); }
			@Override public UID getEntity() { return A.UID; }
			@Override public String getFieldName() { return "IntegerField"; }
			@Override public String getDbColumn() { return null; }
			@Override public Class<Integer> getJavaClass() { return Integer.class; }
			@Override public UID getForeignEntity() { return null; }
			@Override public boolean isNullable() { return false; }
		};

		public static final FieldMeta<Double> DoubleField = new FieldMeta.Valueable<Double>() {
			@Override public UID getUID() { return new UID("A_DoubleField"); }
			@Override public UID getEntity() { return A.UID; }
			@Override public String getFieldName() { return "DoubleField"; }
			@Override public String getDbColumn() { return null; }
			@Override public Class<Double> getJavaClass() { return Double.class; }
			@Override public UID getForeignEntity() { return null; }
			@Override public boolean isNullable() { return false; }
		};

		public static final FieldMeta<BigDecimal> BigDecimalField = new FieldMeta.Valueable<BigDecimal>() {
			@Override public UID getUID() { return new UID("A_BigDecimalField"); }
			@Override public UID getEntity() { return A.UID; }
			@Override public String getFieldName() { return "BigDecimalField"; }
			@Override public String getDbColumn() { return null; }
			@Override public Class<BigDecimal> getJavaClass() { return BigDecimal.class; }
			@Override public UID getForeignEntity() { return null; }
			@Override public boolean isNullable() { return false; }
		};

		public static final FieldMeta<Date> DateField = new FieldMeta.Valueable<Date>() {
			@Override public UID getUID() { return new UID("A_DateField"); }
			@Override public UID getEntity() { return A.UID; }
			@Override public String getFieldName() { return "DateField"; }
			@Override public String getDbColumn() { return null; }
			@Override public Class<Date> getJavaClass() { return Date.class; }
			@Override public UID getForeignEntity() { return null; }
			@Override public boolean isNullable() { return false; }
		};

		public static final FieldMeta<UID> RefField = new FieldMeta<UID>() {
			@Override public UID getUID() { return new UID("A_RefField"); }
			@Override public UID getEntity() { return A.UID; }
			@Override public String getFieldName() { return "RefField"; }
			@Override public String getDbColumn() { return null; }
			@Override public Class<UID> getJavaClass() { return UID.class; }
			@Override public UID getForeignEntity() { return B.UID; }
			@Override public boolean isNullable() { return false; }
		};

		public static final FieldMeta<String> BByRefA1 = new FieldMeta.Valueable<String>() {
			@Override public UID getUID() { return new UID("A_BByRefA1"); }
			@Override public UID getEntity() { return A.UID; }
			@Override public String getFieldName() { return "BByRefA1"; }
			@Override public String getDbColumn() { return null; }
			@Override public Class<String> getJavaClass() { return String.class; }
		};

		public static final FieldMeta<UID> RefUser = new FieldMeta.Valueable<UID>() {
			@Override public UID getUID() { return new UID("A_RefUser"); }
			@Override public UID getEntity() { return A.UID; }
			@Override public UID getForeignEntity() { return E.USER.getUID(); }
			@Override public String getFieldName() { return "RefUser"; }
			@Override public String getDbColumn() { return null; }
			@Override public Class<UID> getJavaClass() { return UID.class; }
		};

		public static final FieldMeta<Integer> _1zahl = new FieldMeta.Valueable<Integer>() {
			@Override public UID getUID() { return new UID("A_1zahl"); }
			@Override public UID getEntity() { return A.UID; }
			@Override public UID getForeignEntity() { return null; }
			@Override public String getFieldName() { return "1zahl"; }
			@Override public String getDbColumn() { return null; }
			@Override public Class<Integer> getJavaClass() { return Integer.class; }
		};
	}

	public static class B extends EntityMeta<Long> {
		public static final UID UID = new UID("B");
		public static String META_ID = "nuclet_b_B";
		@Override public UID getUID() { return UID; }
		@Override public String getDbTable() { return null; }
		@Override public String getEntityName() { return "B"; }
		@Override public Class<Long> getPkClass() { return null; }
		@Override public UID getNuclet() { return new UID("NucletB"); }

		public static final FieldMeta<UID> RefA1 = new FieldMeta<UID>() {
			@Override public UID getUID() { return new UID("B_RefA1"); }
			@Override public UID getEntity() { return B.UID; }
			@Override public String getFieldName() { return "Ref A 1"; }
			@Override public String getDbColumn() { return null; }
			@Override public Class<UID> getJavaClass() { return UID.class; }
			@Override public UID getForeignEntity() { return A.UID; }
		};

		public static final FieldMeta<UID> RefA2 = new FieldMeta<UID>() {
			@Override public UID getUID() { return new UID("B_RefA2"); }
			@Override public UID getEntity() { return B.UID; }
			@Override public String getFieldName() { return "Ref A 2"; }
			@Override public String getDbColumn() { return null; }
			@Override public Class<UID> getJavaClass() { return UID.class; }
			@Override public UID getForeignEntity() { return A.UID; }
		};

		public static final FieldMeta<GenericObjectDocumentFile> DocumentAttachment = new FieldMeta<GenericObjectDocumentFile>() {
			@Override public UID getUID() { return new UID("DocumentAttachment"); }
			@Override public UID getEntity() { return B.UID; }
			@Override public String getFieldName() { return "Document Attachment"; }
			@Override public String getDbColumn() { return null; }
			@Override public Class<GenericObjectDocumentFile> getJavaClass() { return GenericObjectDocumentFile.class; }
			@Override public UID getForeignEntity() { return new UID("zKm4a"); }
		};
	}
	// @formatter:on
}
