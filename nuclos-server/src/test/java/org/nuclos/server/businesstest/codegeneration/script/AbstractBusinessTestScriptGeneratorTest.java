package org.nuclos.server.businesstest.codegeneration.script;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;
import org.junit.Test;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.file.FileNameMapper;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.businesstest.BusinessTestTestBase;
import org.nuclos.server.businesstest.IBusinessTestLogger;
import org.nuclos.server.businesstest.NucletVO;
import org.nuclos.server.businesstest.codegeneration.IBusinessTestNucletCache;
import org.nuclos.server.businesstest.codegeneration.IBusinessTestSampleDataProvider;
import org.nuclos.server.businesstest.codegeneration.source.BusinessTestScriptSource;

import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyShell;
import groovy.lang.Script;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public abstract class AbstractBusinessTestScriptGeneratorTest extends BusinessTestTestBase {

	protected abstract AbstractBusinessTestScriptGenerator newGenerator();

	AbstractBusinessTestScriptGenerator generator = newGenerator();

	@Test
	public void testValidName() {
		assert !FileNameMapper.mustBeEscaped(generator.getTestName());
	}

	private IBusinessTestNucletCache getNucletCache() {
		return new IBusinessTestNucletCache() {
			@Override
			public NucletVO getNuclet(final UID nucletUID) {
				return nuclets.get(nucletUID);
			}

			@Override
			public NucletVO getNucletByPackage(final String pkg) {
				throw new NotImplementedException("TODO: Implement me");
			}
		};
	}

	BusinessTestScriptGeneratorFactory getFactory(EntityMeta<?> entity) {
		return new BusinessTestScriptGeneratorFactory(
				entity,
				getNucletCache(),
				new TestLogger(),
				new TestDataProvider(),
				getContext()
		);
	}

	/**
	 * Generates the necessary entity classes and parses the groovy script.
	 */
	ScriptResult parseScript() throws CommonFinderException, IOException, CommonPermissionException {
		final BusinessTestScriptSource scriptSource = generator.generateScriptSource();
		final String groovy = scriptSource.getGroovySource();

		assert groovy != null;
		System.out.println(groovy);

		GroovyClassLoader cl = loadEntityClasses();
		final GroovyShell groovyShell = new GroovyShell(cl);
		Script script = groovyShell.parse(groovy);

		return new ScriptResult(script, groovy);
	}

	class ScriptResult {
		final Script script;
		final String groovySource;

		ScriptResult(final Script script, final String groovySource) {
			this.script = script;
			this.groovySource = groovySource;
		}

		public Script getScript() {
			return script;
		}

		String getGroovySource() {
			return groovySource;
		}
	}

	class TestDataProvider implements IBusinessTestSampleDataProvider {
		@Override
		public <T> T getSampleData(final FieldMeta<T> field) {
			if (String.class.isAssignableFrom(field.getJavaClass())) {
				return (T) "Sample Data";
			} else if (Boolean.class.isAssignableFrom(field.getJavaClass())) {
				return (T) Boolean.TRUE;
			} else if (Date.class.isAssignableFrom(field.getJavaClass())) {
				try {
					return (T) new SimpleDateFormat("dd.MM.yyyy").parse("01.01.2016");
				} catch (ParseException e) {
					throw new RuntimeException(e);
				}
			} else if (Integer.class.isAssignableFrom(field.getJavaClass())) {
				return (T) new Integer(42);
			} else if (Long.class.isAssignableFrom(field.getJavaClass())) {
				return (T) new Long(12345678901l);
			} else if (Double.class.isAssignableFrom(field.getJavaClass())) {
				return (T) new Double(42.0);
			} else if (BigDecimal.class.isAssignableFrom(field.getJavaClass())) {
				return (T) BigDecimal.ZERO;
			} else {
				return null;
			}
		}

		@Override
		public <T> List<T> getSampleData(final FieldMeta<T> field, final int count) {
			return null;
		}
	}

	class TestLogger implements IBusinessTestLogger {
		@Override
		public boolean isDebug() {
			return false;
		}

		@Override
		public void setDebug(final boolean debug) {

		}

		@Override
		public void debugln(final String message) {
			if (isDebug()) {
				System.out.println(message);
			}
		}

		@Override
		public void println(final String message) {
			System.out.println(message);
		}

		@Override
		public void print(final String message) {
			System.out.print(message);
		}

		@Override
		public void printError(final String message, final Throwable t) {
			System.out.println(message);
			t.printStackTrace();
		}

		@Override
		public void printProgress(final BigDecimal progress) {
			println("Progress: " + progress);
		}

		@Override
		public void printProgress(final int current, final int max) {
			println("Progress: " + current + " / " + max);
		}
	}
}
