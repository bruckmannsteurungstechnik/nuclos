package org.nuclos.server.businesstest.codegeneration.script;

import java.io.IOException;

import org.junit.Test;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class BusinessTestCustomRuleScriptGeneratorTest extends AbstractBusinessTestScriptGeneratorTest {

	@Test
	public void testGenerator() throws CommonFinderException, IOException, CommonPermissionException {
		assert "CUSTOMRULE B TestRule".equals(generator.getTestName());

		AbstractBusinessTestScriptGeneratorTest.ScriptResult script = parseScript();

		assert script != null;
		assert script.getGroovySource().contains(".executeTestRule()");
	}

	@Override
	protected AbstractBusinessTestScriptGenerator newGenerator() {
		return getFactory(new B()).newCustomRuleScriptGenerator("TestRule");
	}
}