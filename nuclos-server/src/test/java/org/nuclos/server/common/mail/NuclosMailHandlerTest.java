package org.nuclos.server.common.mail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.nuclos.api.mail.NuclosMail;
import org.nuclos.common.NuclosFile;
import org.nuclos.common.PortAllocator;
import org.nuclos.common2.exception.MailReceiveException;
import org.nuclos.common2.exception.MailSendException;
import org.nuclos.server.common.mail.properties.IMAPConnectionProperties;
import org.nuclos.server.common.mail.properties.MailConnectionProperties;
import org.nuclos.server.common.mail.properties.POP3ConnectionProperties;
import org.nuclos.server.common.mail.properties.SMTPConnectionProperties;

import com.icegreen.greenmail.user.GreenMailUser;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.GreenMailUtil;
import com.icegreen.greenmail.util.ServerSetup;
import com.icegreen.greenmail.util.ServerSetupTest;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NuclosMailHandlerTest {
	private static Set<ServerSetup> serverSetups = new HashSet<>();
	private static GreenMail greenMail;
	private static GreenMailUser userA;
	private static GreenMailUser userB;
	private static org.nuclos.common.mail.NuclosMail exampleMail;

	private static final String fileNameWithSpecialChars = "attachment_öäüß$%&()[]{}=?!§.txt";

	@BeforeClass
	public static void setup() throws IOException, URISyntaxException {
		exampleMail = new org.nuclos.common.mail.NuclosMail();
		exampleMail.addRecipients(Arrays.asList("user.a@nuclos.de"));
		exampleMail.setSubject("Example Subject");
		exampleMail.setMessage("Example Mail Message");
		exampleMail.addAttachments(Arrays.asList(
				getNuclosFile("/org/nuclos/server/common/mail/attachment.txt"),
				getNuclosFile("/org/nuclos/server/common/mail/attachment.txt", fileNameWithSpecialChars)
		));
		exampleMail.setReplyTo("example.mail@nuclos.de");
		exampleMail.setFrom("example.mail@nuclos.de");

		// Copy the default GreenMail setups and make some adjustments
		for (ServerSetup setup : ServerSetupTest.ALL) {
			final int port = PortAllocator.allocate();

			final ServerSetup newSetup = new ServerSetup(port, setup.getBindAddress(), setup.getProtocol());
			newSetup.setServerStartupTimeout(10000);

			serverSetups.add(newSetup);
		}
		greenMail = new GreenMail(serverSetups.toArray(new ServerSetup[]{}));

		//Start all email servers using non-default ports.
		greenMail.start();
		userA = greenMail.setUser("user.a@nuclos.de", "a", "a");
		userB = greenMail.setUser("user.b@nuclos.de", "b", "b");
	}

	@AfterClass
	public static void teardown() {
		greenMail.stop();
	}

	@Test
	public void _01_receivePOP3() throws MailReceiveException {
		MimeMessage message = createTextMessage();
		userA.deliver(message);
		assert greenMail.getReceivedMessages().length == 1;

		final NuclosMailHandler mailHandler = getPOP3MailHandler();
		List<NuclosMail> mails = mailHandler.receiveMails("INBOX", true);

		assert mails.size() == 1;
	}

	@Test
	public void _02_receiveIMAP() throws MailReceiveException {
		MimeMessage message = createTextMessage();
		userA.deliver(message);
		assert greenMail.getReceivedMessages().length == 1;

		final NuclosMailHandler mailHandler = getIMAPMailHandler();
		List<NuclosMail> mails = mailHandler.receiveMails("INBOX", true);

		assert mails.size() == 1;
	}

	@Test
	public void _03_sendAndReceiveTextMail() throws MailSendException, MailReceiveException {
		final NuclosMailSender mailSender = getMailSender();
		mailSender.sendMail(exampleMail, null, null);

		final NuclosMailHandler mailHandler = getIMAPMailHandler();
		List<NuclosMail> mails = mailHandler.receiveMails("INBOX", true);

		assert mails.size() == 1;
		NuclosMail mail = mails.get(0);

		assert mail.getSubject().equals(exampleMail.getSubject());
		assert mail.getRecipients().equals(exampleMail.getRecipients());
		assert mail.getMessage().equals(exampleMail.getMessage());
		assert mail.getFrom().equals(exampleMail.getFrom());

		assert mail.getAttachments().size() == exampleMail.getAttachments().size();
		Iterator<org.nuclos.api.common.NuclosFile> iterator = mail.getAttachments().iterator();
		assert iterator.next().getName().equals("attachment.txt");
		assert iterator.next().getName().equals(fileNameWithSpecialChars);

		assert mail.getReceivedDate() != null;
		assert mail.getReceivedDate().getTime() < System.currentTimeMillis();

		assert mail.getSentDate() != null;
		assert mail.getSentDate().getTime() < System.currentTimeMillis();
		assert mail.getSentDate().getTime() > System.currentTimeMillis() - 1000 * 3;
	}

	@Test
	public void _04_sendAndReceiveHTMLMail() {
		// TODO
	}

	/**
	 * Receives a complex multipart message with normal and inline attachments.
	 */
	@Test
	public void _05_receiveWithAttachments() throws URISyntaxException, FileNotFoundException, MessagingException, MailReceiveException {
		MimeMessage message = loadMimeMessage("/org/nuclos/server/common/mail/inline_images_and_attachments.eml");
		userA.deliver(message);

		// TODO: Mails deleted only via POP3 are still returned by getReceivedMessage()
//		assert greenMail.getReceivedMessages().length == 1;

		final NuclosMailHandler mailHandler = getIMAPMailHandler();
		List<NuclosMail> mails = mailHandler.receiveMails("INBOX", true);

		assert mails.size() == 1;

		NuclosMail mail = mails.get(0);

		assert mail.getReceivedDate() != null;
		assert mail.getReceivedDate().getTime() < System.currentTimeMillis();

		assert mail.getSentDate() != null;
		assert mail.getSentDate().getTime() < System.currentTimeMillis();

		assert "Test mail with inline images and attachments".equals(mail.getSubject());
		assert "andreas.laemmlein@nuclos.de".equals(mail.getFrom());

		assert mail.getRecipients().size() == 1;
		assert "mailtest@laemmlein.org".equals(mail.getRecipients().get(0));

		// TODO: Correct mail type does not get set by the NuclosMailHandler yet
//		assert "".equals(mail.getMailType());

		assert "andreas.laemmlein@nuclos.de".equals(mail.getReplyTo());

		assert mail.getAttachments().size() == 2;
		assert hasAttachment(mail, "Test_°!\"§$%&⁄()=?.txt");
		assert hasAttachment(mail, "Test_öäüößß%$%&()=?!\"§$%.docx");

		assert mail.getInlineAttachments().size() == 2;
		assert hasInlineAttachment(mail, "favicon.ico");
		assert hasInlineAttachment(mail, "eaheidakbjoooina.png");

		assert ("Nuclos favicon: http://www.nuclos.de/templates/b59-tpl8/favicon.ico\r\n" +
				"\r\n" +
				"Nuclos Logo:\r\n" +
				"\r\n" +
				"\r\n" +
				"\r\n" +
				"-- \r\n" +
				"*Andreas Lämmlein B.Sc.* | *Novabit Informationssysteme GmbH * | \r\n" +
				"*_andreas.laemmlein@nuclos.de <mailto:andreas.laemmlein@nuclos.de>_*\r\n" +
				"Mühlweg 2, 82054 Sauerlach, Germany | Tel +49 8104 6473-13 | Fax +49 \r\n" +
				"8104 6473-99**\r\n" +
				"Sitz der Gesellschaft: Mühlweg 2, 82054 Sauerlach | AG München HRB 129981\r\n" +
				"Geschäftsführer: Ramin Göttlich, Klaus Röder*\r\n" +
				"Download Nuclos at *_*www.nuclos.de* <http://www.nuclos.de>_").equals(mail.getMessage());
	}

	/**
	 * Receives a spam message with incomplete header informations.
	 */
	@Test
	public void _06_testReceiveWithMissingHeaders() throws URISyntaxException, FileNotFoundException, MessagingException, MailReceiveException {
		MimeMessage message = loadMimeMessage("/org/nuclos/server/common/mail/missing_headers.eml");
		userA.deliver(message);

		final NuclosMailHandler mailHandler = getIMAPMailHandler();
		List<NuclosMail> mails = mailHandler.receiveMails("INBOX", true);

		assert mails.size() == 1;

		NuclosMail mail = mails.get(0);

		assert mail.getReceivedDate() != null;
		assert mail.getReceivedDate().getTime() < System.currentTimeMillis();

		assert mail.getSentDate() != null;

		assert mail.getSubject().equals("Test");
		assert mail.getFrom() == null;

		assert mail.getRecipients().size() == 0;
		assert mail.getReplyTo() == null;

		assert mail.getAttachments().size() == 0;

		assert mail.getInlineAttachments().size() == 0;

		assert ("Some text here.").equals(mail.getMessage());
	}

	private MailConnectionProperties getIMAPConnectionProperties() {
		ServerSetup serverSetup = greenMail.getImap().getServerSetup();

		MailConnectionProperties properties = new IMAPConnectionProperties();
		properties.setUser(userA.getLogin());
		properties.setPassword(userA.getPassword());
		properties.setHost(serverSetup.getBindAddress());
		properties.setPort(serverSetup.getPort());
		properties.setFolderFrom("INBOX");
		properties.setFolderTo("DONE");

		return properties;
	}

	private MailConnectionProperties getPOP3ConnectionProperties() {
		ServerSetup serverSetup = greenMail.getPop3().getServerSetup();

		MailConnectionProperties properties = new POP3ConnectionProperties();
		properties.setUser(userA.getLogin());
		properties.setPassword(userA.getPassword());
		properties.setHost(serverSetup.getBindAddress());
		properties.setPort(serverSetup.getPort());
		properties.setFolderFrom("INBOX");
		properties.setFolderTo("DONE");

		return properties;
	}

	private MailConnectionProperties getSMTPConnectionProperties() {
		ServerSetup serverSetup = greenMail.getSmtp().getServerSetup();

		MailConnectionProperties properties = new SMTPConnectionProperties();
		properties.setUser(userB.getLogin());
		properties.setPassword(userB.getPassword());
		properties.setHost(serverSetup.getBindAddress());
		properties.setPort(serverSetup.getPort());

		return properties;
	}

	private NuclosMailHandler getIMAPMailHandler() {
		return new NuclosMailHandler(getIMAPConnectionProperties());
	}

	private NuclosMailHandler getPOP3MailHandler() {
		return new NuclosMailHandler(getPOP3ConnectionProperties());
	}

	private NuclosMailSender getMailSender() {
		return new NuclosMailSender(getSMTPConnectionProperties());
	}

	private boolean hasAttachment(NuclosMail mail, String fileName) {
		return containsFile(mail.getAttachments(), fileName);
	}

	private boolean hasInlineAttachment(NuclosMail mail, String fileName) {
		return containsFile(mail.getInlineAttachments(), fileName);
	}

	private <T extends org.nuclos.api.common.NuclosFile> boolean containsFile(Collection<T> files, String fileName) {
		for (T file : files) {
			if (file.getName().equals(fileName)) {
				return true;
			}
		}
		return false;
	}

	private MimeMessage createTextMessage() {
		try {
			Session e = GreenMailUtil.getSession(greenMail.getImap().getServerSetup());
			MimeMessage mimeMessage = new MimeMessage(e);
			mimeMessage.setSubject(GreenMailUtil.random());
			mimeMessage.setSentDate(new Date());
			mimeMessage.setFrom(new InternetAddress("from@localhost.com"));
			mimeMessage.setRecipients(Message.RecipientType.TO, "to@localhost.com");
			mimeMessage.setText(GreenMailUtil.random());
			return mimeMessage;
		} catch (MessagingException var7) {
			throw new IllegalArgumentException("Can not generate message", var7);
		}
	}

	private MimeMessage loadMimeMessage(String resourceName) throws URISyntaxException, FileNotFoundException, MessagingException {
		URL resourceUrl = getClass().getResource(resourceName);
		Path resourcePath = Paths.get(resourceUrl.toURI());

		InputStream is = new FileInputStream(resourcePath.toFile());

		return new MimeMessage(null, is);
	}

	private static NuclosFile getNuclosFile(
			final String resourceName
	) throws IOException, URISyntaxException {
		return getNuclosFile(resourceName, null);
	}

	private static NuclosFile getNuclosFile(
			final String resourceName,
			final String overrideFileName
	) throws URISyntaxException, IOException {
		final URL resourceUrl = NuclosMailHandlerTest.class.getResource(resourceName);
		final Path resourcePath = Paths.get(resourceUrl.toURI());

		final File file = resourcePath.toFile();
		final String fileName = StringUtils.isNotBlank(overrideFileName)
				? overrideFileName
				: file.getName();

		return new NuclosFile(fileName, FileUtils.readFileToByteArray(file));
	}
}
