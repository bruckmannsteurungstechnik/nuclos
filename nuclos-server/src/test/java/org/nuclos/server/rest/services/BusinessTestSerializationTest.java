package org.nuclos.server.rest.services;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URISyntaxException;

import org.junit.Test;
import org.nuclos.common.UID;
import org.nuclos.common.businesstest.BusinessTestVO;
import org.nuclos.common2.DateTime;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class BusinessTestSerializationTest {

	@Test
	public void testSerialization() throws URISyntaxException, IOException {
		BusinessTestVO test = new BusinessTestVO();
		test.setName("Test");
		test.setDescription("Test-Description");
		test.setPrimaryKey(new UID("UID12345"));
		test.setDuration(42l);
		test.setStartdate(new DateTime());
		test.setEnddate(new DateTime());

		ObjectMapper objectMapper = new ObjectMapper();
		SimpleModule module = new SimpleModule();
		objectMapper.registerModule(module);
		StringWriter stringWriter = new StringWriter();

		objectMapper.writeValue(stringWriter, test);
		String serializedValue = stringWriter.toString();

		BusinessTestVO test2 = objectMapper.readValue(serializedValue, BusinessTestVO.class);

		assert test2.getPrimaryKey().equals(test.getPrimaryKey());
		assert test2.getName().equals(test.getName());
		assert test2.getDuration().equals(test.getDuration());
		assert test2.getStartdate().equals(test.getStartdate());
		assert test2.getEnddate().equals(test.getEnddate());
	}

}
