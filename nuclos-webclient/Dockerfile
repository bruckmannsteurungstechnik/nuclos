ARG NODE_VERSION=10.16.0

# First stage: test and build
FROM node:$NODE_VERSION as builder

# Install Google Chrome
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
RUN apt-get update && apt-get install -y google-chrome-stable

# Prepare / install dependencies
COPY package*.json npm-shrinkwrap.json /src/app/
COPY scripts /src/app/scripts
WORKDIR /src/app
RUN npm run install-dependencies

# Copy app sources
COPY src /src/app/src
COPY nuclos_typings /src/app/nuclos_typings
COPY layout /src/app/layout
COPY rule /src/app/rule
COPY shared /src/app/shared
COPY *.js *.json /src/app/
RUN ls -l /src/app

# Run lint
RUN npm run lint

# Run unit tests
RUN npm run test

# Build app
RUN npm run build --@nuclos/nuclos-webclient:build-base-href="/"


# Second stage: Only serve the previously created Webclient with a simple HTTP server
# Resulting image is ~95% smaller than first stage
FROM node:${NODE_VERSION}-alpine

ENV REST_URL="//localhost:8080/nuclos-war"

# Running http-server will be terminated after TIMEOUT seconds
ENV TIMEOUT=10800

# Prepare serve script
RUN npm install http-server -g
COPY entrypoint.sh /src/app/
RUN chmod +x /src/app/entrypoint.sh

COPY --from=builder /src/app/dist /src/app/dist

# Serve
WORKDIR /src/app/dist
ENTRYPOINT ["/src/app/entrypoint.sh"]
