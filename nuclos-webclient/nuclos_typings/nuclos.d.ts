declare module nuclos {
    interface Errorhandler {
        show(dat:Object, status:string, cookieStore?: Object): Function;
    }

	export interface UID {
		uid: string;
		string?: string;
		stringifiedDefinition?: string;
	}

	export interface IBoList {
		bos: Array<any>;
	}
}
