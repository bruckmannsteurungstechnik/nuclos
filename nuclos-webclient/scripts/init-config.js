let fs = require('fs');

// change server value by adding --@nuclos/nuclos-webclient:nuclos-server="172.17.0.1"
// change server-port value by adding --@nuclos/nuclos-webclient:nuclos-server-port=8090
// change server-context value by adding --@nuclos/nuclos-webclient:nuclos-server-context=extension-war

let server = process.env.npm_package_config_nuclos_server;
let port = process.env.HOST_PORT || process.env.npm_package_config_nuclos_server_port;
let appcontext = process.env.npm_package_config_nuclos_server_context
let conf = {
	"nuclosURL": "//" + server + ":" + port + "/" + appcontext
};
fs.writeFile(
	'src/assets/config.json',
	JSON.stringify(conf),
	error => {console.error(error);}
);
