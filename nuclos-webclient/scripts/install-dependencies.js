#!/usr/bin/env node

const buildMode = process.argv[2];

let runCmd = cmd => {
	console.log('Installing: ', cmd);
	require('child_process').execSync(cmd, {
		cwd: '.',
		stdio: [ 'ignore', 1, 2 ]
	});
};

const npmRegistry = process.env.npm_package_config_npm_registry;
const nodeSassVersion = require('../package.json').dependencies['node-sass'];
const nodeSassBinarySite = process.env.npm_package_config_node_sass_binary_site;

// To skip the compilation of node-sass (which can be time-consuming and resource-intensive) we set the "sass_binary_site":
runCmd("npm config set sass-binary-site="+nodeSassBinarySite)

if ( buildMode == "prod" ) {
	// Do not use "ci"! There are still problems with forwarding the sass-binary-site
	runCmd("npm install --registry="+npmRegistry+" --strict-ssl=false --no-save")
} else {
	runCmd("npm ci --registry="+npmRegistry+" --strict-ssl=false")
}
