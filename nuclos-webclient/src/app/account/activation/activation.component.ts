import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { HasMessage } from '../../shared/has-message';
import { NuclosAccountService } from '../shared/nuclos-account.service';
import { take } from 'rxjs/operators';

@Component({
	selector: 'nuc-activation',
	templateUrl: './activation.component.html',
	styleUrls: ['./activation.component.css']
})
export class ActivationComponent extends HasMessage implements OnInit {

	constructor(
		private accountService: NuclosAccountService,
		private route: ActivatedRoute,
		i18n: NuclosI18nService
	) {
		super(i18n);
	}

	ngOnInit() {
		this.route.params.subscribe(params => {
			let username = params['username'];
			let activationCode = params['activationCode'];
			this.accountService.activate(username, activationCode)
			.pipe(take(1))
			.subscribe(
				() => this.showSuccessMessage(),
				error => this.showErrorFromResponse('webclient.account.error', error)
			);
		});
	}

	private showSuccessMessage() {
		this.setMessage(
			'success',
			'webclient.account.successful.enabled',
			'webclient.account.enabling.was.successful.you.can.sign'
		);
	}
}
