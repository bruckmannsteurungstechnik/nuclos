import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { NOT_FOUND, SERVICE_UNAVAILABLE } from 'http-status-codes';
import { throwError as observableThrowError } from 'rxjs';

import { catchError, take } from 'rxjs/operators';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { Logger } from '../../log/shared/logger';
import { HasMessage } from '../../shared/has-message';
import { NuclosAccountService } from '../shared/nuclos-account.service';

@Component({
	selector: 'nuc-forgot-login-details',
	templateUrl: './forgot-login-details.component.html',
	styleUrls: ['./forgot-login-details.component.css']
})
export class ForgotLoginDetailsComponent extends HasMessage implements OnInit {

	what = 'username';

	email;
	username;

	showForm = true;
	showLoginLink = false;

	constructor(
		private accountService: NuclosAccountService,
		private $log: Logger,
		private location: Location,
		i18n: NuclosI18nService
	) {
		super(i18n);
	}

	ngOnInit() {
	}

	onSubmit() {
		this.clearMessage();

		if (this.what === 'username') {
			// TODO: Check email
			this.accountService.requestUsername(this.email).pipe(
				take(1),
				catchError(e => {
					if (e && e.status === SERVICE_UNAVAILABLE) {
						this.showError(
							'webclient.error.title',
							'webclient.account.forgotLoginDetails.error.serviceUnavailable'
						);
					} else if (e && e.status === NOT_FOUND) {
						this.showError(
							'webclient.error.title',
							'webclient.account.forgotLoginDetails.error.email.notFound'
						);
					} else {
						this.showErrorFromResponse('webclient.error.title', e);
					}
					return observableThrowError(e);
				}))
				.subscribe(() => {
					this.setMessage(
						'success',
						'webclient.account.forgotLoginDetails.success.title',
						'webclient.account.forgotLoginDetails.success.message'
					);
					this.showForm = false;
					this.showLoginLink = true;
				});
		} else if (this.what === 'password') {
			// TODO: Check username
			this.accountService.requestPasswordReset(this.username).pipe(
				take(1),
				catchError(e => {
					if (e && e.status === SERVICE_UNAVAILABLE) {
						this.showError(
							'webclient.error.title',
							'webclient.account.forgotLoginDetails.error.serviceUnavailable'
						);
					} else if (e && e.status === NOT_FOUND) {
						this.showError(
							'webclient.error.title',
							'webclient.account.forgotLoginDetails.error.username.notFound'
						);
					} else {
						this.showErrorFromResponse('webclient.error.title', e);
					}
					return observableThrowError(e);
				}))
				.subscribe(() => {
					this.setMessage(
						'success',
						'webclient.account.forgotLoginDetails.success.title',
						'webclient.account.forgotLoginDetails.success.message'
					);
					this.showForm = false;
					this.showLoginLink = true;
				});
		} else {
			this.$log.warn('Invalid option: %o', this.what);
		}
	}

	canSubmit() {
		return this.what === 'username' && this.email
			|| this.what === 'password' && this.username;
	}

	cancel() {
		this.location.back();
	}
}
