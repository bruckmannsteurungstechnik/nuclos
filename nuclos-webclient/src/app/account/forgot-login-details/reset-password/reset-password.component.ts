import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { take } from 'rxjs/operators';
import { AuthenticationService } from '../../../authentication';
import { NuclosI18nService } from '../../../i18n/shared/nuclos-i18n.service';
import { HasMessage } from '../../../shared/has-message';
import { UserPassword } from '../../password-change/password';
import { NuclosAccountService } from '../../shared/nuclos-account.service';

@Component({
	selector: 'nuc-reset-password',
	templateUrl: './reset-password.component.html',
	styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent extends HasMessage implements OnInit {
	passwordItem: UserPassword = {userName: ''};
	showForm = true;
	showLoginLink = false;

	constructor(
		private accountService: NuclosAccountService,
		private authenticationService: AuthenticationService,
		private route: ActivatedRoute,
		i18n: NuclosI18nService
	) {
		super(i18n);
	}

	ngOnInit() {
		this.route.params.subscribe(params => {
			this.passwordItem.userName = params['username'];
			this.passwordItem.passwordResetCode = params['passwordResetCode'];
		});
	}

	changePassword() {
		this.clearMessage();

		let valid = this.validatePassword();
		if (!valid) {
			return;
		}

		this.accountService.resetPassword(this.passwordItem)
		.pipe(take(1))
		.subscribe(
			() => {
				this.setMessage(
					'success',
					'webclient.user.changepassword.changed',
					''
				);
				this.showForm = false;
				this.showLoginLink = true;
			},
			error => this.showErrorFromResponse('webclient.account.error', error)
		);
	}

	showErrorFromResponse(titleKey: string, error: HttpErrorResponse) {
		if (error.status === 403) {
			this.showError(
				titleKey,
				'webclient.user.changepassword.passwordwrong'
			);
		} else if (error.status === 406) {
			this.showError(
				titleKey,
				'webclient.user.changepassword.passwordnotsuitable'
			);
		} else {
			super.showErrorFromResponse(
				titleKey,
				error
			);
		}
	}

	private validatePassword(): boolean {
		if (!this.passwordItem.newPassword) {
			this.showError(
				'webclient.account.error',
				'webclient.user.changepassword.empty'
			);
			return false;
		}

		if (this.passwordItem.newPassword !== this.passwordItem.confirmNewPassword) {
			this.showError(
				'webclient.account.error',
				'webclient.user.changepassword.nomatch'
			);
			return false;
		}

		return true;
	}
}
