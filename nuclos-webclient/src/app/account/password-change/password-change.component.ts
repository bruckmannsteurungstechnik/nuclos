import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { take, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { AuthenticationService } from '../../authentication';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { HasMessage } from '../../shared/has-message';
import { NuclosAccountService } from '../shared/nuclos-account.service';
import { UserPassword } from './password';

@Component({
	selector: 'nuc-password-change',
	templateUrl: './password-change.component.html',
	styleUrls: ['./password-change.component.css']
})
export class PasswordChangeComponent extends HasMessage implements OnInit, OnDestroy {
	passwordItem: UserPassword;
	showForm;
	oldPasswordEnabled = true;
	showLoginLink = false;

	private unsubscribe$ = new Subject<void>();

	constructor(
		private accountService: NuclosAccountService,
		private authenticationService: AuthenticationService,
		i18n: NuclosI18nService
	) {
		super(i18n);
	}

	ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	ngOnInit() {
		let credentialsExpired = this.authenticationService.getCredentialsExpired();
		if (credentialsExpired) {
			this.passwordItem = credentialsExpired;
			this.setMessage(
				'warning',
				'webclient.credentials.expired.title',
				'webclient.credentials.expired.message'
			);
			this.showForm = true;
			this.oldPasswordEnabled = false;
		} else {
			this.requireLogin();
		}
	}

	changePassword() {
		this.clearMessage();

		let valid = this.validatePassword();
		if (!valid) {
			return;
		}

		this.accountService.changePassword(this.passwordItem)
		.pipe(take(1))
		.subscribe(
			() => {
				this.setMessage('success', 'webclient.user.changepassword.changed', '');
				this.showForm = false;
				if (!this.authenticationService.isLoggedIn()) {
					this.showLoginLink = true;
				}
			},
			error => this.showErrorFromResponse('webclient.account.error', error)
		);
	}

	showErrorFromResponse(titleKey: string, error: HttpErrorResponse) {
		if (error.status === 403) {
			this.showError(titleKey, 'webclient.user.changepassword.passwordwrong');
		} else if (error.status === 406) {
			this.showError(titleKey, 'webclient.user.changepassword.passwordnotsuitable');
		} else {
			super.showErrorFromResponse(titleKey, error);
		}
	}

	private requireLogin() {
		this.authenticationService.observeLoginStatus()
		.pipe(takeUntil(this.unsubscribe$))
		.subscribe(loggedIn => {
			if (!loggedIn) {
				this.showForm = false;
				return;
			}

			let user = this.authenticationService.getCurrentUser();
			if (user) {
				this.passwordItem = {
					userName: user.username
				};
				this.showForm = true;
			}
		});
	}

	private validatePassword(): boolean {
		if (!this.passwordItem.newPassword) {
			this.showError('webclient.account.error', 'webclient.user.changepassword.empty');
			return false;
		}

		if (this.passwordItem.newPassword !== this.passwordItem.confirmNewPassword) {
			this.showError('webclient.account.error', 'webclient.user.changepassword.nomatch');
			return false;
		}

		return true;
	}
}
