export class AccountData {
	username?: string;
	newPassword?: string;
	firstname?: string;
	lastname?: string;
	email?: string;
	email2?: string;
	privacyconsent?: boolean;
}
