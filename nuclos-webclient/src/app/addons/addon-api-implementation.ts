import { Injectable } from '@angular/core';
import {
	AddonContext,
	DatasourceParams,
	DialogApi,
	DialogButton,
	EntityObjectApi,
	IEntityObject,
	IPreference,
	IPreferenceContent,
	IPreferenceFilter,
	ISubEntityObject,
	LayoutContext,
	NavigationApi,
	NgbModalOptions,
	PreferencesApi,
	PreferenceTypeName,
	ResultlistContext
} from '@nuclos/nuclos-addon-api';
import { Locale } from '@nuclos/nuclos-addon-api/api/locale';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { EntityObjectEventService } from '../entity-object-data/shared/entity-object-event.service';
import { EntityObjectNavigationService } from '../entity-object-data/shared/entity-object-navigation.service';
import { EntityObjectResultService } from '../entity-object-data/shared/entity-object-result.service';
import { EntityObject, SubEntityObject } from '../entity-object-data/shared/entity-object.class';
import { EntityObjectService } from '../entity-object-data/shared/entity-object.service';
import { DetailModalService } from '../entity-object/shared/detail-modal.service';
import { NuclosI18nService } from '../i18n/shared/nuclos-i18n.service';
import { AbstractWebComponent } from '../layout/shared/abstract-web-component';
import { Logger } from '../log/shared/logger';
import { DialogService } from '../popup/dialog/dialog.service';
import { Preference, PreferenceContent } from '../preferences/preferences.model';
import { PreferencesService } from '../preferences/preferences.service';
import { SearchfilterService } from '../search/shared/searchfilter.service';
import { DatasourceService } from '../shared/datasource.service';
import { NuclosConfigService } from '../shared/nuclos-config.service';
import { AddonService, Property } from './addon.service';

export class EntityObjectApiImplementation implements EntityObjectApi {

	constructor(
		private entityObjectService: EntityObjectService,
		private entityObjectEventService: EntityObjectEventService,
		private entityObjectNavigationService: EntityObjectNavigationService,
		private entityObjectResultService: EntityObjectResultService
	) {
	}

	loadEo(entityClassId: string, entityObjectId: number): Observable<IEntityObject> {
		return this.entityObjectService.loadEO(entityClassId, entityObjectId);
	}

	reloadSelectedEo(): Observable<IEntityObject> {
		return new Observable<IEntityObject>(observer => {

			let selectedEo = this.entityObjectResultService.getSelectedEo();
			if (selectedEo) {
				this.entityObjectService.reloadEo(<EntityObject>selectedEo)
				.pipe(take(1))
				.subscribe(eo => {
					this.entityObjectEventService.emitModifiedEo(eo);
					observer.next(eo);
				});
			} else {
				return observer.next(undefined);
			}
		});
	}



	onEoCreation(): Observable<IEntityObject> {
		return this.entityObjectEventService.observeCreatedEo();
	}

	onEoAdd(): Observable<IEntityObject> {
		return this.entityObjectEventService.observeAddEo();
	}

	onEoModification(): Observable<IEntityObject> {
		return this.entityObjectEventService.observeEoModification();
	}

	onEoSelection(): Observable<IEntityObject | undefined> {
		return this.entityObjectEventService.observeSelectedEo();
	}

	onEoReset(): Observable<IEntityObject> {
		return this.entityObjectEventService.observeResetEo();
	}

	onEoSave(): Observable<IEntityObject> {
		return this.entityObjectEventService.observeSavedEo();
	}

	onEoDelete(): Observable<IEntityObject> {
		return this.entityObjectEventService.observeDeletedEo();
	}

	onEntityClassIdChange(): Observable<string | undefined> {
		return this.entityObjectResultService.observeSelectedEntityClassId();
	}
}

export class NavigationApiImplementation implements NavigationApi {

	constructor(
		private entityObjectNavigationService: EntityObjectNavigationService,
		private entityObjectResultService: EntityObjectResultService
	) {
	}

	navigateToEo(entityObject: IEntityObject): void {
		this.entityObjectNavigationService.navigateToEo(entityObject);
	}

	navigateToEoById(entityClassId: string, eoId: number | string): void {
		this.entityObjectNavigationService.navigateToEoById(entityClassId, eoId);
	}

	onEntityClassIdChange(): Observable<string | undefined> {
		return this.entityObjectResultService.observeSelectedEntityClassId();
	}

}

export class DialogApiImplementation implements DialogApi {
	constructor(private dialogService: DialogService,
				private detailModalService: DetailModalService) {}

	alert(title: string, message: string): Promise<any> {
		return this.dialogService.alert({ title: title, message: message} );
	}


	openDialog(title: string, message: string, buttonOptions: DialogButton[], modalOptions?: NgbModalOptions): void {
		this.dialogService.openDialog(title, message, buttonOptions, modalOptions);
	}

	openEoInModal(eo: IEntityObject): Observable<any> {
		return this.detailModalService.openEoInModal(eo);
	}

}

export class PreferencesApiImplementation implements PreferencesApi {

	constructor(
		private preferencesService: PreferencesService
	) {}


	newPreference(
		entityClassId: string,
		app: string,
		name: string,
		type: PreferenceTypeName = 'userdefined',
	): IPreference<IPreferenceContent> {
		let pref: IPreference<any> = new Preference(type, entityClassId);
		pref.name = name;
		pref.app = app;
		return pref;
	}

	getPreference(prefId: string): Observable<IPreference<IPreferenceContent>> {
		return this.preferencesService.getPreference(prefId);
	}

	getPreferences(filter: IPreferenceFilter, useCache?: boolean): Observable<Array<IPreference<PreferenceContent>>> {
		return this.preferencesService.getPreferences(filter, useCache);
	}

	savePreference(preferenceItem: IPreference<any>): Observable<IPreference<any>> {
		return this.preferencesService.savePreferenceItem(preferenceItem);
	}

	deletePreference(preferenceItem: IPreference<any>): Observable<boolean> {
		return this.preferencesService.deletePreferenceItem(preferenceItem);
	}
}

@Injectable()
export abstract class AddonContextImplementation implements AddonContext {

	navigation: NavigationApi;
	entityobject: EntityObjectApi;
	preferences: PreferencesApi;
	dialog: DialogApi;

	constructor(
		protected addonService: AddonService,
		protected nuclosConfigService: NuclosConfigService,
		protected entityObjectService: EntityObjectService,
		protected entityObjectEventService: EntityObjectEventService,
		protected entityObjectResultService: EntityObjectResultService,
		protected entityObjectNavigationService: EntityObjectNavigationService,
		protected preferencesService: PreferencesService,
		protected datasourceService: DatasourceService,
		protected searchfilterService: SearchfilterService,
		protected dialogService: DialogService,
		protected detailModalService: DetailModalService,
		protected i18n: NuclosI18nService,
		protected $log: Logger
	) {

		this.navigation = new NavigationApiImplementation(entityObjectNavigationService, entityObjectResultService);
		this.entityobject = new EntityObjectApiImplementation(
			entityObjectService,
			entityObjectEventService,
			entityObjectNavigationService,
			entityObjectResultService
		);
		this.preferences = new PreferencesApiImplementation(preferencesService);
		this.dialog = new DialogApiImplementation(dialogService, detailModalService);
	}

	/**
	 * @deprecated
	 * TODO remove
	 */
	loadEo(entityClassId: string, entityObjectId: number): Observable<IEntityObject> {
		return this.entityObjectService.loadEO(entityClassId, entityObjectId);
	}

	/**
	 * @deprecated
	 * TODO remove
	 */
	onEoCreation(): Observable<IEntityObject> {
		return this.entityObjectEventService.observeCreatedEo();
	}

	/**
	 * @deprecated
	 * TODO remove
	 */
	onEoAdd(): Observable<IEntityObject> {
		return this.entityObjectEventService.observeAddEo();
	}

	/**
	 * @deprecated
	 * TODO remove
	 */
	onEoModification(): Observable<IEntityObject> {
		return this.entityObjectEventService.observeEoModification();
	}


	/**
	 * @deprecated
	 * TODO remove
	 */
	onEoSelection(): Observable<IEntityObject | undefined> {
		return this.entityObjectEventService.observeSelectedEo();
	}

	/**
	 * @deprecated
	 * TODO remove
	 */
	onEoReset(): Observable<IEntityObject> {
		return this.entityObjectEventService.observeResetEo();
	}

	/**
	 * @deprecated
	 * TODO remove
	 */
	onEoSave(): Observable<IEntityObject> {
		return this.entityObjectEventService.observeSavedEo();
	}

	/**
	 * @deprecated
	 * TODO remove
	 */
	onEoDelete(): Observable<IEntityObject> {
		return this.entityObjectEventService.observeDeletedEo();
	}

	/**
	 * @deprecated
	 * TODO remove
	 */
	onEntityClassIdChange(): Observable<string | undefined> {
		return this.entityObjectResultService.observeSelectedEntityClassId();
	}


	/**
	 * @deprecated
	 * TODO remove
	 */
	navigateToEo(entityObject: IEntityObject): void {
		this.entityObjectNavigationService.navigateToEo(entityObject);
	}

	/**
	 * @deprecated
	 * TODO remove
	 */
	navigateToEoById(entityClassId: string, eoId: number | string): void {
		this.entityObjectNavigationService.navigateToEoById(entityClassId, eoId);
	}


	getEntityClassId(): string | undefined {
		return this.entityObjectResultService.getSelectedEntityClassId();
	}


	/**
	 * @deprecated
	 * TODO remove
	 */
	newPreference(
		entityClassId: string,
		app: string,
		name: string,
		type: PreferenceTypeName = 'userdefined',
	): IPreference<IPreferenceContent> {
		let pref: IPreference<any> = new Preference(type, entityClassId);
		pref.name = name;
		pref.app = app;
		return pref;
	}

	/**
	 * @deprecated
	 * TODO remove
	 */
	getPreference(prefId: string): Observable<IPreference<IPreferenceContent>> {
		return this.preferencesService.getPreference(prefId);
	}

	/**
	 * @deprecated
	 * TODO remove
	 */
	getPreferences(filter: IPreferenceFilter, useCache?: boolean): Observable<Array<IPreference<PreferenceContent>>> {
		return this.preferencesService.getPreferences(filter, useCache);
	}

	/**
	 * @deprecated
	 * TODO remove
	 */
	savePreference(preferenceItem: IPreference<any>): Observable<IPreference<any>> {
		return this.preferencesService.savePreferenceItem(preferenceItem);
	}

	/**
	 * @deprecated
	 * TODO remove
	 */
	deletePreference(preferenceItem: IPreference<any>): Observable<boolean> {
		return this.preferencesService.deletePreferenceItem(preferenceItem);
	}


	executeDatasource(datasourceId: string, datasourceParams: DatasourceParams, maxRowCount?: number): Observable<object[]> {
		return this.datasourceService.executeDatasource(datasourceId, datasourceParams, maxRowCount);
	}


	abstract getAddonProperty(key: string): string | undefined;


	openDialog(title: string, message: string, buttonOptions: DialogButton[], modalOptions?: NgbModalOptions): void {
		this.dialogService.openDialog(title, message, buttonOptions, modalOptions);
	}


	/*
	TODO fix type safety in nuclos-api:
	'AddonContextImplementation' is not assignable to the same property in base type 'AddonContext'
	getComponentFactoryClass(componentName: string): Type<Component> | undefined {
		return this.addonService.getComponentFactoryClass(componentName);
	}
	 */
	getComponentFactoryClass(componentName: string) {
		return this.addonService.getComponentFactoryClass(componentName) as any;
	}

	getRestHost(): string {
		return this.nuclosConfigService.getRestHost();
	}

	getLocale(): Locale {
		return this.i18n.getCurrentLocale();
	}

}

@Injectable()
export class LayoutContextImplementation extends AddonContextImplementation implements LayoutContext {

	private webComponent: AbstractWebComponent<any>;

	newDependentEo(parentEo: EntityObject, referenceAttributeId: string): Observable<ISubEntityObject> {

		let subformEoMetaId = referenceAttributeId.substring(0, referenceAttributeId.lastIndexOf('_'));
		return this.entityObjectService.createNew(subformEoMetaId, parentEo).pipe(
			map((subEo: SubEntityObject) => new SubEntityObject(
				parentEo,
				referenceAttributeId,
				subEo.getData()
			)));
	}


	setComponent(webComponent: AbstractWebComponent<any>): void {
		this.webComponent = webComponent;
	}

	getAddonProperty(key: string): string | undefined {

		if (this.webComponent) {
			return this.webComponent.getAdvancedProperty(key);
		}

		return undefined;
	}
}

@Injectable()
export class ResultlistContextImplementation extends AddonContextImplementation implements ResultlistContext {

	private properties: Property[];

	onEntityObjectListUpdate(): Observable<boolean> {
		return this.entityObjectResultService.observeResultListUpdate();
	}

	getEntityObjectList(): IEntityObject[] {
		return this.entityObjectResultService.getResults();
	}

	applyAdditionalResultlistWhereCondition(condition: string | undefined, appIdentifier: string): void {
		let entityClassId = this.getEntityClassId();
		if (entityClassId) {
			this.searchfilterService.applyAdditionalResultlistWhereCondition(condition, entityClassId, appIdentifier);
		} else {
			this.$log.error('Unable to get entity class id.');
		}
	}

	setAddonproperties(properties: Property[]) {
		this.properties = properties;
	}

	getAddonProperty(key: string): string | undefined {
		if (this.properties !== undefined) {
			let property = this.properties.find(p => p.name === key);
			return property ? property.value : undefined;
		}
		return undefined;
	}

}
