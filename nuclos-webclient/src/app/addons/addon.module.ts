import { NgModule } from '@angular/core';
import { LAYOUT_CONTEXT, RESULTLIST_CONTEXT } from '@nuclos/nuclos-addon-api';
import { SecurityModule } from '../security/security.module';
import { LayoutContextImplementation, ResultlistContextImplementation } from './addon-api-implementation';
import { AddonExecutorComponent } from './addon-executor/addon-executor.component';
import { AddonService } from './addon.service';

@NgModule({
	exports: [
		AddonExecutorComponent
	],
	imports: [
		SecurityModule,
	],
	declarations: [
		AddonExecutorComponent
	],
	providers: [
		AddonService,
		ResultlistContextImplementation,
		LayoutContextImplementation,
		{
			// TODO: Provide ResultlistContext directly - no need for an extra InjectionToken
			provide: RESULTLIST_CONTEXT,
			useExisting: ResultlistContextImplementation
		},
		{
			// TODO: Provide LayoutContext directly - no need for an extra InjectionToken
			provide: LAYOUT_CONTEXT,
			useExisting: LayoutContextImplementation
		}
	]
})
export class AddonModule {
}
