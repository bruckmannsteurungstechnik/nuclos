import { Injectable } from '@angular/core';
import { NuclosHttpService } from '../../shared/nuclos-http.service';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class MaintenanceService {

	constructor(private http: NuclosHttpService,
				private nuclosConfig: NuclosConfigService,
	) {
	}

	maintenanceModeStatus(): Observable<string> {
		return this.http.get(this.nuclosConfig.getRestHost() + '/maintenance/mode', {
			withCredentials: true,
			responseType: 'text'
		});
	}

	enableMaintenanceMode(): Observable<string> {
		return this.http.put(this.nuclosConfig.getRestHost() + '/maintenance/mode',
			'on',
			{withCredentials: true, responseType: 'text'}
		);
	}

	disableMaintenanceMode(): Observable<string> {
		return this.http.put(
			this.nuclosConfig.getRestHost() + '/maintenance/mode',
			'off',
			{withCredentials: true, responseType: 'text'}
		);
	}

	setMaintenanceCssClass(value: boolean): void {
		const body = $('body');
		if (value) {
			body.addClass('maintenancemode');
		} else {
			body.removeClass('maintenancemode');
		}
	}
}
