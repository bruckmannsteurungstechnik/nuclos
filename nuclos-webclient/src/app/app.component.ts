import { ChangeDetectorRef, Component, ElementRef, NgZone, OnInit, OnDestroy } from '@angular/core';
import { ANIMATION_TYPES } from 'ngx-loading';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { BrowserDetectionService } from './shared/browser-detection.service';
import { BusyService } from './shared/busy.service';

@Component({
	selector: 'nuc-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
	title = 'Nuclos Webclient v2';

	busy = false;
	loadingAnimationType = ANIMATION_TYPES.threeBounce;

	private unsubscribe$ = new Subject<void>();

	constructor(
		private elementRef: ElementRef,
		private browserDetectionService: BrowserDetectionService,
		private busyService: BusyService,
		private ngZone: NgZone,
		private changeDetectorRef: ChangeDetectorRef,
	) {
		window['NgZone'] = this.ngZone;
	}

	ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	ngOnInit(): void {
		let canonicalUrl = window.location.href.replace(/([^:]\/)\/+/g, '$1');
		if (canonicalUrl !== window.location.href) {
			window.location.href = canonicalUrl;
		}

		if (this.browserDetectionService.isSafari()) {
			$(this.elementRef.nativeElement).addClass('browser-safari');
		}

		this.busyService.isBusy().pipe(takeUntil(this.unsubscribe$)).subscribe(busy => {
			this.busy = busy;

			// https://github.com/angular/angular/issues/17572#issuecomment-323465737
			this.changeDetectorRef.detectChanges();
		});
	}
}


