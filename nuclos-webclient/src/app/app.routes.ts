import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { SetLocaleComponent } from './i18n/set-locale/set-locale.component';

export const ROUTE_CONFIG: Routes = [
	{
		path: 'auth',
		loadChildren: './authentication/authentication.module#AuthenticationModule'
	},
	{
		path: 'account',
		loadChildren: './account/account.module#AccountModule'
	},
	{
		path: 'maintenance',
		loadChildren: './admin/maintenance/maintenance.module#MaintenanceModule'
	},
	{
		path: 'businesstests',
		loadChildren: './businesstest/businesstest.module#BusinesstestModule'
	},
	{
		path: 'cache',
		loadChildren: './cache/cache.module#CacheModule'
	},
	{
		path: 'dashboard',
		loadChildren: './dashboard/dashboard.module#DashboardModule'
	},
	{
		path: '',
		loadChildren: './entity-object/entity-object.module#EntityObjectModule'
	},
	{
		path: 'error',
		loadChildren: './error/error.module#ErrorModule'
	},
	{
		path: 'news',
		loadChildren: './news/news.module#NewsModule'
	},
	{
		path: 'preferences',
		loadChildren: './preferences/preferences.module#PreferencesModule'
	},
	{
		path: 'serverinfo',
		loadChildren: './server-info/server-info.module#ServerInfoModule'
	},
	{
		path: 'swagger-ui',
		loadChildren: './swagger/swagger.module#SwaggerModule'
	},
	{
		path: 'login',
		pathMatch: 'full',
		redirectTo: '/auth/login'
	},
	{
		path: 'logout',
		pathMatch: 'full',
		redirectTo: '/auth/logout'
	},
	{
		path: 'session-info',
		pathMatch: 'full',
		redirectTo: '/auth/session-info'
	},
	{
		path: '',
		pathMatch: 'full',
		redirectTo: 'login'
	},
	{
		path: 'index.html',
		pathMatch: 'full',
		redirectTo: 'login'
	},
	{
		path: 'locale/:locale',
		component: SetLocaleComponent
	},
	{
		path: '**',
		redirectTo: 'error/404'
	}
];

export const AppRoutesModule = RouterModule.forRoot(ROUTE_CONFIG, {
	useHash: true,
	preloadingStrategy: PreloadAllModules
});
