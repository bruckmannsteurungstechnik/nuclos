import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FORBIDDEN, PRECONDITION_FAILED, SERVICE_UNAVAILABLE, TOO_MANY_REQUESTS, UNAUTHORIZED } from 'http-status-codes';
import { EMPTY, Subject, Observable } from 'rxjs';

import { catchError, take, takeUntil } from 'rxjs/operators';
import { NuclosI18nService } from '../i18n/shared/nuclos-i18n.service';
import { NuclosConfigService } from '../shared/nuclos-config.service';
import { SystemParameter } from '../shared/system-parameters';
import { AuthenticationService, LoginData } from './authentication.service';
import { MaintenanceService } from '../admin/maintenance/maintenance.service';

@Component({
	selector: 'nuc-authentication',
	templateUrl: './authentication.component.html',
	styleUrls: ['./authentication.component.css']
})
export class AuthenticationComponent implements OnInit, OnDestroy {
	@Input()
	username = '';

	@Input()
	password: string;

	@Input()
	autologin: boolean;

	@ViewChild('usernameInput') usernameInput;
	@ViewChild('passwordInput') passwordInput;

	showRegistrationLink = false;
	showForgotLoginDetailsLink = false;

	loginFormEnabled = true;
	showMandatorForm = false;
	showMandatorlessSessionButton = false;
	selectedMandatorId;
	loginInfo: LoginInfo;
	forceBrowserReloadAfterLogin: boolean;

	/**
	 * Should be set to an i18n key, e.g. 'webclient.nosession.error.wrongpass'.
	 */
	errorMessage: string | undefined;

	private unsubscribe$ = new Subject<void>();

	constructor(
		private route: ActivatedRoute,
		private authenticationService: AuthenticationService,
		private i18nService: NuclosI18nService,
		private maintenanceService: MaintenanceService,
		private configService: NuclosConfigService
	) {}

	ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	ngOnInit() {
		// initialize username input with username from last login
		let lastLoginUsername = this.authenticationService.getUsername();
		if (lastLoginUsername) {
			this.username = lastLoginUsername;
		}

		this.route.params.subscribe(params => this.i18nService.setLocaleString(params['locale']));

		this.configService.getSystemParameters().pipe(take(1)).subscribe(params => {
			this.showRegistrationLink = params.is(SystemParameter.USER_REGISTRATION_ENABLED);
			this.showForgotLoginDetailsLink = params.is(
				SystemParameter.FORGOT_LOGIN_DETAILS_ENABLED
			);
		});

		if (this.usernameInput) {
			// focus username respectively password input
			if (this.username.length === 0) {
				this.usernameInput.nativeElement.focus();
			} else {
				this.passwordInput.nativeElement.focus();
			}
		}
	}

	login() {
		this.errorMessage = undefined;
		let loginData: LoginData = {
			username: this.username,
			password: this.password,
			locale: this.i18nService.getCurrentLocale().key,
			autologin: this.autologin
		};

		this.authenticationService
			.login(loginData)
			.pipe(
				catchError((error: HttpErrorResponse) => {
					if (error.status === UNAUTHORIZED) {
						this.errorMessage = 'webclient.nosession.error.wrongpass';
					} else if (error.status === TOO_MANY_REQUESTS) {
						this.errorMessage = 'webclient.nosession.error.tooManyRequests';
					} else if (this.isCredentialsExpired(error)) {
						this.authenticationService.setCredentialsExpired({
							userName: loginData.username,
							oldPassword: loginData.password
						});
					} else if (error.error.message === 'nuclos.security.authentication.locked') {
						this.errorMessage = 'nuclos.security.authentication.locked';
					} else if (error.status === SERVICE_UNAVAILABLE) {
						this.errorMessage = 'webclient.maintenance.mode.nologin';
						this.maintenanceService.setMaintenanceCssClass(true);
						this.forceBrowserReloadAfterLogin = true;
					} else if (error.status === FORBIDDEN) {
						this.errorMessage = 'webclient.nosession.error.forbidden';
					} else {
						this.errorMessage = 'webclient.nosession.error.nocode';
					}
					return EMPTY;
				}),
				take(1)
			)
			.subscribe(authenticationData => {
				// show mandator selection dialog if configured
				this.showMandatorlessSessionButton = authenticationData.superUser;
				if (authenticationData.mandators) {
					this.loginInfo = authenticationData;

					// check if the last selected mandator (from cookie) still exists
					let lastMandatorLoggedInId = this.authenticationService.getLastMandatorIdCookie();

					if (!lastMandatorLoggedInId) {
						this.selectedMandatorId = authenticationData.mandators[0].mandatorId;
					} else {
						this.selectedMandatorId = lastMandatorLoggedInId;
					}

					if (authenticationData.mandators.length === 1) {
						// only one mandator configured no need for mandator selection
						this.authenticationService
							.selectMandator(authenticationData.mandators[0])
							.pipe(take(1))
							.subscribe(() => {
								$('body').css('cursor', 'progress');
								this.authenticationService.autoNavigation().then(() => {
									window.location.reload();
								});
							});
					} else {
						this.loginFormEnabled = false;
						this.showMandatorForm = true;
					}
				}

				if (this.forceBrowserReloadAfterLogin) {
					location.reload(true);
				}
			});
	}

	selectMandator() {
		this.authenticationService.onAuthenticationDataAvailable().pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
			if (this.loginInfo.mandators) {
				let mandator = this.loginInfo.mandators
					.filter(m => m.mandatorId === this.selectedMandatorId)
					.shift();
				this.authenticationService.selectMandator(mandator).pipe(take(1)).subscribe(() => {
					this.loginInfo.mandator = mandator!;
					this.navigateHome();
				});
			}
		});
	}

	mandatorlessSession() {
		this.navigateHome();
	}

	private isCredentialsExpired(error: HttpErrorResponse) {
		return (
			error.status === PRECONDITION_FAILED &&
			error.error &&
			error.error.message === 'nuclos.security.authentication.credentialsexpired'
		);
	}

	private navigateHome() {
		$('body').css('cursor', 'progress');
		this.authenticationService.navigateHome();

		if (window.location.href.indexOf('/login') === -1) {
			window.location.reload();
		}
	}
}
