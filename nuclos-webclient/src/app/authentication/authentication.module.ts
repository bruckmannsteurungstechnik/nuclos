import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AuthenticationComponent, AuthenticationService } from '.';
import { CacheModule } from '../cache/cache.module';
import { HttpModule } from '../http/http.module';
import { I18nModule } from '../i18n/i18n.module';
import { LogModule } from '../log/log.module';
import { AuthenticationRoutesModule } from './authentication.routes';
import { LogoutComponent } from './logout/logout.component';
import { SessionInfoComponent } from './session-info/session-info.component';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		RouterModule,

		// Nuclos modules
		LogModule,
		I18nModule,
		CacheModule,
		HttpModule,

		AuthenticationRoutesModule
	],
	declarations: [
		AuthenticationComponent,
		LogoutComponent,
		SessionInfoComponent
	],
	providers: [
		AuthenticationService
	],
	exports: [
	]
})
export class AuthenticationModule {
}
