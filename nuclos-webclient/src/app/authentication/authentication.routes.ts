import { RouterModule, Routes } from '@angular/router';
import { AuthenticationComponent } from './authentication.component';
import { LogoutComponent } from './logout/logout.component';
import { SessionInfoComponent } from './session-info/session-info.component';

export const ROUTE_CONFIG: Routes = [
	{
		path: 'session-info',
		component: SessionInfoComponent
	},
	{
		path: 'login',
		component: AuthenticationComponent
	},
	{
		path: 'logout',
		component: LogoutComponent
	}
];

export const AuthenticationRoutesModule = RouterModule.forChild(ROUTE_CONFIG);
