import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { UserAction } from '@nuclos/nuclos-addon-api';
import {
	BehaviorSubject,
	EMPTY,
	Observable,
	of as observableOf,
	ReplaySubject,
	Subject,
	throwError as observableThrowError
} from 'rxjs';

import { catchError, concat, filter, finalize, mergeMap, take, tap } from 'rxjs/operators';
import { UserPassword } from '../account/password-change/password';
import { NuclosCacheService } from '../cache/shared/nuclos-cache.service';
import { NuclosCookieService } from '../cookie/shared/nuclos-cookie.service';
import { NuclosI18nService } from '../i18n/shared/nuclos-i18n.service';
import { Logger } from '../log/shared/logger';
import { BusyService } from '../shared/busy.service';
import { NuclosConfigService } from '../shared/nuclos-config.service';
import { NuclosHttpService } from '../shared/nuclos-http.service';
import { StringUtils } from '../shared/string-utils';
import { SystemParameter, SystemParameters } from '../shared/system-parameters';
import { User } from './user';

export class LoginData {
	username: string;
	password: string;
	locale: string;
	autologin: boolean;
}

/**
 * TODO: Perform only authentication here. Extract permission checks (authorization) and other stuff.
 */
@Injectable()
export class AuthenticationService {
	static instance: AuthenticationService;

	static readonly COOKIE_KEY_REDIRECT = 'redirect';
	static readonly COOKIE_KEY_LAST_MANDATOR = 'lastMandator';
	static readonly COOKIE_KEY_LAST_ENTITY = 'lastEntity';

	/**
	 * Determines if it is okay to automatically navigate away from the current URL.
	 */
	static isAutoNavigationAllowed() {
		let location = window.location.href;
		return (
			location.indexOf('/account/register') === -1 &&
			location.indexOf('/account/activate') === -1 &&
			location.indexOf('/account/resetPassword') === -1 &&
			location.indexOf('/account/changePassword') === -1 &&
			location.indexOf('/account/forgotLoginDetails') === -1
		);
	}

	private currentUser: User | undefined;
	private allowedActions: Map<string, boolean>;

	private readonly ANONYMOUS_USER_NAME = 'anonymous';

	private loggedIn = false;
	private loginSubject: ReplaySubject<boolean>;

	private anonymousLoginAllowed = false;

	private authentication: LoginInfo | undefined;

	private mandatorSelection: BehaviorSubject<Mandator | undefined>;
	private authenticationDataAvailable: BehaviorSubject<boolean> = new BehaviorSubject(false);

	private loginInProgress = false;

	/**
	 * If set, forces the user to change his password
	 */
	private credentialsExpired?: UserPassword;

	constructor(
		private router: Router,
		private http: NuclosHttpService,
		private cookieService: NuclosCookieService,
		private nuclosConfig: NuclosConfigService,
		private busyService: BusyService,
		private $log: Logger,
		private i18nService: NuclosI18nService,
		private cacheService: NuclosCacheService
	) {
		AuthenticationService.instance = this;
		this.loginSubject = new ReplaySubject<boolean>(1);

		this.router.events.subscribe(event => {
			if (event instanceof NavigationEnd) {
				let url = (<NavigationEnd>event).url;
				// Redirect to the login page, if not logged in and the page requires authentication
				if (this.isAuthenticationRequired(url)) {
					this.observeLoginStatus()
						.pipe(take(1))
						.subscribe(loggedIn => this.checkLogin(loggedIn, url));
				}
			}
		});

		this.nuclosConfig.getSystemParameters().subscribe(parameters => {
			this.anonymousLoginAllowed = parameters.is(
				SystemParameter.ANONYMOUS_USER_ACCESS_ENABLED
			);
		});

		this.nuclosConfig.getSystemParameters().subscribe(parameters => {
			this.anonymousLoginAllowed = parameters.is(
				SystemParameter.ANONYMOUS_USER_ACCESS_ENABLED
			);
		});

		this.http.getUnauthorizedRequests().subscribe(() => {
			this.setUnauthorized();
		});

		this.mandatorSelection = new BehaviorSubject(undefined);

		this.observeLoginStatus().subscribe(() => this.cacheService.invalidateAllCaches());

		this.init();
	}

	init() {
		this.removeRedirectCookie();
		this.setRedirectCookie(window.location.hash);

		this.autologin();
	}

	login(loginData: LoginData): Observable<LoginInfo> {
		this.loginInProgress = true;
		this.removeLastEntityCookie();
		this.credentialsExpired = undefined;

		return this.busyService.busy(
			this.http.post(this.nuclosConfig.getRestHost(), JSON.stringify(loginData)).pipe(
				tap((data: LoginInfo) => {
					this.loginSuccessful(data);
				}),
				catchError(error => {
					this.$log.debug('Login with data %o failed: ', loginData, error);
					return observableThrowError(error);
				}),
				finalize(() => (this.loginInProgress = false))
			)
		);
	}

	onAuthenticationDataAvailable(): BehaviorSubject<boolean> {
		return this.authenticationDataAvailable;
	}

	getCurrentUser(): User | undefined {
		return this.currentUser;
	}

	/**
	 * Determines if the given Action is allowed for the current user.
	 */
	isActionAllowed(action: UserAction): boolean {
		if (this.isSuperUser()) {
			return true;
		}

		// Check allowed actions if available
		if (this.authenticationDataAvailable.getValue() && this.allowedActions) {
			let actionString = UserAction[action];
			return this.allowedActions[actionString];
		}

		return false;
	}

	/**
	 * Determines if the current user is logged in as "anonymous",
	 * based on the given data or the current username.
	 */
	isAnonymous(data?: LoginInfo): boolean {
		let username = (data && data.username) || this.getUsername();

		return (
			StringUtils.equalsIgnoreCase(username, this.ANONYMOUS_USER_NAME) &&
			this.anonymousLoginAllowed
		);
	}

	autoNavigation(): Promise<boolean> {
		if (AuthenticationService.isAutoNavigationAllowed()) {
			let redirect = this.getRedirectCookie();
			if (redirect) {
				this.removeRedirectCookie();
				// Prevent redirecting to the same location
				if (!window.location.href.endsWith(redirect)) {
					this.$log.debug('Redirecting to: %o', redirect);
					return this.router.navigateByUrl(redirect);
				}
			} else {
				return this.navigateHome();
			}
		}
		return Promise.resolve(false);
	}

	navigateHome(): Promise<boolean> {
		return this.router.navigate(['dashboard']);
	}

	navigateToLastEntity(): void {
		let redirect = this.getLastEntityCookie();
		if (redirect) {
			this.router.navigate(['/view', redirect]);
		} else {
			this.navigateHome();
		}
	}

	getMandatorSelection(): Subject<Mandator | undefined> {
		return this.mandatorSelection;
	}

	getCurrentMandatorId(): string | undefined {
		let mandatorData = this.mandatorSelection.getValue();
		return mandatorData ? mandatorData.mandatorId : undefined;
	}

	selectMandator(mandator): Observable<any> {
		let mandatorSelection = this.http.post(mandator.links.chooseMandator.href, {});
		this.setLastMandatorCookie(mandator);
		return mandatorSelection.pipe(tap(() => this.mandatorSelection.next(mandator)));
	}

	isLoginInProgress() {
		return this.loginInProgress;
	}

	waitForLogin(): Observable<any> {
		if (this.isLoginInProgress()) {
			return this.observeLoginStatus().pipe(
				filter(loggedIn => loggedIn),
				take(1)
			);
		}
		return observableOf(true);
	}

	/**
	 * Determines if the page with the given location requires authentication.
	 * This is not the case for start, login/registration and error pages.
	 */
	isAuthenticationRequired(locationPath) {
		let result =
			locationPath.indexOf('/login') === -1 &&
			locationPath.indexOf('/logout') === -1 &&
			locationPath.indexOf('/locale/') === -1 &&
			locationPath !== '' &&
			locationPath !== '/' &&
			locationPath.indexOf('/error') === -1 &&
			locationPath.indexOf('/account/register') === -1 &&
			locationPath.indexOf('/account/activate') === -1 &&
			// Password change must be possible without login for expired credentials
			locationPath.indexOf('/account/changePassword') === -1 &&
			locationPath.indexOf('/account/forgotLoginDetails') === -1 &&
			locationPath.indexOf('/account/resetPassword') === -1;

		this.$log.debug('isAuthenticationRequired(%o) = %o', locationPath, result);

		return result;
	}

	hasRedirectCookie() {
		return this.getRedirectCookie() !== undefined;
	}

	setRedirectCookie(path: string) {
		path = path ? path.replace(/^#*/, '') : '';
		this.$log.info('Setting redirect cookie for location: %o', path);
		if (this.isAuthenticationRequired(path)) {
			this.$log.debug('Setting redirect cookie: %o', path);
			this.cookieService.put(AuthenticationService.COOKIE_KEY_REDIRECT, path);
		}
	}

	removeRedirectCookie() {
		this.$log.debug('Removing redirect cookie');
		this.cookieService.remove(AuthenticationService.COOKIE_KEY_REDIRECT);
	}

	getRedirectCookie() {
		let redirect = this.cookieService.getCached(AuthenticationService.COOKIE_KEY_REDIRECT);
		return redirect ? redirect : undefined;
	}

	getLastEntityCookie() {
		let redirect = this.cookieService.getCached(AuthenticationService.COOKIE_KEY_LAST_ENTITY);
		return redirect ? redirect : undefined;
	}

	removeLastEntityCookie() {
		this.$log.debug('Removing last Entity cookie');
		this.cookieService.remove(AuthenticationService.COOKIE_KEY_LAST_ENTITY);
	}

	setLastEntityCookie(entityClassId: string | undefined) {
		if (entityClassId) {
			this.cookieService.put(AuthenticationService.COOKIE_KEY_LAST_ENTITY, entityClassId);
		}
	}

	setLastMandatorCookie(mandator: Mandator) {
		this.cookieService.put(AuthenticationService.COOKIE_KEY_LAST_MANDATOR, mandator.mandatorId);
	}

	getLastMandatorIdCookie() {
		let lastMandatorId = this.cookieService.getCached(
			AuthenticationService.COOKIE_KEY_LAST_MANDATOR
		);
		return lastMandatorId ? lastMandatorId : undefined;
	}

	invalidateSessionId() {
		let auth: LoginInfo = this.cookieService.getObjectCached('authentication');
		if (auth) {
			delete auth.sessionId;
			this.cookieService.putObject('authentication', auth);
		}
	}

	/**
	 * Sets the redirect cookie for a redirect to the last location that is stored via the "location" cookie,
	 * if the user is not anonymous and does not have a redirect cookie already.
	 */
	redirectToLastLocation(data: LoginInfo) {
		if (this.isAnonymous(data) || this.hasRedirectCookie()) {
			return;
		}

		let username = data.username;
		let lastLocation = this.cookieService.getCached('location.' + username);

		if (lastLocation) {
			this.$log.info('Redirecting to last location: %o', lastLocation);
			this.setRedirectCookie(lastLocation);
			return true;
		}

		return false;
	}

	/**
	 * Sets the redirect cookie for a redirect (after login) to the current location.
	 *
	 * @returns {boolean}
	 */
	redirectToCurrentLocation() {
		let location = window.location.hash;
		if (location && this.isAuthenticationRequired(location)) {
			this.setRedirectCookie(location);
			return true;
		}
		return false;
	}

	/**
	 * Redirects immediately to the login page.
	 */
	redirectToLoginPage() {
		if (this.router.url !== '/login') {
			this.$log.debug('Redirecting to login page...');
			this.router.navigate(['/login']);
		}
	}

	getUsername(): string | undefined {
		return this.cookieService.getCached('authentication')
			? (<LoginInfo>this.cookieService.getObjectCached('authentication')).username
			: undefined;
	}

	getAuthentication(): LoginInfo | undefined {
		return this.authentication;
	}

	/**
	 * Determines if the current user is a superuser.
	 */
	isSuperUser(): boolean {
		// TODO: Use current auth data, not from cookie
		let authentication = this.getAuthentication();
		return !!(authentication && authentication.superUser);
	}

	/**
	 * Deletes the current session from the server and invalidates the session ID.
	 */
	logout(): Observable<any> {
		return this.http.delete(this.nuclosConfig.getRestHost()).pipe(
			tap(data => {
				this.$log.debug('Logout successful', data);
				this.invalidateSessionId();
				this.currentUser = undefined;
				this.authentication = undefined;
				this.router.navigateByUrl('/login');
				this.setLoggedIn(false);
				this.removeLastEntityCookie();
			}),
			catchError(error => {
				this.$log.error('Logout failed', error);
				this.invalidateSessionId();
				this.authentication = undefined;
				this.router.navigateByUrl('/login');
				this.setLoggedIn(false);
				this.removeLastEntityCookie();
				return observableThrowError(error);
			})
		);
	}

	/**
	 * Possibly saves the given url in the "last location" cookie.
	 * The user is redirected to this location after the next login.
	 */
	rememberUrl(url: string) {
		this.$log.debug('Remembering URL: %o', url);

		let key = 'location.' + this.getUsername();
		this.cookieService.put(
			key,
			url,
			new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 365)
		);
	}

	shouldUrlBeRemembered(url: string) {
		return (
			!this.isAnonymous() &&
			this.getUsername() &&
			this.isAuthenticationRequired(url) &&
			!url.endsWith('/new') &&
			!url.startsWith('/popup') &&
			!url.startsWith('/account/changePassword') &&
			!url.endsWith('index.html')
		);
	}

	isLoggedIn(): boolean {
		return this.loggedIn;
	}

	observeLoginStatus(): Observable<boolean> {
		return this.loginSubject;
	}

	resetWorkspace() {
		let link = this.nuclosConfig.getRestHost() + '/meta/resetworkspace';
		this.http.post(link, {}).subscribe(() => window.location.reload());
	}

	setCredentialsExpired(credentialsExpired: UserPassword) {
		this.credentialsExpired = credentialsExpired;
		this.router.navigate(['account', 'changePassword']);
	}

	getCredentialsExpired() {
		return this.credentialsExpired;
	}

	private checkLogin(loggedIn, url: string) {
		this.$log.debug('Logged in: %o', loggedIn);
		if (!loggedIn) {
			this.redirectToLoginPage();
		} else {
			// Remember the last location after successful navigation
			this.$log.debug('Location changed: %o', url);
			if (this.shouldUrlBeRemembered(url)) {
				this.rememberUrl(url);
			}
		}
	}

	/**
	 * Performs a login as the anonymous user.
	 */
	private loginAsAnonymous(): Observable<LoginInfo> {
		this.$log.info('Trying anonymous login...');
		let loginData = {
			username: this.ANONYMOUS_USER_NAME,
			password: this.ANONYMOUS_USER_NAME,
			locale: this.i18nService.getCurrentLocale().key,
			autologin: false
		};

		return this.login(loginData);
	}

	/**
	 * Handles login data after a successful login.
	 */
	private loginSuccessful(data: LoginInfo) {
		this.$log.info('LOGIN SUCCESSFUL: %o', data);

		this.authentication = data;

		this.currentUser = new User(data.username);
		this.allowedActions = (data.allowedActions as any) as Map<string, boolean>;
		this.authenticationDataAvailable.next(true);

		this.redirectToLastLocation(data);
		this.redirectToInitialEntity(data);

		// SAVE COOKIE
		this.cookieService.putObject('authentication', data);

		if (data.mandator) {
			this.mandatorSelection.next(data.mandator);
		}

		if (!data.mandators) {
			this.autoNavigation();
		}

		this.setLoggedIn(true);
	}

	private autologin() {
		this.loginInProgress = true;

		let checkIfLoggedIn = this.http.get(this.nuclosConfig.getRestHost()).pipe(
			tap((data: LoginInfo) => {
				this.loginSuccessful(data);
			}),
			catchError(() => EMPTY)
		);

		let anonymousLogin = this.anonymousLogin();

		this.busyService
			.busy(
				checkIfLoggedIn.pipe(
					concat(anonymousLogin),
					take(1),
					finalize(() => (this.loginInProgress = false))
				)
			)
			.subscribe();
	}

	/**
	 * Logs in as anonymous, if the anonymous login is enabled via system parameter.
	 */
	private anonymousLogin() {
		return this.nuclosConfig.getSystemParameters().pipe(
			mergeMap(params => {
				this.$log.debug('Got system params: %o', params);
				if (params.is(SystemParameter.ANONYMOUS_USER_ACCESS_ENABLED)) {
					return this.loginAsAnonymous().pipe(
						tap(data => this.$log.debug('Logged in as anonymous: %o', data))
					);
				} else {
					return EMPTY;
				}
			})
		);
	}

	/**
	 * Sets the redirect cookie to the "initial entity" of this user, if there is not
	 * already a redirect cookie set.
	 */
	private redirectToInitialEntity(data: LoginInfo) {
		if (!data.initialEntity || this.hasRedirectCookie()) {
			return;
		}

		this.setRedirectCookie('/view/' + data.initialEntity);
	}

	private setLoggedIn(loggedIn: boolean) {
		this.loggedIn = loggedIn;
		this.loginSubject.next(loggedIn);
	}

	private setUnauthorized() {
		this.setLoggedIn(false);

		if (AuthenticationService.isAutoNavigationAllowed()) {
			/**
			 * Redirects back to the current location after login.
			 */
			this.redirectToCurrentLocation();
			this.router.navigate(['/login']);
		}
	}
}
