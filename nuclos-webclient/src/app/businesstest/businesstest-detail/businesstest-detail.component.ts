import { AfterContentInit, AfterViewInit, Component, NgZone, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { IBusinessTestVO } from '../../../../nuclos_typings/nuclos/businesstest';
import { Logger } from '../../log/shared/logger';
import { BusinesstestLogHandler, BusinesstestService } from '../shared/businesstest.service';

declare var ace;

@Component({
	selector: 'nuc-businesstest-detail',
	templateUrl: './businesstest-detail.component.html',
	styleUrls: ['./businesstest-detail.component.css']
})
export class BusinesstestDetailComponent implements OnInit, OnDestroy, AfterViewInit, AfterContentInit {
	@ViewChild('editor') editor;

	test: IBusinessTestVO;
	testId: string;

	/**
	 * Source must be kept separate in order to prevent angular from re-rendering the editor component
	 * with every key press.
	 * */
	source: string;

	started = false;
	new = false;
	log = '';
	logHandler: BusinesstestLogHandler;
	firstChange = true;
	private session;

	private unsubscribe$ = new Subject<void>();

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private businesstestService: BusinesstestService,
		private $log: Logger,
		private ngZone: NgZone
	) {
		this.logHandler = {
			start: () => {
				this.started = true;
				this.log = '';
			},
			receiveMessage: message => {
				this.log += message;
				let output = $('#testLog');
				if (output.length) {
					output.scrollTop(output[0].scrollHeight - output.height());
				}
			},
			receiveProgress: () => {},
			done: () => {
				this.started = false;
			},
			fail: () => {
				this.started = false;
			}
		};
	}

	ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	ngOnInit() {
		this.route.params.subscribe(params => {
			this.$log.debug('params = %o', params);
			this.setTestId(params['testId']);
		});
	}

	ngAfterViewInit(): void {
		this.$log.debug('Ace loaded %o', this.editor);

		this.editor.setTheme('eclipse');

		let _editor = this.editor._editor;

		_editor.setShowInvisibles(true);
		_editor.setFontSize('14');
		_editor.setWrapBehavioursEnabled(true);
		_editor.$blockScrolling = Infinity;

		this.session = _editor.getSession();

		this.session.setUseSoftTabs(false);
		this.session.setUndoManager(new ace.UndoManager());

		// Workaround for a bug with the Ace editor, which starts a macro task that never finishes...
	}

	/**
	 * The test data is loaded only after content init,
	 * else the highlighting would not work initially.
	 */
	ngAfterContentInit(): void {
		if (this.new) {
			this.test = <IBusinessTestVO>{};
			// TODO: This is a hack. Do not set "hasPendingMacroTasks" manually!
			this.ngZone['hasPendingMacrotasks' + ''] = false;
		} else {
			this.refresh(() => {
				this.log = this.test.log;
				// TODO: This is a hack. Do not set "hasPendingMacroTasks" manually!
				this.ngZone['hasPendingMacrotasks' + ''] = false;
			});
		}
	}

	/**
	 * Sets the test source after it was changed in the ace editor.
	 *
	 * @param source
	 */
	setSource(source: string): void {
		if (this.test) {
			this.source = source;
			this.clearHighlights();
		}
	}

	save(): Subscription {
		return this._save().pipe(take(1)).subscribe(test => (this.test = test));
	}

	/**
	 * Runs the current test and continually prints the test logs.
	 */
	run() {
		this.started = true;
		this._save().pipe(take(1)).subscribe(
			() =>
				this.businesstestService
					.runTest(this.logHandler, this.testId)
					.pipe(takeUntil(this.unsubscribe$))
					.subscribe(() => this.refresh()),
			error => {
				this.log = 'Unable to save test: ' + error;
				this.started = false;
			}
		);
	}

	/**
	 * Deletes the current test and navigates back to the overview.
	 */
	delete() {
		this.businesstestService.deleteTest(this.testId).pipe(take(1)).subscribe(() => {
			this.$log.debug('Test deleted');
			this.router.navigate(['/businesstests/advanced']);
		});
	}

	private setTestId(testId: string) {
		this.testId = testId;
		this.new = this.testId === 'new';

		if (this.new) {
			this.test = <IBusinessTestVO>{};
		} else {
			this.refresh(() => (this.log = this.test.log));
		}
	}

	/**
	 * Saves the current test (may be a new test or modified).
	 * Possibly navigates to the new test URL, if the test was new.
	 *
	 * @returns {Observable<IBusinessTestVO>}
	 * @private
	 */
	private _save(): Observable<IBusinessTestVO> {
		let observable: Observable<IBusinessTestVO>;

		this.test.source = this.source;

		if (this.new) {
			observable = this.createNew();
		} else {
			observable = this.businesstestService.save(this.test);
		}

		return new Observable<IBusinessTestVO>(observer => {
			observable.pipe(take(1)).subscribe(
				test => {
					this.$log.debug('Saved test: %o', test);
					this.test = test;
					observer.next(test);
					observer.complete();
					if (this.new) {
						this.router.navigate(['/businesstests', test.id.string]);
					}
				},
				error => observer.error(error)
			);
		});
	}

	/**
	 * Saves the current test as a new record.
	 *
	 * @returns {Observable<IBusinessTestVO>}
	 */
	private createNew(): Observable<IBusinessTestVO> {
		return new Observable<IBusinessTestVO>(observer => {
			this.businesstestService.createNew(this.test).pipe(take(1)).subscribe(
				test => {
					this.$log.debug('Saved test: %o', test);
					this.test = test;
					observer.next(test);
					observer.complete();
					this.router.navigate(['/businesstests', test.id.string]);
				},
				error => observer.error(error)
			);
		});
	}

	/**
	 * Reloads the current test.
	 *
	 * @param callback
	 */
	private refresh(callback?: Function): Subscription {
		this.$log.debug('Refreshing...');
		return this.businesstestService.getTest(this.testId).pipe(take(1)).subscribe(test => {
			this.$log.debug('Test = %o', test);
			this.test = test;
			this.source = test.source;
			if (callback) {
				callback();
			}
			this.highlight();
			this.session.on('change', () => {
				// if (this.firstChange) {
				// 	this.firstChange = false;
				// 	return;
				// }
				this.clearHighlights();
			});
		});
	}

	/**
	 * Clears all error/warning markers in the editor.
	 */
	private clearHighlights() {
		this.$log.debug('clearHighlights()');
		let markers = this.session.getMarkers(false);
		this.$log.debug('Markers: %o', markers);
		for (let i in markers) {
			if (!markers[i]) {
				continue;
			}
			let marker = markers[i];
			if (marker.type === 'fullLine') {
				this.session.removeMarker(marker.id);
			}
			// this.$log.debug('Marker %o => %o', i, marker);
			// this.$log.debug('Removing marker %o', marker);
		}
	}

	/**
	 * Highlights erroneous lines in the editor.
	 */
	private highlight() {
		this.clearHighlights();
		if (this.test.errorLine) {
			this.highlightLine(this.test.errorLine - 1, 'ace_error');
		} else if (this.test.warningLine) {
			this.highlightLine(this.test.warningLine - 1, 'ace_warning');
		}
	}

	private highlightLine(line: number, clazz: string) {
		this.$log.debug('Highlighting line %o with class %o', line, clazz);

		let Range = ace.require('ace/range').Range;
		let range: Range = new Range(line, 0, line, Infinity);
		this.session.addMarker(range, clazz, 'fullLine', false);
		this.editor._editor.scrollToLine(line, true, true, () => {});

		this.$log.debug('highlighting done');
	}
}
