import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BusinesstestOverallResult } from '../../../../nuclos_typings/nuclos/businesstest';
import { BusinesstestService } from '../shared/businesstest.service';

@Component({
	selector: 'nuc-businesstest-overview',
	templateUrl: './businesstest-overview.component.html',
	styleUrls: ['./businesstest-overview.component.css']
})
export class BusinesstestOverviewComponent implements OnInit, OnDestroy {
	overallResult: BusinesstestOverallResult | null;

	private unsubscribe$ = new Subject<void>();

	constructor(private businesstestService: BusinesstestService) {
	}

	ngOnInit() {
		this.businesstestService.getOverallResult().pipe(takeUntil(this.unsubscribe$)).subscribe(result => this.overallResult = result);
		this.businesstestService.updateOverallResult().pipe(takeUntil(this.unsubscribe$)).subscribe();
	}

	ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}
}
