import { RouterModule, Routes } from '@angular/router';
import { BusinesstestAdvancedComponent } from './businesstest-advanced/businesstest-advanced.component';
import { BusinesstestDetailComponent } from './businesstest-detail/businesstest-detail.component';
import { BusinesstestComponent } from './businesstest.component';

export const ROUTE_CONFIG: Routes = [
	{
		path: '',
		component: BusinesstestComponent
	},
	{
		path: 'advanced',
		component: BusinesstestAdvancedComponent
	},
	{
		path: ':testId',
		component: BusinesstestDetailComponent
	}
];

export const BusinesstestRoutesModule = RouterModule.forChild(ROUTE_CONFIG);
