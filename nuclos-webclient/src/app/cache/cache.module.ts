import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { I18nModule } from '../i18n/i18n.module';
import { CacheOverviewComponent } from './cache-overview/cache-overview.component';
import { CacheRoutesModule } from './cache.routes';
import { NuclosCacheService } from './shared/nuclos-cache.service';

@NgModule({
	imports: [
		CommonModule,
		RouterModule,
		I18nModule,

		CacheRoutesModule
	],
	declarations: [
		CacheOverviewComponent
	],
	providers: [
		NuclosCacheService
	]
})
export class CacheModule {
}
