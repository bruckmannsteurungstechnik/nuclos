import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, publishReplay, refCount } from 'rxjs/operators';
import { MapEntriesPipe } from '../../shared/map-entries.pipe';

/**
 * A service for caching the (singular) result of an Observable.
 */
@Injectable({
	providedIn: 'root'
})
export class NuclosCacheService {

	private caches: Map<string, NuclosCache>;

	constructor() {
		this.caches = new Map<string, NuclosCache>();
	}

	getCache(key: string) {
		let result = this.caches.get(key);

		if (!result) {
			result = new NuclosCache();
			this.caches.set(key, result);
		}

		return result;
	}

	getAllCaches() {
		return this.caches;
	}

	invalidateAllCaches() {
		this.caches.forEach(
			cache => cache.clear()
		);
	}
}

export class NuclosCache {
	private cache: Map<string, Observable<any>>;

	constructor() {
		this.cache = new Map<string, Observable<any>>();
	}

	/**
	 * Returns the corresponding Observable for the given key from the cache
	 * or caches the given Observable under the given key and returns it.
	 *
	 * If the Observable throws an error, it is removed from the cache and therefore
	 * retried when it is called the next time.
	 * This prevents e.g. caching of failed HTTP requests.
	 *
	 * @param key
	 * @param observable
	 * @returns {Observable<any>}
	 */
	get(key: string, observable: Observable<any>): Observable<any> {
		let result = this.cache.get(key);

		if (!result) {
			result = observable.pipe(
				publishReplay(1),
				refCount(),
				catchError((error) => {
					this.delete(key);
					throw error;
				}),
			);
			this.cache.set(key, result);
		}

		return result;
	}

	delete(key: string) {
		this.cache.delete(key);
	}

	entries(): { key, value }[] {
		return new MapEntriesPipe().transform(this.cache);
	}

	clear() {
		this.cache.clear();
	}
}
