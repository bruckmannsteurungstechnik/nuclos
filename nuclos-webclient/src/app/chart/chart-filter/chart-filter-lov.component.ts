import { Component, Input, ViewChild } from '@angular/core';
import { SearchTemplate } from '../shared/search-template';
import { EoChartWrapper } from '../shared/eo-chart-wrapper';
import { AutoComplete } from 'primeng/primeng';
import { ChartService } from '../shared/chart.service';

@Component({
	selector: 'nuc-chart-filter-lov',
	templateUrl: './chart-filter-lov.component.html'
})
export class ChartFilterLovComponent {

	@Input() searchTemplate: SearchTemplate;
	@Input() eoChart: EoChartWrapper;


	/**
	 * TODO: Extract a re-usable Autocomplete component, do not re-implement it here.
	 */
	@ViewChild('autoComplete') autoComplete: AutoComplete;

	constructor(
		private chartService: ChartService
	) {
	}


	/**
	 * TODO: Extract a re-usable Autocomplete component, do not re-implement it here.
	 */
	togglePanel() {
		if (this.autoComplete.panelVisible) {
			this.hidePanel();
		} else {
			this.showPanel();
		}
	}

	showPanel() {
		this.autoComplete.panelVisible = true;
		this.autoComplete.show();

		let element = this.autoComplete.panelEL.nativeElement;
		let inputEl = this.autoComplete.inputEL.nativeElement;
		let inputWidth = $(inputEl).outerWidth();
		$(element).outerWidth(inputWidth);
	}

	hidePanel() {
		this.autoComplete.hide();
	}

	search() {
		this.chartService.search(this.eoChart);
	}
}
