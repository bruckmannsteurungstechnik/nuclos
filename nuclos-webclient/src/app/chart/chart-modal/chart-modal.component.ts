import { Component, Input, OnInit } from '@angular/core';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';

@Component({
	selector: 'nuc-chart-modal',
	templateUrl: './chart-modal.component.html',
	styleUrls: ['./chart-modal.component.css']
})
export class ChartModalComponent implements OnInit {

	@Input() eo: EntityObject;

	constructor() {
	}

	ngOnInit() {
	}
}
