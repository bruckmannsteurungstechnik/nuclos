import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartTabsComponent } from './chart-tabs.component';

xdescribe('ChartTabsComponent', () => {
	let component: ChartTabsComponent;
	let fixture: ComponentFixture<ChartTabsComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ChartTabsComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ChartTabsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
