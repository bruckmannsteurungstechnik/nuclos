import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { EntityObject } from '../../../entity-object-data/shared/entity-object.class';
import { ChartService } from '../../shared/chart.service';
import { EoChartWrapper } from '../../shared/eo-chart-wrapper';

@Component({
	selector: 'nuc-chart-tabs',
	templateUrl: './chart-tabs.component.html',
	styleUrls: ['./chart-tabs.component.css']
})
export class ChartTabsComponent implements OnInit, OnDestroy {

	@Input() eo: EntityObject;

	selectedEoChart: EoChartWrapper | undefined;

	eoCharts: EoChartWrapper[] = [];

	private unsubscribe$ = new Subject<void>();

	constructor(
		private chartService: ChartService
	) {
	}

	ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	ngOnInit() {
		this.chartService.getCharts(this.eo).pipe(takeUntil(this.unsubscribe$)).subscribe(
			charts => {
				this.eoCharts = [];
				for (let chart of charts) {
					this.eoCharts.push(
						new EoChartWrapper(
							this.eo,
							chart
						)
					);
				}
				if (this.eoCharts.length > 0) {
					this.selectedEoChart = this.eoCharts[0];
				}
			}
		);
	}

	tabId(index: number) {
		return 'chart-tab-' + index;
	}

	newChart() {
		let eoChart = this.chartService.newChart(this.eo);
		this.eoCharts.push(eoChart);

		if (!this.chartService.isShowConfig()) {
			this.chartService.toggleConfig();
		}

		this.selectChart(eoChart);
	}

	selectChart(eoChart) {
		this.selectedEoChart = eoChart;
	}

	canConfigureChart() {
		return this.chartService.canConfigureCharts();
	}
}
