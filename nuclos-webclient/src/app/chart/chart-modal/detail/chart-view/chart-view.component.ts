import { Component, HostListener, Input, NgZone, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription, Subject, EMPTY } from 'rxjs';
import { map, tap, takeUntil, take, mergeMap } from 'rxjs/operators';
import { EntityMeta } from '../../../../entity-object-data/shared/bo-view.model';
import { SubEntityObject } from '../../../../entity-object-data/shared/entity-object.class';
import { Logger } from '../../../../log/shared/logger';
import { ObjectUtils } from '../../../../shared/object-utils';
import { ChartService } from '../../../shared/chart.service';
import { EoChartWrapper } from '../../../shared/eo-chart-wrapper';
import { ChartOptions } from '../../../shared/chart-options';

import * as c3 from 'c3';
import { ChartConfiguration } from 'c3';
import { ChartViewService } from './chart-view.service';
import { ChartData } from './chart-view.model';
import { FqnService } from '../../../../shared/fqn.service';

@Component({
	selector: 'nuc-chart-view',
	templateUrl: './chart-view.component.html',
	styleUrls: ['./chart-view.component.css']
})
export class ChartViewComponent implements OnInit, OnDestroy {
	@Input() eoChart: EoChartWrapper;

	chartOptions: ChartOptions;
	chartData: ChartData;

	subMeta: EntityMeta;

	loading = false;

	private subscription: Subscription;
	private subEos: SubEntityObject[] | undefined;
	private unsubscribe$ = new Subject<void>();

	constructor(
		private chartService: ChartService,
		private chartViewService: ChartViewService,
		private ngZone: NgZone,
		private $log: Logger
	) {}

	ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	ngOnInit() {
		this.chartOptions = this.getChartOptions();
		this.getDependents().pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
			this.buildChart();
		});

		this.eoChart.chartOptionsChanged.pipe(takeUntil(this.unsubscribe$)).subscribe(() => this.updateChartOptions());
	}

	// refresh chart on window resize
	@HostListener('window:resize', ['$event'])
	onResize() {
		this.buildChart();
	}

	isConfigurationValid() {
		return this.chartService.isValidChartPreference(this.eoChart.chartPreference.content);
	}

	getChartDataCount() {
		return this.eoChart.getCurrentDataCount();
	}

	getChartSeriesCount() {
		return this.eoChart.getChartSeriesCount();
	}

	private updateChartOptions() {
		this.chartOptions = this.getChartOptions();
		this.getDependents().pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
			this.buildChart();
		});
	}

	private getChartOptions() {
		if (this.isConfigurationValid()) {
			try {
				let clone = ObjectUtils.cloneDeep(this.eoChart.chartPreference.content);
				return clone;
			} catch (e) {
				this.$log.warn(e);
			}
		}
		return undefined;
	}

	/**
	 * open data entry in new tab
	 * @param index of chart data entry
	 */
	private openDetail(index: number) {
		if (this.subEos && this.eoChart.chartPreference.content.openReferenceOnClick) {
			const subEo = this.subEos[index];
			subEo.getMeta().pipe(take(1)).subscribe(meta => {
				// use the details entity if it is configured in the diagram datasource
				let entityClassId = meta.getDetailEntityClassId()
					? meta.getDetailEntityClassId()
					: subEo.getEntityClassId();
				const href = '#/view/' + entityClassId + '/' + subEo.getId();
				this.$log.info('Opening entry in new tab: ', index, href);
				window.open(href, '');
			});
		}
	}

	private getDependents(): Observable<void> {
		if (!this.isConfigurationValid()) {
			return EMPTY;
		}
		return this.updateSubMeta().pipe(
			mergeMap(_ => this.subscribeToDependents())
		);
	}

	private subscribeToDependents(): Observable<void> {
		return new Observable(observer => {
			if (this.subscription) {
				this.subscription.unsubscribe();
			}

			this.loading = true;

			if (this.eoChart) {
				this.subscription = this.eoChart.dataSubject
					.pipe(
						map(subEos => {
							this.subEos = subEos;

							let chartData: ChartData | undefined;
							if (subEos) {
								chartData = this.chartViewService.toChartData(
									this.eoChart,
									this.subMeta,
									subEos
								);
							}

							return chartData;
						})
					)
					.subscribe(data => {
						if (data) {
							this.chartData = data;
							this.$log.debug('chartData = %o', data);
							this.chartService.prepareChartOptions(
								this.chartOptions,
								this.chartData
							);
							this.loading = false;
							observer.next();
						}
					});

				// Initially load subform data
				if (
					$('#chart svg').length === 0 ||
					this.chartData === undefined ||
					this.chartData.series.length === 0
				) {
					this.chartService.search(this.eoChart);
				}
			} else {
				this.$log.warn('No EO to load chartData');
			}
		});
	}

	private updateSubMeta() {
		let referenceAttributeId = this.eoChart.chartOptions.primaryChart.refBoAttrId;
		return this.chartService
			.getSubEntityMeta(this.eoChart.eo, referenceAttributeId)
			.pipe(tap(meta => (this.subMeta = meta), take(1)));
	}

	private buildChart() {
		if (!this.chartData) {
			return;
		}

		// transform to c3 data structure
		let columns = this.chartData.series.map(series =>
			series.values.map(data => (data.seriesValue ? data.seriesValue : 0))
		);

		// add label as first value
		columns.forEach((col, index) => {
			col.unshift(this.chartData.series[index].key);
		});

		let categoryIsDateType = this.categoryIsDateType();

		let chartConfig: ChartConfiguration = {
			bindto: '#chart',
			data: {
				type: this.chartOptions.chart.type,
				types: {}, // multple types for linePlusBar charts
				columns: columns
			},
			axis: {
				x: categoryIsDateType
					? {
							type: 'timeseries',
							tick: { format: '%d.%m.%Y' }
					}
					: {
							// needed for string values on x-axis
							type: 'category'
					}
			}
		};

		// add click handler to open data point in new tab
		chartConfig.data.onclick = d => {
			this.openDetail(d.index);
		};

		if (this.chartOptions.chart.type !== 'pie') {
			let categoryValues = this.chartData.category.values
				.filter(v => v !== undefined)
				.map(v => (v.name ? v.name : v)); // transform reference values
			categoryValues.unshift('x');
			chartConfig.data.columns!.unshift(categoryValues);
			chartConfig.data.x = 'x';
		}

		switch (this.chartOptions.chart.type) {
			case 'multiBar':
				chartConfig.data.type = 'bar';
				break;
			case 'multiBarHorizontal':
				chartConfig.data.type = 'bar';
				chartConfig.axis = {
					rotated: true
				};
		}

		// combined chart type (each serie has it's own chart type)
		if (
			this.chartOptions.chart.primaryChart &&
			this.chartOptions.chart.type === 'linePlusBar'
		) {
			const types = this.chartOptions.chart.primaryChart.series.map(serie =>
				serie.type ? serie.type : 'bar'
			);
			chartConfig.data.types = {};
			if (chartConfig.data.columns) {
				chartConfig.data.columns
					.filter(column => column[0] !== 'x') // ignore x axis column
					.forEach((seriesColumn, index) => {
						const key: string = <string>seriesColumn[0];
						if (chartConfig.data.types) {
							chartConfig.data.types[key] = types[index];
						}
					});
			}
		}

		// colors
		chartConfig.data.colors = {};
		this.chartData.series.forEach(serie => {
			if (serie.key !== undefined && serie.color !== undefined) {
				chartConfig.data.colors![serie.key] = serie.color;
			}
		});

		c3.generate(chartConfig);

		// TODO: This is a hack. Do not set "hasPendingMacroTasks" manually!
		this.ngZone['hasPendingMacrotasks' + ''] = false;
	}

	private categoryIsDateType() {
		if (this.subMeta) {
			const subEntityClassId = this.subMeta.getEntityClassId();
			const categoryAttributeName = FqnService.getShortAttributeNameFailsafe(
				subEntityClassId,
				this.chartOptions.chart.primaryChart.categoryBoAttrId
			);
			let categoryIsDateType =
				this.subMeta.getAttribute(categoryAttributeName).type === 'Timestamp' ||
				this.subMeta.getAttribute(categoryAttributeName).type === 'Date';
			return categoryIsDateType;
		}
		return undefined;
	}
}
