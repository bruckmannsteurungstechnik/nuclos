import { ChartSeries } from './chart-series';
import { SearchTemplate } from './search-template';

export interface ChartOptions {
	type: string;
	primaryChart: {
		boMetaId: string;
		refBoAttrId: string;
		categoryBoAttrId: string;
		series: ChartSeries[];
		sort?: {
			sortByBoAttrId?: string;
			sortDirection?: 'asc' | 'desc';
		};
	};
	x?: string | Function;
	y?: string | Function;
	yAxis?: {
		tickFormat?: string | Function
	};
	xAxis?: {
		tickFormat?: string | Function
	};
	searchTemplate?: SearchTemplate[];

	chart?: any;
}
