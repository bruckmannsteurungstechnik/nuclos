import { BehaviorSubject, Subject } from 'rxjs';
import { EntityObject, SubEntityObject } from '../../entity-object-data/shared/entity-object.class';
import { ChartPreferenceContent, Preference } from '../../preferences/preferences.model';
import { ChartOptions } from './chart-options';
import { SearchTemplate } from './search-template';

export class EoChartWrapper {

	dataSubject = new BehaviorSubject<SubEntityObject[] | undefined>(undefined);
	searchItems: SearchTemplate[];

	chartOptions: ChartOptions;
	chartOptionsChanged = new Subject();

	constructor(
		public eo: EntityObject,
		public chartPreference: Preference<ChartPreferenceContent>
	) {
		this.setChartOptions(this.chartPreference.content.chart);
	}

	setChartData(data: SubEntityObject[] | undefined) {
		this.dataSubject.next(data);
	}

	getCurrentChartData() {
		return this.dataSubject.value;
	}

	getCurrentDataCount() {
		let data = this.getCurrentChartData();
		return data ? data.length : 0;
	}

	setPreferenceContent(content: string) {
		this.chartPreference.content = JSON.parse(content);
		this.chartPreference.dirty = true;
		this.setChartOptions(this.chartPreference.content.chart);
	}

	setChartOptions(options: ChartOptions) {
		this.chartOptions = options;
		this.chartOptionsChanged.next();
	}

	getEoDependents() {
		return this.eo.getDependents(
			this.chartOptions.primaryChart.refBoAttrId
		);
	}

	getChartSeriesCount() {
		return this.chartOptions.primaryChart.series.length;
	}
}
