import { LovEntry } from '../../entity-object-data/shared/bo-view.model';

export interface SearchTemplate {
	label: string;
	value: any;
	boAttrId: string;
	comparisonOperator: string;
	mandatory: boolean;
	dropdown: boolean;
	inputType?: 'dropdown' | 'string' | 'datepicker';
	url?: string;
	suggestions?: LovEntry[];
}
