import { Directive, ElementRef, EventEmitter, Input, OnInit, Output, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ClickOutsideService } from './click-outside.service';
import { EvaluateClickOutside } from './evaluate-click-outside.model';

/**
 * fires an event if a mouse-click is registered outside the enclosing HTML element
 */
@Directive({
	selector: '[nucClickOutside]'
})
export class ClickOutsideDirective implements OnInit, OnDestroy {

	@Output() clickOutside = new EventEmitter();

	/**
	 * evaluate if click was outside
	 * needed e.g. for popups
	 */
	@Input() evaluateClickOutside: EvaluateClickOutside;

	private clickOutsideEvents$: Subject<string> = new Subject<string>();
	private unsubscribe$ = new Subject<void>();

	constructor(
		private clickOutsideService: ClickOutsideService,
		private element: ElementRef
	) {

		this.clickOutsideEvents$.asObservable().pipe(takeUntil(this.unsubscribe$)).subscribe(
			(event) => {
				// send click outside event
				this.clickOutside.emit(event);
			}
		);
	}

	ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	ngOnInit(): void {
		this.clickOutsideService.registerCallback(
			this.clickOutsideEvents$,
			this.element,
			this.evaluateClickOutside
		);
	}
}
