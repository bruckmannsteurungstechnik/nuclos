import { ElementRef, Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { OutsideClick } from './click-outside.model';
import { EvaluateClickOutside } from './evaluate-click-outside.model';


@Injectable()
export class ClickOutsideService {

	private callbackList: Array<OutsideClick>;

	constructor() {

		this.callbackList = [];

		window.document.onclick = (mouseEvent: MouseEvent) => {
			this.outsideClick(mouseEvent.target);
		};
	}


	outsideClick(target: EventTarget | null): void {
		if (target) {
			for (let outsideClick of this.callbackList) {

				let clickedOnChild = $(target).closest(outsideClick.element.nativeElement).length > 0;
				let elementIsVisible = $(outsideClick.element.nativeElement).is(':visible');

				// in the subform the clicked element may be already replaced with the editor element
				// this would cause an incorrect clickoutside event
				if (document.documentElement !== null) {
					let clickedElementIsAttachedToDOM = $.contains(document.documentElement, $(target)[0]);

					if (!clickedOnChild && elementIsVisible && clickedElementIsAttachedToDOM &&
						(outsideClick.clickHandler === undefined || outsideClick.clickHandler(target))
					) {
						outsideClick.subject.next(target);
					}
				}
			}
		}
	}


	registerCallback(
		subject: Subject<any>,
		element: ElementRef,
		clickHandler: EvaluateClickOutside
	): void {
		let outsideClick: OutsideClick = {
			element: element,
			subject: subject,
			clickHandler: clickHandler
		};
		if (this.callbackList.indexOf(outsideClick) === -1) {
			this.callbackList.push(outsideClick);
		}
	}
}
