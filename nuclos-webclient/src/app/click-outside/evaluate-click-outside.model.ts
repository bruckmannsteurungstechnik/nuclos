export interface EvaluateClickOutside {
	(target: EventTarget): boolean;
}
