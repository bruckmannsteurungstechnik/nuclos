import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NuclosCookieService } from './shared/nuclos-cookie.service';

@NgModule({
	imports: [
		CommonModule,
	],
	declarations: [],
	providers: [
		NuclosCookieService
	]
})
export class CookieModule {
}
