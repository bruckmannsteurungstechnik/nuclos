import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthenticationService } from '../authentication/authentication.service';

@Component({
	selector: 'nuc-current-mandator',
	templateUrl: './current-mandator.component.html',
	styleUrls: ['./current-mandator.component.css']
})
export class CurrentMandatorComponent implements OnInit, OnDestroy {

	currentMandator: Mandator | undefined;

	private unsubscribe$ = new Subject<void>();

	constructor(private authenticationService: AuthenticationService) {

	}

	ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	ngOnInit() {
		this.authenticationService.observeLoginStatus().pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
			let authData = this.authenticationService.getAuthentication();
			if (authData) {
				if (authData.mandator) {
					this.currentMandator = authData.mandator;
				}
			}

		});
		this.authenticationService.observeLoginStatus().pipe(takeUntil(this.unsubscribe$)).subscribe(loggedIn => {
			if (!loggedIn) {
				this.currentMandator = undefined;
			}
		});
	}
}
