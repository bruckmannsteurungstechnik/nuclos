import { Component, EventEmitter, Input, OnChanges, OnInit, OnDestroy, Output, SimpleChanges, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime, take, takeUntil } from 'rxjs/operators';
import { EntityMetaData } from '../../entity-object-data/shared/bo-view.model';
import { Preference } from '../../preferences/preferences.model';
import { DropdownComponent } from '../../ui-components/dropdown/dropdown.component';
import { DashboardTasklistItem } from '../preference-items/dashboard-tasklist-item';
import { DashboardPreferenceContent } from '../shared/dashboard-preference-content';
import { DashboardService } from '../shared/dashboard.service';
import { TTResizableColumn } from 'primeng/primeng';

@Component({
	selector: 'nuc-dashboard-config',
	templateUrl: './dashboard-config.component.html',
	styleUrls: ['./dashboard-config.component.css']
})
export class DashboardConfigComponent implements OnInit, OnDestroy, OnChanges {
	taskListMetas: EntityMetaData[];

	@Input() dashboard: Preference<DashboardPreferenceContent>;
	@Input() itemsChanged: any;
	@Output() configurationChange = new EventEmitter();

	@ViewChild('taskLists') taskListDropdown: DropdownComponent;

	private configurationChangeSubject = new Subject();
	private unsubscribe$ = new Subject<void>();

	constructor(private dashboardService: DashboardService) {}

	ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	ngOnInit() {
		this.updateTaskLists();

		this.configurationChangeSubject
			.pipe(debounceTime(250), takeUntil(this.unsubscribe$))
			.subscribe(() => this.configurationChange.emit(this.dashboard));
	}

	ngOnChanges(changes: SimpleChanges): void {
		// Maybe a task list was removed from the dashboard, we must therefore update the task list dropdown
		if (changes['itemsChanged']) {
			this.updateTaskLists();
		}
	}

	addTaskList(taskList: EntityMetaData) {
		if (!taskList) {
			return;
		}

		let item = new DashboardTasklistItem(
			taskList.boMetaId,
			taskList.name || 'UNKNOWN',
			taskList.searchfilter
		);

		this.dashboard.content.items.push(item);

		this.taskListMetas = this.filterTaskLists(this.taskListMetas);
		this.clearInput();

		this.configurationChangeSubject.next();
	}

	updateTaskLists() {
		this.dashboardService.getTasklistMetas().pipe(take(1)).subscribe(taskListMetas => {
			this.taskListMetas = this.filterTaskLists(taskListMetas);
		});
	}

	filterTaskLists(taskLists: EntityMetaData[]) {
		return taskLists.filter(taskListMeta => !this.isTasklistOnDashboard(taskListMeta));
	}

	clearInput() {
		window.setTimeout(() => this.taskListDropdown.clearInput());
	}

	setName(name) {
		this.dashboard.name = name;
		this.configurationChangeSubject.next();
	}

	private isTasklistOnDashboard(taskListMeta: EntityMetaData): boolean {
		if (!this.dashboard) {
			return false;
		}

		return !!this.dashboard.content.items.find(
			item =>
				item.type === 'tasklist' &&
				(item as DashboardTasklistItem).taskList.entityClassId === taskListMeta.boMetaId
		);
	}
}
