import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { GridsterModule } from 'angular-gridster2';
import { EntityObjectModule } from '../entity-object/entity-object.module';
import { I18nModule } from '../i18n/i18n.module';
import { LogModule } from '../log/log.module';
import { TaskModule } from '../task/task.module';
import { UiComponentsModule } from '../ui-components/ui-components.module';
import { DashboardConfigComponent } from './dashboard-config/dashboard-config.component';
import { DashboardGridComponent } from './dashboard-grid/dashboard-grid.component';
import { DashboardNotificationCountComponent } from './dashboard-notification-count/dashboard-notification-count.component';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutes } from './dashboard.routes';
import { DashboardTasklistComponent } from './items/dashboard-tasklist/dashboard-tasklist.component';
import { DashboardService } from './shared/dashboard.service';

@NgModule({
	imports: [
		CommonModule,
		RouterModule,
		GridsterModule,
		FormsModule,

		EntityObjectModule,
		I18nModule,
		LogModule,
		TaskModule,
		UiComponentsModule,

		DashboardRoutes
	],
	exports: [
		DashboardNotificationCountComponent
	],
	declarations: [
		DashboardComponent,
		DashboardGridComponent,
		DashboardConfigComponent,
		DashboardTasklistComponent,
		DashboardNotificationCountComponent
	],
	providers: [
		DashboardService
	]
})
export class DashboardModule {
}
