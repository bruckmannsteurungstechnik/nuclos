import { Injectable } from '@angular/core';
import { IPreference } from '@nuclos/nuclos-addon-api';
import * as _ from 'lodash';
import { EMPTY, forkJoin as observableForkJoin, Observable } from 'rxjs';

import { finalize, map, mergeMap, tap } from 'rxjs/operators';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { Preference } from '../../preferences/preferences.model';
import { PreferencesService } from '../../preferences/preferences.service';
import { TaskService } from '../../task/shared/task.service';
import { DashboardTasklistItem } from '../preference-items/dashboard-tasklist-item';
import { DashboardPreferenceContent } from './dashboard-preference-content';

@Injectable()
export class DashboardService {

	/**
	 * TODO: Quickfix to prevent multiple parallel saves - should not happen in the first place
	 */
	private busy = false;

	private selectedDashboard?: Preference<DashboardPreferenceContent>;

	/**
	 * Timestamp of the last notification update. An update is costly, because task list data etc. must be fetched,
	 * and should therefore not happen too often.
	 */
	private lastNotificationUpdate = new Date();
	private notificationUpdateInProgress = false;
	private notificationCount = 0;
	private notificationsSince = new Date();

	constructor(
		private preferenceService: PreferencesService,
		private taskService: TaskService,
		private i18n: NuclosI18nService,
	) {
	}

	getTasklistMetas() {
		return this.taskService.getTaskListDefinitions();
	}

	getDashboardPrefs(): Observable<Preference<DashboardPreferenceContent>[]> {
		return this.preferenceService.getPreferences({
			orLayoutIsNull: true,
			type: ['dashboard']
		}).pipe(tap(prefs => {
			if (!this.selectedDashboard) {
				let selectedPref = prefs.find(pref => !!pref.selected);
				if (selectedPref) {
					this.selectDashboard(selectedPref as Preference<DashboardPreferenceContent>).subscribe();
				}
			}
		})) as Observable<Preference<DashboardPreferenceContent>[]>;
	}

	getDefaultDashboard() {
		return this.getDashboardPrefs().pipe(
			map(prefs => {
				let selected = prefs.find(pref => pref.selected as boolean);
				if (selected) {
					return selected;
				} else if (prefs.length > 0) {
					return prefs[0];
				}

				return this.createNew();
			}));
	}

	createNew(): IPreference<DashboardPreferenceContent> {
		return {
			boMetaId: '',
			type: 'dashboard',
			name: this.i18n.getI18n('webclient.dashboard.new'),
			content: new DashboardPreferenceContent()
		};
	}

	saveDashboard(dashboard: IPreference<DashboardPreferenceContent>) {
		if (this.busy) {
			return EMPTY;
		}

		this.setBusy(true);
		return this.preferenceService.savePreferenceItem(dashboard).pipe(
			mergeMap(() => this.preferenceService.getPreference(dashboard.prefId!).pipe(
				finalize(() => this.setBusy(false))) as Observable<IPreference<DashboardPreferenceContent>>));
	}

	selectDashboard(dashboard: Preference<DashboardPreferenceContent>) {
		if (this.busy) {
			return EMPTY;
		}

		this.setBusy(true);
		this.selectedDashboard = dashboard;

		return this.preferenceService.selectPreference(dashboard).pipe(finalize(() => this.setBusy(false)));
	}

	deleteDashboard(dashboard: Preference<DashboardPreferenceContent>) {
		return this.preferenceService.deletePreferenceItem(dashboard);
	}

	resetDashboard(dashboard: Preference<DashboardPreferenceContent>) {
		return this.preferenceService.resetCustomizedPreferenceItem(dashboard) as Observable<Preference<DashboardPreferenceContent>>;
	}

	getNotificationCount() {
		if (this.lastNotificationUpdate.getTime() > new Date().getTime() - 1000 * 60 * 5) {
			this.updateNotificationCount();
		}

		return this.notificationCount;
	}

	resetNotificationsSince() {
		this.notificationsSince = new Date();
	}

	setBusy(busy: boolean) {
		this.busy = busy;
	}

	private updateNotificationCount() {
		if (this.notificationUpdateInProgress) {
			return;
		}

		this.notificationUpdateInProgress = true;

		// TODO: For now there are only task lists on the dashboard.
		// Later there can be more kinds of dashboard items with their own notifications.
		this.updateTaskListNotificationCount().pipe(finalize(
			() => this.notificationUpdateInProgress = false
		)).subscribe(
			result => {
				this.notificationCount = result;
			}
		);
	}

	private updateTaskListNotificationCount(): Observable<number> {
		if (!this.selectedDashboard) {
			return EMPTY;
		}

		// TODO: Make sure there are no duplicated task lists, or they will be counted more than once
		let taskListRequests = this.selectedDashboard.content.items.filter(item => item instanceof DashboardTasklistItem)
			.map((item: DashboardTasklistItem) => item.taskList)
			.map(taskList => this.taskService.getTaskCountSince(taskList, this.notificationsSince));

		return observableForkJoin(taskListRequests).pipe(map(
			taskListCounts => _.sum(taskListCounts)
		));
	}

}
