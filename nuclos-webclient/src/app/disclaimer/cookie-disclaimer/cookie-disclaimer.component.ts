import { Component, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';
import { NuclosCookieService } from '../../cookie/shared/nuclos-cookie.service';
import { NewsService } from '../../news/shared/news.service';

@Component({
	selector: 'nuc-cookie-disclaimer',
	templateUrl: './cookie-disclaimer.component.html',
	styleUrls: ['./cookie-disclaimer.component.css']
})
export class CookieDisclaimerComponent implements OnInit {
	private cookiesAccepted = false;
	private privacyPolicy: News | undefined;

	constructor(
		private cookieService: NuclosCookieService,
		private newsService: NewsService,
	) {
	}

	ngOnInit() {
		this.cookiesAccepted = this.cookieService.get('cookiesaccepted') === 'true';
		this.newsService.getPrivacyPolicy().pipe(take(1)).subscribe(
			privacyPolicy => this.privacyPolicy = privacyPolicy
		);
	}

	displayCookieDisclaimer() {
		return this.privacyPolicy && !this.cookiesAccepted;
	}

	showPrivacyContent() {
		if (this.privacyPolicy) {
			this.newsService.showNews(this.privacyPolicy);
		}
	}

	acceptCookies() {
		this.cookiesAccepted = true;
		this.cookieService.put('cookiesaccepted', 'true');
	}
}
