import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AutonumberService } from './shared/autonumber.service';
import { DataService } from './shared/data.service';
import { EntityObjectErrorService } from './shared/entity-object-error.service';
import { EntityObjectEventService } from './shared/entity-object-event.service';
import { EntityObjectNavigationService } from './shared/entity-object-navigation.service';
import { EntityObjectResultUpdateService } from './shared/entity-object-result-update.service';
import { EntityObjectResultService } from './shared/entity-object-result.service';
import { EntityObjectSearchfilterService } from './shared/entity-object-searchfilter.service';
import { EntityObjectService } from './shared/entity-object.service';
import { LovDataService } from './shared/lov-data.service';
import { MetaService } from './shared/meta.service';
import { SelectableService } from './shared/selectable.service';

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [],
	providers: [
		AutonumberService,
		DataService,
		EntityObjectErrorService,
		EntityObjectEventService,
		EntityObjectNavigationService,
		EntityObjectSearchfilterService,
		EntityObjectService,
		EntityObjectResultService,
		EntityObjectResultUpdateService,
		LovDataService,
		MetaService,
		SelectableService
	]
})
export class EntityObjectDataModule {
}
