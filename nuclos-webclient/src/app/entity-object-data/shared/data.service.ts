import { HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FORBIDDEN } from 'http-status-codes';
import * as _ from 'lodash';
import * as hash from 'object-hash';
import { EMPTY, Observable, Observer, throwError } from 'rxjs';
import { catchError, finalize, map, tap, switchMap } from 'rxjs/operators';
import { AddonService } from '../../addons/addon.service';
import { NuclosCacheService } from '../../cache/shared/nuclos-cache.service';
import { WsTab } from '../../explorertrees/explorertrees.model';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { Logger } from '../../log/shared/logger';
import {
	AttributeSelectionContent,
	ColumnAttribute,
	Preference,
	SearchtemplateAttribute,
	SearchtemplatePreferenceContent,
	SideviewmenuPreferenceContent
} from '../../preferences/preferences.model';
import { SearchService } from '../../search/shared/search.service';
import { SearchfilterService } from '../../search/shared/searchfilter.service';
import { DatetimeService } from '../../shared/datetime.service';
import { FqnService } from '../../shared/fqn.service';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { NuclosHttpService } from '../../shared/nuclos-http.service';
import {
	BoAttr,
	BoViewModel,
	EntityAttrMeta,
	EntityMeta,
	EntityObjectData,
	InputType
} from './bo-view.model';
import { EntityObjectResultService } from './entity-object-result.service';
import { EntityObject } from './entity-object.class';
import { MetaService } from './meta.service';
import { ResultParams } from './result-params';

export interface Column {
	boAttrId: string;
}
interface Alias {
	[key: string]: Column;
}
export interface QueryObject {
	where: WhereCondition | undefined;
	multiSelectionCondition?: MultiSelectionResult;
}
export interface WhereCondition {
	aliases: Alias;
	clause: string;
}
export interface LoadContext {
	meta: EntityMeta;
	vlpId: string | undefined;
	vlpParams: HttpParams | undefined;
	multiSelectionCondition?: MultiSelectionResult;
	searchFilter?: string | undefined;
	attributeSelection?: Preference<AttributeSelectionContent>;
	searchtemplatePreference?: Preference<SearchtemplatePreferenceContent>;
	resultParams?: ResultParams;
}
interface LoadInnerContext extends LoadContext {
	search: string;
	columns;
	sort;
	resultParams: ResultParams;
}

/**
 * TODO: Split this into more meaningful separate services, instead of one monolith for everything.
 * TODO: Methods are too complex and have too many parameters.
 */
@Injectable()
export class DataService {
	/**
	 * Validates and formats the search input value according to the "input type" of the attribute.
	 */
	private static formatSearchTemplateValue(
		value: any,
		attribute: BoAttr,
		useTicks: boolean
	): string {
		// if (attribute.inputType === 'reference') {
		if (attribute.reference) {
			value = value && value.id ? value.id : value;
		}

		if (
			(value !== undefined &&
				value.length === undefined &&
				(attribute.inputType === InputType.NUMBER ||
					attribute.inputType === InputType.REFERENCE ||
					attribute.inputType === InputType.DATE)) ||
			(value !== undefined && value.length > 0)
		) {
			if (useTicks) {
				if (attribute.inputType === InputType.DATE) {
					value = moment(value).format('YYYY-MM-DD');
				}
				if (typeof value !== 'number') {
					value = value.replace(/'/g, ''); // remove '
				}
			}
			if (useTicks) {
				value = '\'' + value + '\'';
			}
		} else {
			value = null;
		}

		return value;
	}

	eoRequests = new Map<string, Observable<BoViewModel>>();

	constructor(
		private http: NuclosHttpService,
		private nuclosConfig: NuclosConfigService,
		private fqnService: FqnService,
		private searchService: SearchService,
		private searchfilterService: SearchfilterService,
		private addonService: AddonService,
		private i18n: NuclosI18nService,
		private dateTimeService: DatetimeService,
		private $log: Logger,
		private cache: NuclosCacheService,
		private eoResultService: EntityObjectResultService,
		private metaService: MetaService
	) {}

	executeCollectiveProcessing(
		context: LoadContext,
		restUrl: string
	): Observable<CollectiveProcessingExecution> {
		let innerContext = this.buildInnerContext(context);
		let queryParams = this.buildQueryParams(innerContext);
		let queryObject = this.buildQueryObject(innerContext);
		return queryObject.pipe(
			switchMap(q => {
				return this.http.post(restUrl, q, { params: queryParams }).pipe(
					tap((result: CollectiveProcessingExecution) => { this.$log.debug('CollectiveProccesingExecution before map', result); }),
					catchError(e => {
						this.$log.warn(
							'Could not execute collective processing %o for entity meta %o with params %o: %o',
							restUrl,
							context.meta,
							queryParams,
							e
						);
						if (e.status === FORBIDDEN) {
							// Re-throw error 403 - will be handled globally
							return throwError(e);
						} else {
							return EMPTY;
						}
					})
				);
			})
		);
	}

	abortCollectiveProcessing(
		restUrl: string
	): Observable<Object> {
		return this.http.get(restUrl).pipe(
			tap((result: Object) => { this.$log.debug('CollectiveProccesingAbortion before map', result); })
		);
	}

	loadCollectiveProcessingSelectionOptions(
		context: LoadContext
	): Observable<CollectiveProcessingSelectionOptions> {
		let innerContext = this.buildInnerContext(context);
		let queryParams = this.buildQueryParams(innerContext);
		let queryObject = this.buildQueryObject(innerContext);

		this.$log.debug(
			'Loading collective processing selection options for %o...',
			context.meta.getEntityClassId()
		);


		return queryObject.pipe(
			switchMap(q => {
				return this.http.post(this.nuclosConfig.getRestHost() +
					'/collectiveProcessing/' +
					context.meta.getEntityClassId(), q, { params: queryParams })
					.pipe(
						catchError(e => {
							this.$log.warn(
								'Could not load collective processing selection options for entity meta %o with params %o: %o',
								context.meta,
								queryParams,
								e
							);
							if (e.status === FORBIDDEN) {
								return throwError(e);
							} else {
								return EMPTY;
							}
						}),
						map((result: CollectiveProcessingSelectionOptions) => {
							return result;
						})
					);
			})
		);
	}

	/**
	 *  @deprecated: Too many parameters, use loadEoList(LoadContext)
	 */
	loadList(
		meta: EntityMeta,
		vlpId: string | undefined,
		vlpParams: HttpParams | undefined,
		attributeSelection?: Preference<AttributeSelectionContent>,
		searchtemplatePreference?: Preference<SearchtemplatePreferenceContent>,
		resultParams = ResultParams.DEFAULT
	): Observable<BoViewModel> {
		let context: LoadContext = {
			meta: meta,
			vlpId: vlpId,
			vlpParams: vlpParams,
			attributeSelection: attributeSelection,
			searchtemplatePreference: searchtemplatePreference,
			resultParams: resultParams
		};

		return this.loadEoList(context);
	}

	loadEoList(context: LoadContext): Observable<BoViewModel> {
		let innerContext = this.buildInnerContext(context);
		let queryParams = this.buildQueryParams(innerContext);

		return new Observable<BoViewModel>((observer: Observer<BoViewModel>) => {
			this.buildQueryObject(innerContext).subscribe(queryObject => {
				this.loadEoData(innerContext.meta, queryParams, queryObject).subscribe(model => {
					observer.next(model);
					observer.complete();
				});
			});
		});
	}

	/**
	 * Loads an EO result for the given entity and query params.
	 * Results for identical requests will be cached in EOResultService!
	 */
	loadEoData(
		meta: EntityMeta,
		queryParams,
		queryObject: { where: WhereCondition | undefined } = { where: undefined }
	): Observable<BoViewModel> {
		this.$log.debug('Loading EO list for %o...', meta.getEntityClassId());

		let requestHash = hash({
			meta: meta,
			queryParams: queryParams,
			queryObject: queryObject
		});

		let observable = this.http
			.post(
				this.nuclosConfig.getRestHost() + '/bos/' + meta.getEntityClassId() + '/query',
				queryObject,
				{ params: queryParams }
			)
			.pipe(
				catchError(e => {
					this.$log.warn(
						'Could not load EO data for entity meta %o with params %o: %o',
						meta,
						queryParams,
						e
					);
					if (e.status === FORBIDDEN) {
						// Re-throw error 403 - will be handled globally
						return throwError(e);
					} else {
						return EMPTY;
					}
				}),
				map((result: BoViewModel) => {
					let entityObjects: EntityObject[] = [];
					for (let bo of result.bos) {
						entityObjects.push(new EntityObject(<any>bo));
					}
					result.bos = entityObjects;
					return result;
				}),
				finalize(() => {
					this.$log.debug(
						'Loading EO list -> hash = %o, deleting after request finished!',
						requestHash
					);
					this.eoRequests.delete(requestHash);
				})
			);

		return this.eoResultService.cachedResults(requestHash, observable);
	}

	getInputType(attribute): string {
		if (attribute.reference) {
			return InputType.REFERENCE;
		} else if (attribute.type === 'Decimal' || attribute.type === 'Integer') {
			return InputType.NUMBER;
		} else if (attribute.type === 'String') {
			return InputType.STRING;
		} else if (attribute.type === 'Date' || attribute.type === 'Timestamp') {
			return InputType.DATE;
		} else if (attribute.type === 'Boolean') {
			return InputType.BOOLEAN;
		}
		return InputType.STRING;
	}

	formatAttribute(value, attrMeta: EntityAttrMeta): string {
		let result = value;
		if (value.name !== undefined) {
			result = value.name !== null ? value.name : '';
		} else if (attrMeta.isBoolean()) {
			result = value ? this.i18n.getI18n('common.yes') : this.i18n.getI18n('common.no');
		} else if (attrMeta.isDate()) {
			result = this.dateTimeService.formatDate(value);
		} else if (attrMeta.isTimestamp()) {
			result = this.dateTimeService.formatTimestamp(value);
		}
		return result;
	}

	/**
	 * TODO
	 *    - subform column selection
	 *    - searchfilter
	 *    - search (text)
	 */
	exportBoList(
		boId: number | undefined,
		refAttrId: string | undefined,
		meta: EntityMeta,
		columnPreference: Preference<SideviewmenuPreferenceContent>,
		searchtemplatePreference: Preference<SearchtemplatePreferenceContent>,
		format: string,
		pageOrientationLandscape: boolean,
		isColumnScaled: boolean
	): Observable<string> {
		let isSubform: boolean = boId !== undefined && refAttrId !== undefined;

		let offset = undefined;
		let chunkSize = undefined;

		let columns: ColumnAttribute[] = columnPreference
			? columnPreference.content.columns.filter(c => c.selected)
			: [];

		let sort = '';
		if (columnPreference && columnPreference.content.columns) {
			sort = this.getSortString(columnPreference.content.columns);
		}

		let filter = {
			offset: offset,
			chunksize: chunkSize,
			gettotal: false,
			countTotal: true,
			search: this.searchService.getCurrentSearchInputText(),
			searchFilter: undefined, // TODO
			withTitleAndInfo: false,
			attributes: '',
			sort: '',
			searchCondition: '',
			where: {}
		};
		// request only given attributes
		filter.attributes = columns
			// use short name instead of fqn to avoid problems with to long query param
			.map(attribute =>
				this.fqnService.getShortAttributeName(meta.getEntityClassId(), attribute.boAttrId)
			)
			.join(',');

		filter.sort = this.getSortString(columns);
		let searchCondition = this.getSearchCondition(meta);
		if (searchCondition) {
			filter.searchCondition = searchCondition;
		}

		let url;
		if (!isSubform) {
			url =
				this.nuclosConfig.getRestHost() +
				'/bos/' +
				meta.getEntityClassId() +
				'/boListExport/' +
				format +
				'/' +
				pageOrientationLandscape +
				'/' +
				isColumnScaled;
		} else {
			url =
				this.nuclosConfig.getRestHost() +
				'/bos/' +
				meta.getEntityClassId() +
				'/' +
				boId +
				'/subBos/' +
				refAttrId +
				'/export/' +
				format +
				'/' +
				pageOrientationLandscape +
				'/' +
				isColumnScaled;
		}
		url +=
			'?countTotal=true&gettotal=false&offset=0&search=' +
			'&sort=' +
			filter.sort +
			'&withTitleAndInfo=false';

		if (searchtemplatePreference && searchtemplatePreference.content.columns) {
			let entityClassIds = this.getSetOfEntityClassIds(
				searchtemplatePreference.content.columns
			);
			this.metaService.getEntityMetas(entityClassIds).subscribe(mapMeta => {
				filter.where = this.buildWhereCondition(
					searchtemplatePreference.content.columns,
					meta,
					mapMeta,
					this.searchfilterService.getAdditionalWhereConditions(meta.getEntityClassId()),
					ResultParams.DEFAULT
				);
			});
		} else {
			delete filter.where;
		}
		return this.http.post(url, filter).pipe(map(data => data['links'].export.href));
	}

	/**
	 * Fetches the corresponding EO for the given attribute (with only this one attribute filled).
	 * attributeId must be a fully quallified name!
	 */
	fetchByAttribute(
		sourceAttributeId: string,
		sourceEOId: number,
		targetAttributeId: string,
		targetEOId?: number
	): Observable<EntityObject> {
		// TODO: Splitting by underscore is potentially unsafe, the FQN format is flawed.
		let index = sourceAttributeId.lastIndexOf('_');
		if (index <= 0) {
			throw 'Attribute ID must be a fully qualified name! Illegal value: ' +
				sourceAttributeId;
		}
		let url =
			this.nuclosConfig.getRestHost() +
			'/data/fieldget/' +
			sourceAttributeId +
			'/' +
			sourceEOId +
			'/' +
			targetAttributeId +
			'/' +
			targetEOId;

		return this.http.get(url).pipe(map((data: EntityObjectData) => new EntityObject(data)));
	}

	loadMatrixData(webMatrix: WebMatrix, eo: EntityObject): Observable<any> {
		let link = this.nuclosConfig.getRestHost() + '/data/data/matrix/';

		let postData: any = webMatrix;
		postData.entity = eo.getEntityClassId();
		postData.boId = eo.getId();

		return this.http.post(link, JSON.stringify(postData));
	}

	loadTreeData(tree: WsTab): Observable<any[]> {
		let link =
			this.nuclosConfig.getRestHost() + '/data/tree/' + tree.boMetaId + '/' + tree.boId;

		return this.http.get<any[]>(link);
	}

	loadSubTreeData(node_id: string): Observable<any[]> {
		let link = this.nuclosConfig.getRestHost() + '/data/subtree/' + node_id;

		return this.http.get<any[]>(link);
	}

	private buildInnerContext(context: LoadContext): LoadInnerContext {
		let columns = this.buildColumns(context.meta, context.attributeSelection);

		let sort = '';
		if (context.attributeSelection && context.attributeSelection.content.columns) {
			sort = this.getSortString(context.attributeSelection.content.columns);
		}

		let innerContext: LoadInnerContext = {
			...context,
			search: this.searchService.getCurrentSearchInputText(),
			columns: columns,
			sort: sort,
			resultParams:
				context.resultParams === undefined ? ResultParams.DEFAULT : context.resultParams
		};

		return innerContext;
	}

	private buildColumns(
		meta: EntityMeta,
		attributeSelection?: Preference<AttributeSelectionContent>
	) {
		let columns: (ColumnAttribute | undefined)[] = attributeSelection
			? attributeSelection.content.columns.filter(c => c.selected)
			: [];

		let attributeFqns: (string | undefined)[] = columns.map(a => (a ? a.boAttrId : undefined));
		let titleAttributeFqns: string[] = this.fqnService.parseFqns(meta.getTitlePattern());
		let infoAttributeFqns: string[] = this.fqnService.parseFqns(meta.getInfoPattern());

		// add attributes required by addons
		let requiredAddonAttributeFqns: string[] = this.addonService
			.getRequiredResultlistAttributeNames(meta.getEntityClassId())
			.map(attrName => meta.getEntityClassId() + '_' + attrName);

		columns = Array.from(
			new Set<string | undefined>([
				...attributeFqns,
				...titleAttributeFqns,
				...infoAttributeFqns,
				...requiredAddonAttributeFqns
			])
		)

			// filter out columns from other EO's (at the moment only supported by the Java-client result list)
			.filter(fqn => fqn && fqn.startsWith(meta.getEntityClassId() + '_'))

			.map(fqn => {
				if (fqn) {
					let attr = meta.getAttributeMetaByFqn(fqn);
					if (attr) {
						return {
							boAttrId: attr.getAttributeID()
						} as ColumnAttribute;
					}
				}
				return undefined;
			})
			.filter(fqn => fqn !== undefined);

		return columns;
	}

	private buildQueryParams(context: LoadInnerContext) {
		let columns = this.buildColumns(context.meta, context.attributeSelection);

		let sort = '';
		if (context.attributeSelection && context.attributeSelection.content.columns) {
			sort = this.getSortString(context.attributeSelection.content.columns);
		}

		let queryParams = this.buildFilter(context);

		_.forOwn(context.resultParams, (value, key) => {
			if (value === null || value === undefined) {
				value = '';
			}

			queryParams = queryParams.append(key, value as string);
		});

		return queryParams;
	}

	private getSetOfEntityClassIds(cols: SearchtemplateAttribute[]) {
		let entityClassIds: Set<string> = new Set<string>();
		cols.forEach(c => {
			if (c.dependentBoMetaId !== undefined) {
				entityClassIds.add(c.dependentBoMetaId);
			}
		});
		return entityClassIds;
	}

	private buildQueryObject(context: LoadInnerContext): Observable<QueryObject> {
		let cols: SearchtemplateAttribute[] = [];
		if (context.searchtemplatePreference && context.searchtemplatePreference.content.columns) {
			cols = context.searchtemplatePreference.content.columns;
		}

		let entityClassIds = this.getSetOfEntityClassIds(cols);

		return new Observable<QueryObject>((observer: Observer<QueryObject>) => {
			this.metaService.getEntityMetas(entityClassIds).subscribe(mapMeta => {
				let queryObject: QueryObject = {
					where: undefined
				};

				queryObject.where = this.buildWhereCondition(
					cols,
					context.meta,
					mapMeta,
					this.searchfilterService.getAdditionalWhereConditions(
						context.meta.getEntityClassId()
					),
					context.resultParams
				);
				queryObject.multiSelectionCondition = context.multiSelectionCondition;

				if (
					context.meta.getEmptyResultListIfNoSearchCondition() &&
					queryObject.where !== undefined &&
					queryObject.where.clause.length === 0 &&
					context.search.length === 0
				) {
					this.$log.info('Search will be executed only if a search critera is set.');
					// actual a result list query call shouldn't be necessary here
					// but the result list response contains the 'canCreateBo' flag which will be evaluated for displaying the new-button
					// so just request an empty list
					// TODO create a REST service for retrieving 'canCreateBo' flag
					queryObject.where = {
						clause: context.meta.getEntityClassId() + '.id is null',
						aliases: {}
					};
				}
				observer.next(queryObject);
				observer.complete();
			});
		});
	}

	/**
	 * Builds the 'AND'-concatenated WHERE condition based on the given search object.
	 *
	 * TODO: searchObject type
	 */
	private buildWhereCondition(
		attributes: SearchtemplateAttribute[],
		mainMeta: EntityMeta,
		dependentMapMeta: Map<string, EntityMeta>,
		additionalWhereConditions: string[],
		resultParams: ResultParams
	): WhereCondition {
		let result: WhereCondition = {
			aliases: {},
			clause: ''
		};
		let index = 0;
		let andArray: string[] = [];

		// // TODO: Get rid of this workaround
		// searchObject = this.synchronizeAttributes(searchObject);

		let dependentAnds: Map<string, string[]> = new Map();

		for (let searchElement of attributes) {
			if (
				!searchElement.operator ||
				searchElement.enableSearch === false ||
				searchElement.selected === false
			) {
				continue;
			}

			let meta =
				searchElement.dependentBoMetaId === undefined
					? mainMeta
					: dependentMapMeta.get(searchElement.dependentBoMetaId);
			if (meta !== undefined) {
				let attribute = this.fqnService.getAttributeByFqn(meta, searchElement.boAttrId);
				attribute.inputType = this.getInputType(attribute);
				let fqn = searchElement.boAttrId;
				let alias = 'col' + index;
				let andPart = this.buildWhereAndPart(searchElement, attribute, alias);
				if (andPart !== undefined) {
					if (searchElement.dependentBoMetaId !== undefined) {
						let depAndArray = dependentAnds.get(searchElement.dependentBoMetaId);
						if (depAndArray === undefined) {
							depAndArray = [];
							dependentAnds.set(searchElement.dependentBoMetaId, depAndArray);
						}
						depAndArray.push(andPart);
					} else {
						andArray.push(andPart);
					}
					result.aliases[alias] = { boAttrId: fqn };
					index++;
				} else {
					continue;
				}
			}
		}

		dependentAnds.forEach((depAndArray, entityClassId) => {
			let entityContexts = mainMeta.getEntityContexts();
			if (entityContexts) {
				let entityContext = entityContexts.find(
					ec => ec.dependentEntityClassId === entityClassId
				);
				if (entityContext) {
					let alias = 'col' + index;
					let depClause =
						' EXISTS ( SELECT ' +
						entityClassId +
						'.id' +
						' FROM ' +
						entityClassId +
						' WHERE ' +
						mainMeta.getEntityClassId() +
						'.id=' +
						alias +
						' AND ' +
						depAndArray.join(' AND ') +
						' ) ';
					result.aliases[alias] = { boAttrId: entityContext.dependentEntityFieldId };
					index++;
					andArray.push(depClause);
				}
			}
		});

		andArray.push(...additionalWhereConditions);

		if (resultParams.entityObjectId) {
			let systemEntity = mainMeta.isSystemEntity();
			andArray.push(
				' ' +
					mainMeta.getEntityClassId() +
					'.id=' +
					(systemEntity ? '\'' : '') +
					resultParams.entityObjectId +
					(systemEntity ? '\' ' : ' ')
			);
		}

		result.clause = andArray.join(' AND ');

		return result;
	}

	private buildWhereAndPart(
		searchElement: SearchtemplateAttribute,
		attribute: BoAttr,
		alias: string
	): string | undefined {
		let andPart = alias + ' ' + searchElement.operator + ' ';
		let useTicks = this.needsTicks(attribute, searchElement.operator);
		if (!this.isUnaryOperator(searchElement.operator)) {
			if (this.hasMultipleValues(searchElement) && searchElement.operator === '=') {
				let values = searchElement.values;
				if (values) {
					andPart =
						'(' +
						values
							.map(
								value =>
									alias +
									' ' +
									searchElement.operator +
									' ' +
									DataService.formatSearchTemplateValue(
										value,
										attribute,
										useTicks
									)
							)
							.join(' OR ') +
						')';
				}
			} else {
				let value = DataService.formatSearchTemplateValue(
					searchElement.value,
					attribute,
					useTicks
				);
				if (value === null) {
					this.$log.warn(
						'No value given for non-unary operator \'%o\' on attribute \'%o\', ignoring',
						searchElement.operator,
						attribute
					);
					return undefined;
				}

				andPart += value;
			}
		}
		return andPart;
	}

	/**
	 * Determines if the ticks should be used to escape the value for the given attribute in a WHERE-query.
	 *
	 * @param attribute
	 * @param operator
	 * @returns {boolean}
	 */
	private needsTicks(attribute: any, operator: string | undefined): boolean {
		if (attribute.system) {
			return true;
		} else if (attribute.inputType === InputType.NUMBER) {
			return false;
		} else if (
			attribute.inputType === InputType.REFERENCE &&
			attribute.referenceWithUid === false &&
			operator === '='
		) {
			return false;
		}

		return true;
	}

	/**
	 * Determines if the given operator is unary.
	 *
	 * TODO: Available operators and their types should be defined somewhere centrally.
	 */
	private isUnaryOperator(operator: string | undefined): boolean {
		return operator === 'is null' || operator === 'is not null';
	}

	private getSortString(columns: ColumnAttribute[]) {
		return columns
			.filter(col => col.sort && col.sort.direction && col.boAttrId)
			.sort((a, b) => {
				if (!a.sort || !b.sort || a.sort.prio === undefined || b.sort.prio === undefined) {
					return 0;
				}
				if (a.sort.prio < b.sort.prio) {
					return -1;
				}
				if (a.sort.prio > b.sort.prio) {
					return 1;
				}
				return 0;
			})
			.map(column => column.boAttrId + (column.sort ? '+' + column.sort.direction : ''))
			.join(',');
	}

	private buildFilter(context: LoadInnerContext): HttpParams {
		let searchParams = new HttpParams();
		if (context.vlpParams) {
			for (let key of context.vlpParams.keys()) {
				let values = context.vlpParams.getAll(key);
				if (values) {
					for (let value of values) {
						searchParams = searchParams.append(key, value);
					}
				}
			}
		}

		searchParams = searchParams.append('search', context.search);
		if (context.searchFilter) {
			searchParams = searchParams.append('searchFilter', context.searchFilter);
		}
		if (context.vlpId) {
			searchParams = searchParams.append('vlpId', context.vlpId);
		}
		searchParams = searchParams.append('withTitleAndInfo', 'false');

		if (context.columns === undefined) {
			context.columns = [];
		}

		if (context.resultParams.entityObjectIdOnlySelection === true) {
			searchParams = searchParams.append('idOnlySelection', 'true');
		}

		// request only given attributes, in case of id-only-selection we need it for a text search
		if (context.columns !== undefined) {
			searchParams = searchParams.append(
				'attributes',
				context.columns
					// use short name instead of fqn to avoid problems with to long query param
					.map(attribute =>
						this.fqnService.getShortAttributeName(
							context.meta.getEntityClassId(),
							attribute.boAttrId
						)
					)
					.join(',')
			);
		}

		if (context.sort !== undefined) {
			searchParams = searchParams.append('sort', context.sort);
		}

		let searchCondition = this.getSearchCondition(context.meta);
		if (searchCondition) {
			searchParams = searchParams.append('searchCondition', searchCondition);
		}

		// NUCLOS-6311 4)
		searchParams = searchParams.append('skipStatesAndGenerations', 'true');
		return searchParams;
	}

	private getSearchCondition(metaData: EntityMeta) {
		if (!metaData || !metaData.getAttributes()) {
			return undefined;
		}

		let hasFieldFilter = false;
		let fieldFilter = 'CompositeCondition:AND:[';

		for (let attrKey of Object.keys(metaData.getAttributes())) {
			let attribute = <any>metaData.getAttribute(attrKey);

			if (!attribute || attribute.ticked) {
				continue;
			}

			let fieldName = this.fqnService.getShortAttributeName(
				metaData.getBoMetaId(),
				attribute.boAttrId
			);
			if (attribute.search) {
				if (hasFieldFilter) {
					fieldFilter += ',';
				}

				fieldFilter += 'LikeCondition:LIKE:';
				fieldFilter += fieldName;
				fieldFilter += ':*' + attribute.search + '*';

				hasFieldFilter = true;
			}

			if (attribute.valuelist && attribute.valuelist.length > 0) {
				let hasInCondition = false;
				let inCondition = '';

				for (let key of Object.keys(attribute.valuelist)) {
					let value = attribute.valuelist[key];
					if (value.ticked && value.name) {
						if (hasInCondition) {
							inCondition += ',';
						}

						// NUCLOS-4111 CollectableInCondition works with the String Name of a Reference Field
						// TODO: It would be better if it worked with the IDs
						inCondition += '\'' + value.name + '\'';
						hasInCondition = true;
					}
				}

				if (hasInCondition) {
					if (hasFieldFilter) {
						fieldFilter += ',';
					}

					fieldFilter += 'InCondition:IN:';
					fieldFilter += fieldName;
					fieldFilter += ':[';
					fieldFilter += inCondition;
					fieldFilter += ']';

					hasFieldFilter = true;
				}
			}
		}

		if (hasFieldFilter) {
			return fieldFilter + ']';
		}

		return undefined;
	}

	private hasMultipleValues(searchElement: SearchtemplateAttribute) {
		return !searchElement.value && searchElement.values && searchElement.values.length > 0;
	}
}
