import { WebLayoutVisitor } from '../../layout/shared/web-layout-visitor';

export class EntityObjectDependency {

	static fromLayout(
		layout: WebLayout,
		referenceAttributeId: string
	): EntityObjectDependency {

		let dependency = new EntityObjectDependency(referenceAttributeId);

		let subform = EntityObjectDependency.findSubformByReference(layout, referenceAttributeId);
		this.addParentsRecursive(layout, dependency, subform);

		return dependency;
	}

	private static addParentsRecursive(
		layout: WebLayout,
		dependency: EntityObjectDependency,
		subform?: SubformInfo
	) {
		if (subform && subform.parentEntityClassId) {
			let parentEntityClassId = subform.parentEntityClassId;
			subform = EntityObjectDependency.findSubformByEntity(layout, parentEntityClassId);
			if (subform) {
				let parent = new EntityObjectDependency(
					subform.referenceAttributeId
				);
				dependency.getRootDependency().parent = parent;

				this.addParentsRecursive(layout, dependency, subform);
			}
		}
	}

	private static findSubformByEntity(
		layout: WebLayout,
		entityClassId: string
	): SubformInfo | undefined {
		if (!entityClassId) {
			return;
		}

		let visitor = new WebLayoutVisitorForDependents(entityClassId);
		visitor.visitLayout(layout);

		return visitor.getResult();
	}

	private static findSubformByReference(
		layout: WebLayout,
		referenceAttributeId: string
	): SubformInfo | undefined {

		let visitor = new WebLayoutVisitorForDependents(undefined, referenceAttributeId);
		visitor.visitLayout(layout);

		return visitor.getResult();
	}

	constructor(
		private referenceAttributeId: string,
		private parent?: EntityObjectDependency,
		private eoId?: string
	) {
	}

	dependency(referenceAttributeId: string, eoId?: string): EntityObjectDependency {
		return new EntityObjectDependency(
			referenceAttributeId,
			this,
			eoId
		);
	}

	setParent(parent: EntityObjectDependency) {
		this.parent = parent;
	}

	getParent() {
		return this.parent;
	}

	getReferenceAttributeFqn() {
		return this.referenceAttributeId;
	}

	getEoId() {
		return this.eoId;
	}

	setEoId(eoId: string) {
		this.eoId = eoId;
	}

	getRootEoId() {
		if (this.parent) {
			return this.parent.getRootEoId();
		}

		return this.eoId;
	}

	getRootDependency() {
		if (this.parent) {
			return this.parent.getRootDependency();
		} else {
			return this;
		}
	}

	toPath() {
		let result = this.referenceAttributeId;

		if (this.eoId) {
			result += '/' + this.eoId;
		}

		if (this.parent) {
			let parentPath = this.parent.toPath();
			if (!this.parent.eoId) {
				parentPath += '/';
			}
			result = parentPath + '/' + result;
		}

		return result;
	}
}

class SubformInfo {
	referenceAttributeId: string;
	entityClassId: string;
	parentEntityClassId?: string;

	constructor(subform: WebSubform) {
		this.referenceAttributeId = subform.foreignkeyfieldToParent;
		this.entityClassId = subform.entity;
		this.parentEntityClassId = subform.parentSubform;
	}
}

class WebLayoutVisitorForDependents extends WebLayoutVisitor {
	private result?: SubformInfo;

	constructor(
		private entityClassId?: string,
		private referenceAttributeId?: string
	) {
		super();
	}

	visitSubform(item: WebSubform) {
		if (this.result) {
			return;
		}

		if (
			this.referenceAttributeId && item.foreignkeyfieldToParent === this.referenceAttributeId
			|| this.entityClassId && item.entity === this.entityClassId
		) {
			this.result = new SubformInfo(item);
		}
	}

	getResult() {
		return this.result;
	}
}
