import { EntityObject } from './entity-object.class';

/**
 * A listener interface for events on an EntityObject.
 * Listeners can subscribe themselves to concrete EntityObjects.
 */
export interface EntityObjectEventListener {

	/**
	 * Triggered after an attribute value changed.
	 */
	afterAttributeChange?: (
		entityObject: EntityObject,
		attributeName: string,
		oldValue: any,
		newValue: any
	) => any;

	/**
	 * Triggered when saving starts or stops.
	 */
	isSaving?: (entityObject: EntityObject, inProgress: boolean) => any;

	afterSave?: (entityObject: EntityObject) => any;

	afterDirty?: (entityObject: EntityObject) => any;

	afterReload?: (entityObject: EntityObject) => any;

	afterClone?: (entityObject: EntityObject) => any;

	beforeCancel?: (entityObject: EntityObject) => any;
	afterCancel?: (entityObject: EntityObject) => any;

	afterDelete?: (entityObject: EntityObject) => any;

	/**
	 * Triggered after the layout of the given EO was changed.
	 * E.g. after the "Nuclos process" was changed.
	 */
	afterLayoutChange?: (
		entityObject: EntityObject,
		layoutURL: string
	) => any;

	/**
	 * Triggered after the validation status was updated.
	 */
	afterValidationChange?: (entityObject: EntityObject) => any;

	refreshVlp?: (
		entityObject: EntityObject,
		attributeName: string
	) => any;
}
