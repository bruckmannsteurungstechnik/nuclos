import { Injectable } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { IEntityObject } from '@nuclos/nuclos-addon-api';
import * as _ from 'lodash';
import { Logger } from '../../log/shared/logger';
import { EntityObject } from './entity-object.class';

@Injectable()
export class EntityObjectNavigationService {

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private $log: Logger
	) {
	}

	navigateToEo(eo: IEntityObject) {
		this.$log.info('(selecting) Navigating to EO: %o', eo && (<EntityObject>eo).getTitle());
		let entityClassId = eo.getEntityClassId();
		let id = eo.getId() || eo.getTemporaryId() || 'new';

		return this.navigateToEoById(entityClassId, id);
	}

	navigateToEoById(
		entityClassId: string,
		eoId: number | string,
		searchFilterId?: string
	) {

		let queryParams: Params = this.route.snapshot.queryParams;
		let newParams = {};
		_.assign(newParams, queryParams);

		if (searchFilterId) {
			newParams['searchFilterId'] = searchFilterId;
		}

		return this.router.navigate(
			[
				this.router.url.indexOf('/popup/') > -1 ? '/popup' : '/view',
				entityClassId,
				eoId
			],
			{queryParams: newParams}
		);
	}

	navigateToNew(entityClassId: string) {
		return this.router.navigate(['/view', entityClassId, 'new']);
	}

	navigateToView(entityClassId: string) {
		return this.router.navigate(['/view', entityClassId]);
	}
}
