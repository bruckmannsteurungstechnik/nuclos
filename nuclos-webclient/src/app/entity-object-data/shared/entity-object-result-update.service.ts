import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EMPTY, Observable, Subject, timer } from 'rxjs';

import { catchError, finalize, map, mergeMap, tap, retryWhen, delayWhen } from 'rxjs/operators';
import { AuthenticationService } from '../../authentication';
import { EntityObjectGridMultiSelectionResult } from '../../entity-object/entity-object-grid/entity-object-grid-multi-selection-result';
import { LoadMoreResultsEvent } from '../../entity-object/entity-object.component';
import { EntityObjectPreferenceService } from '../../entity-object/shared/entity-object-preference.service';
import { Logger } from '../../log/shared/logger';
import { SearchService } from '../../search/shared/search.service';
import { NuclosHttpService } from '../../shared/nuclos-http.service';
import { ObservableUtils } from '../../shared/observable-utils';
import { BoViewModel } from './bo-view.model';
import { DataService, LoadContext } from './data.service';
import { EntityObjectResultService } from './entity-object-result.service';
import { EntityObjectSearchfilterService } from './entity-object-searchfilter.service';
import { ResultParams } from './result-params';

@Injectable()
export class EntityObjectResultUpdateService {
	private loading = false;

	/**
	 * Triggered when the EO list should be reloaded.
	 */
	private mustLoadListSubject = new Subject<any>();

	private collectiveProcessingInitiated = new Subject<CollectiveProcessingExecution>();

	private collectiveProcessingProgress = new Subject<CollectiveProcessingProgressInfo>();

	private collectiveProcessingFinished = new Subject<any>();

	private searchFilterId;

	constructor(
		private $log: Logger,
		private authenticationService: AuthenticationService,
		private dataService: DataService,
		private eoPreferenceService: EntityObjectPreferenceService,
		private eoResultService: EntityObjectResultService,
		private eoSearchfilterService: EntityObjectSearchfilterService,
		private route: ActivatedRoute,
		private searchService: SearchService,
		private http: NuclosHttpService
	) {
		this.authenticationService.observeLoginStatus().subscribe(loggedIn => {
			if (!loggedIn) {
				this.eoResultService.clear();
			}
		});

		this.eoSearchfilterService
			.observeSelectedSearchfilter()
			.subscribe(() => this.triggerLoadList());

		this.route.queryParams.subscribe(params => {
			this.searchFilterId = params['searchFilterId'];
		});

		this.searchService
			.subscribeDataLoad()
			.subscribe(() => this.eoResultService.invalidateCache());
	}

	observeMustLoadList(): Observable<any> {
		return this.mustLoadListSubject;
	}

	triggerLoadList() {
		if (!this.authenticationService.isLoggedIn()) {
			this.$log.warn('Skipping result list update - requires login');
			return;
		}

		this.$log.debug('Trigger loading of eo list');

		let selectedEntityClassId = this.eoResultService.getSelectedEntityClassId();
		if (selectedEntityClassId) {
			let entityMeta = this.eoResultService.getSelectedMeta();
			if (entityMeta && entityMeta.getBoMetaId() === selectedEntityClassId) {
				this.mustLoadListSubject.next();
			} else {
				this.eoResultService
					.updateSelectedMeta()
					.subscribe(() => this.mustLoadListSubject.next());
			}
		} else {
			this.$log.warn('Cannot load result list - no entity class selected');
		}
	}

	/**
	 * load data - will be called subsequently when scrolling down in sidebar
	 */
	loadData(event: LoadMoreResultsEvent): Observable<BoViewModel> {
		return this.loadResultsForSelectedEntityClass(event).pipe(
			tap(boViewModel => {
				this.eoResultService.addNewData(boViewModel);

				// reset selected EO if list is empty
				const selectedEo = this.eoResultService.getSelectedEo();
				if (
					boViewModel.bos.length === 0 &&
					selectedEo &&
					selectedEo.getId() !== null // ignore new EO
				) {
					this.eoResultService.selectEo(undefined);
				}
			})
		);
	}

	isLoading() {
		return this.loading;
	}

	setLoading(loading) {
		this.loading = loading;
	}

	isEntityObjectIncludedInResult(entityObjectId?: string): Observable<boolean> {
		const meta = this.eoResultService.getSelectedMeta();

		if (!meta) {
			return EMPTY;
		} else {
			let params: ResultParams = {
				offset: 0,
				chunkSize: 1,
				countTotal: false,
				searchFilterId: this.searchFilterId,
				entityObjectId: entityObjectId,
				entityObjectIdOnlySelection: true
			};

			const vlpId = this.eoResultService.vlpId;
			const vlpParams: HttpParams | undefined = this.eoResultService.vlpParams;

			return this.dataService
				.loadList(
					meta,
					vlpId,
					vlpParams,
					this.eoPreferenceService.getSelectedTablePreference(),
					this.eoSearchfilterService.getSelectedSearchfilter(),
					params
				)
				.pipe(map(boViewModel => boViewModel.bos.length > 0));
		}
	}

	executeCollectiveProcessing(
		multiSelectionResult: EntityObjectGridMultiSelectionResult,
		restUrl: string
	): Observable<CollectiveProcessingExecution> {
		let observable: Observable<CollectiveProcessingExecution>;
		const dataLoadContext = this.buildDataLoadContext();
		if (!dataLoadContext) {
			observable = EMPTY;
		} else {
			dataLoadContext.resultParams = {
				offset: 0,
				chunkSize: -1,
				countTotal: false,
				searchFilterId: this.searchFilterId
			};
			dataLoadContext.multiSelectionCondition = multiSelectionResult.toRestMultiSelectionResult();
			observable = this.dataService.executeCollectiveProcessing(dataLoadContext, restUrl);
		}

		return ObservableUtils.onSubscribe(observable, () => {
			this.setLoading(true);
		}).pipe(
			tap((execution: CollectiveProcessingExecution) => {
				this.progressCollectiveProcessing(execution);
			}),
			finalize(() => {
				this.setLoading(false);
			})
		);
	}

	abortCollectiveProcessing(
		restUrl: string
	): Observable<Object> {
		let observable: Observable<Object>;
		observable = this.dataService.abortCollectiveProcessing(restUrl);

		return ObservableUtils.onSubscribe(observable, () => {
			this.setLoading(true);
		}).pipe(
			tap((response: any) => {
				this.$log.debug('Got response: %o', response);
			}),
			finalize(() => {
				this.setLoading(false);
			})
		);
	}

	progressCollectiveProcessing(execution: CollectiveProcessingExecution) {
		this.eoResultService.forceCollectiveProcessingView = true;
		this.collectiveProcessingInitiated.next(execution);

		const progressHttpGetSubscription = this.http
			.get(execution.links.progress.href!)
			.pipe(
				catchError(e => {
					this.$log.warn(
						'Could not load collective processing progress %o: %o',
						execution.links.progress.href,
						e
					);
					progressHttpGetSubscription.unsubscribe();
					return EMPTY;
				}),
				map((info: CollectiveProcessingProgressInfo) => {
					this.collectiveProcessingProgress.next(info);
					if (info.percent >= 100) {
						this.eoResultService.invalidateCache();
						this.collectiveProcessingFinished.next();
						progressHttpGetSubscription.unsubscribe();
					} else {
						throw info;
					}
				}),
				retryWhen(errors => errors.pipe(
					delayWhen(_ => timer(500))
				))
			)
			.subscribe();
	}

	loadCollectiveProcessingSelectionOptions(
		multiSelectionResult: EntityObjectGridMultiSelectionResult
	): Observable<CollectiveProcessingSelectionOptions> {
		return this.eoResultService.updateSelectedMeta().pipe(
			mergeMap(() => {
				let observable: Observable<CollectiveProcessingSelectionOptions>;
				const dataLoadContext = this.buildDataLoadContext();
				if (!dataLoadContext) {
					observable = EMPTY;
				} else {
					dataLoadContext.resultParams = {
						offset: 0,
						chunkSize: -1,
						countTotal: false,
						searchFilterId: this.searchFilterId
					};
					dataLoadContext.multiSelectionCondition = multiSelectionResult.toRestMultiSelectionResult();
					observable = this.dataService
						.loadCollectiveProcessingSelectionOptions(dataLoadContext)
						.pipe
						// tap(selOptions => selOptions.boMetaId = dataLoadContext.meta.getEntityClassId()),
						();
				}

				return ObservableUtils.onSubscribe(observable, () => this.setLoading(true)).pipe(
					finalize(() => this.setLoading(false))
				);
			})
		);
	}

	subscribeCollectiveProcessingInitiated(): Subject<CollectiveProcessingExecution> {
		return this.collectiveProcessingInitiated;
	}

	subscribeCollectiveProcessingProgress(): Subject<CollectiveProcessingProgressInfo> {
		return this.collectiveProcessingProgress;
	}

	subscribeCollectiveProcessingFinished(): Subject<any> {
		return this.collectiveProcessingFinished;
	}

	private buildDataLoadContext(): LoadContext | undefined {
		const meta = this.eoResultService.getSelectedMeta();
		if (!meta) {
			return undefined;
		}
		let result: LoadContext = {
			meta: meta,
			vlpId: this.eoResultService.vlpId,
			vlpParams: this.eoResultService.vlpParams,
			attributeSelection: this.eoPreferenceService.getSelectedTablePreference(),
			searchtemplatePreference: this.eoSearchfilterService.getSelectedSearchfilter()
		};
		return result;
	}

	private loadResultsForSelectedEntityClass(
		event: LoadMoreResultsEvent
	): Observable<BoViewModel> {
		return this.eoResultService.updateSelectedMeta().pipe(
			mergeMap(() => {
				let observable: Observable<BoViewModel>;
				const meta = this.eoResultService.getSelectedMeta();
				const vlpId = this.eoResultService.vlpId;
				const vlpParams: HttpParams | undefined = this.eoResultService.vlpParams;
				if (!meta) {
					observable = EMPTY;
				} else {
					let resultParams: ResultParams = {
						offset: event.offset,
						chunkSize: event.limit,
						countTotal: event.count,
						searchFilterId: this.searchFilterId
					};
					observable = this.dataService
						.loadList(
							meta,
							vlpId,
							vlpParams,
							this.eoPreferenceService.getSelectedTablePreference(),
							this.eoSearchfilterService.getSelectedSearchfilter(),
							resultParams
						)
						.pipe(tap(boViewModel => (boViewModel.boMetaId = meta.getBoMetaId())));
				}

				return ObservableUtils.onSubscribe(observable, () => this.setLoading(true)).pipe(
					finalize(() => this.setLoading(false))
				);
			})
		);
	}
}
