import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { BehaviorSubject, Observable } from 'rxjs';
import { distinctUntilChanged, tap } from 'rxjs/operators';
import { AuthenticationService } from '../../authentication';
import { Logger } from '../../log/shared/logger';
import { PerspectiveService } from '../../perspective/shared/perspective.service';
import {
	Preference,
	PreferenceType,
	SearchtemplateAttribute,
	SearchtemplatePreferenceContent
} from '../../preferences/preferences.model';
import { SearchService } from '../../search/shared/search.service';
import { SearchfilterService } from '../../search/shared/searchfilter.service';
import { FqnService } from '../../shared/fqn.service';
import { EntityMeta } from './bo-view.model';
import { EntityObjectResultService } from './entity-object-result.service';
import { MetaService } from './meta.service';
import { SelectableService } from './selectable.service';

/**
 * Provides access to all available search filter for the currently selected entity.
 * And to the currently selected searchfilter.
 */
@Injectable()
export class EntityObjectSearchfilterService {
	static sortSearchfilters(
		preferences: Preference<SearchtemplatePreferenceContent>[],
		respectPosition
	): Preference<SearchtemplatePreferenceContent>[] {
		let sorted = preferences.sort((a, b) => {
			if (a.content.isTemp === true && b.content.isTemp !== true) {
				return -1;
			}
			if (a.content.isTemp !== true && b.content.isTemp === true) {
				return 1;
			}
			if (respectPosition) {
				if (a.content.position !== undefined && b.content.position === undefined) {
					return -1;
				}
				if (a.content.position === undefined && b.content.position !== undefined) {
					return 1;
				}
				if (a.content.position !== undefined && b.content.position !== undefined) {
					if (a.content.position > b.content.position) {
						return 1;
					} else if (a.content.position < b.content.position) {
						return -1;
					}
				}
			}
			if (a.name && b.name) {
				if (a.name > b.name) {
					return 1;
				}
				if (a.name < b.name) {
					return -1;
				}
			}
			return 0;
		});
		return sorted;
	}
	private selectedSearchfilter: BehaviorSubject<
		Preference<SearchtemplatePreferenceContent> | undefined
	> = new BehaviorSubject(undefined);
	private allSearchfilters: BehaviorSubject<
		Preference<SearchtemplatePreferenceContent>[]
	> = new BehaviorSubject([]);

	private loggedIn = false;

	private entityClassId: string | undefined;

	private urlQuery: string | undefined;

	constructor(
		private authenticationService: AuthenticationService,
		private selectableService: SelectableService,
		private perspectiveService: PerspectiveService,
		private searchService: SearchService,
		private searchfilterService: SearchfilterService,
		private eoResultService: EntityObjectResultService,
		private fqnService: FqnService,
		private metaService: MetaService,
		private $log: Logger
	) {
		this.authenticationService.observeLoginStatus().subscribe(loggedIn => {
			this.loggedIn = loggedIn;
			if (loggedIn) {
				this.loadSearchfilters();
			}
		});

		this.perspectiveService
			.observeSelectedSearchtemplatePreference()
			.subscribe(pref => this.selectSearchfilter(pref));

		this.eoResultService.observeSelectedEntityClassId().subscribe(entityClassId => {
			this.entityClassId = entityClassId;
			this.loadSearchfilters();
		});

		this.selectedSearchfilter
			.pipe(distinctUntilChanged())
			.subscribe(() => this.searchService.initiateDataLoad());
	}

	getSelectedSearchfilter(): Preference<SearchtemplatePreferenceContent> | undefined {
		return this.selectedSearchfilter.getValue();
	}

	sortSearchfilters() {
		let searchfilters = this.getAllSearchfilters();
		searchfilters = this._sortSearchfilters(searchfilters);
		searchfilters = _.clone(searchfilters);
		this.allSearchfilters.next(searchfilters);
	}

	selectSearchfilter(
		searchfilter: Preference<SearchtemplatePreferenceContent> | undefined,
		updatePreference = true,
		clearSearchfilter = false
	) {
		if (clearSearchfilter && searchfilter) {
			searchfilter.content.columns = [];
		}
		this.updateColumns(searchfilter);

		let searchfilters = this.getAllSearchfilters();

		if (searchfilter) {
			if (clearSearchfilter) {
				searchfilter.selected = true;
				this.searchfilterService.saveSearchfilter(searchfilter).subscribe();
			} else if (updatePreference) {
				this.searchfilterService.select(searchfilter);
			} else {
				searchfilter.selected = true;
			}
			let found = searchfilters.find(
				it =>
					!!searchfilter &&
					SearchtemplatePreferenceContent.equals(it.content, searchfilter.content) &&
					it.name === searchfilter.name
			);
			if (found) {
				searchfilter = found;
			} else {
				searchfilters.push(searchfilter);
				searchfilters = this._sortSearchfilters(searchfilters);
				searchfilters = _.clone(searchfilters);
				this.allSearchfilters.next(searchfilters);
			}
		} else {
			// Deselect the last searchfilter, if none is to be selected now
			this.searchfilterService.deselect(this.getSelectedSearchfilter());
		}

		this.selectedSearchfilter.next(searchfilter);

		// update selected flag in all searchfilters
		this.getAllSearchfilters().map(it => (it.selected = it === this.getSelectedSearchfilter()));
	}

	getAllSearchfilters(): Preference<SearchtemplatePreferenceContent>[] {
		return this.allSearchfilters.getValue();
	}

	observeSelectedSearchfilter(): Observable<
		Preference<SearchtemplatePreferenceContent> | undefined
	> {
		return this.selectedSearchfilter.pipe(tap(it => this.updateColumns(it)));
	}

	observeAllSearchfilters(): Observable<Preference<SearchtemplatePreferenceContent>[]> {
		return this.allSearchfilters.pipe(distinctUntilChanged());
	}

	deleteSearchfilter(searchfilter: Preference<SearchtemplatePreferenceContent> | undefined) {
		if (searchfilter) {
			this.searchfilterService.deleteSearchfilter(searchfilter).subscribe(() => {
				let searchfilters = this.getAllSearchfilters();
				let index = searchfilters.indexOf(searchfilter);
				if (index >= 0) {
					searchfilters.splice(index, 1);
					searchfilters = _.clone(searchfilters);
					this.allSearchfilters.next(searchfilters);
				}
				if (this.getSelectedSearchfilter() === searchfilter) {
					this.selectSearchfilter(undefined);
				}
			});
		}
	}

	applySearchtemplateIfNoPerspective(preferences, meta: EntityMeta, urlQuery: string | any) {
		let selectedPerspective = this.perspectiveService.getSelectedPerspective();
		let searchtemplatePreference;

		if (selectedPerspective && selectedPerspective.searchTemplatePrefId) {
			this.$log.debug(
				'Perspective is overriding default user-selected search template, ignoring.'
			);
			return;
		}

		this.urlQuery = urlQuery;
		if (urlQuery) {
			searchtemplatePreference = this.searchfilterService.createFromUrlSearch(meta, urlQuery);
		} else {
			searchtemplatePreference = preferences
				.filter(pref => pref.type === PreferenceType.searchtemplate.name)
				.find(pref => pref.selected as boolean);

			// No auto-selection!
			// if (!searchtemplatePreference) {
			// 	searchtemplatePreference = preferences
			// 		.filter(pref => pref.type === PreferenceType.searchtemplate.name)
			// 		.shift() as Preference<SearchtemplatePreferenceContent>;
			// }
		}

		// add meta data to columns
		if (searchtemplatePreference) {
			this.selectableService.addMetaDataToColumns(searchtemplatePreference, meta);
		}
		this.$log.debug('Selected searchtemplate: %o', searchtemplatePreference);

		if (searchtemplatePreference !== undefined) {
			let updatePreference = searchtemplatePreference.selected !== true;
			this.selectSearchfilter(searchtemplatePreference, updatePreference);
		}
	}

	updateColumns(searchfilter: Preference<SearchtemplatePreferenceContent> | undefined) {
		if (searchfilter && searchfilter.content && searchfilter.content.columns) {
			let entityClassIds: Set<string> = new Set<string>();
			entityClassIds.add(searchfilter.boMetaId);
			searchfilter.content.columns.forEach(c => {
				if (c.dependentBoMetaId !== undefined) {
					entityClassIds.add(c.dependentBoMetaId);
				}
			});
			entityClassIds.forEach(entityClassId =>
				this.metaService.getEntityMeta(entityClassId).subscribe(meta => {
					let mainMeta = searchfilter.boMetaId === entityClassId ? meta : undefined;
					let dependentMeta = mainMeta === undefined ? meta : undefined;
					this.updateColumnsForEntity(searchfilter, mainMeta, dependentMeta);
				})
			);
			searchfilter.content.columns
				.filter(column => column.operator === undefined)
				.forEach(column => {
					try {
						let operators = this.searchfilterService.getOperatorDefinitions()[
							column.inputType
						];
						column.operator = operators[0].operator;
					} catch (e) {
						// Ignore
					}
				});
		}
	}

	getUrlQuery(): string | undefined {
		return this.urlQuery;
	}

	private loadSearchfilters() {
		if (this.entityClassId !== undefined) {
			if (!this.loggedIn) {
				this.$log.debug('Load searchfilters canceled, not logged in!');
				return;
			}
			this.$log.debug('Load searchfilters for %o', this.entityClassId);
			this.searchfilterService
				.getSearchfiltersForEntity(this.entityClassId)
				.subscribe(preferences =>
					this.setSearchfilters(preferences as Preference<
						SearchtemplatePreferenceContent
					>[])
				);
		}
	}

	private _sortSearchfilters(searchfilters) {
		return EntityObjectSearchfilterService.sortSearchfilters(searchfilters, false);
	}

	private setSearchfilters(preferences: Preference<SearchtemplatePreferenceContent>[]) {
		this.$log.debug('Set searchfilters for %o', this.entityClassId);
		let tempFilters = preferences.filter(it => it.content.isTemp === true);
		if (tempFilters.length < 1 && this.entityClassId) {
			let tempFilter = this.searchfilterService.createTempSearchfilter(this.entityClassId);
			preferences.push(tempFilter);
		} else if (tempFilters.length > 1) {
			// clean up to many temps
			for (const tempFilter of tempFilters.splice(0, tempFilters.length - 1)) {
				let index = preferences.indexOf(tempFilter);
				preferences.splice(index, 1);
				this.searchfilterService.deleteSearchfilter(tempFilter).subscribe();
			}
		}
		let sorted = this._sortSearchfilters(preferences);
		this.allSearchfilters.next(sorted);
		this.selectSearchfilter(sorted.find(it => it.selected === true), false);
	}

	private updateColumnsForEntity(
		searchfilter: Preference<SearchtemplatePreferenceContent>,
		mainMeta: EntityMeta | undefined,
		dependentMeta: EntityMeta | undefined
	) {
		let currentMeta = dependentMeta === undefined ? mainMeta : dependentMeta;
		if (currentMeta !== undefined) {
			let meta: EntityMeta = currentMeta; // undefined-safe 'meta'
			this.selectableService.addMetaDataToColumns(searchfilter, meta);
			searchfilter.content.columns
				.filter(c => {
					if (dependentMeta === undefined) {
						// mainEntity - default call
						return c.dependentBoMetaId === undefined;
					} else {
						// call for a dependent attribute
						return c.dependentBoMetaId === dependentMeta.getEntityClassId();
					}
				})
				.forEach(column => {
					try {
						meta.getAttributes().forEach((_attributeMeta, attributeKey) => {
							let shortAttributeName = this.fqnService.getShortAttributeName(
								meta.getBoMetaId(),
								column.boAttrId
							);
							if (attributeKey === shortAttributeName) {
								column.inputType = SearchfilterService.getInputType(<
									SearchtemplateAttribute
								>meta.getAttribute(attributeKey));
								let operator = this.searchfilterService.getOperatorDefinition(
									column
								);
								if (operator) {
									this.searchfilterService.formatValue(column, operator);
								}
								throw 'break';
							}
						});
					} catch (e) {
						// Ignore
					}
				});
		}
	}
}
