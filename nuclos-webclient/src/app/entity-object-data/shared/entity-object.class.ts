import { IEntityObject, IEntityObjectDependents, ISubEntityObject } from '@nuclos/nuclos-addon-api';
import * as _ from 'lodash';
import * as hash from 'object-hash';
import {
	Observable,
	of as observableOf,
	ReplaySubject,
	Subject,
	Subscriber,
	throwError as observableThrowError
} from 'rxjs';

import { distinctUntilChanged, map, mergeMap, tap, take } from 'rxjs/operators';
import { AuthenticationService } from '../../authentication';
import { Command } from '../../command/shared/command';
import { Generation } from '../../generation/shared/generation';
import { Logger } from '../../log/shared/logger';
import { FqnService } from '../../shared/fqn.service';
import { Link, LinkContainer, SubEOLinkContainer } from '../../shared/link.model';
import { NuclosDefaults } from '../../shared/nuclos-defaults';
import { ObjectUtils } from '../../shared/object-utils';
import { StringUtils } from '../../shared/string-utils';
import { State, StateInfo } from '../../state/shared/state';
import { ValidationError } from '../../validation/validation-error';
import { ValidationStatus } from '../../validation/validation-status.enum';
import { AutonumberService } from './autonumber.service';
import {
	EntityAttrMeta,
	EntityMeta,
	EntityObjectData,
	EntityObjectSubEoInfo,
	EntityRestriction,
	LovEntry
} from './bo-view.model';
import { EntityObjectDependentsManager } from './entity-object-dependents-manager';
import { EntityObjectEventListener } from './entity-object-event-listener';
import { EntityObjectEventService } from './entity-object-event.service';
import { EntityObjectResultService } from './entity-object-result.service';
import { EntityObjectService } from './entity-object.service';
import { LovSearchConfig } from './lov-search-config';
import { MetaService } from './meta.service';
import { SortModel } from './sort.model';
import { VlpContext } from './vlp-context';

/**
 * Represents an entity object.
 * Holds a reference to the actual entity object data and provides additional methods.
 */
export class EntityObject implements IEntityObject {
	title: string;
	info: string;
	protected dependentsManager: EntityObjectDependentsManager;
	/**
	 * Remembers the old data when this EO is edited.
	 * This is used for resetting to the previous state when cancelling modifications.
	 */
	private oldEOData: EntityObjectData;

	private meta: EntityMeta;

	private readonly?: boolean;

	private listeners: EntityObjectEventListener[];

	/**
	 * Holds available entries for list-of-values components.
	 *
	 * @type {Map<string, "../../Observable".Observable<LovEntry[]>>}
	 */
	private lovEntries = new Map<string, Map<string, Subject<LovEntry[]>>>();
	private vlpContext;

	private attributeValidation = new Map<string, ValidationStatus>();
	private validatingAttributes = false;

	/**
	 * Emits the layout URL after layout related events (e.g. selection of a perspective).
	 * Might emit duplicates.
	 */
	private layoutSubject = new Subject<string>();

	constructor(private eoData: EntityObjectData) {
		this.listeners = [];
		this.vlpContext = new VlpContext(this);
		this.dependentsManager = new EntityObjectDependentsManager(this);

		this.layoutSubject.pipe(distinctUntilChanged()).subscribe(layoutURL => {
			this.lovEntries.clear();
			this.notifyLayoutChanged(layoutURL);
		});

		this.getService().listenForExternalChanges(this);
	}

	/**
	 * Clones this EO.
	 * The clone can be used as a new EO for inserts.
	 *
	 * @returns {EntityObject}
	 */
	clone(): EntityObject {
		return new EntityObject(this.cloneData());
	}

	/**
	 * Clones this EO on the server side.
	 */
	cloneOnServerSide(layoutid?: string | undefined): Observable<EntityObject> {
		let result: Observable<EntityObject> = this.getService().clone(this, layoutid);

		for (let listener of this.listeners) {
			if (listener.afterClone) {
				listener.afterClone(this);
			}
		}

		return result;
	}

	addListener(listener: EntityObjectEventListener) {
		if (this.listeners.indexOf(listener) < 0) {
			this.listeners.push(listener);
		}
	}

	removeListener(listener: EntityObjectEventListener) {
		let index = this.listeners.indexOf(listener);
		if (index >= 0) {
			this.listeners.splice(index, 1);
		}
	}

	removeListenersByType(type: any) {
		for (let listener of this.listeners) {
			if (listener instanceof type) {
				this.removeListener(listener);
			}
		}
	}

	/**
	 * Returns the complete underlying data object.
	 * This should normally not be accessed directly.
	 *
	 * @returns {EntityObjectData}
	 */
	getData(): EntityObjectData {
		return this.eoData;
	}

	/**
	 * Sets the complete underlying data object.
	 * This should normally not be accessed directly.
	 *
	 * @returns {EntityObjectData}
	 */
	setData(data: EntityObjectData): void {
		this.eoData = data;
	}

	mergeData(data: EntityObjectData) {
		for (let attributeName in data.attributes) {
			if (data.attributes.hasOwnProperty(attributeName)) {
				let value = data.attributes[attributeName];
				this.setAttributeUnsafe(attributeName, value);
			}
		}

		this.eoData.version = data.version;

		if (data.nextStates !== undefined) {
			this.eoData.nextStates = data.nextStates;
		}
		if (data.title !== undefined) {
			this.eoData.title = data.title;
		}
		if (data.info !== undefined) {
			this.eoData.info = data.info;
		}
		if (data.links !== undefined) {
			this.eoData.links = data.links;
		}
		if (data.generations !== undefined) {
			this.eoData.generations = data.generations;
		}

		this.reloaded();
	}

	/**
	 * Returns the record ID of this EO.
	 */
	getId(): number | undefined {
		return this.eoData.boId;
	}

	/**
	 * Returns the temporary ID of a new, unsaved EO.
	 */
	getTemporaryId(): string | undefined {
		return this.eoData.temporaryId;
	}

	/**
	 * Returns the entity class ID (aka "boMetaId") of this EO.
	 */
	getEntityClassId(): string {
		return this.eoData.boMetaId;
	}

	/**
	 * Determines if this EO is new.
	 * If so, it has no ID yet and should be flagged as 'insert'.
	 */
	isNew(): boolean {
		return !this.getId();
	}

	/**
	 * Determines if this EO is dirty, i.e. it has unsaved changes.
	 */
	isDirty(): boolean {
		return !!this.eoData.dirty || this.isNew();
	}

	/**
	 * Marks this EO as modified.
	 *
	 * It should not be necessary to call this externally!
	 * Attribute manipulations change this flag automatically.
	 *
	 * @deprecated DO NOT USE!
	 */
	setDirty(dirty: boolean): EntityObject {
		return this._setDirty(dirty);
	}

	getRowColor() {
		return this.eoData.rowcolor;
	}

	getTextColor(): string | undefined {
		return this.eoData.textcolor;
	}

	setTextColor(textColor: string | undefined) {
		this.eoData.textcolor = textColor;
	}

	getLinks(): LinkContainer | undefined {
		return this.eoData && this.eoData.links;
	}

	getSelfURL(): string | undefined {
		let links = this.getLinks();
		return links && links.self.href;
	}

	getInsertURL(): string | undefined {
		let links = this.getLinks();
		return links && links.insert && links.insert.href;
	}

	getCloneURL(): string | undefined {
		let links = this.getLinks();
		return links && links.clone && links.clone.href;
	}

	getLayoutURL(): string | undefined {
		let links = this.getLinks();
		return links && links.layout && links.layout.href;
	}

	getLayoutId(): string | undefined {
		// TODO: Fixed the hack that relied on the layout ID being the last path param!
		// But it's still not good to search for something ending in "LO". The layout-ID should be properly
		// determined from the metadata, just like the layout URL.
		let layoutUrl = this.getLayoutURL();
		return layoutUrl && layoutUrl.split('/').find(value => value.endsWith('LO'));
	}

	getLayoutURLDynamically(): Observable<string | undefined> {
		return this.getService().getLayoutURLDynamically(this);
	}

	setLayoutURL(layoutURL: string) {
		let oldLayoutURL = this.getLayoutURL();

		let links = this.getLinks();
		if (!links) {
			links = {} as LinkContainer;
		}
		if (!links.layout) {
			links.layout = {} as Link;
		}

		links.layout.href = layoutURL;

		if (layoutURL !== oldLayoutURL) {
			this.checkLayoutChange();
		}
	}

	getPrintoutURL(): string | undefined {
		let links = this.getLinks();
		return links && links.printouts && links.printouts.href;
	}

	/**
	 * Returns all attributes.
	 * Do not use this for modifications - use {@link setAttribute} instead.
	 *
	 * @returns {Object}
	 */
	getAttributes(): Map<string, object> {
		return this.eoData.attributes;
	}

	getAttribute(attributeId: string): any {
		let attributes = this.getAttributes();
		let attributeName = FqnService.getShortAttributeNameFailsafe(
			this.getEntityClassId(),
			attributeId
		);
		attributeName = StringUtils.replaceAll(attributeName, '_', '');

		let result = attributes[attributeName];

		// TODO: The following hack is needed because the REST service prefixes numeric attribute names with an underscore.
		if (typeof result === 'undefined') {
			attributeName = '_' + attributeName;
			result = attributes[attributeName];
		}

		return result;
	}

	getNuclosProcess(): any {
		return this.getAttribute('nuclosProcess');
	}

	getMandatorId(): string {
		let mandatorAttr = this.getAttribute('nuclosMandator');
		return mandatorAttr ? mandatorAttr.id : undefined;
	}

	/**
	 * Clears the attribute with the given ID.
	 * Does nothing if the attribute value is already falsy
	 * or the value is a reference pointing to nothing.
	 *
	 * @param attributeId
	 * @returns {any}
	 */
	clearAttribute(attributeId: string): any {
		let value = this.getAttribute(attributeId);

		if (!value || (value.hasOwnProperty('id') && !value.id)) {
			Logger.instance.debug('Attribute %o already undefined - not clearing');
			return;
		} else {
			Logger.instance.debug('Clearing attribute %o, old value = %o', attributeId, value);
		}

		this.getAttributeMeta(attributeId).pipe(take(1)).subscribe(meta => {
			if (!meta.isNullable() && meta.isBoolean()) {
				this.setAttribute(attributeId, false);
			} else {
				this.setAttribute(attributeId, null);
			}
		});
	}

	/**
	 * Sets the value for the attribute with the given ID
	 * and marks this EO as modified.
	 * This method does nothing if the old value equals the new value.
	 *
	 * TODO: Setting the attribute should be synchronous
	 *
	 * @param attributeId
	 * @param value
	 */
	setAttribute(attributeId: string, value: any): void {
		this.setAttributeWithSubscriber(attributeId, value, undefined);
	}

	// TODO: Setting the attribute should be synchronous
	setAttributeObserved(attributeId: string, value: any): Observable<boolean> {
		return new Observable<boolean>(subscriber =>
			this.setAttributeWithSubscriber(attributeId, value, subscriber)
		);
	}

	/**
	 * Only changes the attribute value without any validations or listener notifications etc.
	 */
	setAttributeUnsafe(attributeNameOrId: string, value: any) {
		let attributeName = FqnService.getShortAttributeNameFailsafe(
			this.getEntityClassId(),
			attributeNameOrId
		);

		let attributes = this.getAttributes();
		attributes[attributeName] = value;
	}

	isAttributeReadable(attributeName: string): boolean {
		let restriction = this.getAttributeRestriction(attributeName);
		return restriction !== 'hidden';
	}

	isAttributeWritable(attributeName: string): boolean {
		let restriction = this.getAttributeRestriction(attributeName);
		return restriction !== 'readonly' && restriction !== 'hidden' && restriction !== 'disabled';
	}

	isAttributeHidden(attributeName: string): boolean {
		const restriction = this.getAttributeRestriction(attributeName);
		return restriction === 'hidden';
	}

	/**
	 * Determines if this EO is deletable.
	 * New objects are not deletable (only cancelable).
	 *
	 * @returns {boolean}
	 */
	canDelete(): boolean {
		return this.eoData.canDelete === true && !this.isNew();
	}

	canWrite(): boolean {
		return this.eoData.canWrite === true && !(this.readonly === true);
	}

	/**
	 * Sets this EO as globally selected (for display/editing).
	 */
	select(navigate = true): void {
		return this.getResultService().selectEo(this, navigate);
	}

	/**
	 * Saves this EntityObject.
	 * The result is an Observable which returns the updated EO after save.
	 *
	 */
	save(): Observable<EntityObject> {
		return this.getService().save(this);
	}

	saving(inProgress: boolean) {
		for (let listener of this.listeners) {
			if (listener.isSaving) {
				listener.isSaving(this, inProgress);
			}
		}
	}

	/**
	 * Triggers the 'saved' event for all listeners.
	 * And recursively for all dependents.
	 */
	saved(data: EntityObjectData): void {
		this.saving(false);

		this.setData(data);

		this._setDirty(false);
		this.getData()._flag = undefined;

		this.dependentsManager.clearSelection();
		this.dependentsManager.getAllDependents().forEach(dependents => {
			// TODO: It is not always necessary to reload the dependents
			dependents.clear();
		});

		for (let listener of this.listeners) {
			if (listener.afterSave) {
				listener.afterSave(this);
			}
		}

		this.checkLayoutChange();

		this.getService().publishChanges(this);
	}

	/**
	 * Re-loads the data for this EO from the server.
	 *
	 */
	reload(): Observable<EntityObject> {
		Logger.instance.debug('Reload: %o', this);

		let eoId = this.getId();
		if (!eoId) {
			return observableThrowError('Can not reload (new) EO without ID');
		}

		return this.getService()
			.reloadEo(this)
			.pipe(tap(() => this.reloaded()));
	}

	/**
	 * Deletes this EntityObject.
	 * The result is an Observable which returns the EO again after deletion.
	 */
	delete(): Observable<EntityObject> {
		return this.getService()
			.delete(this)
			.pipe(
				tap(eo => {
					for (let listener of this.listeners) {
						if (listener.afterDelete) {
							listener.afterDelete(this);
						}
					}
				})
			);
	}

	isMarkedAsDeleted(): boolean {
		return this.getData()._flag === 'delete';
	}

	markAsDeleted(): void {
		this._setDirty(true);
		this.getData()._flag = 'delete';
	}

	/**
	 * Resets this EO to the state it had last before becoming dirty.
	 * Also resets all dependents.
	 */
	reset() {
		if (!this.isDirty()) {
			return;
		}

		this.getVlpContext().clear();

		for (let listener of this.listeners) {
			if (listener.beforeCancel) {
				listener.beforeCancel(this);
			}
		}

		/**
		 * On reset all new, unsaved dependents are deleted.
		 * Modified dependents are reset.
		 */
		this.dependentsManager.getAllDependents().forEach(value => {
			let newDependents: ISubEntityObject[] = [];
			let subEos = value.current();
			if (subEos) {
				subEos.forEach(subEO => {
					if (!subEO.isNew()) {
						subEO.reset();
						newDependents.push(subEO);
					}
				});
			}
			value.set(newDependents);
		});

		if (this.oldEOData) {
			this.setData(this.oldEOData);
			this.checkLayoutChange();
		}

		this._setDirty(false);
		this.updateValidationStatus();

		for (let listener of this.listeners) {
			if (listener.afterCancel) {
				listener.afterCancel(this);
			}
		}
	}

	getMeta(): Observable<EntityMeta> {
		if (this.meta) {
			return observableOf(this.meta);
		}
		return MetaService.instance
			.getEntityMeta(this.getEntityClassId())
			.pipe(tap(meta => (this.meta = meta)));
	}

	getDetailMeta(): Observable<EntityMeta> {
		return new Observable<EntityMeta>(observer => {
			this.getMeta().pipe(take(1)).subscribe(
				meta => {
					let detailEntityClassId = meta.getDetailEntityClassId();
					if (detailEntityClassId) {
						MetaService.instance.getEntityMeta(detailEntityClassId).pipe(take(1)).subscribe(
							detailMeta => {
								observer.next(detailMeta);
								observer.complete();
							},
							error => observer.error(error)
						);
					} else {
						observer.complete();
					}
				},
				error => observer.error(error)
			);
		});
	}

	getAttributeMeta(attributeName: string): Observable<EntityAttrMeta> {
		return this.getMeta().pipe(map(meta => meta.getAttributeMeta(attributeName))) as Observable<
			EntityAttrMeta
		>;
	}

	getAttributeLabel(attributeId: string): Observable<string> {
		return this.getMeta().pipe(map(meta => meta.getAttributeLabel(attributeId))) as Observable<
			string
		>;
	}

	hasSubEos() {
		let subEoLinks = this.getSubEoInfos();
		return subEoLinks && subEoLinks.size > 0;
	}

	/**
	 * Returns the links for all available Sub-EOs.
	 */
	getSubEoInfos(): Map<string, EntityObjectSubEoInfo> | undefined {
		let m = new Map();
		let subEoLinks = this.eoData.subBos;
		if (subEoLinks) {
			for (let x in subEoLinks) {
				if (subEoLinks.hasOwnProperty(x)) {
					m.set(x, subEoLinks[x]);
				}
			}
		}
		return m;
	}

	setSubEOLinks(subEOLinks: Map<string, { links: SubEOLinkContainer }>) {
		this.eoData.subBos = subEOLinks;
	}

	/**
	 * Returns the links for the Sub-EO for the given attribute ID.
	 */
	getSubEoLink(attributeID: string): SubEOLinkContainer | undefined {
		let result;

		let subEoInfos = this.getSubEoInfos();
		if (subEoInfos) {
			let subEoInfo = subEoInfos.get(attributeID);
			result = subEoInfo && subEoInfo.links;
		}

		return result;
	}

	/**
	 * Returns the restriction for the Sub-EO for the given attribute ID.
	 */
	getSubEoRestriction(attributeID: string): EntityRestriction | undefined {
		let result;

		let subEoInfos = this.getSubEoInfos();
		if (subEoInfos) {
			let links = subEoInfos.get(attributeID);
			result = links && links.restriction;
		}

		return result;
	}

	/**
	 * Returns the restriction for the Sub-EO for the given attribute ID.
	 */
	getSubEOReadOnlyAttributes(attributeID: string): Object | undefined {
		let result;

		let subEoInfos = this.getSubEoInfos();
		if (subEoInfos) {
			let subEoInfo = subEoInfos.get(attributeID);
			result = subEoInfo && subEoInfo.readonlyattributes;
		}

		return result;
	}

	/**
	 * Returns the current Nuclos state of this EO.
	 *
	 * @returns {any}
	 */
	getState(): State | undefined {
		let result: State | undefined = undefined;
		let state = this.getAttribute('nuclosState');

		if (state) {
			let stateNumber = this.getAttribute('nuclosStateNumber');
			let stateIconLink = this.getStateIconLink();

			result = {
				nuclosStateId: state.id,
				name: state.name,
				number: stateNumber,

				links: {
					stateIcon: stateIconLink
				}
			};
		}

		return result;
	}

	getStateIconLink(): Link | undefined {
		let links = this.getLinks();
		return links && links.stateIcon;
	}

	/**
	 * Returns infos about all Nuclos states available for the next state change.
	 */
	getNextStates(): StateInfo[] {
		return this.eoData.nextStates ? this.eoData.nextStates : [];
	}

	/**
	 * Changes the state of this EO, without any confirmation dialogs etc.
	 */
	changeState(state: State): Observable<EntityObject> {
		return this.getService().changeState(this, state);
	}

	/**
	 * Returns all available object generators for this EO.
	 */
	getGenerations(): Generation[] {
		let result: Generation[] = [];

		if (this.eoData && this.eoData.generations) {
			result = this.eoData.generations;
		}

		return result;
	}

	/**
	 * Filters internal generations.
	 */
	getGenerationsForUser(): Generation[] {
		let result = this.getGenerations();

		result = result.filter(it => it.internal !== true);

		return result;
	}

	getGeneration(generationId: string): Generation | undefined {
		return this.getGenerations().find(generation => generation.generationId === generationId);
	}

	getImageUrl(attributeId: string) {
		let data = this.getData();
		let attrImages = data.attrImages;
		if (attrImages) {
			const attributeName = FqnService.getShortAttributeNameFailsafe(
				this.getEntityClassId(),
				attributeId
			);
			let link = attrImages.links[attributeName];
			return link ? link.href : undefined;
		}
		return undefined;
	}

	getError(): string | undefined {
		return this.eoData.businessError;
	}

	/**
	 * Executes the CustomRule with the given name.
	 * This also saves this EO.
	 *
	 * TODO: Ensure the given rule is available for this EO?
	 */
	executeRule(rule: string): Observable<EntityObject> {
		return this.getService().executeCustomRule(this, rule);
	}

	/**
	 * Does not load the dependents initially!
	 */
	getDependents(attributeId: string): IEntityObjectDependents {
		return this.dependentsManager.getDependents(attributeId);
	}

	/**
	 * Loads the dependents initially.
	 */
	getDependentsAndLoad(attributeId: string, sortModel?: SortModel): IEntityObjectDependents {
		let dependents = this.getDependents(attributeId);

		dependents.loadIfEmpty(sortModel);

		return dependents;
	}

	getLovEntries(
		attributeFQN: string,
		valuelistProvider?: WebValuelistProvider
	): Observable<LovEntry[]> {
		let attributeName = FqnService.getShortAttributeName('', attributeFQN);
		let vlpMap = this.lovEntries.get(attributeName);

		if (!vlpMap) {
			vlpMap = new Map<string, Subject<LovEntry[]>>();
			this.lovEntries.set(attributeName, vlpMap);
		}

		let vlpHash = valuelistProvider ? hash(valuelistProvider) : '';

		let result = vlpMap.get(vlpHash);
		if (!result) {
			result = new ReplaySubject<LovEntry[]>(1);
			vlpMap.set(vlpHash, result);

			this.loadLovEntries(attributeName, valuelistProvider).pipe(take(1)).subscribe(lovEntries => {
				if (result) {
					result.next(lovEntries);
				}
			});
		}

		return result;
	}

	clearLovEntries(attributeId?: string) {
		if (attributeId) {
			let attributeName = FqnService.getShortAttributeNameFailsafe(
				this.getEntityClassId(),
				attributeId
			);
			this.lovEntries.delete(attributeName);
		} else {
			this.lovEntries.clear();
		}
	}

	refreshVlp(
		attributeId: string,
		valuelistProvider?: WebValuelistProvider
	): Observable<LovEntry[]> {
		let attributeName = FqnService.getShortAttributeName('', attributeId);

		let vlpMap = this.lovEntries.get(attributeName);

		if (!vlpMap) {
			vlpMap = new Map<string, Subject<LovEntry[]>>();
		}

		let vlpHash = valuelistProvider ? hash(valuelistProvider) : '';
		const result = vlpMap.get(vlpHash);
		if (!result) {
			return this.getLovEntries(attributeName, valuelistProvider);
		}

		this.loadLovEntries(attributeName, valuelistProvider).pipe(take(1)).subscribe(lovEntries => {
			result.next(lovEntries);
		});

		return result;
	}

	invalidateValuelist(attributeId: string) {
		let attributeName = FqnService.getShortAttributeName('', attributeId);
		this.lovEntries.delete(attributeName);
	}

	getVlpContext() {
		return this.vlpContext;
	}

	getVlpParameters(attributeId: string) {
		let result = this.getOwnVlpParameters(attributeId);
		let entityClassId = this.getEntityClassId();

		let vlpParameters = this.getRootEo()
			.getVlpContext()
			.getVlpParameters(entityClassId);

		if (vlpParameters) {
			let parentParametersForAttribute = vlpParameters.getParametersForAttribute(attributeId);
			if (result) {
				if (parentParametersForAttribute) {
					result = result.merge(parentParametersForAttribute);
				}
			} else {
				result = parentParametersForAttribute;
			}
		}

		return result;
	}

	/**
	 * Serializes this EO (recursively including dependents) for Update/Insert-Requests.
	 */
	serialize(): EntityObjectData {
		let data = ObjectUtils.cloneDeep(this.getData());

		data.subBos = {
			insert: {},
			update: {},
			delete: {}
		};

		this.dependentsManager.getAllDependents().forEach((value, key) => {
			let subEos = value.current();
			if (subEos) {
				subEos.forEach((subEO: SubEntityObject) => {
					let flag = <string>subEO.getData()._flag;
					if (!flag) {
						return;
					}
					if (!data.subBos[flag][key]) {
						data.subBos[flag][key] = [];
					}

					if (flag === 'delete') {
						data.subBos[flag][key].push(subEO.getId());
					} else {
						data.subBos[flag][key].push(subEO.serialize());
					}
				});
			}
		});

		return data;
	}

	checkLayoutChange() {
		this.getLayoutURLDynamically().pipe(take(1)).subscribe(url => {
			this.layoutSubject.next(url);
		});
	}

	getCommands(): Command<any>[] | undefined {
		let commands = this.getData().commands;
		return commands && commands.list;
	}

	setAttributeValidationError(validationError: ValidationError) {
		let attributeName = FqnService.getShortAttributeName('', validationError.field);

		let status;
		if (validationError.errortype === 'MANDATORY_FIELD_ERROR') {
			status = ValidationStatus.MISSING;
		} else {
			status = ValidationStatus.INVALID;
		}

		this.attributeValidation.set(attributeName, status);
	}

	getValidationStatus(attributeId: string): ValidationStatus | undefined {
		let attributeName = FqnService.getShortAttributeNameFailsafe(
			this.getEntityClassId(),
			attributeId
		);
		let status = this.attributeValidation.get(attributeName);

		// TODO: Check if the given attribute really belongs to this EO, or throw an exception
		if (status === undefined && !this.isInternalAttribute(attributeName)) {
			this.attributeValidation.set(attributeName, ValidationStatus.VALIDATING);
			status = ValidationStatus.VALIDATING;
			this.updateValidationStatus();
		}

		return status;
	}

	deleted() {
		this._setDirty(false);
	}

	getOwner(): any {
		return this.getAttribute('nuclosOwner');
	}

	isLocked(): boolean {
		let owner = this.getOwner();
		return !!(owner && owner.id);
	}

	setMandator(mandator: Mandator) {
		this.setAttribute('nuclosMandator', {
			id: mandator.mandatorId,
			name: mandator.name
		});
	}

	/**
	 * @deprecated client should evaluate title from titlePattern
	 */
	getTitle() {
		return this.getData().title;
	}

	/**
	 * @deprecated client should evaluate info from infoPattern
	 */
	getInfo() {
		return this.getData().info;
	}

	getRootEo(): EntityObject {
		return this;
	}

	getSelectionChangesInParentSubform(parentEntityClassId: string): Observable<SubEntityObject[]> {
		return this.dependentsManager.observeSelectionChange(parentEntityClassId);
	}

	getSelectionInParentSubform(parentEntityClassId: string): SubEntityObject[] {
		return this.dependentsManager.getSelection(parentEntityClassId);
	}

	clearOwner() {
		this.setAttributeUnsafe('nuclosOwner', null);
		let links = this.getLinks();
		if (links) {
			delete links.lock;
		}
	}

	setSubEosSelected(entityClassId: string, selectedSubEOs: SubEntityObject[]) {
		this.dependentsManager.selectionChanged(entityClassId, selectedSubEOs);
	}

	isDynamicTaskList() {
		// TODO: Do not rely on a prefix.
		// The information whether this is some kind of dynamic entity should be part of the meta data.
		return this.getEntityClassId().endsWith('DTL');
	}

	getCreatedBy(): string | undefined {
		return <string | undefined>this.getAttribute('createdBy');
	}

	getCreatedAt(): Date | undefined {
		let createdAt = this.getAttribute('createdAt');
		return createdAt ? new Date(createdAt) : undefined;
	}

	getChangedBy(): string | undefined {
		return <string | undefined>this.getAttribute('changedBy');
	}

	getChangedAt(): Date | undefined {
		let changedAt = this.getAttribute('changedAt');
		return changedAt ? new Date(changedAt) : undefined;
	}

	setReadonly(readonly?: boolean) {
		this.readonly = readonly;
	}

	getReadonly() {
		return this.readonly;
	}

	protected getService(): EntityObjectService {
		return EntityObjectService.instance;
	}

	protected getResultService(): EntityObjectResultService {
		return EntityObjectResultService.instance;
	}

	protected getAuthenticationService(): AuthenticationService {
		return AuthenticationService.instance;
	}

	/**
	 * Clones the underlying data of this EO while omitting the primary key.
	 *
	 * TODO: Omit other meta data, too?
	 */
	protected cloneData(): EntityObjectData {
		let clonedData = ObjectUtils.cloneDeep(this.getData());
		clonedData.boId = undefined;
		clonedData.temporaryId = undefined;
		clonedData._flag = 'insert';
		return clonedData;
	}

	protected _setDirty(dirty: boolean): EntityObject {
		// Remember the current state when this EO gets dirty.
		if (dirty && !this.isDirty()) {
			this.oldEOData = ObjectUtils.cloneDeep(this.eoData);
		}
		this.eoData.dirty = dirty;

		if (dirty) {
			if (this.isNew()) {
				this.getData()._flag = 'insert';
			} else if (!this.isMarkedAsDeleted()) {
				this.getData()._flag = 'update';
			}

			for (let listener of this.listeners) {
				if (listener.afterDirty) {
					listener.afterDirty(this);
				}
			}
		}

		return this;
	}

	protected setAttributeValidated(attributeName: string, value: any): boolean {
		let attributes = this.getAttributes();
		Logger.instance.debug('Setting value %o for attribute %o', value, attributeName);

		let oldValue = attributes[attributeName];
		if (
			oldValue === value ||
			(value === null && oldValue === undefined) ||
			this.hasEqualID(oldValue, value)
		) {
			Logger.instance.debug('Old attribute value equals new value, skipping');
			return false;
		}

		// setDirty() must be called before the value is actually changed!
		this._setDirty(true);
		this.setAttributeUnsafe(attributeName, value);

		for (let listener of this.listeners) {
			if (listener.afterAttributeChange) {
				listener.afterAttributeChange(this, attributeName, oldValue, value);
			}
		}

		if (attributeName === 'nuclosProcess') {
			this.getService().nuclosProcessChanged(this);
		}

		this.updateValidationStatusForAttributeName(attributeName);

		EntityObjectEventService.instance.emitModifiedEo(this);

		return true;
	}

	protected getAttributeRestriction(attributeName: string) {
		if (this.eoData.attrRestrictions !== undefined) {
			return this.eoData.attrRestrictions[attributeName];
		}
		return undefined;
	}

	/**
	 * TODO: VLPs can have default values, which should be selected automatically.
	 */
	protected loadLovEntries(
		attributeName: string,
		valuelistProvider?: WebValuelistProvider
	): Observable<LovEntry[]> {
		attributeName = FqnService.getShortAttributeName('', attributeName);
		let mandatorId =
			this.getMandatorId() ||
			this.getRootEo().getMandatorId() ||
			this.getAuthenticationService().getCurrentMandatorId();

		return this.getAttributeMeta(attributeName).pipe(
			mergeMap(meta => {
				let search: LovSearchConfig = {
					eo: this,
					attributeMeta: meta,
					mandatorId: mandatorId,
					vlp: valuelistProvider,
					resultLimit: NuclosDefaults.DROPDOWN_LOAD_RESULT_LIMIT
				};
				return this.getService().loadLovEntries(search);
			})
		);
	}

	private setAttributeWithSubscriber(
		attributeId: string,
		value: any,
		subscriber: Subscriber<boolean> | undefined
	): void {
		let attributeName = FqnService.getShortAttributeNameFailsafe(
			this.getEntityClassId(),
			attributeId
		);

		this.getAttributeMeta(attributeName).pipe(take(1)).subscribe(meta => {
			let validatedValue = this.getService().validateAttributeValue(value, meta);
			let changed = this.setAttributeValidated(attributeName, validatedValue);
			if (meta) {
				if (meta.isImage()) {
					// image attributes are only available in "attrImages" not in "attributes"
					// to delete an image the REST service expects a null value for the image attribute
					this.setAttributeUnsafe(attributeName, validatedValue);
					changed = true;
					this._setDirty(true);
				}
			} else {
				Logger.instance.warn('Unable to get mata data for "%s".', attributeId);
			}
			if (subscriber) {
				subscriber.next(changed);
				subscriber.complete();
			}
		});
	}

	/**
	 * Determines if the 2 given parameters are both truthy and have an equal "id" attribute.
	 */
	private hasEqualID(o1: any, o2: any): boolean {
		let result =
			o1 && o2 && o1.hasOwnProperty('id') && o2.hasOwnProperty('id') && o1.id === o2.id;

		return result;
	}

	private reloaded() {
		this.dependentsManager.clear();
		this.getVlpContext().clear();
		this.checkLayoutChange();
		this.updateValidationStatus();

		for (let listener of this.listeners) {
			if (listener.afterReload) {
				listener.afterReload(this);
			}
		}
	}

	private getOwnVlpParameters(attributeId: string) {
		let entityClassId = this.getEntityClassId();
		let vlpParameters = this.vlpContext.getVlpParameters(entityClassId);

		let result;

		if (vlpParameters) {
			result = vlpParameters.getParametersForAttribute(attributeId);
		}

		return result;
	}

	private notifyLayoutChanged(layoutURL: string) {
		Logger.instance.debug('Layout changed on %o to %o', this, layoutURL);
		for (let listener of this.listeners) {
			if (listener.afterLayoutChange) {
				listener.afterLayoutChange(this, layoutURL);
			}
		}
	}

	private notifyValidationChanged() {
		Logger.instance.debug('Validation changed on %o', this);
		for (let listener of this.listeners) {
			if (listener.afterValidationChange) {
				listener.afterValidationChange(this);
			}
		}
	}

	private updateValidationStatus() {
		if (this.validatingAttributes) {
			return;
		}

		Logger.instance.debug('Updating validation status...');
		this.validatingAttributes = true;

		this.getMeta().pipe(take(1)).subscribe(
			meta => {
				// Using setTimeout here because another Angular change detection run must be triggered
				// after the validation status is updated.
				setTimeout(() => {
					let attributes = meta.getAttributes();
					attributes.forEach(attributeMeta => {
						this.updateValidationStatusForAttributeMeta(attributeMeta);
					});
					this.notifyValidationChanged();
					this.validatingAttributes = false;
				});
			},
			() => (this.validatingAttributes = false)
		);
	}

	private updateValidationStatusForAttributeName(attributeName: string) {
		this.getAttributeMeta(attributeName).pipe(take(1)).subscribe(meta => {
			this.updateValidationStatusForAttributeMeta(meta);
		});
	}

	private updateValidationStatusForAttributeMeta(attributeMeta: EntityAttrMeta) {
		if (attributeMeta === undefined) {
			Logger.instance.warn('Could not update validation status for undefined meta data');
			return;
		}

		let attributeId = attributeMeta.getAttributeID();
		let attributeName = FqnService.getShortAttributeNameFailsafe(
			this.getEntityClassId(),
			attributeId
		);
		if (attributeId === undefined) {
			Logger.instance.warn('Attribute has no name: %o', attributeMeta);
		} else {
			let value = this.getAttribute(attributeId);
			let status = this.getService().getValidationStatus(attributeMeta, value);
			this.attributeValidation.set(attributeName, status);
		}
	}

	private isInternalAttribute(attributeName: string) {
		return attributeName.startsWith('nuclos');
	}
}

/**
 * Represents an EO which is used in a subform and has a
 * reference attribute to the main (selected) EO.
 */
export class SubEntityObject extends EntityObject implements ISubEntityObject {
	private selected = false;

	private complete = false;

	constructor(
		private parentEo: EntityObject,
		private referenceAttributeId: string,
		eoData: EntityObjectData
	) {
		super(eoData);
	}

	/**
	 * Returns a fake EntityObject which works on this SubEntityObject,
	 * but uses (almost) all methods of the EntityObject class.
	 *
	 * This is necessary e.g. when a SubEntityObject is opened in a detail modal where
	 * it must behave like a normal EntityObject.
	 *
	 * @returns {any}
	 */
	toEntityObject(): EntityObject {
		let fakeEo: any = {};

		this.clearLovEntries();

		let methodsToKeep = [
			'setDirty',
			'_setDirty',
			'getAttributeRestriction'
			// 'getVlpParameters',
			// 'getRootEo'
		];

		/* tslint:disable:forin */
		for (let x in <any>this) {
			let method = this[x];
			if (typeof method === 'function') {
				let replaceMethod = EntityObject.prototype[x];
				if (methodsToKeep.indexOf(x) >= 0) {
					replaceMethod = method;
				}
				fakeEo[x] = (...args) => {
					let result = replaceMethod.apply(fakeEo, args);
					return result;
				};
			} else {
				// Getter/Setter for properties
				Object.defineProperty(fakeEo, x, {
					get: () => {
						// console.log('get %o => %o', x, this[x]);
						return this[x];
					},
					set: value => {
						// console.log('set %o => %o', x, value);
						this[x] = value;
					}
				});
			}
		}

		return fakeEo;
	}

	getParent(): IEntityObject {
		return this.parentEo;
	}

	getReferenceAttributeId() {
		return this.referenceAttributeId;
	}

	/**
	 * Clones this Sub-EO.
	 * See {@link EntityObject#clone()}
	 *
	 * @returns {SubEntityObject}
	 */
	clone(): SubEntityObject {
		let result = new SubEntityObject(
			this.parentEo,
			this.referenceAttributeId,
			this.cloneData()
		);

		result.clearAutonumbers();

		return result;
	}

	setSelected(value: boolean) {
		this.selected = value;
	}

	isSelected() {
		return this.selected;
	}

	setComplete(value: boolean) {
		this.complete = value;
	}

	isComplete() {
		return this.complete;
	}

	init(
		entityClassId: string,
		eoId: number | undefined,
		valuesMap: any // TODO: Use a real Map.
	) {
		this.getData().boMetaId = entityClassId;
		this.getData().boId = eoId;
		this.getData().attributes = new Map<string, object>();

		for (let field in valuesMap) {
			if (field !== undefined) {
				this.setAttributeUnsafe(field, valuesMap[field]);
			}
		}

		if (this.getData().boId === undefined) {
			this.getData()._flag = 'insert';
		}
	}

	getRootEo(): EntityObject {
		return this.parentEo.getRootEo();
	}

	markAsDeleted(): void {
		super.markAsDeleted();

		let siblings = this.getParent().getDependents(this.getReferenceAttributeId());
		if (siblings) {
			AutonumberService.instance.updateAutonumbers(siblings);
		}
	}

	getDetailLink() {
		let links = this.getLinks();
		return links && links.detail;
	}

	canWrite() {
		return this.getSubEoRestriction(this.referenceAttributeId) !== 'readonly';
	}

	protected setAttributeValidated(attributeName: string, value: any): boolean {
		let changed = super.setAttributeValidated(attributeName, value);

		this.getAttributeMeta(attributeName).pipe(take(1)).subscribe(attributeMeta => {
			if (attributeMeta && attributeMeta.isAutonumber()) {
				let dependents = this.parentEo.getDependentsAndLoad(this.getReferenceAttributeId());
				if (dependents) {
					AutonumberService.instance.updateAutonumbersForAttribute(
						dependents,
						attributeMeta
					);
				}
			}
		});

		return changed;
	}

	/**
	 * @param dirty
	 * @returns {EntityObject}
	 * @deprecated USE ONLY IF ABSOLUTELY NECESSARY!
	 */
	protected _setDirty(dirty: boolean): EntityObject {
		if (dirty && this.parentEo) {
			this.parentEo.setDirty(true);
		}

		return super._setDirty(dirty);
	}

	protected getAttributeRestriction(attributeName: string) {
		let restriction; // TODO: Subform-restrictions?

		// Readonly restriction for reference attribute
		if (!restriction) {
			let referenceAttributeName = FqnService.getShortAttributeName(
				'',
				this.referenceAttributeId
			);
			if (attributeName === referenceAttributeName) {
				restriction = 'readonly';
			}
		}

		return restriction;
	}

	private clearAutonumbers() {
		this.getMeta().pipe(take(1)).subscribe(meta => {
			meta.getAttributes().forEach(attributeMeta => {
				if (attributeMeta.isAutonumber()) {
					this.setAttributeUnsafe(attributeMeta.getAttributeID(), undefined);
				}
			});
		});
	}

	// TODO: When the parent is deleted, its dependents should be deleted, too.
	// isMarkedAsDeleted(): boolean {
	// 	return super.isMarkedAsDeleted() || this.parentEo.isMarkedAsDeleted();
	// }
}
