import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of as observableOf } from 'rxjs';

import { catchError, tap } from 'rxjs/operators';
import { Logger } from '../../log/shared/logger';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { NuclosHttpService } from '../../shared/nuclos-http.service';
import { EntityAttrMeta, LovEntry } from './bo-view.model';
import { EntityObject, SubEntityObject } from './entity-object.class';
import { LovSearchConfig } from './lov-search-config';
import { MetaService } from './meta.service';

@Injectable()
export class LovDataService {
	constructor(
		private nuclosConfig: NuclosConfigService,
		private http: NuclosHttpService,
		private metaService: MetaService,
		private $log: Logger
	) {}

	getLovParams(search: LovSearchConfig): HttpParams {
		let params = new HttpParams();

		if (search.vlp) {
			let referenceField = search.attributeMeta.getAttributeID();
			params = params.set('reffield', referenceField);
			params = this.addVlpParams(params, search);
		}

		if (search.mandatorId) {
			params = params.set('mandatorId', search.mandatorId);
		}

		if (search.quickSearchInput) {
			params = params.set('quickSearchInput', search.quickSearchInput);
		}

		if (search.resultLimit && search.resultLimit > 0) {
			params = params.set('chunkSize', '' + search.resultLimit);
		}

		return params;
	}

	loadLovEntries(search: LovSearchConfig): Observable<LovEntry[]> {
		const params = this.getLovParams(search);

		let link;
		if (search.vlp) {
			link = this.nuclosConfig.getRestHost() + '/data/vlpdata';
		} else if (search.attributeMeta.isReference()) {
			let referenceField = search.attributeMeta.getAttributeID();
			link = this.nuclosConfig.getRestHost() + '/data/referencelist/' + referenceField;
		} else {
			this.$log.error(
				'Can not load value-list for non-reference attribute without VLP: %o',
				search
			);
			return observableOf([]);
		}

		return this.http
			.get(link, {
				params: params
			})
			.pipe(
				catchError(e => {
					this.$log.warn('Could not load LoV entries: %o', e);
					return Observable.of([]);
				}),
				tap((data: LovEntry[]) => this.addEmptyOption(search.attributeMeta, data))
			);
	}

	/**
	 * @deprecated Use {@link loadLovEntriesForVlp}
	 */
	deprecatedLoadLovEntriesForVlp(
		select: { reffield: string; vlp: any },
		eoId: number | null,
		quickSearchInput?: string,
		mandatorId?
	): Observable<LovEntry[]> {
		let link = this.nuclosConfig.getRestHost() + '/data/referencelist/' + select.reffield;
		let params = new HttpParams();

		if (select.vlp) {
			link = this.nuclosConfig.getRestHost() + '/data/vlpdata';

			let vlpparams = {
				vlptype: select.vlp.type,
				vlpvalue: select.vlp.value,
				intid: eoId,
				mandatorId: mandatorId,
				reffield: select.reffield
			};

			for (let i in select.vlp.params) {
				if (select.vlp.params.hasOwnProperty(i)) {
					vlpparams[i] = select.vlp.params[i];
				}
			}

			for (let key in vlpparams) {
				if (vlpparams[key] !== undefined && vlpparams[key] !== null) {
					params = params.set(key, vlpparams[key]);
				}
			}
		}

		if (mandatorId) {
			params = params.set('mandatorId', mandatorId);
		}

		if (quickSearchInput) {
			params = params.set('quickSearchInput', quickSearchInput);
		}

		return this.http.get<LovEntry[]>(link, {
			params: params
		});
	}

	// NUCLOS-6499: Note that actually there has to be a check done, if there is a layout for this particular eo.
	// For the sake of simplicity we suppose that an eo which has explicit read rights, has a layout, too

	canOpenReference(attributeMeta: EntityAttrMeta): Observable<boolean> {
		return new Observable<boolean>(observer => {
			this.metaService
				.canReadEo(attributeMeta.getReferencedEntityClassId())
				.subscribe(canCreateReferencedEo => {
					observer.next(canCreateReferencedEo);
				});
		});
	}

	/**
	 * the user is allowed to create a new EO and select it as reference
	 * if the referenced EO can be created and the referencing EO is writable
	 */
	canAddReference(eo: EntityObject, attributeMeta: EntityAttrMeta): Observable<boolean> {
		return new Observable<boolean>(observer => {
			let result = eo instanceof SubEntityObject || eo.canWrite(); // for main eo check 'canWrite' flag
			if (!result) {
				observer.next(false);
				return;
			}
			this.metaService
				.canCreateEo(attributeMeta.getReferencedEntityClassId())
				.subscribe(canCreateReferencedEo => {
					observer.next(canCreateReferencedEo);
				});
		});
	}

	private addEmptyOption(entityAttributeMeta: EntityAttrMeta, data) {
		if (entityAttributeMeta.isNullable()) {
			data.unshift(LovEntry.EMPTY);
		}
	}

	private addVlpParams(params: HttpParams, search: LovSearchConfig) {
		let vlp = search.vlp;

		if (!vlp) {
			return params;
		}

		let vlpparams = {
			vlptype: vlp.type,
			vlpvalue: vlp.value,
			mandatorId: search.mandatorId,
			reffield: search.attributeMeta.getAttributeID()
		};

		if (search.searchmode) {
			vlpparams['searchmode'] = search.searchmode;
		}

		if (search.eo) {
			vlpparams['intid'] = search.eo.getRootEo().getId();
		}

		if (vlp.parameter) {
			for (let param of vlp.parameter) {
				// Special handling for intid, because it is no normal attribute:
				// Use intid from VLP-params only if explicitly specified (i.e. not an empty string),
				// else the intid of the current EO (main record) will be passed. See NUCLOS-7470
				if (param.name === 'intid' && !param.value) {
					continue;
				}

				if (param.value === null || param.value === undefined) {
					continue;
				}

				vlpparams[param.name] = param.value;
			}
		}

		if (search.eo) {
			let vlpParametersForAttribute = search.eo.getVlpParameters(
				search.attributeMeta.getAttributeID()
			);

			this.$log.debug('VLP parameters: %o', vlpParametersForAttribute);

			if (vlpParametersForAttribute) {
				vlpParametersForAttribute.forEach((value, key) => {
					vlpparams[key] = value.getValue();
				});
			}
		}

		// should always be set for a vlp
		vlpparams['searchmode'] = vlpparams['searchmode'] === true ? 'true' : 'false';

		/**
		 * TODO: VLP params should not be simply added as URL params,
		 * 	but be somehow prefixed ore escaped to avoid collisions.
		 */
		for (let key in vlpparams) {
			if (vlpparams[key] !== undefined && vlpparams[key] !== null) {
				let value = vlpparams[key];
				if (value && value.hasOwnProperty('id')) {
					value = value.id;
				}
				params = params.set(key, value);
			} else {
				params = params.set(key, '');
			}
		}

		return params;
	}
}
