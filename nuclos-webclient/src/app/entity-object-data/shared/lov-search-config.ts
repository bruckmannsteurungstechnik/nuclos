import { EntityAttrMeta, EntityMeta } from './bo-view.model';
import { EntityObject } from './entity-object.class';

export interface LovSearchConfig {
	eo?: EntityObject;
	entityMeta?: EntityMeta;
	attributeMeta: EntityAttrMeta;

	resultLimit: number;

	vlp?: WebValuelistProvider;
	mandatorId?: string;
	quickSearchInput?: string;
	searchmode?: boolean;
}
