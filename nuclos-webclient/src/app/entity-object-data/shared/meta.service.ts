import { Injectable } from '@angular/core';
import { Logger } from '@nuclos/nuclos-addon-api';
import { NOT_FOUND } from 'http-status-codes';
import { EMPTY, forkJoin, Observable, Observer, throwError } from 'rxjs';
import { catchError, take, tap } from 'rxjs/operators';
import { HttpMethod } from '../../shared/link.model';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { NuclosHttpService } from '../../shared/nuclos-http.service';
import { EntityMeta } from './bo-view.model';

@Injectable()
export class MetaService {
	static instance: MetaService;

	constructor(
		private http: NuclosHttpService,
		private nuclosConfig: NuclosConfigService,
		private $log: Logger,
	) {
		MetaService.instance = this;
	}

	getBoMeta(boMetaId: string): Observable<EntityMeta> {
		return this.http.getCachedJSON(
			this.nuclosConfig.getRestHost() + '/boMetas/' + boMetaId,
			json => new EntityMeta(json)
		).pipe(
			catchError(e => {
				this.$log.warn('Could not load meta data for entity %o: %o', boMetaId, e);

				// Error 404 will be handled globally
				if (e.status && e.status === NOT_FOUND) {
					return throwError(e);
				}

				return EMPTY;
			})
		);
	}

	getEntityMeta(entityClassId: string): Observable<EntityMeta> {
		return this.getBoMeta(entityClassId);
	}

	getEntityMetasByContexts(entityContexts: EntityContext[]): Observable<Map<string, EntityMeta>> {
		let entityClassIds: Set<string> = new Set<string>();
		entityContexts.forEach(c => entityClassIds.add(c.dependentEntityClassId));
		return this.getEntityMetas(entityClassIds);
	}

	getEntityMetas(entityClassIds: Set<string>): Observable<Map<string, EntityMeta>> {
		let ids = Array.from(entityClassIds.values());
		return new Observable<Map<string, EntityMeta>>((observer: Observer<Map<string, EntityMeta>>) => {
			let mapMeta: Map<string, EntityMeta> = new Map();
			if (ids.length === 0) {
				observer.next(mapMeta);
				observer.complete();
			} else {
				forkJoin(ids.map(id => this.getEntityMeta(id)))
					.subscribe(results => {
						results.forEach((meta, index) => mapMeta.set(ids[index], meta));
						observer.next(mapMeta);
						observer.complete();
					});
			}
		});
	}

	getSubBoMeta(href: string): Observable<EntityMeta> {
		return this.http.getCachedJSON(
			href,
			json => new EntityMeta(json)
		);
	}

	getSubBoMetaByReferenceAttribute(entityClassId: string, refAttribute: string): Observable<EntityMeta> {
		return this.getSubBoMeta(this.nuclosConfig.getRestHost() + '/boMetas/' + entityClassId + '/subBos/' + refAttribute);
	}


	// Rest-Service actually gives away information "canCreateEo" in another way, e.g:
	// /rest/bos/example_rest_Auftrag
	// { "all": true,  "bos": [], "canCreateBo": true }

	canCreateEo(entityClassId: string): Observable<boolean> {
		return this.checkEntityForHttpMethod(entityClassId, HttpMethod.POST);
	}

	canReadEo(entityClassId: string): Observable<boolean> {
		return this.checkEntityForHttpMethod(entityClassId, HttpMethod.GET);
	}

	checkEntityForHttpMethod(entityClassId: string, httpMethod: HttpMethod): Observable<boolean> {
		return new Observable<boolean>(observer => {
			this.http.getCachedJSON(
				this.nuclosConfig.getRestHost() + '/bos',
				json => json.map(m => new EntityMeta(m))
			).pipe(
				take(1),
				tap(entityMetaArray => {
					let meta: EntityMeta = entityMetaArray.filter(
						entityMeta => entityMeta.getBoMetaId() === entityClassId).shift();
					if (meta && meta.getLinks().bos) {
						let links = meta.getLinks();
						if (links && links.bos && links.bos.methods) {
							observer.next(links.bos.methods.indexOf(httpMethod) !== -1);
						}
					}
				}),
			).subscribe();
		});
	}
}
