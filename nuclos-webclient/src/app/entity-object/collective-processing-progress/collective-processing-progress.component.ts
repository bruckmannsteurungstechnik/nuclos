import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { EntityObjectResultUpdateService } from '../../entity-object-data/shared/entity-object-result-update.service';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { Logger } from '../../log/shared/logger';
import { GridOptions } from 'ag-grid';
import { ISubscription } from 'rxjs/Subscription';
import { BooleanRendererComponent } from 'app/grid/grid/cell-renderer';

@Component({
	selector: 'nuc-collective-processing-progress',
	templateUrl: 'collective-processing-progress.component.html',
	styleUrls: ['collective-processing-progress.component.scss']
})
export class CollectiveProcessingProgressComponent implements OnInit, OnDestroy {

	gridOptions: GridOptions = <GridOptions>{};

	@Output() progressNumber = new EventEmitter<number>();

	columnDefs = [
		{ headerName: '#', field: 'rowNumber' },
		{ headerName: this.i18n.getI18n('webclient.collectiveProcessingProgress.header.name'), field: 'object' },
		{
			headerName: this.i18n.getI18n('webclient.collectiveProcessingProgress.header.result'),
			field: 'result',
			cellRendererFramework: BooleanRendererComponent,
			editable: false
		},
		{ headerName: this.i18n.getI18n('webclient.collectiveProcessingProgress.header.resultmessage'), field: 'resultMessage' },
	];

	rowData = [{}];

	private subscription: ISubscription;

	constructor(
		protected i18n: NuclosI18nService,
		private eoResultUpdateService: EntityObjectResultUpdateService,
		private $log: Logger
	) {
	}

	ngOnInit() {
		this.subscription = this.eoResultUpdateService.subscribeCollectiveProcessingProgress().subscribe(info => {
			this.progressNumber.emit(info.percent);
			if (info.objectInfos) {
				info.objectInfos.forEach(oInfo => {
					let progressRow = {
						rowNumber: oInfo.infoRowNumber,
						object: oInfo.name,
						result: oInfo.result === 'Ok',
						resultMessage: oInfo.resultMessage
					};
					this.rowData[oInfo.infoRowNumber - 1] = progressRow;
					if (this.gridOptions && this.gridOptions.api) {
						this.gridOptions.api.setRowData(this.rowData);
					}
				});
			}
		}
		);
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}

}
