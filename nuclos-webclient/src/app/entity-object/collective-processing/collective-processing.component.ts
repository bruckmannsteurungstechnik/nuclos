import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Subject, EMPTY } from 'rxjs';
import { takeUntil, take, catchError } from 'rxjs/operators';
import { EntityObjectResultUpdateService } from '../../entity-object-data/shared/entity-object-result-update.service';
import { EntityObjectResultService } from '../../entity-object-data/shared/entity-object-result.service';
import { EntityObjectService } from '../../entity-object-data/shared/entity-object.service';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { Logger } from '../../log/shared/logger';
import { EntityObjectGridMultiSelectionResult } from '../entity-object-grid/entity-object-grid-multi-selection-result';
import { DialogService } from 'app/popup/dialog/dialog.service';
import { BusyService } from 'app/shared/busy.service';

@Component({
	selector: 'nuc-collective-processing',
	templateUrl: 'collective-processing.component.html',
	styleUrls: ['collective-processing.component.scss']
})
export class CollectiveProcessingComponent implements OnInit, OnDestroy {
	@Output() onMultiSelectionChange = new EventEmitter();

	multiSelectionResult: EntityObjectGridMultiSelectionResult;

	selectionOptions: CollectiveProcessingSelectionOptions;
	isProgressing = false;

	private unsubscribe$ = new Subject<void>();

	private abortUrl: string | undefined;


	constructor(
		protected i18n: NuclosI18nService,
		private eoService: EntityObjectService,
		private eoResultService: EntityObjectResultService,
		private eoResultUpdateService: EntityObjectResultUpdateService,
		private $log: Logger,
		private dialogService: DialogService,
		private busyService: BusyService
	) { }

	ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	ngOnInit() {
		this.setMultiSelectionResult(this.eoResultService.getMultiSelectionResult());
		this.eoResultService.observeMultiSelectionChange().pipe(takeUntil(this.unsubscribe$)).subscribe(multiSelectionResult => {
			this.setMultiSelectionResult(multiSelectionResult);
		});
	}

	executeStateChange(action: CollectiveProcessingAction) {
		this.eoResultUpdateService
			.executeCollectiveProcessing(this.multiSelectionResult, action.links.execute.href!)
			.pipe(take(1))
			.subscribe((execution: CollectiveProcessingExecution) => {
				this.abortUrl = execution.links.abort.href!;
				this.isProgressing = true;
			});
	}

	showProgress() {
		return this.eoResultService.forceCollectiveProcessingView;
	}

	closeProgress() {
		this.eoResultService.forceCollectiveProcessingView = false;
		this.backToDetails();
	}

	backToDetails() {
		this.eoResultService.showCollectiveProcessing = false;
	}

	cancelCollectiveProcessing() {
		this.multiSelectionResult.clear();
		this.multiSelectionResult.headerMultiSelectionAll = false;
		this.onMultiSelectionChange.emit(this.multiSelectionResult);
	}

	abortProgress() {
		this.eoResultUpdateService
			.abortCollectiveProcessing(this.abortUrl!)
			.pipe(
				take(1),
				catchError(e => {
					if (e.status === 404) {
						this.dialogService.alert({
							title: this.i18n.getI18n('webclient.error.404.title'),
							message: this.i18n.getI18n('webclient.error.code404')
						});
					} else {
						this.dialogService.alert({
							title: this.i18n.getI18n('webclient.error.unknown.title'),
							message: this.i18n.getI18n('webclient.error.unknown.text')
						});
					}

					return EMPTY;
				})
			)
			.subscribe(_ => {
				this.dialogService.alert({
					title: this.i18n.getI18n('webclient.collectiveProcessing.confirm.title'),
					message: this.i18n.getI18n('webclient.collectiveProcessing.confirm.message')
				});
				this.progress(100);
			});
	}

	progress(newProgress: number) {
		if (newProgress >= 100) {
			this.isProgressing = false;
		}
	}

	private setMultiSelectionResult(multiSelectionResult: EntityObjectGridMultiSelectionResult) {
		this.multiSelectionResult = multiSelectionResult;
		if (multiSelectionResult.hasSelection()) {
			this.loadSelectionOptions(multiSelectionResult);
		}
	}

	private loadSelectionOptions(multiSelectionResult: EntityObjectGridMultiSelectionResult) {
		this.busyService.busy(
			this.eoResultUpdateService.loadCollectiveProcessingSelectionOptions(multiSelectionResult)
		)
		.pipe(take(1))
		.subscribe(selOptions => {
			this.selectionOptions = selOptions;
			if (Object.keys(selOptions).length === 0 && multiSelectionResult.hasSelection()) {
				this.$log.warn('selOptions received: %o (MultiSelection: %o)', selOptions, multiSelectionResult);
				this.dialogService.alert({
					title: this.i18n.getI18n('webclient.collectiveProcessing.selOptionsNotFound.title'),
					message: this.i18n.getI18n('webclient.collectiveProcessing.selOptionsNotFound.message')
				});
			}
		});
	}
}
