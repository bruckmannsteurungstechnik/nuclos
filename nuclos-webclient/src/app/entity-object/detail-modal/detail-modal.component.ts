import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IEntityObject } from '@nuclos/nuclos-addon-api';
import { Observable, of as observableOf, from as observableFrom } from 'rxjs';
import { take } from 'rxjs/operators';
import { ClickOutsideService } from '../../click-outside/click-outside.service';
import { EntityObjectEventListener } from '../../entity-object-data/shared/entity-object-event-listener';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { DialogService } from '../../popup/dialog/dialog.service';
// import { EntityObjectResultService } from '../../entity-object-data/shared/entity-object-result.service';
// import { EntityObjectNavigationService } from '../../entity-object-data/shared/entity-object-navigation.service'

@Component({
	selector: 'nuc-detail-modal',
	templateUrl: './detail-modal.component.html',
	styleUrls: ['./detail-modal.component.css']
})
export class DetailModalComponent implements OnInit {
	@Input() independentContext = false;

	@Input() sourceEo: EntityObject | undefined;

	triggerUnsavedChangesPopover: Date;

	private _eo: EntityObject;

	private eoChangeListener: EntityObjectEventListener = {
		afterDelete: (entityObject: EntityObject) => {
			this.reloadSourceObject().pipe(take(1)).subscribe(() => this.close());
		},
		afterSave: (entityObject: EntityObject) => {
			this.reloadSourceObject().pipe(take(1)).subscribe();
		}
	};

	constructor(
		protected clickoutside: ClickOutsideService,
		// protected eoResultService: EntityObjectResultService,
		// protected eoNavigationService: EntityObjectNavigationService,
		private dialog: DialogService,
		protected activeModal: NgbActiveModal,
		protected ref: ElementRef
	) {}

	ngOnInit() {}

	@Input()
	set eo(eo: EntityObject) {
		this._eo = eo;
		this._eo.addListener(this.eoChangeListener);
		if (this.independentContext) {
			let error = eo.getError();
			if (error) {
				this.dialog.alert({
					title: 'Error',
					message: error
				});
			}
		}
	}

	get eo(): EntityObject {
		return this._eo;
	}

	cancel() {
		this._eo.reset();
		this.close(false);
	}

	close(closeResult = true) {
		if (this.independentContext && this._eo.isDirty()) {
			if (!this._eo.isNew()) {
				this.warnAboutUnsavedChanges();
				return;
			}
		}
		// To notify open subform cell editors etc.
		this.clickoutside.outsideClick(this.ref.nativeElement);

		this.activeModal.close(closeResult);
	}

	private warnAboutUnsavedChanges() {
		this.triggerUnsavedChangesPopover = new Date();
	}

	private reloadSourceObject(): Observable<IEntityObject | undefined> {
		if (this.independentContext && this.sourceEo) {
			return observableFrom(this.sourceEo.reload());
		}
		return observableOf(undefined);
	}
}
