/*
import { Input } from '@angular/core';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { DetailModalComponent } from './detail-modal.component';

export class GenerationDetailModalComponent extends DetailModalComponent {

	@Input() generationSourceEo: EntityObject;

	cancel() {
		this.eo.reset();
		// this.close(false);
		this.activeModal.close(false);
	}


	close(closeResult = true) {

		this.eo.save().subscribe(savedEo => {

			// To notify open subform cell editors etc.
			this.clickoutside.outsideClick(this.ref.nativeElement);

			this.activeModal.close(closeResult);

			// refresh sidebar
			let entityClassId = this.eoResultService.getSelectedEntityClassId();
			if (entityClassId) {
				this.eoNavigationService.navigateToView(entityClassId).then(() => {

					// navigate to generated eo if it is of the same entity class as source eo
					if (
						savedEo instanceof EntityObject &&
						this.generationSourceEo &&
						this.generationSourceEo.getEntityClassId() === savedEo.getEntityClassId()
					) {
						this.eoResultService.selectEo(savedEo);
					}
				});
			}

		});
	}
}
*/
