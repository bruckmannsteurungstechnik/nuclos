import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DatetimeService } from '../../shared/datetime.service';
import { EntityObjectEventService } from '../../entity-object-data/shared/entity-object-event.service';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { EntityObjectEventListener } from '../../entity-object-data/shared/entity-object-event-listener';

@Component({
	selector: 'nuc-detail-statusbar',
	templateUrl: './detail-statusbar.component.html',
	styleUrls: ['./detail-statusbar.component.scss']
})
export class DetailStatusbarComponent implements OnInit, OnDestroy {
	createdByString = '';
	createdAtString = '';

	changedByString = '';
	changedAtString = '';

	private datePattern: string;

	private eoListener: EntityObjectEventListener;

	private removeEoListeners: Function[] = [];

	private unsubscribe$ = new Subject<void>();

	constructor(
		private entityObjectEventService: EntityObjectEventService,
		private datetimeService: DatetimeService,
		private nuclosI18n: NuclosI18nService
	) {
		this.datePattern = this.nuclosI18n.getCurrentLocale().dateTimeWithSecondsPattern;
	}

	ngOnInit() {
		this.eoListener = {
			afterSave: eo => this.updateView(eo)
		};
		this.entityObjectEventService.observeSelectedEo().pipe(takeUntil(this.unsubscribe$)).subscribe(eo => {
			if (eo) {
				this.updateView(eo);
				(<EntityObject>eo).addListener(this.eoListener);
				this.removeEoListeners.push(() => {
					(<EntityObject>eo).removeListener(this.eoListener);
				});
			}
		});
	}

	ngOnDestroy(): void {
		for (let removeEoListener of this.removeEoListeners) {
			removeEoListener();
		}

		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	private updateView(eo) {
		let createdBy = eo.getCreatedBy();
		this.createdByString = createdBy ? createdBy : '';

		let createdAt = eo.getCreatedAt();
		this.createdAtString = createdAt
			? this.datetimeService.formatTimestamp(createdAt, this.datePattern)
			: '';

		let changedBy = eo.getChangedBy();
		this.changedByString = changedBy ? changedBy : '';

		let changedAt = eo.getChangedAt();
		this.changedAtString = changedAt
			? this.datetimeService.formatTimestamp(changedAt, this.datePattern)
			: '';
	}
}
