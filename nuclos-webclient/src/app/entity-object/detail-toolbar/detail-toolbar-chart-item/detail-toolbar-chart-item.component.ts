import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { take } from 'rxjs/operators';
import { AuthenticationService } from '../../../authentication';
import { ChartService } from '../../../chart/shared/chart.service';
import { EntityObject } from '../../../entity-object-data/shared/entity-object.class';
import { UserAction } from '@nuclos/nuclos-addon-api';

@Component({
	selector: 'nuc-detail-toolbar-chart-item',
	templateUrl: './detail-toolbar-chart-item.component.html',
	styleUrls: ['./detail-toolbar-chart-item.component.css']
})
export class DetailToolbarChartItemComponent implements OnInit, OnChanges {
	@Input() eo: EntityObject;

	visible = false;

	constructor(
		private authService: AuthenticationService,
		private chartService: ChartService
	) {
	}

	ngOnInit() {
		this.updateVisibility();
	}

	ngOnChanges(changes: SimpleChanges): void {
		this.updateVisibility();
	}

	private updateVisibility() {
		if (this.eo) {
			this.chartService.getCharts(this.eo).pipe(take(1)).subscribe(charts => {
				// Show the chart button, if charts are configured for this user
				if (charts.length > 0) {
					this.visible = true;
					return;
				}

				// Else show the chart button only if this user can configure new charts
				if (this.eo && this.eo.hasSubEos()) {
					let chartsAllowed = this.authService.isActionAllowed(UserAction.ConfigureCharts);
					this.visible = chartsAllowed;
				}
			});
		}
	}
}
