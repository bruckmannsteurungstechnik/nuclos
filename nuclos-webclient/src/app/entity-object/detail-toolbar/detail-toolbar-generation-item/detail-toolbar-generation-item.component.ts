import { Component, Input, OnInit } from '@angular/core';
import { EntityObject } from '../../../entity-object-data/shared/entity-object.class';

@Component({
	selector: 'nuc-detail-toolbar-generation-item',
	templateUrl: './detail-toolbar-generation-item.component.html',
	styleUrls: ['./detail-toolbar-generation-item.component.css']
})
export class DetailToolbarGenerationItemComponent implements OnInit {
	@Input() eo: EntityObject;

	constructor() {
	}

	ngOnInit() {
	}

	isVisible(): boolean {
		if (!this.eo || this.eo.isNew()) {
			return false;
		}

		return this.eo.getGenerationsForUser().length > 0;
	}
}
