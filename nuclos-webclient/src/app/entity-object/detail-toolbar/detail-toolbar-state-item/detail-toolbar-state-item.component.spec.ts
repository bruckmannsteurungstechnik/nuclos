import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailToolbarStateItemComponent } from './detail-toolbar-state-item.component';

xdescribe('DetailToolbarStateItemComponent', () => {
	let component: DetailToolbarStateItemComponent;
	let fixture: ComponentFixture<DetailToolbarStateItemComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [DetailToolbarStateItemComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(DetailToolbarStateItemComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
