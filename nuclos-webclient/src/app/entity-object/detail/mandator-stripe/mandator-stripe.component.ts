import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { IEntityObject } from '@nuclos/nuclos-addon-api';
import { AuthenticationService } from '../../../authentication';
import { EntityObjectEventService } from '../../../entity-object-data/shared/entity-object-event.service';

/**
 * adds a colored stripe on top of the eo detail form
 * the color is defined at the assigned mandator of the eo
 */
@Component({
	selector: 'nuc-mandator-stripe',
	templateUrl: './mandator-stripe.component.html',
	styleUrls: ['./mandator-stripe.component.css']
})
export class MandatorStripeComponent implements OnInit, OnDestroy {

	mandatorColor: string | undefined;

	private unsubscribe$ = new Subject<void>();

	constructor(
		private authenticationService: AuthenticationService,
		private eoEventService: EntityObjectEventService,
	) {
	}

	ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	ngOnInit() {
		this.eoEventService.observeSelectedEo().pipe(takeUntil(this.unsubscribe$)).subscribe(eo => {
			if (eo) {
				this.mandatorColor = this.getMandatorColor(eo);
			}
		});
	}

	getMandatorColor(eo: IEntityObject): string | undefined {
		let mandatorId = eo.getMandatorId();
		let authentication = this.authenticationService.getAuthentication();
		if (authentication) {
			let mandators = authentication.mandators;
			if (mandators) {
				return mandators
					.filter(m => m.mandatorId === mandatorId)
					.map(m => m.color)
					.shift();
			}
		}
		return undefined;
	}
}
