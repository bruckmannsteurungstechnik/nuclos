import { Component } from '@angular/core';
import { AgRendererComponent } from 'ag-grid-angular';
import { EntityAttrMeta } from '../../../../entity-object-data/shared/bo-view.model';
import { NuclosCellRendererParams } from '../../../../layout/web-subform/web-subform.model';

@Component({
	selector: 'nuc-reference-renderer',
	templateUrl: 'reference-renderer.component.html',
	styleUrls: ['reference-renderer.component.scss']
})
export class ReferenceRendererComponent implements AgRendererComponent {
	attributeMeta: EntityAttrMeta;
	isEditable = false;
	value?: string;
	attribute: any;

	private params: any;
	private nuclosCellRenderParams: NuclosCellRendererParams;

	constructor() {}

	agInit(params: any) {
		this.params = params;

		this.value = params.value;
		if (params.value && params.value.hasOwnProperty('id')) {
			this.value = params.value.name;
		}

		// Replace null with undefined - or else we see "null" in the grid
		if (this.value === null) {
			this.value = undefined;
		}

		this.nuclosCellRenderParams = params.nuclosCellRenderParams as NuclosCellRendererParams;
		this.attributeMeta = this.nuclosCellRenderParams.attrMeta;
		this.isEditable = this.nuclosCellRenderParams.editable;
	}

	refresh(params: any): boolean {
		return false;
	}
}
