import { Input } from '@angular/core';

export class EntityObjectGridMultiSelectionResult {

	headerMultiSelectionAll = false;
	selectedIds = new Set();
	unselectedIds = new Set();

	clear(): void {
		this.selectedIds.clear();
		this.unselectedIds.clear();
	}

	hasIds(): boolean {
		return this.selectedIds.size > 0 || this.unselectedIds.size > 0;
	}

	countIds(): number {
		return this.selectedIds.size + this.unselectedIds.size;
	}

	hasSelection(): boolean {
		return this.hasIds() || this.headerMultiSelectionAll;
	}

	toRestMultiSelectionResult(): MultiSelectionResult {
		let result: MultiSelectionResult = {
			allSelected: this.headerMultiSelectionAll,
			selectedIds: this.transformSetToObjectIdList(this.selectedIds),
			unselectedIds: this.transformSetToObjectIdList(this.unselectedIds)
		};

		return result;
	}

	private transformSetToObjectIdList(idSet: Set<any>): ObjectId[] {
		let result: ObjectId[] = [];
		idSet.forEach(id => {
			let oId: ObjectId = {
				long: typeof id === 'number' ? id : undefined,
				string: typeof id === 'string' ?  id : undefined
			};
			result.push(oId);
		});
		return result;
	}

}
