import {
	Component,
	EventEmitter,
	Input,
	OnChanges,
	OnInit,
	OnDestroy,
	Output,
	SimpleChanges
} from '@angular/core';
import { ColDef, GridOptions } from 'ag-grid';
import { Observable, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { SortModel } from '../../entity-object-data/shared/sort.model';
import {
	BooleanRendererComponent,
	DateRendererComponent,
	NumberRendererComponent,
	StateIconRendererComponent
} from '../../grid/grid/cell-renderer';
import {
	NuclosCellEditorParams,
	NuclosCellRendererParams
} from '../../layout/web-subform/web-subform.model';
import { Logger } from '../../log/shared/logger';
import { SidebarViewItem } from '../sidebar/view/sidebar-view.model';
import { ReferenceRendererComponent } from './cell-renderer';
import { EntityObjectGridColumn } from './entity-object-grid-column';
import { EntityObjectGridMultiSelectionResult } from './entity-object-grid-multi-selection-result';
import { EntityObjectGridService } from './entity-object-grid.service';
import { ImageRendererComponent } from '../../grid/grid/cell-renderer/image-renderer/image-renderer.component';
import { MultiSelectionHeaderComponent } from './multi-selection-header/multi-selection-header.component';
import { MultiSelectionEditorComponent } from './multi-selection-editor/multi-selection-editor.component';
import { MultiSelectionRendererComponent } from './multi-selection-renderer/multi-selection-renderer.component';
import has = Reflect.has;
import * as deepEqual from 'deep-equal';
import { EntityObjectResultService } from 'app/entity-object-data/shared/entity-object-result.service';

/**
 * A Grid component for EntityObject data that handles column preferences and NuclosRowColor.
 *
 * TODO: Init columns
 * TODO: Init sorting
 * TODO: Handle NuclosRowColor
 */
@Component({
	selector: 'nuc-entity-object-grid',
	templateUrl: './entity-object-grid.component.html',
	styleUrls: ['./entity-object-grid.component.css']
})
export class EntityObjectGridComponent implements OnInit, OnDestroy, OnChanges {
	@Input() columns: EntityObjectGridColumn[];
	@Input() sortModel: SortModel;

	// TODO: Should not be used externally
	@Input() gridOptions: GridOptions = <GridOptions>{};

	@Output() onMultiSelectionChange = new EventEmitter();
	@Output() rowSelected = new EventEmitter();
	@Output() gridReady = new EventEmitter();
	@Output() rowClicked = new EventEmitter();
	@Output() modelUpdated = new EventEmitter();

	private _showMultiSelection = false;
	private _multiSelectionDisabled = false;
	private _headerMultiSelectionChangeObserver = new Subject<boolean>();
	private _rowMultiSelectionChangeObserver = new Subject();
	private _multiSelectionResult = new EntityObjectGridMultiSelectionResult();

	private unsubscribe$ = new Subject<void>();

	constructor(
		private eoGridService: EntityObjectGridService,
		private $log: Logger,
		private eoResultService: EntityObjectResultService) {}

	ngOnInit() {
		this.initGrid();
	}

	ngOnChanges(changes: SimpleChanges): void {
		this.$log.debug('changes (sortModel) = %o', changes);

		if (changes['sortModel']) {
			if (!deepEqual(changes['sortModel'].currentValue, changes['sortModel'].previousValue)) {
				this.applySortModel();
			}
		}

		if (changes['columns']) {
			if (!deepEqual(changes['columns'].currentValue, changes['columns'].previousValue)) {
				this.applyColumns();
			}
		}
	}

	ngOnDestroy(): void {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	handleModelUpdated(value?: any) {
		this.modelUpdated.emit();
		this.resetMultiSelection();
	}

	private applySortModel() {
		if (!this.sortModel) {
			return;
		}

		if (this.gridOptions.api) {
			this.gridOptions.api.setSortModel(this.sortModel.getColumns());
			this.$log.debug('SortModel changed: %o', this.sortModel);
		} else {
			this.gridReady.pipe(take(1)).subscribe(() => this.applySortModel());
		}
	}

	private applyColumns() {
		if (!this.gridOptions.api || !this.columns) {
			return;
		}

		this.columns.forEach(column => {
			let attributeMeta = column.attributeMeta;
			if (attributeMeta) {
				column.headerClass = this.eoGridService.getTextAlignmentClass(attributeMeta);
				column.cellClass = this.eoGridService.getTextAlignmentClass(attributeMeta);

				// suppress sorting for images (NULCOS-8092)
				column.suppressSorting = attributeMeta.isImage() || attributeMeta.isStateIcon();

				// provide data for render component
				column.cellRendererParams = {
					nuclosCellRenderParams: {
						editable: false,
						attrMeta: attributeMeta
					} as NuclosCellRendererParams
				};

				// provide data for editor component
				column.cellEditorParams = {
					attrMeta: attributeMeta
				} as NuclosCellEditorParams;

				if (attributeMeta.isBoolean()) {
					column.cellRendererFramework = BooleanRendererComponent;
				} else if (attributeMeta.isDate() || attributeMeta.isTimestamp()) {
					column.cellRendererFramework = DateRendererComponent;
				} else if (attributeMeta.isNumber()) {
					column.cellRendererFramework = NumberRendererComponent;
				} else if (attributeMeta.isStateIcon()) {
					column.cellRendererFramework = StateIconRendererComponent;
				} else if (attributeMeta.isReference()) {
					column.cellRendererFramework = ReferenceRendererComponent;
				} else if (attributeMeta.isImage()) {
					column.cellRendererFramework = ImageRendererComponent;
				}
			}
		});

		let gridColumns: ColDef[] = this.columns;
		if (this.showMultiSelection === true) {
			gridColumns.unshift(this.createMultiSelectionColumn());
		}
		this.gridOptions.api.setColumnDefs(gridColumns);

		this.applySortModel();
	}

	private createMultiSelectionColumn(): ColDef {
		let result = {
			width: 20,
			pinned: true,
			lockPosition: true,
			lockPinned: true,
			suppressFilter: true,
			suppressMovable: true,
			suppressNavigable: true,
			suppressResize: true,
			suppressSorting: true,
			field: 'selected',
			headerName: '',
			headerComponentFramework: MultiSelectionHeaderComponent,
			editable: true,
			cellRendererFramework: MultiSelectionRendererComponent,
			cellEditorFramework: MultiSelectionEditorComponent
		} as ColDef;
		return result;
	}

	private initGrid() {
		this._headerMultiSelectionChangeObserver.pipe(takeUntil(this.unsubscribe$)).subscribe(value => {
			this.setHeaderMultiSelectionAll(value);
		});
		this._rowMultiSelectionChangeObserver.pipe(takeUntil(this.unsubscribe$)).subscribe(rowNode => {
			this.rowMultiSelectionChanged(rowNode);
		});
		this.gridOptions.context = {
			headerMultiSelectionChangeObserver: this._headerMultiSelectionChangeObserver,
			rowMultiSelectionChangeObserver: this._rowMultiSelectionChangeObserver,
			multiSelectionDisabled: this.multiSelectionDisabled
		};

		this.gridOptions.enableColResize = true;
		this.gridOptions.enableSorting = true;
		this.gridOptions.cacheBlockSize = 100;

		this.gridOptions.columnDefs = [{}];

		this.gridOptions.getRowNodeId = item => {
			return item.getId();
		};

		this.gridOptions.rowSelection = 'single';

		// NuclosRowColor
		this.gridOptions.getRowClass = (params: { data: SidebarViewItem }) => {
			if (params.data) {
				return params.data.rowclass || '';
			}
			return '';
		};
		this.gridOptions.getRowStyle = (params: { data: SidebarViewItem }) => {
			if (params.data) {
				let rowColor = params.data.getRowColor();
				let textColor = params.data.getTextColor();
				let result = {};
				if (rowColor !== undefined) {
					result['background-color'] = rowColor;
				}
				if (textColor !== undefined) {
					result['color'] = textColor;
				}
				return result;
			}
			return undefined;
		};

		this.eoResultService.observeMultiSelectionChange().pipe(takeUntil(this.unsubscribe$)).subscribe(multiSelectionResult => {
			// update selection
			this.updateMultiSelection(multiSelectionResult);
		});

		this.gridOptions.onGridReady = () => {
			this.applyColumns();
		};
	}

	get multiSelectionDisabled(): boolean {
		return this._multiSelectionDisabled;
	}

	@Input()
	set multiSelectionDisabled(multiSelectionDisabled: boolean) {
		this._multiSelectionDisabled = multiSelectionDisabled;
		if (this.gridOptions && this.gridOptions.context) {
			this.gridOptions.context.multiSelectionDisabled = multiSelectionDisabled;
		}
	}

	get showMultiSelection(): boolean {
		return this._showMultiSelection;
	}

	@Input()
	set showMultiSelection(showMultiselection: boolean) {
		if (this._showMultiSelection !== showMultiselection) {
			if (this._showMultiSelection) {
				this.columns.splice(0, 1);
			}
			this._showMultiSelection = showMultiselection;
			this.applyColumns();
			this.resetMultiSelection();
		}
	}

	private setHeaderMultiSelectionAll(headerMultiSelectionAll: boolean) {
		this._multiSelectionResult.headerMultiSelectionAll = headerMultiSelectionAll;
		this.resetMultiSelectionIds();
		this.onMultiSelectionChange.emit(this._multiSelectionResult);
	}

	private rowMultiSelectionChanged(rowNode: any) {
		let eoId = rowNode.data.getId();
		if (eoId) {
			if (this._multiSelectionResult.headerMultiSelectionAll) {
				if (rowNode.data.unselected !== true) {
					this._multiSelectionResult.unselectedIds.delete(eoId);
				} else if (rowNode.data.unselected === true) {
					this._multiSelectionResult.unselectedIds.add(eoId);
				}
			} else {
				if (rowNode.data.selected !== true) {
					this._multiSelectionResult.selectedIds.delete(eoId);
				} else if (rowNode.data.selected === true) {
					this._multiSelectionResult.selectedIds.add(eoId);
				}
			}
			this.onMultiSelectionChange.emit(this._multiSelectionResult);
		}
	}

	private updateMultiSelection(multiSelectionResult: EntityObjectGridMultiSelectionResult) {
		this._multiSelectionResult = multiSelectionResult;
		this.gridOptions.context.headerMultiSelectionAll = multiSelectionResult.headerMultiSelectionAll;
		if (this.gridOptions.api) {
			this.gridOptions.api.forEachNode(function (rowNode) {
				let eoId = rowNode.data.getId();
				rowNode.data.selected = multiSelectionResult.selectedIds.has(eoId);
				rowNode.data.unselected = multiSelectionResult.unselectedIds.has(eoId);
			});
			this.gridOptions.api.refreshCells();
		}
	}

	private resetMultiSelectionIds() {
		if (this._multiSelectionResult.hasIds()) {
			this._multiSelectionResult.clear();
			if (this.gridOptions.api) {
				this.gridOptions.api.forEachNode(function(rowNode) {
					rowNode.data.selected = undefined;
					rowNode.data.unselected = undefined;
				});
				this.gridOptions.api.refreshCells();
			}
		}
	}

	private resetMultiSelection() {
		let selectionChanged = this._multiSelectionResult.hasSelection();
		this._multiSelectionResult.headerMultiSelectionAll = false;
		this.gridOptions.context.headerMultiSelectionAll = this._multiSelectionResult.headerMultiSelectionAll;
		this.resetMultiSelectionIds();
		if (selectionChanged) {
			// ExpressionChangedAfterItHasBeenCheckedErrors will occur without timeout
			setTimeout(() => this.onMultiSelectionChange.emit(this._multiSelectionResult));
		}
	}
}
