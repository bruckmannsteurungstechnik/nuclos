import { Component, Input, Output } from '@angular/core';
import { AgRendererComponent } from 'ag-grid-angular';
import { Subject } from 'rxjs';
import { ICellRendererParams } from 'ag-grid';

@Component({
	selector: 'nuc-multi-selection-renderer',
	templateUrl: './multi-selection-renderer.component.html',
	styleUrls: ['./multi-selection-renderer.component.css']
})
export class MultiSelectionRendererComponent implements AgRendererComponent {

	protected params: ICellRendererParams;

	constructor() {
	}

	agInit(params: ICellRendererParams) {
		this.params = params;
	}

	refresh(params: ICellRendererParams): boolean {
		this.params = params;
		return false;
	}

	getValue(): any {
		if (this.params.node && this.params.node.data) {
			if (this.params.context.headerMultiSelectionAll === true) {
				return !(this.params.node.data.unselected === true);
			} else {
				return this.params.node.data.selected === true;
			}
		}
		return false;
	}

}
