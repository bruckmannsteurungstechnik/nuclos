import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { throwError as observableThrowError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { EntityObjectNavigationService } from '../entity-object-data/shared/entity-object-navigation.service';
import { EntityObjectResultService } from '../entity-object-data/shared/entity-object-result.service';
import { Logger } from '../log/shared/logger';

@Injectable()
export class EntityObjectRouteService {

	constructor(
		private eoNavigationService: EntityObjectNavigationService,
		private eoResultService: EntityObjectResultService,
		private $log: Logger,
	) {
	}

	handleParams(params: Params) {
		let eoId = params['entityObjectId'];

		let entityClassId = params['entityClassId'];
		this.eoResultService.selectEntityClassId(entityClassId);

		// Load the selected EO or reset
		if (eoId) {
			if (eoId === 'new') {
				this.createNewEO();
			} else {
				this.eoResultService.selectEoByClassAndId(entityClassId, eoId).pipe(
					catchError(e => {
						this.$log.error(e);
						return observableThrowError(e);
					})
				).subscribe();
			}
		}
	}

	private createNewEO() {
		this.eoResultService.createNew().subscribe(
			eo => {
				eo.select();
				this.eoNavigationService.navigateToEo(eo);
			}
		);
	}
}
