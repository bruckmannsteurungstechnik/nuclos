import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';
import { ResizableModule } from 'angular-resizable-element';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { PrettyJsonModule } from 'angular2-prettyjson';
import { AddonModule } from '../addons/addon.module';
import { AuthenticationModule } from '../authentication/authentication.module';
import { ChartModule } from '../chart/chart.module';
import { CommandModule } from '../command/command.module';
import { EntityObjectDataModule } from '../entity-object-data/entity-object-data.module';
import { EntityObjectResultUpdateService } from '../entity-object-data/shared/entity-object-result-update.service';
import { EntityObjectResultService } from '../entity-object-data/shared/entity-object-result.service';
import { EntityObjectService } from '../entity-object-data/shared/entity-object.service';
import { ExplorerTreesModule } from '../explorertrees/explorertrees.module';
import { ExplorerTreesService } from '../explorertrees/explorertrees.service';
import { GenerationModule } from '../generation/generation.module';
import { GridModule } from '../grid/grid.module';
import {
	BooleanRendererComponent,
	DateRendererComponent,
	NumberRendererComponent,
	StateIconRendererComponent
} from '../grid/grid/cell-renderer';
import { I18nModule } from '../i18n/i18n.module';
import { InputRequiredModule } from '../input-required/input-required.module';
import { LayoutModule } from '../layout/layout.module';
import { PerspectiveModule } from '../perspective/perspective.module';
import { PrintoutModule } from '../printout/printout.module';
import { RuleModule } from '../rule/rule.module';
import { SearchModule } from '../search/search.module';
import { StateModule } from '../state/state.module';
import { StorageModule } from '../storage/storage.module';
import { CollectiveProcessingProgressComponent } from './collective-processing-progress/collective-processing-progress.component';
import { CollectiveProcessingComponent } from './collective-processing/collective-processing.component';
import { DetailButtonsComponent } from './detail-buttons/detail-buttons.component';
import { DetailModalComponent } from './detail-modal/detail-modal.component';
import {
	DetailToolbarChartItemComponent,
	DetailToolbarGenerationItemComponent,
	DetailToolbarLockingItemComponent,
	DetailToolbarPrintoutItemComponent,
	DetailToolbarStateItemComponent
} from './detail-toolbar';
import { DetailToolbarItemComponent } from './detail-toolbar/detail-toolbar-item/detail-toolbar-item.component';
import { DetailToolbarComponent } from './detail-toolbar/detail-toolbar.component';
import { DetailComponent } from './detail/detail.component';
import { MandatorStripeComponent } from './detail/mandator-stripe/mandator-stripe.component';
import { ReferenceRendererComponent } from './entity-object-grid/cell-renderer/reference-renderer/reference-renderer.component';
import { EntityObjectGridComponent } from './entity-object-grid/entity-object-grid.component';
import { EntityObjectGridService } from './entity-object-grid/entity-object-grid.service';
import { MultiSelectionHeaderComponent } from './entity-object-grid/multi-selection-header/multi-selection-header.component';
import { MultiSelectionEditorComponent } from './entity-object-grid/multi-selection-editor/multi-selection-editor.component';
import { MultiSelectionRendererComponent } from './entity-object-grid/multi-selection-renderer/multi-selection-renderer.component';
import { EntityObjectRouteService } from './entity-object-route.service';
import { EntityObjectComponent, EntityObjectPopupComponent } from './entity-object.component';
import { EntityObjectRoutes } from './entity-object.routes';
import { LegacyRouteComponent } from './legacy-route/legacy-route.component';
import { LockingComponent } from './locking/locking.component';
import { NavigationGuard } from './navigation-guard';
import { DetailModalService } from './shared/detail-modal.service';
import { EntityObjectPreferenceService } from './shared/entity-object-preference.service';
import { SideviewmenuService } from './shared/sideviewmenu.service';
import { SideTaskListComponent } from './side-task-list/side-task-list.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ExportBoListComponent } from './sidebar/statusbar/export-bo-list/export-bo-list.component';
import {
	ExportBoListTemplateDirective,
	ExportBoListTemplateState,
	StatusbarComponent
} from './sidebar/statusbar/statusbar.component';
import { SideTreeComponent } from './sidetree/sidetree.component';
import { ViewPreferencesConfigModule } from './view-preferences-config/view-preferences-config.module';
import { DetailStatusbarComponent } from './detail-statusbar/detail-statusbar.component';
import { ImageRendererComponent } from '../grid/grid/cell-renderer/image-renderer/image-renderer.component';

export function navGuardFactory(
	eoService: EntityObjectService,
	eoResultService: EntityObjectResultService) {
	return new NavigationGuard(eoService, eoResultService);
}

@NgModule({
	imports: [
		AddonModule,
		CommonModule,
		FormsModule,

		AuthenticationModule,
		ChartModule,
		CommandModule,
		EntityObjectDataModule,
		EntityObjectRoutes,
		ExplorerTreesModule,
		GenerationModule,
		GridModule,
		I18nModule,
		InfiniteScrollModule,
		InputRequiredModule,
		LayoutModule,
		PerspectiveModule,
		PrintoutModule,
		RuleModule,
		SearchModule,
		StateModule,
		StorageModule,
		ViewPreferencesConfigModule,

		AgGridModule,
		AgGridModule.withComponents([
			ImageRendererComponent,
			StateIconRendererComponent,
			BooleanRendererComponent,
			NumberRendererComponent,
			DateRendererComponent,
			ReferenceRendererComponent,
			MultiSelectionEditorComponent,
			MultiSelectionHeaderComponent,
			MultiSelectionRendererComponent
		]),
		PrettyJsonModule,
		ResizableModule,
	],
	declarations: [
		ImageRendererComponent,
		DateRendererComponent,
		DetailButtonsComponent,
		DetailComponent,
		DetailModalComponent,
		DetailToolbarChartItemComponent,
		DetailToolbarComponent,
		DetailToolbarGenerationItemComponent,
		DetailToolbarItemComponent,
		DetailToolbarLockingItemComponent,
		DetailToolbarPrintoutItemComponent,
		DetailToolbarStateItemComponent,
		EntityObjectComponent,
		EntityObjectPopupComponent,
		ExportBoListComponent,
		ExportBoListTemplateDirective,
		LegacyRouteComponent,
		LockingComponent,
		MandatorStripeComponent,
		NumberRendererComponent,
		SidebarComponent,
		SideTreeComponent,
		StateIconRendererComponent,
		StatusbarComponent,
		SideTaskListComponent,
		EntityObjectGridComponent,
		ReferenceRendererComponent,
		DetailStatusbarComponent,
		MultiSelectionEditorComponent,
		MultiSelectionHeaderComponent,
		MultiSelectionRendererComponent,
		CollectiveProcessingComponent,
		CollectiveProcessingProgressComponent,
	],
	providers: [
		SideviewmenuService,
		ExplorerTreesService,
		ExportBoListTemplateState,
		EntityObjectGridService,
		EntityObjectRouteService,
		EntityObjectPreferenceService,
		{
			provide: NavigationGuard,
			useFactory: navGuardFactory,
			deps: [EntityObjectService, EntityObjectResultService]
		},
		DetailModalService,
	],
	exports: [
		DetailModalComponent,
		EntityObjectGridComponent,
	],
	entryComponents: [
		DetailModalComponent
	]
})
export class EntityObjectModule {
}
