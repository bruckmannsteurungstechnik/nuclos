import { Component, Input, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';
import { EntityObjectResultService } from '../../entity-object-data/shared/entity-object-result.service';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { EntityObjectService } from '../../entity-object-data/shared/entity-object.service';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';

@Component({
	selector: 'nuc-locking',
	templateUrl: 'locking.component.html',
	styleUrls: ['locking.component.css']
})
export class LockingComponent implements OnInit {
	@Input() eo: EntityObject;

	constructor(
		protected i18n: NuclosI18nService,
		private eoService: EntityObjectService,
		private eoResultService: EntityObjectResultService,
	) {
	}

	ngOnInit() {
	}

	/**
	 * Unlocks a locked eo
	 */
	unlock() {
		this.eoService.unlock(this.eo).pipe(take(1)).subscribe(
			(eo) => eo.reload().pipe(take(1)).subscribe(
				eo2 => this.eoResultService.selectEo(eo2)
			)
		);
	}
}
