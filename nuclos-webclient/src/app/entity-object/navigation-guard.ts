import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot } from '@angular/router';
import { take } from 'rxjs/operators';
import { EntityObjectResultService } from '../entity-object-data/shared/entity-object-result.service';
import { EntityObject } from '../entity-object-data/shared/entity-object.class';
import { EntityObjectService } from '../entity-object-data/shared/entity-object.service';
import { EntityObjectComponent } from './entity-object.component';

export class NavigationGuard implements CanDeactivate<EntityObjectComponent> {

	constructor(
		private eoService: EntityObjectService,
		private eoResultService: EntityObjectResultService,
	) {
	}

	canDeactivate(
		component: EntityObjectComponent,
		route: ActivatedRouteSnapshot,
		_state: RouterStateSnapshot
	): boolean {
		let selectedEO = this.eoResultService.getSelectedEo() as EntityObject;

		if (selectedEO) {
			// allow navigation if new eo was not modified
			if (selectedEO.isNew() && !selectedEO.isDirty()) {
				// TODO: This class is a guard. It should not actively interfere with EO services.
				this.eoService.delete(selectedEO).pipe(take(1)).subscribe(
					() => {
						this.eoResultService.selectEo(undefined);
					}
				);
			}
		}

		if (selectedEO && selectedEO.isDirty()) {
			component.warnAboutUnsavedChanges();

			// Throwing an error is necessary here, because we must know about it in the sidebar component - only true/false is not enough.
			throw new Error('Navigation failed - there are unsaved changes');
		}

		if (this.eoResultService.forceCollectiveProcessingView) {
			throw new Error('Navigation failed - collective processing is executing or result must be confirmed');
		}

		return true;
	}

}
