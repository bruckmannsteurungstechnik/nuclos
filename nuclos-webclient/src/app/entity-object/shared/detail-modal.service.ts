import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IEntityObject, Logger } from '@nuclos/nuclos-addon-api';
import { EMPTY, from as observableFrom, Observable } from 'rxjs';

import { catchError, finalize } from 'rxjs/operators';
import { SubEntityObject } from '../../entity-object-data/shared/entity-object.class';
import { RuleService } from '../../rule/shared/rule.service';
import { DetailModalComponent } from '../detail-modal/detail-modal.component';

@Injectable()
export class DetailModalService {

	constructor(
		private modalService: NgbModal,
		private ruleService: RuleService,
		private $log: Logger,
	) {
	}

	openEoInModal(eo: IEntityObject, sourceEo: IEntityObject | undefined = undefined, independentContext = false): Observable<any> {
		let ngbModalRef = this.modalService.open(
			DetailModalComponent,
			{size: 'lg', windowClass: 'fullsize-modal-window'}
		);

		let result = observableFrom(ngbModalRef.result).pipe(
			catchError(e => {
				this.$log.debug('Caught error in modal: %o', e);
				return EMPTY;
			})
		);

		/**
		 * A SubEntityObject must be modified to behave like a main EO if it is opened via modal.
		 */
		if (eo instanceof SubEntityObject) {
			const subEo = eo;

			// After the modal is closed, layout rules may change again
			result.pipe(finalize(() => this.ruleService.updateRuleExecutor(subEo))).subscribe();

			eo = subEo.toEntityObject();
		}

		ngbModalRef.componentInstance.independentContext = independentContext;
		ngbModalRef.componentInstance.sourceEo = sourceEo;
		ngbModalRef.componentInstance.eo = eo;

		return result;
	}
}
