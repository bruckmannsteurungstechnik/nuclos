import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of as observableOf } from 'rxjs';

import { concat, filter, map, take, tap } from 'rxjs/operators';
import { EntityMeta } from '../../entity-object-data/shared/bo-view.model';
import { EntityObjectSearchfilterService } from '../../entity-object-data/shared/entity-object-searchfilter.service';
import { SelectableService } from '../../entity-object-data/shared/selectable.service';
import { Logger } from '../../log/shared/logger';
import { PerspectiveService } from '../../perspective/shared/perspective.service';
import { SubformTablePreferenceSelection } from '../../perspective/shared/subform-table-preference-selection';
import {
	Preference,
	PreferenceType,
	SideviewmenuPreferenceContent
} from '../../preferences/preferences.model';
import { PreferencesService } from '../../preferences/preferences.service';
import { SearchfilterService } from '../../search/shared/searchfilter.service';
import { SideviewmenuService } from './sideviewmenu.service';

/**
 * holds preference data for the actually selected EO class
 * the preference data will always be loaded when navigating to another EO class
 */
@Injectable()
export class EntityObjectPreferenceService {
	selectedSideviewmenuPref$: BehaviorSubject<
		Preference<SideviewmenuPreferenceContent> | undefined
	> = new BehaviorSubject(undefined);

	private selectedSubformColumnPreferences: Map<
		string,
		BehaviorSubject<Preference<SideviewmenuPreferenceContent> | undefined>
	> = new Map();

	constructor(
		private selectableService: SelectableService,
		private sideviewmenuService: SideviewmenuService,
		private searchfilterService: SearchfilterService,
		private preferenceService: PreferencesService,
		private perspectiveService: PerspectiveService,

		// TODO: Invert dependency
		private eoSearchfilterService: EntityObjectSearchfilterService,

		private $log: Logger
	) {
		this.perspectiveService
			.observeSelectedTablePreference()
			.pipe(filter(pref => !!pref))
			.subscribe(pref => this.selectTablePreference(pref));

		this.perspectiveService
			.observeSelectedSubformTablePreferences()
			.subscribe(selection => this.selectSubformTable(selection));
	}

	getSelectedTablePreference() {
		return this.selectedSideviewmenuPref$.getValue();
	}

	loadPreferences(meta: EntityMeta, urlQuery: string | undefined): Observable<any> {
		return this.preferenceService
			.getPreferences({
				boMetaId: meta.getBoMetaId(),
				type: [PreferenceType.table.name, PreferenceType.searchtemplate.name],
				selected: true
			})
			.pipe(
				tap(preferences => {
					this.applySideviewMenuIfNoPerspective(preferences, meta);
					this.eoSearchfilterService.applySearchtemplateIfNoPerspective(
						preferences,
						meta,
						urlQuery
					);
				})
			);
	}

	getSubformColumnPreferenceSelection(
		entityClassId: string
	): BehaviorSubject<Preference<SideviewmenuPreferenceContent> | undefined> {
		let subject = this.selectedSubformColumnPreferences.get(entityClassId);

		if (!subject) {
			subject = new BehaviorSubject(undefined);
			this.selectedSubformColumnPreferences.set(entityClassId, subject);
		}

		this.$log.debug('Subject for %o: %o', entityClassId, subject);
		return subject;
	}

	loadSubformPreferences(
		subformMeta: EntityMeta,
		parentEntityUid: string,
		layoutId: string | undefined
	): Observable<Preference<SideviewmenuPreferenceContent> | undefined> {
		let subj = this.getSubformColumnPreferenceSelection(subformMeta.getBoMetaId());

		let fromPerspective = this.perspectiveService.getSelectedSubformTablePreference(
			subformMeta
		);
		let fromPreferences = this.preferenceService
			.getPreferences(
				{
					boMetaId: subformMeta.getBoMetaId(),
					type: ['subform-table'],
					layoutId: layoutId,
					orLayoutIsNull: true
				},
				false
			)
			.pipe(
				map(preferences => {
					// selected prefs at beginning of list (should be max one) - later take the first
					preferences.sort((a, b) => {
						return a.selected ? -1 : 1;
					});
					return preferences;
				}),
				filter(preferences => preferences.length > 0),
				map(preferences => preferences[0] as Preference<SideviewmenuPreferenceContent>)
			);
		let createNew = observableOf(undefined).pipe(
			map(() => {
				// no preference saved -> create a new one
				let newSubformTablePreference = this.sideviewmenuService.emptySubformPreference(
					subformMeta,
					parentEntityUid,
					true
				);
				newSubformTablePreference.selected = true;

				newSubformTablePreference.content.columns = newSubformTablePreference.content.columns.filter(
					c => c.selected
				);

				return newSubformTablePreference;
			})
		);

		return fromPerspective.pipe(
			concat(fromPreferences),
			filter(pref => !!pref),
			concat(createNew),
			take(1),
			tap(selectedSubformColumnPreference => {
				subj.next(selectedSubformColumnPreference as Preference<
					SideviewmenuPreferenceContent
				>);
			})
		);
	}

	selectTablePreference(pref: Preference<SideviewmenuPreferenceContent> | undefined) {
		this.$log.debug('Selecting sideview menu pref: %o', pref && pref.boMetaId);
		this.selectedSideviewmenuPref$.next(pref);
	}

	private applySideviewMenuIfNoPerspective(preferences, meta: EntityMeta) {
		let selectedPerspective = this.perspectiveService.getSelectedPerspective();
		if (selectedPerspective && selectedPerspective.searchTemplatePrefId) {
			this.$log.debug(
				'Perspective is overriding user-selected result table preferences, ignoring.'
			);
			return;
		}

		let sideviewmenuPreference;
		let sideviewmenuPrefs = preferences.filter(
			pref =>
				pref.type === PreferenceType.table.name ||
				pref.type === PreferenceType.subformTable.name
		) as Preference<SideviewmenuPreferenceContent>[];

		// set selected preference
		sideviewmenuPreference = sideviewmenuPrefs.filter(pref => pref.selected).shift();

		// if none is selected use the first one
		if (!sideviewmenuPreference && sideviewmenuPrefs.length > 0) {
			sideviewmenuPreference = sideviewmenuPrefs.shift();
		}

		if (!sideviewmenuPreference) {
			sideviewmenuPreference = this.sideviewmenuService.emptySideviewmenuPreference(
				meta,
				true,
				false
			);
		}

		// if pref has no selected column use default columns
		if (sideviewmenuPreference.content.columns.length === 0) {
			sideviewmenuPreference.content.columns = this.sideviewmenuService.emptySideviewmenuPreference(
				meta,
				true,
				false
			).content.columns;
		}

		// add meta data to columns
		this.selectableService.addMetaDataToColumns(sideviewmenuPreference, meta);

		this.selectTablePreference(sideviewmenuPreference);
	}

	private selectSubformTable(selection: SubformTablePreferenceSelection) {
		if (!selection) {
			return;
		}
		let subject = this.selectedSubformColumnPreferences.get(selection.entityClassId);
		if (subject) {
			this.$log.debug('Selecting subform prefs: %o', selection);
			subject.next(selection.subformTablePreference);
		}
	}
}
