import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { EntityMeta } from '../../../../entity-object-data/shared/bo-view.model';
import { DataService } from '../../../../entity-object-data/shared/data.service';
import { ExportBoListTemplateState } from '../statusbar.component';

@Component({
	selector: 'nuc-export-bo-list',
	templateUrl: './export-bo-list.component.html',
	styleUrls: ['./export-bo-list.component.css']
})
export class ExportBoListComponent implements OnInit, OnDestroy {

	format: string;
	pageOrientationLandscape = false;
	isColumnScaled = false;
	exportLink: string | undefined;

	@Input()
	meta: EntityMeta;

	private unsubscribe$ = new Subject<void>();

	constructor(
		private state: ExportBoListTemplateState,
		private dataService: DataService
	) {
	}

	ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	ngOnInit() {
	}

	executeListExport() {
		this.dataService.exportBoList(
			this.state.boId,
			this.state.refAttrId,
			this.state.meta ? this.state.meta : this.meta,
			this.state.columnPrefrenrence,
			this.state.searchtemplatePreference,
			this.format,
			this.pageOrientationLandscape,
			this.isColumnScaled
		)
		.pipe(takeUntil(this.unsubscribe$))
		.subscribe(
			exportLink => {
				this.exportLink = exportLink;
				window.location.href = exportLink;
			}
		);
	}

	close() {
		this.state.modalRef.close('confirmed');
	}

}
