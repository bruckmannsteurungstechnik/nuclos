import {
	Component,
	Directive,
	EventEmitter,
	Injectable,
	Input,
	OnChanges,
	OnDestroy,
	OnInit,
	Output,
	TemplateRef,
	ViewChild
} from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { AuthenticationService } from '../../../authentication';
import { EntityMeta, EntityMetaData } from '../../../entity-object-data/shared/bo-view.model';
import { EntityObjectEventService } from '../../../entity-object-data/shared/entity-object-event.service';
import { EntityObjectResultService } from '../../../entity-object-data/shared/entity-object-result.service';
import {
	Preference,
	SearchtemplatePreferenceContent,
	SideviewmenuPreferenceContent
} from '../../../preferences/preferences.model';
import { NuclosConfigService } from '../../../shared/nuclos-config.service';
import { SystemParameter } from '../../../shared/system-parameters';
import { SidebarComponent } from '../sidebar.component';
import { UserAction } from '@nuclos/nuclos-addon-api';

/**
 * contains reference to the modal template
 */
@Injectable()
export class ExportBoListTemplateState {
	modalRef: NgbModalRef;
	templateRef: TemplateRef<any>;
	boId: number | undefined;
	refAttrId: string | undefined;
	meta: EntityMeta | undefined;
	columnPrefrenrence: Preference<SideviewmenuPreferenceContent>;
	searchtemplatePreference: Preference<SearchtemplatePreferenceContent>;
}

@Directive({
	selector: '[nucExportBoListTemplate]'
})
export class ExportBoListTemplateDirective {
	constructor(template: TemplateRef<any>, state: ExportBoListTemplateState) {
		state.templateRef = template;
	}
}

@Component({
	selector: 'nuc-statusbar',
	templateUrl: './statusbar.component.html',
	styleUrls: ['./statusbar.component.scss']
})
export class StatusbarComponent implements OnInit, OnChanges, OnDestroy {
	showCollapseButton = false;
	showExpandButton = false;
	showExpandFullButton = false;

	@ViewChild('totoalCount')
	totoalCount;

	@Input()
	meta: EntityMetaData;

	@Input()
	sideviewMenuWidth: number;

	totalEOCount: number;

	@Input()
	sideviewmenuPreference: Preference<SideviewmenuPreferenceContent>;

	@Input()
	searchtemplatePreference: Preference<SearchtemplatePreferenceContent>;

	@Output()
	sideviewMenuWidthChange = new EventEmitter<number>();

	@Input() private sidebar: SidebarComponent;

	private showPrintSearchResultListButton = false;
	private showMultiselectionButton = false;
	private subscriptions: Subscription = new Subscription();

	constructor(
		authenticationService: AuthenticationService,
		private entityObjectResultService: EntityObjectResultService,
		private entityObjectEventService: EntityObjectEventService,
		private modalService: NgbModal,
		private exportBoListTemplateState: ExportBoListTemplateState,
		private config: NuclosConfigService
	) {
		this.showPrintSearchResultListButton = authenticationService.isActionAllowed(
			UserAction.PrintSearchResultList
		);
		/*this.showMultiselectionButton =
			this.showPrintSearchResultListButton ||
			authenticationService.isActionAllowed(UserAction.CollectiveProcessing);
		*/ this.config
			.getSystemParameters()
			.pipe(take(1))
			.subscribe(params => {
				this.showMultiselectionButton =
					this.showPrintSearchResultListButton ||
					params.is(SystemParameter.FUNCTION_BLOCK_DEV);
			});
	}

	ngOnInit() {
		this.subscriptions.add(
			this.entityObjectResultService.observeResultListUpdate().subscribe(() => {
				this.udateCount();
			})
		);
		this.subscriptions.add(
			this.entityObjectEventService.observeSavedEo().subscribe(() => {
				this.udateCount();
			})
		);
	}

	ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	ngOnChanges(): void {
		this.updateSideviewmenuStatusbar();
	}

	expandSideview() {
		this.scrollLeft();
		this.showNumberOfColumns(this.getNrOfVisibleColumns() + 1);
	}

	collapseSideview() {
		this.scrollLeft();
		this.showNumberOfColumns(this.getNrOfVisibleColumns() - 1);
	}

	expandSideviewFull() {
		this.scrollLeft();
		this.showNumberOfColumns(this.getNrOfColumns());
	}

	collapseSideviewFull() {
		this.scrollLeft();
		this.sidebar.width = '20px';
	}

	openExportBoListModal() {
		this.exportBoListTemplateState.boId = undefined;
		this.exportBoListTemplateState.refAttrId = undefined;
		this.exportBoListTemplateState.meta = undefined;
		this.exportBoListTemplateState.columnPrefrenrence = this.sideviewmenuPreference;
		this.exportBoListTemplateState.searchtemplatePreference = this.searchtemplatePreference;
		this.exportBoListTemplateState.modalRef = this.modalService.open(
			this.exportBoListTemplateState.templateRef
		);
	}

	toggleMultiselection() {
		this.sidebar.showMultiSelection = !this.sidebar.showMultiSelection;
	}

	private udateCount() {
		let totalEOCount = this.entityObjectResultService.getTotalResultCount();
		if (totalEOCount !== undefined) {
			this.totalEOCount = totalEOCount;
			if (this.totoalCount) {
				$(this.totoalCount.nativeElement).text(this.totalEOCount);
			}
		}
	}

	private getNrOfColumns(): number {
		return $('nuc-sidebar th').length;
	}

	/**
	 * number of visible columns starting from status column
	 */
	private getNrOfVisibleColumns(): number {
		let navWidth = this.sideviewMenuWidth;
		let itemIndex = 0;
		let widthSum = 0;
		let headerElements = $('nuc-sidebar th').toArray();
		for (let headerElement of headerElements) {
			widthSum += $(headerElement).width();
			if (widthSum > navWidth - (itemIndex > 1 ? 10 : 0)) {
				return itemIndex > this.getNrOfColumns() ? this.getNrOfColumns() : itemIndex;
			}
			itemIndex++;
		}
		return itemIndex > this.getNrOfColumns() ? this.getNrOfColumns() : itemIndex;
	}

	private updateSideviewmenuStatusbar() {
		this.showExpandButton = this.getNrOfVisibleColumns() < this.getNrOfColumns();
		this.showExpandFullButton = this.showExpandButton;
		this.showCollapseButton = this.getNrOfVisibleColumns() > 1;
	}

	private showNumberOfColumns(numberOfColumns: number) {
		if (numberOfColumns > this.getNrOfColumns()) {
			numberOfColumns = this.getNrOfColumns();
		}
		let th = $($('nuc-sidebar th')[numberOfColumns - 1]);
		let newWidth = th.offset().left + th.width() - $($('nuc-sidebar tbody')).offset().left + 6;

		// Don't make the sideview wider than the document
		let documentWidth = $(document).width();
		newWidth = Math.min(documentWidth, newWidth);

		this.sidebar.width = newWidth + 'px';

		this.sideviewMenuWidth = newWidth;
		this.sideviewMenuWidthChange.emit(newWidth);

		this.updateSideviewmenuStatusbar();
	}

	private scrollLeft() {
		this.sidebar.scrollLeft();
	}
}
