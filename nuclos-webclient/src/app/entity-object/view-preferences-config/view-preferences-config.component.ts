import { DOCUMENT } from '@angular/common';
import {
	Component,
	Directive,
	ElementRef,
	Inject,
	Injectable,
	Input,
	OnDestroy,
	OnInit,
	TemplateRef,
	ViewChild
} from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { IPreferenceFilter, UserAction } from '@nuclos/nuclos-addon-api';
import { BehaviorSubject, Observable, of as observableOf, Subscription, Subject } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import { AuthenticationService } from '../../authentication';
import { EntityMeta } from '../../entity-object-data/shared/bo-view.model';
import { EntityObjectResultService } from '../../entity-object-data/shared/entity-object-result.service';
import { SelectableService } from '../../entity-object-data/shared/selectable.service';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { DialogService } from '../../popup/dialog/dialog.service';
import {
	AttributeSelectionContent,
	ColumnAttribute,
	Preference,
	PreferenceType,
	SelectedAttribute,
	SidebarLayoutType,
	SideviewmenuPreferenceContent
} from '../../preferences/preferences.model';
import { PreferencesService } from '../../preferences/preferences.service';
import { FqnService } from '../../shared/fqn.service';
import { Selectable } from '../../two-side-multi-select/two-side-multi-select.model';
import { SideviewmenuService } from '../shared/sideviewmenu.service';

@Injectable()
export class PreferenceDialogState {
	/**
	 * last opened view preference modalRef
	 */
	modalRef: NgbModalRef;

	/**
	 * ref containing the modal component
	 */
	templateRef: TemplateRef<any>;

	meta: EntityMeta;
}

@Component({
	selector: 'nuc-view-preferences-modal',
	templateUrl: './view-preferences-modal.component.html',
	styleUrls: ['./view-preferences-modal.component.css']
})
export class ViewPreferencesModalComponent implements OnInit, OnDestroy {
	/*
	 injected via ViewPreferencesModalContainerComponent
	 */
	meta: EntityMeta;

	/* id  of the layout where the subform is configured */
	layoutId: string | undefined;

	/* list of attribute fqns */
	hiddenColumns: string[];

	selectedSideviewmenuPref$: BehaviorSubject<
		Preference<SideviewmenuPreferenceContent> | undefined
	> = new BehaviorSubject(undefined);

	backupSelectedSideviewmenuPref: Preference<SideviewmenuPreferenceContent>;

	showResetSortingButton = false;
	editSideviewmenu;
	sideviewmenuColumns: Selectable[] = [];
	selectedSideviewmenuPreference: Preference<SideviewmenuPreferenceContent> | undefined;
	sideviewmenuPreferences: Preference<SideviewmenuPreferenceContent>[] = [];

	viewtype: SidebarLayoutType | undefined;

	alowSidebarCustomization: boolean;

	@ViewChild('sideviewmenuSelector') sideviewmenuSelector: ElementRef;

	private unsubscribe$ = new Subject<void>();

	constructor(
		@Inject(DOCUMENT) private document,
		private state: PreferenceDialogState,
		private eoResultService: EntityObjectResultService,
		private preferencesService: PreferencesService,
		private sideviewmenuService: SideviewmenuService,
		private selectableService: SelectableService,
		private fqnService: FqnService,
		private authenticationService: AuthenticationService,
		private dialogService: DialogService,
		private i18n: NuclosI18nService
	) {}

	ngOnInit() {
		if (this.selectedSideviewmenuPref$.getValue()) {
			this.backupSelectedSideviewmenuPref = JSON.parse(
				JSON.stringify(this.selectedSideviewmenuPref$.getValue())
			);
		}

		this.selectedSideviewmenuPref$.pipe(takeUntil(this.unsubscribe$)).subscribe(pref => {
			if (pref) {
				this.selectedSideviewmenuPreference = pref;

				this.sideviewmenuColumnsChanged(false);

				// add attributes which are not already in pref
				this.sideviewmenuPreferences.forEach(p => {
					this.addAvailableColumnsFromMeta(p, this.meta);
					this.removeHiddenColumns(p);
				});
				if (this.selectedSideviewmenuPreference) {
					this.addAvailableColumnsFromMeta(
						this.selectedSideviewmenuPreference,
						this.meta
					);
					this.removeHiddenColumns(this.selectedSideviewmenuPreference);

					// add meta data to columns
					this.sideviewmenuPreferences.forEach(p =>
						this.selectableService.addMetaDataToColumns(p, this.meta)
					);

					this.sideviewmenuColumns = this.selectedSideviewmenuPreference.content.columns.filter(
						value => value.hidden === undefined || !value.hidden
					) as Selectable[];
				}
				this.sideviewmenuColumns = this.selectedSideviewmenuPreference.content.columns.filter(
					value => value.hidden === undefined || !value.hidden
				) as Selectable[];
			}
		});

		// load preferences for sideview menu and searchtemplate
		let filter: IPreferenceFilter = {
			boMetaId: this.meta.getBoMetaId(),
			type: [
				this.meta.isSubform() ? PreferenceType.subformTable.name : PreferenceType.table.name
			]
		};

		if (this.layoutId) {
			filter.layoutId = this.layoutId;
			filter.orLayoutIsNull = true;
		}

		this.preferencesService.getPreferences(filter).pipe(takeUntil(this.unsubscribe$)).subscribe(preferences => {
			/*
				 sidebar preferences
				 */

			// set table prefs
			this.sideviewmenuPreferences = preferences.filter(
				p =>
					p.type === PreferenceType.table.name ||
					p.type === PreferenceType.subformTable.name
			) as Preference<SideviewmenuPreferenceContent>[];

			// in preferences only selected columns are stored (without selected flag)
			this.sideviewmenuPreferences.forEach(pref =>
				pref.content.columns.forEach(column => (column.selected = true))
			);

			this.selectedSideviewmenuPreference = this.selectedSideviewmenuPref$.getValue();
			if (this.sideviewmenuPreferences.length === 0) {
				if (
					this.selectedSideviewmenuPreference &&
					this.selectedSideviewmenuPreference.prefId === undefined
				) {
					// only one temporary pref exists - add it to option list
					this.sideviewmenuPreferences.push(this.selectedSideviewmenuPreference);
				}
			} else {
				// selected pref
				this.selectedSideviewmenuPreference = this.sideviewmenuPreferences
					.filter(p => p.selected)
					.shift();

				// if no pref is selected select the first pref in list
				if (
					!this.selectedSideviewmenuPreference &&
					this.sideviewmenuPreferences.length > 0
				) {
					this.selectedSideviewmenuPreference = this.sideviewmenuPreferences[0];
				}
			}

			// add attributes which are not already in pref
			this.sideviewmenuPreferences.forEach(pref =>
				this.addAvailableColumnsFromMeta(pref, this.meta)
			);

			// add meta data to columns
			this.sideviewmenuPreferences.forEach(pref =>
				this.selectableService.addMetaDataToColumns(pref, this.meta)
			);

			this.selectSideviewmenu(this.selectedSideviewmenuPreference);

			this.viewtype = this.sideviewmenuService.getViewType().getValue();
		});

		this.checkAlowCustomization();
	}

	ngOnDestroy(): void {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	setViewtype(viewtype: SidebarLayoutType): void {
		this.viewtype = viewtype;
		this.sideviewmenuService.setViewType(viewtype);
	}

	showViewTypeConfig(): boolean {
		// TODO not implemented for subforms
		return !this.meta.isSubform();
	}

	sideviewmenuColumnsChanged(dirty: boolean): void {
		if (this.selectedSideviewmenuPreference) {
			this.selectedSideviewmenuPreference.dirty = dirty;
			// update position columns
			this.selectedSideviewmenuPreference.content.columns.forEach(
				(column, index) => (column.position = index)
			);
		}
		this.selectableService.updatePreferenceName(this.selectedSideviewmenuPreference);
	}

	close(): void {
		// reset to initial state
		if (this.backupSelectedSideviewmenuPref) {
			this.selectedSideviewmenuPref$.next(this.backupSelectedSideviewmenuPref);
		}

		this.state.modalRef.close('confirmed');
	}

	save(): void {
		if (
			this.selectedSideviewmenuPreference &&
			this.selectedSideviewmenuPreference.content.columns.filter(c => c.selected).length === 0
		) {
			alert(this.i18n.getI18n('webclient.preferences.dialog.columnsRequired'));
			return;
		}

		if (this.selectedSideviewmenuPreference) {
			this.selectedSideviewmenuPreference.selected = true;

			if (
				this.selectedSideviewmenuPreference.prefId ||
				this.selectedSideviewmenuPreference.dirty
			) {
				this.selectedSideviewmenuPreference.layoutId = this.layoutId;
				this.selectableService.savePreference(
					this.selectedSideviewmenuPreference
				)
				.pipe(take(1))
				.subscribe((preferenceItem: Preference<SideviewmenuPreferenceContent>) => {
					this.sideviewmenuService.sideviewmenuPrefChanged();
					if (preferenceItem) {
						this.selectedSideviewmenuPref$.next(preferenceItem);
					}
					this.state.modalRef.close('confirmed');
				});
			}
		}
	}

	selectSideviewmenu(
		sideviewmenuPreference: Preference<SideviewmenuPreferenceContent> | undefined
	): void {
		if (!sideviewmenuPreference) {
			return;
		}

		this.showResetSortingButton =
			sideviewmenuPreference.content.columns.filter(column => column.sort && column.sort.prio)
				.length > 0;

		this.selectedSideviewmenuPreference = sideviewmenuPreference;

		if (this.selectedSideviewmenuPreference.prefId) {
			this.preferencesService
				.selectPreference(this.selectedSideviewmenuPreference, this.layoutId)
				.pipe(take(1))
				.subscribe();
		}

		this.selectedSideviewmenuPref$.next(this.selectedSideviewmenuPreference);

		this.sideviewmenuColumns = sideviewmenuPreference.content.columns as Selectable[];
	}

	checkAlowCustomization(): void {
		this.authenticationService.onAuthenticationDataAvailable().pipe(take(1)).subscribe(loginSuccessful => {
			this.alowSidebarCustomization =
				loginSuccessful &&
				this.authenticationService.isActionAllowed(
					UserAction.WorkspaceCustomizeEntityAndSubFormColumn
				);
		});
	}

	editSideviewmenuPref(): void {
		window.setTimeout(() => {
			this.editSideviewmenu = true;
			window.setTimeout(() => {
				// focus/select text input
				this.document.getElementById('sideviewmenu-name-input').select();
			});
		});
	}

	editSideviewmenuPrefDone(): void {
		if (this.selectedSideviewmenuPreference) {
			this.selectedSideviewmenuPreference.layoutId = this.layoutId;
			this.selectableService
				.savePreference(this.selectedSideviewmenuPreference)
				.pipe(take(1))
				.subscribe(() => {
					this.editSideviewmenu = false;
				});
		}
	}

	showDeleteSideviewmenuPref(): boolean {
		return (
			this.selectedSideviewmenuPreference !== undefined &&
			!this.editSideviewmenu &&
			!this.selectedSideviewmenuPreference.shared &&
			(this.sideviewmenuPreferences.length > 1 ||
				(this.sideviewmenuPreferences.length === 1 &&
					this.sideviewmenuPreferences[0].prefId !== undefined))
		);
	}

	/**
	 * delete a not shared pref
	 */
	deleteSideviewmenuPref() {
		if (this.selectedSideviewmenuPreference) {
			this.deletePref(
				this.selectedSideviewmenuPreference,
				this.sideviewmenuPreferences
			).pipe(take(1)).subscribe(() => {
				delete this.selectedSideviewmenuPreference;
				if (this.sideviewmenuPreferences.length > 0) {
					this.selectedSideviewmenuPreference = this.sideviewmenuPreferences[0];
				}
				if (this.sideviewmenuPreferences.length === 0) {
					this.newSideviewmenuPref(false, true);
				}
				if (this.selectedSideviewmenuPreference) {
					this.sideviewmenuColumns = this.selectedSideviewmenuPreference.content.columns.filter(
						value => value.hidden === undefined || !value.hidden
					) as Selectable[];
				}
			});
		}
	}

	/**
	 * discard user changes of sideviewmenu pref
	 * deletes customized pref
	 * reselect shared pref
	 */
	discardSideviewmenuPref() {
		if (this.selectedSideviewmenuPreference) {
			this.preferencesService
				.deleteCustomizedPreferenceItem(this.selectedSideviewmenuPreference)
				.pipe(take(1))
				.subscribe(() => {
					if (
						this.selectedSideviewmenuPreference &&
						this.selectedSideviewmenuPreference.prefId
					) {
						this.preferencesService
							.getPreference(this.selectedSideviewmenuPreference.prefId)
							.pipe(take(1))
							.subscribe((pref: Preference<SideviewmenuPreferenceContent>) => {
								if (pref) {
									let prefInList = this.sideviewmenuPreferences
										.filter(p => p.prefId === pref.prefId)
										.shift();
									if (prefInList) {
										prefInList.content = pref.content;
										prefInList.customized = pref.customized;
									}
									this.selectedSideviewmenuPref$.next(pref);

									// TODO select HTML option - this should not be necessary
									if (this.selectedSideviewmenuPreference) {
										let selectedPrefId = this.selectedSideviewmenuPreference
											.prefId;
										if (selectedPrefId) {
											let index =
												this.sideviewmenuPreferences.findIndex(
													p => p.prefId === selectedPrefId
												) + 1;
											setTimeout(() => {
												this.sideviewmenuSelector.nativeElement.selectedIndex = index;
											});
										}
									}
								}
							});
					}
				});
		}
	}

	resetSorting() {
		if (this.selectedSideviewmenuPreference) {
			this.selectedSideviewmenuPreference.content.columns.forEach(col => {
				delete col.sort;
			});
		}
		this.showResetSortingButton = false;
	}

	newSideviewmenuPref(editMode = true, selectDefaultColumns = false) {
		this.editSideviewmenu = editMode;

		let sideviewmenuPref;
		let selectedEo = this.eoResultService.getSelectedEo();
		if (this.meta.isSubform()) {
			if (selectedEo) {
				sideviewmenuPref = this.sideviewmenuService.emptySubformPreference(
					this.meta,
					selectedEo.getEntityClassId(),
					selectDefaultColumns
				);
			}
		} else {
			sideviewmenuPref = this.sideviewmenuService.emptySideviewmenuPreference(
				this.meta,
				selectDefaultColumns
			);
		}
		if (sideviewmenuPref) {
			this.sideviewmenuPreferences.push(sideviewmenuPref);
		}
		this.selectSideviewmenu(sideviewmenuPref);

		if (editMode) {
			window.setTimeout(() => {
				// focus text input
				this.document.getElementById('sideviewmenu-name-input').focus();
			});
		}
	}

	handleNameChange(preferenceItem: Preference<AttributeSelectionContent>) {
		preferenceItem.content.userdefinedName =
			preferenceItem.name !== undefined &&
			preferenceItem.name.length > 0 &&
			preferenceItem.name !== this.selectableService.getDefaultName(preferenceItem);
	}

	private removeHiddenColumns(pref: Preference<AttributeSelectionContent>) {
		if (this.hiddenColumns) {
			pref.content.columns = pref.content.columns.filter(
				col => this.hiddenColumns.indexOf(col.boAttrId) === -1
			);
		}
	}

	private addAvailableColumnsFromMeta(
		pref: Preference<AttributeSelectionContent>,
		metaData: EntityMeta
	): void {
		this.meta.getAttributes().forEach((_attributeMeta, attrKey) => {
			let entityAttrMeta = this.meta.getAttributeMeta(attrKey);
			if (entityAttrMeta) {
				let attributeKey = this.fqnService.getShortAttributeName(
					metaData.getBoMetaId(),
					entityAttrMeta.getAttributeID()
				);
				let attribute = <SelectedAttribute>this.meta.getAttribute(attributeKey);
				if (attribute) {
					if (
						pref.content.columns &&
						pref.content.columns.filter(
							column => column.boAttrId === attribute.boAttrId
						).length === 0
					) {
						if (attribute.boAttrId !== metaData.getRefAttrId()) {
							// make a copy of the attribute object
							let attr: ColumnAttribute = Object.assign({}, attribute);

							if (attr && attr.boAttrId && metaData) {
								let fieldName = this.fqnService.getShortAttributeName(
									metaData.getBoMetaId(),
									attr.boAttrId
								);
								let attributeMeta = metaData.getAttributeMeta(fieldName);
								attr.sort = {
									enabled: attributeMeta && !attributeMeta.isCalculated()
								};
							}
							pref.content.columns.push(attr);
						}
					}
				}
			}
		});
	}

	private deletePref(
		prefItem: Preference<AttributeSelectionContent>,
		prefList: Preference<AttributeSelectionContent>[]
	): Observable<void> {
		return new Observable<void>(observer => {
			let removeFromModel = () => {
				prefList.splice(prefList.indexOf(prefItem), 1);
				if (prefList.length > 0) {
					prefItem = prefList[0];
				}
			};
			if (prefItem.prefId) {
				this.preferencesService
					.findReferencingPreferences([prefItem])
					.toPromise()
					.then(prefs => {
						if (prefs.length > 0) {
							this.dialogService.alert({
								title: this.i18n.getI18n(
									'webclient.preferences.dialog.error.referencingPreferences'
								),
								message: this.i18n.getI18n(
									'webclient.preferences.dialog.error.referencingPreferencesMessage',
									prefs.map(p => p.name).join(', ')
								)
							});
						} else {
							this.preferencesService.deletePreferenceItem(prefItem).pipe(take(1)).subscribe(() => {
								if (prefItem) {
									removeFromModel();
								}
								observer.next();
								observer.complete();
							});
						}
					});
			} else {
				removeFromModel();
				observer.next();
				observer.complete();
			}
		});
	}
}

@Directive({
	selector: '[nucViewPreferencesModal]'
})
export class ViewPreferencesModalDirective {
	constructor(confirmTemplate: TemplateRef<any>, state: PreferenceDialogState) {
		state.templateRef = confirmTemplate;
	}
}

@Component({
	selector: 'nuc-view-preferences-config',
	styleUrls: ['./view-preferences-modal-container.component.css'],
	template: `
		<ng-template nucViewPreferencesModal>
			<nuc-view-preferences-modal> </nuc-view-preferences-modal>
		</ng-template>
		<div *ngIf="mainbarButton">
			<button
				class="btn btn-sm view-preferences-button-mainbar"
				(click)="showViewPreferencesModal()"
			>
				<i class="fa fa-columns"></i>
			</button>
		</div>
		<button
			*ngIf="!mainbarButton"
			class="btn btn-sm view-preferences-button"
			(click)="showViewPreferencesModal()"
		>
			<i class="fa fa-columns"></i>
		</button>
	`
})
export class ViewPreferencesModalContainerComponent {
	@Input() mainbarButton: boolean;

	@Input()
	meta: EntityMeta;

	@Input()
	layoutId: string;

	@Input()
	hiddenColumns: any[];

	@Input()
	selectedSideviewmenuPref$: BehaviorSubject<
		Preference<SideviewmenuPreferenceContent> | undefined
	>;

	constructor(private modalService: NgbModal, private state: PreferenceDialogState) {}

	showViewPreferencesModal(): Promise<any> {
		this.state.modalRef = this.modalService.open(ViewPreferencesModalComponent, { size: 'lg' });
		this.state.modalRef.componentInstance.selectedSideviewmenuPref$ = this.selectedSideviewmenuPref$;
		this.state.modalRef.componentInstance.meta = this.meta;
		this.state.modalRef.componentInstance.layoutId = this.layoutId;
		this.state.modalRef.componentInstance.hiddenColumns = this.hiddenColumns;
		return this.state.modalRef.result;
	}
}
