import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NuclosI18nService } from '../i18n/shared/nuclos-i18n.service';

@Component({
	selector: 'nuc-error',
	templateUrl: './error.component.html',
	styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {
	statusCode: number;
	errorTitle: string;
	errorText: string;

	constructor(private route: ActivatedRoute, private nuclosI18n: NuclosI18nService) {
		this.route.params.subscribe(params => {
			this.statusCode = params['status'];
			this.errorTitle = this.getErrorTitle();
			this.errorText = this.getErrorText();
		});
	}

	ngOnInit() {}

	private getErrorTitle() {
		let text = this.nuclosI18n.getI18n('webclient.error.' + this.statusCode + '.title');
		if (!text) {
			text = this.nuclosI18n.getI18n('webclient.error.unknown.title');
		}
		return text;
	}

	private getErrorText() {
		let text = this.nuclosI18n.getI18n('webclient.error.' + this.statusCode + '.text');
		if (!text) {
			text = this.nuclosI18n.getI18n('webclient.error.unknown.text');
		}
		return text;
	}
}
