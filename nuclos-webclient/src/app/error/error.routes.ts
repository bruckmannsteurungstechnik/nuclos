import { RouterModule, Routes } from '@angular/router';
import { ErrorComponent } from './error.component';

export const ROUTE_CONFIG: Routes = [
	{
		path: ':status',
		component: ErrorComponent
	}
];

export const ErrorRoutesModule = RouterModule.forChild(ROUTE_CONFIG);
