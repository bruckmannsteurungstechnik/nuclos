import { Component, EventEmitter, Input, OnInit, OnDestroy, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { WsTab } from './explorertrees.model';
import { ExplorerTreesService } from './explorertrees.service';

@Component({
	selector: 'nuc-explorertrees',
	templateUrl: './explorertrees.component.html',
	styleUrls: ['./explorertrees.component.css']
})
export class ExplorerTreesComponent implements OnInit, OnDestroy {

	@Input()
	selected: WsTab;

	@Output()
	onSelect: EventEmitter<WsTab> = new EventEmitter<WsTab>();

	private tabs: WsTab[];
	private unsubscribe$ = new Subject<void>();

	/**
	 */

	constructor(
		private explorerTreesService: ExplorerTreesService,
	) {
	}

	ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	ngOnInit() {
		this.explorerTreesService.getMenuSelector().pipe(takeUntil(this.unsubscribe$)).subscribe(tabs => this.tabs = tabs);
	}

	getWsTabs() {
		return this.tabs;
	}

	getExplorerTabs() {
		return this.tabs && this.tabs.filter(tab => tab.type === 'explorer');
	}

	selectTab(tab: WsTab) {
		this.onSelect.emit(tab);
	}

}
