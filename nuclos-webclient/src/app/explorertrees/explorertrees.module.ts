import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { I18nModule } from '../i18n/i18n.module';
import { ExplorerTreesComponent } from './explorertrees.component';

@NgModule({
	imports: [
		CommonModule,
		I18nModule,
		NgbModule,
	],
	exports: [
		ExplorerTreesComponent
	],
	declarations: [
		ExplorerTreesComponent
	]
})
export class ExplorerTreesModule {
}
