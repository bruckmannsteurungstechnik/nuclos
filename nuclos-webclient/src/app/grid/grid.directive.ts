import { Directive } from '@angular/core';
import { ColDef } from 'ag-grid';
import { AgGridNg2 } from 'ag-grid-angular';
import { GridHeaderComponent } from './grid-header/grid-header.component';

/**
 * Makes some adjustments to the column headers for any ag-Grid component
 * annotated with this directive.
 *
 * TODO: After refactoring the grid components so that ag-Grid is not used directly
 * anymore except in the grid module, this directive probably won't be needed anymore.
 */
@Directive({
	selector: '[nucGrid]'
})
export class GridDirective {

	defaultColDef: ColDef = {
		headerComponentFramework: GridHeaderComponent,
	};

	constructor(grid: AgGridNg2) {
		if (!grid.defaultColDef) {
			grid.defaultColDef = {};
		}

		grid.defaultColDef.headerComponentFramework = GridHeaderComponent;
	}

}
