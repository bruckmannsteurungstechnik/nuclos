import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';
import { GridHeaderComponent } from './grid-header/grid-header.component';
import { GridDirective } from './grid.directive';
import { GridService } from './grid.service';
import {
	BooleanRendererComponent,
	DateRendererComponent,
	NumberRendererComponent,
	StateIconRendererComponent
} from './grid/cell-renderer';
import { GridComponent } from './grid/grid.component';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		AgGridModule.withComponents([
			StateIconRendererComponent,
			BooleanRendererComponent,
			NumberRendererComponent,
			DateRendererComponent
		]),
	],
	declarations: [
		GridHeaderComponent,
		GridDirective,
		GridComponent,
		BooleanRendererComponent
	],
	providers: [
		GridService
	],
	exports: [
		GridDirective,
		GridComponent,
		BooleanRendererComponent
	],
	entryComponents: [
		GridHeaderComponent,
	]
})
export class GridModule {
}
