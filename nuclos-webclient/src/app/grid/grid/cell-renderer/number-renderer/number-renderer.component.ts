import { Component } from '@angular/core';
import { NumberService } from '../../../../shared/number.service';
import { AbstractCellRenderer } from '../abstract-cell-renderer';

// TODO refactor with SubformNumberRendererComponent
@Component({
	selector: 'nuc-number-renderer',
	templateUrl: './number-renderer.component.html',
	styleUrls: ['./number-renderer.component.css']
})
export class NumberRendererComponent extends AbstractCellRenderer {

	formattedValue: string;

	constructor(private numberService: NumberService) {
		super();
	}

	agInit(params: any) {
		super.agInit(params);
		this.refresh(params);
	}

	refresh(params: any): boolean {
		super.refresh(params);
		let attributeMeta = this.getAttributeMeta();
		if (attributeMeta) {
			this.formattedValue = this.numberService.format(params.value, attributeMeta.getPrecision());
		}
		return false;

	}
}
