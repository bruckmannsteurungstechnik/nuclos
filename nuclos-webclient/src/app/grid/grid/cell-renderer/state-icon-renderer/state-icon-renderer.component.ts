import { Component } from '@angular/core';
import { AgRendererComponent } from 'ag-grid-angular';

@Component({
	selector: 'nuc-state-icon-renderer',
	templateUrl: './state-icon-renderer.component.html',
	styleUrls: ['./state-icon-renderer.component.css']
})
export class StateIconRendererComponent implements AgRendererComponent {

	stateIconUrl: string;

	params;

	constructor() {
	}

	agInit(params: any) {
		this.params = params;
		this.stateIconUrl = params.node.data ? params.node.data.nuclosStateIconUrl : undefined;
	}

	refresh(params: any): boolean {
		this.params = params;
		this.stateIconUrl = params.node.data ? params.node.data.stateIconUrl : undefined;
		return false;
	}

}
