import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { GridOptions } from 'ag-grid';
import { GridColumn } from '../grid-column';

/**
 * A generic grid component (wrapper for ag-grid).
 * Does not know about column preferences and other Nuclos entity specific stuff.
 *
 * TODO: Init cell renderers
 * TODO: Init cell editors?
 */
@Component({
	selector: 'nuc-grid',
	templateUrl: './grid.component.html',
	styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit {
	@Input() columns: GridColumn[];

	// TODO: Should not be used externally
	@Input() gridOptions: GridOptions = <GridOptions>{};

	@Output() rowSelected = new EventEmitter();
	@Output() gridReady = new EventEmitter();
	@Output() rowClicked = new EventEmitter();
	@Output() modelUpdated = new EventEmitter();

	constructor() {
	}

	ngOnInit() {
	}

}
