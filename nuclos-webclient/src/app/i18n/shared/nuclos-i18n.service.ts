import { Injectable } from '@angular/core';
import { Logger } from '@nuclos/nuclos-addon-api';
import { BehaviorSubject, EMPTY, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { NuclosCookieService } from '../../cookie/shared/nuclos-cookie.service';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { NuclosHttpService } from '../../shared/nuclos-http.service';
import { Locale } from './locale';

@Injectable()
export class NuclosI18nService {
	private selectedLocale: Locale;
	private defaultLocale: Locale = Locale.ENGLISH;
	private locales: Locale[] = Locale.getAllLocales();
	private messages = Locale.messages;
	private locale: BehaviorSubject<Locale>;

	constructor(
		private cookieService: NuclosCookieService,
		private http: NuclosHttpService,
		private config: NuclosConfigService,
		private $log: Logger
	) {
		this.locale = new BehaviorSubject<Locale>(this.defaultLocale);
		this.checkDefaultLocale();
	}

	getLocales(): Locale[] {
		return this.locales;
	}

	setLocaleString(localeString: string) {
		let locale = this.findLocale(localeString);
		this.setLocale(locale);
	}

	setLocale(locale: Locale) {
		if (!locale || this.selectedLocale === locale) {
			return;
		}

		// If a locale was selected before, we must refresh the page after selecting the new locale
		let needsRefresh = !!this.selectedLocale;

		this.selectedLocale = locale;
		this.cookieService.put('locale', locale.key);

		this.http
			.put(this.config.getRestHost() + '/locale', {
				locale: locale.key
			})
			.pipe(
				catchError(e => {
					this.$log.warn('Could not set locale on server side: %o', e);
					return EMPTY;
				})
			)
			.subscribe();

		if (needsRefresh) {
			$('body').css('cursor', 'progress');
			window.location.reload();
		}

		this.locale.next(this.selectedLocale);
	}

	getBrowserLocale(): string {
		let browserlocale = this.cookieService.get('locale');
		if (!browserlocale) {
			browserlocale = navigator.language;
		}
		if (!browserlocale) {
			browserlocale = 'en';
		}
		return browserlocale;
	}

	checkDefaultLocale() {
		if (this.selectedLocale) {
			return;
		}

		let browserlocale = this.getBrowserLocale();
		for (let locale of this.locales) {
			if (locale.key === browserlocale) {
				this.setLocale(locale);
				return;
			}
		}

		for (let locale of this.locales) {
			if (locale.key.substring(0, 2) === browserlocale.substring(0, 2)) {
				this.setLocale(locale);
				return;
			}
		}

		this.setLocale(this.defaultLocale);
	}

	getI18n(key: string, ...params: any[]): string {
		let value = this.getI18nOrUndefined(key, ...params);

		if (!value) {
			console.warn('Key "' + key + '" not found in locale.js');
			return key;
		}

		return value;
	}

	getI18nOrUndefined(key: string, ...params: any[]): string | undefined {
		if (this.messages[this.selectedLocale.key] == null) {
			return key;
		}

		let value: string = this.messages[this.selectedLocale.key][key];
		if (!value) {
			return undefined;
		}

		for (let i = 0; i < params.length; i++) {
			let param = params[i];
			value = value.replace(new RegExp('\\{' + i + '\\}', 'g'), param);
		}

		return value;
	}

	getLocale(): Observable<Locale> {
		return this.locale;
	}

	getCurrentLocale(): Locale {
		return this.locale.getValue();
	}

	private findLocale(key: string): any {
		for (let locale of this.locales) {
			if (locale.key === key) {
				return locale;
			}
		}
	}
}
