import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { InputRequiredSepecification } from '../shared/model';
import { StringUtils } from '../../shared/string-utils';

@Component({
	selector: 'nuc-input-required-dialog',
	templateUrl: './input-required-dialog.component.html',
	styleUrls: ['./input-required-dialog.component.css']
})
export class InputRequiredDialogComponent implements OnInit {

	messageFormatted: string;

	@Input() specification: InputRequiredSepecification;

	inputValue: string;

	constructor(private activeModal: NgbActiveModal) {
	}

	ngOnInit() {
		if (this.specification.type === 'input_option') {
			this.inputValue = this.specification.defaultoption;
		}
		this.messageFormatted = StringUtils.textToHtml(this.specification.message);
	}

	ok(result: any) {
		this.activeModal.close(result);
	}

	cancel() {
		this.activeModal.dismiss();
	}
}
