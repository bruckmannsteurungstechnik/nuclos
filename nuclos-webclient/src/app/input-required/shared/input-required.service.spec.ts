/* tslint:disable:no-unused-variable */
import { inject, TestBed } from '@angular/core/testing';
import { InputRequiredService } from './input-required.service';

xdescribe('Service: InputRequired', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [InputRequiredService]
		});
	});

	it('should ...', inject([InputRequiredService], (service: InputRequiredService) => {
		expect(service).toBeTruthy();
	}));
});
