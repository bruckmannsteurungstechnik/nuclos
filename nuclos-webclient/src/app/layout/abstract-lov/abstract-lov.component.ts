///<reference path="lov-handler.ts"/>
import {
	ElementRef,
	Injector,
	Input,
	OnChanges,
	OnInit,
	OnDestroy,
	SimpleChanges,
	ViewChild
} from '@angular/core';
import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { Observable, Subscription, Subject } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';
import { ClickOutsideService } from '../../click-outside/click-outside.service';
import { EntityAttrMeta, LovEntry } from '../../entity-object-data/shared/bo-view.model';
import { EntityObjectEventListener } from '../../entity-object-data/shared/entity-object-event-listener';
import { LovSearchConfig } from '../../entity-object-data/shared/lov-search-config';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { FqnService } from '../../shared/fqn.service';
import { NuclosDefaults } from '../../shared/nuclos-defaults';
import { AbstractInputComponent } from '../shared/abstract-input-component';
import { ComboboxUtils } from '../shared/combobox-utils';
import { LovHandler } from './lov-handler';

/**
 * TODO: Use DropdownComponent here instead of PrimeNG Autocomplete.
 */
export abstract class AbstractLovComponent<T extends WebInputComponent>
	extends AbstractInputComponent<T>
	implements OnInit, OnDestroy, OnChanges {
	@Input() handler: LovHandler = LovHandler.DUMMY;

	@ViewChild('autoComplete') autoComplete: AutoComplete;

	lovEntries: LovEntry[] = [];
	protected attributeMeta: EntityAttrMeta;

	private inputModified = false;
	private keydownEventRegistered = false;

	/**
	 * Fake dropdown entry to display the current search string.
	 */
	private searchValue?: LovEntry;

	private resultSubscription: Subscription;

	private eoListener: EntityObjectEventListener = {
		refreshVlp: () => this.refreshEntries()
	};

	private unsubscribe$ = new Subject<void>();

	constructor(
		protected injector: Injector,
		private fqnService: FqnService,
		protected elementRef: ElementRef
	) {
		super(injector);
	}

	ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	ngOnInit() {
		let name = this.getName();
		if (name) {
			this.eo.getAttributeMeta(name).pipe(takeUntil(this.unsubscribe$)).subscribe(meta => {
				this.attributeMeta = meta;
			});
		}

		this.eo.addListener(this.eoListener);

		// when using the dropdown in the primeng p-autocomplete component the input element will be focused when clicking on the dropdown
		// this behaviour will always set back the focus to the input when trying to leave the input with a keyboard tab
		// disabling the dropdown button fixes this
		setTimeout(() => {
			let dropdownButton = this.elementRef.nativeElement.querySelector('button');
			if (dropdownButton) {
				$(dropdownButton).prop('tabindex', -1);
			}
		});
	}

	setFocus() {
		$(this.autoComplete.inputEL.nativeElement).focus();
	}

	blur() {
		this.searchValue = undefined;

		if (this.inputModified) {
			if (this.autoComplete.highlightOption) {
				this.selectDropdownOption(this.autoComplete.highlightOption);
			} else {
				this.clearModelValue();
			}
		}
	}

	ngOnChanges(changes: SimpleChanges) {
		let eoChange = changes['eo'];
		if (eoChange) {
			if (eoChange.previousValue) {
				eoChange.previousValue.removeListener(this.eoListener);
			}
			if (eoChange.currentValue) {
				eoChange.currentValue.addListener(this.eoListener);
			}
		}
	}

	isWritable(): boolean {
		if (this.handler.isEnabled) {
			return this.handler.isEnabled();
		}

		return super.isWritable();
	}

	getLovSearchConfig(search: string): LovSearchConfig {
		return {
			attributeMeta: this.attributeMeta,
			eo: this.eo,
			quickSearchInput: search,
			vlp: this.webComponent.valuelistProvider,
			resultLimit: NuclosDefaults.DROPDOWN_SHOW_RESULT_LIMIT,
			mandatorId: this.getEntityObject()
				.getRootEo()
				.getMandatorId()
		};
	}

	getVlpId(): string | undefined {
		if (this.getVlp()) {
			return this.getVlp()!.value;
		}
		return undefined;
	}

	getVlp(): WebValuelistProvider | undefined {
		return this.webComponent.valuelistProvider;
	}

	getLovHandler(): LovHandler {
		return this.handler;
	}

	isLoading(): boolean {
		return this.resultSubscription && !this.resultSubscription.closed;
	}

	/**
	 * fix for https://github.com/primefaces/primeng/issues/745
	 */
	showAllEntries() {
		if (!this.isWritable()) {
			return;
		}

		this.showResultPanel();

		this.subscribeToNewEntries(
			this.handler.loadEntries().pipe(finalize(() => this.highlightSelectedOption()))
		);
	}

	autocomplete(search: string) {
		search = this.getInputValue();

		// Do not search again for the same text
		if (this.searchValue && this.searchValue.name === search) {
			this.autoComplete.loading = false;
			return;
		}

		this.showResultPanel();

		this.inputModified = true;
		this.searchValue = { name: search, id: null };
		this.autoComplete.highlightOption = undefined;

		this.subscribeToNewEntries(
			this.filter(search).pipe(finalize(() => this.highlightUniqueMatch()))
		);
	}

	/**
	 * The current search value should be always visible in the input field,
	 * therefore it is returned first.
	 */
	getValue(): any {
		if (this.attributeMeta && !this.attributeMeta.isReference()) {
			return this.getValueForTextfield();
		}

		if (this.searchValue) {
			return this.searchValue;
		}

		let result = super.getValue();

		if (!result || !result.name) {
			if (this.inputModified && this.hasFocus()) {
				return this.getInputValue();
			}
			// NUCLOS-5594 - workaround for https://github.com/primefaces/primeng/issues/2698
			result = null;
		}

		return result;
	}

	openReference(createNewEntry = false) {
		let field = this.eo.getAttribute(this.webComponent.name);
		if (!field) {
			return;
		}

		let fieldName = this.fqnService.getShortAttributeName(
			this.eo.getEntityClassId(),
			this.attributeMeta.getAttributeID()
		);
		if (fieldName) {
			let referenceBo = createNewEntry ? { id: 'new' } : this.eo.getAttribute(fieldName);

			if (referenceBo && referenceBo.id) {
				let href =
					location.href.substring(0, location.href.indexOf('/#/')) +
					'/#/view/' +
					this.attributeMeta.getReferencedEntityClassId() +
					'/' +
					referenceBo.id +
					'?expand=true';
				if (field.properties && field.properties.popupparameter) {
					window.open(href, '', field.properties.popupparameter);
				} else {
					window.open(href);
				}
			}
		}
	}

	selectDropdownOption(value) {
		this.searchValue = undefined;

		this.setValue(value);

		// TODO: Input value should be updated automatically by the autocomplete
		if (value) {
			$(this.autoComplete.inputEL.nativeElement).val(value.name);
		}

		if (this.handler.entrySelected) {
			this.handler.entrySelected(value);
		}

		if (this.autoComplete.panelVisible) {
			// sometimes it seems to remain open (during testing)
			this.autoComplete.hide();
		}
	}

	/**
	 * Validates the given value and writes it to the model.
	 */
	setValue(value: any): any {
		if (!this.attributeMeta.isReference()) {
			if (value && value.name) {
				value = value.name;
			}
		}

		return super.setValue(value);
	}

	/**
	 * Only for non-reference fields should the text input be written to the model immediately.
	 * For reference field, the model is only written after an entry is selected from the dropdown.
	 */
	setValueImmediatelyIfNoReference(value: any) {
		if (!this.attributeMeta.isReference() && this.isInsertable()) {
			this.setValue(value);
		}
	}

	toggleResultPanelAndLoad($event?) {
		if ($event) {
			// We handle the dropdown panel. Prevent the event from reaching the autocomplete component:

			let eventObject = $event;

			// The Autocomplete component wraps the original MouseEvent
			if (typeof eventObject.originalEvent === 'object') {
				eventObject = eventObject.originalEvent;
			}

			if (typeof eventObject.preventDefault === 'function') {
				eventObject.preventDefault();
			}
			if (typeof eventObject.stopPropagation === 'function') {
				eventObject.stopPropagation();
			}
		}

		if (this.autoComplete.panelVisible) {
			this.autoComplete.hide();
		} else {
			this.showAllEntries();
		}

		// select lov input text
		let textInput = this.autoComplete.inputEL.nativeElement;
		if (textInput.selectionEnd - textInput.selectionStart === 0) {
			textInput.setSelectionRange(0, textInput.value.length);
		}
	}

	dropdownClick($event) {
		if (this.autoComplete.panelVisible) {
			this.autoComplete.hide();
			return;
		}

		this.toggleResultPanelAndLoad($event);
		this.autoComplete.handleDropdownClick($event);
	}

	keyup($event: KeyboardEvent) {
		if ($event.key === 'ArrowUp' || $event.key === 'ArrowDown') {
			return;
		}
		// The autocomplete component does not trigger a search, if there is no input
		if ($(this.autoComplete.inputEL.nativeElement).val() === '') {
			this.clearModelValue();
			this.showAllEntries();
		}
	}

	getAttributeMeta() {
		return this.attributeMeta;
	}

	getAutocomplete() {
		return this.autoComplete;
	}

	outsideClick(target: EventTarget) {
		if (
			target !== this.elementRef.nativeElement &&
			!this.autoComplete.el.nativeElement.contains(target)
		) {
			this.autoComplete.hide();
		}
	}

	getWidth() {
		return this.handler.getWidth && this.handler.getWidth();
	}

	getHeight() {
		return this.handler.getHeight && this.handler.getHeight();
	}

	searchReferenceClick() {
		if (this.handler.searchReferenceClick) {
			this.handler.searchReferenceClick();
		}
	}

	addReferenceClick() {
		if (this.handler.addReferenceClick) {
			this.handler.addReferenceClick();
		}
	}

	private refreshEntries() {
		if (this.handler.refreshEntries) {
			this.handler.refreshEntries();
		}
	}

	private getInputValue() {
		let inputValue = $(this.autoComplete.inputEL.nativeElement).val();
		inputValue = inputValue.replace('\u200b', '');
		return inputValue;
	}

	/**
	 * Loads new LOV entries from the given Observable,
	 * cancels previous Subscription.
	 */
	private subscribeToNewEntries(observable: Observable<LovEntry[]>) {
		if (this.resultSubscription && !this.resultSubscription.closed) {
			this.resultSubscription.unsubscribe();
		}

		this.resultSubscription = observable
			.pipe(finalize(() => this.updateMessage()), takeUntil(this.unsubscribe$))
			.subscribe(data => this.setLovEntries(data));

		this.loadingStarted();
	}

	private highlightSelectedOption() {
		if (this.getValue()) {
			// highlight selected option
			this.autoComplete.highlightOption = this.lovEntries
				.filter(option => !!option.id && option.id === this.getValue().id)
				.shift();

			this.lovEntries.forEach(
				option => (option.selected = !!option.id && option.id === this.getValue().id)
			);
		}
	}

	private highlightUniqueMatch() {
		// if only one item is suggested preselect it
		if (this.lovEntries) {
			let nonEmptyResults = this.lovEntries.filter(entry => entry.id !== null);
			if (nonEmptyResults.length === 1) {
				let uniqueMatch = nonEmptyResults[0];
				this.autoComplete.highlightOption = uniqueMatch;
				if (this.getModelValue() && this.getModelValue().id !== uniqueMatch.id) {
					this.clearModelValue();
				}
			} else {
				this.clearModelValue();
			}
		}
	}

	private setLovEntries(results: LovEntry[]) {
		this.lovEntries = results.slice(0, NuclosDefaults.DROPDOWN_SHOW_RESULT_LIMIT);
		this.autoComplete.loading = false;
		this.updateMessage();
	}

	private showResultPanel() {
		if (!this.isWritable()) {
			return;
		}

		this.notifyClickOutside();

		this.autoComplete.focusInput();
		this.autoComplete.show();
		this.autoComplete.align();
		this.setupPanelAndInput();
	}

	private getValueForTextfield() {
		let value = super.getValue();

		if (typeof value === 'string') {
			if (this.searchValue) {
				this.searchValue.name = value;
			} else {
				this.searchValue = { name: value, id: null };
			}
			value = this.searchValue;
		}

		return value;
	}

	private hasFocus() {
		let result =
			this.autoComplete &&
			this.autoComplete.inputEL &&
			this.autoComplete.inputEL.nativeElement === document.activeElement;

		return result;
	}

	private clearModelValue() {
		if (this.attributeMeta.isReference()) {
			this.setValue(LovEntry.EMPTY);
		} else {
			this.setValue(undefined);
		}

		if (this.handler.clearValue) {
			this.handler.clearValue();
		}
	}

	private filter(search: string) {
		// TODO: It should not be necessary to filter again here,
		// but filtering does not work reliably in LOVs yet.
		return ComboboxUtils.filter(this.handler.loadFilteredEntries(search), search);
	}

	private setupPanelAndInput() {
		let element = this.autoComplete.panelEL.nativeElement;
		element.setAttribute('for-id', this.getId());

		let inputEl = this.autoComplete.inputEL.nativeElement;
		let inputWidth = $(inputEl).outerWidth();
		$(element).outerWidth(inputWidth);

		if (!this.keydownEventRegistered) {
			this.keydownEventRegistered = true;

			inputEl.addEventListener('keydown', event => {
				if (this.handler.keydownHandler) {
					this.handler.keydownHandler(event);
				}
			});
		}
	}

	private notifyClickOutside() {
		let clickOutsideService = this.injector.get(ClickOutsideService);
		if (clickOutsideService) {
			clickOutsideService.outsideClick(this.elementRef.nativeElement);
		}
	}

	private loadingStarted() {
		// Make sure we are really still loading something
		if (!this.isLoading()) {
			return;
		}

		this.lovEntries = [];
		this.updateMessage();
		this.showResultPanel();
	}

	/**
	 * Abuses the empty-message for displaying a "loading" text, since the normal loading indicator of
	 * the component can not be used because it overlaps with Nuclos reference buttons.
	 */
	private updateMessage() {
		if (this.isLoading()) {
			this.autoComplete.emptyMessage = this.getI18nService().getI18n('common.loading');
		} else {
			if (typeof this.lovEntries !== 'undefined' && this.lovEntries.length === 0) {
				this.autoComplete.emptyMessage = this.getI18nService().getI18n('common.empty');
			} else {
				this.autoComplete.emptyMessage = '';
			}
		}
	}

	private getI18nService() {
		return this.injector.get(NuclosI18nService);
	}
}
