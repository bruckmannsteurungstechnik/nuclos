import { Component, Input, OnInit } from '@angular/core';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';

@Component({
	selector: 'nuc-grid-layout',
	templateUrl: './grid-layout.component.html',
	styleUrls: ['./grid-layout.component.css']
})
export class GridLayoutComponent implements OnInit {
	@Input() webGrid: WebGrid;
	@Input() eo: EntityObject;

	constructor() {
	}

	ngOnInit() {
	}

}
