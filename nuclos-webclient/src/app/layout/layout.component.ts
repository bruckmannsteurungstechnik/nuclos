import {
	Component,
	ElementRef,
	EventEmitter,
	Input,
	OnChanges,
	OnDestroy,
	OnInit,
	Output,
	SimpleChanges
} from '@angular/core';
import { BrowserDetection } from '@angular/platform-browser/testing/src/browser_util';
import { Observable, Subscriber, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ClickOutsideService } from '../click-outside/click-outside.service';
import { EntityObjectEventListener } from '../entity-object-data/shared/entity-object-event-listener';
import { EntityObject } from '../entity-object-data/shared/entity-object.class';
import { Logger } from '../log/shared/logger';
import { RuleService } from '../rule/shared/rule.service';
import { BrowserDetectionService } from '../shared/browser-detection.service';
import { BusyService } from '../shared/busy.service';
import { LayoutService } from './shared/layout.service';

@Component({
	selector: 'nuc-layout',
	templateUrl: './layout.component.html',
	styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit, OnChanges, OnDestroy {
	@Input() eo: EntityObject | undefined;
	@Output() onSubmit = new EventEmitter<any>();

	webLayout: WebLayout | undefined;
	rules: Rules | undefined;

	private eoListener: EntityObjectEventListener;
	private subscriptions: Subscription = new Subscription();

	constructor(
		private layoutService: LayoutService,
		private busyService: BusyService,
		private ruleService: RuleService,
		private clickOutsideServer: ClickOutsideService,
		private browserDetectionService: BrowserDetectionService,
		private elementRef: ElementRef,
		private $log: Logger
	) {}

	ngOnInit() {}

	ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	ngOnChanges(changes: SimpleChanges): void {
		let eoChanges = changes['eo'];
		if (eoChanges) {
			let previousEO = changes['eo'].previousValue;

			if (previousEO && this.eoListener) {
				previousEO.removeListener(this.eoListener);
			}

			let eo: EntityObject = changes['eo'].currentValue;
			if (eo) {
				this.createListener();
				eo.addListener(this.eoListener);

				this.subscriptions.add(this.busyService.busy(this.updateLayout(eo)).subscribe());
			}
		}
	}

	submit() {
		// TODO: Submitting the form somehow also triggers adding of a new row in the subform
		this.onSubmit.next();
	}

	keydown($event: KeyboardEvent) {
		if ($event.key === 'Tab') {
			let srcElement = $event.srcElement;
			if (!srcElement) {
				return;
			}

			$event.preventDefault();
			$event.stopPropagation();

			if ($event.shiftKey) {
				this.tabToPrevious(srcElement);
			} else {
				this.tabToNext(srcElement);
			}
		}
	}

	private createListener() {
		// Create an Observable for the busy service
		let subscriber: Subscriber<any>;
		let saving: Observable<any> = new Observable(internalSubscriber => {
			subscriber = internalSubscriber;
		});

		this.eoListener = {
			afterLayoutChange: (entityObject: EntityObject, _layoutURL: string) => {
				this.subscriptions.add(
					this.busyService.busy(this.updateLayout(entityObject)).subscribe()
				);
			},
			isSaving: (_eo, inProgress) => {
				if (inProgress) {
					this.subscriptions.add(this.busyService.busy(saving).subscribe());
					subscriber.next(true);
				} else {
					subscriber.next(false);
					subscriber.complete();
				}
				Logger.instance.debug('Saving: %o', inProgress);
			}
		};
	}

	private updateLayout(eo: EntityObject): Observable<any> {
		return this.layoutService.getWebLayout(eo).pipe(
			tap(weblayout => {
				this.webLayout = weblayout;
				this.subscriptions.add(this.ruleService.updateRuleExecutor(eo).subscribe());
			})
		);
	}

	private tabToNext(srcElement: Element): boolean {
		// next-focus-field might be set only on some parent element (e.g. dropdown component),
		// such a parent element can be found via .closest()
		let nextFocusFieldName = $(srcElement)
			.closest('[next-focus-field]')
			.attr('next-focus-field');

		if (nextFocusFieldName) {
			let field = this.findFieldByName(nextFocusFieldName);

			if (field.length === 0) {
				this.$log.warn('Could not find next focus field for %o', nextFocusFieldName);
				return false;
			}

			for (let i = 0; i < field.length; i++) {
				if (this.canFocus(field.get(i))) {
					this.focus(field.get(i));
					return true;
				}
			}

			for (let i = 0; i < field.length; i++) {
				if (this.tabToNext(field.get(i))) {
					return true;
				}
			}

			return false;
		}
		return false;
	}

	private tabToPrevious(srcElement: Element): boolean {
		// Field might not have a name itself (e.g. the input of a dropdown), so we must look for the closes parent with a name
		let fieldName = $(srcElement)
			.closest('[name]')
			.attr('name');

		if (!fieldName) {
			this.$log.warn('Field has no name');
			return false;
		}

		let previousField = this.findFieldByNextField(fieldName);

		if (previousField.length === 0) {
			this.$log.warn('Could not find previous focus field for %o', fieldName);
			return false;
		}

		for (let i = previousField.length - 1; i >= 0; i--) {
			if (this.canFocus(previousField.get(i))) {
				this.focus(previousField.get(i));
				return true;
			}
		}

		for (let i = previousField.length - 1; i >= 0; i--) {
			if (this.tabToPrevious(previousField.get(i))) {
				return true;
			}
		}

		return false;
	}

	private focus(element) {
		// Simulate an outside click for previous element, because "blur" is not enough in some cases (dropdowns).
		this.clickOutsideServer.outsideClick(this.elementRef.nativeElement);

		element.focus();
	}

	private canFocus(element): boolean {
		let $el = $(element);
		if (
			(element.type === 'checkbox' && $el.is(':disabled')) ||
			(element.type !== 'checkbox' &&
				($el.is(':hidden') ||
					$el.is(':disabled') ||
					(this.browserDetectionService.isFirefox()
						? $el.is('input:-moz-read-only')
						: $el.is('input:read-only'))))
		) {
			return false;
		}

		let tabIndex = +$el.attr('tabindex');
		tabIndex = isNaN(tabIndex) ? -1 : tabIndex;
		return $el.is(':input, a[href], area[href], iframe') || tabIndex > -1;
	}

	private findFieldByName(fieldName: string) {
		return $(this.elementRef.nativeElement).find('[name="' + fieldName + '"]');
	}

	private findFieldByNextField(fieldName: string) {
		return $(this.elementRef.nativeElement).find('[next-focus-field="' + fieldName + '"]');
	}
}
