import { Injector } from '@angular/core';
import { ValidationStatus } from '../../validation/validation-status.enum';
import { AbstractWebComponent } from './abstract-web-component';

export abstract class AbstractInputComponent<T extends WebInputComponent> extends AbstractWebComponent<T> {

	private validationStatus: ValidationStatus | undefined;
	private cssClasses: any;

	constructor(
		protected injector: Injector
	) {
		super(injector);
	}

	isWritable(): boolean {
		return super.isWritable()
			&& this.webComponent && this.webComponent.enabled
			&& this.eo.isAttributeWritable(this.getName()!)
			&& this.eo.canWrite();
	}

	isInsertable(): boolean {
		return this.isWritable()
			&& this.webComponent && this.webComponent.insertable;
	}

	getValidationCssClasses(): any {
		let status = this.getValidationStatus();
		if (this.validationStatus === status) {
			return this.cssClasses;
		}

		this.validationStatus = status;
		let result: any = {
			'nuc-validation': true
		};

		if (status !== undefined) {
			let statusName = ValidationStatus[status].toLowerCase();
			result['nuc-validation-' + statusName] = true;
		}

		this.cssClasses = result;

		return result;
	}

	/**
	 * Gets the validation status based on the model value.
	 * May be overriden by 'INVALID' if the current input is invalid.
	 *
	 * @returns {ValidationStatus|undefined}
	 */
	getValidationStatus() {
		let status = this.eo.getValidationStatus(this.getName()!);

		if (this.isDirty() && !this.isValid()) {
			status = ValidationStatus.INVALID;
		}

		return status;
	}

	/**
	 * Determines if the current input value is valid.
	 * Additional constraints might be applied here (e.g. email format validation, etc.).
	 * Subclasses which do actual validation themselves should override this method.
	 *
	 * @returns {ValidationStatus|undefined}
	 */
	isValid(): boolean {
		return true;
	}

	isDirty() {
		let value = this.getModelValue();

		if (value === null || value === undefined) {
			return false;
		}

		// Reference attribute must have an ID
		if (typeof value === 'object') {
			if (!value.id) {
				return false;
			}
		}

		return true;
	}

	getModelValue() {
		return this.eo && this.eo.getAttribute(this.getName()!);
	}
}
