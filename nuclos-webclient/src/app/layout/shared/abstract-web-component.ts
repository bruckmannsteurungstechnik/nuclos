import { Injector, Input } from '@angular/core';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { IdFactoryService } from '../../shared/id-factory.service';

export abstract class AbstractWebComponent<T extends WebComponent> {
	@Input() eo: EntityObject;

	@Input() componentId: string | undefined;

	tooltip: string | undefined;
	@Input() protected webComponent: T;

	constructor(protected injector: Injector) {
		if (this.injector) {
			this.webComponent = this.injector.get('webComponent');
			this.eo = this.injector.get('eo');
			this.tooltip = this.getTooltip();
		}
	}

	isVisible(): boolean {
		return true;
	}

	isWritable(): boolean {
		return true;
	}

	isHidden() {
		return this.eo.isAttributeHidden(this.getAttributeName()!);
	}

	/**
	 * The ID of this component, to be used as HTML "id" attribute.
	 */
	getId(): string {
		if (!this.componentId) {
			this.componentId = this.getComponentIdOrNew();
		}

		return this.componentId;
	}

	setId(id: string) {
		this.componentId = id;
	}

	getName(): string | undefined {
		return this.webComponent && this.webComponent.name;
	}

	/**
	 * The ID of this component, to be used as HTML "name" attribute.
	 */
	getNameForHtml(): string | undefined {
		return 'attribute-' + this.getName();
	}

	getNextFocusField(): string | undefined {
		return this.webComponent && 'attribute-' + this.webComponent.nextFocusField;
	}

	getValue(): any {
		let result;

		let attributeName = this.getName();
		if (attributeName) {
			result = this.eo && this.eo.getAttribute(attributeName);
		}

		return result;
	}

	setValue(value: any) {
		let attributeName = this.getName();
		if (attributeName && this.eo) {
			this.eo.setAttribute(attributeName, value);
		}
	}

	getEntityObject(): EntityObject {
		return this.eo;
	}

	/**
	 * advanced properties from layout (Erweiterte Eigenschaften)
	 * @param key
	 * @return {any}
	 */
	getAdvancedProperty(key: string): string | undefined {
		if (!this.webComponent.advancedProperties) {
			return undefined;
		}
		let property = this.webComponent.advancedProperties.filter(p => p.name === key).shift();
		return property ? property.value : undefined;
	}

	private getComponentIdOrNew() {
		if (this.webComponent.id) {
			return 'component-' + this.webComponent.id;
		}

		let idFactory = this.injector.get(IdFactoryService);
		return 'generated-' + idFactory.getNextId();
	}

	private getAttributeName(): string | undefined {
		return this.webComponent && this.webComponent.name.replace('_', '');
	}

	private getTooltip(): string | undefined {
		return (
			this.webComponent &&
			this.webComponent.alternativeTooltip &&
			this.webComponent.alternativeTooltip.replace('\\n', '\n')
		);
	}
}
