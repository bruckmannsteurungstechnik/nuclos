import { Injectable } from '@angular/core';
import { Observable, of as observableOf } from 'rxjs';

import { concat, first, map, mergeMap } from 'rxjs/operators';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { Logger } from '../../log/shared/logger';
import { ILayoutInfo } from '../../perspective/layout-info';
import { PerspectiveModel } from '../../perspective/perspective-model';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { NuclosHttpService } from '../../shared/nuclos-http.service';

@Injectable()
export class LayoutService {
	/**
	 * Returns the Link of the default layout for the entity class of the given EO.
	 */
	static getDefaultWebLayoutURL(eo: EntityObject): Observable<string> {
		return eo.getDetailMeta().pipe(
			concat(eo.getMeta()),
			first(),
			map(meta => {
				let url;

				let link = meta.getLinks().defaultLayout;
				if (link && link.href) {
					url = link.href;
				}

				return url;
			})
		);
	}
	private perspectiveModel: PerspectiveModel | undefined;

	constructor(
		private http: NuclosHttpService,
		private config: NuclosConfigService,
		private $log: Logger
	) {}

	/**
	 * Determines the correct layout and fetches it.
	 */
	getWebLayout(eo: EntityObject): Observable<WebLayout | undefined> {
		return this.getWebLayoutURLDynamically(eo).pipe(
			mergeMap(url => {
				if (url) {
					return this.fetchWebLayout(url);
				} else {
					return observableOf(undefined);
				}
			})
		);
	}

	/**
	 * Determines the correct layout Link for the given EO.
	 * This is either the layout defined via a Link of the EO itself
	 * or the default layout of the entity class.
	 *
	 * @param eo
	 * @returns {any}
	 */
	getWebLayoutURLDynamically(eo: EntityObject) {
		let url = this.getWebLayoutURLFromPerspective(eo);

		if (!url) {
			url = eo.getLayoutURL();
		}

		if (url) {
			return observableOf(url);
		}

		return LayoutService.getDefaultWebLayoutURL(eo);
	}

	getWebLayoutURLFromPerspective(eo: EntityObject): string | undefined {
		if (!this.perspectiveModel) {
			return;
		}

		let perspective = this.perspectiveModel.selectedPerspective;
		if (!perspective || perspective.boMetaId !== eo.getEntityClassId()) {
			return;
		}

		if (perspective.boMetaId !== eo.getEntityClassId()) {
			this.$log.warn(
				'Selected perspective %o does not match selected entity object %o',
				perspective,
				eo
			);
			return;
		}

		if (eo.isNew() && !perspective.layoutForNew) {
			this.$log.debug('Skipping perspective layout for new EO');
			return;
		}

		let layoutId = perspective.layoutId;
		if (!layoutId) {
			return;
		}

		let layoutInfo = this.perspectiveModel.getLayoutInfo(layoutId);
		if (!layoutInfo) {
			return;
		}

		return this.getLayoutURLForFQN(layoutInfo.fqn);
	}

	getLayoutsForEntity(boMetaId: string): Observable<ILayoutInfo[]> {
		let url = this.config.getRestHost() + '/boMetas/' + boMetaId + '/layouts';
		return this.http.getCachedJSON(url);
	}

	getLayoutInfo(layoutId: string): Observable<ILayoutInfo> {
		let url = this.config.getRestHost() + '/layout/' + layoutId + '/info';
		return this.http.getCachedJSON(url);
	}

	/**
	 * Returns the complete URL for the given layout FQN.
	 */
	getLayoutURLForFQN(layoutFQN: string) {
		// TODO: Use HATEOS or some kind of layout registry to lookup the layout URL.
		return this.config.getRestHost() + '/layout/' + layoutFQN + '/calculated';
	}

	setPerspectiveModel(model: PerspectiveModel) {
		this.perspectiveModel = model;
	}

	/**
	 * Fetches the WebLayout from the given Link.
	 *
	 * TODO: Always returns the "fixed" layout. Maybe in a later step the webclient
	 * should dynamically switch to a responsive layout for mobile devices.
	 */
	private fetchWebLayout(url: string): Observable<WebLayout | undefined> {
		return this.http.getCachedJSON(url);
	}
}
