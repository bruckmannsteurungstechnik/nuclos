import { Component, Injector, OnInit, ReflectiveInjector, ViewChild, ViewContainerRef } from '@angular/core';
import { LAYOUT_CONTEXT } from '@nuclos/nuclos-addon-api';
import { LayoutContextImplementation } from '../../addons/addon-api-implementation';
import { AddonService } from '../../addons/addon.service';
import { Logger } from '../../log/shared/logger';
import { AbstractWebComponent } from '../shared/abstract-web-component';

@Component({
	selector: 'nuc-web-addon',
	templateUrl: './web-addon.component.html',
	styleUrls: ['./web-addon.component.css']
})
export class WebAddonComponent extends AbstractWebComponent<WebAddon> implements OnInit {

	@ViewChild('dynamicComponentContainer', {read: ViewContainerRef})
	dynamicComponentContainer: ViewContainerRef;

	constructor(injector: Injector,
				private $log: Logger,
				private layoutContextImplementation: LayoutContextImplementation,
				private addonService: AddonService) {
		super(injector);
	}


	ngOnInit() {

		// create a new object from the layoutContextImplementation singleton to make it possible to use multiple addon instances
		let layoutContextImplementation = {} as LayoutContextImplementation;
		// tslint:disable-next-line
		for (let property in this.layoutContextImplementation) {
			layoutContextImplementation[property] = this.layoutContextImplementation[property];
		}
		layoutContextImplementation.setComponent(this);


		let inputProviders = [
			{ provide: LAYOUT_CONTEXT, useValue: layoutContextImplementation },
		];
		let resolvedInputs = ReflectiveInjector.resolve(inputProviders);

		// injector provides the input data to the new component
		let injector = ReflectiveInjector.fromResolvedProviders(resolvedInputs, this.dynamicComponentContainer.parentInjector);

		let addonName = this.getName()!.replace('component-', '');

		let addonComponentName = addonName + 'Component';
		let addonComponentRef = this.addonService.instantiateComponent(addonComponentName, injector);
		if (addonComponentRef) {
			this.dynamicComponentContainer.insert(addonComponentRef!.hostView);
		} else {
			this.$log.error('Unable to instantiate addon \'%s\'.', addonName);
		}

	}

}
