import {
	Component,
	ComponentFactoryResolver,
	Injector,
	Type,
	ViewChild,
	ViewContainerRef
} from '@angular/core';
import { WebButtonComponent } from '../web-button.component';
import { AddonService } from '../../../addons/addon.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DialogService } from '../../../popup/dialog/dialog.service';
import { Logger } from '../../../log/shared/logger';
import { ElementData } from '@angular/core/src/view';

@Component({
	selector: 'nuc-web-button-addon',
	templateUrl: '../web-button.component.html',
	styleUrls: ['../web-button.component.css']
})
export class WebButtonAddonComponent extends WebButtonComponent<WebButtonAddon> {
	@ViewChild('addonViewContainerDummy', { read: ViewContainerRef })
	addonViewContainerDummyRef: ViewContainerRef;

	constructor(
		injector: Injector,
		private addonService: AddonService,
		private dialogService: DialogService,
		private ngbModalService: NgbModal,
		private componentFactoryResolver: ComponentFactoryResolver,
		private $log: Logger
	) {
		super(injector);
	}

	buttonClicked() {
		this.instantiateAddonComponent();
	}

	getCssClass(): string {
		return 'nuc-button-addon';
	}
	isEnabled(): boolean {
		return (
			super.isEnabled() &&
			this.eo &&
			!this.eo.isNew() &&
			this.eo.isAttributeWritable(this.webComponent.name) &&
			this.isWritable() &&
			this.eo.canWrite()
		);
	}

	/**
	 * if addon component uses a template the addon will be opened in a modal
	 * otherwise it will be instantiated invisible
	 */
	private instantiateAddonComponent() {
		const addonComponent = this.addonService.getComponentFactoryClass(
			this.webComponent.addonComponentName + 'Component'
		);
		if (addonComponent) {
			const addonComponentFactory = this.componentFactoryResolver.resolveComponentFactory(
				addonComponent as Type<Component>
			);

			const injector = Injector.create({
				providers: [
					{
						provide: addonComponent,
						useClass: addonComponent,
						deps: []
					}
				]
			});
			const cmpRef = addonComponentFactory.create(injector);

			// TODO find a better way to check if component has a template
			const elementData: ElementData = (<any>cmpRef.hostView)['_view'].nodes[0];
			const emptyTemplate = Object.keys(elementData.componentView.nodes).length === 0;
			let componentInstance;
			if (emptyTemplate) {
				// instantiate invisible
				this.addonViewContainerDummyRef.insert(cmpRef.hostView);
				componentInstance = cmpRef.instance;
				componentInstance.eo = this.eo;
			} else {
				// open addon in modal
				const ngbModalRef = this.ngbModalService.open(addonComponent, {
					size: 'lg',
					windowClass: 'fullsize-modal-window'
				});

				ngbModalRef.componentInstance.ngbModalRef = ngbModalRef;
				ngbModalRef.componentInstance.eo = this.eo;
			}
		} else {
			const message = 'Unable to open addon: ' + this.webComponent.addonComponentName;
			this.$log.error(message);
			this.dialogService.alert({ title: 'Error', message: message });
		}
	}
}
