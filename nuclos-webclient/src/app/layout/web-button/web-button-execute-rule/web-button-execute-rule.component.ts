import { Component, Injector } from '@angular/core';
import { take } from 'rxjs/operators';
import { WebButtonComponent } from '../web-button.component';

@Component({
	selector: 'nuc-web-button-execute-rule',
	templateUrl: '../web-button.component.html',
	styleUrls: ['../web-button.component.css']
})
export class WebButtonExecuteRuleComponent extends WebButtonComponent<WebButtonExecuteRule> {

	constructor(
		injector: Injector
	) {
		super(injector);
	}

	buttonClicked() {
		this.eo.executeRule(this.webComponent.rule).pipe(take(1)).subscribe();
	}

	getCssClass(): string {
		return 'nuc-button-execute-rule';
	}

	isEnabled(): boolean {
		return super.isEnabled() && this.eo && !this.eo.isNew()
			&& this.eo.isAttributeWritable(this.webComponent.name) && this.isWritable() && this.eo.canWrite();

	}
}

