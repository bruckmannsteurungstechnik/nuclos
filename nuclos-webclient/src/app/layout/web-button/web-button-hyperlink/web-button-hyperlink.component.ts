import { Component, Injector } from '@angular/core';
import { HyperlinkService } from '../../../shared/hyperlink.service';
import { WebButtonComponent } from '../web-button.component';

@Component({
	selector: 'nuc-web-button-hyperlink',
	templateUrl: '../web-button.component.html',
	styleUrls: ['../web-button.component.css']
})
export class WebButtonHyperlinkComponent extends WebButtonComponent<WebButtonHyperlink> {
	constructor(
		injector: Injector,
		private hyperlinkService: HyperlinkService
	) {
		super(injector);
	}

	getCssClass(): string {
		return 'nuc-button-hyperlink';
	}

	buttonClicked() {
		this.openLink();
	}

	openLink() {
		let url = this.getUrl();
		if (url) {
			this.hyperlinkService.open(url);
		}
	}

	getUrl(): string {
		let url = this.getHyperlinkAttribute();

		if (url) {
			url = this.hyperlinkService.validateURL(url);
		}

		return url;
	}

	private getHyperlinkAttribute(): any {
		return this.eo.getAttribute(this.webComponent.hyperlinkField);
	}
}
