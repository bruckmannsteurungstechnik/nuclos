import { Component, Injector, OnInit } from '@angular/core';
import * as tinycolor from 'tinycolor2';
import { AbstractInputComponent } from '../shared/abstract-input-component';

@Component({
	selector: 'nuc-web-colorchooser',
	templateUrl: './web-colorchooser.component.html',
	styleUrls: ['./web-colorchooser.component.css']
})
export class WebColorchooserComponent extends AbstractInputComponent<WebColorchooser> implements OnInit {

	value;
	parsedValue;

	constructor(
		injector: Injector
	) {
		super(injector);
	}

	ngOnInit() {
	}

	isWritable() {
		return super.isWritable();
	}

	getValue(): any {
		// If the value has changed (e.g. because a different EO was selected), parse it again.
		if (this.value !== super.getValue()) {
			this.value = super.getValue();
			this.parsedValue = this.parseValue(super.getValue());
		}

		return this.parsedValue;
	}

	setValue(value) {
		value = this.parseValue(value);

		super.setValue(value);
	}

	private parseValue(value) {
		let color = tinycolor(value);

		if (color.isValid()) {
			return color.toHexString();
		}

		color = tinycolor('rgb(' + value + ')');
		if (color.isValid()) {
			return color.toHexString();
		}

		return '';
	}
}
