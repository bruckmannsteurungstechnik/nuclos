/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WebComboboxComponent } from './web-combobox.component';

xdescribe('WebComboboxComponent', () => {
	let component: WebComboboxComponent;
	let fixture: ComponentFixture<WebComboboxComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [WebComboboxComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebComboboxComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
