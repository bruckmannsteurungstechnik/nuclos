import { Component, ElementRef, Injector, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';
import { FqnService } from '../../shared/fqn.service';
import { AbstractLovComponent } from '../abstract-lov/abstract-lov.component';
import { ComboboxUtils } from '../shared/combobox-utils';

@Component({
	selector: 'nuc-web-combobox',
	templateUrl: '../abstract-lov/abstract-lov.component.html',
	styleUrls: ['../abstract-lov/abstract-lov.component.scss']
})
export class WebComboboxComponent extends AbstractLovComponent<WebCombobox> implements OnInit {

	constructor(
		injector: Injector,
		fqnService: FqnService,
		elementRef: ElementRef
	) {
		super(injector, fqnService, elementRef);

		this.handler = {
			loadEntries: () => this.loadEntries(),
			loadFilteredEntries: search => this.loadFilteredEntries(search),
			getValue: () => this.getValue(),
			refreshEntries: () => this.doRefreshEntries()
		};
	}

	ngOnInit() {
		super.ngOnInit();
	}

	loadEntries() {
		return this.eo.getLovEntries(
			this.attributeMeta.getAttributeID(),
			this.webComponent.valuelistProvider
		).pipe(take(1));
	}

	loadFilteredEntries(search: string) {
		return ComboboxUtils.filter(this.loadEntries(), search);
	}

	private doRefreshEntries() {
		this.eo.refreshVlp(
			this.getAttributeMeta().getAttributeID(),
			this.webComponent.valuelistProvider
		).pipe(take(1)).subscribe();
	}
}
