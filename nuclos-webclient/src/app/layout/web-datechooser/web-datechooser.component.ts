import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { DatechooserComponent } from '../../ui-components/datechooser/datechooser.component';
import { AbstractInputComponent } from '../shared/abstract-input-component';

@Component({
	selector: 'nuc-web-datechooser',
	templateUrl: './web-datechooser.component.html',
	styleUrls: ['./web-datechooser.component.css']
})
export class WebDatechooserComponent extends AbstractInputComponent<WebDatechooser> implements OnInit {

	@Output() onValueChange = new EventEmitter();

	@ViewChild('nucDatechooser') datechooser: DatechooserComponent;

	constructor(
		injector: Injector
	) {
		super(injector);
	}

	ngOnInit() {
	}

	setAttributeValue(value?: string) {
		this.eo.setAttribute(this.webComponent.name, value);
		this.onValueChange.next();
	}

	focusInput() {
		this.datechooser.focusInput();
	}

	selectInputText() {
		this.datechooser.selectInputText();
	}

	toggleDatepicker() {
		this.datechooser.toggleDatepicker();
	}

	setInputText(value: string) {
		this.datechooser.setInputText(value);
	}

	commitValue() {
		this.datechooser.commitValue();
	}

	getModel() {
		return this.getModelValue();
	}

	isValid() {
		return this.datechooser && this.datechooser.isValid();
	}

	getRawInput() {
		return this.datechooser.getRawInput();
	}

	getValidationCssClasses(): any {
		return super.getValidationCssClasses();
	}
}
