/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WebHyperlinkComponent } from './web-hyperlink.component';

xdescribe('WebHyperlinkComponent', () => {
	let component: WebHyperlinkComponent;
	let fixture: ComponentFixture<WebHyperlinkComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [WebHyperlinkComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebHyperlinkComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
