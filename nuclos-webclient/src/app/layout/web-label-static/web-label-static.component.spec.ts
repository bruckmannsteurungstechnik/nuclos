/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WebLabelStaticComponent } from './web-label-static.component';

xdescribe('WebLabelStaticComponent', () => {
	let component: WebLabelStaticComponent;
	let fixture: ComponentFixture<WebLabelStaticComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [WebLabelStaticComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebLabelStaticComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
