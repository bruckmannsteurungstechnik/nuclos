import { Component, Injector, OnInit } from '@angular/core';
import { AbstractLabelComponent } from '../shared/abstract-label-component';

@Component({
	selector: 'nuc-web-label-static',
	templateUrl: './web-label-static.component.html',
	styleUrls: ['./web-label-static.component.css']
})
export class WebLabelStaticComponent extends AbstractLabelComponent<WebLabelStatic> implements OnInit {

	constructor(injector: Injector) {
		super(injector);
	}

	ngOnInit() {
	}

	getText() {
		return this.webComponent.text;
	}
}
