import { Component } from '@angular/core';
import { take } from 'rxjs/operators';
import { LovDataService } from '../../../entity-object-data/shared/lov-data.service';
import { BrowserRefreshService } from '../../../shared/browser-refresh.service';
import { FqnService } from '../../../shared/fqn.service';
import { AbstractReferenceTargetComponent } from '../abstract-reference-target/abstract-reference-target.component';

@Component({
	selector: 'nuc-add-reference-target',
	templateUrl: './add-reference-target.component.html',
	styleUrls: ['./add-reference-target.component.css']
})
export class AddReferenceTargetComponent extends AbstractReferenceTargetComponent {
	constructor(
		protected browserRefreshService: BrowserRefreshService,
		protected fqnService: FqnService,
		protected lovDataService: LovDataService
	) {
		super(browserRefreshService, fqnService, lovDataService);
	}

	addReference($event: MouseEvent) {
		if (this.lovHandler.stopEditing) {
			this.lovHandler.stopEditing();
		}
		this.targetReference('new', true, false, this.getVlpId(), undefined);
		$event.stopPropagation();
	}

	protected updateVisibility() {
		if (!this.attributeMeta || !this.attributeMeta.isReference() || !this.eo) {
			this.isVisible = false;
			return;
		}

		this.lovDataService
			.canAddReference(this.eo, this.attributeMeta)
			.pipe(take(1))
			.subscribe(canAddReference => (this.isVisible = canAddReference));
	}
}

