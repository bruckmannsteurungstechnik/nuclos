import { Component, Input } from '@angular/core';
import { LovEntry } from '../../../entity-object-data/shared/bo-view.model';
import { LovDataService } from '../../../entity-object-data/shared/lov-data.service';
import { BrowserRefreshService } from '../../../shared/browser-refresh.service';
import { FqnService } from '../../../shared/fqn.service';
import { AbstractReferenceTargetComponent } from '../abstract-reference-target/abstract-reference-target.component';

@Component({
	selector: 'nuc-edit-reference-target',
	templateUrl: './edit-reference-target.component.html',
	styleUrls: ['./edit-reference-target.component.css']
})
export class EditReferenceTargetComponent extends AbstractReferenceTargetComponent {
	@Input() private attribute: LovEntry | undefined;

	constructor(
		protected browserRefreshService: BrowserRefreshService,
		protected fqnService: FqnService,
		protected lovDataService: LovDataService
	) {
		super(browserRefreshService, fqnService, lovDataService);
	}

	editReference($event: MouseEvent) {
		if (this.attribute && this.attribute.id) {
			this.targetReference(this.attribute.id, true, true, this.getVlpId(), undefined);
		}
		$event.stopPropagation();
	}

	protected updateVisibility() {
		let bVisible =
			this.attribute !== undefined &&
			this.attribute !== null &&
			this.attribute.id !== undefined &&
			this.attribute.id !== null;
		if (!bVisible) {
			this.isVisible = false;
			return;
		}
		super.updateVisibility();
	}
}
