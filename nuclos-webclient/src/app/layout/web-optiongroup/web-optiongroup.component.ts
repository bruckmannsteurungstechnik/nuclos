import { Component, Injector, OnChanges, OnInit } from '@angular/core';
import { AbstractInputComponent } from '../shared/abstract-input-component';

@Component({
	selector: 'nuc-web-optiongroup',
	templateUrl: './web-optiongroup.component.html',
	styleUrls: ['./web-optiongroup.component.css']
})
export class WebOptiongroupComponent extends AbstractInputComponent<WebOptiongroup>
	implements OnInit, OnChanges {
	constructor(injector: Injector) {
		super(injector);
	}

	ngOnInit() {
		this.setDefaultIfNull();
	}

	ngOnChanges() {
		this.setDefaultIfNull();
	}

	getOptions() {
		return this.webComponent.options;
	}

	private setDefaultIfNull() {
		if (!this.getValue()) {
			this.setValue(this.webComponent.options.default);
		}
	}
}
