import { Component, Injector, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { AbstractWebComponent } from '../shared/abstract-web-component';

@Component({
	selector: 'nuc-web-panel',
	templateUrl: './web-panel.component.html',
	styleUrls: ['./web-panel.component.css']
})
export class WebPanelComponent extends AbstractWebComponent<WebPanel> implements OnInit, OnChanges {
	titleStyle;
	panelStyle;
	panelClasses;

	constructor(injector: Injector) {
		super(injector);
	}

	ngOnInit() {
		this.updateStyles();
	}

	ngOnChanges(changes: SimpleChanges): void {
		this.updateStyles();
	}

	getTitle() {
		return this.webComponent && this.webComponent.title;
	}

	getWebComponent() {
		return this.webComponent;
	}

	private updateStyles() {
		this.panelClasses = {
			'panel-content': true,
			opaque: this.isOpaque(),
			'with-title': this.getTitle()
		};

		this.panelStyle = {
			'background-color': this.webComponent.backgroundColor,
			'border-width': this.webComponent.borderWidth,
			'border-color': this.webComponent.borderColor
		};
		this.titleStyle = {
			'background-color': this.webComponent.backgroundColor
		};
	}

	private isOpaque() {
		return this.webComponent && this.webComponent.opaque;
	}
}
