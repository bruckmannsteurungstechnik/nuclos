import { Component, Injector, OnInit } from '@angular/core';
import { AbstractInputComponent } from '../shared/abstract-input-component';

@Component({
	selector: 'nuc-web-password',
	templateUrl: './web-password.component.html',
	styleUrls: ['./web-password.component.css']
})
export class WebPasswordComponent extends AbstractInputComponent<WebPassword> implements OnInit {

	constructor(injector: Injector) {
		super(injector);
	}

	ngOnInit() {
	}

}
