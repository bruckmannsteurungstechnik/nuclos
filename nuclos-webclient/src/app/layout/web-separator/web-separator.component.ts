import { Component, Injector, OnInit } from '@angular/core';
import { AbstractWebComponent } from '../shared/abstract-web-component';

@Component({
	selector: 'nuc-web-separator',
	templateUrl: './web-separator.component.html',
	styleUrls: ['./web-separator.component.css']
})
export class WebSeparatorComponent extends AbstractWebComponent<WebSeparator> implements OnInit {

	constructor(injector: Injector) {
		super(injector);
	}

	ngOnInit() {
	}

	getOrientation() {
		return this.webComponent.orientation;
	}

}
