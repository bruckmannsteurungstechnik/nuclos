import { ElementRef } from '@angular/core';
import { IEntityObject } from '@nuclos/nuclos-addon-api';
import { take } from 'rxjs/operators';
import { ColDef, GridApi } from 'ag-grid';
import { EntityObjectEventService } from '../../entity-object-data/shared/entity-object-event.service';
import { EntityObject, SubEntityObject } from '../../entity-object-data/shared/entity-object.class';
import { EntityObjectService } from '../../entity-object-data/shared/entity-object.service';
import { NuclosCellParams } from './web-subform.model';
import { WebSubformService } from './web-subform.service';

export abstract class AbstractCellComponent<T extends NuclosCellParams> {
	columnWidth;
	rowHeight;

	protected params: T;
	private eo: IEntityObject | undefined;

	constructor(
		protected entityObjectService: EntityObjectService,
		protected eoEventService: EntityObjectEventService,
		protected ref: ElementRef | undefined
	) {}

	init(params) {
		this.eoEventService.observeSelectedEo().pipe(take(1)).subscribe(eo => {
			this.eo = eo;
		});
		this.params = params;
	}

	getParam(key: string) {
		return this.params[key];
	}

	/**
	 * Returns the actual column width in pixel.
	 */
	getWidth() {
		return this.columnWidth;
	}

	/**
	 * Returns the (fixed) row height in pixels.
	 */
	getHeight() {
		let fixedHeight = this.getParam('fixedHeight');

		if (!fixedHeight) {
			fixedHeight = WebSubformService.defaultLineHeightInPx;
		}

		// Total column height minus inline border width
		return fixedHeight;
	}

	updateParams(params) {
		this.params = params;
	}

	setEntityObject(entityObject: EntityObject) {
		this.eo = entityObject;
	}

	getEntityObject(): IEntityObject | undefined {
		return this.eo;
	}

	/**
	 * @deprecated The EO is not really dirty and should therefore not be marked as dirty.
	 */
	setEntityObjectDirty(): void {
		if (this.eo) {
			(this.eo as EntityObject).setDirty(true);
		}
		this.addModifiedClassToRow();
	}

	getValue() {
		return this.params ? this.params.value : undefined;
	}

	setValue(value) {
		if (value !== this.params.value && !this.isEqualNumber(value, this.params.value)) {
			this.params.value = value;
			this.setEntityObjectDirty();
		}
	}

	getGridApi(): GridApi {
		return this.params.api;
	}

	getSubEntityObject(): SubEntityObject {
		return this.params.data || this.params.node.data;
	}

	getColDef(): ColDef {
		return this.params.colDef;
	}

	isPopup() {
		return true;
	}

	/**
	 * Adds the "row-modified" class to all cells of the current row.
	 */
	protected addModifiedClassToRow() {
		if (this.ref) {
			let rowIndex = this.params.node.rowIndex;

			let body = $(this.ref.nativeElement)
				.closest('ag-grid-angular')
				.find('.ag-body');

			body.find('[row-index=' + rowIndex + '] .ag-cell').addClass('row-modified');
		}
	}

	private isEqualNumber(value1: any, value2: any) {
		if (typeof value1 === 'number' || typeof value2 === 'number') {
			return '' + value1 === '' + value2;
		}
		return false;
	}
}
