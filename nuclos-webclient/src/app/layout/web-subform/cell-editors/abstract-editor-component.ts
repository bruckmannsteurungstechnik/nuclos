import { ElementRef } from '@angular/core';
import { AgEditorComponent } from 'ag-grid-angular';
import { StartEditingCellParams } from 'ag-grid/dist/lib/gridApi';
import { EntityAttrMeta } from '../../../entity-object-data/shared/bo-view.model';
import { EntityObjectEventService } from '../../../entity-object-data/shared/entity-object-event.service';
import { EntityObjectService } from '../../../entity-object-data/shared/entity-object.service';
import { AbstractCellComponent } from '../abstract-cell-component';
import { NuclosCellEditorParams } from '../web-subform.model';

export abstract class AbstractEditorComponent extends AbstractCellComponent<NuclosCellEditorParams>
	implements AgEditorComponent {
	value;
	protected attributeMeta: EntityAttrMeta;

	constructor(
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService,
		protected ref: ElementRef
	) {
		super(entityObjectService, eoEventService, ref);
	}

	agInit(params: NuclosCellEditorParams) {
		super.init(params);

		this.attributeMeta = params.attrMeta;
		this.columnWidth = params.column.getActualWidth();

		if (this.ref) {
			this.ref.nativeElement.setAttribute('for-cell', this.getCellCompId());
		}

		setTimeout(() => {
			if (this.ref) {
				let input = $(this.ref.nativeElement).find('input');
				if (params.charPress) {
					input.focus();
				} else {
					input.select();
				}
			}
		});
	}

	getVlp(): WebValuelistProvider | undefined {
		return this.getParam('valuelistProvider');
	}

	keydown(event: KeyboardEvent): void {
		let key = event.which || event.keyCode;
		if (
			key === 37 || // left
			key === 39
		) {
			// right
			event.stopPropagation();
		} else if (event.code === 'Tab') {
			event.preventDefault();
			event.stopPropagation();
			this.editNextCell(event.shiftKey);
		}
	}

	getAttributeName() {
		return this.attributeMeta.getAttributeName();
	}

	getCellCompId() {
		if (!this.params) {
			return '';
		}

		return this.params.eGridCell.attributes['comp-id'].nodeValue;
	}

	protected editNextCell(shift: boolean) {
		let tabbed;
		if (shift) {
			tabbed = this.getGridApi().tabToPreviousCell();
		} else {
			tabbed = this.getGridApi().tabToNextCell();
		}

		if (tabbed) {
			let gridCell = this.getGridApi().getFocusedCell();
			let editParams: StartEditingCellParams = {
				rowIndex: gridCell.rowIndex,
				colKey: gridCell.column
			};
			this.getGridApi().startEditingCell(editParams);
		} else {
			this.reFocusCurrentCell();
			this.getGridApi().stopEditing();
		}
	}

	protected reFocusCurrentCell() {
		let gridCell = this.getGridApi().getFocusedCell();
		if (gridCell) {
			this.getGridApi().setFocusedCell(gridCell.rowIndex, gridCell.column);
		}
	}
}
