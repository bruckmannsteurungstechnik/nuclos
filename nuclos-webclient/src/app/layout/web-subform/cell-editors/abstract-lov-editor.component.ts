///<reference path="abstract-editor-component.ts"/>
import { HttpParams } from '@angular/common/http';
import { ElementRef, Injector, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { EntityAttrMeta, LovEntry } from '../../../entity-object-data/shared/bo-view.model';
import { EntityObjectEventService } from '../../../entity-object-data/shared/entity-object-event.service';
import { SubEntityObject } from '../../../entity-object-data/shared/entity-object.class';
import { EntityObjectService } from '../../../entity-object-data/shared/entity-object.service';
import { LovDataService } from '../../../entity-object-data/shared/lov-data.service';
import { LovSearchConfig } from '../../../entity-object-data/shared/lov-search-config';
import { FqnService } from '../../../shared/fqn.service';
import { IdFactoryService } from '../../../shared/id-factory.service';
import { NuclosDefaults } from '../../../shared/nuclos-defaults';
import { AbstractLovComponent } from '../../abstract-lov/abstract-lov.component';
import { LovHandler } from '../../abstract-lov/lov-handler';
import { NuclosCellEditorParams } from '../web-subform.model';
import { AbstractEditorComponent } from './abstract-editor-component';

export const AUTOCOMPLETE_PANEL_SELECTOR = 'p-autocomplete .ui-autocomplete-panel';

export abstract class AbstractLovEditorComponent extends AbstractEditorComponent {
	/**
	 * Keys which must be caught (and not propagated) by the dropdown component,
	 * because they also trigger navigation in the subform.
	 */
	private static CATCH_KEYS = ['ArrowDown', 'ArrowUp', 'Enter'];

	lovEntries: LovEntry[] = [];
	subEO: SubEntityObject;
	advancedProperties: WebAdvancedProperty[];

	@ViewChild('lovComponent') lovComponent: AbstractLovComponent<WebInputComponent>;

	protected lovHandler: LovHandler;

	protected attribute: any;

	private staticValues: boolean;

	private componentId;
	constructor(
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService,
		protected elementRef: ElementRef,
		protected idFactory: IdFactoryService,
		protected fqnService: FqnService,
		protected injector: Injector
	) {
		super(entityObjectService, eoEventService, elementRef);

		this.lovHandler = {
			loadEntries: () => this.loadEntries(),
			loadFilteredEntries: search => this.loadFilteredEntries(search),
			getValue: () => this.getValue(),
			clearValue: () => (this.params.value = undefined),
			getWidth: () => this.getWidth(),
			getHeight: () => this.getHeight(),
			stopEditing: () => this.stopEditing(),
			searchReferenceClick: () => this.stopEditing(),
			addReferenceClick: () => this.stopEditing(),
			entrySelected: value => this.selectDropdownOption(value),
			isEnabled: () => true,
			keydownHandler: event => this.handleKey(event),
			getVlpId: () => this.getVlpId(),
			getVlpParams: () => this.getVlpParams()
		};

		// when using the dropdown in the primeng p-autocomplete component the input element will be focused when clicking on the dropdown
		// this behaviour will always set back the focus to the input when trying to leave the input with a keyboard tab
		// disabling the dropdown button fixes this
		setTimeout(() => {
			let dropdownButton = this.elementRef.nativeElement.querySelector('button');
			if (dropdownButton) {
				$(dropdownButton).prop('disabled', true);
			}
		});
	}

	getVlpParams(): HttpParams {
		let lovDataService = this.getLovDataService();
		if (lovDataService) {
			return lovDataService.getLovParams(this.getLovSearchConfig(''));
		} else {
			return new HttpParams();
		}
	}

	getVlpId(): string | undefined {
		if (this.getVlp()) {
			return this.getVlp()!.value;
		}
		return undefined;
	}

	getLovSearchConfig(search: string): LovSearchConfig {
		return {
			attributeMeta: this.attributeMeta,
			eo: this.subEO,
			quickSearchInput: search,
			vlp: this.getVlp(),
			resultLimit: NuclosDefaults.DROPDOWN_SHOW_RESULT_LIMIT,
			mandatorId: this.getEntityObject()
				.getRootEo()
				.getMandatorId()
		};
	}

	abstract getLovDataService(): LovDataService | undefined;

	abstract loadEntries(): Observable<LovEntry[]>;

	abstract loadFilteredEntries(search: string): Observable<LovEntry[]>;

	getHandler() {
		return this.lovHandler;
	}

	getEntityObject() {
		return this.subEO;
	}

	agInit(params: NuclosCellEditorParams) {
		super.agInit(params);

		let attributeName = this.attributeMeta.getAttributeName();
		if (attributeName !== undefined && this.subEO !== undefined) {
			this.attribute = this.subEO.getAttribute(attributeName);
		}

		this.rowHeight = params.node.rowHeight + 'px';
		this.subEO = params.api.getModel().getRow(params.rowIndex).data;

		if (params.staticList) {
			this.lovEntries = params.staticList;
			this.staticValues = true;
		}

		window.setTimeout(() => {
			// fix wrong popup position for small subforms
			let popupEditor = this.elementRef.nativeElement.parentNode;
			let popupEditorPosition = $(popupEditor).position();
			if (popupEditorPosition && popupEditorPosition.top < 0) {
				let top = (params.rowIndex + 1) * params.node.rowHeight;
				$(popupEditor).css({ top: top + 'px' });
			}

			// open dropdown when starting edit
			this.lovComponent.showAllEntries();
			this.lovComponent.getAutocomplete().focus = true;
			this.lovComponent.getAutocomplete().focusInput();

			// select lov input text
			let textInput = this.lovComponent.getAutocomplete().inputEL.nativeElement;
			if (textInput.selectionEnd - textInput.selectionStart === 0) {
				textInput.setSelectionRange(0, textInput.value.length);
			}
		});

		this.advancedProperties = params.advancedProperties;
	}

	getName() {
		if (!this.attributeMeta) {
			return undefined;
		}

		let fieldName = this.fqnService.getShortAttributeName(
			this.subEO.getEntityClassId(),
			this.attributeMeta.getAttributeID()
		);
		return fieldName;
	}

	getNameForHtml() {
		let result = this.getName();

		if (result) {
			result = 'attribute-' + result;
		}

		return result;
	}

	/**
	 * The ID of this component, to be used as HTML "id" attribute.
	 */
	getId(): string {
		if (!this.componentId) {
			this.componentId = this.getComponentIdOrNew();
		}

		return this.componentId;
	}

	isPopup() {
		return true;
	}

	selectDropdownOption(value) {
		let v = this.getValue();
		if (!v || v.id !== value.id) {
			this.setValue(value);
		}

		this.getGridApi().stopEditing();

		// set focus to current cell after stopEditing
		super.reFocusCurrentCell();
	}

	getAttributeMeta(): EntityAttrMeta {
		return this.attributeMeta;
	}

	getAttribute(): any {
		return this.attribute;
	}

	stopEditing(): void {
		this.getGridApi().stopEditing();
	}

	protected getAutocomplete() {
		return this.lovComponent.getAutocomplete();
	}

	private getComponentIdOrNew() {
		return 'generated-' + this.idFactory.getNextId();
	}

	private handleKey(event: KeyboardEvent) {
		if (AbstractLovEditorComponent.CATCH_KEYS.indexOf(event.code) >= 0) {
			event.preventDefault();
			event.stopPropagation();
		}

		super.keydown(event);
	}
}
