import { Component, ElementRef, ViewChild } from '@angular/core';
import { WebDatechooserComponent } from '../../../web-datechooser/web-datechooser.component';
import { EntityObjectEventService } from '../../../../entity-object-data/shared/entity-object-event.service';
import { EntityObjectService } from '../../../../entity-object-data/shared/entity-object.service';
import { FqnService } from '../../../../shared/fqn.service';
import { AbstractEditorComponent } from '../abstract-editor-component';

@Component({
	selector: 'nuc-subform-date-editor',
	templateUrl: 'subform-date-editor.component.html',
	styleUrls: ['subform-date-editor.component.css']
})
export class SubformDateEditorComponent extends AbstractEditorComponent {
	@ViewChild('nuclosDatechooser') nuclosDatechooser: WebDatechooserComponent;

	constructor(
		private fqnService: FqnService,
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService,
		private elementRef: ElementRef
	) {
		super(entityObjectService, eoEventService, elementRef);
	}

	agInit(params: any) {
		super.agInit(params);

		setTimeout(() => {
			this.nuclosDatechooser.focusInput();
			this.nuclosDatechooser.selectInputText();
			if (params.charPress) {
				this.nuclosDatechooser.setInputText(params.charPress);
			}
		});
	}

	getName() {
		if (!this.attributeMeta) {
			return undefined;
		}

		let eo = this.getSubEntityObject();
		if (!eo) {
			return;
		}
		let fieldName = this.fqnService.getShortAttributeName(
			eo.getEntityClassId(),
			this.attributeMeta.getAttributeID()
		);
		return fieldName;
	}

	valueChanged() {
		this.updateStringValue();
	}

	keydown(event: KeyboardEvent) {
		if (event.code === 'Enter' || event.code === 'Tab' || event.code === 'Escape') {
			this.nuclosDatechooser.commitValue();
		}

		if (event.code === 'Enter') {
			this.reFocusCurrentCell();
		}

		super.keydown(event);
	}

	isPopup(): boolean {
		// If this is a popup editor, we get problems with click-outside behavior.
		// See NUCLOS-7760
		return false;
	}

	private updateStringValue() {
		let value = this.nuclosDatechooser.getValue();
		let stringValue: string | null = null;

		let m = moment(value);
		if (m.isValid()) {
			stringValue = m.format('YYYY-MM-DD');
		}

		this.setValue(stringValue);
	}
}
