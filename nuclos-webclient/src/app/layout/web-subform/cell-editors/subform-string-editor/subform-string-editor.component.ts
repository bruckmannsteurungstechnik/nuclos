import { Component, ElementRef, ViewChild } from '@angular/core';
import { EntityObjectEventService } from '../../../../entity-object-data/shared/entity-object-event.service';
import { EntityObjectService } from '../../../../entity-object-data/shared/entity-object.service';
import { Logger } from '../../../../log/shared/logger';
import { NuclosCellEditorParams } from '../../web-subform.model';
import { AbstractEditorComponent } from '../abstract-editor-component';

@Component({
	selector: 'nuc-subform-string-editor',
	templateUrl: './subform-string-editor.component.html',
	styleUrls: ['./subform-string-editor.component.css']
})
export class SubformStringEditorComponent extends AbstractEditorComponent {

	@ViewChild('input') input: ElementRef;

	constructor(
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService,
		private $log: Logger,
		ref: ElementRef
	) {
		super(entityObjectService, eoEventService, ref);
	}

	agInit(params: NuclosCellEditorParams): any {
		super.agInit(params);

		if (params.charPress) {
			super.setValue(params.charPress);
			return;
		}

		super.setValue(params.value);
	}

	setStringValue(value) {
		super.setValue(value);
	}
}
