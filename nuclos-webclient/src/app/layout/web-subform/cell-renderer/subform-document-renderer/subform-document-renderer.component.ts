import { Component, Inject, Injector, NgZone } from '@angular/core';
import { ColDef, ICellRendererParams } from 'ag-grid';
import { AgRendererComponent } from 'ag-grid-angular';
import { UploadOutput } from 'ngx-uploader';
import { WebFileComponent } from '../../../web-file/web-file.component';
import { BoAttr, DocumentAttribute } from '../../../../entity-object-data/shared/bo-view.model';
import { EntityObject, SubEntityObject } from '../../../../entity-object-data/shared/entity-object.class';
import { NuclosI18nService } from '../../../../i18n/shared/nuclos-i18n.service';
import { DialogService } from '../../../../popup/dialog/dialog.service';
import { NuclosConfigService } from '../../../../shared/nuclos-config.service';
import { ImageService } from '../../../shared/image.service';
import { NuclosCellRendererParams } from '../../web-subform.model';

@Component({
	selector: 'nuc-subform-document-renderer',
	templateUrl: '../../../web-file/web-file.component.html',
	styleUrls: [
		'../../../web-file/web-file.component.scss',
		'./subform-document-renderer.component.css'
	]
})
export class SubformDocumentRendererComponent extends WebFileComponent
	implements AgRendererComponent {
	isEditable = false;
	private params: ICellRendererParams;
	private value: any;

	private colDef: ColDef;
	private attrMeta: BoAttr;

	private imageHref: string;

	constructor(
		injector: Injector,
		nuclosConfigService: NuclosConfigService,
		dialogService: DialogService,
		nuclosI18nService: NuclosI18nService,
		imageService: ImageService,
		i18nService: NuclosI18nService,
		@Inject(NgZone) zone: NgZone
	) {
		super(
			injector,
			nuclosConfigService,
			dialogService,
			nuclosI18nService,
			imageService,
			i18nService,
			zone
		);
	}

	agInit(params: ICellRendererParams) {
		this.refresh(params);
		setTimeout(() => {
			let colDef = this.getColDef();
			if (colDef && colDef.field) {
				let documentAttribute = this.getDocumentAttribute();
				if (documentAttribute === undefined) {
					this.isImage = true;
					this.documentIsSelected = false;
					let subEoData = this.getSubEntityObject().getData();
					if (
						subEoData &&
						subEoData.attrImages &&
						subEoData.attrImages.links[colDef.field]
					) {
						this.imageHref = subEoData.attrImages.links[colDef.field].href;
						this.documentIsSelected = true;
					}
				} else {
					this.isImage = false;

					// if file was just uploaded use filename from attribute otherwise from params
					this.fileName =
						(documentAttribute && documentAttribute.name) ||
						(params.value && params.value.name);
					this.fileIconUrl = this.getFileIconUrl();
					this.documentIsSelected =
						documentAttribute &&
						(!!documentAttribute['uploadToken'] || !!documentAttribute.id);
				}
			}
		});

		let nuclosCellRenderParams = params['nuclosCellRenderParams'] as NuclosCellRendererParams;
		this.isEditable = nuclosCellRenderParams.editable;
	}

	refresh(params: any): boolean {
		this.colDef = params.colDef;
		this.params = params;
		this.value = params.value;
		if (this.colDef.cellEditorParams) {
			this.attrMeta = this.colDef.cellEditorParams['attrMeta'];
		}
		return false;
	}

	getColDef(): ColDef {
		return this.params.colDef;
	}

	setValue(value) {
		this.params.value = value;
		let eo = this.getEntityObject();
		if (eo) {
			let colDef = this.getColDef();
			if (colDef && colDef.field) {
				this.getSubEntityObject().setAttribute(colDef.field, value);
			}
		}
	}

	setDocumentAttribute(value): void {
		this.setValue(value);
		return this.getSubEntityObject().setAttribute(this.webComponent.name, value);
	}

	getEntityObject(): EntityObject {
		return this.getSubEntityObject();
	}

	handleUpload(uploadedOutput: UploadOutput) {
		if (this.isEditable) {
			super.handleUpload(uploadedOutput);
		}
	}

	isWritable() {
		return this.isEditable;
	}

	/**
	 * @return undefined if attribute is image
	 */
	getDocumentAttribute(): DocumentAttribute | undefined {
		let colDef = this.getColDef();
		if (colDef && colDef.field) {
			return this.getSubEntityObject().getAttribute(colDef.field);
		} else {
			throw Error('Unable to get ag-grid column definition.');
		}
	}

	openImageModal() {
		let subEo = this.getSubEntityObject();

		this.getImageService().openGalleryForSubform(subEo, this.getImageAttributeId());
	}

	getImageService() {
		return this.injector.get(ImageService);
	}

	protected openDocument() {
		let docAttrId =
			this.getSubEntityObject().getEntityClassId() + '_' + this.params.colDef.field;
		if (this.isImage) {
			if (this.imageHref) {
				window.open(this.imageHref);
			}
		} else {
			// TODO: HATEOAS
			let link =
				this.nuclosConfigService.getRestHost() +
				'/boDocuments/' +
				this.eo.getEntityClassId() +
				'/' +
				this.eo.getId() +
				'/subBos/' +
				this.getSubEntityObject().getReferenceAttributeId() +
				'/' +
				this.getSubEntityObject().getId() +
				'/documents/' +
				docAttrId;
			window.open(link);
		}
	}

	protected resetDocument(): void {
		if (!this.params.colDef.field) {
			return;
		}
		this.getSubEntityObject().setAttribute(this.params.colDef.field, null);
		delete this.imageHref;

		this.resetModel(this.getSubEntityObject().getData().attrImages);
	}

	protected getImageAttributeId(): string {
		let colDef = this.getColDef();
		let attributeId = colDef && colDef.field;

		if (!attributeId) {
			throw Error('Field is not defined');
		}

		return attributeId;
	}

	private getSubEntityObject(): SubEntityObject {
		return this.params.data;
	}
}
