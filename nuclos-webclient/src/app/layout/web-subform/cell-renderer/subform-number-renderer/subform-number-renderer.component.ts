import { Component } from '@angular/core';
import { ColDef } from 'ag-grid';
import { AgRendererComponent } from 'ag-grid-angular';
import { EntityAttrMeta } from '../../../../entity-object-data/shared/bo-view.model';
import { NumberService } from '../../../../shared/number.service';

@Component({
	selector: 'nuc-subform-number',
	templateUrl: 'subform-number-renderer.component.html',
	styleUrls: ['subform-number-renderer.component.css']
})
export class SubformNumberRendererComponent implements AgRendererComponent {
	formattedValue: string;

	private colDef: ColDef;
	private attrMeta: EntityAttrMeta;

	private params: any;
	private value: number;

	constructor(private numberService: NumberService) {}

	agInit(params: any) {
		this.refresh(params);
	}

	refresh(params: any): boolean {
		this.params = params;
		this.colDef = params.colDef;

		if (this.colDef.cellEditorParams) {
			this.attrMeta = this.colDef.cellEditorParams['attrMeta'];

			this.value = params.data.getAttribute(this.colDef.field);
			this.formattedValue = this.numberService.format(
				this.value,
				this.attrMeta.getPrecision()
			);
		}
		return false;
	}
}
