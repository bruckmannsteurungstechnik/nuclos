import { Component } from '@angular/core';
import { AgRendererComponent } from 'ag-grid-angular';
import { EntityAttrMeta } from '../../../../entity-object-data/shared/bo-view.model';
import { SubEntityObject } from '../../../../entity-object-data/shared/entity-object.class';
import { FqnService } from '../../../../shared/fqn.service';
import { NuclosCellRendererParams } from '../../web-subform.model';

/**
 * TODO: Consolidate with ReferenceRendererComponent.
 */
@Component({
	selector: 'nuc-subform-reference',
	templateUrl: 'subform-reference-renderer.component.html',
	styleUrls: ['subform-reference-renderer.component.scss']
})
export class SubformReferenceRendererComponent implements AgRendererComponent {
	popupParameter: string | undefined;
	attributeMeta: EntityAttrMeta;
	subEO: SubEntityObject;
	isEditable = false;
	value: string;
	attribute: any;

	private params: any;
	private nuclosCellRenderParams: NuclosCellRendererParams;

	constructor(private fqnService: FqnService) {}

	agInit(params: any) {
		this.params = params;
		this.value = params.value && params.value.id ? params.value.name : undefined;

		this.nuclosCellRenderParams = params.nuclosCellRenderParams as NuclosCellRendererParams;
		this.attributeMeta = this.nuclosCellRenderParams.attrMeta;
		this.isEditable = this.nuclosCellRenderParams.editable;
		this.popupParameter = this.getPopupParameter();
		this.subEO = params.api.getModel().getRow(params.rowIndex).data;

		if (this.attributeMeta) {
			let fieldName = this.fqnService.getShortAttributeName(
				this.subEO.getEntityClassId(),
				this.attributeMeta.getAttributeID()
			);
			if (fieldName !== undefined) {
				this.attribute = this.subEO.getAttribute(fieldName);
			}
		}
	}

	refresh(params: any): boolean {
		return false;
	}

	private getPopupParameter(): string | undefined {
		if (this.nuclosCellRenderParams.advancedProperties) {
			const key = 'popupparameter';
			let popupparameterProperty = this.nuclosCellRenderParams.advancedProperties
				.filter(p => p.name === key)
				.shift();
			if (popupparameterProperty) {
				return popupparameterProperty.value;
			}
		}
		return undefined;
	}
}
