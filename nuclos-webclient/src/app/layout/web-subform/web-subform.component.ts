import { Component, DoCheck, ElementRef, Injector, OnDestroy, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserAction } from '@nuclos/nuclos-addon-api';
import { ColDef, ColumnEvent, GridApi, GridOptions, RowNode } from 'ag-grid';
import * as _ from 'lodash';
import {
	BehaviorSubject,
	EMPTY,
	Observable,
	of as observableOf,
	Subject,
	Subscription
} from 'rxjs';

import { debounceTime, distinctUntilChanged, map, mergeMap, skip, take, tap, switchMap } from 'rxjs/operators';
import { AuthenticationService } from '../../authentication';
import { EntityAttrMeta, EntityMeta } from '../../entity-object-data/shared/bo-view.model';
import { EntityObjectDependency } from '../../entity-object-data/shared/entity-object-dependency';
import { EntityObjectDependents } from '../../entity-object-data/shared/entity-object-dependents';
import { EntityObject, SubEntityObject } from '../../entity-object-data/shared/entity-object.class';
import { EntityObjectService } from '../../entity-object-data/shared/entity-object.service';
import { MetaService } from '../../entity-object-data/shared/meta.service';
import { SelectableService } from '../../entity-object-data/shared/selectable.service';
import { SortAttribute, SortModel } from '../../entity-object-data/shared/sort.model';
import { EntityObjectGridService } from '../../entity-object/entity-object-grid/entity-object-grid.service';
import { DetailModalService } from '../../entity-object/shared/detail-modal.service';
import { EntityObjectPreferenceService } from '../../entity-object/shared/entity-object-preference.service';
import { SideviewmenuService } from '../../entity-object/shared/sideviewmenu.service';
import { ExportBoListTemplateState } from '../../entity-object/sidebar/statusbar/statusbar.component';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { Logger } from '../../log/shared/logger';
import {
	ColumnAttribute,
	Preference,
	SelectedAttribute,
	SideviewmenuPreferenceContent
} from '../../preferences/preferences.model';
import { RuleService } from '../../rule/shared/rule.service';
import { BrowserDetectionService } from '../../shared/browser-detection.service';
import { FqnService } from '../../shared/fqn.service';
import { Link, SubEOLinkContainer } from '../../shared/link.model';
import { Mixin } from '../../shared/mixin';
import { NuclosValidationService } from '../../validation/nuclos-validation.service';
import { AbstractGridComponent } from '../shared/abstract-grid-component';

import { ColumnLayoutChanges } from '../shared/column-layout-changes';
import { LayoutService } from '../shared/layout.service';
import { SubformBooleanEditorComponent } from './cell-editors/subform-boolean-editor/subform-boolean-editor.component';
import { SubformComboboxEditorComponent } from './cell-editors/subform-combobox-editor/subform-combobox-editor.component';
import { SubformDateEditorComponent } from './cell-editors/subform-date-editor/subform-date-editor.component';
import { SubformLovEditorComponent } from './cell-editors/subform-lov-editor/subform-lov-editor.component';
import { SubformMultilineEditorComponent } from './cell-editors/subform-multiline-editor/subform-multiline-editor.component';
import { SubformNumberEditorComponent } from './cell-editors/subform-number-editor/subform-number-editor.component';
import { SubformStringEditorComponent } from './cell-editors/subform-string-editor/subform-string-editor.component';
import {
	SubformBooleanRendererComponent,
	SubformDateRendererComponent,
	SubformDocumentRendererComponent,
	SubformEditRowRendererComponent,
	SubformHiddenRendererComponent,
	SubformNumberRendererComponent,
	SubformReferenceRendererComponent,
	SubformStateIconRendererComponent
} from './cell-renderer';
import { SubformCellValidator } from './subform-cell-validator';
import { SubformEoStatusToRowClass } from './subform-eo-status-to-row-class';
import { SubformRowComparator } from './subform-row-comparator';
import {
	NuclosCellEditorParams,
	NuclosCellRendererParams,
	RowEditCellParams,
	SubformColumn
} from './web-subform.model';
import { WebSubformService } from './web-subform.service';

@Component({
	selector: 'nuc-web-subform',
	templateUrl: './web-subform.component.html',
	styleUrls: ['./web-subform.component.scss']
})
@Mixin([ColumnLayoutChanges])
export class WebSubformComponent extends AbstractGridComponent<WebSubform>
	implements ColumnLayoutChanges, OnInit, DoCheck, OnDestroy {
	static isUndefinedOrTrue(value: any) {
		return value === undefined || value === true;
	}
	showPrintSearchResultListButton = false;
	hiddenColumns: Array<string | undefined>;
	private subformLinks: SubEOLinkContainer | undefined;

	private subformMeta: EntityMeta;

	private selectedColumnPref$: BehaviorSubject<
		Preference<SideviewmenuPreferenceContent> | undefined
		> = new BehaviorSubject(undefined);

	private tablePreferenceSubscription: Subscription;

	private sortModel: SortModel = new SortModel([]);

	private columnsFromLayout = new Map<string, WebSubformColumn>();

	/**
	 * The dependency between this subform and the root EO, might span multiple subforms.
	 */
	private dependency: EntityObjectDependency;

	/**
	 * The selected SubEos of the parent subform, if this is a sub-subform.
	 */
	private selectedParents?: SubEntityObject[];

	private subscriptions: Subscription = new Subscription();

	private columnsPref: Preference<SideviewmenuPreferenceContent>;

	/* mixins */
	// tslint:disable-next-line
	columnLayoutChanged: Subject<Date> = new Subject<Date>();
	// tslint:disable-next-line
	columnLayoutChangedSubscription: Subscription;

	/**
	 * TODO: Too many dependencies
	 */
	constructor(
		injector: Injector,
		private metaService: MetaService,
		private entityObjectService: EntityObjectService,
		private webSubformService: WebSubformService,
		private selectableService: SelectableService,
		private fqnService: FqnService,
		private $log: Logger,
		private ruleService: RuleService,
		private entityObjectPreferenceService: EntityObjectPreferenceService,
		private i18nService: NuclosI18nService,
		elementRef: ElementRef,
		private modalService: NgbModal,
		private exportBoListTemplateState: ExportBoListTemplateState,
		private authenticationService: AuthenticationService,
		private browserDetectionService: BrowserDetectionService,
		private validationService: NuclosValidationService,
		private detailModalService: DetailModalService,
		private sideviewmenuService: SideviewmenuService,
		private eoGridService: EntityObjectGridService,
		private layoutService: LayoutService
	) {
		super(injector, i18nService, elementRef);

		this.eoChangeListener = {
			afterAttributeChange: () => this.softRefresh(),
			afterValidationChange: () => this.softRefresh(),
			afterReload: () => this.softRefresh()
		};

		this.showPrintSearchResultListButton = this.authenticationService.isActionAllowed(
			UserAction.PrintSearchResultList
		);
	}

	// handleColumnChanges(gridOptions: GridOptions): void {};
	// onColumnChangesDebounced(): Observable<Date> {return {} as Observable<Date>};
	onColumnChangesDebounced(gridOptions: GridOptions): Observable<Date> {
		return {} as Observable<Date>;
	}

	getColDef(gridOptions: GridOptions, event: ColumnEvent): ColDef | undefined {
		return;
	}

	ngOnInit() {
		// NuclosRowColor
		this.gridOptions.getRowStyle = (params: { data: SubEntityObject }) => {
			let rowColor = params.data.getRowColor();
			return rowColor !== undefined
				? { 'background-color': rowColor }
				: undefined;
		};

		if (
			this.authenticationService.isActionAllowed(
				UserAction.WorkspaceCustomizeEntityAndSubFormColumn
			)
		) {
			// save column layout when column order, width or sort order was changed
			this.subscriptions.add(
				this.onColumnChangesDebounced(this.gridOptions).subscribe(() => {
					let columnPref = this.selectedColumnPref$.getValue();
					if (columnPref && this.gridOptions.api) {
						this.sortModel = new SortModel(
							this.gridOptions.api!.getSortModel() as SortAttribute[]
						);

						// let subformColumns = this.sideviewmenuService.gridToSubformColumns(this.gridOptions, this.subformMeta);
						columnPref.content.columns = this.sideviewmenuService.getSelectedAttributesFromGrid(
							this.gridOptions,
							this.subformMeta
						);

						columnPref.layoutId = this.eo.getLayoutId();
						this.selectableService.savePreference(columnPref).pipe(take(1)).subscribe();
					}
				})
			);
		}
	}

	ngOnDestroy(): void {
		this.clearSubscriptions();
	}

	isWritable(): boolean {
		let restriction = this.eo.getSubEoRestriction(this.webComponent.foreignkeyfieldToParent);
		let result = super.isWritable() && restriction !== 'readonly' && this.subformLinks;
		return !!result;
	}

	isVisible(): boolean {
		return (
			super.isVisible() && !!this.eo.getSubEoLink(this.webComponent.foreignkeyfieldToParent)
		);
	}

	/**
	 * Loads Subform data from the server.
	 */
	loadData() {
		if (!this.subformLinks) {
			this.$log.error('Could not load subform data - no link');
			return;
		}

		this.$log.debug('Loading Subform %o...', this.subformLinks.boMeta.href);
		this.updateDependents();
		if (this.dependents) {
			this.dependents.loadIfEmpty(this.sortModel);
		}
	}

	/**
	 * Instantiates a new Sub-EO and adds it to the grid data.
	 */
	newEntry() {
		this.createSubEo().pipe(take(1)).subscribe(subEo => {
			this.addSubEO(subEo);
			this.updateGridData();
			if (this.canOpenModal()) {
				this.detailModalService.openEoInModal(subEo).pipe(take(1)).subscribe(() =>
					// This is called when the modal is closed.
					// Textarea cells might have to be resized if their content was changed in the modal.
					this.resetRowHeight()
				);
			} else {
				this.editFirstCell();
			}
		});
	}

	/**
	 * Marks already saved Sub-EOs for deletion.
	 * Removes new Sub-EOs completely.
	 */
	deleteSelected(): void {
		for (let subEO of this.getSelectedSubEOs()) {
			if (!subEO.isNew()) {
				// mark already saved subbos as deleted
				subEO.markAsDeleted();
				subEO.setSelected(false);
			} else if (this.dependents) {
				this.eo.getDependents(this.webComponent.foreignkeyfieldToParent).removeAll([subEO]);
			}
		}
		this.updateGridData();
	}

	/**
	 * Clones the selected Sub-EOs (for insert).
	 */
	cloneSelected(): void {
		let clones: SubEntityObject[] = [];
		this.createSubEo().pipe(take(1)).subscribe(newSubEo => {
			this.getSelectedSubEOs().forEach(selectedSubEo => {
				let clone = newSubEo.clone();
				this.transferClonableAttributes(selectedSubEo, clone);
				clones.push(clone);
			});
			this.addSubEOs(clones);
		});
	}

	/**
	 * Flags the underlying Sub-EO as selected, when a row is selected.
	 *
	 * @param params
	 */
	rowSelected(params: { node: RowNode }) {
		let subEO: SubEntityObject = params.node.data;
		subEO.setSelected(params.node.isSelected());

		this.updateSubEoSelectionInRoot();
	}

	openExportBoListModal() {
		this.exportBoListTemplateState.boId = this.eo.getId();
		this.exportBoListTemplateState.refAttrId = this.subformMeta.getRefAttrId();
		this.exportBoListTemplateState.meta = this.subformMeta;
		this.exportBoListTemplateState.modalRef = this.modalService.open(
			this.exportBoListTemplateState.templateRef
		);
		if (this.selectedColumnPref$.getValue()) {
			this.exportBoListTemplateState.columnPrefrenrence = this.selectedColumnPref$.getValue() as Preference<
				SideviewmenuPreferenceContent
				>;
		}
	}

	isReadonly() {
		if (!this.isWritable()) {
			return true;
		}

		let subEORestriction = this.eo.getSubEoRestriction(
			this.webComponent.foreignkeyfieldToParent
		);
		return subEORestriction === 'readonly';
	}

	isNewVisible() {
		if (this.isReadonly()) {
			return false;
		}

		if (!WebSubformComponent.isUndefinedOrTrue(this.webComponent.newEnabled)) {
			return false;
		}

		// NUCLOS-6134
		let subEORestriction = this.eo.getSubEoRestriction(
			this.webComponent.foreignkeyfieldToParent
		);

		// TODO: Use a map!
		return subEORestriction !== 'nocreate' && subEORestriction !== 'nocreate,nodelete';
	}

	isNewEnabled() {
		if (this.isSubSubform()) {
			// Currently sub-subforms only work for a single parent
			if (this.selectedParents && this.selectedParents.length !== 1) {
				return false;
			}
		}

		return this.isNewVisible();
	}

	isEditEnabled() {
		if (this.isReadonly()) {
			return false;
		}

		return WebSubformComponent.isUndefinedOrTrue(this.webComponent.editEnabled);
	}

	isCloneVisible() {
		if (this.isReadonly()) {
			return false;
		}

		if (!WebSubformComponent.isUndefinedOrTrue(this.webComponent.cloneEnabled)) {
			return false;
		}

		// NUCLOS-6134
		let subEORestriction = this.eo.getSubEoRestriction(
			this.webComponent.foreignkeyfieldToParent
		);
		return subEORestriction !== 'nocreate' && subEORestriction !== 'nocreate,nodelete';
	}

	isCloneEnabled() {
		return this.isCloneVisible() && this.getSelectedSubEOs().length > 0;
	}

	isDeleteVisible() {
		if (this.isReadonly()) {
			return false;
		}

		if (!WebSubformComponent.isUndefinedOrTrue(this.webComponent.deleteEnabled)) {
			return false;
		}

		// NUCLOS-6134
		let subEORestriction = this.eo.getSubEoRestriction(
			this.webComponent.foreignkeyfieldToParent
		);
		return subEORestriction !== 'nodelete' && subEORestriction !== 'nocreate,nodelete';
	}

	isDeleteEnabled() {
		return this.isDeleteVisible() && this.getSelectedSubEOs().length > 0;
	}

	hasSubformLayout() {
		if (this.subformMeta) {
			return this.subformMeta.getLinks().defaultLayout !== undefined;
		}
		return false;
	}

	canOpenModal() {
		if (this.subformMeta) {
			return this.hasSubformLayout() && !this.webComponent.ignoreSubLayout;
		}
		return false;
	}

	refresh(params: any): boolean {
		return false;
	}

	cellEditingStopped(event: { node: RowNode }) {
		if (this.gridOptions && this.gridOptions.api) {
			this.gridOptions.api.refreshCells({
				rowNodes: [event.node]
			});
		}
	}

	getColumnFromLayout(attributeId: string): WebSubformColumn | undefined {
		return (
			this.webComponent.subformColumns &&
			this.webComponent.subformColumns.find(column => column.name === attributeId)
		);
	}

	isCloneable(attributeId: string): boolean {
		let column = this.getColumnFromLayout(attributeId);

		if (!column) {
			this.$log.debug('Could not find layout column for attribute %o', attributeId);
			return true;
		}

		return !column.notCloneable;
	}

	hasAutonumber() {
		return this.getAutonumberAttribute() !== undefined;
	}

	canMoveSelectedRowsUp() {
		if (!this.gridOptions.api) {
			return;
		}

		let model = this.gridOptions.api.getModel();
		if (!model || model.getRowCount() === 0) {
			return false;
		}

		// Can't move up, if the first entry is selected
		if (model.getRow(0).data.isSelected()) {
			return false;
		}

		// We can move up, if there is any other selected dependent
		for (let i = 1; i < model.getRowCount(); i++) {
			if (model.getRow(i).data.isSelected()) {
				return true;
			}
		}

		return false;
	}

	canMoveSelectedRowsDown() {
		if (!this.gridOptions.api) {
			return;
		}

		let model = this.gridOptions.api.getModel();
		if (!model || model.getRowCount() === 0) {
			return false;
		}

		// Can't move down, if the last entry is selected
		if (model.getRow(model.getRowCount() - 1).data.isSelected()) {
			return false;
		}

		// We can move down, if there is any other selected dependent
		for (let i = 0; i < model.getRowCount() - 1; i++) {
			if (model.getRow(i).data.isSelected()) {
				return true;
			}
		}

		return false;
	}

	moveSelectedRowsUp() {
		if (!this.gridOptions.api) {
			return;
		}

		let attribute = this.getAutonumberAttribute();
		if (!attribute) {
			this.$log.warn('Could not move selected dependents up - no autonumber attribute');
			return;
		}

		let model = this.gridOptions.api.getModel();
		if (model.getRowCount() === 0) {
			this.$log.warn('Could not move selected dependents up - no dependents');
			return;
		}

		for (let i = 1; i < model.getRowCount(); i++) {
			let dep = model.getRow(i).data;
			let otherDep = model.getRow(i - 1).data;
			if (dep.isSelected()) {
				// Change position with previous dependent
				let value = dep.getAttribute(attribute.getAttributeID());
				let otherValue = otherDep.getAttribute(attribute.getAttributeID());

				// Set the attribute without notifying listeners - otherwise this would trigger an autonumber update
				dep.setDirty(true);
				dep.setAttributeUnsafe(attribute.getAttributeID(), otherValue);

				otherDep.setAttribute(attribute.getAttributeID(), value);
			}
		}
	}

	moveSelectedRowsDown() {
		if (!this.gridOptions.api) {
			return;
		}

		let attribute = this.getAutonumberAttribute();
		if (!attribute) {
			this.$log.warn('Could not move selected dependents up - no autonumber attribute');
			return;
		}

		let model = this.gridOptions.api.getModel();
		if (model.getRowCount() === 0) {
			this.$log.warn('Could not move selected dependents up - no dependents');
			return;
		}

		for (let i = model.getRowCount() - 2; i >= 0; i--) {
			let dep = model.getRow(i).data;
			let otherDep = model.getRow(i + 1).data;
			if (dep.isSelected()) {
				// Change position with previous dependent
				let value = dep.getAttribute(attribute.getAttributeID());
				let otherValue = otherDep.getAttribute(attribute.getAttributeID());

				// Set the attribute without notifying listeners - otherwise this would trigger an autonumber update
				dep.setDirty(true);
				dep.setAttributeUnsafe(attribute.getAttributeID(), otherValue);

				otherDep.setAttribute(attribute.getAttributeID(), value);
			}
		}
	}

	isClickOutside(target: any) {
		// If a datepicker is opened, its panel is attached to the body, not to the subform.
		// Therefore it is not a child of the subform in the DOM and the click appears to be outside.
		// But since there should be always at most 1 datepicker open at the same time, it should
		// be the one belonging to the subform, which means "no outside click".
		// TODO: This assumption might be wrong, if e.g. there was another datepicker component
		//  that is not displayed in an exclusive overlay.
		//  We should make sure, that the datepicker which received the click really belongs to
		//  this subform and the currently active cell editor.
		return $(target).closest('ngb-datepicker').length === 0;
	}

	protected onGridApiReady(api: GridApi) {
		// set initial sorting when api is ready
		setTimeout(() => {
			if (api) {
				api.setSortModel(this.sortModel.getColumns());
			}
		});
	}

	protected init() {
		this.$log.debug('Subform init...');

		this.clearSubscriptions();

		this.subformLinks = this.eo.getSubEoLink(this.webComponent.foreignkeyfieldToParent);

		if (!this.subformLinks) {
			this.$log.error('Could not init subform - no link');
			return;
		}

		this.metaService
			.getSubBoMeta(this.subformLinks.boMeta.href)
			.pipe(
				take(1),
				tap(meta => {
					this.subformMeta = meta;

					// configure grid options
					this.gridOptions.enableSorting = true;
					this.gridOptions.enableColResize = true;
					this.gridOptions.rowSelection = 'multiple';
					this.gridOptions.suppressRowClickSelection = this.subformMeta.showSubformSelectionColumn();
				}),
				mergeMap(() => this.loadTablePreferences()),
				switchMap(() => this.initDependents())
			)
			.subscribe(dependents => {
				this.setDependents(dependents);
				this.loadData();
			});
	}

	protected updateRowSelection() {
		super.updateRowSelection();

		this.updateSubEoSelectionInRoot();
	}

	protected getLogger(): Logger {
		return this.$log;
	}

	private isColumnEnabledByLayout(attributeId: string) {
		let subformColumn = this.columnsFromLayout.get(attributeId);

		if (!subformColumn) {
			// No definition means no restriction
			return true;
		}

		return subformColumn.enabled;
	}

	private isColumnVisibleByLayout(attributeId: string) {
		let subformColumn = this.columnsFromLayout.get(attributeId);

		if (!subformColumn) {
			// No definition means no restriction
			return true;
		}

		return subformColumn.visible;
	}

	private clearSubscriptions() {
		this.subscriptions.unsubscribe();
		if (this.dependentSubscription) {
			this.dependentSubscription.unsubscribe();
		}
		if (this.tablePreferenceSubscription) {
			this.tablePreferenceSubscription.unsubscribe();
		}
		if (this.columnLayoutChangedSubscription) {
			this.columnLayoutChangedSubscription.unsubscribe();
		}
	}

	private initDependents(): Observable<EntityObjectDependents | undefined> {
		if (this.webComponent.parentSubform) {
			this.$log.debug('Subscribing to parent selection changes...');
			return this.handleParentSelections();
		} else {
			let dependents = this.eo.getDependentsAndLoad(
				this.getReferenceAttributeId()
			) as EntityObjectDependents;
			return observableOf(dependents);
		}
	}

	private handleParentSelections(): Observable<EntityObjectDependents | undefined> {
		return this.layoutService.getWebLayout(this.eo).pipe(
			mergeMap(layout => {
				let result: Observable<EntityObjectDependency> = EMPTY;
				if (layout) {
					result = observableOf(
						EntityObjectDependency.fromLayout(
							layout,
							this.webComponent.foreignkeyfieldToParent
						)
					);
				} else {
					this.$log.warn('No Layout');
				}
				return result;
			}),
			mergeMap(dependency => {
				this.dependency = dependency;
				this.$log.trace('Dependency: %o', dependency);
				return this.eo
					.getSelectionChangesInParentSubform(this.webComponent.parentSubform)
					.pipe(
						mergeMap((selectedParents: SubEntityObject[]) => {
							this.selectedParents = selectedParents;
							let dependents = this.getChildDependents(selectedParents);
							return observableOf(dependents);
						})
					);
			})
		);
	}

	private getChildDependents(
		selectedParents: SubEntityObject[]
	): EntityObjectDependents | undefined {
		let dependents;
		this.$log.debug('Parent changed');
		if (selectedParents.length === 1) {
			let parentEo = selectedParents[0];
			dependents = parentEo.getDependentsAndLoad(
				this.getReferenceAttributeId(),
				this.sortModel
			);
			let parentEoId = selectedParents[0].getId();
			let parentDependency = this.dependency.getParent();
			if (parentDependency) {
				this.dependency.getParent()!.setEoId('' + parentEoId);
			}
		}
		return dependents;
	}

	private setDependents(dependents: EntityObjectDependents | undefined) {
		if (this.dependentSubscription) {
			this.dependentSubscription.unsubscribe();
		}

		this.dependents = dependents;

		if (this.dependents) {
			this.updateGridStatusOverlay();

			this.dependentSubscription =
				this.dependents.asObservable().subscribe((subEos: SubEntityObject[]) => {
					this.updateGridStatusOverlay();

					this.setSubEos(subEos);
					this.safariLayoutHack();
				});
		} else {
			this.setSubEos([]);
		}
	}

	private updateDependents() {
		if (this.webComponent.parentSubform) {
			let selectedParents = this.eo.getSelectionInParentSubform(
				this.webComponent.parentSubform
			);
			let dependents = this.getChildDependents(selectedParents);
			this.setDependents(dependents);
		}
	}

	private loadTablePreferences() {
		let parentEntityUid = this.eo.getEntityClassId();

		return this.entityObjectPreferenceService
			.loadSubformPreferences(this.subformMeta, parentEntityUid, this.eo.getLayoutId())
			.pipe(
				tap(() => {
					this.selectedColumnPref$ = this.entityObjectPreferenceService.getSubformColumnPreferenceSelection(
						this.subformMeta.getBoMetaId()
					);

					if (this.tablePreferenceSubscription) {
						this.tablePreferenceSubscription.unsubscribe();
					}

					this.tablePreferenceSubscription = this.selectedColumnPref$.subscribe(
							pref => {
								if (pref) {
									this.columnsPref = pref;
									this.initColumns();
									this.sortModel = new SortModel(
										this.sideviewmenuService.getAgGridSortModel(
											pref.content.columns.filter(
												col => col.sort && col.sort.direction
											)
										)
									);
								}
							}
					);
				})
			);
	}

	private setSubEos(dependents: SubEntityObject[] | undefined) {
		this.getLogger().debug('updateDep: ' + this.dependents + ' => ' + dependents);
		let hardRefresh: boolean = dependents === undefined && this.dependents === undefined;
		let softRefresh: boolean = dependents !== undefined && this.dependents !== undefined;

		if (dependents) {
			dependents.forEach(eo => eo.addListener(this.eoChangeListener));
		}

		this.updateGridStatusOverlay();

		if (hardRefresh) {
			this.hardRefresh();
			return;
		}

		this.updateGridData();

		if (softRefresh) {
			this.softRefresh();
		}

		if (
			this.gridOptions.api &&
			dependents &&
			this.gridOptions.enableSorting &&
			this.sortModel &&
			!_.isEqual(this.gridOptions.api!.getSortModel(), this.sortModel.getColumns())
		) {
			this.gridOptions.api.setSortModel(this.sortModel.getColumns());
		}
	}

	/**
	 * handle safari flexbox bug
	 */
	private safariLayoutHack() {
		if (this.browserDetectionService.isSafari()) {
			setTimeout(() => {
				let height = $(this.elementRef.nativeElement)
					.closest('.tab-container-panel')
					.height();
				$(this.elementRef.nativeElement)
					.find('.ag-bl')
					.height(height);
			});
		}
	}

	private initColumns(): void {
		if (!this.columnsPref || !this.subformMeta) {
			this.$log.error('Could not init subform');
			return;
		}

		this.subscriptions.add(this.createColumnDefinitionsAndRowHeight().subscribe(colDefs => {
			if (this.isWritable()) {
				for (let colDef of colDefs) {
					this.addStatusClasses(colDef);
				}
			}
			this.setColumns(colDefs);
			this.subscribeToColumnChanges();
		}));
	}

	private createColumnDefinitionsAndRowHeight() {
		let columnDefinitions: ColDef[] = [];

		if (this.subformMeta.showSubformSelectionColumn()) {
			columnDefinitions.push(this.createSelectionColumn());
		}
		let meta = this.subformMeta;
		let visibleColumns = this.getVisibleColumns();

		for (let column of visibleColumns) {
			let colDef = this.createColumnDefinition(column, meta);
			if (colDef) {
				columnDefinitions.push(colDef);
			}
		}

		this.updateRowHeightFunction(visibleColumns);

		return this.getEditRowColumnIfLayoutExists().pipe(
			take(1),
			tap(colDef => {
				if (colDef) {
					columnDefinitions.push(colDef);
				}
			}),
			map(colDef => columnDefinitions)
		);
	}

	private getVisibleColumns(): Array<ColumnAttribute> {
		let columns = this.columnsPref.content.columns;
		let meta = this.subformMeta;

		this.updateColumnsFromLayout();

		this.hiddenColumns = columns
			.map(column => meta.getAttributeMetaByFqn(column.boAttrId))
			.filter(attrMeta => attrMeta && !this.isColumnVisible(attrMeta, meta))
			.map(attrMeta => (attrMeta ? attrMeta.getAttributeID() : undefined))
			.filter(attributeId => attributeId !== undefined);

		let visibleColumns = this.filterVisibleColumns(columns, meta);

		visibleColumns.forEach(col => {
			let attribute = this.subformMeta.getAttributeMetaByFqn(col.boAttrId);
			col.getPreferredHeight = (eo: SubEntityObject) => {
				if (attribute) {
					return this.getPreferredHeight(eo, attribute);
				}
				return undefined;
			};
		});

		return visibleColumns;
	}

	private updateRowHeightFunction(visibleColumns: Array<ColumnAttribute>) {
		if (!this.webComponent.dynamicCellHeightsDefault) {
			return;
		}

		this.gridOptions.getRowHeight = (params: { data: SubEntityObject }) => {
			let height = WebSubformService.defaultLineHeightInPx;
			visibleColumns.forEach(column => {
				if (column.getPreferredHeight) {
					let cellHeight = column.getPreferredHeight(params.data);
					if (cellHeight && cellHeight > height) {
						height = cellHeight;
					}
				}
			});
			return height;
		};
	}

	private createColumnDefinition(column: ColumnAttribute, meta: EntityMeta) {
		let fieldName = this.fqnService.getShortAttributeName(
			this.subformMeta.getBoMetaId(),
			column.boAttrId
		);
		let attribute = meta.getAttributeMeta(fieldName);
		if (!attribute) {
			this.$log.warn('Unknown attribute: %o', fieldName);
			return;
		}

		let restriction = this.getColumnRestriction(attribute);
		let editable = !restriction;

		let colDef: ColDef = {
			headerName: attribute.getName(),
			field: fieldName,
			colId: attribute.getAttributeID(),
			width: column.width,
			editable: editable,
			valueGetter: this.eoGridService.valueGetter,

			// allow access to this.elementRef in AbstractGridComponent
			newValueHandler: params => {
				return this.newValueHandler(params);
			},

			cellEditorParams: {},
			sort: column.sort && column.sort.direction
		};

		this.addStylesAndClasses(editable, colDef, attribute);

		colDef.headerClass = this.eoGridService.getTextAlignmentClass(attribute);

		let subformColumn = this.webComponent.subformColumns
			? this.webComponent.subformColumns.filter(col => col.name === column.boAttrId).shift()
			: undefined;

		// provide data for render component
		colDef.cellRendererParams = {
			nuclosCellRenderParams: {
				editable: editable,
				attrMeta: attribute,
				advancedProperties: subformColumn ? subformColumn.advancedProperties : undefined
			} as NuclosCellRendererParams
		};

		// provide data for editor component
		colDef.cellEditorParams = {
			attrMeta: attribute,
			subformMeta: this.subformMeta,
			subformColumn: subformColumn,
			advancedProperties: subformColumn ? subformColumn.advancedProperties : undefined,
			fixedHeight: this.isMultiline(subformColumn)
				? undefined
				: WebSubformService.defaultLineHeightInPx
		} as NuclosCellEditorParams;

		if (restriction === 'hidden') {
			colDef.cellRendererFramework = SubformHiddenRendererComponent;
		} else if (attribute.isBoolean()) {
			colDef.comparator = SubformRowComparator.booleanComparator;
			colDef.cellRendererFramework = SubformBooleanRendererComponent;
			colDef.cellEditorFramework = SubformBooleanEditorComponent;
		} else if (attribute.isNumber()) {
			colDef.comparator = SubformRowComparator.numberComparator;
			colDef.cellRendererFramework = SubformNumberRendererComponent;
			colDef.cellEditorFramework = SubformNumberEditorComponent;
		} else if (attribute.isDate() || attribute.isTimestamp()) {
			colDef.cellRendererFramework = SubformDateRendererComponent;
			colDef.cellEditorFramework = SubformDateEditorComponent;
			colDef.cellClass = 'subform-date-cell';
		} else if (attribute.isStateIcon()) {
			colDef.comparator = SubformRowComparator.nameComparator;
			colDef.cellRendererFramework = SubformStateIconRendererComponent;
			colDef.editable = false;
		} else if (attribute.isDocument() || attribute.isImage()) {
			colDef.comparator = SubformRowComparator.nameComparator;
			colDef.cellRendererFramework = SubformDocumentRendererComponent;
			colDef.editable = false;
		} else if (attribute.isReference()) {
			colDef.cellRendererFramework = SubformReferenceRendererComponent;
			if (attribute.isState()) {
				// the state can't be changed in subform
				colDef.editable = false;
			} else {
				colDef.comparator = SubformRowComparator.nameComparator;
				if (attribute.getComponentType() === 'Combobox') {
					colDef.cellEditorFramework = SubformComboboxEditorComponent;
				} else {
					colDef.cellEditorFramework = SubformLovEditorComponent;
				}
			}
		} else if (this.isMultiline(subformColumn)) {
			colDef.cellEditorFramework = SubformMultilineEditorComponent;
			colDef.comparator = SubformRowComparator.textComparator;
		} else {
			colDef.cellEditorFramework = SubformStringEditorComponent;
			colDef.comparator = SubformRowComparator.textComparator;
		}

		// add custom CellEditor/CellRenderer if configured
		if (subformColumn) {
			const customCellRenderer = this.webSubformService.getCustomCellRenderer(
				subformColumn.advancedProperties
			);
			if (customCellRenderer) {
				colDef.cellRendererFramework = customCellRenderer;
			}
			const customCellEditor = this.webSubformService.getCustomCellEditor(
				subformColumn.advancedProperties
			);
			if (customCellEditor) {
				colDef.cellEditorFramework = customCellEditor;
			}
		}

		this.addVlpToColumnDefinition(attribute, colDef);

		if (colDef.icons === undefined) {
			colDef.icons = {};
		}
		colDef.icons['sortAscending'] = '<i class="fa fa-chevron-up"/>';
		colDef.icons['sortDescending'] = '<i class="fa fa-chevron-down"/>';

		if (!colDef.comparator) {
			colDef.comparator = SubformRowComparator.textComparator;
		}

		return colDef;
	}

	private filterVisibleColumns(columns: ColumnAttribute[], meta: EntityMeta): ColumnAttribute[] {
		return columns.filter(column => {
			if (!column.selected) {
				return false;
			}

			let attr = meta.getAttributeMetaByFqn(column.boAttrId);

			if (!attr) {
				this.$log.warn('Unknown attribute: %o', column.boAttrId);
				return false;
			}

			if (!this.isColumnVisible(attr, meta)) {
				return false;
			}
			return true;
		});
	}

	private isColumnVisible(attr: EntityAttrMeta, meta: EntityMeta): boolean {
		let visible =
			!attr.isHidden() &&
			attr.getAttributeID() !== meta.getRefAttrId() &&
			this.isColumnVisibleByLayout(attr.getAttributeID());
		return visible;
	}

	private getColumnRestriction(attr: EntityAttrMeta): string {
		let roAttributes = this.eo.getSubEOReadOnlyAttributes(
			this.webComponent.foreignkeyfieldToParent
		);
		if (roAttributes !== undefined) {
			let restr = roAttributes[attr.getAttributeID()];
			if (restr) {
				return restr;
			}
		}

		let editable =
			this.isWritable() &&
			!attr.isReadonly() &&
			this.isColumnEnabledByLayout(attr.getAttributeID());
		return editable ? '' : 'ro';
	}

	private updateColumnsFromLayout() {
		this.columnsFromLayout.clear();
		if (this.webComponent.subformColumns) {
			for (let subformColumn of this.webComponent.subformColumns) {
				this.columnsFromLayout.set(subformColumn.name, subformColumn);
			}
		}
	}

	private createSelectionColumn(): ColDef {
		// row selection checkbox
		let rowSelectionCheckboxColDef: ColDef = {
			checkboxSelection: true,
			headerTooltip: this.i18nService.getI18n('webclient.checkbox.selectAll'),
			headerCheckboxSelection: true,
			headerName: '',
			width: 20,
			editable: false,
			pinned: true,
			lockPosition: true,
			lockPinned: true,
			suppressFilter: true,
			suppressMovable: true,
			suppressNavigable: true,
			suppressResize: true,
			suppressSorting: true
		};

		return rowSelectionCheckboxColDef;
	}

	private addVlpToColumnDefinition(attr: EntityAttrMeta, colDef: ColDef) {
		let subformColumn = this.columnsFromLayout.get(attr.getAttributeID());

		if (subformColumn) {
			let vlp = subformColumn.valuelistProvider;
			if (vlp) {
				// TODO: Use a proper interface here
				colDef.cellEditorParams['valuelistProvider'] = vlp;
			}
		}
	}

	private getEditRowColumnIfLayoutExists(): Observable<ColDef | undefined> {
		return this.getSubformLayoutLink().pipe(
			map(layout => {
				if (!layout) {
					this.$log.debug('Subform BO has no default layout');
					return undefined;
				}

				let couldDisplayPopupIcon = !!this.subformMeta.getPopupParameter();

				let editRowIconColDef: ColDef = {
					headerName: '',
					cellRendererFramework: SubformEditRowRendererComponent,
					width: couldDisplayPopupIcon ? 40 : 20,
					editable: false,
					pinned: 'right',
					suppressFilter: true,
					suppressMovable: true,
					suppressNavigable: true,
					suppressResize: true,
					suppressSorting: true
				};

				editRowIconColDef.cellRendererParams = new RowEditCellParams(
					this.webComponent.advancedProperties
				);
				editRowIconColDef.cellRendererParams.canOpenModal = this.canOpenModal();

				return editRowIconColDef;
			})
		);
	}

	private getSubformLayoutLink(): Observable<Link> {
		let detailEntityClassId = this.subformMeta.getDetailEntityClassId();
		if (detailEntityClassId) {
			return this.metaService
				.getEntityMeta(detailEntityClassId)
				.pipe(map(detailMeta => detailMeta.getLinks().defaultLayout)) as Observable<Link>;
		}

		return observableOf(this.subformMeta.getLinks().defaultLayout) as Observable<Link>;
	}

	private addStylesAndClasses(editable: boolean, colDef: ColDef, attr: EntityAttrMeta) {
		if (this.isWritable()) {
			this.addValidationClasses(attr, colDef);
			if (!editable) {
				this.lightenBackground(colDef);
			}
		}
	}

	private addValidationClasses(attr: EntityAttrMeta, colDef: ColDef) {
		let validator;

		if (attr.isNumber()) {
			validator = _params => this.validationService.isValidForInput(_params.value, 'number');
		}

		if (!colDef.cellClassRules) {
			colDef.cellClassRules = {};
		}

		colDef.cellClassRules['nuc-validation'] = () => true;
		colDef.cellClassRules['nuc-validation-valid'] = params =>
			new SubformCellValidator(params, validator).hasClass('nuc-validation-valid');
		colDef.cellClassRules['nuc-validation-missing'] = params =>
			new SubformCellValidator(params, validator).hasClass('nuc-validation-missing');
		colDef.cellClassRules['nuc-validation-invalid'] = params =>
			new SubformCellValidator(params, validator).hasClass('nuc-validation-invalid');
	}

	private addStatusClasses(colDef: ColDef) {
		if (!colDef.cellClassRules) {
			colDef.cellClassRules = {};
		}

		SubformEoStatusToRowClass.addCellClassRulesTo(colDef.cellClassRules);
	}

	private lightenBackground(colDef: ColDef) {
		colDef.cellStyle = { 'background-image': 'url("assets/lighten.png")' };
	}

	private subscribeToColumnChanges() {
		// save column layout when column order, width or sort order was changed
		// TODO sort order
		if (this.columnLayoutChangedSubscription) {
			this.columnLayoutChangedSubscription.unsubscribe();
		}

		this.columnLayoutChangedSubscription = this.columnLayoutChanged
			.asObservable()
			.pipe(
				debounceTime(500),
				distinctUntilChanged(),
				skip(1) // TODO: Prevent the initial event instead of skipping it here
			)
			.subscribe(() => {
				this.storeSubformColumnLayout();
			});
	}

	private storeSubformColumnLayout(): void {
		if (
			!this.authenticationService.isActionAllowed(
				UserAction.WorkspaceCustomizeEntityAndSubFormColumn
			)
		) {
			return;
		}

		if (this.gridOptions.columnApi && this.gridOptions.api) {
			this.sortModel = new SortModel(this.gridOptions.api.getSortModel() as SortAttribute[]);

			// column order and column width
			let subformColumns: SubformColumn[] = this.gridOptions.columnApi
				.getColumnState()
				.filter(col => col.colId.length > 1) // filter out row selection column
				.map(cs => {
					let fieldName = this.fqnService.getShortAttributeName(
						this.subformMeta.getBoMetaId(),
						cs.colId
					);
					return {
						eoAttrFqn: cs.colId,
						name: this.subformMeta.getAttribute(fieldName).name,
						fieldName: fieldName,
						width: cs.width
					};
				}) as SubformColumn[];

			// map ag-grid columns to preference columns
			subformColumns = this.gridOptions.columnApi
				.getAllGridColumns()
				.map(c => c.getId())
				.map((colId: string) => {
					return (
						subformColumns.filter(col => col.eoAttrFqn === colId).shift() ||
						({} as SubformColumn)
					);
				});

			// column sort order
			let sortPrio = 0;
			this.gridOptions.api.getSortModel().forEach(col => {
				let subformColumn = subformColumns.filter(sc => sc.eoAttrFqn === col.colId).shift();
				if (subformColumn) {
					subformColumn.sort = {
						direction: col.sort,
						prio: sortPrio++
					};
				}
			});

			let columnPref = this.selectedColumnPref$.getValue();

			if (columnPref) {
				let position = 0;
				columnPref.content.columns = subformColumns
					.filter(col => col.eoAttrFqn)
					.map(gc => {
						return {
							boAttrId: gc.eoAttrFqn,
							name: gc['name'], // TODO refactor column interfaces
							width: gc.width,
							position: position++,
							selected: true,
							sort: gc.sort
						} as SelectedAttribute;
					});

				this.webSubformService.evictSubformColumnLayoutInCache(
					this.eo.getEntityClassId(),
					this.subformMeta.getBoMetaId(),
					{ columns: subformColumns }
				);

				columnPref.layoutId = this.eo.getLayoutId();
				this.selectableService.savePreference(columnPref).pipe(take(1)).subscribe();
			}
		}
	}

	private editFirstCell() {
		if (this.gridOptions.api && this.gridOptions.columnApi) {
			let firstRow = this.gridOptions.api.getModel().getRow(0);
			let editableCol = this.gridOptions.columnApi
				.getAllDisplayedColumns()
				.find(col => col.isCellEditable(firstRow));

			if (editableCol) {
				this.gridOptions.api.startEditingCell({
					rowIndex: firstRow.rowIndex,
					colKey: editableCol
				});
			}
		}
	}

	private getReferenceAttributeId() {
		let attributeId = this.subformMeta.getMetaData().refAttrId;

		if (!attributeId) {
			this.$log.warn('Unknown reference attribute');
			attributeId = '';
		}

		return attributeId;
	}

	private createSubEo() {
		return this.entityObjectService
			.createNew(this.subformMeta.getBoMetaId(), this.eo)
			.pipe(map((eo: EntityObject) => this.prepareNewSubEo(eo)));
	}

	/**
	 * Creates a new Sub-EO for insertion into this subform, based on the data of the given EO.
	 */
	private prepareNewSubEo(eo: EntityObject) {
		let subEo = new SubEntityObject(this.eo, this.getReferenceAttributeId(), eo.getData());

		this.subscriptions.add(this.ruleService.updateRuleExecutor(subEo, this.eo).subscribe());
		subEo.addListener(this.eoChangeListener);
		subEo.setComplete(true);

		// TODO: Clearing attribute restrictions here, because they apply only for main EOs.
		// This is only a workaround. There should be another generation service to
		// create subform data, which sets correct attribute restrictions in the first place.
		subEo.getData().attrRestrictions = undefined;

		let refAttrId = this.getReferenceAttributeId();
		if (refAttrId) {
			// Restrict reference to main EO
			let ref = {
				id: this.eo.getId(),
				name: this.eo.getTitle() // TODO: Respect possibly different string representation of VLP
			};
			subEo.setAttribute(refAttrId, ref);
		} else {
			this.$log.error('No refAttrId defined.');
		}

		return subEo;
	}

	private transferClonableAttributes(sourceSubEo: SubEntityObject, clone: SubEntityObject) {
		for (let attrKey of Object.keys(sourceSubEo.getAttributes())) {
			let attributeMeta = this.subformMeta.getAttributeMeta(attrKey);

			if (!attributeMeta) {
				this.$log.warn('No attribute meta for %o', attrKey);
				continue;
			}

			if (!this.isCloneable(attributeMeta.getAttributeID())) {
				continue;
			}

			// set attribute unsafe - without triggering layoutml rules
			if (
				!attributeMeta.isAutonumber() &&
				(!attributeMeta.isSystemAttribute() || attributeMeta.isProcess())
			) {
				clone.setAttributeUnsafe(attrKey, sourceSubEo.getAttribute(attrKey));
			}
		}
		// Update VLP params for a clone... (NUCLOS-7015)
		this.subscriptions.add(this.ruleService.updateRuleExecutor(clone, this.eo).subscribe());
	}

	/**
	 * Returns the currently selected Sub-EOs.
	 *
	 * @returns {SubEntityObject[]}
	 */
	private getSelectedSubEOs(): SubEntityObject[] {
		let currentDependents = this.dependents && this.dependents.current();

		if (!currentDependents) {
			return [];
		}

		return currentDependents.filter(subEO => subEO.isSelected()) as SubEntityObject[];
	}

	/**
	 * Adds the given sub-EO to the top of the grid.
	 */
	private addSubEO(subEO: SubEntityObject): void {
		this.addSubEOs([subEO]);
	}

	/**
	 * Adds multiple subEos to the top of the grid.
	 */
	private addSubEOs(subEOs: SubEntityObject[]): void {
		if (this.dependents) {
			this.dependents.addAll(subEOs);
		}
	}

	private updateSubEoSelectionInRoot() {
		this.eo.setSubEosSelected(this.webComponent.entity, this.getSelectedSubEOs());
	}

	/**
	 * Returns true if this subform has a parent subform.
	 */
	private isSubSubform() {
		return this.webComponent.parentSubform !== undefined;
	}

	private getPreferredHeight(eo: SubEntityObject, attribute: EntityAttrMeta) {
		if (attribute.getType() === 'String') {
			let value: string = eo.getAttribute(attribute.getAttributeID());
			return WebSubformService.calculateCellHeightForText(value, [
				'form-control',
				'form-control-sm'
			]).height;
		}

		return undefined;
	}

	private isMultiline(subformColumn?: WebSubformColumn) {
		if (!subformColumn) {
			return false;
		}
		return subformColumn.controlType === 'TEXTAREA';
	}
}
