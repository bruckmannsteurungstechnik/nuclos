import { ColDef, GridApi, ICellEditorParams, ICellRendererParams, RowNode } from 'ag-grid';
import { EntityAttrMeta, EntityMeta } from '../../entity-object-data/shared/bo-view.model';

export class SubformLayout {
	static getCacheKey(parentEntityUid: string, subformEoMetaId: string): string {
		return parentEntityUid + '_' + subformEoMetaId;
	}
	columns: SubformColumn[];
}

export class SubformColumn {
	displayName?: string;
	eoAttrFqn?: string;
	sort?: { direction: string, prio?: number };
	type?: string;
	uid?: string; // TODO rename in REST-service
	width: number;
}

export class RowEditCellParams {
	canOpenModal = false;

	constructor(private advancedProperties: WebAdvancedProperty[]) {
	}

	getAdvancedProperty(key: string): string | undefined {
		if (!this.advancedProperties) {
			return undefined;
		}
		let property = this.advancedProperties.filter(p => p.name === key).shift();
		return property ? property.value : undefined;
	}
}

/**
 * additional parameters for CellRenderer components
 */
export interface NuclosCellParams {
	data: any;
	value: any;
	colDef: ColDef;
	api: GridApi;
	node: RowNode;
}

export interface NuclosCellRendererParams extends ICellRendererParams {
	editable: boolean;
	attrMeta: EntityAttrMeta;
	advancedProperties: any | undefined;
}

export interface NuclosCellEditorParams extends ICellEditorParams {
	data: any;
	colDef: ColDef;
	attrMeta: EntityAttrMeta;
	subformMeta: EntityMeta;
	subformColumn?: WebSubformColumn;
	advancedProperties: any | undefined;
	staticList: any;
	fixedHeight?: number;
}
