import { Component, Injectable, Type } from '@angular/core';
import { Observable } from 'rxjs';
import { AddonService } from '../../addons/addon.service';
import { NuclosCache, NuclosCacheService } from '../../cache/shared/nuclos-cache.service';
import { Logger } from '../../log/shared/logger';
import { SubformLayout } from './web-subform.model';

@Injectable()
export class WebSubformService {
	static defaultLineHeightInPx = 25;

	static calculateCellHeightForText(text, classes) {
		classes = classes || [];

		let div = document.createElement('div');
		div.setAttribute('class', classes.join(' '));

		$(div)
			.text(text + ' ')
			.css('position', 'absolute')
			.css('visibility', 'hidden')
			.css('height', 'auto')
			.css('width', 'auto')
			.css('white-space', 'pre')
			.css('font-size', '0.6875rem');

		document.body.appendChild(div);

		let dimensions = {
			width: jQuery(div).outerWidth(),
			height: jQuery(div).outerHeight()
		};

		div.parentNode!.removeChild(div);

		Logger.instance.debug(
			'calculated height for classes %o of text %o = %o',
			classes,
			text,
			dimensions
		);

		return dimensions;
	}

	private subformLayoutCache: NuclosCache;

	constructor(private cacheService: NuclosCacheService, private addonService: AddonService) {
		this.subformLayoutCache = this.cacheService.getCache('subformLayoutCache');
	}

	evictSubformColumnLayoutInCache(
		mainEntityUid: string,
		subformEoMetaId: string,
		subformLayout: SubformLayout
	) {
		let cacheKey = SubformLayout.getCacheKey(mainEntityUid, subformEoMetaId);

		this.subformLayoutCache.delete(cacheKey);

		this.subformLayoutCache.get(
			cacheKey,
			new Observable<SubformLayout>(observer => {
				observer.next(subformLayout);
			})
		);
	}

	/**
	 * a custom CellRenderer can be configured as an advanced property of a subform column with the key 'cellRenderer'
	 */
	public getCustomCellRenderer(
		advancedProperties: WebAdvancedProperty[]
	): Type<Component> | undefined {
		return this.getAddonComponent(advancedProperties, 'cellRenderer');
	}

	/**
	 * a custom CellEditor can be configured as an advanced property of a subform column with the key 'cellEditor'
	 */
	public getCustomCellEditor(
		advancedProperties: WebAdvancedProperty[]
	): Type<Component> | undefined {
		return this.getAddonComponent(advancedProperties, 'cellEditor');
	}

	private getAddonComponent(
		advancedProperties: WebAdvancedProperty[],
		advancedPropertyKey: string
	): Type<Component> | undefined {
		const cellEditor = advancedProperties
			? advancedProperties.find(ap => ap.name === advancedPropertyKey)
			: undefined;
		const cellEditorName = cellEditor ? cellEditor.value : undefined;
		if (cellEditorName) {
			return this.addonService.getComponentFactoryClass(cellEditorName);
		}
		return undefined;
	}
}
