import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { LayoutConstant } from '../layout-constant';

@Component({
	selector: 'nuc-web-table',
	templateUrl: './web-table.component.html',
	styleUrls: ['./web-table.component.css']
})
export class WebTableComponent implements OnInit {
	@Input() webComponent: WebTable;
	@Input() eo: EntityObject;

	constructor(private el: ElementRef) {}

	ngOnInit() {}

	// isVisible(): boolean {
	//
	// }
	//
	// private hasVisibleChild(): boolean {
	// 	let result = false;
	// 	for (let row of this.webComponent.rows) {
	// 		for (let cell of row.cells) {
	// 			for (let component of cell.components) {
	// 				if (component.is
	// 			}
	// 		}
	// 	}
	// }

	/**
	 * Dynamically calculates the availabe height for this table.
	 */
	getHeight() {
		let height = this.el.nativeElement.parentElement.offsetHeight;
		if (height <= 0) {
			height = LayoutConstant.FULL_SIZE;
		} else {
			height = Math.min(this.el.nativeElement.parentElement.offsetHeight, window.innerHeight);
		}
		return height;
	}

	/**
	 * Returns a string to be used as CSS width/height.
	 * If the size is a positive number, it is interpreted as pixel size.
	 * If it is negative, it could e.g. be a "100%" size.
	 */
	sizeToString(size: number): string | undefined {
		if (isNaN(size)) {
			return undefined;
		}
		if (size === LayoutConstant.FULL_SIZE) {
			return '100%';
		}
		return size + 'px';
	}

	/**
	 * Calculates the total table width based on column widths.
	 */
	getTableWidth() {
		let columnSizes = this.getColumnSizes();
		if (!columnSizes || columnSizes.indexOf(-1) > -1) {
			return LayoutConstant.FULL_SIZE;
		}

		let tableWidth = 0;
		for (let columnSize of columnSizes) {
			tableWidth += columnSize;
		}

		if (tableWidth <= 0) {
			return LayoutConstant.FULL_SIZE;
		}

		return tableWidth;
	}

	/**
	 * Returns the width of the given column.
	 */
	getColumnWidth(index: number) {
		let columnSizes = this.getColumnSizes();
		if (!columnSizes) {
			return undefined;
		}

		let columnSize = columnSizes[index];

		// First column is only visible if "Standardrand" is active, then it has a fixed size > 0
		if (index === 0) {
			if (columnSize > 0) {
				return columnSize;
			} else {
				return 0.01;
			}
		}

		if (!columnSize || columnSize === -1) {
			return LayoutConstant.FULL_SIZE;
		}

		return columnSize;
	}

	/**
	 * Return the height of the given row.
	 */
	getRowHeight(index: number) {
		let rowSizes = this.getRowSizes();
		if (!rowSizes) {
			return LayoutConstant.FULL_SIZE;
		}

		let rowSize = rowSizes[index];

		// First row is only visible if "Standardrand" is active, then it has a fixed size > 0
		if (index === 0) {
			if (rowSize > 0) {
				return rowSize;
			} else {
				return 0.01;
			}
		}

		if (!rowSize || rowSize === -1) {
			return LayoutConstant.FULL_SIZE;
		}

		return rowSize;
	}

	/**
	 * Calculates the total height (based on covered rows) for the given cell.
	 *
	 * TODO: Pre-calculate the values on server side?
	 */
	getCellHeight(rowNum: number, cell: WebCell): number {
		let height = 0;
		let rowspan = cell.rowspan ? cell.rowspan : 1;

		for (let i = rowNum; i < rowNum + rowspan; i++) {
			let rowHeight = this.getRowHeight(i);
			if (rowHeight === LayoutConstant.FULL_SIZE) {
				return LayoutConstant.FULL_SIZE;
			}
			height += rowHeight;
		}

		return height;
	}

	private getColumnSizes(): number[] | undefined {
		if (!this.webComponent) {
			return undefined;
		}
		return this.webComponent.columnSizes;
	}

	private getRowSizes(): number[] | undefined {
		if (!this.webComponent) {
			return undefined;
		}
		return this.webComponent.rowSizes;
	}
}
