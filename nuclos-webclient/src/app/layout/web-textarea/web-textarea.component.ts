import { ChangeDetectorRef, Component, Injector, OnChanges, OnInit } from '@angular/core';
import { AbstractInputComponent } from '../shared/abstract-input-component';

@Component({
	selector: 'nuc-web-textarea',
	templateUrl: './web-textarea.component.html',
	styleUrls: ['./web-textarea.component.css']
})
export class WebTextareaComponent extends AbstractInputComponent<WebTextarea> implements OnInit, OnChanges {

	constructor(injector: Injector,
				private cdr: ChangeDetectorRef) {
		super(injector);
	}

	ngOnChanges(): void {
		this.cdr.detectChanges();
	}

	ngOnInit() {
	}

	getRows() {
		let rows = this.webComponent && this.webComponent.rows;

		if (rows > 0) {
			return rows;
		}

		return 2;
	}
}
