/* tslint:disable:no-unused-variable */

import { ListFilterPipe } from './list-filter.pipe';

xdescribe('Pipe: ListFilter', () => {
	it('should filter an array of objects', () => {
		let pipe = new ListFilterPipe();
		expect(pipe).toBeTruthy();

		let list = [
			{firstName: 'Mustermann'},
			{firstName: 'Meier'},
			{firstName: 'Müller'},
			{firstName: 'Schmidt'}
		];
		expect(list.length === 4);
		expect(pipe.transform(list, [{firstName: 'M'}]).length === 3);
		expect(pipe.transform(list, [{firstName: 'Mü'}]).length === 1);
	});
});
