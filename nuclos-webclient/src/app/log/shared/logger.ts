// TODO replace usages with Logger from api

// Set up the default logger. The default logger doesn't actually log anything; but, it
// provides the Dependency-Injection (DI) token that the rest of the application can use
// for dependency resolution. Each platform can then override this with a platform-
// specific logger implementation, like the ConsoleLogService (below).
import { ILogger, LogLevel } from '@nuclos/nuclos-addon-api';

export class Logger implements ILogger {
	static instance: ILogger;

	assert(..._args: any[]): void {
		// ... the default logger does no work.
	}

	error(..._args: any[]): void {
		// ... the default logger does no work.
	}

	group(..._args: any[]): void {
		// ... the default logger does no work.
	}

	groupEnd(..._args: any[]): void {
		// ... the default logger does no work.
	}

	info(..._args: any[]): void {
		// ... the default logger does no work.
	}

	log(..._args: any[]): void {
		// ... the default logger does no work.
	}

	warn(..._args: any[]): void {
		// ... the default logger does no work.
	}

	debug(..._args: any[]): void {
		// ... the default logger does no work.
	}

	trace(..._args: any[]): void {
		// ... the default logger does no work.
	}

	getLogLevel(): LogLevel {
		return LogLevel.OFF;
	}

	setLogLevel(logLevel: LogLevel): void {
	}
}
