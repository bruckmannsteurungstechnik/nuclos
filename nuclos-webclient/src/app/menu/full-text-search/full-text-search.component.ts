import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FullTextSearchResultItem } from './full-text-search.model';
import { FullTextSearchService } from './full-text-search.service';

@Component({
	selector: 'nuc-full-text-search',
	templateUrl: './full-text-search.component.html',
	styleUrls: ['./full-text-search.component.css']
})
export class FullTextSearchComponent implements OnInit, OnDestroy {

	showFullTextSearch = false;

	@Input() searchtext: string;

	results: FullTextSearchResultItem[] = [];

	private unsubscribe$ = new Subject<void>();

	constructor(private fullTextSearchService: FullTextSearchService) {
	}

	ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	ngOnInit() {
		this.fullTextSearchService.isFullTextSearchEnabled()
		.pipe(takeUntil(this.unsubscribe$)).subscribe(enabled => this.showFullTextSearch = enabled);
	}


	searchtextChanged() {
		if (this.searchtext) {
			if (this.searchtext.length === 0) {
				this.results = [];
			}

			this.fullTextSearchService.search(this.searchtext).pipe(takeUntil(this.unsubscribe$)).subscribe(
				(results: FullTextSearchResultItem[]) => {
					this.results = results;
				}
			);
		}
	}
}
