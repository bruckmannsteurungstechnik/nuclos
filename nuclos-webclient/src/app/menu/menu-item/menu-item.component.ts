import { Component, Input, OnInit } from '@angular/core';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { MenuItem } from '../shared/menu.model';

@Component({
	selector: 'nuc-menu-item',
	templateUrl: './menu-item.component.html',
	styleUrls: ['./menu-item.component.css', '../menu.component.css']
})
export class MenuItemComponent implements OnInit {

	iconBaseHref: string;

	@Input() menuItem: MenuItem;

	constructor(
		private config: NuclosConfigService
	) {
	}

	ngOnInit() {
		this.iconBaseHref = this.config.getRestHost() + '/meta/icon';
	}

}
