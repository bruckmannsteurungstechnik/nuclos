import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { NuclosConfigService } from '../shared/nuclos-config.service';
import { NuclosHttpService } from '../shared/nuclos-http.service';
import { MenuItem } from './shared/menu.model';

@Injectable()
export class MenuService {

	constructor(
		private http: NuclosHttpService,
		private nuclosConfig: NuclosConfigService
	) {
	}

	getMenuStructure(): Observable<MenuItem> {
		return this.http.get<MenuItem>(
			this.nuclosConfig.getRestHost() + '/meta/menustructure'
		);
	}
}
