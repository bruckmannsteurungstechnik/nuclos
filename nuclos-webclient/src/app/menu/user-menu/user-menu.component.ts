import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthenticationService } from '../../authentication/authentication.service';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { DialogService } from '../../popup/dialog/dialog.service';
import { PreferencesResetModalComponent } from './preferences-reset-modal/preferences-reset-modal.component';

@Component({
	selector: 'nuc-user-menu',
	templateUrl: './user-menu.component.html',
	styleUrls: ['./user-menu.component.css']
})
export class UserMenuComponent implements OnInit, OnDestroy {
	@Input() loggedIn = false;
	@Input() username: string | undefined;
	@Input() anonymous: boolean;
	@Input() canSharePreferences: boolean;

	mandators: Array<Mandator>;
	currentMandator: Mandator;

	private unsubscribe$ = new Subject<void>();

	constructor(private authenticationService: AuthenticationService,
				private dialogService: DialogService,
				private modalService: NgbModal,
				public router: Router,
				private i18n: NuclosI18nService) {
	}

	ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	ngOnInit() {

		this.authenticationService.observeLoginStatus().pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
			let authData = this.authenticationService.getAuthentication();
			if (authData) {
				if (authData.mandators) {
					this.mandators = authData.mandators;
				}
				if (authData.mandator) {
					this.currentMandator = authData.mandator;
				}
			}

		});
	}

	selectMandator(mandator: Mandator) {
		this.currentMandator = mandator;
		this.authenticationService.selectMandator(mandator).pipe(take(1)).subscribe(() => {
			$('body').css('cursor', 'progress');

			// this.authenticationService.navigateHome();
			// TODO auskommentiert wegen NUCLOS-6836 / RRPTD-94 mit Andreas/Ramin nochmal klären, wie das gemerged werden soll
			this.authenticationService.navigateToLastEntity();
			window.location.reload();
		});
	}

	resetWorkspace() {
		this.dialogService.confirm({
				title: this.i18n.getI18n('webclient.user.resetworkspace'),
				message: this.i18n.getI18n('webclient.user.resetworkspace.confirm')
			}
		).then(
			() => {
				// ok, resetworkspace
				this.authenticationService.resetWorkspace();
			},
			() => {
				// cancel
			}
		);
	}

	openPreferencesResetModal() {
		this.modalService.open(PreferencesResetModalComponent, {size: 'lg'});
	}
}
