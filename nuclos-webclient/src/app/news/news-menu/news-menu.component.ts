import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NewsService } from '../shared/news.service';

@Component({
	selector: 'nuc-news-menu',
	templateUrl: './news-menu.component.html',
	styleUrls: ['./news-menu.component.css']
})
export class NewsMenuComponent implements OnInit, OnDestroy {
	news: News[];

	private unsubscribe$ = new Subject<void>();

	constructor(
		private newsService: NewsService,
	) {
	}

	ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	ngOnInit() {
		this.newsService.getNews().pipe(takeUntil(this.unsubscribe$)).subscribe(
			data => {
				if (data && data.length > 0) {
					this.news = data;
				}
			}
		);
	}

	showNews(news) {
		this.newsService.showNews(news);
	}
}
