import { Injectable } from '@angular/core';
import { DialogButton } from '@nuclos/nuclos-addon-api';
import { concat, Observable } from 'rxjs';
import { distinctUntilChanged, filter, map, mergeMap } from 'rxjs/operators';
import { AuthenticationService } from '../../authentication';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { Logger } from '../../log/shared/logger';
import { DialogService } from '../../popup/dialog/dialog.service';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { NuclosHttpService } from '../../shared/nuclos-http.service';

@Injectable({
	providedIn: 'root'
})
export class NewsService {
	constructor(
		private configService: NuclosConfigService,
		private http: NuclosHttpService,
		private dialogService: DialogService,
		private authService: AuthenticationService,
		private i18n: NuclosI18nService,
		private $log: Logger
	) {
		this.authService
			.observeLoginStatus()
			.pipe(
				distinctUntilChanged(),
				filter(login => login) // Filter for login === true
			)
			.subscribe(() => this.showUnconfirmedAndUnreadNews());
	}

	getNews(): Observable<News[]> {
		let link = this.configService.getRestHost() + '/news';

		return this.http.getCachedJSON(link);
	}

	getPrivacyPolicy(): Observable<News | undefined> {
		return this.getNews().pipe(map(news => news.find(item => item.privacyPolicy)));
	}

	getUnconfirmedNews(): Observable<News[]> {
		let link = this.configService.getRestHost() + '/news/unconfirmed';

		return this.http.get<News[]>(link);
	}

	showUnreadNews(): Observable<any> {
		return this.getUnreadNews().pipe(mergeMap(news => this.showAllUnreadNews(news)));
	}

	getUnreadNews(): Observable<News[]> {
		let link = this.configService.getRestHost() + '/news/unread';

		return this.http.get<News[]>(link);
	}

	markNewsConfirmed(news: News) {
		let link = this.configService.getRestHost() + '/news/' + news.id + '/confirmed';

		return this.http.put(link, {});
	}

	markNewsRead(news: News) {
		let link = this.configService.getRestHost() + '/news/' + news.id + '/viewed';

		return this.http.put(link, {});
	}

	showNewsByIdOrNameOrTitle(idOrNameOrTitle: string) {
		this.$log.debug('Trying to show news %o...', idOrNameOrTitle);
		return this.getNews()
			.pipe(
				map(news =>
					news.find(
						item =>
							item.id === idOrNameOrTitle ||
							item.name === idOrNameOrTitle ||
							item.title === idOrNameOrTitle
					)
				)
			)
			.subscribe(result => {
				if (result) {
					this.showNews(result);
				} else {
					this.$log.warn('Unknown news: %o', idOrNameOrTitle);
				}
			});
	}

	showNews(news: News) {
		this.$log.debug('Showing news %o...', news);
		this.dialogService.display({
			title: news.title || news.name,
			message: news.content
		});
	}

	confirmAllNews(news: News[]): Observable<any> {
		this.$log.info('confirmAllNews: %o', news);
		return new Observable(observer => {
			this.$log.info('confirmAllNews Observable: %o', news);
			if (news.length === 0) {
				this.$log.info('confirmAllNews empty -> complete');
				observer.next();
				observer.complete();
				return;
			}

			let item = news.splice(0, 1)[0];
			let callback = () => {
				// Item declined -> logout
				this.authService.logout().subscribe();
				observer.error();
				observer.complete();
			};

			this.confirmNews(
				item,
				() => {
					this.markNewsConfirmed(item).subscribe(() => {
						// Item confirmed and confirmation saved -> continue with remaining news
						this.confirmAllNews(news).subscribe(() => {
							observer.next();
							observer.complete();
						});
					});
				},
				callback
			).catch(() => callback());
		});
	}

	confirmNews(news: News, confirmationCallback: Function, declinationCallback: Function) {
		this.$log.debug('Showing news %o for confirmation...', news);
		return this.dialogService.openDialog(
			news.title || news.name,
			news.content,
			[
				new DialogButton(
					'button-ok',
					this.i18n.getI18n('webclient.news.accept'),
					'btn btn-primary',
					confirmationCallback
				),
				new DialogButton(
					'button-cancel',
					this.i18n.getI18n('webclient.news.decline'),
					'btn btn-primary',
					declinationCallback
				)
			],
			{
				size: 'lg'
			}
		);
	}

	showAllUnreadNews(news: News[]): Observable<any> {
		this.$log.info('showAllUnreadNews: %o', news);
		return new Observable(observer => {
			if (news.length === 0) {
				observer.complete();
				return;
			}

			let item = news.splice(0, 1)[0];

			let callback = () => {
				// Item confirmed and confirmation saved -> continue with remaining news
				this.showAllUnreadNews(news).subscribe(() => observer.complete());
			};

			this.showUnreadNewsItem(
				item,
				() => {
					this.markNewsRead(item).subscribe(callback);
				},
				callback
			).catch(callback);
		});
	}

	showUnreadNewsItem(news: News, confirmationCallback: Function, declinationCallback: Function) {
		this.$log.debug('Showing news %o for confirmation...', news);
		return this.dialogService.openDialog(
			news.title || news.name,
			news.content,
			[
				new DialogButton(
					'button-ok',
					this.i18n.getI18n('webclient.news.read'),
					'btn btn-primary',
					confirmationCallback
				),
				new DialogButton(
					'button-cancel',
					this.i18n.getI18n('webclient.news.unread'),
					'btn btn-primary',
					declinationCallback
				)
			],
			{
				size: 'lg'
			}
		);
	}

	/**
	 * First shows all News that require confirmation.
	 * After all required News have been confirmed, shows unread News.
	 */
	private showUnconfirmedAndUnreadNews() {
		this.$log.debug('showUnconfirmedAndUnreadNews');
		concat(this.showUnconfirmedNews(), this.showUnreadNews()).subscribe(() =>
			this.$log.warn('showUnconfirmedAndUnreadNews finished.')
		);
	}

	private showUnconfirmedNews(): Observable<any> {
		return this.getUnconfirmedNews().pipe(mergeMap(news => this.confirmAllNews(news)));
	}
}
