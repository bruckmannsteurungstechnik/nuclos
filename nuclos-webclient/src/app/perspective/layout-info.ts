import UID = nuclos.UID;

export interface ILayoutInfo {
	description: string;
	uid: UID;
	nucletUID: UID;
	name: string;
	fqn: string;
}
