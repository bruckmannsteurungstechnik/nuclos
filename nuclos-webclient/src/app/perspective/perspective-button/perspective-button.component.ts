import { Component, Input, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';
import { IPerspective } from '../perspective';
import { Preference } from '../../preferences/preferences.model';
import { PerspectiveService } from '../shared/perspective.service';

@Component({
	selector: 'nuc-perspective-button',
	templateUrl: './perspective-button.component.html',
	styleUrls: ['./perspective-button.component.css']
})
export class PerspectiveButtonComponent implements OnInit {
	@Input() perspectivePref: Preference<IPerspective>;

	mouseover = false;

	constructor(
		private perspectiveService: PerspectiveService
	) {
	}

	ngOnInit() {
	}

	isEditAllowed() {
		return this.perspectiveService.isEditAllowed();
	}

	editPerspective() {
		this.perspectiveService.editPerspective(this.perspectivePref);
	}

	togglePerspective() {
		this.perspectiveService.togglePerspective(this.perspectivePref);
	}

	deletePerspective() {
		this.perspectiveService.unshareAndDelete(this.perspectivePref).pipe(take(1)).subscribe();
	}

	getId() {
		return this.perspectivePref.prefId;
	}
}
