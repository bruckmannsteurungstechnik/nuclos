import { EventEmitter, Input, Output } from '@angular/core';
import { IPerspective } from '../perspective';
import { PerspectiveModel } from '../perspective-model';

export abstract class AbstractPerspectiveEditComponent {
	@Input()
	model: PerspectiveModel;

	@Input()
	perspective: IPerspective;

	@Output()
	change = new EventEmitter<any>();
}
