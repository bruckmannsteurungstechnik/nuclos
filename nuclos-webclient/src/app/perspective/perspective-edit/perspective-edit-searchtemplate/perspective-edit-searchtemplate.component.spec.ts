import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerspectiveEditSearchtemplateComponent } from './perspective-edit-searchtemplate.component';

xdescribe('PerspectiveEditSearchtemplateComponent', () => {
	let component: PerspectiveEditSearchtemplateComponent;
	let fixture: ComponentFixture<PerspectiveEditSearchtemplateComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [PerspectiveEditSearchtemplateComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(PerspectiveEditSearchtemplateComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
