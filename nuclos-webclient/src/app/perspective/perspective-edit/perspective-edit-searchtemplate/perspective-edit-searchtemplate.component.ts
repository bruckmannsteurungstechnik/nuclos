import { Component } from '@angular/core';
import { AbstractPerspectiveEditComponent } from '../abstract-perspective-edit.component';

@Component({
	selector: 'nuc-perspective-edit-searchtemplate',
	templateUrl: './perspective-edit-searchtemplate.component.html',
	styleUrls: ['./perspective-edit-searchtemplate.component.css']
})
export class PerspectiveEditSearchtemplateComponent extends AbstractPerspectiveEditComponent {

	hasSearchtemplatePrefs() {
		return this.model.getSearchTemplatePrefs(this.perspective).length > 0;
	}

}
