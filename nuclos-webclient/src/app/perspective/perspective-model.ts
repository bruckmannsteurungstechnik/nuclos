import {
	Preference,
	SearchtemplatePreferenceContent,
	SideviewmenuPreferenceContent
} from '../preferences/preferences.model';
import { ILayoutInfo } from './layout-info';
import { IPerspective } from './perspective';
import { SubformEntry } from './subform-entry';

export interface PerspectiveModel {
	/**
	 * Existing perspectives for this BO.
	 */
	readonly perspectives: Array<Preference<IPerspective>>;

	/**
	 * The currently toggled perspective, if any.
	 */
	selectedPerspective?: IPerspective;

	/**
	 * Other available prefs for the current BO.
	 * These are used to configure new perspectives.
	 */
	searchTemplatePrefs: Array<Preference<SearchtemplatePreferenceContent>>;
	sideviewMenuPrefs: Array<Preference<SideviewmenuPreferenceContent>>;
	layoutInfos: Array<ILayoutInfo>;
	subformTablePrefs: Array<Preference<SideviewmenuPreferenceContent>>;
	subformTablePrefsBySubEntity: SubformEntry[];

	getSearchTemplatePrefs: (perspective: IPerspective) => Array<Preference<any>>;
	getSideviewMenuPrefs: (perspective: IPerspective) => Array<Preference<any>>;

	getSearchTemplate(searchTemplatePrefId: string): Preference<any> | null;
	getSideviewMenu(prefId: string): Preference<any> | null;
	getLayoutInfo(layoutUID: string): ILayoutInfo | null;
	getSubformTablePrefs(subformEntityClassId: string): Array<Preference<SideviewmenuPreferenceContent>> | undefined;

	getLayoutFqn(layoutUid: string | undefined);

	/**
	 * Selects the given perspective, deselects all other perspectives.
	 *
	 * @param perspective
	 */
	togglePerspective(perspective: IPerspective);

	removePerspective(perspectivePref: Preference<IPerspective>);

	/**
	 * Adds the given perspective to the model, if it is new.
	 * Updates the perspective in the model, if it is only modified.
	 *
	 * @param perspectivePref
	 */
	addPerspective(perspectivePref: Preference<IPerspective>): void;

	/**
	 * Setter method for the perspectives of this model.
	 * If a perspective in the given array has the "selected" flag, it is set as selectedPerspective.
	 *
	 * @param perspectives
	 */
	setPerspectives(perspectives: Array<Preference<IPerspective>>);

	/**
	 * Sets some default values.
	 * TODO: There should be a Perspective class which does this on its own.
	 *
	 * @param perspective
	 */
	setDefaultValues(perspective: IPerspective);

	isValidPerspective(perspective: IPerspective): boolean;

	getSubformPreferencesForSelectedLayout(): SubformEntry[];
}
