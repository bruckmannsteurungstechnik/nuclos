import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AuthenticationModule } from '../authentication/authentication.module';
import { I18nModule } from '../i18n/i18n.module';
import { LayoutModule } from '../layout/layout.module';
import { LogModule } from '../log/log.module';
import { PreferencesModule } from '../preferences/preferences.module';
import { PerspectiveButtonComponent } from './perspective-button/perspective-button.component';
import {
	PerspectiveEditDetailviewComponent,
	PerspectiveEditLayoutComponent,
	PerspectiveEditSearchtemplateComponent,
	PerspectiveEditSideviewComponent,
	PerspectiveEditSubformsComponent
} from './perspective-edit';
import { PerspectiveEditComponent } from './perspective-edit/perspective-edit.component';
import { PerspectiveComponent } from './perspective/perspective.component';
import { PerspectiveService } from './shared/perspective.service';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,

		AuthenticationModule,
		PreferencesModule,
		LayoutModule,
		LogModule,
		I18nModule
	],
	declarations: [
		PerspectiveComponent,
		PerspectiveEditComponent,
		PerspectiveButtonComponent,
		PerspectiveEditLayoutComponent,
		PerspectiveEditSideviewComponent,
		PerspectiveEditSearchtemplateComponent,
		PerspectiveEditSubformsComponent,
		PerspectiveEditDetailviewComponent
	],
	exports: [
		PerspectiveComponent
	],
	providers: [
		PerspectiveService
	],
	entryComponents: [
		PerspectiveEditComponent
	]
})
export class PerspectiveModule {
}
