import { TestBed, inject } from '@angular/core/testing';

import { PerspectiveService } from './perspective.service';

xdescribe('PerspectiveService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [PerspectiveService]
		});
	});

	it('should ...', inject([PerspectiveService], (service: PerspectiveService) => {
		expect(service).toBeTruthy();
	}));
});
