import { Preference, SideviewmenuPreferenceContent } from '../../preferences/preferences.model';

export class SubformTablePreferenceSelection {
	entityClassId: string;
	subformTablePreference: Preference<SideviewmenuPreferenceContent> | undefined;
}
