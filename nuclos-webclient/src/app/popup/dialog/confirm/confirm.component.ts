import { Component } from '@angular/core';
import { DialogButton } from '@nuclos/nuclos-addon-api';
import { DialogOptions } from '../dialog.model';
import { DialogService, DialogState } from '../dialog.service';

@Component({
	selector: 'nuc-confirm-modal-component',
	templateUrl: './confirm.component.html'
})
export class ConfirmComponent {

	options: DialogOptions;
	buttonOptions: DialogButton[];

	constructor(private dialogService: DialogService,
				private state: DialogState) {
		this.options = state.options;
		this.buttonOptions = state.buttonOptions;
	}

	close() {
		if (this.options.dialogId) {
			this.dialogService.closeDialog(this.options.dialogId);
		}
	}

	dismiss() {
		if (this.options.dialogId) {
			let modalRef = this.state.modalRefs.get(this.options.dialogId);
			if (modalRef) {
				modalRef.dismiss();
			}
		}
	}
}
