import { Directive, TemplateRef } from '@angular/core';
import { DialogState } from '../dialog.service';

@Directive({
	selector: '[nucConfirm]'
})
export class ConfirmDirective {
	constructor(confirmTemplate: TemplateRef<any>, state: DialogState) {
		state.dialogTemplateRef = confirmTemplate;
	}
}
