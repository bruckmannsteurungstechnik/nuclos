import { Injectable, TemplateRef } from '@angular/core';
import { NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DialogButton, Logger } from '@nuclos/nuclos-addon-api';
import { DialogOptions } from './dialog.model';


/**
 * An internal service allowing to access, from the confirm modalRef component, the options and the modalRef reference.
 * It also allows registering the TemplateRef containing the confirm modalRef component.
 *
 * It must be declared in the providers of the NgModule, but is not supposed to be used in application code
 */
@Injectable()
export class DialogState {

	options: DialogOptions;

	buttonOptions: DialogButton[];

	/**
	 * open modalRefs
	 */
	modalRefs: Map<number, NgbModalRef> = new Map();

	/**
	 * ref containing the alert modal component
	 */
	alertTemplateRef: TemplateRef<any>;

	/**
	 * ref containing the dialog modal component
	 */
	dialogTemplateRef: TemplateRef<any>;
}

@Injectable()
export class DialogService {

	private static idCounter = 0;

	constructor(
		private modalService: NgbModal,
		private state: DialogState,
		private $log: Logger
	) {
	}

	/**
	 * TODO: Return Observable instead of Promise
	 *
	 * opens a alert modal
	 * @param options the options for the modal (title and message)
	 * @returns {Promise<any>} a promise that is fulfilled when the user clicks ok button
	 */
	alert(options: DialogOptions): Promise<any> {
		DialogService.idCounter ++;
		options.dialogId = DialogService.idCounter;
		this.state.options = options;
		const modalRef = this.modalService.open(this.state.alertTemplateRef);
		this.state.modalRefs.set(DialogService.idCounter, modalRef);
		this.focusConfirmationButton();
		return modalRef.result;
	}

	/**
	 * TODO: Return Observable instead of Promise
	 *
	 * opens a display modal (similar to alert modal, but larger)
	 * @param options the options for the modal (title and message)
	 * @returns {Promise<any>} a promise that is fulfilled when the user clicks ok button
	 */
	display(options: DialogOptions): Promise<any> {
		DialogService.idCounter ++;
		options.dialogId = DialogService.idCounter;
		this.state.options = options;
		let nmo: NgbModalOptions = {size: 'lg'};
		const modalRef = this.modalService.open(this.state.alertTemplateRef, nmo);
		this.state.modalRefs.set(DialogService.idCounter, modalRef);
		this.focusConfirmationButton();
		return modalRef.result;
	}

	/**
	 * TODO: Return Observable instead of Promise
	 *
	 * opens a confirm modal
	 * @param options the options for the modal (title and message)
	 * @returns {Promise<any>} a promise that is fulfilled when the user chooses to confirm, and rejected when
	 * the user chooses not to confirm, or closes the modal
	 */
	confirm(options: DialogOptions): Promise<any> {
		DialogService.idCounter ++;
		options.dialogId = DialogService.idCounter;

		return new Promise<any>((resolve: Function, reject: Function) => {
				this.state.options = options;

				this.state.buttonOptions = [
					new DialogButton(
						'button-ok',
						'webclient.button.yes',
						'btn-primary',
						() => {
							modalRef.close();
							resolve();
						}),

					new DialogButton(
						'button-cancel',
						'webclient.button.no',
						'btn-warning',
						() => {
							modalRef.close();
							reject();
						}),
				];

				const modalRef = this.modalService.open(this.state.dialogTemplateRef, {size: 'lg'});
				this.state.modalRefs.set(DialogService.idCounter, modalRef);
				this.focusConfirmationButton();
			}
		);
	}



	/**
	 * opens a modal dialog
	 * interaction should be handled via ButtonOption.callback
	 * @param {string} title
	 * @param {string} message
	 * @param {DialogButton[]} buttonOptions
	 * @param {NgbModalOptions} modalOptions
	 */
	openDialog(
		title: string,
		message: string,
		buttonOptions: DialogButton[],
		modalOptions?: NgbModalOptions
	) {

		DialogService.idCounter ++;

		let options: DialogOptions = {
			title: title,
			message: message,
			dialogId: DialogService.idCounter,
		};

		this.state.buttonOptions = buttonOptions;
		this.state.options = options;
		const modalRef = this.modalService.open(this.state.dialogTemplateRef, modalOptions);
		this.state.modalRefs.set(DialogService.idCounter, modalRef);
		this.focusConfirmationButton();

		return modalRef.result;
	}

	focusConfirmationButton() {
		$('#button-ok').focus();
	}

	hasOpenDialog() {
		// TODO: Track open modal instances instead of testing for a button
		return $('#button-ok:visible').length > 0;
	}

	closeDialog(dialogId: number) {
		const modalRef = this.state.modalRefs.get(dialogId);
		if (modalRef) {
			modalRef.close();
			this.state.modalRefs.delete(dialogId);
		} else {
			this.$log.error('Unable to close modal:', dialogId);
		}
	}
}
