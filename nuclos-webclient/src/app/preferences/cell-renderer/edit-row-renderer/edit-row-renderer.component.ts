import { Component } from '@angular/core';
import { take } from 'rxjs/operators';
import { AgRendererComponent } from 'ag-grid-angular';
import { Preference } from '../../preferences.model';
import { DialogService } from '../../../popup/dialog/dialog.service';
import { NuclosI18nService } from '../../../i18n/shared/nuclos-i18n.service';
import { Router } from '@angular/router';
import { PreferencesService } from '../../preferences.service';
import { PreferencesAdminService } from '../../preferences-admin.service';
import { PreferencesComponent } from '../../preferences.component';

@Component({
	selector: 'nuc-edit-row-renderer',
	templateUrl: 'edit-row-renderer.component.html',
	styleUrls: ['edit-row-renderer.component.css']
})
export class EditRowRendererComponent implements AgRendererComponent {

	preferenesComponent: PreferencesComponent;
	preferenceItem: Preference<any>;

	constructor(private dialogService: DialogService,
				private i18n: NuclosI18nService,
				private router: Router,
				private preferences: PreferencesService,
				private preferencesAdminService: PreferencesAdminService) {
	}

	agInit(params: any) {
		this.preferenceItem = params.value;
		this.preferenesComponent = params.context.componentParent;
	}

	deletePreferenceItem(preferenceItem: Preference<any>): void {
		this.dialogService.confirm(
			{
				title: this.i18n.getI18n('webclient.button.delete'),
				message: this.i18n.getI18n('webclient.dialog.delete')
			}
		).then(
			() => {
				this.preferences.deletePreferenceItem(preferenceItem).pipe(take(1)).subscribe(
					() => {
						this.router.navigate(['/preferences']);

						// refresh PreferencesComponent
						let index = this.preferenesComponent.preferencesItems.findIndex(p => p.prefId === preferenceItem.prefId);
						if (index > -1) {
							this.preferenesComponent.preferencesItems.splice(index, 1);
						}
						this.preferenesComponent.updatePreferenceItemView();
					}
				);
			},
			() => {
				// cancel
			}
		);
	}

	updatePreferenceShare(preferenceItem: Preference<any>): void {
		this.preferencesAdminService.updatePreferenceShare(preferenceItem);
	}


	/**
	 * discard the changes the user made on a shared preference
	 * @param preferenceItem
	 */
	discardChanges(preferenceItem: Preference<any>): void {
		this.dialogService.confirm(
			{
				title: this.i18n.getI18n('webclient.button.delete'),
				message: this.i18n.getI18n('webclient.preferences.dialog.confirm.discardCustomizedPreferenceItem')
			}
		).then(
			() => {
				this.preferencesAdminService.deleteCustomizedPreferenceItem(preferenceItem);
			},
			() => {
				// cancel
			}
		);
	}

	refresh(params: any): boolean {
		return false;
	}
}
