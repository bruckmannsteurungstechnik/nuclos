import { Component, ViewChild, ViewContainerRef } from '@angular/core';
import { IFilterAngularComp } from 'ag-grid-angular';
import { IAfterGuiAttachedParams, IDoesFilterPassParams, IFilterParams, RowNode } from 'ag-grid';
import { Preference } from '../../preferences.model';

type CustomizedFilterType = 'all' | 'customized' | 'notcustomized';

@Component({
	selector: 'nuc-customized-filter',
	templateUrl: './customized-filter.component.html',
	styleUrls: ['./customized-filter.component.css']
})
export class CustomizedFilterComponent implements IFilterAngularComp {
	customizedFilterType: CustomizedFilterType = 'all';

	@ViewChild('customizedFilterAll', { read: ViewContainerRef }) public customizedFilterAll;

	private params: IFilterParams;
	private valueGetter: (rowNode: RowNode) => any;

	onChangeIsCustomized(event) {
		this.customizedFilterType = event;
		this.params.filterChangedCallback();
	}

	agInit(params: IFilterParams): void {
		this.params = params;
		this.valueGetter = params.valueGetter;
	}

	isFilterActive(): boolean {
		return this.customizedFilterType === 'customized' || this.customizedFilterType === 'notcustomized';
	}

	doesFilterPass(params: IDoesFilterPassParams): boolean {
		let pref: Preference<any> = this.valueGetter(params.node);
		return (
			this.customizedFilterType === 'all' ||
			(this.customizedFilterType === 'customized' && !!pref.customized) ||
			(this.customizedFilterType === 'notcustomized' && !pref.customized)
		);
	}

	getModel(): any {
		return {
			value: this.customizedFilterType
		};
	}

	setModel(model: any): void {
		if (model) {
			this.customizedFilterType = model.value;
		}
	}

	afterGuiAttached(params: IAfterGuiAttachedParams): void {
		this.customizedFilterAll.element.nativeElement.focus();
	}
}
