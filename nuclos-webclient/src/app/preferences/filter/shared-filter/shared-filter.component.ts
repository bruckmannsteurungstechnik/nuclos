import { Component, ViewChild, ViewContainerRef } from '@angular/core';
import { IFilterAngularComp } from 'ag-grid-angular';
import { IAfterGuiAttachedParams, IDoesFilterPassParams, IFilterParams, RowNode } from 'ag-grid';
import { Preference } from '../../preferences.model';

type SharedFilterType = 'all' | 'shared' | 'notshared';

@Component({
	selector: 'nuc-shared-filter',
	templateUrl: './shared-filter.component.html',
	styleUrls: ['./shared-filter.component.css']
})
export class SharedFilterComponent implements IFilterAngularComp {
	sharedFilterType: SharedFilterType = 'all';

	@ViewChild('sharedFilterAll', { read: ViewContainerRef }) public sharedFilterAll;

	private params: IFilterParams;
	private valueGetter: (rowNode: RowNode) => any;

	onChangeIsShared(event) {
		this.sharedFilterType = event;
		this.params.filterChangedCallback();
	}

	agInit(params: IFilterParams): void {
		this.params = params;
		this.valueGetter = params.valueGetter;
	}

	isFilterActive(): boolean {
		return this.sharedFilterType === 'shared' || this.sharedFilterType === 'notshared';
	}

	doesFilterPass(params: IDoesFilterPassParams): boolean {
		let pref: Preference<any> = this.valueGetter(params.node);
		return (
			this.sharedFilterType === 'all' ||
			(this.sharedFilterType === 'shared' && !!pref.shared) ||
			(this.sharedFilterType === 'notshared' && !pref.shared)
		);
	}

	getModel(): any {
		return {
			value: this.sharedFilterType
		};
	}

	setModel(model: any): void {
		if (model) {
			this.sharedFilterType = model.value;
		}
	}

	afterGuiAttached(params: IAfterGuiAttachedParams): void {
		this.sharedFilterAll.element.nativeElement.focus();
	}
}
