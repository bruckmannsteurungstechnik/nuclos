import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthenticationService } from '../authentication';
import { UserAction } from '@nuclos/nuclos-addon-api';

export class PreferencesNavigationGuard implements CanActivate {

	constructor(private authenticationService: AuthenticationService) {
	}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
		return this.authenticationService.onAuthenticationDataAvailable().pipe(
			map((loginSuccessful: boolean) => loginSuccessful && this.authenticationService.isActionAllowed(UserAction.SharePreferences))
		);
	}
}
