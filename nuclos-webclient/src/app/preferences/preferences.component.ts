import { Component, HostListener, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { GridOptions } from 'ag-grid';
import { Subscription, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { AuthenticationService } from '../authentication';
import { NuclosI18nService } from '../i18n/shared/nuclos-i18n.service';
import { DialogService } from '../popup/dialog/dialog.service';
import { BrowserRefreshService } from '../shared/browser-refresh.service';
import { NuclosConfigService } from '../shared/nuclos-config.service';
import { SystemParameter } from '../shared/system-parameters';
import { EditRowRendererComponent } from './cell-renderer/edit-row-renderer/edit-row-renderer.component';
import { SharedRendererComponent } from './cell-renderer/shared-renderer/shared-renderer.component';
import { TypeRendererComponent } from './cell-renderer/type-renderer/type-renderer.component';
import { SharedFilterComponent } from './filter/shared-filter/shared-filter.component';
import { CustomizedFilterComponent } from './filter/customized-filter/customized-filter.component';
import { PreferencesAdminService } from './preferences-admin.service';
import { IUserRole, Preference } from './preferences.model';
import { PreferencesService } from './preferences.service';

type SelectionType = 'none' | 'single' | 'multiple';

@Component({
	selector: 'nuc-preferences',
	templateUrl: './preferences.component.html',
	styleUrls: ['./preferences.component.css']
})
export class PreferencesComponent implements OnInit, OnDestroy {
	gridOptions = <GridOptions>{};

	selection: SelectionType = 'none';

	isDevMode = false;
	isSharingAllowed = true;
	preferencesItems: Preference<any>[] = [];
	selectedPreferenceItem: Preference<any> | undefined = undefined;
	selectedUserUserRoles = [];
	multiselectUserRoles: IUserRole[];
	filter = {
		boMetaId: undefined,
		layoutName: undefined,
		name: undefined
	};

	routeParamsSubscription: Subscription;

	private unsubscribe$ = new Subject<void>();

	constructor(
		private nuclosConfig: NuclosConfigService,
		private dialogService: DialogService,
		private authenticationService: AuthenticationService,
		private i18n: NuclosI18nService,
		private router: Router,
		private route: ActivatedRoute,
		private browserRefreshService: BrowserRefreshService,
		private preferences: PreferencesService,
		private preferencesAdminService: PreferencesAdminService
	) { }

	ngOnInit() {
		this.ngOnInitImpl();
	}

	ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	gridReady(param): void {
		this.gridOptions.api = param.api;
		this.updatePreferenceItemView();
	}

	selectPreferenceItem(preferenceItem: Preference<any>): void {
		this.router.navigate(['/preferences', preferenceItem.prefId]);
	}

	shareOrUnshareSelectedPreferenceItem(userRole: IUserRole): void {
		if (this.selectedPreferenceItem) {
			this.preferences
				.shareOrUnsharePreferenceItem(this.selectedPreferenceItem, userRole)
				.pipe(take(1))
				.subscribe(() => {
					if (this.selectedPreferenceItem && this.selectedPreferenceItem.prefId) {
						this.preferences
							.getPreference(this.selectedPreferenceItem.prefId)
							.pipe(take(1))
							.subscribe(pref => {
								this.selectedPreferenceItem = pref;
								let prefInList = this.preferencesAdminService.preferencesItems
									.getValue()
									.filter(p => p.prefId === pref.prefId)
									.shift();
								if (prefInList) {
									prefInList.shared = this.selectedPreferenceItem.shared;
								}
							});
					}
				});
		}
	}

	updatePreferenceItemView(): void {
		if (this.gridOptions.api) {
			this.gridOptions.api.setRowData(this.preferencesItems);
		}
	}

	layoutAssignmentText(): string {
		return this.selectedPreferenceItem
			? this.i18n.getI18n(
				'webclient.preferences.layoutAssignmentText',
				this.selectedPreferenceItem.layoutName
			)
			: '';
	}

	layoutAssignmentButtonText(): string {
		return this.selectedPreferenceItem
			? this.i18n.getI18n(
				'webclient.preferences.layoutAssignmentButtonText',
				this.getBoName(this.selectedPreferenceItem)
			)
			: '';
	}

	resetLayoutAssignment(): void {
		if (this.selectedPreferenceItem) {
			delete this.selectedPreferenceItem.layoutId;
			this.preferences.savePreferenceItem(this.selectedPreferenceItem).pipe(take(1)).subscribe(pref => {
				if (pref) {
					this.selectedPreferenceItem = pref;
					this.selectedPreferenceItem.layoutName = '';
					this.preferencesItems
						.filter(p => p.prefId === pref.prefId)
						.forEach(p => (p.layoutName = ''));
					if (this.gridOptions.api) {
						this.gridOptions.api.redrawRows();
					}
				}
			});
		}
	}

	/**
	 * check if any of the selected rows are customized
	 * boolean is used to determine if buttons for discard/save changes should be displayed
	 */
	selectedPreferencesCustomized(): boolean {
		if (this.gridOptions.api) {
			let selectedRows = this.gridOptions.api.getSelectedRows().filter(p => p.customized);
			return selectedRows.length > 0;
		}
		return false;
	}

	/**
	 * update preference share for all selected rows which are customized
	 */
	updatePreferenceShares(): void {
		if (this.gridOptions.api) {
			this.gridOptions.api
				.getSelectedRows()
				.filter(p => p.customized)
				.forEach(p => {
					this.preferencesAdminService.updatePreferenceShare(p);
				});
		}
	}

	/**
	 * discard the changes the user made on a shared preference for all selected rows which are customized
	 */
	discardChanges(): void {
		this.dialogService
			.confirm({
				title: this.i18n.getI18n('webclient.button.delete'),
				message: this.i18n.getI18n(
					'webclient.preferences.dialog.confirm.discardCustomizedPreferenceItem'
				)
			})
			.then(
				() => {
					if (this.gridOptions.api) {
						this.gridOptions.api
							.getSelectedRows()
							.filter(p => p.customized)
							.forEach(p => {
								this.preferencesAdminService.deleteCustomizedPreferenceItem(p);
							});
					}
				},
				() => {
					// cancel
				}
			);
	}

	/**
	 * delete selected preferences
	 * shared preferences will be unshared before deletion
	 */
	deleteSelectedPreferences(): void {
		this.dialogService
			.confirm({
				title: this.i18n.getI18n('webclient.button.delete'),
				message: this.i18n.getI18n(
					'webclient.preferences.dialog.confirm.deleteSelectedPreferences'
				)
			})
			.then(
				() => {
					if (this.gridOptions.api) {
						let preferenceItems = this.gridOptions.api.getSelectedRows();
						this.preferences
							.findReferencingPreferences(preferenceItems)
							.toPromise()
							.then(prefs => {
								if (prefs.length > 0) {
									this.dialogService.alert({
										title: this.i18n.getI18n(
											'webclient.preferences.dialog.error.referencingPreferences'
										),
										message: this.i18n.getI18n(
											'webclient.preferences.dialog.error.referencingPreferencesMessage',
											prefs.map(p => p.name).join(', ')
										)
									});
								} else {
									this.preferences
										.deletePreferenceItems(preferenceItems)
										.toPromise()
										.then(() => {
											this.router.navigate(['/preferences/list']).then(() => {
												this.selectedPreferenceItem = undefined;
												this.loadData();
											});
										});
								}
							});
					}
				},
				() => {
					// cancel
				}
			);
	}

	/**
	 * assign/remove selected user groups to all selected preferences
	 * not selected user groups will be removed
	 */
	assignUserRolesForSelectedPreferences(): void {
		this.dialogService
			.confirm({
				title: this.i18n.getI18n(
					'webclient.preferences.assignUserRolesForSelectedPreferences'
				),
				message: this.i18n.getI18n(
					'webclient.preferences.dialog.confirm.assignUserRolesForSelectedPreferencesText'
				)
			})
			.then(
				() => {
					if (this.gridOptions.api) {
						let preferenceItems = this.gridOptions.api.getSelectedRows();
						this.preferences
							.shareUserRoles(preferenceItems, this.multiselectUserRoles)
							.then(() => {
								this.loadData();
							});
					}
				},
				() => {
					// cancel
				}
			);
	}

	selectAllCustomizedRows() {
		if (this.gridOptions.api) {
			this.setCustomizedFiltering('customized');
			this.handleKeyboardEventSelectAll();
		}
	}

	/**
	 * select all preferences with CTRL-A / CMD-A
	 */
	@HostListener('window:keydown.control.a', ['$event'])
	@HostListener('window:keydown.meta.a', ['$event'])
	handleKeyboardEventSelectAll() {
		if (this.gridOptions.api) {
			this.gridOptions.api.selectAllFiltered();
			this.router.navigate(['/preferences/list']);
		}
	}

	/**
	 * deselect all preferences with CTRL-SHIFT-A / CMD-SHIFT-A
	 */
	@HostListener('window:keydown.control.shift.a', ['$event'])
	@HostListener('window:keydown.meta.shift.a', ['$event'])
	handleKeyboardEventDeselectAll() {
		if (this.gridOptions.api) {
			this.gridOptions.api.deselectAllFiltered();
			this.router.navigate(['/preferences/list']);
		}
	}

	resetSelection() {
		if (this.gridOptions.api) {
			this.setCustomizedFiltering('all');
			this.setSharedFiltering('all');
			this.gridOptions.api.setFilterModel(null);
			this.gridOptions.api.onFilterChanged();
			this.gridOptions.api.deselectAll();
		}
	}

	private setSharedFiltering(value: string) {
		if (this.gridOptions.api) {
			this.gridOptions.api.getFilterInstance('shared').setModel({
				value: value
			});
			this.gridOptions.api.onFilterChanged();
		}
	}

	private setCustomizedFiltering(value: string) {
		if (this.gridOptions.api) {
			this.gridOptions.api.getFilterInstance('customized').setModel({
				value: value
			});
			this.gridOptions.api.onFilterChanged();
		}
	}

	private ngOnInitImpl() {
		this.nuclosConfig.getSystemParameters().pipe(take(1)).subscribe(params => {
			this.isDevMode = params.is(SystemParameter.ENVIRONMENT_DEVELOPMENT);
		});

		/**
		 * update view if preferences have been changed from other browser tabs
		 */
		this.browserRefreshService
			.onPreferenceChange()
			.pipe(takeUntil(this.unsubscribe$))
			.subscribe((preference: Preference<any> | undefined) => {
				if (preference === undefined) {
					this.loadData();
					this.updatePreferenceItemView();
				} else {
					let index = this.preferencesItems.findIndex(
						p => p.prefId === preference.prefId
					);
					if (index > -1) {
						if (preference.prefId) {
							this.preferences.getPreference(preference.prefId).pipe(take(1)).subscribe(
								loadedPref => {
									loadedPref.boName = this.getBoName(loadedPref);
									this.preferencesItems[index] = loadedPref;
									if (
										this.selectedPreferenceItem &&
										this.selectedPreferenceItem.prefId === preference.prefId
									) {
										this.selectedPreferenceItem = loadedPref;
									}
									this.updatePreferenceItemView();
								},
								error => {
									// remove pref from list
									this.preferencesItems.splice(index, 1);
								}
							);
						}
					} else {
						// new entry load complete list
						this.loadData();
						this.updatePreferenceItemView();
					}
				}
			});

		this.loadData();
	}

	private loadData() {
		this.preferencesAdminService.loadPreferences().pipe(takeUntil(this.unsubscribe$)).subscribe(prefs => {
			this.preferencesItems = prefs;
			this.updatePreferenceItemView();

			if (this.gridOptions.api) {
				this.gridOptions.api.sizeColumnsToFit();
			}

			if (this.routeParamsSubscription) {
				this.routeParamsSubscription.unsubscribe();
			}

			this.routeParamsSubscription = this.route.params.subscribe((params: Params) => {
				let prefId = params['prefId'];
				if (prefId) {
					// TODO
					// navigating between "/preferences" and "/preferences/{prefId}" destroys/recreates new component
					// prevent this by using 'list' as prefId parameter
					if (prefId === 'list') {
						return;
					}

					this.selectedPreferenceItem = this.preferencesItems.filter(
						pref => pref.prefId === prefId
					)[0];
					if (this.selectedPreferenceItem) {
						// select row in ag-grid
						setTimeout(() => {
							if (this.gridOptions.api) {
								let firstElement = this.gridOptions.api
									.getRenderedNodes()
									.filter(node => node.data.prefId === prefId)
									.shift();
								if (firstElement) {
									firstElement.setSelected(true);
								}
							}
						});

						if (this.isSharingAllowed) {
							this.preferences
								.getPreferenceShareGroups(this.selectedPreferenceItem)
								.pipe(take(1))
								.subscribe((data: any) => {
									this.selectedUserUserRoles = data.userRoles;
								});
						}
					}
				}
			});

			this.gridOptions.context = {
				componentParent: this
			};
			this.gridOptions.floatingFilter = true;
			this.gridOptions.deltaRowDataMode = true;
			this.gridOptions.getRowNodeId = data => data.prefId;
			this.gridOptions.columnDefs = [
				{
					headerName: this.i18n.getI18n('webclient.preferences.shared'),
					field: 'shared',
					width: 80,
					cellRendererFramework: SharedRendererComponent,
					headerCheckboxSelection: true,
					headerCheckboxSelectionFilteredOnly: true,
					valueGetter: this.valueGetter,
					comparator: (valueA, valueB) => {
						if (valueA && valueB) {
							return valueA.shared > valueB.shared ? -1 : 1;
						}
						return 0;
					},
					filterFramework: SharedFilterComponent
				},
				{
					headerName: this.i18n.getI18n('webclient.preferences.type'),
					field: 'type',
					width: 80,
					cellRendererFramework: TypeRendererComponent,
					valueGetter: this.valueGetter,
					comparator: (valueA, valueB) => {
						if (valueA && valueB) {
							return valueA.type > valueB.type ? -1 : 1;
						}
						return 0;
					}
				},
				{
					headerName: this.i18n.getI18n('webclient.preferences.businessObject'),
					field: 'boName',
					width: 200,
					sort: 'asc'
				},
				{
					headerName: this.i18n.getI18n('webclient.preferences.layout'),
					field: 'layoutName',
					width: 200
				},
				{
					headerName: this.i18n.getI18n('webclient.preferences.name'),
					field: 'name',
					width: 200
				},
				{
					headerName: '',
					width: 50,
					pinned: 'right',
					field: 'customized',
					cellRendererFramework: EditRowRendererComponent,
					valueGetter: this.valueGetter,
					suppressSorting: true,
					filterFramework: CustomizedFilterComponent
				}
			];
			this.gridOptions.enableColResize = true;
			this.gridOptions.enableFilter = true;
			this.gridOptions.enableSorting = true;
			this.gridOptions.rowSelection = 'multiple';

			this.gridOptions.onSelectionChanged = () => {
				if (this.gridOptions.api) {
					let selectedRows = this.gridOptions.api.getSelectedRows();
					if (selectedRows.length === 1) {
						this.selection = 'single';
						let preferenceItem = selectedRows[0];
						this.selectPreferenceItem(preferenceItem);
					} else if (selectedRows.length > 1) {
						this.selection = 'multiple';
						this.selectedPreferenceItem = selectedRows[0];
						this.displayMultipleRowUserGroupSelection();
					} else {
						this.selection = 'none';
						this.selectedPreferenceItem = undefined;
					}
				}
			};
			if (this.gridOptions.api) {
				this.gridOptions.api.redrawRows();
			}
		});
	}

	private valueGetter(col): Preference<any> {
		return col.data;
	}

	private getBoName(item: Preference<any>): string {
		return item.boMetaId.substring(item.boMetaId.lastIndexOf('_') + 1);
	}

	private displayMultipleRowUserGroupSelection() {
		if (!this.multiselectUserRoles && this.selectedPreferenceItem) {
			/*
			 the user should be able to share preferences to the same group he is assigned to
			 it is appropriate to get the share groups of any preference
			 */
			this.preferences
				.getPreferenceShareGroups(this.selectedPreferenceItem)
				.pipe(take(1))
				.subscribe((data: any) => {
					this.multiselectUserRoles = data.userRoles.map(ur => {
						return {
							name: ur.name,
							userRoleId: ur.userRoleId,
							shared: false
						};
					});
				});
		}
	}
}
