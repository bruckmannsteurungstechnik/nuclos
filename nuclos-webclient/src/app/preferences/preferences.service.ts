import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IPreference, IPreferenceContent, IPreferenceFilter } from '@nuclos/nuclos-addon-api';
import { EMPTY, Observable, of as observableOf, throwError as observableThrowError } from 'rxjs';

import { catchError, concatMap, map, mergeMap, tap } from 'rxjs/operators';
import { NuclosCache, NuclosCacheService } from '../cache/shared/nuclos-cache.service';
import { MetaService } from '../entity-object-data/shared/meta.service';
import { Logger } from '../log/shared/logger';
import { BrowserRefreshService } from '../shared/browser-refresh.service';
import { FqnService } from '../shared/fqn.service';
import { NuclosConfigService } from '../shared/nuclos-config.service';
import { NuclosHttpService } from '../shared/nuclos-http.service';
import {
	IUserRole,
	Preference,
	PreferenceContent,
	PreferenceType,
	SearchtemplatePreferenceContent,
	SideviewmenuPreferenceContent
} from './preferences.model';

export const PREFERENCE_NAME_MAX_LENGTH = 255;

@Injectable()
export class PreferencesService {
	private preferencesCache: NuclosCache;

	constructor(
		private nuclosConfig: NuclosConfigService,
		private cacheService: NuclosCacheService,
		private http: NuclosHttpService,
		private metaService: MetaService,
		private fqnService: FqnService,
		private browserRefreshService: BrowserRefreshService,
		private $log: Logger
	) {
		this.preferencesCache = this.cacheService.getCache('preferencesCache');
	}

	getPreference(prefId: string): Observable<IPreference<IPreferenceContent>> {
		return this.http
			.get(this.nuclosConfig.getRestHost() + '/preferences/' + prefId)
			.pipe(map(this.handleSideviewmenuPrefs));
	}

	getPreferences(
		filter: IPreferenceFilter,
		useCache = false
	): Observable<Array<Preference<PreferenceContent>>> {
		this.$log.debug('filter: %o', filter);

		let filterString = filter.type ? filter.type.join(',') : undefined;
		let key = filter.boMetaId + ':' + filterString;
		this.$log.debug('pref key: %o', key);

		let params = new HttpParams();
		if (filter.boMetaId) {
			params = params.append('boMetaId', filter.boMetaId);
		}
		if (filter.layoutId) {
			params = params.append('layoutId', filter.layoutId);
		}
		if (filter.orLayoutIsNull) {
			params = params.append('orLayoutIsNull', 'true');
		}
		if (filter.returnSubBoPreferences) {
			params = params.append('returnSubBo', 'true');
		}
		if (filterString) {
			params = params.append('type', filterString);
		}

		let observable = this.http
			.get(this.nuclosConfig.getRestHost() + '/preferences', {
				params: params
			})
			.pipe(
				catchError(e => {
					this.$log.warn('Could not load prefs for filter %o: %o', filter, e);
					return EMPTY;
				}),
				map((preferences: Preference<any>[]) => {
					return preferences.map(this.handleSideviewmenuPrefs);
				}),
				map(this.preferenceFormatMapper()),
				tap(prefs =>
					Logger.instance.debug('Loaded prefs for filter %o = %o', filter, prefs)
				)
			);

		let cacheKey = key + '-' + filterString;

		return useCache ? this.preferencesCache.get(cacheKey, observable) : observable;
	}

	getPreferenceShareGroups(preferenceItem: Preference<any>) {
		return this.http.get(
			this.nuclosConfig.getRestHost() + '/preferences/' + preferenceItem.prefId + '/share'
		);
	}

	shareOrUnsharePreferenceItem(preferenceItem: Preference<any>, userRole: IUserRole) {
		let url =
			this.nuclosConfig.getRestHost() +
			'/preferences/' +
			preferenceItem.prefId +
			'/share/' +
			userRole.userRoleId;

		let restCall: Observable<any>;
		if (userRole.shared) {
			restCall = this.http.post(url, null);
		} else {
			restCall = this.http.delete(url);
		}

		return restCall.pipe(
			catchError(e => {
				this.$log.warn('Could not share or unshare preference %o: %o', preferenceItem, e);
				return EMPTY;
			}),
			tap(() => {
				this.browserRefreshService.emitPreferenceChange(preferenceItem);
			})
		);
	}

	/**
	 * removes all shared roles
	 * @param preferenceItem
	 * @return {Promise<T>}
	 */
	unsharePreferenceItem(preferenceItem: Preference<any>): Promise<any> {
		return new Promise(resolve => {
			this.getPreferenceShareGroups(preferenceItem).subscribe((data: any) => {
				let promises: Promise<any>[] = [];
				for (let userRole of data.userRoles) {
					if (userRole.shared) {
						let url =
							this.nuclosConfig.getRestHost() +
							'/preferences/' +
							preferenceItem.prefId +
							'/share/' +
							userRole.userRoleId;
						let promise = this.http.delete(url).toPromise();
						promises.push(promise);
					}
				}
				Promise.all(promises).then(() => {
					this.browserRefreshService.emitPreferenceChange(preferenceItem);
					resolve();
				});
			});
		});
	}

	updatePreferenceShare(preferenceItem: Preference<any>): Observable<void> {
		return this.http.put<any>(
			this.nuclosConfig.getRestHost() + '/preferences/' + preferenceItem.prefId + '/share',
			preferenceItem
		);
	}

	/**
	 * reset customized preferences
	 */
	resetCustomizedPreferenceItems(
		types: PreferenceType[],
		eoClassId?: string
	): Observable<boolean> {
		let typesParamString = '?types=' + types.join(',');
		let eoClasssIdParamString = eoClassId ? '&boMetaId=' + eoClassId : '';
		return this.http
			.delete(
				this.nuclosConfig.getRestHost() +
					'/preferences/customized' +
					typesParamString +
					eoClasssIdParamString
			)
			.pipe(
				map(
					// no json response when resetting preference
					() => true
				),
				tap(() => {
					this.browserRefreshService.emitPreferencesChange();
				})
			);
	}

	/**
	 * TODO: Should be moved to SelectableService?!
	 */
	selectPreference(preferenceItem: Preference<any>, layoutId?: string): Observable<void> {
		// If this is a new preference, it must be saved first
		if (!preferenceItem.prefId) {
			return this.savePreferenceItem(preferenceItem).pipe(
				concatMap(() => this.selectPreference(preferenceItem, layoutId))
			);
		}

		return this.http.put<any>(this.getSelectionURL(preferenceItem, layoutId), undefined).pipe(
			catchError(e => {
				this.$log.warn('Could not select preference %o: %o', preferenceItem, e);
				return EMPTY;
			})
		);
	}

	/**
	 * TODO: Should be moved to SelectableService?!
	 */
	deselectPreference(preferenceItem: Preference<any>, layoutId?: string): Observable<void> {
		if (!preferenceItem.prefId) {
			this.$log.warn('Can not delete preference without prefId');
			return EMPTY;
		}

		return this.http.delete<any>(this.getSelectionURL(preferenceItem, layoutId)).pipe(
			catchError(e => {
				this.$log.warn('Could not deselect preference %o: %o', preferenceItem, e);
				return EMPTY;
			})
		);
	}

	deleteCustomizedPreferenceItem(preferenceItem: Preference<any>): Observable<boolean> {
		if (preferenceItem.customized) {
			return this.deletePreferenceItem(preferenceItem);
		}
		return observableOf(false);
	}

	/**
	 * Resets a customized preference, i.e. deletes the customization and reloads the preference.
	 * The given preferenceItem is updated with the reloaded preference data.
	 */
	resetCustomizedPreferenceItem(preferenceItem: Preference<any>) {
		if (preferenceItem.customized) {
			return this.deletePreferenceItem(preferenceItem).pipe(
				mergeMap(() => this.getPreference(preferenceItem.prefId!))
			);
		}

		return observableThrowError('Preference is not customized - can not reset');
	}

	/**
	 * Saves the given preference item.
	 *
	 * If the item is new (without prefId), it is saved as new preference.
	 * If it is updated, it might be saved as 'customization' of a shared preference.
	 *
	 */
	savePreferenceItem(preferenceItem: Preference<any>): Observable<Preference<any>> {
		if (preferenceItem.name && preferenceItem.name.length > PREFERENCE_NAME_MAX_LENGTH) {
			preferenceItem.name =
				preferenceItem.name.substr(0, PREFERENCE_NAME_MAX_LENGTH - 2) + '..';
		}

		if (preferenceItem.prefId) {
			// update existing preference
			return this.http
				.put(
					this.nuclosConfig.getRestHost() + '/preferences/' + preferenceItem.prefId,
					preferenceItem
				)
				.pipe(
					map(
						// no json response when updating preference
						() => preferenceItem
					),
					tap(pref => {
						pref.dirty = false;
						// update a shared preference? -> set it customized
						if (pref.shared === true) {
							pref.customized = true;
						}
						this.browserRefreshService.emitPreferenceChange(pref);
					})
				);
		} else {
			return this.http
				.post(this.nuclosConfig.getRestHost() + '/preferences', preferenceItem)
				.pipe(
					tap((pref: Preference<any>) => {
						preferenceItem.prefId = pref.prefId;
						preferenceItem.dirty = false;
						this.browserRefreshService.emitPreferenceChange(pref);
					})
				);
		}
	}

	deletePreferenceItem(preferenceItem: Preference<any>): Observable<boolean> {
		return this.http
			.delete(this.nuclosConfig.getRestHost() + '/preferences/' + preferenceItem.prefId)
			.pipe(
				map(
					// no json response when deleting preference
					() => true
				),
				tap(() => {
					this.browserRefreshService.emitPreferenceChange(preferenceItem);
				})
			);
	}

	/**
	 * Delete multipe preference items.
	 * Shared preferences will be unshared.
	 * @param preferenceItems
	 * @return {Observable<R>}
	 */
	deletePreferenceItems(preferenceItems: Preference<any>[]): Observable<boolean> {
		let prefIds = preferenceItems.map(p => p.prefId);
		return this.http
			.post(this.nuclosConfig.getRestHost() + '/preferences/delete', prefIds)
			.pipe(
				map(
					// no json response when deleting preference
					() => true
				)
			);
	}

	/**
	 * find preferences which are referencing given preferences
	 * @param preferenceItems
	 * @return {Observable<R>}
	 */
	findReferencingPreferences(preferenceItems: Preference<any>[]): Observable<Preference<any>[]> {
		let prefIds = preferenceItems.map(p => p.prefId);
		return this.http.post<Preference<any>[]>(
			this.nuclosConfig.getRestHost() + '/preferences/findReferencingPreferences',
			prefIds
		);
	}

	/**
	 * assign/remove user roles to all preferences depending on userRole.selected flag
	 * not selected user roles will be removed
	 *
	 * TODO: Use Observables instead of Promises
	 */
	shareUserRoles(preferenceItems: Preference<any>[], userRoles: IUserRole[]): Promise<any> {
		return Promise.all([
			...preferenceItems.map(pref => {
				return Promise.all([
					...userRoles.map(userRole => {
						return this.shareOrUnsharePreferenceItem(pref, userRole).toPromise();
					})
				]);
			})
		]);
	}

	/**
	 * convert preferences format from webclient1 to webclient2
	 */
	private preferenceFormatMapper() {
		return (preferences: Preference<any>[]) => {
			return preferences.map((pref: Preference<any>) => {
				if (pref.type === 'searchtemplate') {
					let searchtemplatePref = pref as Preference<SearchtemplatePreferenceContent>;
					if (pref.content.attributes && !pref.content.columns) {
						searchtemplatePref.content.columns = pref.content.attributes;
						this.metaService.getBoMeta(pref.boMetaId).subscribe(meta => {
							searchtemplatePref.content.columns.forEach(column => {
								if (column.enableSearch) {
									column.selected = true;
									column.isValid = true;
								}
								let attrName = this.fqnService.getShortAttributeName(
									pref.boMetaId,
									column.boAttrId
								);
								let attributeLabel = meta.getAttributeLabel(attrName);
								if (attributeLabel) {
									column.name = attributeLabel;
								}
							});
						});
					}
				}
				return pref;
			});
		};
	}

	/*
	 * in preferences only selected columns are stored (without selected flag)
	 */
	private handleSideviewmenuPrefs(
		pref: Preference<PreferenceContent>
	): Preference<PreferenceContent> {
		if (pref.type === 'table' || pref.type === 'subform-table') {
			let sideviewmenuPref = <Preference<SideviewmenuPreferenceContent>>pref;
			if (sideviewmenuPref.content) {
				if (sideviewmenuPref.content.columns) {
					sideviewmenuPref.content.columns.forEach(c => (c.selected = true));
				}
				if (sideviewmenuPref && !sideviewmenuPref.content.sideviewMenuWidth) {
					sideviewmenuPref.content.sideviewMenuWidth = 200;
				}
			}
		}
		return pref;
	}

	private getSelectionURL(preferenceItem: Preference<any>, layoutId?: string) {
		return (
			this.nuclosConfig.getRestHost() +
			'/preferences/' +
			preferenceItem.prefId +
			'/select' +
			(layoutId ? '/' + layoutId : '')
		);
	}
}
