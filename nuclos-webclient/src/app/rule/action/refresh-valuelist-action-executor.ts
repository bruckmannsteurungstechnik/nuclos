import { take } from 'rxjs/operators';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { Logger } from '../../log/shared/logger';
import { AbstractActionExecutor } from './abstract-action-executor';

export class RefreshValuelistActionExecutor extends AbstractActionExecutor<RuleActionRefreshValuelist> {
	constructor(
		event: RuleEvent,
		action: RuleActionRefreshValuelist
	) {
		super(event, action);
	}

	execute(eo: EntityObject) {
		Logger.instance.debug('Refresh valuelist: %o on %o', this.action, eo);
		this.setVlpParameters(eo);
		eo.invalidateValuelist(this.action.targetcomponent);
	}

	setVlpParameters(eo: EntityObject) {
		let entity = this.action.entity || eo.getEntityClassId();
		let parameterName = this.action.parameterForSourcecomponent;

		let sourceEo = eo;

		// If the sourcecomponent FQN does not match the given EO, then the given EO
		// is a Sub-EO and the parameter must be taken from the root EO.
		if (!this.event.sourcecomponent.startsWith(eo.getEntityClassId() + '_')) {
			Logger.instance.warn('Using root EO for %o on %o', this.event.sourcecomponent, eo);
			sourceEo = eo.getRootEo();
		}

		if (!this.event.sourcecomponent.startsWith(sourceEo.getEntityClassId() + '_')) {
			Logger.instance.warn('Source EO %o does not match source component %o - ignoring', sourceEo, this.event.sourcecomponent);
			return;
		}

		let parameterValue = sourceEo.getAttribute(this.event.sourcecomponent);

		let targetAttributeName = this.action.targetcomponent;
		sourceEo.getMeta().pipe(take(1)).subscribe(entityMeta => {
			let attributeMeta = entityMeta.getAttributeMeta(this.event.sourcecomponent);

			if (!attributeMeta) {
				Logger.instance.error('No attributeMeta for %o on %o', this.event.sourcecomponent, entityMeta);
				return;
			}

			// TODO: It should not be necessary to set a missing reference parameter explicitly to null
			if (attributeMeta && attributeMeta.isReference() && (!parameterValue || !parameterValue.id)) {
				parameterValue = {id: null};
			}

			eo.getVlpContext().setVlpParameter(
				entity,
				targetAttributeName,
				parameterName,
				parameterValue
			);
		});
	}
}
