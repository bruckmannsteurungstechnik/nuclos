import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { Logger } from '../../log/shared/logger';
import { AbstractActionExecutor } from './abstract-action-executor';

export class ReinitSubformActionExecutor extends AbstractActionExecutor<RuleActionReinitSubform> {
	constructor(
		event: RuleEvent,
		action: RuleActionReinitSubform
	) {
		super(event, action);
	}

	/**
	 * Re-initializes a subform by reloading its data.
	 */
	execute(eo: EntityObject) {
		Logger.instance.debug('Re-init subform: %o on %o', this.action, eo);
		eo.getDependents(this.action.targetcomponent).reload();
	}
}
