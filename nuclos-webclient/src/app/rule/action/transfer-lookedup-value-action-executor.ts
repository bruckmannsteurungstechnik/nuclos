import { take } from 'rxjs/operators';
import { DataService } from '../../entity-object-data/shared/data.service';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { Logger } from '../../log/shared/logger';
import { AbstractActionExecutor } from './abstract-action-executor';

/**
 * Transfers a value from a referenced object to another attribute of the given EO.
 */
export class TransferLookedupValueActionExecutor extends AbstractActionExecutor<RuleActionTransferLookedupValue> {
	constructor(
		event: RuleEvent,
		action: RuleActionTransferLookedupValue
	) {
		super(event, action);
	}

	/**
	 * Transfers the value.
	 * Needs a data service to fetch the referenced EO attribute.
	 */
	execute(eo: EntityObject, dataService: DataService) {
		Logger.instance.debug('Transfer looked up value: %o on %o', this.action, eo);
		let source = eo.getAttribute(this.event.sourcecomponent);
		let id = source && source.id;
		if (id) {
			dataService.fetchByAttribute(
				this.action.sourcefield,
				id,
				this.action.targetcomponent,
				eo.getId()
			).pipe(take(1)).subscribe(sourceEO => {
				let value = sourceEO.getAttribute(this.action.sourcefield);
				eo.setAttribute(this.action.targetcomponent, value);
			});
		} else {
			eo.setAttribute(this.action.targetcomponent, null);
		}
	}
}
