/* tslint:disable:no-unused-variable */
import { inject, TestBed } from '@angular/core/testing';
import { RuleService } from './rule.service';

xdescribe('Service: Rule', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [RuleService]
		});
	});

	it('should ...', inject([RuleService], (service: RuleService) => {
		expect(service).toBeTruthy();
	}));
});
