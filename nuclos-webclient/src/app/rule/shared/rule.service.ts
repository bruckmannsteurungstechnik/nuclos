import { Injectable, Injector } from '@angular/core';
import { EMPTY, Observable } from 'rxjs';

import { map, mergeMap, publishReplay, refCount } from 'rxjs/operators';
import { DataService } from '../../entity-object-data/shared/data.service';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { LayoutService } from '../../layout/shared/layout.service';
import { Logger } from '../../log/shared/logger';
import { NuclosHttpService } from '../../shared/nuclos-http.service';
import { RuleExecutor } from './rule-executor';

@Injectable()
export class RuleService {
	private rulesCache: Map<string, Rules>;

	constructor(
		private layoutService: LayoutService,
		private http: NuclosHttpService,
		private injector: Injector,
		private $log: Logger,
	) {
		this.rulesCache = new Map<string, Rules>();
	}

	/**
	 * Fetches rules for the current layout of the given EO.
	 *
	 * TODO: Rules should later be defined directly on the Entity class.
	 */
	getLayoutRules(eo: EntityObject): Observable<Rules> {
		return this.layoutService.getWebLayoutURLDynamically(eo).pipe(
			mergeMap(url => {
					if (!url) {
						this.$log.warn('Not layout defined for %o - could not fetch layout rules', eo);
						return EMPTY;
					}

					if (!this.rulesCache[url]) {
						this.rulesCache[url] = this.http
							.get(url.replace('/calculated', '/rules'))	// TODO: Avoid URL manipulations
							.pipe(
								publishReplay(1),
								refCount(),
							);
					}

					return this.rulesCache[url];
				}
			));
	}

	/**
	 * Fetches the rules of the given EO and subscribes a new RuleExecutor to it.
	 *
	 * If a parent EO is given, the Rules are fetched from its layout, as the eo is a then
	 * a subform EO and subform rules are defined in the parent layout.
	 */
	updateRuleExecutor(eo: EntityObject, parentEO?: EntityObject): Observable<EntityObject> {
		let layoutEO = parentEO || eo;
		return this.getLayoutRules(layoutEO).pipe(map(
			rules => {
				let ruleExecutor = new RuleExecutor(rules, this.injector.get(DataService));
				eo.removeListenersByType(RuleExecutor);
				eo.addListener(ruleExecutor);
				ruleExecutor.initializeVlps(eo);
				return eo;
			}
		));
	}
}
