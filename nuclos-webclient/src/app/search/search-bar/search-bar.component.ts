import { Component, Input, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { EntityObjectEventService } from '../../entity-object-data/shared/entity-object-event.service';
import { EntityMeta } from '../../entity-object-data/shared/bo-view.model';
import { IEntityObject } from '@nuclos/nuclos-addon-api';
import { Preference } from '../../preferences/preferences.model';
import { SearchtemplatePreferenceContent } from '../../preferences/preferences.model';
import { EntityObjectSearchfilterService } from '../../entity-object-data/shared/entity-object-searchfilter.service';

@Component({
	selector: 'nuc-search-bar',
	templateUrl: './search-bar.component.html',
	styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnDestroy {
	@Input() meta: EntityMeta;

	eo: IEntityObject | undefined;

	private favoriteSearchfilters: Preference<SearchtemplatePreferenceContent>[] = [];

	private unsubscribe$ = new Subject<void>();

	constructor(
		private eoEventService: EntityObjectEventService,
		private eoSearchfilterService: EntityObjectSearchfilterService
	) {
		this.eoEventService.observeSelectedEo().pipe(takeUntil(this.unsubscribe$)).subscribe(eo => (this.eo = eo));

		this.eoSearchfilterService.observeAllSearchfilters().pipe(takeUntil(this.unsubscribe$)).subscribe(allSearchfilters => {
			this.setFavoriteSearchfilters(allSearchfilters);
		});
	}

	ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	getFavoriteSearchfilters(): Preference<SearchtemplatePreferenceContent>[] {
		return this.favoriteSearchfilters;
	}

	hasFavoriteSearchfilters() {
		return this.favoriteSearchfilters.length > 0;
	}

	isSelectedSearchfilter(searchfilter: Preference<SearchtemplatePreferenceContent>) {
		/*
		let selected = this.eoSearchfilterService.getSelectedSearchfilter();
		return selected === searchfilter;
		 */
		return searchfilter.selected === true;
	}

	/**
	 * for tests
	 */
	getSearchfilterElementClass(searchfilter: Preference<SearchtemplatePreferenceContent>) {
		if (searchfilter.name) {
			return (
				'searchfilter-favorite-button-' +
				searchfilter.name.toLowerCase().replace(/[^a-z0-9-_]/g, '-')
			);
		} else {
			return 'searchfilter-favorite-button-no-name';
		}
	}

	selectSearchfilter(searchfilter) {
		if (this.isSelectedSearchfilter(searchfilter)) {
			// unselect -> find "My search" and select it
			let tempFilter = this.eoSearchfilterService
				.getAllSearchfilters()
				.find(sf => sf.content.isTemp === true);
			if (tempFilter) {
				this.eoSearchfilterService.selectSearchfilter(tempFilter);
			}
		} else {
			this.eoSearchfilterService.selectSearchfilter(searchfilter);
		}
	}

	private setFavoriteSearchfilters(
		allSearchfilters: Preference<SearchtemplatePreferenceContent>[]
	) {
		this.favoriteSearchfilters = EntityObjectSearchfilterService.sortSearchfilters(
			allSearchfilters.filter(searchfilter => searchfilter.content.showAsFavorite === true),
			true
		);
	}
}
