import { Component, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';
import { Preference, SearchtemplatePreferenceContent } from '../../preferences/preferences.model';
import { EntityObjectSearchfilterService } from '../../entity-object-data/shared/entity-object-searchfilter.service';
import { EntityObjectPreferenceService } from '../../entity-object/shared/entity-object-preference.service';
import { SideviewmenuService } from '../../entity-object/shared/sideviewmenu.service';
import { SearchEditorComponent } from '../search-editor/search-editor.component';

@Component({
	selector: 'nuc-search-editor-button',
	templateUrl: './search-editor-button.component.html',
	styleUrls: ['./search-editor-button.component.css']
})
export class SearchEditorButtonComponent implements OnInit {

	constructor(
		private sideviewmenuService: SideviewmenuService,
		private entityObjectPreferenceService: EntityObjectPreferenceService,
		private eoSearchfilterService: EntityObjectSearchfilterService
	) {}

	ngOnInit() {
	}

	toggleSearchEditor() {
		if (this.isEditorFixed()) {
			this.setEditorFixedAndShowing(!this.isEditorFixedAndShowing());
			this.setPopoverShowing(false);
		} else {
			this.setPopoverShowing(!this.isPopoverShowing());
		}
	}

	adjustPopover() {
		SearchEditorComponent.adjustPopover();
	}

	isSelectedSearchfilterEnabled(): boolean {
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			for (let searchElement of searchfilter.content.columns) {
				if (searchElement.operator && searchElement.enableSearch !== false && searchElement.selected !== false) {
					return true;
				}
			}
		}
		return false;
	}

	getSelectedSearchfilter(): Preference<SearchtemplatePreferenceContent> | undefined {
		return this.eoSearchfilterService.getSelectedSearchfilter();
	}

	getSideviewmenuPref() {
		return this.entityObjectPreferenceService.selectedSideviewmenuPref$.getValue();
	}

	isPopoverShowing(): boolean {
		let pref = this.getSideviewmenuPref();
		if (pref) {
			return pref.content.searchEditorPopoverShowing;
		}
		return false;
	}

	setPopoverShowing(popoverShowing: boolean) {
		let pref = this.getSideviewmenuPref();
		if (pref) {
			pref.content.searchEditorPopoverShowing = popoverShowing;
		}
		if (popoverShowing) {
			this.adjustPopover();
		}
	}

	isEditorFixed(): boolean {
		let pref = this.getSideviewmenuPref();
		if (pref) {
			return pref.content.sideviewSearchEditorFixed;
		}
		return false;
	}

	isEditorFixedAndShowing() {
		let pref = this.getSideviewmenuPref();
		if (pref && pref.content.sideviewSearchEditorFixed) {
			return pref.content.sideviewSearchEditorFixedAndShowing;
		}
		return false;
	}

	private setEditorFixedAndShowing(value: boolean) {
		let pref = this.getSideviewmenuPref();
		if (pref && pref.content.sideviewSearchEditorFixed) {
			pref.content.sideviewSearchEditorFixedAndShowing = value;
			this.saveSideviewmenuPreference();
		}
	}

	private saveSideviewmenuPreference(): void {
		let pref = this.getSideviewmenuPref();
		if (pref) {
			this.sideviewmenuService.saveSideviewmenuPreference(pref).pipe(take(1)).subscribe();
		}
	}

}
