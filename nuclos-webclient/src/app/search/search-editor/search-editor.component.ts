import { Component, ElementRef, Input, ViewChild, OnDestroy } from '@angular/core';
import { IEntityObject, UserAction } from '@nuclos/nuclos-addon-api';
import { debounceTime, distinctUntilChanged, take, tap, takeUntil } from 'rxjs/operators';
import { EMPTY, Observable, Observer, Subject, of } from 'rxjs';
import { AuthenticationService } from '../../authentication';
import {
	EntityAttrMeta,
	EntityMeta,
	InputType
} from '../../entity-object-data/shared/bo-view.model';
import { EntityObjectEventService } from '../../entity-object-data/shared/entity-object-event.service';
import { EntityObjectResultService } from '../../entity-object-data/shared/entity-object-result.service';
import { EntityObjectSearchfilterService } from '../../entity-object-data/shared/entity-object-searchfilter.service';
import { MetaService } from '../../entity-object-data/shared/meta.service';
import { EntityObjectPreferenceService } from '../../entity-object/shared/entity-object-preference.service';
import { SideviewmenuService } from '../../entity-object/shared/sideviewmenu.service';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { Logger } from '../../log/shared/logger';
import { DialogService } from '../../popup/dialog/dialog.service';
import {
	Preference,
	SearchtemplateAttribute,
	SearchtemplatePreferenceContent
} from '../../preferences/preferences.model';
import { DatetimeService } from '../../shared/datetime.service';
import { FqnService } from '../../shared/fqn.service';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { SystemParameter } from '../../shared/system-parameters';
import { Selectable } from '../../two-side-multi-select/two-side-multi-select.model';
import { Operator } from '../operator';
import { SearchService } from '../shared/search.service';
import { SearchfilterService } from '../shared/searchfilter.service';
import { SearchFilterSelectorComponent } from './search-filter-selector/search-filter-selector.component';

@Component({
	selector: 'nuc-search-editor',
	templateUrl: './search-editor.component.html',
	styleUrls: ['./search-editor.component.css']
})
export class SearchEditorComponent implements OnDestroy {
	static adjustPopover(usePrefsButton = false) {
		let button = $(
			usePrefsButton ? '.view-preferences-button-mainbar' : '#search-editor-button'
		);
		if (button.offset() !== undefined) {
			let popover = $('#search-editor-popover');
			popover.css('top', button.offset().top + button.outerHeight() + 'px');
		}
	}
	@Input() searchAttributesContainerHeight: number;

	@ViewChild('searchfilterSelector') searchfilterSelector: SearchFilterSelectorComponent;

	customSearchfilterName;

	metaEditingActive = false;

	itemResortActive = false;

	itemResortBefore: string[];

	itemSelectables: Selectable[] = [];

	showAsFavorite = false;

	favoriteIcon: string | undefined;

	position: number | undefined;

	datePattern: string;

	quicksearchDelayTime: number;

	private meta: EntityMeta | undefined;
	private dependentMetaMap: Map<string, EntityMeta>;

	private eo: IEntityObject | undefined;

	private selectedSearchfilter: Preference<SearchtemplatePreferenceContent> | undefined;

	private allSearchfilters: Preference<SearchtemplatePreferenceContent>[];

	/**
	 * provides changes of searchtemplate for debounced search / pref save
	 */
	private searchRequestTimer: Subject<number> = new Subject<number>();
	private searchFilterUpdateTimer: Subject<number> = new Subject<number>();

	private unsubscribe$ = new Subject<void>();

	constructor(
		private authService: AuthenticationService,
		private config: NuclosConfigService,
		private fqnService: FqnService,
		private datetimeService: DatetimeService,
		private searchService: SearchService,
		private searchfilterService: SearchfilterService,
		private sideviewmenuService: SideviewmenuService,
		private entityObjectPreferenceService: EntityObjectPreferenceService,
		private eoEventService: EntityObjectEventService,
		private eoSearchfilterService: EntityObjectSearchfilterService,
		private eoResultService: EntityObjectResultService,
		private metaService: MetaService,
		private dialogService: DialogService,
		private i18n: NuclosI18nService,
		private ref: ElementRef,
		private $log: Logger
	) {
		this.datePattern = this.datetimeService.getDatePattern();

		this.eoEventService.observeSelectedEo().pipe(take(1)).subscribe(eo => (this.eo = eo));

		this.config.getSystemParameters().pipe(take(1)).subscribe(params => {
			this.quicksearchDelayTime = params.get(SystemParameter.QUICKSEARCH_DELAY_TIME);
			this.searchRequestTimer
				.asObservable()
				.pipe(
					debounceTime(this.quicksearchDelayTime),
					distinctUntilChanged(),
					takeUntil(this.unsubscribe$)
				)
				.subscribe(() => {
					this.executeSearch();
				});
		});

		this.searchFilterUpdateTimer
			.asObservable()
			.pipe(
				debounceTime(1000),
				distinctUntilChanged(),
				takeUntil(this.unsubscribe$)
			)
			.subscribe(() => {
				this.handleChangeOfSelectedSearchfilter();
			});

		this.eoResultService.observeSelectedEntityClassId().pipe(takeUntil(this.unsubscribe$)).subscribe(entityClassId => {
			this.updateMeta(entityClassId).pipe(take(1)).subscribe();
		});

		this.eoSearchfilterService.observeSelectedSearchfilter().pipe(takeUntil(this.unsubscribe$)).subscribe(searchfilter => {
			this.selectedSearchfilter = searchfilter;
			if (!this.showSearchItems()) {
				// active editing -> dismiss editor
				this.metaEditingActive = false;
				this.itemResortActive = false;
			}
		});

		this.eoSearchfilterService.observeAllSearchfilters().pipe(takeUntil(this.unsubscribe$)).subscribe(searchfilters => {
			this.allSearchfilters = searchfilters;
		});
	}

	ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	getMeta(): EntityMeta | undefined {
		return this.meta;
	}

	getAttributeMeta(attribute: SearchtemplateAttribute): EntityAttrMeta | undefined {
		if (attribute.dependentBoMetaId === undefined) {
			if (this.meta) {
				return this.meta.getAttributeMetaByFqn(attribute.boAttrId);
			}
		} else {
			if (this.dependentMetaMap !== undefined) {
				let depMeta = this.dependentMetaMap.get(attribute.dependentBoMetaId);
				if (depMeta) {
					return depMeta.getAttributeMetaByFqn(attribute.boAttrId);
				}
			}
		}
		return undefined;
	}

	/**
	 * define which search input component will be used for which attribute
	 */
	getSearchComponent(attribute: SearchtemplateAttribute): string {
		let inputType = this.getInputType(attribute);
		if (inputType === InputType.BOOLEAN) {
			return 'boolean';
		} else if (
			inputType === InputType.REFERENCE &&
			(!attribute.operator || attribute.operator === '=')
		) {
			return 'lov';
		} else if (inputType === InputType.DATE) {
			return 'datepicker';
		}
		return 'string';
	}

	getOperatorDefinitions(inputType: any) {
		return this.searchfilterService.getOperatorDefinitions()[inputType];
	}

	getOperatorDefinition(attribute: SearchtemplateAttribute): Operator | undefined {
		return this.searchfilterService.getOperatorDefinition(attribute);
	}

	showOperator(attribute: SearchtemplateAttribute, operatorDef: Operator): boolean {
		return (
			!(!attribute.nullable && operatorDef.isUnary) &&
			!(
				attribute.boAttrName === 'nuclosStateNumber' ||
				attribute.boAttrName === 'nuclosState'
			)
		);
	}

	inputSearchValue(attribute: SearchtemplateAttribute): void {
		// TODO
		// if (attribute.value !== undefined && attribute.value.id !== undefined) {
		// 	// empty dropdown selection
		// 	attribute.value = undefined;
		// }

		attribute.enableSearch = true;

		let operatorDef = this.searchfilterService.getOperatorDefinition(attribute);
		if (operatorDef) {
			if (operatorDef.isUnary) {
				attribute.value = '';
			} else {
				// TODO: Hack
				window.setTimeout(() => {
					$('#searchfilter-value').focus();
				});
			}
		}

		if (operatorDef) {
			this.searchfilterService.formatValue(attribute, operatorDef);
		}

		// clear input if reference was selected before in lov and then a 'like' search was called
		if (
			attribute.reference &&
			operatorDef &&
			operatorDef.operator === 'like' &&
			attribute &&
			attribute.value &&
			attribute &&
			attribute.value.id
		) {
			delete attribute.value;
		}

		this.searchFilterUpdateTimer.next(new Date().getTime());

		this.doSearch();
	}

	makeTempPersistent(): void {
		let tempSearchfilter = this.getSelectedSearchfilter();
		if (this.meta && tempSearchfilter && tempSearchfilter.content.isTemp) {
			let newSearchfilter = this.createSearchfilter(this.meta);
			this.customSearchfilterName = newSearchfilter.name;
			newSearchfilter.content = tempSearchfilter.content;
			newSearchfilter.content.isTemp = false;
			this.searchfilterService.saveSearchfilter(newSearchfilter).pipe(take(1)).subscribe();
			this.eoSearchfilterService.selectSearchfilter(newSearchfilter);
			let newTemp = this.searchfilterService.createTempSearchfilter(
				this.meta.getEntityClassId()
			);
			tempSearchfilter.selected = false;
			tempSearchfilter.name = newTemp.name;
			tempSearchfilter.content = newTemp.content;
			if (tempSearchfilter.prefId !== undefined) {
				this.searchfilterService.saveSearchfilter(tempSearchfilter).pipe(take(1)).subscribe();
			}
			this.eoSearchfilterService.sortSearchfilters();
		}
	}

	saveChanges() {
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter && searchfilter.dirty === true) {
			// write name changes to pref
			this.editMetaDone();
			// changes to temp are stored immediately
			if (searchfilter.content.isTemp !== true) {
				this.searchfilterService.saveAndSelectSearchfilter(searchfilter).pipe(take(1)).subscribe();
			}
		}
	}

	undoChanges() {
		let searchfilter = this.getSelectedSearchfilter();
		if (
			searchfilter &&
			searchfilter.dirty === true &&
			!searchfilter.content.isTemp &&
			searchfilter.prefId
		) {
			this.searchfilterService.getSearchfilter(searchfilter.prefId).pipe(take(1)).subscribe(pref => {
				this.transferContentToSelectedSearchfilter(pref as Preference<
					SearchtemplatePreferenceContent
				>);
				this.metaEditingActive = false;
				this.doSearch();
			});
		}
	}

	resetCustomization() {
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter && searchfilter.prefId) {
			if (searchfilter.customized === true) {
				this.searchfilterService
					.resetCustomizedSearchfilter(searchfilter)
					.pipe(take(1))
					.subscribe(pref => {
						this.transferContentToSelectedSearchfilter(pref as Preference<
							SearchtemplatePreferenceContent
						>);
						this.metaEditingActive = false;
						this.doSearch();
					});
			} else if (searchfilter.dirty === true) {
				this.undoChanges();
			}
		}
	}

	handleChangeOfSelectedSearchfilter() {
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			if (searchfilter.content.isTemp === true) {
				this.searchfilterService.saveAndSelectSearchfilter(searchfilter).pipe(take(1)).subscribe();
			} else {
				searchfilter.dirty = true;
			}
			for (const attribute of searchfilter.content.columns) {
				if (attribute.focusInput === true) {
					attribute.focusInput = false;
					setTimeout(() => {
						if (this.ref) {
							let componentType = this.getSearchComponent(attribute);
							if (componentType === 'string') {
								let inputs = $(this.ref.nativeElement).find(
									'[id=search-value-' +
										componentType +
										'-' +
										attribute.boAttrId +
										']'
								);
								this.focusFirstInput(inputs);
							} else if (componentType === 'lov') {
								let searchLov = $(this.ref.nativeElement).find(
									'[id=search-value-' +
										componentType +
										'-' +
										attribute.boAttrId +
										']'
								);
								if (searchLov && searchLov.length > 0) {
									this.focusFirstInput(searchLov.find('input'));
								}
							} else if (componentType === 'datepicker') {
								let datepicker = $(this.ref.nativeElement).find(
									'[id=search-value-' +
										componentType +
										'-' +
										attribute.boAttrId +
										']'
								);
								if (datepicker && datepicker.length > 0) {
									this.focusFirstInput(datepicker.find('input'));
								}
							}
						}
					});
				}
			}
		}
	}

	searchItemCheckClicked() {
		this.doSearch();
		this.handleChangeOfSelectedSearchfilter();
	}

	doSearch(doSearch = this.isAutosearch(), immediately = false): void {
		this.$log.debug('doSearch(%o, %o)', doSearch, immediately);

		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			this.validateSearchForm(searchfilter.content);

			if (doSearch) {
				if (immediately) {
					this.executeSearch();
				} else {
					this.searchRequestTimer.next(new Date().getTime());
				}
			}
		}
	}

	isAutosearch() {
		return this.quicksearchDelayTime >= 0;
	}

	removeSearchItem(attribute: SearchtemplateAttribute) {
		attribute.selected = false;

		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			let index = searchfilter.content.columns.findIndex(
				it => it.boAttrId === attribute.boAttrId
			);
			if (index >= 0) {
				searchfilter.content.columns.splice(index, 1);
				this.doSearch();
			}
			this.handleChangeOfSelectedSearchfilter();
		}
	}

	clearSearchfilter() {
		if (this.meta) {
			// find temp filter
			let searchfilter = this.getSelectedSearchfilter();
			if (searchfilter === undefined || !searchfilter.content.isTemp) {
				searchfilter = this.getAllSearchfilters().find(it => it.content.isTemp === true);
			}
			if (searchfilter === undefined) {
				searchfilter = this.searchfilterService.createTempSearchfilter(
					this.meta.getEntityClassId()
				);
			}
			let hasSearchAttributesBefore = searchfilter.content.columns.length > 0;
			this.eoSearchfilterService.selectSearchfilter(searchfilter, true, true);
			if (hasSearchAttributesBefore) {
				this.doSearch();
			}
			/*
			setTimeout(() => {
				if (this.ref) {
					let inputs = $(this.ref.nativeElement).find('input');
					if (inputs && inputs.length === 2) {
						inputs[1].focus();
						inputs[1].click();
					}
				}
			});
			*/
		}
	}

	isDisabled() {
		return !!this.eo && this.eo.isDirty();
	}

	isDirty(): boolean {
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			return searchfilter.dirty === true;
		}
		return false;
	}

	getSelectedSearchfilter(): Preference<SearchtemplatePreferenceContent> | undefined {
		return this.selectedSearchfilter;
	}

	getSelectableColumns(): Selectable[] {
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			return searchfilter.content.columns as Selectable[];
		}
		return [];
	}

	getAllSearchfilters(): Preference<SearchtemplatePreferenceContent>[] {
		return this.allSearchfilters;
	}

	getSideviewmenuPref() {
		return this.entityObjectPreferenceService.selectedSideviewmenuPref$.getValue();
	}

	isTempSearchfilter(): boolean {
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			return searchfilter.content.isTemp === true;
		}
		return false;
	}

	isShared(): boolean {
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			return searchfilter.shared === true;
		}
		return false;
	}

	isCustomized(): boolean {
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			return searchfilter.customized === true;
		}
		return false;
	}

	isShareAllowed(): boolean {
		return this.authService.isActionAllowed(UserAction.SharePreferences) === true;
	}

	isEditorFixed(): boolean {
		let pref = this.getSideviewmenuPref();
		if (pref) {
			return pref.content.sideviewSearchEditorFixed;
		}
		return false;
	}

	setEditorFixed(editorFixed: boolean) {
		let pref = this.getSideviewmenuPref();
		if (pref) {
			if (!editorFixed) {
				// show popover
				pref.content.searchEditorPopoverShowing = true;
				SearchEditorComponent.adjustPopover(true);
			}
			pref.content.sideviewSearchEditorFixed = editorFixed;
			pref.content.sideviewSearchEditorFixedAndShowing = editorFixed;
			this.saveSideviewmenuPreference();
		}
	}

	showSearchItems(): boolean {
		return !this.metaEditingActive && !this.itemResortActive;
	}

	editMeta() {
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			this.itemResortActive = false;
			this.metaEditingActive = !this.metaEditingActive;
			if (this.metaEditingActive) {
				// hide popover, click outside not fired (why?)
				this.searchfilterSelector.setPopoverShowing(false);
				// activated ... init
				this.customSearchfilterName = searchfilter.name;
				this.showAsFavorite = searchfilter.content.showAsFavorite
					? searchfilter.content.showAsFavorite
					: false;
				this.favoriteIcon = searchfilter.content.favoriteIcon;
				this.position = searchfilter.content.position;
			}
		}
	}

	editMetaDone() {
		// save
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			if (this.isMetaChanged()) {
				searchfilter.content.userdefinedName = true;
				searchfilter.name = this.customSearchfilterName;
				searchfilter.content.showAsFavorite = this.showAsFavorite;
				searchfilter.content.favoriteIcon = this.favoriteIcon;
				searchfilter.content.position = this.validatePosition(this.position);
				this.handleChangeOfSelectedSearchfilter();
				this.eoSearchfilterService.sortSearchfilters();
				this.customSearchfilterName = undefined;
				this.showAsFavorite = false;
				this.favoriteIcon = undefined;
				this.position = undefined;
			}
			if (this.itemResortActive) {
				searchfilter.content.columns.sort((a, b) => {
					let indexA = this.itemSelectables.findIndex(value => value.name === a.name);
					let indexB = this.itemSelectables.findIndex(value => value.name === b.name);
					return indexA < indexB ? -1 : 1;
				});
				let itemResortAfter = searchfilter.content.columns.map(col => col.boAttrId);
				if (JSON.stringify(this.itemResortBefore) !== JSON.stringify(itemResortAfter)) {
					this.handleChangeOfSelectedSearchfilter();
				}
				this.itemResortBefore = [];
			}
		}
		this.itemResortActive = false;
		this.metaEditingActive = false;
	}

	resortItems() {
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			this.metaEditingActive = false;
			this.itemResortActive = !this.itemResortActive;
			if (this.itemResortActive) {
				// activated ... init
				this.itemSelectables = searchfilter.content.columns as Selectable[];
				this.itemResortBefore = searchfilter.content.columns.map(col => col.boAttrId);
			}
		}
	}

	deleteSearchfilter() {
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			this.dialogService
				.confirm({
					title: this.i18n.getI18n('webclient.searcheditor.searchfilter.delete'),
					message: this.i18n.getI18n('webclient.searcheditor.searchfilter.delete.confirm')
				})
				.then(
					() => {
						// ok, delete
						this.metaEditingActive = false;
						this.eoSearchfilterService.deleteSearchfilter(searchfilter);
						this.selectTempFilter();
					},
					() => {
						// cancel
					}
				);
		}
	}

	selectTempFilter() {
		let tempFilter = this.getAllSearchfilters().find(it => it.content.isTemp === true);
		if (tempFilter) {
			this.eoSearchfilterService.selectSearchfilter(tempFilter);
		}
	}

	private getInputType(attribute: SearchtemplateAttribute): string | undefined {
		if (attribute.inputType !== undefined) {
			return attribute.inputType;
		}
		let attributeMeta = this.getAttributeMeta(attribute);
		if (attributeMeta) {
			return SearchfilterService.getInputType(<SearchtemplateAttribute>(
				attributeMeta.getMetaData()
			));
		}
		return undefined;
	}

	private transferContentToSelectedSearchfilter(
		otherSearchfilter: Preference<SearchtemplatePreferenceContent>
	) {
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			this.customSearchfilterName = otherSearchfilter.name;
			this.showAsFavorite = otherSearchfilter.content.showAsFavorite
				? otherSearchfilter.content.showAsFavorite
				: false;
			this.favoriteIcon = otherSearchfilter.content.favoriteIcon;
			this.position = otherSearchfilter.content.position;
			searchfilter.name = otherSearchfilter.name;
			searchfilter.content = otherSearchfilter.content;
			searchfilter.dirty = false;
			searchfilter.shared = otherSearchfilter.shared;
			searchfilter.customized = otherSearchfilter.customized;
			this.eoSearchfilterService.selectSearchfilter(searchfilter);
		}
	}

	private focusFirstInput(inputs: JQuery) {
		if (inputs && inputs.length > 0) {
			inputs[0].focus();
			inputs[0].click();
		}
	}

	private executeSearch() {
		this.searchService.initiateDataLoad();
		this.scrollSidebarToTop();
	}

	private validateSearchForm(searchtemplate: SearchtemplatePreferenceContent): void {
		if (!searchtemplate) {
			this.$log.warn('No searchtemplate given');
			return;
		}

		this.$log.debug(
			'Validating searchtemplate: %o with %o attributes',
			searchtemplate,
			searchtemplate.columns.length
		);

		let searchFormIsValid = true;
		searchtemplate.columns.forEach(attribute => {
			let operatorDef = this.searchfilterService.getOperatorDefinition(attribute);
			if (operatorDef) {
				if (operatorDef.isUnary) {
					attribute.value = '';
				}

				attribute.isValid =
					operatorDef.isUnary ||
					((attribute.value !== undefined &&
						attribute.value.length === undefined &&
						attribute.inputType === InputType.NUMBER) ||
					(attribute.value !== undefined &&
						operatorDef.operator === '=' &&
						attribute.value.id &&
						attribute.inputType === InputType.REFERENCE) || // equal search reference with dropdown
					(attribute.value !== undefined &&
						operatorDef.operator === 'like' &&
						attribute.value.length > 0 &&
						attribute.inputType === InputType.REFERENCE) || // equal search reference with dropdown
						(attribute.value !== undefined &&
							attribute.value.length === undefined &&
							attribute.inputType === InputType.DATE) ||
						(attribute.value !== undefined && attribute.value.length > 0) ||
						(operatorDef.operator === '=' &&
							attribute.values &&
							attribute.values.length > 0)); // IN condition

				if (!attribute.isValid && attribute.enableSearch) {
					searchFormIsValid = false;
				}
			} else {
				if (attribute.enableSearch) {
					searchFormIsValid = false;
				}
			}
			searchtemplate.isValid = searchFormIsValid;
		});
	}

	private createSearchfilter(meta: EntityMeta): Preference<SearchtemplatePreferenceContent> {
		let searchfilter = this.searchfilterService.createSearchfilter(meta);
		const searchfilterNew = this.i18n.getI18n('webclient.searchfilter.new');
		const regexp = new RegExp('^' + searchfilterNew + ' [0-9]* *$');
		let searchfilterNumber = 0;
		for (const sf of this.getAllSearchfilters()) {
			if (sf.name && regexp.test(sf.name)) {
				let usedNumber = +sf.name.substr(sf.name.indexOf(' ') + 1).trim();
				if (usedNumber > searchfilterNumber) {
					searchfilterNumber = usedNumber;
				}
			}
		}
		searchfilterNumber++;
		searchfilter.name = searchfilterNew + ' ' + searchfilterNumber;
		searchfilter.content.userdefinedName = true;
		return searchfilter;
	}

	/**
	 * @deprecated: TODO: Sidebar should do this by itself, instead of being manipulated here
	 */
	private scrollSidebarToTop(): void {
		$('#sidebar-list-container').scrollTop(0);
	}

	private saveSideviewmenuPreference(): void {
		let pref = this.getSideviewmenuPref();
		if (pref) {
			this.sideviewmenuService.saveSideviewmenuPreference(pref).pipe(take(1)).subscribe();
		}
	}

	private validatePosition(position: number | undefined): number | undefined {
		if (position) {
			if (position < 1) {
				return 1;
			}
			if (position > 9999) {
				return 9999;
			}
			return position;
		}
		return undefined;
	}

	private isMetaChanged(): boolean {
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			return (
				searchfilter.name !== this.customSearchfilterName ||
				searchfilter.content.showAsFavorite !== this.showAsFavorite ||
				searchfilter.content.favoriteIcon !== this.favoriteIcon ||
				searchfilter.content.position !== this.position
			);
		}
		return false;
	}

	private updateMeta(entityClassId: string | undefined): Observable<EntityMeta> {
		if (!entityClassId) {
			this.meta = undefined;
			return EMPTY;
		}

		return this.metaService.getEntityMeta(entityClassId).pipe(
			tap(meta => {
				let entityContexts = meta.getEntityContexts();
				this.meta = meta;
				if (entityContexts !== undefined) {
					this.metaService.getEntityMetasByContexts(entityContexts)
					.pipe(
						take(1)
					).subscribe(mapMeta => {
						this.dependentMetaMap = mapMeta;
					});
				}
			})
		);
	}
}
