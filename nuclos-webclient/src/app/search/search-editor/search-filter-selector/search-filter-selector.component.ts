import { Component, ElementRef, Input, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { EntityMeta } from '../../../entity-object-data/shared/bo-view.model';
import { EntityObjectSearchfilterService } from '../../../entity-object-data/shared/entity-object-searchfilter.service';
import { Preference, SearchtemplatePreferenceContent } from '../../../preferences/preferences.model';
import { ViewChild } from '@angular/core';

@Component({
	selector: 'nuc-search-filter-selector',
	templateUrl: './search-filter-selector.component.html',
	styleUrls: ['./search-filter-selector.component.css']
})
export class SearchFilterSelectorComponent implements OnInit, OnDestroy {

	@Input() meta: EntityMeta;

	@Input() searchfilter: Preference<SearchtemplatePreferenceContent> | undefined;

	@Input() disabled = false;

	@ViewChild('searchfilterSelectorDropdownMenu') searchfilterSelectorDropdownMenu: ElementRef;

	popoverShowing = false;

	allSearchfilters: Preference<SearchtemplatePreferenceContent>[] = [];

	private unsubscribe$ = new Subject<void>();

	constructor(
		private eoSearchfilterService: EntityObjectSearchfilterService
	) {
	}

	ngOnInit() {
		this.eoSearchfilterService.observeAllSearchfilters().pipe(takeUntil(this.unsubscribe$)).subscribe(
			allSearchfilters => {
				if (allSearchfilters) {
					this.allSearchfilters = allSearchfilters;
				} else {
					this.allSearchfilters = [];
				}
			}
		);
		document.querySelector('body')!.appendChild(this.searchfilterSelectorDropdownMenu.nativeElement);
	}

	ngOnDestroy(): void {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
		document.querySelector('body')!.removeChild(this.searchfilterSelectorDropdownMenu.nativeElement);
	}

	getCurrentSearchfilterName() {
		if (this.searchfilter) {
			return this.searchfilter.name;
		}
		return '';
	}

	togglePopover() {
		this.setPopoverShowing(!this.popoverShowing);
	}

	setPopoverShowing(popoverShowing: boolean) {
		if (this.disabled) {
			return;
		}
		this.popoverShowing = popoverShowing;
		if (popoverShowing) {
			this.adjustPopover();
		}
		let menu = $('#searchfilter-selector-dropdown-menu');
		menu.css('display', popoverShowing ? 'block' : 'none');
	}

	adjustPopover() {
		let dropdown = $('#searchfilter-selector-dropdown');
		if (dropdown.offset() !== undefined) {
			let menu = $('#searchfilter-selector-dropdown-menu');
			menu.css('top', (dropdown.offset().top + dropdown.outerHeight() - 6) + 'px');
			menu.css('left', (dropdown.offset().left) + 'px');
		}
	}

	selectSearchfilter(searchfilter) {
		this.eoSearchfilterService.selectSearchfilter(searchfilter);
	}

}
