import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractSearchItem } from '../abstract-search-item';

@Component({
	selector: 'nuc-search-item-boolean',
	templateUrl: './search-item-boolean.component.html',
	styleUrls: ['./search-item-boolean.component.css']
})
export class SearchItemBooleanComponent extends AbstractSearchItem implements OnInit {

	@Input() groupname: string;

	ngOnInit() {
	}
}
