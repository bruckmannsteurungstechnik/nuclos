import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractSearchItem } from '../abstract-search-item';

@Component({
	selector: 'nuc-search-item-datechooser',
	templateUrl: './search-item-datechooser.component.html',
	styleUrls: ['./search-item-datechooser.component.css']
})
export class SearchItemDatechooserComponent extends AbstractSearchItem implements OnInit {

	ngOnInit() {
	}
}
