import { Component, ElementRef, Input, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import { AutoComplete } from 'primeng/primeng';
import {
	BoAttr,
	EntityAttrMeta,
	EntityMeta,
	LovEntry
} from '../../../entity-object-data/shared/bo-view.model';
import { EntityObjectSearchfilterService } from '../../../entity-object-data/shared/entity-object-searchfilter.service';
import { LovDataService } from '../../../entity-object-data/shared/lov-data.service';
import { LovSearchConfig } from '../../../entity-object-data/shared/lov-search-config';
import { MetaService } from '../../../entity-object-data/shared/meta.service';
import {
	Preference,
	SearchtemplatePreferenceContent
} from '../../../preferences/preferences.model';
import { FqnService } from '../../../shared/fqn.service';
import { NuclosDefaults } from '../../../shared/nuclos-defaults';
import { AbstractSearchItem } from '../abstract-search-item';

@Component({
	selector: 'nuc-search-item-lov',
	templateUrl: './search-item-lov.component.html',
	styleUrls: ['./search-item-lov.component.css']
})
export class SearchItemLovComponent extends AbstractSearchItem implements OnInit, OnDestroy {
	@Input() meta: EntityMeta | undefined;
	@Input() attributeMeta: EntityAttrMeta | undefined;
	@Input() suggestions: LovEntry[];
	@Input() attribute: any;

	@ViewChild('autoComplete') autoComplete: AutoComplete;

	private selectedSearchfilter: Preference<SearchtemplatePreferenceContent> | undefined;
	private unsubscribe$ = new Subject<void>();

	constructor(
		private fqnService: FqnService,
		private metaService: MetaService,
		private lovDataService: LovDataService,
		private eoSearchfilterService: EntityObjectSearchfilterService,
		private _elemRef: ElementRef
	) {
		super();
	}

	ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	ngOnInit() {
		this.modelChanged.pipe(takeUntil(this.unsubscribe$)).subscribe(() => this.filterSuggestations());

		this.eoSearchfilterService.observeSelectedSearchfilter().pipe(takeUntil(this.unsubscribe$)).subscribe(searchfilter => {
			this.selectedSearchfilter = searchfilter;
		});
	}

	loadSearchLovDropDownOptions(query, attribute: BoAttr): void {
		// it could be a dependent entity field, which is why we need the metadata directly
		// if (this.meta) {
		// 		let attrName = this.fqnService.getShortAttributeName(this.meta.getBoMetaId(), attribute.boAttrId);
		// 		this.metaService.getEntityMeta(this.meta.getBoMetaId()).subscribe(entityMeta => {
		// 			let attributeMeta = entityMeta.getAttributeMeta(attrName);
		if (this.attributeMeta) {
			let search: LovSearchConfig = {
				attributeMeta: this.attributeMeta,
				vlp: this.getWebVlp(this.attributeMeta),
				quickSearchInput: query,
				resultLimit: NuclosDefaults.DROPDOWN_LOAD_RESULT_LIMIT,
				searchmode: true
			};
			this.lovDataService.loadLovEntries(search).pipe(take(1)).subscribe(data => {
				if (data) {
					this.suggestions = data;
					this.filterSuggestations();
				}
			});
		}
		//      });
		// }
	}

	getWebVlp(attrMeta: EntityAttrMeta): WebValuelistProvider | undefined {
		if (attrMeta.isState() || attrMeta.isStateNumber()) {
			let statusVlp: WebValuelistProvider = {
				fieldname: '',
				idFieldname: '',
				name: 'status',
				value: 'status',
				type: 'named',
				parameter: []
			};
			if (this.meta) {
				let processId = this.getSearchValueFromSelectedFilter(
					this.meta.getEntityClassId() + '_nuclosProcess'
				);
				if (processId) {
					statusVlp.parameter.push({
						name: 'processId',
						value: processId
					});
				}
			}
			return statusVlp;
		}
		if (attrMeta.isProcess()) {
			let processVlp: WebValuelistProvider = {
				fieldname: '',
				idFieldname: '',
				name: 'process',
				value: 'process',
				type: 'named',
				parameter: []
			};
			return processVlp;
		}

		let vlpConfig = attrMeta.getVlpConfig();
		if (!vlpConfig) {
			return undefined;
		}
		let vlp: WebValuelistProvider = {
			fieldname: vlpConfig.valueField,
			idFieldname: vlpConfig.idField === undefined ? '' : vlpConfig.idField,
			name: vlpConfig.vlpId,
			value: vlpConfig.vlpId,
			type: 'ds',
			parameter: []
		};
		// this should be set automatically :-(
		vlp.parameter.push({
			name: 'id-fieldname',
			value: vlp.idFieldname
		});
		vlp.parameter.push({
			name: 'fieldname',
			value: vlp.fieldname
		});
		let vlpContext = this.getSuitableVlpContext(vlpConfig);
		if (vlpContext) {
			let vlpParams = vlpContext.vlpParams;
			if (vlpParams) {
				vlpParams.forEach(p => {
					let valueFromSearchfilter = false;
					if (p.valueFromEntityFieldId !== undefined) {
						let searchValueFromSelectedFilter = this.getSearchValueFromSelectedFilter(
							p.valueFromEntityFieldId
						);
						if (searchValueFromSelectedFilter) {
							vlp.parameter.push({
								name: p.name,
								value: searchValueFromSelectedFilter
							});
							valueFromSearchfilter = true;
						}
					}
					if (!valueFromSearchfilter && p.value !== undefined) {
						vlp.parameter.push({
							name: p.name,
							value: p.value
						});
					}
				});
			}
		}
		return vlp;
	}

	getSuitableVlpContext(vlpConfig: EntityFieldVlpConfig): EntityFieldVlpContext | undefined {
		let result: EntityFieldVlpContext | undefined;
		if (vlpConfig.vlpContexts) {
			// look for a exact context match
			if (this.meta) {
				let entityClassId = this.meta.getEntityClassId();
				result = vlpConfig.vlpContexts.find(
					c => c.entityClassId === entityClassId && c.search
				);
			}
			// look for the generic context
			if (!result) {
				result = vlpConfig.vlpContexts.find(c => c.entityClassId === undefined && c.search);
			}
		}
		return result;
	}

	getSearchValueFromSelectedFilter(valueFromEntityFieldId: string): string | undefined {
		if (this.selectedSearchfilter) {
			let attribute = this.selectedSearchfilter.content.columns.find(
				c => c.boAttrId === valueFromEntityFieldId
			);
			if (attribute) {
				if (attribute.values && attribute.values.length === 1) {
					return attribute.values[0].id;
				}
				if (attribute.value && attribute.value.id) {
					return attribute.value.id;
				}
				return attribute.value;
			}
		}
		return undefined;
	}

	/**
	 * prevent navigation in sidebar component
	 */
	keydown($event: KeyboardEvent) {
		$event.stopPropagation();
	}

	openLovPanel($event) {
		if (!this.isLovPanelVisible()) {
			this.showResultPanel();
		}
	}

	isLovPanelVisible(): boolean {
		if (this.autoComplete) {
			return this.autoComplete.panelVisible;
		}
		return false;
	}

	private filterSuggestations() {
		if (this.suggestions && this.model) {
			this.suggestions = this.suggestions.filter(
				suggestionEntry =>
					suggestionEntry.id != null &&
					!this.model.find(selectedEntry => suggestionEntry.id === selectedEntry.id)
			);
		}
	}

	private showResultPanel() {
		this.autoComplete.show();
		this.setupPanel();
	}

	private setupPanel() {
		let element = this.autoComplete.panelEL.nativeElement;
		element.setAttribute('for-id', this._elemRef.nativeElement.id);
	}
}
