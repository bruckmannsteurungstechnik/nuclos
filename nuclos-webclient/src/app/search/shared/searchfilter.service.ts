import { Injectable } from '@angular/core';
import { of as observableOf } from 'rxjs';
import { EntityMeta, InputType } from '../../entity-object-data/shared/bo-view.model';
import { SelectableService } from '../../entity-object-data/shared/selectable.service';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import {
	Preference,
	SearchtemplateAttribute,
	SearchtemplatePreferenceContent
} from '../../preferences/preferences.model';
import { PreferencesService } from '../../preferences/preferences.service';
import { DatetimeService } from '../../shared/datetime.service';
import { Operator } from '../operator';
import { OperatorDefinitions } from '../operator-definitions';
import { SearchService } from './search.service';

@Injectable()
export class SearchfilterService {
	static getInputType(attribute: SearchtemplateAttribute): string {
		if (attribute.reference) {
			return InputType.REFERENCE;
		} else if (attribute.type === 'Decimal' || attribute.type === 'Integer') {
			return InputType.NUMBER;
		} else if (attribute.type === 'String') {
			return InputType.STRING;
		} else if (attribute.type === 'Date' || attribute.type === 'Timestamp') {
			return InputType.DATE;
		} else if (attribute.type === 'Boolean') {
			return InputType.BOOLEAN;
		}
		return InputType.STRING;
	}

	private static urlQueryCounter = 0;

	/**
	 * might be set from addon to filter result via addon
	 */
	private additionalWhereConditions: Map<
		string /* entityClassId */,
		Map<string /* appIdentifier */, string | undefined>
	> = new Map();

	private operatorDefinitions: OperatorDefinitions;

	constructor(
		private datetimeService: DatetimeService,
		// TODO: We should not need 2 different services here for handling searchfilter preferences
		private selectableService: SelectableService,
		private preferenceService: PreferencesService,
		private nuclosI18n: NuclosI18nService,
		// TODO: Temporarily injected here to resolve merge conflicts - remove later
		private searchService: SearchService
	) {
		this.operatorDefinitions = {
			NUMBER: [
				{
					operator: '=',
					isUnary: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.number.equal')
				},
				{
					operator: '!=',
					isUnary: false,
					label: this.nuclosI18n.getI18n(
						'webclient.searchtemplate.operator.number.notequal'
					)
				},
				{
					operator: '>',
					isUnary: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.number.gt')
				},
				{
					operator: '<',
					isUnary: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.number.lt')
				},
				{
					operator: '>=',
					isUnary: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.number.gte')
				},
				{
					operator: '<=',
					isUnary: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.number.lte')
				},
				{
					operator: 'is null',
					isUnary: true,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.null')
				},
				{
					operator: 'is not null',
					isUnary: true,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.notnull')
				}
			],
			BOOLEAN: [
				{
					operator: '=',
					isUnary: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.number.equal')
				}
			],
			STRING: [
				{
					operator: '=',
					isUnary: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.string.equal')
				},
				{
					operator: 'like',
					isUnary: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.string.like')
				},
				{
					operator: 'is null',
					isUnary: true,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.null')
				},
				{
					operator: 'is not null',
					isUnary: true,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.notnull')
				}
			],
			REFERENCE: [
				{
					operator: '=',
					isUnary: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.string.equal')
				},
				{
					operator: 'like',
					isUnary: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.string.like')
				},
				{
					operator: 'is null',
					isUnary: true,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.null')
				},
				{
					operator: 'is not null',
					isUnary: true,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.notnull')
				}
			]
		};
		this.operatorDefinitions[InputType.DATE] = this.operatorDefinitions[InputType.NUMBER];
	}

	getOperatorDefinitions(): OperatorDefinitions {
		return this.operatorDefinitions;
	}

	getOperatorDefinition(attribute: SearchtemplateAttribute): Operator | undefined {
		let type = attribute.inputType;
		let operator = attribute.operator;

		if (type === undefined) {
			return;
		}
		return this.operatorDefinitions[type].filter(op => op.operator === operator).shift();
	}

	createTempSearchfilter(entityClassId: string): Preference<SearchtemplatePreferenceContent> {
		let tempFilter = this.createSearchfilterWithoutAttributes(entityClassId);
		tempFilter.name = this.nuclosI18n.getI18n('webclient.searcheditor.searchfilter.temp.name');
		tempFilter.content.isTemp = true;
		tempFilter.selected = true;
		return tempFilter;
	}

	createSearchfilterWithoutAttributes(
		entityClassId: string
	): Preference<SearchtemplatePreferenceContent> {
		let searchtemplatePreference: Preference<SearchtemplatePreferenceContent> = {
			type: 'searchtemplate',
			boMetaId: entityClassId,
			selected: false,
			content: {
				columns: [],
				isValid: false,
				disabled: false
			}
		};
		return searchtemplatePreference;
	}

	createSearchfilter(metaData: EntityMeta): Preference<SearchtemplatePreferenceContent> {
		let searchtemplatePreference: Preference<SearchtemplatePreferenceContent> = {
			type: 'searchtemplate',
			boMetaId: metaData.getBoMetaId(),
			selected: false,
			content: {
				columns: [],
				isValid: false,
				disabled: false
			}
		};

		// load all attributes from meta data
		metaData.getAttributes().forEach(attributeMeta => {
			if (
				!attributeMeta.isStateIcon() &&
				!attributeMeta.isDeletedFlag() &&
				!attributeMeta.isHidden() &&
				!attributeMeta.isStateIcon() &&
				!attributeMeta.isImage()
			) {
				searchtemplatePreference.content.columns.push({
					name: attributeMeta.getName(),
					boAttrId: attributeMeta.getAttributeID(),
					system: attributeMeta.isSystemAttribute(),
					selected: false
				});
			}
		});

		return searchtemplatePreference;
	}

	createFromUrlSearch(
		meta: EntityMeta,
		searchString: string
	): Preference<SearchtemplatePreferenceContent> {
		let searchtemplatePreference = this.createSearchfilter(meta);

		let sp = searchString.split(',');
		let searchColumns: Array<SearchtemplateAttribute> = [];
		for (let searchParameter of sp) {
			let paramName = searchParameter.split('=')[0];
			let paramValue = searchParameter.split('=')[1];
			let attrFqn = meta.getBoMetaId() + '_' + paramName;
			if (paramName !== undefined && paramValue !== undefined) {
				let attributes = searchtemplatePreference.content.columns.filter(attr => {
					return attr.boAttrId === attrFqn;
				});

				if (attributes !== undefined && attributes.length !== 0) {
					let attrMeta = <SearchtemplateAttribute>meta.getAttribute(paramName);

					let attribute = attributes[0];
					searchColumns.push(attribute);
					if (attribute.inputType === 'number') {
						attribute.value = Number(paramValue);
					} else {
						attribute.value = paramValue;
					}

					// Do a LIKE search if the search value starts or ends with a wildcard charater (* or ?)
					if (/(^[*?])|([*?]$)/.test(attribute.value)) {
						attribute.operator = 'like';
					} else {
						attribute.operator = '=';
					}
					let inputType = SearchfilterService.getInputType(attrMeta);
					let operatorDefsForInputType = this.operatorDefinitions[inputType];
					if (operatorDefsForInputType.length !== 0) {
						let operatorDefs = operatorDefsForInputType.filter(
							elem => elem.operator === attribute.operator
						);
						let operatorDef;
						if (operatorDefs.length !== 0) {
							operatorDef = operatorDefs[0];
						} else {
							operatorDef = operatorDefsForInputType[0];
						}
						attribute.operator = operatorDef.operator;
						this.formatValue(attribute, operatorDef);
					}

					attribute.selected = true;
					attribute.enableSearch = true;
					attribute.isValid = true;
				} else {
					alert('Wrong attribute names: ' + paramName);
					// TODO use errorhandler
				}
			}
		}
		searchtemplatePreference.content.columns = searchColumns;
		searchtemplatePreference.content.isValid = true;
		searchtemplatePreference.content.isTemp = true;
		searchtemplatePreference.selected = true;
		searchtemplatePreference.name = '[URL:' + ++SearchfilterService.urlQueryCounter + ']';

		return searchtemplatePreference;
	}

	formatValue(attribute: SearchtemplateAttribute, operatorDef: Operator) {
		if (attribute.values) {
			attribute.formattedValue = '(';
			attribute.values.forEach((value, index) => {
				if (index > 0) {
					attribute.formattedValue += ', ';
				}
				attribute.formattedValue += this.valueToString(attribute, operatorDef, value);
			});
			attribute.formattedValue += ')';
		} else {
			attribute.formattedValue = this.valueToString(attribute, operatorDef, attribute.value);
		}
	}

	valueToString(attribute: SearchtemplateAttribute, operatorDef: Operator, value: any) {
		let result = value;

		if (attribute.inputType === InputType.DATE) {
			result = this.datetimeService.formatDate(attribute.value);
		} else if (attribute.inputType === InputType.REFERENCE) {
			result = value && value.name ? value.name : value;
		}

		if (
			(attribute.inputType === InputType.STRING ||
				attribute.inputType === InputType.REFERENCE) &&
			(operatorDef && !operatorDef.isUnary)
		) {
			result = '\'' + result + '\'';
		}

		return result;
	}

	saveSearchfilter(searchfilter: Preference<SearchtemplatePreferenceContent>) {
		return this.selectableService.savePreference(searchfilter);
	}

	saveAndSelectSearchfilter(searchfilter: Preference<SearchtemplatePreferenceContent>) {
		searchfilter.selected = true;
		// updating the preference does not set customization flag :(
		// we do it manually in advance
		if (searchfilter.shared === true) {
			searchfilter.customized = true;
		}
		return this.selectableService.savePreference(searchfilter);
	}

	deleteSearchfilter(searchfilter: Preference<SearchtemplatePreferenceContent>) {
		if (!searchfilter.prefId) {
			return observableOf(true);
		}

		return this.preferenceService.deletePreferenceItem(searchfilter);
	}

	resetCustomizedSearchfilter(searchfilter: Preference<SearchtemplatePreferenceContent>) {
		return this.preferenceService.resetCustomizedPreferenceItem(searchfilter);
	}

	getSearchfilter(prefId: string) {
		return this.preferenceService.getPreference(prefId);
	}

	getSearchfiltersForEntity(entityClassId: string | undefined) {
		return this.preferenceService.getPreferences({
			boMetaId: entityClassId,
			type: ['searchtemplate']
		});
	}

	applyAdditionalResultlistWhereCondition(
		condition: string | undefined,
		entityClassId: string,
		appIdentifier: string
	) {
		let additionWehereConditionsForEntity = this.additionalWhereConditions.get(entityClassId);
		if (!additionWehereConditionsForEntity) {
			additionWehereConditionsForEntity = new Map();
			this.additionalWhereConditions.set(entityClassId, additionWehereConditionsForEntity);
		}
		additionWehereConditionsForEntity.set(appIdentifier, condition);
		this.initateDataLoad();
	}

	getAdditionalWhereConditions(entityClassId: string): string[] {
		let additionWehereConditionsForEntity =
			this.additionalWhereConditions.get(entityClassId) || new Map();
		return Array.from(additionWehereConditionsForEntity.values()).filter(
			c => c !== undefined
		) as string[];
	}

	initateDataLoad(): void {
		this.searchService.initiateDataLoad();
	}

	select(searchfilter: Preference<SearchtemplatePreferenceContent>) {
		if (searchfilter && searchfilter.prefId) {
			this.preferenceService
				.selectPreference(searchfilter)
				.subscribe(() => (searchfilter.selected = true));
		}
	}

	deselect(searchfilter: Preference<SearchtemplatePreferenceContent> | undefined) {
		if (searchfilter) {
			this.preferenceService.deselectPreference(searchfilter).subscribe();
		}
	}
}
