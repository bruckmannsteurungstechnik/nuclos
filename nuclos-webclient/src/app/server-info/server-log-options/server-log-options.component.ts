import {
	ChangeDetectionStrategy,
	ChangeDetectorRef,
	Component,
	EventEmitter,
	Input,
	OnInit,
	OnDestroy,
	Output
} from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Logger } from '../../log/shared/logger';
import { ServerLogLevel } from '../shared/server-log-level';
import { ServerLogService, SqlLoggingMode } from '../shared/server-log.service';

@Component({
	selector: 'nuc-server-log-options',
	templateUrl: './server-log-options.component.html',
	styleUrls: ['./server-log-options.component.css'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ServerLogOptionsComponent implements OnInit, OnDestroy {
	@Input() autoScroll = true;
	@Output() autoScrollChange = new EventEmitter<boolean>();

	@Input() logLevel: string = ServerLogLevel[ServerLogLevel.INFO];
	@Output() logLevelChange = new EventEmitter<string>();

	@Input() sqlLoggingMode;
	@Output() sqlLoggingModeChange = new EventEmitter<any>();

	@Input() textFilter = '';
	@Output() textFilterChange = new EventEmitter<string>();

	@Input() debugSqlInput = '';
	@Output() debugSqlInputChange = new EventEmitter<string>();

	@Input() minExecTime = '';
	@Output() minExecTimeChange = new EventEmitter<string>();

	logLevels: string[] = Object.keys(ServerLogLevel).filter(key => isNaN(Number(key)));

	sqlLoggingOptions = [
		{ name: '', value: SqlLoggingMode.Disabled },
		{ name: 'SQL-Logging', value: SqlLoggingMode.SQLLogger },
		{ name: 'SQL-Timer-Logging', value: SqlLoggingMode.SQLTimerLogger },
		{ name: 'SQL-UpdateInsertDelete-Logging', value: SqlLoggingMode.SQLUpdateLogger }
	];

	private unsubscribe$ = new Subject<void>();

	constructor(
		private $log: Logger,
		private ref: ChangeDetectorRef,
		private serverLogService: ServerLogService
	) {}

	ngOnInit() {
		this.serverLogService.getSqlLoggingMode().pipe(takeUntil(this.unsubscribe$)).subscribe(sqlLoggingMode => {
			this.sqlLoggingMode = sqlLoggingMode;
			this.$log.debug('sql logging mode = %o', sqlLoggingMode);
			this.ref.markForCheck();
		});
		this.serverLogService.getDebugSql().pipe(takeUntil(this.unsubscribe$)).subscribe(debugSql => {
			this.debugSqlInput = debugSql.debugSQL;
			this.minExecTime = debugSql.minExecTime !== null ? '' + debugSql.minExecTime : '';
			this.ref.markForCheck();
			this.$log.debug('debugSql = %o', debugSql);
		});
	}

	ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	setAutoscroll(autoScroll: boolean) {
		this.autoScroll = autoScroll;
		this.autoScrollChange.emit(autoScroll);
	}

	setLogLevel(logLevel: string) {
		this.logLevel = logLevel;
		this.logLevelChange.emit(logLevel);
	}

	setSqlLoggingMode(sqlLoggingMode) {
		this.sqlLoggingMode = sqlLoggingMode;
		this.sqlLoggingModeChange.emit(sqlLoggingMode);
	}

	setTextFilter(textFilter: string) {
		textFilter = textFilter.toLocaleLowerCase();
		this.$log.debug(textFilter);
		this.textFilter = textFilter;
		this.textFilterChange.emit(textFilter);
	}

	selectSqlLoggingMode(sqlLoggingMode: SqlLoggingMode) {
		this.serverLogService.setLoggingMode(sqlLoggingMode);
	}

	setDebugSqlInput(setDebugSqlInput: string) {
		this.$log.debug(setDebugSqlInput);
		this.debugSqlInput = setDebugSqlInput;
		this.debugSqlInputChange.emit(setDebugSqlInput);
		this.setDebugSqlString(this.debugSqlInput, this.minExecTime);
	}

	setMinExecTime(minExecTime: string) {
		this.$log.debug(minExecTime);
		this.minExecTime = minExecTime;
		this.minExecTimeChange.emit(minExecTime);
		this.setDebugSqlString(this.debugSqlInput, this.minExecTime);
	}

	private setDebugSqlString(debugSqlString: string, minExecTime: string) {
		this.serverLogService.setDebugSqlString(debugSqlString, minExecTime);
	}
}
