import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Logger } from '../../log/shared/logger';
import { ServerLogLevel } from '../shared/server-log-level';
import { ServerLogMessage } from '../shared/server-log-message';
import { ServerLogService } from '../shared/server-log.service';

/**
 * This component displays a live stream of server log messages.
 */
@Component({
	changeDetection: ChangeDetectionStrategy.OnPush,
	selector: 'nuc-server-log-viewer',
	templateUrl: './server-log-viewer.component.html',
	styleUrls: ['./server-log-viewer.component.css']
})
export class ServerLogViewerComponent implements OnInit, OnDestroy {
	/**
	 * Holds all current messages.
	 */
	messages: Array<ServerLogMessage>;

	/**
	 * Maximum number of displayed messages.
	 * After the limit is reached, old messages are deleted.
	 * @type {number}
	 */
	messageLimit = 10000;

	/**
	 * (De-)actives auto-scrolling of the output.
	 * @type {boolean}
	 */
	autoScroll = true;

	/**
	 * Only messages with the selected log level are displayed.
	 * All other messages are hidden, but not deleted.
	 * @type {any}
	 */
	logLevel: string = ServerLogLevel[ServerLogLevel.INFO];

	/**
	 * Only messages which contain the textFilter text are displayed.
	 * All other messages are hidden, but not deleted.
	 * @type {any}
	 */
	textFilter = '';

	/**
	 * Counts the received messages.
	 * @type {number}
	 */
	private counter = 0;

	/**
	 * The HTML element which contains the output and needs to be scrolled.
	 */
	private outputWrapper;

	private unsubscribe$ = new Subject<void>();

	constructor(
		private $log: Logger,
		private serverLogService: ServerLogService,
		private ref: ChangeDetectorRef
	) {
		this.messages = [];
	}

	ngOnInit() {
		this.outputWrapper = $('.output-wrapper');

		this.serverLogService.getServerLogBuffered().pipe(takeUntil(this.unsubscribe$)).subscribe(
			(messages: Array<ServerLogMessage>) => {
				this.showLog(messages);
			},
			error => {
				this.$log.error(error);
			}
		);
	}

	ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	/**
	 * Scrolls to the last log message, if auto-scroll is enabled.
	 *
	 * @param _element The element that caused the event, i.e. the last log message element
	 */
	scrollOutput(_element: any): void {
		if (this.autoScroll) {
			let height = this.outputWrapper.prop('scrollHeight');
			this.outputWrapper.scrollTop(height);
			this.$log.debug(height);
		}
	}

	/**
	 * The download URL for the complete server.log file.
	 *
	 * @returns {string}
	 */
	getDownloadLink() {
		return this.serverLogService.getDownloadLink();
	}

	/**
	 * Appends the log message to the output element.
	 * Possibly trims the messages according to the message limit.
	 *
	 * @param log
	 */
	private showLog(messages: Array<ServerLogMessage>) {
		if (messages.length === 0) {
			return;
		}

		for (let message of messages) {
			this.prepareMessage(message);
		}

		this.messages = this.messages.concat(messages);
		if (this.messages.length > this.messageLimit) {
			this.messages = this.messages.splice(this.messages.length - this.messageLimit);
		}

		this.ref.markForCheck();
	}

	/**
	 * Counts and formats the given message.
	 *
	 * @param message
	 */
	private prepareMessage(message: ServerLogMessage) {
		this.counter++;
		message.counter = this.counter;

		// TODO: Use a pipe instead
		message.formattedDate = moment(message.date).format('DD.MM.YYYY HH:mm:ss');
		message.message = message.message.replace(/\n/g, '<br/>');
		message.message = message.message.replace(/\t/g, '&nbsp;&nbsp;&nbsp;&nbsp;');
	}
}
