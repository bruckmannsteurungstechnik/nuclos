import { Injectable } from '@angular/core';

@Injectable()
export class BrowserDetectionService {

	constructor() {
	}

	isSafari() {
		return /constructor/i.test(window['HTMLElement']) || (function (p) {
			return p.toString() === '[object SafariRemoteNotification]';
		})(!window['safari'] || window['safari'].pushNotification);
	}

	isFirefox() {
		// @ts-ignore
		return typeof InstallTrigger !== 'undefined';
	}

	/* needs to be tested
	// Opera 8.0+
	isOpera() {
		return (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0
	};

	// Internet Explorer 6-11
	isIE() {
		return !!document.documentMode;
	}

	// Edge 20+
	isEdge() {
		return !isIE() && !!window.StyleMedia;
	}

	// Chrome 1+

	isChrome() {
		return !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);
	}

	// Blink engine detection
	isBlink() {
		return (isChrome() || isOpera()) && !!window.CSS;
	}*/

}
