import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { ObservableUtils } from './observable-utils';

/**
 * TODO: This locks the whole site!
 * Only the components should be locked, which are really loading.
 * This is not easily achievable with ngx-loading, maybe we should use another component.
 */
@Injectable()
export class BusyService {
	private busy$: BehaviorSubject<boolean> = new BehaviorSubject(false);

	private observables: Observable<any>[] = [];

	constructor() {}

	/**
	 * Locks the site as busy, when the given Observable is subscribed to.
	 * Unlocks the site after all "busy" Observables are finalized.
	 *
	 * TODO: Maybe this could be implemented as an RxJS 6 pipe operator?
	 */
	busy<T>(observable: Observable<T>): Observable<T> {
		return ObservableUtils.onSubscribe(observable, () => {
			this.observables.push(observable);
			this.busyStart();
		}).pipe(
			finalize(() => {
				this.observables.splice(this.observables.indexOf(observable));
				if (this.observables.length === 0) {
					this.busyEnd();
				}
			})
		);
	}

	isBusy(): BehaviorSubject<boolean> {
		return this.busy$;
	}

	private busyStart(): void {
		this.busy$.next(true);
	}

	private busyEnd(): void {
		this.busy$.next(false);
	}
}
