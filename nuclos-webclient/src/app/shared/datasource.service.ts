import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DatasourceParams } from '@nuclos/nuclos-addon-api';
import { Observable } from 'rxjs';
import { NuclosConfigService } from './nuclos-config.service';
import { NuclosHttpService } from './nuclos-http.service';

@Injectable()
export class DatasourceService {

	constructor(private http: NuclosHttpService, private nuclosConfig: NuclosConfigService) {
	}

	executeDatasource(
		datasourceId: string,
		datasourceParams: DatasourceParams,
		maxRowCount?: number
	): Observable<object[]> {
		return this.http.get<object[]>(
			this.nuclosConfig.getRestHost() + '/data/datasource/' + datasourceId + (maxRowCount ? '/' + maxRowCount : ''),
			{
				params: datasourceParams as HttpParams
			}
		);
	}
}
