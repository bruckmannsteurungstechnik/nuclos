/* tslint:disable */

interface CollectiveProcessingAction extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	links: CollectiveProcessingActionLinks;
	name: string;
	subName?: string;
	description?: string;
	color?: string;
}

interface CollectiveProcessingActionLinks extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	execute: RestLink;
}

interface CollectiveProcessingExecution extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	links: CollectiveProcessingExecutionLinks;
}

interface CollectiveProcessingExecutionLinks extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	progress: RestLink;
	abort: RestLink;
}

interface CollectiveProcessingObjectInfo extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	id: ObjectId;
	infoRowNumber: number;
	name: string;
	inProgress: boolean;
	result?: string;
	resultMessage?: string;
	boMetaId?: string;
}

interface CollectiveProcessingProgressInfo extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	objectInfos?: CollectiveProcessingObjectInfo[];
	percent: number;
}

interface CollectiveProcessingSelectionOptions extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	stateChanges?: CollectiveProcessingAction[];
}

interface DebugSql extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	debugSQL?: string;
	minExecTime?: string;
}

interface EntityMetaBase extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	links: EntityMetaBaseLinks;
	boMetaId: string;
	name: string;
	createNew: boolean;
}

interface EntityMetaBaseLinks extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	boMeta: RestLink;
	bos?: RestLink;
	resourceicon?: RestLink;
}

interface EntityMetaOverview extends EntityMetaBase, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	processMetaId?: string;
}

interface EntityObject extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	links: EntityObjectLinks;
	attributes: EntityObjectAttributes;
	attrRestrictions: any;
	attrImages: any;
	boId?: string;
	boMetaId?: string;
	canWrite?: boolean;
	canDelete?: boolean;
	version?: number;
}

interface EntityObjectAttributes extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	primaryKey?: string;
	createdBy?: string;
	createdAt?: string;
	changedBy?: string;
	changedAt?: string;
}

interface EntityObjectLinks extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	self: RestLink;
	boMeta: RestLink;
	clone: RestLink;
	printouts: RestLink;
	stateIcon: RestLink;
}

interface EntityObjectResultList extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	bos: EntityObject[];
	all?: boolean;
	total?: number;
	canCreateBo?: boolean;
}

interface EvaluatedTitleExpression extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	evaluated_expression?: string;
}

interface GlobalSearchResult extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	pk?: string;
	uid?: string;
	text?: string;
}

interface InputRequired extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	result: any;
}

interface InputRequiredContext extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	inputrequired: InputRequired;
}

interface JobStatus extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	status?: string;
}

interface LayoutLinks extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	layout: RestLink;
}

interface LegalDisclaimer extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	name: string;
	text: string;
}

interface LocaleInfo extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	locale: string;
}

interface LogLevel extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	level?: string;
}

interface Logger extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	name?: string;
	level?: string;
	href?: string;
}

interface Loggers extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	logger: Logger[];
}

interface LoginInfo extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	allowedActions: AllowedActions;
	links: LoginInfoLinks;
	mandator?: Mandator;
	mandators?: Mandator[];
	sessionId: string;
	username: string;
	locale: string;
	maintenanceMode: boolean;
	superUser: boolean;
	clientIp?: string;
	initialEntity?: string;
}

interface AllowedActions extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	SharePreferences?: boolean;
	ConfigureCharts?: boolean;
	ConfigurePerspectives?: boolean;
	PrintSearchResultList?: boolean;
	WorkspaceCustomizeEntityAndSubFormColumn?: boolean;
	collectiveProcessing?: boolean;
}

interface LoginInfoLinks extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	boMetas: RestLink;
	menu: RestLink;
	tasks: RestLink;
	search: RestLink;
}

interface LoginParams extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	username: string;
	password?: string;
	datalanguage?: string;
	locale?: string;
	autologin?: boolean;
}

interface LovEntry extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	name?: string;
	id?: number;
}

interface Mandator extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	links: MandatorLinks;
	mandatorId: string;
	mandatorLevelId: string;
	name: string;
	path: string;
	color?: string;
}

interface MandatorLinks extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	chooseMandator: RestLink;
}

interface MatrixData extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	xAxisBoList: MatrixXAxisObject[];
	yAxisBoList: MatrixYAxisObject[];
	entityMatrixBoDict: any[];
	xrefData: any[];
	cellInputType?: string;
	editable?: boolean;
}

interface MatrixRequestParameters extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	entityMatrix?: string;
	entityMatrixValueType?: string;
	entityMatrixReferenceField?: string;
	entityX?: string;
	entityY?: string;
	entityFieldMatrixParent?: string;
	entityFieldMatrixXRefField?: string;
	cellInputType?: string;
	editable?: string;
	boId?: string;
}

interface MatrixXAxisObject extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	boId?: number;
	_title?: string;
}

interface MatrixYAxisObject extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	boId?: number;
	evaluatedHeaderTitle?: string;
}

interface MenuEntry extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	entries: EntityMetaBase[];
	path?: string;
}

interface MultiSelectionResult extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	selectedIds?: ObjectId[];
	unselectedIds?: ObjectId[];
	allSelected: boolean;
}

interface News extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	id: string;
	name: string;
	title: string;
	content: string;
	revision: number;
	active: boolean;
	confirmationRequired: boolean;
	showAtStartup: boolean;
	validFrom?: XMLGregorianCalendar;
	validUntil?: XMLGregorianCalendar;
	privacyPolicy: boolean;
}

interface ObjectFactory {
}

interface ObjectId extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	long?: number;
	string?: string;
}

interface PrintoutList extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	outputFormats: PrintoutOutputFormat[];
	name?: string;
	printoutId?: string;
}

interface PrintoutOutputFormat extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	name?: string;
	outputFormatId?: string;
}

interface ProcessLayoutLinks extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	links: LayoutLinks;
}

interface QueryContext extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	offset?: number;
	chunkSize?: number;
	countTotal?: boolean;
	search?: string;
	searchFilterId?: string;
	vlpId?: string;
	where?: string;
	attributes?: string;
	idOnlySelection?: boolean;
	orderBy?: string;
	skipStatesAndGenerations?: boolean;
	withTitleAndInfo?: boolean;
	withMetaLink?: boolean;
	withLayoutLink?: boolean;
	withDetailLink?: boolean;
}

interface RestLink extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	methods: string[];
	href?: string;
}

interface RestPreference extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	content: any;
	prefId: string;
	type: string;
	shared: boolean;
	customized: boolean;
	app?: string;
	boMetaId?: string;
	layoutId?: string;
	name?: string;
	menuRelevant?: string;
	selected?: string;
	nucletId?: string;
}

interface RestPreferenceShare extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	userRoles: RestPreferenceUserRole[];
}

interface RestPreferenceUserRole extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	userRoleId?: string;
	name?: string;
	shared?: boolean;
}

interface RestSystemparameters extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	ANONYMOUS_USER_ACCESS_ENABLED?: boolean;
	ENVIRONMENT_DEVELOPMENT?: boolean;
	FORGOT_LOGIN_DETAILS_ENABLED?: boolean;
	FULLTEXT_SEARCH_ENABLED?: boolean;
	FUNCTION_BLOCK_DEV?: boolean;
	QUICKSEARCH_DELAY_TIME?: number;
	USER_REGISTRATION_ENABLED?: boolean;
	WEBCLIENT_CSS?: string;
}

interface SearchfilterEntityMeta extends EntityMetaOverview, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	searchfilter?: string;
}

interface SearchfilterInfo extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	pk?: string;
	filterName?: string;
	default?: boolean;
	order?: number;
	icon?: string;
}

interface ServerStatus extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	ready: boolean;
}

interface SideviewMenuSelector extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	links: SideviewMenuSelectorLinks;
	type?: string;
	label?: string;
	boMetaId?: string;
	boId?: string;
}

interface SideviewMenuSelectorLinks extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	tree: RestLink;
}

interface StatusInfo extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	statename?: string;
	numeral?: string;
	description?: string;
	color?: string;
	buttonIcon?: string;
}

interface TaskEntityMeta extends SearchfilterEntityMeta, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	dynamicEntityFieldName?: string;
	taskMetaId?: string;
	taskEntity?: string;
}

interface TaskMenuEntry extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	entries: TaskEntityMeta[];
	path?: string;
}

interface TreeNode extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	boMetaId?: string;
	boId?: number;
	node_id?: string;
	title?: string;
	icon?: string;
}

interface Serializable {
}

interface Cloneable {
}

interface CopyTo {
}

interface Equals {
}

interface HashCode {
}

interface ToString {
}

interface XMLGregorianCalendar extends Cloneable {
}
