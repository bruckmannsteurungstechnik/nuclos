import { Injectable } from '@angular/core';
import { BoAttr, EntityMeta } from '../entity-object-data/shared/bo-view.model';
import { StringUtils } from './string-utils';

export interface FqnFormatter {
	(fqn: string);
}

@Injectable()
export class FqnService {
	// TODO: Refactor static calls and use NuclosCacheService instead of this Map.
	static fqnCache = new Map<string, string | undefined>();

	static toFqn(entityMeta: EntityMeta, attributeName: string): string {
		return entityMeta.getBoMetaId() + '_' + attributeName;
	}

	static getShortAttributeNameFailsafe(eoMetaId: string, attributeFqn: string): string {
		let cacheKey = eoMetaId + '|' + attributeFqn;
		let result = attributeFqn;

		if (!this.fqnCache.has(cacheKey)) {
			try {
				result = FqnService.getShortAttributeName(eoMetaId, attributeFqn);
			} catch (e) {}

			result = StringUtils.replaceAll(result, '_', '');

			this.fqnCache.set(cacheKey, result);
		}

		result = this.fqnCache.get(cacheKey) || '';

		return result;
	}

	/**
	 * convert attribute FQN to short name (e.g.: example_rest_Order_orderNumber -> orderNumber)
	 */
	static getShortAttributeName(eoMetaId: string, attributeFqn: string): string {
		let result;

		let separator = '_';
		if (eoMetaId) {
			let prefix = eoMetaId + separator;
			if (attributeFqn.lastIndexOf(prefix) === 0) {
				result = attributeFqn.substring(prefix.length);
			}
		} else {
			let index = attributeFqn.lastIndexOf(separator);
			result = attributeFqn.substring(index + 1);
		}

		if (!result) {
			// TODO: Should we really throw an Error here?
			throw Error('Unable to get short attribute name for ' + eoMetaId + ', ' + attributeFqn);
		}

		return result;
	}

	/**
	 * Extracts the entity part of an attribute FQN.
	 */
	static getEntityClassId(attributeFqn: string) {
		let entityClassParts: string[] = [];

		let parts = attributeFqn.split('_');
		for (let part of parts) {
			entityClassParts.push(part);
			if (part.match(/^[A-Z]/)) {
				break;
			}
		}

		return entityClassParts.join('_');
	}

	private static FQN_REGEXP: RegExp = /\${([^\}]+)}/g;

	constructor() {}

	/**
	 * creates an array of FQN's used in inputString, like:
	 * "${com_example_Test_attribute1} ${com_example_Test_attribute2}"
	 * ->
	 * ['com_example_Test_attribute1', 'com_example_Test_attribute1']
	 */
	parseFqns(inputString: string | undefined): string[] {
		let result: string[] = [];

		if (!inputString) {
			return result;
		}

		let match;
		do {
			match = FqnService.FQN_REGEXP.exec(inputString);
			if (match !== undefined && match !== null) {
				let fqn = match[1];
				result.push(fqn);
			}
		} while (match !== undefined && match !== null);
		return result;
	}

	/**
	 * @param inputString containing attribute patterns like '${orderNumber} - ${orderDate}
	 * @param formatter formats each fqn token
	 */
	formatFqnString(inputString: string | undefined, formatter: FqnFormatter): string {
		if (inputString === undefined) {
			return '';
		}

		let result = inputString;

		let match: RegExpExecArray | null;
		do {
			match = FqnService.FQN_REGEXP.exec(inputString);
			if (match !== undefined && match !== null) {
				let token = match[0];
				let fqn = match[1];
				result = result.replace(token, formatter(fqn));
			}
		} while (match !== undefined && match !== null);
		return result;
	}

	/**
	 * Searches by the given attribute FQN for the corresponding attribute in the given Meta.
	 */
	getAttributeByFqn(meta: EntityMeta, attributeFqn: string): BoAttr {
		let attributeName = attributeFqn.replace(meta.getBoMetaId(), '').replace('_', '');
		return meta.getAttribute(attributeName);
	}

	/**
	 * convert attribute FQN to short name (e.g.: example_rest_Order_orderNumber -> orderNumber)
	 */
	getShortAttributeName(eoMetaId: string, attributeFqn: string): string {
		return FqnService.getShortAttributeName(eoMetaId, attributeFqn);
	}
}
