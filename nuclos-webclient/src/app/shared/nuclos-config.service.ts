import { Injectable, Injector } from '@angular/core';
import { LogLevel } from '@nuclos/nuclos-addon-api';
import { BehaviorSubject, Observable, Observer } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Addonusage } from '../addons/addon.service';
import { Logger } from '../log/shared/logger';
import { NuclosHttpService } from './nuclos-http.service';
import { SystemParameter, SystemParameters } from './system-parameters';

/**
 * Possible config values, exactly written like in the actual JSON file.
 */
enum Config {
	nuclosURL
}

@Injectable()
export class NuclosConfigService {
	private http: NuclosHttpService;
	private config: any;

	private addonConfig = new BehaviorSubject<Addonusage[] | undefined>(undefined);

	constructor(private injector: Injector, private $log: Logger) {
		// Http must be loaded via injector to prevent dependency cycles
		this.http = this.injector.get(NuclosHttpService);
	}

	/**
	 * Loads the config file.
	 */
	load(): Promise<boolean> {
		this.$log.info('Loading config...');
		return new Observable<boolean>((observer: Observer<boolean>) => {
			this.http.get('assets/config.json').subscribe(config => {
				this.$log.debug('Config: %o', config);
				this.config = config;
				observer.next(true);
				observer.complete();

				this.loadAddonConfiguration();
			});
		}).toPromise();
	}

	/**
	 * Returns the system parameters, possibly loading them from the server first.
	 */
	getSystemParameters(): Observable<SystemParameters> {
		return this.http
			.getCachedJSON(
				this.getRestHost() + '/meta/systemparameters',
				json => new SystemParameters(json)
			)
			.pipe(
				tap(params => {
					if (params.is(SystemParameter.ENVIRONMENT_DEVELOPMENT)) {
						this.$log.setLogLevel(LogLevel.ALL);
					} else {
						this.$log.setLogLevel(LogLevel.INFO);
					}
				})
			);
	}

	/**
	 * Returns the Nuclos URL, possibly replacing a "<host>" placeholder with the current location host.
	 *
	 * @returns {string}
	 */
	getNuclosURL(): string {
		return this.getString(Config.nuclosURL)
			.replace('<host>', window.location.hostname)
			.replace('<port>', window.location.port);
	}

	getRestHost(): string {
		return this.getNuclosURL() + '/rest';
	}

	getServerLogWebsocketURL(): string {
		return this.getNuclosURL() + '/websocket/serverlog';
	}

	/**
	 * Returns the full URL for the given resource (e.g. a button icon).
	 *
	 * @param resourceId
	 * @returns {string}
	 */
	getResourceURL(resourceId: string) {
		return this.getRestHost() + '/data/resource/' + resourceId;
	}

	getAddonUsages(): BehaviorSubject<Addonusage[] | undefined> {
		return this.addonConfig;
	}

	private getString(config: Config): string {
		let key: string = Config[config];
		let value = this.config[key];

		// this.$log.debug('Config: %o = %o', key, value);

		return value;
	}

	private loadAddonConfiguration(): void {
		this.http.getCachedJSON(this.getRestHost() + '/meta/addonusages').subscribe(addonUsages => {
			this.addonConfig.next(addonUsages.resultlists);
		});
	}
}
