import { HttpClient, HttpHandler, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { NOT_ACCEPTABLE, SERVICE_UNAVAILABLE, UNAUTHORIZED } from 'http-status-codes';
import { Observable, Subject, throwError as observableThrowError } from 'rxjs';

import { catchError, finalize, map } from 'rxjs/operators';
import { NuclosCacheService } from '../cache/shared/nuclos-cache.service';
import { Logger } from '../log/shared/logger';
import { ObservableUtils } from './observable-utils';

@Injectable()
export class NuclosHttpService extends HttpClient {
	private pendingRequests = new Map<number, string>();
	private requestsPendingSince: Date | undefined;
	private lastRequestID = 0;
	// TODO: Observable for error 401, register to it from authentication service
	private unauthorizedRequests = new Subject();

	constructor(
		protected handler: HttpHandler,
		protected $log: Logger,
		protected cacheService: NuclosCacheService,
		protected injector: Injector
	) {
		super(handler);
	}

	/**
	 * TODO: Get rid of @ts-ignore
	 */
	// @ts-ignore
	post<T>(
		url: string,
		body: any | null,
		options?: {
			headers?: HttpHeaders;
			observe?: 'body';
			params?:
				| HttpParams
				| {
						[param: string]: string | string[];
				};
			reportProgress?: boolean;
			responseType?: 'json';
			withCredentials?: boolean;
		}
	): Observable<T> {
		if (!options) {
			options = {};
		}
		if (!options.headers) {
			options.headers = new HttpHeaders();
		}

		options.withCredentials = true;
		options.headers = options.headers.set('Content-Type', 'application/json');

		return super.post<T>(url, body, options);
	}

	/**
	 * TODO: Get rid of @ts-ignore
	 */
	// @ts-ignore
	request<T>(
		method: string,
		url: string,
		options?: {
			body?: any;
			headers?: HttpHeaders | { [p: string]: string | string[] };
			params?: HttpParams | { [p: string]: string | string[] };
			observe?: 'body';
			reportProgress?: boolean;
			responseType?: 'json';
			withCredentials?: boolean;
		}
	): Observable<T> {
		if (!options) {
			options = {};
		}

		options.withCredentials = true;

		let urlString: any = url;

		const requestID = this.nextRequestID();

		let request = super.request<T>(method, url, options);

		this.$log.debug('%s %s with headers %o and body %o', method, url, options, options.body);

		return ObservableUtils.onSubscribe<T>(request, () =>
			this.requestStarted(requestID, urlString)
		).pipe(
			catchError(err => {
				this.handleError(err);
				return observableThrowError(err);
			}),
			finalize(() => this.requestFinished(requestID))
		);
	}

	nextRequestID() {
		return ++this.lastRequestID;
	}

	requestStarted(requestID: number, url: string) {
		if (!this.requestsPendingSince) {
			this.requestsPendingSince = new Date();
		}

		this.pendingRequests.set(requestID, url);
	}

	requestFinished(requestID: number) {
		this.pendingRequests.delete(requestID);

		if (!this.hasPendingRequest()) {
			this.requestsPendingSince = undefined;
		}
	}

	// // @ts-ignore
	// post<T>(
	// 	url: string,
	// 	body: any | null,
	// 	options?: {
	// 		headers?: HttpHeaders | { [p: string]: string | string[] };
	// 		observe?: 'body';
	// 		params?: HttpParams | { [p: string]: string | string[] };
	// 		reportProgress?: boolean;
	// 		responseType?: 'arraybuffer' | 'blob' | 'json' | 'text';
	// 		withCredentials?: boolean
	// 	}
	// ): Observable<T> {
	// 	// return undefined;
	// // }
	//
	// /**
	//  * Performs a POST request.
	//  *
	//  * It is assumed we always send JSON data, so the Content-Type header is set to 'application/json'.
	//  */
	// // post(url: string, body: any, options?: RequestOptions): Observable<Response> {
	// 	options = this.setCustomHeaders(
	// 		url,
	// 		options,
	// 		new Map<string, string>().set('Content-Type', 'application/json')
	// 	);
	// 	// @ts-ignore
	// 	return super.post(url, body, options) as Observable<T>;
	// }

	/**
	 * Same as {@link Http#get}, but caches the result.
	 *
	 * If an additional mapper is provided, the JSON is mapped again before caching.
	 */
	getCachedJSON(url: string, mapper?: (json: any) => any, options?: any): Observable<any> {
		let cache = this.cacheService.getCache('http.GET');

		let observable = this.get(url, options);
		if (mapper) {
			observable = observable.pipe(map(mapper));
		}

		return cache.get(url, observable);
	}

	hasPendingRequest() {
		return this.pendingRequests.size > 0;
	}

	/**
	 * How long since there were no pending requests (in ms).
	 */
	getPendingRequestTime() {
		if (!this.requestsPendingSince) {
			return -1;
		}

		return Date.now() - this.requestsPendingSince.getTime();
	}

	getUnauthorizedRequests(): Observable<any> {
		return this.unauthorizedRequests;
	}

	private handleError(err) {
		if (err.status === NOT_ACCEPTABLE && err.error.message) {
			// alert(err.error.message);
			this.$log.error(err.error);
		} else if (err.status === UNAUTHORIZED) {
			this.unauthorizedRequests.next();
		} else if (err.status === SERVICE_UNAVAILABLE) {
			if (window.location.href.indexOf('#/log') === -1) {
				window.location.href = '/#/logout';
			}
		}
	}
}
