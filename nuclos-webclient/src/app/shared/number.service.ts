import { Injectable } from '@angular/core';
import { NuclosI18nService } from '../i18n/shared/nuclos-i18n.service';
import { Logger } from '../log/shared/logger';
import { StringUtils } from './string-utils';
import NumberFormatOptions = Intl.NumberFormatOptions;

@Injectable()
export class NumberService {
	constructor(private nuclosI18nService: NuclosI18nService, private $log: Logger) {}

	format(value: number, precision?: number): string {
		if (value == null || typeof value !== 'number') {
			return '';
		}

		let result = '' + value;

		if (precision && precision > 0) {
			result = this.formatDecimal(value, precision);
		} else if (!Number.isInteger(value)) {
			precision = this.precision(value);
			result = this.formatDecimal(value, precision);
		}

		// this.$log.debug('Formatting number %o to %o (precision = %o)', value, result, precision);
		return result;
	}

	parseNumber(value: string, precision?: number): number | null {
		if (value === undefined || value === null || value === '') {
			return null;
		}

		if (typeof value === 'string') {
			let locale = this.nuclosI18nService.getCurrentLocale();

			value = StringUtils.replaceAll(value, locale.thousandsSeparator, '');
			value = StringUtils.replaceAll(value, locale.decimalSeparator, '.');
		}

		let result = parseFloat(value);

		if (precision !== undefined && !isNaN(result)) {
			result = +result.toFixed(precision);
		}

		return result;
	}

	private formatDecimal(value: number, precision: number) {
		try {
			let options: NumberFormatOptions = {
				minimumFractionDigits: precision,
				maximumFractionDigits: precision
			};

			let numberFormat = Intl.NumberFormat(
				this.nuclosI18nService.getCurrentLocale().key,
				options
			);

			let result = numberFormat.format(value);
			if (result === 'NaN') {
				result = '';
			}

			return result;
		} catch (e) {
			this.$log.warn('Could not format number %o, %o', value, e);
			return '' + value;
		}
	}

	private precision(value) {
		if (!isFinite(value)) {
			return 0;
		}

		let e = 1,
			p = 0;
		while (Math.round(value * e) / e !== value) {
			e *= 10;
			p++;
		}

		return p;
	}
}
