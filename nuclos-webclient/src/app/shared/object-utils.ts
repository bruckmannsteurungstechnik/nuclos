export class ObjectUtils {

	/**
	 * Use this as an alternative for lodash.cloneDeep, because lodash does not clone Maps recursively anymore.
	 */
	static cloneDeep(obj) {
		return JSON.parse(JSON.stringify(obj));
	}
}
