import { EMPTY, Observable } from 'rxjs';

import { concat, tap } from 'rxjs/operators';

export class ObservableUtils {

	/**
	 * Executes the given action when the given Observable is subscribed to.
	 */
	static onSubscribe<T>(
		observable: Observable<T>,
		action: () => any
	): Observable<T> {
		return EMPTY.pipe(
			tap(
				() => {},
				undefined,
				action
			),
			concat(observable),
		);
	}
}
