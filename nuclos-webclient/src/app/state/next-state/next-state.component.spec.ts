import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NextStateComponent } from './next-state.component';

xdescribe('NextStateComponent', () => {
	let component: NextStateComponent;
	let fixture: ComponentFixture<NextStateComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [NextStateComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(NextStateComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
