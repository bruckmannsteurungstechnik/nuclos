import { Component, ElementRef, OnInit } from '@angular/core';
import { Logger } from '@nuclos/nuclos-addon-api';
import { SwaggerUIBundle, SwaggerUIStandalonePreset } from 'swagger-ui-dist';
import * as urlParse from 'url-parse';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { NuclosHttpService } from '../../shared/nuclos-http.service';
import { SystemParameter } from '../../shared/system-parameters';

const swaggerUIBundle = SwaggerUIBundle;
const swaggerUIStandalonePreset = SwaggerUIStandalonePreset;

@Component({
	selector: 'nuc-swagger-ui',
	templateUrl: './swagger-ui.component.html',
	styleUrls: ['./swagger-ui.component.css']
})
export class SwaggerUiComponent implements OnInit {

	constructor(
		private el: ElementRef,
		private config: NuclosConfigService,
		private http: NuclosHttpService,
		private $log: Logger,
	) {
	}

	async ngOnInit() {
		let params = await this.config.getSystemParameters().toPromise();

		let supportedSubmitMethods: string[] = [];
		if (params.is(SystemParameter.ENVIRONMENT_DEVELOPMENT)) {
			// Allow to actually send requests via "Try it out" only in dev mode.
			supportedSubmitMethods = ['get', 'put', 'post', 'delete', 'options', 'head', 'patch', 'trace'];
		}

		// We must fetch the spec ourself, because swagger-ui will not send credentials when
		// fetching the spec and then some endpoints would not be visible.
		let url = this.config.getRestHost() + '/openapi.json';
		let spec: any = await this.http.get(url).toPromise();

		let parsedUrl = urlParse(this.config.getRestHost());
		this.$log.warn(parsedUrl);
		if (spec && spec.servers) {
			for (let server of spec.servers) {
				server.url = parsedUrl.origin + server.url;
			}
		}

		this.initSwagger(spec, supportedSubmitMethods);
	}

	private initSwagger(spec: any, supportedSubmitMethods: string[]) {
		const ui = swaggerUIBundle({
			spec: spec || {},
			domNode: this.el.nativeElement.querySelector('.swagger-container'),

			// deepLinking is incompatible with the anchor based routing of Angular
			deepLinking: false,

			filter: true,
			presets: [
				swaggerUIBundle.presets.apis,
				swaggerUIStandalonePreset
			],
			plugins: [
				swaggerUIBundle.plugins.DownloadUrl
			],
			layout: 'BaseLayout',
			supportedSubmitMethods: supportedSubmitMethods,
			apisSorter : 'alpha',
			operationsSorter : 'alpha',
		});

		// Hack to send all requests with credentials, because there is no configuration for it yet
		// See https://github.com/swagger-api/swagger-ui/issues/2895
		ui.fn.fetch.withCredentials = true;
	}
}
