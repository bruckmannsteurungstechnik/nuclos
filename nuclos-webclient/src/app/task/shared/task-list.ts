/**
 * Represents either a dynamic task list or a searchfilter based tasklist.
 */
export class TaskList {
	/**
	 * The entity class ID of the dynamic task list, if this is a dynamic task list.
	 * Else the entity class ID of the target entity, if this is a searchfilter based task list.
	 */
	entityClassId: string;

	name: string;

	/**
	 * Only available if this is a searchfilter based task list.
	 */
	searchfilter?: string;
}
