/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwoSideMultiSelectComponent } from './two-side-multi-select.component';

xdescribe('TwoSideMultiSelectComponent', () => {
	let component: TwoSideMultiSelectComponent;
	let fixture: ComponentFixture<TwoSideMultiSelectComponent>;

	const testList = [
		{title: 'A', selected: false},
		{title: 'B', selected: false},
		{title: 'C', selected: true},
		{title: 'D', selected: true},
		{title: 'E', selected: false},
		{title: 'F', selected: false}
	];

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [TwoSideMultiSelectComponent]
		})
			.compileComponents();

		TestBed.createComponent(TwoSideMultiSelectComponent).componentInstance.list = JSON.parse(JSON.stringify(testList));
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(TwoSideMultiSelectComponent);
		component = fixture.componentInstance;
		component.list = JSON.parse(JSON.stringify(testList));
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
		expect(component.list.length).toBe(6);
		expect(component.availableItems.length).toBe(4);
		expect(component.selectedItems.length).toBe(2);
	});

	it('should select all items', () => {
		component.selectAll();

		expect(component.list.length).toBe(6);
		expect(component.availableItems.length).toBe(0);
		expect(component.selectedItems.length).toBe(6);
	});

	it('should deselect all items', () => {
		component.deselectAll();

		expect(component.list.length).toBe(6);
		expect(component.availableItems.length).toBe(6);
		expect(component.selectedItems.length).toBe(0);
	});

	it('should select 1 item', async(() => {

		fixture.whenStable().then(() => {

			// mark first item
			let firstAvailableItem = fixture.debugElement.nativeElement.querySelector('.select-item');
			firstAvailableItem.click();

			expect(component.list.filter(item => item.marked).length).toBe(1);

			// select marked items
			fixture.debugElement.nativeElement.querySelector('#btn-selected-marked').click();

			expect(component.list.length).toBe(6);
			expect(component.availableItems.length).toBe(3);
			expect(component.selectedItems.length).toBe(3);
		});

	}));

});
