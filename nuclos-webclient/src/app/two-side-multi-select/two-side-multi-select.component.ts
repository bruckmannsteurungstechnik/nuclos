import {
	Component,
	EventEmitter,
	Input,
	OnChanges,
	OnInit,
	Output,
	SimpleChanges
} from '@angular/core';
import { ArrayUtils } from '../shared/array-utils';
import { Selectable } from './two-side-multi-select.model';

@Component({
	selector: 'nuc-two-side-multi-select',
	templateUrl: './two-side-multi-select.component.html',
	styleUrls: ['./two-side-multi-select.component.css']
})
export class TwoSideMultiSelectComponent implements OnInit, OnChanges {
	@Input()
	list: Selectable[];

	@Input()
	availableHeaderKey = '';

	@Input()
	selectedHeaderKey = '';

	@Input()
	singleSideMode = false;

	@Input()
	disableFilter = false;

	/**
	 * true if model was modified
	 * @type {EventEmitter<boolean>}
	 */
	@Output()
	onChange = new EventEmitter<void>();

	availableItems: Selectable[];
	selectedItems: Selectable[];

	disableSelectMarkedButton = true;
	disableDeselectMarkedButton = true;

	disableSelectAllButton = true;
	disableDeselectAllButton = true;

	lastClickedElement: Selectable;

	constructor() {}

	ngOnInit() {
		this.updateView(false);
	}

	ngOnChanges(_changes: SimpleChanges): void {
		this.updateView(false);
	}

	/**
	 * mark / unmark the clicked item
	 *
	 * marks / unmarks multiple items if clicked with shift key
	 *
	 * @param clickedItem
	 * @param $event
	 */
	toggleMark(clickedItem: Selectable, $event) {
		if ($event.shiftKey) {
			if (this.lastClickedElement) {
				let list =
					this.availableItems.indexOf(clickedItem) > -1
						? this.availableItems
						: this.selectedItems;
				let indexLastclick = list.indexOf(this.lastClickedElement);
				let indexClicked = list.indexOf(clickedItem);
				let from = indexLastclick;
				let to = indexClicked;
				if (from > to) {
					from = indexClicked;
					to = indexLastclick;
				}
				let toggleState = this.lastClickedElement.marked;
				for (let i = from; i <= to; i++) {
					list[i].marked = toggleState;
				}
			}
		} else {
			clickedItem.marked = !clickedItem.marked;
			this.lastClickedElement = clickedItem;
		}
		this.updateView(true);
	}

	select(item: Selectable) {
		item.selected = true;
		this.moveToEndOfList([item]);
		this.updateView(true);
	}

	deselect(item: Selectable) {
		item.selected = false;
		this.updateView(true);
	}

	selectAll() {
		this.list.forEach(item => (item.selected = true));
		this.updateView(true);
	}

	deselectAll() {
		this.list.forEach(item => (item.selected = false));
		this.updateView(true);
	}

	selectMarked() {
		let items: Selectable[] = [];
		this.list.forEach((item, index) => {
			if (!item.selected && item.marked) {
				item.selected = true;
				items.push(item);
			}
			item.marked = false;
		});

		this.moveToEndOfList(items);
		this.updateView(true);
	}

	deselectMarked() {
		this.list.forEach(item => {
			if (item.selected && item.marked) {
				item.selected = false;
				item.marked = false;
			}
		});
		this.updateView(true);
	}

	moveLeft(fromIndex: number) {
		let selectedList = this.list.filter(li => li.selected && li.name !== undefined);
		if (fromIndex > 0) {
			let toIndex = fromIndex - 1;
			this.switchSelectOption(selectedList, fromIndex, toIndex);
		}
	}

	/**
	 * item was dragged and dropped to reorder
	 * @param selectedItems
	 */
	public dropped(selectedItems) {
		// doesn't work - array changes will not apply outside of component
		// this.list = [...this.availableItems, ...selectedItems];

		let selectedCounter = -1;
		for (let i = 0; i < this.list.length; i++) {
			let item = this.list[i];
			if (item.selected) {
				selectedCounter++;
				this.list[i] = selectedItems[selectedCounter];
			}
		}

		this.updateView(true);
	}

	moveRight(fromIndex: number) {
		let selectedList = this.list.filter(li => li.selected && li.name !== undefined);
		if (selectedList.length > fromIndex) {
			let toIndex = fromIndex + 1;
			this.switchSelectOption(selectedList, fromIndex, toIndex);
		}
	}

	private updateView(isModified: boolean) {
		if (this.list) {
			this.selectedItems = this.list.filter(item => item.selected);
			this.availableItems = this.list
				.filter(item => !item.selected)
				.sort((a, b) => (a.name < b.name ? -1 : 1));
			this.disableSelectMarkedButton = this.availableItems.filter(i => i.marked).length === 0;
			this.disableDeselectMarkedButton =
				this.selectedItems.filter(i => i.marked).length === 0;
			this.disableSelectAllButton = this.availableItems.length === 0;
			this.disableDeselectAllButton = this.selectedItems.length === 0;

			if (isModified) {
				this.onChange.emit();
			}
		}
	}

	/**
	 * selected columns should be added at the end of the list
	 */
	private moveToEndOfList(columns: Selectable[]) {
		columns
			.sort((a, b) => (a.name > b.name ? -1 : 1))
			.forEach((column, i) => {
				let columnIndex = this.list.findIndex(it => it === column);
				ArrayUtils.move(this.list, columnIndex, this.list.length - 1 - i);
			});
	}

	private switchSelectOption(list: Selectable[], indexA: number, indexB: number) {
		let optionA = list[indexA];
		let optionB = list[indexB];

		this.list.forEach((listItem, index) => {
			if (listItem === optionA) {
				this.list[index] = optionB;
			}
			if (listItem === optionB) {
				this.list[index] = optionA;
			}
		});

		this.updateView(true);
	}
}
