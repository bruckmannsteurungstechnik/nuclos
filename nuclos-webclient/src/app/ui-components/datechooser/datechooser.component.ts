import {
	Component,
	ElementRef,
	EventEmitter,
	HostListener,
	Input,
	OnChanges,
	OnInit,
	OnDestroy,
	Output,
	SimpleChanges,
	ViewChild
} from '@angular/core';
import {
	NgbDate,
	NgbDateParserFormatter,
	NgbDatepickerI18n,
	NgbDateStruct,
	NgbInputDatepicker
} from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { Logger } from '../../log/shared/logger';
import { DatetimeService } from '../../shared/datetime.service';
import { NuclosDatepickerI18n } from './nuclos-datepicker-i18n.class';
import { NuclosNgbDateParserFormatter } from './nuclos-ngb-date-parser-formatter';

@Component({
	selector: 'nuc-datechooser',
	templateUrl: './datechooser.component.html',
	styleUrls: ['./datechooser.component.css'],
	providers: [
		{
			provide: NgbDatepickerI18n,
			useClass: NuclosDatepickerI18n
		},
		{
			provide: NgbDateParserFormatter,
			useClass: NuclosNgbDateParserFormatter
		}
	]
})
export class DatechooserComponent implements OnInit, OnDestroy, OnChanges {
	// TODO: Implement custom ControlValueAccessor and use the ngModel mechanism,
	// see e.g. https://stackoverflow.com/questions/34948961/angular-2-custom-form-input/34998780#34998780
	@Input() model;

	@Input() id;
	@Input() nameForHtml;
	@Input() nextFocusField;
	@Input() enabled;
	@Input() cssClasses;

	dateModel: NgbDateStruct;
	datePattern: string;
	highlightDate = moment();

	@ViewChild('datepicker') datepicker: NgbInputDatepicker;
	@ViewChild('datepickerInput') datepickerInput: ElementRef;

	@Output() onValueChange = new EventEmitter();
	@Output() modelChange = new EventEmitter<string | null>();

	private unsubscribe$ = new Subject<void>();

	constructor(
		private datetimeService: DatetimeService,
		private dateParser: NgbDateParserFormatter,
		private nuclosI18n: NuclosI18nService,
		private $log: Logger
	) {
		this.datePattern = this.datetimeService.getDatePattern();
		this.modelChange.pipe(distinctUntilChanged(), takeUntil(this.unsubscribe$)).subscribe(value => {
			if (!this.isInputFocused()) {
				this.highlightDate = moment();
				return;
			}

			this.highlightDate = this.datetimeService.parseDate(value);
			let yearMonth = {
				year: this.highlightDate.year(),
				month: this.highlightDate.month() + 1
			};

			this.$log.debug('Navigating to date %o', yearMonth);
			this.datepicker.navigateTo(yearMonth);
		});
	}

	ngOnInit() {}

	ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	ngOnChanges(changes: SimpleChanges): void {
		let change = changes['model'];
		if (change) {
			// this.rawInput = change.currentValue;
		}
	}

	getDateModel(): NgbDateStruct | undefined {
		this.updateModel();

		return this.dateModel;
	}

	mousedown($event: MouseEvent) {
		if ($event.button === 0) {
			this.openDatepicker();
		}
	}

	openDatepicker() {
		this.datepicker.open();

		// Re-focus the input, because toggling the datepicker steals the focus
		this.focusInput();
	}

	toggleDatepicker() {
		this.datepicker.toggle();

		// Re-focus the input, because toggling the datepicker steals the focus
		this.focusInput();
	}

	closeDatepicker() {
		this.datepicker.close();
	}

	focusInput() {
		// Using setTimeout() to regain the focus after the datepicker panel stole it.
		setTimeout(() => {
			if (this.datepickerInput) {
				this.datepickerInput.nativeElement.focus();
			}
		});
	}

	isInputFocused() {
		return this.datepickerInput && $(this.datepickerInput.nativeElement).is(':focus');
	}

	/**
	 * EvaluateClickOutside
	 * prevent closing datepicker, when clicking on it
	 * @return true when not clicking on datepicker
	 */
	isClickOutside(target: EventTarget) {
		return $(target).closest('ngb-datepicker').length === 0;
	}

	isHighlighted(date: { day: number; month: number; year: number }) {
		return (
			this.highlightDate.date() === date.day &&
			this.highlightDate.month() + 1 === date.month &&
			this.highlightDate.year() === date.year
		);
	}

	getFormattedDate() {
		return this.datetimeService.formatDate(this.model);
	}

	selectInputText() {
		this.datepickerInput.nativeElement.select();
	}

	setInputText(value: any) {
		this.datepickerInput.nativeElement.value = value;
	}

	isValid(): boolean {
		let value = this.getRawInput();
		let date = this.dateParser.parse(value);
		return !value || (date && (!isNaN(date.day) && !isNaN(date.month) && !isNaN(date.year)));
	}

	isDirty(): boolean {
		let input = this.getRawInput();
		return !!input;
	}

	getRawInput() {
		return this.datepickerInput && this.datepickerInput.nativeElement.value;
	}

	@HostListener('keydown', ['$event'])
	closeDatepickerOnTabKey($event) {
		if ($event.key === 'Tab') {
			// this.datepicker.close();
		}
	}

	commitValue() {
		this.datepickerInput.nativeElement.blur();
	}

	isEnabled() {
		return this.enabled === true || this.enabled === undefined;
	}

	setModelValue(value?: NgbDate) {
		let valueString: string | null = null;

		// Use raw input while the user is still typing, or when the datepicker submits no value for some reason...
		if (!value || this.isInputFocused()) {
			valueString = this.getRawInput();
		} else {
			if (
				value &&
				value.year !== undefined &&
				value.month !== undefined &&
				value.day !== undefined
			) {
				// value was set from datepicker component
				let date = moment(new Date(value.year, value.month - 1, value.day)).format(
					'YYYY-MM-DD'
				);

				valueString = date;
			}
		}

		this.modelChange.emit(valueString);
	}

	clickOutside() {
		this.commitValue();
		this.datepicker.close();
	}

	blur() {
		this.datepickerInput.nativeElement.blur();
		if (!this.dateModel && !this.getRawInput()) {
			this.$log.debug('blur(): Resetting empty date value');
			this.datepickerInput.nativeElement.value = '';
		}
	}

	private getNewDateModel() {
		let value = this.model;
		let newDateModel;

		if (value) {
			let date = new Date(value);
			newDateModel = new NgbDate(date.getFullYear(), date.getMonth() + 1, date.getDate());
		}

		return newDateModel;
	}

	private updateModel() {
		// Do not overwrite input value while the input is focused
		if (this.isInputFocused()) {
			return;
		}

		let newDateModel = this.getNewDateModel();

		if (!this.modelEquals(this.dateModel, newDateModel)) {
			this.dateModel = newDateModel;
		}
	}

	private modelEquals(model1: NgbDateStruct | undefined, model2: NgbDateStruct | undefined) {
		if (!model1 && !model2) {
			return true;
		} else if (model1 && model2) {
			return (
				model1.year === model2.year &&
				model1.month === model2.month &&
				model1.day === model2.day
			);
		} else {
			return false;
		}
	}
}
