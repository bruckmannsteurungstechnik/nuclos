import { Component, Input, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NuclosHttpService } from '../../shared/nuclos-http.service';

@Component({
	selector: 'nuc-icon-picker-modal',
	templateUrl: './icon-picker-modal.component.html',
	styleUrls: ['./icon-picker-modal.component.css']
})
export class IconPickerModalComponent implements OnInit {

	@Input()
	selectedIcon: string;

	icons: any[] = [];

	constructor(
		private activeModal: NgbActiveModal,
		private http: NuclosHttpService,
	) {
	}

	ngOnInit() {
		this.http.get('./assets/material-design-icons.json').pipe(take(1)).subscribe(
			(icons: any[]) => this.icons = icons
		);
	}

	selectIcon(iconObject) {
		this.activeModal.close(iconObject);
	}

	getFontAwesomeIconClass(iconObject): string {
		return 'fa-' + iconObject.id;
	}

	close() {
		this.activeModal.dismiss();
	}

}
