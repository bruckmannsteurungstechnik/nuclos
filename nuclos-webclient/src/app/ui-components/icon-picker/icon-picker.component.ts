import { Component, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IconPickerModalComponent } from './icon-picker-modal.component';
import { from as observableFrom, Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
	selector: 'nuc-icon-picker',
	templateUrl: './icon-picker.component.html',
	styleUrls: ['./icon-picker.component.css']
})
export class IconPickerComponent {

	@Input()
	pickerLabel: string;

	@Input()
	disabled = false;

	@Input()
	selectedIcon: string;

	@Output()
	selectedIconChange = new EventEmitter<any>();

	constructor(
		private modalService: NgbModal
	) {
	}

	iconSelected() {
	}

	selectIcon() {
		this.showIconPicker().pipe(take(1)).subscribe(
			result => {
				this.selectedIcon = result ? result.name : undefined;
				if (this.selectedIconChange) {
					this.selectedIconChange.emit(this.selectedIcon);
				}
			}
		);
	}

	private showIconPicker(): Observable<any> {
		const modalRef = this.modalService.open(IconPickerModalComponent, {size: 'lg'});
		modalRef.componentInstance.selectedIcon = this.selectedIcon;
		return observableFrom(modalRef.result);
	}

}
