import {
	Component,
	ContentChildren,
	EventEmitter,
	forwardRef,
	Input,
	OnChanges,
	OnInit,
	Output,
	QueryList,
	SimpleChanges,
	TemplateRef,
	ViewChild
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { NgbTabChangeEvent, NgbTabset } from '@ng-bootstrap/ng-bootstrap';
import { TabComponent } from './tab/tab.component';

@Component({
	selector: 'nuc-tab-bar',
	templateUrl: './tab-bar.component.html',
	styleUrls: ['./tab-bar.component.css'],
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => TabBarComponent),
			multi: true
		}
	]
})
export class TabBarComponent implements ControlValueAccessor, OnInit, OnChanges {
	@Input() canAdd = false;
	@Input() values: any[];
	@Input() titleTemplate: TemplateRef<any>;
	@Input() contentTemplate: TemplateRef<any>;
	@Input() idForAdd: string;

	@Output() addValue = new EventEmitter();

	@ContentChildren(TabComponent) tabs: QueryList<TabComponent>;

	@ViewChild('tabset') ngbTabset: NgbTabset;

	addTabId = 'tab-add';

	/**
	 * The selected value (which is displayed in the currently selected tab).
	 */
	value: any;

	_onChange: any;
	_onTouched: any;
	disabled = false;

	private selectedTabId;

	constructor() {}

	ngOnInit() {}

	ngOnChanges(changes: SimpleChanges): void {}

	tabId(index: number) {
		return 'tab-' + index;
	}

	addTab(event) {
		event.stopPropagation();
		event.preventDefault();
		this.addValue.emit('');
	}

	getSelectedTabId() {
		return this.selectedTabId;
	}

	emitTabChange(event: NgbTabChangeEvent) {
		if (event.nextId === this.addTabId) {
			event.preventDefault();
			this.addValue.emit('');
		} else {
			this.selectedTabId = event.nextId;

			let selectedValue = this.getSelectedValue();
			if (this._onChange) {
				this._onChange(selectedValue);
			}
		}
	}

	registerOnChange(fn: any): void {
		this._onChange = fn;
	}

	registerOnTouched(fn: any): void {
		this._onTouched = fn;
	}

	setDisabledState(isDisabled: boolean): void {
		this.disabled = isDisabled;
	}

	writeValue(obj: any): void {
		this.value = obj;
		this.selectTab(this.value);
	}

	private getSelectedValue() {
		// First find the index of the currently selected tab
		let selectedIndex = -1;
		this.ngbTabset.tabs.find((tab, index) => {
			if (tab.id === this.selectedTabId) {
				selectedIndex = index;
				return true;
			}
			return false;
		});

		// Selected tab index corresonds to value index
		let selectedValue = undefined;
		if (this.values && selectedIndex >= 0 && selectedIndex < this.values.length) {
			selectedValue = this.values[selectedIndex];
		}

		return selectedValue;
	}

	private selectTab(value) {
		if (this.values) {
			let tabIndex = this.values.indexOf(value);
			this.selectedTabId = this.tabId(tabIndex);
			setTimeout(() => {
				this.ngbTabset.select(this.getSelectedTabId());
			});
		}
	}
}
