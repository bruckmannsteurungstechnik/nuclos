import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { ClickOutsideModule } from '../click-outside/click-outside.module';
import { I18nModule } from '../i18n/i18n.module';
import { LogModule } from '../log/log.module';
import { CssFromSystemparameterComponent } from './css-from-systemparameter/css-from-systemparameter.component';
import { DatechooserComponent } from './datechooser/datechooser.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { TabBarComponent } from './tab-bar/tab-bar.component';
import { TabContentDirective } from './tab-bar/tab-content.directive';
import { TabTitleDirective } from './tab-bar/tab-title.directive';
import { TabComponent } from './tab-bar/tab/tab.component';
import { IconPickerComponent } from './icon-picker/icon-picker.component';
import { IconPickerModalComponent } from './icon-picker/icon-picker-modal.component';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,

		ClickOutsideModule,
		I18nModule,
		LogModule,

		AutoCompleteModule,
		NgbModule,
		NgbModalModule,
	],
	declarations: [
		CssFromSystemparameterComponent,
		DatechooserComponent,
		TabBarComponent,
		TabComponent,
		TabContentDirective,
		TabTitleDirective,
		DropdownComponent,
		IconPickerComponent,
		IconPickerModalComponent
	],
	exports: [
		CssFromSystemparameterComponent,
		DatechooserComponent,
		DropdownComponent,
		TabBarComponent,
		TabComponent,
		TabContentDirective,
		TabTitleDirective,
		IconPickerComponent
	],
	providers: [],
	entryComponents: [
		IconPickerModalComponent
	]
})
export class UiComponentsModule {
}
