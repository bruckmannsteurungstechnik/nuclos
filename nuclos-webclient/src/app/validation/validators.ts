import { AttributeType, EntityAttrMeta } from '../entity-object-data/shared/bo-view.model';
import { Logger } from '../log/shared/logger';
import { NumberService } from '../shared/number.service';

export abstract class Validator<T extends AttributeType> {
	abstract validate(value: T, meta: EntityAttrMeta): any;
}

export class NumberValidator extends Validator<'Integer' | 'Decimal'> {
	constructor(private numberService: NumberService) {
		super();
	}

	validate(value, meta: EntityAttrMeta) {
		let precision = meta.getPrecision();

		if (typeof value !== 'number') {
			value = this.numberService.parseNumber(value, precision);
		}

		if (!isNaN(value) && precision !== undefined) {
			value = +(<number>value).toFixed(precision);
		} else {
			Logger.instance.debug('Numbervalidator: precision undefined for value %o', value);
		}

		return value;
	}
}

export class BooleanValidator extends Validator<'Boolean'> {
	validate(value) {
		return !!value;
	}
}

export class DateValidator extends Validator<'Date'> {
	private dateFormat = /^\d{4}-\d{2}-\d{2}$/;

	validate(value) {
		if (typeof value !== 'string' || (value.length > 0 && !value.match(this.dateFormat))) {
			throw('Illegal value for Date type: "' + value + '"');
		}
		return value;
	}
}

export class TimestampValidator extends Validator<'Timestamp'> {
	/**
	 * TODO: Use a standard format here, must be changed in the REST service first.
	 * See http://apiux.com/2013/03/20/5-laws-api-dates-and-times/
	 */
	private timestampFormat = /^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/;

	validate(value) {
		if (typeof value !== 'string' || !value.match(this.timestampFormat)) {
			throw('Illegal value for Timestamp type: "' + value + '"');
		}

		return value;
	}
}

export class DocumentValidator extends Validator<'Document'> {
	validate(value) {
		// TODO: Can we validate a Document?
		return value;
	}
}

export class StringValidator extends Validator<'String'> {
	validate(value, meta: EntityAttrMeta) {
		// Reference fields have the type 'String'...
		// TODO: This should be a separate type.
		if (typeof value === 'object' && value.hasOwnProperty('id')) {
			if (meta.isReference()) {
				return value;
			} else {
				return value.name;
			}
		}
		return '' + value;
	}
}

export class DummyValidator extends Validator<any> {
	validate(value) {
		return value;
	}
}
